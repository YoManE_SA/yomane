﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Risk
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuItem;
        
        public override string Name { get { return "Risk Items"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return "Manage risk items."; } }

        public Module() : base(Bll.Risk.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu item
            menuItem = new ApplicationMenu("Risk Items", "~/Risk/BlockItems", null, 60);

            //Register the route
            Application.RegisterRoute("Risk/BlockItems", "BlockItems", typeof(Controls.DataTablePage), module:this);

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {               
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {                        
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Risk", menuItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuItem.Parent.RemoveChild(menuItem);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "BlockItems")
            {
                page.PageController.LoadItem += PageController_LoadItemBlockItems;
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Risk/Filter.ascx"));
                page.AddControlToList(page.LoadControl("~/Modules/Risk/List.ascx"));
                page.AddControlToForm("Data", page.LoadControl("~/Modules/Risk/Data.ascx"),this, "", Bll.Risk.RiskItem.SecuredObject);
            }
        }
        
        void PageController_LoadItemBlockItems(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            if (page.PageController.DataKey["RiskItemUniqueId"] != null)
            {
                string riskItemUniqueId = page.PageController.DataKey["RiskItemUniqueId"].ToString();
                var loadedItem = Netpay.Bll.Risk.RiskItem.RiskItemFromUniqueStr(riskItemUniqueId);
                page.SetItemData(loadedItem);
                page.ItemID = loadedItem.ID;
                page.FormButtons.EnableSave = false;
            }

            page.FormButtons.EnableDelete = Bll.Risk.RiskItem.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Delete);
        }
    }
}