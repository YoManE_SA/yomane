﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Risk
{
    public partial class Data : Controls.TemplateControlBase
    {
        public Bll.Risk.RiskItem Item { get { return TemplatePage.GetItemData<Bll.Risk.RiskItem>(); } set { TemplatePage.SetItemData(value); } }
        protected string MerchantName { get; private set; }

        protected override void OnLoad(EventArgs e)
        {
            if (!IsPostBack)
            {
                rbtRiskItemType.SelectedIndex = (int)Bll.Risk.RiskItem.RiskItemType.Black_List;
                mvRiskItemTypeView.ActiveViewIndex = (int)Bll.Risk.RiskItem.RiskItemType.Black_List;

                // Set items of radio button selection rbtRiskItemType for risk item type selection
                var riskItemTypeFields = (typeof(Bll.Risk.RiskItem.RiskItemType)).GetFields();
                foreach (var riskItemTypeField in riskItemTypeFields)
                {
                    if (riskItemTypeField.Name.Equals("value__")) continue;
                    ListItem item = new ListItem(riskItemTypeField.Name.Replace('_',' '), riskItemTypeField.GetRawConstantValue().ToString());
                    rbtRiskItemType.Items.Add(item);
                }
            }
            TemplatePage.PageController.SaveItem += Save_Click;
            TemplatePage.PageController.DeleteItem += Delete_Click;

            ddlRiskListType.ExcludeValue = Netpay.Bll.Risk.RiskItem.RiskListType.Black.ToString();
            ddlCommonValueType.ExcludeValue = Netpay.Bll.Risk.RiskItem.RiskValueType.AccountValue1.ToString();

            base.OnLoad(e);
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            Item = Netpay.Bll.Risk.RiskItem.RiskItemFromUniqueStr(hfRiskItemID.Value);
            if (Item != null)
                Item.Delete();
        }

        protected override void DataBindChildren()
        {
            if (Item == null) Item = new Bll.Risk.RiskItem();
            if (Item.ID > 0)
            {
                if (Item.MerchantID != null)
                {
                    var accountInfo = Bll.Accounts.Account.GetMerchantNames(new List<int> { (int)Item.MerchantID }).FirstOrDefault();
                    if (accountInfo.Key == Item.MerchantID)
                        MerchantName = accountInfo.Value;
                }
            }
            base.DataBindChildren();
        }
        
        protected void Save_Click(object sender, EventArgs e)
        {
            if (Item != null)
                return; // Don't support editing

            Item = new Bll.Risk.RiskItem();

            if (rbtRiskItemType.SelectedIndex == (int)Bll.Risk.RiskItem.RiskItemType.Black_List)
            {
                //// Create Black List //// 

                // Credit Card Number
                var riskItemValuesList = new List<Bll.Risk.RiskItem.RiskItemValue>();
                var riskItemValue = new Bll.Risk.RiskItem.RiskItemValue() { Value = txtBlackValue.Text, ValueType = Bll.Risk.RiskItem.RiskValueType.AccountValue1 };
                riskItemValuesList.Add(riskItemValue);
                
                // Create Black List Item
                Bll.Risk.RiskItem.Create(Bll.Risk.RiskItem.RiskSource.Merchant, Bll.Risk.RiskItem.RiskListType.Black, riskItemValuesList, null, null, null, null, null, null, null, txtBlackComment.Text, null);
            }else if (rbtRiskItemType.SelectedIndex == (int)Bll.Risk.RiskItem.RiskItemType.White_List)
            {
                //// Create White List //// 

                // Merchant Id
                int? merchantId = null;
                if (apWhiteMerchant.Value.HasValue)
                {
                    var account = Bll.Accounts.Account.LoadAccount(apWhiteMerchant.Value.Value);
                    if (account != null && account.AccountType == Bll.Accounts.AccountType.Merchant)
                    merchantId = account.MerchantID.Value;
                }

                // Credit Card Number
                string cardNumber = txtWhiteValue.Text;
                var riskItemValuesList = new List<Bll.Risk.RiskItem.RiskItemValue>();
                var riskItemValue = new Bll.Risk.RiskItem.RiskItemValue() { Value = cardNumber, ValueType = Bll.Risk.RiskItem.RiskValueType.AccountValue1 };
                riskItemValuesList.Add(riskItemValue);

                // Credit Card Expiration Date
                byte? expirationMonth = (byte?)mddExpirationMonth.SelectedMonth;
                short? expirationYear = (short?)yddExpirationYear.SelectedYear;

                // Card Holder
                string cardHolder = txtCardHolder.Text;

                // Level
                Bll.Risk.RiskItem.RiskListType? riskListType = ddlRiskListType.SelectedValue.ToNullableEnumByValue<Netpay.Bll.Risk.RiskItem.RiskListType>();

                // Comment 
                string comment = txtWhiteComment.Text;

                // IP
                string ip = Request.UserHostAddress;

                // Create White List Item
                if (riskListType != null && cardHolder.Length > 0 && merchantId != null)
                    Bll.Risk.RiskItem.Create(Bll.Risk.RiskItem.RiskSource.Merchant, (Bll.Risk.RiskItem.RiskListType)riskListType, riskItemValuesList, cardHolder, merchantId, expirationMonth, expirationYear, null, null, null, comment, ip);
            }else if (rbtRiskItemType.SelectedIndex == (int)Bll.Risk.RiskItem.RiskItemType.Item_Black_List)
            {
                //// Create Black List Common //// 

                // Merchant Id
                int? merchantId = null;
                if (apCommonMerchant.Value.HasValue)
                {
                    var account = Bll.Accounts.Account.LoadAccount(apCommonMerchant.Value.Value);
                    if (account != null && account.AccountType == Bll.Accounts.AccountType.Merchant)
                    merchantId = account.MerchantID.Value;
                }

                // Value Type
                Bll.Risk.RiskItem.RiskValueType? valueType = ddlCommonValueType.SelectedValue.ToNullableEnumByValue<Netpay.Bll.Risk.RiskItem.RiskValueType>();

                // Value
                string value = txtCommonValue.Text;

                // Value List - from ValueType and Value
                var riskItemValuesList = new List<Bll.Risk.RiskItem.RiskItemValue>();
                if (valueType != null)
                {
                    var riskItemValue = new Bll.Risk.RiskItem.RiskItemValue() { Value = value, ValueType = (Bll.Risk.RiskItem.RiskValueType)valueType };
                    riskItemValuesList.Add(riskItemValue);
                }
                
                // Comment 
                string comment = txtCommonComment.Text;

                // Create Common Black List Item
                if (valueType != null)
                    Bll.Risk.RiskItem.Create(Bll.Risk.RiskItem.RiskSource.Merchant, Bll.Risk.RiskItem.RiskListType.Black, riskItemValuesList, null, null, null, null, null, null, null, comment, null);
            }

            ClearFormFields();

            // Change to list view
            TemplatePage.PageController.Mode = Admin.Controls.DataPageController.ViewMode.List;
        }

        private void ClearFormFields()
        {
            apCommonMerchant.Clear();

            ddlCommonValueType.Value = null;

            apWhiteMerchant.Clear();

            ddlRiskListType.Value = null;

            mddExpirationMonth.Value = null;
            yddExpirationYear.Value = null;
        }


        protected void OnRadioRiskItemTypeSelectedIndexChanged(object sender, EventArgs e)
        {
            int activeViewIndex = 0;
            if (rbtRiskItemType != null && rbtRiskItemType.SelectedValue != null && rbtRiskItemType.SelectedValue.Length > 0)
            {
                int tmp = 0;
                if (int.TryParse(rbtRiskItemType.SelectedValue, out tmp))
                    activeViewIndex = tmp;
            }
            mvRiskItemTypeView.ActiveViewIndex = activeViewIndex;

            upRiskItemTypeView.Update();
        }

    }
}