﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.LogsHistory.Filter" %>

<admin:FilterSection runat="server" Title="History Log - Filter">
    <div class="form-group">
        <asp:Label ID="lblAccount" runat="server" Text="Merchant Name/ID" />
        <admin:AccountPicker runat="server" ID="MerchantNameIdFilter" LimitToType="Merchant" UseTargetID="false" />
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblStatus" runat="server" Text="Date Range" />
                <JQ:DateRange runat="server" ID="DateRangeFilter" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label1" runat="server" Text="ID" />
                <JQ:IntRange runat="server" ID="HitoryIdFilter" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <asp:Label ID="Label2" runat="server" Text="Reference Number" />
                <asp:TextBox runat="server" ID="SourceIdentityFilter" CssClass="form-control" />
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                Event Type
                <netpay:EnumDropDown runat="server" ID="EventType" CssClass="form-control" ViewEnumName="Netpay.CommonTypes.LogHistoryType" />
            </div>
        </div>
    </div>
</admin:FilterSection>
