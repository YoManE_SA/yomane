﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.LogsHistory
{
    public partial class Data : Controls.TemplateControlBase
    {

        protected Bll.Logs.LogHistory Item
        {
          get { return TemplatePage.GetItemData<Bll.Logs.LogHistory>(); }
          set { TemplatePage.SetItemData(value); }
        }

        protected List<Bll.Logs.LogHistory> LogsHistoryList { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            if (Item == null)
            {
                Item = new Bll.Logs.LogHistory();
            }
        
            base.DataBindChildren();
        }
    }
}