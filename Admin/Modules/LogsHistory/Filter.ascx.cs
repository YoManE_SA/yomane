﻿using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.LogsHistory
{
    public partial class Filter : Controls.TemplateControlBase
    {
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
        {
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Netpay.Bll.Logs.LogHistory.SearchFilters();

            //The Merchant Name/ID input:
            if (MerchantNameIdFilter.Value.HasValue)
            {
                var merchant = Bll.Merchants.Merchant.LoadByAccountId(MerchantNameIdFilter.Value.Value);
                if (merchant != null)
                    sf.MerchantId = new Range<int?>(merchant.ID);
            }

            //The Date Range Input
            sf.DateRange = DateRangeFilter.Value;

            //The ID input
            sf.HistoryId = HitoryIdFilter.Value;

            //The Source Identity Input
            int SourceIdentityInt;
            bool tryParse = int.TryParse(SourceIdentityFilter.Text, out SourceIdentityInt);
            if (tryParse) sf.SourceIdentity = SourceIdentityInt;

            //The Event Type input
            sf.LogHistoryType = EventType.Value.ToNullableEnum<Netpay.CommonTypes.LogHistoryType>();

            TemplatePage.SetFilter(sf);
        }
    }
}