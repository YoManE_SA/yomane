﻿using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.LogsHistory
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuLog;
        public override string Name { get { return "LogsHistory"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return ""; } }

        public Module() : base(Bll.Log.Module.Current) { }
                

        protected override void OnInit(EventArgs e)
        {
            //Create menu 
            menuLog = new ApplicationMenu("History", "~/Logs/LogsHistory", null, 170);

            //Register route
            Application.RegisterRoute("Logs/LogsHistory", "LogsHistory", typeof(Controls.DataTablePage), module:this);

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {                
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {                
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Logs", menuLog);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuLog.Parent.RemoveChild(menuLog);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "LogsHistory")
            {
                page.PageController.LoadItem += PageController_LogsHistory_LoadItem;
                page.AddControlToList(page.LoadControl("~/Modules/LogsHistory/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/LogsHistory/Filter.ascx"));
                page.AddControlToForm("Data", page.LoadControl("~/Modules/LogsHistory/Data.ascx"), this, "", Bll.Logs.LogHistory.SecuredObject);
            }
        }

        void PageController_LogsHistory_LoadItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            page.ItemID = page.PageController.DataKey["HistoryId"].ToNullableInt();
            var item = Netpay.Bll.Logs.LogHistory.Load(page.ItemID.GetValueOrDefault());
            if (page.ItemID != null) page.SetItemData(item);
            page.FormButtons.EnableSave = false;
            page.FormButtons.EnableDelete = false;

        }
    }
}