﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.LogsHistory.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Log History">
    <Header></Header>
    <Body>

        <div class="table-responsive">
            <admin:AdminList runat="server" SaveAjaxState="true" ID="rptList" DataKeyNames="HistoryId" AutoGenerateColumns="false" PageSize="25" BubbleLoadEvent="true">
                <Columns>
                    <%--The History ID coloum--%>
                    <asp:TemplateField HeaderText="History ID" SortExpression="History_id">
                        <ItemTemplate>
                            <%# Eval("HistoryId") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%--The History Date coloum--%>
                    <asp:TemplateField HeaderText="History Date" SortExpression="InsertDate">
                        <ItemTemplate>
                            <%# Eval("InsertDate") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%--The Type coloum--%>
                    <asp:TemplateField HeaderText="Type" SortExpression="HistoryTypeName">
                        <ItemTemplate>
                            <%# Eval("HistoryTypeName") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%--The Merchant coloum--%>
                    <asp:TemplateField HeaderText="Merchant" SortExpression="MerchantName">
                        <ItemTemplate>
                            <%# Eval("MerchantName") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%--The Reference Number coloum--%>
                    <asp:TemplateField HeaderText="Reference Number" SortExpression="SourceIdentity">
                        <ItemTemplate>
                            <%# Eval("SourceIdentity") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%--The IP Address coloum--%>
                    <asp:TemplateField HeaderText="IP Address" SortExpression="InsertIPAddress">
                        <ItemTemplate>
                            <%# Eval("InsertIPAddress") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </admin:AdminList>
        </div>

    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd="false" />
    </Footer>
</admin:ListSection>
