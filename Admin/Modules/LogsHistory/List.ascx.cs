﻿using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.LogsHistory
{
    public partial class List : Controls.TemplateControlBase
    {
        protected Dictionary<int, string> m_merchantNames;

        protected override void OnLoad(EventArgs e)
        {
            rptList.DataBinding += List_DataBinding;
            base.OnLoad(e);
        }

        private void List_DataBinding(object sender, EventArgs e)
        {
            var list = Bll.Logs.LogHistory.Search(TemplatePage.GetFilter<Bll.Logs.LogHistory.SearchFilters>(), rptList);
            if (list != null)
            {
                m_merchantNames = Bll.Accounts.Account.GetMerchantNames((list.Where(i => i.MerchantId != null).Select(i => i.MerchantId.Value).ToList()));
                rptList.DataSource = list;
            }
        }

        protected string GetMerchantName(int id)
        {
            if (id <= 0)
                return null;
            string ret;
            if (!m_merchantNames.TryGetValue(id, out ret)) return null;
            return ret;
        }

    }
}