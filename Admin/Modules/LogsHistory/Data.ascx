﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Data.ascx.cs" Inherits="Netpay.Admin.LogsHistory.Data" %>
<admin:FormSections runat="server" Title="Details">
    <!-- Data -->
    <div class="col-md-12">
        <div class="row">
            <h4>Data:</h4>
            <div class="panel panel-default">
                <div class="panel-body">
                    <span class="BreakRow">
                        <%# Item.VariableChar %>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <!-- XML -->
    <div class="col-md-12">
        <div class="row">
            <h4>XML:</h4>
            <div class="panel panel-default">
                <div class="panel-body">
                    <span class="BreakRow">
                        <%# Item.VariableXML!=null? Item.VariableXML.ToString().Replace("<", "&lt;").Replace(">", "&gt;") : "" %>
                    </span>
                </div>
            </div>
        </div>
    </div>

</admin:FormSections>
