﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Bll;
using Netpay.Bll.Accounts;
using Netpay.Bll.Merchants;

namespace Netpay.Admin.Fraud.Rules
{
    public partial class FraudRules : Controls.TemplateControlBase
    {
        public int? RuleAccountID { get; set; }

        public List<FraudDetection.DetectionRuleBase> currentItems { get;set;}

        public int? AccountPageAccountID
        {
            get
            {
                if (Page is Controls.AccountPage)
                {
                    if ((Page as Controls.AccountPage).Account != null)
                    {
                        return (Page as Controls.AccountPage).Account.AccountID;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        public int SimulatorAccountID
        {
            get
            {
                if (RuleAccountID != null)
                    return RuleAccountID.Value;

                int merchantId = ddlSimulatorMerchant.SelectedValue.ToInt32();
                Merchant merchant = Merchant.Load(merchantId);
                return merchant.AccountID;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            if (TemplatePage != null && TemplatePage.TemplateName == "Merchant")
            {
                TemplatePage.PageController.SaveItem += Save;
            }

            if (TemplatePage != null && TemplatePage.TemplateName != "Merchant")
            {
                var simulateButton = new Button() { Text = "Simulation", CssClass = "btn btn-primary", CommandName = "Show" };
                simulateButton.Command += Simulate_Command;
                TemplatePage.FormButtons.Controls.Add(simulateButton);
            }

            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            if (TemplatePage != null && TemplatePage.TemplateName == "Merchant" && TemplatePage.ItemID.HasValue)
            {
                RuleAccountID = TemplatePage.ItemID;
            }

            if (RuleAccountID.HasValue)
            {
                Account account = Account.LoadAccount(RuleAccountID.Value);
                divMerchantName.InnerText = $"Rules for merchant '{account.AccountName}', account id:{account.AccountID}, merchant id:{account.MerchantID}, merchant number:{account.AccountNumber}";
            }
            else
                divMerchantName.InnerText = "Global Rules";

            rptRules.DataSource = FraudDetection.DetectionRuleBase.Load(RuleAccountID);
            base.DataBindChildren();
        }

        public void Save(object sender, EventArgs e)
        {
            foreach (var v in GetItems()) v.Save();
            //DataBindChildren();
            //upList.Update();
        }

        protected List<FraudDetection.DetectionRuleBase> GetItems()
        {
            if (Page is Controls.AccountPage)
            {
                currentItems = FraudDetection.DetectionRuleBase.Load(AccountPageAccountID);
            }
            else
            {
                currentItems = FraudDetection.DetectionRuleBase.Load(RuleAccountID);
            }

            foreach (RepeaterItem ri in rptRules.Items)
            {
                var itemId = (ri.FindControl("hfItemID") as HiddenField).Value;
                var item = currentItems.Where(v => v.Name == itemId).SingleOrDefault();
                if (item == null) continue;
                item.IsEnabled = (ri.FindControl("chkIsEnabled") as CheckBox).Checked;
                foreach (RepeaterItem tri in (ri.FindControl("rptThreshold") as Repeater).Items)
                {
                    var hfPeriod = (tri.FindControl("hfPeriod") as HiddenField).Value.ToNullableEnum<FraudDetection.Threshold.ThresholdPeriod>().GetValueOrDefault();
                    var hfType = (tri.FindControl("hfType") as HiddenField).Value.ToNullableEnum<FraudDetection.Threshold.ThresholdType>().GetValueOrDefault();
                    var hfAction = (tri.FindControl("hfAction") as HiddenField).Value.ToNullableEnum<FraudDetection.Action.ActionType>().GetValueOrDefault();
                    var threshold = item.Thresholds.Where(v => v.Period == hfPeriod && v.Type == hfType && v.Action == hfAction).SingleOrDefault();
                    if (threshold != null)
                    {
                        var spValue = (tri.FindControl("spValue") as Controls.GlobalControls.JQueryControls.Spinner).Value;
                        if (threshold is FraudDetection.Threshold<int>)
                            (threshold as FraudDetection.Threshold<int>).Value = spValue.GetValueOrDefault();
                        else if (threshold is FraudDetection.Threshold<decimal>)
                            (threshold as FraudDetection.Threshold<decimal>).Value = spValue.GetValueOrDefault();
                    }
                }
            }
            return currentItems;
        }

        public void Simulate_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Show":
                    dlgSimulate.DataBind();
                    dlgSimulate.RegisterShow();
                    break;
                case "Run":
                    var items = GetItems();

                    FraudDetection.FraudDetectionManager.SimulationResult result = FraudDetection.FraudDetectionManager.Simulate(SimulatorAccountID, items);
                    rptSummary.DataSource = result.Summary;
                    rptSummary.DataBind();
                    rptResult.DataSource = result.Results;
                    rptResult.DataBind();
                    dlgSimulate.UpdatePanel.Update();
                    break;
            }
        }
    }
}