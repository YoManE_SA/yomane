<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GlobalRules.ascx.cs" Inherits="Netpay.Admin.Modules.Fraud.Rules.GlobalRules" %>
<%@ Register Src="~/Modules/Fraud/Rules/FraudRules.ascx" TagPrefix="uc1" TagName="FraudRules" %>
<div style="padding:20px;background-color:#f0f0f0;">
        
    <admin:AccountPicker runat="server" ID="ctlAccount" UseTargetID="false" LimitToType="Merchant" />
    <br />
    <asp:Button ID="btnEditMerchant" runat="server" Text="Edit Merchant Rules" CssClass="btn btn-primary" OnClick="btnEditMerchant_Click" />
    <asp:Button ID="btnEditGlobals" runat="server" Text="Edit Global Rules" CssClass="btn btn-primary" OnClick="btnEditGlobals_Click" />
    <div id="divAlert" Visible="False" class="alert-warning" runat="server">Select a merchant</div>

</div>
<div style="padding:20px;">
    <uc1:FraudRules runat="server" ID="FraudRules" />
</div>
<admin:DataButtons runat="server" EnableSave="true" OnSave="Save_Command" >
    <%--<asp:Button runat="server" Text="Simulate" CssClass="btn btn-primary" OnClick="Simulate_Command" />--%>
</admin:DataButtons>
