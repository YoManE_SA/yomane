﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Modules.Fraud.Rules
{
    public partial class GlobalRules : System.Web.UI.UserControl
    {
        protected override void OnLoad(EventArgs e)
        {
            FraudRules.RuleAccountID = ctlAccount.Value;
            base.OnLoad(e);
        }

        protected void Save_Command(object sender, EventArgs e)
        {
            FraudRules.Save(sender, e);
        }

        protected void Simulate_Command(object sender, EventArgs e)
        {
            FraudRules.Simulate_Command(sender, new CommandEventArgs("Show", ""));
        }

        /*
        protected void Merchant_SelectedIndexChanged(object sender, EventArgs e)
        {
            FraudRules.RuleMerchantId = ddlMerchant.Value.ToNullableInt();
            FraudRules.DataBind();
        }
        */

        protected void btnEditMerchant_Click(object sender, EventArgs e)
        {
            divAlert.Visible = !ctlAccount.Value.HasValue;
            FraudRules.RuleAccountID = ctlAccount.Value;
            FraudRules.DataBind();
        }

        protected void btnEditGlobals_Click(object sender, EventArgs e)
        {
            divAlert.Visible = false;
            FraudRules.RuleAccountID = null;
            FraudRules.DataBind();
        }
    }
}