﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FraudRules.ascx.cs" Inherits="Netpay.Admin.Fraud.Rules.FraudRules" %>
<admin:FormSections runat="server" Title="Fraud Rules" Flexible="false">

    <div id="divMerchantName" runat="server" style="font-weight: bold;" class="row"></div>
    <br />

    <div class="row">
            <NP:UpdatePanel runat="server" ID="upList" RenderMode="Block" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <netpay:DynamicRepeater runat="server" ID="rptRules">
                        <ItemTemplate>
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <asp:HiddenField runat="server" ID="hfItemID" Value='<%# Eval("Name") %>' />
                                    <div class="panel-heading">
                                        <div class="row" style="text-transform: none !important;">
                                            <div class="col-lg-2 col-md-2">
                                                <asp:Literal runat="server" ID="ltName" Text='<%# ((string)Eval("Name")).ToSentence() %>' />
                                                <a href="#" data-toggle="tooltip" data-placement="right" title="<%# Eval("Description") %>">(?)</a>
                                            </div>
                                            <div class="col-lg-2 col-md-2">
                                                <asp:Literal runat="server" ID="ltMode" Text='<%# Eval("Mode") %>' />
                                            </div>
                                            <div class="col-lg-2 col-md-2">
                                                <asp:Literal runat="server" ID="ltCode" Text='<%# Eval("Code") %>' />
                                            </div>
                                            <div class="col-lg-2  text-right">
                                                <asp:CheckBox runat="server" ID="chkIsEnabled" Checked='<%# Eval("IsEnabled") %>' />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <asp:Repeater runat="server" ID="rptThreshold" DataSource='<%# Eval("Thresholds") %>'>
                                            <ItemTemplate>
                                                <asp:HiddenField runat="server" ID="hfPeriod" Value='<%# Eval("Period") %>' />
                                                <asp:HiddenField runat="server" ID="hfType" Value='<%# Eval("Type") %>' />
                                                <asp:HiddenField runat="server" ID="hfAction" Value='<%# Eval("Action") %>' />
                                                <div class="row">
                                                    <div class="col-lg-2 col-md-2">
                                                        <asp:Literal runat="server" ID="ltCode" Text='<%# Eval("Period") %>' />
                                                    </div>
                                                    <div class="col-lg-2 col-md-2">
                                                        <asp:Literal runat="server" ID="Literal2" Text='<%# Eval("Type") %>' />
                                                    </div>
                                                    <div class="col-lg-2 col-md-2">
                                                        <asp:Literal runat="server" ID="Literal3" Text='<%# Eval("Action") %>' />
                                                    </div>
                                                    <div class="col-lg-2 col-md-2">
                                                        <JQ:Spinner runat="server" ID="spValue" Text='<%# Eval("Value") %>' Style="width: 60px;" />
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                        <EmptyContainer>
                            <div class="alert alert-info"><strong>Info!</strong> No record founds</div>
                        </EmptyContainer>
                    </netpay:DynamicRepeater>
                </ContentTemplate>
            </NP:UpdatePanel>
    </div>
</admin:FormSections>

<admin:ModalDialog runat="server" Title="Simulate Rules" ID="dlgSimulate" OnCommand="Simulate_Command">
    <Header></Header>
    <Body>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                Merchant:
                <div class="form-group">
                    <netpay:MerchantsDropDown runat="server" Visible="<%# !RuleAccountID.HasValue %>" ID="ddlSimulatorMerchant" AutoPostBack="false" EnableBlankSelection="false" />
                </div>
            </div>
            <%-- 
            <div class="col-lg-6 col-md-6">
                Card Number:
                <div class="form-group">
                    <asp:TextBox runat="server" id="txtCardNumber" Text="4580000000000000" />
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                Exp Date:
                <div class="form-group">
                    <JQ:DatePicker runat="server" id="dpExpDate" Value='<%# DateTime.Now.AddYears(2).Date %>' />
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                Amount:
                <div class="form-group">
                    <JQ:Spinner runat="server"  id="spAmount" Text="100" />
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                Currency:
                <div class="form-group">
                    <netpay:CurrencyDropDown runat="server" id="ddlCurrency" SelectedCurrencyIso="USD" />
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                Full Name:
                <div class="form-group">
                    <asp:TextBox runat="server" id="txtFullName" Text="Jhon dow" />
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                Terminal Number:
                <div class="form-group">
                    <asp:TextBox runat="server" Text="0" id="txtTerminal" />
                </div>
            </div>
            --%>
        </div>
        <div runat="server" class="row" style="background-color: #e0e0e0; margin: 10px 0px; padding: 10px;">
            <asp:Repeater runat="server" ID="rptSummary">
                <HeaderTemplate>
                    <div class="row">
                        <div class="col-lg-2 col-md-2">
                            <asp:Literal runat="server" Text='Action Type' />
                        </div>
                        <div class="col-lg-2 col-md-2">
                            <asp:Literal runat="server" Text='Count' />
                        </div>
                    </div>
                </HeaderTemplate>
                <ItemTemplate>
                    <div class="row">
                        <div class="col-lg-2 col-md-2">
                            <asp:Literal runat="server" Text='<%# Eval("ActionType") %>' />
                        </div>
                        <div class="col-lg-2 col-md-2">
                            <asp:Literal runat="server" Text='<%# Eval("Count") %>' />
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>

            <br />
            <br />

            <asp:Repeater runat="server" ID="rptResult">
                <HeaderTemplate>
                    <div class="row">
                        <div class="col-lg-2 col-md-2">
                            <asp:Literal runat="server" Text='Detected' />
                        </div>
                        <div class="col-lg-2 col-md-2">
                            <asp:Literal runat="server" Text='Name' />
                        </div>
                        <div class="col-lg-2 col-md-2">
                            <asp:Literal runat="server" Text='Code' />
                        </div>
                        <div class="col-lg-2 col-md-2">
                            <asp:Literal runat="server" Text='Mode' />
                        </div>
                        <div class="col-lg-2 col-md-2">
                            <asp:Literal runat="server" Text='Message' />
                        </div>
                        <div class="col-lg-2 col-md-2">
                            <asp:Literal runat="server" Text='Action' />
                        </div>
                    </div>
                </HeaderTemplate>
                <ItemTemplate>
                    <div class="row" style='background-color: <%# ((bool)Eval("IsDetected")) ? "#efbdc0" : "#abeab6" %>;'>
                        <div class="col-lg-2 col-md-2">
                            <asp:Literal runat="server" Text='<%# Eval("IsDetected") %>' />
                        </div>
                        <div class="col-lg-2 col-md-2">
                            <asp:Literal runat="server" Text='<%# Eval("Rule.Name") %>' />
                        </div>
                        <div class="col-lg-2 col-md-2">
                            <asp:Literal runat="server" Text='<%# Eval("Rule.Code") %>' />
                        </div>
                        <div class="col-lg-2 col-md-2">
                            <asp:Literal runat="server" Text='<%# Eval("Rule.Mode") %>' />
                        </div>
                        <div class="col-lg-2 col-md-2">
                            <asp:Literal runat="server" Text='<%# Eval("Message") %>' />
                        </div>
                        <div class="col-lg-2 col-md-2">
                            <asp:Literal runat="server" Text='<%# Eval("Action") %>' />
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </Body>
    <Footer>
        <asp:Button runat="server" ID="btnSimulate" CssClass="btn btn-primary" Text="Simulate" CommandName="Run" />
    </Footer>
</admin:ModalDialog>
