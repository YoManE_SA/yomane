﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Fraud.Report.Filter" %>
<admin:FilterSection runat="server" Title="Record Information">
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label3" runat="server" Text="Insert Date" />
                <JQ:DateRange runat="server" ID="rng_InsertDate" />
            </div>
        </div>
    </div>
</admin:FilterSection>
<admin:FilterSection runat="server" Title="General">
    <div class="form-group">
        <div class="row">
            <div class="col-lg-12">
                <asp:Label ID="Label2" runat="server" Text="Merchant" />
                <admin:AccountPicker runat="server" ID="apMerchant" UseTargetID="false" LimitToType="Merchant" />
            </div>
        </div>
    </div>
</admin:FilterSection>
<admin:FilterSection runat="server" Title="Search Value">

    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <asp:Label runat="server" Text="Action" />
                <netpay:EnumDropDown runat="server" ID="ddlAction" CssClass="form-control" ViewEnumName="Netpay.FraudDetection.Action+ActionType" />
            </div>
        </div>
    </div>
</admin:FilterSection>
