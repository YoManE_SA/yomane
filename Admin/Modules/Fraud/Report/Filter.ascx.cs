﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Fraud.Report
{
    public partial class Filter : Controls.TemplateControlBase
    {
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
            base.OnLoad(e);
        }

        private void PageController_Search(object sender, EventArgs e)
        {
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new FraudDetection.FraudDetectionReport.SearchFilters();
            sf.Date = rng_InsertDate.Value;
            sf.AccountId = apMerchant.Value;
            sf.Action = ddlAction.Value.ToNullableEnumByValue<FraudDetection.Action.ActionType>();
            TemplatePage.SetFilter(sf);
        }
    }
}