﻿using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Fraud.Report
{
	public partial class List : Controls.TemplateControlBase
	{
        protected Dictionary<int, string> m_merchantNames;

        protected override void OnLoad(EventArgs e)
		{
			rptList.DataBinding += List_DataBinding;
			base.OnLoad(e);
		}

		private void List_DataBinding(object sender, EventArgs e)
		{
            var list = FraudDetection.FraudDetectionReport.Search(TemplatePage.GetFilter<FraudDetection.FraudDetectionReport.SearchFilters>(), rptList); //rptList
            if (list != null)
                m_merchantNames = Bll.Accounts.Account.GetAccountInfo((list.Where(i => i.AccountId != null).Select(i => (int)i.AccountId).ToList())).ToDictionary(v=>v.Key, v => v.Value.AccountName);
            rptList.DataSource = list;
		}

        protected string GetMerchantName(int? id)
        {
            if (id == null) return null;
            string ret;
            if (!m_merchantNames.TryGetValue((int)id, out ret)) return null;
            return ret;
        }

    }
}
