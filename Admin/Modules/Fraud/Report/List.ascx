﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Fraud.Report.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Fraud Report">
    <Header>
    </Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" ID="rptList" SaveAjaxState="true" AutoGenerateColumns="false" BubbleLoadEvent="false" DisableRowSelect="true">
                <Columns>
                    <%-- Account ID --%>
                    <asp:TemplateField HeaderText="Account ID">
                        <ItemTemplate>
                            <%# GetMerchantName((int?)Eval("AccountId")) %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Action --%>
                    <asp:BoundField HeaderText="Action" DataField="Action" />

                    <%-- Count --%>
                    <asp:BoundField HeaderText="Count" DataField="Count" />
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" />
    </Footer>
</admin:ListSection>
