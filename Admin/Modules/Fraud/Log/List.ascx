﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Fraud.Log.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Fraud Log">
    <Header>
        <admin:LegendColors runat="server" ItemsText='<%# Netpay.FraudDetection.FraudDetectionLog.FraudLogText %>' Items='<%# Netpay.FraudDetection.FraudDetectionLog.FraudLogColor %>'  FloatRight="true" />
    </Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" SaveAjaxState="true" ID="rptList" AutoGenerateColumns="false" BubbleLoadEvent="false" DisableRowSelect="true">
                <Columns>
                    
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <span style="background-color: <%# Netpay.FraudDetection.FraudDetectionLog.FraudLogColor.MapHtmlColor((bool)Eval("IsDetected")) %>;" class="legend-item"></span>&nbsp;
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Account ID --%>
                    <asp:TemplateField HeaderText="Account ID">
                        <ItemTemplate>
                            <asp:Literal runat="server" ID="ltId" Text='<%# Eval("ID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Merchant --%>
                    <asp:TemplateField HeaderText="Merchant">
                        <ItemTemplate>
                            <%# GetMerchantName((int?)Eval("AccountId")) %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Date --%>
                    <asp:TemplateField HeaderText="Date">
                        <ItemTemplate>
                            <%# ((System.DateTime)Eval("Date")).ToString("dd/MM/yyyy HH:mm")%>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Reason --%>
                    <asp:BoundField HeaderText="Reason" DataField="Reason" />

                    <%-- Rule Code --%>
                    <asp:TemplateField HeaderText="Rule Code">
                        <ItemTemplate>
                            <asp:Literal runat="server" Text='<%# Eval("RuleCode") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Rule Mode --%>
                    <asp:BoundField HeaderText="Rule Mode" DataField="RuleMode" />

                    <%-- Rule Name --%>
                    <asp:BoundField HeaderText="Rule Name" DataField="RuleName" />

                    <%-- Action --%>
                    <asp:BoundField HeaderText="Action" DataField="Action" />

                    <%-- Block --%>
                    <asp:TemplateField HeaderText="Block">
                        <ItemTemplate>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" />
    </Footer>
</admin:ListSection>
