﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Fraud.Log
{
    public partial class Filter : Controls.TemplateControlBase
    {
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.Search += PageController_Search;
            if (IsPostBack)
                UpdateFilters();
            base.OnLoad(e);
        }

        private void UpdateFilters()
        {
            var sf = new FraudDetection.FraudDetectionLog.SearchFilters();
            sf.IsDetected = cbIsDetected.Checked ? true : (bool?)null;
            sf.Date = rng_InsertDate.Value;
            sf.AccountId = apMerchant.Value;
            sf.RuleMode = ddlRuleMode.Value.ToNullableEnumByValue<FraudDetection.DetectionRuleBase.DetectionRuleMode>();
            sf.RuleCode = ddRuleCodes.SelectedValue.NullIfEmpty();
            sf.RuleName = ddRuleNames.SelectedValue.NullIfEmpty();
            sf.Action = ddActions.SelectedItem.Text.NullIfEmpty();
            TemplatePage.SetFilter(sf);
        }

        private void PageController_Search(object sender, EventArgs e)
        {
            UpdateFilters();
        }
    }
}