﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Fraud.Log.Filter" %>
<admin:FilterSection runat="server" Title="Record Information">
    <div class="form-group">
        <div class="row">
            <div class="col-lg-12">
                <asp:CheckBox ID="cbIsDetected" Text="Is Detected" runat="server" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label3" runat="server" Text="From" CssClass="fieldText" />
                <JQ:DateRange runat="server" ID="rng_InsertDate" />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-6 col-lg-6">
                <asp:Label ID="Label2" runat="server" Text="Merchant" CssClass="fieldText" />
                <admin:AccountPicker runat="server" ID="apMerchant" UseTargetID="false" LimitToType="Merchant" />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-lg-6">
                <asp:Label runat="server" Text="Rule Mode" />
                <netpay:EnumDropDown CssClass="form-control" runat="server" ID="ddlRuleMode" ViewEnumName="Netpay.FraudDetection.DetectionRuleBase+DetectionRuleMode" />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-lg-6">
                <asp:Label ID="Label4" runat="server" Text="Rule Code" />
                <netpay:DropDownBase CssClass="form-control" ID="ddRuleCodes" DataSource="<%# Netpay.FraudDetection.DetectionRuleBase.RuleCodes %>" EnableBlankSelection="true" runat="server"></netpay:DropDownBase>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-lg-6">
                <asp:Label ID="Label7" runat="server" Text="Rule Name" />
                <netpay:DropDownBase CssClass="form-control" ID="ddRuleNames" DataSource="<%# Netpay.FraudDetection.DetectionRuleBase.RuleNames %>" DataTextField="FriendlyName" DataValueField="Name" EnableBlankSelection="true" runat="server"></netpay:DropDownBase>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-lg-6">
                <asp:Label runat="server" Text="Action" />
                <netpay:EnumDropDown CssClass="form-control" runat="server" ID="ddActions" ViewEnumName="Netpay.FraudDetection.Action+ActionType" />
            </div>
        </div>
    </div>
</admin:FilterSection>
