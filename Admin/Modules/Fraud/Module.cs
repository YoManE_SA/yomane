﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Fraud
{
	public class Module : Admin.CoreBasedModule
	{
		private ApplicationMenu menuRules;
        private ApplicationMenu menuLog;
        private ApplicationMenu menuReport;

        public override string Name { get { return "Fraud"; } }
		public override decimal Version { get { return 1.0m; } }
		public override string Author { get { return "OBL ltd."; } }
		public override string Description { get { return ""; } }

        public Module() : base(FraudDetection.Module.Current) { }

		protected override void OnInit(EventArgs e)
		{
            //Create menu items
            menuRules = new ApplicationMenu("Fraud Rules", "~/Fraud/Rules/", null, 100);
            menuLog = new ApplicationMenu("Fraud Log", "~/Fraud/Log/", null, 110);
            menuReport = new ApplicationMenu("Fraud Report", "~/Fraud/Report/", null, 120);

            //Register routes
            Application.RegisterRoute("Fraud/Rules", "Fraud/Rules", typeof(Controls.Page), module:this);
            Application.RegisterRoute("Fraud/Log", "Fraud/Log", typeof(Controls.DataTablePage) , module : this);
            Application.RegisterRoute("Fraud/Report", "Fraud/Report", typeof(Controls.DataTablePage), module:this);

            //Register event
            Application.InitPage += Application_InitPage;
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
		}

		protected override void OnActivate(EventArgs e)
		{   			
            base.OnActivate(e);
		}

		protected override void OnDeactivate(EventArgs e)
		{            
            base.OnDeactivate(e);
		}

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Risk", menuRules);
            Application.AddMenuItem("Risk", menuLog);
            Application.AddMenuItem("Risk", menuReport);
            base.OnInstallAdmin(e);
        }
        
        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuRules.Parent.RemoveChild(menuRules);
            menuLog.Parent.RemoveChild(menuLog);
            menuReport.Parent.RemoveChild(menuReport);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitPage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.Page;
            if (page.TemplateName == "Fraud/Rules")
            {
                if (IsActive)
                {
                    if (FraudDetection.Module.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Read))
                    {
                        if (!FraudDetection.Module.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Edit))
                        {
                            var control = page.LoadControl("~/Modules/Fraud/Rules/GlobalRules.ascx");
                            Controls.DataTablePage.DisableAllChidControls(control);
                            page.ContentContainer.Controls.Add(control);
                        }
                        else
                        {
                            page.ContentContainer.Controls.Add(page.LoadControl("~/Modules/Fraud/Rules/GlobalRules.ascx"));
                        }
                    }
                    else
                    {
                        page.ContentContainer.Controls.Add(page.ModuleNotAuthorizedControl);
                    }
                }
                else
                {
                    var ctl = page.ModuleNotActivatedControl;
                    (ctl as Modules.ModuleNotActivated.ModuleNotActivated).ModuleName = Name;
                    page.ContentContainer.Controls.Add(ctl);
                }
            }
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
		{
            if (!CoreModule.IsInstalled)
                return;

			var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "Merchant")
            {
                page.AddControlToForm("Fraud", page.LoadControl("~/Modules/Fraud/Rules/FraudRules.ascx"), this, "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject, FraudDetection.Module.SecuredObject).GetActivObject);
            }

            else if (page.TemplateName == "Fraud/Log") {
                page.AddControlToList(page.LoadControl("~/Modules/Fraud/Log/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Fraud/Log/Filter.ascx"));
            }

            else if (page.TemplateName == "Fraud/Report")
            {
                page.AddControlToList(page.LoadControl("~/Modules/Fraud/Report/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Fraud/Report/Filter.ascx"));
            }
        }
    }
}