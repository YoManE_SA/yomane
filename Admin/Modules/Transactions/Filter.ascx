﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Transactions.Filter" %>
<admin:FilterSection runat="server" Title="Status">

    <div class="form-group">
        <asp:Label runat="server" Text="Merchant" />
        <admin:AccountPicker runat="server" ID="apMerchant" UseTargetID="false" LimitToType="Merchant" />
    </div>

    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label runat="server" Text="Transaction insert Date" />
                <JQ:DateRange runat="server" ID="rng_InsertDate" />
            </div>
        </div>
    </div>

    <div class="form-group">
        <asp:Label runat="server" Text="DebitCompany" />
        <netpay:DebitCompanyDropDown CssClass="form-control" runat="server" ID="ddlDebitCompany" />
    </div>

    <div class="form-group">
        <asp:Label runat="server" Text="Terminal" />
        <admin:TerminalPicker ID="tpTerminal" runat="server" />
    </div>

    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label runat="server" Text="Amount" />
                <JQ:DecimalRange runat="server" ID="rng_Amount" />
            </div>
        </div>
    </div>
    <div class="form-group">
        <h4>
            <asp:Label runat="server" Text="Search Fraud" /></h4>
        <ul class="list-group">
            <li class="list-group-item">
                <asp:CheckBox runat="server" ID="chkIsFraud" Text="Is Fraud" /></li>
        </ul>


    </div>

    <div class="form-group">
        <asp:Label runat="server" Text="Currency" />
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
            <netpay:CurrencyDropDown CssClass="form-control" runat="server" ID="ddlCurrency" />
        </div>
    </div>

    <div class="form-group">
        <h4>
            <asp:Label runat="server" Text="Transaction Status" /></h4>
        <asp:CheckBoxList runat="server" ID="chklStatus" CssClass="checkbox-list" RepeatLayout="UnorderedList" OnDataBound="chklStatus_DataBound" DataSource='<%#System.Enum.GetValues(typeof(Netpay.Infrastructure.TransactionStatus))%>' />
    </div>

    <div class="form-group">
        <h4>
            <asp:Label runat="server" ID="chkPaymentStatusLable" Text="Payment Status" /></h4>
        <asp:CheckBoxList runat="server" ID="chkPaymentStatus" CssClass="checkbox-list" RepeatLayout="UnorderedList"
            DataSource='<%#System.Enum.GetValues(typeof(Netpay.Infrastructure.PaymentsStatus))%>' OnDataBound="chkPaymentStatus_DataBound">
        </asp:CheckBoxList><br />


    </div>

    <ul class="list-group">
        <li class="list-group-item">
            <asp:CheckBox Text="Has Unsettled Installments" ID="chkHasUnSettledInstallments" runat="server" /></li>
    </ul>

    <div class="form-group">
        <h4>
            <asp:Label runat="server" ID="BankLable" Text="Bank" /></h4>
        <asp:DropDownList ID="BankDropDownList" CssClass="form-control" runat="server">
            <asp:ListItem Value="0" Text=""></asp:ListItem>
            <asp:ListItem Value="1" Text="Payments overdue"></asp:ListItem>
            <asp:ListItem Value="2" Text="Payments received"></asp:ListItem>
            <asp:ListItem Value="3" Text="Payments not received"></asp:ListItem>
        </asp:DropDownList>
    </div>

    <hr />
    <div class="form-group">
        <h4>
            <asp:Label ID="Label2" runat="server" Text="Credit Type" /></h4>
        <asp:CheckBoxList runat="server" ID="chklCreditType" CssClass="checkbox-list" RepeatLayout="UnorderedList" DataSource='<%# System.Enum.GetValues(typeof(Netpay.Admin.Transactions.Filter.CreditTypeFilter)) %>' />
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <asp:Label runat="server" Text="Reply Code" />
                <asp:TextBox runat="server" ID="txtReplyCode" CssClass="form-control" />
            </div>
        </div>
    </div>
</admin:FilterSection>

<admin:FilterSection runat="server" Title="ChargeBack (For Captured)">
    <div class="form-group">

        <asp:Label runat="server" Text="Filter Setting" />
        <asp:DropDownList CssClass="form-control" ID="DeniedStatusDropDown" runat="server">
            <asp:ListItem Value="" Text=""></asp:ListItem>
            <asp:ListItem Value="0" Text="Exclude Chargebacks"></asp:ListItem>
            <asp:ListItem Value="1" Text="Chargebacks Only"></asp:ListItem>
        </asp:DropDownList>
    </div>

    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label runat="server" Text="Chb Date" />
                <JQ:DateRange runat="server" ID="rng_ChargebackDate" />
            </div>
        </div>
    </div>

    <div class="form-group">
        <asp:Label runat="server" Text="Chb / RR Reason" />

        <netpay:ChargeBackReasonDropDown CssClass="form-control" runat="server" ID="ChargeBackReasonDropDown">
        </netpay:ChargeBackReasonDropDown>

    </div>
</admin:FilterSection>

<admin:FilterSection runat="server" Title="Declined">
    <%--<div class="form-group"> !Not In Use!
        <h4>
            <asp:Label runat="server" Text="Auto Refund" /></h4>
        <asp:DropDownList ID="DeclinedAutoRefundDropDown" runat="server">
            <asp:ListItem Value="0" Text=""></asp:ListItem>
            <asp:ListItem Value="ARF_WAIT" Text="Waiting Refund"></asp:ListItem>
            <asp:ListItem Value="ARF_DONE" Text="Refund Succeeded"></asp:ListItem>
            <asp:ListItem Value="ARF_FAIL" Text="Refund Not Needed"></asp:ListItem>
        </asp:DropDownList>
    </div>--%>

    <div class="form-group">
        Type
        <asp:DropDownList runat="server" CssClass="form-control" ID="DeclinedTypeDropDown">
            <asp:ListItem Value="-1" Text=""></asp:ListItem>
            <asp:ListItem Value="0" Text="Debit"></asp:ListItem>
            <asp:ListItem Value="1" Text="Pre-Auth"></asp:ListItem>
        </asp:DropDownList>
    </div>
</admin:FilterSection>

<admin:FilterSection runat="server" Title="Payment Details">
    <div class="form-group">
        <asp:Label runat="server" Text="Customer" />
        <admin:AccountPicker runat="server" ID="apCustomer" LimitToType="Customer" />
    </div>

    <div class="form-group">
        <asp:Label runat="server" Text="Payer Email" CssClass="fieldText fixed" />
        <asp:TextBox runat="server" ID="txtPayerEmail" CssClass="form-control" />
    </div>

    <div class="form-group">
        <asp:Label runat="server" Text="Payer First Name" CssClass="fieldText fixed" />
        <asp:TextBox runat="server" ID="txtPayerFirstName" CssClass="form-control" />
    </div>

    <div class="form-group">
        <asp:Label runat="server" Text="Payer Last Name" CssClass="fieldText fixed" />
        <asp:TextBox runat="server" ID="txtPayerLastName" CssClass="form-control" />
    </div>

    <div class="form-group">
        <asp:Label runat="server" Text="Type" />
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
            <netpay:PaymentMethodDropDown CssClass="form-control" runat="server" ID="ddlPaymentMethod" />
        </div>
    </div>

    <div class="row">
        <div class="col-lg-8">
            <div class="form-group">
                <asp:Label runat="server" Text="First 6" />
                <asp:TextBox runat="server" ID="txtPaymentFirst6" CssClass="form-control" MaxLength="6" />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <asp:Label runat="server" Text="Last 4" />
                <asp:TextBox runat="server" ID="txtPaymentLast4" CssClass="form-control" MaxLength="4" />
            </div>
        </div>
    </div>

    <div class="form-group">
        <asp:Label runat="server" Text="Country" />

        <netpay:CountryDropDown CssClass="form-control" runat="server" ID="PaymentDetailsCountryDropDown">
        </netpay:CountryDropDown>

    </div>
</admin:FilterSection>

<%-- Rolling reserve search filters --%>
<admin:FilterSection runat="server" Title="Rolling Reserve">
    <div class="alert alert-info">
        <span>In order to filter Rolling Reserve transactions</span><br />
        <span>Please check the 'Show Only Rolling Reserve' checkbox.</span><br />
    </div>

    <div class="row">
        <div class="col-md-12">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox Text="Show Only Rolling Reserve" ID="chbShowOnlyRollingReserve" runat="server" /></li>
            </ul>
        </div>
    </div>

    <%-- Create Date filter --%>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label runat="server" Text="Release Date" />
                <JQ:DateRange runat="server" ID="rngReleaseDate" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h4>TYPE</h4>
        </div>
        <asp:UpdatePanel ID="upiNotReleaseCheckBox" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="col-md-6">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <asp:CheckBox ID="chbiCreditType0" runat="server" Text="Reserve Hold - " OnCheckedChanged="iCreditType0_CheckedChanged" AutoPostBack="true" /></li>
                        <li class="list-group-item">
                            <ul class="child-list-group">
                                <li>
                                    <asp:CheckBox ID="chbiNotRelease" runat="server" Text="Only overdue holdings" ClientIDMode="Static" Enabled="false" /></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <asp:CheckBox ID="chbiCreditType1" runat="server" Text="Release" /></li>
                    </ul>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</admin:FilterSection>


<admin:FilterSection runat="server" Title="Transaction Search">
    <%--<admin:MultiSearch runat="server" ID="msQuickSearch" PlaceHolder="Search..." TypePlaceHolder="Action">
        <asp:ListItem Text="Trans ID" />
        <asp:ListItem Text="Refernce Code" />
        <asp:ListItem Text="Refernce Number" />
        <asp:ListItem Text="Approval Number" />
    </admin:MultiSearch>--%>
    <asp:TextBox ID="SpecificTransID" Visible="false" runat="server"></asp:TextBox>

    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label runat="server" Text="Trans ID" />
                <JQ:IntRange runat="server" ID="rng_ID" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <asp:Label runat="server" Text="Reference Code" />
                <asp:TextBox ID="txtDebitReferenceCode" runat="server" CssClass="form-control" />
            </div>
        </div>
        <div class="col-lg-12">
            <div class="form-group">
                <asp:Label runat="server" Text="Reference Number" />
                <asp:TextBox ID="txtDebitReferenceNum" runat="server" CssClass="form-control" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <asp:Label runat="server" Text="Approval Number" />
                <asp:TextBox ID="txtApprovalNumber" runat="server" CssClass="form-control" />
            </div>
        </div>
    </div>
</admin:FilterSection>

