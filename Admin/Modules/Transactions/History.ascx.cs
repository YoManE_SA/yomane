﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Transactions
{
    public partial class History : Controls.TemplateControlBase
    {
        public bool IsHistoryListExist { get; set; }

        protected override void DataBindChildren()
        {
            var ItemData = TemplatePage.GetItemData<Bll.Transactions.Transaction>();
            if (ItemData != null)
            {
                var HistoryList = Bll.Transactions.History.GetTransactionHistory(ItemData.ID, ItemData.Status);
                if (HistoryList.Count() > 0)
                {
                    IsHistoryListExist = true;
                    rptList.DataSource = Bll.Transactions.History.GetTransactionHistory(ItemData.ID, ItemData.Status);
                }
                else
                {
                    IsHistoryListExist = false;
                }
            }

            base.DataBindChildren();
        }
			
    }
}
