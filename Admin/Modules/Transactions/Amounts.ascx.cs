﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Transactions
{
	public partial class Amounts : Controls.TemplateControlBase
	{
		protected override void DataBindChildren()
		{
			var ItemData = TemplatePage.GetItemData<Bll.Transactions.Transaction>();
			if (ItemData != null) rptList.DataSource = Bll.Finance.TransactionAmount.LoadForTransaction(ItemData.ID, ItemData.Status); 
			base.DataBindChildren();
		}
	}
}