<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Summary.ascx.cs" Inherits="Netpay.Admin.Transactions.Summary" %>
<script type="text/javascript">
    function checkTextAreaMaxLength(textBox, e, length) {

        var mLen = textBox["MaxLength"];
        if (null == mLen)
            mLen = length;

        var maxLength = parseInt(mLen);
        if (!checkSpecialKeys(e)) {
            if (textBox.value.length > maxLength - 1) {
                if (window.event)//IE
                    e.returnValue = false;
                else//Firefox
                    e.preventDefault();
            }
        }
    }

    function checkSpecialKeys(e) {
        if (e.keyCode != 8 && e.keyCode != 46 && e.keyCode != 37 && e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40)
            return false;
        else
            return true;
    }

    function OpenChargeAttemptLog(applicationPath, transactionId, transStatusId) {
        window.open(applicationPath + "/Logs/ChargeAttemptsLog?ctl00.TransId=" + transactionId + "&Search=1&Page=0&NoLayout=1&TransactionStatus=" + transStatusId,
        "Terminal", "height=470, width=1240,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,left=150,top=150");
    }

</script>
<div style="clear: both;"></div>
<asp:PlaceHolder runat="server" ID="phActions">
    <admin:FormSections runat="server" Title="General Actions">
        <asp:UpdatePanel runat="server" ID="upCommandStatus" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Label runat="server" ID="ltNotification" CssClass="Notification" EnableViewState="false" Visible="false" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            <%-- Send Merchant Email Button --%>
                            <asp:Button CssClass="btn btn-primary" Width="200" runat="server" ID="lbSendMerchantEmail" Text="Email Merchant" CommandName="EmailMerchant" OnCommand="DoActions" CommandArgument='<%#ItemData.ID+"~"+ItemData.Status%>' /><br />
                        </div>

                        <div class="form-group">
                            <%-- Send Client Email Button --%>
                            <asp:Button Visible="<%#ItemData.Status!=TransactionStatus.Declined%>" CssClass="btn btn-primary" Width="200" runat="server" ID="lbSendClientEmail" Text="Email Client" CommandName="EmailClient" OnCommand="DoActions" CommandArgument='<%#ItemData.ID+"~"+ItemData.Status%>' /><br />
                        </div>

                        <div class="form-group">
                            <%-- Charge Attempts Log Link --%>
                            <asp:HyperLink runat="server" ID="lbChargeAttemptLink" CssClass="btn btn-primary" Width="200" Style="cursor: pointer;"
                                Text='Log Charge Attempt' onclick='<%# "OpenChargeAttemptLog("+"\""+WebUtils.ApplicationPath+"\""+","+"\""+ItemData.ID+"\""+","+"\""+(int)ItemData.Status+"\""+"); return false;" %>'
                                Target="_blank" />
                        </div>

                        <div class="form-group">
                            <%-- Set Fraud Button --%>
                            <asp:LinkButton runat="server" CssClass="btn btn-primary" Width="200" ID="hlSetFraud" Text='<%# ItemData.IsFraud ? "Cancel Fraud" : "Set Fraud" %>' CausesValidation="false" CommandName="Fraud" OnCommand="DoActions" CommandArgument='<%#ItemData.ID+"~"+ItemData.Status%>' />
                        </div>
                        <%--<asp:LinkButton runat="server" ID="lbCreateInvoice" Text="Create Invoice" CommandName="CreateInvoice" /><br />--%>
                        <%--<a href="#" id="" onclick="<%# Netpay.Web.Controls.PopupContainer.Popup(this, "Refund Request", 500, "RefundRequest.ascx", string.Format("ID={0}&Status={1}", ItemData.ID, ItemData.Status)) %>">Ask Refund</a><br />--%><%--<a href="#" id="" onclick="<%# Netpay.Web.Controls.PopupContainer.Popup(this, "Refund Request", 500, "RefundRequest.ascx", string.Format("ID={0}&Status={1}", ItemData.ID, ItemData.Status)) %>">Ask Refund</a><br />--%>
                    </div>
                </div>
            </div>
        </div>
    </admin:FormSections>
</asp:PlaceHolder>

<%-- TRANSACTION INFO --%>
<asp:PlaceHolder runat="server" ID="phTransaction">
    <admin:FormSections runat="server" Title="Transaction Info">
        <div class="row">
            <div class="col-lg-12">
                <ul class="list-group">

                    <%-- Authorization Code --%>
                    <asp:PlaceHolder runat="server" Visible='<%#ItemData.StatusText.EmptyIfNull()=="Captured" && !string.IsNullOrEmpty(ItemData.ApprovalNumber.EmptyIfNull()) && ItemData.ApprovalNumber.EmptyIfNull().Trim()!="0000000" %>'>
                        <li class="list-group-item">
                            <asp:Label runat="server" Font-Bold="true" Text="Authorization Code: " />
                            <%# ItemData.ApprovalNumber %><br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- Comment --%>
                    <asp:PlaceHolder runat="server" Visible="<%#!string.IsNullOrEmpty(ItemData.Comment.EmptyIfNull().Trim())%>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Payer Comment: " />
                            <%# ItemData.Comment.EmptyIfNull().Trim().Length<=20? ItemData.Comment.EmptyIfNull() : ItemData.Comment.EmptyIfNull().Substring(0,20)+".." %><br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- Note(SystemText field) --%>
                    <asp:PlaceHolder runat="server" Visible='<%# !string.IsNullOrEmpty(ItemData.SystemText.EmptyIfNull().Trim()) %>'>
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Note: " />
                            <%#  ItemData.SystemText.EmptyIfNull() %><br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- Ip Address --%>
                    <asp:PlaceHolder runat="server" Visible="<%#!string.IsNullOrEmpty(ItemData.IP) %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="IP Address: " />
                            <%# ItemData.IP.EmptyIfNull() %><br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- Payer Id (not for pre-authorized) --%>
                    <asp:PlaceHolder runat="server" Visible="<%#ItemData.Status!=TransactionStatus.Authorized && !string.IsNullOrEmpty(ItemData.PayerIDUsed.EmptyIfNull()) %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Payer ID: " />
                            <%# ItemData.PayerIDUsed.EmptyIfNull() %><br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- Debit Reference Code --%>
                    <asp:PlaceHolder runat="server" Visible="<%#!string.IsNullOrEmpty(ItemData.DebitReferenceCode.EmptyIfNull()) %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Debit Reference Code: " />
                            <%# ItemData.DebitReferenceCode.EmptyIfNull() %><br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- Debit Reference Number --%>
                    <asp:PlaceHolder runat="server" Visible="<%#!string.IsNullOrEmpty(ItemData.DebitReferenceNum.EmptyIfNull()) %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Debit Reference Num: " />
                            <%# ItemData.DebitReferenceNum.EmptyIfNull() %><br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- Acquirer Reference Num --%>
                    <asp:PlaceHolder runat="server" Visible='<%#ItemData.StatusText.EmptyIfNull()=="Captured"&&(!string.IsNullOrEmpty(ItemData.AcquirerReferenceNum.EmptyIfNull()))%>'>
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Acquirer Reference Num: " />
                            <%# ItemData.AcquirerReferenceNum.EmptyIfNull() %><br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- Order Number --%>
                    <asp:PlaceHolder runat="server" Visible="<%#!string.IsNullOrEmpty(ItemData.OrderNumber.EmptyIfNull()) %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Order Number: " />
                            <%# ItemData.OrderNumber.EmptyIfNull() %><br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- Debit Company Name --%>
                    <asp:PlaceHolder runat="server" Visible="<%# ItemData.DebitCompany!=null && !string.IsNullOrEmpty(ItemData.DebitCompany.Name) %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Debit Company: " />
                            <%#ItemData.DebitCompany!=null? ItemData.DebitCompany.Name.EmptyIfNull() : "" %><br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- Terminal Number - This should be link to terminals and show terminal name --%>
                    <asp:PlaceHolder runat="server" Visible="<%#!string.IsNullOrEmpty(ItemData.TerminalNumber.EmptyIfNull()) %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Terminal: " />
                            <asp:HyperLink runat="server"
                                Font-Underline="true"
                                Style="cursor: pointer;"
                                Text='<%# !string.IsNullOrEmpty(ItemData.TerminalNumber.EmptyIfNull()) && Netpay.Bll.DebitCompanies.Terminal.Search(new Netpay.Bll.DebitCompanies.Terminal.SearchFilters{Number = ItemData.TerminalNumber},null).FirstOrDefault()!=null? 
                                          Netpay.Bll.DebitCompanies.Terminal.Search(new Netpay.Bll.DebitCompanies.Terminal.SearchFilters{Number = ItemData.TerminalNumber},null).FirstOrDefault().Name : "Terminal not found" %>'
                                onclick='<%# "OpenTerminalPopUp("+"\""+WebUtils.ApplicationPath+"\""+","+"\""+ItemData.TerminalNumber+"\""+"); return false;" %>'
                                Target="_blank" /><br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- PD date - in NewAdmin there is no PD field --%>

                    <%-- Costs --%>
                    <asp:PlaceHolder runat="server" Visible='<%#ItemData.StatusText=="Captured" %>'>
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Costs: " />
                            <%# ItemData.DebitFee.ToAmountFormat(ItemData.CurrencyIsoCode.EmptyIfNull()) %>
                            <br />
                        </li>
                    </asp:PlaceHolder>

                    <%--Chb Costs --%>
                    <asp:PlaceHolder runat="server" Visible='<%#ItemData.StatusText.EmptyIfNull()=="Captured"&&ItemData.DebitFeeChb!=0%>'>
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Chb Costs: " />
                            <%# ItemData.DebitFeeChb.ToAmountFormat(ItemData.CurrencyIsoCode.EmptyIfNull()) %>
                            <br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- Min Pay date  --%>
                    <asp:PlaceHolder runat="server" Visible="<%# ItemData.MerchantPayDate!=null %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Min Pay Date:" />
                            <%# ItemData.MerchantPayDate!=null? ItemData.MerchantPayDate.Value.ToShortDateString():"" %>
                            <br />
                        </li>
                    </asp:PlaceHolder>
                </ul>
            </div>
        </div>
    </admin:FormSections>
</asp:PlaceHolder>

<%-- Pending Transactions Actions --%>
<asp:PlaceHolder runat="server" ID="phPendingActions" Visible="false">
    <admin:FormSections runat="server" Title="Pending Transaction Actions">
        <div class="row">
            <div class="col-md-2 col-lg-2">
                <asp:Label runat="server" Text="Move To Pass"></asp:Label>
            </div>
            <div class="col-md-2 col-lg-2">
                <asp:Button OnClientClick="return confirm('Are you sure?');" CssClass="btn btn-primary" runat="server" Text="Go" OnCommand="General_Command" CommandName="Pass" CommandArgument="<%#ItemData.ID %>" /><br />
            </div>
        </div>

        <div class="row">
            <div class="col-lg-2 col-md-2">
                <asp:Label runat="server" Text="Move To Fail"></asp:Label>
            </div>
            <div class="col-lg-2 col-md-2">
                <asp:Button OnClientClick="return confirm('Are you sure?');" CssClass="btn btn-primary" runat="server" Text="Go" OnCommand="General_Command" CommandName="Fail" CommandArgument="<%#ItemData.ID %>" /><br />
            </div>
        </div>

        <div class="row">
            <div class="col-lg-2 col-md-">
                <asp:Label runat="server" Text="Return"></asp:Label>
            </div>
            <div class="col-lg-2 col-md-2">
                <asp:Button OnClientClick="return confirm('Are you sure?');" runat="server" CssClass="btn btn-primary" Text="Go" OnCommand="General_Command" CommandName="Return" CommandArgument="<%#ItemData.ID %>" /><br />
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 col-lg-8">
                <div class="form-group">
                    Note
                    <asp:TextBox onkeyDown="checkTextAreaMaxLength(this,event,'100');" Rows="2" TextMode="MultiLine" ToolTip="Note will be inserted into system text" CssClass="form-control" runat="server" ID="txtSystemText" MaxLength="100"></asp:TextBox>
                </div>
            </div>
        </div>
    </admin:FormSections>
</asp:PlaceHolder>

<%-- Pre-Authorized Transactions 'Confirmation' and 'Charge' --%>
<asp:PlaceHolder runat="server" ID="phPreAuthTransactions" Visible="false">
    <admin:FormSections runat="server" Title="Confirmation & Charge">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <asp:Label runat="server" Text='<%#"Confirmation: "+ItemData.ApprovalNumber.EmptyIfNull() %>' />
                </div>
                <div class="form-group">
                    Charge:
                   
                    <asp:Label runat="server" Text='<%#ItemData.PassedTransactionID!=null? ItemData.PassedTransactionID.ToString() : "---" %>' />
                </div>
            </div>
        </div>
    </admin:FormSections>
</asp:PlaceHolder>

<%-- "Transaction was refunded Section" --%>

<asp:PlaceHolder runat="server" ID="phGlobal" Visible="false">
    <asp:UpdatePanel runat="server" ID="upTransactionInfo">
        <ContentTemplate>
            <admin:FormSections runat="server" Title="Transaction Info">
                <asp:MultiView runat="server" ID="RefundMultiView">
                    <asp:View runat="server" ID="View0">
                        <asp:Label runat="server" Text="Transaction was refunded - ID:"></asp:Label>
                        <asp:Repeater runat="server" ID="RefundRepeater">
                            <ItemTemplate>
                                <asp:HyperLink
                                    Text='<%#(Container.ItemIndex==RefundRepeater.Items.Count)? Container.DataItem.ToString() : Container.DataItem.ToString()+" ," %>'
                                    NavigateUrl='<%# WebUtils.ApplicationPath+"/Transactions/0?ctl00.rng_ID.From="+Container.DataItem.ToString()+"&ctl00.rng_ID.To="+Container.DataItem.ToString()+"&search=1"%>'
                                    runat="server"
                                    Target="_blank">
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:Repeater>
                    </asp:View>
                    <asp:View runat="server" ID="View1">
                        <asp:Label runat="server" Text="This is a refund of transaction - ID:"></asp:Label>
                        <asp:HyperLink
                            Text="<%#ItemData.OriginalTransactionID%>"
                            NavigateUrl='<%#WebUtils.ApplicationPath+"/Transactions/0?ctl00.rng_ID.From="+ItemData.OriginalTransactionID+"&ctl00.rng_ID.To="+ItemData.OriginalTransactionID+"&ctl00.rng_InsertDate.To="+Server.UrlEncode(ItemData.InsertDate.ToString())+"&search=1" %>'
                            runat="server"
                            Target="_blank">
                        </asp:HyperLink>
                    </asp:View>
                </asp:MultiView><br />

                <asp:MultiView runat="server" ID="phTransactionDeniedStatus">
                    <asp:View runat="server" ID="DeniedView0">
                        Denied status:
               
                       

                        <asp:Label runat="server" Text="<%#Netpay.Bll.Transactions.DeniedStatusDes.GetDecsctiptionByStatusNumber(ItemData.DeniedStatusID)%>"></asp:Label><br />
                    </asp:View>
                    <asp:View runat="server" ID="DeniedView1">
                        <asp:Label runat="server" Text="Denied Status:" Font-Bold="true"></asp:Label>
                        <asp:Label runat="server" Text="<%#Netpay.Bll.Transactions.DeniedStatusDes.GetDecsctiptionByStatusNumber(ItemData.DeniedStatusID) %>"></asp:Label>
                        <asp:Literal runat="server" Text='<%#ItemData.DeniedSendDate<ItemData.ChargebackDate? " - mail not sent":"" %>' />
                        <asp:Literal runat="server" Text='<%#string.IsNullOrEmpty(ItemData.DeniedAdminComment)? " - comment not sent":"" %>' /><br />
                    </asp:View>
                    <asp:View runat="server" ID="DeniedView2">
                        Denied status:
               
                       

                        <asp:Label runat="server" Text="<%#Netpay.Bll.Transactions.DeniedStatusDes.GetDecsctiptionByStatusNumber(ItemData.DeniedStatusID) %>"></asp:Label><br />
                    </asp:View>
                    <asp:View runat="server" ID="DeniedView3">
                        Denied status:
               
                       

                        <asp:Label runat="server" Text="<%#Netpay.Bll.Transactions.DeniedStatusDes.GetDecsctiptionByStatusNumber(ItemData.DeniedStatusID) %>"></asp:Label><br />
                        Trans Duplicated:
               
                       

                        <asp:Repeater runat="server" ID="DuplicatedRepeater">
                            <ItemTemplate>
                                <asp:HyperLink
                                    Text='<%# (Container.ItemIndex==DuplicatedRepeater.Items.Count)? Container.DataItem.ToString() : Container.DataItem.ToString()+" ," %>'
                                    runat="server"
                                    NavigateUrl='<%#WebUtils.ApplicationPath+"/Transactions/0?ctl00.rng_ID.From="+Container.DataItem.ToString()+"&ctl00.rng_ID.To="+Container.DataItem.ToString()+"&search=1" %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:Repeater>
                    </asp:View>
                    <asp:View runat="server" ID="DeniedView4">
                    </asp:View>
                </asp:MultiView>
                <asp:PlaceHolder runat="server" ID="phOriginalTransactionLink" Visible="false">
                    <asp:Label runat="server" Font-Bold="true" Text="Original Transaction - ID:"></asp:Label>
                    <asp:HyperLink
                        Text="<%#ItemData.OriginalTransactionID%>"
                        NavigateUrl='<%#WebUtils.ApplicationPath+"/Transactions/0?ctl00.rng_ID.From="+ItemData.OriginalTransactionID+"&ctl00.rng_ID.To="+ItemData.OriginalTransactionID+"&search=1" %>'
                        runat="server"
                        Target="_blank">
                    </asp:HyperLink>
                </asp:PlaceHolder>
            </admin:FormSections>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:PlaceHolder>

<%-- Rolling Reserve Transaction --%>
<asp:PlaceHolder runat="server" ID="phRollingReserveAdmin" Visible="false">
    <admin:FormSections runat="server" Title='<%# ItemData != null && ItemData.PaymentMethodID == Netpay.CommonTypes.PaymentMethodEnum.RollingReserve ? "Rolling Reserve" : "Admin Transaction"%>'>
        <asp:Label Font-Bold="true" runat="server" Text="Comment" />
        <div class="input-group">
            <asp:TextBox runat="server" ID="txtAdminComment" CssClass="form-control" Rows="5" TextMode="MultiLine" />
            <div class="input-group-addon">
                <%-- Save Button - Always show --%>
                <asp:Button CssClass="btn btn-primary" ID="btnSave" runat="server" Text="Save" OnCommand="SaveComment" />
            </div>
        </div>
        <br />
        <NP:UpdatePanel runat="server" ID="upRollingReserve" ChildrenAsTriggers="true" RenderMode="Block" UpdateMode="Conditional" style="float: right;">
            <ContentTemplate>
                <asp:Button runat="server" CssClass="btn btn-primary" ID="btnReleaseReserve" Text="Release Reserve" OnCommand="RollingReserve_Command" CommandArgument="Release" />
                <%-- Text - Visibility depends on conditions --%>
                <asp:Literal ID="lblReserevReleased" runat="server" Text="This reserve already released." />
            </ContentTemplate>
        </NP:UpdatePanel>
    </admin:FormSections>
</asp:PlaceHolder>

