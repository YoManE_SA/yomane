﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountTransactions.ascx.cs" Inherits="Netpay.Admin.Transactions.AccountTransactions" %>
<%-- (Account != null && Account.AccountID != 0) --%>
<admin:FormPanel runat="server" ID="fpUnsettledStatus" Title="Admin Transactions" OnCommand="Unsettled_Command" EnableAdd='<%#IsVisible%>'>
    <script type="text/javascript">
        function ValidateUnsettledPage() {

            if (typeof (Page_ClientValidate) == 'function') {
                var check = Page_ClientValidate("Transactions_AccountTransactions");
            }

            if (check) {
                var button = document.getElementById("UnsettledSaveButton");
                button.removeAttribute("disabled");
                button.style.opacity = "1.0";
                button.style.cursor = "pointer";
            }

            else {
                var button = document.getElementById("UnsettledSaveButton");
                button.disabled = true;
                button.style.opacity = "0.2";
                Page_BlockSubmit = false;
            }
        }
       </script>
   
            <asp:MultiView ID="AccountTransactionMultiView" runat="server" ActiveViewIndex='<%# IsVisible ? 0 : 1%>'>
                <asp:View ID="ViewIfVisible" runat="server">
            <NP:PagedRepeater runat="server" ID="rptData">
                <HeaderTemplate>
                     <table class="table table-hover table-customize">
                </HeaderTemplate>
                <ItemTemplate>
                    <tbody>
                        <td><%# Eval("Key") %> (Total)</td>
                        <td><%# Eval("Value", "{0:00}") %></td>
                        <td><asp:HyperLink runat="server" Text="Transactions" NavigateUrl='<%# WebUtils.ApplicationPath + string.Format("/Transactions/0?ctl00.apMerchant.hdID={0}&ctl00.chkHasUnSettledInstallments=1&ctl00.ddlCurrency={1}&NoDateRange=1&Search=1", (TemplatePage as AccountPage).Account.MerchantID, Currency.Get(((KeyValuePair<string,decimal>)Container.DataItem).Key).ID) %>' /></td>
                    </tbody>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
                <EmptyContainer>
                    <div class="alert alert-info"><strong>Info!</strong> No record information</div>
                </EmptyContainer>
                
            </NP:PagedRepeater>
                    </asp:View>

                        <asp:View ID="ViewIfNotVisible" runat="server">
                            <div class="alert alert-info">
                    You need to save your details in order to edit this section.
                                </div>
                </asp:View>
                </asp:MultiView>
</admin:FormPanel>

<admin:ModalDialog runat="server" ID="mdAddTransaction" Title="Create Admin Transaction" OnCommand="Unsettled_Command">
    <Body>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    Amount:
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtAmount" CausesValidation="True" onkeyup="ValidateUnsettledPage();" />
                    <asp:RequiredFieldValidator ForeColor="Red" ControlToValidate="txtAmount" ID="RequiredFieldValidatortxtAmount" runat="server" ErrorMessage="Insert amount bigger than 0." Display="Dynamic" ValidationGroup="Transactions_AccountTransactions" />
                    <%--<asp:RangeValidator ForeColor="Red" ControlToValidate="txtAmount" ID="RangeValidatortxtAmount" runat="server" ErrorMessage="Insert amount bigger than 0." MinimumValue="0.01" MaximumValue="<%#Int32.MaxValue %>" Type="Double" Display="Dynamic" ValidationGroup="Transactions_AccountTransactions" />--%>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    Choose Currency type
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-money"></i></span>
                        <netpay:CurrencyDropDown runat="server" CssClass="form-control" ID="ddlCurrency" BlankSelectionText="Choose Currency type" BlankSelectionValue="-1" onchange="ValidateUnsettledPage();" CausesValidation="True" />
                    </div>
                    <asp:RequiredFieldValidator ForeColor="red" ControlToValidate="ddlCurrency" ID="RequiredFieldValidatorddlCurrency" runat="server" ErrorMessage="Choose currency type." InitialValue="-1" Display="Dynamic" ValidationGroup="Transactions_AccountTransactions" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    Pay-For text:
                    <asp:TextBox runat="server" ID="txtText" CssClass="form-control" MaxLength="500" CausesValidation="True" ValidationGroup="Transactions_AccountTransactions" onkeyup="ValidateUnsettledPage();" />
                    <asp:RequiredFieldValidator ForeColor="red" ControlToValidate="txtText" ID="RequiredFieldValidatortxtText" runat="server" ErrorMessage="Insert a Pay-For text with minimum 3 characters." Display="Dynamic" ValidationGroup="Transactions_AccountTransactions" />
                    <asp:RegularExpressionValidator ValidationExpression="^(?:.{3,}|)$" ControlToValidate="txtText" ForeColor="red" ID="RegularExpressionValidatortxtText" runat="server" ErrorMessage="Insert a Pay-For text with minimum 3 characters." ValidationGroup="Transactions_AccountTransactions" Display="Dynamic" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <h4>Transaction Mode:</h4>
                <asp:RadioButtonList ID="TransactionModeList" runat="server" RepeatLayout="Table" RepeatDirection="Vertical" ClientIDMode="Static" EnableViewState="true">
                    <asp:ListItem Text="Debit" Selected="True" Value="0" />
                    <asp:ListItem Text="Credit" />
                </asp:RadioButtonList>

            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <h4>Transaction Type:</h4>
                    <asp:RadioButtonList ID="TransactionTypeList" OnSelectedIndexChanged="TransactionTypeSelectedIndexChanged" runat="server" RepeatLayout="Table" RepeatDirection="Vertical" AutoPostBack="true" EnableViewState="true">
                        <asp:ListItem Text="Admin" Selected="True" />
                        <asp:ListItem Text="Fees" />
                    </asp:RadioButtonList>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <hr />
                <div class="form-group">
                    Comment:
                    <asp:TextBox runat="server" ID="txtComment" CssClass="form-control" TextMode="MultiLine" Rows="3" CausesValidation="True" onkeyup="ValidateUnsettledPage();" MaxLength="500" />
                    <asp:RequiredFieldValidator
                        ForeColor="red"
                        ControlToValidate="txtComment"
                        ID="RequiredFieldValidator1"
                        runat="server"
                        ErrorMessage="Insert a comment with minimum 6 characters."
                        Display="Dynamic"
                        ValidationGroup="Transactions_AccountTransactions">
                    </asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator
                        ValidationExpression="^(?:.{6,}|)$"
                        ControlToValidate="txtComment"
                        ForeColor="red"
                        ID="RegularExpressionValidatortxtComment"
                        runat="server"
                        ErrorMessage="Insert a comment with minimum 6 characters."
                        ValidationGroup="Transactions_AccountTransactions"
                        Display="Dynamic">
                    </asp:RegularExpressionValidator>
                </div>
            </div>
        </div>
    </Body>
    <Footer>
        <asp:Button ID="UnsettledSaveButton" runat="server" Text="Save" CommandName="SaveItem" CssClass="btn btn-cons-short btn-info margin-left-5 + CssFloatRight" ClientIDMode="Static" />
    </Footer>
</admin:ModalDialog>
