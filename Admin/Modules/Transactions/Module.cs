﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Transactions
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuTransaction, quickSearchApproved, quickSearchDecline, quickSearchAuthorized, quickSearchPending;
        public override string Name { get { return "Transactions"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return "Transactions management"; } }


        public Module() : base(Bll.Transactions.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu items
            menuTransaction = new ApplicationMenu("Transactions Management", "~/Transactions/0", "fa-credit-card", 1);
            quickSearchApproved = new ApplicationMenu("Trans Approved#", "~/Transactions/0?Search=1&SelectedIndex=0&ctl00.chklStatus$0=Captured&ctl00.SpecificTransID=", "fa-credit-card");
            quickSearchDecline = new ApplicationMenu("Trans Decline#", "~/Transactions/0?Search=1&SelectedIndex=0&ctl00.chklStatus$1=Declined&ctl00.SpecificTransID=", "fa-credit-card");
            quickSearchAuthorized = new ApplicationMenu("Trans Pre-Auth#", "~/Transactions/0?Search=1&SelectedIndex=0&ctl00.chklStatus$2=Authorized&ctl00.SpecificTransID=", "fa-credit-card");
            quickSearchPending = new ApplicationMenu("Trans Pending#", "~/Transactions/0?Search=1&SelectedIndex=0&ctl00.chklStatus$3=Pending&ctl00.SpecificTransID=", "fa-credit-card");

            //Register routes
            Application.RegisterRoute("Transactions/PrintDoc/{*id}", "PrintTransaction", typeof(PrintDoc), module: this);
            Application.RegisterRoute("Transactions/{*id}", "Transaction", typeof(Controls.DataTablePage), module: this);

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Transactions", menuTransaction);
            Application.QuickSearch.AddChild(quickSearchApproved);
            Application.QuickSearch.AddChild(quickSearchDecline);
            Application.QuickSearch.AddChild(quickSearchAuthorized);
            Application.QuickSearch.AddChild(quickSearchPending);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuTransaction.Parent.RemoveChild(menuTransaction);
            Application.QuickSearch.RemoveChild(quickSearchApproved);
            Application.QuickSearch.RemoveChild(quickSearchDecline);
            Application.QuickSearch.RemoveChild(quickSearchAuthorized);
            Application.QuickSearch.RemoveChild(quickSearchPending);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "Transaction")
            {
                if (!page.IsPostBack) page.Load += Page_Load;
                page.PageController.LoadItem += PageController_LoadItem;
                page.AddControlToList(page.LoadControl("~/Modules/Transactions/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Transactions/Filter.ascx"));
                page.AddControlToForm("Summary", page.LoadControl("~/Modules/Transactions/Summary.ascx"), this, "", Bll.Transactions.Transaction.SecuredObject);
                page.AddControlToForm("Data", page.LoadControl("~/Modules/Transactions/Details.ascx"), this, "", Bll.Transactions.Transaction.SecuredObject);
                page.AddControlToForm("History", page.LoadControl("~/Modules/Transactions/History.ascx"), this, "", Bll.Transactions.Transaction.SecuredObject);
                //page.AddControlToForm("Amounts", page.LoadControl("~/Modules/Transactions/Amounts.ascx"));
                page.AddControlToForm("Chargeback", page.LoadControl("~/Modules/Transactions/Chargeback.ascx"), this, "", Bll.Transactions.Transaction.SecuredObject);
                page.AddControlToForm("Block Items", page.LoadControl("~/Modules/Transactions/BlockItems.ascx"), this, "", new Controls.SecuredObjectSelector(Bll.Transactions.Transaction.SecuredObject, Bll.Risk.RiskItem.SecuredObject).GetActivObject);
                page.AddControlToForm("Installments", page.LoadControl("~/Modules/Transactions/Installments.ascx"), this, "", Bll.Transactions.Transaction.SecuredObject);

            }
            else if (Accounts.Module.TemplateIsAccountType(page.TemplateName) &&
                     page.TemplateName != "DebitCompany" && page.TemplateName != "Customer" &&
                     (page.ViewName == null))
            {
                var acTransaction = page.LoadControl("~/Modules/Transactions/AccountTransactions.ascx");
                acTransaction.DataBinding += acTransaction_DataBinding;
                page.AddControlToForm("Summary", acTransaction, this, "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject, Bll.Transactions.Transaction.SecuredObject).GetActivObject);
            }
            else if (Accounts.Module.TemplateIsAccountType(page.TemplateName) && (page.ViewName == "Transactions"))
            {
                if (IsActive)
                {
                    var lst = page.LoadControl("~/Modules/Transactions/List.ascx") as Transactions.List;
                    lst.ID = "TransactionList";
                    page.PageController.FormView.ContentTemplateContainer.Controls.Add(lst);
                    page.FormPanel.Visible = false;
                    (page as Controls.AccountPage).AccountChanged += delegate
                    {
                        lst = page.PageController.FormView.ContentTemplateContainer.FindControl("TransactionList") as Transactions.List;
                        lst.Title = page.FormPanel.Title + " :Transactions";
                        var filters = new Bll.Transactions.Transaction.SearchFilters();
                        var account = (page as Controls.AccountPage).Account;
                        if (account == null) return;
                        if (account.MerchantID.HasValue) filters.merchantIDs = new List<int>() { account.MerchantID.Value };
                        if (account.CustomerID.HasValue) filters.customersIDs = new List<int>() { account.CustomerID.Value };
                        if (account.DebitCompanyID.HasValue) filters.debitCompanyID = account.DebitCompanyID.Value;
                        page.SetFilter(filters);
                    };
                }
                else
                {
                    page.PageController.FormView.ContentTemplateContainer.Controls.Add(page.ModuleNotActivatedControl);
                    page.FormPanel.Visible = false;
                }
            }
            else if (page.TemplateName == "Settlement")
            {
                //Adding Transactions list to Settlements page.
                if (IsActive)
                {
                    if (Bll.Transactions.Transaction.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Read))
                    {
                        var tsl = page.AddControlToForm("Transactions", page.LoadControl("~/Modules/Transactions/List.ascx")).Controls[0] as Transactions.List;
                        tsl.PageChanged += (s, ev) => page.PageController.FormView.BindAndUpdate();
                        tsl.Controls.OfType<Controls.ListSection>().Single().Controls[1].Controls.OfType<Controls.AdminList>().Single().DisableRowSelect = true;
                        tsl.BeforeListDataBinding += (s, ev) =>
                        {
                            var filters = new Bll.Transactions.Transaction.SearchFilters() { PayID = page.ItemID };
                            if (filters.PayID == null) filters.ID = new Infrastructure.Range<int?>(-1);
                            page.SetFilter(filters);
                        };
                    }
                    else
                    {
                        page.AddControlToForm("Transactions", page.LoadControl("~/Modules/Transactions/List.ascx"), this, "", Bll.Transactions.Transaction.SecuredObject);
                    }
                }
                else
                {
                    page.AddControlToForm("Transactions", page.LoadControl("~/Modules/Transactions/List.ascx"), this);
                }
            }
            else if (page.TemplateName == "SettlementsUpcoming")
            {
                //Adding Transactions list to Settlements page.
                if (IsActive)
                {
                    if (Bll.Transactions.Transaction.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Read))
                    {
                        var tsl = page.AddControlToForm("Transactions", page.LoadControl("~/Modules/Transactions/List.ascx")).Controls[0] as Transactions.List;
                        tsl.PageChanged += (s, ev) => page.PageController.FormView.BindAndUpdate();
                        tsl.Controls.OfType<Controls.ListSection>().Single().Controls[1].Controls.OfType<Controls.AdminList>().Single().DisableRowSelect = true;
                        tsl.BeforeListDataBinding += (s, ev) =>
                        {
                            int merchantId = page.PageController.DataKey["MerchantID"].ToNullableInt().GetValueOrDefault();
                            string currency = "";
                            if (page.PageController.DataKey["Currency"] != null)
                            {
                                currency = page.PageController.DataKey["Currency"].ToString();
                            }

                            var filters = new Bll.Transactions.Transaction.SearchFilters();
                            if (merchantId != 0 && !string.IsNullOrEmpty(currency))
                            {
                                filters.merchantIDs = new List<int>() { merchantId };
                                filters.currencyID = int.Parse(currency);
                                filters.paymentsStatusList = new List<PaymentsStatus>() { PaymentsStatus.Unsettled };
                                page.SetFilter(filters);
                            }
                            else
                            {
                                filters.ID = new Infrastructure.Range<int?>(-1);
                                page.SetFilter(filters);
                            }
                        };
                    }
                    else
                    {
                        page.AddControlToForm("Transactions", page.LoadControl("~/Modules/Transactions/List.ascx"), this, "", Bll.Transactions.Transaction.SecuredObject);
                    }
                }
                else
                {
                    page.AddControlToForm("Transactions", page.LoadControl("~/Modules/Transactions/List.ascx"), this);
                }
            }
        }

        private void PageController_LoadItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            var status = page.PageController.DataKey["Status"].ToNullableEnum<Infrastructure.TransactionStatus>();
            var itemId = page.PageController.DataKey["ID"].ToNullableInt();

            //The loaded transaction variable
            Bll.Transactions.Transaction CurrentTransactionLoaded = null;

            if (status != null && itemId != null)
                CurrentTransactionLoaded = Bll.Transactions.Transaction.GetTransaction(null, itemId.Value, status.Value);

            //Rolling Reserve Transactions / Admin Transactions - Show only 'Summary' tab.
            if (CurrentTransactionLoaded != null && (CurrentTransactionLoaded.PaymentMethodID == CommonTypes.PaymentMethodEnum.RollingReserve || CurrentTransactionLoaded.PaymentMethodID == CommonTypes.PaymentMethodEnum.Admin))
            {
                // Remove all tabs but the "Summary" Tab
                var tabsToRemove = page.FormTabs.Tabs
                    .Where(i => i.Title != "Summary")
                    .ToList();

                foreach (var tab in tabsToRemove)
                {
                    page.RemoveControlFromForm(tab.ID);
                }
            }

            if (CurrentTransactionLoaded != null)
                page.SetItemData(CurrentTransactionLoaded);

            page.FormButtons.EnableDelete = false;
            page.FormButtons.EnableSave = false;
        }

        private void Page_Load(object sender, EventArgs e)
        {
            Bll.Transactions.Transaction transaction = null;
            var page = sender as Controls.DataTablePage;
            if (!page.RouteData.Values.ContainsKey("id") || page.RouteData.Values["id"] == null) return;
            var values = ((string)page.RouteData.Values["id"]).Split('/');
            if (values.Length != 2) return;
            var searchStatus = values[0].ToNullableEnum<Infrastructure.TransactionStatus>();
            var searchValue = values[1];
            if (searchValue != null)
            {
                var itemId = searchValue.ToNullableInt();
                if (itemId != null && itemId.Value == 0) return;
                if (itemId != null && itemId.Value != 0) transaction = Bll.Transactions.Transaction.GetTransaction(null, itemId.Value, searchStatus.GetValueOrDefault());
            }
            if (transaction != null)
            {
                page.SetItemData(transaction);
                page.PageController.Mode = Controls.DataPageController.ViewMode.Form;
                page.PageController.DataKey = new System.Web.UI.WebControls.DataKey(new System.Collections.Specialized.OrderedDictionary() { { "ID", transaction.ID }, { "Status", transaction.Status } });
            }
        }

        public void acTransaction_DataBinding(object sender, EventArgs e)
        {
            var control = sender as Transactions.AccountTransactions;
            if ((control.Page as Controls.AccountPage).Account != null && (control.Page as Controls.AccountPage).Account.AccountID > 0)
                control.IsVisible = true;
            else
                control.IsVisible = false;
        }
    }
}