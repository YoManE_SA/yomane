﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Transactions
{
    public partial class Filter : Controls.TemplateControlBase
    {
        public enum CreditTypeFilter
        {
            Refund = 0,
            Regular = 1,
            Installments = 8
        }


        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.Search += PageController_Search;
            chkPaymentStatus.Items.Remove("None");

            //Initialize the "Transaction Insert Date" filter
            //With 6 months range
            DateTime now = DateTime.Now;
            var queryString = TemplatePage.Request.QueryString.ToString();
            if (!Page.IsPostBack)
            {
                if ((!queryString.Contains("rng_InsertDate.From=")) && (!queryString.Contains("rng_InsertDate.To=")) && (rng_InsertDate.From.Text == "") && (rng_InsertDate.To.Text == "") && (!rng_ID.Value.From.HasValue) && (!rng_ID.Value.To.HasValue) && (!queryString.Contains("ctl00.rng_ID")) && !queryString.Contains("NoDateRange=1") && !queryString.Contains("SpecificTransID"))
                {
                    rng_InsertDate.From.Text = now.AddDays(-7).MinTime().ToString();
                    rng_InsertDate.To.Text = now.MaxTime().ToString();
                }
            }

            UpdateSearchFilter(); // Used for pagination situation that the filter should be invoked from the PageNumber button
            base.OnLoad(e);
        }

        public Bll.Transactions.Transaction.LoadOptions LoadOptions
        {
            get
            {
                var ret = new Bll.Transactions.Transaction.LoadOptions();
                foreach (ListItem item in chklStatus.Items)
                    if (item.Selected) ret.dataStores.Add(item.Text.ToNullableEnumByName<TransactionStatus>().GetValueOrDefault());
                return ret;
            }
        }

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
        {
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Bll.Transactions.Transaction.SearchFilters();

            //Load Options transaction status - [V]  
            sf.LoadOptionsFilter = LoadOptions;

            //Merchant AccountPicker filter - [V]
            if (apMerchant.Value.HasValue)
            {
                var merchant = Bll.Merchants.Merchant.LoadByAccountId(apMerchant.Value.Value);
                if (merchant != null)
                    sf.merchantIDs = new int[] { merchant.ID }.ToList();
            }

            //Insert date filter - [V]
            sf.date = rng_InsertDate.Value;

            //Debit company filter - [V]
            sf.debitCompanyID = ddlDebitCompany.Value.ToNullableInt();

            //Terminal filter - [V]
            //sf.debitTerminalNumber = tpTerminal.Value.NullIfEmpty();
            sf.debitTerminalNumber = tpTerminal.Text;

            //Currency filter - [V]
            sf.currencyID = ddlCurrency.SelectedValue.ToNullableInt();

            //Amount filter - [V]
            sf.amount = rng_Amount.Value;

            //Payment Status filter - [V]
            sf.paymentsStatusList = new List<PaymentsStatus>();
            foreach (ListItem paymentstatus in chkPaymentStatus.Items)
                if (paymentstatus.Selected) sf.paymentsStatusList.Add(paymentstatus.Text.ToNullableEnumByName<PaymentsStatus>().GetValueOrDefault());

            //Payment status bank drop down filter
            if (BankDropDownList.SelectedValue != "0")
            {
                sf.PaymentStatusBankDropDown = int.Parse(BankDropDownList.SelectedValue);
            }

            //Credit Type check box list filter [V]
            sf.creditTypes = new List<int>();
            foreach (ListItem item in chklCreditType.Items)
                if (item.Selected) sf.creditTypes.Add((int)item.Text.ToNullableEnumByName<CreditType>().GetValueOrDefault());

            //Transaction ID filter - [V]
            sf.ID = rng_ID.Value;

            //Customer account picker filter - [V]
            if (apCustomer.Value.ToNullableInt() != null)
            {
                var customersSf = new Bll.Customers.Customer.SearchFilters();
                customersSf.AccountsIdsList = new List<int>() { apCustomer.Value.Value };
                var customer = Bll.Customers.Customer.Search(customersSf, null).SingleOrDefault();
                if (customer != null)
                    sf.customersIDs = new int[] { customer.ID }.ToList();
            }

            //Payer filter - [Need To Be Fixed]
            sf.PayerFilters = new Bll.Transactions.Payer.SearchFilters();
            sf.PayerFilters.EmailAddress = txtPayerEmail.Text.NullIfEmpty();
            sf.PayerFilters.FirstName = txtPayerFirstName.Text.NullIfEmpty();
            sf.PayerFilters.LastName = txtPayerLastName.Text.NullIfEmpty();


            //Reference code filter - [V]
            sf.debitReferenceCode = txtDebitReferenceCode.Text.NullIfEmpty();

            //Reference number filter - It's value is always null in database , can't be checked.
            sf.debitReferenceNumber = txtDebitReferenceNum.Text.NullIfEmpty();

            //Approval number filter - [V]
            sf.debitApprovalNumber = txtApprovalNumber.Text.NullIfEmpty();

            //Reply code filter - [V]
            sf.replyCode = txtReplyCode.Text.NullIfEmpty();

            //--------------------------------------------------//

            //Charge Back Filters:
            //Denied Status filter - [V]
            if (DeniedStatusDropDown.SelectedIndex != 0)
            {
                //sf.isChargeback = true;
                sf.deniedStatusIntVal = int.Parse(DeniedStatusDropDown.SelectedValue);
            }

            //Chb Reason filter. - [V]
            if (ChargeBackReasonDropDown.SelectedIndex != -1 && ChargeBackReasonDropDown.SelectedIndex != 0)
            {
                sf.ChbReason = int.Parse(ChargeBackReasonDropDown.SelectedValue);
            }

            //Chb date filter - [V]
            sf.deniedDate = rng_ChargebackDate.Value;

            //------------------------------------------//

            //Payment method filter
            sf.paymentMethodID = (int?)ddlPaymentMethod.Value.ToNullableEnumByValue<CommonTypes.PaymentMethodEnum>();

            //Payment filters
            sf.PaymentFilters = new Bll.Transactions.Payment.SearchFilters();

            //Payment method filter - [Need to be fixed]
            //sf.PaymentFilters.PaymentMethod = ddlPaymentMethod.Value.ToNullableEnumByValue<CommonTypes.PaymentMethodEnum>();

            //First 6 digits - [V]
            sf.PaymentFilters.First6Digits = txtPaymentFirst6.Text.NullIfEmpty();
            //Last 4 digits - [V]
            sf.PaymentFilters.Last4Digits = txtPaymentLast4.Text.NullIfEmpty();


            if (chkIsFraud.Checked) sf.isFraud = true;

            //Country filter - [V]
            if (PaymentDetailsCountryDropDown.SelectedIndex != 0)
                sf.PaymentFilters.IssuerCountryIsoCode = PaymentDetailsCountryDropDown.SelectedCountry.IsoCode2;

            //Declined Transaction filters
            if (DeclinedTypeDropDown.SelectedIndex != 0)
            {
                sf.declinedType = int.Parse(DeclinedTypeDropDown.SelectedValue);
            }

            //Has un settled intallments filters
            sf.HasUnsettledInstallments = chkHasUnSettledInstallments.Checked;


            //Rolling Reserve filters
            sf.releaseDate = rngReleaseDate.Value;
            sf.showOnlyRollingReserve = chbShowOnlyRollingReserve.Checked;
            sf.iCreditType0 = chbiCreditType0.Checked;
            sf.iCreditType1 = chbiCreditType1.Checked;
            sf.iNotRelease = chbiNotRelease.Checked;

            //Specific TransID filter
            if (!string.IsNullOrEmpty(SpecificTransID.Text))
            {
                sf.specificTransactionID = int.Parse(SpecificTransID.Text);
            }

            TemplatePage.SetFilter(sf);
        }


        //Make the Transaction Status Checkboxlist all checked in order 
        //To show all types of transactions when the list is load for the
        //First time.
        protected void chklStatus_DataBound(object sender, EventArgs e)
        {
            if (TemplatePage.Request.QueryString.Count == 0)
            {
                foreach (ListItem item in chklStatus.Items)
                {
                    if (item.Value != "DeclinedArchived")
                        item.Selected = true;
                }

                UpdateSearchFilter();
                var filter = TemplatePage.GetFilter<Bll.Transactions.Transaction.SearchFilters>();
                filter.LoadOptionsFilter = LoadOptions;
                TemplatePage.SetFilter(filter);
            }
            else
            {
                var url = TemplatePage.Request.QueryString.ToString();
                foreach (ListItem item in chklStatus.Items)
                {
                    if (url.Contains("=" + item.Text))
                        item.Selected = true;
                }

                var filter = TemplatePage.GetFilter<Bll.Transactions.Transaction.SearchFilters>();
                filter.LoadOptionsFilter = LoadOptions;
                TemplatePage.SetFilter(filter);
            }

        }


        protected void chkPaymentStatus_DataBound(object sender, EventArgs e)
        {
            chkPaymentStatus.Items.Remove("None");

            var url = TemplatePage.Request.QueryString.ToString();
            foreach (ListItem item in chkPaymentStatus.Items)
            {
                if (url.Contains("=" + item.Text))
                    item.Selected = true;
            }

            UpdateSearchFilter();
            var filter = TemplatePage.GetFilter<Bll.Transactions.Transaction.SearchFilters>();
            filter.LoadOptionsFilter = LoadOptions;
            TemplatePage.SetFilter(filter);
        }

        protected void iCreditType0_CheckedChanged(object sender, EventArgs e)
        {
            chbiNotRelease.Enabled = chbiCreditType0.Checked;

            if (chbiNotRelease.Enabled == false)
            {
                chbiNotRelease.Checked = false;
            }

            upiNotReleaseCheckBox.DataBind();
            upiNotReleaseCheckBox.Update();
        }
    }
}