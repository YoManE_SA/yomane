﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Installments.ascx.cs" Inherits="Netpay.Admin.Transactions.Installments" %>

<admin:FormSections runat="server" Title="Transaction Installments">
    <asp:MultiView runat="server" ID="InstallmentsMultiView">
        <asp:View runat="server">
            <admin:AdminList runat="server" ID="rptList"  DisableRowSelect="true" AutoGenerateColumns="false" PageSize="25" IsInListView="false">
                <Columns>
                    <%-- Comment column --%>
                    <asp:BoundField HeaderText="Comment" DataField="Comment" SortExpression="Comment" />

                    <%-- Amount Column --%>
                    <asp:TemplateField HeaderText="Amount">
                        <ItemTemplate>
                            <%# ((decimal)Eval("Amount")).ToString("0.00")%>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Merchant PD column --%>
                    <asp:TemplateField HeaderText="Merchant PD">
                        <ItemTemplate>
                            <%# ((DateTime)Eval("MerchantPD")).ToShortDateString()%>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Is Paid --%>
                    <asp:TemplateField HeaderText="Bank Payed">
                        <ItemTemplate>
                            Bank Payed : <%# ((Eval("IsPaid")!=null)&&((bool)Eval("IsPaid")==true))? "Yes" : "No" %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Settlement --%>
                    <asp:TemplateField HeaderText="Settlement">
                        <ItemTemplate>
                            Settlement : <%# Eval("SettlementID") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </admin:AdminList>
        </asp:View>
        <asp:View runat="server">
            <div class="alert alert-info">
            No installments found for this transaction.
           </div >
        </asp:View>
    </asp:MultiView>
</admin:FormSections>
