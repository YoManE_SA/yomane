﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Chargeback.ascx.cs" Inherits="Netpay.Admin.Transactions.Chargeback" %>
<script type="text/javascript">

    function OpenPop(sURL, sName, sWidth, sHeight, sScroll) {
        var placeLeft = screen.width / 2 - sWidth / 2
        var placeTop = screen.height / 2 - sHeight / 2
        var winPrint = window.open(sURL, sName, 'TOP=' + placeTop + ',LEFT=' + placeLeft + ',WIDTH=' + sWidth + ',HEIGHT=' + sHeight + ',status=1,scrollbars=' + sScroll);
    }

</script>
<admin:FormSections runat="server" Title="Create ChargeBack">
    <asp:UpdatePanel runat="server" ID="UpCreateChargeBack" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="form-group">
                <asp:Label runat="server" Text="Select Chb Reason" />
                <div class="fieldValue">
                    <asp:DropDownList Enabled='<%# ItemData!=null && sIsShowDenied && sDeniedButton == "btn_SetDenialEng" %>' runat="server" CssClass="form-control" ID="ddlReasonChb" DataTextField="Value" DataValueField="Key" />
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" Text="More Text" />
                <div class="fieldValue">
                    <asp:TextBox Enabled='<%# ItemData!=null && sIsShowDenied && sDeniedButton == "btn_SetDenialEng" %>' runat="server" CssClass="form-control" ID="txtChb" TextMode="MultiLine" Rows="3" />
                </div>
            </div>
            <%-- Create ChargeBack button --%>
            <asp:Button Enabled='<%# ItemData!=null && sIsShowDenied && sDeniedButton == "btn_SetDenialEng" %>' runat="server" Text="Create" CssClass="btn btn-primary" OnCommand="Create_Chargeback" CommandArgument='<%#ItemData!=null? ItemData.ID : 0%>' /><br />
            <%-- Create ChargeBack button --%>
        </ContentTemplate>
    </asp:UpdatePanel>
</admin:FormSections>
<admin:FormSections runat="server" Title=" ChargeBack Details" ID="ChargeBackDetailsFormSection">
    <asp:UpdatePanel runat="server" ID="UpChargeBackDetails" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:MultiView runat="server" ID="ChargeBackDetailsMultiView" ActiveViewIndex="<%#ItemData!=null && ItemData.DeniedStatusID>0 && ItemData.DeniedStatusID!=3 ? 0 : 1  %>">
                <asp:View runat="server">
                    <asp:Label runat="server" Text='<%#ItemData!=null? Netpay.Bll.Transactions.DeniedStatusDes.GetDecsctiptionByStatusNumber(ItemData.DeniedStatusID) : ""%>' Font-Bold="true"></asp:Label><br />
                    <asp:Repeater runat="server" ID="ChargeBackRepeater">
                        <ItemTemplate>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <asp:Literal runat="server"
                                        Text='<%#ItemData!=null ? GetTransactionChargeBackText(new int[] {ItemData.ID , (int)Eval("ID")}) : "" %>'>
                                    </asp:Literal>
                                </li>
                                <li class="list-group-item">
                                    <asp:PlaceHolder runat="server" Visible="<%#ChargeBackHistoryItemData!=null && !string.IsNullOrEmpty(ChargeBackHistoryItemData.Description) %>">
                                        <asp:Label ForeColor="#006699" runat="server" Text="Description:" />
                                        <asp:Literal runat="server" Text='<%#ChargeBackHistoryItemData!=null? ChargeBackHistoryItemData.Description : "" %>' />
                                    </asp:PlaceHolder>

                                </li>
                                <li class="list-group-item">
                                    <asp:PlaceHolder runat="server" Visible="<%#ChargeBackHistoryItemData!=null && !string.IsNullOrEmpty(ChargeBackHistoryItemData.Description) %>">
                                        <asp:Label ForeColor="#006699" runat="server" Text="Required Media:"></asp:Label>
                                        <asp:Literal runat="server" Text='<%#ChargeBackHistoryItemData!=null? ChargeBackHistoryItemData.RequiredMedia : "" %>' />
                                    </asp:PlaceHolder>

                                </li>
                                <li class="list-group-item">
                                    <asp:PlaceHolder runat="server" Visible="<%#ChargeBackHistoryItemData!=null && !string.IsNullOrEmpty(ChargeBackHistoryItemData.Description) %>">
                                        <asp:Label ForeColor="#006699" runat="server" Text="Refundability:"></asp:Label>
                                        <asp:Literal runat="server" Text='<%#ChargeBackHistoryItemData!=null? ChargeBackHistoryItemData.RefundInfo : "" %>' />
                                    </asp:PlaceHolder>
                                </li>

                            </ul>
                            <div class="form-group">
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <%-- Remove ChargeBack button --%>
                    <asp:Button Visible='<%# sIsShowDenied && sDeniedButton == "btn_ClearDenialEng" && !DeniedStatusCantRemoveChargeBack.Contains(ItemData.DeniedStatusID) %>' runat="server" Text="Remove ChargeBack" CssClass="btn btn-primary" OnCommand="RemoveChargeBack" CommandArgument='<%#ItemData!=null? ItemData.ID : 0 %>' BackColor="Red" /><br />
                </asp:View>

                <asp:View runat="server">
                    No ChargeBack made.
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>
</admin:FormSections>

<admin:FormSections runat="server" Title="Create Retrieval Request">
    <asp:UpdatePanel runat="server" ID="UpRetrievalRequest" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="form-group">
                <asp:Label runat="server" Text="Select clarification reason" />
                <div class="fieldValue">
                    <asp:DropDownList Enabled='<%#ItemData!=null && sIsShowClarify && sClrfButton == "btn_SetClrfEng" %>' runat="server" CssClass="form-control" ID="ddlReasonRetrieval" DataTextField="Value" DataValueField="Key" />
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" Text="More Text" />
                <div class="fieldValue">
                    <asp:TextBox Enabled='<%#ItemData!=null && sIsShowClarify && sClrfButton == "btn_SetClrfEng" %>' runat="server" CssClass="form-control" ID="txtRetrieval" TextMode="MultiLine" Rows="3" />
                </div>
            </div>
            <%-- Create RetrievalRequest button --%>
            <asp:Button Enabled='<%# ItemData!=null && sIsShowClarify && sClrfButton == "btn_SetClrfEng" %>' runat="server" Text="Create" CssClass="btn btn-primary" OnCommand="Create_Retrieval" CommandArgument='<%#ItemData!=null? ItemData.ID : 0%>' /><br />
        </ContentTemplate>
    </asp:UpdatePanel>
</admin:FormSections>
<admin:FormSections runat="server" Title="Retrieval Request Details" ID="RetrievalRequestFormSection">
    <asp:UpdatePanel runat="server" ID="UpRetrievalRequestDetails" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:MultiView runat="server" ID="RetrievalRequestDetailsMultiView" ActiveViewIndex="<%#ItemData!=null && Netpay.Bll.Transactions.Transaction.ChargebackHistory.LoadForTransaction(ItemData.ID).Where(x=>x.TypeID==Netpay.CommonTypes.TransactionHistoryType.RetrievalRequest).Count()>0? 0 : 1 %>">
                <asp:View runat="server">
                    <asp:Label runat="server" Visible="<%#ItemData!=null && ItemData.DeniedStatusID==3 %>" Text='<%#ItemData!=null? Netpay.Bll.Transactions.DeniedStatusDes.GetDecsctiptionByStatusNumber(ItemData.DeniedStatusID) : ""%>' Font-Bold="true"></asp:Label><br />
                    <asp:Repeater runat="server" ID="RetrievalRequestRepeater">
                        <ItemTemplate>
                            <div class="form-group">
                                <asp:Literal runat="server"
                                    Text='<%#ItemData!=null ? GetTransactionRetrievalText(new int[] {ItemData.ID , (int)Eval("ID")}) : "" %>'>
                                </asp:Literal>

                                <asp:PlaceHolder runat="server" Visible="<%#ChargeBackHistoryItemData!=null && ChargeBackHistoryItemData.TypeID==Netpay.CommonTypes.TransactionHistoryType.RetrievalRequest && !string.IsNullOrEmpty(ChargeBackHistoryItemData.Description) %>">
                                    <br />
                                    <asp:Label ForeColor="#006699" runat="server" Text="Description:"></asp:Label>
                                    <asp:Literal runat="server" Text='<%#ChargeBackHistoryItemData!=null && ChargeBackHistoryItemData.TypeID==Netpay.CommonTypes.TransactionHistoryType.RetrievalRequest? ChargeBackHistoryItemData.Description : "" %>' /><br />
                                </asp:PlaceHolder>

                                <asp:PlaceHolder runat="server" Visible="<%#ChargeBackHistoryItemData!=null && ChargeBackHistoryItemData.TypeID==Netpay.CommonTypes.TransactionHistoryType.RetrievalRequest && !string.IsNullOrEmpty(ChargeBackHistoryItemData.RequiredMedia) %>">
                                    <asp:Label ForeColor="#006699" runat="server" Text="Required Media:"></asp:Label>
                                    <asp:Literal runat="server" Text='<%#ChargeBackHistoryItemData!=null && ChargeBackHistoryItemData.TypeID==Netpay.CommonTypes.TransactionHistoryType.RetrievalRequest? ChargeBackHistoryItemData.RequiredMedia : "" %>' /><br />
                                </asp:PlaceHolder>

                                <asp:PlaceHolder runat="server" Visible="<%#ChargeBackHistoryItemData!=null && ChargeBackHistoryItemData.TypeID==Netpay.CommonTypes.TransactionHistoryType.RetrievalRequest && !string.IsNullOrEmpty(ChargeBackHistoryItemData.RefundInfo)%>">
                                    <asp:Label ForeColor="#006699" runat="server" Text="Refundability:"></asp:Label>
                                    <asp:Literal runat="server" Text='<%#ChargeBackHistoryItemData!=null && ChargeBackHistoryItemData.TypeID==Netpay.CommonTypes.TransactionHistoryType.RetrievalRequest? ChargeBackHistoryItemData.RefundInfo : "" %>' /><br />
                                </asp:PlaceHolder>
                            </div>
                            <%-- Remove RetrievalRequest button --%>
                            <asp:Button Visible='<%#ItemData!=null && sIsShowClarify && sClrfButton == "btn_ClearClrfEng" && !DeniedStatusCantRemoveChargeBack.Contains(ItemData.DeniedStatusID) && ItemData.PrimaryPaymentID<=0%>' runat="server" Text="Remove Retrieval Request" CssClass="btn btn-primary" OnCommand="RemoveChargeBack" CommandArgument='<%#ItemData!=null? ItemData.ID : 0%>' BackColor="Red" />
                        </ItemTemplate>
                    </asp:Repeater>
                </asp:View>
                <asp:View runat="server">
                    <div class="alert alert-info">
                    No retrieval request made.
                    </div >
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>
</admin:FormSections>

<asp:UpdatePanel runat="server" ID="UpUpdateDetails" UpdateMode="Conditional">
    <ContentTemplate>
        <admin:FormSections runat="server" ID="UpdateDetailsFormSection" Title="Update Details" Visible='<%#ItemData!=null && ItemData.StatusText=="Captured" && (((ItemData.IsChargeback.Value)&&(ItemData.OriginalTransactionID==0))||ItemData.DeniedStatusID==3||ItemData.DeniedStatusID==9||ItemData.DeniedStatusID==10||ItemData.DeniedStatusID==11||ItemData.DeniedStatusID==12) %>'>
            <asp:Label runat="server" Text="CHB: " Font-Bold="true"></asp:Label><br />
            <asp:Label runat="server" Text="LANG:" ForeColor="#99ccff"></asp:Label>
            <asp:Literal runat="server" Text="<%# LanguagePreference %>"></asp:Literal><br />
            <asp:Label runat="server" Text="MESSAGE:" ForeColor="#99ccff"></asp:Label>
            <asp:LinkButton ID="SendEmailButton" runat="server" Text='<%#"Email " + sMailDateText %>' OnCommand="Send_Command" CommandName="Email" CommandArgument="<%#ItemData!=null? ItemData.ID : 0 %>"></asp:LinkButton>|
            <asp:HyperLink ID="PrintDoc"
                ClientIDMode="Static"
                runat="server"
                ForeColor="#428bca"
                Style="cursor: pointer;"
                Text='<%#"Print" + sPrintDateText %>'
                onclick='<%#ItemData!=null?"OpenPop("+"\""+"PrintDoc/"+ItemData.ID+"\""+","+"\""+"DocPrint"+"\""+",660,400,1);":"" %>'
                Target="_blank">
            </asp:HyperLink>
            <br />

            <asp:Label runat="server" Text="COMMENT:" ForeColor="#99ccff"></asp:Label>
            <asp:TextBox ID="txtChbComment"
                runat="server"
                Text='<%#  ItemData!=null && ItemData.StatusText=="Captured" && (!string.IsNullOrEmpty(ItemData.DeniedAdminComment)) ? ItemData.DeniedAdminComment : ItemData!=null && ItemData.StatusText=="Captured"? GetChargeBackHistoryItemTitle(ItemData.ID,ItemData.IsChargeback.Value , ItemData.IsRetrievalRequest.Value) : "" %>'
                Width="300">
            </asp:TextBox><br />

            <asp:Label runat="server" Text="DATE:" ForeColor="#99ccff" Width="65"></asp:Label>
            <JQ:DatePicker runat="server" ID="chbDate" Text='<%# ItemData!=null && ItemData.StatusText=="Captured" && ItemData.ChargebackDate!=null? ItemData.ChargebackDate.ToShortDateString() : ""%>' />
            <asp:Button ID="UpdateButton" runat="server" Text="Update" CssClass="btn btn-primary" OnCommand="Update_Chargeback" CommandArgument='<%# ItemData!=null? ItemData.ID : 0 %>' /><br />

        </admin:FormSections>
    </ContentTemplate>
</asp:UpdatePanel>

<admin:ModalDialog runat="server" ID="dlgSendFunction" Title="Send Email/Print">
    <Body>
        <asp:Literal runat="server" ID="ltEmailText"></asp:Literal>
        <netpay:ActionNotify runat="server" ID="SendFunctionActionNotify" />
    </Body>
</admin:ModalDialog>





