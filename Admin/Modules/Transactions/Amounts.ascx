﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Amounts.ascx.cs" Inherits="Netpay.Admin.Transactions.Amounts" %>

<admin:FormSections runat="server" Title="Transaction Amounts">

    <admin:AdminList runat="server" ID="rptList" DataKeyNames="ID" AutoGenerateColumns="false" PageSize="25"  IsInListView="false">
        <Columns>
            <asp:BoundField HeaderText="Date" DataField="InsertDate" SortExpression="InsertDate" />
            <asp:BoundField HeaderText="Type" DataField="AmountType" SortExpression="AmountType" />
            <asp:BoundField HeaderText="Installment" DataField="Installment" SortExpression="Installment" />

            <asp:BoundField HeaderText="PercentValue" DataField="PercentValue" SortExpression="PercentValue" />
            <asp:BoundField HeaderText="PercentAmount" DataField="PercentAmount" SortExpression="PercentAmount" />
            <asp:BoundField HeaderText="FixedAmount" DataField="FixedAmount" SortExpression="FixedAmount" />
            <asp:BoundField HeaderText="Total" DataField="Total" SortExpression="Total" />

            <asp:BoundField HeaderText="Settlement" DataField="SettlementType" SortExpression="SettlementType" />
            <asp:BoundField HeaderText="SettlementDate" DataField="SettlementDate" SortExpression="SettlementDate" />
            <asp:BoundField HeaderText="SettlementId" DataField="SSettlementId" SortExpression="SettlementId" />

        </Columns>
    </admin:AdminList>

</admin:FormSections>
