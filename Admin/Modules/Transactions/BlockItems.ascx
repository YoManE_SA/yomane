﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="BlockItems.ascx.cs" Inherits="Netpay.Admin.Transactions.BlockItems" %>

<admin:FormSections runat="server" SurroundOnly="true">
    <asp:UpdatePanel runat="server" ID="upExistingBlocksRepeater" UpdateMode="Conditional">
        <ContentTemplate>
            <%-- Item Black List --%>

            <asp:PlaceHolder runat="server" ID="PlaceHolder1" Visible="<%# ItemData!=null && sPaymentMethod==1 %>">
                <admin:FormSections runat="server" Title="Item Black List">
                    <div class="row">
                        <div class="col-lg-6">
                            <h4>Item Black List</h4>
                            <div class="form-group">
                                <asp:CheckBoxList runat="server" ID="cblBlockItems" RepeatLayout="OrderedList" CssClass="checkbox-list" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                Block Type:
                            <asp:DropDownList ID="BlockType" CssClass="form-control" runat="server">
                                <asp:ListItem Text="Merchant Only" Value="Merchant Only" />
                                <asp:ListItem Text="Global" Value="Global" />
                            </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <asp:Label runat="server" Text="Comment" />
                                <asp:TextBox runat="server" ID="txtItemBlackListComment" CssClass="form-control" Rows="1" TextMode="MultiLine" />
                            </div>
                            <div class="form-group">
                                <asp:Button runat="server" ID="btnUpdate" CssClass="btn btn-primary" Text="Add" OnCommand="GeneralCommand" CommandName="ItemBlackList" CommandArgument='<%# string.Format("{0}|{1}", ItemData!=null? ItemData.ID : 0 , ItemData!=null? ItemData.StatusText : "") %>' />
                            </div>
                        </div>
                    </div>

                </admin:FormSections>
            </asp:PlaceHolder>

            <%-- CC Black List --%>
            <asp:PlaceHolder runat="server" ID="CCBlackListPlaceHolder" Visible="<%# sPaymentMethod==1 && isCardDetails %>">
                <admin:FormSections runat="server" Title="CC Black List">
                    <div class="row">
                        <div class="col-lg-12">
                            <h4>
                                <asp:Label runat="server" Text="CC Black List" /></h4>
                            <asp:Label runat="server" ForeColor="Red" ID="CCBlackListExistOrNotText" />
                            <asp:CheckBoxList runat="server" CssClass="checkbox-list" ID="cblCcBlackList" RepeatLayout="OrderedList" Enabled="false" /><br />
                            <div class="form-group">
                                <asp:Label runat="server" Text="Comment" />
                                <asp:TextBox runat="server" ID="txtCCBlackListComment" CssClass="form-control" Rows="1" TextMode="MultiLine" />
                            </div>
                            <div class="form-group">
                                <asp:Button runat="server" ID="btnCCBlackList" CssClass="btn btn-primary" Text="Add" OnCommand="GeneralCommand" CommandName="CCBlackList" CommandArgument='<%# string.Format("{0}|{1}", ItemData!=null? ItemData.ID : 0 , ItemData!=null? ItemData.StatusText : "") %>' />
                            </div>
                        </div>
                    </div>
                </admin:FormSections>
            </asp:PlaceHolder>

            <%-- CC White List --%>
            <asp:PlaceHolder runat="server" ID="CCWhiteListPlaceHolder" Visible="<%# ItemData!=null && sPaymentMethod==1 %>">
                <admin:FormSections runat="server" Title="CC White List">
                    <div class="row">
                        <div class="col-lg-6">

                            <h4>
                                <asp:Label runat="server" Text="CC White List" /></h4>
                            <asp:Label runat="server" ForeColor="Red" ID="CCWhiteListExistOrNot" />
                            <div class="form-group">
                                <asp:CheckBoxList runat="server" ID="cblCcWhiteList" RepeatLayout="OrderedList" CssClass="checkbox-list" Enabled="false" />
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:Literal runat="server" Text="Level:" />
                                <netpay:EnumDropDown CssClass="form-control" runat="server" ID="ddlListType" ViewEnumName="Netpay.Bll.Risk.RiskItem+RiskListType" BlankSelectionValue="-2" />
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <asp:Label runat="server" Text="Comment" />
                                <asp:TextBox runat="server" ID="txtCommentWhiteList" CssClass="form-control" Rows="1" TextMode="MultiLine" />
                            </div>
                            <div class="form-group">
                                <asp:Button runat="server" ID="btnCCWhiteList" CssClass="btn btn-primary" Text="Add" CommandName="CCWhiteList" OnCommand="GeneralCommand" CommandArgument='<%# string.Format("{0}|{1}", ItemData!=null? ItemData.ID : 0 , ItemData!=null? ItemData.StatusText : "") %>' />
                            </div>
                        </div>
                    </div>
                </admin:FormSections>
            </asp:PlaceHolder>

            <admin:FormSections runat="server" Title="Existing Blocks">
                <asp:MultiView runat="server" ActiveViewIndex="<%#ResultItems>0? 0 : 1 %>">
                    <asp:View runat="server">
                        <div class="row inner-table-titles">
                            <div class="col-md-1">Date</div>
                            <div class="col-md-1">Source</div>
                            <div class="col-md-1">Value Type</div>
                            <div class="col-md-2" style="text-align: center">Value</div>
                            <div class="col-md-1">List Type</div>
                            <div class="col-md-2">Risk Type</div>
                            <div class="col-md-2">Comment</div>
                            <div class="col-md-1">Delete</div>
                        </div>

                        <asp:Repeater runat="server" ID="rptList" OnItemCommand="RemoveRiskItem">
                            <ItemTemplate>
                                <div class="row table-ui">
                                    <div class="col-md-1"><%# Eval("InsertDate", "{0:d}") %></div>
                                    <div class="col-md-1"><%# Eval("Source") %></div>
                                    <div class="col-md-1"><%# Eval("ValueType") %></div>
                                    <div class="col-md-2" style="text-align: center"><%# Eval("Display") %></div>
                                    <div class="col-md-1"><%# Eval("ListType") %></div>
                                    <div class="col-md-2"><%# EnumHelper.EnumNameToEnumDisplayText(Eval("RiskType").ToString()) %></div>
                                    <div class="col-md-2"><%# string.IsNullOrEmpty(Eval("Comment").ToString())? "---" : Eval("Comment") %></div>
                                    <div class="col-md-1">
                                        <asp:LinkButton ID="btnRemove" runat="server" Text="Remove" CssClass="btn btn-danger" CommandName="RemoveItem" CommandArgument='<%# string.Format("{0}|{1}|{2}|{3}|{4}", Eval("ValueType"), Eval("ListType"), Eval("ID"), ItemData!=null?ItemData.ID:0 , ItemData!=null?ItemData.StatusText:"") %>' />
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </asp:View>

                    <asp:View runat="server">
                        <div class="alert alert-info">
                            <asp:Literal runat="server" Text="No existing blocks found"></asp:Literal>
                        </div>
                    </asp:View>

                </asp:MultiView>
            </admin:FormSections>
        </ContentTemplate>
    </asp:UpdatePanel>
</admin:FormSections>
