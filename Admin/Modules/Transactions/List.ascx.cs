﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.Admin.Transactions
{
    public partial class List : Controls.TemplateControlBase
    {
        public string[] DataKeyNames = { "ID", "Status" };
        public event EventHandler PageChanged { add { rptList.PageIndexChanged += value; } remove { rptList.PageIndexChanged -= value; } }
        public event EventHandler BeforeListDataBinding;
        public Netpay.Bll.Transactions.Transaction.SearchFilters Filters { get; set; }
        public Netpay.Bll.Transactions.Transaction.LoadOptions LoadOptions { get; set; }
        public string Title { get { return lstSection.Title; } set { lstSection.Title = value; } }
        protected override void OnLoad(EventArgs e)
        {
            rptList.DataBinding += List_DataBinding;
            base.OnLoad(e);
        }

        private void List_DataBinding(object sender, EventArgs e)
        {
            BeforeListDataBinding?.Invoke(this, e);
            if (LoadOptions == null)
            {
                LoadOptions = new Bll.Transactions.Transaction.LoadOptions();
                LoadOptions.dataStores.Add(Infrastructure.TransactionStatus.Captured);
            }
            LoadOptions.loadPaymentData = false;
            LoadOptions.loadDebitCompany = true;
            LoadOptions.loadMerchant = true;
            LoadOptions.loadPayerData = true;
            LoadOptions.sortAndPage = rptList;

            //
            if (TemplatePage.GetFilter<Bll.Transactions.Transaction.SearchFilters>() != null)
            {
                var loadOptions = TemplatePage.GetFilter<Bll.Transactions.Transaction.SearchFilters>().LoadOptionsFilter;
                if (loadOptions != null && loadOptions.dataStores != null && loadOptions.dataStores.Count() >= 1)
                    LoadOptions.dataStores = loadOptions.dataStores;
            }

            var transactionList = Netpay.Bll.Transactions.Transaction.Search(LoadOptions, TemplatePage.GetFilter<Bll.Transactions.Transaction.SearchFilters>());
            rptList.DataSource = transactionList;
        }

        public string GetStatusText(string paymentidandprimaryid)
        {
            string[] results = paymentidandprimaryid.Split(',');
            string paymentidresult = results[0];
            string primarypayedid = results[1];

            if (paymentidresult.Contains(";X;")) return "Archived"; // [V]

            else if (paymentidresult.Length > 3 && paymentidresult.Contains(";0;")) return "Partial Settled"; // [V]

            else if (primarypayedid != "0") return "Settled" + "(" + primarypayedid + ")"; // [V]

            else if ((paymentidresult.Replace(";", "") == "0") && (paymentidresult.Contains(";0;"))) return "Unsettled";

            else return "";
        }


        protected void LinkButton_Command(object sender, CommandEventArgs e)
        {
            var Url = Page.Request.Url.ToString();

            var UrlRefferer = Page.Request.UrlReferrer.ToString();

            var MerchantId = e.CommandArgument.ToString();

            // When pressing on merchant name for the first time and there are no 
            // Query parameters in the UrlReferrer that means that the filter is in default situation
            // The query string is empty , and all the status button
            // Are checked.
            if (Page.Request.QueryString.Count == 0 && Page.Request.UrlReferrer.Query == "")
            {
                Response.Redirect(Url + "?ctl00.apMerchant.hdID=" + MerchantId + "&ctl00.chklStatus$0=Captured&ctl00.chklStatus$1=Declined&ctl00.chklStatus$2=Authorized&ctl00.chklStatus$3=Pending&ctl00.chklStatus$4=DeclinedArchived&ctl00.BankDropDownList=0&Search=1");
            }

            // If there is querystring and there is no merchant that was filtered
            // Add the filter of the merchant that was pressed
            else
            {
                if (UrlRefferer.Contains("apMerchant")) Response.Redirect(UrlRefferer);
                else Response.Redirect(UrlRefferer + "&ctl00.apMerchant.hdID=" + MerchantId);
            }
        }

        /// <summary>
        /// Check the row if it's disabled and should not open the Tabs section at all.
        /// These rows are only if the transaction is in "Captured" status , and depends
        /// On the paymentMethodId conditions that are located inside the function.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rptList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //Get the current row PaymentMethodId and Status in order to decide if
                //The row is disabled.
                int paymentMethod = int.Parse((e.Row.FindControl("hfPaymentMethod") as HiddenField).Value);
                string status = (e.Row.FindControl("hfStatus") as HiddenField).Value.ToString();
                bool isTest = (e.Row.FindControl("hfIsTest") as HiddenField).Value.ToBoolean(false);

                if (status == "Captured")
                {
                    if (!(paymentMethod == 1 || paymentMethod == 2 || paymentMethod == 4 || paymentMethod > 14))
                    {
                        e.Row.Attributes.Remove("onmouseover");
                        e.Row.Attributes.Remove("onclick");
                        e.Row.Attributes.Add("style", "cursor:no-drop;");
                        e.Row.CssClass = "danger";
                    }

                    int ID = int.Parse((e.Row.FindControl("hfID") as HiddenField).Value);
                    int originalTransactionID = int.Parse((e.Row.FindControl("hfOriginalTransactionID") as HiddenField).Value);
                    bool isChargeBack = (e.Row.FindControl("hfIsTest") as HiddenField).Value.ToBoolean(false);

                    //Refund transaction
                    if ((originalTransactionID > 1) && (originalTransactionID < ID) && (!isChargeBack))
                    {
                        e.Row.CssClass = "numericMinus";
                    }
                }

                if (isTest)
                {
                    var label = e.Row.FindControl("IsTestLabel") as Label;
                    label.Text = "X";
                }
            }
        }
    }
}

