﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;
using Netpay.CommonTypes;
using Netpay.Process.NetpayConnector;


namespace Netpay.Admin.Transactions
{
    public partial class Chargeback : Controls.TemplateControlBase, Web.Controls.ILoadControlParameters
    {
        public Bll.Transactions.Transaction ItemData { get; set; }

        public int OriginalTransactionId { get; set; }
        public bool bSendClarify { get; set; }

        public int[] DeniedStatusCantRemoveChargeBack = {0, 2, 4, 7, 8, 10, 11, 12};

        public bool sIsShowDenied { get; set; } //The logic is at 'trans_detail_administration.asp' line 126
        public bool sIsShowClarify { get; set; }
        public string sDeniedButton { get; set; }
        public string sClrfButton { get; set; }
        public int sDeniedStatus { get; set; }
        public int sClrfStatus { get; set; }


        public string LanguagePreference
        {
            get
            {
                if (ItemData == null) return "";
                if (ItemData.Merchant.LanguagePreference == "eng")
                {
                    return "English";
                }
                if (ItemData.Merchant.LanguagePreference == "heb")
                {
                    return "Hebrew";
                }
                return "";
            }
        }

        public string sMailDateText
        {
            get
            {
                if (ItemData != null && ItemData.Merchant != null && ItemData.DeniedSendDate != null)
                {
                    if (ItemData.DeniedSendDate.Value > new DateTime(2000, 1, 1))
                    {
                        return "&nbsp;(" + ItemData.DeniedSendDate.Value.ToShortDateString() + ")";
                    }
                    else return "";
                }
                else return "";
            }
        }

        public string sPrintDateText
        {
            get
            {
                if (ItemData != null && ItemData.Merchant != null && ItemData.DeniedPrintDate != null)
                {
                    if (ItemData.DeniedPrintDate.Value > new DateTime(2000, 1, 1))
                    {
                        return "&nbsp;(" + ItemData.DeniedPrintDate.Value.ToShortDateString() + ")";
                    }
                    else return "";
                }
                else return "";
            }
        }

        public DateTime? newDeniedDate { get; set; }

        public string newDeniedAdminComment { get; set; }

        public string ReasoneCode { get; set; }

        public Bll.Transactions.Transaction.ChargebackHistory ChargeBackHistoryItemData { get; set; }

        public void LoadControlParameters(System.Collections.Specialized.NameValueCollection parameters)
        {
            int id = parameters["ID"].ToNullableInt().GetValueOrDefault();
            TransactionStatus? transStatus = parameters["Status"].ToNullableEnumByName<TransactionStatus>();
            if (transStatus != null)
            {
                ItemData = Bll.Transactions.Transaction.GetTransaction(null, id, transStatus.Value);
                if (ItemData != null && ItemData.Status == TransactionStatus.Captured)
                {
                    //Bind to the ChargeBackRepeater the Chargebacklist
                    if (Bll.Transactions.Transaction.ChargebackHistory.LoadForTransaction(ItemData.ID).Where(x => x.TypeID == Netpay.CommonTypes.TransactionHistoryType.Chargeback).Count() > 0)
                    {
                        ChargeBackRepeater.DataSource = Bll.Transactions.Transaction.ChargebackHistory.LoadForTransaction(ItemData.ID).Where(x => x.TypeID == Netpay.CommonTypes.TransactionHistoryType.Chargeback).OrderBy(x=>x.RecordDate).ToList();
                    }

                    //Bind to the RetrievalRepeater the Chargebacklist
                    if (Bll.Transactions.Transaction.ChargebackHistory.LoadForTransaction(ItemData.ID).Where(x => x.TypeID == Netpay.CommonTypes.TransactionHistoryType.RetrievalRequest).Count() > 0)
                    {
                        RetrievalRequestRepeater.DataSource = Bll.Transactions.Transaction.ChargebackHistory.LoadForTransaction(ItemData.ID).Where(x => x.TypeID == Netpay.CommonTypes.TransactionHistoryType.RetrievalRequest).OrderBy(x => x.RecordDate).ToList();
                    }
                    

                    if (ItemData.CreditType != CreditType.Refund)
                    {
                        if ((ItemData.DeniedStatusID == (int)DeniedStatus.ValidTransaction) && (ItemData.CreditType == CreditType.Installments))
                        {
                            //In this situation , when you create a chargeback 
                            //A duplicate transaction is created , and only in the
                            //Duplicate transaction scope, you can cancel the ChargeBack.
                            //The link to the duplicate transaction , should show in the 
                            //Summary tab - 'Transaction Info' section.
                            //When the AdminUser make a chargeback , a link to the duplicate transaction
                            //Is created in Summary tab.
                            sIsShowDenied = true;
                            sIsShowClarify = true;
                            sDeniedButton = "btn_SetDenialEng";
                            sClrfButton = "btn_SetClrfEng";
                            sDeniedStatus = ItemData.PrimaryPaymentID > 0 ? 2 : 1;
                            sClrfStatus = 3;
                            //[V] 

                        }
                        //Paid transaction
                        else if ((ItemData.DeniedStatusID == (int)DeniedStatus.ValidTransaction) && ItemData.PaymentsIDs != ";0;")
                        {
                            sIsShowDenied = true;
                            sIsShowClarify = true;
                            sDeniedButton = "btn_SetDenialEng";
                            sClrfButton = "btn_SetClrfEng";
                            sDeniedStatus = 2;
                            sClrfStatus = 3;
                            //[V] 
                        }
                        //Waiting transaction
                        else if (ItemData.DeniedStatusID == (int)DeniedStatus.ValidTransaction)
                        {
                            sIsShowDenied = true;
                            sIsShowClarify = true;
                            sDeniedButton = "btn_SetDenialEng";
                            sClrfButton = "btn_SetClrfEng";
                            sDeniedStatus = 1;
                            sClrfStatus = 3;
                            //[V]
                        }
                        else if (ItemData.DeniedStatusID == (int)DeniedStatus.UnsettledInVerification)
                        {
                            sIsShowDenied = true;
                            sDeniedButton = "btn_ClearDenialEng";
                            sDeniedStatus = 0;
                            //[V]
                        }
                        else if (ItemData.DeniedStatusID == (int)DeniedStatus.SettledInVerification)
                        {
                            sIsShowDenied = true;
                            sDeniedButton = "btn_ClearDenialEng";
                            sDeniedStatus = 0;
                            //[V]
                        }
                        else if ((ItemData.DeniedStatusID == (int)DeniedStatus.UnsettledBeenSettledAndValid) || (ItemData.DeniedStatusID == (int)DeniedStatus.SetFoundValidRefunded))
                        {
                            sDeniedButton = "btn_SetDenialEng";
                            sDeniedStatus = 1;
                            sIsShowDenied = true;
                            sIsShowClarify = true;
                            sClrfButton = "btn_ClearClrfEng";
                            sClrfStatus = 0;
                            //[V] - Check if denied status 9 can be removed.
                        }
                        else if (ItemData.DeniedStatusID == (int)DeniedStatus.DuplicateValid)
                        {
                            sIsShowClarify = true;
                            sClrfButton = "btn_ClearClrfEng";
                            sClrfStatus = 0;
                            //[V]
                        }
                        else if (ItemData.DeniedStatusID == (int)DeniedStatus.DuplicateTransactionWorkedOut)
                        {
                            sIsShowDenied = true;
                            sDeniedButton = "btn_SetDenialEng";
                            sDeniedStatus = 2;
                            //[V] 
                        }
                    }
                    else if ((ItemData.DeniedStatusID == (int)DeniedStatus.DuplicateDeducted) && (ItemData.PaymentsIDs == ";0;"))
                    {
                        sIsShowDenied = true;
                        sDeniedButton = "btn_ClearDenialEng";
                        sDeniedStatus = 0;
                    }
                    else
                    {
                        sIsShowDenied = false;
                    }
                }
            }
        }

        public override void DataBind()
        {
            ddlReasonChb.DataSource = Bll.Transactions.Transaction.GetChargebackReasons();
            ddlReasonRetrieval.DataSource = Bll.Transactions.Transaction.GetChargebackReasons();
            base.DataBind();
        }

        protected void Create_Chargeback(object sender, EventArgs e)
        {
            int transactionId;
            string commandArg = (e as CommandEventArgs).CommandArgument.ToString();
            if (!string.IsNullOrEmpty(commandArg) && commandArg != "0")
            {
                transactionId = int.Parse((e as CommandEventArgs).CommandArgument.ToString());
                Bll.Transactions.Transaction.CreateChargeback(transactionId, ddlReasonChb.SelectedValue.ToNullableInt().GetValueOrDefault(), txtChb.Text);
                                                
                //Refresh all tabs in order to show the duplicate chargeback link
                //In the summary tab - if exist , and also refresh the 
                //ChargeBack tab itself
                TemplatePage.PageController.LoadActiveItem();
                TemplatePage.PageController.FormView.DataBind();
                TemplatePage.PageController.FormView.Update();
            }
        }

        protected void Create_Retrieval(object sender, EventArgs e)
        {
            int transactionId;
            string commandArg = (e as CommandEventArgs).CommandArgument.ToString();
            if (!string.IsNullOrEmpty(commandArg) && commandArg != "0")
            {
                transactionId = int.Parse((e as CommandEventArgs).CommandArgument.ToString());
                Bll.Transactions.Transaction.CreateRetrievalRequest(transactionId, ddlReasonRetrieval.SelectedValue.ToNullableInt().GetValueOrDefault(), txtRetrieval.Text);

                //Load the ItemData after ChargeBack was made in order
                //To bind it again
                ItemData = Bll.Transactions.Transaction.GetTransaction(null, transactionId, TransactionStatus.Captured);

                //Refresh all tabs in order to show the duplicate chargeback link
                //In the summary tab - if exist , and also refresh the 
                //ChargeBack tab itself
                TemplatePage.PageController.LoadActiveItem();
                TemplatePage.PageController.FormView.DataBind();
                TemplatePage.PageController.FormView.Update();
            }
        }

        public string GetTransactionChargeBackText(int[] ids)
        {
            int TransactionId = ids[0];
            int ChargeBackHistoryItemId = ids[1];
            //Need to build this :
            // ChargeBack | "Date" | "ReasoneCode" "Brand" : "Title"
            string date;
            string brand;
            string title;

            //Load the current ChargeBackItem 
            ChargeBackHistoryItemData = Bll.Transactions.Transaction.ChargebackHistory.LoadForTransaction(TransactionId).Where(x=>x.ID == ChargeBackHistoryItemId).SingleOrDefault();
            
            if (ChargeBackHistoryItemData != null)
            {
                //get the date string
                date = ChargeBackHistoryItemData.RecordDate.ToShortDateString();

                //get the reasonecode
                if (ChargeBackHistoryItemData.ReasonCode != null)
                    ReasoneCode = ChargeBackHistoryItemData.ReasonCode.Value.ToString();

                //get the brand (from the 'ChargeBackReasone class itself')
                if (ChargeBackHistoryItemData.ReasonCode == 0)
                {
                    brand = "";
                }
                else
                {
                    var ChargeBackReasonItem = Bll.Transactions.ChargeBackReason.Cache.Where(x => x.ReasonCode == ChargeBackHistoryItemData.ReasonCode).FirstOrDefault();
                    if (ChargeBackReasonItem != null)
                    {
                        brand = ChargeBackReasonItem.Brand;
                    }
                }

                //get the title
                title = ChargeBackHistoryItemData.Title;

                return "<b>ChargeBack</b> | " + date + " | " + ReasoneCode + " " + title;
            }
            return "";
        }

        public string GetTransactionRetrievalText(int[] ids)
        {
            int TransactionId = ids[0];
            int ChargeBackHistoryItemId = ids[1];
            //Need to build this :
            // RetrievalRequest | "Date" | "ReasoneCode" : "Title"
            string date;
            string title;

            //Load the current transaction chargeBack
            ChargeBackHistoryItemData = Bll.Transactions.Transaction.ChargebackHistory.LoadForTransaction(TransactionId).Where(x=>x.ID == ChargeBackHistoryItemId).SingleOrDefault();
            
            if (ChargeBackHistoryItemData != null)
            {
                //get the date string
                date = ChargeBackHistoryItemData.RecordDate.ToShortDateString();

                //get the reasonecode
                if (ChargeBackHistoryItemData.ReasonCode != null)
                    ReasoneCode = ChargeBackHistoryItemData.ReasonCode.Value.ToString();

                //get the title
                title = ChargeBackHistoryItemData.Title;

                return "<b>Retrieval Request</b> | " + date + " | " + ReasoneCode + " " + title;
            }
            return "";
        }

        protected void RemoveChargeBack(object sender, EventArgs e)
        {
            int transactionId;
            string commandArg = (e as CommandEventArgs).CommandArgument.ToString();
            if (!string.IsNullOrEmpty(commandArg) && commandArg != "0")
            {
                if (ItemData==null)
                {
                    TemplatePage.PageController.LoadActiveItem();
                    ItemData = TemplatePage.GetItemData<Bll.Transactions.Transaction>();
                    OriginalTransactionId = ItemData.OriginalTransactionID;                    
                }
                
                transactionId = int.Parse((e as CommandEventArgs).CommandArgument.ToString());
                if (transactionId > 0) Bll.Transactions.Transaction.RemoveChargeback(transactionId);

                //Load the ItemData after ChargeBack was made in order
                //To bind it again
                ItemData = Bll.Transactions.Transaction.GetTransaction(null, transactionId, TransactionStatus.Captured);

                //Invoke "LoadControlParameters" , in order to refresh all the 
                //Parameters inside it.
                if (ItemData != null) //The ItemData is null when a duplicated-Chargeback transaction is removed
                {                    
                    //Refresh all tabs after removing the chargeback
                    TemplatePage.PageController.LoadActiveItem();
                    TemplatePage.PageController.FormView.DataBind();
                    TemplatePage.PageController.FormView.Update();
                }
                //The ItemData is null - Because it is a duplicated - chargeback transaction
                //In this case - navigate to the original transaction
                else 
                {
                    Response.Redirect("~/Transactions/0?ctl00.rng_ID.From="+OriginalTransactionId+"&ctl00.rng_ID.To="+OriginalTransactionId+"&Search=1");
                }
            }
        }


        //Not in use for now , the logic of the "Remove" functionality is
        //Like AdminCahs , when deleting the Chargeback , it delete's also the
        // Retrieval request history.

        //protected void RemoveRetrievalRequest(object sender, EventArgs e)
        //{
        //    int transactionId;
        //    string commandArg = (e as CommandEventArgs).CommandArgument.ToString();
        //    if (!string.IsNullOrEmpty(commandArg) && commandArg != "0")
        //    {
        //        transactionId = int.Parse((e as CommandEventArgs).CommandArgument.ToString());
        //        if (transactionId > 0) Bll.Transactions.Transaction.RemoveRetrievalRequest(transactionId);

        //        //Load the ItemData after ChargeBack was made in order
        //        //To bind it again
        //        ItemData = Bll.Transactions.Transaction.GetTransaction(null, transactionId, TransactionStatus.Captured);

        //        //Bind and update Create RetrievalRequest section
        //        UpRetrievalRequest.DataBind();
        //        UpRetrievalRequest.Update();

        //        //Bind and update RetrievalRequest details section.
        //        UpRetrievalRequestDetails.DataBind();
        //        UpRetrievalRequestDetails.Update();
        //    }
        //}

        protected void Update_Chargeback(object sender, CommandEventArgs e)
        {
            int transactionId;
            string commandArg = (e as CommandEventArgs).CommandArgument.ToString();
            if (!string.IsNullOrEmpty(commandArg) && commandArg != "0")
            {
                transactionId = int.Parse((e as CommandEventArgs).CommandArgument.ToString());
                ItemData = Bll.Transactions.Transaction.GetTransaction(null, transactionId, TransactionStatus.Captured);

                if (ItemData != null && ItemData.ID > 0)
                {
                    //Declare the newDeniedDate
                    if (chbDate.Value.HasValue) newDeniedDate = chbDate.Value.Value;
                    else if (ItemData.ChargebackDate != null) newDeniedDate = ItemData.ChargebackDate;
                    else newDeniedDate = DateTime.Now;

                    //Declare the newDeniedAdminComment
                    if (!string.IsNullOrEmpty(txtChbComment.Text))
                    {
                        newDeniedAdminComment = txtChbComment.Text;
                    }
                    else
                    {
                        newDeniedAdminComment = string.Empty;
                    }

                    ItemData.ChargebackDate = newDeniedDate.Value;
                    ItemData.UpdateChargeBackForTransPass(ItemData.ID, newDeniedAdminComment, newDeniedDate.Value);
                }
            }
        }

        public string GetChargeBackHistoryItemTitle(int transactionId, bool isChargeBack, bool isRetrievalRequest)
        {
            string title;
            if (isChargeBack)
            {
                title = Bll.Transactions.Transaction.ChargebackHistory.LoadForTransaction(transactionId).Where(x => x.TypeID == CommonTypes.TransactionHistoryType.Chargeback).FirstOrDefault().Title;
            }
            else if (isRetrievalRequest)
            {
                title = Bll.Transactions.Transaction.ChargebackHistory.LoadForTransaction(transactionId).Where(x => x.TypeID == CommonTypes.TransactionHistoryType.RetrievalRequest).FirstOrDefault().Title;
            }
            else title = "";

            return title;
        }


        //Send Email button
        protected void Send_Command(object sender, CommandEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.CommandArgument.ToString()))
            {

                int transactionId;
                bool check = int.TryParse(e.CommandArgument.ToString(), out transactionId);
                if (check) ItemData = Bll.Transactions.Transaction.GetTransaction(null, transactionId, TransactionStatus.Captured);

                if (ItemData != null && ItemData.Merchant != null)
                {
                    string Email;
                    string Language;
                    string Message;
                    bool Success = true;
                    bSendClarify = (ItemData.DeniedStatusID == 3 || ItemData.DeniedStatusID == 9 || ItemData.DeniedStatusID == 5 || ItemData.DeniedStatusID == 11);

                    switch (e.CommandName)
                    {
                        //Send Email
                        case "Email":

                            //Set the ModalDialog title
                            dlgSendFunction.Title = "Send Email";

                            Email = !string.IsNullOrEmpty(ItemData.Merchant.ChargeBackNotifyEmail) ? ItemData.Merchant.ChargeBackNotifyEmail : ItemData.Merchant.EmailAddress;
                            Language = ItemData.Merchant.LanguagePreference;
                            CommonTypes.PendingEventType PendintEvent = bSendClarify ? CommonTypes.PendingEventType.EmailPhotoCopy : CommonTypes.PendingEventType.EmailChargeBack;

                            //Create the message that will show in the actionnotify window
                            Message = "<b>Email send scheduled.</b><br/>Mail:" + Email + "<br/>Language:" + Language;

                            try
                            {
                                Bll.PendingEvents.Create(PendintEvent, ItemData.ID, TransactionStatus.Captured, "");
                            }

                            catch (Exception ex)
                            {
                                Success = false;
                                SendFunctionActionNotify.SetException(ex);
                                dlgSendFunction.RegisterShow();
                            }

                            if (Success)
                            {
                                SendFunctionActionNotify.SetSuccess(Message);
                                dlgSendFunction.RegisterShow();
                                Bll.Transactions.Transaction.UpdateTransactionDeniedSendDate(ItemData.ID, DateTime.Now);
                            }
                            break;

                            // Print functionality located at the file - PrintDoc.aspx
                    }
                }
                else
                {
                    SendFunctionActionNotify.SetMessage("Transaction not found", true);
                    dlgSendFunction.RegisterShow();
                }
            }
        }
    }
}