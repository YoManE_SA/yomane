﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="History.ascx.cs" Inherits="Netpay.Admin.Transactions.History" %>

<admin:FormSections runat="server" Title="Transaction History">
    <asp:MultiView runat="server" ActiveViewIndex="<%#IsHistoryListExist? 0 : 1 %>">
        <asp:View runat="server">
            <admin:AdminList runat="server" ID="rptList" DataKeyNames="ID" AutoGenerateColumns="false" PageSize="25" IsInListView="false" BubbleLoadEvent="false">
                <Columns>
                    <asp:BoundField HeaderText="Date" DataField="InsertDate" SortExpression="InsertDate" />
                    <asp:BoundField HeaderText="Type" DataField="HistoryType" SortExpression="HistoryType" />
                    <asp:BoundField HeaderText="Success" DataField="IsSucceeded" SortExpression="IsSucceeded" />
                    <asp:BoundField HeaderText="Description" DataField="Description" SortExpression="Description" />
                    <asp:HyperLinkField HeaderText="Reference" DataTextField="ReferenceNumber" DataNavigateUrlFields="ReferenceUrl" SortExpression="ReferenceNumber" />
                </Columns>
            </admin:AdminList>
        </asp:View>
        <asp:View runat="server">
            No history found.
        </asp:View>
    </asp:MultiView>
</admin:FormSections>
