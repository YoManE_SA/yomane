﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.CommonTypes;
using Netpay.Process.NetpayConnector;


namespace Netpay.Admin.Transactions
{
    public partial class PrintDoc : Controls.Page
    {
        public string text { get; set; }

        public PrintDoc(string templateName, string viewName = null)
            : base(templateName, viewName)
        {
            NoMasterPage = true;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            Bll.Transactions.Transaction ItemData;
            var transId = (string)RouteData.Values["id"];
            bool check;
            int transIdInt;
            check = int.TryParse(transId, out transIdInt);
            if (check)
            {
                ItemData = Netpay.Bll.Transactions.Transaction.GetTransaction(null, transIdInt, TransactionStatus.Captured);
                var ctrl = new Literal();
                ctrl.ID = "ltEmailText";
                ctrl.Text = "Button";

                Controls.Add(ctrl);
                ltEmailText = ctrl;

                if (ItemData != null)
                {
                    bool bSendClarify = (ItemData.DeniedStatusID == 3 || ItemData.DeniedStatusID == 9 || ItemData.DeniedStatusID == 5 || ItemData.DeniedStatusID == 11);
                    EmailTransaction.EmailType emailType = bSendClarify ? EmailTransaction.EmailType.MerchantNotify_Clarify : EmailTransaction.EmailType.MerchantNotify_Denied;
                    string text = ((EmailTransaction)(Netpay.Process.Application.Modules["EmailTransaction"])).getEmailText(Netpay.Process.Application.ProcessEngine, Netpay.Web.WebUtils.CurrentDomain.Host, ItemData.ID, emailType, null);

                    ctrl.Text = text;
                }

                else
                {
                    ctrl.Text = "<b>Tranasction not found</b><br/>";
                    ctrl.Text += DateTime.Now.ToShortDateString() + " Transction not found";
                }
            }
        }
    }
}