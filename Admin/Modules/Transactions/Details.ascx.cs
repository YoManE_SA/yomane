﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Netpay.Bll.DebitCompanies;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;

namespace Netpay.Admin.Transactions
{
    public partial class Details : Controls.TemplateControlBase
    {
        public Bll.Transactions.Transaction ItemData { get; set; }

        public Bll.Transactions.FraudDetectionLog FraudDetectionItem { get; set; }

        public string Val1; //Will hold decripted data value1

        public string Val2; //Will hold decripted data value2

        public string CUI // returns decripted data value 2 , CVV number.
        {
            get
            {
                ItemData.PaymentData.MethodInstance.GetDecryptedData(out Val1, out Val2);
                return Val2;
            }
        }

        public string nRatioChargePercentage { get; set; }
        protected override void DataBindChildren()
        {
            //Get the selected transaction from the list
            if (ItemData == null) ItemData = TemplatePage.GetItemData<Bll.Transactions.Transaction>();
            if (ItemData == null) return;
            phTransactionFees.DataBind();

            if (ItemData.Status == Infrastructure.TransactionStatus.Declined || ItemData.Status == Infrastructure.TransactionStatus.DeclinedArchived)
            {
                phDeclineTransaction.DataBind();
                phDeclineTransaction.Visible = true;
            }
            if (ItemData.PaymentData != null)
            {
                phCreditCardDetails.DataBind();
                phCreditCardDetails.Visible = true;

                if (ItemData.PaymentData.BillingAddress != null)
                {
                    phBillingAddress.DataBind();
                    phBillingAddress.Visible = true;
                }
            }
            if (ItemData.PayerData != null)
            {
                phCardHolderInfo.DataBind();
                phCardHolderInfo.Visible = true;
            }

            if (ItemData.FraudDetectionLogId > 0)
            {
                // Check the Transaction Status and get the
                // Fraud Detection Item that belongs to the transaction.
                if (ItemData.Status == TransactionStatus.Captured)
                {
                    FraudDetectionItem = Bll.Transactions.FraudDetectionLog.LoadByTransPassId(ItemData.ID);
                }
                else if (ItemData.Status == TransactionStatus.Authorized)
                {
                    FraudDetectionItem = Bll.Transactions.FraudDetectionLog.LoadByTransPreAuthId(ItemData.ID);
                }
                else if (ItemData.Status == TransactionStatus.Declined)
                {
                    FraudDetectionItem = Bll.Transactions.FraudDetectionLog.LoadByTransFailId(ItemData.ID);
                }
                else if (ItemData.Status == TransactionStatus.Pending)
                {
                    FraudDetectionItem = Bll.Transactions.FraudDetectionLog.LoadByTransPendingId(ItemData.ID);
                }

                phFraudDetection.DataBind();
                phFraudDetection.Visible = true;
            }
        }
        
        public string GetFailReasonText(int transactionId)
        {
            ItemData = Bll.Transactions.Transaction.GetTransaction(null, transactionId, TransactionStatus.Declined);
            string replyCode = "";
            string sDescription = "";

            if ((ItemData.DebitCompanyID != null) && (!string.IsNullOrEmpty(ItemData.ReplyCode)))
            {

                if (!string.IsNullOrEmpty(ItemData.ReplyCode)) replyCode = ItemData.ReplyCode;
                if (!string.IsNullOrEmpty(ItemData.ReplyDescriptionText))
                {
                    sDescription = "&nbsp;-&nbsp;" + ItemData.ReplyDescriptionText;
                    return replyCode +"-"+sDescription;
                }
                else
                {
                    sDescription = "&nbsp;- No Details";
                    var responseItem = Bll.DebitCompanies.ResponseCode.GetItem((int)ItemData.DebitCompanyID, ItemData.ReplyCode);
                    if (responseItem != null)
                    {
                        sDescription = responseItem.Description;
                        return replyCode + "-" + sDescription;
                    }
                    
                    return replyCode+sDescription;
                }
            }
            else
            {
                return "";
            }
        }
    }
}