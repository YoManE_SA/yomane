﻿<%@ Page Title="Test1" Language="C#" AutoEventWireup="true" CodeBehind="PrintDoc.aspx.cs" Inherits="Netpay.Admin.Transactions.PrintDoc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Test</title>
</head>
<body>
    <asp:placeholder runat="server" id="placeHolder" />
    <form id="form1" runat="server">
    <div>
    <asp:Literal runat="server" ID="ltEmailText" Text="<%#text%>" Mode="PassThrough" />
    </div>
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td style="font-size: 13px; text-align: center;">
                    <b><asp:Literal ID="litTitle" runat="server" /></b><br />

                    Language:
                    <asp:Literal ID="litError" runat="server" /><br />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
