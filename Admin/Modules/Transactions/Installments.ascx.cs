﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Transactions
{
    public partial class Installments : Controls.TemplateControlBase
    {
        protected override void DataBindChildren()
        {
            var ItemData = TemplatePage.GetItemData<Bll.Transactions.Transaction>();
            if (ItemData != null)
            {
                var Installmentslist = Bll.Transactions.Installment.LoadForTransaction(ItemData.ID);
                if (Installmentslist.Count > 0)
                {
                    InstallmentsMultiView.ActiveViewIndex = 0;
                    rptList.DataSource = Installmentslist;
                }
                else
                {
                    InstallmentsMultiView.ActiveViewIndex = 1;
                }
            }
            base.DataBindChildren();
        }
    }
}