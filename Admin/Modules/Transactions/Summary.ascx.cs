﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;


namespace Netpay.Admin.Transactions
{
    public partial class Summary : Controls.TemplateControlBase
    {
        public Bll.Transactions.Transaction ItemData { get; set; }

        public List<int> RefundTransactionsIds { get; set; }

        public List<int> DuplicatedTransactionsIds { get; set; }

        public string MerchantEmail { get; set; }

        protected override void DataBindChildren()
        {
            if (ItemData == null) ItemData = TemplatePage.GetItemData<Bll.Transactions.Transaction>();
            if (ItemData == null) return;
            phTransaction.DataBind();
            phActions.DataBind();
            
            //Conditions and views for 'Captured' transactions.
            if (ItemData.Status == TransactionStatus.Captured)
            {
                //Refund MultiView Conditions
                if ((int)ItemData.CreditType > 0)
                {
                    //Get the current transaction refunds
                    RefundTransactionsIds = Bll.Transactions.Transaction.GetRefundsAdmin(ItemData.ID);
                    //If there is refund , show it in the "Transaction Refund" section.
                    if (RefundTransactionsIds.Count() > 0)
                    {
                        RefundMultiView.ActiveViewIndex = 0;
                        RefundRepeater.DataSource = RefundTransactionsIds;
                        phGlobal.DataBind();
                        phGlobal.Visible = true;

                        // [V]
                    }
                }
                /*line 479*/
                else if ((ItemData.OriginalTransactionID > 1) && (ItemData.OriginalTransactionID < ItemData.ID) && (ItemData.IsChargeback == null || ItemData.IsChargeback.Value != true))
                {
                    RefundMultiView.ActiveViewIndex = 1;
                    phGlobal.DataBind();
                    phGlobal.Visible = true;
                    //[V]
                }
                //End of Refund MultiView conditions.

                //Denied Status multiView conditions
                if ((ItemData.DeniedStatusID == 3) || (ItemData.DeniedStatusID == 9))
                {
                    //Show DeniedStatusDesc (line 494)
                    phTransactionDeniedStatus.ActiveViewIndex = 0;
                    phGlobal.DataBind();
                    phGlobal.Visible = true;
                } //[V]

                else if ((ItemData.DeniedStatusID == 5) || (ItemData.DeniedStatusID == 6))
                {
                    //Show DeniedStatusDesc (line 504)
                    phTransactionDeniedStatus.ActiveViewIndex = 1;
                    phGlobal.DataBind();
                    phGlobal.Visible = true;
                } //[V]

                else if ((ItemData.IsChargeback.HasValue && ItemData.IsChargeback.Value) && (ItemData.OriginalTransactionID == 0))
                {
                    //Show DeniedStatusDesc (line 516)
                    phTransactionDeniedStatus.ActiveViewIndex = 2;
                    phGlobal.DataBind();
                    phGlobal.Visible = true;
                } // [V]

                else if (((int)ItemData.CreditType != 0) && (ItemData.DeniedStatusID >= 10))
                {
                    DuplicatedTransactionsIds = Bll.Transactions.Transaction.GetDuplicate(ItemData.ID);
                    if (DuplicatedTransactionsIds.Count() > 0)
                    {
                        //Show DeniedStatusDes (line 528)
                        phTransactionDeniedStatus.ActiveViewIndex = 3;
                        DuplicatedRepeater.DataSource = DuplicatedTransactionsIds;
                        phGlobal.DataBind();
                        phGlobal.Visible = true;
                    } //[V]
                }
                else
                {
                    //phTransactionDeniedStatus.ActiveViewIndex = 4;
                    //phGlobal.Visible = false;
                }
                //End of Denied status multiView conditions.

                //Show link to original transaction (Original code at AdminCash is at "Trans_detail_chb.asp" line 81...)
                if ((ItemData.DeniedStatusID > 0) && (ItemData.OriginalTransactionID > 0))
                {
                    phGlobal.DataBind();
                    phGlobal.Visible = true;
                    phOriginalTransactionLink.Visible = true;
                }
                //Rolling Reserve Transactions / Admin Transcations - both of them show only Summary tab.
                phRollingReserveAdmin.Visible = (ItemData.PaymentMethodID == CommonTypes.PaymentMethodEnum.RollingReserve || ItemData.PaymentMethodID == CommonTypes.PaymentMethodEnum.Admin); 
                if (phRollingReserveAdmin.Visible) //Show 'Rolling Resereve' FormSection
                {
                    phActions.Visible = false; // Don't show 'General Actions' FormSection
                    phRollingReserveAdmin.DataBind(); // Bind The 
                    txtAdminComment.Text = ItemData.Comment;

                    upRollingReserve.Visible = ItemData.PaymentMethodID == CommonTypes.PaymentMethodEnum.RollingReserve;
                    if (upRollingReserve.Visible) {
                        if (ItemData.CreditType == CreditType.Refund)
                            lblReserevReleased.Visible = !(btnReleaseReserve.Visible = (ItemData.OriginalTransactionID == 0));
                        else  btnReleaseReserve.Visible = lblReserevReleased.Visible = false;
                    }
                }
            }

            if (ItemData.Status == TransactionStatus.Pending)
            {
                phPendingActions.DataBind();
                phPendingActions.Visible = true;
            }

            if (ItemData.Status == TransactionStatus.Authorized)
            {
                phPreAuthTransactions.DataBind();
                phPreAuthTransactions.Visible = true;
            }
        }

        protected void DoActions(object source, EventArgs args)
        {
            var e = args as CommandEventArgs;
            if (e != null)
            {
                //Get transaction 'id' and 'status' from the CommandEventArgs
                var transactionIdAndStatus = e.CommandArgument.ToString().Split('~');
                int TransactionIdVal = int.Parse(transactionIdAndStatus[0]);
                TransactionStatus? TransactionStatusVal = transactionIdAndStatus[1].ToNullableEnumByName<TransactionStatus>();

                //Load the Transaction based on it's "id" and "status" 
                if (TransactionIdVal > 0 && TransactionStatusVal != null)
                    ItemData = Bll.Transactions.Transaction.GetTransaction(null, TransactionIdVal, TransactionStatusVal.Value);
                if (ItemData == null) return;

                string commandName = e.CommandName;
                try
                {
                    switch (e.CommandName)
                    {
                        case "EmailClient": // [V]
                            // Get the client Email Address from the 'ItemData.PayerData' property.
                            string ClientEmailAddress;
                            if (ItemData.PayerData != null)
                            {
                                ClientEmailAddress = ItemData.PayerData.EmailAddress;
                                if (ClientEmailAddress != "")
                                {
                                    ItemData.SendClientEmail(ClientEmailAddress);
                                }
                                else
                                {
                                    throw new Exception("To send transaction notificatin to client, First need to set an address for notifications in client settings.");
                                }
                            }
                            break;

                        case "EmailMerchant": // [V]

                            //Check if this merchant has Email address
                            if (ItemData.Merchant != null)
                            {
                                if (!string.IsNullOrEmpty(ItemData.Merchant.ProcessSettings.MerchantNotifyEmails))
                                    MerchantEmail = ItemData.Merchant.ProcessSettings.MerchantNotifyEmails;

                                if (!string.IsNullOrEmpty(MerchantEmail))
                                {
                                    ItemData.SendMerchantEmail(MerchantEmail);
                                }
                                else
                                { throw new Exception("To send transaction notificatin to merchant, First need to set an address for notifications in merchant settings."); }
                            }
                            else
                            { throw new Exception("To send transaction notificatin to merchant, First need to set an address for notifications in merchant settings."); }
                            break;

                        case "Fraud":
                            ItemData.SetFraud(!ItemData.IsFraud);
                            break;
                            //default:
                            //    return base.OnBubbleEvent(source, args);
                    }
                    ltNotification.Visible = true;
                    ltNotification.Text = commandName + " Success";
                    TemplatePage.PageController.FormView.DataBind();
                    TemplatePage.PageController.FormView.Update();
                    //upCommandStatus.Update();
                }
                catch (Exception ex)
                {
                    ltNotification.Visible = true;
                    ltNotification.Text = commandName + " Error: " + ex.Message;
                    upCommandStatus.Update();
                }
                //return true;
            }
            // return base.OnBubbleEvent(source, args);
        }


        protected void General_Command(object sender, CommandEventArgs e)
        {
            int transactionId = int.Parse(e.CommandArgument.ToString());
            ItemData = Bll.Transactions.Transaction.GetTransaction(null, transactionId, TransactionStatus.Pending);
            if (ItemData == null || ItemData.ID <= 0) return;

            //Get the note value
            string systemText = txtSystemText.Text;

            switch (e.CommandName)
            {    
                case "Pass":
                    int lChTransID = Bll.Transactions.Transaction.MovePendingTransaction(ItemData.ID, "000", null, systemText);

                    if (lChTransID != 0)
                    {    
                        TemplatePage.SetListViewMessage("Moved to passed transactions, new transaction id:" + lChTransID, false);
                        TemplatePage.PageController.OnDeleted(new EventArgs());
                    }
                    else
                    {
                        TemplatePage.FormActionNotify.SetMessage("Moving transaction to passed transaction failed.", true);
                        TemplatePage.PageController.FormView.BindAndUpdate();
                    }
                    //Implement here the logic of ChargeBackTrans ... line 153 on admincash
                    break;
                case "Fail":
                    lChTransID = Bll.Transactions.Transaction.MovePendingTransaction(ItemData.ID, ItemData.ReplyCode, null, systemText);
                    if (lChTransID != 0)
                    {
                        TemplatePage.SetListViewMessage("Moved to failed transactions, new failed transaction id:" + lChTransID, false);
                        TemplatePage.PageController.OnDeleted(new EventArgs());
                    }
                    else
                    {
                        TemplatePage.FormActionNotify.SetMessage("Moving transaction to failed transaction failed.", true);
                        TemplatePage.PageController.FormView.BindAndUpdate();
                    }
                    break;
                case "Return":
                    lChTransID = Bll.Transactions.Transaction.MovePendingTransaction(ItemData.ID, "ACRET", null, systemText);
                    if (lChTransID != 0)
                    {
                        TemplatePage.SetListViewMessage("Moved to failed transactions, new failed transaction id:" + lChTransID, false);
                        TemplatePage.PageController.OnDeleted(new EventArgs());
                    }
                    else
                    {
                        TemplatePage.FormActionNotify.SetMessage("Moving transaction to passed transaction failed.", true);
                        TemplatePage.PageController.FormView.BindAndUpdate();
                    }
                    break;
            }
        }

        protected void SaveComment(object sender, CommandEventArgs e)
        {
            if (TemplatePage.ItemID == null) return;
            Bll.Transactions.Transaction.UpdateTransactionComment(TemplatePage.ItemID.Value, txtAdminComment.Text);
        }

        protected void RollingReserve_Command(object sender, CommandEventArgs e)
        {
            if (TemplatePage.ItemID == null) return;
            ItemData = Bll.Transactions.Transaction.GetTransaction(null, TemplatePage.ItemID.Value, TransactionStatus.Captured);
            if ((string) e.CommandArgument != "Release") return;
            var payment = Bll.Totals.CPayment.Create(ItemData.MerchantID.GetValueOrDefault(), ItemData.CurrencyID);
            payment.AddRetReserve(TemplatePage.ItemID.Value, true);
            payment.UpdatePayment(DateTime.Now, null, true);
            lblReserevReleased.Text = string.Format("Settlement Created # {0}", payment.PayID);
            phRollingReserveAdmin.Visible = true;
            lblReserevReleased.Visible = !(btnReleaseReserve.Visible = false);
        }
    }
}
