﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Details.ascx.cs" Inherits="Netpay.Admin.Transactions.Details" %>

<script type="text/javascript">

    function OpenTerminalPopUp(applicationPath, terminalNumber) {
        window.open(applicationPath + "/DebitCompanies/Terminals/0?ctl00.txtNumber=" + terminalNumber + "&Search=1&Page=0&SelectedIndex=0&NoLayout=1",
        "Terminal", "height=800, width=1350,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,left=150");
    }
</script>

<asp:Label runat="server" ID="ltNotification" CssClass="Notification" EnableViewState="false" Visible="false" />
<div style="clear: both;"></div>

<%-- Failed transaction info --%>
<asp:PlaceHolder runat="server" ID="phDeclineTransaction" Visible="false">
    <admin:FormSections runat="server" Title="Failed transaction details">
        <div class="row">
            <div class="col-lg-12">
                <ul class="list-group">
                    <%-- Fail reason --%>
                    <asp:PlaceHolder runat="server" Visible='<%#ItemData.Status==TransactionStatus.Declined %>'>
                        <li class="list-group-item">
                            <asp:Label runat="server" Font-Bold="true" Text="Failed reason:" />
                            <%# ItemData!=null? GetFailReasonText(ItemData.ID) : ""  %><br />
                        </li>
                    </asp:PlaceHolder>
                </ul>
            </div>
        </div>
    </admin:FormSections>
</asp:PlaceHolder>

<%-- Fee Info --%>
<asp:PlaceHolder ID="phTransactionFees" runat="server" Visible="<%#ItemData.Status!=TransactionStatus.Pending%>">
    <admin:FormSections runat="server" Title="Fee Info">
        <div class="row">
            <div class="col-lg-12">
                <ul class="list-group">

                    <asp:PlaceHolder runat="server" Visible="false">

                        <%#nRatioChargePercentage = ItemData!=null && ItemData.StatusText=="Captured" && ItemData.Amount!=0? (((ItemData.RatioFee)/(ItemData.Amount))*100).ToString("0.0") : ""  %>
                        
                    </asp:PlaceHolder>

                    <%-- Trans  --%>
                    <asp:PlaceHolder runat="server" Visible="<%#ItemData.Status==TransactionStatus.Captured %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Trans: " />
                            <%# ItemData.TransactionFee.ToAmountFormat(ItemData.CurrencyIsoCode) %>
                            <br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- Clearing  --%>
                    <asp:PlaceHolder runat="server" Visible="<%#ItemData.Status==TransactionStatus.Captured %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Clearing: " />
                            <%# "(&plusmn;"+nRatioChargePercentage+"%) "+ItemData.RatioFee.ToAmountFormat(ItemData.CurrencyIsoCode) %>
                            <br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- Copy R  --%>
                    <asp:PlaceHolder runat="server" Visible="<%#ItemData.Status==TransactionStatus.Captured %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Copy R: " />
                            <%# ItemData.ClarificationFee.ToAmountFormat(ItemData.CurrencyIsoCode) %>
                            <br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- Chb  --%>
                    <asp:PlaceHolder runat="server" Visible="<%#ItemData.Status==TransactionStatus.Captured %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Chb: " />
                            <%# ItemData.ChargebackFee.ToAmountFormat(ItemData.CurrencyIsoCode) %>
                            <br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- Handling  --%>
                    <asp:PlaceHolder runat="server" Visible="<%#ItemData.Status==TransactionStatus.Captured %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Handling: " />
                            <%# ItemData.HandlingFee.ToAmountFormat(ItemData.CurrencyIsoCode) %>
                            <br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- Trans for "Declined" or "Pre Authorized"  --%>
                    <asp:PlaceHolder runat="server" Visible="<%#ItemData.Status==TransactionStatus.Declined||ItemData.Status==TransactionStatus.Authorized %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Trans: " />
                            <%# ItemData.TransactionFee.ToAmountFormat(ItemData.CurrencyIsoCode) %>
                            <br />
                        </li>
                    </asp:PlaceHolder>
                </ul>
            </div>
        </div>
    </admin:FormSections>
</asp:PlaceHolder>

<%-- Credit Card Info --%>
<asp:PlaceHolder ID="phCreditCardDetails" runat="server" Visible="false">
    <admin:FormSections runat="server" Title="Credit Card Info">
        <div class="row">
            <div class="col-lg-12">
                <ul class="list-group">
                    <%-- Bin Number (First 6)  --%>
                    <asp:PlaceHolder runat="server" Visible="<%# !string.IsNullOrEmpty(ItemData.PaymentData.MethodInstance.Value1First6)%>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Bin Number: " />
                            <%# !string.IsNullOrEmpty(ItemData.PaymentData.MethodInstance.Value1First6)? ItemData.PaymentData.MethodInstance.Value1First6 : "" %>
                            <br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- Exp Date  --%>
                    <asp:PlaceHolder runat="server" Visible="<%#ItemData.PaymentData.MethodInstance.ExpirationDate.HasValue %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Exp Date: " />
                            <%#ItemData.PaymentData.MethodInstance.ExpirationDate.HasValue? ItemData.PaymentData.MethodInstance.ExpirationDate.Value.ToString("MM")+"/"+ItemData.PaymentData.MethodInstance.ExpirationDate.Value.ToString("yy"): ""%>
                            <br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- Type  --%>
                    <asp:PlaceHolder runat="server" Visible="<%# !string.IsNullOrEmpty(ItemData.PaymentData.MethodInstance.PaymentMethod.Name) %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Type: " />
                            <%# ItemData.PaymentData.MethodInstance.PaymentMethod.Name%>
                            <br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- Country  --%>
                    <asp:PlaceHolder runat="server" Visible="<%#!string.IsNullOrEmpty(ItemData.PaymentData.IssuerCountryIsoCode)%>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Country: " />
                            <%#ItemData.PaymentData.IssuerCountryIsoCode!=null? Country.Get(ItemData.PaymentData.IssuerCountryIsoCode).Name : ""%>
                            <br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- CUI - For now it's not in use. --%>
                    <%--<asp:PlaceHolder runat="server" Visible="<%#!string.IsNullOrEmpty(CUI) && ObjectContext.Current.User.IsInRole(Netpay.Infrastructure.Security.UserRole.Admin) %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="CUI: " />
                            <%# !string.IsNullOrEmpty(CUI)? CUI : "" %>
                            <br />
                        </li>
                    </asp:PlaceHolder>--%>
                </ul>
            </div>
        </div>
    </admin:FormSections>
</asp:PlaceHolder>

<%-- Card Holder's Info --%>
<asp:PlaceHolder ID="phCardHolderInfo" runat="server" Visible="false">
    <admin:FormSections runat="server" Title="CARD HOLDER'S INFO">
        <div class="row">
            <div class="col-lg-12">
                <ul class="list-group">
                    <%-- Name  --%>
                    <asp:PlaceHolder runat="server" Visible="<%# !string.IsNullOrEmpty(ItemData.PayerData.FullName) %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Name: " />
                            <%# !string.IsNullOrEmpty(ItemData.PayerData.FullName)?ItemData.PayerData.FullName : ""  %>
                            <br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- Personal Number  --%>
                    <asp:PlaceHolder runat="server" Visible="<%# !string.IsNullOrEmpty(ItemData.PayerData.PersonalNumber) %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Personal Number: " />
                            <%# !string.IsNullOrEmpty(ItemData.PayerData.PersonalNumber)? ItemData.PayerData.PersonalNumber : "" %>
                            <br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- Phone Number  --%>
                    <asp:PlaceHolder runat="server" Visible="<%# !string.IsNullOrEmpty(ItemData.PayerData.PhoneNumber) %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Phone Number: " />
                            <%# !string.IsNullOrEmpty(ItemData.PayerData.PhoneNumber)? ItemData.PayerData.PhoneNumber : "" %>
                            <br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- Email --%>
                    <asp:PlaceHolder runat="server" Visible="<%# !string.IsNullOrEmpty(ItemData.PayerData.EmailAddress)%>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Email: " />
                            <%# !string.IsNullOrEmpty(ItemData.PayerData.EmailAddress)? ItemData.PayerData.EmailAddress : ""%>
                            <br />
                        </li>
                    </asp:PlaceHolder>
                </ul>
            </div>
        </div>
    </admin:FormSections>
</asp:PlaceHolder>

<%-- Billing Address --%>
<asp:PlaceHolder runat="server" ID="phBillingAddress" Visible="false">
    <admin:FormSections runat="server" Title="Billing Address">
        <div class="row">
            <div class="col-lg-12">
                <ul class="list-group">
                    <%-- address1 --%>
                    <asp:PlaceHolder runat="server" Visible="<%#!string.IsNullOrEmpty(ItemData.PaymentData.BillingAddress.AddressLine1) %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Address 1: " />
                            <%# ItemData.PaymentData.BillingAddress.AddressLine1%>
                            <br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- address2 --%>
                    <asp:PlaceHolder runat="server" Visible="<%#!string.IsNullOrEmpty(ItemData.PaymentData.BillingAddress.AddressLine2) %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Address 2: " />
                            <%# ItemData.PaymentData.BillingAddress.AddressLine2%>
                            <br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- City --%>
                    <asp:PlaceHolder runat="server" Visible="<%#!string.IsNullOrEmpty(ItemData.PaymentData.BillingAddress.City) %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="City: " />
                            <%# ItemData.PaymentData.BillingAddress.City %>
                            <br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- Zip/Postal code --%>
                    <asp:PlaceHolder runat="server" Visible="<%#!string.IsNullOrEmpty(ItemData.PaymentData.BillingAddress.PostalCode) %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Postal Code: " />
                            <%# ItemData.PaymentData.BillingAddress.PostalCode %>
                            <br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- State Name --%>
                    <asp:PlaceHolder runat="server" Visible="<%#!string.IsNullOrEmpty(ItemData.PaymentData.BillingAddress.StateISOCode) %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="State: " />
                            <%# !string.IsNullOrEmpty(ItemData.PaymentData.BillingAddress.StateISOCode)? Netpay.Bll.State.GetName(ItemData.PaymentData.BillingAddress.StateISOCode) : "" %>
                            <br />
                        </li>
                    </asp:PlaceHolder>

                    <%-- Country Name  --%>
                    <asp:PlaceHolder runat="server" Visible="<%#!string.IsNullOrEmpty(ItemData.PaymentData.BillingAddress.CountryISOCode) %>">
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Country: " />
                            <%# !string.IsNullOrEmpty(ItemData.PaymentData.BillingAddress.CountryISOCode)? Netpay.Bll.Country.Get(ItemData.PaymentData.BillingAddress.CountryISOCode).Name : "" %>
                            <br />
                        </li>
                    </asp:PlaceHolder>
                </ul>
            </div>
        </div>
    </admin:FormSections>
</asp:PlaceHolder>

<%-- Fraud Detection Data --%>
<asp:PlaceHolder runat="server" ID="phFraudDetection" Visible="false">
    <admin:FormSections runat="server" Title="Fraud Mng">
        <div class="row">
            <div class="col-lg-12">
                <ul class="list-group">
                    <%-- Score Received --%>
                    <asp:PlaceHolder runat="server" Visible="<%# ItemData.FraudDetectionLogId>0 %>">
                        <li class="list-group-item">
                            <%-- Score Received --%>
                            <asp:Label Font-Bold="true" runat="server" Text="Score Received: " />
                            <%# FraudDetectionItem!=null && FraudDetectionItem.ReturnScore.HasValue? FraudDetectionItem.ReturnScore.Value.ToString("N") : "Fraud detection log not found" %>
                            <%# FraudDetectionItem!=null && FraudDetectionItem.IsTemperScore.HasValue && FraudDetectionItem.IsTemperScore.Value ? "*" : "Fraud detection log not found" %>
                            <%# FraudDetectionItem!=null && FraudDetectionItem.ReturnRiskScore.HasValue? " / " + FraudDetectionItem.ReturnRiskScore.Value.ToString("N") : "Fraud detection log not found" %><br />
                        </li>
                        <%-- Score Allowed --%>
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Score Allowed: " />
                            <asp:HyperLink ID="hlScoreAllowed" runat="server"
                                Font-Underline="true"
                                Text='<%#FraudDetectionItem!=null? "(" +FraudDetectionItem.AllowedScore + " / " + FraudDetectionItem.AllowedRiskScore +")" : ""%>'
                                NavigateUrl='<%# WebUtils.ApplicationPath+"/Merchants/0?ctl00.apAccount.hdID="+ItemData.MerchantID+"&Search=1&Page=0&SelectedIndex=0&OpenTab=true#BodyContent_PageController_FormView_FormPanel_Risk" %>'
                                Target="_blank">
                            </asp:HyperLink><br />
                        </li>

                        <%-- Reference Code --%>
                        <li class="list-group-item">
                            <asp:Label Font-Bold="true" runat="server" Text="Reference Code: " />
                            <asp:HyperLink ID="hlReferenceCode" runat="server"
                                Font-Underline="true"
                                Text='<%#FraudDetectionItem!=null? FraudDetectionItem.ReferenceCode : "" %>'
                                NavigateUrl='<%# FraudDetectionItem!=null? WebUtils.ApplicationPath + "/Risk/MaxMind/0?ctl00.rng_ID.From="+FraudDetectionItem.FraudDetectionId+"&ctl00.rng_ID.To="+FraudDetectionItem.FraudDetectionId+"&Search=1&Page=0" : ""  %>'
                                Target="_blank">
                            </asp:HyperLink>
                        </li>
                    </asp:PlaceHolder>
                </ul>
            </div>
        </div>
    </admin:FormSections>
</asp:PlaceHolder>


<%--
<asp:PlaceHolder runat="server" ID="phChargeback" Visible="false">
    <admin:FormSections runat="server" Title="Chargeback">
        <div class="row">
            <div class="col-lg-6">
                <asp:Label runat="server" Text="Denied Status" />
                <%# ItemData.DeniedStatusText %>
                <br />
                <asp:Label runat="server" Text="Is Chargeback" />
                <%# ItemData.IsChargeback %>
            </div>
            <div class="col-lg-6">
                <asp:Label runat="server" Text="Original TransID" />
                <%# ItemData.OriginalTransactionID %>
                <br />
                <asp:Label runat="server" Text="Admin Comment" />
                <%# ItemData.DeniedAdminComment %>
            </div>
        </div>
    </admin:FormSections>
</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="phShippingAddress" Visible="false">
    <admin:FormSections runat="server" Title="Shipping Address">
        <%# Netpay.Web.Controls.Format.FormatAddress(ItemData.PayerData.ShippingDetails, Netpay.Web.Controls.FormatAddressStyle.MultipleLines) %>
    </admin:FormSections>
</asp:PlaceHolder>--%>

<%-- This block of code maybe will be needed in the future
     <a href="#" id="lbRefundAsk" onclick="<%# Netpay.Web.Controls.PopupContainer.Popup(this, "Refund Request", 500, "RefundRequest.ascx", string.Format("ID={0}&Status={1}", ItemData.ID, ItemData.Status)) %>">Ask Refund</a><br />
     <asp:HyperLink runat="server" ID="hlLogChargeAttempts" Text="Charge Attempt" Target="_blank" NavigateUrl='<%# string.Format("../Log_ChargeAttempts.aspx?iTransNum={0}&iTransTbl={1}", ItemData.ID, ItemData.Status) %>' /><br />
     <asp:HyperLink runat="server" ID="hlVirtualTerminal" Text="Virtual Terminal" Target="_top" NavigateUrl='<%# string.Format("../charge_form.asp?CompanyID={2}&tblName={1}&MerchantTransID={0}", ItemData.ID, ItemData.Status, ItemData.MerchantID) %>' />
--%> 



