﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Bll.Risk;


namespace Netpay.Admin.Transactions
{
    public partial class BlockItems : System.Web.UI.UserControl, Web.Controls.ILoadControlParameters
    {
        public int TransactionID { get; set; }
        public TransactionStatus? Status { get; set; }

        public string TransStatusText { get; set; }

        public int PMD_CC_MIN = 20;

        public int PMD_CC_MAX = 99;

        public bool IsBlock { get; set; }

        public int ResultItems { get; set; }

        public bool isCardDetails { get; set; }
        public int sPaymentMethod { get; set; }
        public Bll.Transactions.Transaction ItemData { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            ddlListType.ExcludeValue = Netpay.Bll.Risk.RiskItem.RiskListType.Black.ToString();
        }

        public void LoadControlParameters(System.Collections.Specialized.NameValueCollection parameters)
        {

            TransactionID = parameters["ID"].ToNullableInt().GetValueOrDefault();
            Status = parameters["Status"].ToNullableEnumByName<TransactionStatus>().GetValueOrDefault();

            if (TransactionID > 0 && Status != null)
            {
                ItemData = Bll.Transactions.Transaction.GetTransaction(null, TransactionID, Status.Value);
            }

            if (ItemData != null)
            {
                if (((int)this.ItemData.PaymentMethodID - PMD_CC_MIN >= 0) && (this.ItemData.PaymentMethodID - PMD_CC_MAX <= 0))
                {
                    sPaymentMethod = 1;
                }

                if (this.ItemData.PaymentData != null)
                {
                    isCardDetails = true; //Look at trans_detail_cc.asp line 192 etc.
                }
                else isCardDetails = false;

                //Clear the Risk Items checkboxes from the existing values
                cblBlockItems.Items.Clear();
                cblCcBlackList.Items.Clear();
                cblCcWhiteList.Items.Clear();

                //Create list of Risk Item Values that will contain the optional values that can be signed as Risk Item
                var riv = new List<Bll.Risk.RiskItem.RiskItemValue>();

                //Add to the Risk Item Values list the relevant items from the current transaction(ItemData)
                if (ItemData != null)
                {
                    if (ItemData.PayerData != null)
                    {
                        if (ItemData.PayerData.EmailAddress!=null)
                        addRiskItem(riv, Bll.Risk.RiskItem.RiskValueType.Email, ItemData.PayerData.EmailAddress.Trim());

                        if (ItemData.PayerData.FullName!=null)
                        addRiskItem(riv, Bll.Risk.RiskItem.RiskValueType.FullName, ItemData.PayerData.FullName);

                        if (ItemData.PayerData.PhoneNumber!=null)
                        addRiskItem(riv, Bll.Risk.RiskItem.RiskValueType.Phone, ItemData.PayerData.PhoneNumber);

                        if (ItemData.PayerData.PersonalNumber!=null)
                        addRiskItem(riv, Bll.Risk.RiskItem.RiskValueType.PersonalNumber, ItemData.PayerData.PersonalNumber);
                    }
                    //For now take care only to credit card details and not BankAccount.
                    if (ItemData.PaymentData != null && ItemData.PaymentData.MethodInstance is Netpay.Bll.PaymentMethods.CreditCard)
                    {
                        if (ItemData.PaymentData.MethodInstance.HasValue1)
                           addRiskItem(riv, Bll.Risk.RiskItem.RiskValueType.AccountValue1, System.Convert.ToBase64String(ItemData.PaymentData.MethodInstance.GetEncyptedValue1()));
                        
                        if (ItemData.PaymentData.MethodInstance.Value1First6!=null)
                        addRiskItem(riv, Bll.Risk.RiskItem.RiskValueType.Bin, ItemData.PaymentData.MethodInstance.Value1First6.Trim());

                        if (ItemData.PaymentData.IssuerCountryIsoCode!=null)
                        addRiskItem(riv, Bll.Risk.RiskItem.RiskValueType.BinCountry, ItemData.PaymentData.IssuerCountryIsoCode.Trim());
                    }
                }

                //Search the existing risk items in order to decide if the checkbox's are selected or not.
                var result = Bll.Risk.RiskItem.Search(null, null, riv, true);

                //Check if 'CC White List' exists , if so get the comment and level , if not reset the comment and level
                if ((result.Count() > 0) && result.Exists(x => x.RiskType == RiskItem.RiskItemType.White_List))
                {
                    ddlListType.Value = ((int)result.Where(x => x.RiskType == RiskItem.RiskItemType.White_List).SingleOrDefault().ListType).ToString();
                    txtCommentWhiteList.Text = result.Where(x => x.RiskType == RiskItem.RiskItemType.White_List).SingleOrDefault().Comment;
                    CCWhiteListExistOrNot.Text = "CC WhiteList already exist.";
                    btnCCWhiteList.Text = "Update";
                }
                else //Happens when removing the white list item or when there is no white list item exist
                {
                    ddlListType.Value = "-2"; // Set the dropdown to blank selection value.
                    txtCommentWhiteList.Text = ""; // Set the comment to be empty.
                    CCWhiteListExistOrNot.Text = "";
                    btnCCWhiteList.Text = "Add";
                }

                //Check if 'CC Black List' exists , if so get the comment.
                if ((result.Count() > 0) && result.Exists(x => x.RiskType == RiskItem.RiskItemType.Black_List))
                {
                    txtCCBlackListComment.Text = result.Where(x => x.RiskType == RiskItem.RiskItemType.Black_List).FirstOrDefault().Comment;
                    CCBlackListExistOrNotText.Text = "CC BlackList already exist.";
                    txtCCBlackListComment.Enabled = false;
                    btnCCBlackList.Enabled = false;

                }
                else // Happens when removing the 'CC Black List' item.
                {
                    txtCCBlackListComment.Text = "";
                    CCBlackListExistOrNotText.Text = "";
                    txtCCBlackListComment.Enabled = true;
                    btnCCBlackList.Enabled = true;
                }


                //Add the values that are int the Risk Item Values list(riv) to the different checkbox's and decide
                //If the CheckBox is selected or not , based on the "result" value of the "search" function
                foreach (var nItem in riv)
                {
                    if (nItem.ValueType == RiskItem.RiskValueType.AccountValue1)
                    {
                        //Get the masked Credit Card number.
                        string masked1;
                        string masked2;
                        ItemData.PaymentData.MethodInstance.GetMaskedData(out masked1, out masked2);

                        //Create the checkbox that represents the CCBlack List.
                        cblCcBlackList.Items.Add(new ListItem("Credit Card" + ": " + masked1, nItem.ValueType.ToString(), true) { Selected = result.Exists(n => (n.ValueType == nItem.ValueType) && (n.RiskType == RiskItem.RiskItemType.Black_List)) });

                        //Create the checkbox that represents that CCWhite List.
                        cblCcWhiteList.Items.Add(new ListItem("Credit Card" + ": " + masked1, nItem.ValueType.ToString(), true) { Selected = result.Exists(n => (n.ValueType == nItem.ValueType) && (n.RiskType == RiskItem.RiskItemType.White_List)) });
                    }
                    else
                    {
                        cblBlockItems.Items.Add(new ListItem(nItem.ValueType + ": " + nItem.Value, nItem.ValueType.ToString(), true) { Selected = result.Exists(n => n.ValueType == nItem.ValueType) });
                    }
                }

                //Bind the Existing black list items found to the Existing Blocks list.              
                ResultItems = result.Count();
                rptList.DataSource = result;
            }
        }

        private void addRiskItem(List<Bll.Risk.RiskItem.RiskItemValue> items, Bll.Risk.RiskItem.RiskValueType valueType, string value)
        {
            if (string.IsNullOrEmpty(value)) return;
            items.Add(new Bll.Risk.RiskItem.RiskItemValue() { ValueType = valueType, Value = value });
        }

        protected void RemoveRiskItem(object source, RepeaterCommandEventArgs e)
        {
            string[] RiskItemAndTransactionDetails = e.CommandArgument.ToString().Split('|');
            //Get the RiskItem details
            RiskItem.RiskValueType ValueType = (RiskItem.RiskValueType)Enum.Parse(typeof(RiskItem.RiskValueType), RiskItemAndTransactionDetails[0]);
            RiskItem.RiskListType ListType = (RiskItem.RiskListType)Enum.Parse(typeof(RiskItem.RiskListType), RiskItemAndTransactionDetails[1]);
            int? RiskItemId = RiskItemAndTransactionDetails[2].ToNullableInt().GetValueOrDefault();

            //Delete the RiskItem                      
            Bll.Risk.RiskItem.Delete(ValueType, ListType, new List<int>() { RiskItemId.GetValueOrDefault() });

            //Get the current transaction details
            //Get the transaction id
            if (RiskItemAndTransactionDetails[3] != "0")
            {
                int TransId;
                bool check = int.TryParse(RiskItemAndTransactionDetails[3], out TransId);
                if (check) TransactionID = TransId;
            }

            //Get the transaction status
            if (!string.IsNullOrEmpty(RiskItemAndTransactionDetails[4]))
                TransStatusText = RiskItemAndTransactionDetails[4];


            //LoadControlParameters with the current transaction details in order to invoke LoadControlParameters that refreshes the data
            var values = new System.Collections.Specialized.NameValueCollection();
            values.Add("ID", TransactionID.ToString());
            values.Add("Status", TransStatusText);
            LoadControlParameters(values);

            //Update the ExistingBlocks List
            upExistingBlocksRepeater.DataBind();
            upExistingBlocksRepeater.Update();
        }

        protected void GeneralCommand(object sender, CommandEventArgs e)
        {
            string[] TransactionDetails = e.CommandArgument.ToString().Split('|');

            //Get the current transaction details
            //Get the transaction id
            if (TransactionDetails[0] != "0")
            {
                int TransId;
                bool check = int.TryParse(TransactionDetails[0], out TransId);
                if (check) TransactionID = TransId;
            }

            //Get the transaction status
            if (!string.IsNullOrEmpty(TransactionDetails[1]))
                TransStatusText = TransactionDetails[1];


            var trans = Bll.Transactions.Transaction.GetTransaction(null, TransactionID, (TransactionStatus)Enum.Parse(typeof(TransactionStatus), TransStatusText));
            if (trans == null) return;

            switch (e.CommandName)
            {
                case "ItemBlackList":
                    var riv = new List<Bll.Risk.RiskItem.RiskItemValue>();
                    foreach (ListItem li in cblBlockItems.Items)
                    {
                        if (!li.Selected) continue;
                        var value = li.Value.ToNullableEnumByName<Bll.Risk.RiskItem.RiskValueType>();
                        switch (value.GetValueOrDefault())
                        {
                            case Bll.Risk.RiskItem.RiskValueType.Email:
                                addRiskItem(riv, Bll.Risk.RiskItem.RiskValueType.Email, trans.PayerData.EmailAddress); break;
                            case Bll.Risk.RiskItem.RiskValueType.FullName:
                                addRiskItem(riv, Bll.Risk.RiskItem.RiskValueType.FullName, trans.PayerData.FullName); break;
                            case Bll.Risk.RiskItem.RiskValueType.Phone:
                                addRiskItem(riv, Bll.Risk.RiskItem.RiskValueType.Phone, trans.PayerData.PhoneNumber); break;
                            case Bll.Risk.RiskItem.RiskValueType.PersonalNumber:
                                addRiskItem(riv, Bll.Risk.RiskItem.RiskValueType.PersonalNumber, trans.PayerData.PersonalNumber); break;
                            case Bll.Risk.RiskItem.RiskValueType.Bin:
                                addRiskItem(riv, Bll.Risk.RiskItem.RiskValueType.Bin, trans.PaymentData.MethodInstance.Value1First6); break;
                            case Bll.Risk.RiskItem.RiskValueType.BinCountry:
                                addRiskItem(riv, Bll.Risk.RiskItem.RiskValueType.BinCountry, trans.PaymentData.IssuerCountryIsoCode); break;
                        }
                    }
                    //Create the Black list items list
                    // Note: First parameter is of RiskSource type. Zeev said (16/5/16) that the value from Admin should always be 1.
                    //       The first parameter 'source' is of type RiskSource, which doesn't seem to be related to BlockLevel. TBD to review and fix where necessary.
                    if (BlockType.SelectedValue == "Merchant Only")
                    {
                        Bll.Risk.RiskItem.Create(Bll.Risk.RiskItem.RiskSource.Merchant, Bll.Risk.RiskItem.RiskListType.Black, riv, null, trans.MerchantID, null, null, null, null, trans.ReplyCode, txtItemBlackListComment.Text, null);
                    }
                    else
                    {
                        Bll.Risk.RiskItem.Create(Bll.Risk.RiskItem.RiskSource.Merchant, Bll.Risk.RiskItem.RiskListType.Black, riv, null, null, null, null, null, null, trans.ReplyCode, txtItemBlackListComment.Text, null);
                    }
                    break;

                case "CCBlackList":
                    //Get The CreateCreditCardBlackList function param's
                    string maskedNumber;
                    string masked2;
                    trans.PaymentData.MethodInstance.GetMaskedData(out maskedNumber, out masked2);

                    RiskItem.RiskSource source = RiskItem.RiskSource.Merchant;
                    int? blockMinutes = null;
                    int? riskRuleId = null;
                    short? paymentMethodId = (short)trans.PaymentMethodID;
                    string replyCode = trans.ReplyCode;
                    string comment = txtCCBlackListComment.Text;
                    System.Data.Linq.Binary cc256 = trans.PaymentData.MethodInstance.GetEncyptedValue1();

                    //Create the CCBlackListItem
                    RiskItem.CreateCreditCardBlackList(source, blockMinutes, riskRuleId, paymentMethodId, maskedNumber, replyCode, comment, cc256);
                    break;

                case "CCWhiteList":
                    //Remove whitelist is only from the Existing blocks list
                    if (ddlListType.SelectedIndex == 0) return;

                    //Get the CreateCreditCardWhiteList function parameters
                    int? binNumber = int.Parse(trans.PaymentData.MethodInstance.Value1First6);
                    string binCountry = trans.PaymentData.IssuerCountryIsoCode; // Check if correct
                    string cardHolder = trans.PayerData.FullName; //Check if this is the Card Holder
                    System.Data.Linq.Binary WLcc256 = trans.PaymentData.MethodInstance.GetEncyptedValue1();
                    string WLcomment = txtCommentWhiteList.Text;
                    byte? expMonth = (byte?)trans.PaymentData.MethodInstance.ExpirationMonth;
                    short? expYear = (short?)(trans.PaymentData.MethodInstance.ExpirationYear); // Fix this to be 4 digits year
                    short ccLast4 = Convert.ToInt16(trans.PaymentData.MethodInstance.Value1Last4);
                    Bll.Risk.RiskItem.RiskListType ListType = (RiskItem.RiskListType)Enum.Parse(typeof(RiskItem.RiskListType), ddlListType.SelectedValue);
                    int? merchantIdParam = trans.MerchantID;
                    byte? WLpaymentMethodId = (byte?)trans.PaymentData.MethodInstance.PaymentMethodId;
                    string ip = trans.IP;

                    //Create the whitelist
                    Bll.Risk.RiskItem.CreateCreditCardWhiteList(binNumber, binCountry, cardHolder, WLcc256, WLcomment, expMonth, expYear, ccLast4, ListType, merchantIdParam, WLpaymentMethodId, ip);
                    break;
            }

            //LoadControlParameters...
            DataBind();

            //Update the page
            upExistingBlocksRepeater.DataBind();
            upExistingBlocksRepeater.Update();
        }
    }
}