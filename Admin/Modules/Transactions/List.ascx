﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Transactions.List" %>

<script type="text/javascript">

    function OpenPopSettlement(applicationPath, PrimaryPaymentID, sWidth, sHeight) {
        var placeLeft = screen.width / 2 - sWidth / 2
        var placeTop = screen.height / 2 - sHeight / 2
        window.open(applicationPath + "/Settlements/0?ctl00.rngID.From=" + PrimaryPaymentID + "&ctl00.rngID.To=" + PrimaryPaymentID + "&search=1&NoLayout=1",
        "Settlement", 'top=' + placeTop + ',left=' + placeLeft + ',width=' + sWidth + ',height=' + sHeight + ',status=no,resizable=no,scrollbars=yes');
    }
</script>


<admin:ListSection ID="lstSection" runat="server" Title="Transactions">
    <Header>
        <admin:LegendMultiColors runat="server" Items='<%# Netpay.Bll.Transactions.Transaction.TransactionColorDictionary %>' ItemsText='<%# Netpay.Bll.Transactions.Transaction.TransactionColorDictionaryText %>' IsX='<%# Netpay.Bll.Transactions.Transaction.TransactionColorDictionaryIsX %>' FloatRight="true" />
    </Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" ID="rptList" SaveAjaxState="true" DataKeyNames='<%#DataKeyNames%>' AutoGenerateColumns="false" BubbleLoadEvent="true" OnRowDataBound="rptList_RowDataBound" IsInListView='<%# (Page as Netpay.Admin.Controls.DataTablePage).TemplateName == "Settlement" ? false : true %>'>
                <Columns>
                    <%-- Legend column --%>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <div class="div-wrap-cell-test-transaction">
                                <div class="text-center div-has-x-sign">
                                    <asp:Label runat="server" ID="IsTestLabel"></asp:Label>
                                </div>
                                <div class="legend-item-maybe-test-left" style='background-color: <%# Netpay.Bll.Transactions.Transaction.TransactionColorDictionary.MapHtmlMultiColorLeft((Tuple<TransactionStatus?,FailedTransactionType?>)Eval("GetTransactionLegendKey")) %>;'></div>
                                <div class="legend-item-maybe-test-right" style='background-color: <%# Netpay.Bll.Transactions.Transaction.TransactionColorDictionary.MapHtmlMultiColorRight((Tuple<TransactionStatus?,FailedTransactionType?>)Eval("GetTransactionLegendKey")) %>;'></div>
                            </div>
                            <asp:HiddenField runat="server" ID="hfID" Value='<%# Eval("ID") %>' />
                            <asp:HiddenField runat="server" ID="hfIsChargeBack" Value='<%# Eval("IsChargeback")!= null? (bool)Eval("IsChargeBack") : false %>' />
                            <asp:HiddenField runat="server" ID="hfOriginalTransactionID" Value='<%# (int)Eval("OriginalTransactionID") %>' />
                            <asp:HiddenField runat="server" ID="hfStatus" Value='<%# Eval("Status") %>' />
                            <asp:HiddenField runat="server" ID="hfPaymentMethod" Value='<%# (int)Eval("PaymentMethodID") %>' />
                            <asp:HiddenField runat="server" ID="hfIsTest" Value='<%# (bool)Eval("IsTest") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- ID column --%>
                    <asp:TemplateField HeaderText="ID">
                        <ItemTemplate>
                            <asp:MultiView runat="server" ActiveViewIndex='<%# (Page as DataTablePage).TemplateName == "Transaction"? 0 : 1 %>'>
                                <asp:View runat="server">
                                    <%# Eval("ID") %>
                                </asp:View>
                                <asp:View runat="server">
                                    <asp:HyperLink runat="server">
                                        <asp:HyperLink Style="cursor: pointer" runat="server" onclick='<%# "OpenPop(" + "\"" +  string.Format("../Transactions/0?ctl00.rng_ID.From={0}&ctl00.rng_ID.To={0}&Search=1&Page=0&SelectedIndex=0&NoLayout=1", Eval("ID")) + "\"" + ",\"Transactions\",\"1330\",\"820\" , \"1\"); return false;"  %>' Text='<%# Eval("ID") %>' Target="_blank" />
                                    </asp:HyperLink>
                                </asp:View>
                            </asp:MultiView>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Date column --%>
                    <asp:BoundField HeaderText="Date" DataField="InsertDate" />

                    <%-- Amount column --%>
                    <asp:TemplateField HeaderText="Amount">
                        <ItemTemplate>
                            <asp:Label CssClass='<%# (decimal)Eval("SignedAmount") < 0 ? "numericMinus" : "" %>' Text='<%# new Money((decimal)Eval("SignedAmount"), (int)Eval("CurrencyID")).ToIsoString() %>' runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Payment Method column --%>
                    <asp:TemplateField HeaderText="Payment Method">
                        <ItemTemplate>
                            <asp:Image runat="server" ImageUrl='<%# "/NPCommon/ImgPaymentMethod/23X12/" + (int)Eval("PaymentMethodID") + ".gif" %>' ImageAlign="Middle" />
                            <asp:Image runat="server" ImageUrl='<%# "/NPCommon/ImgCountry/18X12/" + Eval("IssuerCountryIsoCode") + ".gif" %>' ImageAlign="Middle" Visible='<%# !string.IsNullOrEmpty((string)Eval("IssuerCountryIsoCode")) %>' />
                            <asp:Literal runat="server" Text='<%#Eval("PaymentMethodDisplay")%>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Credit Type column --%>
                    <asp:BoundField HeaderText="CreditType" DataField="CreditType" />

                    <%-- Transaction source column --%>
                    <asp:BoundField HeaderText="Source" DataField="TransactionSource"/>

                    <%-- Merchant Name column 'onclick="event.cancelBubble=true;"' --%>
                    <asp:TemplateField HeaderText="Merchant Name">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton" runat="server"
                                CommandArgument='<%#Eval("Merchant.ID").ToString()%>'
                                OnCommand="LinkButton_Command"
                                CommandName="Link"
                                Text='<%# Eval("Merchant.Name") %>'
                                OnClientClick="event.cancelBubble=true;">
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Debit column --%>
                    <asp:TemplateField HeaderText="Debit">
                        <ItemTemplate>
                            <asp:Image runat="server" ImageUrl='<%# "/NPCommon/ImgDebitCompanys/23X12/" + Eval("DebitCompanyID") + ".gif" %>' ImageAlign="Middle" Visible='<%# ((int?) Eval("DebitCompanyID")) != null %>' />
                            <%# Eval("DebitCompany.Name") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Reply code column --%>
                    <asp:BoundField HeaderText="Reply" DataField="ReplyCode" />

                    <%-- Status column --%>
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:HyperLink
                                class='<%# Eval("PrimaryPaymentID").ToString() == "0" ? "link-button-disabled" : "link-button-enabled" %>'
                                runat="server"
                                Text='<%# Eval("PaymentsIDs") == null ? "" : GetStatusText(Eval("PaymentsIDs").ToString()+","+Eval("PrimaryPaymentID").ToString()) %>'
                                onclick='<%# Eval("PrimaryPaymentID").ToString() != "0" ? "OpenPopSettlement("+"\""+WebUtils.ApplicationPath+"\""+","+"\""+Eval("PrimaryPaymentID")+"\""+",1270,480);event.cancelBubble=true;" : "event.cancelBubble=true;" %>'>
                            </asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" ID="adButtons" PagedControlID="rptList" EnableExport="true" EnableAdd="false" />
    </Footer>
</admin:ListSection>


