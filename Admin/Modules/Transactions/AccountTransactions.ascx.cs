﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Transactions
{
    public partial class AccountTransactions : Controls.TemplateControlBase
    {
        public bool IsVisible {get; set;}
       
        protected void Page_Load(object sender, EventArgs e)
        {
            rptData.DataBinding += delegate
            {
                if ((TemplatePage as Controls.AccountPage).Account != null)
                {
                    rptData.DataSource = Bll.Transactions.Transaction.GetBalanceByCurrency(new Bll.Transactions.Transaction.SearchFilters() { HasUnsettledInstallments = true, merchantIDs = new List<int>() { (TemplatePage as Controls.AccountPage).Account.MerchantID.GetValueOrDefault() } });
                    
                }
            };

            if ((TemplatePage as Controls.AccountPage).Account != null && (TemplatePage as Controls.AccountPage).Account.AccountID > 0)
                IsVisible = true;
            else
                IsVisible = false;
        }

        protected void Unsettled_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "AddItem":
                    mdAddTransaction.RegisterShow();
                    Page.Validate("Transactions_AccountTransactions");
                    if (Page.IsValid == false)
                    {
                        UnsettledSaveButton.Style.Add("opacity", "0.2");
                        UnsettledSaveButton.Style.Add("cursor", "default");
                        UnsettledSaveButton.Attributes.Add("disabled", "true");
                    }
                    else
                    {
                        UnsettledSaveButton.Style.Add("opacity", "1");
                        UnsettledSaveButton.Style.Add("cursor", "pointer");
                        UnsettledSaveButton.Attributes.Remove("disabled");
                    }
                    break;


                case "SaveItem":
                    if (Page.IsValid)
                    {
                        var trans = new Bll.Transactions.Transaction() { PaymentData = null , Status = TransactionStatus.Captured , PaymentsIDs = ";0;" };
                                        
                        trans.MerchantID = (TemplatePage as Controls.AccountPage).Account.MerchantID.GetValueOrDefault();
                        trans.TransactionSource = TransactionSource.AdminCharge;

                        if (TransactionTypeList.SelectedIndex == 0) // Admin
                        {
                            trans.IsPaymentManagerTransaction = true;
                            trans.PaymentMethodDisplay = "---";
                            trans.PaymentMethodID = CommonTypes.PaymentMethodEnum.Admin;
                        }
                        else // Fees
                        {
                            trans.IsPaymentFeesTransaction = true;
                            trans.PaymentMethodDisplay = "Manual Fee";
                            trans.PaymentMethodID = CommonTypes.PaymentMethodEnum.ManualFees;
                        }

                        if (TransactionModeList.SelectedIndex == 1) // (Manager,Credit) or (Manager,Both)
                            trans.CreditType = CreditType.Regular;
                        else  // (Manager,Debit) or (Fee,)
                            trans.CreditType = CreditType.Refund;

                        trans.IP = Web.WebUtils.GetVisitorIPAddress();
                    

                        trans.TransactionSource = TransactionSource.AdminCharge;
                        trans.Amount = txtAmount.Text.ToNullableDecimal().GetValueOrDefault();
                        trans.CurrencyID = ddlCurrency.SelectedValue.ToInt32(); // TBD: Verify with Zeev
                        trans.CurrencyIsoCode = ddlCurrency.SelectedCurrencyIso; // TBD: Verify with Zeev
                        trans.Comment = txtComment.Text;
                        trans.MerchantPayDate = DateTime.Now;

                        trans.Installments = 1;

                        trans.PayForText = txtText.Text;

                        trans.Save();

                        //Clean the input fields after saving item.
                        txtAmount.Text = "";
                        ddlCurrency.SelectedIndex = 0;
                        txtText.Text = "";
                        txtComment.Text = "";
                    }

                    mdAddTransaction.RegisterHide();
                    fpUnsettledStatus.BindAndUpdate();
                    
                    break;
            }
        }

        protected void TransactionTypeSelectedIndexChanged(object sender, EventArgs e)
        {
            if (TransactionTypeList.SelectedIndex == 0)
            {
                for (int i = 0; i < TransactionModeList.Items.Count; i++)
                {
                    TransactionModeList.Items[i].Enabled = true;
                }
                
                TransactionModeList.ClearSelection();
                TransactionModeList.Style.Add("opacity", "1");
                mdAddTransaction.BindAndUpdate();
            }
            else
            {
                TransactionModeList.SelectedIndex = 0;
                for (int i = 0; i < TransactionModeList.Items.Count; i++)
                {
                    TransactionModeList.Items[i].Enabled = false;
                }
                            
                TransactionModeList.Style.Add("opacity", "0.2");
                mdAddTransaction.BindAndUpdate();
                
            }

        }
    }
}
