﻿<%@ Control Language="C#" AutoEventWireup="False" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Content.List" %>
<admin:FormPanel runat="server" Title="Content" ID="pnlList" EnableAdd='<%# ((TemplatePage.Page is AccountPage) && TemplatePage.ItemID != null && TemplatePage.ItemID != 0) || (TemplatePage.ItemID != null && TemplatePage.TemplateName == "ApplicationIdentity" && TemplatePage.ItemID>0) %>' OnCommand="Edit_Command">
    <asp:MultiView ID="EditLoginMultiView" runat="server" ActiveViewIndex='<%# ((TemplatePage.Page is AccountPage) && TemplatePage.ItemID!=null && TemplatePage.ItemID!=0) || (TemplatePage.ItemID!=null && TemplatePage.TemplateName == "ApplicationIdentity" && TemplatePage.ItemID>0) ? 0 : 1%>'>
        <asp:View ID="ViewIfVisible" runat="server">
     <div class="table-responsive">
        <admin:AdminList EmptyDataRowStyle-CssClass="EmptyData" DisableRowSelect="false" runat="server" ID="rptList" DataKeyNames="FileName,IsPublic,SourceID" AutoGenerateColumns="False">
	        <Columns>
		        <asp:BoundField HeaderText="IsPublic" DataField="IsPublic" />
		           <asp:TemplateField HeaderText="File Name">
                        <ItemTemplate>
                            <asp:Button  ForeColor="Blue" CssClass="btn-link-style" ID="Button1" runat="server"  Text='<%# Eval("FileName") %>'  OnCommand="Edit_Command" CommandName="EditItem"></asp:Button>
                        </ItemTemplate>
                    </asp:TemplateField>
	        </Columns>
            <EmptyDataTemplate>
                <div class="alert alert-info">
                No data found.
                </div>
            </EmptyDataTemplate>
        </admin:AdminList>
     </div>
             </asp:View>
        <asp:View ID="ViewIfNotVisible" runat="server">
            <div class="alert alert-info">
            You need to save your details in order to edit this section.
                </div>
        </asp:View>
    </asp:MultiView>
</admin:FormPanel>

<admin:ModalDialog runat="server" ID="dlgEdit" Title="Edit Content" OnCommand="Edit_Command">
    <Body>
        <asp:HiddenField runat="server" ID="hfSourceId" Value='<%# ItemData.SourceID %>' />
        <asp:HiddenField runat="server" ID="hfFileName" Value='<%# ItemData.FileName %>' />
        <asp:HiddenField runat="server" ID="hfIsPublic" Value='<%# ItemData.IsPublic %>' />
        Title
		<asp:TextBox runat="server" ID="txtTitle" CssClass="form-control" Text='<%# ItemData.Title %>' /><br />
        <netpay:ActionNotify runat="server" ID="errorActionNotify" />
		<JQ:HtmlEdit runat="server" ID="heHtmlData" Text='<%# ItemData.HtmlData %>' />
	</Body>
    <Footer>
        <admin:DataButtons ID="DataButtons1" runat="server" EnableAdd="false" EnableDelete="true" EnableSave="true" />
    </Footer>
</admin:ModalDialog>
