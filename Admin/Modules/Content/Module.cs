﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Content
{
	public class Module : Admin.CoreBasedModule
	{
		public override string Name { get { return "Content"; } }
		public override decimal Version { get { return 1.0m; } }
		public override string Author { get { return "OBL ltd."; } }
		public override string Description { get { return "Content pages management for merchants and application identities"; } }

		public Module() : base(Bll.Content.Module.Current) {}

        protected override void OnInit(EventArgs e)
        {
            //No menu item

            //No Route

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
		{			
			base.OnActivate(e);
		}

		protected override void OnDeactivate(EventArgs e)
		{		
			base.OnDeactivate(e);
		}

		protected void Application_InitTemplatePage(object sender, EventArgs e)
		{
            if (!CoreModule.IsInstalled)
                return;

			var page = sender as Controls.DataTablePage;
            if ((page.TemplateName == "ApplicationIdentity" || page.TemplateName == "Merchant") && (page.ViewName == null))
			{
                var ctl = page.LoadControl("~/Modules/Content/List.ascx") as List;
                if (page.TemplateName == "Merchant")
                {                    
                    ctl.UserRole = Infrastructure.Security.UserRole.Merchant;
                    page.AddControlToForm("Hosted", ctl, this, "", new Controls.SecuredObjectSelector(Bll.Merchants.Merchant.SecuredObject, Bll.Content.StaticFiles.SecuredObject).GetActivObject);
                }
                else //app identity
                {    
                    ctl.UserRole = Infrastructure.Security.UserRole.Admin;
                    page.AddControlToForm("Hosted", ctl, this, "", new Controls.SecuredObjectSelector(Bll.ApplicationIdentity.SecuredObject, Bll.Content.StaticFiles.SecuredObject).GetActivObject);
                }
			}
		}
        
    }
}