﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Content
{
    public partial class List : Controls.TemplateControlBase
    {
        public Bll.Content.StaticFiles ItemData { get; set; }
        public Infrastructure.Security.UserRole? UserRole { get; set; }
        public string SourceID { get; set; }
        protected override void OnLoad(EventArgs e)
        {
            ItemData = new Bll.Content.StaticFiles();

            rptList.DataBinding += delegate 
            {
                rptList.DataSource = Bll.Content.StaticFiles.Search(new Bll.Content.StaticFiles.SearchFilters() { IsPublic = false, Source = UserRole, SourceID = (Page is Controls.AccountPage) && (Page as Controls.AccountPage).Account != null ? (Page as Controls.AccountPage).Account.MerchantID.ToString() : TemplatePage.ItemID.ToString() }, rptList);
            };

            dlgEdit.DataBinding += delegate { if (ItemData == null) ItemData = new Bll.Content.StaticFiles(); };
            base.OnLoad(e);
        }

        protected void List_SelectedIndexChanged(object sender, EventArgs e)
        {
            Edit_Command(this, new CommandEventArgs("EditItem", null));
        }

        protected void Edit_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "AddItem":

                    ItemData = new Bll.Content.StaticFiles();
                    dlgEdit.BindAndShow();
                    break;

                case "EditItem":
                    ItemData = Bll.Content.StaticFiles.Load(new Bll.Content.StaticFiles.SearchFilters() { Source = UserRole, SourceID = rptList.SelectedDataKey["SourceID"].ToString(), IsPublic = rptList.SelectedDataKey["IsPublic"].ToNullableBool().GetValueOrDefault(), FileName = rptList.SelectedDataKey["FileName"].ToString() });
                    dlgEdit.BindAndShow();
                    break;

                case "SaveItem":
                    string exceptionMessage = null;
                    if (string.IsNullOrEmpty(hfSourceId.Value) && // Check if in new file mode (i.e. not edit)
                        Bll.Content.StaticFiles.IsExist(new Bll.Content.ContentType()
                        {
                            Source = UserRole,
                            FileName = txtTitle.Text
                        }))
                    {
                        // If we got here then this is a new file with file name that already exist
                        exceptionMessage = "test";
                        errorActionNotify.SetMessage("File already exist", true);
                        errorActionNotify.DataBind();
                    }

                    if (exceptionMessage == null)
                    {
                        ItemData = Bll.Content.StaticFiles.Load(
                                                new Bll.Content.StaticFiles.SearchFilters()
                                                {
                                                    Source = UserRole,
                                                    SourceID = hfSourceId.Value,
                                                    IsPublic = hfIsPublic.Value.ToNullableBool().GetValueOrDefault(),
                                                    FileName = hfFileName.Value
                                                });

                        if (ItemData == null)
                        {
                            ItemData = new Bll.Content.StaticFiles();

                            ItemData.IsPublic = false;
                            ItemData.Source = UserRole;
                            if (Page is Controls.AccountPage)
                            {
                                //Merchants
                                ItemData.SourceID = (Page as Controls.AccountPage).Account.MerchantID.Value.ToString();
                            }
                            else
                            {
                                //App identity
                                ItemData.SourceID = TemplatePage.ItemID.Value.ToString();
                            }
                            ItemData.Title = txtTitle.Text;
                            ItemData.FileName = txtTitle.Text;
                            ItemData.HtmlData = heHtmlData.Text;

                            ItemData.Save();
                        }
                        else
                        {
                            ItemData.Title = txtTitle.Text;
                            ItemData.HtmlData = heHtmlData.Text;
                            ItemData.Save();
                        }

                        dlgEdit.RegisterHide();
                        rptList.DataSource = Bll.Content.StaticFiles.Search(new Bll.Content.StaticFiles.SearchFilters() { IsPublic = false, Source = UserRole, SourceID = (Page is Controls.AccountPage) && (Page as Controls.AccountPage).Account != null ? (Page as Controls.AccountPage).Account.MerchantID.ToString() : TemplatePage.ItemID.ToString() }, rptList);
                        rptList.DataBind();
                        pnlList.BindAndUpdate();
                    }


                    break;
                case "DeleteItem":
                    ItemData = Bll.Content.StaticFiles.Load(new Bll.Content.StaticFiles.SearchFilters() { Source = UserRole, SourceID = hfSourceId.Value, IsPublic = hfIsPublic.Value.ToNullableBool().GetValueOrDefault(), FileName = hfFileName.Value });

                    if (ItemData != null) ItemData.Delete();
                    dlgEdit.RegisterHide();
                    pnlList.BindAndUpdate();
                    break;
            }
        }
    }
}