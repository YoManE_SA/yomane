﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;

namespace Netpay.Admin.Settlements
{
    public partial class Filter : Controls.TemplateControlBase
    {
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
        {
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Netpay.Bll.Settlements.Settlement.SearchFilters();
            if (apMerchant.Value.ToNullableInt() != null)
            {
                var merchant = Bll.Merchants.Merchant.LoadByAccountId(apMerchant.Value.Value);
                if (merchant != null)
                    sf.MerchantIDs = new List<int>() { merchant.ID };
            }
            if (!string.IsNullOrEmpty(SpecificSettlementID.Text)) sf.SpecificID = int.Parse(SpecificSettlementID.Text);
            sf.ID = rngID.Value;
            sf.Date = rngDate.Value;
            sf.Amount = rngAmount.Value;
            sf.VisibleToMerchant = bddVisibleToMerchant.BoolValue;
            TemplatePage.SetFilter(sf);
        }
    }
}