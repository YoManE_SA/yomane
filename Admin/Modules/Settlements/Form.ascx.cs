﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Settlements
{
	public partial class Form : Controls.TemplateControlBase
	{
		public int PayID { get; set; }
        public Bll.Settlements.Settlement currentSettlement { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.FormButtons.EnableSave = false;
            //TemplatePage.FormButtons.EnableDelete = false;
            TemplatePage.PageController.DeleteItem += PageController_DeleteItem;
            base.OnLoad(e);
        }

        private void PageController_DeleteItem(object sender, EventArgs e)
        {
            PayID = TemplatePage.ItemID.GetValueOrDefault();
            Bll.Totals.CPayment.ClacDeletePayment(PayID, null, false);
            PayID = 0; TemplatePage.ItemID = null;
            TemplatePage.PageController.ListView.BindAndUpdate();
            TemplatePage.PageController.FormView.BindAndUpdate();
        }

        protected override void DataBindChildren()
        {
            PayID = TemplatePage.ItemID.GetValueOrDefault();
            if (PayID != 0)
            {
                currentSettlement = Bll.Settlements.Settlement.Search(new Bll.Settlements.Settlement.SearchFilters() { SpecificID = PayID }, null).SingleOrDefault();
                if (currentSettlement != null)
                {
                    TemplatePage.FormButtons.EnableDelete = currentSettlement.IsDeleteable;
                }
            }

            base.DataBindChildren();
        }

    }
}