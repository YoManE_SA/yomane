﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Settlements.Filter" %>
<admin:FilterSection runat="server" Title="Status">
    <asp:TextBox runat="server" ID="SpecificSettlementID" Visible="false"></asp:TextBox>

    <div class="form-group">
        <asp:Label ID="lblAccount" runat="server" Text="Merchant" />
        <admin:AccountPicker runat="server" ID="apMerchant" UseTargetID="false" LimitToType="Merchant" />
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblWireTotal" runat="server" Text="Amount" />
                <JQ:DecimalRange runat="server" ID="rngAmount" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label2" runat="server" Text="Date" />
                <JQ:DateRange runat="server" ID="rngDate" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label3" runat="server" Text="Settlement #" />
                <JQ:IntRange runat="server" ID="rngID" />
            </div>
        </div>
    </div>
    <div class="form-group">
        <asp:Label ID="Label1" runat="server" Text="Show To Merchant" />
        <netpay:BoolDropDown runat="server" ID="bddVisibleToMerchant" CssClass="form-control" />
    </div>

</admin:FilterSection>
