﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Settlements
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuSettlements, memuUpcoming;
        public override string Name { get { return "Settlement"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return ""; } }

        public Module() : base(Bll.Settlements.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu items 
            menuSettlements = new ApplicationMenu("Settlements", "~/Settlements/0", null, 70);
            memuUpcoming = new ApplicationMenu("Upcoming Settlements", "~/Settlements/Upcoming/0", null, 80);

            //Register routs
            Application.RegisterRoute("Settlements/Upcoming/{*id}", "SettlementsUpcoming", typeof(Controls.DataTablePage), module : this);
            Application.RegisterRoute("Settlements/{*id}", "Settlement", typeof(Controls.DataTablePage), module : this);

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {            
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {            
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Finance", menuSettlements);
            Application.AddMenuItem("Finance", memuUpcoming);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuSettlements.Parent.RemoveChild(menuSettlements);
            memuUpcoming.Parent.RemoveChild(memuUpcoming);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "Settlement")
            {
                page.PageController.LoadItem += PageController_SetLoadItem;
                page.AddControlToList(page.LoadControl("~/Modules/Settlements/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Settlements/Filter.ascx"));
                page.AddControlToForm("Data", page.LoadControl("~/Modules/Settlements/Form.ascx"), this, "", Bll.Settlements.Settlement.SecuredObject);
            }
            else if (page.TemplateName == "SettlementsUpcoming")
            {
                page.PageController.LoadItem += PageController_UpLoadItem;
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Settlements/Upcoming/Filter.ascx"));
                page.AddControlToList(page.LoadControl("~/Modules/Settlements/Upcoming/List.ascx"));
                page.AddControlToForm("Create", page.LoadControl("~/Modules/Settlements/Upcoming/Form.ascx"), this, "", Bll.Settlements.UpcomingSettlement.SecuredObject);
            }
        }

        private void PageController_UpLoadItem(object sender, EventArgs e)
        {
            var page = (sender as Controls.DataPageController).Page as Controls.DataTablePage;
        }

        private void PageController_SetLoadItem(object sender, EventArgs e)
        {
            var page = (sender as Controls.DataPageController).Page as Controls.DataTablePage;
        }
    }
}