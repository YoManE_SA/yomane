﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Settlements.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Settlements">
    <Header></Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList OnRowCommand="rptList_RowCommand" runat="server" ID="rptList" SaveAjaxState="true" DataKeyNames="ID" AutoGenerateColumns="false" BubbleLoadEvent="true" DisableRowSelect="false">
                <Columns>
                    <%-- Settlement --%>
                    <asp:TemplateField HeaderText="Settlement">
                        <ItemTemplate>
                            <asp:HiddenField runat="server" ID="hfID" Value='<%# Eval("ID") %>' />
                            <%# Eval("ID") %><br />
                            <%# Eval("PayDate") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Merchant --%>
                    <asp:TemplateField HeaderText="Merchant">
                        <ItemTemplate>
                            <%# Eval("CompanyID") %><br />
                            <%# GetMerchantName((int)Eval("CompanyID")) %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Settlement Amount --%>
                    <asp:TemplateField HeaderText="Set' Amount">
                        <ItemTemplate>
                            <span <%#(double)Eval("TransPayTotal") < 0? "style=\"color:red;\"" : "" %>><%# new Money((decimal)(double)Eval("TransPayTotal"), (int)Eval("CurrencyID")) %> </span>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Admin Fees --%>
                    <asp:TemplateField HeaderText="Admin Fees">
                        <ItemTemplate>
                            <span <%#(decimal)Eval("TransRollingReserve")<0?"style=\"color:red;\"":"" %>><%# new Money(((decimal?)Eval("TransRollingReserve")).GetValueOrDefault(), (int)Eval("CurrencyID")) %></span><br />
                            <%# new Money((((decimal?)(double)Eval("TransChargeTotal")).GetValueOrDefault()), (int)Eval("CurrencyID")) %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Wire Status --%>
                    <asp:TemplateField HeaderText="Wire Status">
                        <ItemTemplate>
                            <asp:MultiView runat="server" ActiveViewIndex='<%# ((int?)Eval("WireID")).HasValue ? 0 : 1 %>'>
                                <asp:View runat="server">
                                    <asp:PlaceHolder runat="server" Visible='<%# ((int?)Eval("WireID")).HasValue %>'>
                                        <asp:Label runat="server" Text='<%# ((int?)Eval("WireID")).HasValue? GetApprovalStatus(((int?)Eval("WireID")).Value) : "" %>'></asp:Label><br />
                                        <asp:Label runat="server" Text='<%# ((byte?)Eval("WireStatus")).HasValue ? ((Netpay.Bll.Wires.Wire.WireStatus)(((byte?)Eval("WireStatus")).Value)).ToString() : "" %>'></asp:Label><br />
                                        <asp:HyperLink
                                            Style="cursor: pointer"
                                            runat="server"
                                            Text='<%# Eval("WireID") %>'
                                            onclick='<%# "OpenPop(" + "\"" +  string.Format("../Wires/0?ctl00.rngID.From={0}&ctl00.rngID.To={0}&NoLayout=1&Search=1", Eval("WireID")) + "\"" + ",\"Wire\",\"1330\",\"820\" , \"1\"); event.cancelBubble=true; return false;" %>'
                                            Target="_blank" />
                                    </asp:PlaceHolder>
                                </asp:View>
                                <asp:View runat="server">
                                    <asp:Literal runat="server" Text="---"></asp:Literal>
                                </asp:View>
                            </asp:MultiView>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Vat --%>
                    <asp:TemplateField HeaderText="Vat">
                        <ItemTemplate>
                            <%# Eval("VatAmount", "{0:P1}") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Invoice --%>
                    <asp:TemplateField HeaderText="Invoice">
                        <ItemTemplate>
                            <asp:MultiView runat="server" ActiveViewIndex='<%# (int)Eval("InvoiceNumber")==0 ? 0 : 1 %>'>
                                <asp:View runat="server">
                                    <asp:LinkButton
                                        Enabled='<%# IsInvoiceCreatable && ((double)Eval("TransChargeTotal") > 0) %>'
                                        runat="server"
                                        OnClientClick="event.cancelBubble=true;"
                                        Text='<%#(int)Eval("InvoiceNumber") == 0? "Create" : Eval("InvoiceNumber")%>'
                                        CssClass='<%# IsInvoiceCreatable && ((double)Eval("TransChargeTotal") > 0)? "btn btn-primary" : "btn btn-primary disabled" %>'
                                        CommandName="CreateInvoice"
                                        CommandArgument='<%#Eval("ID")%>'></asp:LinkButton>
                                </asp:View>
                                <asp:View runat="server">
                                    <asp:HyperLink
                                        onclick="event.cancelBubble=true;"
                                        runat="server"
                                        Text='<%# Eval("InvoiceNumber") %>'
                                        NavigateUrl='<%# "~/Invoices/0?Search=1&ctl00.SpecificInvoiceDocumentID=" + Eval("InvoiceDocumentID") %>'
                                        Target="_blank">
                                    </asp:HyperLink>
                                </asp:View>
                            </asp:MultiView>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Show to merchant --%>
                    <asp:TemplateField HeaderText="Show To Merchant">
                        <ItemTemplate>
                            <asp:CheckBox onclick="event.cancelBubble=true;" runat="server" Enabled='<%# IsSettlementEditable %>' ID="chkIsVisible" Checked='<%# Eval("IsShow") %>' Text="Change" AutoPostBack="true" OnCheckedChanged="chkIsVisible_CheckedChanged" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd="false" />
    </Footer>
</admin:ListSection>
