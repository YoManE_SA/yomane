﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Settlements.Upcoming.Filter" %>
<admin:FilterSection runat="server" Title="Status">
    <div class="form-group">
        Currency:
        <netpay:CurrencyDropDown CssClass="form-control" runat="server" ID="ddlCurrency" />
    </div>
    <div class="form-group">
        Date:
        <JQ:DatePicker runat="server" ID="ddlToDate" CssClass="form-control" />
    </div>
    <div class="form-group">
        Pay Percent:
        <asp:DropDownList runat="server" ID="ddlPartialPay" CssClass="form-control">
            <asp:ListItem Text="= 100" Selected="True" /> 
            <asp:ListItem Text="< 100" /> 
        </asp:DropDownList>
    </div>
</admin:FilterSection>
<admin:FilterSection runat="server" Title="Merchant unsettled">
    <div class="form-group">
        Choose merchant:
        <admin:AccountPicker UseTargetID="true" ID="apMerchant" runat="server" LimitToType="Merchant" />
    </div>
</admin:FilterSection>
