﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Settlements.Upcoming.List" %>
<admin:ListSection runat="server" Title="Upcoming Settlements">
    <Header></Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList Visible='<%# filters == null || !filters.IsSpecificMerchant.HasValue %>' runat="server" ID="rptList" SaveAjaxState="true" DataKeyNames='<%# DataKeyNames %>' AutoGenerateColumns="false" BubbleLoadEvent="true">
                <Columns>
                    <%-- Merchant --%>
                    <asp:TemplateField HeaderText="Merchant">
                        <ItemTemplate>
                            <%# Eval("MerchantID") %><br />
                            <%# Eval("MerchantName") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Trans to pay , Trans to Hold --%>
                    <asp:TemplateField HeaderText="Trans to Pay/Hold">
                        <ItemTemplate>
                            <%# GetPartialAmountRow(Container.DataItem as Netpay.Bll.Settlements.UpcomingSettlement) %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- 100% to pay , 100% to Hold --%>
                    <asp:TemplateField HeaderText="100% to Pay/Hold">
                        <ItemTemplate>
                            <asp:PlaceHolder runat="server" Visible='<%# Eval("IsPartial") %>'>
                                <%# GetAmountRow(Container.DataItem as Netpay.Bll.Settlements.UpcomingSettlement) %>
                            </asp:PlaceHolder>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Rolling reserve Pay/Hold ratio --%>
                    <asp:TemplateField HeaderText="Rolling reserve<br/> Pay/Hold ratio">
                        <ItemTemplate>
                            <%# GetSecurityDepositRow(Container.DataItem as Netpay.Bll.Settlements.UpcomingSettlement) %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Last Settlement --%>
                    <asp:BoundField DataField="LastSettlement" HeaderText="Last Settlement" />
                </Columns>
            </admin:AdminList>

            <admin:AdminList Visible='<%# filters != null && filters.IsSpecificMerchant.HasValue && filters.IsSpecificMerchant.Value %>' runat="server" ID="rptUnsettled" SaveAjaxState="true" DataKeyNames='<%# DataKeyNames %>' AutoGenerateColumns="false" BubbleLoadEvent="true">
                <Columns>
                    <asp:TemplateField HeaderText="Merchant">
                        <ItemTemplate>
                            <%# Eval("MerchantID") %><br />
                            <%# GetMerchantName((int)Eval("MerchantID")) %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Currency">
                        <ItemTemplate>
                           <%# GetCurrencyIso((int)Eval("Currency")) %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Trans' Count">
                        <ItemTemplate>
                            <%# Eval("numTransToPay") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd="false" Visible='<%# filters == null || !filters.IsSpecificMerchant.HasValue %>'/>
        <admin:DataButtons runat="server" PagedControlID="rptUnsettled" EnableExport="true" EnableAdd="false" Visible='<%# filters != null && filters.IsSpecificMerchant.HasValue && filters.IsSpecificMerchant.Value %>' />
    </Footer>
</admin:ListSection>
