﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Settlements.Upcoming
{
    public partial class Filter : Controls.TemplateControlBase
    {
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
            base.OnLoad(e);
        }

        private void PageController_Search(object sender, EventArgs e)
        {
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Netpay.Bll.Settlements.UpcomingSettlement.SearchFilters();

            if (apMerchant.Value.HasValue)
            {
                sf.IsSpecificMerchant = true;
                sf.merchantId = apMerchant.Value.Value;
            }
            else
            {
                sf.currency = ddlCurrency.Value.ToNullableEnumByValue<CommonTypes.Currency>().GetValueOrDefault();
                sf.toDate = ddlToDate.Value.GetValueOrDefault();
                if (sf.toDate < DateTime.Now) sf.toDate = DateTime.Now;
                sf.partialPay = ddlPartialPay.SelectedIndex == 1;
            }
            TemplatePage.SetFilter(sf);
        }
    }
}