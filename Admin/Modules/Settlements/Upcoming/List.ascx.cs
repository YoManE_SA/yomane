﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Settlements.Upcoming
{
    public partial class List : Controls.TemplateControlBase
    {
        public string[] DataKeyNames = { "MerchantID", "Currency" };

        public Bll.Settlements.UpcomingSettlement.SearchFilters filters 
                => TemplatePage.GetFilter<Bll.Settlements.UpcomingSettlement.SearchFilters>();
       

        public Dictionary<int, string> merchantNames { get; set; }
        
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.FormButtons.EnableSave = false;
            TemplatePage.FormButtons.EnableDelete = false;
            rptList.DataBinding += List_DataBinding;
            rptUnsettled.DataBinding += List_DataBinding;
            base.OnLoad(e);
        }

        private void List_DataBinding(object sender, EventArgs e)
        {
            var sf = TemplatePage.GetFilter<Bll.Settlements.UpcomingSettlement.SearchFilters>();
            if (sf != null && sf.IsSpecificMerchant.HasValue && sf.IsSpecificMerchant.Value)
            {
                var list = Bll.Settlements.UpcomingSettlement.Search(sf, rptUnsettled);
                merchantNames = Bll.Merchants.Merchant.GetMerchantNamesByMerchantId(list.Select(x => x.MerchantID).ToList());
                
                rptUnsettled.DataSource = list;
            }
            else
            {
                if (sf == null) sf = new Bll.Settlements.UpcomingSettlement.SearchFilters() { toDate = DateTime.Now };
                rptList.DataSource = Bll.Settlements.UpcomingSettlement.Search(sf, rptList);
            }
        }

        public string GetPartialAmountRow(Bll.Settlements.UpcomingSettlement us)
        {
            if (!us.IsPartial) return GetAmountRow(us);
            var PayPercent = us.PayPercent / 100.0m;
            var sb = new System.Text.StringBuilder();
            sb.AppendFormat("<b>{0}</b> ({1:0.0}%)", new Bll.Money(us.PayAmount * PayPercent, us.Currency).ToIsoString(), us.PayPercent);
            sb.AppendFormat("<br/>{0}", new Bll.Money((us.FullAmount - us.PayAmount) * PayPercent, us.Currency).ToIsoString());
            return sb.ToString();
        }

        public string GetAmountRow(Bll.Settlements.UpcomingSettlement us)
        {
            var sb = new System.Text.StringBuilder();
            sb.AppendFormat("<b>{0}</b> ({1:0})", new Bll.Money(us.PayAmount, us.Currency).ToIsoString(), us.numTransToPay);
            sb.AppendFormat("<br/><b>{0}</b> ({1:0})", new Bll.Money(us.FullAmount - us.PayAmount, us.Currency).ToIsoString(), us.FullCount - us.numTransToPay);
            return sb.ToString();
        }

        public string GetSecurityDepositRow(Bll.Settlements.UpcomingSettlement us)
        {
            var sb = new System.Text.StringBuilder();
            decimal percent = 0;
            sb.AppendFormat("{0:0.0} % / {1}<br/>", us.SecurityDeposit, new Bll.Money(us.RollingReserveSum, us.Currency).ToIsoString());
            if (us.PayAmount != 0 && (us.FullAmount + us.RollingReserveSum) != 0) percent = (us.PayAmount / (us.FullAmount + us.RollingReserveSum)) * 100;
            sb.AppendFormat("<b>{0:0.0} %</b>", percent);
            return sb.ToString();
        }

        public string GetCurrencyIso(int id)
        {
            return Bll.Currency.GetIsoCode((CommonTypes.Currency)id);
        }

        public string GetMerchantName(int id)
        {
            string name;
            merchantNames.TryGetValue(id, out name);
            return string.IsNullOrEmpty(name)? "" : name;
        }
    }
}