﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Admin.Modules.Settlements.Upcoming
{
    public partial class Form : Controls.TemplateControlBase
    {
        public int merchantID;
        public string currencyIso;
        public List<Bll.Totals.CPayment> Results;
        private CommonTypes.Currency CurrencyID { get { return ddlCurrency.Value.ToNullableEnumByValue<CommonTypes.Currency>().GetValueOrDefault(); } }
        public Bll.Totals.CPayment.CreateSettings Settings;

        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.LoadItem += PageController_LoadItem;
            base.OnLoad(e);
        }

        private void PageController_LoadItem(object sender, EventArgs e)
        {
            merchantID = TemplatePage.PageController.DataKey["MerchantID"].ToNullableInt().GetValueOrDefault();
            ddlCurrency.Value = TemplatePage.PageController.DataKey["Currency"].ToString();
        }

        protected void ddlCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            TemplatePage.PageController.FormView.BindAndUpdate();
        }

        protected override void DataBindChildren()
        {
            fsResult.Visible = !(fsCreate.Visible = true);
            Settings = Bll.Totals.CPayment.CreateSettings.DefaultForMerchant(merchantID, CurrencyID);
            ddlExistingPayment.DataSource = (from p in DataContext.Reader.tblTransactionPays join w in DataContext.Reader.Wires on p.id equals w.MerchantSettlement_id where p.CompanyID == merchantID && !p.isBillingPrintOriginal && p.Currency == (int)CurrencyID && w.WireStatus == (byte)Bll.Wires.Wire.WireStatus.Pending orderby p.PayDate descending select new { ID = p.id, Value = p.PayDate }).ToList();
            ddlPaymentDate.DataSource = Bll.Merchants.Merchant.GetMerchantNextSettlementDates(merchantID, DateTime.Now);
            base.DataBindChildren();
            if (ddlExistingPayment.Items.Count > 0 && ddlExistingPayment.SelectedIndex == -1) ddlExistingPayment.SelectedIndex = 0;
            if (ddlPaymentDate.Items.Count > 0 && ddlPaymentDate.SelectedIndex == -1) ddlPaymentDate.SelectedIndex = 0;
        }

        public void UpdatePayment(Bll.Totals.CPayment payment, DateTime payDate, Bll.Totals.CPayment.CreateSettings settings, bool bCreateWire)
        {
            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
            //Logger.Log(LogSeverity.Info, LogTag.Wires, "Inside Form.ascx.cs.UpdatePayment function");
            if (payment.UpdatePayment(payDate, settings, true))
            {
                var setSetting = Bll.Settlements.MerchantSettings.LoadForMerchant(merchantID, CurrencyID);
                if (setSetting == null) setSetting = new Bll.Settlements.MerchantSettings(merchantID, CurrencyID) { MinSettlementAmount = Decimal.MinValue };
                if (setSetting.MinSettlementAmount > payment.Total)
                {
                    payment.Delete();
                }
                else
                {
                    if (bCreateWire)
                    {
                        var account = Bll.Accounts.Account.LoadAccount(Bll.Accounts.AccountType.Merchant, merchantID);
                        var wire = new Bll.Wires.Wire(account.AccountID, payment.PayID);
                        //nReceiveCurrency = ExecScalar("Select dbo.GetMerchantPayoutCurrency(" & nCompanyID & ", " & nCurrency & ")", nCurrency)
                        //wire.BankAccount = Bll.Accounts.BankAccount.LoadForAccount(account.AccountID).FirstOrDefault();
                        var wireTotal = setSetting.GetWireAmount(payment);
                        wire.SetAmount(CurrencyID, wireTotal);
                    }
                }
            }
            Results.Add(payment);
        }

        protected void Create_Click(object sender, EventArgs e)
        {
            Infrastructure.Logger.Log(LogSeverity.Info, LogTag.None, "Create button clicked in upcoming settlement.");

            if (Netpay.Infrastructure.Security.Login.Current != null && Netpay.Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Settlements.UpcomingSettlement.SecuredObject, PermissionValue.Add);

            int? payID = null;
            merchantID = TemplatePage.PageController.DataKey["MerchantID"].ToNullableInt().GetValueOrDefault();
            Server.ScriptTimeout = 600;
            Results = new List<Bll.Totals.CPayment>();
            if (Settings == null) Settings = new Bll.Totals.CPayment.CreateSettings();

            Settings.TransPD = rngMerchantPD.Value;
            Settings.TransDate = rngTransDate.Value;

            Settings.IncProcessTrans = chkProcessTrans.Checked;
            Settings.IncAdminTrans = chkAdminTrans.Checked;

            Settings.IncNormalCharge = chkIncCharge.Checked;
            Settings.IncRefund = chkIncRefund.Checked;

            Settings.IncRR = chkIncRR.Checked;
            Settings.RetRR = chkRetRR.Checked;

            Settings.IncApprovalFee = chkApprovalFee.Checked;
            Settings.AutoChb = chkAutoChb.Checked;

            Settings.IncFailFee = chkFailFee.Checked;
            Settings.AutoInstallements = chkInstallements.Checked;

            Settings.IncStorageFee = chkStorageFee.Checked;
            Settings.IncTestTransactions = chkTestTransactions.Checked;
            Settings.ForceEpaPaid = !chkIgnoreEpaPaid.Checked;
            Settings.PaymentPerPm = rbPaymentPerPM.Checked;

            DateTime payDate = ddlPaymentDate.Value.ToNullableDate().GetValueOrDefault().Date;
            if (rdExistingPayment.Checked) payID = ddlExistingPayment.Value.ToNullableInt();

            if (rbPaymentPerPM.Checked)
            {
                if (payID.GetValueOrDefault() != 0) throw new Exception("PayCreate With type=ExecPerPM - can't be used to update existing payment");
                var pms = (from t in DataContext.Reader.tblCompanyTransPasses where t.companyID == merchantID && t.Currency == (int)CurrencyID && t.PayID.Contains(";0;") && t.PaymentMethod > (int)CommonTypes.PaymentMethodEnum.MaxInternal group t by t.PaymentMethod into g select g.Key).ToList();
                foreach (var pm in pms)
                {
                    var payment = Bll.Totals.CPayment.Create(merchantID, (int)CurrencyID);
                    Settings.PaymentMethods = new List<CommonTypes.PaymentMethodEnum>() { (CommonTypes.PaymentMethodEnum)pm };
                    UpdatePayment(payment, payDate, Settings, true);
                }
            }
            else
            {
                try
                {
                    var ret = payID.HasValue ? Bll.Totals.CPayment.Load(payID.Value, merchantID) : Bll.Totals.CPayment.Create(merchantID, (int)CurrencyID);
                    //if(TestNumVar(Request("MaxTransAmount"), 0, -1, 0) <> 0) {
                    //  var nTransID = xPayment.GetTransIDUpTo(xWhere, TestNumVar(Request("MaxTransAmount"), 0, -1, 0));
                    //  if (nTransID = "") nTransID = -1;
                    //  sWhere += IIF(xWhere <> "", " And ", "") & " (tblCompanyTransPass.ID IN(" & nTransID & ") Or DeniedStatus<>0)";
                    //}
                    UpdatePayment(ret, payDate, Settings, !payID.HasValue);
                }
                catch(Exception ex)
                {
                    Logger.Log(ex);
                }
            }
            rptPayments.DataBind();
            fsResult.Visible = !(fsCreate.Visible = false);
            TemplatePage.PageController.FormView.Update();
            TemplatePage.PageController.ListView.BindAndUpdate();
        }
    }
}