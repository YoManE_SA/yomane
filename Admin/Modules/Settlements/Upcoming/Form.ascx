﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Form.ascx.cs" Inherits="Netpay.Admin.Modules.Settlements.Upcoming.Form" %>
<admin:FormSections runat="server" ID="fsCreate" Title="Create Settlement">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                Currency:
           
                <netpay:CurrencyDropDown runat="server" CssClass="form-control" ID="ddlCurrency" AutoPostBack="true" OnSelectedIndexChanged="ddlCurrency_SelectedIndexChanged" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                MerchantPD:
                <JQ:DateRange runat="server" ID="rngMerchantPD" Value='<%# Settings.TransPD %>' />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                Trans Date:            
                <JQ:DateRange runat="server" ID="rngTransDate" Value='<%# Settings.TransDate %>' />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <h4>Payment Method</h4>
                <ul class="list-group">
                    <li class="list-group-item">
                        <asp:CheckBox runat="server" ID="chkProcessTrans" Text="Process Trans" Checked='<%# Settings.IncProcessTrans %>' /></li>
                    <li class="list-group-item">
                        <asp:CheckBox runat="server" ID="chkAdminTrans" Text="Admin Trans" Checked='<%# Settings.IncAdminTrans %>' /></li>
                </ul>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="form-group">
                <h4>Credit Type</h4>
                <ul class="list-group">
                    <li class="list-group-item">
                        <asp:CheckBox runat="server" ID="chkIncCharge" Text="Charge" Checked='<%# Settings.IncNormalCharge %>' /></li>
                    <li class="list-group-item">
                        <asp:CheckBox runat="server" ID="chkIncRefund" Text="Refund" Checked='<%# Settings.IncRefund %>' /></li>
                </ul>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="form-group">
                <h4>Rolling Reserve</h4>
                <ul class="list-group">
                    <li class="list-group-item">
                        <asp:CheckBox runat="server" ID="chkRetRR" Text="Release" Checked='<%# Settings.RetRR %>' /></li>
                    <li class="list-group-item">
                        <asp:CheckBox runat="server" ID="chkIncRR" Text="Reserve Hold" Checked='<%# Settings.IncRR %>' /></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <div class="form-group">
                <h4>include in pay</h4>
                <ul class="list-group">
                    <li class="list-group-item">
                        <asp:CheckBox runat="server" ID="chkApprovalFee" Text="Approval Fee" Checked='<%# Settings.IncApprovalFee %>' /></li>
                    <li class="list-group-item">
                        <asp:CheckBox runat="server" ID="chkAutoChb" Text="Auto Debit CHB" Checked='<%# Settings.AutoChb %>' /></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <ul class="list-group">
                    <li class="list-group-item">
                        <asp:CheckBox runat="server" ID="chkFailFee" Text="Fail Fee" Checked='<%# Settings.IncFailFee %>' /></li>
                    <li class="list-group-item">
                        <asp:CheckBox runat="server" ID="chkInstallements" Text="Next Installments" Checked='<%# Settings.AutoInstallements %>' /></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <ul class="list-group">
                    <li class="list-group-item">
                        <asp:CheckBox runat="server" ID="chkStorageFee" Text="Storage Fee" Checked='<%# Settings.IncStorageFee %>' /></li>
                    <li class="list-group-item">
                        <asp:CheckBox runat="server" ID="chkTestTransactions" Text="Test Transactions" Checked='<%# Settings.IncTestTransactions %>' /></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <ul class="list-group">
                    <li class="list-group-item">
                        <asp:CheckBox runat="server" ID="chkIgnoreEpaPaid" Text="Include Overdue bank payments" Checked='<%# !Settings.ForceEpaPaid %>' /></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <h4>Create/Update</h4>
                <ul class="list-group">
                    <li class="list-group-item">
                        <asp:RadioButton runat="server" ID="rdNewPayment" Text="New Payment" Checked="true" GroupName="NewPay" /></li>
                    <li class="list-group-item">
                        <asp:RadioButton runat="server" ID="rdExistingPayment" Text="Existing Payment" GroupName="NewPay" /></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                Payment Date
               
                <netpay:DropDownBase runat="server" CssClass="form-control" ID="ddlPaymentDate" />

            </div>

        </div>
        <div class="col-lg-6">
            <div class="form-group">
                Existing Payment
               
                <netpay:DropDownBase runat="server" CssClass="form-control" ID="ddlExistingPayment" DataTextField="Value" DataValueField="ID" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <h4>Multiple Payments:</h4>
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:RadioButton runat="server" ID="rbSinglePayment" Text="Single Payment" Checked="true" GroupName="MultiPay" /></li>
                <li class="list-group-item">
                    <asp:RadioButton runat="server" ID="rbPaymentPerPM" Text="Payment Per PM" GroupName="MultiPay" /></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <asp:Button CssClass="btn btn-primary pull-right" runat="server" Text="Create" ID="btnCreate" OnCommand="Create_Click" />
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" ID="fsResult" Title="Result">
    <asp:Repeater runat="server" ID="rptPayments" DataSource='<%# Results %>'>
        <ItemTemplate>
            Settlement #<b><%# Eval("PayID") %> - <%# Eval("CompanyName") %></b>
            <asp:Label runat="server" Visible='<%# (int)Eval("PayID") == 0 %>' ForeColor="Red" Text="DELETED" /><br />
            <div style="padding: 5px;">
                <netpay:Totals runat="server" Payment='<%# Container.DataItem %>' HideTitle="true" />
            </div>
            <hr />
        </ItemTemplate>
    </asp:Repeater>
</admin:FormSections>
