﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Settlements
{
    public partial class List : Controls.TemplateControlBase
    {
        public bool IsSettlementEditable { get; set; }
        public bool IsInvoiceCreatable { get; set; }

        private Dictionary<int, string> merchantNames;
        protected override void OnLoad(EventArgs e)
        {
            rptList.DataBinding += delegate
            {
                var items = Bll.Settlements.Settlement.Search(TemplatePage.GetFilter<Bll.Settlements.Settlement.SearchFilters>(), rptList);
                rptList.DataSource = items;
                merchantNames = Bll.Merchants.Merchant.GetMerchantNamesByMerchantId(items.Select(v => v.CompanyID).ToList());
            };

            IsSettlementEditable = Bll.Settlements.Settlement.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Edit);
            IsInvoiceCreatable = Bll.Invoices.InvoiceDocument.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Add);

            base.OnLoad(e);
        }

        protected string GetMerchantName(int merchantId)
        {
            string ret;
            if (!merchantNames.TryGetValue(merchantId, out ret)) return null;
            return ret;
        }

        protected string GetApprovalStatus(int wireid)
        {
            if (wireid <= 0) return "";

            var wire = Bll.Wires.Wire.Load(wireid);
            if (wire != null)
                return wire.ApprovalStatus.ToString();
            else
                return ""; 
        }

        protected string GetWireStatus(int settlementId)
        {
            if (settlementId <= 0) return null;

            var sf = new Bll.Wires.Wire.SearchFilters() { MerchantSettlementID = settlementId };
            Bll.Wires.Wire currentWire = Bll.Wires.Wire.Search(sf, null).SingleOrDefault();

            if (currentWire == null)
            {
                return "---";
            }
            else
            {
                return currentWire.ApprovalStatus.ToString() + "<br>" + currentWire.Status.ToString();
            }
        }

        protected string GetWireID(int settlementId)
        {
            if (settlementId <= 0) return null;

            var sf = new Bll.Wires.Wire.SearchFilters() { MerchantSettlementID = settlementId };
            Bll.Wires.Wire currentWire = Bll.Wires.Wire.Search(sf, null).SingleOrDefault();

            if (currentWire == null)
            {
                return null;
            }
            else
            {
                return currentWire.ID.ToString();
            }
        }

        protected void chkIsVisible_CheckedChanged(object sender, EventArgs e)
        {
            rptList_RowCommand(rptList, new GridViewCommandEventArgs(sender, new CommandEventArgs("VisibleToMerchant", null)));
        }

        protected void rptList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int itemId = e.CommandArgument != null ? int.Parse(e.CommandArgument.ToString()) : 0;

            switch (e.CommandName)
            {
                case "CreateInvoice":
                    //The itemId came from the e.CommandArgument
                    //Bll.Invoices.Local.CreateInvoice(itemId);
                    try
                    {
                        Bll.Invoices.Local.CreateInvoiceForSettlement(itemId, Web.WebUtils.IsLoggedin ? Web.WebUtils.LoggedUser.UserName : "");
                    }
                    catch (Exception ex)
                    {
                        ((sender as Admin.Controls.AdminList).Page as Controls.DataTablePage).SetListViewMessage(ex.Message, true);
                    }

                    //Get to the PageController and update the list view.
                    ((e.CommandSource as LinkButton).NamingContainer.Page as Controls.DataTablePage).PageController.ListView.BindAndUpdate();
                    break;

                case "VisibleToMerchant":
                    //Get the clicked checkbox.
                    var chkIsVisible = (e.CommandSource as CheckBox);

                    //Get the itemId from the GridViewRow 
                    itemId = int.Parse(((chkIsVisible.NamingContainer as GridViewRow).FindControl("hfID") as HiddenField).Value);

                    //Set Visible
                    Bll.Settlements.Settlement.SetVisible(itemId, chkIsVisible.Checked);

                    //Update the list view
                    (chkIsVisible.NamingContainer.Page as Controls.DataTablePage).PageController.ListView.BindAndUpdate();
                    break;
            }
        }
    }
}