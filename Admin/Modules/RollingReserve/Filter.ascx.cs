﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.RollingReserve
{
    public partial class Filter : Controls.TemplateControlBase
    {
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
        {
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Bll.Transactions.Transaction.SearchFilters();
            if (apAccount.Value != null)
            {
                var merchant = Bll.Merchants.Merchant.LoadByAccountId(apAccount.Value.Value);
                if (merchant != null)
                    sf.merchantIDs = new List<int>() { merchant.ID };
            }
            sf.paymentMethodID = (int)CommonTypes.PaymentMethodEnum.RollingReserve;
            sf.date = rngCreateDate.Value;
            sf.amount = rngAmount.Value;
            sf.currencyID = ddlCurrency.Value.ToNullableInt();

            sf.LoadOptionsFilter = new Bll.Transactions.Transaction.LoadOptions()
            {
                dataStores = new List<TransactionStatus> { TransactionStatus.Captured },
                loadPaymentData = false,
                loadMerchant = false
            };

            TemplatePage.SetFilter(sf);
        }
    }
}