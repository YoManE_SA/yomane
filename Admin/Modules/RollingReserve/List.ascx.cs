﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.RollingReserve
{
	public partial class List : Controls.TemplateControlBase
	{
		protected override void OnLoad(EventArgs e)
		{
			rptList.DataBinding += List_DataBinding;
			base.OnLoad(e);
		}

		private void List_DataBinding(object sender, EventArgs e)
		{
            //Check for permissions of rolling reserve and transactions secured objects.
			rptList.DataSource = Bll.Transactions.Transaction.Search(TemplatePage.GetFilter<Bll.Transactions.Transaction.SearchFilters>(), rptList);
		}
	}
}