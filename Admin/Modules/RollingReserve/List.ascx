﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.RollingReserve.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Rolling Reserve">
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" ID="rptList" SaveAjaxState="true" DataKeyNames="ID" AutoGenerateColumns="false" BubbleLoadEvent="false" DisableRowSelect="true">
                <Columns>
                    <asp:TemplateField HeaderText="ID" SortExpression="ID">
                        <ItemTemplate>
                            <%# Eval("ID") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Merchant" DataField="MerchantID" SortExpression="MerchantID" />
                    <asp:BoundField HeaderText="Date" DataField="InsertDate" SortExpression="InsertDate" />
                    <asp:BoundField HeaderText="ID" DataField="ID" SortExpression="ID" />
                    <asp:BoundField HeaderText="Settlement" DataField="PrimaryPaymentID" SortExpression="PrimaryPaymentID" />
                    <asp:BoundField HeaderText="Currency" DataField="CurrencyIsoCode" SortExpression="CurrencyIsoCode" />
                    <asp:BoundField HeaderText="Amount" DataField="Amount" />
                    <asp:BoundField HeaderText="Comment" DataField="Comment" />
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" />
    </Footer>
</admin:ListSection>
