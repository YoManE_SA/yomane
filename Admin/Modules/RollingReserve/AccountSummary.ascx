﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountSummary.ascx.cs" Inherits="Netpay.Admin.RollingReserve.AccountSummary" %>
<admin:FormSections runat="server" Title="Rolling Reserve">
    <NP:PagedRepeater runat="server" ID="rptRolling">
        <HeaderTemplate>
             <table class="table table-hover table-customize">
                <thead>
                    <th>Currency</th>
                    <th>Balance</th>
                </thead>
        </HeaderTemplate>
        <ItemTemplate>
            <tbody>
                <td><%# Currency.Get((int)Eval("Currency")).IsoCode %></td>
                <td><%# new Money((decimal)Eval("Balance"), (int) Eval("Currency")).ToIsoString() %></td>     
            </tbody>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        <EmptyContainer>
            <div class="alert alert-info"><strong>Info!</strong> No record information</div>
        </EmptyContainer>
    </NP:PagedRepeater>
</admin:FormSections>
