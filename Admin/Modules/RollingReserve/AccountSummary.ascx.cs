﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.RollingReserve
{
	public partial class AccountSummary : Controls.TemplateControlBase
	{
		public Netpay.CommonTypes.Currency SelectedCurrency { get; set; }
		public int? MerchantID { get; set; }
		protected override void DataBindChildren()
		{
			switch(TemplatePage.TemplateName){
			case "Wire":
				if (TemplatePage.GetItemData<Bll.Wires.Wire>() != null)
					MerchantID = TemplatePage.GetItemData<Bll.Wires.Wire>().Account.MerchantID;
				break;
			case "Merchant":
				if(TemplatePage.GetItemData<Bll.Accounts.Account>() != null)
					MerchantID = TemplatePage.GetItemData<Bll.Accounts.Account>().MerchantID;
				break;
			}
			rptRolling.DataSource = Netpay.Bll.Merchants.RollingReserve.GetMerchantStatus(MerchantID.GetValueOrDefault());
			base.DataBindChildren();
		}
	}
}