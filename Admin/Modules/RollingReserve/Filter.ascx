﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.RollingReserve.Filter" %>
<admin:FilterSection runat="server" Title="General">
    <div class="form-group">
        <asp:Label ID="lblMerchant" runat="server" Text="Merchant" />
        <admin:AccountPicker runat="server" ID="apAccount" UseTargetID="false" LimitToType="Merchant" />
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblInsertDate" runat="server" Text="Create Date" />
                <JQ:DateRange runat="server" ID="rngCreateDate" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label3" runat="server" Text="Release Date" />
                <JQ:DateRange runat="server" ID="rngReleaseDate" />
            </div>
        </div>
    </div>
    <ul class="list-group">
        <li class="list-group-item">
            <asp:CheckBox runat="server" ID="chkReserveHold" Text="ReserveHold" /></li>
        <li class="list-group-item">
            <ul class="child-list-group">
                <li>
                    <asp:CheckBox runat="server" ID="CheckBox1" Text="Only overdue holdings" /></li>
            </ul>
        </li>

    </ul>
    <ul class="list-group">
        <li class="list-group-item">
            <asp:CheckBox runat="server" ID="chkRelease" Text="Release" /></li>
    </ul>

    <div class="row">
        <div class="col-lg-3">
            <div class="form-group">
                <asp:Label ID="Label1" runat="server" Text="Currency" />
                <netpay:CurrencyDropDown runat="server" CssClass="form-control" ID="ddlCurrency" />
            </div>
        </div>
        <div class="col-lg-9">
       
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                     <asp:Label ID="Label2" runat="server" Text="Amount" />
                <JQ:DecimalRange runat="server" ID="rngAmount" />
            </div>
        </div>
    </div>
</admin:FilterSection>
