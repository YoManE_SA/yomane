﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.RollingReserve
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu mainMenuItem;
        public override string Name { get { return "RollingReserve"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return ""; } }

        public Module() : base(Bll.Merchants.RollingReserves.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu item
            mainMenuItem = new ApplicationMenu("RollingReserve", "~/RollingReserve/0", "fa-group", 150);

            //Register route
            Application.RegisterRoute("RollingReserve/{id}", "RollingReserve", typeof(Controls.DataTablePage), module:this);

            //Register event
            Application.InitDataTablePage += application_InitTemplatePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {            
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {            
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Finance", mainMenuItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            mainMenuItem.Parent.RemoveChild(mainMenuItem);
            base.OnUninstallAdmin(e);
        }

        private void application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "RollingReserve")
            {
                page.AddControlToList(page.LoadControl("~/Modules/RollingReserve/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/RollingReserve/Filter.ascx"));
            }
            else if (page.TemplateName == "Wire" && (page.ViewName == null))
            {
                page.AddControlToForm("Data", page.LoadControl("~/Modules/RollingReserve/AccountSummary.ascx"), this, "", new Controls.SecuredObjectSelector(Bll.Wires.Wire.SecuredObject, Netpay.Bll.Merchants.RollingReserve.SecuredObject).GetActivObject);
            }
            else if (page.TemplateName == "Merchant" && (page.ViewName == null))
            {
                page.AddControlToForm("Summary", page.LoadControl("~/Modules/RollingReserve/AccountSummary.ascx"), this, "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject, Bll.Merchants.RollingReserve.SecuredObject).GetActivObject);
            }
        }
    }
}