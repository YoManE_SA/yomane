﻿<%@ Page Title="Product Category" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductCategory.aspx.cs" Inherits="Netpay.Admin.Modules.DataManager.ProductCategory" %>

<asp:Content ContentPlaceHolderID="BodyContent" runat="server">
    <NP:UpdatePanel runat="server" ID="upEventTypeList" RenderMode="Block" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>
            <admin:ListSection runat="server" Title="Product Category">
                <Header>
                    <admin:DataButtons  runat="server" EnableSave="false" EnableNew="true" OnNew="MakeNewCategory" />
                    <netpay:ActionNotify runat="server" ID="ListMessage" />
                </Header>
                <Body>
                    <table class="table table-striped">
                        <netpay:DynamicRepeater runat="server" ID="rptList" OnItemCommand="rptList_ItemCommand">
                            <HeaderTemplate>
                                <thead>
                                    <tr>
                                        <th>Product Category ID</th>
                                        <th>Parent ID</th>
                                        <th>Name</th>
                                        <th>Sort Order</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:MultiView ID="MultiViewSection" runat="server" ActiveViewIndex='<%# (short)Eval("ID")==EditableId ? 0 : 1 %>'>
                                    <asp:View runat="server" ID="zero">
                                        <tr>
                                            <td>
                                                <asp:Literal runat="server" ID="txtID" Text='<%# Eval("ID") %>' />
                                            </td>
                                            <td>
                                                <asp:TextBox CssClass="form-control" runat="server" ID="txtParentID" Text='<%# Eval("ParentID") %>' />
                                            </td> 
                                            <td>
                                                <asp:TextBox CssClass="form-control" runat="server" ID="txtName" Text='<%# Eval("Name")  %>' />
                                            </td>
                                            <td>
                                                <asp:TextBox CssClass="form-control" runat="server"  ID="txtSortOrder" Text='<%# Eval("SortOrder")  %>' />
                                            </td>
                                            <td>
                                                <asp:Button ID="CancelButton" CssClass="btn btn-default" runat="server" Text="Cancel" CommandName="Cancel" />
                                                <asp:Button ID="UpdateButton" CssClass="btn btn-success" runat="server" Text="Update" CommandName="Update" CommandArgument='<%# Eval("ID") %>' ClientIDMode="Static" />
                                            </td>
                                        </tr>
                                    </asp:View>

                                    <asp:View runat="server" ID="one">
                                        <tr>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("ID") %>' />
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("ParentID")==null? "Parent" : Eval("ParentID") %>' />
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Name") %>' />
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("SortOrder") %>' />
                                            </td>
                                            <td>
                                                <asp:Button runat="server" CssClass="btn btn-primary" Text="Edit" CommandArgument='<%# Eval("ID") %>' CommandName="Edit" />
                                            </td>
                                        </tr>
                                    </asp:View>
                                </asp:MultiView>
                            </ItemTemplate>
                        </netpay:DynamicRepeater>
                    </table>
                </Body>
                <Footer>
                </Footer>
            </admin:ListSection>
        </ContentTemplate>
    </NP:UpdatePanel>
</asp:Content>


