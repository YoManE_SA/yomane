﻿using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Modules.DataManager
{
    public partial class ProductCategory : Controls.ModulePage
    {
        public ProductCategory() : base(ProductCategoryModule.Module.ModuleName) { }

        public int EditableId { get; set; }

        public bool Check { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            EditableId = -1;
            rptList.DataBinding += List_DataBinding;

            base.OnLoad(e);
        }

        protected void List_DataBinding(object sender, EventArgs e)
        {
            var items = Netpay.Bll.Shop.Products.Category.GetAllCategories;
            if (ViewState["IsNewItemAdded"] != null && (bool)ViewState["IsNewItemAdded"])
            {
                items.Add(new Bll.Shop.Products.Category());
                items = items.OrderBy(x => x.ID).ToList(); // Move the new item that has id=0 to the begining.
            }

            rptList.DataSource = items;
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            switch (e.CommandName)
            {
                case "Update":

                    //Check if the Name column is not empty , if it's empty , don't save.
                    if (string.IsNullOrEmpty((e.Item.FindControl("txtName") as TextBox).Text))
                    {
                        ListMessage.SetMessage("You can't save without a name", true);
                        upEventTypeList.Update();

                        return;
                    }

                    //Declare variables
                    int id;
                    Bll.Shop.Products.Category productCategoryItem;

                    //Get the selected item id
                    Check = int.TryParse(e.CommandArgument.ToString(), out id);
                    if (Check)
                    {
                        bool checkSortOrder = false;
                        bool checkParentID = false;
                        short? newParentID = null;
                        byte? newSortOrder = null;
                        string newName = "";
                        if (id == 0) // Means that its a new item.
                        {
                            productCategoryItem = new Bll.Shop.Products.Category();
                        }
                        else
                        {
                            productCategoryItem = Bll.Shop.Products.Category.Get(id);
                        }

                        //Get newName value
                        {
                            newName = ((e.Item.FindControl("txtName") as TextBox).Text);
                        }
                                               

                        if (!string.IsNullOrEmpty((e.Item.FindControl("txtParentID") as TextBox).Text))
                        {
                            short tmpNewParentID;
                            checkParentID = short.TryParse(((e.Item.FindControl("txtParentID") as TextBox).Text), out tmpNewParentID);

                            if (checkParentID)
                            {
                                newParentID = tmpNewParentID;
                            }
                            else
                            {
                                ListMessage.SetMessage("Parent ID value is not valid", true);
                                upEventTypeList.Update();
                                return;
                            }
                        }

                        if (!string.IsNullOrEmpty((e.Item.FindControl("txtSortOrder") as TextBox).Text))
                        {
                            byte tmpNewSortOrder;
                            checkSortOrder = byte.TryParse(((e.Item.FindControl("txtSortOrder") as TextBox).Text), out tmpNewSortOrder);

                            if (checkSortOrder)
                            {
                                newSortOrder = tmpNewSortOrder;
                            }
                            else
                            {
                                ListMessage.SetMessage("Sort Order value is not valid", true);
                                upEventTypeList.Update();
                                return;
                            }
                        }
                                                
                        //Insert the newName to the productTypeItem ans Save.
                        productCategoryItem.Name = newName;
                        productCategoryItem.ParentID = newParentID;
                        productCategoryItem.SortOrder = newSortOrder;
                        productCategoryItem.Save(true);

                        //after save it's not a new item anymore.
                        ViewState["IsNewItemAdded"] = false;

                        //Refresh the list
                        EditableId = -1;
                        upEventTypeList.DataBind();
                        upEventTypeList.Update();
                    }
                    break;

                case "Cancel":
                    ViewState["IsNewItemAdded"] = false;

                    EditableId = -1;
                    upEventTypeList.DataBind();
                    upEventTypeList.Update();
                    break;

                case "Edit":
                    if (Netpay.Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                        ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Shop.Products.Category.SecuredObject, PermissionValue.Edit);

                    int editid;
                    bool check = int.TryParse(e.CommandArgument.ToString(), out editid);
                    if (check) EditableId = editid;

                    upEventTypeList.DataBind();
                    upEventTypeList.Update();
                    break;
            }
        }

        protected void MakeNewCategory(object sender, EventArgs e)
        {
            if (Netpay.Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Shop.Products.Category.SecuredObject, PermissionValue.Add);

            //Declare the viewState - used in the List_DataBinding function 
            //In order to know if to add a new item.
            ViewState["IsNewItemAdded"] = true;

            //Declare the editable item id=0 , because a new item has id=0.
            EditableId = 0;

            //Bind the data to the list.
            rptList.DataBind();

            upEventTypeList.Update();
        }
    }
}