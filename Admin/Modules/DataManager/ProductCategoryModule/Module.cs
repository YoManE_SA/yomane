﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Modules.DataManager.ProductCategoryModule
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuProductCategory;

        public static readonly string ModuleName = "ProductCategoryList";

        public override string Name { get { return ModuleName; } }

        public override decimal Version { get { return 1.0m; } }

        public override string Author { get { return "OBL ltd."; } }

        public override string Description { get { return "Manage the List.ProductCategory table in DB"; } }

        public Module() : base(Bll.Shop.Products.Categories.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu
            menuProductCategory = new ApplicationMenu("Product Category", "~/Modules/DataManager/ProductCategoryModule/ProductCategory.aspx", null, 370);

            //Register route
            Application.RegisterRoute("DataManager/ProductCategory", "ProductCategory", typeof(Modules.DataManager.ProductCategory), module:this);

            //no event

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {            
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {            
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Data Manager", menuProductCategory);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuProductCategory.Parent.RemoveChild(menuProductCategory);
            base.OnUninstallAdmin(e);
        }

    }
}