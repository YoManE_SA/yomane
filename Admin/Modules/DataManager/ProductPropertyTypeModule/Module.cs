﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Modules.DataManager.ProductPropertyTypeModule
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuProductPropertyType;

        public static readonly string ModuleName = "ProductPropertyTypeList";

        public override string Name { get { return ModuleName; } }

        public override decimal Version { get { return 1.0m; } }

        public override string Author { get { return "OBL ltd."; } }

        public override string Description { get { return "Manage the List.ProductPropertyType table in DB"; } }

        public Module() : base(Bll.GlobalDataManager.ProductPropertyTypes.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu
            menuProductPropertyType = new ApplicationMenu("Product Property Type", "~/Modules/DataManager/ProductPropertyTypeModule/ProductPropertyType.aspx", null, 340);

            //Register route
            Application.RegisterRoute("DataManager/ProductPropertyType", "ProductPropertyType", typeof(Modules.DataManager.ProductPropertyType), module : this);

            //no event

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {            
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {            
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Data Manager", menuProductPropertyType);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuProductPropertyType.Parent.RemoveChild(menuProductPropertyType);
            base.OnUninstallAdmin(e);
        }
    }
}