﻿using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Modules.DataManager
{
    public partial class ProductType : Controls.ModulePage
    {
        public ProductType() : base(ProductTypeModule.Module.ModuleName) { }

        public int EditableId { get; set; }
        
        public bool Check { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            EditableId = -1;
            rptList.DataBinding += List_DataBinding;

            base.OnLoad(e);
        }

        protected void List_DataBinding(object sender, EventArgs e)
        {
            rptList.DataSource = Bll.DataManager.ProductType.Cache;
        }
        
        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            switch (e.CommandName)
            {
                case "Update":
                    //Declare variables
                    int id;
                    Bll.DataManager.ProductType productTypeItem;

                    //Get the selected item id
                    Check = int.TryParse(e.CommandArgument.ToString(), out id);
                    if (Check)
                    {
                        productTypeItem = Bll.DataManager.ProductType.Get(id);
                        string newName = (e.Item.FindControl("txtName") as TextBox).Text;

                        //Insert the newName to the productTypeItem ans Save.
                        productTypeItem.Name = newName;
                        productTypeItem.Save();
                        
                        //Refresh the list
                        EditableId = -1;
                        upEventTypeList.DataBind();
                        upEventTypeList.Update();
                    }
                    break;
                    
                case "Cancel":
                    EditableId = -1;
                    upEventTypeList.DataBind();
                    upEventTypeList.Update();
                    break;

                case "Edit":
                    if (Netpay.Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                        ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.DataManager.ProductType.SecuredObject, PermissionValue.Edit);

                    int editid;
                    bool check = int.TryParse(e.CommandArgument.ToString(), out editid);
                    if (check) EditableId = editid;

                    upEventTypeList.DataBind();
                    upEventTypeList.Update();
                    break;
            }
        }
    }
}