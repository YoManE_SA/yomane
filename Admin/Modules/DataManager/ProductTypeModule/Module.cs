﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Modules.DataManager.ProductTypeModule
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuProductType;

        public static readonly string ModuleName = "ProductTypeList";

        public override string Name { get { return ModuleName; } }

        public override decimal Version { get { return 1.0m; } }

        public override string Author { get { return "OBL ltd."; } }

        public override string Description { get { return "Manage List.ProductType table in DB"; } }

        public Module() : base(Bll.GlobalDataManager.ProductTypes.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu item
            menuProductType = new ApplicationMenu("Product Type", "~/Modules/DataManager/ProductTypeModule/ProductType.aspx", null, 330);

            //Register route
            Application.RegisterRoute("DataManager/ProductType", "ProductType", typeof(Modules.DataManager.ProductType), module:this);

            //No event
            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {            
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {            
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Data Manager", menuProductType);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuProductType.Parent.RemoveChild(menuProductType);
            base.OnUninstallAdmin(e);
        }
    }
}