﻿<%@ Page Title="Product Type" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductType.aspx.cs" Inherits="Netpay.Admin.Modules.DataManager.ProductType" %>

<asp:Content ContentPlaceHolderID="BodyContent" runat="server">
    <admin:ListSection runat="server" Title="Product Type">
        <Header></Header>
        <Body>
            <NP:UpdatePanel runat="server" ID="upEventTypeList" RenderMode="Block" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <table class="table table-striped">
                        <netpay:DynamicRepeater runat="server" ID="rptList" OnItemCommand="rptList_ItemCommand">
                            <HeaderTemplate>
                                <thead>
                                    <tr>
                                        <th>Product Type ID</th>
                                        <th>Name</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:MultiView ID="MultiViewSection" runat="server" ActiveViewIndex='<%# (byte)Eval("ProductTypeId")==EditableId ? 0 : 1 %>'>
                                    <asp:View runat="server" ID="zero">
                                        <tr>
                                            <td>
                                                <asp:Literal runat="server" ID="txtProductTypeId" Text='<%# Eval("ProductTypeId") %>' />
                                            </td>
                                            <td>
                                                <asp:TextBox CssClass="form-control" runat="server" ID="txtName" Text='<%# Eval("Name") %>' />
                                            </td>
                                            <td>
                                                <asp:Button ID="CancelButton" CssClass="btn btn-default" runat="server" Text="Cancel"  CommandName="Cancel" />
                                                <asp:Button ID="UpdateButton" CssClass="btn btn-success" runat="server" Text="Update" CommandName="Update" CommandArgument='<%# Eval("ProductTypeId") %>'  ClientIDMode="Static" />
                                            </td>
                                        </tr>
                                    </asp:View>

                                    <asp:View runat="server" ID="one">
                                        <tr>
                                            <td>
                                                <asp:Literal runat="server"  Text='<%# Eval("ProductTypeId") %>' />
                                            </td>
                                            <td>
                                                <asp:Literal runat="server"  Text='<%# Eval("Name") %>' />
                                            </td>
                                            <td>
                                                <asp:Button runat="server"  CssClass="btn btn-primary" Text="Edit" CommandArgument='<%# Eval("ProductTypeId") %>'  CommandName="Edit" />
                                            </td>
                                        </tr>
                                    </asp:View>
                                </asp:MultiView>
                            </ItemTemplate>
                        </netpay:DynamicRepeater>
                    </table>
                </ContentTemplate>
            </NP:UpdatePanel>
        </Body>
        <Footer>
            <admin:DataButtons runat="server" EnableSave="false"  EnableNew="false" />
        </Footer>
    </admin:ListSection>
</asp:Content>
