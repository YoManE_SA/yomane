﻿using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Modules.DataManager
{
    public partial class CountryList : Controls.ModulePage
    {
        public CountryList() : base(CountryListModule.Module.ModuleName) {}

        public int EditableId { get; set; }

        public bool Check { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            EditableId = -1;
            rptList.DataBinding += List_DataBinding;

            base.OnLoad(e);
        }

        protected void List_DataBinding(object sender, EventArgs e)
        {
            rptList.DataSource = Bll.Country.Cache;
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Update":
                    //Declare variables
                    int id;
                    Bll.Country countryItem;

                    //Get the selected item id
                    Check = int.TryParse(e.CommandArgument.ToString(), out id);
                    if (Check)
                    {
                        countryItem = Bll.Country.Get(id);

                        //Check the SortOrder field, if it cant be parsed , don't save
                        if (!(string.IsNullOrEmpty((e.Item.FindControl("txtSortOrder") as TextBox).Text)))
                        {
                            bool checkSortOrder;
                            byte tmpSortOrder;
                            checkSortOrder = byte.TryParse(((e.Item.FindControl("txtSortOrder") as TextBox).Text), out tmpSortOrder);

                            if (!checkSortOrder)
                            {
                                ListMessage.SetMessage("Sort Order must be tiny int", true);
                                upEventTypeList.Update();
                                return;
                            }
                            else
                            {
                                countryItem.SortOrder = tmpSortOrder;
                                countryItem.Save();
                            }
                        }
                    }
                    

                    //Refresh the list
                    EditableId = -1;
                    upEventTypeList.DataBind();
                    upEventTypeList.Update();

                    break;

                case "Cancel":

                    EditableId = -1;
                    upEventTypeList.DataBind();
                    upEventTypeList.Update();
                    break;

                case "Edit":
                    if (Netpay.Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                        ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Country.SecuredObject, PermissionValue.Edit);

                    int editid;
                    bool check = int.TryParse(e.CommandArgument.ToString(), out editid);
                    if (check) EditableId = editid;

                    upEventTypeList.DataBind();
                    upEventTypeList.Update();
                    break;
            }
        }
    }
}