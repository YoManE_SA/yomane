﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Modules.DataManager.CountryListModule
{
    public class Module : Admin.CoreBasedModule
    {
        public static readonly string ModuleName = "CountryList";

        private ApplicationMenu menuCountryList;

        public override string Name { get { return ModuleName; } }

        public override decimal Version { get { return 1.0m; } }

        public override string Author { get { return "OBL ltd."; } }

        public override string Description { get { return "Manage List.Country table in DB"; } }

        public Module() : base(Bll.International.Countries.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu
            menuCountryList = new ApplicationMenu("Country List", "~/Modules/DataManager/CountryListModule/CountryList.aspx", null, 310);

            //Register route
            Application.RegisterRoute("DataManager/CountryList", "CountryList", typeof(Modules.DataManager.CountryList), module:this);

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {            
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {            
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Data Manager", menuCountryList);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuCountryList.Parent.RemoveChild(menuCountryList);
            base.OnUninstallAdmin(e);
        }

    }
}