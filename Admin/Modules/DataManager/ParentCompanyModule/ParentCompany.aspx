﻿<%@ Page Title="Parent Company" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ParentCompany.aspx.cs" Inherits="Netpay.Admin.Modules.DataManager.ParentCompany" %>

<asp:Content ContentPlaceHolderID="BodyContent" runat="server">
    <NP:UpdatePanel runat="server" ID="upEventTypeList" RenderMode="Block" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>
            <admin:ListSection runat="server" Title="Parent Company">
                <Header>
                    <netpay:ActionNotify runat="server" ID="ListMessage" />
                    <admin:DataButtons runat="server" EnableSave="false" EnableNew="true" OnNew="MakeNewParentCompany" />
                </Header>
                <Body>
                    <table class="table table-striped">
                        <netpay:DynamicRepeater runat="server" ID="rptList" OnItemCommand="rptList_ItemCommand">
                            <HeaderTemplate>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Code</th>
                                        <th>Recurring Url</th>
                                        <th>Is Default</th>
                                        <th>Recurring Echeck Url</th>
                                        <th>Sms Url</th>
                                        <th>Template Name</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:MultiView ID="MultiViewSection" runat="server" ActiveViewIndex='<%# (int)Eval("ID")==EditableId ? 0 : 1 %>'>
                                    <asp:View runat="server" ID="zero">
                                        <tr>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("ID") %>'/>
                                            </td>
                                            <td>
                                                <asp:TextBox CssClass="form-control" runat="server" ID="txtCode" MaxLength="20" Text='<%# Eval("Code") %>' />
                                            </td>
                                            <td>
                                                <asp:TextBox CssClass="form-control" runat="server" ID="txtReccuringUrl" MaxLength="100" Text='<%# Eval("ReccuringUrl") %>' />
                                            </td>
                                            <td>
                                                <netpay:BoolDropDown runat="server" ID="bddIsDefault" CssClass="form-control" TrueText="True" FalseText="False"  Value='<%#Eval("IsDefault") %>' EnableBlankSelection="false" />
                                            </td>
                                            <td>
                                                <asp:TextBox CssClass="form-control" runat="server" ID="txtRecurringEcheckUrl" Text='<%# Eval("ReccuringEcheckUrl") %>' />
                                            </td>
                                            <td>
                                                <asp:TextBox CssClass="form-control" runat="server" ID="txtSmsUrl" Text='<%# Eval("SmsUrl") %>' />
                                            </td>
                                            <td>
                                                <asp:TextBox CssClass="form-control" runat="server" ID="txtTemplateName" Text='<%# Eval("TemplateName") != null ? Eval("TemplateName") : "" %>'></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="Button3" CssClass="btn btn-default" runat="server" Text="Cancel" CommandName="Cancel" />
                                                <asp:Button ID="Button4" CssClass="btn btn-success" runat="server" Text="Update" CommandName="Update" CommandArgument='<%# Eval("ID") %>' ClientIDMode="Static" />
                                            </td>
                                        </tr>
                                    </asp:View>

                                    <asp:View runat="server" ID="one">
                                        <tr>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("ID") %>'/>
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Code") %>'/>
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("ReccuringUrl") %>'/>
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("IsDefault") %>'/>
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("ReccuringEcheckUrl") %>'/>
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("SmsUrl") %>'/>
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("TemplateName") != null ? Eval("TemplateName") : "" %>' />
                                            </td>
                                            <td>
                                                <asp:Button runat="server" CssClass="btn btn-primary" Text="Edit" CommandArgument='<%# Eval("ID") %>' CommandName="Edit" />
                                            </td>
                                        </tr>
                                    </asp:View>
                                </asp:MultiView>
                            </ItemTemplate>
                        </netpay:DynamicRepeater>
                    </table>
                </Body>
                <Footer>
                </Footer>
            </admin:ListSection>
        </ContentTemplate>
    </NP:UpdatePanel>
</asp:Content>


