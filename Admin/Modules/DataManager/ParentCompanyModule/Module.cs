﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Modules.DataManager.ParentCompanyModule
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuParentCompany;

        public static readonly string ModuleName = "ParentCompanyList";

        public override string Name { get { return ModuleName; } }

        public override decimal Version { get { return 1.0m; } }

        public override string Author { get { return "OBL ltd."; } }

        public override string Description { get { return "Manage List.ParentCompany table in DB"; } }

        public Module() : base(Bll.GlobalDataManager.ParentCompanies.Module.Current) {}

        protected override void OnInit(EventArgs e)
        {
            //Create menu
            menuParentCompany = new ApplicationMenu("Parent Company", "~/Modules/DataManager/ParentCompanyModule/ParentCompany.aspx", null, 320);

            //Register route
            Application.RegisterRoute("DataManager/ParentCompany", "ParentCompany", typeof(Modules.DataManager.ParentCompany), module:this);
            
            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {            
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {            
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Data Manager", menuParentCompany);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuParentCompany.Parent.RemoveChild(menuParentCompany);
            base.OnUninstallAdmin(e);
        }
    }
}