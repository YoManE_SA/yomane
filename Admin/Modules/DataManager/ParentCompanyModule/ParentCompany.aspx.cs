﻿using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Modules.DataManager
{
    public partial class ParentCompany : Controls.ModulePage
    {
        public ParentCompany() : base(ParentCompanyModule.Module.ModuleName) { }

        public int EditableId { get; set; }

        public bool Check { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            EditableId = -1;
            rptList.DataBinding += List_DataBinding;

            base.OnLoad(e);
        }

        protected void List_DataBinding(object sender, EventArgs e)
        {
            var items = Bll.DataManager.ParentCompany.GetAllParentCompanies();
            if (ViewState["IsNewItemAdded"] != null && (bool)ViewState["IsNewItemAdded"])
            {
                if (!items.Exists(x => x.ID == 0))
                    items.Insert(0, new Bll.DataManager.ParentCompany());
            }

            rptList.DataSource = items;
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Update":

                    //Declare variables
                    int id;
                    Bll.DataManager.ParentCompany ParentCompanyItem;

                    //Get the selected item id
                    Check = int.TryParse(e.CommandArgument.ToString(), out id);
                    if (Check)
                    {
                        //Check Data Values

                        if ((string.IsNullOrEmpty((e.Item.FindControl("txtCode") as TextBox).Text.Trim()))
                        || (string.IsNullOrEmpty((e.Item.FindControl("txtReccuringUrl") as TextBox).Text.Trim()))
                        || (string.IsNullOrEmpty((e.Item.FindControl("txtRecurringEcheckUrl") as TextBox).Text.Trim()))
                        || (string.IsNullOrEmpty((e.Item.FindControl("txtSmsUrl") as TextBox).Text.Trim()))
                        || (string.IsNullOrEmpty((e.Item.FindControl("txtTemplateName") as TextBox).Text.Trim())))
                        {
                            ListMessage.SetMessage("All fields must have values", true);
                            upEventTypeList.Update();
                            return;
                        }
                                                
                        if (id == 0) // Means that its a new item.
                        {
                            //Create new parent copany
                            ParentCompanyItem = new Bll.DataManager.ParentCompany();
                        }
                        else
                        {
                            ParentCompanyItem = Bll.DataManager.ParentCompany.Get(id);
                        }

                        //Insert the new Data to the parent company.
                        ParentCompanyItem.Code = (e.Item.FindControl("txtCode") as TextBox).Text.Trim();
                        ParentCompanyItem.ReccuringUrl = (e.Item.FindControl("txtReccuringUrl") as TextBox).Text.Trim();
                        ParentCompanyItem.ReccuringEcheckUrl = (e.Item.FindControl("txtRecurringEcheckUrl") as TextBox).Text.Trim();
                        ParentCompanyItem.SmsUrl = (e.Item.FindControl("txtSmsUrl") as TextBox).Text.Trim();
                        ParentCompanyItem.IsDefault = (e.Item.FindControl("bddIsDefault") as Netpay.Web.Controls.BoolDropDown).BoolValue.Value;
                        ParentCompanyItem.TemplateName = (e.Item.FindControl("txtTemplateName") as TextBox).Text.Trim();

                        //Save
                        ParentCompanyItem.Save();

                    }
                    else // the Command argument cannot be parsed to Int value.
                    {
                        return;
                    }
                    
                    //after save it's not a new item anymore.
                    ViewState["IsNewItemAdded"] = false;
                    
                    //Refresh the list
                    EditableId = -1;
                    upEventTypeList.DataBind();
                    upEventTypeList.Update();
                    break;

                case "Cancel":
                    ViewState["IsNewItemAdded"] = false;
                    Infrastructure.Domain.Current.RemoveCachData("States");

                    EditableId = -1;
                    upEventTypeList.DataBind();
                    upEventTypeList.Update();
                    break;

                case "Edit":
                    if (Netpay.Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                        ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.DataManager.ParentCompany.SecuredObject, PermissionValue.Edit);

                    int editid;
                    bool check = int.TryParse(e.CommandArgument.ToString(), out editid);
                    if (check) EditableId = editid;

                    upEventTypeList.DataBind();
                    upEventTypeList.Update();
                    break;
            }
        }

        protected void MakeNewParentCompany(object sender, EventArgs e)
        {
            if (Netpay.Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.DataManager.ParentCompany.SecuredObject, PermissionValue.Add);


            //Declare the viewState - used in the List_DataBinding function 
            //In order to know if to add a new item.
            ViewState["IsNewItemAdded"] = true;

            //Declare the editable item id=0 , because a new item has id=0.
            EditableId = 0;

            //Bind the data to the list.
            rptList.DataBind();

            upEventTypeList.Update();
        }
    }
}