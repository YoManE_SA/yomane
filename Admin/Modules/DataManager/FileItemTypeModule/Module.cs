﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Modules.DataManager.FileItemTypeModule
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuFileItemType;

        public static readonly string ModuleName = "FileItemTypeList";

        public override string Name { get { return ModuleName; } }

        public override string Author { get { return "OBL ltd."; } }

        public override string Description { get { return "Manage List.FileItemType table in DB"; } }

        public Module() : base(Bll.GlobalDataManager.FileItemTypes.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu
            menuFileItemType = new ApplicationMenu("File Item Type", "~/Modules/DataManager/FileItemTypeModule/FileItemType.aspx", null, 323);

            //Register route
            Application.RegisterRoute("DataManager/FileItemType", "FileItemType", typeof(FileItemType), module: this);

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Data Manager", menuFileItemType);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuFileItemType.Parent.RemoveChild(menuFileItemType);
            base.OnUninstallAdmin(e);
        }
    }
}