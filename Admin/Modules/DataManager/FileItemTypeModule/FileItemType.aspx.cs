﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Modules.DataManager.FileItemTypeModule
{
    public partial class FileItemType : Controls.ModulePage
    {
        public FileItemType() : base(Module.ModuleName) { }

        public int EditableId { get; set; }

        public bool Check { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            EditableId = -1;
            rptList.DataBinding += List_DataBinding;

            base.OnLoad(e);
        }

        protected void List_DataBinding(object sender, EventArgs e)
        {
            var items = Bll.GlobalDataManager.FileItemTypes.FileItemType.Cache;
            if (ViewState["IsNewItemAdded"] != null && (bool)ViewState["IsNewItemAdded"])
            {
                if (!items.Exists(x => x.ID == 0))
                    items.Insert(0, new Bll.GlobalDataManager.FileItemTypes.FileItemType());
            }

            rptList.DataSource = items;
        }

        

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Update":

                    //Declare variables
                    int id;
                    bool isexist = true;
                    Bll.GlobalDataManager.FileItemTypes.FileItemType FileItemTypeItem;

                    //Get the selected item id
                    Check = int.TryParse(e.CommandArgument.ToString(), out id);
                    if (Check)
                    {
                        //Check data values
                        if (string.IsNullOrEmpty((e.Item.FindControl("txtName") as TextBox).Text.Trim()))
                        {
                            ViewState["IsNewItemAdded"] = false;
                            Infrastructure.Domain.Current.RemoveCachData("FilesManager.FileTypes");
                            EditableId = -1;

                            ListMessage.SetMessage("Please insert name.", true);

                            upFileItemType.DataBind();
                            upFileItemType.Update();
                            return;
                        }

                        if (id == 0) //Means its a new item.
                        {
                            isexist = false;
                            FileItemTypeItem = new Bll.GlobalDataManager.FileItemTypes.FileItemType();
                            FileItemTypeItem.ID = Bll.GlobalDataManager.FileItemTypes.FileItemType.GetNewItemId;
                        }
                        else
                        {
                            FileItemTypeItem = Bll.GlobalDataManager.FileItemTypes.FileItemType.Get(id);
                        }

                        //Insert new data
                        FileItemTypeItem.Name = (e.Item.FindControl("txtName") as TextBox).Text.Trim();

                        //Save
                        FileItemTypeItem.Save(isexist);
                    }
                    else // the Command argument cannot be parsed to Int value.
                    {
                        return;
                    }

                    //after save it's not a new item anymore.
                    Infrastructure.Domain.Current.RemoveCachData("FilesManager.FileTypes");
                    ViewState["IsNewItemAdded"] = false;

                    //Refresh the list
                    EditableId = -1;
                    upFileItemType.DataBind();
                    upFileItemType.Update();
                    break;

                case "Delete":
                    int deleteid;
                    Bll.GlobalDataManager.FileItemTypes.FileItemType itemToDelete;
                    Check = int.TryParse(e.CommandArgument.ToString(), out deleteid);

                    if (Check)
                    {
                        itemToDelete = Bll.GlobalDataManager.FileItemTypes.FileItemType.Get(deleteid);
                        itemToDelete.Delete();
                    }

                    //Refresh the list
                    Infrastructure.Domain.Current.RemoveCachData("FilesManager.FileTypes");
                    EditableId = -1;
                    upFileItemType.DataBind();
                    upFileItemType.Update();
                    break;

                case "Cancel":
                    ViewState["IsNewItemAdded"] = false;
                    Infrastructure.Domain.Current.RemoveCachData("FilesManager.FileTypes");

                    EditableId = -1;
                    upFileItemType.DataBind();
                    upFileItemType.Update();
                    break;

                case "Edit":
                    int editid;
                    bool check = int.TryParse(e.CommandArgument.ToString(), out editid);
                    if (check) EditableId = editid;

                    upFileItemType.DataBind();
                    upFileItemType.Update();
                    break;
            }
        }

        protected void CreateNewFileItemType(object sender, EventArgs e)
        {
            //Declare the viewState - used in the List_DataBinding function 
            //In order to know if to add a new item.
            ViewState["IsNewItemAdded"] = true;

            //Declare the editable item id=0 , because a new item has id=0.
            EditableId = 0;

            //Bind the data to the list.
            rptList.DataBind();

            upFileItemType.Update();
        }
    }
}