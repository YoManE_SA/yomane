﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Modules.DataManager.PeopleRelationTypeModule
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuPeopleRelationType;

        public static readonly string ModuleName = "PeopleRelationTypeList";

        public override string Name { get { return ModuleName; } }

        public override decimal Version { get { return 1.0m; } }

        public override string Author { get { return "OBL ltd."; } }

        public override string Description { get { return "Manage the List.PeopleRelationType table in DB"; } }

        public Module() : base(Bll.GlobalDataManager.PeopleRelationTypes.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //create menu
            menuPeopleRelationType = new ApplicationMenu("People Relation Type", "~/Modules/DataManager/PeopleRelationTypeModule/PeopleRelationType.aspx", null, 360);

            //Register route
            Application.RegisterRoute("DataManager/PeopleRelationType", "PeopleRelationType", typeof(Modules.DataManager.PeopleRelationType), module:this);

            //no event
            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {            
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {            
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Data Manager", menuPeopleRelationType);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuPeopleRelationType.Parent.RemoveChild(menuPeopleRelationType);
            base.OnUninstallAdmin(e);
        }
    }
}