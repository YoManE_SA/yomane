﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Modules.DataManager.PaymentMethodGroupModule
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuPaymentMethodGroup;

        public static readonly string ModuleName = "PaymentMethodGroupList";

        public override string Name { get { return ModuleName; } }

        public override decimal Version { get { return 1.0m; } }

        public override string Author { get { return "OBL ltd."; } }

        public override string Description { get { return "Manage the List.PaymentMethodGroup table in DB"; } }

        public Module() : base(Bll.PaymentMethods.Groups.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu
            menuPaymentMethodGroup = new ApplicationMenu("Payment Method Group", "~/Modules/DataManager/PaymentMethodGroupModule/PaymentMethodGroup.aspx", null, 350);

            //Register route
            Application.RegisterRoute("DataManager/PaymentMethodGroup", "PaymentMethodGroup", typeof(Modules.DataManager.PaymentMethodGroup));

            //no event
            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {            
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {            
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Data Manager", menuPaymentMethodGroup);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuPaymentMethodGroup.Parent.RemoveChild(menuPaymentMethodGroup);
            base.OnUninstallAdmin(e);
        }
    }
}