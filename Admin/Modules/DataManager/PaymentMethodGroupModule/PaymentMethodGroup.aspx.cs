﻿using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Modules.DataManager
{
    public partial class PaymentMethodGroup : Controls.ModulePage
    {
        public PaymentMethodGroup() : base(PaymentMethodGroupModule.Module.ModuleName) { }

        public int EditableId { get; set; }

        public bool Check { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            EditableId = -1;
            rptList.DataBinding += List_DataBinding;

            base.OnLoad(e);
        }

        protected void List_DataBinding(object sender, EventArgs e)
        {
            rptList.DataSource = Bll.PaymentMethods.Group.Search();
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            switch (e.CommandName)
            {
                case "Update":
                    //Declare variables
                    int id;
                    Bll.PaymentMethods.Group PaymentMethodGroupItem;

                    //Get the selected item id
                    Check = int.TryParse(e.CommandArgument.ToString(), out id);
                    if (Check)
                    {
                        PaymentMethodGroupItem = Bll.PaymentMethods.Group.Get(id);
                        string newName = (e.Item.FindControl("txtName") as TextBox).Text;
                        if (string.IsNullOrEmpty(newName))
                        {
                            ListMessage.SetMessage("You can't save without a name", true);
                            upEventTypeList.Update();
                            return;
                        }

                        //Insert the newName to the productTypeItem ans Save.
                        PaymentMethodGroupItem.Name = newName;
                        PaymentMethodGroupItem.SortOrder = byte.Parse((e.Item.FindControl("spValue") as TextBox).Text);
                        PaymentMethodGroupItem.IsPopular = (bool)((e.Item.FindControl("ddlIsPopular") as Web.Controls.BoolDropDown).BoolValue);
                        PaymentMethodGroupItem.Save();

                        //Refresh the list
                        EditableId = -1;
                        upEventTypeList.DataBind();
                        upEventTypeList.Update();
                    }
                    break;

                case "Cancel":
                    EditableId = -1;
                    upEventTypeList.DataBind();
                    upEventTypeList.Update();
                    break;

                case "Edit":
                    if (Netpay.Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                        ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.PaymentMethods.Group.SecuredObject, PermissionValue.Edit);
                    
                    int editid;
                    bool check = int.TryParse(e.CommandArgument.ToString(), out editid);
                    if (check) EditableId = editid;

                    upEventTypeList.DataBind();
                    upEventTypeList.Update();
                    break;
            }
        }
    }
}