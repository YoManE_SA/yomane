﻿<%@ Page Title="Payment Method Group" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PaymentMethodGroup.aspx.cs" Inherits="Netpay.Admin.Modules.DataManager.PaymentMethodGroup" %>

<asp:Content ContentPlaceHolderID="BodyContent" runat="server">
    <NP:UpdatePanel runat="server" ID="upEventTypeList" RenderMode="Block" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>
            <admin:ListSection runat="server" Title="Payment Method Group">
                <Header>
                    <netpay:ActionNotify runat="server" ID="ListMessage" />
                </Header>
                <Body>

                    <contenttemplate>
                    <table class="table table-striped">
                        <netpay:DynamicRepeater runat="server" ID="rptList" OnItemCommand="rptList_ItemCommand">
                            <HeaderTemplate>
                                <thead>
                                    <tr>
                                        <th>Group ID</th>
                                        <th>Group Type</th>
                                        <th>Name</th>
                                        <th>Short Name</th>
                                        <th>Sort Order</th>
                                        <th>Is Popular</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:MultiView ID="MultiViewSection" runat="server" ActiveViewIndex='<%# (int)Eval("ID")==EditableId ? 0 : 1 %>'>
                                    <asp:View runat="server" ID="zero">
                                        <tr>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("ID") %>'></asp:Literal>
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# (Netpay.CommonTypes.PaymentMethodType)Eval("Type") %>' />
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" CssClass="form-control" ID="txtName" Text='<%# Eval("Name") %>' />
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("ShortName") %>' />
                                            </td>
                                            <td>
                                                <JQ:Spinner runat="server" Height="15px" ID="spValue" Text='<%# Eval("SortOrder") %>' MinimumValue="0" MaximumValue="255"  />
                                            </td>
                                            <td>
                                                <netpay:BoolDropDown runat="server" CssClass="form-control" TrueText="True" FalseText="False"  ID="ddlIsPopular" Value='<%#Eval("IsPopular") %>' EnableBlankSelection="false" />
                                            </td>
                                            <td>
                                                <asp:Button ID="CancelButton" CssClass="btn btn-default" runat="server" Text="Cancel" CommandName="Cancel" />
                                                <asp:Button ID="UpdateButton" CssClass="btn btn-success" runat="server" Text="Update" CommandName="Update" CommandArgument='<%# Eval("ID") %>' ClientIDMode="Static" />
                                            </td>
                                        </tr>
                                    </asp:View>

                                    <asp:View runat="server" ID="one">
                                        <tr>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("ID") %>' />
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# (Netpay.CommonTypes.PaymentMethodType)Eval("Type") %>' />
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Name") %>' />
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("ShortName") %>' />
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("SortOrder") %>' />
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# (bool)Eval("IsPopular") %>' />
                                            </td>
                                            <td>
                                                <asp:Button runat="server" CssClass="btn btn-primary" Text="Edit" CommandArgument='<%# Eval("ID") %>' CommandName="Edit" />
                                            </td>
                                        </tr>
                                    </asp:View>
                                </asp:MultiView>
                            </ItemTemplate>
                        </netpay:DynamicRepeater>
                    </table>
                </contenttemplate>
                </Body>
                <Footer>
                    <admin:DataButtons runat="server" EnableSave="false" EnableNew="false" />
                </Footer>
            </admin:ListSection>
        </ContentTemplate>
    </NP:UpdatePanel>
</asp:Content>


