﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Modules.DataManager.StateListModule
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuStateList;

        public static readonly string ModuleName = "StateList";

        public override string Name { get { return ModuleName; } }

        public override decimal Version { get { return 1.0m; } }

        public override string Author { get { return "OBL ltd."; } }

        public override string Description { get { return "Manage the List.StateList table in DB"; } }

        public Module() : base(Bll.International.States.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu
            menuStateList = new ApplicationMenu("State List", "~/Modules/DataManager/StateListModule/StateList.aspx", null, 380);

            //Register route
            Application.RegisterRoute("DataManager/StateList", "StateList", typeof(Modules.DataManager.StateList), module:this);

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {            
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {            
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Data Manager", menuStateList);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuStateList.Parent.RemoveChild(menuStateList);
            base.OnUninstallAdmin(e);
        }
    }
}