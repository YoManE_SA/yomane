﻿<%@ Page Title="State List" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StateList.aspx.cs" Inherits="Netpay.Admin.Modules.DataManager.StateList" %>

<asp:Content ContentPlaceHolderID="BodyContent" runat="server">
    <NP:UpdatePanel runat="server" ID="upEventTypeList" RenderMode="Block" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>
            <admin:ListSection runat="server" Title="State List">
                <Header>
                    <netpay:ActionNotify runat="server" ID="ListMessage" />
                    <admin:DataButtons runat="server" EnableSave="false" EnableNew="true" OnNew="MakeNewState" />
                </Header>
                <Body>
                    <table class="table table-striped">
                        <netpay:DynamicRepeater runat="server" ID="rptList" OnItemCommand="rptList_ItemCommand">
                            <HeaderTemplate>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>State IsoCode</th>
                                        <th>Country IsoCode</th>
                                        <th>Name</th>
                                        <th>SortOrder</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:MultiView ID="MultiViewSection" runat="server" ActiveViewIndex='<%# (int)Eval("ID")==EditableId ? 0 : 1 %>'>
                                    <asp:View runat="server" ID="zero">
                                        <asp:MultiView runat="server" ActiveViewIndex='<%# (int)Eval("ID") == 0 ? 0 : 1 %>'>
                                            <asp:View runat="server">
                                                <tr>
                                                    <td>
                                                        <asp:Literal runat="server" Text='<%# Eval("ID") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtNewIsoCode" MaxLength="2" Text='<%# Eval("IsoCode") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtNewCountryIsoCode" MaxLength="2" Text='<%# Eval("CountryIsoCode") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtNewName" Text='<%# Eval("Name") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtNewSortOrder" Text='<%# Eval("SortOrder") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="Button3" CssClass="btn btn-default" runat="server" Text="Cancel" CommandName="Cancel" />
                                                        <asp:Button ID="Button4" CssClass="btn btn-success" runat="server" Text="Update" CommandName="Update" CommandArgument='<%# Eval("ID") %>' ClientIDMode="Static" />
                                                    </td>
                                                </tr>
                                            </asp:View>
                                            <asp:View runat="server">
                                                <tr>
                                                    <td>
                                                        <asp:Literal runat="server" Text='<%# Eval("ID") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Literal runat="server" ID="txtIsoCode" Text='<%# Eval("IsoCode") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Literal runat="server" ID="txtCountryIsoCode" Text='<%# Eval("CountryIsoCode") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtName" Text='<%# Eval("Name") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtSortOrder" Text='<%# Eval("SortOrder") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="Button1" CssClass="btn btn-default" runat="server" Text="Cancel" CommandName="Cancel" />
                                                        <asp:Button ID="Button2" CssClass="btn btn-success" runat="server" Text="Update" CommandName="Update" CommandArgument='<%# Eval("ID") %>' ClientIDMode="Static" />
                                                    </td>
                                                </tr>
                                            </asp:View>
                                        </asp:MultiView>
                                    </asp:View>

                                    <asp:View runat="server" ID="one">
                                        <tr>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("ID") %>' />
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("IsoCode") %>' />
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("CountryIsoCode") %>' />
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Name") %>' />
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("SortOrder") %>' />
                                            </td>
                                            <td>
                                                <asp:Button runat="server" CssClass="btn btn-primary" Text="Edit" CommandArgument='<%# Eval("ID") %>' CommandName="Edit" />
                                            </td>
                                        </tr>
                                    </asp:View>
                                </asp:MultiView>
                            </ItemTemplate>
                        </netpay:DynamicRepeater>
                    </table>
                </Body>
                <Footer>
                </Footer>
            </admin:ListSection>
        </ContentTemplate>
    </NP:UpdatePanel>
</asp:Content>
