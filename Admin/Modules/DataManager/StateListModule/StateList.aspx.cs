﻿using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Modules.DataManager
{
    public partial class StateList : Controls.ModulePage
    {
        public StateList() : base(StateListModule.Module.ModuleName) { }

        public int EditableId { get; set; }

        public bool Check { get; set; }
        protected override void OnLoad(EventArgs e)
        {
            EditableId = -1;
            rptList.DataBinding += List_DataBinding;

            //Refresh Cache data
            Infrastructure.Domain.Current.RemoveCachData("States");

            base.OnLoad(e);
        }

        protected void List_DataBinding(object sender, EventArgs e)
        {
            var items = Netpay.Bll.State.Cache;
            if (ViewState["IsNewItemAdded"] != null && (bool)ViewState["IsNewItemAdded"])
            {
                if (!items.Exists(x => x.ID == 0))
                    items.Insert(0, new Bll.State());
            }

            rptList.DataSource = items;
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            switch (e.CommandName)
            {
                case "Update":

                    //Declare variables
                    int id;
                    Bll.State StateItem;

                    //Get the selected item id
                    Check = int.TryParse(e.CommandArgument.ToString(), out id);
                    if (Check)
                    {
                        bool checkSortOrder = false;
                        byte? newSortOrder = null;
                        string newName = "";

                        if (id == 0) // Means that its a new item.
                        {
                            int? newStateId = null;

                            //Check data values
                            //StateIsoCode - must be with 2 characters , must not be in DB
                            if ((e.Item.FindControl("txtNewIsoCode") as TextBox).Text.Trim().Length != 2)
                            {
                                ListMessage.SetMessage("State IsoCode must have 2 characters", true);
                                upEventTypeList.Update();
                                return;
                            }
                            else if ((e.Item.FindControl("txtNewIsoCode") as TextBox).Text.Trim().Length == 2)
                            {
                                if (Bll.State.CheckIfStateIsoCodeExist((e.Item.FindControl("txtNewIsoCode") as TextBox).Text.Trim()))
                                {
                                    ListMessage.SetMessage("State IsoCode already exist, insert other value", true);
                                    upEventTypeList.Update();
                                    return;
                                }
                            }

                            //CountryIsoCode
                            if ((e.Item.FindControl("txtNewCountryIsoCode") as TextBox).Text.Trim().Length != 2)
                            {
                                ListMessage.SetMessage("Country IsoCode must have 2 characters", true);
                                upEventTypeList.Update();
                                return;
                            }

                            //Name 
                            newName = (e.Item.FindControl("txtNewName") as TextBox).Text.Trim();
                            if (string.IsNullOrEmpty(newName))
                            {
                                ListMessage.SetMessage("You can't save without name", true);
                                upEventTypeList.Update();
                                return;
                            }

                            //SortOrder
                            if (!string.IsNullOrEmpty((e.Item.FindControl("txtNewSortOrder") as TextBox).Text))
                            {
                                byte tmpNewSortOrder;
                                checkSortOrder = byte.TryParse(((e.Item.FindControl("txtNewSortOrder") as TextBox).Text), out tmpNewSortOrder);

                                if (checkSortOrder)
                                {
                                    newSortOrder = tmpNewSortOrder;
                                }
                                else
                                {
                                    ListMessage.SetMessage("Sort Order value is not valid", true);
                                    upEventTypeList.Update();
                                    return;
                                }
                            }

                            newStateId = Bll.State.GetNewStateIdValue();
                            if (!newStateId.HasValue)
                            {
                                ListMessage.SetMessage("Can't generate State ID value", true);
                                upEventTypeList.Update();
                                return;
                            }

                            //Create new state
                            StateItem = new Bll.State();

                            //Insert new data
                            StateItem.Name = newName;
                            StateItem.IsoCode = (e.Item.FindControl("txtNewIsoCode") as TextBox).Text.Trim().ToUpper();
                            StateItem.CountryIsoCode = (e.Item.FindControl("txtNewCountryIsoCode") as TextBox).Text.Trim().ToUpper();
                            StateItem.SortOrder = newSortOrder;
                            StateItem.ID = newStateId.Value;
                            StateItem.Save();

                        }
                        else
                        {
                            newName = (e.Item.FindControl("txtName") as TextBox).Text;
                            if (string.IsNullOrEmpty(newName))
                            {
                                ListMessage.SetMessage("You can't save without name", true);
                                upEventTypeList.Update();
                                return;
                            }


                            if (!string.IsNullOrEmpty((e.Item.FindControl("txtSortOrder") as TextBox).Text))
                            {
                                byte tmpNewSortOrder;
                                checkSortOrder = byte.TryParse(((e.Item.FindControl("txtSortOrder") as TextBox).Text), out tmpNewSortOrder);

                                if (checkSortOrder)
                                {
                                    newSortOrder = tmpNewSortOrder;
                                }
                                else
                                {
                                    ListMessage.SetMessage("Sort Order value is not valid", true);
                                    upEventTypeList.Update();
                                    return;
                                }
                            }

                            StateItem = Bll.State.Get(id);

                            //Insert the newName to the.
                            StateItem.Name = newName;
                            StateItem.SortOrder = newSortOrder;
                            StateItem.Save();

                        }


                        //after save it's not a new item anymore.
                        ViewState["IsNewItemAdded"] = false;

                        //Refresh Cache data
                        Infrastructure.Domain.Current.RemoveCachData("States");

                        //Refresh the list
                        EditableId = -1;
                        upEventTypeList.DataBind();
                        upEventTypeList.Update();
                    }
                    else // the Command argument cannot be parsed to Int value.
                    {
                        return;
                    }
                    break;

                case "Cancel":
                    ViewState["IsNewItemAdded"] = false;
                    Infrastructure.Domain.Current.RemoveCachData("States");

                    EditableId = -1;
                    upEventTypeList.DataBind();
                    upEventTypeList.Update();
                    break;

                case "Edit":
                    if (Netpay.Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                        ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.State.SecuredObject, PermissionValue.Edit);

                    int editid;
                    bool check = int.TryParse(e.CommandArgument.ToString(), out editid);
                    if (check) EditableId = editid;

                    upEventTypeList.DataBind();
                    upEventTypeList.Update();
                    break;
            }
        }

        protected void MakeNewState(object sender, EventArgs e)
        {
            if (Netpay.Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.State.SecuredObject, PermissionValue.Add);


            //Declare the viewState - used in the List_DataBinding function 
            //In order to know if to add a new item.
            ViewState["IsNewItemAdded"] = true;

            //Declare the editable item id=0 , because a new item has id=0.
            EditableId = 0;

            //Bind the data to the list.
            rptList.DataBind();

            upEventTypeList.Update();
        }

    }
}