﻿<%@ Page Title="Merchant Department" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MerchantDepartment.aspx.cs" Inherits="Netpay.Admin.Modules.DataManager.MerchantDepartmentModule.MerchantDepartment" %>

<asp:Content ContentPlaceHolderID="BodyContent" runat="server">
    <NP:UpdatePanel runat="server" ID="upMerchantDepartmentList" RenderMode="Block" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>
            <admin:ListSection runat="server" Title="Merchant Department">
                <Header>
                    <netpay:ActionNotify runat="server" ID="ListMessage" />
                    <admin:DataButtons runat="server" EnableSave="false" EnableNew="true" OnNew="CreateNewDepartment" />
                </Header>
                <Body>
                    <table class="table table-striped">
                        <netpay:DynamicRepeater runat="server" ID="rptList" OnItemCommand="rptList_ItemCommand">
                            <HeaderTemplate>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:MultiView ID="MultiViewSection" runat="server" ActiveViewIndex='<%# (int)Eval("ID") == EditableId ? 0 : 1 %>'>
                                    <asp:View runat="server" ID="zero">
                                        <tr>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("ID") %>'/>
                                            </td>
                                            <td>
                                                <asp:TextBox CssClass="form-control" runat="server" ID="txtName" MaxLength="50" Text='<%# Eval("Name") %>' />
                                            </td>
                                            <td>
                                                <asp:Button ID="Button3" CssClass="btn btn-default" runat="server" Text="Cancel" CommandName="Cancel" />
                                                <asp:Button ID="Button4" CssClass="btn btn-success" runat="server" Text="Update" CommandName="Update" CommandArgument='<%# Eval("ID") %>' ClientIDMode="Static" />
                                            </td>
                                        </tr>
                                    </asp:View>
                                    <asp:View runat="server" ID="one">
                                        <tr>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("ID") %>'/>
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Name") %>'/>
                                            </td>
                                            <td>
                                                <asp:Button runat="server" Text="Edit" CssClass="btn btn-primary" CommandArgument='<%# Eval("ID") %>' CommandName="Edit" />
                                                <asp:Button runat="server" Text="Delete" CssClass="btn btn-danger"  CommandArgument='<%# Eval("ID") %>' CommandName="Delete" OnClientClick="return confirm('Are you sure?')"/>
                                            </td>
                                        </tr>
                                    </asp:View>
                                </asp:MultiView>
                            </ItemTemplate>
                        </netpay:DynamicRepeater>
                    </table>
                </Body>
            </admin:ListSection>
        </ContentTemplate>
    </NP:UpdatePanel>
</asp:Content>

