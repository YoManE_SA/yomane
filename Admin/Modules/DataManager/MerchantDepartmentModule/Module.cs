﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Modules.DataManager.MerchantDepartmentModule
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuMerchantDepartment;

        public static readonly string ModuleName = "MerchantDepartmentList";

        public override string Name { get { return ModuleName; } }

        public override string Author { get { return "OBL ltd."; } }

        public override string Description { get { return "Manage dbo.MerchantDepartment table in DB"; } }

        public Module() : base(Bll.GlobalDataManager.MerchantDepartments.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu
            menuMerchantDepartment = new ApplicationMenu("Merchant Department", "~/Modules/DataManager/MerchantDepartmentModule/MerchantDepartment.aspx", null, 321);


            //Register route
            Application.RegisterRoute("DataManager/MerchantDepartment", "MerchantDepartment", typeof(Modules.DataManager.MerchantDepartmentModule.MerchantDepartment), module: this);

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Data Manager", menuMerchantDepartment);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuMerchantDepartment.Parent.RemoveChild(menuMerchantDepartment);
            base.OnUninstallAdmin(e);
        }
    }
}