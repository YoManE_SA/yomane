﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.CommonTypes;
using Netpay.Bll;

namespace Netpay.Admin.RefundRequests
{
    public partial class TransView : Controls.TemplateControlBase
    {
        protected int TransactionID { get { return TemplatePage.ItemID.GetValueOrDefault(); } }

        public Bll.Transactions.Transaction ItemData { get; set; }

        public bool sIsShowRefund //The logic of bool value loacted at 'trans_detail_administration.asp' , line 89
        {
            get
            {
                if (ItemData.Status != TransactionStatus.Captured)
                {
                    return false;
                }
                else
                {
                    if ((ItemData.CreditType==0) || (ItemData.IsTest && ItemData.MerchantID!=35))
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            var id = TransactionID;
        }

        protected override void DataBindChildren()
        {
            if (ItemData == null) ItemData = TemplatePage.GetItemData<Bll.Transactions.Transaction>();
            if (ItemData == null) return;

            if (sIsShowRefund) GlobalMultiView.ActiveViewIndex = 0;
            else GlobalMultiView.ActiveViewIndex = 1;

            //Get the refund reuquest list for the current transaction
            List<Bll.Transactions.RefundRequest> currentTransactionList = Bll.Transactions.RefundRequest.RequestsForTrasaction(TransactionID);
            if (currentTransactionList.Count > 0)
            {
                rptList.DataSource = Bll.Transactions.RefundRequest.RequestsForTrasaction(TransactionID);
                RequestsListMultiView.ActiveViewIndex = 0;
            }
            else
            {
                RequestsListMultiView.ActiveViewIndex = 1;
            }


            //The translate of dbo.GetRefundAmountAvailable method
            if (ItemData != null && ItemData.MerchantID > 0)
            {
                //Get the transaction amount:
                decimal transactionAmount = ItemData.Amount;

                //Get the total refund requests amount that were made on 
                //The current transaction
                decimal totalAmountRefund = Bll.Transactions.RefundRequest.TotalRefundAmountForTransaction(ItemData.ID);

                //If the total refund requests amount is equal or bigger 
                //Than the transaction amount , no refund requests is allowed.
                decimal allowedAmount = transactionAmount - totalAmountRefund;

                if (allowedAmount <= 0) NewRequestMultiView.ActiveViewIndex = 1;
                else
                {
                    NewRequestMultiView.ActiveViewIndex = 0;
                    //Check the case that DebitCompanyId is null
                    txtAmount.Text = Decimal.Round(allowedAmount, 2).ToString();
                    if (ItemData.DebitCompanyID.HasValue)
                    {
                        txtAmount.ReadOnly = !Bll.DebitCompanies.DebitCompany.LoadDebitCompanyById(ItemData.DebitCompanyID.Value).IsAllowPartialRefund;
                    }

                    Netpay.Bll.Merchants.ProcessTerminals terminalItem = Netpay.Bll.Merchants.ProcessTerminals.GetTerminalForTransaction(ItemData.MerchantID.Value, (CommonTypes.Currency)ItemData.CurrencyID, ItemData.PaymentMethodID, ItemData.IPCountry);
                    if (terminalItem == null)
                    {
                        NewRequestMultiView.ActiveViewIndex = 2;
                    }
                    else if (terminalItem != null && rblFeeType.Items.Count == 0)
                    {
                        rblFeeType.Items.Add(new ListItem(string.Format("Normal ({0})", terminalItem.RefundFixedFee.ToIsoAmountFormat(terminalItem.CurrencyID)), "0"));
                        rblFeeType.Items.Add(new ListItem(String.Format("Partial ({0})", terminalItem.PartialRefundFixedFee.ToIsoAmountFormat(terminalItem.CurrencyID)), "1"));
                        rblFeeType.Items.Add(new ListItem(String.Format("Fraud ({0})", terminalItem.FraudRefundFixedFee.ToIsoAmountFormat(terminalItem.CurrencyID)), "2"));
                        if (rblFeeType.Items.Count > 0) rblFeeType.SelectedIndex = 0;
                    }
                }
            }
            base.DataBindChildren();
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            ItemData = Bll.Transactions.Transaction.GetTransaction(null, TransactionID, TransactionStatus.Captured);

            byte refundType = byte.Parse(rblFeeType.SelectedValue);
            var res = Bll.Transactions.RefundRequest.CreateForAdmin(TransactionID, TransactionStatus.Captured, txtAmount.Text.ToNullableDecimal().GetValueOrDefault(), txtComment.Text, refundType);
            ltResult.Text = res.ToString();
            if (res != Bll.RequestRefundResult.Success) return;
            txtAmount.Text = txtComment.Text = "";
            DataBindChildren();

            UpNewRequest.Update();
            UpRequestList.Update();
        }
    }
}