﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.RefundRequests
{
	public partial class List : Controls.TemplateControlBase
	{
		protected override void OnLoad(EventArgs e)
		{
			rptList.DataBinding += List_DataBinding;
			
			base.OnLoad(e);
		}
        
        protected void Load_Command(object sender, CommandEventArgs e)
        {            
            RaiseBubbleEvent(this, new CommandEventArgs("LoadItem", new DataKey(new System.Collections.Specialized.OrderedDictionary() { { "ID", e.CommandArgument.ToString() } })));
        }

        public void btnProcess_Click(object sender, EventArgs e)
		{
			var dataList = Bll.Transactions.RefundRequest.Search(TemplatePage.GetFilter<Bll.Transactions.RefundRequest.SearchFilters>(), rptList);
			foreach (GridViewRow item in rptList.Rows) 
			{
				var itemId = rptList.DataKeys[item.DataItemIndex][0].ToNullableInt().GetValueOrDefault();
				var dataItem = dataList.Where(r => r.ID == itemId).SingleOrDefault();
				decimal newAmount = (item.FindControl("txtAmount") as TextBox).Text.ToNullableDecimal().GetValueOrDefault();
                
				bool isProcess = (item.FindControl("rbProcess") as RadioButton).Checked;
				bool isCreate = (item.FindControl("rbCreate") as RadioButton).Checked;
				bool isCancel = (item.FindControl("rbCancel") as RadioButton).Checked;

                if (newAmount != dataItem.RefundAmount.Value && newAmount > 0 && newAmount < dataItem.RefundAmount.Value)
                {
                    dataItem.RefundAmount = newAmount;
                }

                if (isProcess)
                {
                    if (dataItem.RefundAmount < dataItem.Transaction.Amount)
                    {
                        if (dataItem.Transaction.DebitCompany != null && dataItem.Transaction.DebitCompany.IsAllowPartialRefund)
                        {
                            dataItem.Process(dataItem.RefundAmount.Value);
                        }
                        else
                        {
                            dataItem.AddLog("Partial refund process request denied.");
                        }
                    }
                    else
                    {
                        dataItem.Process(dataItem.RefundAmount.Value);
                    }

                }

                else if (isCreate)
                {
                    Bll.Transactions.RefundRequest.RefundType newType = (Bll.Transactions.RefundRequest.RefundType)(int.Parse((item.FindControl("ddlRefundFee") as Web.Controls.EnumDropDown).Value));
                    dataItem.Type = newType;
                    string ip = Page.Request.ServerVariables["REMOTE_ADDR"];
                    dataItem.Create(dataItem.RefundAmount.Value, ip);
                }

                else if (isCancel) dataItem.CancelRequestByAdmin();
            }

            TemplatePage.PageController.ListView.BindAndUpdate();
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (rptList != null && rptList.HeaderRow != null)
                foreach (TableCell cell in rptList.HeaderRow.Cells)
                {
                    if (cell.Text.Contains("<br/>")) cell.Text = HttpUtility.HtmlDecode(cell.Text);
                }
        }

        private void List_DataBinding(object sender, EventArgs e)
		{
			rptList.DataSource = Bll.Transactions.RefundRequest.Search(TemplatePage.GetFilter<Bll.Transactions.RefundRequest.SearchFilters>(), rptList);
		}

        protected void btnReset_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow item in rptList.Rows)
            {                
                 (item.FindControl("rbProcess") as RadioButton).Checked = false;
                 (item.FindControl("rbCreate") as RadioButton).Checked = false;
                 (item.FindControl("rbCancel") as RadioButton).Checked = false;
            }

            TemplatePage.PageController.ListView.Update();
        }


        protected void btnFlagImage_Command(object sender, CommandEventArgs e)
        {
            int refundId = int.Parse(e.CommandArgument.ToString());
            UpdatePanel currentUpdatePanel = (sender as ImageButton).Parent.Parent as UpdatePanel;

            var refundItem = Bll.Transactions.RefundRequest.Load(refundId);
            if (refundItem != null)
            {
                refundItem.UpdateFlag();
            }

            (sender as ImageButton).ImageUrl = refundItem.FlagImageUrl;
            currentUpdatePanel.Update();
        }
    }
}