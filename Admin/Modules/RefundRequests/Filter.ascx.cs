﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.RefundRequests
{
	public partial class Filter : Controls.TemplateControlBase
	{
		protected override void OnLoad(EventArgs e)
		{
			TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
            base.OnLoad(e);
		}

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
		{
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Bll.Transactions.RefundRequest.SearchFilters();
            if (apMerchant.Value.ToNullableInt() != null)
            {
                var merchant = Bll.Merchants.Merchant.LoadByAccountId(apMerchant.Value.Value);
                if (merchant != null)
                sf.MerchantIDs = new List<int> { merchant.ID };
            }

            sf.Status = new List<Bll.Transactions.RefundRequest.RequestStatus>();
            foreach (ListItem i in chklStatus.Items)
                if (i.Selected) sf.Status.Add(i.Value.ToNullableEnumByName<Bll.Transactions.RefundRequest.RequestStatus>().GetValueOrDefault());

            sf.TransactionID = new Range<int?>(txtOriginalTransID.Text.ToNullableInt());

            if (apDebitCompany.Value.HasValue)
            {
                int? debitCompanyId = null;
                Bll.Accounts.Account account = Bll.Accounts.Account.LoadAccount(apDebitCompany.Value.Value);
                if (account != null && account.AccountType == Bll.Accounts.AccountType.DebitCompany)
                {
                    debitCompanyId = account.DebitCompanyID;
                }
                
                if (debitCompanyId.HasValue)
                {
                    sf.DebitCompanyID = (byte)debitCompanyId.Value;
                }
            }            
            
            if (!string.IsNullOrEmpty(tpTerminal.Value))
            {
                sf.terminalNumber = tpTerminal.Value;
            }
            
            //lblReplyCode

            sf.CurrencyISOCode = ddlCurrency.SelectedCurrencyIso;
            sf.ID = rngID.Value;
            sf.Date = rngDate.Value;
            sf.Amount = rngAmount.Value;
            TemplatePage.SetFilter(sf);
        }
    }
}