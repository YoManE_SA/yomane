﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.RefundRequests.Filter" %>

<admin:FilterSection runat="server" Title="Status">
    <asp:CheckBoxList ClientIDMode="Static" runat="server" ID="chklStatus" RepeatLayout="UnorderedList" CssClass="checkbox-list" DataSource='<%# System.Enum.GetValues(typeof(Netpay.Bll.Transactions.RefundRequest.RequestStatus)) %>' />
</admin:FilterSection>
<admin:FilterSection runat="server" Title="General">
    <div class="form-group">
        <asp:Label ID="lblAccount" runat="server" Text="Account" />
        <admin:AccountPicker runat="server" ID="apMerchant" LimitToType="Merchant" />
    </div>
    <div class="form-group">
        <asp:Label ID="lblDebitCompany" runat="server" Text="DebitCompany" />
        <admin:AccountPicker runat="server" ID="apDebitCompany" LimitToType="DebitCompany" />
    </div>

    <div class="form-group">
        <asp:Label ID="lblTerminal" runat="server" Text="Terminal" />
        <admin:TerminalPicker runat="server" ID="tpTerminal" />
    </div>

    <div class="form-group">
        <asp:Label ID="lblOriginalTransID" runat="server" Text="OriginalTransID: " />
        <asp:TextBox runat="server" ID="txtOriginalTransID" CssClass="form-control" />
    </div>

    <div class="form-group">
        <asp:Label ID="lblCurrency" runat="server" Text="Currency" />
        <div class="input-group">
            <span class="input-group-addon"><span class="fa fa-dollar"></span></span>
            <netpay:CurrencyDropDown CssClass="form-control" runat="server" ID="ddlCurrency" />
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblAmount" runat="server" Text="Amount" />
                <JQ:DecimalRange runat="server" ID="rngAmount" />
            </div>
        </div>
    </div>
</admin:FilterSection>
<admin:FilterSection runat="server" Title="Recorded Information">
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblID" runat="server" Text="ID Range" />
                <JQ:IntRange runat="server" ID="rngID" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblDate" runat="server" Text="Date Range" />
                <JQ:DateRange runat="server" ID="rngDate" />
            </div>
        </div>
    </div>
</admin:FilterSection>




















