﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="TransView.ascx.cs" Inherits="Netpay.Admin.RefundRequests.TransView" %>
<admin:FormSections runat="server" SurroundOnly="true">
    <asp:MultiView runat="server" ID="GlobalMultiView">
        <asp:View runat="server">
            <admin:FormSections runat="server" Title="New Request">
                <asp:UpdatePanel runat="server" ID="UpNewRequest" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:MultiView runat="server" ID="NewRequestMultiView">
                            <asp:View runat="server">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h4>
                                            <asp:Label runat="server" Text="Fee" /></h4>
                                        <asp:RadioButtonList runat="server" ID="rblFeeType" RepeatLayout="UnorderedList" CssClass="checkbox-list" />
                                    </div>
                                    <div class="col-md-12">
                                        <hr />
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <asp:Label ID="Label1" runat="server" Text="Amount" />
                                            <asp:TextBox runat="server" ID="txtAmount" CssClass="form-control" />
                                            <asp:Literal runat="server" ID="ltResult" />
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <asp:Label ID="Label2" runat="server" Text="Comment" />
                                            <asp:TextBox runat="server" CssClass="form-control" ID="txtComment" TextMode="MultiLine" Rows="3" />
                                        </div>
                                    </div>
                                </div>
                                <asp:Button runat="server" ID="btnSave" CssClass="btn btn-primary" Text="Create" OnClick="Save_Click" />
                            </asp:View>
                            <asp:View runat="server">
                                <div class="alert alert-info">
                                    <div>Full refund has already requested.</div>
                                </div>
                            </asp:View>
                            <asp:View runat="server">
                                <div class="alert alert-danger">
                                    <div>Process terminal was not found, need to add one at Merchants -> Terminals tab.</div>
                                </div>
                            </asp:View>
                        </asp:MultiView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </admin:FormSections>
            <admin:FormSections runat="server" Title="Related Requests">
                <asp:UpdatePanel runat="server" ID="UpRequestList" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:MultiView runat="server" ID="RequestsListMultiView">
                            <asp:View runat="server">
                                <asp:Repeater runat="server" ID="rptList">
                                    <HeaderTemplate>
                                        <div class="row inner-table-titles">
                                            <div class="col-md-2">Merchant</div>
                                            <div class="col-md-2">Date</div>
                                            <div class="col-md-2">Request Amount</div>
                                            <div class="col-md-2">Refund Amount</div>
                                            <div class="col-md-2">Status</div>
                                            <div class="col-md-2">Comment</div>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="row table-ui">
                                            <div class="col-md-2">
                                                <asp:HyperLink
                                                    Style="cursor: pointer"
                                                    runat="server"
                                                    Text='<%# Netpay.Bll.Merchants.Merchant.GetMerchantNameByMerchantId((int)Eval("MerchantID"))%>'
                                                    onclick='<%# "OpenPop(" + "\"" +  string.Format("../Merchants/0?ctl00.apAccount.hdID={0}&Search=1&SelectedIndex=0", Eval("AccountID")) + "\"" + ",\"Merchant\",\"1330\",\"820\" , \"1\"); event.cancelBubble=true; return false;" %>'
                                                    Target="_blank" />
                                            </div>
                                            <div class="col-md-2"><%# Eval("Date")%></div>
                                            <div class="col-md-2"><%# new Money((decimal)Eval("RequestAmount"), (string) Eval("CurrencyIso")).ToIsoString() %></div>
                                            <div class="col-md-2"><%# new Money(((decimal?)Eval("RefundAmount")).GetValueOrDefault(), (string)Eval("CurrencyIso")).ToIsoString() %></div>
                                            <div class="col-md-2"><%# Eval("Status")%></div>
                                            <div class="col-md-2"><%# Eval("Comment")%></div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </asp:View>
                            <asp:View runat="server">
                                <div class="alert alert-info">
                                    <div>No requests made.</div>
                                </div>
                            </asp:View>
                        </asp:MultiView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </admin:FormSections>
        </asp:View>
        <asp:View runat="server">
            <admin:FormSections runat="server" Title="New Request">
                <div class="alert alert-info">
                    <div>Ask refund is not available for this transaction.</div>
                </div>
            </admin:FormSections>
        </asp:View>
    </asp:MultiView>
</admin:FormSections>

