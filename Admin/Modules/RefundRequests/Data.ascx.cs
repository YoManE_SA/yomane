﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.RefundRequests
{
    public partial class Data : Controls.TemplateControlBase
    {
        public Bll.Transactions.RefundRequest ItemData { get; set; }
        protected override void DataBindChildren()
        {
            ItemData = TemplatePage.GetItemData<Bll.Transactions.RefundRequest>();
            if (ItemData == null) return;

            rptHistory.DataSource = Bll.StatusChange.Search(new Bll.StatusChange.SearchFilters() { ItemID = ItemData.ID, ItemType = Bll.StatusChange.StatusItemType.RefundReq }, null);

            base.DataBindChildren();
        }

        protected void GeneralCommand(object sender, CommandEventArgs e)
        {
            ItemData = Bll.Transactions.RefundRequest.Load(int.Parse(e.CommandArgument.ToString()));
            
            switch (e.CommandName)
            {
                case "Save":
                    ItemData.UpdateCommentAndConfirmationNumber(txtComment.Text, txtConfirmationNumber.Text);
                    break;
            }

            (Page as Controls.DataTablePage).PageController.FormView.Update();
        }
    }
}

//if (ItemData.RefundAmount.Value != decimal.Parse(txtRefundAmount.Text) && (decimal.Parse((txtRefundAmount.Text))) > 0 && (decimal.Parse((txtRefundAmount.Text))) < ItemData.RefundAmount)
//{
//    ItemData.RefundAmount = (decimal.Parse((txtRefundAmount.Text)));
//}

//case "Process":
//    if (ItemData.RefundAmount < ItemData.Transaction.Amount)
//    {
//        if (!ItemData.Transaction.DebitCompany.IsAllowPartialRefund)
//        {
//            throw new Exception("Partial refund not allowed.");
//        }
//    }

//    ItemData.Process(ItemData.RefundAmount.Value);
//    break;

//case "Create":
//    ItemData.Create(ItemData.RefundAmount.Value);
//    break;

//case "Cancel":
//    ItemData.CancelRequestByAdmin();
//    break;
