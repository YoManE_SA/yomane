﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;
using System.Threading.Tasks;
using Netpay.Web;

namespace Netpay.Admin.RefundRequests
{
	public class Module : Admin.CoreBasedModule
	{
		private ApplicationMenu menuItem;
		public override string Name { get { return "Refund Requests"; } }
		public override decimal Version { get { return 1.0m; } }
		public override string Author { get { return "OBL ltd."; } }
		public override string Description { get { return ""; } }

        public Module() : base(Bll.Transactions.RefundRequests.Module.Current) { }

		protected override void OnInit(EventArgs e)
		{
            //Create menu item
            menuItem = new ApplicationMenu("Refund Requests", "~/Transactions/RefundRequests/0", null, 60);

            //Register route
            Application.RegisterRoute("Transactions/RefundRequests/{*id}", "RefundRequests", typeof(Controls.DataTablePage), module : this);

            //Register event.
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
		}

		protected override void OnActivate(EventArgs e)
		{
			base.OnActivate(e);
		}

		protected override void OnDeactivate(EventArgs e)
		{		
			base.OnDeactivate(e);
		}

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Transactions", menuItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuItem.Parent.RemoveChild(menuItem);
            base.OnUninstallAdmin(e);
        }

        private void Application_InitTemplatePage(object sender, EventArgs e)
		{
            if (!CoreModule.IsInstalled)
                return;

			var page = sender as Controls.DataTablePage;
			if (page.TemplateName == "RefundRequests")
			{
				page.PageController.LoadItem += PageController_LoadItem;
                page.AddControlToList(page.LoadControl("~/Modules/RefundRequests/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/RefundRequests/Filter.ascx"));
                page.AddControlToForm("Data", page.LoadControl("~/Modules/RefundRequests/Data.ascx"), this, "", Bll.Transactions.RefundRequest.SecuredObject);
			}
            else if (page.TemplateName == "Transaction")
            {
                page.AddControlToForm("Refund Requests", page.LoadControl("~/Modules/RefundRequests/TransView.ascx"), this, "", new Controls.SecuredObjectSelector(Bll.Transactions.Transaction.SecuredObject, Bll.Transactions.RefundRequest.SecuredObject).GetActivObject);
			}
		}

		void PageController_LoadItem(object sender, EventArgs e)
		{
			var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
			page.ItemID = page.PageController.DataKey["ID"].ToNullableInt();
			if (page.ItemID != null) page.SetItemData(Bll.Transactions.RefundRequest.Load(page.ItemID.GetValueOrDefault()));
            //if (page.GetItemData<Bll.Transaction.RefundRequest>() == null) page.SetItemData(new Bll.Transaction.RefundRequest());

            page.FormButtons.EnableDelete = false;
            page.FormButtons.EnableSave = false;
        }
	}
}