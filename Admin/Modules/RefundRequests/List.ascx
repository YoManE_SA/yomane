﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.RefundRequests.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Refund Requests">
    <Header>
        <admin:LegendColors runat="server" Items='<%# Netpay.Bll.Transactions.RefundRequest.StatusColor %>' FloatRight="true" />
    </Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList PageSize="20" runat="server" ID="rptList" SaveAjaxState="true" DataKeyNames="ID" AutoGenerateColumns="false" DisableRowSelect="true">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <span style="background-color: <%# Netpay.Bll.Transactions.RefundRequest.StatusColor.MapHtmlColor((Netpay.Bll.Transactions.RefundRequest.RequestStatus)Eval("Status")) %>;" class="legend-item"></span>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ID" SortExpression="ID">
                        <ItemTemplate>
                            <%# Eval("ID") %><br />
                            <asp:HiddenField runat="server" ID="hiddenID" Value='<%# Eval("ID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Date" SortExpression="Date">
                        <ItemTemplate>
                            <%# Eval("Date") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField HeaderText="Merchant ID" DataField="MerchantID" SortExpression="MerchantID" />
                    <asp:BoundField ItemStyle-CssClass="NoBreak" HeaderText="Payment Method Display" DataField="Transaction.PaymentMethodDisplay" SortExpression="Transaction.PaymentMethodDisplay" />
                    <asp:TemplateField HeaderText="Amount" SortExpression="Amount">
                        <ItemTemplate>
                            <div class="input-group">
                                <asp:TextBox runat="server" Enabled='<%# ((bool?)Eval("IsAllowChangeAmount")).HasValue? Eval("IsAllowChangeAmount") : false %>' ID="txtAmount" class="form-control" Text='<%# Eval("RefundAmount", "{0:0.00}") %>' />
                                <span class="input-group-addon">
                                    <asp:Literal runat="server" ID="ltCurrencyIso" Text='<%# Eval("CurrencyIso") %>' /></span>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Refund<br/>Fee">
                        <ItemTemplate>
                            <netpay:EnumDropDown
                                Width="85"
                                runat="server"
                                ID="ddlRefundFee"
                                CssClass="form-control"
                                ViewEnumName="Netpay.Bll.Transactions.RefundRequest+RefundType"
                                EnableBlankSelection="false"
                                Value='<%# (int)Eval("Type") %>'
                                Enabled='<%# Eval("IsPendingOrInProgress") %>'>
                            </netpay:EnumDropDown>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField HeaderText="Terminal Number" DataField="Transaction.TerminalNumber" SortExpression="Transaction.TerminalNumber" />
                    <asp:BoundField HeaderText="Transaction ID" DataField="Transaction.ID" SortExpression="Transaction.ID" />

                    <asp:TemplateField HeaderText="Flag">
                        <ItemTemplate>
                            <asp:UpdatePanel UpdateMode="Conditional" ID="upFlagImage" runat="server">
                                <ContentTemplate>
                                    <asp:ImageButton ID="btnFlagImage" OnCommand="btnFlagImage_Command" CommandArgument='<%# Eval("ID") %>' runat="server" ImageUrl='<%# Eval("FlagImageUrl") %>' />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- process checkbox --%>
                    <asp:TemplateField HeaderText="Process" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:MultiView runat="server" ActiveViewIndex='<%# (bool)Eval("IsEnableProcess") ? 0 : ((int)Eval("Status") == 51 ? 1 : 2) %>'>
                                <asp:View runat="server">
                                    <asp:RadioButton Enabled='<%# Eval("IsEnableProcess") %>' Text=" " runat="server" ID="rbProcess" GroupName='<%# "ACT_" + Eval("ID") %>' />
                                </asp:View>
                                <asp:View runat="server">
                                    <asp:Image runat="server" ImageUrl="~/Images/checkbox.gif" ImageAlign="Middle" />
                                </asp:View>
                                <asp:View runat="server">
                                    <asp:Image runat="server" ImageUrl="~/Images/checkbox_off.gif" ImageAlign="Middle" />
                                </asp:View>
                            </asp:MultiView>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- create checkbox --%>
                    <asp:TemplateField HeaderText="Create" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:MultiView runat="server" ActiveViewIndex='<%# (bool)Eval("IsEnableCreate") ? 0 : ((int)Eval("Status") == 50 ? 1 : 2)  %>'>
                                <asp:View runat="server">
                                    <asp:RadioButton Enabled='<%# Eval("IsEnableCreate") %>' Text=" " runat="server" ID="rbCreate" GroupName='<%# "ACT_" + Eval("ID") %>' />
                                </asp:View>
                                <asp:View runat="server">
                                    <asp:Image runat="server" ImageUrl="~/Images/checkbox.gif" ImageAlign="Middle" />
                                </asp:View>
                                <asp:View runat="server">
                                    <asp:Image runat="server" ImageUrl="~/Images/checkbox_off.gif" ImageAlign="Middle" />
                                </asp:View>
                            </asp:MultiView>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- cancel checkbox --%>
                    <asp:TemplateField HeaderText="Cancel" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:MultiView runat="server" ActiveViewIndex='<%# (bool)Eval("IsEnableCancel") ? 0 : ((int)Eval("Status") == 101 ? 1 : 2) %>'>
                                <asp:View runat="server">
                                    <asp:RadioButton Enabled='<%# Eval("IsEnableCancel") %>' Text=" " runat="server" ID="rbCancel" GroupName='<%# "ACT_" + Eval("ID") %>' />
                                </asp:View>
                                <asp:View runat="server">
                                    <asp:Image runat="server" ImageUrl="~/Images/checkbox.gif" ImageAlign="Middle" />
                                </asp:View>
                                <asp:View runat="server">
                                    <asp:Image runat="server" ImageUrl="~/Images/checkbox_off.gif" ImageAlign="Middle" />
                                </asp:View>
                            </asp:MultiView>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="Details">
                        <ItemTemplate>
                            <div class="input-group">
                                <asp:Button CssClass="btn btn-primary" runat="server" OnCommand="Load_Command" CommandArgument='<%# Eval("ID") %>' Text="Details" />
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <asp:Button runat="server" OnClick="btnProcess_Click" CssClass="btn btn-cons-short btn-primary margin-left-5 pull-right" Text="Submit" />
        <asp:Button runat="server" OnClick="btnReset_Click" CssClass="btn btn-cons-short btn-primary margin-left-5 pull-right" Text="Reset" />
        <admin:DataButtons EnableExport="true" runat="server" PagedControlID="rptList"></admin:DataButtons>
    </Footer>
</admin:ListSection>
