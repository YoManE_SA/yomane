﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Data.ascx.cs" Inherits="Netpay.Admin.RefundRequests.Data" %>
<admin:FormSections runat="server" Title="Details">
    <div class="row">
        <div class="col-lg-4">
            <div class="form-group">
                Refund Amount
			<asp:TextBox ID="txtRefundAmount" Enabled="false" CssClass="form-control" runat="server" Text='<%# ItemData.RefundAmount.GetValueOrDefault(ItemData.RequestAmount).ToString("0.00") %>' />

            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                Currency
                <br />
                <asp:Literal ID="ltCurrency" runat="server" Text='<%# ItemData.CurrencyIso %>' />
            </div>
        </div>
        <div class="col-lg-12">
            <div class="form-group">
                Comment
			<asp:TextBox ID="txtComment" CssClass="form-control" runat="server" TextMode="MultiLine" Text="<%# ItemData.Comment %>" />
            </div>
        </div>
        <div class="col-lg-12">
            <div class="form-group">
                Confirmation Number
			<asp:TextBox MaxLength="30" ID="txtConfirmationNumber" CssClass="form-control" runat="server" TextMode="SingleLine" Text="<%# ItemData.ConfirmationNum %>" />
            </div>
        </div>
        <div class="col-lg-12">
            <div class="form-group">
            <asp:Button ID="btnUpdate" CssClass="btn btn-primary" runat="server" CommandName="Save" CommandArgument='<%# ItemData.ID %>' Text="Update" OnCommand="GeneralCommand" />
            </div>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Log">
    <admin:AdminList EmptyDataRowStyle-CssClass="EmptyData" runat="server" ID="rptHistory" AutoGenerateColumns="false" BubbleLoadEvent="false" DisableRowSelect="true" SortDesc="true" SortKey="InsertDate" IsInListView="false">
        <Columns>
            <asp:BoundField HeaderText="Date" DataField="InsertDate" />
            <asp:BoundField HeaderText="Status" DataField="Status" />
            <asp:BoundField HeaderText="Description" DataField="Text" />
        </Columns>
        <EmptyDataTemplate>
            <div class="alert alert-info">
                No history found.
            </div>
        </EmptyDataTemplate>
    </admin:AdminList>
</admin:FormSections>

<%--<admin:FormSections runat="server" Title="Actions">
    <asp:Button ID="btnProcess" CssClass="btn btn-primary" runat="server" CommandName="Process" Enabled='<%# ItemData.IsEnableProcess %>' CommandArgument='<%# ItemData.ID %>' Text="Process" OnCommand="GeneralCommand" />
    <asp:Button ID="btnCreate" CssClass="btn btn-primary" runat="server" CommandName="Create" Enabled='<%# ItemData.IsEnableCreate %>' CommandArgument='<%# ItemData.ID %>' Text="Create" OnCommand="GeneralCommand" />
    <asp:Button ID="btnCancel" CssClass="btn btn-primary" runat="server" CommandName="Cancel" Enabled='<%# ItemData.IsEnableCancel %>' CommandArgument='<%# ItemData.ID %>' Text="Admin Cancel" OnCommand="GeneralCommand" />
</admin:FormSections>--%>
