﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Faqs
{
	public partial class GroupForm : System.Web.UI.UserControl
	{
		public Bll.Faqs.Group ItemData { get; set; }
		protected override void DataBindChildren()
		{
			if (ItemData == null) return;
			base.DataBindChildren();
		}

		public void Save(object sender, EventArgs e)
		{
            if (string.IsNullOrEmpty(txtName.Text))
            {
                GroupActionNotify.SetMessage("You can't save group without name.", true);
                return;
            }

            bool isnew = false;
			ItemData = Bll.Faqs.Group.Load(hfID.Value.ToNullableInt().GetValueOrDefault());
            if (ItemData == null)
            {
                ItemData = new Bll.Faqs.Group();
                ItemData.ID = Bll.Faqs.Group.NewID;
                isnew = true;
            }
			ItemData.Name = txtName.Text;
            ItemData.LanguageId = byte.Parse(ddlLanguage.SelectedValue);
            ItemData.ParentCompanyId = ddlParentCompany.SelectedValue != "0" ? int.Parse(ddlParentCompany.SelectedValue).ToNullableInt() : null;
            ItemData.TemplateRestriction = ItemData.ParentCompanyId.HasValue ? Bll.DataManager.ParentCompany.Get(ItemData.ParentCompanyId.Value).TemplateName : null;
			ItemData.IsVisible = chkIsVisible.Checked;
			ItemData.IsDevCenter = chkIsDevCenter.Checked;
			ItemData.IsMerchantCP = chkIsMerchantCP.Checked;
			ItemData.IsWallet = chkIsWallet.Checked;
			ItemData.IsWebsite = chkIsWebsite.Checked;
			ItemData.Save(isnew);
		}
		
		public void Delete(object sender, EventArgs e)
		{
			ItemData = Bll.Faqs.Group.Load(hfID.Value.ToNullableInt().GetValueOrDefault());
            if (ItemData != null)
            {
                if (ItemData.CurrentGroupQna.Count > 0)
                {
                    GroupActionNotify.SetMessage("You must delete all group's questions before you can delete it.",true);
                    return;
                }
                ItemData.Delete();
            }
		}

        protected void ddlParent_DataBound(object sender, EventArgs e)
        {
            ddlParentCompany.SelectedValue = hidParentCompany.Value;
        }
    }
}