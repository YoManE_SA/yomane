﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Faqs
{
	public partial class Form : System.Web.UI.UserControl
	{
		public Bll.Faqs.Faq ItemData { get; set; }
		protected override void DataBindChildren()
		{
			if (ItemData == null) return;
			base.DataBindChildren();
		}

		public void Save(object sender, EventArgs e)
		{
			ItemData = Bll.Faqs.Faq.Load(hfID.Value.ToNullableInt().GetValueOrDefault());
			if (ItemData == null) ItemData = new Bll.Faqs.Faq() {GroupId = hfGroup.Value.ToNullableInt() };
			ItemData.Question = txtQuestion.Text;
			ItemData.Answer = txtAnswer.Text;
			ItemData.Save();
		}

		public void Delete(object sender, EventArgs e)
		{
			ItemData = Bll.Faqs.Faq.Load(hfID.Value.ToNullableInt().GetValueOrDefault());
			if (ItemData != null) ItemData.Delete();
		}

	}
}