﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Netpay.Admin.Faqs.List" %>

<%@ Register Src="~/Modules/Faqs/Form.ascx" TagPrefix="CUC" TagName="Form" %>
<%@ Register Src="~/Modules/Faqs/GroupForm.ascx" TagPrefix="CUC" TagName="GroupForm" %>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">Faqs</div>

        <div class="panel-body">
            <div class="row">
                <div class="col-lg-2">
                    <netpay:DropDownBase runat="server" CssClass="form-control" ID="ddlGroupID" AutoPostBack="true" OnSelectedIndexChanged="Filter_Change" DataTextField="Name" DataValueField="ID" EnableBlankSelection="false" />
                    <br />
                </div>
                <div class="col-lg-1">
                    <NP:Button ID="btnManageGroups" runat="server" Text="Manage Groups" RegisterAsync="true" CssClass="btn btn-primary" CommandName="Edit" OnCommand="Group_Command" />
                    <br />
                </div>
                <div class="col-lg-9">
                    <NP:Button CssClass="btn btn-primary pull-right" runat="server" Text="AddItem" RegisterAsync="true" CommandName="AddItem" OnCommand="Faq_Command" />
                </div>
            </div>
            <NP:UpdatePanel runat="server" ID="upFaqs" UpdateMode="Conditional" RenderMode="Block" ChildrenAsTriggers="false">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel-group" id="accordion">
                                <netpay:DynamicRepeater runat="server" ID="rptList">
                                    <ItemTemplate>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <div class="row">
                                                    <div class="col-md-10">
                                                        <h4 class="panel-title">

                                                            <a data-toggle="collapse" data-parent="#accordion" href="#Faq<%# Eval("ID") %>"><%# Eval("Question") %></a>
                                                        </h4>
                                                    </div>
                                                    <div class="col-md-2 text-right">
                                                        <NP:Button CssClass="btn btn-default" runat="server" Text="Edit" RegisterAsync="true" CommandName="EditItem" CommandArgument='<%# Eval("ID") %>' OnCommand="Faq_Command" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="Faq<%# Eval("ID") %>" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <%# Eval("Answer") %>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                    <EmptyContainer>
                                        <div class="panel panel-info">
                                            <div class="panel-body">
                                                <div class="alert alert-info">
                                                    There are no questions in this group !
                                                </div>
                                            </div>
                                        </div>
                                    </EmptyContainer>
                                </netpay:DynamicRepeater>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlGroupID" EventName="SelectedIndexChanged" />
                </Triggers>
            </NP:UpdatePanel>
            <admin:ModalDialog runat="server" ID="dlgEdit" Title="Edit Faq" OnCommand="Faq_Command">
                <Body>
                    <CUC:Form runat="server" ID="editForm" />
                </Body>
                <Footer>
                    <admin:DataButtons runat="server" EnableSave="true" EnableDelete="true" />
                </Footer>
            </admin:ModalDialog>
            <admin:ModalDialog runat="server" ID="dlgGroups" Title="ManageGroups" OnCommand="Group_Command">
                <Body>
                    <admin:AdminList runat="server" DataKeyNames="ID" ID="rptGroupList" AutoGenerateColumns="false" BubbleLoadEvent="true">
                        <Columns>
                            <asp:BoundField HeaderText="Name" DataField="Name" />
                            <asp:CheckBoxField Text=" " HeaderText="Visible" DataField="IsVisible" />
                        </Columns>
                    </admin:AdminList>
                    <CUC:GroupForm runat="server" ID="editGroup" />
                </Body>
                <Footer>
                    <admin:DataButtons ID="DataButtons1" runat="server" EnableSave="true" EnableDelete="true" EnableAdd="true" />
                </Footer>
            </admin:ModalDialog>
        </div>
        <div class="panel-footer clearfix text-right">
        </div>
    </div>
</asp:Content>
