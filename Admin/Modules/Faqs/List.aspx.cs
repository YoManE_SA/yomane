﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Faqs
{
    public partial class List : Controls.ModulePage
    {
        public List() : base(Admin.Faqs.Module.ModuleName) { }

        protected override void OnLoad(EventArgs e)
        {
            ddlGroupID.DataBinding += delegate { ddlGroupID.DataSource = Bll.Faqs.Group.Search(null, null); };
            rptGroupList.DataBinding += delegate { rptGroupList.DataSource = Bll.Faqs.Group.Search(null, null); };
            rptList.DataBinding += delegate { rptList.DataSource = Bll.Faqs.Faq.Search(new Bll.Faqs.Faq.SearchFilters() { GroupId = ddlGroupID.Value.ToNullableInt() }, null); };
            base.OnLoad(e);
        }

        protected void Filter_Change(object sender, EventArgs e)
        {
            rptList.DataBind();
        }

        protected void Faq_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "EditItem":
                    editForm.ItemData = Bll.Faqs.Faq.Search(new Bll.Faqs.Faq.SearchFilters() { ID = new Range<int?>(e.CommandArgument.ToNullableInt()) }, null).SingleOrDefault();
                    editForm.DataBind();

                    if(!Bll.Faqs.Faq.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Edit))
                    {
                        dlgEdit.Enabled = false;
                    }
                    dlgEdit.RegisterShow();
                    break;

                case "AddItem":
                    editForm.ItemData = new Bll.Faqs.Faq() { GroupId = ddlGroupID.Value.ToNullableInt() };
                    dlgEdit.BindAndShow();
                    break;

                case "SaveItem":
                    editForm.Save(this, e);
                    dlgEdit.RegisterHide();
                    rptList.DataBind();
                    upFaqs.Update();
                    break;

                case "DeleteItem":
                    editForm.Delete(this, e);
                    dlgEdit.RegisterHide();
                    rptList.DataBind();
                    upFaqs.Update();
                    break;
            }
        }

        protected void Group_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Edit":
                    if (!Bll.Faqs.Faq.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Edit))
                    {
                        dlgGroups.Enabled = false;
                        dlgGroups.Body.Controls.OfType<Admin.Controls.AdminList>().Single().Enabled = false;
                    }
                    editGroup.ItemData = new Bll.Faqs.Group();
                    
                    dlgGroups.BindAndUpdate();
                    dlgGroups.RegisterShow();
                    break;

                case "LoadItem":
                    editGroup.ItemData = Bll.Faqs.Group.Load((e.CommandArgument as DataKey)[0].ToNullableInt().GetValueOrDefault());
                    editGroup.DataBind();
                    break;

                case "AddItem":
                    editGroup.ItemData = new Bll.Faqs.Group();
                    editGroup.DataBind();

                    rptGroupList.SelectedIndex = -1;
                    rptGroupList.DataBind();
                    break;

                case "SaveItem":
                    editGroup.Save(this, e);
                    editGroup.ItemData = new Bll.Faqs.Group();
                    editGroup.DataBind();

                    rptGroupList.SelectedIndex = -1;
                    rptGroupList.DataBind();

                    dlgGroups.BindAndUpdate();
                    break;

                case "DeleteItem":
                    editGroup.Delete(this, e);
                    editGroup.ItemData = new Bll.Faqs.Group();
                    editGroup.DataBind();


                    rptGroupList.SelectedIndex = -1;
                    rptGroupList.DataBind();

                    dlgGroups.BindAndUpdate();
                    break;

                case "DialogClose":
                    rptGroupList.SelectedIndex = -1;

                    ddlGroupID.Value = null;                    
                    ddlGroupID.DataBind();
                    break;
            }
        }
    }
}