﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GroupForm.ascx.cs" Inherits="Netpay.Admin.Faqs.GroupForm" %>

<hr />

<netpay:ActionNotify runat="server" ID="GroupActionNotify" />

<div class="row">
    <div class="col-lg-5">
        <div class="form-group">
            <asp:HiddenField runat="server" ID="hfID" Value='<%# ItemData.ID %>' />
            <asp:Label ID="Label2" runat="server" Text="Group Name" />
            <asp:TextBox runat="server" CssClass="form-control" ID="txtName" Text='<%# ItemData.Name %>' Enabled='<%# ItemData.ID == 0 %>' />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-5">
        <div class="form-group">
            <asp:Label runat="server" Text="Parent Compnay"></asp:Label>
            <asp:HiddenField ID="hidParentCompany" runat="server" Value='<%# ItemData.ParentCompanyId.HasValue? ItemData.ParentCompanyId.Value : 0 %>' />
            <netpay:ParentCompanyDropDown CssClass="form-control" OnDataBound="ddlParent_DataBound" runat="server" ID="ddlParentCompany" EnableBlankSelection="true" BlankSelectionValue="0"></netpay:ParentCompanyDropDown>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-5">
        <div class="form-group">
            <asp:Label runat="server" Text="Language" />
            <netpay:LanguageDropDown CssClass="form-control" runat="server" ID="ddlLanguage" Value='<%# ItemData.LanguageId %>' EnableBlankSelection="false"></netpay:LanguageDropDown>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <div class="modal-chkList">
                <asp:CheckBox class="ModalchkList-label" runat="server" ID="chkIsVisible" Text="Visible" Checked='<%# ItemData.IsVisible %>' />
                <asp:CheckBox class="ModalchkList-label" runat="server" ID="chkIsDevCenter" Text="DevCenter" Checked='<%# ItemData.IsDevCenter %>' />
                <asp:CheckBox class="ModalchkList-label" runat="server" ID="chkIsMerchantCP" Text="MerchantCP" Checked='<%# ItemData.IsMerchantCP %>' />
                <asp:CheckBox class="ModalchkList-label" runat="server" ID="chkIsWallet" Text="Wallet" Checked='<%# ItemData.IsWallet %>' />
                <asp:CheckBox class="ModalchkList-label" runat="server" ID="chkIsWebsite" Text="Website" Checked='<%# ItemData.IsWebsite %>' />
            </div>
        </div>
    </div>
</div>



