﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Form.ascx.cs" Inherits="Netpay.Admin.Faqs.Form" %>
<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <asp:HiddenField runat="server" ID="hfGroup" Value='<%# ItemData.GroupId %>' />
            <asp:HiddenField runat="server" ID="hfID" Value='<%# ItemData.ID %>' />
            <asp:TextBox CssClass="form-control" runat="server" ID="txtQuestion" Text='<%# ItemData.Question %>' /><br />
            <JQ:HtmlEdit runat="server" ID="txtAnswer" Text='<%# ItemData.Answer %>' />
        </div>
    </div>
</div>



