﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Faqs
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu mainMenuItem;

        public static readonly string ModuleName = "Faqs";
        public override string Name { get { return ModuleName; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return ""; } }

        public Module() : base(Bll.Faqs.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu item
            mainMenuItem = new ApplicationMenu("Faqs", "~/Modules/Faqs/List.aspx", "fa-question-circle", 300);

            //No route

            //No event
            
            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {               
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {            
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Settings", mainMenuItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            mainMenuItem.Parent.RemoveChild(mainMenuItem);
            base.OnUninstallAdmin(e);
        }

    }
}