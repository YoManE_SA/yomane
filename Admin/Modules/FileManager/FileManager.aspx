﻿<%@ Page Title="File Manager" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="True" CodeBehind="FileManager.aspx.cs" Inherits="Netpay.Admin.FileManager.FileManager" %>

<asp:Content ContentPlaceHolderID="BodyContent" runat="Server">
    <div class="row">
        <asp:Literal runat="server" ID="txtError" />
        <div class="col-lg-6">
            <admin:PanelSection runat="server" Title="Browse Files">
                <Body>
                     Browse and manage files on server
				        <div class="form-group" style="display: none;">
                            <div class="input-group multi-search">
                                <input type="text" class="form-control" placeholder="Search&hellip;  ">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-primary">Go</button>
                                </span>
                            </div>
                        </div>
                         <hr />
                    <NP:UpdatePanel runat="server" ID="updatetvFiles" UpdateMode="Conditional">
                        <ContentTemplate>
                            <JQ:TabControl runat="server">
                                <asp:Repeater runat="server" ID="rptFolders">
                                    <ItemTemplate>
                                        <JQ:TabView runat="server" Title="<%# Container.DataItem %>" ID="tvFiles">
                                            <div class="row">
                                                <div class="col-lg-12 head-tab-panel">Details</div>
                                                <div class="col-lg-12 tab-panel">
                                                    <asp:Literal runat="server" ID="txtFiles" />
                                                </div>
                                            </div>
                                        </JQ:TabView>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </JQ:TabControl>
                        </ContentTemplate>
                    </NP:UpdatePanel>
                </Body>
                <Footer>
                    <!-- Orginal Control return confirm('Continue with update files?'); -->
                    <asp:Button runat="server" ID="btnDelete" CssClass="btn btn-danger btn-cons-short pull-right" Text="Delete" OnClick="Delete_Click" OnClientClick="return confirm('Continue with delete files?');" />
                    <JQ:MenuButton runat="server" ID="btnMove" CssClass="btn-info" OnCommand="Move_Click" Text="Move To" />
                </Footer>
            </admin:PanelSection>
        </div>
        <div class="col-lg-6">
            <admin:PanelSection runat="server" Title="Upload Files" Icon="fa-code">
                <Header>
                    <div style="float: right;">
                        Debit Company:
                            <asp:DropDownList runat="server" ID="ddlHandlerDir" AutoPostBack="true" OnSelectedIndexChanged="ItemType_SelectedIndexChanged" EnableViewState="true" />
                    </div>
                </Header>
                <Body>
                    <div class="row">
                        <div class="col-lg-12">
                            Upload new files to server (more then one can be selected)
                            <hr />
                            <div class="form-group">
                                <div class="input-group multi-search">
                                    <span class="input-group-btn">
                                        <span class="btn btn-primary btn-file">Browse…
											<asp:FileUpload runat="server" ID="FileUpload1" AllowMultiple="true" onchange='<%# "adminFileManagerSelectedFilesHandler(this," + Netpay.Infrastructure.Application.MaxAdminFileManagerUploadSizeBytes + ")" %>' />
                                        </span>
                                    </span>
                                    <input class="form-control" readonly="" type="text">
                                </div>
                            </div>
                        </div>
                    </div>
                </Body>
                <Footer>
                    <img id="imgUploadProgress" src="../../Images/ajax-loader-1.gif" alt="" style="display: none;" />
                    <asp:Button runat="server" ID="btnUpload" Enabled="false" CssClass="btn btn-primary pull-right" OnClick="Upload_Click" Text="Upload" OnClientClick="if(ddlEpaType.selectedIndex < 1){alert('you must select file type.'); return false; }; $('#imgUploadProgress').show();" />
                </Footer>
            </admin:PanelSection>
        </div>
    </div>
</asp:Content>
