﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using System.IO;
using Netpay.Bll.EpaFileManager;
using Netpay.Infrastructure.Security;

namespace Netpay.Admin.FileManager
{
    
    public partial class FileManager : Controls.ModulePage, IComparer<System.IO.FileInfo>
    {
        public FileManager() : base("") { }

        private Bll.BankHandler.EpaFolder m_epaFolder;
        private Bll.BankHandler.BankHandlerManagerBase m_manager;
        private Bll.BankHandler.BankHandlerBase m_handler;

        private static readonly string SelectedDebitCompanyValue = "SelectedDebitCompanyValue";

        public string CurrentModuleNameByQs
        {
            get
            {
                if (Request["Type"] == null)
                    return null;
                else
                {
                    switch (Request["Type"])
                    {
                        case "AUTH": return Admin.FileMananger.Module.ModuleName; 

                        case "EPA": return Admin.FileManager.EpaModule.ModuleName;

                        case "CHB": return Admin.FileMananger.Module.ModuleName;

                        default: return null;
                    }
                }
               
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            m_epaFolder = Request.QueryString["Folder"].ToNullableEnumByName<Bll.BankHandler.EpaFolder>().GetValueOrDefault(Bll.BankHandler.EpaFolder.New);

            
            ModuleName = CurrentModuleNameByQs;

            base.OnLoad(e);
        }

        private bool UpdateHandlers(string bankDirName)
        {
            if (Request["Type"] == null)
                return false;

            switch (Request["Type"])
            {
                case "AUTH":
                    m_manager = Netpay.Bll.BankHandler.AuthHandlerManager.Current; break;

                case "EPA":
                    m_manager = Netpay.Bll.BankHandler.EpaHandlerManager.Current; break;

                case "CHB":
                    m_manager = Netpay.Bll.BankHandler.ChbHandlerManager.Current; break;
            }

            // If bankDirName is empty, select the first by default
            if (string.IsNullOrEmpty(bankDirName))
            {
                var firstHandler = m_manager.Handlers.FirstOrDefault();
                if (firstHandler != null)
                {
                    bankDirName = firstHandler.BankDirName;
                }
            }

            if (!string.IsNullOrEmpty(bankDirName))
            {
                ddlHandlerDir.DataSource = m_manager.Handlers.Select(i => i.BankDirName);
                m_handler = m_manager.Handlers.Where(v => v.BankDirName == bankDirName).SingleOrDefault();
            }

            return true;
        }

        protected override void DataBindChildren()
        {
            // Restore DebitCompany drop down selection
            if (ViewState[SelectedDebitCompanyValue] != null)
                ddlHandlerDir.SelectedValue = (string)ViewState[SelectedDebitCompanyValue];

            rptFolders.DataSource = System.Enum.GetValues(typeof(Bll.BankHandler.EpaFolder));

            // if (handler != null) handler = manager.Handlers.FirstOrDefault();

            base.DataBindChildren();
            if (UpdateHandlers(ddlHandlerDir.Text))
            {

                foreach (RepeaterItem item in rptFolders.Items)
                {
                    var forlder = (item.FindControl("tvFiles") as Controls.GlobalControls.JQueryControls.TabView).Title.ToNullableEnumByName<Bll.BankHandler.EpaFolder>().GetValueOrDefault();
                    var strOut = new System.Text.StringBuilder();
                    strOut.Append("<div class=\"row inner-table-titles\">");
                    strOut.Append("<div class=\"col-md-4\">File Name</div><div class=\"col-md-4\">Last Update</div><div class=\"col-md-4\">Select</div>");
                    strOut.Append("</div>");
                    RecDrawFiles(new System.IO.DirectoryInfo(m_handler.GetPath(forlder)), "", strOut, (forlder == Bll.BankHandler.EpaFolder.Archive ? 5 : int.MaxValue));
                    (item.FindControl("txtFiles") as Literal).Text = strOut.ToString();
                }
                btnMove.Items.Clear();
                btnMove.AddRange(m_manager.Handlers.Select(i => new MenuItem(i.BankDirName, i.BankDirName)));
            }
        }


        protected override void OnPreRender(EventArgs e)
        {
            Admin.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterScript(Page, "Plugins/Netpay-Plugin/UploadFile.js");
            DataBindChildren();
            base.OnPreRender(e);
        }
        
        protected void Upload_Click(object sender, EventArgs e)
        {
            //----------------------------------------------------
            rptFolders.DataSource = System.Enum.GetValues(typeof(Bll.BankHandler.EpaFolder));

            UpdateHandlers(ddlHandlerDir.Text);

            //   if (handler != null) handler = manager.Handlers.FirstOrDefault();
            //----------------------------------------------------

            if (Request.Files.Count > 0)
            {
                EpaFileLog item = new EpaFileLog();
                for (int idx = 0; idx < Request.Files.Count; idx++)
                {
                        HttpPostedFile file = Request.Files[idx];
                        byte[] data = ReadFile(file.InputStream);
                 //   updatetvFiles.Update();

                    Bll.BankHandler.EpaFolder forlder = default(Bll.BankHandler.EpaFolder);
                    var path = m_handler.GetPath(forlder); //file.FileName;
                    File.WriteAllBytes(path+ file.FileName, data);
                    item.ActionDate = DateTime.Now;
                    item.ActionType = (int)Netpay.Bll.BankHandler.LogAction.Uploaded;
                    item.OriginalFileName = file.FileName;
                    item.StoredFileName = path+file.FileName;
                    item.Save();
                }
            }
        }


        public static byte[] ReadFile(Stream input)
        {
            byte[] buffer = new byte[input.Length];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public int Compare(System.IO.FileInfo x, System.IO.FileInfo y)
        {
            return System.Math.Sign((y.LastWriteTime - x.LastWriteTime).TotalMilliseconds);
        }

        protected void RecDrawFiles(System.IO.DirectoryInfo dir, string padd, System.Text.StringBuilder strOut, int maxItems)
        {
            var directories = dir.GetDirectories();
            foreach (System.IO.DirectoryInfo d in directories)
            {
                strOut.Append("<td><br/></td></tr><td><img src=\"/NPCommon/Images/icon_folder.gif\" align=\"top\"> " + padd + d.Name + "<br/></td><td></td><td></td></tr>");
                RecDrawFiles(d, padd + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", strOut, maxItems);
            }
            var files = dir.GetFiles();
            if (files == null || files.Length == 0)
            {
                if (padd != string.Empty) strOut.Append("<td style=\"color:#cccccc!important;\">" + padd + "- Empty" + "</td></tr>");
                return;
            }
            Array.Sort(files, this);
            int nItem = 0;
            foreach (System.IO.FileInfo f in files)
            {
                nItem += 1;
                if (nItem > maxItems) break;
                strOut.Append("<div class=\"row\"><div class=\"col-md-4\">" + padd + f.Name + "</div><div class='col-md-4'>" + f.LastWriteTime.ToString("dd/MM/yyy HH:mm") + "</div><div class=\"col-md-4\"><input type=\"checkbox\" style=\"display: block !important;\" name=\"selectedFile\" value=\"" + f.FullName + "\"/></div></div>");
            }
            if (nItem < files.Length) strOut.Append("<div class=\"row\"><div class=\"col-md-12\" style=\"color:#cccccc!important;\">" + padd + "- More files exist" + "</div></div>");
        }

        private int DirFileCount(string dir)
        {
            int nRet = System.IO.Directory.GetFiles(dir).Length;
            var nDirs = System.IO.Directory.GetDirectories(dir);
            foreach (string d in nDirs)
                nRet += DirFileCount(d);
            return nRet;
        }

        public void Delete_Click(object sender, EventArgs e)
        {
            var fileNames = Request.Form.GetValues("selectedFile");
            if (fileNames == null) return;
            foreach (string f in fileNames)
            {
                try
                {
                    System.IO.File.Delete(f);
                }
                catch (Exception ex)
                {
                    txtError.Text = "Error - " + ex.Message + "<br/>";
                }
            }
        }

        public void Move_Click(object sender, EventArgs e)
        {
            var commandEventArgs = (CommandEventArgs)e;

            var fileNames = Request.Form.GetValues("selectedFile");
            if (fileNames == null) return;

            rptFolders.DataSource = System.Enum.GetValues(typeof(Bll.BankHandler.EpaFolder));

            UpdateHandlers((string)commandEventArgs.CommandArgument);

            Bll.BankHandler.EpaFolder forlder = default(Bll.BankHandler.EpaFolder);
            var path = m_handler.GetPath(forlder);

            foreach (string f in fileNames)
            {
                try
                {
                    System.IO.File.Move(f, path + System.IO.Path.GetFileName(f));
                }
                catch (Exception ex)
                {
                    txtError.Text = "Error - " + ex.Message + "<br/>";
                }
            }
        }
        protected void ItemType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlHandlerDir.SelectedValue = ddlHandlerDir.Text;
            ViewState[SelectedDebitCompanyValue] = ddlHandlerDir.SelectedValue;
        }
    }
  
}