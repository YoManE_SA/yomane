﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.FileMananger
{
	public class Module : Admin.CoreBasedModule
	{
        private ApplicationMenu menuChbItem, menuAuthItem;

        public static readonly string ModuleName = "CHB/Auth FileManager";
        public override string Name { get { return ModuleName; } }
		public override decimal Version { get { return 1.0m; } }
		public override string Author { get { return "OBL ltd."; } }
		public override string Description { get { return ""; } }

        public Module() : base(Bll.BankHandler.Module.Current) { }

        protected override void OnInit(EventArgs e)
		{            
            //Create menu items
            menuChbItem = new ApplicationMenu("CHB File Manager", "~/Modules/FileManager/FileManager.aspx?Type=CHB",null, 90);
            menuAuthItem = new ApplicationMenu("Auth File Manager", "~/Modules/FileManager/FileManager.aspx?Type=AUTH", null, 100);

            //no routes

            //no event

			base.OnInit(e);
		}
		protected override void OnActivate(EventArgs e)
		{	            
            base.OnActivate(e);
		}

		protected override void OnDeactivate(EventArgs e)
		{				
            base.OnDeactivate(e);
		}

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Transactions", menuAuthItem);
            Application.AddMenuItem("Transactions", menuChbItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuChbItem.Parent.RemoveChild(menuChbItem);
            menuAuthItem.Parent.RemoveChild(menuAuthItem);
            base.OnUninstallAdmin(e);
        }

    }
}