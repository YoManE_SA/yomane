﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.FileManager
{
    //This EpaModule file , is like all other Admin-Modules , 
    //But because the FileManager.aspx file is used in two different modules,
    //This for Epa , and  for CHB/AUTH , i created this EpaModule file that
    //Handles that file separately.
    public class EpaModule : Admin.CoreBasedModule
    {
        private ApplicationMenu menuEpaItem;

        public static readonly string ModuleName = "Epa FileManager";
        public override string Name { get { return ModuleName; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return "Epa File Manager"; } }

        public EpaModule() : base(Bll.BankHandler.EpaHandler.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu
            menuEpaItem = new ApplicationMenu("EPA File Manager", "~/Modules/FileManager/FileManager.aspx?Type=EPA", null, 110);

            //no route 

            //no event

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {            
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {            
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Finance", menuEpaItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuEpaItem.Parent.RemoveChild(menuEpaItem);
            base.OnUninstallAdmin(e);
        }
    }
}