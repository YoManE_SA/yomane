﻿using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Netpay.Admin.LogsDebitRules
{
    public partial class Filter : Controls.TemplateControlBase
    {
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
            base.OnLoad(e);            
        }

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
        {
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Netpay.Bll.DebitCompanies.DebitBlockLog.SearchFilters();

            //The Debit Company Name input:
            if (DebitCompanyNameFilter.Text != "")
                sf.DebitCompany = DebitCompanyNameFilter.Text;

            //The Rule Number Input
            int number;
            bool check = int.TryParse(RuleNumberFilter.Text, out number);
            if (check) sf.RuleNumber = number;

            //The Rule Type Input
            sf.RuleType = RuleTypeFilter.Value.ToNullableEnum<Netpay.Bll.DebitCompanies.eRuleType>();

            //Record Type Input
            if (RecordTypeFilter.Text != "")
                sf.RecordType = RecordTypeFilter.Text;

            //The date Range input
            sf.DateRange = DateRangeFilter.Value;


            //ID filter for testing
            //int numberId;
            //bool checkId = int.TryParse(IDfilter.Text, out numberId);
            //if (checkId) sf.ID = numberId;


            TemplatePage.SetFilter(sf);
        }

    }
}




