﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.LogsDebitRules.List" %>

<admin:ListSection ID="DebitRulesList" runat="server" Title="RULES & NOTIFICATIONS LOG">


    <Header>
        <admin:LegendColors runat="server" Items='<%# Netpay.Bll.DebitCompanies.DebitBlockLog.DebitRuleColor %>' FloatRight="true" />
    </Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" SaveAjaxState="true" ID="rptList" DataKeyNames="ID" AutoGenerateColumns="false" PageSize="20" BubbleLoadEvent="true" SortKey="dbl.ID" SortDesc="true">
                <Columns>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <span style="background-color: <%# Netpay.Bll.DebitCompanies.DebitBlockLog.DebitRuleColor.MapHtmlColor((Netpay.Bll.DebitCompanies.DebitRuleStatus)Eval("Status")) %>;" class="legend-item"></span>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%--The ID coloum--%>
                    <asp:TemplateField HeaderText="ID">
                        <ItemTemplate>
                            <%# Eval("ID") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%--The Date coloum--%>
                    <asp:TemplateField HeaderText="Date & Time" SortExpression="dbl.dbl_Date">
                        <ItemTemplate>
                            <%# Eval("Date") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%--Debit Company--%>
                    <asp:TemplateField HeaderText="Debit Company" SortExpression="dcincludempty.dc_name">
                        <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("DebitCompanyName")==null? "---": Eval("DebitCompanyName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%--Rule Type --%>
                    <asp:TemplateField HeaderText="Rule Type" SortExpression="drincludempty.dr_IsAutoDisable">
                        <ItemTemplate>
                            <asp:Label runat="server" Text='<%# GetRuleTypeName(Eval("RuleType") , Eval("RecordType")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%--Rule Number --%>
                    <asp:TemplateField HeaderText="Rule Number" SortExpression="dbl.dbl_DebitRule">
                        <ItemTemplate>
                            <asp:Label runat="server" Text='<%# (int)Eval("RuleNumber")==0? "---": Eval("RuleNumber") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%--Record Type --%>
                    <asp:TemplateField HeaderText="Record Type" SortExpression="dbl.dbl_Type">
                        <ItemTemplate>
                            <%# Eval("RecordType") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </admin:AdminList>
        </div>
    </Body>

    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd="false" />
    </Footer>

</admin:ListSection>


