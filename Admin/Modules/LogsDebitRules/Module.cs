﻿using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.LogsDebitRules
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuLog;
        public override string Name { get { return "LogsDebitRules"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return ""; } }

        public Module() : base(Bll.DebitCompanies.DebitCompanyBlockLogs.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu item
            menuLog = new ApplicationMenu("Debit Rules", "~/Logs/LogsDebitRules", null, 180);

            //Register route
            Application.RegisterRoute("Logs/LogsDebitRules", "LogsDebitRules", typeof(Controls.DataTablePage), module:this);

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {                        
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {                     
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Logs", menuLog);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuLog.Parent.RemoveChild(menuLog);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "LogsDebitRules")
            {
                page.PageController.LoadItem += PageController_LogsDebitRules_LoadItem;
                page.AddControlToList(page.LoadControl("~/Modules/LogsDebitRules/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/LogsDebitRules/Filter.ascx"));
                page.AddControlToForm("Data", page.LoadControl("~/Modules/LogsDebitRules/Data.ascx"), this, "", Bll.DebitCompanies.DebitBlockLog.SecuredObject);
            }
        }

        void PageController_LogsDebitRules_LoadItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            page.ItemID = page.PageController.DataKey["ID"].ToNullableInt();
            var item = Netpay.Bll.DebitCompanies.DebitBlockLog.Load(page.ItemID.GetValueOrDefault());
            if (page.ItemID != null) page.SetItemData(item);
            page.FormButtons.EnableSave = false;
            page.FormButtons.EnableDelete = false;

        }

    }
}