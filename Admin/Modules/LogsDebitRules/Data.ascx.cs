﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;
// Test change
namespace Netpay.Admin.LogsDebitRules
{
    public partial class Data : Controls.TemplateControlBase
    {
        protected Bll.DebitCompanies.DebitBlockLog Item
        {
            get { return TemplatePage.GetItemData<Bll.DebitCompanies.DebitBlockLog>(); }
            set { TemplatePage.SetItemData(value); }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            if (Item == null)
            {
                Item = new Bll.DebitCompanies.DebitBlockLog();
            }

            base.DataBindChildren();
        }
    }
}