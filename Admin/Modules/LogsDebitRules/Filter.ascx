﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.LogsDebitRules.Filter" %>

<admin:FilterSection runat="server" Title="RULES & NOTIFICATIONS - FILTER">

    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <asp:Label runat="server" Text="Debit Company" />
                <netpay:DropDownBase ID="DebitCompanyNameFilter" CssClass="form-control" runat="server" DataSource="<%#Netpay.Bll.DebitCompanies.DebitBlockLog.DebitCompaniesNames%>" />
            </div>
            <div class="form-group">
                Rule Type
        <netpay:EnumDropDown runat="server" ID="RuleTypeFilter" CssClass="form-control" ViewEnumName="Netpay.Bll.DebitCompanies.eRuleType" EnableBlankSelection="False" ClientIDMode="Inherit" />
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <asp:Label runat="server" Text="Rule Number" />
                <asp:TextBox runat="server" ID="RuleNumberFilter" CssClass="form-control" />
                <asp:RangeValidator ForeColor="Red" ControlToValidate="RuleNumberFilter" Display="Dynamic" ID="RuleNumberFilterRangeValidator" runat="server" ErrorMessage="Please insert a number bigger than 0." MinimumValue="1" MaximumValue="<%#Int32.MaxValue%>" Type="Integer" />
            </div>
            <div class="form-group">
                Record Type
        <netpay:DropDownBase ID="RecordTypeFilter" CssClass="form-control" runat="server" DataSource="<%#Netpay.Bll.DebitCompanies.DebitBlockLog.RecordTypeList%>" EnableBlankSelection="False" />
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label runat="server" Text="Date Range" />
                <JQ:DateRange runat="server" ID="DateRangeFilter" />
            </div>
        </div>
    </div>

</admin:FilterSection>
