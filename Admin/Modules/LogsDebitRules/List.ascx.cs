﻿using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

namespace Netpay.Admin.LogsDebitRules
{
    public partial class List : Controls.TemplateControlBase
    {
        protected override void OnLoad(EventArgs e)
        {
            rptList.DataBinding += List_DataBinding;
            base.OnLoad(e);
        }

        private void List_DataBinding(object sender, EventArgs e)
        {
            var list = Bll.DebitCompanies.DebitBlockLog.Search(TemplatePage.GetFilter<Bll.DebitCompanies.DebitBlockLog.SearchFilters>(), rptList);
            if (list != null)
            {
                rptList.DataSource = list;
            }
        }

 

        public string GetRuleTypeName(object ruletype, object dbl_type)
        {
            if ((string)dbl_type != "" && dbl_type!=null)
            {
                var txt = dbl_type.ToString();
                if (txt.StartsWith("MONTHLY"))
                {
                    return "CHB";
                }
            }

            if (ruletype != null)
            {
                var txt = ruletype.ToString();
                if (txt == "False") return "Warning";
                else if (txt == "True") return "Block";
            }

            if (ruletype == null)
            {
                return "---";
            }
            return "";
        }
    }
}