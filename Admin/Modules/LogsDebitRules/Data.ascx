﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Data.ascx.cs" Inherits="Netpay.Admin.LogsDebitRules.Data" %>
<admin:FormSections runat="server" Title="Details">
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <asp:Literal ID="Literal1" runat="server" Text="<%#Item.Text%>" />
                </div>
            </div>
        </div>

    </div>
</admin:FormSections>
