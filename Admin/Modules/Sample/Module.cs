﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Modules.Sample
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuSample;


        public static readonly string ModuleName = "Sample";
        public override string Name { get { return ModuleName; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return "Sample management"; } }

        public Module() : base(Bll.Sample.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Menu item
            menuSample = new ApplicationMenu("Sample", "~/Modules/Sample/Sample.aspx", null, 141);

            //Register route
            Application.RegisterRoute("Settings/Sample", "Sample", typeof(Netpay.Admin.Modules.Sample.Sample), module:this);

            //Register event
            Application.InitDataTablePage += Application_InitDataTablePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            //Add sub menu (adding to existing menu that was already added in the AddLocalModules function in Application.cs file)
            Application.AddMenuItem("Settings", menuSample);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuSample.Parent.RemoveChild(menuSample);
            base.OnUninstallAdmin(e);
        }

        private void Application_InitDataTablePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;
        }
    }
}