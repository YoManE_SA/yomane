﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Admin.Modules.Sample
{
    public partial class Sample : Controls.ModulePage
    {
        public Sample() : base(Admin.Modules.Sample.Module.ModuleName) { }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }
    }
}