﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="PaymentMethods.aspx.cs" Inherits="Netpay.Admin.Settings.PaymentMethods.PaymentMethods" %>

<asp:Content  ContentPlaceHolderID="BodyContent" runat="Server">

    <script type="text/javascript">

        function ValidatePage() {
            if (typeof (Page_ClientValidate) == 'function') {
                Page_ClientValidate();
            }

            if (Page_IsValid) {
                document.getElementById("UpdateButton").removeAttribute("disabled");
                document.getElementById("UpdateButton").style.opacity = "1.0";
                return;
            }
            else {
                var button = document.getElementById("UpdateButton");
                document.getElementById("UpdateButton").disabled = true;
                button.style.opacity = "0.5";
                Page_BlockSubmit = false;
            }
        }

        function CheckRegex(oSrc, args) {

            var isValid = true;
            try {
                new RegExp(args.Value);
            } catch (e) {
                isValid = false;
            }
            args.IsValid = isValid;
        }
    </script>

    <admin:ListSection  runat="server" Title="Payment Methods">
        <Header></Header>
        <Body>
            <NP:UpdatePanel runat="server" ID="upPaymentMethodList" RenderMode="Block" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <table class="table table-striped">
                        <netpay:DynamicRepeater runat="server" ID="rptList">
                            <HeaderTemplate>
                                <thead>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Abbreviation</th>
                                    <th>Is Popular</th>
                                    <th>Group</th>
                                    <th>Value1 Caption</th>
                                    <th>Value2 Caption</th>
                                    <th>Value1 Regex</th>
                                    <th>Value2 Regex</th>
                                    <th>Edit</th>
                                </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:MultiView ID="MultiViewSection" runat="server" ActiveViewIndex='<%# (int)Eval("ID")==EditableId ? 0 : 1 %>'>
                                    <asp:View runat="server">
                                        <tr>
                                            <td>
                                                <asp:Literal runat="server" ID="ltID" Text='<%# Eval("ID") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Name") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Abbreviation") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("IsPopular") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Group") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Value1EncryptedCaption") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Value2EncryptedCaption") %>' /></td>
                                            <td>
                                                <asp:TextBox runat="server" class="form-control" ID="txtValue1EncryptedValidationRegex" Text='<%# Eval("Value1EncryptedValidationRegex") %>' onblur="ValidatePage();" MaxLength="30" />
                                                <asp:CustomValidator
                                                    ControlToValidate="txtValue1EncryptedValidationRegex"
                                                    ForeColor="red"
                                                    ID="RegexValidator1"
                                                    runat="server"
                                                    ErrorMessage="Please insert a regular expression"
                                                    OnServerValidate="RegexValidator_ServerValidate"
                                                    ClientValidationFunction="CheckRegex"
                                                    ValidationGroup="regex">
                                                </asp:CustomValidator>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" class="form-control" ID="txtValue2EncryptedValidationRegex" Text='<%# Eval("Value2EncryptedValidationRegex") %>' onblur="ValidatePage();" MaxLength="30" />
                                                <asp:CustomValidator
                                                    ControlToValidate="txtValue2EncryptedValidationRegex"
                                                    ForeColor="red"
                                                    ID="RegexValidator2"
                                                    runat="server"
                                                    ErrorMessage="Please insert a regular expression"
                                                    OnServerValidate="RegexValidator_ServerValidate"
                                                    ClientValidationFunction="CheckRegex"
                                                    ValidationGroup="regex">
                                                </asp:CustomValidator>
                                            </td>
                                            <td>
                                                <asp:Button ID="UpdateButton" runat="server" CssClass="btn btn-success" Text="Update" CommandArgument='<%# Eval("ID") %>' OnCommand="UpdateButton_Command" ClientIDMode="Static" />
                                                <asp:Button ID="CancelButton" runat="server" CssClass="btn btn-default" Text="Cancel" OnCommand="CancelButton_Command" />
                                            </td>
                                        </tr>
                                    </asp:View>

                                    <asp:View runat="server">
                                        <tr>
                                            <td>
                                                <asp:Literal runat="server" ID="Literal1" Text='<%# Eval("ID") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Name") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Abbreviation") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("IsPopular") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Group") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Value1EncryptedCaption") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Value2EncryptedCaption") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Value1EncryptedValidationRegex") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Value2EncryptedValidationRegex") %>' /></td>
                                            <td>
                                                <asp:Button runat="server" Text="Edit" CssClass="btn btn-primary" CommandArgument='<%# Eval("ID") %>' OnCommand="EditButton_Command" />
                                            </td>
                                        </tr>
                                    </asp:View>
                                </asp:MultiView>
                            </ItemTemplate>
                        </netpay:DynamicRepeater>
                    </table>
                </ContentTemplate>
            </NP:UpdatePanel>
        </Body>
        <Footer>
            <admin:DataButtons runat="server" EnableSave="false" />
        </Footer>

    </admin:ListSection>

</asp:Content>

