﻿using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Settings.PaymentMethods
{
    public partial class PaymentMethods : Controls.ModulePage
    {
        public PaymentMethods() : base(Admin.Modules.Settings.PaymentMethodsMng.Module.ModuleName) { }
        public Bll.PaymentMethods.PaymentMethod Item { get; set; }

        public int EditableId { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            EditableId = -1;
            rptList.DataBinding += List_DataBinding;
            base.OnLoad(e);
        }

        protected void List_DataBinding(object sender, EventArgs e)
        {
            rptList.DataSource = Bll.PaymentMethods.PaymentMethod.Search(null, null);
        }

        protected void UpdateButton_Command(object sender, CommandEventArgs e)
        {
            int id;
            bool check = int.TryParse(e.CommandArgument.ToString(), out id);
            if (check)
            {
                Item = Bll.PaymentMethods.PaymentMethod.Get(id);
            }

            string selectedId = id.ToString();

            RepeaterItem selectedItem = rptList.Items
                                        .Cast<RepeaterItem>()
                                        .Where(i => ((Literal)i.FindControl("ltID")).Text == selectedId)
                                        .FirstOrDefault();


            TextBox txtValue1EncryptedValidationRegex = (TextBox)selectedItem.FindControl("txtValue1EncryptedValidationRegex");
            if ((txtValue1EncryptedValidationRegex.Text != "") && (txtValue1EncryptedValidationRegex.Text != null))
            {
                Item.Value1EncryptedValidationRegex = txtValue1EncryptedValidationRegex.Text;
            }
            else if ((txtValue1EncryptedValidationRegex.Text == "") || (txtValue1EncryptedValidationRegex.Text == null))
            {
                Item.Value1EncryptedValidationRegex = null;
            }


            TextBox txtValue2EncryptedValidationRegex = (TextBox)selectedItem.FindControl("txtValue2EncryptedValidationRegex");
            if ((txtValue2EncryptedValidationRegex.Text != "") && (txtValue2EncryptedValidationRegex.Text != null))
            {
                Item.Value2EncryptedValidationRegex = txtValue2EncryptedValidationRegex.Text;
            }
            else if ((txtValue2EncryptedValidationRegex.Text == "") || (txtValue2EncryptedValidationRegex.Text == null))
            {
                Item.Value2EncryptedValidationRegex = null;
            }

            if (Page.IsValid)
            {
                Item.Save();
                EditableId = -1;
                upPaymentMethodList.DataBind();
                upPaymentMethodList.Update();
            }
        }

        protected void EditButton_Command(object sender, CommandEventArgs e)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
            {
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.PaymentMethods.PaymentMethod.SecuredObject, PermissionValue.Edit);
            }


            int id;
            bool check = int.TryParse(e.CommandArgument.ToString(), out id);
            if (check) EditableId = id;

            upPaymentMethodList.DataBind();
            upPaymentMethodList.Update();
        }

        protected void CancelButton_Command(object sender, CommandEventArgs e)
        {
            EditableId = -1;
            upPaymentMethodList.DataBind();
            upPaymentMethodList.Update();
        }

        public static bool IsValidRegex(string pattern)
        {
            if (string.IsNullOrEmpty(pattern)) return false;

            try
            {
                Regex.Match("", pattern);
            }
            catch (ArgumentException)
            {
                return false;
            }

            return true;
        }

        protected void RegexValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = PaymentMethods.IsValidRegex(args.Value);
        }
    }
}