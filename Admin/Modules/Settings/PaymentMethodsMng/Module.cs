﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Modules.Settings.PaymentMethodsMng
{
    public class Module : Admin.CoreBasedModule
    {        
        private ApplicationMenu menuPaymentMethods;

        public static readonly string ModuleName = "Payment Methods Settings";
        public override string Name { get { return ModuleName; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return "Payment methods management"; } }

        public Module() : base(Bll.PaymentMethods.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu item
            menuPaymentMethods = new ApplicationMenu("Payment Methods", "~/Modules/Settings/PaymentMethodsMng/PaymentMethods.aspx", null, 260);

            //Register route
            Application.RegisterRoute("Settings/PaymentMethods", "PaymentMethods", typeof(Admin.Settings.PaymentMethods.PaymentMethods), module:this);
            
            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {            
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {            
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Settings", menuPaymentMethods);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuPaymentMethods.Parent.RemoveChild(menuPaymentMethods);
            base.OnUninstallAdmin(e);
        }
    }
}