﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Modules.Settings.PendingEventTypes
{
	public class Module : Admin.CoreBasedModule
	{        
        private ApplicationMenu menuPendingEventTypes;

        public static readonly string ModuleName = "PendingEventType Settings";
        public override string Name { get { return ModuleName; } }
		public override decimal Version { get { return 1.0m; } }
		public override string Author { get { return "OBL ltd."; } }
		public override string Description { get { return ""; } }

        public Module() : base(Bll.PendingEventTypesMng.Module.Current) { }
		protected override void OnInit(EventArgs e)
        { 
            menuPendingEventTypes = new ApplicationMenu("Pending Event Types", "~/Modules/Settings/PendingEventTypes/PendingEventTypes.aspx", null, 240);
            Application.RegisterRoute("Settings/PendingEventTypes", "PendingEventTypes", typeof(Admin.Modules.Settings.PendingEventTypes.PendingEventTypes), module:this);
            base.OnInit(e);
		}

		protected override void OnActivate(EventArgs e)
		{	           
            base.OnActivate(e);
		}

		protected override void OnDeactivate(EventArgs e)
		{            
            base.OnDeactivate(e);
		}

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Settings", menuPendingEventTypes);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuPendingEventTypes.Parent.RemoveChild(menuPendingEventTypes);
            base.OnUninstallAdmin(e);
        }
    }
}