﻿using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Modules.Settings.PendingEventTypes
{
    public partial class PendingEventTypes : Controls.ModulePage
    {
        public PendingEventTypes() : base(Modules.Settings.PendingEventTypes.Module.ModuleName) { }

        public int EditableId { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            EditableId = -1;
            rptList.DataBinding += List_DataBinding;
            base.OnLoad(e);
        }



        protected void List_DataBinding(object sender, EventArgs e)
        {
            rptList.DataSource = Bll.PendingEventTypes.Search(null, null);
        }

        protected void DeleteButton_Command(object sender, CommandEventArgs e)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
            {
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.PendingEventTypes.SecuredObject, PermissionValue.Delete);
            }

            var Item = new Bll.PendingEventTypes();
            int id;
            bool check = int.TryParse(e.CommandArgument.ToString(), out id);
            if (check)
            {
                Item = Bll.PendingEventTypes.Load(id);
            }

            Item.Delete();

            EditableId = -1;
            upEventTypeList.DataBind();
            upEventTypeList.Update();

        }

        protected void UpdateButton_Command(object sender, CommandEventArgs e)
        {
            var Item = new Bll.PendingEventTypes();
            int id;
            bool check = int.TryParse(e.CommandArgument.ToString(), out id);
            if (check)
            {
                Item = Bll.PendingEventTypes.Load(id);
            }

            // Get the selected item from the repeater.
            string selectedId = id.ToString();

            RepeaterItem selectedItem = rptList.Items
                                        .Cast<RepeaterItem>()
                                        .Where(i => ((Literal)i.FindControl("ltID")).Text == selectedId)
                                        .FirstOrDefault();

            TextBox txtName = (TextBox)selectedItem.FindControl("txtName");
            if ((txtName.Text != "") && (txtName.Text != null) && (txtName.Text.Length <= 50))
            {
                Item.Name = txtName.Text;
            }

            TextBox txtMinutesForWarning = (TextBox)selectedItem.FindControl("txtMinutesForWarning");
            if (txtMinutesForWarning.Text == "" || txtMinutesForWarning.Text == null)
            {
                Item.MinutesForWarning = null;
            }
            else
            {
                byte byteMinutesForWarning;
                if (byte.TryParse(txtMinutesForWarning.Text, out byteMinutesForWarning))
                {
                    Item.MinutesForWarning = byteMinutesForWarning;
                }
            }

            if (Page.IsValid)
            {
                Item.Save();
                EditableId = -1;
                upEventTypeList.DataBind();
                upEventTypeList.Update();
            }
        }

        protected void EditButton_Command(object sender, CommandEventArgs e)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
            {
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.PendingEventTypes.SecuredObject, PermissionValue.Edit);
            }

            int id;
            bool check = int.TryParse(e.CommandArgument.ToString(), out id);
            if (check) EditableId = id;

            upEventTypeList.DataBind();
            upEventTypeList.Update();
        }

        protected void CancelButton_Command(object sender, CommandEventArgs e)
        {
            EditableId = -1;
            upEventTypeList.DataBind();
            upEventTypeList.Update();
        }
    }

  
}