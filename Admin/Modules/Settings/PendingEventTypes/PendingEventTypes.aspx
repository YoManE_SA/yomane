﻿<%@  Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PendingEventTypes.aspx.cs" Inherits="Netpay.Admin.Modules.Settings.PendingEventTypes.PendingEventTypes" %>


<asp:Content ContentPlaceHolderID="BodyContent" runat="server">

    <script type="text/javascript">

        function ValidatePage() {

            if (typeof (Page_ClientValidate) == 'function') {
                Page_ClientValidate();
            }

            if (Page_IsValid) {

                document.getElementById("UpdateButton").removeAttribute("disabled");
                document.getElementById("UpdateButton").style.opacity = "1.0";
            }
            else {
                var button = document.getElementById("UpdateButton");
                document.getElementById("UpdateButton").disabled = true;
                button.style.opacity = "0.5";
                Page_BlockSubmit = false;
            }
        }
    </script>

    <admin:ListSection ID="EventTypesList" runat="server" Title="Pending Event Types">
        <Header></Header>
        <Body>
            <NP:UpdatePanel runat="server" ID="upEventTypeList" RenderMode="Block" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>

                    <table class="table table-striped">
                        <netpay:DynamicRepeater runat="server" ID="rptList">
                            <HeaderTemplate>
                                <tr>
                                    <thead>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Minutes For Warning</th>
                                        <th>Edit</th>
                                    </thead>
                                </tr>
                            </HeaderTemplate>


                            <ItemTemplate>
                                <asp:MultiView ID="MultiViewSection" runat="server" ActiveViewIndex='<%# (int)Eval("ID")==EditableId ? 0 : 1 %>'>
                                    <asp:View runat="server" ID="zero">
                                        <tr>
                                            <td>
                                                <asp:Literal ID="ltID" runat="server" Text='<%# Eval("ID") %>' />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtName" runat="server" class="form-control" Text='<%# Eval("Name") %>' MaxLength="50" onblur="ValidatePage();" />
                                                <asp:RequiredFieldValidator
                                                    ControlToValidate="txtName"
                                                    ForeColor="red"
                                                    ID="RequiredFieldValidatortxtName"
                                                    runat="server"
                                                    ErrorMessage="This field can't be empty."
                                                    ValidationGroup="pending">
                                                </asp:RequiredFieldValidator>
                                            </td>

                                            <td>
                                                <asp:TextBox ID="txtMinutesForWarning" runat="server" class="form-control" Text='<%# Eval("MinutesForWarning") %>' onblur="ValidatePage();" />
                                                <asp:RangeValidator
                                                    ForeColor="Red"
                                                    MinimumValue="0"
                                                    MaximumValue="<%#byte.MaxValue%>"
                                                    Type="Integer"
                                                    ID="RangeValidatortxtMinutesForWarning"
                                                    runat="server"
                                                    ErrorMessage="Please insert a number between 0 - 255."
                                                    ControlToValidate="txtMinutesForWarning"
                                                    ValidationGroup="pending">
                                                </asp:RangeValidator>
                                            </td>

                                            <td>
                                               
                                                <asp:Button ID="UpdateButton" runat="server" CssClass="btn btn-success" Text="Update" CommandArgument='<%# Eval("ID") %>' OnCommand="UpdateButton_Command" ClientIDMode="Static" />
                                                 <asp:Button ID="CancelButton" CssClass="btn btn-default" runat="server" Text="Cancel" OnCommand="CancelButton_Command" />
                                            </td>
                                        </tr>

                                    </asp:View>

                                    <asp:View runat="server" ID="one">
                                        <tr>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("ID") %>' />
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Name") %>' />
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("MinutesForWarning") %>' />
                                            </td>
                                            <td>
                                                <asp:Button runat="server" Text="Edit" CssClass="btn btn-primary"  CommandArgument='<%# Eval("ID") %>' OnCommand="EditButton_Command" />
                                                <asp:Button runat="server" Text="Delete" CssClass="btn btn-danger"  CommandArgument='<%# Eval("ID") %>' OnCommand="DeleteButton_Command" />
                                            </td>
                                        </tr>
                                    </asp:View>
                                </asp:MultiView>


                            </ItemTemplate>
                        </netpay:DynamicRepeater>
                    </table>

                </ContentTemplate>
            </NP:UpdatePanel>
        </Body>

        <Footer>
            <admin:DataButtons runat="server" EnableSave="false" EnableDelete="false" />
        </Footer>
    </admin:ListSection>
</asp:Content>

