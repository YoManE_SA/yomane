﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System.Text.RegularExpressions;
using System.Drawing;


namespace Netpay.Admin.Settings
{
    public partial class PrepaidPaymentMethods : Controls.ModulePage
    {
        public enum PrepaidPageType { PrepaidMethods, PrepaidGroups}
                
        public PrepaidPaymentMethods() : base(Modules.Settings.PrepaidPaymentMethods.Module.ModuleName)
        {
        }

        public PrepaidPageType Type { get; set; }

        public string PrePaidPageTypeAsString
        {
            get
            {
                return Type == PrepaidPageType.PrepaidGroups ? "Prepaid Groups" : "Prepaid Payment Methods";
            }
        }

        public int EditableId { get; set; }

        public bool Check { get; set; }

        protected override void OnInit(EventArgs e)
        {
            if (Request["Type"] == null)
            {
                throw new Exception("That page must have a 'Type' query param");
            }
            else
            {
                PrepaidPageType type;
                if (Enum.TryParse(Request["Type"], out type))
                {
                    Type = type;
                    Page.Title = Type == PrepaidPageType.PrepaidGroups ? "Prepaid Groups" : "Prepaid Payment Methods";
                }
                else
                {
                    throw new Exception("Can't convert query param Type to enum PrepaidPageType");
                }
            }

            mdError.DialogClose += MdError_DialogClose;

            base.OnInit(e);
        }

        private void MdError_DialogClose(object sender, CommandEventArgs e)
        {
            EditableId = 0;
            upEventTypeList.DataBind();
            upEventTypeList.Update();
        }

        protected override void OnLoad(EventArgs e)
        {
            EditableId = -1;
            rptList.DataBinding += List_DataBinding;

            //Refresh Cache data
            Infrastructure.Domain.Current.RemoveCachData("PaymentMethod");

            base.OnLoad(e);
        }

        protected void List_DataBinding(object sender, EventArgs e)
        {
            // check if Type == PrepaidGroups, if so, load the ones who
            // have BaseBin == null
            List<Bll.PaymentMethods.PaymentMethod> items = null;
            if (Type == PrepaidPageType.PrepaidGroups)
            {
                items = Netpay.Bll.PaymentMethods.PaymentMethod.CachePrepaid.Where(pm => pm.IsPrepaidGroup)
                        .Concat(Netpay.Bll.PaymentMethods.PaymentMethod.Cache.Where(x => x.ID == (int)CommonTypes.PaymentMethodEnum.CCTogglecard))
                        .ToList();
            }

            // check if Type == PrepaidMethods, if so, load the ones
            // who have BaseBin value
            else if (Type == PrepaidPageType.PrepaidMethods)
            {
                items = Netpay.Bll.PaymentMethods.PaymentMethod.CachePrepaid.Where(pm => pm.IsPrepaidMethod).ToList();
            }


            if (ViewState["IsNewItemAdded"] != null && (bool)ViewState["IsNewItemAdded"])
            {
                if (!items.Exists(x => x.ID == 0))
                    items.Insert(0, new Bll.PaymentMethods.PaymentMethod()
                    {
                        IsPull = false,
                        IsPMInfoMandatory = true,
                        IsTerminalRequired = true,
                        Type = CommonTypes.PaymentMethodType.CreditCard,
                        Group = CommonTypes.PaymentMethodGroupEnum.Prepaid
                    });
            }
            rptList.DataSource = items;
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int editid;

            switch (e.CommandName)
            {
                case "Update":
                    //Declare variables
                    int id;
                    Bll.PaymentMethods.PaymentMethod PPPMItem;

                    //Get the selected item id
                    Check = int.TryParse(e.CommandArgument.ToString(), out id);
                    if (Check)
                    {
                        //Validate values 
                        //Name column
                        if (string.IsNullOrEmpty((e.Item.FindControl("txtName") as TextBox).Text))
                        {
                            ListMessage.SetMessage("Name field can't be empty", true);
                            mdError.RegisterShow();
                            return;
                        }
                        //Abbreviation
                        if (((e.Item.FindControl("txtAbbreviation") as TextBox).Text).Length != 4)
                        {
                            ListMessage.SetMessage("Abbreviation must have 4 letters.", true);
                            mdError.RegisterShow();
                            return;
                        }
                        //BaseBin validation - Check if it has less than 6 char's or if it exist in DB.
                        //Validate only in PrepaidGroups mode, in Methods mode, value comes from a closed
                        //Drop down.
                        if (Type == PrepaidPageType.PrepaidGroups && (e.Item.FindControl("txtBaseBin") as TextBox).Text.Trim().Length != 6)
                        {
                            ListMessage.SetMessage("Base Bin must have 6 digits.", true);
                            mdError.RegisterShow();
                            return;
                        }
                        //Check if exist in db - validate only if it's a new item
                        if (Type == PrepaidPageType.PrepaidGroups && (id == 0) && Bll.PaymentMethods.CreditCardBin.CheckIfBinExist((e.Item.FindControl("txtBaseBin") as TextBox).Text.Trim()))
                        {
                            ListMessage.SetMessage("Base Bin already exist.", true);
                            mdError.RegisterShow();
                            return;
                        }

                        //Country
                        if (Type == PrepaidPageType.PrepaidGroups && (e.Item.FindControl("ddlCountry") as Netpay.Web.Controls.CountryDropDown).SelectedValue == "-1")
                        {
                            ListMessage.SetMessage("You must choose country.", true);
                            mdError.RegisterShow();
                            return;
                        }

                        if (id == 0) //new item
                        {
                            //TBD - handle the case that the new id is -1 ... 
                            int newPaymentMethodId = Bll.PaymentMethods.PaymentMethod.GetNewPrepaidPaymentMethodIdValue();
                            if (newPaymentMethodId == -1)
                            {
                                ListMessage.SetMessage("No place in DB to insert new Prepaid Payment Method.", true);
                                mdError.RegisterShow();
                                return;
                            }

                            //create a new payment method object.
                            PPPMItem = new Bll.PaymentMethods.PaymentMethod()
                            {
                                IsPull = false,
                                IsPMInfoMandatory = true,
                                IsTerminalRequired = true,
                                Type = CommonTypes.PaymentMethodType.CreditCard,
                                Group = CommonTypes.PaymentMethodGroupEnum.Prepaid,
                                Value1EncryptedCaption = "Card Number",
                                Value1EncryptedValidationRegex = "^([0-9]{16})$",
                                Value2EncryptedValidationRegex = "^([0-9]{3,4})$",
                                PendingCleanUpDays = 60,
                                PendingKeepAliveMinutes = 30
                            };

                            PPPMItem.ID = newPaymentMethodId;
                            PPPMItem.Name = (e.Item.FindControl("txtName") as TextBox).Text.Trim();
                            PPPMItem.Abbreviation = (e.Item.FindControl("txtAbbreviation") as TextBox).Text.Trim();
                            PPPMItem.IsBillingAddressMandatory = (e.Item.FindControl("chbIsBillingAddressMandatory") as CheckBox).Checked;
                            PPPMItem.IsPopular = (e.Item.FindControl("chbIsPopular") as CheckBox).Checked;
                            PPPMItem.IsExpirationDateMandatory = (e.Item.FindControl("IsExpDateMandatory") as CheckBox).Checked;
                            PPPMItem.IsPersonalIDRequired = (e.Item.FindControl("IsPersonalIDRequired") as CheckBox).Checked;

                            // save the BaseBin value, if the current page is PrepaidMethods, then save the base bin
                            // inside List.PaymentMethods, else, save the bin in tblCreditCardBin
                            PPPMItem.BaseBin =  Type == PrepaidPageType.PrepaidMethods? (e.Item.FindControl("ddlBaseBin") as Web.Controls.PrepaidBinsDropDown).SelectedValue : null;

                            PPPMItem.SavePrePaidItem(false);

                            if (Bll.PaymentMethods.PaymentMethod.IsIdExist(PPPMItem.ID) && Type == PrepaidPageType.PrepaidGroups) // Means that save seccssefully passed.
                            {
                                //Update tblCreditCardBin table ...
                                var BinItem = new Bll.PaymentMethods.CreditCardBin();
                                BinItem.BIN = (e.Item.FindControl("txtBaseBin") as TextBox).Text.Trim();
                                BinItem.CountryIsoCode = (e.Item.FindControl("ddlCountry") as Netpay.Web.Controls.CountryDropDown).SelectedCountry.IsoCode2;
                                BinItem.CCName = PPPMItem.Abbreviation;
                                BinItem.CCType = 50;//TheOne
                                BinItem.InsertDate = DateTime.UtcNow;
                                BinItem.PaymentMethodID = (CommonTypes.PaymentMethodEnum)PPPMItem.ID;
                                BinItem.Save();

                                //Save the icon only after save , because it uses the new generated id as the folder name
                                var fileUpload = e.Item.FindControl("fuFileUpload") as Controls.GlobalControls.FileUpload;
                                if (fileUpload.HasFile)
                                {
                                    //Check the image extention and size , if it's not gif or the size is not correct return.
                                    if (System.IO.Path.GetExtension(fileUpload.FileName) != ".gif")
                                    {
                                        ListMessage.SetMessage("The icon wasn't saved, it must be with extention \"gif\".", true);
                                        mdError.RegisterShow();
                                        return;
                                    }

                                    //Check the image size
                                    {
                                        using (System.Drawing.Image img = System.Drawing.Image.FromFile(fileUpload.LocalPath))
                                        {
                                            if ((img.Width != 23) || (img.Height != 12))
                                            {
                                                ListMessage.SetMessage("The icon wasn't saved, it must have size of 23X12.", true);
                                                mdError.RegisterShow();
                                                return;
                                            }
                                        }
                                        GC.Collect();
                                    }

                                    //Handle the icon
                                    string fileName = PPPMItem.ID.ToString() + System.IO.Path.GetExtension(fileUpload.FileName);
                                    string paymentMethodImageDir = Domain.Current.MapPaymentMethoddImage23On12;
                                    if (System.IO.Directory.Exists(paymentMethodImageDir))
                                    {
                                        string FileNameToSave = System.IO.Path.Combine(paymentMethodImageDir, fileName);
                                        fileUpload.SaveAs(FileNameToSave);
                                    }

                                }
                            }
                        }
                        else //existing item
                        {
                            //Get payment method item
                            PPPMItem = Bll.PaymentMethods.PaymentMethod.Get(id);

                            //Get Bin item connected to the Payment method - maybe it also need to be updated(Abbreviation and country values)
                            Bll.PaymentMethods.CreditCardBin BinItemToUpdate = Bll.PaymentMethods.CreditCardBin.Load(PPPMItem.BaseBin);

                            //Declare boolean values that will be a flag for updating or not.
                            bool IsPaymentMethodsItemNeedToBeUpdated = false;
                            bool IsBinItemNeedToBeUpdated = false;

                            //If BinItem was not found , return.
                            if (BinItemToUpdate == null)
                            {
                                ListMessage.SetMessage("Bin item not found.", true);
                                mdError.RegisterShow();
                                return;
                            }

                            //Check if country changed - only for PrepaidGroups mode
                            if (Type == PrepaidPageType.PrepaidGroups && (e.Item.FindControl("ddlCountry") as Netpay.Web.Controls.CountryDropDown).SelectedCountry.IsoCode2 != BinItemToUpdate.CountryIsoCode)
                            {
                                BinItemToUpdate.CountryIsoCode = (e.Item.FindControl("ddlCountry") as Netpay.Web.Controls.CountryDropDown).SelectedCountry.IsoCode2;
                                IsBinItemNeedToBeUpdated = true;
                            }

                            //Check if Name changed
                            if (PPPMItem.Name != (e.Item.FindControl("txtName") as TextBox).Text)
                            {
                                PPPMItem.Name = (e.Item.FindControl("txtName") as TextBox).Text;
                                IsPaymentMethodsItemNeedToBeUpdated = true;
                            }

                            //Check if abbreviation changed
                            if (PPPMItem.Abbreviation != (e.Item.FindControl("txtAbbreviation") as TextBox).Text)
                            {
                                PPPMItem.Abbreviation = (e.Item.FindControl("txtAbbreviation") as TextBox).Text;
                                IsPaymentMethodsItemNeedToBeUpdated = true;
                                if (Type == PrepaidPageType.PrepaidGroups)
                                {
                                    BinItemToUpdate.CCName = (e.Item.FindControl("txtAbbreviation") as TextBox).Text;
                                    IsBinItemNeedToBeUpdated = Type == PrepaidPageType.PrepaidGroups ? true : false;
                                }
                            }

                            //Check bool values
                            if (PPPMItem.IsBillingAddressMandatory != (e.Item.FindControl("chbIsBillingAddressMandatory") as CheckBox).Checked)
                            {
                                PPPMItem.IsBillingAddressMandatory = (e.Item.FindControl("chbIsBillingAddressMandatory") as CheckBox).Checked;
                                IsPaymentMethodsItemNeedToBeUpdated = true;
                            }

                            if (PPPMItem.IsPopular != (e.Item.FindControl("chbIsPopular") as CheckBox).Checked)
                            {
                                PPPMItem.IsPopular = (e.Item.FindControl("chbIsPopular") as CheckBox).Checked;
                                IsPaymentMethodsItemNeedToBeUpdated = true;
                            }

                            if (PPPMItem.IsExpirationDateMandatory != (e.Item.FindControl("IsExpDateMandatory") as CheckBox).Checked)
                            {
                                PPPMItem.IsExpirationDateMandatory = (e.Item.FindControl("IsExpDateMandatory") as CheckBox).Checked;
                                IsPaymentMethodsItemNeedToBeUpdated = true;
                            }

                            if (PPPMItem.IsPersonalIDRequired != (e.Item.FindControl("IsPersonalIDRequired") as CheckBox).Checked)
                            {
                                PPPMItem.IsPersonalIDRequired = (e.Item.FindControl("IsPersonalIDRequired") as CheckBox).Checked;
                                IsPaymentMethodsItemNeedToBeUpdated = true;
                            }


                            //Icon issue
                            var fileUpload = e.Item.FindControl("fuFileUpload") as Controls.GlobalControls.FileUpload;
                            if (fileUpload.HasFile)
                            {
                                //Check the image extention and size , if it's not gif or the size is not correct return.
                                if (System.IO.Path.GetExtension(fileUpload.FileName) != ".gif")
                                {
                                    ListMessage.SetMessage("The icon wasn't saved, it must be with extention \"gif\".", true);
                                    mdError.RegisterShow();
                                    return;
                                }

                                //Check the image size
                                {
                                    using (System.Drawing.Image img = System.Drawing.Image.FromFile(fileUpload.LocalPath))
                                    {
                                        if ((img.Width != 23) || (img.Height != 12))
                                        {
                                            ListMessage.SetMessage("The icon wasn't saved, it must have size of 23X12.", true);
                                            mdError.RegisterShow();
                                            return;
                                        }
                                    }
                                    GC.Collect();
                                }

                                //Handle the icon
                                string fileName = PPPMItem.ID.ToString() + System.IO.Path.GetExtension(fileUpload.FileName);
                                string paymentMethodImageDir = Domain.Current.MapPaymentMethoddImage23On12;
                                if (System.IO.Directory.Exists(paymentMethodImageDir))
                                {
                                    string FileNameToSave = System.IO.Path.Combine(paymentMethodImageDir, fileName);
                                    fileUpload.SaveAs(FileNameToSave);
                                }
                            }

                            if (IsPaymentMethodsItemNeedToBeUpdated) PPPMItem.SavePrePaidItem(true);
                            if (IsBinItemNeedToBeUpdated) BinItemToUpdate.Save();
                        }

                        //--- After Save ---//
                        //after save it's not a new item anymore.
                        ViewState["IsNewItemAdded"] = false;

                        //Refresh Cache data
                        Infrastructure.Domain.Current.RemoveCachData("PaymentMethod");

                        //Refresh the list
                        EditableId = -1;
                        
                        upEventTypeList.DataBind();
                        upEventTypeList.Update();
                    }
                    else //Selected id can't be parsed to int
                    {
                        ListMessage.SetMessage("Can't parse selected id.", true);
                        mdError.RegisterShow();
                        return;
                    }

                    break;

                case "Cancel":
                    ViewState["IsNewItemAdded"] = false;
                    Infrastructure.Domain.Current.RemoveCachData("PaymentMethod");

                    EditableId = -1;
                    upEventTypeList.DataBind();
                    upEventTypeList.Update();
                    break;

                case "Edit":
                    if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                    {
                        ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.PaymentMethods.Prepaid.Module.SecuredObject, PermissionValue.Edit);
                    }

                    Check = int.TryParse(e.CommandArgument.ToString(), out editid);
                    if (Check) EditableId = editid;

                    upEventTypeList.DataBind();
                    upEventTypeList.Update();
                    break;
            }
        }

        protected void AddPrepaidPaymentMethod(object sender, EventArgs e)
        {
            //Security check
            if (Netpay.Infrastructure.Security.Login.Current != null && Netpay.Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.PaymentMethods.Prepaid.Module.SecuredObject, PermissionValue.Add);

            // if the page type is PrepaidMethods, 
            // check if any prepaid group bin exist,
            // if not, throw show error message.
            if (Type == PrepaidPageType.PrepaidMethods)
            {
                if (Bll.PaymentMethods.CreditCardBin.PrepaidBins.Count() == 0)
                {
                    ListMessage.SetMessage("You must add at least one prepaid group with Bin before you can add prepaid methods.",true);
                    mdError.RegisterShow();
                    return;
                }
            }

            //Declare the viewState - used in the List_DataBinding function 
            //In order to know if to add a new item.
            ViewState["IsNewItemAdded"] = true;

            //Declare the editable item id=0 , because a new item has id=0.
            EditableId = 0;

            //Bind the data to the list.
            rptList.DataBind();
            upEventTypeList.DataBind();
            upEventTypeList.Update();
        }

        public string GetImagePath(int prepaidpaymentmethodid)
        {
            string imagePath =  Domain.Current.MapPaymentMethoddImage23On12 + "/" + prepaidpaymentmethodid + ".gif";
            if (System.IO.File.Exists(imagePath))
            {
                return Domain.Current.CommonVirtualPath + "ImgPaymentMethod" + "/" + "23X12" + "/" + prepaidpaymentmethodid + ".gif" + "?time=" + DateTime.Now.ToString(); // The time issue makes the image refresh even though the name is the same
            }
            else
            {
                return "";
            }
        }
        
        public string GetBaseBinCountry(string basebin)
        {
            var binItem = Bll.PaymentMethods.CreditCardBin.Load(basebin, false);
            if (binItem == null) return "-1";

            string isocode = binItem.CountryIsoCode;
            Match match = Regex.Match(isocode, "[^a-z]", RegexOptions.IgnoreCase);
            if (match.Success)
            {
                return "-1";
            }
            else
            {
                return Bll.Country.Get(isocode).ID.ToString();
            }
        }

        protected void rptList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (Type == PrepaidPageType.PrepaidMethods)
            {
                if (e.Item.ItemType == ListItemType.Header)
                {
                    var headerItem = e.Item.FindControl("thCountry");
                    if (headerItem != null)
                    {
                        ((System.Web.UI.HtmlControls.HtmlTableCell)headerItem).Visible = false;
                    }
                }


                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    var editable = e.Item.FindControl("tdEditableCountry");
                    if (editable != null)
                    {
                        ((System.Web.UI.HtmlControls.HtmlTableCell)editable).Visible = false;
                    }

                    var notEditable = e.Item.FindControl("tdNotEditableCountry");
                    if (notEditable != null)
                    {
                        ((System.Web.UI.HtmlControls.HtmlTableCell)notEditable).Visible = false;
                    }
                }
            }
        }
    }
}

