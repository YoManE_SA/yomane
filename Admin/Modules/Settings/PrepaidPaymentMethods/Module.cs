﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Modules.Settings.PrepaidPaymentMethods
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuPrepaidGroups;
        private ApplicationMenu menuPrepaidPaymentMethods;

        public static readonly string ModuleName = "Prepaid Paymentmethods Settings";
        public override string Name { get { return ModuleName; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return ""; } }

        public Module() : base(Bll.PaymentMethods.Prepaid.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu item for payment method groups
            menuPrepaidGroups = new ApplicationMenu("Prepaid Groups", "~/Modules/Settings/PrepaidPaymentMethods/PrepaidPaymentMethods.aspx?Type=PrepaidGroups", null, 270);
            
            //Create menu item for payment methods
            menuPrepaidPaymentMethods = new ApplicationMenu("Prepaid Payment Methods", "~/Modules/Settings/PrepaidPaymentMethods/PrepaidPaymentMethods.aspx?Type=PrepaidMethods", null, 271);

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {            
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {            
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Settings", menuPrepaidGroups);
            Application.AddMenuItem("Settings", menuPrepaidPaymentMethods);
            base.OnInstallAdmin(e);
        }
        
        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuPrepaidGroups.Parent.RemoveChild(menuPrepaidGroups);
            menuPrepaidPaymentMethods.Parent.RemoveChild(menuPrepaidPaymentMethods);
            base.OnUninstallAdmin(e);
        }
    }
}