﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PrepaidPaymentMethods.aspx.cs" Inherits="Netpay.Admin.Settings.PrepaidPaymentMethods" %>

<asp:Content ContentPlaceHolderID="BodyContent" runat="server">
    <NP:UpdatePanel runat="server" ID="upEventTypeList" RenderMode="Block" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>
            <admin:ListSection runat="server" Title='<%# PrePaidPageTypeAsString %>'>
                <Header>
                    <admin:DataButtons runat="server" EnableSave="false" EnableNew="true" OnNew="AddPrepaidPaymentMethod" />
                </Header>
                <Body>
                    <table class="table table-striped">
                        <netpay:DynamicRepeater runat="server" OnItemDataBound="rptList_ItemDataBound" ID="rptList" OnItemCommand="rptList_ItemCommand">
                            <HeaderTemplate>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Abbreviation</th>
                                        <th>Is Billing
                                        <br />
                                            Address Mandatory</th>
                                        <th>Is Popular</th>
                                        <th>Group</th>
                                        <th>Type</th>
                                        <th>Exp' Date<br />
                                            Mandatory</th>
                                        <th>Personal ID<br />
                                            Required</th>
                                        <th>Base Bin</th>
                                        <th runat="server" id="thCountry">Country</th>
                                        <th>Icon</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:MultiView ID="MultiViewSection" runat="server" ActiveViewIndex='<%# (int)Eval("ID")==EditableId ? 0 : 1 %>'>
                                    <%-- Editable mode --%>
                                    <asp:View runat="server" ID="zero">
                                        <tr>
                                            <td>
                                                <asp:Literal ID="ID" runat="server" Text='<%# Eval("ID") %>' />
                                            </td>
                                            <td>
                                                <asp:TextBox CssClass="form-control" runat="server" ID="txtName" Text='<%# Eval("Name") %>' MaxLength="12"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox CssClass="form-control" runat="server" ID="txtAbbreviation" Text='<%# Eval("Abbreviation") %>' MaxLength="4"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:CheckBox Text=" " runat="server" ID="chbIsBillingAddressMandatory" Checked='<%# Eval("IsBillingAddressMandatory") %>' />
                                            </td>
                                            <td>
                                                <asp:CheckBox Text=" " runat="server" ID="chbIsPopular" Checked='<%# Eval("IsPopular") %>' />
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Group") %>' />
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Type") %>' />
                                            </td>
                                            <td>
                                                <asp:CheckBox Text=" " runat="server" ID="IsExpDateMandatory" Checked='<%# Eval("IsExpirationDateMandatory") %>' />
                                            </td>
                                            <td>
                                                <asp:CheckBox Text=" " runat="server" ID="IsPersonalIDRequired" Checked='<%# Eval("IsPersonalIDRequired") %>' />
                                            </td>
                                            <td>
                                                <asp:MultiView ID="mvBaseBin" runat="server" ActiveViewIndex='<%# Type == PrepaidPageType.PrepaidGroups? 0 : 1 %>'>
                                                    <asp:View runat="server">
                                                        <asp:TextBox runat="server" ID="txtBaseBin" CssClass="form-control" MaxLength="6" Enabled='<%# (int)Eval("ID")==0 %>' Text='<%# Eval("BaseBin") %>'></asp:TextBox>
                                                    </asp:View>
                                                    <asp:View runat="server">
                                                        <netpay:PrepaidBinsDropDown Width="150" EnableBlankSelection="false" runat="server" ID="ddlBaseBin" CssClass="form-control" Value='<%# (int)Eval("ID") == 0? Netpay.Bll.PaymentMethods.CreditCardBin.PrepaidBins.First().BIN : Eval("BaseBin") %>' Enabled='<%# (int)Eval("ID")==0 %>'>
                                                        </netpay:PrepaidBinsDropDown>
                                                    </asp:View>
                                                </asp:MultiView>
                                            </td>
                                            <td runat="server" id="tdEditableCountry">
                                                <netpay:CountryDropDown CssClass="form-control" runat="server" ID="ddlCountry" EnableBlankSelection="true" BlankSelectionText="Choose Country" BlankSelectionValue="-1" Value='<%# (int)Eval("ID")==0? "-1" : GetBaseBinCountry(Eval("BaseBin").ToString()) %>' />
                                            </td>
                                            <td>
                                                <NP:FileUpload runat="server" ID="fuFileUpload" />
                                            </td>
                                            <td>
                                                <asp:Button CssClass="btn btn-default" runat="server" Text="Cancel" CommandName="Cancel" />
                                                <asp:Button CssClass="btn btn-success" runat="server" Text="Update" CommandName="Update" CommandArgument='<%# Eval("ID") %>' ClientIDMode="Static" />
                                            </td>
                                        </tr>
                                    </asp:View>
                                    <%-- Not editable mode --%>
                                    <asp:View runat="server" ID="one">
                                        <tr>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("ID") %>' />
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Name") %>'></asp:Literal>
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Abbreviation") %>'></asp:Literal>
                                            </td>
                                            <td>
                                                <asp:CheckBox Text=" " runat="server" Enabled="false" Checked='<%# Eval("IsBillingAddressMandatory") %>' />
                                            </td>
                                            <td>
                                                <asp:CheckBox Text=" " runat="server" Enabled="false" Checked='<%# Eval("IsPopular") %>' />
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Group") %>' />
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Type") %>' />
                                            </td>
                                            <td>
                                                <asp:CheckBox Text=" " runat="server" Enabled="false" Checked='<%# Eval("IsExpirationDateMandatory") %>' />
                                            </td>
                                            <td>
                                                <asp:CheckBox Text=" " runat="server" Enabled="false" Checked='<%# Eval("IsPersonalIDRequired") %>' />
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("BaseBin")  %>'></asp:Literal>
                                            </td>
                                            <td runat="server" id="tdNotEditableCountry">
                                                <%# Eval("BaseBin")==null ? "" : GetBaseBinCountry(Eval("BaseBin").ToString()) == "-1" ? "---" : Country.Get(int.Parse(GetBaseBinCountry(Eval("BaseBin").ToString()))).IsoCode2 %>
                                            </td>
                                            <td>
                                                <asp:Image runat="server" ImageUrl='<%# GetImagePath((int)Eval("ID")) %>' />
                                            </td>
                                            <td>
                                                <asp:Button runat="server" Text="Edit" CssClass="btn btn-primary" CommandArgument='<%# Eval("ID") %>' CommandName="Edit" />
                                            </td>
                                        </tr>
                                    </asp:View>
                                </asp:MultiView>
                            </ItemTemplate>
                        </netpay:DynamicRepeater>
                    </table>

                </Body>
                <Footer>
                </Footer>
            </admin:ListSection>
        </ContentTemplate>
    </NP:UpdatePanel>

    <netpay:ModalDialog runat="server" ID="mdError" Title="Error">
        <Body>
            <netpay:ActionNotify runat="server" ID="ListMessage" />
        </Body>
    </netpay:ModalDialog>
</asp:Content>
