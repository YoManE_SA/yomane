﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;

namespace Netpay.Admin.Currencies
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuCurrencies;

        public static readonly string ModuleName = "Currencies settings";
        public override string Name { get { return ModuleName; } }

        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return ""; } }

        public Module() : base(Netpay.Bll.Currencies.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu item
            menuCurrencies = new ApplicationMenu("Currencies", "~/Modules/Settings/Currencies/Currencies.aspx", null, 250);

            //Register route
            Application.RegisterRoute("Settings/Currencies", "Currencies", typeof(Netpay.Admin.Settings.Currencies.Currencies), module:this);

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {                        
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {                     
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Settings", menuCurrencies);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuCurrencies.Parent.RemoveChild(menuCurrencies);
            base.OnUninstallAdmin(e);
        }
    }
}