﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Text.RegularExpressions;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Admin.Settings.Currencies
{
	public partial class Currencies : Controls.ModulePage
	{
        public Currencies() : base(Admin.Currencies.Module.ModuleName) { }

        public int EditableId { get; set; }

		protected override void OnLoad(EventArgs e)
		{
            EditableId = -1;
            rptList.DataBinding += List_DataBinding;
            
			base.OnLoad(e);
		}

		protected void List_DataBinding(object sender, EventArgs e)
		{
			rptList.DataSource = Bll.Currency.Cache;
		}

        protected void UpdateButton_Command(object sender, CommandEventArgs e)
        {
            var Item = new Bll.Currency();
            int id;
            bool check = int.TryParse(e.CommandArgument.ToString(), out id);
            if (check)
            {
                Item = Bll.Currency.Get(id);
            }

            string selectedId = id.ToString();

            //Get the selected item from the repeater.
            RepeaterItem selectedItem = rptList.Items
                                        .Cast<RepeaterItem>()
                                        .Where(i => ((Literal)i.FindControl("ltID")).Text == selectedId)
                                        .FirstOrDefault();


            TextBox txtBaseRate = (TextBox)selectedItem.FindControl("txtBaseRate");
            if (txtBaseRate.Text != "" && txtBaseRate.Text != null)
            {
                decimal baseRateDecimalValue;
                if ((decimal.TryParse(txtBaseRate.Text, out baseRateDecimalValue)))
                {
                    Item.BaseRate = baseRateDecimalValue;
                }
            }
           

            TextBox txtMaxTransactionAmount = (TextBox)selectedItem.FindControl("txtMaxTransactionAmount");
            if (txtMaxTransactionAmount.Text == "" || txtMaxTransactionAmount.Text == null)
            {
                Item.MaxTransactionAmount = null;
            }
            else
            {
                decimal maxTransactionDecimalValue;
                if ((decimal.TryParse(txtMaxTransactionAmount.Text,out maxTransactionDecimalValue)))
                {
                    Item.MaxTransactionAmount = maxTransactionDecimalValue;
                }
            }
                       
            TextBox txtConversionFee = (TextBox)selectedItem.FindControl("txtConversionFee");
            if (txtConversionFee.Text != "" && txtConversionFee.Text != null)
            {
                decimal ConversionFeeDecimalValue;
                if ((decimal.TryParse(txtConversionFee.Text, out ConversionFeeDecimalValue)))
                {
                    Item.ConversionFee = ConversionFeeDecimalValue/100;
                }
            }
            
            Item.IsSymbolBeforeAmount = (selectedItem.FindControl("ddlIsSymboleBeforeAmount") as Netpay.Web.Controls.BoolDropDown).BoolValue.Value;

            if (Page.IsValid)
            {
                Item.Save();
                EditableId = -1;
                upEventTypeList.DataBind();
                upEventTypeList.Update();
            }
        //End of Update function.
        }

        protected void EditButton_Command(object sender, CommandEventArgs e)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
            {
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Currency.SecuredObject, PermissionValue.Edit);
            }

            int id;
            bool check = int.TryParse(e.CommandArgument.ToString(), out id);
            if (check) EditableId = id;

            upEventTypeList.DataBind();
            upEventTypeList.Update();
        }

        protected void CancelButton_Command(object sender, CommandEventArgs e)
        {
            EditableId = -1;
            upEventTypeList.DataBind();
            upEventTypeList.Update();
        }

        protected void Update_Command(object sender, CommandEventArgs e)
        {
            Bll.Updates.UpdateCurrencies();
            Domain.Current.RemoveCachData("Currencies");
            upEventTypeList.DataBind();
            upEventTypeList.Update();
        }
    }
}