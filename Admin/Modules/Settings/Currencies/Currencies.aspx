﻿<%@ Page Title="Currencies" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Currencies.aspx.cs" Inherits="Netpay.Admin.Settings.Currencies.Currencies" %>

<asp:Content ContentPlaceHolderID="BodyContent" runat="Server">

    <script type="text/javascript">

        function ValidatePage() {

            if (typeof (Page_ClientValidate) == 'function') {
                Page_ClientValidate();
            }

            if (Page_IsValid) {

                document.getElementById("UpdateButton").removeAttribute("disabled");
                document.getElementById("UpdateButton").style.opacity = "1.0";
            }
            else {
                var button = document.getElementById("UpdateButton");
                document.getElementById("UpdateButton").disabled = true;
                button.style.opacity = "0.5";
                Page_BlockSubmit = false;
            }
        }
    </script>

    <admin:ListSection runat="server" Title="Currencies">
        <Header></Header>
        <Body>
            <NP:UpdatePanel runat="server" ID="upEventTypeList" RenderMode="Block" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>

                    <table class="table table-striped">
                        <netpay:DynamicRepeater runat="server" ID="rptList">
                            <HeaderTemplate>
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Symbol</th>
                                        <th>ISO3Code</th>
                                        <th>ISONumber</th>
                                        <th>IsSymbolBeforeAmount</th>
                                        <th>Rate</th>
                                        <th>Request Date</th>
                                        <th>Value Date</th>
                                        <th>Max Trans Amount</th>
                                        <th>Fee</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
                            </HeaderTemplate>

                            <ItemTemplate>
                                <asp:MultiView ID="MultiViewSection" runat="server" ActiveViewIndex='<%# (int)Eval("ID")==EditableId ? 0 : 1 %>'>
                                    <asp:View runat="server" ID="zero">

                                        <tr>
                                            <td>
                                                <asp:Literal runat="server" ID="ltID" Text='<%# Eval("ID") %>' Visible="false" /><asp:Literal runat="server" Text='<%# Eval("Name") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Symbol") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("IsoCode") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("ISONumber") %>' /></td>
                                            <td>
                                                <netpay:BoolDropDown CssClass="form-control" EnableBlankSelection="false" runat="server" ID="ddlIsSymboleBeforeAmount" TrueText="True" FalseText="False" BoolValue='<%# (bool)Eval("IsSymbolBeforeAmount") %>'>
                                                </netpay:BoolDropDown>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtBaseRate" Text='<%# Eval("BaseRate") %>' class="form-control" onblur="ValidatePage();" />
                                                <asp:RangeValidator
                                                    ForeColor="red"
                                                    ID="RangeValidatortxtBaseRate"
                                                    ControlToValidate="txtBaseRate"
                                                    runat="server"
                                                    ErrorMessage="Insert a positive number."
                                                    MinimumValue="0"
                                                    MaximumValue="<%#Int32.MaxValue %>"
                                                    Type="Double"
                                                    ValidationGroup="fee">
                                                </asp:RangeValidator>
                                                <asp:RequiredFieldValidator
                                                    ForeColor="red"
                                                    ID="RequiredFieldValidatortxtBaseRate"
                                                    ControlToValidate="txtBaseRate"
                                                    runat="server"
                                                    ErrorMessage="This field can't be empty."
                                                    ValidationGroup="fee">
                                                </asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("RateRequestDate") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("RateValueDate") %>' /></td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtMaxTransactionAmount" class="form-control" Text='<%# (Eval("MaxTransactionAmount")) %>' onblur="ValidatePage();" />
                                                <asp:RangeValidator
                                                    ForeColor="red"
                                                    ID="RangeValidatortxtMaxTransactionAmount"
                                                    ControlToValidate="txtMaxTransactionAmount"
                                                    runat="server"
                                                    ErrorMessage="Insert a positive number."
                                                    MinimumValue="0"
                                                    MaximumValue="<%#Int32.MaxValue %>"
                                                    Type="Double"
                                                    ValidationGroup="fee">
                                                </asp:RangeValidator>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtConversionFee" class="form-control" Text='<%# string.Format("{0:0.00}",((decimal)Eval("ConversionFee")*100))  %>' onblur="ValidatePage();" />
                                                <asp:RequiredFieldValidator
                                                    ForeColor="red"
                                                    ID="RequiredFieldValidatortxtConversionFee"
                                                    ControlToValidate="txtConversionFee"
                                                    runat="server"
                                                    ErrorMessage="This field can't be empty."
                                                    ValidationGroup="fee">
                                                </asp:RequiredFieldValidator>
                                                <asp:RangeValidator
                                                    ForeColor="red"
                                                    ID="RangeValidator1"
                                                    ControlToValidate="txtConversionFee"
                                                    runat="server"
                                                    ErrorMessage="Insert a positive number."
                                                    MinimumValue="0"
                                                    MaximumValue="100"
                                                    Type="Double"
                                                    ValidationGroup="fee">
                                                </asp:RangeValidator>
                                            </td>
                                            <td>
                                                <asp:Button ID="CancelButton" CssClass="btn btn-default" runat="server" Text="Cancel" OnCommand="CancelButton_Command" />
                                                <asp:Button ID="UpdateButton"  CssClass="btn btn-success" runat="server" Text="Update" CommandArgument='<%# Eval("ID") %>' OnCommand="UpdateButton_Command" ClientIDMode="Static" />
                                            </td>
                                        </tr>
                                    </asp:View>

                                    <asp:View runat="server" ID="one">
                                        <tr>
                                            <td>
                                                <asp:Literal runat="server" ID="Literal1" Text='<%# Eval("ID") %>' Visible="false" /><asp:Literal runat="server" Text='<%# Eval("Name") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("Symbol") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("IsoCode") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("ISONumber") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("IsSymbolBeforeAmount") %>' /></td>

                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("BaseRate") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("RateRequestDate") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("RateValueDate") %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# string.Format("{0:N}",Eval("MaxTransactionAmount")) %>' /></td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# string.Format("{0:P}",Eval("ConversionFee")) %>' />
                                            </td>
                                            <td>
                                                <asp:Button runat="server"  CssClass="btn btn-primary" Text="Edit" CommandArgument='<%# Eval("ID") %>' OnCommand="EditButton_Command" />
                                            </td>
                                        </tr>

                                    </asp:View>
                                </asp:MultiView>

                            </ItemTemplate>
                        </netpay:DynamicRepeater>
                    </table>
                </ContentTemplate>
            </NP:UpdatePanel>
        </Body>
        <Footer>
            <asp:Button Text="Update" Visible="false" CssClass="btn btn-primary" runat="server" OnCommand="Update_Command" />
            <admin:DataButtons runat="server" EnableSave="false" />
        </Footer>
    </admin:ListSection>
</asp:Content>

