﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.ActionPage
{
   
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuActionPage;

        public static readonly string ModuleName = "Action Page";
        public override string Name { get { return ModuleName; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return ""; } }

        public Module() : base(Bll.ActionPage.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu item
            menuActionPage = new ApplicationMenu("Action Page", "~/Modules/Settings/ActionPage/ActionPage.aspx", null, 290);

            //Register route
            Application.RegisterRoute("Settings/ActionPage", "Action Page", typeof(ActionPage.Module), module:this);

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Settings", menuActionPage);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuActionPage.Parent.RemoveChild(menuActionPage);
            base.OnUninstallAdmin(e);
        }
    }
}