﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Text.RegularExpressions;
using Netpay.Bll.BankHandler;

namespace Netpay.Admin.Settings.ActionPage
{
    public partial class ActionPage : Controls.ModulePage
    {
        public ActionPage() : base(Admin.ActionPage.Module.ModuleName) { }

        //Each function here will have to have a security check with Bll.ActionPage.Module.SecurityObject.
        protected void GenerateAuth_Click(object sender, CommandEventArgs e)
        {
            Netpay.Bll.BankHandler.AuthHandlerManager.ExecGenerate(typeof(eCentricAuthHandler));
        }

    
        protected void rptWireProviders_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            var prv = Netpay.Bll.Wires.Provider.Get(e.CommandArgument.ToString());
            if (prv == null) return;
            prv.NextBetchNumber = Convert.ToInt32((e.Item.FindControl("txtNextBetchNumber") as System.Web.UI.WebControls.TextBox).Text);
            rptWireProviders.DataBind();
        }

        protected void btnSetMerchantGroup_Click(object sender, EventArgs e)
        {
            int groupID = int.Parse(ddlMerchantGroup.SelectedValue);

            if (Bll.Merchants.Merchant.IsMerchantWithoutGroupExist)
            {
                try
                {
                    Bll.Merchants.Merchant.SetMerchantsGroups(groupID);
                }
                catch (Exception ex)
                {
                    //Set message if error happened.
                    merchantGroupsNotify.SetException(ex);
                }

                //Set success message.
                merchantGroupsNotify.SetSuccess("Group " +"\"" + ddlMerchantGroup.SelectedItem.Text + "\"" + " assigned to all merchants without a group.");
            }
            else
            {
                merchantGroupsNotify.SetMessage("There are no merchants without group.", true);
            }

            //Update screen
            MerchantGroupFormPanel.Update();
        }
    }
}