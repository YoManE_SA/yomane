//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Netpay.Admin.Settings.ActionPage {
    
    
    public partial class ActionPage {
        
        /// <summary>
        /// rptWireProviders control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Repeater rptWireProviders;
        
        /// <summary>
        /// MerchantGroupFormPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Netpay.Admin.Controls.FormPanel MerchantGroupFormPanel;
        
        /// <summary>
        /// merchantGroupsNotify control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Netpay.Web.Controls.ActionNotify merchantGroupsNotify;
        
        /// <summary>
        /// ddlMerchantGroup control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Netpay.Web.Controls.MerchantGroupsDropDown ddlMerchantGroup;
        
        /// <summary>
        /// btnSetMerchantGroup control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnSetMerchantGroup;
    }
}
