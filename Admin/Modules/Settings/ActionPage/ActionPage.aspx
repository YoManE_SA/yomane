<%@ Page Title="Actions Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="ActionPage.aspx.cs" Inherits="Netpay.Admin.Settings.ActionPage.ActionPage" %>

<asp:Content ContentPlaceHolderID="BodyContent" runat="Server">
    <div class="panel panel-default">
        <div class="panel-heading">Actions Page</div>
        <div class="panel-body">
            <div class="col-md-12">
                <admin:FormSections runat="server" Title="Auth Handlers">
                    <div class="row">
                        <div class="col-lg-6">
                            eCentric Auth Handler  
                            <asp:Button CssClass="btn btn-success" runat="server" Text="Execute" OnCommand="GenerateAuth_Click" />
                        </div>
                    </div>
                </admin:FormSections>
                <admin:FormSections runat="server" Title="Wire Providers Next File Indexes">
                    <div class="row">
                        <div class="col-lg-3">
                            <asp:Repeater OnItemCommand="rptWireProviders_ItemCommand" runat="server" ID="rptWireProviders" DataSource='<%# Netpay.Bll.Wires.Provider.ActionPageCache %>'>
                                <ItemTemplate>
                                    <%# Eval("FriendlyName") %>
                                    <div class="input-group">
                                        <asp:TextBox runat="server" ID="txtNextBetchNumber" CssClass="form-control" Text='<%# Eval("NextBetchNumber") %>' />
                                        <span class="input-group-btn">
                                            <asp:LinkButton runat="server" CssClass="btn btn-info" Text="Update" CommandArgument='<%# Eval("Name") %>' />
                                        </span>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </admin:FormSections>
                <admin:FormPanel ID="MerchantGroupFormPanel" runat="server" Title="Set merchants without groups">
                    <div class="row">
                        <div class="col-lg-5">
                            <netpay:ActionNotify runat="server" ID="merchantGroupsNotify" />
                        </div>     
                    </div>               
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="alert alert-info">
                                By pressing that button you will assign all unassigned merchants to the chosen group.
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2">
                          <netpay:MerchantGroupsDropDown CssClass="form-control" ID="ddlMerchantGroup" runat="server" EnableBlankSelection="false">
                          </netpay:MerchantGroupsDropDown>
                        </div>
                        <div class="col-lg-3">
                            <asp:Button Text="Set merchant groups" CssClass="btn btn-primary" ID="btnSetMerchantGroup" runat="server" OnClick="btnSetMerchantGroup_Click" />
                        </div>
                    </div>
                </admin:FormPanel>
            </div>
        </div>
    </div>
</asp:Content>


