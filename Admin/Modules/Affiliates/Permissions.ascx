﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Permissions.ascx.cs" Inherits="Netpay.Admin.Affiliates.Permissions" %>
<admin:FormSections runat="server" Title="Allow Pages">
    <asp:MultiView runat="server" ActiveViewIndex="<%#Account!=null && Account.AffiliateID != null && Account.LoginID!=null ? 0 : 1 %>">
        <asp:View runat="server">
            <asp:CheckBoxList runat="server" CssClass="checkbox-list" ID="chlAllowPages" RepeatLayout="UnorderedList">
            </asp:CheckBoxList>
        </asp:View>
        <asp:View runat="server">
            <div class="alert alert-info">
                Save affiliate and afterwards insert correct login details in order to edit this section.
            </div>
        </asp:View>
    </asp:MultiView>
</admin:FormSections>

<admin:ModalDialog runat="server" ID="dlgCountryNotSelectedError" Title="Save Affiliate Error Msg">
    <Body>
        <netpay:ActionNotify runat="server" ID="SaveAffiliateErrorMsg" />
    </Body>
</admin:ModalDialog>
