﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="MerchantSettings.ascx.cs" Inherits="Netpay.Admin.Affiliates.MerchantSettings" %>
<admin:FormPanel ID="frmSection" runat="server" Title="Add Merchant">
    <asp:MultiView runat="server" ActiveViewIndex='<%# (TemplatePage.Page is Netpay.Admin.Controls.AccountPage) && TemplatePage.ItemID != null && TemplatePage.ItemID != 0 ? 0 : 1 %>'>
        <asp:View runat="server">
            <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                 Select Merchant:
                <admin:AccountPicker runat="server" ID="apMerchant" LimitToType="Merchant" UseTargetID="false" />
            </div>
        </div>
        <div class="col-md-6">
            <br />
            <asp:Button runat="server" CssClass="btn btn-primary" Text="Add" CausesValidation="false" CommandName="Add" OnCommand="Merchants_ItemCommand" />
        </div>
    </div>
    <div class="row inner-table-titles spacer">
        <div class="col-md-2 col-lg-2">ID</div>
        <div class="col-md-4 col-lg-4">Merchant</div>
        <div class="col-md-2 col-lg-2">UserID</div>
        <div class="col-md-2 col-lg-2">Display %</div>
        <div class="col-md-2 col-lg-2">Delete</div>
    </div>
    <netpay:DynamicRepeater ID="rptMerchants" runat="server" OnItemCommand="Merchants_ItemCommand" >
        <ItemTemplate>
            <div class="row table-ui">
                <div class="col-md-2 col-lg-2">
                    <asp:Literal ID="Literal1" runat="server" Text='<%#Eval("ID")%>' />
                </div>
                <div class="col-md-4 col-lg-4">
                    <asp:HiddenField runat="server" ID="hfMerchantID" Value='<%#Eval("MerchantID")%>' />
                    <asp:Literal ID="ltMerchantName" runat="server" Text='<%# GetName((int)Eval("MerchantID"))%>' />
                </div>
                <div class="col-md-4 col-lg-2">
                    <asp:TextBox CssClass="form-control" ID="txtUserID" runat="server" Text='<%#Eval("UserID")%>' />
                </div>
                <div class="col-md-2 col-lg-2">
                    <asp:TextBox ID="txtFeesReducedPercentage" runat="server" Text='<%# Eval("FeesReducedPercentage") %>' CssClass="form-control" />
                    <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtFeesReducedPercentage" MinimumValue="0" MaximumValue="999999" ErrorMessage="The number is invalid!" Display="Dynamic" SetFocusOnError="true" Type="Double" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ControlToValidate="txtFeesReducedPercentage" SetFocusOnError="true" ErrorMessage="The number must be filled!" ValidationGroup="4" />
                </div>
                <div class="col-md-2 col-lg-2">
                    <asp:LinkButton runat="server" ID="btnDeleteAffMerchant" CssClass="btn btn-danger" CommandName="Delete" OnClientClick="if(!confirm('delete reference, are you sure?')) return false;"><i class="fa fa-trash"> Delete</i></asp:LinkButton></div>
            </div>
        </ItemTemplate>
    </netpay:DynamicRepeater>
        </asp:View>
        <asp:View runat="server">
            <div class="alert alert-info"> 
                Save account details to edit this section.
            </div>
        </asp:View>
    </asp:MultiView>
</admin:FormPanel>
