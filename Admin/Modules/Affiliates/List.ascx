﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Affiliates.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Partners">
    <Header>
        <admin:LegendColors runat="server" Items='<%# Netpay.Infrastructure.Enums.BoolColorMap %>' FloatRight="true" />
    </Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" SaveAjaxState="true" ID="rptList" DataKeyNames="AccountID"  AutoGenerateColumns="false" BubbleLoadEvent="true">
                <EditRowStyle CssClass="selected-row" />
                <Columns>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <span style="background-color: <%# Netpay.Infrastructure.Enums.BoolColorMap.MapHtmlColor((bool)Eval("IsActive")) %>;" class="legend-item"></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ID" SortExpression="ID">
                        <ItemTemplate>
					        <%# Eval("AccountID") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Name" DataField="Name" />
                    <asp:BoundField HeaderText="Merchants" DataField="MerchantsNum"  />
                    <asp:BoundField HeaderText="Leads" DataField="LeadsNum"  />
                    <asp:BoundField HeaderText="Settlements" DataField="SettlementsNum"  />
                    <asp:BoundField HeaderText="Is Active" DataField="IsActive" />
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd='<%# Netpay.Bll.Affiliates.Affiliate.SecuredObject.HasPermission(Netpay.Infrastructure.Security.PermissionValue.Delete) %>' />
    </Footer>

</admin:ListSection>

