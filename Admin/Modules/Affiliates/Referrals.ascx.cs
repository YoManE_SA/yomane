﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;

namespace Netpay.Admin.Affiliates
{
    public partial class Referrals : Controls.AccountControlBase
    {
        protected override void DataBindChildren()
        {
            if (Account != null && Account.AffiliateID != null)
            {
                var list = Netpay.Bll.Affiliates.Referral.Search(new Netpay.Bll.Affiliates.Referral.SearchFilters() { AffiliateId = Account.AffiliateID }, null);
                if (list.Count > 0)
                {
                    rptList.DataSource = list;
                    ReferralsMultiView.ActiveViewIndex = 0;
                }
                else
                { ReferralsMultiView.ActiveViewIndex = 1; }
            }
            else
            {
                ReferralsMultiView.ActiveViewIndex = 1;
            }
            base.DataBindChildren();
        }
    }
}