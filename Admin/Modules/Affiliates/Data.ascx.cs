﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;


namespace Netpay.Admin.Affiliates
{
    public partial class Data : Controls.AccountControlBase
    {
        protected Netpay.Bll.Affiliates.Affiliate ItemData;

        public List<string> AffiliatesSecuredObjectsNames { get; set; }

        public System.IO.FileInfo fiFile { get; set; }
        public System.Drawing.Bitmap imgFile { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += PageController_SaveItem;
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            ItemData = Account as Netpay.Bll.Affiliates.Affiliate;
            
            if (ItemData == null)
            {
                ItemData = new Bll.Affiliates.Affiliate();
            }
            else
            {
                if (ItemData.GenerateRandomCardPin.HasValue)
                {
                    ddlPinGeneration.SelectedValue = ItemData.GenerateRandomCardPin.ToString();
                }

                if (!string.IsNullOrEmpty(ItemData.HeaderImageFileName))
                {
                    string sFilePath = Domain.Current.MapPublicDataPath(ItemData.HeaderImageFileName);
                    if (System.IO.File.Exists(sFilePath))
                    {
                        fiFile = new System.IO.FileInfo(sFilePath);
                        imgFile = new System.Drawing.Bitmap(sFilePath);
                    }
                    else
                    {
                        litFileNotFound.Visible = true;
                    }
                }
            }

            base.DataBindChildren();
        }

        private void PageController_SaveItem(object sender, EventArgs e)
        {
            //Save event will be invoked only if validation was passed.
            //So inside this function there won't be any validation checks.

            ItemData = Account as Netpay.Bll.Affiliates.Affiliate;
            if (ItemData == null) ItemData = new Netpay.Bll.Affiliates.Affiliate();

            //Payout Information
            ItemData.Name = txtName.Text.Trim(); // [V - include validation]
            ItemData.LinkRefID = txtLinkRefID.Text.Trim(); // [V - include validation]
            ItemData.LegalName = txtLegalName.Text.Trim(); // [V]
            ItemData.LegalNumber = txtLegalNumber.Text.Trim(); // [V]
            ItemData.PaymentPayeeName = txtPaymentPayeeName.Text.Trim(); // [V]
            ItemData.PaymentReceiveCurrency = int.Parse(ddlCurrency.SelectedValue);
            if (!ddlPinGeneration.SelectedItem.Value.Equals("-1"))
                ItemData.GenerateRandomCardPin = Convert.ToInt16(ddlPinGeneration.SelectedItem.Value);
            //Personal address
            ItemData.PersonalAddress = caAddress.Save(ItemData.PersonalAddress);
            if (ItemData.PersonalAddress == null)
            {
                throw new Exception("Country wasn't selected in Data tab.");
            }

            ItemData.Save();
            if (Account == null) AccountPage.Account = ItemData; //Happens when it is a new affiliate.
        }

        protected void CheckRefCode(object source, ServerValidateEventArgs args)
        {
            if (args.Value.Trim().Length == 0)
            {
                args.IsValid = true;
            }
            else
            {
                if (Account != null && Account.AccountType == Bll.Accounts.AccountType.Affiliate)
                    ItemData = Account as Netpay.Bll.Affiliates.Affiliate;


                if (!Bll.Affiliates.Affiliate.CheckIfAFLinkRefExist(txtLinkRefID.Text, ItemData.ID))
                {
                    args.IsValid = true;
                }
                else
                {
                    args.IsValid = false;
                }
            }
        }

        protected void CheckName(object source, ServerValidateEventArgs args)
        {
            if (string.IsNullOrEmpty(txtName.Text.Trim()))
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }


        public void UploadFile(object sender, EventArgs e)
        {
            ItemData = Account as Netpay.Bll.Affiliates.Affiliate;
            if (ItemData == null) return;

            //If no file choosed , return.
            if (!fuDocument.HasFile) return;

            var sExt = System.IO.Path.GetExtension(fuDocument.FileName).ToLower();
            if (".gif;.jpg;.jpe;.jpeg;.png".Contains(sExt))
            {
                string fileName = "CP_Logo" + sExt;
                if (!System.IO.Directory.Exists(ItemData.MapPublicPath(null)))
                {
                    System.IO.Directory.CreateDirectory(ItemData.MapPublicPath(null));
                }
                fuDocument.SaveAs(ItemData.MapPublicPath(fileName));
                string sFileName = ItemData.MapPublicOffset(fileName);
                Bll.Affiliates.Affiliate.SetHeaderImageFileName(ItemData.AffiliateID.Value, sFileName);

                //Load the Affiliate after the File was saved.
                ItemData = Bll.Affiliates.Affiliate.Load(ItemData.AffiliateID);

                //Declare the fiFile and imgFile properties. 
                if (!string.IsNullOrEmpty(ItemData.HeaderImageFileName))
                {
                    string sFilePath = Domain.Current.MapPublicDataPath(ItemData.HeaderImageFileName);
                    if (System.IO.File.Exists(sFilePath))
                    {
                        fiFile = new System.IO.FileInfo(sFilePath);
                        imgFile = new System.Drawing.Bitmap(sFilePath);
                    }
                    else
                    {
                        litFileNotFound.Visible = true;
                    }
                }
                LogoFormPanel.BindAndUpdate();
            }
        }

        public void DeleteFile(object sender, EventArgs e)
        {
            ItemData = Account as Netpay.Bll.Affiliates.Affiliate;
            //Save the fileName in order to delete it from the current pc file system.
            string fileName = ItemData.HeaderImageFileName;
            if (!string.IsNullOrEmpty(fileName))
            {
                Bll.Affiliates.Affiliate.DeleteHeaderFileName(ItemData.AffiliateID.Value);
                string sFilePath = Domain.Current.MapPublicDataPath(fileName);
                if (System.IO.File.Exists(sFilePath))
                {
                    try { System.IO.File.Delete(sFilePath); }
                    catch (Exception) { }
                }
            }

            //Load affiliate after file was deleted.
            ItemData = Bll.Affiliates.Affiliate.Load(ItemData.AffiliateID);
            fuDocument.DeleteTemporaryFile();
            LogoFormPanel.BindAndUpdate();
        }
    }
}