﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Header.ascx.cs" Inherits="Netpay.Admin.Affiliates.TabBar" %>
<%@ Register Src="../Toolbar.ascx" TagPrefix="AUC" TagName="Toolbar" %>

<table width="100%" border="0" cellspacing="0" cellpadding="2">
	
		<td id="HeaderTitle">
			<span class="HeaderTitleText">AFFILIATE MANAGEMENT</span> -
			<asp:Literal runat="server" ID="ltCustomerName" />
		</td>
		<td align="right">
			Active: <netpay:BoolDropDown runat="server" ID="ddlStatus" AutoPostBack="true" OnSelectedIndexChanged="Status_Changes" EnableBlankSelection="false" />
		</td>
		<td align="right">
			<asp:HyperLink runat="server" ID="hlEmail"><asp:Image runat="server" ImageUrl="~/images/img/post_button_2.gif" title="send e-mail" width="25" height="21" border="0" /></asp:HyperLink>
			<asp:HyperLink runat="server" ID="hlDeposits"><asp:Image runat="server" ImageUrl="~/images/img/post_button_4.gif" title="deposit" width="25" height="21" border="0" /></asp:HyperLink>
			<asp:HyperLink runat="server" ID="hlCustomerUI"><asp:Image runat="server" ImageUrl="~/images/img/post_button_1.gif" title="to client interface" width="40" height="21" border="0" /></asp:HyperLink>
		</td>
	</tr>
</table>
<br />
<AUC:Toolbar runat="server" ID="tbTop" />
