﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;



namespace Netpay.Admin.Affiliates
{
    public partial class Permissions : Controls.AccountControlBase
    {
        protected Netpay.Bll.Affiliates.Affiliate ItemData;
        public List<string> AffiliatesSecuredObjectsNames { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += PageController_SaveItem;
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            ItemData = Account as Netpay.Bll.Affiliates.Affiliate;
            if (ItemData == null)
            {
                ItemData = new Bll.Affiliates.Affiliate();
            }
            else
            {
                ///ALLOW PAGES SECTION DATABIND///
                //Get a list of the Affiliates 
                AffiliatesSecuredObjectsNames = Bll.Affiliates.Affiliate.GetSecuredObjectsForAffiliates();

                //Create the CheckBoxList of the affiliate permissions
                chlAllowPages.Items.Clear();
                foreach (var securedObjectName in AffiliatesSecuredObjectsNames)
                {
                    chlAllowPages.Items.Add(new ListItem(securedObjectName, securedObjectName));
                }

                //Get the Permissions that the current affiliate has 
                var values = Netpay.Infrastructure.Security.Permission.LoadForLogin(ItemData.LoginID.GetValueOrDefault());

                //Set the 'selected' property of the checkboxlist items 
                foreach (ListItem l in chlAllowPages.Items)
                {
                    var permission = values.Where(x => x.ObjectID == Netpay.Infrastructure.Security.SecuredObject.Get("PCP.Security", (l.Value)).ID).SingleOrDefault();
                    if (permission == null) l.Selected = false;
                    else l.Selected = (permission.Value == Netpay.Infrastructure.Security.PermissionValue.Execute);
                }
                //ALLOW PAGES SECTION DATABIND
            }

            base.DataBindChildren();
        }

        private void PageController_SaveItem(object sender, EventArgs e)
        {
            ItemData = Account as Netpay.Bll.Affiliates.Affiliate;
            if (ItemData == null) ItemData = new Netpay.Bll.Affiliates.Affiliate();

            //Edit Login - Get the EditLogin details just if the Affiliate was already saved
            //Because if not , the EditLogin control is not visible
            if (Account != null && Account.AffiliateID != null)
            {
                var permissionsTab = TemplatePage.FormTabs.FindControl("Permissions") as Controls.GlobalControls.JQueryControls.TabView;
                var editLoginControl = permissionsTab.Controls[0];
                if (string.IsNullOrEmpty((editLoginControl.FindControl("txtUserName") as TextBox).Text) || string.IsNullOrEmpty((editLoginControl.FindControl("txtEmailAddress") as TextBox).Text))
                {                    
                    throw new Exception("Please insert correct Login Details in permissions tab.");
                }
                ItemData.Login.UserName = (editLoginControl.FindControl("txtUserName") as TextBox).Text;
                ItemData.Login.EmailAddress = (editLoginControl.FindControl("txtEmailAddress") as TextBox).Text;
                ItemData.Login.IsActive = (editLoginControl.FindControl("chkIsActive") as CheckBox).Checked;
            }

            //Allow Pages Section - Save the Allow pages section just if the affiliate was already saved.
            //And the affiliate has LoginID beacuse this section uses the LoginID and it cant be null
            if (Account != null && Account.AffiliateID != null && ItemData.LoginID != null)
            {
                var values = new Dictionary<int, Infrastructure.Security.PermissionValue?>();
                foreach (ListItem t in chlAllowPages.Items)
                {
                    string valueName = t.Value;
                    if (Infrastructure.Security.SecuredObject.Get("PCP.Security", valueName) == null)
                    {
                        Infrastructure.Security.SecuredObject.Create(Infrastructure.Module.Get("PCP.Security"), valueName, Infrastructure.Security.PermissionGroup.Execute);
                    }
                    if (t.Selected)
                    {
                        values.Add(Infrastructure.Security.SecuredObject.Get("PCP.Security", valueName).ID, Infrastructure.Security.PermissionValue.Execute);
                    }
                    else
                    {
                        values.Add(Infrastructure.Security.SecuredObject.Get("PCP.Security", valueName).ID, null);
                    }
                }
                Infrastructure.Security.Permission.SetToLoginAffiliate(ItemData.LoginID.GetValueOrDefault(), values);
            }

            ItemData.Save();

        }
    }
}