﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Settlements.ascx.cs" Inherits="Netpay.Admin.Modules.Affiliates.Settlements" %>
<admin:FormPanel runat="server" ID="frmPanel" Title="SETTLEMENTS">
    <asp:MultiView runat="server" ID="AfSetMultiView">
        <asp:View runat="server">
            <div class="row inner-table-titles">
                <div class="col-md-2">Mer' Name</div>
                <div class="col-md-2">Set' Number</div>
                <div class="col-md-2">Set' Date</div>
                <div class="col-md-2">Original Total</div>
                <div class="col-md-2">Payment Total</div>
                <div class="col-md-2">Totals</div>

            </div>
            <NP:PagedRepeater runat="server" ID="rptList" OnItemCommand="List_ItemCommand" PageSize="10" SortKey="id" SortDesc="true" OnDataBinding="List_DataBinding" >
                <ItemTemplate>
                    <div class="row table-ui">
                        <div class="col-md-2"><%# GetMerchantName((int)Eval("MerchantID")) %>  </div>
                        <div class="col-md-2"><%# Eval("TransPaymentID") %>  </div>
                        <div class="col-md-2"><%# Eval("PaymentDate", "{0:d}") %>  </div>
                        <div class="col-md-2"><%# new Money(((decimal?)Eval("MerchantPaidAmount")).GetValueOrDefault(), (int)Eval("CurrencyID"))%> </div>
                        <div class="col-md-2"><%# (int)Eval("ID") == 0 ? "---" : new Money((decimal)Eval("Amount"), (int)Eval("CurrencyID")).ToString() %></div>
                        <div class="col-md-2"><asp:Button runat="server" CssClass="btn btn-primary" Text="Show" CommandName="ShowTotal" CommandArgument='<%#Eval("TransPaymentID")%>' /></div>
                    </div>
                </ItemTemplate>
            </NP:PagedRepeater>
   
            <admin:Pager runat="server" PagedControlID="rptList" OnPageChanged="List_PageChanged" />
        </asp:View>
        <asp:View runat="server">
             <div class="alert alert-info">
            No settlements found.
                 </div>
        </asp:View>
    </asp:MultiView>
</admin:FormPanel>
<admin:ModalDialog runat="server" ID="mdTotal" Title="Affiliate Settlement Total" OnCommand="List_ItemCommand">
    <Body>
        <div class="row">
            <div class="col-md-12">
                <netpay:Totals runat="server" ID="totals" HideTitle="true" />
            </div>
        </div>
        <netpay:DynamicRepeater runat="server" ID="rptEditLines">
            <ItemTemplate>
                <div class="row">
                    <div class="panel panel-default">
                        <div class="panel panel-header">
                            Add custom lines
                        </div>
                        <div class="panel panel-body">
                            <div class="col-md-12">
                                <div class="form-group">
                                    Item Text
                                    <asp:TextBox runat="server" ID="txtLineText" TextMode="MultiLine" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                Price
                                <asp:TextBox runat="server" ID="txtLineAmount" CssClass="form-control" />
                            </div>
                            <div class="col-md-6">
                                Quantity
                                <asp:TextBox runat="server" ID="txtLineQuantity" CssClass="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </netpay:DynamicRepeater>
    </Body>
    <Footer>
        <asp:Button runat="server" ID="btnCreate" CssClass="btn btn-primary" CommandName="CreatePayment" Text="Create" />
        <asp:Button runat="server" ID="btnDelete" CssClass="btn btn-primary" CommandName="DeletePayment" Text="Delete" />
    </Footer>
</admin:ModalDialog>
