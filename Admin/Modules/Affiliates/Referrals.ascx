﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Referrals.ascx.cs" Inherits="Netpay.Admin.Affiliates.Referrals" %>
<admin:FormSections runat="server" Title="Referrals">
    <asp:MultiView runat="server" ID="ReferralsMultiView">
        <asp:View runat="server">
            <div class="row inner-table-titles">
                <div class="col-md-3">Type</div>
                <div class="col-md-3">IP Address</div>
                <div class="col-md-3">Date</div>
                <div class="col-md-3">Referral</div>
            </div>
            <asp:Repeater runat="server" ID="rptList">
                <ItemTemplate>
                    <div class="row table-ui">
                        <div class="col-md-3"><%# Eval("Type")%></div>
                        <div class="col-md-3"><%# Eval("IpAddress")%></div>
                        <div class="col-md-3"><%# Eval("Date")%></div>
                        <div class="col-md-3"><%# Eval("ReferralVal")%></div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </asp:View>
        <asp:View runat="server">
            <div class="alert alert-info">
            No items found.
                </div>
        </asp:View>
    </asp:MultiView>
</admin:FormSections>
