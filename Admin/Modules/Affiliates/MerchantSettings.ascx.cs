﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Admin.Affiliates
{
    public partial class MerchantSettings : Controls.AccountControlBase
    {
        private Dictionary<int, string> merchantNames;
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += PageController_SaveItem;
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            if (Account != null && Account.AffiliateID != null)
            {
                var data = Bll.Affiliates.MerchantSettings.Search(new Netpay.Bll.Affiliates.MerchantSettings.SearchFilters() { AffiliateId = Account.AffiliateID }, null);
                merchantNames = Bll.Merchants.Merchant.GetMerchantNames(data.Select(v => v.MerchantId).ToList());
                rptMerchants.DataSource = data;
            }
            base.DataBindChildren();
        }

        protected void Merchants_ItemCommand(object source, CommandEventArgs e)
        {
            if (e.CommandName == "Delete")
                rptMerchants.RemoveItem((e as RepeaterCommandEventArgs).Item.ItemIndex);

            else if (e.CommandName == "Add")
            {
                if (!apMerchant.Value.HasValue) return;
                int merchantId = 0;
                var merchant = Bll.Merchants.Merchant.LoadByAccountId(apMerchant.Value.Value);
                if (merchant != null)
                {
                    merchantId = merchant.ID;
                }
                else
                {
                    return;
                }

                var item = Bll.Affiliates.MerchantSettings.Load(Account.AffiliateID.Value, merchantId);
                if (item != null) return;

                //Permission check
                if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Affiliates.MerchantSettings.SecuredObject, PermissionValue.Add);

                item = new Bll.Affiliates.MerchantSettings(merchantId, Account.AffiliateID.Value);
                merchantNames = Bll.Merchants.Merchant.GetMerchantNames(new List<int>() { item.MerchantId });
                rptMerchants.AddItem(item);
                apMerchant.Value = null;
            }
            frmSection.Update();
        }

        protected string GetName(int id)
        {
            string ret;
            if (merchantNames.TryGetValue(id, out ret)) return ret;
            return null;
        }

        private void PageController_SaveItem(object sender, EventArgs e)
        {
            if (Account == null) (sender as Controls.DataPageController).LoadActiveItem();
            var existing = Bll.Affiliates.MerchantSettings.Search(new Netpay.Bll.Affiliates.MerchantSettings.SearchFilters() { AffiliateId = Account.AffiliateID }, null);
            var newList = new List<Bll.Affiliates.MerchantSettings>();
            foreach (RepeaterItem item in rptMerchants.Items)
            {
                var merchantId = (item.FindControl("hfMerchantID") as HiddenField).Value.ToNullableInt();
                if (merchantId == null) continue;
                var dateItem = existing.Where(s => s.MerchantId == merchantId.Value).FirstOrDefault();
                if (dateItem == null) dateItem = new Bll.Affiliates.MerchantSettings(merchantId.Value, Account.AffiliateID.Value);
                dateItem.FeesReducedPercentage = (item.FindControl("txtFeesReducedPercentage") as TextBox).Text.ToNullableDecimal();
                dateItem.UserID = (item.FindControl("txtUserID") as TextBox).Text;
                newList.Add(dateItem);
            }
            existing = existing.Where(i => !newList.Contains(i)).ToList();
            foreach (var item in existing) item.Delete();
            foreach (var item in newList) item.Save();

        }
    }
}