﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Netpay.Admin.Affiliates {
    
    
    public partial class Permissions {
        
        /// <summary>
        /// chlAllowPages control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBoxList chlAllowPages;
        
        /// <summary>
        /// dlgCountryNotSelectedError control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Netpay.Admin.Controls.ModalDialog dlgCountryNotSelectedError;
        
        /// <summary>
        /// SaveAffiliateErrorMsg control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Netpay.Web.Controls.ActionNotify SaveAffiliateErrorMsg;
    }
}
