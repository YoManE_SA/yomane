﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Admin.Modules.Affiliates
{
    public partial class Fees : Controls.AccountControlBase
    {
        protected Bll.Affiliates.Fees Item { get; set; }

        protected override void DataBindChildren()
        {
            if (Account != null) {
                var merchantSettings = Bll.Affiliates.MerchantSettings.Search(new Bll.Affiliates.MerchantSettings.SearchFilters() { AffiliateId = Account.AffiliateID, MerchantId = Account.MerchantID }, null);
                List<KeyValuePair<int, string>> referenceNames = null; 
                if (Account.AffiliateID.HasValue) referenceNames = Bll.Merchants.Merchant.GetNames(merchantSettings.Select(v => v.MerchantId).ToList(), Bll.Accounts.AccountType.Merchant).OrderBy(v=> v.Value).ToList();
                else if (Account.MerchantID.HasValue) referenceNames = Bll.Merchants.Merchant.GetNames(merchantSettings.Select(v => v.PartnerId).ToList(), Bll.Accounts.AccountType.Affiliate).OrderBy(v => v.Value).ToList();
                if (referenceNames != null) referenceNames.Insert(0, new KeyValuePair<int, string>(0, "[All]"));
                rbReference.DataSource = referenceNames;
            } else rbReference.DataSource = null;
            Item = new Bll.Affiliates.Fees(0, null);
            base.DataBindChildren();
        }

        protected void List_DataBinding(object sender, EventArgs e)
        {
            rptList.DataSource = null;
            if (Account == null) return;
            var referenceId = rbReference.SelectedValue.ToNullableInt();
            if (referenceId.GetValueOrDefault() == 0) referenceId = null;
            if (Account.AffiliateID.HasValue)
                rptList.DataSource = Bll.Affiliates.Fees.Load(Account.AffiliateID, referenceId);
            else if (Account.MerchantID.HasValue && referenceId.HasValue)
                rptList.DataSource = Bll.Affiliates.Fees.Load(referenceId.Value, Account.MerchantID);
        }

        protected void Merchant_SelectedIndexChanged(object sender, EventArgs e)
        {
            rptList.DataBind();
            frmPanel.Update();
        }

        protected void References_DataBound(object sender, EventArgs e)
        {
            if (rbReference.Items.Count > 0) rbReference.Items[0].Selected = true;
        }

        protected void Item_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "AddItem") {
                //Permission check
                if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Affiliates.Fees.SecuredObject, PermissionValue.Read);

                if (Account == null) Account = Bll.Accounts.Account.LoadAccount(TemplatePage.ItemID.Value);
                rptSteps.DataSource = null;
                var referenceId = rbReference.SelectedValue.ToNullableInt();
                if (referenceId.GetValueOrDefault() == 0) referenceId = null;
                if (Account.AffiliateID.HasValue) Item = new Bll.Affiliates.Fees(Account.AffiliateID.GetValueOrDefault(), referenceId);
                else if (Account.MerchantID.HasValue && referenceId.HasValue) Item = new Bll.Affiliates.Fees(referenceId.Value, Account.MerchantID.GetValueOrDefault());
                else return; //must have either merchantid or affiliate
                while (Item.Steps.Count < 5) Item.Steps.Add(new Bll.Affiliates.Fees.FeeStep());
                hfItemIndex.Value = "";
            } else if (e.CommandName == "DeleteItem") {
                var repeaterCommand = (e as RepeaterCommandEventArgs);
                int? hfID = (repeaterCommand.Item.FindControl("hfID") as HiddenField).Value.ToNullableInt();
                Item = Bll.Affiliates.Fees.Load(hfID.GetValueOrDefault());
                if (Item == null) return;
                Item.Delete();
                (sender as Web.Controls.DynamicRepeater).RemoveItem(repeaterCommand.Item.ItemIndex);
                frmPanel.Update();
                return;
            } else if (e.CommandName == "EditItem") {
                var repeaterCommand = (e as RepeaterCommandEventArgs);
                int? hfID = (repeaterCommand.Item.FindControl("hfID") as HiddenField).Value.ToNullableInt();
                Item = Bll.Affiliates.Fees.Load(hfID.GetValueOrDefault());
                if (Item == null) return;
                hfItemIndex.Value = repeaterCommand.Item.ItemIndex.ToString();
                while (Item.Steps.Count < 5) Item.Steps.Add(new Bll.Affiliates.Fees.FeeStep());
            } else if (e.CommandName == "SaveItem") {
                Item = new Bll.Affiliates.Fees(hfAffiliateID.Value.ToNullableInt().GetValueOrDefault(), hfMerchantID.Value.ToNullableInt());
                Item.Currency = ddlCurrncy.Value.ToNullableEnum<CommonTypes.Currency>().GetValueOrDefault();
                Item.PaymentMethod = ddlPaymentMethod.Value.ToNullableEnum<CommonTypes.PaymentMethodEnum>();
                Item.TransType = ddlTransType.Value.ToNullableEnum<Infrastructure.PartnerTransType>().GetValueOrDefault();
                Item.CalcMethod = ddlCalcMethod.Value.ToNullableEnum<Infrastructure.PartnerFeeCalc>().GetValueOrDefault();
                foreach (RepeaterItem stepItem in rptSteps.Items) {
                    var step = new Bll.Affiliates.Fees.FeeStep() {
                        PercentFee = (stepItem.FindControl("txtPercentFee") as TextBox).Text.ToNullableDecimal().GetValueOrDefault(),
                        SlicePercent = (stepItem.FindControl("txtSlicePercent") as TextBox).Text.ToNullableDecimal(),
                        UpToAmount = (stepItem.FindControl("txtUpToAmount") as TextBox).Text.ToNullableDecimal().GetValueOrDefault(),
                    };
                    if (!step.IsEmpty) Item.Steps.Add(step);
                }
                Item.Save();
                var listIndex = hfItemIndex.Value.ToNullableInt();
                if (listIndex.HasValue && rptList.Items.Count > listIndex) {
                    rptList.Items[listIndex.Value].DataItem = Item;
                    rptList.Items[listIndex.Value].DataBind();
                } else rptList.AddItem(Item);
                mdEdit.RegisterHide();
                frmPanel.Update();
                return;
            }
            mdEdit.BindAndShow();
        }

    }
}