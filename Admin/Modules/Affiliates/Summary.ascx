﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Summary.ascx.cs" Inherits="Netpay.Admin.Modules.Affiliates.Summary" %>
<admin:FormSections runat="server">
    <div class="row">
        <div class="col-md-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:HyperLink Target="_blank" runat="server" NavigateUrl='<%#GetPartnerControlPanelUrl()%>'><i class="fa fa-link"></i> Partner Control Panel</asp:HyperLink></li>
            </ul>
        </div>
        <div class="col-md-6">
            <ul class="list-group"> 
                <li class="list-group-item"><asp:CheckBox Text="Account Is Active" Checked="<%#ItemData.IsActive %>" ID="chbIsAccountActive" runat="server" /></li>
            </ul>
        </div>
    </div>



    <%--<asp:PlaceHolder runat="server" Visible="<%#!string.IsNullOrEmpty(ItemData.LinkRefID) %>">
        <asp:HyperLink
            Target="_blank"
            runat="server"
            Text="Homepage"
            NavigateUrl='<%#"https://www.netpay-intl.com/?RefCode="+ItemData.LinkRefID%>' /><br />

        <asp:HyperLink
            Target="_blank"
            runat="server"
            Text="Signup page"
            NavigateUrl='<%#"https://www.netpay-intl.com/Website/SiteSignup.aspx?RefCode="+ItemData.LinkRefID%>' /><br />

        <asp:HyperLink
            Target="_blank"
            runat="server"
            Text="Campaign page"
            NavigateUrl='<%#"https://www.netpay-intl.com/campaigns/MerchantProcessing.aspx?RefCode="+ItemData.LinkRefID%>' />
    </asp:PlaceHolder>--%>

   
</admin:FormSections>
