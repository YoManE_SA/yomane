﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Affiliates.Filter" %>
<admin:FilterSection runat="server" Title="Status">
    <div class="form-group">
        <asp:Label ID="lblAccount" runat="server" Text="Name/ID" />
        <admin:AccountPicker runat="server" ID="apAccount" LimitToType="Affiliate" />
    </div>
    <div class="form-group">
        <asp:Label ID="lblStatus" runat="server" Text="Status" />
        <netpay:BoolDropDown TrueText="Active" FalseText="Not Active" CssClass="form-control" runat="server" ID="ddlIsActive" />
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblID" runat="server" Text="ID Range" />
                <JQ:IntRange runat="server" ID="rngID" />
            </div>
        </div>
    </div>
</admin:FilterSection>
