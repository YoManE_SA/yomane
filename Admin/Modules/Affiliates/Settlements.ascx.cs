using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Modules.Affiliates
{
    public partial class Settlements : Controls.AccountControlBase
    {
        private Dictionary<int, string> companyNames;

        protected void List_PageChanged(object source, EventArgs e)
        {
            frmPanel.Update();
        }
        
        protected void List_DataBinding(object source, EventArgs e)
        {
            if (Account != null && Account.AffiliateID.HasValue)
            {                                
                var setList = Bll.Settlements.PartnerPayment.GetUpcoming(new Bll.Settlements.PartnerPayment.SearchFilters() { AffiliateId = Account.AffiliateID.Value }, rptList);
                if (setList.Count > 0) {
                    rptList.DataSource = setList;
                    companyNames = Bll.Merchants.Merchant.GetCompanyNames(setList.Select(v => v.MerchantID).ToList());
                    AfSetMultiView.ActiveViewIndex = 0;
                } else AfSetMultiView.ActiveViewIndex = 1;
            } else AfSetMultiView.ActiveViewIndex = 1;
        }

        public string GetMerchantName(int merchantId)
        {
            string ret;
            if (!companyNames.TryGetValue(merchantId, out ret)) return null;
            return ret;
        }

        protected void List_ItemCommand(object source, CommandEventArgs e)
        {
            if (Account == null) TemplatePage.PageController.LoadActiveItem();
            int merchantSettlementId = e.CommandArgument.ToNullableInt().GetValueOrDefault();
            var item = Bll.Settlements.PartnerPayment.GetUpcoming(new Bll.Settlements.PartnerPayment.SearchFilters() { AffiliateId = Account.AffiliateID.Value, PaymentID = merchantSettlementId }, null).FirstOrDefault();
            if (item == null) return;
            bool didCommand = false;
            if (e.CommandName == "CreatePayment") {
                var lines = new List<Bll.Totals.AffiliateTotalLine>();
                foreach (RepeaterItem ri in rptEditLines.Items) {
                    var line = new Bll.Totals.AffiliateTotalLine();
                    line.Text = (ri.FindControl("txtLineText") as TextBox).Text;
                    line.Amount = (ri.FindControl("txtLineAmount") as TextBox).Text.ToNullableDecimal().GetValueOrDefault();
                    line.Quantity = (ri.FindControl("txtLineQuantity") as TextBox).Text.ToNullableInt().GetValueOrDefault();
                    if (line.Quantity == 0 && line.Amount == 0 && string.IsNullOrEmpty(line.Text)) continue;
                    lines.Add(line);
                }
                item.Create(lines);
                while (rptEditLines.Items.Count > 0) rptEditLines.RemoveItem(0);
                didCommand = true;
            } else if (e.CommandName == "DeletePayment") {
                item.Delete();
                didCommand = true;
            }
            totals.AffiliateID = item.AffiliateID;
            totals.PayID = item.TransPaymentID;
            totals.CompanyID = item.MerchantID;
            totals.Currency = item.CurrencyID;
            totals.AFP_ID = item.ID;

            btnDelete.CommandArgument = btnCreate.CommandArgument = totals.PayID.ToString();
            btnCreate.Visible = (item.ID == 0);
            rptEditLines.Visible = (item.ID == 0);
            btnDelete.Visible = (item.ID != 0);
            if (didCommand) {
                mdTotal.BindAndUpdate();
                frmPanel.BindAndUpdate();
            }else mdTotal.BindAndShow();
            if (rptEditLines.Visible)
                while (rptEditLines.Items.Count < 5) rptEditLines.AddItem(null);

        }
    }
}