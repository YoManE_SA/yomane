﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;

namespace Netpay.Admin.Modules.Affiliates
{
    public partial class Summary : Controls.AccountControlBase
    {
        protected Netpay.Bll.Affiliates.Affiliate ItemData;

        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += PageController_SaveItem;
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            ItemData = Account as Netpay.Bll.Affiliates.Affiliate;
            if (ItemData == null) return;
            base.DataBindChildren();
        }

        private void PageController_SaveItem(object sender, EventArgs e)
        {
            ItemData = Account as Netpay.Bll.Affiliates.Affiliate;
            if (ItemData == null) ItemData = new Netpay.Bll.Affiliates.Affiliate();

            ItemData.IsActive = chbIsAccountActive.Checked; 

            ItemData.Save();
        }

        public string GetPartnerControlPanelUrl()
        {
            if (ItemData == null) TemplatePage.PageController.LoadActiveItem();
            //Get the partners URL
            string partnersUrl = Web.WebUtils.CurrentDomain.PartnersUrl; 

            string password = Netpay.Infrastructure.Security.Encryption.Encrypt(Netpay.Infrastructure.Security.Login.GetPassword(ItemData.LoginID.GetValueOrDefault(), HttpContext.Current.Request.UserHostAddress)).ToBase64UrlString();
            string username = Netpay.Infrastructure.Security.Encryption.Encrypt(ItemData.Login.UserName).ToBase64UrlString();
            string email = Netpay.Infrastructure.Security.Encryption.Encrypt(ItemData.Login.EmailAddress).ToBase64UrlString();

            string partnerFullUrl = string.Format("{0}Login.aspx?login=outer&mail={1}&username={2}&userpassword={3}", partnersUrl, email, username, password);

            return partnerFullUrl;
        }
    }
}