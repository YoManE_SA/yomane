﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;

namespace Netpay.Admin.Affiliates
{
	public partial class TabBar : System.Web.UI.UserControl
	{
		protected Netpay.Web.Admin_AccountPage AccountPage { get { return Page as Netpay.Web.Admin_AccountPage; } }
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack) {
				var ItemData = AccountPage.Account as Netpay.Bll.Affiliates.Affiliate;
				//tbTop.LoadPartnerTabs(ItemData.AccountID);
				if (ItemData != null) {
					ltCustomerName.Text = string.Format("{0} | {1}", ItemData.ID, ItemData.Name);
					//hlEmail.NavigateUrl = string.Format("mailto:{0}", ItemData.EmailAddress);
					//hlDeposits.NavigateUrl = string.Format("../customer_dataDeposit.aspx?Affiliate={0}", ItemData.ID);
					hlCustomerUI.NavigateUrl = string.Format("javascript:window.open('outer_login.asp?Affiliate={0}');void(0)", ItemData.ID);
					ddlStatus.BoolValue = ItemData.IsActive;
				}
			}
		}

		protected void Status_Changes(object sender, EventArgs e)
		{
			var ItemData = AccountPage.Account as Netpay.Bll.Affiliates.Affiliate;
			ItemData.IsActive = ddlStatus.BoolValue.GetValueOrDefault();
			ItemData.Save();
		}
	}
}