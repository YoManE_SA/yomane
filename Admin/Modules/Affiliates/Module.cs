using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Admin.Affiliates
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu ManagmentMenuItem;

        public override string Name { get { return "Affiliates"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return "Affiliate management UI"; } }

        public Module() : base(Bll.Affiliates.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu item
            ManagmentMenuItem = new ApplicationMenu("Partner Management", "~/Affiliates/0", "fa-share-alt", 1);

            //Register the route
            Application.RegisterRoute("Affiliates/{*id}", "Partners", typeof(Controls.AccountPage), module : this);

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Partners", ManagmentMenuItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            ManagmentMenuItem.Parent.RemoveChild(ManagmentMenuItem);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "Partners")
            {
                (page as Controls.AccountPage).AccountChanged += delegate
                {
                    if ((page as Controls.AccountPage).Account != null)
                        (page as Controls.AccountPage).SetStatusColor(Netpay.Infrastructure.Enums.BoolColorMap.MapColor(((page as Controls.AccountPage).Account as Bll.Affiliates.Affiliate).IsActive));
                };

                (page as Controls.AccountPage).PageController.LoadItem += PageController_LoadItem;
                (page as Controls.AccountPage).PageController.NewItem += PageController_NewItem;
                (page as Controls.AccountPage).PageController.PostSaveItem += PageController_PostSaveItem;

                //Add to the editLogin DataBinding event the Login_DataBinding method.
                var editLogin = page.LoadControl("~/Modules/Security/EditLogin.ascx");
                editLogin.DataBinding += Login_DataBinding;
                page.AddControlToForm("Permissions", editLogin, this, "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject,Infrastructure.Security.Login.SecuredObject).GetActivObject);

                page.AddControlToList(page.LoadControl("~/Modules/Affiliates/List.ascx"));
                page.AddControlToForm("Summary", page.LoadControl("~/Modules/Affiliates/Summary.ascx"), this, "", page.CurrentPageSecuredObject);
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Affiliates/Filter.ascx"));
                page.AddControlToForm("Data", page.LoadControl("~/Modules/Affiliates/Data.ascx"), this, "", page.CurrentPageSecuredObject);
                page.AddControlToForm("Merchant Settings", page.LoadControl("~/Modules/Affiliates/MerchantSettings.ascx"), this, "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject, Bll.Affiliates.MerchantSettings.SecuredObject).GetActivObject);
                page.AddControlToForm("Referrals", page.LoadControl("~/Modules/Affiliates/Referrals.ascx"), this, "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject,Bll.Affiliates.Referral.SecuredObject).GetActivObject);
                page.AddControlToForm("Settlements", page.LoadControl("~/Modules/Affiliates/Settlements.ascx"), this, "", page.CurrentPageSecuredObject);
                page.AddControlToForm("Permissions", page.LoadControl("~/Modules/Affiliates/Permissions.ascx"), this, "", page.CurrentPageSecuredObject);
                page.AddControlToForm("Fees", page.LoadControl("~/Modules/Affiliates/Fees.ascx"), this, "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject,Bll.Affiliates.Fees.SecuredObject).GetActivObject);
            }
        }

        private void PageController_LoadItem(object sender, EventArgs e)
        {
            var page = (sender as Controls.DataPageController).Page as Controls.DataTablePage;
            page.FormButtons.EnableDelete = Bll.Affiliates.Affiliate.SecuredObject.HasPermission(PermissionValue.Delete);
            page.FormButtons.EnableSave = Bll.Affiliates.Affiliate.SecuredObject.HasPermission(PermissionValue.Edit);
        }

        private void PageController_NewItem(object sender, EventArgs e)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Affiliates.Affiliate.SecuredObject, PermissionValue.Read);

            // Create a new root Affilate for a tabs. Happens when pressing Add 
            // in order to create a new affiliate.
            var page = (sender as Controls.DataPageController).Page as Controls.AccountPage;
            (page as Controls.AccountPage).Account = new Bll.Affiliates.Affiliate();

            page.FormButtons.EnableDelete = Bll.Affiliates.Affiliate.SecuredObject.HasPermission(PermissionValue.Delete);
        }

        private void PageController_PostSaveItem(object sender, EventArgs e)
        {
        	Domain.Current.RemoveCachData("AffiliatesList");
            var page = (sender as System.Web.UI.Control).Page as Controls.AccountPage;

            //Try to get the Merchant after save because now it has a new ID
            //And it is needed to load the account after it was saved 
            //In order to show new details that were generated by the database.
            if (page != null)
            {
                if (page.Account.AccountType == Bll.Accounts.AccountType.Affiliate)
                {
                    if (page.Account == null) return;
                    int affiliateid = (int)page.Account.AffiliateID;
                    var AffiliateAfterSave = Bll.Affiliates.Affiliate.Load(affiliateid);

                    page.Account = AffiliateAfterSave;
                    page.PageController.DataKey = new System.Web.UI.WebControls.DataKey(new System.Collections.Specialized.OrderedDictionary() { { "ID", AffiliateAfterSave.AccountID } });
                    page.PageController.FormView.Update();
                }
            }
        }

        private void Login_DataBinding(object sender, EventArgs e)
        {
            var control = sender as Security.EditLogin;
            if ((control.Page as Controls.AccountPage).Account != null)
                control.ItemData = (control.Page as Controls.AccountPage).Account.Login;

            if ((control.Page as Controls.AccountPage).Account != null && (control.Page as Controls.AccountPage).Account.AccountID > 0)
                control.IsVisible = true;
            else
                control.IsVisible = false;
        }
    }
}