﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Fees.ascx.cs" Inherits="Netpay.Admin.Modules.Affiliates.Fees" %>
<admin:FormPanel runat="server" ID="frmPanel" Title="Affiliate Fees" EnableAdd='<%# (TemplatePage.Page is Netpay.Admin.Controls.AccountPage) && TemplatePage.ItemID != null && TemplatePage.ItemID != 0 %>' OnCommand="Item_Command">
    <asp:MultiView runat="server"  ActiveViewIndex='<%# (TemplatePage.Page is Netpay.Admin.Controls.AccountPage) && TemplatePage.ItemID != null && TemplatePage.ItemID != 0 ? 0 : 1 %>'>
        <asp:View runat="server">
            <div class="row">
        <div class="col-md-6">
            <h4>Select Reference:</h4>
            <asp:RadioButtonList runat="server" ID="rbReference" CssClass="checkbox-list" RepeatLayout="OrderedList" DataValueField="Key" DataTextField="Value" AutoPostBack="true" OnSelectedIndexChanged="Merchant_SelectedIndexChanged" OnDataBound="References_DataBound" />
        </div>
        <div class="col-md-6">
            <h4>Fees:</h4>
		    <netpay:DynamicRepeater runat="server" id="rptList" OnDataBinding="List_DataBinding" OnItemCommand="Item_Command" >
			    <ItemTemplate>
                    <div class="panel panel-default">
    				    <asp:HiddenField runat="server" ID="hfID" Value='<%# Eval("ID") %>' />
                        <div class="panel-heading font-small" style="background-color: #fcfcfc;">
                            <div class="row">
                                <div class="col-lg-8 col-md-8">
        						    <img width="23" height="15" align="middle" border="0" src="/NPCommon/ImgPaymentMethod/23X12/<%# ((int?)(Netpay.CommonTypes.PaymentMethodEnum?)Eval("PaymentMethod")).GetValueOrDefault() %>.gif" />
                                    <%# Eval("PaymentMethod") == null ? "[All]" : Netpay.Bll.PaymentMethods.PaymentMethod.Get((int)Eval("PaymentMethod")).Name %> 
                                    : <%# Netpay.Bll.Currency.Get((int)Eval("Currency")).IsoCode %>
                                    - Trans : <%# Eval("TransType") %>
                                </div>
                                <div class="col-lg-4 col-md-4 text-right font-small">
                                    <NP:Button runat="server" BackColor="Red" BorderColor="Red" Text="Delete" CssClass="btn btn-xs btn-success text-uppercase" RegisterAsync="true" CommandName="DeleteItem" />
                                    <NP:Button runat="server" Text="Edit" CssClass="btn btn-xs btn-success text-uppercase" RegisterAsync="true" CommandName="EditItem" />
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
    						        Calc Method: <b><asp:Label runat="server" Text='<%# Eval("CalcMethod") %>' /></b>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <div class="row">
			                            <div class="col-md-4"><u><span title="Affiliate Sale Price (%)">Split Total (%)</u></span></div>
			                            <div class="col-md-4"><u><span title="this is the upto amount for the step">Step Amount</u></span></div>
			                            <div class="col-md-4"><u><span title="Actual fee taken from result">Fee (%)</u></span></div>
                                    </div>
                                    <asp:Repeater runat="server" DataSource='<%# Eval("Steps") %>'>
                                        <ItemTemplate>
                                            <div class="row">
				                                <div class="col-md-4"><%# Eval("SlicePercent", "{0:0.00}") %>%</div>
				                                <div class="col-md-4"><%# Eval("UpToAmount", "{0:0.000}") %></div>
				                                <div class="col-md-4"><%# Eval("PercentFee", "{0:0.00}") %>%</div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
			    </ItemTemplate>
		    </netpay:DynamicRepeater>
        </div>
    </div>
        </asp:View>
        <asp:View runat="server">
            <div class="alert alert-info"> 
                Save account details to edit this section.
            </div>
        </asp:View>
    </asp:MultiView>
</admin:FormPanel>
<admin:ModalDialog runat="server" ID="mdEdit" Title="Edit Fee" OnCommand="Item_Command">
    <Body>
        <asp:HiddenField runat="server" ID="hfItemIndex" />
        <asp:HiddenField runat="server" ID="hfMerchantID" Value='<%# Item.MerchantID %>' />
        <asp:HiddenField runat="server" ID="hfAffiliateID" Value='<%# Item.AffiliateID %>' />
        <h4>
            <asp:Label runat="server" Text="For New Fee" Font-Bold="true" /></h4>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    Currency
                    <netpay:CurrencyDropDown ShowSymbol="false" Enabled='<%# Item.ID == 0 %>' CssClass="form-control" runat="server" ID="ddlCurrncy" Value='<%# (int)Item.Currency %>' EnableBlankSelection="false" />
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    Payment Method
                    <netpay:PaymentMethodDropDown Enabled='<%# Item.ID == 0 %>' CssClass="form-control" runat="server" ID="ddlPaymentMethod" Value='<%# (int?)Item.PaymentMethod %>' EnableBlankSelection="true" BlankSelectionValue="" BlankSelectionText="[All]" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                Trans Type<br />
                <netpay:EnumDropDown runat="server" ID="ddlTransType" CssClass="form-control" ViewEnumName="Netpay.Infrastructure.PartnerTransType" Value='<%# (int)Item.TransType %>' Enabled='<%# Item.ID == 0 %>' EnableBlankSelection="false" />
            </div>
            <div class="col-md-8">
                Caclulation<br />
                <netpay:EnumDropDown runat="server" CssClass="form-control" ID="ddlCalcMethod" ViewEnumName="Netpay.Infrastructure.PartnerFeeCalc" Value='<%# (int)Item.CalcMethod %>' EnableBlankSelection="false" />
            </div>
        </div>
        <div class="alert alert-info spacer">
            <b>Calculation Explanation:</b><br />
            <ol>
                <li><i>From Volume : </i>
                    <br />
                    Calculate affiliate fees from the total volume minus refunds & CHB's. </li>
                <li><i>From Fees : </i>
                    <br />
                    Calculate affiliate fees from the fees netpay took from the merchant. </li>
                <li><i>(MerchantSale - TerminalBuy) : </i>
                    <br />
                    Calculate affiliate fees from the fees netpay took from the merchant minus the fees the bank otook from us.</li>
                <li><i>(MerchantSale - AffiliateBuy) : </i>
                    <br />
                    Calculate affiliate fees from the fees netpay took from the merchant minus affiliate buy price (in the row).</li>
            </ol>
        </div>
        <h4>
            <asp:Label runat="server" Text="Fee Steps" Font-Bold="true" /></h4>
        <div class="row">
            <div class="col-md-4"><span title="Affiliate Sale Price (%)">Split Total (%)</span></div>
            <div class="col-md-4"><span title="this is the upto amount for the step">Step Amount</span></div>
            <div class="col-md-4"><span title="Actual fee taken from result">Fee (%)</span></div>
        </div>
        <netpay:DynamicRepeater runat="server" ID="rptSteps" DataSource='<%# Item.Steps %>'>
            <ItemTemplate>
                <div class="row spacer">
                    <div class="col-md-4">
                        <asp:TextBox ID="txtSlicePercent" runat="server" Text='<%# Eval("SlicePercent", "{0:0.00}") %>' CssClass="form-control" />
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtUpToAmount" runat="server" Text='<%# Eval("UpToAmount", "{0:0.000}") %>' CssClass="form-control" />
                    </div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtPercentFee" runat="server" Text='<%# Eval("PercentFee", "{0:0.00}") %>' CssClass="form-control" />
                    </div>
                </div>
            </ItemTemplate>
        </netpay:DynamicRepeater>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" EnableSave="true"></admin:DataButtons>
    </Footer>
</admin:ModalDialog>
