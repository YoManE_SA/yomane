﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Data.ascx.cs" Inherits="Netpay.Admin.Affiliates.Data" %>
<%@ Register Src="~/Modules/Accounts/Address.ascx" TagPrefix="CUC" TagName="AccountAddress" %>

<admin:FormSections runat="server" Title="Payout information">
    <div class="row">
        <div class="col-lg-4">
            <div class="form-group">
                Affiliate Name
                 <asp:TextBox runat="server" CssClass="form-control" ID="txtName" Text='<%# ItemData.Name %>' />
                <asp:CustomValidator
                    ValidateEmptyText="true"
                    runat="server"
                    ID="cfvName"
                    ControlToValidate="txtName"
                    Display="Dynamic"
                    ForeColor="red"
                    ErrorMessage="Merchant title cannot be blank!"
                    OnServerValidate="CheckName" />
            </div>

            <div class="form-group">
                Reference Code
                <asp:TextBox runat="server" CssClass="form-control" ID="txtLinkRefID" Text='<%# ItemData.LinkRefID %>' /><br />
                <asp:CustomValidator
                    runat="server"
                    ID="cfvLinkRefId"
                    ControlToValidate="txtLinkRefID"
                    Display="Dynamic"
                    ForeColor="red"
                    ErrorMessage="Reference Code is already in use,<br />Plese enter a new one."
                    OnServerValidate="CheckRefCode" />
            </div>
            <div class="form-group">
                Pin Generation Mode<br />
                <asp:DropDownList ID="ddlPinGeneration" CssClass="form-control" runat="server">
                     <asp:ListItem Value="-1">Select ...</asp:ListItem>
            <asp:ListItem Value="1">ID Number: Last 4</asp:ListItem>
            <asp:ListItem Value="2">Random Number</asp:ListItem>
                </asp:DropDownList> 
                <asp:RequiredFieldValidator 
                    ID="rfv_ddlPinGeneration" ControlToValidate="ddlPinGeneration" InitialValue ="-1" runat="server" 
                    ErrorMessage="Select a valid pin code generation mode."></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                Legal Name
		    	<asp:TextBox runat="server" CssClass="form-control" ID="txtLegalName" Text='<%# ItemData.LegalName %>' />
            </div>
            <div class="form-group">
                Legal Number
		    	<asp:TextBox runat="server" CssClass="form-control" ID="txtLegalNumber" Text='<%# ItemData.LegalNumber %>' />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                Payee Name
			    <asp:TextBox runat="server" CssClass="form-control" ID="txtPaymentPayeeName" Text='<%# ItemData.PaymentPayeeName %>' />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                Default Payment Currency
		    <netpay:CurrencyDropDown
                CssClass="form-control"
                ID="ddlCurrency"
                runat="server"
                EnableBlankSelection="false"
                Value="<%#ItemData.PaymentReceiveCurrency.HasValue ? ItemData.PaymentReceiveCurrency.Value : 0 %>">
            </netpay:CurrencyDropDown>
            </div>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Address">
    <CUC:AccountAddress runat="server" ID="caAddress" AddressData='<%# ItemData.PersonalAddress %>' ValidationGroup="Affiliates_Data_ddl" />
</admin:FormSections>

<%--<admin:FormSections runat="server" Title="Allow Pages">
    <asp:MultiView runat="server" ActiveViewIndex="<%#Account!=null && Account.AffiliateID != null && Account.LoginID!=null ? 0 : 1 %>">
        <asp:View runat="server">
            <asp:CheckBoxList runat="server" CssClass="checkbox-list" ID="chlAllowPages" RepeatLayout="UnorderedList">
            </asp:CheckBoxList>
        </asp:View>
        <asp:View runat="server">
            Save affiliate and afterwards insert correct login details in order to edit this section.
        </asp:View>
    </asp:MultiView>
</admin:FormSections>--%>

<admin:FormPanel runat="server" ID="LogoFormPanel" Title="Top Logo"> 
        <asp:MultiView runat="server" ActiveViewIndex="<%# (ItemData == null || string.IsNullOrEmpty(ItemData.HeaderImageFileName))? 0 : 1 %>">
        <asp:View runat="server">
          
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                    <div class="input-group">
					    <NP:FileUpload runat="server" ID="fuDocument" />
				    </div>
                    </div>
                     <div class="form-group">
                        <asp:Button ID="btnUpload" OnClick="UploadFile" Text="Upload File" CssClass="btn btn-info" runat="server" />
                    </div>
                </div>
                
            </div>
        </asp:View>
        <asp:View runat="server">
            <div class="row">
                <div class="col-md-12">
                    <asp:Literal runat="server" ID="litFileNotFound" Visible="false" Text="File not found. Please delete and upload again."></asp:Literal>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <ul class="list-group">
                            <li class="list-group-item"> <i class="fa fa-picture-o fa-fw"></i> <asp:Literal runat="server" Text='<%# fiFile!=null? fiFile.Name : "" %>'/></li>
                            <li class="list-group-item"> <i class="fa fa-file-image-o fa-fw"></i> <asp:Literal runat="server" Text='<%# fiFile!=null? fiFile.Length + " bytes " +imgFile.Width + "px width x " + imgFile.Height + "px height" : "" %>' /></li>       
                             <asp:PlaceHolder runat="server" Visible='<%# (ItemData != null && !string.IsNullOrEmpty(ItemData.HeaderImageFileName))  %>'>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-md-6">
                                        <% if (imgFile != null) imgFile.Dispose(); %>
                                        <img runat="server" class="img-thumbnail" src='<%# (ItemData != null && !string.IsNullOrEmpty(ItemData.HeaderImageFileName))? Netpay.Infrastructure.Domain.Current.MapPublicDataVirtualPath(ItemData.HeaderImageFileName): "" %>' height='<%# (imgFile ==null || imgFile.Height>80)? 80 : imgFile.Height  %>' />
                                    </div>
                                    <div class="col-md-6 text-left">
                                       <asp:Linkbutton  OnClick="DeleteFile" CssClass="btn btn-danger" runat="server"><i class="fa fa-trash"></i> Delete File</asp:Linkbutton>
                                    </div>
                                </div>
                            </li>
                            </asp:PlaceHolder>
                        </ul>
                    </div>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>
</admin:FormPanel>


<admin:ModalDialog runat="server" ID="dlgCountryNotSelectedError" Title="Save Affiliate Error Msg">
    <Body>
        <netpay:ActionNotify runat="server" ID="SaveAffiliateErrorMsg" />
    </Body>
</admin:ModalDialog>


