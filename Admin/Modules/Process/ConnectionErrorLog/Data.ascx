﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Data.ascx.cs" Inherits="Netpay.Admin.Process.ConnectionErrorLog.Data" %>
<admin:FormSections runat="server" Title="Details">
    <asp:HiddenField runat="server" ID="hfItemID" Value='<%# Item.ID %>' />

    <!-- HTTP Error -->
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                HTTP Error
                <asp:TextBox runat="server" ReadOnly="true" ID="lblHttpError" CssClass="form-control" Text='<%# Item.HTTP_Error %>' />
            </div>
        </div>
    </div>

</admin:FormSections>

<admin:FormSections runat="server" Title="Log Refund Attempt">
    <asp:MultiView runat="server" ActiveViewIndex='<%# (RefundLog != null && RefundLog.Count > 0) ? 0 : 1 %>'>
        <asp:View runat="server">
            <div class="table-responsive">
                <admin:AdminList runat="server" SaveAjaxState="true" ID="rptList" DataKeyNames="ID" AutoGenerateColumns="false" BubbleLoadEvent="true"  IsInListView="false">
                    <Columns>
                        <asp:BoundField HeaderText="ID" DataField="ID" SortExpression="ID" />
                        <asp:BoundField HeaderText="Date & Time" DataField="InsertDate" SortExpression="InsertDate" />
                        <asp:BoundField HeaderText="Reply" DataField="ReplyCode" SortExpression="ReplyCode" />
                        <asp:BoundField HeaderText="Description" DataField="DescriptionOriginal" SortExpression="DescriptionOriginal" />
                        <asp:BoundField HeaderText="Answer" DataField="Answer" SortExpression="Answer" />
                    </Columns>
                </admin:AdminList>
            </div>
        </asp:View>
        <asp:View runat="server">
            <div class="alert alert-info">
                No refund attempt found for this transaction.
            </div>
        </asp:View>
    </asp:MultiView>
</admin:FormSections>
