﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;

namespace Netpay.Admin.Process.ConnectionErrorLog
{
    public partial class Data : Controls.TemplateControlBase
    {
        protected Bll.Process.ConnectionErrorLog Item { get { return TemplatePage.GetItemData<Bll.Process.ConnectionErrorLog>(); } set { TemplatePage.SetItemData(value); } }
        protected List<Bll.Process.ConnectionErrorLog.RefundLogItem> RefundLog
        {
            get
            {
                if (Item == null || Item.TransactionFailID <= 0)
                    return null;

                return Item.GetRefundLog(Item.TransactionFailID);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            if (Item == null) Item = new Bll.Process.ConnectionErrorLog();
            rptList.DataSource = RefundLog;
            base.DataBindChildren();
        }
    }
}