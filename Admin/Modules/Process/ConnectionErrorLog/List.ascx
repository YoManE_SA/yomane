﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Process.ConnectionErrorLog.List" %>
<admin:ListSection runat="server" Title="Connection Errors Log">
    <Header>
        <admin:LegendColors runat="server" Items='<%# Netpay.Bll.Process.ConnectionErrorLog.RefundStatusColor %>' FloatRight="true" />
    </Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" ID="rptList" DataKeyNames="ID" SaveAjaxState="true" AutoGenerateColumns="false" PageSize="25" BubbleLoadEvent="true">
                <Columns>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <span style="background-color: <%# Netpay.Bll.Process.ConnectionErrorLog.RefundStatusColor.MapHtmlColor((RefundStatus)Eval("AutoRefundStatus")) %>;" class="legend-item"></span>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ID" SortExpression="ID">
                        <ItemTemplate>
                            <%# Eval("ID") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Reply" SortExpression="DebitReturnCode">
                        <ItemTemplate>
                            <%# Eval("DebitReturnCode") != null ? ((int)Eval("DebitReturnCode")).ToString() : "" %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Failed Transaction ID" DataField="TransactionFailID" SortExpression="TransactionFailID" />
                    <asp:BoundField HeaderText="Insert Date" DataField="InsertDate" SortExpression="InsertDate" />
                    <asp:BoundField HeaderText="Merchant Name" DataField="MerchantName" SortExpression="MerchantName" />
                    <asp:TemplateField HeaderText="Debit Company" SortExpression="DebitCompanyName">
                        <ItemTemplate>
                            <asp:Image ID="imgDebitCompany" runat="server" ImageUrl='<%# "/NPCommon/ImgDebitCompanys/23X12/" + (int)Eval("DebitCompanyID") + ".gif" %>' ImageAlign="Left" Visible='<%# (Eval("DebitCompanyName") != null && System.IO.File.Exists(Server.MapPath("/NPCommon/ImgDebitCompanys/23X12/" + (int)Eval("DebitCompanyID") + ".gif")))  %>' />
                            <%# Eval("DebitCompanyName") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Terminal Number" DataField="TerminalNumber" SortExpression="TerminalNumber" />
                    <asp:BoundField HeaderText="Ip Address" DataField="IpAddress" SortExpression="IpAddress" />
                    <asp:TemplateField HeaderText="Amount" SortExpression="Amount">
                        <ItemTemplate>
                            <%# new Money((decimal)Eval("Amount"), (byte)Eval("Currency")).ToIsoString() %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="HTTP Error" DataField="HTTP_Error" SortExpression="HTTP_Error" />
                    <asp:BoundField HeaderText="Refund Status" DataField="AutoRefundStatus" SortExpression="AutoRefundStatus" />
                    <asp:BoundField HeaderText="Last Attempt" DataField="AutoRefundDate" SortExpression="AutoRefundDate" />
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd="false" />
    </Footer>

</admin:ListSection>
