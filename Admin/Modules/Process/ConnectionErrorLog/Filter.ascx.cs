﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Process.ConnectionErrorLog
{
    public partial class Filter : Controls.TemplateControlBase
    {
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
        {
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var queryString = TemplatePage.Request.QueryString.ToString();
            var sf = new Bll.Process.ConnectionErrorLog.SearchFilters();

            //This query comes from Home page - notifications.
            if (queryString.Contains("LastSixHours"))
            {
                sf.Date = new Range<DateTime?>() { From = DateTime.Now.AddHours(-6) };
                TemplatePage.SetFilter(sf);

                //Set the ViewMode.
                (Page as Controls.DataTablePage).PageController.ForceModeUpdate = Admin.Controls.DataPageController.ViewMode.List;
            }
            else if (queryString.Contains("ErrorsWithoutRefund"))
            {
                sf.Date.From = DateTime.Now.AddDays(-3);
                sf.Date.To = DateTime.Now.AddHours(-8);
                sf.AttemptedRefund = false;
                TemplatePage.SetFilter(sf);

                //Set the ViewMode.
                (Page as Controls.DataTablePage).PageController.ForceModeUpdate = Admin.Controls.DataPageController.ViewMode.List;
            }
            else
            {
                // ID
                sf.ID = rngID.Value;

                // Date
                sf.Date = rngDate.Value;

                // MerchantID
                if (apAccount.Value.HasValue)
                {
                    var account = Bll.Accounts.Account.LoadAccount(apAccount.Value.GetValueOrDefault());
                    sf.MerchanId = account.MerchantID;
                }

                // TransactionFailID
                sf.TransactionFailID = new List<int>();
                if (txtTransactionFailIDs.Text != null && txtTransactionFailIDs.Text.Contains(","))
                {
                    // In this case there are multiple values separated by comma
                    string[] transcationFailIDsStr = txtTransactionFailIDs.Text.Split(',').Where(i => i != null).ToArray();
                    foreach (string transcationFailIDStr in transcationFailIDsStr)
                    {
                        int transactionFailID;
                        if (int.TryParse(transcationFailIDStr, out transactionFailID))
                            sf.TransactionFailID.Add(transactionFailID);
                    }
                }
                else if (txtTransactionFailIDs.Text != null && txtTransactionFailIDs.Text.Length > 0)
                {
                    // In this case there should be a single value
                    int transactionFailID;
                    if (int.TryParse(txtTransactionFailIDs.Text, out transactionFailID))
                        sf.TransactionFailID.Add(transactionFailID);
                }

                // DebitCompany
                sf.DebitCompany = (DebitCompany?)ddlDebitCompany.Value.ToNullableInt();

                // DebitReturnCodeValues
                sf.DebitReturnCodeValues = new List<Bll.DebitReturnCodes>();
                sf.DebitReturnCodeValues.AddRange(emsDebitReturnCode.SelectedToEnumValues<Bll.DebitReturnCodes>());

                // TerminalNumber
                sf.TerminalNumber = txtTerminalNumber.Text;

                // AttemptedRefund
                switch (rbtAttemptedRefund.Text)
                {
                    case "1":
                        sf.AttemptedRefund = true;
                        break;
                    case "0":
                        sf.AttemptedRefund = false;
                        break;
                    default:
                        sf.AttemptedRefund = null;
                        break;
                }

                sf.AutoRefundStatus = emsRefundStatus.SelectedToEnumValues<Bll.RefundStatus>();

                // Set Filter
                TemplatePage.SetFilter(sf);
            }
        }
    }
}