﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Process.ConnectionErrorLog.Filter" %>

<admin:FilterSection runat="server" Title="Record Information">
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblID" runat="server" Text="ID Range" />
                <JQ:IntRange runat="server" ID="rngID" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblStatus" runat="server" Text="Insert Date Range" />
                <JQ:DateRange runat="server" ID="rngDate" />
            </div>
        </div>
    </div>
</admin:FilterSection>

<admin:FilterSection runat="server" Title="Merchant">
    <div class="form-group">
        <asp:Label ID="lblAccount" runat="server" Text="Merchant Name"  />
        <admin:AccountPicker runat="server" ID="apAccount" LimitToType="Merchant" />
    </div>
</admin:FilterSection>

<admin:FilterSection runat="server" Title="Initial Transaction">
    <div class="form-group">
        <asp:Label ID="lblTransactionFailIDs" runat="server" Text="Transaction Fail ID" />
        <div class="row">
            <div class="col-md-12">
                <asp:TextBox runat="server" ID="txtTransactionFailIDs" CssClass="form-control" ToolTip="Type here single transaction ID or multiple comma-separated IDs"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="form-group">
        <asp:Label ID="lblDebitReturnCode" runat="server" Text="Reply Code" />
        <div class="row">
            <div class="col-md-12">
                <netpay:EnumMultiSelect runat="server" ID="emsDebitReturnCode" CssClass="chkList" ViewEnumName="Netpay.Bll.DebitReturnCodes" RepeatColumns="3" RepeatLayout="Flow"
                    CellPadding="0" CellSpacing="0" />
            </div>
        </div>
    </div>
</admin:FilterSection>

<admin:FilterSection runat="server" Title="Debit Company">
    <div class="form-group">
        <asp:Label runat="server" Text="DebitCompany" />
        <div class="row">
            <div class="col-md-12">
                <netpay:DebitCompanyDropDown CssClass="form-control" runat="server" ID="ddlDebitCompany" />
            </div>
        </div>
    </div>
    <div class="form-group">
        <asp:Label ID="lblTerminalNumber" runat="server" Text="Terminal Number" />
        <div class="row">
            <div class="col-md-12">
                <asp:TextBox runat="server" ID="txtTerminalNumber" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
    </div>
</admin:FilterSection>

<admin:FilterSection runat="server" Title="Refund Attempt">
    <div class="form-group">
        <asp:Label ID="lblAttemptedRefund" runat="server" Text="AttemptedRefund" />
        <div class="row">
            <div class="col-md-12">
                <asp:RadioButtonList ID="rbtAttemptedRefund" runat="server"
                    RepeatDirection="Horizontal" RepeatLayout="Table" CssClass="radio">
                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                    <asp:ListItem Text="All" Value="" Selected="True"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
    </div>
    <div class="form-group">
        <asp:Label ID="lblRefundStatus" runat="server" Text="Refund Status" />
        <div class="row">
            <div class="col-md-12">
                <netpay:EnumMultiSelect runat="server" ID="emsRefundStatus" CssClass="chkList" ViewEnumName="Netpay.Bll.RefundStatus" RepeatColumns="3" RepeatLayout="Flow"
                    CellPadding="0" CellSpacing="0" />
            </div>
        </div>
    </div>
</admin:FilterSection>
