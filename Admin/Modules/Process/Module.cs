﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Process
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuChargeAttempts, menuConnectionErrors;
        public override string Name { get { return "Process"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return "Manage Connection Error and Charge Attempts logs."; } }

        public Module() : base(Bll.Process.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu item
            menuChargeAttempts = new ApplicationMenu("Charge Attempts", "~/Logs/ChargeAttemptsLog", null, 120);
            menuConnectionErrors = new ApplicationMenu("Connection Errors", "~/Logs/ConnectionErrorLog", null, 130);

            //Register routes
            Application.RegisterRoute("Logs/ChargeAttemptsLog", "ChargeAttemptsLog", typeof(Controls.DataTablePage), module: this);
            Application.RegisterRoute("Logs/ConnectionErrorLog", "ConnectionErrorLog", typeof(Controls.DataTablePage), module: this);

            //Register events
            Application.InitDataTablePage += Application_InitDataTablePage;
            Application.InitPage += Application_InitPage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Logs", menuChargeAttempts);
            Application.AddMenuItem("Logs", menuConnectionErrors);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuChargeAttempts.Parent.RemoveChild(menuChargeAttempts);
            menuConnectionErrors.Parent.RemoveChild(menuConnectionErrors);
            base.OnUninstallAdmin(e);
        }

        private void Application_InitPage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Default;
            if (page != null && page.TemplateName == "Home")
            {
                if (IsActive)
                {
                    if (Bll.Process.ChargeAttemptLog.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Read))
                    {
                        var pfInfo = new Infrastructure.SortAndPage(0, 1);
                        Netpay.Bll.Process.ChargeAttemptLog.Search(new Bll.Process.ChargeAttemptLog.SearchFilters() { ReplyCode = "---", Date = new Infrastructure.Range<DateTime?>(DateTime.Now.AddDays(-60), DateTime.Now.AddDays(-7)) }, pfInfo);
                        if (pfInfo.RowCount > 0) page.Notifications.Controls.Add(new Controls.Notification() { TitleText = "Uncomplete Processing", MessageText = string.Format("There are {0} uncomplete or error transaction attempts", pfInfo.RowCount) });
                    }

                    else
                    {
                        var ctl1 = page.ModuleNotAuthorized;
                        string name = Admin.Application.Current.Modules.Where(x => (x is CoreBasedModule) && (x as CoreBasedModule).CoreModule.ID == Bll.Process.ChargeAttemptLog.SecuredObject.ModuleID).SingleOrDefault().Name;
                        (ctl1 as Admin.Modules.ModuleNotAuthorized.ModuleNotAuthorized).ModuleName = name;
                        (ctl1 as Admin.Modules.ModuleNotAuthorized.ModuleNotAuthorized).Permission = "Read";

                        page.Notifications.Controls.Add(ctl1);
                    }

                    if (Bll.Transactions.Transaction.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Read))
                    {
                        var ts = Netpay.Bll.Transactions.Transaction.GetTransactionCount(DateTime.Now.AddDays(-2));
                        page.Status.Controls.Add(new Controls.Counter() { TitleText = "Total Processing On 48 Hours", Value = ts.PassCount + ts.FailCount + ts.ApprovalCount + ts.PendingCount });
                        page.Status.Controls.Add(new Controls.Counter() { TitleText = "Total Authorized On 48 Hours", Value = ts.ApprovalCount });
                        page.Status.Controls.Add(new Controls.Counter() { TitleText = "Total Aprovals On 48 Hours", Value = ts.PassCount });
                        page.Status.Controls.Add(new Controls.Counter() { TitleText = "Total Declines On 48 Hours", Value = ts.FailCount });
                    }
                    else
                    {
                        var ctl2 = page.ModuleNotAuthorized;
                        string name = Admin.Application.Current.Modules.Where(x => (x is CoreBasedModule) && (x as CoreBasedModule).CoreModule.ID == Bll.Transactions.Transaction.SecuredObject.ModuleID).SingleOrDefault().Name;
                        (ctl2 as Admin.Modules.ModuleNotAuthorized.ModuleNotAuthorized).ModuleName = name;
                        (ctl2 as Admin.Modules.ModuleNotAuthorized.ModuleNotAuthorized).Permission = "Read";

                        page.Status.Controls.Add(ctl2);
                    }

                }
                else
                {
                    var ctl = page.ModuleNotActivated;
                    (ctl as Modules.ModuleNotActivated.ModuleNotActivated).ModuleName = Name;
                    page.Notifications.Controls.Add(ctl);
                    page.Status.Controls.Add(ctl);
                }
            }
        }

        private void Application_InitDataTablePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;

            if (page.TemplateName == "ChargeAttemptsLog")
            {
                page.PageController.LoadItem += PageController_ChargeAttemptsLog_LoadItem;
                page.AddControlToList(page.LoadControl("~/Modules/Process/ChargeAttemptsLog/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Process/ChargeAttemptsLog/Filter.ascx"));
                page.AddControlToForm("Data", page.LoadControl("~/Modules/Process/ChargeAttemptsLog/Data.ascx"), this, "", Bll.Process.ChargeAttemptLog.SecuredObject);

                var control = page.LoadControl("~/Modules/Transactions/Details.ascx");
                page.Items.Add("ChargeAttemptsLog_TransactionInfoControl", control);
                page.AddControlToForm("Transaction Info", control, this, "", new Controls.SecuredObjectSelector(Bll.Process.ChargeAttemptLog.SecuredObject, Bll.Transactions.Transaction.SecuredObject).GetActivObject);
            }

            else if (page.TemplateName == "ConnectionErrorLog")
            {
                page.PageController.LoadItem += PageController_ConnectionErrorLog_LoadItem;
                page.AddControlToList(page.LoadControl("~/Modules/Process/ConnectionErrorLog/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Process/ConnectionErrorLog/Filter.ascx"));
                page.AddControlToForm("Data", page.LoadControl("~/Modules/Process/ConnectionErrorLog/Data.ascx"), this, "", Bll.Process.ConnectionErrorLog.SecuredObject);

                var control = page.LoadControl("~/Modules/Transactions/Details.ascx");
                page.Items.Add("ConnectionErrorLog_TransactionInfoControl", control);
                page.AddControlToForm("Transaction Info", control, this, "", new Controls.SecuredObjectSelector(Bll.Process.ConnectionErrorLog.SecuredObject, Bll.Transactions.Transaction.SecuredObject).GetActivObject);
            }
        }

        void PageController_ChargeAttemptsLog_LoadItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            page.ItemID = page.PageController.DataKey["ID"].ToNullableInt();
            var item = Netpay.Bll.Process.ChargeAttemptLog.Load(page.ItemID.GetValueOrDefault());
            if (page.ItemID != null) page.SetItemData(item);
            page.FormButtons.EnableSave = false;
            page.FormButtons.EnableDelete = false;

            var control = page.Items["ChargeAttemptsLog_TransactionInfoControl"] as Transactions.Details;
            if (control != null)
            {
                control.ItemData = item.Transaction;
            }
        }

        void PageController_ConnectionErrorLog_LoadItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            page.ItemID = page.PageController.DataKey["ID"].ToNullableInt();
            var item = Netpay.Bll.Process.ConnectionErrorLog.Load(page.ItemID.GetValueOrDefault());
            if (page.ItemID != null) page.SetItemData(item);
            page.FormButtons.EnableSave = false;
            page.FormButtons.EnableDelete = false;

            var control = page.Items["ConnectionErrorLog_TransactionInfoControl"] as Transactions.Details;
            if (control != null)
            {
                control.ItemData = item.Transaction;
            }
        }
    }
}