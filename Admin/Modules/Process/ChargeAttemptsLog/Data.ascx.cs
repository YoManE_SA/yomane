﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;

namespace Netpay.Admin.Process.ChargeAttemptsLog
{
    public partial class Data : Controls.TemplateControlBase
    {
        protected Bll.Process.ChargeAttemptLog Item { get { return TemplatePage.GetItemData<Bll.Process.ChargeAttemptLog>(); } set { TemplatePage.SetItemData(value); } }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            if (Item == null) Item = new Bll.Process.ChargeAttemptLog();
            base.DataBindChildren();
        }
    }
}