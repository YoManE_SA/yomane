﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;

namespace Netpay.Admin.Process.ChargeAttemptsLog
{
	public partial class Filter : Controls.TemplateControlBase
	{
		protected override void OnLoad(EventArgs e)
		{
			TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
            base.OnLoad(e);
		}

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
		{
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var queryString = TemplatePage.Request.QueryString.ToString();
            var sf = new Bll.Process.ChargeAttemptLog.SearchFilters();
            
            //This query comes from Home page - notifications.
            if (queryString.Contains("ChargeAttemptsWithoutTransaction") && Request.Params["__EVENTTARGET"] == null)
            {
                sf.Date = new Range<DateTime?>() { From = DateTime.Now.AddDays(-3), To = DateTime.Now.AddMinutes(-5) };
                sf.WithoutTransaction = true;
                TemplatePage.SetFilter(sf);

                //Set the mode to List mode.
                (Page as Controls.DataTablePage).PageController.ForceModeUpdate = Admin.Controls.DataPageController.ViewMode.List;
            }
            // Regular flow
            else
            {
                sf.ID = rngID.Value;
                sf.Date = rngDate.Value;
                if (!string.IsNullOrEmpty(TransId.Text))
                    sf.TransactionId = int.Parse(TransId.Text);
                if (!string.IsNullOrEmpty(SpecificChargeAttemptLogID.Text))
                {
                    sf.specificChargeAttemptLogId = int.Parse(SpecificChargeAttemptLogID.Text.Trim());
                }

                if (ddIsSuccess.IsSelected)
                {
                    sf.IsSuccess = ddIsSuccess.BoolValue.Value;
                }

                if (Request.QueryString["TransactionStatus"] != null)
                {
                    int transStatusIntVal = int.Parse(Request.QueryString["TransactionStatus"].ToString());
                    sf.TransactionStatus = (TransactionStatus)transStatusIntVal;
                }

                TemplatePage.SetFilter(sf);

                if (sf.specificChargeAttemptLogId.HasValue) // That means you get here from HomePage-Notifications tab - and you aren't in AsyncPostBack mode, so you need to enforce the FilterList viewmode.
                {
                    (Page as Controls.DataTablePage).PageController.ForceModeUpdate = Admin.Controls.DataPageController.ViewMode.List;
                }
            }
        }
    }
}