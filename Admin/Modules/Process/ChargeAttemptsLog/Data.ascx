﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Data.ascx.cs" Inherits="Netpay.Admin.Process.ChargeAttemptsLog.Data" %>
<admin:FormSections runat="server" Title="Query String">
    <asp:HiddenField runat="server" ID="hfItemID" Value='<%# Item.ID %>' />
    <div class="row">

        <div class="col-md-12">
            <asp:Repeater ID="rptQueryStringItems" runat="server" DataSource="<%# (Item != null && Item.QueryString != null && Item.QueryString.Contains('|')) ? Item.QueryString.Split(new char[] { '|' }) : new string[] { } %>">
                <HeaderTemplate>
                    <ul class="list-group">
                </HeaderTemplate>
                <ItemTemplate>
                    <li class="list-group-item"><b><%# !((string)Container.DataItem).Contains('=') ? "" : ((string)Container.DataItem).Substring(0, ((string)Container.DataItem).IndexOf('=')) %></b> <%# !((string)Container.DataItem).Contains('=') ? "" : ((string)Container.DataItem).Substring(((string)Container.DataItem).IndexOf('=')) %></li>
                </ItemTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Session Contents">
    <div class="row">
        <div class="col-md-12">
            <asp:Repeater ID="rptSessionContents" runat="server" DataSource="<%# (Item != null && Item.SessionContents != null && Item.SessionContents.Contains('|')) ? Item.SessionContents.Split(new char[] { '|' }) : new string[] { } %>">
                <HeaderTemplate>
                    <ul class="list-group">
                </HeaderTemplate>
                <ItemTemplate>
                    <li class="list-group-item"><b><%# !((string)Container.DataItem).Contains('=') ? "" : ((string)Container.DataItem).Substring(0, ((string)Container.DataItem).IndexOf('=')) %></b>
                        <%# !((string)Container.DataItem).Contains('=') ? "" : ((string)Container.DataItem).Substring(((string)Container.DataItem).IndexOf('=')) %></li>
                </ItemTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Http Host">
    <div class="row">
        <div class="col-md-12">
            <ul class="list-group">
                <li class="list-group-item"><b>Http Host:</b> <%# Item.HttpHost %></li>
            </ul>
        </div>
    </div>
</admin:FormSections>
<!-- RequestMethod -->
<admin:FormSections runat="server" Title="Request Method">
    <div class="row">
        <div class="col-md-12">
            <ul class="list-group">
                <li class="list-group-item"><b>Request Method:</b> <%# Item.RequestMethod %></li>
            </ul>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Local Address">
    <div class="row">
        <div class="col-md-12">
            <ul class="list-group">
                <li class="list-group-item"><b>Local Address</b> <%# Item.LocalIPAddress %></li>
            </ul>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Time String">
    <div class="row">
        <div class="col-md-12">
            <asp:Repeater ID="rptTimeString" runat="server" DataSource="<%# (Item != null && Item.TimeString != null && Item.TimeString.Contains('|')) ? Item.TimeString.Split(new char[] { '|' }) : new string[] { } %>">
                <HeaderTemplate>
                    <ul class="list-group">
                </HeaderTemplate>
                <ItemTemplate>
                    <li class="list-group-item"><b><%# !((string)Container.DataItem).Contains('=') ? "" : ((string)Container.DataItem).Substring(0, ((string)Container.DataItem).IndexOf('=')) %></b> <%# !((string)Container.DataItem).Contains('=') ? "" : ((string)Container.DataItem).Substring(((string)Container.DataItem).IndexOf('=')) %></li>
                </ItemTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
</admin:FormSections>

<admin:FormSections runat="server" Title="Response String">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                Request String
<%--                <asp:TextBox runat="server" TextMode="MultiLine" ReadOnly="true" Rows="6" ID="lblRequestString" CssClass="form-control" Text='<%# Item.RequestString %>' Visible="false" />--%>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                Response String
<%--                <asp:TextBox runat="server" TextMode="MultiLine" ReadOnly="true" Rows="6" ID="lblResponseString" CssClass="form-control" Text='<%# Item.ResponseString %>' Visible="false" />--%>
            </div>
        </div>
    </div>
</admin:FormSections>

