﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Process.ChargeAttemptsLog.Filter" %>
<admin:FilterSection runat="server" Title="Search">

    <asp:TextBox ID="SpecificChargeAttemptLogID" Visible="false" runat="server"></asp:TextBox>

    <div class="form-group">
        <asp:Label ID="lblAccount" runat="server" Text="Name" />
        <admin:AccountPicker runat="server" ID="apAccount" LimitToType="Merchant" />
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblStatus" runat="server" Text="Date Range" />
                <JQ:DateRange runat="server" ID="rngDate" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblID" runat="server" Text="ID Range" />
                <JQ:IntRange runat="server" ID="rngID" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label runat="server" Text="Trans ID"></asp:Label><br />
                <asp:TextBox runat="server" ID="TransId" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label runat="server" Text="Is Success"></asp:Label><br />
                <netpay:BoolDropDown CssClass="form-control" runat="server" TrueText="Success" FalseText="Fail" ID="ddIsSuccess">
                </netpay:BoolDropDown>
            </div>
        </div>
    </div>
</admin:FilterSection>
