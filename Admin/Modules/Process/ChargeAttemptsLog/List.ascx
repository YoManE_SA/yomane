﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Process.ChargeAttemptsLog.List" %>
<admin:ListSection runat="server" Title="Charge Attempts Log">
    <Header>
        <admin:LegendColors runat="server" Items='<%# Netpay.Bll.Process.ChargeAttemptLog.SuccessOrFailStatus %>' FloatRight="true" />
    </Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" ID="rptList" DataKeyNames="ID" SaveAjaxState="true" AutoGenerateColumns="false" PageSize="25" BubbleLoadEvent="true">
                <Columns>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <span style="background-color: <%# Netpay.Infrastructure.Enums.BoolColorMap.MapHtmlColor((bool)Eval("IsSuccess")) %>;" class="legend-item"></span>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ID" SortExpression="ID">
                        <ItemTemplate>
                            <%# Eval("ID") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Date Start" DataField="DateStart" SortExpression="DateStart" />
                    <asp:TemplateField HeaderText="Duration (sec)" SortExpression="Duration">
                        <ItemTemplate>
                            <%# Eval("Duration") != null ? System.Math.Round(((TimeSpan)Eval("Duration")).TotalSeconds).ToString() : "" %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Merchant" SortExpression="MerchantNumber">
                        <ItemTemplate>
                            <%# GetMerchantName((string)Eval("MerchantNumber")) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="IP Address" DataField="RemoteIPAddress" SortExpression="RemoteIPAddress" />
                    <asp:BoundField HeaderText="Reply" DataField="ReplyCode" SortExpression="ReplyCode" />
                    <asp:BoundField HeaderText="Trans ID" DataField="TransactionID" SortExpression="TransactionID" />
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd="false" />
    </Footer>

</admin:ListSection>
