﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Modules.DebitCompanies
{
    public partial class ReplyCodes : Controls.AccountControlBase
    {
        public bool ButtonFilterClicked;

        public Bll.DebitCompanies.DebitCompany DebitCompanyItem { get; set; }
        public Bll.DebitCompanies.ResponseCode ItemData { get; set; }

        public List<Bll.DebitCompanies.ResponseCode> ReplyCodesList { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            ItemData = new Bll.DebitCompanies.ResponseCode();
            FilterMultiView.ActiveViewIndex = 1;
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            DebitCompanyItem = Account as Netpay.Bll.DebitCompanies.DebitCompany;
            if (DebitCompanyItem == null) DebitCompanyItem = new Bll.DebitCompanies.DebitCompany();
            LoadReplyCodeList();

            base.DataBindChildren();
        }

        protected void LoadReplyCodeList()
        {
            if (DebitCompanyItem == null || DebitCompanyItem.DebitCompanyID == null)
            {
                ReplyCodesList = new List<Bll.DebitCompanies.ResponseCode>();
                return;
            }

            Netpay.Bll.DebitCompanies.ResponseCode.SearchFilters replyCodesFilter = new Bll.DebitCompanies.ResponseCode.SearchFilters();
            replyCodesFilter.DebitCompanyID = DebitCompanyItem.DebitCompanyID;
            ReplyCodesList = Netpay.Bll.DebitCompanies.ResponseCode.Search(replyCodesFilter, null);
            rptReplyCodesList.DataSource = ReplyCodesList;
            rptReplyCodesList.DataBind();
            upReplyCodesList.Update();
        }


        protected void GeneralCommand(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {

                case "FilterShowAll":

                    FilterMultiView.ActiveViewIndex = 1;

                    var DebitCompanyIdShowAll = e.CommandArgument.ToNullableInt().GetValueOrDefault();
                    Netpay.Bll.DebitCompanies.ResponseCode.SearchFilters replyCodesFilterShowAll = new Bll.DebitCompanies.ResponseCode.SearchFilters();
                    replyCodesFilterShowAll.DebitCompanyID = DebitCompanyIdShowAll;
                    ReplyCodesList = Netpay.Bll.DebitCompanies.ResponseCode.Search(replyCodesFilterShowAll, null);
                    rptReplyCodesList.DataSource = ReplyCodesList;
                    rptReplyCodesList.DataBind();

                    upReplyCodesList.Update();
                    break;

                case "FilterFailSource":

                    FilterMultiView.ActiveViewIndex = 0;
                    //get the 2 command arguments : DebitCompanyId and FailSource:
                    var arg = e.CommandArgument.ToString().Split(';');

                    byte failSource;
                    if (!byte.TryParse(arg[0], out failSource))
                    {
                        // Stop handling since got bad value for fail source
                        break;
                    }
                    int debitCompanyId;
                    if (!int.TryParse(arg[1], out debitCompanyId))
                    {
                        // Stop handling since got bad value for debit company id
                        break;
                    }

                    //load the new filtered list
                    Netpay.Bll.DebitCompanies.ResponseCode.SearchFilters replyCodesFilter = new Bll.DebitCompanies.ResponseCode.SearchFilters();
                    replyCodesFilter.DebitCompanyID = debitCompanyId;
                    replyCodesFilter.FailSource = failSource;
                    ReplyCodesList = Netpay.Bll.DebitCompanies.ResponseCode.Search(replyCodesFilter, null);
                    rptReplyCodesList.DataSource = ReplyCodesList;
                    rptReplyCodesList.DataBind();

                    upReplyCodesList.Update();
                    break;

                case "EditReplyCode":
                    var ID = e.CommandArgument.ToNullableInt().GetValueOrDefault();
                    EditReplyCodeFunction(ID);
                    break;

                case "UpdateReplyCode":
                    //get the ID of the ReplyCode item that is going to be updated
                    var updateID = e.CommandArgument.ToNullableInt().GetValueOrDefault();

                    //Get the item from cach:
                    var UpdateReplyCodeItem = Bll.DebitCompanies.ResponseCode.GetItemByResponseCodeId(updateID);

                    //Updating the ReplyCodeItem values.

                    //Merchant Heb
                    UpdateReplyCodeItem.DescriptionMerchantHeb = DescriptionMerchantHebInput.Text;

                    //Merchant Eng
                    UpdateReplyCodeItem.DescriptionMerchantEng = DescriptionMerchantEngInput.Text;

                    //Customer Heb
                    UpdateReplyCodeItem.DescriptionCustomerHeb = DescriptionCustomerHebInput.Text;

                    //Customer Eng
                    UpdateReplyCodeItem.DescriptionCustomerEng = DescriptionCustomerEngInput.Text;

                    //Attempt Series
                    if (RecurringAttemptsSeriesInput.Text != "")
                    {
                        int attemptSeries;
                        bool checkint = int.TryParse(RecurringAttemptsSeriesInput.Text, out attemptSeries);
                        if (checkint && attemptSeries >= 0 && attemptSeries <= 100)
                        {
                            UpdateReplyCodeItem.RecurringAttemptsSeries = attemptSeries;
                        }
                    }

                    //Attempt Charge
                    if (RecurringAttemptsChargeInput.Text != "")
                    {
                        int attemptCharge;
                        bool checkint = int.TryParse(RecurringAttemptsChargeInput.Text, out attemptCharge);
                        if (checkint && attemptCharge >= 0 && attemptCharge <= 100)
                        {
                            UpdateReplyCodeItem.RecurringAttemptsCharge = attemptCharge;
                        }
                    }

                    // Apply Fee Checkbox
                    if (ApplyFeeCheckBox.Checked)
                    {
                        UpdateReplyCodeItem.ChargeFail = true;
                    }
                    else UpdateReplyCodeItem.ChargeFail = false;

                    // Block credit card Checkbox
                    if (BlockCreditCardCheckBox.Checked)
                    {
                        UpdateReplyCodeItem.BlockCC = true;
                    }
                    else UpdateReplyCodeItem.BlockCC = false;

                    //Block email address checkbox
                    if (BlockEmailAddressCheckBox.Checked)
                    {
                        UpdateReplyCodeItem.BlockMail = true;
                    }
                    else UpdateReplyCodeItem.BlockMail = false;


                    //Is cascade checkbox
                    if (ContinueNextCascadedTermianlCheckBox.Checked)
                    {
                        UpdateReplyCodeItem.IsCascade = true;
                    }
                    else UpdateReplyCodeItem.IsCascade = false;


                    if (LocalErrorDropDown.IsSelected || LocalErrorDropDown.Text == "")
                    {
                        if (LocalErrorDropDown.SelectedItem.Text == "")
                        {
                            UpdateReplyCodeItem.LocalError = "";
                        }
                        else
                        {
                            UpdateReplyCodeItem.LocalError = LocalErrorDropDown.Value;
                        }
                    }


                    //Fail Source Enum DropDown input:
                    byte byteValue;
                    bool check = byte.TryParse(RejectTypeEnumInput.Value, out byteValue);
                    if (check) UpdateReplyCodeItem.FailSource = byteValue;


                    if (TemplatePage.Page.IsValid)
                    {
                        UpdateReplyCodeItem.Save();
                    }

                    break;
            }
        }

        protected void EditReplyCodeFunction(int id)
        {
            Netpay.Bll.DebitCompanies.ResponseCode.SearchFilters replyCodesFilter = new Bll.DebitCompanies.ResponseCode.SearchFilters();
            replyCodesFilter.ID = new Netpay.Infrastructure.Range<int?>(id);
            ItemData = Bll.DebitCompanies.ResponseCode.Search(replyCodesFilter, null).FirstOrDefault();

            dlgEditReplyCode.BindAndShow();

        }
    }
}