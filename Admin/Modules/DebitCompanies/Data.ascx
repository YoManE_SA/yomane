﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Data.ascx.cs" Inherits="Netpay.Admin.DebitCompanies.Data" %>
<%@ Register Src="../Accounts/Address.ascx" TagPrefix="CUC" TagName="AccountAddress" %>
<admin:FormSections runat="server" Title="Information">
    <div class="row">
        <div class="col-lg-4">
            <div class="form-group">
                Name
                <asp:TextBox runat="server" CssClass="form-control" ID="txtName" Text='<%# ItemData.Name %>' />
            </div>
            <div class="form-group">
                Description
            
                <asp:TextBox runat="server" ID="txtDescription" CssClass="form-control" Text='<%# ItemData.Description %>' />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                Emergency Phone
                <asp:TextBox runat="server" ID="txtEmergencyPhone" CssClass="form-control" Text='<%# ItemData.EmergencyPhone %>' />
            </div>
            <div class="form-group">
                Emergency Contact
                <asp:TextBox runat="server" ID="txtEmergencyContact" CssClass="form-control" Text='<%# ItemData.EmergencyContact %>' />
            </div>
        </div>

    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Address" Visible="false">
    <CUC:AccountAddress runat="server" ID="caAddress" AddressData='<%# ItemData.PersonalAddress %>'  ValidationGroup="DebitCompanies_Data_ddl"/>
</admin:FormSections>
<admin:FormSections runat="server" Title="Options">
    <div class="row">
        <div class="col-lg-6">
              <ul class="list-group" >
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsActive" Checked='<%# ItemData.IsActive %>' Text="Active" /></li>
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsBlocked" Checked='<%# ItemData.IsBlocked %>' Text="Blocked" /></li>
               <!-- <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsDynamicDescriptor" Checked='' Text="Dynamic Descriptor" /></li>-->
               <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsReturnCode" Checked='<%# ItemData.IsReturnCode %>' Text="ReturnCode" /></li>
           <!--<li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsAllowPayoutWithoutTransfer" Checked='' Text="Allow Payout Without Transfer" /></li>-->
            </ul>
        </div>
        <div class="col-lg-6">
              <ul class="list-group" >
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsAllowRefund" Checked='<%# ItemData.IsAllowRefund %>' Text="Allow Refund" /></li>
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsAllowPartialRefund" Checked='<%# ItemData.IsAllowPartialRefund %>' Text="Allow Partial Refund" /></li>
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsAutoRefund" Checked='<%# ItemData.IsAutoRefund %>' Text="Auto Refund" /></li>
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsNotifyRetReqAfterRefund" Checked='<%# ItemData.IsNotifyRetReqAfterRefund %>' Text="Notify RetReq After Refund" /></li>
            </ul>
        </div>
    </div>

</admin:FormSections>
