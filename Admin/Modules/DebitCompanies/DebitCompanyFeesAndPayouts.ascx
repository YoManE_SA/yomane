﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DebitCompanyFeesAndPayouts.ascx.cs" Inherits="Netpay.Admin.Modules.DebitCompanies.DebitCompanyFeesAndPayouts" %>
<admin:FormSections ID="FeesAndPayoutsFormSection" runat="server" Title="Fees & Payouts" EnableAdd="<%#CheckIfEnableAdd()%>" OnCommand="Dialog_Command" Flexible="false">
    <asp:MultiView ID="MultiViewGlobal" runat="server" ActiveViewIndex="<%# CheckIfEnableAdd()? 0 : 1 %>">
        <asp:View ID="ViewIfDebitCompanyNotNew" runat="server">
            <NP:UpdatePanel runat="server" ID="upFeesAndPayoutsList" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:MultiView ID="FeesAndPayoutsMultiView" runat="server" ActiveViewIndex="<%# (DebitCompanyFeesList.Count > 0) ? 0 : 1%>">
                        <asp:View ID="ViewIfFeeExist" runat="server">
                            <div class="scroll-bar">
                                <NP:PagedRepeater runat="server" ID="rptDebitCompanyFeesList" PageSize="20">
                                    <HeaderTemplate>

                                        <div class="table-responsive">

                                            <table class="table table-hover table-customize">
                                                <thead>
                                                    <th class="text-nowrap">Actions:</th>
                                                    <th class="text-nowrap">Payment Method:</th>
                                                    <th class="text-nowrap">Iso Code: </th>
                                                    <th class="text-nowrap">Currency:</th>
                                                    <th class="text-nowrap">Trans:</th>
                                                    <th class="text-nowrap">Inter (min)</th>
                                                    <th class="text-nowrap">Sale (max)</th>
                                                    <th class="text-nowrap">Clearing</th>
                                                    <th class="text-nowrap">Pre-Auth</th>
                                                    <th class="text-nowrap">Refund</th>
                                                    <th class="text-nowrap">Copy R.</th>
                                                    <th class="text-nowrap">Fail</th>
                                                    <th class="text-nowrap">CHB:</th>
                                                    <th class="text-nowrap">Transaction Days:</th>
                                                    <th class="text-nowrap">PayIn Days: </th>

                                                </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:PlaceHolder Visible="false" runat="server">
                                            <asp:Literal runat="server" ID="ltId" Text='<%# Eval("DcfID") %>' />
                                        </asp:PlaceHolder>
                                        <asp:HiddenField ID="hfDebitCompanyIdFormSection" runat="server" Value='<%# Eval("DcfDebitCompanyId") %>' />
                                        <tbody>
                                            <td class="text-nowrap">
                                                <NP:Button BackColor="Red" OnClientClick="return confirm('Are you sure?')" BorderColor="Red" runat="server" Text="Delete" CssClass="btn btn-xs btn-success text-uppercase" RegisterAsync="true" CommandName="DeleteItem" OnCommand="Dialog_Command" CommandArgument='<%#Eval("DcfID")%>' />
                                                <NP:Button runat="server" Text="Edit" CssClass="btn btn-xs btn-success text-uppercase" RegisterAsync="true" CommandName="EditItem" OnCommand="Dialog_Command" CommandArgument='<%#Eval("DcfID")%>' />
                                            </td>
                                            <td class="text-nowrap"><%# (short)Eval("DcfPaymentMethod")==0? "[All]" : Netpay.Bll.PaymentMethods.PaymentMethod.Get((short)Eval("DcfPaymentMethod")).Name %> </td>
                                            <td class="text-nowrap"><%# (byte)Eval("DcfCurrencyId")==255? "[All]" : Netpay.Bll.Currency.Get((byte)Eval("DcfCurrencyId")).IsoCode %></td>
                                            <td class="text-nowrap"><span class="badge"><%# Eval("DcfFixedCurrency")==null || (byte)Eval("DcfFixedCurrency")==255 ? "[Native]" : Netpay.Bll.Currency.Get((byte)Eval("DcfFixedCurrency")).IsoCode %></span></td>
                                            <td class="text-nowrap"><span class="badge"><%# string.Format("{0:N}",Eval("DcfFixedFee"))%></span></td>
                                            <td class="text-nowrap"><span class="badge"><%# Eval("DcfMinPrecFee")==null? "" : string.Format("{0:N}",Eval("DcfMinPrecFee"))+"%"%></span></td>
                                            <td class="text-nowrap"><span class="badge"><%# Eval("DcfMaxPrecFee")==null? "" : string.Format("{0:N}",Eval("DcfMaxPrecFee"))+"%"%></span></td>
                                            <td class="text-nowrap"><span class="badge"><%# string.Format("{0:N}",Eval("DcfPercentFee"))%>%</span></td>
                                            <td class="text-nowrap"><span class="badge"><%# string.Format("{0:N}",Eval("DcfApproveFixedFee")) %></span></td>
                                            <td class="text-nowrap"><span class="badge"><%# string.Format("{0:N}",Eval("DcfRefundFixedFee")) %></span></td>
                                            <td class="text-nowrap"><span class="badge"><%# string.Format("{0:N}",Eval("DcfClarificationFee")) %></span></td>
                                            <td class="text-nowrap"><span class="badge"><%# string.Format("{0:N}",Eval("DcfFailFixedFee")) %></span></td>
                                            <td class="text-nowrap">
                                                <netpay:CurrencyDropDown Enabled="false" CssClass="form-control" runat="server" ID="ddlDcfChbCurrencyshow" Value='<%# Eval("DcfChbCurrency")==null? 255 : Eval("DcfChbCurrency") %>' BlankSelectionText="[Native]" BlankSelectionValue="255" EnableBlankSelection="True" Width="100" />
                                                <asp:TextBox ReadOnly="true" ID="ChbTextBox" CssClass="form-control" runat="server" Text='<%# string.Format("{0:N}",Eval("DcfCbFixedFee")) %>' Width="100" />
                                            </td>
                                            <td class="text-nowrap"><span class="badge"><%# Eval("DcfPayTransDays") %></span></td>
                                            <td class="text-nowrap"><span class="badge"><%# Eval("DcfPayInDays") %></span></td>

                                        </tbody>


                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                        </div>
                                    </FooterTemplate>
                                </NP:PagedRepeater>
                            </div>
                        </asp:View>
                        <asp:View ID="ViewIfFeeNotExist" runat="server">
                            <div class="alert alert-info"><b>Info!</b> No record information</div>
                        </asp:View>
                    </asp:MultiView>
                </ContentTemplate>
            </NP:UpdatePanel>
        </asp:View>
        <asp:View ID="ViewIfDebitCompanyNew" runat="server">
            <div class="alert alert-info">
                <asp:Literal ID="NewItemMessage" runat="server" Visible="<%#!IsItTerminalPage%>">Save Debit Company to edit this section.</asp:Literal>
                <asp:Literal ID="Literal1" runat="server" Visible="<%#IsItTerminalPage%>">Save Terminal to edit this section.</asp:Literal>
            </div>
        </asp:View>
    </asp:MultiView>
</admin:FormSections>

<%-- Modal dialog for addin/editing new Debit company fee --%>
<admin:ModalDialog runat="server" ID="dlgEdit" OnCommand="Dialog_Command" Title="Add/Edit Fee">
    <Body>

        <asp:HiddenField runat="server" ID="ModalDialogHiddenFieldFeeId" Value="<%#DebitCompanyFeeItem.DcfID%>" />

        <asp:HiddenField runat="server" ID="ModalDialogHiddenFieldDebitCompanyId" Value="<%#DebitCompanyFeeItem.DcfDebitCompanyId%>" />

        <asp:HiddenField runat="server" ID="ModalDialogHiddenFieldTerminalNumber" Value="<%#DebitCompanyFeeItem.DcfTerminalNumber %>" />




        <h4>
            <asp:Label ID="LabelOne" runat="server" Text="For New Fee" Font-Bold="true" /></h4>
        <div class="row">
            <div class="col-md-4">Payment Method:<netpay:PaymentMethodDropDown Enabled="<%#DebitCompanyFeeItem.DcfID==0%>" CssClass="form-control" runat="server" ID="ddlPaymentMethod" Value='<%# DebitCompanyFeeItem.DcfPaymentMethod %>' EnableBlankSelection="true" PaymentMethodTypes="<%#PaymentMethodTypes%>" BlankSelectionValue="0" BlankSelectionText="[All]" /></div>
            <div class="col-md-4">
                Currency:
       <netpay:CurrencyDropDown ShowSymbol="false" Enabled="<%#DebitCompanyFeeItem.DcfID==0%>" CssClass="form-control" runat="server" ID="ddlCurrncy" Value='<%# DebitCompanyFeeItem.DcfCurrencyId %>' BlankSelectionText="[All]" BlankSelectionValue="255" EnableBlankSelection="True" />
            </div>
        </div>
        <hr />


        <h4>
            <asp:Label ID="LabelTwo" runat="server" Text="Fees" Font-Bold="true" /></h4>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    Currency:   
        <netpay:CurrencyDropDown CssClass="form-control" runat="server" ID="ddlDcfFixedCurrency" Value='<%# DebitCompanyFeeItem.DcfFixedCurrency %>' BlankSelectionText="[Native]" BlankSelectionValue="255" EnableBlankSelection="True" />
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    Trans: 
        <asp:TextBox ID="DcfFixedFeeTxt" CssClass="form-control" runat="server" Text='<%# string.Format("{0:N}",DebitCompanyFeeItem.DcfFixedFee)%>' />
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    Inter (min): 
                <div class="input-group">
                    <asp:TextBox ID="DcfMinPrecFeeTxt" CssClass="form-control" runat="server" Text='<%# string.Format("{0:N}",DebitCompanyFeeItem.DcfMinPrecFee)%>' />
                    <span class="input-group-addon">%</span>
                </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    Sale (max): 
                   
                <div class="input-group">
                    <asp:TextBox ID="DcfMaxPrecFeeTxt" CssClass="form-control" runat="server" Text='<%# string.Format("{0:N}",DebitCompanyFeeItem.DcfMaxPrecFee)%>' />
                    <span class="input-group-addon">%</span>
                </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    Clearing: 

                <div class="input-group">
                    <asp:TextBox ID="DcfPercentFeeTxt" CssClass="form-control" runat="server" Text='<%# string.Format("{0:N}",DebitCompanyFeeItem.DcfPercentFee)%>' />
                    <span class="input-group-addon">%</span>
                </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    Pre-Auth:   
                <div class="input-group">
                    <asp:TextBox ID="DcfApproveFixedFeeTxt" CssClass="form-control" runat="server" Text='<%# string.Format("{0:N}",DebitCompanyFeeItem.DcfApproveFixedFee)%>' />
                </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    Refund:   
                <div class="input-group">
                    <asp:TextBox ID="DcfRefundFixedFeeTxt" CssClass="form-control" runat="server" Text='<%# string.Format("{0:N}",DebitCompanyFeeItem.DcfRefundFixedFee)%>' />
                </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    Copy R:     
                <div class="input-group">
                    <asp:TextBox ID="DcfClarificationFeeTxt" CssClass="form-control" runat="server" Text='<%# string.Format("{0:N}",DebitCompanyFeeItem.DcfClarificationFee)%>' />
                </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    Fail:       
                <div class="input-group">
                    <asp:TextBox ID="DcfFailFixedFeeTxt" CssClass="form-control" runat="server" Text='<%# string.Format("{0:N}",DebitCompanyFeeItem.DcfFailFixedFee)%>' />
                </div>
                </div>
            </div>
        </div>

        <hr />

        <h4>
            <asp:Label ID="LabelThree" runat="server" Text="CHB" Font-Bold="true" /></h4>

        <div class="row">
            <div class="col-md-4">
                <netpay:CurrencyDropDown CssClass="form-control" runat="server" ID="ddlDcfChbCurrency" Value='<%# DebitCompanyFeeItem.DcfChbCurrency==null? 255 : DebitCompanyFeeItem.DcfChbCurrency %>' BlankSelectionText="[Native]" BlankSelectionValue="255" EnableBlankSelection="True" />
            </div>
            <div class="col-md-4">
                <asp:TextBox ID="DcfCbFixedFeeTxt" CssClass="form-control" runat="server" Text='<%# string.Format("{0:N}",DebitCompanyFeeItem.DcfCbFixedFee) %>' />
            </div>
        </div>
        <hr />
        <h4>
            <asp:Label ID="LabelFour" runat="server" Text="Payouts" Font-Bold="true" /></h4>
        <div class="row">
            <div class="col-md-2">
                Transaction Days:
            </div>
            <div class="col-md-4">
                <asp:TextBox ID="DcfPayTransDaysTxt" CssClass="form-control" runat="server" Text='<%# DebitCompanyFeeItem.DcfPayTransDays %>' />
            </div>
        </div>
        <br />
        <div class="row">
            <div class="alert alert-info">
                Enter transaction dates seperated with a comma.<br />
                <br />
                <b>Example:</b> 1,10,20<br />
                split the month into 3 parts 1-10, 11-20, 21-31
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-md-2">
                PayIn Days:
            </div>
            <div class="col-md-4">
                <asp:TextBox ID="DcfPayInDaysTxt" CssClass="form-control" runat="server" Text='<%# DebitCompanyFeeItem.DcfPayInDays %>' />
            </div>
        </div>
        <br />
        <div class="row">
            <div class="alert alert-info">
                Enter pay-in dates seperated with a comma, this field complement the dates and must use with the same month parts.<br />
                <br />
                <b>Example:</b><br />
                Transaction Days: 1,10,20, PayIn Days: 15,25,5.<br />
                means transactions from 1-10 will be payed on the 15th, transactions from 11-20 will be payed on the 25th, transactions from 21-31 will be payed on the 5th
            </div>
        </div>

    </Body>
    <Footer>
        <admin:DataButtons runat="server" EnableSave="true"></admin:DataButtons>
    </Footer>
</admin:ModalDialog>
