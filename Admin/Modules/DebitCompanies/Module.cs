﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;


namespace Netpay.Admin.DebitCompanies
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu ManagmentMenuItem ;

        public override string Name { get { return "DebitCompany"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return ""; } }

        public Module() : base(Bll.DebitCompanies.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu item
            ManagmentMenuItem = new ApplicationMenu("Debit Companies Management", "~/DebitCompanies/0", "fa fa-university", 1);

            //Register route
            Application.RegisterRoute("DebitCompanies/{id}", "DebitCompany", typeof(Controls.AccountPage), module : this);

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Debit Companies", ManagmentMenuItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            ManagmentMenuItem.Parent.RemoveChild(ManagmentMenuItem);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "DebitCompany")
            {
                (page as Controls.AccountPage).AccountChanged += delegate
                {
                    if ((page as Controls.AccountPage).Account != null)
                        (page as Controls.AccountPage).SetStatusColor(Netpay.Infrastructure.Enums.BoolColorMap.MapColor(((page as Controls.AccountPage).Account as Bll.DebitCompanies.DebitCompany).IsActive));
                };
                
                (page as Controls.AccountPage).PageController.LoadItem += PageController_LoadItem;
                (page as Controls.AccountPage).PageController.PostSaveItem += PageController_PostSave;
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/DebitCompanies/Filter.ascx"));
                page.AddControlToList(page.LoadControl("~/Modules/DebitCompanies/List.ascx"));
                page.AddControlToForm("Data", page.LoadControl("~/Modules/DebitCompanies/Data.ascx"),this, "", page.CurrentPageSecuredObject);
                page.AddControlToForm("Reply Codes", page.LoadControl("~/Modules/DebitCompanies/ReplyCodes.ascx"),this, "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject,Bll.DebitCompanies.ResponseCode.SecuredObject).GetActivObject);
                page.AddControlToForm("Notifications", page.LoadControl("~/Modules/DebitCompanies/Notifications.ascx"), this, "", page.CurrentPageSecuredObject);
                page.AddControlToForm("Fees & Payouts", page.LoadControl("~/Modules/DebitCompanies/DebitCompanyFeesAndPayouts.ascx"), this, "", page.CurrentPageSecuredObject);
            }
        }

        private void PageController_LoadItem(object sender, EventArgs e)
        {
            // Create a new root Debit company. Happens when pressing Add 
            var page = (sender as Controls.DataPageController).Page as Controls.DataTablePage;

            page.FormButtons.EnableSave = page.CurrentPageSecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Edit);
            page.FormButtons.EnableDelete = page.CurrentPageSecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Delete);
        }

        private void PageController_PostSave(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.AccountPage;

            if (page != null)
            {
                if (page.Account.AccountType == Bll.Accounts.AccountType.DebitCompany)
                {
                    if (page.Account == null) return;
                    int debitid = (int)page.Account.DebitCompanyID;
                    var DebitComAfterSave = Bll.DebitCompanies.DebitCompany.LoadDebitCompanyById(debitid);

                    page.Account = DebitComAfterSave;
                    page.PageController.DataKey = new System.Web.UI.WebControls.DataKey(new System.Collections.Specialized.OrderedDictionary() { { "ID", DebitComAfterSave.AccountID } });
                    page.PageController.FormView.Update();
                }
            }
        }
    }
}