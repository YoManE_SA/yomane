﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Notifications.ascx.cs" Inherits="Netpay.Admin.Modules.DebitCompanies.Notifications" %>

<admin:FormSections runat="server" Title="AUTOMATIC REFUND NOTIFICATION">
    <asp:MultiView ID="MultiViewOne" runat="server" ActiveViewIndex="<%# (TemplatePage.Page is AccountPage) && TemplatePage.ItemID!=null && TemplatePage.ItemID!=0 ? 0 : 1 %>">
        <asp:View ID="ViewIfNotNewDebitCompany1" runat="server">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <h4>Notify by email :</h4>
                    <asp:CheckBoxList OnDataBound="cblAutoRefundNotifyUsers_DataBound" CssClass="checkbox-list" ID="cblAutoRefundNotifyUsers" RepeatLayout="UnorderedList" runat="server" DataSource="<%#Netpay.Bll.DebitCompanies.DebitCompanyRule.NotifyByMailUsers%>" />
                </div>
            </div>
        </asp:View>
        <asp:View ID="ViewIfNew1" runat="server">
            Save Debit Company to edit this section.
       
        </asp:View>
    </asp:MultiView>

</admin:FormSections>

<admin:FormSections runat="server" Title="CHB ALERT">
    <asp:MultiView ID="MultiViewTwo" runat="server" ActiveViewIndex="<%# (TemplatePage.Page is AccountPage) && TemplatePage.ItemID!=null && TemplatePage.ItemID!=0 ? 0 : 1 %>">
        <asp:View ID="ViewIfNotNewDebitCompany2" runat="server">
            <div class="panel panel-default">
                <div class="panel-heading font-small">
                    <asp:Image runat="server" ImageUrl='<%# "/NPCommon/ImgPaymentMethod/23X12/22.gif" %>' ImageAlign="Middle" />
                    VISA
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                Monthly limit
                    <asp:TextBox ID="txtMonthlyLimitCHB" CssClass="form-control" runat="server" Text="<%#DebitCompanyItem.MonthlyLimitCHB %>" />
                                <asp:RangeValidator ControlToValidate="txtMonthlyLimitCHB" MinimumValue="0" MaximumValue="999999" ErrorMessage="Monthly limit is invalid!" Display="none" SetFocusOnError="true" Type="Integer" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <h4>Notify By Mail</h4>
                            <asp:CheckBoxList OnDataBound="cblMonthlyLimitCHBNotifyUsers_DataBound" ID="cblMonthlyLimitCHBNotifyUsers" CssClass="checkbox-list" RepeatDirection="Vertical" RepeatLayout="UnorderedList" runat="server" DataSource="<%#Netpay.Bll.DebitCompanies.DebitCompanyRule.NotifyByMailUsers%>" />
                        </div>
                        <div class="col-md-6">
                            <h4>Notify By SMS</h4>
                            <asp:CheckBoxList OnDataBound="cblMonthlyLimitCHBNotifyUsersSMS_DataBound" ID="cblMonthlyLimitCHBNotifyUsersSMS" CssClass="checkbox-list" RepeatDirection="Vertical" RepeatLayout="UnorderedList" runat="server" DataSource="<%#Netpay.Bll.DebitCompanies.DebitCompanyRule.NotifyBySmsUsers %>" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading font-small">
                    <asp:Image runat="server" ImageUrl='<%# "/NPCommon/ImgPaymentMethod/23X12/25.gif" %>' ImageAlign="Middle" />
                    Master Card
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                Monthly limit
                                <asp:TextBox ID="txtMonthlyMCLimitCHB" CssClass="form-control" runat="server" Text="<%#DebitCompanyItem.MonthlyMCLimitCHB%>" />
                                <asp:RangeValidator ControlToValidate="txtMonthlyMCLimitCHB" MinimumValue="0" MaximumValue="999999" ErrorMessage="MonthlyMC limit is invalid!" Display="none" SetFocusOnError="true" Type="Integer" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <h4>Notify By Mail</h4>
                            <asp:CheckBoxList OnDataBound="cblMonthlyMCLimitCHBNotifyUsers_DataBound" ID="cblMonthlyMCLimitCHBNotifyUsers" CssClass="checkbox-list" RepeatDirection="Vertical" RepeatLayout="UnorderedList" runat="server" DataSource="<%#Netpay.Bll.DebitCompanies.DebitCompanyRule.NotifyByMailUsers%>" />
                        </div>
                        <div class="col-md-6">
                            <h4>Notify By SMS</h4>
                            <asp:CheckBoxList OnDataBound="cblMonthlyMCLimitCHBNotifyUsersSMS_DataBound" ID="cblMonthlyMCLimitCHBNotifyUsersSMS" CssClass="checkbox-list" RepeatDirection="Vertical" RepeatLayout="UnorderedList" runat="server" DataSource="<%#Netpay.Bll.DebitCompanies.DebitCompanyRule.NotifyBySmsUsers%>" />
                        </div>
                    </div>

                </div>
            </div>
        </asp:View>
        <asp:View ID="ViewIfNew2" runat="server">
            Save Debit Company to edit this section.
       
        </asp:View>
    </asp:MultiView>
</admin:FormSections>

<admin:FormSections ID="NotificationFormSection" runat="server" Title="NOTIFICATIONS" EnableAdd="<%# (TemplatePage.Page is AccountPage) && TemplatePage.ItemID!=null && TemplatePage.ItemID!=0 %>" OnCommand="Dialog_Command">
    <asp:MultiView ID="ViewIfNotNewDebitCompany3" runat="server" ActiveViewIndex="<%# (TemplatePage.Page is AccountPage) && TemplatePage.ItemID!=null && TemplatePage.ItemID!=0 ? 0 : 1 %>">
        <asp:View ID="View1" runat="server">
            <NP:UpdatePanel runat="server" ID="upRuleList" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:MultiView ID="NotificationMultiView" runat="server" ActiveViewIndex="<%# (DebitCompanyRulesList.Count>0) ? 0 : 1 %>">
                        <asp:View ID="ViewIfRuleExist" runat="server">

                            <NP:PagedRepeater runat="server" ID="rptDebitRuleList" PageSize="20">
                                <HeaderTemplate>
                                    <asp:HiddenField ID="hfDebitCompanyIdFormSection" runat="server" Value='<%# Eval("DebitCompanyId") %>' />
                                    <asp:PlaceHolder Visible="false" runat="server">
                                        <div>
                                            <asp:Literal runat="server" ID="ltId" Text='<%# Eval("ID") %>' />
                                        </div>
                                    </asp:PlaceHolder>
                                    <div class="table-responsive">
                                       <table class="table table-hover table-customize">
                                            <thead>
                                                <th class="text-nowrap">Actions</th>
                                               <%-- <th class="text-nowrap">Arrows</th>--%>
                                                <th class="text-nowrap">Rule ID</th>
                                                <th class="text-nowrap">Rule Rating</th>
                                                <th class="text-nowrap">Reply Codes</th>
                                                <th class="text-nowrap">Fails</th>
                                                <th class="text-nowrap">Attempts</th>
                                                <th class="text-nowrap">Notify users mail</th>
                                                <th class="text-nowrap">Notify users sms</th>
                                                <th class="text-nowrap">Is Auto Disable:</th>
                                                <th class="text-nowrap">Is Auto Enable:</th>
                                                <th class="text-nowrap">Minutes:</th>
                                                <th class="text-nowrap">Attempts:</th>
                                                <th class="text-nowrap">Active:</th>
                                            </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tbody>
                                        <td class="text-nowrap">
                                            <NP:Button BackColor="Red" BorderColor="Red" runat="server" Text="Delete" CssClass="btn btn-xs btn-success text-uppercase" RegisterAsync="true" CommandName="DeleteItem" OnCommand="Dialog_Command" CommandArgument='<%#Eval("ID")%>' />
                                            <NP:Button runat="server" Text="Edit" CssClass="btn btn-xs btn-success text-uppercase" RegisterAsync="true" CommandName="EditItem" OnCommand="Dialog_Command" CommandArgument='<%#Eval("ID")%>' />
                                        </td>
                                        
                                        <%--<td><%# Eval("ID") %></td>--%>
                                        <td><%# Eval("Rating") %></td>

                                        <td><span class="badge"><%# Eval("ReplyCodes")%></td>
                                        <td><span class="badge"><%# Eval("FailCount")%></span></td>
                                        <td><span class="badge"><%# Eval("AttemptCount")%></span></td>
                                        <td><span class="badge"><%# Eval("NotifyUsers") %></span></td>
                                        <td><span class="badge"><%# Eval("NotifyUsersSMS") %></span></td>
                                        <td><span class="badge"><%# Eval("IsAutoDisable") %></span></td>
                                        <td><span class="badge"><%# Eval("IsAutoEnable") %></span></td>
                                        <td><span class="badge"><%# Eval("AutoEnableMinutes") %></span></td>
                                        <td><span class="badge"><%# Eval("AutoEnableAttempts") %></span></td>
                                        <td><span class="badge"><%# Eval("IsActive") %></span></td>
                                        <td></td>
                                    </tbody>
                                </ItemTemplate>
                                <FooterTemplate>
                                      </table>
                                    </div>
                                </FooterTemplate>
                            </NP:PagedRepeater>

                        </asp:View>
                        <asp:View ID="ViewIfRuleNotExist" runat="server">
                            <p class="text-center">No rules found.</p>
                        </asp:View>
                    </asp:MultiView>
                </ContentTemplate>
            </NP:UpdatePanel>
        </asp:View>
        <asp:View ID="ViewIfNew3" runat="server">
            Save Debit Company to edit this section.
        </asp:View>
    </asp:MultiView>
</admin:FormSections>

<%-- Modal Dialog for adding/editing new rule --%>
<admin:ModalDialog runat="server" ID="dlgEdit" OnCommand="Dialog_Command" Title="Add/Edit Rule">
    <Body>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    Rule ID:
                       <asp:HiddenField ID="hfDebitCompanyId" runat="server" Value="<%#DebitCompanyRuleItem.DebitCompanyId%>" />
                    <asp:TextBox CssClass="form-control" ReadOnly="true" ID="RuleId" runat="server" Text='<%#DebitCompanyRuleItem.ID%>' />
                </div>
                <div class="form-group">
                    Reply Codes:
                   
                <asp:TextBox ID="ReplyCodesTxt" CssClass="form-control" runat="server" Text="<%#DebitCompanyRuleItem.ReplyCodes %>" />
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    Fails:
                        <asp:TextBox CssClass="form-control" ID="FailCountTxt" runat="server" Text="<%# DebitCompanyRuleItem.FailCount %>" />
                </div>
                <div class="form-group">
                    Attempts:
                         
                <asp:TextBox ID="AttemptsCountTxt" CssClass="form-control" runat="server" Text="<%# DebitCompanyRuleItem.AttemptCount %>" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h4>Notify users by mail:</h4>
                <asp:CheckBoxList OnDataBound="ModalDialogNotifyEmailUsers_DataBound" ID="MdNotifyUsersByEmailCheckBoxList" CssClass="checkbox-list" RepeatLayout="OrderedList" runat="server" DataSource="<%# Netpay.Bll.DebitCompanies.DebitCompanyRule.NotifyByMailUsers %>" />
            </div>
            <div class="col-md-6">
                <h4>Notify users by sms:</h4>
                <asp:CheckBoxList OnDataBound="ModalDialogNotifySmsUsers_DataBound" ID="MdNotifyUsersBySmsCheckBoxList" CssClass="checkbox-list" runat="server" RepeatLayout="OrderedList" DataSource="<%# Netpay.Bll.DebitCompanies.DebitCompanyRule.NotifyBySmsUsers %>" />

            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <asp:CheckBox ID="Unblockchb" runat="server" Checked="<%#DebitCompanyRuleItem.IsAutoEnable %>" Text="Unblock" />
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <asp:CheckBox ID="Blockchb" runat="server" Checked="<%#DebitCompanyRuleItem.IsAutoDisable %>" Text="Block" />
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <asp:CheckBox ID="IsActivechb" runat="server" Checked="<%#DebitCompanyRuleItem.IsActive %>" Text="Active" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">

                <div class="form-group">
                    Unblock after minutes:
                    <asp:TextBox ID="UnblockAfterMinutesTxt" CssClass="form-control" runat="server" Text="<%#DebitCompanyRuleItem.AutoEnableMinutes %>" />
                </div>
                <div class="form-group">
                    Last Unblock:  
                    <asp:TextBox ID="LastUnblockDateTxt" runat="server" CssClass="form-control" Text='<%#DebitCompanyRuleItem.LastUnblockDate.Date.Year==1900||DebitCompanyRuleItem.LastUnblockDate.Date.Year==0001?"Never Occured":DebitCompanyRuleItem.LastUnblockDate.ToLongDateString()%>' />
                </div>
            </div>
            <div class="col-md-6">

                <div class="form-group">
                    Unblock after attempts:
                <asp:TextBox ID="UnblockAfterAttemptsTxt" CssClass="form-control" runat="server" Text="<%#DebitCompanyRuleItem.AutoEnableAttempts %>" />
                </div>
                <div class="form-group">
                    Last Fail:
                   
                <asp:TextBox ID="LastFailTxt" runat="server" CssClass="form-control" Text='<%#DebitCompanyRuleItem.LastFailDate.Date.Year==1900||DebitCompanyRuleItem.LastFailDate.Date.Year==0001?"Never Occured":DebitCompanyRuleItem.LastFailDate.ToLongDateString()%>' />
                </div>
            </div>
        </div>


    </Body>
    <Footer>
        <admin:DataButtons runat="server" EnableSave="true" EnableDelete="false" />
    </Footer>
</admin:ModalDialog>
