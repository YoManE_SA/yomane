﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;
using Netpay.Bll.Accounts;

namespace Netpay.Admin.Modules.DebitCompanies.Terminals
{
    public partial class TerminalNotifications : Controls.TemplateControlBase
    {
        public Netpay.Bll.DebitCompanies.Terminal ItemData { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += PageController_SaveItem;
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            ItemData = TemplatePage.GetItemData<Bll.DebitCompanies.Terminal>();
            if (ItemData == null) ItemData = new Bll.DebitCompanies.Terminal(0);
            base.DataBindChildren();
        }

        protected void PageController_SaveItem(object sender, EventArgs e)
        {
            ItemData = TemplatePage.GetItemData<Bll.DebitCompanies.Terminal>();
            if (ItemData == null) return;

            //Visa Row Save
            ItemData.MonthlyLimitCHB = txtMonthlyLimitCHB.Text.ToInt32();
            ItemData.MonthlyMCLimitCHB = txtMonthlyMCLimitCHB.Text.ToInt32();

            //Visa notify users save
            List<string> SIcblMonthlyLimitCHBNotifyUsers = new List<string>();
            foreach (ListItem item in cblMonthlyLimitCHBNotifyUsers.Items)
            {
                if (item.Selected) SIcblMonthlyLimitCHBNotifyUsers.Add(item.Text);
            }
            if (SIcblMonthlyLimitCHBNotifyUsers.Count == 0) ItemData.MonthlyLimitCHBNotifyUsers = "";
            else
            {
                string result = String.Join(";", SIcblMonthlyLimitCHBNotifyUsers.ToArray());
                ItemData.MonthlyLimitCHBNotifyUsers = result;
            }

            //Visa notify SMS save
            List<string> SIcblMonthlyLimitCHBNotifyUsersbySMS = new List<string>();
            foreach (ListItem item in cblMonthlyLimitCHBNotifyUsersSMS.Items)
            {
                if (item.Selected) SIcblMonthlyLimitCHBNotifyUsersbySMS.Add(item.Text);
            }
            if (SIcblMonthlyLimitCHBNotifyUsersbySMS.Count == 0) ItemData.MonthlyLimitCHBNotifyUsersSMS = "";
            else
            {
                string result = String.Join(";", SIcblMonthlyLimitCHBNotifyUsersbySMS.ToArray());
                ItemData.MonthlyLimitCHBNotifyUsersSMS = result;
            }

            //MC users notify
            List<string> SIcblMonthlyMCLimitCHBNotifyUsers = new List<string>();
            foreach (ListItem item in cblMonthlyMCLimitCHBNotifyUsers.Items)
            {
                if (item.Selected) SIcblMonthlyMCLimitCHBNotifyUsers.Add(item.Text);
            }
            if (SIcblMonthlyMCLimitCHBNotifyUsers.Count == 0) ItemData.MonthlyMCLimitCHBNotifyUsers = "";
            else
            {
                string result = String.Join(";", SIcblMonthlyMCLimitCHBNotifyUsers.ToArray());
                ItemData.MonthlyMCLimitCHBNotifyUsers = result;
            }

            //MC users notify SMS
            List<string> SIcblMonthlyMCLimitCHBNotifyUsersSMS = new List<string>();
            foreach (ListItem item in cblMonthlyLimitCHBNotifyUsersSMS.Items)
            {
                if (item.Selected) SIcblMonthlyMCLimitCHBNotifyUsersSMS.Add(item.Text);
            }
            if (SIcblMonthlyMCLimitCHBNotifyUsersSMS.Count == 0) ItemData.MonthlyMCLimitCHBNotifyUsersSMS = "";
            else
            {
                string result = String.Join(";", SIcblMonthlyMCLimitCHBNotifyUsersSMS.ToArray());
                ItemData.MonthlyMCLimitCHBNotifyUsersSMS = result;
            }

            ItemData.Save();
        }

        protected void cblMonthlyLimitCHBNotifyUsers_DataBound1(object sender, EventArgs e)
        {
            string UsersList;
            if (ItemData != null)
            {
                if (ItemData.MonthlyLimitCHBNotifyUsers != "" && ItemData.MonthlyLimitCHBNotifyUsers != null)
                {
                    UsersList = ItemData.MonthlyLimitCHBNotifyUsers;
                    foreach (ListItem name in cblMonthlyLimitCHBNotifyUsers.Items)
                    {
                        if (UsersList.Contains(name.Text)) name.Selected = true;
                    }
                }
            }
        }

        protected void cblMonthlyLimitCHBNotifyUsersSMS_DataBound1(object sender, EventArgs e)
        {
            string UsersList;
            if (ItemData != null)
            {
                if (ItemData.MonthlyLimitCHBNotifyUsersSMS != "" && ItemData.MonthlyLimitCHBNotifyUsersSMS != null)
                {
                    UsersList = ItemData.MonthlyLimitCHBNotifyUsersSMS;
                    foreach (ListItem name in cblMonthlyLimitCHBNotifyUsersSMS.Items)
                    {
                        if (UsersList.Contains(name.Text)) name.Selected = true;
                    }
                }
            }
        }

        protected void cblMonthlyMCLimitCHBNotifyUsers_DataBound(object sender, EventArgs e)
        {
            string UsersList;
            if (ItemData != null)
            {
                if (ItemData.MonthlyMCLimitCHBNotifyUsers != "" && ItemData.MonthlyMCLimitCHBNotifyUsers != null)
                {
                    UsersList = ItemData.MonthlyMCLimitCHBNotifyUsers;
                    foreach (ListItem name in cblMonthlyMCLimitCHBNotifyUsers.Items)
                    {
                        if (UsersList.Contains(name.Text)) name.Selected = true;
                    }
                }
            }
        }

        protected void cblMonthlyMCLimitCHBNotifyUsersSMS_DataBound(object sender, EventArgs e)
        {
            string UsersList;
            if (ItemData != null)
            {
                if (ItemData.MonthlyMCLimitCHBNotifyUsersSMS != "" && ItemData.MonthlyMCLimitCHBNotifyUsersSMS != null)
                {
                    UsersList = ItemData.MonthlyMCLimitCHBNotifyUsersSMS;
                    foreach (ListItem name in cblMonthlyMCLimitCHBNotifyUsersSMS.Items)
                    {
                        if (UsersList.Contains(name.Text)) name.Selected = true;
                    }
                }
            }
        }

        protected void ResetMonthlyChb_Click(object sender, CommandEventArgs e)
        {
            int id;
            if (int.TryParse(e.CommandArgument.ToString(), out id))
            {
                ItemData = Bll.DebitCompanies.Terminal.Load(id);
                ItemData.MonthlyCHBWasSent = false;
                ItemData.Save();
                TemplatePage.SetItemData(ItemData);
                upAlertStatus.DataBind();
                upAlertStatus.Update();
            }
        }

        protected void ResetMonthlyMcChb_Click(object sender, CommandEventArgs e)
        {
            int id;
            if (int.TryParse(e.CommandArgument.ToString(), out id))
            {
                ItemData = Bll.DebitCompanies.Terminal.Load(id);
                ItemData.MonthlyMCCHBWasSent = false;
                ItemData.Save();
                TemplatePage.SetItemData(ItemData);
                upAlertStatus.DataBind();
                upAlertStatus.Update();
            }
        }
    }
}