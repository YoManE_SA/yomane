﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CardCompanyDetails.ascx.cs" Inherits="Netpay.Admin.Modules.DebitCompanies.Terminals.CardCompanyDetails" %>
<admin:FormSections runat="server" Title="Card Company Details">

    <!-- Visa -->
    <div class="panel panel-default">
        <div class="panel-heading font-small" style="background-color: #fcfcfc;">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <asp:Image runat="server" ImageUrl='<%# "/NPCommon/ImgPaymentMethod/23X12/22.gif" %>' ImageAlign="Middle" />
                    Visa
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        Supplier Num.           
                        <asp:TextBox CssClass="form-control" ID="txtCompanyNum_visa" runat="server" Text="<%#ItemData.dt_CompanyNum_visa %>" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        Comment
                        <asp:TextBox ID="txtComments_visa" CssClass="form-control" TextMode="MultiLine" runat="server" Text="<%#ItemData.dt_Comments_visa %>" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MasterCard -->
    <div class="panel panel-default">
        <div class="panel-heading font-small" style="background-color: #fcfcfc;">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <asp:Image runat="server" ImageUrl='<%# "/NPCommon/ImgPaymentMethod/23X12/25.gif" %>' ImageAlign="Middle" />
                    MasterCard
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        Supplier Num.           
                        <asp:TextBox ID="txtCompanyNum_mastercard" CssClass="form-control" runat="server" Text="<%#ItemData.dt_CompanyNum_mastercard %>" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        Comment
                        <asp:TextBox ID="txtComments_mastercard" CssClass="form-control" TextMode="MultiLine" runat="server" Text="<%#ItemData.dt_Comments_mastercard %>" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Diners-->
    <div class="panel panel-default">
        <div class="panel-heading font-small" style="background-color: #fcfcfc;">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <asp:Image runat="server" ImageUrl='<%# "/NPCommon/ImgPaymentMethod/23X12/23.gif" %>' ImageAlign="Middle" />
                    Diners
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        Supplier Num.          
                         <asp:TextBox ID="txtCompanyNum_diners" CssClass="form-control" runat="server" Text="<%#ItemData.dt_CompanyNum_diners %>" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        Comment
                        <asp:TextBox ID="txtComments_diners" CssClass="form-control" TextMode="MultiLine" runat="server" Text="<%#ItemData.dt_Comments_diners %>" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Amex -->
    <div class="panel panel-default">
        <div class="panel-heading font-small" style="background-color: #fcfcfc;">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <asp:Image runat="server" ImageUrl='<%# "/NPCommon/ImgPaymentMethod/23X12/24.gif" %>' ImageAlign="Middle" />
                    Amex
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        Supplier Num.          
                         <asp:TextBox ID="txtCompanyNum_americanexp" CssClass="form-control" runat="server" Text="<%#ItemData.dt_CompanyNum_americanexp %>" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        Comment
                           <asp:TextBox ID="txtComments_americanexp" CssClass="form-control" TextMode="MultiLine" runat="server" Text="<%#ItemData.dt_Comments_americanexp %>" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- IsraCard -->
    <div class="panel panel-default">
        <div class="panel-heading font-small" style="background-color: #fcfcfc;">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <asp:Image runat="server" ImageUrl='<%# "/NPCommon/ImgPaymentMethod/23X12/21.gif" %>' ImageAlign="Middle" />
                    IsraCard
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        Supplier Num.          
                        <asp:TextBox ID="txtCompanyNum_isracard" CssClass="form-control" runat="server" Text="<%#ItemData.dt_CompanyNum_isracard %>" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        Comment
                        <asp:TextBox ID="txtComments_isracard" CssClass="form-control" TextMode="MultiLine" runat="server" Text="<%#ItemData.dt_Comments_isracard %>" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Direct -->
    <div class="panel panel-default">
        <div class="panel-heading font-small" style="background-color: #fcfcfc;">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <asp:Image runat="server" ImageUrl='<%# "/NPCommon/ImgPaymentMethod/23X12/26.gif" %>' ImageAlign="Middle" />
                    Direct
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        Supplier Num.          
                        <asp:TextBox ID="txtCompanyNum_direct" CssClass="form-control" runat="server" Text="<%#ItemData.dt_CompanyNum_direct %>" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        Comment
                        <asp:TextBox ID="txtComments_direct" CssClass="form-control" TextMode="MultiLine" runat="server" Text="<%#ItemData.dt_Comments_direct %>" />
                    </div>
                </div>
            </div>
        </div>
    </div>

</admin:FormSections>
