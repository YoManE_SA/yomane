﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;

/*
	litTerminalName.Text = dbPages.ExecScalar("SELECT dc_Name+' | '+TerminalNumber+' | '+dt_Name FROM tblDebitCompany" & _
" INNER JOIN tblDebitTerminals ON DebitCompany_ID=DebitCompany WHERE ID=" & nTerminal)
tlbTop.AddItem("Terminal Details", "system_terminalTermDetail.aspx?terminalID=" & nTerminal)
tlbTop.AddItem("Card Company Details", "system_terminalCardDetail.aspx?terminalID=" & nTerminal)
tlbTop.AddItem("Fees & Payouts", "system_terminalFees.aspx?terminalID=" & nTerminal)
tlbTop.AddItem("Notifications", "system_terminalRules.aspx?TerminalID=" & nTerminal, True)
tlbTop.AddItem("Miscellaneous", "system_terminalMisc.aspx?terminalID=" & nTerminal)
*/

namespace Netpay.Admin.DebitCompanies.Terminals
{
    public partial class Data : Controls.TemplateControlBase
    {
        
        protected Netpay.Bll.DebitCompanies.Terminal ItemData;

        public List<string> SearchTagsList
        {
            get
            {
                return Bll.DebitCompanies.Terminal.SearchTagsList;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += PageController_SaveItem;
            TemplatePage.PageController.DeleteItem += PageController_DeleteItem;
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            ItemData = TemplatePage.GetItemData<Bll.DebitCompanies.Terminal>();
            if (ItemData == null) ItemData = new Bll.DebitCompanies.Terminal(0);

            if (!string.IsNullOrEmpty(ItemData.SearchTag))
                ddlSearchTags.Value = ItemData.SearchTag;

            base.DataBindChildren();
        }

        protected void PageController_SaveItem(object sender, EventArgs e)
        {
            ItemData = TemplatePage.GetItemData<Bll.DebitCompanies.Terminal>();
            if (ItemData == null) return;
            
            //Check if it's a new terminal , if so , check if the terminal number already exist in 
            //The database , if the terminal number exist , save will not be invoked.
            if (ItemData.ID==0)
            {
                //Check if the terminal number is empty
                if (string.IsNullOrEmpty(txtTerminalNumber.Text.Trim()))
                {                   
                    //Stop execution of the "TemplatePage.PageController.SaveItem" Event Handler.
                    throw new Exception("Terminal number can't be empty.");
                }

                List<string> TerminalNumberList = (from dt in DataContext.Reader.tblDebitTerminals
                                                   select dt.terminalNumber)
                                                   .Distinct()
                                                   .ToList();

                if (TerminalNumberList.Contains(txtTerminalNumber.Text.Trim()))
                {
                    TerminalNumberNotExistNotify.SetMessage("Terminal number already exist, change it in order to save terminal", true);
                    dlgTerminalNumberCheck.RegisterShow();

                    //Stop execution of the "TemplatePage.PageController.SaveItem" Event Handler.
                    throw new Exception("Terminal number exist");
                }
                else
                {
                    ItemData.TerminalNumber = txtTerminalNumber.Text.Trim();
                }
            }
            else //The terminal is not new terminal , check if the terminal number exist with other id
                 //If so , update all fields except the terminal number.
            {
                if (ItemData.TerminalNumber != txtTerminalNumber.Text.Trim())
                {
                    var TerminalNumberList = (from dt in DataContext.Reader.tblDebitTerminals
                                              select new { dt.id, dt.terminalNumber })
                                                       .Distinct()
                                                       .ToList();

                if   (TerminalNumberList.Exists(x=>x.terminalNumber== txtTerminalNumber.Text.Trim() && x.id != ItemData.ID))
                    {
                        TerminalNumberNotExistNotify.SetMessage("Terminal number is already used, Update was made without it", false);
                        dlgTerminalNumberCheck.RegisterShow();
                    }
                else //The terminal number doesn't exist with other id so save its value.
                    {
                        ItemData.TerminalNumber = txtTerminalNumber.Text.Trim();
                    }
                }
            }


            ItemData.Name = txtName.Text.Trim();
            ItemData.TerminalNumber3D = txtTerminalNumber3D.Text.Trim();
            ItemData.Mcc = txtMcc.Text.Trim();
            ItemData.Descriptor = txtDescriptor.Text.Trim();
            ItemData.BankExternalID = txtBankExternalID.Text.Trim();
            ItemData.TerminalMonthlyCost = txtTerminalMonthlyCost.Text.Trim().ToDecimal(0);
            ItemData.BankAccountID = dt_BankAccountID.SelectedValue.ToInt32();
            ItemData.ProcessingMethod = byte.Parse(ddlprocessingMethod.SelectedValue);
            ItemData.SMSShortCode = txtSMSShortCode.Text;
            ItemData.ContractNumber = txtContractNumber.Text.Trim();
            ItemData.TerminalNotes = txtTerminalNotes.Text.Trim();
            ItemData.IsNetpayTerminal = chkIsNetpayTerminal.Checked;
            ItemData.IsShvaMasterTerminal = chkIsShvaMasterTerminal.Checked;
            ItemData.IsManipulateAmount = chkIsManipulateAmount.Checked;
            ItemData.IsUseBlackList = chkIsUseBlackList.Checked;
            ItemData.IsRefundBlocked = chkIsRefundBlocked.Checked;
            ItemData.EnableRecurringBank = chkEnableRecurringBank.Checked;
            ItemData.EnableAuthorization = chkEnableAuthorization.Checked;
            ItemData.Enable3dsecure = chkEnable3dsecure.Checked;
            ItemData.IsPersonalNumberRequired = chkIsPersonalNumberRequired.Checked;
            ItemData.IsTestTerminal = chkIsTestTerminal.Checked;
            ItemData.SearchTag = txtSearchTag.Text.Trim();

            ItemData.Save();
            TemplatePage.SetItemData<Bll.DebitCompanies.Terminal>(ItemData);

            //The 2 fields below moved to Summary tab.
            //ItemData.DebitCompany = (byte)ddlDebitCompany.Value.ToNullableInt().GetValueOrDefault();
            //ItemData.IsActive = chkIsActive.Checked;

            //The 3 fields below are not in use for now.
            //ItemData.MobileCat = ddlMobileCat.SelectedValue.ToInt32();
            //ItemData.ManagingCompany = txtManagingCompany.Text;
            //ItemData.ManagingPSP = txtManagingPSP.Text;
        }

        protected void PageController_DeleteItem(object sender , EventArgs e)
        {
            if (TemplatePage.ItemID!= null)
            ItemData = Bll.DebitCompanies.Terminal.Load((int)TemplatePage.ItemID);
            if (ItemData!=null)
            ItemData.Delete();
        }

        protected void ddlprocessingMethod_DataBound(object sender, EventArgs e)
        {
            if (ItemData.ProcessingMethod != 0 && ItemData.ID != 0)
                ddlprocessingMethod.SelectedValue = ItemData.ProcessingMethod.ToString();
        }
                
        protected void ddlSearchTags_SelectedIndexChanged(object sender, EventArgs e)
        {
            //txtSearchTag.Text = ddlSearchTags.SelectedValue;
            SearchTagUpdatePanel.DataBind();
            SearchTagUpdatePanel.Update();
        }

        protected void txtTerminalNumber_TextChanged(object sender, EventArgs e)
        {
            TemplatePage.PageController.FormView.Update();
        }

        //protected void ddlMobileCat_DataBound(object sender, EventArgs e)
        //{
        //    if (ItemData.MobileCat != 0 && ItemData.ID != 0)
        //        ddlMobileCat.SelectedValue = ItemData.MobileCat.ToString();
        //}
    }
}