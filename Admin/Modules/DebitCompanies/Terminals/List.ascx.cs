﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.DebitCompanies.Terminals
{
	public partial class List : Controls.TemplateControlBase
	{
		protected override void OnLoad(EventArgs e)
		{
			rptList.DataBinding += List_DataBinding;
			base.OnLoad(e);
		}

		private void List_DataBinding(object sender, EventArgs e)
		{
			rptList.DataSource = Netpay.Bll.DebitCompanies.Terminal.Search(TemplatePage.GetFilter<Bll.DebitCompanies.Terminal.SearchFilters>(), rptList);
		}

        protected string GetDebitCompanyName()
        {
            var debitCompany = Bll.DebitCompanies.DebitCompany.LoadDebitCompanyById((byte)Eval("DebitCompany"));
            if (debitCompany != null) return debitCompany.Name;
            return "";
        }
	}
}