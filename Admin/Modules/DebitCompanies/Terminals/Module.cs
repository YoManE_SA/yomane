﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Admin.Modules.DebitCompanies.Terminals
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu terminalMenuItem;
        public override string Name { get { return "DebitTerminals"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return ""; } }

        public Module() : base(Bll.DebitCompanies.Terminals.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu
            terminalMenuItem = new ApplicationMenu("Terminals", "~/DebitCompanies/Terminals/0",null, 30);

            //Register route
            Application.RegisterRoute("DebitCompanies/Terminals/{id}", "Terminal", typeof(Controls.DataTablePage), module:this);

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {               
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {            
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Debit Companies", terminalMenuItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            terminalMenuItem.Parent.RemoveChild(terminalMenuItem);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "Terminal")
            {
                page.PageController.LoadItem += PageController_LoadItem;
                page.PageController.NewItem += PageController_NewTerminalItem;
                page.PageController.PostSaveItem += PageController_PostSaveItem;
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/DebitCompanies/Terminals/Filter.ascx"));
                page.AddControlToList(page.LoadControl("~/Modules/DebitCompanies/Terminals/List.ascx"));
                page.AddControlToForm("Summary", page.LoadControl("~/Modules/DebitCompanies/Terminals/Summary.ascx"), this, "", Bll.DebitCompanies.Terminal.SecuredObject);
                page.AddControlToForm("Data", page.LoadControl("~/Modules/DebitCompanies/Terminals/Data.ascx"), this, "", Bll.DebitCompanies.Terminal.SecuredObject);
                page.AddControlToForm("Connection", page.LoadControl("~/Modules/DebitCompanies/Terminals/ConnectionInfo.ascx"), this, "", Bll.DebitCompanies.Terminal.SecuredObject);
                page.AddControlToForm("Card Company Details", page.LoadControl("~/Modules/DebitCompanies/Terminals/CardCompanyDetails.ascx"), this, "", Bll.DebitCompanies.Terminal.SecuredObject);
                page.AddControlToForm("Fees & Payouts", page.LoadControl("~/Modules/DebitCompanies/DebitCompanyFeesAndPayouts.ascx"), this, "", Bll.DebitCompanies.Terminal.SecuredObject);
                page.AddControlToForm("Notifications", page.LoadControl("~/Modules/DebitCompanies/Terminals/TerminalNotifications.ascx"), this, "", Bll.DebitCompanies.Terminal.SecuredObject);
            }
        }

        private void PageController_PostSaveItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            var item = page.GetItemData<Bll.DebitCompanies.Terminal>();
            if (item == null) return;

            page.PageController.DataKey = new System.Web.UI.WebControls.DataKey(new System.Collections.Specialized.OrderedDictionary() { { "ID", item.ID } });
        }

        private void PageController_LoadItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            page.ItemID = page.PageController.DataKey["ID"].ToNullableInt();
            if (page.ItemID != null)
            {
                page.SetItemData(Bll.DebitCompanies.Terminal.Load(page.ItemID.GetValueOrDefault()));
            }
            if (page.GetItemData<Bll.DebitCompanies.Terminal>() == null) page.SetItemData(new Bll.DebitCompanies.Terminal(0));

            page.FormButtons.EnableSave = Bll.DebitCompanies.Terminal.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Edit);
            page.FormButtons.EnableDelete = Bll.DebitCompanies.Terminal.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Delete);
        }

        private void PageController_NewTerminalItem(object sender, EventArgs e)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.DebitCompanies.Terminal.SecuredObject, PermissionValue.Add);

            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            page.SetItemData(new Bll.DebitCompanies.Terminal(0));

            page.FormButtons.EnableDelete = Bll.DebitCompanies.Terminal.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Delete);
        }
    }
}