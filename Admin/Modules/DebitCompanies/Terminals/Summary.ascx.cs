﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;

namespace Netpay.Admin.Modules.DebitCompanies.Terminals
{
    public partial class Summary : Controls.TemplateControlBase
    {
        protected Netpay.Bll.DebitCompanies.Terminal ItemData;

        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += PageController_SaveItem;
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            ItemData = TemplatePage.GetItemData<Bll.DebitCompanies.Terminal>();
            if (ItemData == null) ItemData = new Bll.DebitCompanies.Terminal(0);
            base.DataBindChildren();
        }

        protected void PageController_SaveItem(object sender, EventArgs e)
        {
            if (ddlDebitCompany.Value == "0") throw new Exception("Please select debit company before you save a new terminal.");

            ItemData = TemplatePage.GetItemData<Bll.DebitCompanies.Terminal>();
            if (ItemData == null) return;

            ItemData.IsActive = chkIsActive.Checked;

            if (ItemData.ID == 0)
            {
                ItemData.DebitCompany = (byte)ddlDebitCompany.Value.ToNullableInt().GetValueOrDefault();
            }

            // Save is not invoked yet because there's a need to check if it's a new 
            // Terminal and check if the terminal number exist with other id in database.
            //ItemData.Save();
            //TemplatePage.SetItemData<Bll.DebitCompanies.Terminal>(ItemData);
        }

        protected int PassCount()
        {
            ItemData= TemplatePage.GetItemData<Bll.DebitCompanies.Terminal>();
            int num;
            if (ItemData != null && ItemData.ID != 0)
            {
                num = (from ctp in DataContext.Reader.tblCompanyTransPasses
                           where ctp.TerminalNumber == ItemData.TerminalNumber
                           select ctp).Count();
                return num;
            }
            return 0;
            
        }

        protected int FailCount()
        {
            ItemData = TemplatePage.GetItemData<Bll.DebitCompanies.Terminal>();
            if (ItemData != null && ItemData.ID != 0)
            {
                int num = (from ctf in DataContext.Reader.tblCompanyTransFails
                           where ctf.TerminalNumber == ItemData.TerminalNumber
                           select ctf).Count();
                return num;
            }
            return 0;
        }

        protected int PreAuthCount()
        {
            ItemData = TemplatePage.GetItemData<Bll.DebitCompanies.Terminal>();
            if (ItemData != null && ItemData.ID != 0)
            {
                int num = (from cpa in DataContext.Reader.tblCompanyTransApprovals
                           where cpa.TerminalNumber == ItemData.TerminalNumber
                           select cpa).Count();
                return num;
            }
            return 0;
        }

        protected int DelCount()
        {
            ItemData = TemplatePage.GetItemData<Bll.DebitCompanies.Terminal>();
            if (ItemData != null && ItemData.ID != 0)
            {
                int num = (from ctp in DataContext.Reader.tblCompanyTransRemoveds
                           where ctp.TerminalNumber == ItemData.TerminalNumber
                           select ctp).Count();
                return num;
            }
            return 0;
        }

        protected List<KeyValuePair<string,int>> GetMerchantList()
        {
            ItemData = TemplatePage.GetItemData<Bll.DebitCompanies.Terminal>();

            if (ItemData!=null && ItemData.ID != 0)
            {
                //"SELECT tblCompany.ID, tblCompany.CompanyName FROM tblCompanyCreditFeesTerminals 
                //INNER JOIN tblCompany ON tblCompanyCreditFeesTerminals.CCFT_CompanyID = tblCompany.ID 
                //WHERE (tblCompanyCreditFeesTerminals.CCFT_Terminal = '" & sTerminalNumber & "') 
                //GROUP BY tblCompany.ID, tblCompany.CompanyName"
                var list = (from ccft in DataContext.Reader.tblCompanyCreditFeesTerminals
                            join tc in DataContext.Reader.tblCompanies
                            on ccft.CCFT_CompanyID equals tc.ID
                            where ccft.CCFT_Terminal == ItemData.TerminalNumber
                            select new KeyValuePair<string, int>(tc.CompanyName, tc.ID))
                            .Distinct()
                            .ToList();

                return list;
            }
            return new List<KeyValuePair<string,int>>();
        }

        protected string GetMerchantTerminalURL()
        {
            return "~/Merchants/0?ctl00.rngID.From=" + ddlCompanyID.SelectedValue + "&ctl00.rngID.To=" + ddlCompanyID.SelectedValue + "&Search=1&Page=0&SelectedIndex=0&OpenTab=true#BodyContent_PageController_FormView_FormPanel_Terminals";
        }

        protected void ddlCompanyID_SelectedIndexChanged(object sender, EventArgs e)
        {
            //var linkcontrol = (Page as Controls.DataTablePage).FormTabs.Tabs._tabs[0].FindControl("MerchantLink") as HyperLink;
            //linkcontrol.NavigateUrl = url;
            string url = Web.WebUtils.CurrentDomain.ContentUrl + "admin/Merchants/0?ctl00.rngID.From=" + ddlCompanyID.SelectedValue + "&ctl00.rngID.To=" + ddlCompanyID.SelectedValue + "&Search=1&Page=0&SelectedIndex=0&OpenTab=true#BodyContent_PageController_FormView_FormPanel_Terminals";
            //MerchantLink.NavigateUrl = url;
            //MerchantLink.DataBind();
            //UPLink.Update();
        }

        protected void ddlDebitCompany_SelectedIndexChanged(object sender, EventArgs e)
        {            
            TemplatePage.PageController.FormView.Update();
        }
    }
}