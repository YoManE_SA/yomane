﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Summary.ascx.cs" Inherits="Netpay.Admin.Modules.DebitCompanies.Terminals.Summary" %>
<admin:FormSections ID="FormSections1" runat="server" Title="Status">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <ul class="list-group">
                <li class="list-group-item"><asp:CheckBox runat="server" ID="chkIsActive" Checked='<%# ItemData.IsActive %>' Text="Is Active" /></li>
                <asp:MultiView runat="server" ActiveViewIndex='<%# ItemData.ID == 0? 0 : 1 %>'>
                    <asp:View runat="server">
                        <li class="list-group-item">Debit Company <netpay:DebitCompanyDropDown AutoPostBack="true" OnSelectedIndexChanged="ddlDebitCompany_SelectedIndexChanged" runat="server" CssClass="form-control" ID="ddlDebitCompany" Value='<%# ItemData.DebitCompany %>' Enabled='<%# ItemData.ID == 0 %>' BlankSelectionText="Choose Debit Company" BlankSelectionValue="0" EnableBlankSelection="True" /></li>
                    </asp:View>
                    <asp:View runat="server">
                        <li class="list-group-item"><asp:Literal runat="server" Text='<%# ItemData.DebitCompany != 0 ? "Debit Company : "+ Netpay.Bll.DebitCompanies.DebitCompany.LoadDebitCompanyById(ItemData.DebitCompany).Name : "" %>'></asp:Literal></li>
                    </asp:View>
                </asp:MultiView>
                
            </ul>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections ID="statistics" runat="server" Title="Statistics">
    <div class="row inner-table-titles">
        <div class="col-lg-3 col-md-3">Pass</div>
        <div class="col-lg-3 col-md-3">Fail</div>
        <div class="col-lg-3 col-md-3">Pre-Auth</div>
        <div class="col-lg-3 col-md-3">Del</div>
    </div>
    <div class="row table-ui">
        <div class="col-lg-3 col-md-3">
            <asp:Image runat="server" ImageUrl="~/Images/icon_ccOkTransShekel2.gif" ImageAlign="Middle" />
            (<asp:Literal ID="Passtxt" runat="server" Text="<%#PassCount()%>"></asp:Literal>)
        </div>
        <div class="col-lg-3 col-md-3">
            <asp:Image runat="server" ImageUrl="~/Images/icon_ccFailedTrans.gif" ImageAlign="Middle" />
            (<asp:Literal ID="Failtxt" runat="server" Text="<%#FailCount()%>"></asp:Literal>)
        </div>
        <div class="col-lg-3 col-md-3">
            <asp:Image runat="server" ImageUrl="~/Images/icon_ccOkTrans.gif" ImageAlign="Middle" />
            (<asp:Literal ID="PreAuthtxt" runat="server" Text="<%#PreAuthCount()%>"></asp:Literal>)
        </div>
        <div class="col-lg-3 col-md-3">
            Del(<asp:Literal ID="Deltxt" runat="server" Text="<%#DelCount()%>"></asp:Literal>) 
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" ID="MerchantList" Title="Merchant List">
    <asp:MultiView ID="MerchantListMultiView" runat="server" ActiveViewIndex="<%# GetMerchantList().Count()>0? 0 : 1 %>">
        <asp:View runat="server">
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <div class="form-group">
                        <asp:Literal ID="ltMerchantList" runat="server" Text="Merchant list &nbsp;" Visible="true" />
                    </div>
                </div>
                <div class="col-lg-3 col-md-4">
                    <div class="form-group">
                        <asp:DropDownList ID="ddlCompanyID" runat="server" DataSource="<%#GetMerchantList()%>" DataTextField="Key" DataValueField="Value" Visible="true" />
                    </div>
                </div>
           
            </div>
        </asp:View>
        <asp:View runat="server">
            <div class="form-group alert alert-info">
                <asp:Literal ID="Literal1" runat="server" Text="There are no merchants with this terminal" Visible="true" />
            </div>
        </asp:View>
    </asp:MultiView>
</admin:FormSections>
