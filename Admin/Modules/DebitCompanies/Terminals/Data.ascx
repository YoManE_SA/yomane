﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Data.ascx.cs" Inherits="Netpay.Admin.DebitCompanies.Terminals.Data" %>
<admin:FormSections runat="server" Title="Terminal Details">
    <div class="row">
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Terminal Number
				<asp:TextBox AutoPostBack="true" OnTextChanged="txtTerminalNumber_TextChanged" runat="server" CssClass="form-control" ID="txtTerminalNumber" Text='<%# ItemData.TerminalNumber %>' />
            </div>

            <div class="form-group">
                Terminal Number 3D
				<asp:TextBox runat="server" CssClass="form-control" ID="txtTerminalNumber3D" Text='<%# ItemData.TerminalNumber3D %>' />
            </div>
            <div class="form-group">
                Name
				<asp:TextBox runat="server" CssClass="form-control" ID="txtName" Text='<%# ItemData.Name %>' />
            </div>
            <div class="form-group">
                Contract Number
				<asp:TextBox runat="server" CssClass="form-control" ID="txtContractNumber" Text='<%# ItemData.ContractNumber %>' />
            </div>
        </div>

        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Descriptor
				<asp:TextBox runat="server" CssClass="form-control" ID="txtDescriptor" Text='<%# ItemData.Descriptor %>' />
            </div>
            <div class="form-group">
                Mcc
				<asp:TextBox runat="server" CssClass="form-control" ID="txtMcc" Text='<%# ItemData.Mcc %>' />
            </div>
            <div class="form-group">
                Bank Ext ID
				<asp:TextBox runat="server" CssClass="form-control" ID="txtBankExternalID" Text='<%# ItemData.BankExternalID %>' />
            </div>
            <div class="form-group">
                Bank Account
                <netpay:BankAccountDetailsDropDown CssClass="form-control" runat="server" ID="dt_BankAccountID" EnableBlankSelection="True" BlankSelectionText="None" BlankSelectionValue="0" Value="<%#ItemData.BankAccountID.ToString() %>" />
            </div>

        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Type
                <asp:DropDownList ID="ddlprocessingMethod" CssClass="form-control" runat="server" OnDataBound="ddlprocessingMethod_DataBound">
                    <asp:ListItem Value="1" Text="To debit company" />
                    <asp:ListItem Value="2" Text="Insert to pending" />
                </asp:DropDownList>
            </div>
            <div class="form-group">
                Monthly Pay
                <div class="input-group">
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtTerminalMonthlyCost" Text='<%# ItemData.TerminalMonthlyCost %>' />
                    <span class="input-group-addon"><span>ILS</span></span>
                </div>
            </div>
            <div class="form-group">
                SMS ShortCode
				<asp:TextBox runat="server" CssClass="form-control" ID="txtSMSShortCode" Text='<%# ItemData.SMSShortCode %>' />
            </div>
        </div>
        <div class="col-lg-12">
            <asp:UpdatePanel runat="server" ID="SearchTagUpdatePanel" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                Choose search tag
                            </div>
                            <div class="col-md-6">
                                Insert new search tag
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <netpay:DropDownBase OnSelectedIndexChanged="ddlSearchTags_SelectedIndexChanged" CssClass="form-control" EnableBlankSelection="true" BlankSelectionValue="" BlankSelectionText="Choose Search Tag" runat="server" ID="ddlSearchTags" DataSource='<%# SearchTagsList %>' AutoPostBack="true">
                                </netpay:DropDownBase>
                            </div>

                            <div class="col-md-6">
                                <asp:TextBox runat="server" CssClass="form-control" ID="txtSearchTag" Text='<%# ddlSearchTags.SelectedValue %>'>
                                </asp:TextBox>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="col-lg-12">
            Terminal Notes
			<asp:TextBox runat="server" CssClass="form-control" ID="txtTerminalNotes" Text='<%# ItemData.TerminalNotes %>' Rows="5" TextMode="MultiLine" />
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Options">
    <div class="row">
        <div class="col-lg-6  col-md-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsNetpayTerminal" Checked='<%# ItemData.IsNetpayTerminal %>' Text="Owned by Gateway" /><li />
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsShvaMasterTerminal" Checked='<%# ItemData.IsShvaMasterTerminal %>' Text="Is Shva Master Terminal" />
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsManipulateAmount" Checked='<%# ItemData.IsManipulateAmount %>' Text="Manipulate Charge Amount" /><li />
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsUseBlackList" Checked='<%# ItemData.IsUseBlackList %>' Text="Is Use BlackList" /><li />
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsRefundBlocked" Checked='<%# ItemData.IsRefundBlocked %>' Text="Refund is prohibited" /><li />
            </ul>
        </div>
        <div class="col-lg-6  col-md-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkEnableRecurringBank" Checked='<%# ItemData.EnableRecurringBank %>' Text="Enable Bank Recurring" /><li />
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkEnableAuthorization" Checked='<%# ItemData.EnableAuthorization %>' Text="Enable Authorization only transaction" /><li />
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkEnable3dsecure" Checked='<%# ItemData.Enable3dsecure %>' Text="Enable 3D secure" /><li />
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsPersonalNumberRequired" Checked='<%# ItemData.IsPersonalNumberRequired %>' Text="Requires personal number" /><li />
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsTestTerminal" Checked='<%# ItemData.IsTestTerminal %>' Text="This is a test terminal" /><li />
            </ul>
        </div>
    </div>
</admin:FormSections>
<admin:ModalDialog runat="server" ID="dlgTerminalNumberCheck" Title="Terminal Number Msg">
    <Body>
        <netpay:ActionNotify runat="server" ID="TerminalNumberNotExistNotify" />
    </Body>
</admin:ModalDialog>
