﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.DebitCompanies.Terminals
{
    public partial class Filter : Controls.TemplateControlBase
    {
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
        {
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Bll.DebitCompanies.Terminal.SearchFilters();
            sf.ID = rngID.Value;
            if (apAccount.Value.HasValue)
            {
                var account = Bll.Accounts.Account.LoadAccount(apAccount.Value.Value);
                if (account != null && account.AccountType == Bll.Accounts.AccountType.DebitCompany)
                    sf.DebitCompanyId = account.DebitCompanyID;
            }
            sf.Number = txtNumber.Text.NullIfEmpty();
            sf.IsActive = ddlIsActive.BoolValue;
            TemplatePage.SetFilter(sf);
        }
    }
}