﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConnectionInfo.ascx.cs" Inherits="Netpay.Admin.DebitCompanies.Terminals.ConnectionInfo" %>
<admin:FormSections runat="server" Title="Connection information">
    <div class="row">
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                AccssesName1
				<asp:TextBox runat="server" CssClass="form-control" ID="txtAccssesName1" Text='<%# ItemData.AccssesName1 %>' />
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                AccssesName2
			<asp:TextBox runat="server" CssClass="form-control" ID="txtAccssesName2" Text='<%# ItemData.AccssesName2 %>' />
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                AccssesName3
				<asp:TextBox runat="server" CssClass="form-control" ID="txtAccssesName3" Text='<%# ItemData.AccssesName3 %>' />
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Password
				<asp:TextBox runat="server" CssClass="form-control" ID="txtPassword" />
            </div>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="3D Connection information">
    <div class="row">
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                3DAccssesName1
				<asp:TextBox runat="server" CssClass="form-control" ID="txtAccssesName1_3D" Text='<%# ItemData.AccssesName1_3D %>' />
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
				3DAccssesName2
				<asp:TextBox runat="server" CssClass="form-control" ID="txtAccssesName2_3D" Text='<%# ItemData.AccssesName2_3D %>' />
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                3DAccssesName3
				<asp:TextBox runat="server" CssClass="form-control" ID="txtAccssesName3_3D" Text='<%# ItemData.AccssesName3_3D %>' />
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Password
				<asp:TextBox runat="server" CssClass="form-control" ID="txtPassword3D" />
            </div>
        </div>
    </div>
</admin:FormSections>
