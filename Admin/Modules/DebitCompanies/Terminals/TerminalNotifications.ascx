﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalNotifications.ascx.cs" Inherits="Netpay.Admin.Modules.DebitCompanies.Terminals.TerminalNotifications" %>
<admin:FormSections runat="server" Title="CHB ALERT">
    <asp:MultiView ID="MultiViewTwo" runat="server" ActiveViewIndex="<%# TemplatePage.ItemID!=null && TemplatePage.ItemID!=0 ? 0 : 1 %>">
        <asp:View ID="ViewIfNotNewDebitCompany2" runat="server">
            <div class="panel panel-default">
                <div class="panel-heading font-small" style="background-color: #fcfcfc;">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <asp:Image runat="server" ImageUrl='<%# "/NPCommon/ImgPaymentMethod/23X12/22.gif" %>' ImageAlign="Middle" />
                            VISA
                        </div>

                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <ul class="list-group">

                                <li class="list-group-item">Monthly limit:
                                    <asp:TextBox ID="txtMonthlyLimitCHB" CssClass="form-control" runat="server" Text="<%# ItemData.MonthlyLimitCHB > 0 ? ItemData.MonthlyLimitCHB.ToString() : string.Empty %>" />
                                    <asp:RangeValidator ControlToValidate="txtMonthlyLimitCHB" MinimumValue="0" MaximumValue="999999" ErrorMessage="Monthly limit is invalid!" Display="none" SetFocusOnError="true" Type="Integer" runat="server" /></li>
                                <li class="list-group-item">Count:
                                    <asp:Literal ID="CountChb" runat="server" Text="<%#ItemData.MonthlyCHB%>" /></li>
                            </ul>
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Notify By Mail:</h4>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <asp:CheckBoxList OnDataBound="cblMonthlyLimitCHBNotifyUsers_DataBound1" ID="cblMonthlyLimitCHBNotifyUsers" CssClass="option" RepeatDirection="Vertical" runat="server" DataSource="<%#Netpay.Bll.DebitCompanies.DebitCompanyRule.NotifyByMailUsers%>" />
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <h4 class="margin-top-10">Notify By SMS:</h4>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <asp:CheckBoxList OnDataBound="cblMonthlyLimitCHBNotifyUsersSMS_DataBound1" ID="cblMonthlyLimitCHBNotifyUsersSMS" CssClass="option" RepeatDirection="Vertical" runat="server" DataSource="<%#Netpay.Bll.DebitCompanies.DebitCompanyRule.NotifyBySmsUsers %>" /></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>


            <div class="panel panel-default">
                <div class="panel-heading font-small" style="background-color: #fcfcfc;">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <asp:Image runat="server" ImageUrl='<%# "/NPCommon/ImgPaymentMethod/23X12/25.gif" %>' ImageAlign="Middle" />
                            Master Card
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <ul class="list-group">
                                <li class="list-group-item">Monthly limit:
                                    <asp:TextBox ID="txtMonthlyMCLimitCHB" CssClass="form-control" runat="server" Text="<%#ItemData.MonthlyMCLimitCHB > 0 ? ItemData.MonthlyMCLimitCHB.ToString() : string.Empty %>" />
                                    <asp:RangeValidator ControlToValidate="txtMonthlyMCLimitCHB" MinimumValue="0" MaximumValue="999999" ErrorMessage="MonthlyMC limit is invalid!" Display="none" SetFocusOnError="true" Type="Integer" runat="server" />
                                </li>
                                <li class="list-group-item">Count:
                                    <asp:Literal ID="CountMcChb" runat="server" Text="<%#ItemData.MonthlyMCCHB%>" />
                                </li>
                            </ul>
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Notify By Mail:</h4>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <asp:CheckBoxList OnDataBound="cblMonthlyMCLimitCHBNotifyUsers_DataBound" ID="cblMonthlyMCLimitCHBNotifyUsers" CssClass="option" RepeatDirection="Vertical" runat="server" DataSource="<%#Netpay.Bll.DebitCompanies.DebitCompanyRule.NotifyByMailUsers%>" />
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <h4 class="margin-top-10">Notify By SMS:</h4>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <asp:CheckBoxList OnDataBound="cblMonthlyMCLimitCHBNotifyUsersSMS_DataBound" ID="cblMonthlyMCLimitCHBNotifyUsersSMS" CssClass="option" RepeatDirection="Vertical" runat="server" DataSource="<%#Netpay.Bll.DebitCompanies.DebitCompanyRule.NotifyBySmsUsers%>" />
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <asp:UpdatePanel runat="server" ID="upAlertStatus" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-warning">
                                <strong>Alert Status:</strong>
                                <br />
                                <div>

                                    <asp:Image runat="server" ImageUrl='<%# "/NPCommon/ImgPaymentMethod/23X12/22.gif" %>' ImageAlign="Middle" />
                                    <asp:Literal ID="MonthlyChbWasSendTxt" runat="server" Text='<%#ItemData.MonthlyCHBWasSent? "Last alert sent on:"+ItemData.MonthlyCHBSendDate.ToString()+"-No additional alerts will be sent this month - " : "No alert was sent this month"  %>' />
                                    <asp:LinkButton runat="server" ID="ResetMonthlyChb" Text='<%#ItemData.MonthlyCHBWasSent? "Reset" : string.Empty %>' OnCommand="ResetMonthlyChb_Click" CommandArgument="<%# ItemData.ID %>" />
                                </div>
                                <div class="margin-top-10">

                                    <asp:Image runat="server" ImageUrl='<%# "/NPCommon/ImgPaymentMethod/23X12/25.gif" %>' ImageAlign="Middle" />
                                    <asp:Literal ID="MonthlyMcChbWasSendTxt" runat="server" Text='<%#ItemData.MonthlyMCCHBWasSent? "Last alert sent on:"+ItemData.MonthlyMCCHBSendDate.ToString()+"-No additional alerts will be sent this month -" : "No alert was sent this month"  %>' />
                                    <asp:LinkButton runat="server" ID="ResetMonthlyMcChb" Text='<%#ItemData.MonthlyMCCHBWasSent? "Reset" : string.Empty %>' OnCommand="ResetMonthlyMcChb_Click" sCommandArgument="<%#ItemData.ID%>" />
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:View>
        <asp:View ID="ViewIfNew2" runat="server">
            <div class="alert alert-info">
                Save Terminal to edit this section.
            </div>
        </asp:View>
    </asp:MultiView>
</admin:FormSections>
