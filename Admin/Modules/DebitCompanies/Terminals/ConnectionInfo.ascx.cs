﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;

namespace Netpay.Admin.DebitCompanies.Terminals
{
	public partial class ConnectionInfo : Controls.TemplateControlBase
	{
		protected Netpay.Bll.DebitCompanies.Terminal ItemData;
		protected override void OnLoad(EventArgs e)
		{
			TemplatePage.PageController.SaveItem += PageController_SaveItem;
			base.OnLoad(e);
		}

		protected override void DataBindChildren()
		{
			ItemData = TemplatePage.GetItemData<Bll.DebitCompanies.Terminal>();
			if (ItemData == null) ItemData = new Bll.DebitCompanies.Terminal(0);
			string passwrod, password3d;
			ItemData.GetDecryptedData(out passwrod, out password3d);
			txtPassword.Text = passwrod;
			txtPassword3D.Text = password3d;
			base.DataBindChildren();
		}

		protected void PageController_SaveItem(object sender, EventArgs e)
		{
			ItemData = TemplatePage.GetItemData<Bll.DebitCompanies.Terminal>();
			if(ItemData == null) return;

			ItemData.AccssesName1 = txtAccssesName1.Text.Trim();
			ItemData.AccssesName2 = txtAccssesName2.Text.Trim().Replace("'","''");
			ItemData.AccssesName3 = txtAccssesName3.Text.Trim().Replace("'","''");

			ItemData.AccssesName1_3D = txtAccssesName1_3D.Text.Trim();
			ItemData.AccssesName2_3D = txtAccssesName2_3D.Text.Trim().Replace("'", "''");
			ItemData.AccssesName3_3D = txtAccssesName3_3D.Text.Trim().Replace("'", "''");

			ItemData.SetDecryptedData(txtPassword.Text.Trim().Replace("'","''"), txtPassword3D.Text.Trim().Replace("'", "''"));
			ItemData.Save();
            txtPassword.Text = "";
            txtPassword3D.Text = "";
            TemplatePage.SetItemData<Bll.DebitCompanies.Terminal>(ItemData);
        }
	}
}