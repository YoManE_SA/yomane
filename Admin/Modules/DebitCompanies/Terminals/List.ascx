﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.DebitCompanies.Terminals.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Terminals">
    <Header>
        <admin:LegendColors runat="server" Items='<%# Netpay.Bll.DebitCompanies.DebitCompany.StatusColor  %>' ItemsText='<%# Netpay.Bll.DebitCompanies.DebitCompany.StatusText %>' FloatRight="true" />
    </Header>
    <Body>
        <admin:AdminList runat="server" ID="rptList" SaveAjaxState="true" DataKeyNames="ID" AutoGenerateColumns="false" BubbleLoadEvent="true">
            <Columns>
                <asp:TemplateField HeaderText="ID" SortExpression="ID">
                    <ItemTemplate>
                        <span style="background-color: <%# Netpay.Infrastructure.Enums.BoolColorMap.MapHtmlColor((bool)Eval("IsActive")) %>;" class="legend-item"></span>&nbsp;
					<%# Eval("ID") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Name" DataField="Name" SortExpression="Name" />
                <asp:TemplateField HeaderText="Debit Company">
                    <ItemTemplate>
                        <asp:Image runat="server" ImageUrl='<%# "/NPCommon/ImgDebitCompanys/23X12/" + Eval("DebitCompany") + ".gif" %>' ImageAlign="Middle" Visible='<%# ((byte)Eval("DebitCompany")) != 0 %>' />
                        <asp:Literal runat="server" Text='<%# GetDebitCompanyName() %>'></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Terminal Number" DataField="TerminalNumber" SortExpression="TerminalNumber" />
                <asp:BoundField HeaderText="Contract Number" DataField="ContractNumber" SortExpression="ContractNumber" />
            </Columns>
        </admin:AdminList>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd='<%# Netpay.Bll.DebitCompanies.Terminal.SecuredObject.HasPermission(Netpay.Infrastructure.Security.PermissionValue.Add) %>' />
    </Footer>

</admin:ListSection>
