﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Modules.DebitCompanies.Terminals
{
    public partial class CardCompanyDetails : Controls.TemplateControlBase
    {
        protected Netpay.Bll.DebitCompanies.Terminal ItemData;
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += PageController_SaveItem;
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            ItemData = TemplatePage.GetItemData<Bll.DebitCompanies.Terminal>();
            if (ItemData == null) ItemData = new Bll.DebitCompanies.Terminal(0);
            base.DataBindChildren();
        }

        protected void PageController_SaveItem(object sender, EventArgs e)
        {
            ItemData = TemplatePage.GetItemData<Bll.DebitCompanies.Terminal>();
            if (ItemData == null) return;

            //Visa 
            ItemData.dt_CompanyNum_visa = txtCompanyNum_visa.Text;
            ItemData.dt_Comments_visa = txtComments_visa.Text;

            //MasterCard
            ItemData.dt_CompanyNum_mastercard = txtCompanyNum_mastercard.Text;
            ItemData.dt_Comments_mastercard = txtComments_mastercard.Text;

            //Diners
            ItemData.dt_CompanyNum_diners = txtCompanyNum_diners.Text;
            ItemData.dt_Comments_diners = txtComments_diners.Text;

            //Amex
            ItemData.dt_CompanyNum_americanexp = txtCompanyNum_americanexp.Text;
            ItemData.dt_Comments_americanexp = txtComments_americanexp.Text;

            //IsraCard
            ItemData.dt_CompanyNum_isracard = txtCompanyNum_isracard.Text;
            ItemData.dt_Comments_isracard = txtComments_isracard.Text;

            //Direct
            ItemData.dt_CompanyNum_direct = txtCompanyNum_direct.Text;
            ItemData.dt_Comments_direct = txtComments_direct.Text;

            ItemData.Save();
            TemplatePage.SetItemData<Bll.DebitCompanies.Terminal>(ItemData);
        }
    }
}