﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.DebitCompanies.Terminals.Filter" %>
<admin:FilterSection runat="server" Title="Status">
    <div class="form-group">
        <asp:Label ID="lblDebitCompany" runat="server" Text="DebitCompany"  />
        <admin:AccountPicker runat="server" ID="apAccount" LimitToType="DebitCompany" UseTargetID="false" />
    </div>
    <div class="form-group">
        <asp:Label ID="lblStatus" runat="server" Text="Status"  />
        <netpay:BoolDropDown CssClass="form-control" runat="server" ID="ddlIsActive" TrueText="Active" FalseText="Not Active" />
    </div>
    <div class="form-group">
        <asp:Label ID="lblNumber" runat="server" Text="Terminal Number"  />
        <asp:TextBox CssClass="form-control" runat="server" ID="txtNumber" />
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblID" runat="server" Text="ID Range"  />
                <JQ:IntRange runat="server" ID="rngID" />
            </div>
        </div>
    </div>

</admin:FilterSection>
