﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;
using Netpay.Bll.Accounts;

namespace Netpay.Admin.Modules.DebitCompanies
{
    public partial class Notifications : Controls.AccountControlBase
    {
        public DateTime MinimumDate = DateTime.Parse("1900-01-01 00:00:00.000");

        public Bll.DebitCompanies.DebitCompany DebitCompanyItem { get; set; }

        public Bll.DebitCompanies.DebitCompanyRule DebitCompanyRuleItem { get; set; }

        public List<Bll.DebitCompanies.DebitCompanyRule> DebitCompanyRulesList { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            DebitCompanyRuleItem = new Netpay.Bll.DebitCompanies.DebitCompanyRule();
            TemplatePage.PageController.SaveItem += PageController_SaveItem;
            base.OnLoad(e);

            //DebitCompanyItem = Account as Netpay.Bll.DebitCompanies.DebitCompany;
            //if (DebitCompanyItem!= null)
            //DebitCompanyRulesList = Bll.DebitCompanies.DebitCompanyRule.GetRulesByDebitCompanyId(DebitCompanyItem.ID);
        }

        protected override void DataBindChildren()
        {
            DebitCompanyItem = Account as Netpay.Bll.DebitCompanies.DebitCompany;
            if (DebitCompanyItem == null) DebitCompanyItem = new Bll.DebitCompanies.DebitCompany();

            DebitCompanyRulesList = Bll.DebitCompanies.DebitCompanyRule.GetRulesByDebitCompanyId(DebitCompanyItem.ID).OrderBy(x=>x.Rating).ToList();
            if (DebitCompanyRulesList != null)
            {
                rptDebitRuleList.DataSource = DebitCompanyRulesList;
                rptDebitRuleList.DataBind();
            }

            base.DataBindChildren();
        }

        protected void PageController_SaveItem(object sender, EventArgs e)
        {
            DebitCompanyItem = Account as Netpay.Bll.DebitCompanies.DebitCompany;
            if (DebitCompanyItem == null || DebitCompanyItem.DebitCompanyID == null || DebitCompanyItem.DebitCompanyID==0) return;

            //SaveNotifyUserscbl
            List<string> SelectedItems = new List<string>();
            foreach (ListItem item in cblAutoRefundNotifyUsers.Items)
            {
                if (item.Selected) SelectedItems.Add(item.Text);
            }
            if (SelectedItems.Count == 0) DebitCompanyItem.AutoRefundNotifyUsers = "";
            else
            {
                string result = String.Join(";", SelectedItems.ToArray());
                DebitCompanyItem.AutoRefundNotifyUsers = result;
            }

            //SaveChbAlertSection 
            if (DebitCompanyItem != null)
            {
                //Monthly limit VISA input:
                if (txtMonthlyLimitCHB.Text == "") DebitCompanyItem.MonthlyLimitCHB = 0;
                else
                {
                    int MonthlyLimitChbInt;
                    if (Int32.TryParse(txtMonthlyLimitCHB.Text, out MonthlyLimitChbInt))
                    {
                        DebitCompanyItem.MonthlyLimitCHB = MonthlyLimitChbInt;
                    }
                }

                //Monthly limit VISA notify users.
                List<string> SIcblMonthlyLimitCHBNotifyUsers = new List<string>();
                foreach (ListItem item in cblMonthlyLimitCHBNotifyUsers.Items)
                {
                    if (item.Selected) SIcblMonthlyLimitCHBNotifyUsers.Add(item.Text);
                }
                if (SIcblMonthlyLimitCHBNotifyUsers.Count == 0) DebitCompanyItem.MonthlyLimitCHBNotifyUsers = "";
                else
                {
                    string result = String.Join(";", SIcblMonthlyLimitCHBNotifyUsers.ToArray());
                    DebitCompanyItem.MonthlyLimitCHBNotifyUsers = result;
                }

                //Monthly limit VISA notify users by SMS
                List<string> SIcblMonthlyLimitCHBNotifyUsersbySMS = new List<string>();
                foreach (ListItem item in cblMonthlyLimitCHBNotifyUsersSMS.Items)
                {
                    if (item.Selected) SIcblMonthlyLimitCHBNotifyUsersbySMS.Add(item.Text);
                }
                if (SIcblMonthlyLimitCHBNotifyUsersbySMS.Count == 0) DebitCompanyItem.MonthlyLimitCHBNotifyUsersSMS = "";
                else
                {
                    string result = String.Join(";", SIcblMonthlyLimitCHBNotifyUsersbySMS.ToArray());
                    DebitCompanyItem.MonthlyLimitCHBNotifyUsersSMS = result;
                }


                //Monthly limit MC imput:
                if (txtMonthlyMCLimitCHB.Text == "") DebitCompanyItem.MonthlyMCLimitCHB = 0;
                else
                {
                    int MonthlyMcLimitCHBInt;
                    if (Int32.TryParse(txtMonthlyMCLimitCHB.Text, out MonthlyMcLimitCHBInt))
                    {
                        DebitCompanyItem.MonthlyMCLimitCHB = MonthlyMcLimitCHBInt;
                    }
                }

                //Monthly limit MC notify users:
                List<string> SIcblMonthlyMCLimitCHBNotifyUsers = new List<string>();
                foreach (ListItem item in cblMonthlyMCLimitCHBNotifyUsers.Items)
                {
                    if (item.Selected) SIcblMonthlyMCLimitCHBNotifyUsers.Add(item.Text);
                }
                if (SIcblMonthlyMCLimitCHBNotifyUsers.Count == 0) DebitCompanyItem.MonthlyMCLimitCHBNotifyUsers = "";
                else
                {
                    string result = String.Join(";", SIcblMonthlyMCLimitCHBNotifyUsers.ToArray());
                    DebitCompanyItem.MonthlyMCLimitCHBNotifyUsers = result;
                }

                //Monthly limit MC notify users by SMS
                List<string> SIcblMonthlyMCLimitCHBNotifyUsersSMS = new List<string>();
                foreach (ListItem item in cblMonthlyLimitCHBNotifyUsersSMS.Items)
                {
                    if (item.Selected) SIcblMonthlyMCLimitCHBNotifyUsersSMS.Add(item.Text);
                }
                if (SIcblMonthlyMCLimitCHBNotifyUsersSMS.Count == 0) DebitCompanyItem.MonthlyMCLimitCHBNotifyUsersSMS = "";
                else
                {
                    string result = String.Join(";", SIcblMonthlyMCLimitCHBNotifyUsersSMS.ToArray());
                    DebitCompanyItem.MonthlyMCLimitCHBNotifyUsersSMS = result;
                }

                DebitCompanyItem.Save();
                TemplatePage.SetItemData(DebitCompanyItem);
            }
        }

        
        protected void cblAutoRefundNotifyUsers_DataBound(object sender, EventArgs e)
        {
            string UsersList;
            if (DebitCompanyItem != null)
            {
                if (DebitCompanyItem.AutoRefundNotifyUsers != "" && DebitCompanyItem.AutoRefundNotifyUsers != null)
                {
                    UsersList = DebitCompanyItem.AutoRefundNotifyUsers;
                    foreach (ListItem name in cblAutoRefundNotifyUsers.Items)
                    {
                        if (UsersList.Contains(name.Text)) name.Selected = true;
                    }
                }
            }
        }

        protected void cblMonthlyLimitCHBNotifyUsers_DataBound(object sender, EventArgs e)
        {
            string UsersList;
            if (DebitCompanyItem != null)
            {
                if (DebitCompanyItem.MonthlyLimitCHBNotifyUsers != "" && DebitCompanyItem.MonthlyLimitCHBNotifyUsers != null)
                {
                    UsersList = DebitCompanyItem.MonthlyLimitCHBNotifyUsers;
                    foreach (ListItem name in cblMonthlyLimitCHBNotifyUsers.Items)
                    {
                        if (UsersList.Contains(name.Text)) name.Selected = true;
                    }
                }
            }
        }

        protected void cblMonthlyMCLimitCHBNotifyUsers_DataBound(object sender, EventArgs e)
        {
            string UsersList;
            if (DebitCompanyItem != null)
            {
                if (DebitCompanyItem.MonthlyMCLimitCHBNotifyUsers != "" && DebitCompanyItem.MonthlyMCLimitCHBNotifyUsers != null)
                {
                    UsersList = DebitCompanyItem.MonthlyMCLimitCHBNotifyUsers;
                    foreach (ListItem name in cblMonthlyMCLimitCHBNotifyUsers.Items)
                    {
                        if (UsersList.Contains(name.Text)) name.Selected = true;
                    }
                }
            }
        }

        protected void ModalDialogNotifyEmailUsers_DataBound(object sender, EventArgs e)
        {
            string UsersList;
            if (DebitCompanyRuleItem != null)
            {
                if (DebitCompanyRuleItem.NotifyUsers != "" && DebitCompanyRuleItem.NotifyUsers != null)
                {
                    UsersList = DebitCompanyRuleItem.NotifyUsers;
                    foreach (ListItem name in MdNotifyUsersByEmailCheckBoxList.Items)
                    {
                        if (UsersList.Contains(name.Text)) name.Selected = true;
                    }
                }
            }
        }

        protected void ModalDialogNotifySmsUsers_DataBound(object sender, EventArgs e)
        {
            string UsersList;
            if (DebitCompanyRuleItem != null)
            {
                if (DebitCompanyRuleItem.NotifyUsersSMS != "" && DebitCompanyRuleItem.NotifyUsersSMS != null)
                {
                    UsersList = DebitCompanyRuleItem.NotifyUsersSMS;
                    foreach (ListItem name in MdNotifyUsersBySmsCheckBoxList.Items)
                    {
                        if (UsersList.Contains(name.Text)) name.Selected = true;
                    }
                }
            }
        }

        protected void cblMonthlyLimitCHBNotifyUsersSMS_DataBound(object sender, EventArgs e)
        {
            string UsersList;
            if (DebitCompanyItem != null)
            {
                if (DebitCompanyItem.MonthlyLimitCHBNotifyUsersSMS != "" && DebitCompanyItem.MonthlyLimitCHBNotifyUsersSMS != null)
                {
                    UsersList = DebitCompanyItem.MonthlyLimitCHBNotifyUsersSMS;
                    foreach (ListItem name in cblMonthlyLimitCHBNotifyUsersSMS.Items)
                    {
                        if (UsersList.Contains(name.Text)) name.Selected = true;
                    }
                }
            }
        }

        //The modal-dialog Dialog_Command method.
        protected void Dialog_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "AddItem":
                    var page = (sender as Netpay.Admin.Controls.FormSections).Page as Controls.AccountPage;
                    int? debitcompanyid = page.Account.DebitCompanyID;
                    if (debitcompanyid != null)
                    {
                        DebitCompanyRuleItem = new Netpay.Bll.DebitCompanies.DebitCompanyRule() { DebitCompanyId = (int)debitcompanyid };
                        dlgEdit.BindAndShow();
                    }
                    break;

                case "EditItem":
                    //get the pressed debit rule item id:
                    int debitruleid;
                    if (Int32.TryParse(e.CommandArgument.ToString(), out debitruleid))
                    {
                        //load the object represents the debit rule item pressed:
                        DebitCompanyRuleItem = Netpay.Bll.DebitCompanies.DebitCompanyRule.Load(debitruleid);
                    }
                    if (DebitCompanyRuleItem != null) dlgEdit.BindAndShow();
                    break;

                case "SaveItem":
                    if (RuleId.Text == "0")
                    {
                        DebitCompanyRuleItem = new Bll.DebitCompanies.DebitCompanyRule() { LastFailDate = MinimumDate, LastUnblockDate = MinimumDate, DebitCompanyId = hfDebitCompanyId.Value.ToInt32() };
                    }
                    else
                    {
                        int debitruleidtosave = int.Parse(RuleId.Text);
                        DebitCompanyRuleItem = Netpay.Bll.DebitCompanies.DebitCompanyRule.Load(debitruleidtosave);
                    }
                    // save details ...
                    DebitCompanyRuleItem.ReplyCodes = ReplyCodesTxt.Text;
                    DebitCompanyRuleItem.FailCount = FailCountTxt.Text.ToInt32();
                    DebitCompanyRuleItem.AttemptCount = AttemptsCountTxt.Text.ToInt32();
                    
                    //Notify by mail users list :  
                    List<string> NotifyByMailUsersList = new List<string>();
                    foreach (ListItem item in MdNotifyUsersByEmailCheckBoxList.Items)
                    {
                        if (item.Selected) NotifyByMailUsersList.Add(item.Text);
                    }
                    if (NotifyByMailUsersList.Count == 0) DebitCompanyRuleItem.NotifyUsers = "";
                    else
                    {
                        string result = String.Join(";", NotifyByMailUsersList.ToArray());
                        DebitCompanyRuleItem.NotifyUsers = result;
                    }

                    // Notify by sms users list :
                    List<string> NotifyBySmsUsersList = new List<string>();
                    foreach (ListItem item in MdNotifyUsersBySmsCheckBoxList.Items)
                    {
                        if (item.Selected) NotifyBySmsUsersList.Add(item.Text);
                    }
                    if (NotifyBySmsUsersList.Count == 0) DebitCompanyRuleItem.NotifyUsersSMS = "";
                    else
                    {
                        string result = String.Join(";", NotifyBySmsUsersList.ToArray());
                        DebitCompanyRuleItem.NotifyUsersSMS = result;
                    }
                    

                    DebitCompanyRuleItem.IsAutoDisable = Blockchb.Checked;
                    DebitCompanyRuleItem.IsAutoEnable = Unblockchb.Checked;
                    DebitCompanyRuleItem.AutoEnableMinutes = UnblockAfterAttemptsTxt.Text.ToInt32();
                    DebitCompanyRuleItem.AutoEnableAttempts = UnblockAfterAttemptsTxt.Text.ToInt32();
                    DebitCompanyRuleItem.IsActive = IsActivechb.Checked;
                    DebitCompanyRuleItem.Save();
                    dlgEdit.RegisterHide();

                    DebitCompanyRulesList = Bll.DebitCompanies.DebitCompanyRule.GetRulesByDebitCompanyId(hfDebitCompanyId.Value.ToInt32()).OrderBy(x=>x.Rating).ToList();
                    rptDebitRuleList.DataSource = DebitCompanyRulesList;
                    rptDebitRuleList.DataBind();
                    NotificationFormSection.DataBind();
                    upRuleList.Update();
                    break;

                case "DeleteItem":
                    
                    //get the pressed debit rule item id:
                    int debitruleidtodelete;
                    int dr_debitcompanyid;
                    if (Int32.TryParse(e.CommandArgument.ToString(), out debitruleidtodelete))
                    {
                        //load the object represents the debit rule item pressed:
                        DebitCompanyRuleItem = Netpay.Bll.DebitCompanies.DebitCompanyRule.Load(debitruleidtodelete);
                        dr_debitcompanyid = DebitCompanyRuleItem.DebitCompanyId;
                        DebitCompanyRuleItem.Delete();

                        //Refresh the list.
                        DebitCompanyRulesList = Bll.DebitCompanies.DebitCompanyRule.GetRulesByDebitCompanyId(dr_debitcompanyid).OrderBy(x => x.Rating).ToList();
                        rptDebitRuleList.DataSource = DebitCompanyRulesList;
                        rptDebitRuleList.DataBind();
                        NotificationFormSection.DataBind();
                        upRuleList.Update();
                    }
                    break;
            }
        }

        protected void MoveRating_Command(object sender, CommandEventArgs e)
        {
            string[] commandArgs = new string[2];
            commandArgs = e.CommandArgument.ToString().Split(',');
            int DebitRuleId = int.Parse(commandArgs[0]);
            int DebitCompanyId = int.Parse(commandArgs[1]);
            switch (e.CommandName)
            {
                case "Up":
                    //get the debit company rules list and debit rule item
                    DebitCompanyRulesList = Bll.DebitCompanies.DebitCompanyRule.GetRulesByDebitCompanyId(DebitCompanyId).OrderBy(x=>x.Rating).ToList();
                    //if the list has only 1 element return:
                    if (DebitCompanyRulesList.Count == 1) return;

                    DebitCompanyRuleItem = Bll.DebitCompanies.DebitCompanyRule.Load(DebitRuleId);
                    int index = DebitCompanyRulesList.FindIndex(x=>x.Rating==DebitCompanyRuleItem.Rating);
                                        

                    //if the rating is the max in the list return:
                    if (DebitCompanyRuleItem.Rating == DebitCompanyRulesList.Min(x => x.Rating)) return;

                    //replace the rating value 
                    int currentRatingVal = DebitCompanyRuleItem.Rating;
                    int afterReplaceRatingVal = DebitCompanyRulesList[index - 1].Rating;

                    //insert the afterReplaceRatingVal to the selected debit rule item
                    DebitCompanyRuleItem.Rating = afterReplaceRatingVal;
                    DebitCompanyRuleItem.Save();

                    //insert the current rating(before replacement) to the debit rule item that is need to be replaced and save
                    DebitCompanyRulesList[index-1].Rating = currentRatingVal;
                    DebitCompanyRulesList[index-1].Save();

                    rptDebitRuleList.DataSource=Bll.DebitCompanies.DebitCompanyRule.GetRulesByDebitCompanyId(DebitCompanyId).OrderBy(x=>x.Rating);
                    rptDebitRuleList.DataBind();
                    NotificationFormSection.DataBind();
                    upRuleList.Update();
                    break;

                case "Down":
                    //get the debit copmany rules list and debit rule item
                    DebitCompanyRulesList = Bll.DebitCompanies.DebitCompanyRule.GetRulesByDebitCompanyId(DebitCompanyId).OrderBy(x=>x.Rating).ToList();
                    //if the list has 1 element return:
                    if (DebitCompanyRulesList.Count == 1) return;

                    DebitCompanyRuleItem = Bll.DebitCompanies.DebitCompanyRule.Load(DebitRuleId);
                    int indexOfLow = DebitCompanyRulesList.FindIndex(x => x.Rating == DebitCompanyRuleItem.Rating);
                                        
                    //if the value of the rating is minimum return
                    if ((DebitCompanyRuleItem.Rating) == DebitCompanyRulesList.Max(x => x.Rating)) return;

                    //replace the rating value 
                    int currentRatingValOfLow = DebitCompanyRuleItem.Rating;
                    int afterReplaceRatingValOfLow = DebitCompanyRulesList[indexOfLow + 1].Rating;

                    //insert the afterReplaceRatingVal to the selected debit rule item
                    DebitCompanyRuleItem.Rating = afterReplaceRatingValOfLow;
                    DebitCompanyRuleItem.Save();

                    //insert the current rating(before replacement) to the debit rule item that is need to be replaced and save
                    DebitCompanyRulesList[indexOfLow + 1].Rating = currentRatingValOfLow;
                    DebitCompanyRulesList[indexOfLow + 1].Save();

                    rptDebitRuleList.DataSource = Bll.DebitCompanies.DebitCompanyRule.GetRulesByDebitCompanyId(DebitCompanyId).OrderBy(x => x.Rating);
                    rptDebitRuleList.DataBind();
                    NotificationFormSection.DataBind();
                    upRuleList.Update();
                    
                    break;
            }
        }

        protected void cblMonthlyMCLimitCHBNotifyUsersSMS_DataBound(object sender, EventArgs e)
        {
            string UsersList;
            if (DebitCompanyItem != null)
            {
                if (DebitCompanyItem.MonthlyMCLimitCHBNotifyUsersSMS != "" && DebitCompanyItem.MonthlyMCLimitCHBNotifyUsersSMS != null)
                {
                    UsersList = DebitCompanyItem.MonthlyMCLimitCHBNotifyUsersSMS;
                    foreach (ListItem name in cblMonthlyMCLimitCHBNotifyUsersSMS.Items)
                    {
                        if (UsersList.Contains(name.Text)) name.Selected = true;
                    }
                }
            }
        }
    }
}