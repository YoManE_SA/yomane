﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.DebitCompanies
{
	public partial class Filter : Controls.TemplateControlBase
	{
		protected override void OnLoad(EventArgs e)
		{
			TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
            base.OnLoad(e);
		}

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
		{
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Bll.DebitCompanies.DebitCompany.SearchFilters();
            sf.ID = rngID.Value;
            if (apAccount.Value.HasValue) sf.ID = new Infrastructure.Range<int?>(apAccount.Value);
            sf.IsActive = ddlIsActive.BoolValue;
            TemplatePage.SetFilter(sf);
        }
    }
}