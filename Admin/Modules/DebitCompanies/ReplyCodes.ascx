﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReplyCodes.ascx.cs" Inherits="Netpay.Admin.Modules.DebitCompanies.ReplyCodes" %>


<admin:FormSections runat="server" Title="Reply Codes" Flexible="false">
    <NP:UpdatePanel runat="server" ID="upReplyCodesList" RenderMode="Block" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:MultiView runat="server" ActiveViewIndex='<%# (ReplyCodesList.Count() > 0) ? 0 : 1 %>'>
                <asp:View runat="server">

                    <asp:MultiView ID="FilterMultiView" runat="server">

                        <asp:View runat="server">
                            Filters:
                            <NP:Button CssClass="btn btn-default btn-xs" runat="server" Text='Show all Reject-Types' CommandName="FilterShowAll" OnCommand="GeneralCommand" CommandArgument='<%#DebitCompanyItem.DebitCompanyID%>' />
                        </asp:View>

                        <asp:View runat="server">
                            Filters: Click in the table on a Reject-Type value to filter by it
                        </asp:View>

                    </asp:MultiView>
                    <hr />

                    <div class="scroll-bar">
                        <NP:PagedRepeater runat="server" ID="rptReplyCodesList" PageSize="20">
                            <HeaderTemplate>
                                <div class="table-responsive">
                                    <asp:PlaceHolder Visible="false" runat="server">
                                        <div>
                                            <asp:Literal runat="server" ID="ltId" Text='<%# Eval("ID") %>' />
                                        </div>
                                    </asp:PlaceHolder>
                                       <table class="table table-hover table-customize">
                                        <thead>
                                            <th class="text-nowrap">Actions</th>
                                            <th class="text-nowrap">Code</th>
                                            <th class="text-nowrap">Description</th>
                                            <th class="text-nowrap">Block CC:</th>
                                            <th class="text-nowrap">Block Email:</th>
                                            <th class="text-nowrap">Apply Fee:</th>
                                            <th class="text-nowrap">Charge:</th>
                                            <th class="text-nowrap">Series:</th>
                                            <th class="text-nowrap">Cascaded:</th>
                                        </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tbody>
                                    <td class="text-nowrap">
                                        <NP:Button runat="server" Text="Edit" CssClass="btn btn-xs btn-success text-uppercase" RegisterAsync="true" CommandName="EditReplyCode" OnCommand="GeneralCommand" CommandArgument='<%#Eval("ID")%>' />
                                        <NP:Button runat="server" CssClass="btn btn-default btn-xs text-uppercase" Text='<%#(Netpay.CommonTypes.FailSource)Convert.ToInt32(Eval("FailSource"))%>' CommandName="FilterFailSource" OnCommand="GeneralCommand" CommandArgument='<%#Eval("FailSource")+";"+Eval("DebitCompanyID")%>' />
                                    </td>
                                    <td><%# Eval("Code") %></td>
                                    <td><%# Eval("Description") %></td>
                                    <td><span class="badge"><%# (bool)Eval("BlockCC")?"Yes":"No"%></span></td>
                                    <td><span class="badge"><%# (bool)Eval("BlockMail")?"Yes":"No"%></span></td>
                                    <td><span class="badge"><%# (bool)Eval("ChargeFail")?"Yes":"No"%></span></td>
                                    <td><span class="badge"><%# Eval("RecurringAttemptsCharge") %></span></td>
                                    <td><span class="badge"><%# Eval("RecurringAttemptsSeries") %></span></td>
                                    <td><span class="badge"><%# Eval("IsCascade") %></span></td>
                                   
                                </tbody>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                                </div>
                            </FooterTemplate>
                        </NP:PagedRepeater>
                    </div>
                </asp:View>
                <asp:View runat="server">
                    <div class="alert alert-info">
                        No records found
                    </div>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
    </NP:UpdatePanel>
</admin:FormSections>


<admin:ModalDialog runat="server" ID="dlgEditReplyCode" Title="Edit Reply Code" OnCommand="GeneralCommand">
    <Body>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    Local Error
             <netpay:DropDownBase ID="LocalErrorDropDown" TabIndex="1" CssClass="form-control" runat="server" DataSource="<%# ItemData.LocalErrorList %>" DataValueField="Value" DataTextField="Key" Value="<%#ItemData.LocalError%>" />
                </div>
                <div class="form-group">
                    Merchant Desc Eng
             <asp:TextBox runat="server" ID="DescriptionMerchantEngInput" TabIndex="3" CssClass="form-control" Text='<%# ItemData.DescriptionMerchantEng%>' MaxLength="550" />
                </div>
                <div class="form-group">
                    Customer Desc Eng
             <asp:TextBox runat="server" ID="DescriptionCustomerEngInput" TabIndex="5" CssClass="form-control" Text='<%#ItemData.DescriptionCustomerEng%>' MaxLength="550" />
                </div>
                <div class="form-group">
                    Recurring Attempts Series
              <asp:TextBox runat="server" Type="text" ID="RecurringAttemptsSeriesInput" TabIndex="7" CssClass="form-control" Text='<%# ItemData.RecurringAttemptsSeries%>' CausesValidation="True" />
                    <asp:RequiredFieldValidator runat="server" ForeColor="red" Display="Dynamic" ControlToValidate="RecurringAttemptsSeriesInput" ErrorMessage="This field is required." SetFocusOnError="True" />
                    <asp:RangeValidator runat="server" ForeColor="red" Display="Dynamic" ErrorMessage="Please insert a number between 0 to 100." ControlToValidate="RecurringAttemptsSeriesInput" MaximumValue="100" MinimumValue="0" Type="Integer" SetFocusOnError="True" />
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    Reject Type
            <netpay:EnumDropDown ID="RejectTypeEnumInput" TabIndex="2" CssClass="form-control" runat="server" ViewEnumName="Netpay.CommonTypes.FailSource" Value="<%#Convert.ToInt32(ItemData.FailSource)%>" EnableBlankSelection="False" />
                </div>
                <div class="form-group">
                    Merchant Desc Heb
             <asp:TextBox runat="server" ID="DescriptionMerchantHebInput" TabIndex="4" CssClass="form-control" Text='<%# ItemData.DescriptionMerchantHeb %>' MaxLength="550" />
                </div>
                <div class="form-group">
                    Customer Desc Heb
             <asp:TextBox runat="server" ID="DescriptionCustomerHebInput" TabIndex="6" CssClass="form-control" Text='<%# ItemData.DescriptionCustomerHeb%>' MaxLength="550" />
                </div>
                <div class="form-group">
                    Recurring Attempts Charge
              <asp:TextBox runat="server" ID="RecurringAttemptsChargeInput" CssClass="form-control" TabIndex="8" Text='<%# ItemData.RecurringAttemptsCharge%>' />
                    <asp:RequiredFieldValidator runat="server" ForeColor="red" Display="Dynamic" ControlToValidate="RecurringAttemptsChargeInput" ErrorMessage="This field is required." SetFocusOnError="True" />
                    <asp:RangeValidator runat="server" ForeColor="red" Display="Dynamic" ErrorMessage="Please insert a number between 0 to 100." ControlToValidate="RecurringAttemptsChargeInput" MaximumValue="100" MinimumValue="0" Type="Integer" SetFocusOnError="True" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <ul class="list-group">
                    <li class="list-group-item">
                        <asp:CheckBox ID="BlockCreditCardCheckBox" AutoPostBack="False" TabIndex="9" Text="Block Credit Card" runat="server" Checked='<%#ItemData.BlockCC%>' /></li>
                    <li class="list-group-item">
                        <asp:CheckBox ID="BlockEmailAddressCheckBox" AutoPostBack="False" TabIndex="10" Text="Block Email Address" runat="server" Checked='<%#ItemData.BlockMail%>' /></li>
                </ul>
            </div>
            <div class="col-lg-6">
                <ul class="list-group">
                    <li class="list-group-item">
                        <asp:CheckBox ID="ApplyFeeCheckBox" AutoPostBack="False" TabIndex="11" Text="Apply Fee" runat="server" Checked='<%#ItemData.ChargeFail%>' /></li>
                    <li class="list-group-item">
                        <asp:CheckBox ID="ContinueNextCascadedTermianlCheckBox" TabIndex="12" AutoPostBack="False" Text="Continue next cascaded terminal" runat="server" Checked='<%#ItemData.IsCascade%>' /></li>
                </ul>
            </div>
        </div>
    </Body>
    <Footer>
        <NP:Button runat="server" Text="Update" CssClass="btn btn-primary" TabIndex="13" CausesValidation="true" CommandName="UpdateReplyCode" OnCommand="GeneralCommand" CommandArgument='<%#ItemData.ID%>' />
    </Footer>

</admin:ModalDialog>



