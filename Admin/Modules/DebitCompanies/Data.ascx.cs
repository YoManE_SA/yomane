﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;

namespace Netpay.Admin.DebitCompanies
{
	public partial class Data : Controls.AccountControlBase
	{
		protected Netpay.Bll.DebitCompanies.DebitCompany ItemData;
		protected override void OnLoad(EventArgs e)
		{
			TemplatePage.PageController.SaveItem += PageController_SaveItem;
			base.OnLoad(e);
		}

		protected override void DataBindChildren()
		{
			ItemData = Account as Netpay.Bll.DebitCompanies.DebitCompany;
			if (ItemData == null) ItemData = new Bll.DebitCompanies.DebitCompany();
			base.DataBindChildren();
		}

		protected void PageController_SaveItem(object sender, EventArgs e)
		{
			ItemData = Account as Netpay.Bll.DebitCompanies.DebitCompany;
			if (ItemData == null)  //ItemData = new Bll.DebitCompanies.DebitCompany() { UnblockAttemptString="" };
            throw new Exception("Unable to add new debit Company from app");

            ItemData.Name = txtName.Text;
			ItemData.Description = txtDescription.Text;
			ItemData.EmergencyContact = txtEmergencyContact.Text;
			ItemData.EmergencyPhone = txtEmergencyPhone.Text;

			ItemData.IsActive = chkIsActive.Checked;
			ItemData.IsBlocked = chkIsBlocked.Checked;
			//ItemData.IsDynamicDescriptor = chkIsDynamicDescriptor.Checked;
			ItemData.IsReturnCode = chkIsReturnCode.Checked;
			//ItemData.IsAllowPayoutWithoutTransfer = chkIsAllowPayoutWithoutTransfer.Checked;
			ItemData.IsAllowRefund = chkIsAllowRefund.Checked;
			ItemData.IsAllowPartialRefund = chkIsAllowPartialRefund.Checked;
			ItemData.IsAutoRefund = chkIsAutoRefund.Checked;
			ItemData.IsNotifyRetReqAfterRefund = chkIsNotifyRetReqAfterRefund.Checked;
			/*
			ItemData.LinkRefID = txtLinkRefID.Text;
			ItemData.LegalName = txtLegalName.Text;
			ItemData.LegalNumber = txtLegalNumber.Text;
			ItemData.PaymentPayeeName = txtPaymentPayeeName.Text;
		
			ItemData.EmailAddress = txtEmailAddress.Text;
			ItemData.UserName = txtUserName.Text;
			*/
			//ItemData.PersonalAddress = caAddress.Save(ItemData.PersonalAddress);
			ItemData.Save();
		}
	}
}