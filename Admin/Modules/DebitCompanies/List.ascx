﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.DebitCompanies.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Debit Companies">
    <Header>
        <admin:LegendColors runat="server" Items='<%# Netpay.Bll.DebitCompanies.DebitCompany.StatusColor  %>' ItemsText='<%# Netpay.Bll.DebitCompanies.DebitCompany.StatusText %>' FloatRight="true" />
    </Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" ID="rptList" SaveAjaxState="true" DataKeyNames="AccountID" AutoGenerateColumns="false" BubbleLoadEvent="true">
                <Columns>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <span style="background-color: <%# Netpay.Infrastructure.Enums.BoolColorMap.MapHtmlColor((bool)Eval("IsActive")) %>;" class="legend-item"></span>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ID" SortExpression="ID">
                        <ItemTemplate>
					        <%# Eval("AccountID") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField HeaderText="Name" DataField="Name" SortExpression="Name" />

                    <asp:TemplateField HeaderText="Rules(All)">
                        <ItemTemplate>
                            <%# Netpay.Bll.DebitCompanies.DebitCompanyRule.GetRulesByDebitCompanyId((int)Eval("ID")).Where(x => x.IsActive == true).Count() %>/<%# Netpay.Bll.DebitCompanies.DebitCompanyRule.GetRulesByDebitCompanyId((int)Eval("ID")).Count() %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Reply Codes">
                        <ItemTemplate>
                            <asp:Label runat="server" Text='<%#Netpay.Bll.DebitCompanies.ResponseCode.IsAnyReplyCodeForDebitCompany((int)Eval("ID"))? "Exist" : "Not Exist" %>'> </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd="false" />
    </Footer>
</admin:ListSection>
