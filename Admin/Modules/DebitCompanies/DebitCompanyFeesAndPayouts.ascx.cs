﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Bll.DebitCompanies;
using Netpay.CommonTypes;
using Netpay.Infrastructure;

namespace Netpay.Admin.Modules.DebitCompanies
{
    public partial class DebitCompanyFeesAndPayouts : Controls.AccountControlBase
    {
        
        public bool IsItTerminalPage
        {
            get
            {
                return ((TemplatePage is Controls.DataTablePage)&&!(TemplatePage is Controls.AccountPage));
            }
        }

        public bool IsItNewItemPage
        {
            get
            {
                {
                    return (TemplatePage.ItemID == 0 || TemplatePage.ItemID == null);
                }
            }
        }
                
        public Bll.DebitCompanies.DebitCompany DebitCompanyItem { get; set; }

        //This list can be list for Terminal fees , Or list for DebitCompany fees,
        //Depands on the page loaded.
        public DebitCompanyFee DebitCompanyFeeItem { get; set; }

        public List<DebitCompanyFee> DebitCompanyFeesList { get; set; }
        
        //Array of enum's PaymentMethodType
        public PaymentMethodType[] PaymentMethodTypes = new PaymentMethodType[]
        {
            PaymentMethodType.CreditCard,
            PaymentMethodType.ECheck,
            PaymentMethodType.BankTransfer,
            PaymentMethodType.PhoneDebit,
            PaymentMethodType.Wallet,
            PaymentMethodType.ExternalWallet
        };
        
        protected override void OnLoad(EventArgs e)
        {
            DebitCompanyFeeItem = new DebitCompanyFee();
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            var check = TemplatePage.GetItemData<Bll.DebitCompanies.Terminal>();
            if (check is Bll.DebitCompanies.Terminal && check.ID!=0)
            {
                var terminal = Bll.DebitCompanies.Terminal.Load(check.ID);
                DebitCompanyItem = Bll.DebitCompanies.DebitCompany.LoadDebitCompanyById(terminal.DebitCompany);

                DebitCompanyFeesList = DebitCompanyFee.GetFeesListByTerminalNumber(terminal.TerminalNumber);
                if (DebitCompanyFeesList != null)
                {
                    rptDebitCompanyFeesList.DataSource = DebitCompanyFeesList;
                    rptDebitCompanyFeesList.DataBind();
                }
            }
            else
            {
                if (TemplatePage is Admin.Controls.AccountPage)
                    DebitCompanyItem = Account as Bll.DebitCompanies.DebitCompany;
                if (DebitCompanyItem == null) DebitCompanyItem = new Bll.DebitCompanies.DebitCompany();

                DebitCompanyFeesList = DebitCompanyFee.GetFeesListByDebitCompanyId(DebitCompanyItem.ID);
                if (DebitCompanyFeesList != null)
                {
                    rptDebitCompanyFeesList.DataSource = DebitCompanyFeesList;
                    rptDebitCompanyFeesList.DataBind();
                }
            }

            base.DataBindChildren();
        }

        //The modal-dialog Dialog_Command method.
        protected void Dialog_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "AddItem":
                    int? debitcompanyid;
                    if (IsItTerminalPage)
                    {
                        var terminal = Bll.DebitCompanies.Terminal.Load((int)TemplatePage.ItemID);
                        debitcompanyid = terminal.DebitCompany;
                        if (debitcompanyid != null)
                        {
                            DebitCompanyFeeItem = new DebitCompanyFee() { DcfDebitCompanyId = (int)debitcompanyid, DcfCurrencyId = 255, DcfFixedCurrency = 255, DcfChbCurrency = 255, DcfTerminalNumber = terminal.TerminalNumber };
                            dlgEdit.BindAndShow();
                        }
                    }
                    else
                    {
                        var page = (sender as Netpay.Admin.Controls.FormSections).Page as Controls.AccountPage;
                        debitcompanyid = page.Account.DebitCompanyID;
                        if (debitcompanyid != null)
                        {
                            DebitCompanyFeeItem = new DebitCompanyFee() { DcfDebitCompanyId = (int)debitcompanyid, DcfCurrencyId = 255, DcfFixedCurrency = 255, DcfChbCurrency = 255, DcfTerminalNumber = "" };
                            dlgEdit.BindAndShow();
                        }
                    }
                    break;

                case "EditItem":
                    //Get the pressed debit fee item id:
                    int debitfeeid;
                    if (Int32.TryParse(e.CommandArgument.ToString(), out debitfeeid))
                    {
                        //load the pressed debitfee item
                        DebitCompanyFeeItem = DebitCompanyFee.Load(debitfeeid);
                        if (DebitCompanyFeeItem != null) dlgEdit.BindAndShow();
                    }
                    break;

                case "SaveItem":
                    //Get the selected object to save:
                    if (int.Parse(ModalDialogHiddenFieldFeeId.Value) == 0)
                    {
                        DebitCompanyFeeItem = new DebitCompanyFee() { DcfDebitCompanyId = ModalDialogHiddenFieldDebitCompanyId.Value.ToInt32(), DcfTerminalNumber = ModalDialogHiddenFieldTerminalNumber.Value };

                        //If it's new Fee declare the dropdowns values:
                        byte bytepaymentmethodid;
                        if (byte.TryParse(ddlPaymentMethod.Value, out bytepaymentmethodid))
                            DebitCompanyFeeItem.DcfPaymentMethod = bytepaymentmethodid;

                        byte bytecurrency;
                        if (byte.TryParse(ddlCurrncy.Value, out bytecurrency))
                            DebitCompanyFeeItem.DcfCurrencyId = bytecurrency;
                    }
                    else
                    {
                        DebitCompanyFeeItem = DebitCompanyFee.Load(ModalDialogHiddenFieldFeeId.Value.ToInt32());
                    }

                    //Inserting the data to the object
                    DebitCompanyFeeItem.DcfFixedCurrency = byte.Parse(ddlDcfFixedCurrency.Value);

                    DebitCompanyFeeItem.DcfFixedFee = DcfFixedFeeTxt.Text.ToDecimal(0);

                    DebitCompanyFeeItem.DcfMinPrecFee = DcfMinPrecFeeTxt.Text.ToDecimal(0);

                    decimal DcfMaxPrecFeeDecimalVal;
                    if (decimal.TryParse(DcfMaxPrecFeeTxt.Text, out DcfMaxPrecFeeDecimalVal))
                        DebitCompanyFeeItem.DcfMaxPrecFee = DcfMaxPrecFeeDecimalVal;
                    else
                        DebitCompanyFeeItem.DcfMaxPrecFee = null;

                    DebitCompanyFeeItem.DcfPercentFee = DcfMaxPrecFeeTxt.Text.ToDecimal(0);

                    DebitCompanyFeeItem.DcfApproveFixedFee = DcfApproveFixedFeeTxt.Text.ToDecimal(0);

                    DebitCompanyFeeItem.DcfRefundFixedFee = DcfRefundFixedFeeTxt.Text.ToDecimal(0);

                    DebitCompanyFeeItem.DcfClarificationFee = DcfClarificationFeeTxt.Text.ToDecimal(0);

                    DebitCompanyFeeItem.DcfFailFixedFee = DcfFailFixedFeeTxt.Text.ToDecimal(0);

                    //check this drop down when the database should get null !
                    DebitCompanyFeeItem.DcfChbCurrency = byte.Parse(ddlDcfChbCurrency.Value);

                    DebitCompanyFeeItem.DcfCbFixedFee = DcfCbFixedFeeTxt.Text.ToDecimal(0);

                    DebitCompanyFeeItem.DcfPayTransDays = DcfPayTransDaysTxt.Text;

                    DebitCompanyFeeItem.DcfPayInDays = DcfPayInDaysTxt.Text;

                    DebitCompanyFeeItem.Save();

                    //Refresh the page
                    dlgEdit.RegisterHide();
                    if (IsItTerminalPage)
                    { DebitCompanyFeesList = DebitCompanyFee.GetFeesListByTerminalNumber(DebitCompanyFeeItem.DcfTerminalNumber); }
                    else
                    { DebitCompanyFeesList = DebitCompanyFee.GetFeesListByDebitCompanyId(ModalDialogHiddenFieldDebitCompanyId.Value.ToInt32()); }
                    rptDebitCompanyFeesList.DataSource = DebitCompanyFeesList;
                    rptDebitCompanyFeesList.DataBind();
                    FeesAndPayoutsFormSection.DataBind();
                    upFeesAndPayoutsList.Update();
                    break;

                case "DeleteItem":
                    string terminalnumberref;
                    int debitcompanyidref;
                    DebitCompanyFeeItem = DebitCompanyFee.Load(e.CommandArgument.ToString().ToInt32());
                    if (DebitCompanyFeeItem != null)
                    {
                        terminalnumberref = DebitCompanyFeeItem.DcfTerminalNumber;
                        debitcompanyidref = DebitCompanyFeeItem.DcfDebitCompanyId;
                        DebitCompanyFeeItem.Delete();


                        if (IsItTerminalPage) { DebitCompanyFeesList = DebitCompanyFee.GetFeesListByTerminalNumber(terminalnumberref); }
                        else { DebitCompanyFeesList = DebitCompanyFee.GetFeesListByDebitCompanyId(debitcompanyidref); }
                        rptDebitCompanyFeesList.DataSource = DebitCompanyFeesList;
                        rptDebitCompanyFeesList.DataBind();
                        FeesAndPayoutsFormSection.DataBind();
                        upFeesAndPayoutsList.Update();
                    }
                    break;
            }
        }

        public string GetCurrencyTitle(object dcffixedcurrency)
        {
            if (dcffixedcurrency == null) return "NULL";
            else
            {
                byte currencyid = (byte)dcffixedcurrency;

                if (currencyid == 255) return "[Native]";
                else return Netpay.Bll.Currency.Get(currencyid).IsoCode;
            }
        }

        public bool CheckIfEnableAdd()
        {
            return ((TemplatePage.Page is Controls.AccountPage || TemplatePage.Page is Controls.DataTablePage)
                    && TemplatePage.ItemID != null
                    && TemplatePage.ItemID != 0);
        }
    }
}