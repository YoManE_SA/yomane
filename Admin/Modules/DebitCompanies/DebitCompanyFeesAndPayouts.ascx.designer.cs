﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Netpay.Admin.Modules.DebitCompanies {
    
    
    public partial class DebitCompanyFeesAndPayouts {
        
        /// <summary>
        /// FeesAndPayoutsFormSection control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Netpay.Admin.Controls.FormSections FeesAndPayoutsFormSection;
        
        /// <summary>
        /// MultiViewGlobal control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.MultiView MultiViewGlobal;
        
        /// <summary>
        /// ViewIfDebitCompanyNotNew control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.View ViewIfDebitCompanyNotNew;
        
        /// <summary>
        /// upFeesAndPayoutsList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Netpay.Admin.Controls.GlobalControls.UpdatePanel upFeesAndPayoutsList;
        
        /// <summary>
        /// FeesAndPayoutsMultiView control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.MultiView FeesAndPayoutsMultiView;
        
        /// <summary>
        /// ViewIfFeeExist control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.View ViewIfFeeExist;
        
        /// <summary>
        /// rptDebitCompanyFeesList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Netpay.Admin.Controls.GlobalControls.PagedRepeater rptDebitCompanyFeesList;
        
        /// <summary>
        /// ViewIfFeeNotExist control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.View ViewIfFeeNotExist;
        
        /// <summary>
        /// ViewIfDebitCompanyNew control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.View ViewIfDebitCompanyNew;
        
        /// <summary>
        /// NewItemMessage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal NewItemMessage;
        
        /// <summary>
        /// Literal1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal Literal1;
        
        /// <summary>
        /// dlgEdit control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Netpay.Admin.Controls.ModalDialog dlgEdit;
        
        /// <summary>
        /// ModalDialogHiddenFieldFeeId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField ModalDialogHiddenFieldFeeId;
        
        /// <summary>
        /// ModalDialogHiddenFieldDebitCompanyId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField ModalDialogHiddenFieldDebitCompanyId;
        
        /// <summary>
        /// ModalDialogHiddenFieldTerminalNumber control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField ModalDialogHiddenFieldTerminalNumber;
        
        /// <summary>
        /// LabelOne control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelOne;
        
        /// <summary>
        /// ddlPaymentMethod control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Netpay.Web.Controls.PaymentMethodDropDown ddlPaymentMethod;
        
        /// <summary>
        /// ddlCurrncy control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Netpay.Web.Controls.CurrencyDropDown ddlCurrncy;
        
        /// <summary>
        /// LabelTwo control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelTwo;
        
        /// <summary>
        /// ddlDcfFixedCurrency control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Netpay.Web.Controls.CurrencyDropDown ddlDcfFixedCurrency;
        
        /// <summary>
        /// DcfFixedFeeTxt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox DcfFixedFeeTxt;
        
        /// <summary>
        /// DcfMinPrecFeeTxt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox DcfMinPrecFeeTxt;
        
        /// <summary>
        /// DcfMaxPrecFeeTxt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox DcfMaxPrecFeeTxt;
        
        /// <summary>
        /// DcfPercentFeeTxt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox DcfPercentFeeTxt;
        
        /// <summary>
        /// DcfApproveFixedFeeTxt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox DcfApproveFixedFeeTxt;
        
        /// <summary>
        /// DcfRefundFixedFeeTxt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox DcfRefundFixedFeeTxt;
        
        /// <summary>
        /// DcfClarificationFeeTxt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox DcfClarificationFeeTxt;
        
        /// <summary>
        /// DcfFailFixedFeeTxt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox DcfFailFixedFeeTxt;
        
        /// <summary>
        /// LabelThree control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelThree;
        
        /// <summary>
        /// ddlDcfChbCurrency control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Netpay.Web.Controls.CurrencyDropDown ddlDcfChbCurrency;
        
        /// <summary>
        /// DcfCbFixedFeeTxt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox DcfCbFixedFeeTxt;
        
        /// <summary>
        /// LabelFour control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelFour;
        
        /// <summary>
        /// DcfPayTransDaysTxt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox DcfPayTransDaysTxt;
        
        /// <summary>
        /// DcfPayInDaysTxt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox DcfPayInDaysTxt;
    }
}
