﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Finanace
{
	public partial class List : Controls.TemplateControlBase
	{
		protected override void OnLoad(EventArgs e)
		{
			rptList.DataBinding += List_DataBinding;
			base.OnLoad(e);
		}

		private void List_DataBinding(object sender, EventArgs e)
		{
			rptList.DataSource = Bll.Finance.Settlement.Search(TemplatePage.GetFilter<Bll.Finance.Settlement.SearchFilters>(), rptList);
		}
	}
}