﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpcommingList.ascx.cs" Inherits="Netpay.Admin.Finanace.UpcommingList" %>
<admin:AdminList runat="server" ID="rptList" DataKeyNames="AccountID" AutoGenerateColumns="false" BubbleLoadEvent="true">
	<Columns>
		<asp:BoundField HeaderText="acceptorName" DataField="acceptorName" SortExpression="acceptorName" />
		<asp:BoundField HeaderText="TotalAmount" DataField="TotalAmount" SortExpression="TotalAmount" />
		<asp:BoundField HeaderText="Currency" DataField="Currency" SortExpression="Currency" />
		<asp:BoundField HeaderText="TotalCount" DataField="TotalCount" SortExpression="TotalCount" />
		<asp:BoundField HeaderText="LastSettlementDate" DataField="LastSettlementDate" SortExpression="LastSettlementDate" />
	</Columns>
</admin:AdminList>