﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Finanace
{
	public partial class AccountFloorSettings : Admin.Controls.AccountControlBase
	{
		protected override void OnLoad(EventArgs e)
		{
			TemplatePage.PageController.SaveItem += PageController_SaveItem;
			base.OnLoad(e);
		}

		private void PageController_SaveItem(object sender, EventArgs e)
		{
			var currentItems = Bll.Finance.AccountFloorSettings.Search(AccountID.GetValueOrDefault());
			foreach (RepeaterItem item in rptFloors.Items)
			{
				var dataItem = currentItems.Where(i => i.ID == (item.FindControl("hfID") as HiddenField).Value.ToNullableInt().GetValueOrDefault()).SingleOrDefault();
				if (dataItem == null) dataItem = new Bll.Finance.AccountFloorSettings(AccountID.GetValueOrDefault());
				dataItem.Title = (item.FindControl("ltText") as Literal).Text;
				dataItem.CurrencyISOCode = (item.FindControl("ltCurrencyIso") as Literal).Text;

				var currentFees = dataItem.Fees.ToList();
				dataItem.Fees.Clear();
				foreach (RepeaterItem vItem in (item.FindControl("rptItems") as Repeater).Items) {
					var feeItem = currentFees.Where(i => i.ID == (vItem.FindControl("hfID") as HiddenField).Value.ToNullableInt().GetValueOrDefault()).SingleOrDefault();
					feeItem.AmountTop = (vItem.FindControl("txtAmountTop") as TextBox).Text.ToDecimal(0);
					feeItem.FixedAmount = (vItem.FindControl("txtFixedAmount") as TextBox).Text.ToNullableDecimal();
					feeItem.PercentValue = (vItem.FindControl("txtPercentValue") as TextBox).Text.ToNullableDecimal();
					dataItem.Fees.Add(feeItem);
				}
				//dataItem.Save();
			}
		}

		protected override void DataBindChildren()
		{
			rptFloors.DataSource = Bll.Finance.AccountFloorSettings.Search(AccountID.GetValueOrDefault());
			base.DataBindChildren();
		}

		protected void Add_Click(object sender, EventArgs e)
		{
			var item = new Bll.Finance.AccountFloorSettings(AccountID.GetValueOrDefault());
			item.CurrencyISOCode = ddlCurrency.SelectedCurrencyIso;
			item.Title = txtTitle.Text;
			while (item.Fees.Count < 6) item.Fees.Add(new Bll.Finance.AccountFloorSettings.FloorFee(item.ID));
			rptFloors.AddItem(item);
		}
		
		protected void Floors_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			var rptItems = e.Item.FindControl("rptItems") as Repeater;
			rptItems.DataSource = (e.Item.DataItem as Netpay.Bll.Finance.AccountFloorSettings).Fees;
		}

		protected void Items_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			var rptItems = (source as Web.Controls.DynamicRepeater);
			if (e.CommandName == "Delete") rptItems.RemoveItem(e.Item.ItemIndex);
		}

		protected void Floors_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			if (e.CommandName == "Delete") rptFloors.RemoveItem(e.Item.ItemIndex);
		}
	}
}