﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="AccountSettings.ascx.cs" Inherits="Netpay.Admin.Finanace.AccountSettings" %>
<admin:FormSections runat="server" Title="Settlement Templates">

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <asp:Button ID="btnAddSettings" runat="server" Text="Add Settlement Template" CssClass="btn btn-primary" CommandName="SettingsAdd" OnCommand="Button_OnCommand" />
                </div>
            </div>
        </div>
    </div>

</admin:FormSections>


<netpay:DynamicRepeater runat="server" ID="rptItems" OnItemCommand="Button_OnCommand">
    <ItemTemplate>
        <admin:FormSections runat="server" Title="Settlement Templates">
            <div class="row">
                <div class="col-md-12">
                    <asp:CheckBox ID="chkEnabled" runat="server" Text="Enabled" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        Currency 
                                <netpay:CurrencyDropDown ID="ddlCurrency" runat="server" CssClass="form-control" />
                    </div>
                    <div class="form-group">
                        <asp:Label runat="server" Text="Initial" />
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtSettleInitialHoldDays" />
                    </div>
                    <div class="form-group">
                        <asp:Label runat="server" Text="Payout" />
                        <asp:TextBox runat="server" ID="txtMinPayoutAmount" CssClass="form-control" />
                    </div>

                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <asp:Label runat="server" Text="Hold Margin" />
                        <asp:TextBox ID="txtSettleHoldDays" CssClass="form-control" runat="server" />
                    </div>
                    <div class="form-group">
                        <asp:Label runat="server" Text="Settlement" />
                        <asp:TextBox ID="txtMinSettlementAmount" CssClass="form-control" runat="server" />
                    </div>
                    <div class="form-group">
                        <asp:Label runat="server" Text="Wire Fee Amount" />
                        <asp:TextBox runat="server" ID="txtWireFeeAmount" CssClass="form-control" />
                    </div>

                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        Wire Fee Percent %
                                <asp:TextBox runat="server" ID="txtWireFeePercent" CssClass="form-control" />
                    </div>
                    <div class="form-group">
                        Currency 
                                <netpay:CurrencyDropDown runat="server" ID="ddlWireCurrencyIsoCode" CssClass="form-control" />

                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label1" runat="server" Text="Storage Fee" />
                        <asp:TextBox runat="server" ID="txtStorageFeeAmount" CssClass="form-control" />
                    </div>
                </div>
                <div class="col-md-3">

                    <div class="form-group">
                        Currency Iso Code
                                <netpay:CurrencyDropDown runat="server" ID="ddlStorageFeeCurrencyIsoCode" CssClass="form-control" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Restrictions</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        Currencies:
                                <asp:TextBox ID="txtCurrencies" CssClass="form-control" runat="server" />
                                    </div>
                                    <div class="form-group">
                                        Accounts:
                                <asp:TextBox ID="txtAccounts" CssClass="form-control" runat="server" />
                                    </div>
                                    <div class="form-group">
                                        PaymentMethods:
                                <asp:TextBox ID="txtPaymentMethods" CssClass="form-control" runat="server" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        Countries Groups:
                                <asp:TextBox ID="txtAccount" CssClass="form-control" runat="server" />
                                    </div>
                                    <div class="form-group">
                                        Countries:
                                <asp:TextBox ID="txtCountries" CssClass="form-control" runat="server" />
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="AddFee" CssClass="btn btn-cons-short btn-default" CommandName="TransactionFeeAdd" />
                    </div>
                </div>
            </div>
            <netpay:DynamicRepeater runat="server" ID="rptFees">
                <HeaderTemplate>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>AmountType</th>
                                <th nowrap>Fee Curency</th>
                                <th nowrap>Use Floor Fees</th>
                                <th>Fixed</th>
                                <th>Percent</th>
                                <th>Source Amount</th>
                                <th>Settle Percent</th>
                            </tr>
                        </thead>
                </HeaderTemplate>
                <ItemTemplate>
                    <tbody>
                        <tr>
                            <td>
                                <netpay:EnumDropDown runat="server" ID="EnumDropDown1" ViewEnumName="Netpay.Bll.Finance.AmountType" CssClass="form-control" EnableBlankSelection="false" /></td>
                            <td>
                                <netpay:CurrencyDropDown runat="server" ID="ddlCurrency" BlankSelectionText="[Source]" CssClass="form-control" /></td>
                            <td>
                                <asp:DropDownList ID="ddlFloorId" runat="server" Width="80px" DataTextField="Title" CssClass="form-control" DataValueField="SetTransactionFloor_id" AppendDataBoundItems="true">
                                    <asp:ListItem Value="" />
                                </asp:DropDownList></td>
                            <td>
                                <asp:TextBox ID="txtFixedAmount" runat="server" Text='<%# Eval("FixedAmount", "{0:0.00}") %>' CssClass="form-control" Width="60" />
                            </td>
                            <td>
                                <div class="input-group">
                                    <span class="input-group-addon">%</span></span>
                    	<asp:TextBox ID="txtPercentValue" runat="server" Text='<%# Eval("PercentValue", "{0:0.000}") %>' CssClass="form-control" />
                                </div>
                                </div>
                            </td>
                            <td>
                                <netpay:EnumDropDown runat="server" ID="ddlCalcMethod" ViewEnumName="Netpay.Bll.Finance.FeeCalcMethod" EnableBlankSelection="false" CssClass="form-control" /></td>
                            <td>
                                <div class="input-group">
                                    <span class="input-group-addon">% </span></span>
                    	    <asp:TextBox ID="txtSettlementPercentValue" runat="server" Text='<%# Eval("SettlementPercentValue", "{0:0.00}") %>' CssClass="form-control" />
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </netpay:DynamicRepeater>
        </admin:FormSections>
    </ItemTemplate>
</netpay:DynamicRepeater>
