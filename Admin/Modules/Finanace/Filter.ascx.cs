﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Finanace
{
	public partial class Filter : Controls.TemplateControlBase
	{
		protected override void OnLoad(EventArgs e)
		{
			TemplatePage.PageController.Search += PageController_Search;
			base.OnLoad(e);
		}

		private void PageController_Search(object sender, EventArgs e)
		{
			var sf = new Netpay.Bll.Finance.Settlement.SearchFilters();
			if (apAccount.Value.ToNullableInt() != null) sf.AccountID = new List<int>() { apAccount.Value.GetValueOrDefault() };
			sf.ID = rngID.Value;
			sf.InsertDate = rngInsertDate.Value;
			sf.SettlementDate = rngSettlementDate.Value;
			
			sf.WireTotal = rngWireTotal.Value;
			sf.WireTotal = rngInvoiceTotal.Value;
			TemplatePage.SetFilter(sf);
		}
	}
}