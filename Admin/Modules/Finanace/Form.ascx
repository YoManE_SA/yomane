﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Form.ascx.cs" Inherits="Netpay.Admin.Finanace.Form" %>
<admin:FormSections runat="server" Title="Settlement">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        Settlement: #
    <asp:Literal runat="server" ID="ltSettlementId" Text='<%# Item.ID %>' />
                        (<a target="_blank" href="../include/common_totalTransShow.asp?LNG=1&payID=<%# Item.TransactionPay_id %>"><%# Item.TransactionPay_id %></a>)
                    </div>
                    <div class="form-group">
                        Currency: <%# Item.SettlementCurrencyISOCode %>
                    </div>
                    <div class="form-group">
                        To: <%# Item.PayeeName %>
                    </div>
                    <div class="form-group">
                        Settlement Date: <%# Item.SettlementDate.ToString("d") %>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        Settlement Total: <%# formatCurrency(Item.SettlementTotal) %>
                    </div>
                    <div class="form-group">
                        Invoice Number: <%# Item.InvoiceNumber %>
                    </div>
                    <div class="form-group">
                        Invoice Total: <%# formatCurrency(Item.InvoiceTotal, Item.InvoiceCurrencyISOCode)%>
                    </div>
                    <div class="form-group">
                        Invoice Tax: <%# formatCurrency(Item.InvoiceVATPercent, Item.InvoiceCurrencyISOCode)%>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        Wire ID:
                    </div>
                    <div class="form-group">
                        Wire ID:
                    </div>
                    <div class="form-group">
                        Wire Fee: <%# formatCurrency(Item.WireFee, Item.WireCurrencyISOCode) %>
                    </div>
                    <div class="form-group">
                        Wire Total: <%# formatCurrency(Item.WireTotal, Item.WireCurrencyISOCode) %>
                    </div>
                </div>
            </div>

            <div class="row">
            </div>

        </div>
    </div>

    <h4>Items</h4>
    <table class="table">
        <thead>
            <tr>
                <th>Item</th>
                <th>Value</th>
                <th>Amount</th>
                <th>Quantity</th>
                <th>Total</th>
            </tr>
        </thead>
        <asp:Repeater runat="server" ID="rptGroups">
            <ItemTemplate>
                <tr>
                    <td colspan="5" style="border-bottom: 1px solid #c0c0c0; font-weight: bold;"><%# Eval("Key") ?? "Other" %></td>
                </tr>
                <asp:Repeater runat="server" ID="rptItems" DataSource='<%# Container.DataItem %>'>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <%# Eval("LineText") %>
                                <%# (bool)Eval("IsFee") ? " Fee": "" %>
                            </td>
                            <td>
                                <%# Eval("PercentValue", "{0:0.00}% Of") %>
                            </td>
                            <td><%# formatAmountField(Container.DataItem as Netpay.Bll.Finance.SettlementItem) %></td>
                            <td><%# Eval("Quantity") %></td>
                            <td><%# formatCurrency((decimal?)Eval("Total")) %></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <tr>
                    <td colspan="3"></td>
                    <td colspan="2" style="border-top: 1px dashed #c0c0c0; text-align: right; direction: ltr; font-weight: bold;">
                        <asp:Literal runat="server" ID="ltGroupTotoal" /></td>
                </tr>
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
        <asp:PlaceHolder runat="server" ID="phEdit" Visible="false">
            <tr>
                <td>
                    <asp:TextBox runat="server" ID="txtLineText" Width="350px" /></td>
                <td></td>
                <td style="text-align: right; direction: ltr;">
                    <asp:TextBox runat="server" ID="txtAmount" Width="40px" /></td>
                <td style="text-align: right; direction: ltr;">
                    <asp:TextBox runat="server" ID="txtQuantity" Width="40px" /></td>
                <td style="text-align: right; direction: ltr;">
                    <asp:Button runat="server" ID="btnAddItem" Text="Add Line" OnClick="AddLine_Click" /></td>
            </tr>
        </asp:PlaceHolder>
        <!--Start Total-->
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <th colspan="5">Total</th>
        </tr>
        <tr>
            <td colspan="4">Total Transfer</td>
            <td style="text-align: right; direction: ltr;"><%# formatCurrency(TotalTransfer) %></td>
        </tr>
        <tr>
            <td colspan="4">Total Fees</td>
            <td style="text-align: right; direction: ltr;"><%# formatCurrency(TotalFees) %></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td colspan="2" style="border-top: 1px dashed #c0c0c0; text-align: right; direction: ltr; font-weight: bold;"><%# formatCurrency(TotalTransfer + TotalFees) %></td>
        </tr>
        <!--End Total-->
        <!--Start Invoice-->
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="5" style="font-weight: bold; border-bottom: 1px solid #c0c0c0;">Invoice</td>
        </tr>
        <tr id="Tr1" runat="server" visible='<%# Item.InvoiceCurrencyExchangeRate.GetValueOrDefault(1) != 1 %>'>
            <td colspan="4">Converting <%# formatCurrency(-TotalFees) %> to <%# Item.InvoiceCurrencyISOCode %> - rate <%# Item.InvoiceCurrencyExchangeRate.GetValueOrDefault(1).ToString("0.000") %></td>
            <td style="text-align: right; direction: ltr;"><%# formatCurrency(-TotalFees * Item.InvoiceCurrencyExchangeRate.GetValueOrDefault(1), Item.InvoiceCurrencyISOCode) %></td>
        </tr>
        <tr id="Tr2" runat="server" visible='<%# Item.InvoiceCurrencyExchangeRate.GetValueOrDefault(1) == 1 %>'>
            <td colspan="4">Total Fees</td>
            <td style="text-align: right; direction: ltr;"><%# formatCurrency(-TotalFees, Item.InvoiceCurrencyISOCode) %></td>
        </tr>
        <tr id="Tr3" runat="server" visible='<%# Item.InvoiceVATPercent.HasValue %>'>
            <td colspan="4">Tax (<%# Item.InvoiceVATPercent.GetValueOrDefault().ToString("0.00") %>%)</td>
            <td style="text-align: right; direction: ltr;"><%# formatCurrency(Item.InvoiceVATPercent, Item.InvoiceCurrencyISOCode) %></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td colspan="2" style="border-top: 1px dashed #c0c0c0; text-align: right; direction: ltr; font-weight: bold;"><%# formatCurrency(Item.InvoiceTotal, Item.InvoiceCurrencyISOCode) %></td>
        </tr>
        <!--End Invoice-->
        <!--Start Wire-->
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="5" style="font-weight: bold; border-bottom: 1px solid #c0c0c0;">Wire</td>
        </tr>
        <tr id="Tr4" runat="server" visible='<%# Item.WireCurrencyExchangeRate.GetValueOrDefault(1) != 1 %>'>
            <td colspan="4">Converting <%# formatCurrency(Item.SettlementTotal) %> to <%# Item.WireCurrencyISOCode %> - rate <%# Item.WireCurrencyExchangeRate.GetValueOrDefault(1).ToString("0.000") %></td>
            <td style="text-align: right; direction: ltr;"><%# formatCurrency(Item.SettlementTotal * Item.WireCurrencyExchangeRate.GetValueOrDefault(1), Item.WireCurrencyISOCode) %></td>
        </tr>
        <tr id="Tr5" runat="server" visible='<%# Item.WireCurrencyExchangeRate.GetValueOrDefault(1) == 1 %>'>
            <td colspan="4">Settlement Total</td>
            <td style="text-align: right; direction: ltr;"><%# formatCurrency(Item.SettlementTotal, Item.WireCurrencyISOCode) %></td>
        </tr>
        <tr id="Tr6" runat="server" visible='<%# Item.WireFee != 0 %>'>
            <td colspan="4">Wire Fee (<%# formatCurrency(Item.WireFeeFixedAmount, Item.WireCurrencyISOCode) %> + <%# Item.WireFeePercentValue.GetValueOrDefault().ToString("0.00") %>%)</td>
            <td style="text-align: right; direction: ltr;"><%# formatCurrency(-Item.WireFee, Item.WireCurrencyISOCode) %></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td colspan="2" style="border-top: 1px dashed #c0c0c0; text-align: right; direction: ltr; font-weight: bold;"><%# formatCurrency(Item.WireTotal, Item.WireCurrencyISOCode) %></td>
        </tr>
        <!--End Wire-->
    </table>
</admin:FormSections>
