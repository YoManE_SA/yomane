﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Finanace.Filter" %>
<admin:FilterSection runat="server" Title="Status">
    <div class="form-group">
        <asp:Label ID="lblAccount" runat="server" Text="Account" />
        <admin:AccountPicker runat="server" ID="apAccount" />
    </div>
    <div class="form-group">
        <asp:Label ID="lblWireTotal" runat="server" Text="Wire Total" />
        <netpay:CurrencyDropDown CssClass="form-control" runat="server" ID="ddlWireTotalCurrency" />
    </div>
    <div class="form-group">
        <div class="row">
            <JQ:DecimalRange runat="server" ID="rngWireTotal" />
        </div>

    </div>
    <hr />
    <div class="form-group">
        <asp:Label ID="InvoiceTotal" runat="server" Text="Invoice Amount" />
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
            <netpay:CurrencyDropDown CssClass="form-control" runat="server" ID="ddlInvoiceTotalCurrency" />
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <JQ:DecimalRange runat="server" ID="rngInvoiceTotal" />
        </div>
    </div>
    <hr />
    <div class="form-group">
        <asp:Label ID="Label1" runat="server" Text="Settlement Date Range" />
        <div class="row">
            <JQ:DateRange runat="server" ID="rngSettlementDate" />
        </div>
    </div>

    <div class="form-group">
        <asp:Label ID="lblInsertDate" runat="server" Text="Insert Date Range" />
        <div class="row">
            <JQ:DateRange runat="server" ID="rngInsertDate" />
        </div>
    </div>
    <div class="form-group">
        <asp:Label ID="lblID" runat="server" Text="ID Range" />
        <div class="row">
            <JQ:IntRange runat="server" ID="rngID" />
        </div>
    </div>
</admin:FilterSection>
