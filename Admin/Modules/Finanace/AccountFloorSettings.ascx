﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="AccountFloorSettings.ascx.cs" Inherits="Netpay.Admin.Finanace.AccountFloorSettings" %>

<admin:FormSections runat="server" Title="Account Floor Settings">

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                Name : 
                <asp:TextBox CssClass="form-control" runat="server" ID="txtTitle" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                Currency:
                <netpay:CurrencyDropDown CssClass="form-control" runat="server" ID="ddlCurrency" />
            </div>
        </div>
        <div class="col-md-4">
     
                <asp:Button ID="Button1" CssClass="btn btn-primary" runat="server" Text="Add" OnClick="Add_Click" />
          
        </div>
    </div>
   
    <netpay:DynamicRepeater runat="server" ID="rptFloors" OnItemDataBound="Floors_ItemDataBound" OnItemCommand="Floors_ItemCommand">
        <ItemTemplate>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <asp:HiddenField runat="server" ID="hfID" Value='<%# Eval("ID") %>' />
                        <asp:Literal runat="server" ID="ltText" Text='<%# Eval("Title") %>' />
                        <asp:Literal runat="server" ID="ltCurrencyIso" Text='<%# Eval("CurrencyISOCode") %>' />
                        <asp:Button ID="btnDelete" Text="Delete" CommandName="Delete" runat="server" CssClass="btn btn-danger" />
                    </div>
                </div>
            </div>
            <table class="table">
                <thead>

                    <tr>
                        <th>Amount Top</th>
                        <th>Value</th>
                        <th>Percent</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <netpay:DynamicRepeater runat="server" ID="rptItems" OnItemCommand="Items_ItemCommand">
                    <ItemTemplate>
                        <tbody>
                            <tr>
                                <td>
                                    <asp:HiddenField runat="server" ID="hfID" Value='<%# Eval("ID") %>' />
                                    <asp:TextBox ID="txtAmountTop" runat="server" Text='<%# Eval("AmountTop", "{0:0.00}") %>' CssClass="form-control" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFixedAmount" runat="server" Text='<%# Eval("FixedAmount", "{0:0.00}") %>' CssClass="form-control" />

                                </td>
                                <td>
                                    <div class="input-group">
                                        <asp:TextBox ID="txtPercentValue" runat="server" Text='<%# Eval("PercentValue", "{0:0.000}") %>' CssClass="form-control" />
                                        <span class="input-group-addon">%</span>
                                    </div>
                                </td>
                                <td>
                                    <asp:Button ID="btnDelete" Text="Delete" CssClass="btn btn-danger btn-cons-short" CommandName="Delete" runat="server" /></td>
                            </tr>
                        </tbody>
                    </ItemTemplate>
                </netpay:DynamicRepeater>
            </table>
        </ItemTemplate>
    </netpay:DynamicRepeater>
</admin:FormSections>
