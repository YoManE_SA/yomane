﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Finanace.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Settlements">
    <Header></Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" ID="rptList" DataKeyNames="ID" AutoGenerateColumns="false" BubbleLoadEvent="true">
                <Columns>
                    <asp:BoundField HeaderText="Settlement id" DataField="ID" SortExpression="Settlement_id" />
                    <asp:BoundField HeaderText="Settlement Type id" DataField="SettlementType_id" SortExpression="SettlementType_id" />
                    <asp:BoundField HeaderText="Settlement Date" DataField="SettlementDate" />
                    <asp:BoundField HeaderText="Settlement Total" DataField="SettlementTotal" />
                    <asp:BoundField HeaderText="WireTotal" DataField="WireTotal" />
                    <asp:BoundField HeaderText="Invoice Total" DataField="InvoiceTotal" />
                    <asp:BoundField HeaderText="Invoice URL" DataField="InvoiceURL" />
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd="true" />
    </Footer>
</admin:ListSection>
