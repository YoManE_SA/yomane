﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Finanace
{
	public partial class AccountSettings : Controls.AccountControlBase
	{
		protected override void DataBindChildren()
		{
			rptItems.DataSource = Bll.Finance.AccountSettings.Load(new List<int>(AccountID.GetValueOrDefault()));
			base.DataBindChildren();
		}

		protected void Button_OnCommand(object sender, CommandEventArgs e)
		{
			switch (e.CommandName){
			case "SettingsAdd":
					rptItems.AddItem(new Bll.Finance.AccountSettings(AccountID.GetValueOrDefault(), Bll.Finance.SettlementType.Merchant));
				break;
			case "TransactionFeeAdd":
				var rptFees = (e as RepeaterCommandEventArgs).Item.FindControl("rptFees") as Web.Controls.DynamicRepeater;
				rptFees.AddItem(new Bll.Finance.AccountFeeSettings(AccountID.GetValueOrDefault(), 0));
				break;
			}
		}
	}
}