﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Finanace
{
	public partial class Form : Controls.TemplateControlBase
	{
		public Bll.Finance.Settlement Item { get; set; }
		protected decimal TotalFees { get; set; }
		protected decimal TotalTransfer { get; set; }
		protected void Page_Load(object sender, EventArgs e)
		{
			if (Request["Create"] == "1")
			{
				Item = new Bll.Finance.Settlement(Request["SetSettlement_id"].ToNullableInt().GetValueOrDefault(),Request["CurrencyIso"].EmptyIfNull());
				var createOptions = new Bll.Finance.Settlement.AddTransactionsOptions();
				createOptions.SettlementDate = Request["SettlementDate"].ToNullableDateTime().GetValueOrDefault(DateTime.Now);
				createOptions.includeTest = true;
				Item.AddTransactions(createOptions);
				//if (Item != null) Response.Redirect("Settlement.aspx?settlement_Id=" + Item.Settlement_id);
			}
			else Item = Bll.Finance.Settlement.Load(Request["settlement_Id"].ToNullableInt().GetValueOrDefault());
			if (Item == null) Item = new Bll.Finance.Settlement(0, "USD");
		}

		protected override void OnPreRender(EventArgs e)
		{
			var items = Item.Items;
			TotalFees = items.Where(t => t.IsFee).Sum(t => t.Total).GetValueOrDefault();
			TotalTransfer = items.Where(t => !t.IsFee).Sum(t => t.Total).GetValueOrDefault();
			var groups = items.GroupBy(i => i.AmountType);
			rptGroups.DataSource = groups;
			DataBind();
			int rptItem = 0;
			foreach (var g in groups)
				(rptGroups.Items[rptItem++].FindControl("ltGroupTotoal") as Literal).Text = formatCurrency(g.Sum(i => i.Total));
			base.OnPreRender(e);
		}

		protected void Delete_Click(object sender, EventArgs e)
		{
			//ltSettlementId.Text.ToNullableInt().GetValueOrDefault()
			//Item.Delete();
			//if (!string.IsNullOrEmpty(Request["redirect"])) Response.Redirect(Request["redirect"]);
			//Response.Write("Settlement Deleted");
		}

		protected void AddLine_Click(object sender, EventArgs e)
		{
			var lineItem = new Bll.Finance.SettlementItem(Item.ID, Bll.Finance.AmountType.System);
			lineItem.IsFee = true;
			lineItem.LineText = txtLineText.Text;
			lineItem.Quantity = txtQuantity.Text.ToNullableInt();
			lineItem.Amount = txtAmount.Text.ToNullableDecimal();
			lineItem.Total = lineItem.Quantity * lineItem.Amount;
			Item.Items.Add(lineItem);
		}

		protected string formatAmountField(Bll.Finance.SettlementItem dsi)
		{
			if (dsi.PercentValue != null)
			{
				decimal decValue = dsi.Total.GetValueOrDefault() / (dsi.PercentValue.Value / 100);
				if (decValue < 0) decValue = -decValue;
				return formatCurrency(decValue);
			}
			return formatCurrency(dsi.Amount);
		}

		protected string formatCurrency(decimal? amount, string currencyIso = null)
		{
			if (amount == null) return string.Empty;
			if (currencyIso == null) currencyIso = Item.SettlementCurrencyISOCode;
			string valueString = amount.GetValueOrDefault().ToString("0.00") + " " + currencyIso;
			if (amount < 0) valueString = string.Format("<span style=\"color:red;\">{0}</span>", valueString);
			return valueString;
		}

	}
}