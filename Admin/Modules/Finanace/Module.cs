﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Finanace
{
	public class Module : Admin.Module
	{
		private ApplicationMenu menuSettlements, memuUpcomming;
		public override string Name { get { return "Settlement"; } }
		public override decimal Version { get { return 1.0m; } }
		public override string Author { get { return "OBL ltd."; } }
		public override string Description { get { return ""; } }

		protected override void OnInit(EventArgs e)
		{
			Application.RegisterRoute("Settlements/{*id}", "Settlement", typeof(Controls.DataTablePage));
			menuSettlements = new ApplicationMenu("Settlements", "~/Settlements/0");
			memuUpcomming = new ApplicationMenu("Upcomming Settlements", "~/Settlements/Upcomming/0");
			base.OnInit(e);
		}

		protected override void OnActivate(EventArgs e)
		{
			Application.InitDataTablePage += Application_InitTemplatePage;
			Application.AddMenuItem("Finance", menuSettlements);
			Application.AddMenuItem("Finance", memuUpcomming);
			base.OnActivate(e);
		}

		protected override void OnDeactivate(EventArgs e)
		{
			Application.InitDataTablePage -= Application_InitTemplatePage;
			menuSettlements.Parent.RemoveChild(menuSettlements);
			memuUpcomming.Parent.RemoveChild(memuUpcomming);
			base.OnDeactivate(e);
		}

		protected void Application_InitTemplatePage(object sender, EventArgs e)
		{
			var page = sender as Controls.DataTablePage;
			if (page.TemplateName == "Settlement")
			{
				page.AddControlToList(page.LoadControl("~/Finanace/List.ascx"));
				page.AddControlToFilter("Data", page.LoadControl("~/Finanace/Filter.ascx"));
				page.AddControlToForm("Data", page.LoadControl("~/Finanace/Form.ascx"));
			} 
			else  if (Admin.Accounts.Module.TemplateIsAccountType(page.TemplateName))
			{
                page.AddControlToForm("Fees", page.LoadControl("~/Finanace/AccountFloorSettings.ascx"));
                page.AddControlToForm("Fees", page.LoadControl("~/Finanace/AccountSettings.ascx"));
            }
		}
	}
}