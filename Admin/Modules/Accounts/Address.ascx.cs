﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Accounts
{
    public partial class Address : System.Web.UI.UserControl, IValidator
    {
        
        public Netpay.Bll.Accounts.AccountAddress AddressData { get; set; }

        public string ValidationGroup { get; set; }

        public string OnSelectedIndexChanged { get; set; }

        public bool IsValid
        {
            get
            {
                return ((IValidator)RequiredFieldValidatorddlCountry).IsValid;
            }

            set
            {
                ((IValidator)RequiredFieldValidatorddlCountry).IsValid = value;
            }
        }

        public string ErrorMessage
        {
            get
            {
                return ((IValidator)RequiredFieldValidatorddlCountry).ErrorMessage;
            }

            set
            {
                ((IValidator)RequiredFieldValidatorddlCountry).ErrorMessage = value;
            }
        }

        protected override void DataBindChildren()
        {
            if (AddressData == null) AddressData = new Netpay.Bll.Accounts.AccountAddress();
            base.DataBindChildren();
            if (!string.IsNullOrEmpty(AddressData.StateISOCode)) ddlState.ISOCode = AddressData.StateISOCode;
            else
            {
                ddlState.SelectedIndex = 0;
            }
            
            if (!string.IsNullOrEmpty(AddressData.CountryISOCode)) ddlCountry.ISOCode = AddressData.CountryISOCode;
            else
            {
                ddlCountry.SelectedValue = "0";
            }
        }

        public Netpay.Bll.Accounts.AccountAddress Save(Netpay.Bll.Accounts.AccountAddress ad = null)
        {
            if (ddlCountry.SelectedIndex == 0) return null;
            if (ad != null) AddressData = ad;
            if (AddressData == null) AddressData = new Netpay.Bll.Accounts.AccountAddress();
            AddressData.AddressLine1 = txtAddress1.Text;
            AddressData.AddressLine2 = txtAddress2.Text;
            AddressData.City = txtCity.Text;
            AddressData.PostalCode = txtPostal.Text;
            AddressData.StateISOCode = ddlState.ISOCode;
            AddressData.CountryISOCode = ddlCountry.ISOCode;
            return AddressData;
        }

        public void Validate()
        {
            ((IValidator)RequiredFieldValidatorddlCountry).Validate();
        }

        protected override void OnInit(EventArgs e)
        {

            RequiredFieldValidatorddlCountry.ValidationGroup = this.ValidationGroup;

            OnSelectedIndexChanged = OnSelectedIndexChanged ?? "";

            base.OnInit(e);
        }
    }
}