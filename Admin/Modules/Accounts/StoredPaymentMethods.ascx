﻿<%@ Control Language="C#" CodeBehind="StoredPaymentMethods.ascx.cs" Inherits="Netpay.Admin.Accounts.StoredPaymentMethods" %>
<%@ Register Src="Address.ascx" TagPrefix="CUC" TagName="AccountAddress" %>
<%@ Register Src="IsraeliBankAccount.ascx" TagPrefix="CUC" TagName="IsraeliBankAccount" %>
<admin:FormPanel runat="server" ID="pnlListMethod" Title="Stored Payment Methods" OnCommand="PM_Command" EnableAdd="true">
    <netpay:ActionNotify runat="server" ID="StoredPmActionNotify"/>
<script type="text/javascript">
    function ValidatePage() {

        if (typeof (Page_ClientValidate) == 'function') {
            var check = Page_ClientValidate("creditcard");
        }

        if (check) {
            var button = document.getElementById("btnSaveMethod");
            button.removeAttribute("disabled");
            button.style.opacity = "1.0";
        }

        else {
            var button = document.getElementById("btnSaveMethod");
            button.disabled = true;
            button.style.opacity = "0.2";
            Page_BlockSubmit = false;
        }
    }
       </script>

    <NP:PagedRepeater runat="server" ID="rptPaymentMethods" OnItemCommand="PM_Command">
        <HeaderTemplate>

            <table class="table table-hover table-customize">
                <thead>
                    <tr>
                        <th>ID</th>
                         <th>Title</th>
                         <th>Method Type</th>
                         <th>Account Data</th>
                         <th>Exp.Date</th>
                         <th>Balance</th>
                         <th>Default</th>
                    </tr>
                </thead>
        </HeaderTemplate>
        <ItemTemplate>
            <tbody>
                <tr>
                    <td><%# Eval("ID") %></td>
                    <td><asp:LinkButton runat="server" Text='<%# ((string)Eval("Title")).NullIfEmpty().ValueIfNull("[Untitled]")  %>' CommandName="Edit" CommandArgument='<%# Eval("ID") %>' /></td>
                    <td> <img src="/NPCommon/ImgPaymentMethod/23X12/<%# (int)Eval("MethodInstance.PaymentMethodId") %>.gif" align="middle" border="0" /> <asp:Literal runat="server" Text='<%# Eval("MethodInstance.PaymentMethod.Name") %>' /></td>
                    <td><%# Eval("MethodInstance.Value1First6") %>...<%# Eval("MethodInstance.Value1Last4") %></td>
                    <td><%# Eval("MethodInstance.ExpirationDate", "{0:MM-yyy}") %></td>
                    <td><asp:LinkButton runat="server" ID="btnCardBalace" Text='<%# (bool)Eval("HasBalance")? "Show Balance" : "Not supported<br>By provider"  %>' OnCommand="PM_Command" CommandName="ShowBalance" CommandArgument='<%# Eval("ID") %>' Enabled='<%# Eval("HasBalance") %>' /></td>
                    <td><%# Eval("IsDefault") %></td>
                </tr>
            </tbody>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        <EmptyContainer><div class="alert alert-info"><strong>Info!</strong> No record information</div></EmptyContainer>
    </NP:PagedRepeater>
</admin:FormPanel>
<admin:ModalDialog runat="server" ID="dlgShowBalance" Title="Card Balance">
    <Body>
        <admin:AdminList runat="server" ID="rptCardBalance" SaveAjaxState="true" DataKeyNames="ID" AutoGenerateColumns="false" BubbleLoadEvent="false" DisableRowSelect="true">
            <Columns>
                <asp:TemplateField HeaderText="ID" SortExpression="ID">
                    <ItemTemplate>
                        <i class="fa fa-arrow-circle-<%# (decimal)Eval("Total") > 0 ? "right" : "left" %>"></i><%# Eval("ID") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="InsertDate" DataField="InsertDate" SortExpression="InsertDate" />
                <asp:TemplateField HeaderText="SourceType" SortExpression="SourceType">
                    <ItemTemplate><%# (string)Eval("SourceType") %></ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="SourceID" DataField="SourceID" SortExpression="SourceID" Visible="false" />
                <asp:BoundField HeaderText="Text" DataField="Text" />
                <asp:TemplateField HeaderText="Amount" SortExpression="Amount">
                    <ItemTemplate><%# ((decimal)Eval("Amount")).ToAmountFormat((string)Eval("CurrencyIso")) %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Total" SortExpression="Total">
                    <ItemTemplate><%# ((decimal)Eval("Total")).ToAmountFormat((string)Eval("CurrencyIso")) %></ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="IsPending" DataField="IsPending" />
            </Columns>
        </admin:AdminList>
    </Body>
</admin:ModalDialog>
<admin:ModalDialog runat="server" ID="dlgEditMethod" Title="Edit Payment Method" OnCommand="PM_Command">
    <Header>
        <netpay:ActionNotify runat="server" ID="EditMethodActionNotify" />
    </Header>
    <Body>
        <div class="row ">
            <div class="col-lg-6 col-md-6">
                Title
                <asp:TextBox runat="server" ID="txtTitle" CssClass="form-control" Text='<%# MethodData.Title %>' />
            </div>
            <div class="col-lg-6 col-md-6">
                Method Type
                <netpay:PaymentMethodDropDown CssClass="form-control" runat="server" ID="ddlPaymentMethod" Value='<%# (int)MethodData.MethodInstance.PaymentMethodId %>' AutoPostBack="true" OnSelectedIndexChanged="PaymentMethod_Changed" EnableBlankSelection="False" />
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="form-group">
                    ID: <i>
                        <asp:Literal runat="server" ID="ltID" Text='<%# MethodData.ID %>' /></i> | PaymentMethod: <i>
                            <asp:Literal runat="server" ID="ltPaymentMethod" Text='<%# (int) MethodData.MethodInstance.PaymentMethodId %>' Visible="false" /></i>
                    <i>
                        <asp:Literal runat="server" ID="ltPaymentMethodName" Text='<%# MethodData.MethodInstance.PaymentMethod.Name %>' />
                    </i>| EncryptionKey: <i>
                        <asp:Literal runat="server" ID="ltEncryptionKey" Text='<%# MethodData.MethodInstance.EncryptionKey %>' />
                    </i>| Issuer Country Iso: <i>
                        <asp:Literal runat="server" ID="ltIssuerCountryIsoCode" Text='<%# MethodData.IssuerCountryIsoCode %>' />
                    </i>
                </div>
            </div>
            <div class="col-lg-12 col-md-12">
                <div class="form-group">
                    Provider: <i>
                        <asp:Literal runat="server" ID="Literal4" Text='<%# MethodData.ProviderID %>' /></i> |
                    Reference: <i>
                        <asp:Literal runat="server" ID="Literal3" Text='<%# MethodData.ProviderReference1 %>' /></i>
                </div>
            </div>
        </div>
        <h4>Personal Details</h4>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    Owner Name
                    <asp:TextBox CssClass="form-control" runat="server" ID="txtOwnerName" Text='<%# MethodData.OwnerName %>' MaxLength="80" CausesValidation="True" onkeyup="ValidatePage();" />
                    <%-- Validation is for Phase 2.
                        <asp:RequiredFieldValidator
                        ForeColor="red"
                        ControlToValidate="txtOwnerName"
                        ID="RequiredFieldValidatortxtOwnerName"
                        runat="server"
                        ErrorMessage="This field is required."
                        ValidationGroup="creditcard">
                    </asp:RequiredFieldValidator>--%>
                </div>
                <div class="form-group" runat="server" id="trEncValue1">
                    <asp:Literal runat="server" Text="Account Value 1" ID="ltEncValue1" />
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtEncValue1" onkeyup="ValidatePage();" />

                    <%--//When credit number in format like:"1111...1111" , validation should not work
                    //For now this issue is for phase 2 , and this field validation is commented.
                    <asp:RequiredFieldValidator
                        ControlToValidate="txtEncValue1"
                        ForeColor="red"
                        ID="RequiredFieldValidatortxtEncValue1"
                        runat="server"
                        ErrorMessage="This field is required , insert a valid credit card number."
                        ValidationGroup="creditcard"
                         Display="Dynamic">
                    </asp:RequiredFieldValidator>

                    <asp:RegularExpressionValidator 
                        ValidationExpression="^[0-9]{6,16}$"
                        ControlToValidate="txtEncValue1"
                        ForeColor="red"
                        ID="RegularExpressionValidatortxtEncValue1" 
                        runat="server" 
                        ErrorMessage="This field is required , insert a valid credit card number."
                        ValidationGroup="creditcard"
                        Display="Dynamic">
                    </asp:RegularExpressionValidator>--%>
                </div>
                <div class="form-group" runat="server" id="trEncValue2">
                    <asp:Literal runat="server" Text="Account Value 2" ID="ltEncValue2" />
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtEncValue2" />
                </div>
            </div>
            <div class="col-lg-6 col-md-6" runat="server" id="trExpDate">
                <div class="from-group">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                Month
                                <netpay:MonthDropDown runat="server" CssClass="form-control" ID="ddlExpMonth" Value='<%# MethodData.MethodInstance.ExpirationMonth %>' EnableBlankSelection="False" />
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="from-group">
                                Year
                                <netpay:YearDropDown runat="server" CssClass="form-control" ID="ddlExpYear" Value='<%# MethodData.MethodInstance.ExpirationYear==1901? DateTime.Now.Year : MethodData.MethodInstance.ExpirationYear %>' YearFrom='<%# DateTime.Now.Year - 5 %>' YearTo='<%# DateTime.Now.Year + 10 %>' EnableBlankSelection="False" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="from-group">
                    <br />
                    <asp:CheckBox runat="server" ID="chkIsDefaultMethod" Checked='<%# MethodData.IsDefault %>' Text="Is Default" />
                </div>
            </div>
        </div>
        <h4>Billing Address</h4>
        <CUC:AccountAddress runat="server" ID="caAddress" AddressData='<%# MethodData.BillingAddress %>' ValidationGroup="Accounts_Storedpm_ddl" />

    </Body>
    <Footer>
        <asp:Button runat="server" ID="btnSaveMethod" CssClass="btn btn-primary" CommandName="Save" CommandArgument='<%# MethodData.ID %>' Text="Save" ClientIDMode="Static" />
        <asp:Button runat="server" ID="btnDeleteMethod" CssClass="btn btn-danger" CommandName="Delete" CommandArgument='<%# MethodData.ID %>' Text="Delete" />
    </Footer>
</admin:ModalDialog>

<admin:FormPanel runat="server" ID="pnlListBankAccounts" Title="Stored Bank Accounts" OnCommand="BankAccount_Command" EnableAdd="true">
    <NP:PagedRepeater runat="server" ID="rptBankAccounts" OnItemCommand="BankAccount_Command">
        <HeaderTemplate>  
        <table class="table table-hover table-customize">
        <thead>
          <tr>
            <th>ID</th>
            <th>Currency</th>
            <th>Account Number</th>
            <th>Account Name</th>
            <th>Default</th>
          </tr>
        </thead>
        <tbody>
        </HeaderTemplate>
        <ItemTemplate>
          <tr>
                <td><%# Eval("ID") %></td>
                <td><%# Eval("CurrencyISOCode") %></td>
                <td>
                    <asp:LinkButton ID="LinkButton1" runat="server" Text='<%# ((string)Eval("AccountNumber")).NullIfEmpty().ValueIfNull("[Untitled]") %>' CommandName="Edit" CommandArgument='<%# Eval("ID") %>' />
                </td>
                <td>
                    <%# Eval("AccountName") %>
                </td>
                <td>
                    <%# Eval("IsDefault") %>
               </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
             </tbody>
            </table>
        </FooterTemplate>
        <EmptyContainer><div class="alert alert-info"><strong>Info!</strong> No record information</div></EmptyContainer>
    </NP:PagedRepeater>
</admin:FormPanel>

<admin:ModalDialog runat="server" ID="dlgEditBankAccount" Title="Edit Bank Account" OnCommand="BankAccount_Command">

    <Body>
        <div class="row">
            <div class="col-lg-12">
                ID
			    <asp:Literal runat="server" ID="Literal1" Text='<%# BankData.ID %>' />
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    Account Name
			        <asp:TextBox CssClass="form-control" runat="server" ID="txtAccountName" Text='<%# BankData.AccountName %>' />
                </div>
                <div class="form-group">
                    Account Number
			        <asp:TextBox CssClass="form-control" runat="server" ID="txtAccountNumber" Text='<%# BankData.AccountNumber %>' />
                </div>
                <div class="form-group">
                    ABA
			<asp:TextBox CssClass="form-control" runat="server" ID="txtABA" Text='<%# BankData.ABA %>' />
                </div>
                <div class="form-group">
                    IBAN
                <div class="input-group">
                    <asp:TextBox CssClass="form-control" runat="server" ID="txtIBAN" Text='<%# BankData.IBAN %>' />
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-default">...</button>
                    </span>
                </div>
                    <CUC:IsraeliBankAccount ID="IsraeliBankAccount1" runat="server" Visible="false" IbanTextField="txtIBAN" />
                </div>
                <div class="form-group">
                    Swift Number
					<asp:TextBox CssClass="form-control" runat="server" ID="txtSwiftNumber" Text='<%# BankData.SwiftNumber %>' />
                </div>
                <div class="form-group">
                    Currency Iso
                    <asp:MultiView runat="server" ActiveViewIndex='<%# IsShowCurrencies? 0 : 1 %>'>
                        <asp:View runat="server">
                            <netpay:CurrencyDropDown BlankSelectionText="" CssClass="form-control" runat="server" ID="ddlCurrency" SelectedCurrencyIso='<%# string.IsNullOrEmpty(BankData.CurrencyISOCode)? "" : BankData.CurrencyISOCode %>'></netpay:CurrencyDropDown>
                        </asp:View>
                        <asp:View runat="server">
                            <div class="alret alert-info">
                                Please declare at least one currency<br /> Settings in payouts tab
                            </div>
                        </asp:View>
                    </asp:MultiView>
                    
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    Bank Name
					<asp:TextBox CssClass="form-control" runat="server" ID="txtBankName" Text='<%# BankData.BankName %>' />
                </div>
                <div class="form-group">
                    Bank Address Line 1
					<asp:TextBox CssClass="form-control" runat="server" ID="txtBankStreet1" Text='<%# BankData.BankStreet1 %>' />
                </div>
                <div class="form-group">
                    Bank Address Line 2
					<asp:TextBox CssClass="form-control" runat="server" ID="txtBankStreet2" Text='<%# BankData.BankStreet2 %>' />
                </div>
                <div class="form-group">
                    Bank Address City
					<asp:TextBox CssClass="form-control" runat="server" ID="txtBankCity" Text='<%# BankData.BankCity %>' />
                </div>
                <div class="form-group">
                    Bank Address Country
					<netpay:CountryDropDown CssClass="form-control" runat="server" ID="ddlBankCountryISOCode" ISOCode='<%# BankData.BankCountryISOCode %>' />
                </div>

            </div>
        </div>
        <asp:CheckBox Text="Is Default" runat="server" ID="chkIsDefaultAccount" Checked='<%# BankData.IsDefault %>' />
    </Body>
    <Footer>
        <asp:Button runat="server" ID="btnSaveAccount" CssClass="btn btn-primary" CommandName="Save" CommandArgument='<%# BankData.ID %>' Text="Save" />
        <asp:Button OnClientClick="return confirm('Are you sure?');" runat="server" ID="btnDeleteAccount" CssClass="btn btn-danger" CommandName="Delete" CommandArgument='<%# BankData.ID %>' Text="Delete" />
    </Footer>
</admin:ModalDialog>
<admin:ModalDialog runat="server" ID="dlgCountryDropDown" Title="Save Stored PM Error Msg">
    <Body>
        <netpay:ActionNotify runat="server" ID="CountryNotSelectedNotify" />
    </Body>
</admin:ModalDialog>
