﻿<%@ Control Language="C#" CodeBehind="FilesAndNotes.ascx.cs" Inherits="Netpay.Admin.Accounts.FilesAndNotes" %>
<%@ Register Src="~/Modules/Accounts/FilesArchive/Data.ascx" TagPrefix="CUC" TagName="Data" %>

<admin:FormPanel runat="server" ID="pnlNotes" Title="Notes" OnCommand="Note_Command">
    <asp:MultiView ID="MultiViewNotes" runat="server">
        <asp:View ID="ViewIfNotNewDebitCompany1" runat="server">
    <NP:PagedRepeater ID="rptNotes" runat="server">
         <HeaderTemplate>
        <table class="table table-hover table-customize">
            <thead>
                <th>Date</th>
                <th>Added by</th>
                <th>Text</th>
            </thead>
        </HeaderTemplate>
        <ItemTemplate>
            <tbody>
                <td><asp:Literal runat="server" Text='<%# Eval("InsertDate") %>' /></td>
                <td><asp:Literal runat="server" Text='<%# Eval("UserName") %>' /></td>
                <td><asp:Literal runat="server" Text='<%# Eval("Text") %>' /></td> 
            </tbody>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        <EmptyContainer><div class="alert alert-info"><strong>Info!</strong> No record information</div></EmptyContainer>
    </NP:PagedRepeater>
            </asp:View>
        <asp:View ID="ViewIfNew1" runat="server">
            <div class="alert alert-info"> 
                Save account details to edit this section.
                </div>
        </asp:View>
    </asp:MultiView>
</admin:FormPanel>
<netpay:ModalDialog runat="server" ID="dlgNote" Title="Edit Note" OnCommand="Note_Command">
    <Body>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="form-group">
                    Notes
                    <asp:TextBox ID="txtNote" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control" />
                </div>
            </div>
        </div>
    </Body>
    <Footer>
        <asp:Button ID="Button1" runat="server" Text="Save" CommandName="Save" class="btn btn-primary" />
    </Footer>
</netpay:ModalDialog>

<admin:FormPanel runat="server" ID="pnlFiles" Title="Files" EnableAdd="true" OnCommand="File_Command">
    <asp:MultiView ID="MultiViewFiles" runat="server">
        <asp:View ID="View1" runat="server">
    <NP:PagedRepeater ID="rptFiles" runat="server">
        <HeaderTemplate>
        <table class="table table-hover table-customize">
            <thead>
                <th>File Title</th>
                <th>File Name</th>
                <th>Description</th>
                <th>Type</th>
                <th>Insert Date</th>
                <th>Approved</th>
            </thead>
        </HeaderTemplate>
        <ItemTemplate>
              <tbody>
                <td>   
                    <asp:ImageButton runat="server" CommandName="EditItem" OnCommand="File_Command" CommandArgument='<%# Eval("ID") %>' ImageUrl='<%# string.Format("/NPCommon/ImgFileExt/14X14/{0}.gif", (string)Eval("FileExt")) %>' Width="14" Height="14" />               
                    <asp:LinkButton runat="server" CommandName="EditItem" OnCommand="File_Command" CommandArgument='<%# Eval("ID") %>' Text='<%# Eval("FileTitle") %>' /> </td>
                <td><asp:Literal runat="server" Text='<%# Eval("FileName") %>' /></td>
                  <td><asp:Literal runat="server" Text='<%# Eval("AdminComment") %>' /></td>
                <td><asp:Literal runat="server" Text='<%# (byte?)Eval("FileItemType_id") != null ? Netpay.Bll.Accounts.File.GetFileTypeText((byte?)Eval("FileItemType_id")) : "No type selected" %>' /></td>
                <td><asp:Literal runat="server" Text='<%# Eval("InsertDate") %>' /></td>
                <td><asp:Literal runat="server" Text='<%# Eval("AdminApprovalDate") == null ? "Not Approved" : "<img src=\"/NPCommon/Images/icon_v.gif\" align=\"top\" /> [" + Eval("AdminApprovalUser") + "]" %>' /> </td>
            </tbody>
        </ItemTemplate>
        <FooterTemplate>
             </table>
        </FooterTemplate>
        <EmptyContainer><div class="alert alert-info"><strong>Info!</strong> No record information</div></EmptyContainer>
    </NP:PagedRepeater>
               </asp:View>
        <asp:View ID="View2" runat="server">
            <div class="alert alert-info">
                Save account details to edit this section.
             </div> 
        </asp:View>
    </asp:MultiView>
</admin:FormPanel>
<admin:ModalDialog runat="server" ID="dlgFile" Title="Edit File" OnCommand="FileDialogCommand">
    <Body>
        <CUC:Data runat="server" ID="fileData" />
    </Body>
    <Footer>
        <admin:DataButtons runat="server" EnableSave="true" EnableDelete='<%# fileData.Item != null && fileData.Item.ID > 0 %>' />
    </Footer>
</admin:ModalDialog>

