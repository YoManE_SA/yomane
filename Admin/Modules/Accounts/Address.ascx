﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Address.ascx.cs" Inherits="Netpay.Admin.Accounts.Address" %>

<div class="row">
    <div class="col-lg-4 col-md-4">
        <div class="form-group">
            Address Line 1 
            <asp:TextBox runat="server" CssClass="form-control" ID="txtAddress1" Text='<%# AddressData.AddressLine1 %>' />
        </div>
        <div class="form-group clear-form-group">
            Address Line 2 
            <asp:TextBox runat="server" CssClass="form-control" ID="txtAddress2" Text='<%# AddressData.AddressLine2 %>' />
        </div>
    </div>
    <div class="col-lg-4 col-md-4">
        <div class="form-group">
            City 
            <asp:TextBox runat="server" CssClass="form-control" ID="txtCity" Text='<%# AddressData.City %>' />
        </div>
        <div class="form-group clear-form-group">
            Postal Code
            <asp:TextBox runat="server" CssClass="form-control" ID="txtPostal" Text='<%# AddressData.PostalCode %>' />
        </div>
    </div>
    <div class="col-lg-4 col-md-4">
        <div class="form-group">
            Province 
            <netpay:StateDropDown CssClass="form-control" runat="server" ID="ddlState" />
        </div>
        <div class="form-group clear-form-group">
            Country
           <netpay:CountryDropDown CssClass="form-control" runat="server" ID="ddlCountry" CausesValidation="True" BlankSelectionText="Choose country" BlankSelectionValue="0"  RequiredField="true" onchange="<%# OnSelectedIndexChanged %>"/>
            <asp:RequiredFieldValidator 
                ForeColor="red"
                ID="RequiredFieldValidatorddlCountry" 
                ControlToValidate="ddlCountry"
                runat="server" 
                ErrorMessage="Select country." 
                InitialValue=0
                ValidationGroup="uc_address">
            </asp:RequiredFieldValidator>
        </div>
    </div>
</div>
