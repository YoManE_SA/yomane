﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using System.Reflection;

namespace Netpay.Admin.Modules.Accounts
{
    public partial class ExternalServiceLogin : Controls.AccountControlBase
    {
        protected Netpay.Bll.Accounts.ExternalServiceLogin ItemData;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            rptExternalLogin.DataBinding += delegate { if (AccountID != null) rptExternalLogin.DataSource = Netpay.Bll.Accounts.ExternalServiceLogin.LoadForAccount(AccountID.Value); };
        }

        protected override void DataBindChildren()
        {
            pnlExternalLogin.DataBind();
            //base.DataBindChildren();
        }

        protected string[] GetServiceTypes()
        {
            var tp = typeof(Netpay.Bll.Accounts.ExternalServiceLogin.ServiceTypes);
            var fieldInfos = tp.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy);
            return fieldInfos.Where(v => v.IsLiteral && !v.IsInitOnly).Select(v => (string)v.GetValue(null)).ToArray();
        }

        protected string[] GetMethodTypes()
        {
            var tp = typeof(Netpay.Bll.Accounts.ExternalServiceLogin.Protocols);
            var fieldInfos = tp.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy);
            return fieldInfos.Where(v => v.IsLiteral && !v.IsInitOnly).Select(v => (string)v.GetValue(null)).ToArray();
        }

        private void BindSave()
        {
            ItemData.Method = ddlMethod.Text;
            ItemData.IsActive = chkIsActive.Checked;
            ItemData.ServerURL = txtServerURL.Text;
            ItemData.Username = txtUsername.Text;
            ItemData.Password = txtPassword.Text;
            ItemData.Reference1 = txtReference1.Text;
        }

        protected void ESL_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Edit":
                    ItemData = Netpay.Bll.Accounts.ExternalServiceLogin.Load(((string)e.CommandArgument).EmptyIfNull().ToInt32(0));
                    if (ItemData == null) return;
                    dlgExternalLogin.BindAndShow();
                    break;
                case "AddItem":
                    ItemData = new Netpay.Bll.Accounts.ExternalServiceLogin(AccountID.Value, "");
                    dlgExternalLogin.BindAndShow();
                    break;
                case "Delete":
                    ItemData = Netpay.Bll.Accounts.ExternalServiceLogin.Load(((string)e.CommandArgument).EmptyIfNull().ToInt32(0));
                    ItemData.Delete();
                    dlgExternalLogin.RegisterHide();
                    pnlExternalLogin.BindAndUpdate();
                    break;
                case "Save":
                    ItemData = Netpay.Bll.Accounts.ExternalServiceLogin.Load(((string)e.CommandArgument).EmptyIfNull().ToInt32(0));
                    if (ItemData == null) ItemData = new Netpay.Bll.Accounts.ExternalServiceLogin(AccountID.Value, ddlServiceType.Text);
                    BindSave();
                    ItemData.Save();
                    dlgExternalLogin.RegisterHide();
                    pnlExternalLogin.BindAndUpdate();
                    break;
                case "TestCon":
                    ItemData = Netpay.Bll.Accounts.ExternalServiceLogin.Load(((string)e.CommandArgument).EmptyIfNull().ToInt32(0));
                    if (ItemData == null) return;
                    BindSave();
                    string testResult;
                    lblTestResult.Style["display"] = "block";
                    var testRet = ItemData.TestConnection(out testResult);
                    lblTestResult.Text = testResult;
                    lblTestResult.BackColor = testRet ? System.Drawing.Color.FromArgb(0xddffdd) : System.Drawing.Color.FromArgb(0xffdddd);
                    break;
            }
        }
    }
}