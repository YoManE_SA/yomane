﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Accounts.FilesArchive
{
    public partial class Data : Controls.TemplateControlBase
    {
        public Bll.Accounts.File Item { get { return TemplatePage.GetItemData<Bll.Accounts.File>(); } set { TemplatePage.SetItemData(value); } }

        protected override void OnLoad(EventArgs e)
        {
            if (TemplatePage.TemplateName == "FilesArchive")
            {
                TemplatePage.PageController.DeleteItem += PageController_DeleteItem;
                TemplatePage.PageController.SaveItem += PageController_SaveItem; 
            }
            base.OnLoad(e);
        }

        private void PageController_DeleteItem(object sender, EventArgs e)
        {
            Item = Netpay.Bll.Accounts.File.Load(hfFileID.Value.ToNullableInt().GetValueOrDefault());
            if (Item != null) Item.Delete();
            return;
        }

        private void PageController_SaveItem(object sender, EventArgs e)
        {
            Item = Netpay.Bll.Accounts.File.Load(hfFileID.Value.ToNullableInt().GetValueOrDefault());
            if (Item == null)
            {
                if (apAccount.Value.HasValue)
                {
                    Item = new Netpay.Bll.Accounts.File(apAccount.Value.Value);
                }
                else
                {
                    Item = new Netpay.Bll.Accounts.File();
                }

                if (FileTypeDropDown.IsSelected)
                {
                    Item.FileItemType_id = byte.Parse(FileTypeDropDown.SelectedValue);
                }
                else
                {
                    Item.FileItemType_id = null;
                }

                Item.FileTitle = txtFileTitle.Text;
                Item.AdminComment = txtAdminComment.Text;
                if (fuFile.FileName.Length > 0 && fuFile.FileBytes != null)
                {
                    Item.Save(fuFile.FileName, fuFile.FileBytes);
                    fuFile.DeleteTemporaryFile();
                    fuFile.LocalPath = Item.FilePath;
                    upStatus.DataBind();
                    upStatus.Update();
                }
            }
            else
            {
                //Check if the file account id value was changed, if so move it to the right folder.
                int? oldAccountId = Item.Account_id;
                string oldFilePath = Item.FilePath;

                if (FileTypeDropDown.IsSelected)
                {
                    Item.FileItemType_id = byte.Parse(FileTypeDropDown.SelectedValue);
                }
                else
                {
                    Item.FileItemType_id = null;
                }

                Item.FileTitle = txtFileTitle.Text;
                Item.AdminComment = txtAdminComment.Text;
                Item.Account_id = apAccount.Value;
                Item.Save();

                //Check if the account id value was changed , if so 
                //Change the position of the file on the file system
                if (Item.Account_id != oldAccountId)
                {
                    if (System.IO.File.Exists(oldFilePath))
                    {
                        //Check if a file with the same name already exist in the new path
                        if (System.IO.File.Exists(Item.FilePath))
                        {
                            //Return the Account_id to the old one , file couldn't be moved.
                            Item.Account_id = oldAccountId;
                            Item.Save();

                            TemplatePage.FormViewGlobalModalDialog.Title = "Error";
                            TemplatePage.FormViewGlobalActionNotify.SetMessage("File with the same name already exist, Please change the file name and upload it.", true);
                            TemplatePage.FormViewGlobalModalDialog.RegisterShow();

                            throw new Exception("file name already exist.");
                        }

                        //Check if the directory exist , if not create it
                        if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(Item.FilePath)))
                        {
                            System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(Item.FilePath));
                        }

                        System.IO.File.Move(oldFilePath, Item.FilePath);
                    }
                }

                upStatus.DataBind();
                upStatus.Update();
            }
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            Item = Netpay.Bll.Accounts.File.Load(hfFileID.Value.ToNullableInt().GetValueOrDefault());
            Item.Delete();
        }

        protected override void DataBindChildren()
        {
            if (Item == null) Item = new Netpay.Bll.Accounts.File();
            base.DataBindChildren();
        }

        public void Save(int accountId)
        {
            //That Save function comes only from an account FilesAndNotes control so that the account
            //Is known and cannot be changed from one to another.
            Item = Netpay.Bll.Accounts.File.Load(hfFileID.Value.ToNullableInt().GetValueOrDefault());
            if (Item == null)
            {
                Item = new Netpay.Bll.Accounts.File(accountId);
                if (FileTypeDropDown.IsSelected)
                {
                    Item.FileItemType_id = byte.Parse(FileTypeDropDown.SelectedValue);
                }
                else
                {
                    Item.FileItemType_id = null;
                }
                                 
                Item.FileTitle = txtFileTitle.Text;
                Item.AdminComment = txtAdminComment.Text;
                if (fuFile.FileName.Length > 0 && fuFile.FileBytes != null)
                {
                    Item.Save(fuFile.FileName, fuFile.FileBytes);
                    fuFile.DeleteTemporaryFile();
                    fuFile.LocalPath = Item.FilePath;
                    upStatus.DataBind();
                    upStatus.Update();
                }
            }
            else
            {
                if (FileTypeDropDown.IsSelected)
                {
                    Item.FileItemType_id = byte.Parse(FileTypeDropDown.SelectedValue);
                }
                else
                {
                    Item.FileItemType_id = null;
                }

                Item.FileTitle = txtFileTitle.Text;
                Item.AdminComment = txtAdminComment.Text;
                Item.Save();
                upStatus.DataBind();
                upStatus.Update();
            }
        }

        public void Delete()
        {
            var file = Netpay.Bll.Accounts.File.Load(hfFileID.Value.ToNullableInt().GetValueOrDefault());
            if (file != null)
                file.Delete();
        }

        protected void File_Command(object sender, CommandEventArgs e)
        {
            Item = Netpay.Bll.Accounts.File.Load(hfFileID.Value.ToNullableInt().GetValueOrDefault());
            if (Item == null) return;
            switch (e.CommandName)
            {
                case "Approve":
                    Item.SetApprove(false);
                    Item.Save();
                    upStatus.DataBind();
                    upStatus.Update();
                    break;
                case "Decline":
                    Item.SetApprove(true);
                    Item.Save();
                    upStatus.DataBind();
                    upStatus.Update();
                    break;
            }
        }



    }
}