﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Accounts.FilesArchive
{
	public partial class Filter : Controls.TemplateControlBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
        }

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
        {
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Bll.Accounts.File.SearchFilters();
            sf.FileName = apFileName.Text;
            sf.ID = rngID.Value; 
            sf.FileDate = rngFileDate.Value;
            sf.AccountId = apAccount.Value;
            sf.IsOnlyAdmin = IsAdmin.Checked;
            sf.FileTypes = new List<string>();
            foreach (ListItem l in fileTypes.Items)
                if (l.Selected) sf.FileTypes.Add(l.Value);

            /*
            sf.ID = apAccount.Value.HasValue ? new Range<int?>(apAccount.Value) : rngID.Value;
            sf.OpenDate = rndOpenDate.Value;
            sf.CloseDate = rndCloseDate.Value;
            sf.IndustryID = ddlIndustry.Value.ToNullableInt();
            sf.Department = ddlDepartment.Value.ToNullableInt();
            sf.Status = new List<MerchantStatus>();
            foreach (ListItem l in chklStatus.Items)
                if (l.Selected) sf.Status.Add(l.Value.ToNullableEnum<Netpay.Infrastructure.MerchantStatus>().GetValueOrDefault());
                */
            TemplatePage.SetFilter(sf);
        }

    }
}