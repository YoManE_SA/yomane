﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;

namespace Netpay.Admin.Accounts.FilesArchive
{
	public partial class List : Controls.TemplateControlBase
	{
        protected Dictionary<int, string> accountNames;

        protected void List_DataBinding(object sender, EventArgs e)
        {
            var list =  Bll.Accounts.File.Search(TemplatePage.GetFilter<Bll.Accounts.File.SearchFilters>(), rptList);
            accountNames = Bll.Accounts.Account.GetNames(list.Where(x => x.Account_id.HasValue).Select(i => i.Account_id.Value).ToList());
            rptList.DataSource = list;
        }

        protected string GetAccountName(int acccountId)
        {
            string ret;
            if (!accountNames.TryGetValue(acccountId, out ret)) return null;
            return ret;
        }

    }   
}