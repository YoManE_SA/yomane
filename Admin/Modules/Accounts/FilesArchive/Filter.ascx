﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Accounts.FilesArchive.Filter" %>
<admin:FilterSection runat="server" Title="Search">

    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblName" runat="server" Text="File Name" />
                <admin:FilePicker runat="server" ID="apFileName" />
            </div>
        </div>
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label runat="server" Text="Account" />
                <admin:AccountPicker runat="server" ID="apAccount" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4">
            <asp:CheckBox runat="server" ID="IsAdmin" Text="Show only Admin files" />
        </div>
    </div>
    <hr />

    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblID" runat="server" Text="ID Range" />
                <JQ:IntRange runat="server" ID="rngID" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblInsertDate" runat="server" Text="Date" />
                <JQ:DateRange runat="server" ID="rngFileDate" />
            </div>
        </div>
    </div>

    <hr />

    <div class="form-group">
        <h4>
            <asp:Label ID="lblStatus" runat="server" Text="File Types" /></h4>
        <asp:CheckBoxList runat="server" CssClass="checkbox-list" ID="fileTypes" RepeatLayout="UnorderedList" CellPadding="0" CellSpacing="0" DataSource='<%# Netpay.Bll.Accounts.File.AllFileExt %>' />
    </div>

</admin:FilterSection>
