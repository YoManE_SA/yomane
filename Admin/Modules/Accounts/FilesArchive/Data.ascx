﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Data.ascx.cs" Inherits="Netpay.Admin.Accounts.FilesArchive.Data" %>

<%-- "FormSections" control was replaced with "Label" because there is bug
      when a "Modal-Dialog" has "FormSections" control inside it 
      the bug is when pressing the "Expand" button at the top right corner
      of the "Form" it shows the "FormSections" that is inside the modal-dialog.--%>
<%--<admin:FormSections runat="server" Title="Details">--%>

<asp:HiddenField runat="server" ID="hfFileID" Value='<%# Item.ID %>' />
<asp:Label runat="server" Text="Details" Font-Bold="true"></asp:Label>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="form-group">
            Maximum file size is 5mb
            <NP:FileUpload ID="fuFile" runat="server" LocalPath='<%# Item.FilePath %>' />
        </div>
    </div>
</div>

<asp:PlaceHolder runat="server" Visible='<%# TemplatePage.TemplateName == "FilesArchive"? true : false %>'>
    <div class="row">
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                <admin:AccountPicker runat="server" ID="apAccount" Value='<%# Item.Account_id %>' Text='<%# Item.AccountName %>' PlaceHolder="Choose account" UseTargetID="false"/>
            </div>
        </div>
    </div>
</asp:PlaceHolder>

<hr />
<div class="row">
    <div class="col-lg-12 col-md-12">
        <NP:UpdatePanel runat="server" ID="upStatus" UpdateMode="Conditional" ChildrenAsTriggers="false" RenderMode="Inline" Visible='<%# Item != null && Item.ID > 0 %>'>
            <ContentTemplate>
                <asp:MultiView runat="server" ID="mvStatus" ActiveViewIndex='<%# Item.IsApproved ? 0 : 1 %>'>
                    <asp:View runat="server">
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                Status: Apporved
                            </div>
                            <div class="col-lg-5 col-md-5">
                                Approval Date: <%# Item.AdminApprovalDate %>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                Approval User: <%# Item.AdminApprovalUser %>
                            </div>
                        </div>
                    </asp:View>
                    <asp:View runat="server">
                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                Status: Not Apporved
                            </div>
                        </div>
                    </asp:View>
                </asp:MultiView>
                <div class="row spacer">
                    <div class="col-lg-3 col-md-3">
                        Change Status:
                    </div>
                    <div class="col-lg-9 col-md-9">
                        <asp:Button runat="server" CssClass="btn btn-info" CommandArgument='<%# Item.ID %>' Text="Approve" CommandName="Approve" OnCommand="File_Command" />
                        <asp:Button runat="server" CssClass="btn btn-danger" CommandArgument='<%# Item.ID %>' Text="Decline" CommandName="Decline" OnCommand="File_Command" />
                    </div>
                </div>
            </ContentTemplate>
        </NP:UpdatePanel>
    </div>
</div>
<hr />
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="form-group">
            File Type
            <netpay:DropDownBase ID="FileTypeDropDown" EnableBlankSelection="true" BlankSelectionText="" BlankSelectionValue="" runat="server" CssClass="form-control" DataSource='<%# Netpay.Bll.Accounts.File.FileTypes %>' Value='<%# Item.FileItemType_id.HasValue? Item.FileItemType_id.Value.ToString() : "" %>' DataTextField="Value" DataValueField="Key"></netpay:DropDownBase>
        </div>
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="form-group">
            File Title
                <asp:TextBox ID="txtFileTitle" runat="server" TextMode="SingleLine" CssClass="form-control" Text='<%# Item.FileTitle %>' />
        </div>
    </div>
    <div class="col-lg-12 col-md-12">
        <div class="form-group">
            Description
                <asp:TextBox ID="txtAdminComment" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control" Text='<%# Item.AdminComment %>' />
        </div>
    </div>
</div>
<%--</admin:FormSections>--%>
