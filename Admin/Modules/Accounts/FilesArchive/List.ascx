﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Accounts.FilesArchive.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Files Archive">
    <Header></Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" ID="rptList" SaveAjaxState="true" DataKeyNames="ID" OnDataBinding="List_DataBinding" AutoGenerateColumns="false" BubbleLoadEvent="true">
                <Columns>
                    <asp:BoundField HeaderText="ID" DataField="ID" SortExpression="ID" />
                    <asp:BoundField HeaderText="Date & Time" DataField="InsertDate" SortExpression="InsertDate" />
                    <asp:TemplateField HeaderText="Account">
                        <ItemTemplate>
                            <%# Eval("Account_id") != null ? GetAccountName((int)Eval("Account_id")) : "Admin" %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Description" DataField="FileTitle" SortExpression="FileTitle" />
                    <asp:BoundField HeaderText="File" DataField="FileName" SortExpression="FileName" />
                    <asp:BoundField HeaderText="File Type" DataField="FileExt" SortExpression="FileExt" />
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd="true" />
    </Footer>
</admin:ListSection>

