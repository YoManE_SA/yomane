﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;
using Netpay.Infrastructure.Security;

namespace Netpay.Admin.Accounts
{
    public partial class StoredPaymentMethods : Controls.AccountControlBase
    {
        protected Netpay.Bll.Accounts.StoredPaymentMethod MethodData;
        protected Netpay.Bll.Accounts.BankAccount BankData;
        protected List<Netpay.Bll.Accounts.BankAccount> AccountBanksList;

        public bool IsShowCurrencies
        {
            get
            {
                if (AccountPage.Account != null && AccountPage.Account.AccountType != Bll.Accounts.AccountType.Merchant)
                {
                    return true;
                }
                else if (AccountPage.Account != null && Bll.Settlements.MerchantSettings.LoadForMerchant(Account.MerchantID.Value).Select(x => x.Key).ToList().Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        protected bool IsGroupB_Valid(string group)
        {
            var validators = Page.GetValidators(group);
            bool isValid = true;
            foreach (var item in Page.GetValidators(group))
            {
                if (!((IValidator)item).IsValid)
                    isValid = false;
            }

            return isValid;
        }
        public void ValidateByGroup(string group)
        {
            Page.Validate(group);
            if (!Page.IsValid)
            {
                btnSaveMethod.Style.Add("opacity", "0.2");
                btnSaveMethod.Style.Add("cursor", "default");
                btnSaveMethod.Attributes.Add("disabled", "true");
            }
            else
            {
                btnSaveMethod.Style.Add("opacity", "1");
                btnSaveMethod.Style.Add("cursor", "pointer");
                btnSaveMethod.Attributes.Remove("disabled");
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            rptPaymentMethods.DataBinding += delegate { if (AccountID != null) rptPaymentMethods.DataSource = Netpay.Bll.Accounts.StoredPaymentMethod.LoadForAccount(AccountID.Value); };
            rptBankAccounts.DataBinding += delegate { if (AccountID != null) rptBankAccounts.DataSource = Netpay.Bll.Accounts.BankAccount.LoadForAccount(AccountID.Value); };
        }

        protected override void DataBindChildren()
        {
            rptPaymentMethods.DataBind();
            rptBankAccounts.DataBind();
            //if (BankData != null) dlgEditBankAccount.DataBind();
            //if (MethodData != null) dlgEditMethod.DataBind();
            //base.DataBindChildren();
        }

        protected void PaymentMethod_Changed(object sender, EventArgs e)
        {
            var pmInfo = Bll.PaymentMethods.PaymentMethod.Get(ddlPaymentMethod.Value.ToNullableEnum<CommonTypes.PaymentMethodEnum>().GetValueOrDefault(CommonTypes.PaymentMethodEnum.Unknown));
            if (pmInfo.Name == "Unknown")
            {
                ltEncValue1.Text = "Card Number";
                trEncValue1.Visible = true;
                txtEncValue1.CausesValidation = true;
                trEncValue2.Visible = false;
                trExpDate.Visible = true;
                if (e.GetType().ToString() != "Netpay.Admin.Accounts.EditEvent")
                { ValidateByGroup("creditcard"); }
            }
            else
            {
                ltEncValue1.Text = pmInfo.Value1EncryptedCaption;
                ltEncValue2.Text = pmInfo.Value2EncryptedCaption;

                trEncValue1.Visible = !string.IsNullOrEmpty(ltEncValue1.Text);
                txtEncValue1.CausesValidation = trEncValue1.Visible;
                if (e.GetType().ToString() != "Netpay.Admin.Accounts.EditEvent")
                {
                    if (txtEncValue1.CausesValidation)
                    { ValidateByGroup("creditcard"); }
                }
                trEncValue2.Visible = !string.IsNullOrEmpty(ltEncValue2.Text);
                trExpDate.Visible = pmInfo.IsExpirationDateMandatory;
            }
            dlgEditMethod.UpdatePanel.Update();
        }

        protected void PM_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "ShowBalance":
                    try
                    {
                        MethodData = Netpay.Bll.Accounts.StoredPaymentMethod.Load(((string)e.CommandArgument).EmptyIfNull().ToInt32(0));
                        rptCardBalance.DataSource = MethodData.Provider.GetBalace(MethodData, new Range<DateTime>(DateTime.Now.AddYears(-1), DateTime.Now));
                    }
                    catch (Exception ex)
                    {
                        StoredPmActionNotify.SetException(ex);
                        pnlListMethod.Update();
                        return;
                    }
                    dlgShowBalance.BindAndShow();
                    break;
                case "Edit":
                    MethodData = Netpay.Bll.Accounts.StoredPaymentMethod.Load(((string)e.CommandArgument).EmptyIfNull().ToInt32(0));
                    if (MethodData == null) return;
                    dlgEditMethod.DataBind();

                    PaymentMethod_Changed(this, new EditEvent() { Name = "edit" });
                    txtEncValue1.Text = (MethodData.MethodInstance.Value1First6).Substring(0, 4) + "..." + MethodData.MethodInstance.Value1Last4;

                    // don't allow changing the credit card
                    txtEncValue1.Enabled = false;

                    // don't allow changing payment method
                    ddlPaymentMethod.Enabled = false;

                    // don't allow changing value 2
                    txtEncValue2.Text = "***";
                    txtEncValue2.Enabled = false;

                    ////////////////////////////////
                    ValidateByGroup("creditcard");
                    //////////////////////////////////
                    dlgEditMethod.RegisterShow();
                    break;
                case "AddItem":
                    //Check for Add permission
                    if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                        ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Accounts.StoredPaymentMethod.SecuredObject, PermissionValue.Add);

                    MethodData = new Netpay.Bll.Accounts.StoredPaymentMethod();
                    MethodData.MethodInstance.PaymentMethodId = Netpay.CommonTypes.PaymentMethodEnum.Unknown;
                    dlgEditMethod.DataBind();
                    PaymentMethod_Changed(this, new EditEvent());
                    txtEncValue1.Enabled = true;
                    txtEncValue1.Text = txtEncValue2.Text = string.Empty;
                    ////////////////////////////////
                    ValidateByGroup("creditcard");
                    //////////////////////////////////
                    dlgEditMethod.RegisterShow();
                    break;
                case "Delete":
                    MethodData = Netpay.Bll.Accounts.StoredPaymentMethod.Load(((string)e.CommandArgument).EmptyIfNull().ToInt32(0));
                    MethodData.Delete();
                    dlgEditMethod.RegisterHide();
                    pnlListMethod.BindAndUpdate();
                    break;
                case "Save":
                    try
                    {
                        MethodData = Netpay.Bll.Accounts.StoredPaymentMethod.Load(((string)e.CommandArgument).EmptyIfNull().ToInt32(0));
                        if (MethodData == null)
                        {
                            // following updates supported only for new cards
                            // after adding a card you can't change the number and payment method

                            // new card flow
                            MethodData = new Netpay.Bll.Accounts.StoredPaymentMethod() { Account_id = AccountID.Value };

                            // set the payment method 
                            MethodData.MethodInstance.PaymentMethodId = ddlPaymentMethod.Value.ToNullableEnum<CommonTypes.PaymentMethodEnum>().GetValueOrDefault(CommonTypes.PaymentMethodEnum.Unknown);

                            // set the new card number 
                            MethodData.MethodInstance.SetEncryptedData(txtEncValue1.Text, txtEncValue2.Text);
                        }

                        // following updates supported for edit/add mode
                        MethodData.Title = txtTitle.Text;
                        MethodData.OwnerName = txtOwnerName.Text;
                        MethodData.MethodInstance.ExpirationDate = new DateTime(ddlExpYear.SelectedYear.GetValueOrDefault(1900), ddlExpMonth.SelectedMonth.GetValueOrDefault(1), 1).AddMonths(1).AddDays(-1);
                        MethodData.IsDefault = chkIsDefaultMethod.Checked;

                        MethodData.BillingAddress = caAddress.Save(MethodData.BillingAddress);

                        //Check if the BillingAddress property is null , which means that there was no
                        //Country selected and the save function should not be invoked.
                        if (MethodData.BillingAddress == null)
                        {
                            throw new Exception("You can't save Stored PM without choosing country");
                        }
                        else
                        {
                            string PaymentMethodName = Netpay.Bll.PaymentMethods.PaymentMethod.Cache.Where(x => x.ID == (int)MethodData.MethodInstance.PaymentMethodId).Select(x => x.Name).SingleOrDefault();
                            MethodData.PaymentMethodText = PaymentMethodName + " : " + MethodData.MethodInstance.Value1Last4;
                            MethodData.Save();
                            dlgEditMethod.RegisterHide();
                            pnlListMethod.BindAndUpdate();
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.StartsWith("?NotAuthorized"))
                        {
                            throw ex;
                        }
                        EditMethodActionNotify.SetException(ex);
                        dlgEditMethod.UpdatePanel.Update();
                    }
                    break;
            }
        }

        protected void BankAccount_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Edit":
                    //If located at Merchants page , filter currencies list
                    FilterCurrenciesDropDown();
                    BankData = Netpay.Bll.Accounts.BankAccount.Load(((string)e.CommandArgument).EmptyIfNull().ToInt32(0));
                    if (BankData == null) return;
                    dlgEditBankAccount.BindAndShow();
                    break;
                case "AddItem":
                    //Check for permission
                    if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                        ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Accounts.StoredPaymentMethod.SecuredObject, PermissionValue.Add);

                    FilterCurrenciesDropDown();
                    BankData = new Netpay.Bll.Accounts.BankAccount();
                    dlgEditBankAccount.BindAndShow();
                    break;
                case "Delete":
                    BankData = Netpay.Bll.Accounts.BankAccount.Load(((string)e.CommandArgument).EmptyIfNull().ToInt32(0));
                    BankData.Delete();
                    dlgEditBankAccount.RegisterHide();
                    pnlListBankAccounts.BindAndUpdate();
                    break;
                case "Save":
                    BankData = Netpay.Bll.Accounts.BankAccount.Load(((string)e.CommandArgument).EmptyIfNull().ToInt32(0));
                    if (BankData == null)
                    {
                        BankData = new Netpay.Bll.Accounts.BankAccount();
                        BankData.Account_id = AccountID.Value;
                    }

                    //Check if bank account with the same currency exist, invoke check only if the drop down
                    //Has a selected value.
                    if (ddlCurrency.IsSelected)
                    {
                        if (Bll.Accounts.BankAccount.IsAccountHaveAnotherBankAccountWithCurrency(ddlCurrency.SelectedCurrencyIso, AccountID.Value, BankData.ID))
                        {
                            dlgEditBankAccount.RegisterHide();
                            TemplatePage.FormViewGlobalModalDialog.Title = "Error";
                            TemplatePage.FormViewGlobalActionNotify.SetMessage("Account id:" + AccountID.Value +" already have bank account that recieve:"+ddlCurrency.SelectedCurrencyIso, true);
                            TemplatePage.FormViewGlobalModalDialog.RegisterShow();
                            return;
                        }
                    }

                    BankData.AccountName = txtAccountName.Text;
                    BankData.AccountNumber = txtAccountNumber.Text;
                    BankData.ABA = txtABA.Text;
                    BankData.IBAN = txtIBAN.Text;
                    BankData.SwiftNumber = txtSwiftNumber.Text;
                    if (ddlCurrency.IsSelected)
                    {
                        BankData.CurrencyISOCode = ddlCurrency.SelectedCurrencyIso;
                    }
                    else
                    {
                        BankData.CurrencyISOCode = null;
                    }
                    BankData.BankName = txtBankName.Text;
                    BankData.BankStreet1 = txtBankStreet1.Text;
                    BankData.BankStreet2 = txtBankStreet2.Text;
                    BankData.BankCity = txtBankCity.Text;
                    BankData.BankCountryISOCode = ddlBankCountryISOCode.ISOCode;
                    BankData.IsDefault = chkIsDefaultAccount.Checked;
                    BankData.Save();
                    dlgEditBankAccount.RegisterHide();
                    pnlListBankAccounts.BindAndUpdate();
                    break;
            }
        }

        public void FilterCurrenciesDropDown()
        {
            //If located at Merchants page , filter currencies list
            if (AccountPage != null && AccountPage.Account != null && AccountPage.Account.AccountType == Bll.Accounts.AccountType.Merchant)
            {
                //Load the current merchant Settlement settings , in order to know which currencies supported.
                Dictionary<int, Bll.Settlements.MerchantSettings> merchantSettings = Bll.Settlements.MerchantSettings.LoadForMerchant(Account.MerchantID.Value);

                List<int> supportedCurrencies = merchantSettings.Select(x => x.Key).ToList();

                //Check if there is any value
                if (supportedCurrencies.Count > 0)
                {
                    string commaJoinedSupportedCurrencies = string.Join(",", Netpay.Bll.Currency.Cache.Where(x => supportedCurrencies.Contains(x.ID)).Select(y => y.ID.ToString()).ToList());
                    ddlCurrency.IncludeCurrencyIDs = commaJoinedSupportedCurrencies;
                }
            }
        }
    }

    public class EditEvent : EventArgs
    {
        public string Name { get; set; }
    }
}