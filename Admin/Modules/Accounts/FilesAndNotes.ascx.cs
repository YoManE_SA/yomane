﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Admin.Accounts
	{
	public partial class FilesAndNotes : Controls.AccountControlBase
	{
        protected Bll.Accounts.File FileItem { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            rptFiles.DataBinding += delegate { rptFiles.DataSource = Netpay.Bll.Accounts.File.LoadForAccount(AccountID.GetValueOrDefault()); };
            rptNotes.DataBinding += delegate { rptNotes.DataSource = Netpay.Bll.Accounts.Note.LoadForAccount(AccountID.GetValueOrDefault()); };
            base.OnLoad(e);
        }
        
        protected override void DataBindChildren()
		{
            rptFiles.DataBind(); rptNotes.DataBind();
            pnlFiles.BindAndUpdate(); pnlNotes.BindAndUpdate();

            pnlNotes.EnableAdd = (TemplatePage.Page is Controls.AccountPage) && TemplatePage.ItemID != null && TemplatePage.ItemID != 0;
            MultiViewNotes.ActiveViewIndex = (TemplatePage.Page is Controls.AccountPage) && TemplatePage.ItemID != null && TemplatePage.ItemID != 0 ? 0 : 1;

            pnlFiles.EnableAdd = (TemplatePage.Page is Controls.AccountPage) && TemplatePage.ItemID != null && TemplatePage.ItemID != 0;
            MultiViewFiles.ActiveViewIndex = (TemplatePage.Page is Controls.AccountPage) && TemplatePage.ItemID != null && TemplatePage.ItemID != 0 ? 0 : 1;
            //base.DataBindChildren();
        }

        protected void Note_Command(object sender, CommandEventArgs e)
		{
            switch (e.CommandName) { 
            case "AddItem":
                txtNote.Text = "";
                dlgNote.BindAndShow();
                break;
            case "Save":
                Netpay.Bll.Accounts.Note.AddNote(AccountID.GetValueOrDefault(), Netpay.Web.WebUtils.LoggedUser.UserName, txtNote.Text);
                dlgNote.RegisterHide();
                pnlNotes.BindAndUpdate();
                break;
            }
		}

        protected void FileDialogCommand(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "SaveItem":
                    fileData.Save(AccountID.GetValueOrDefault());
                    dlgFile.RegisterHide();
                    pnlFiles.BindAndUpdate();
                    break;
                case "DeleteItem":
                    fileData.Delete();
                    dlgFile.RegisterHide();
                    pnlFiles.BindAndUpdate();
                    break;
                case "DialogClose": // Refresh also in case of DialogClose since approve/decline require a refresh to the list
                    dlgFile.RegisterHide();
                    pnlFiles.BindAndUpdate();
                    break;
            }
        }

        protected void File_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "AddItem":
                    if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                        ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Accounts.File.SecuredObject, PermissionValue.Add);

                    FileItem = new Netpay.Bll.Accounts.File(AccountID.GetValueOrDefault());
                    dlgFile.BindAndShow();
                    pnlFiles.BindAndUpdate();
                    break;
                case "EditItem":
                    FileItem = Netpay.Bll.Accounts.File.Load(e.CommandArgument.ToNullableInt().GetValueOrDefault());
                    if (FileItem != null)
                    {
                        fileData.Item = FileItem;
                        dlgFile.BindAndShow();
                    }
                    break;

                case "Approve":
                    FileItem = Netpay.Bll.Accounts.File.Load(e.CommandArgument.ToNullableInt().GetValueOrDefault());
                    FileItem.SetApprove(false);
                    dlgFile.BindAndUpdate();
                    break;
                case "Decline":
                    FileItem = Netpay.Bll.Accounts.File.Load(e.CommandArgument.ToNullableInt().GetValueOrDefault());
                    FileItem.SetApprove(true);
                    dlgFile.BindAndUpdate();
                    break;

                case "DeleteItem":
                    FileItem = Netpay.Bll.Accounts.File.Load(e.CommandArgument.ToNullableInt().GetValueOrDefault());
                    if (FileItem != null) FileItem.Delete();
                    dlgFile.RegisterHide();
                    pnlFiles.BindAndUpdate();
                    break;
                case "SaveItem":
                    FileItem = Netpay.Bll.Accounts.File.Load(e.CommandArgument.ToNullableInt().GetValueOrDefault());
                    if (FileItem == null)
                    {
                        return;
                    }
                    //if (!fuFile.HasFile) return;
                    //FileItem = new Netpay.Bll.Accounts.File(AccountID.GetValueOrDefault());
                    //FileItem.FileTitle = fuFile.FileName;
                    //}
                    //FileItem.AdminComment = txtFileNote.Text;
                    
                    FileItem.Save();
                    dlgFile.RegisterHide();
                    pnlFiles.BindAndUpdate();
                    break;

            }
        }

	}
}