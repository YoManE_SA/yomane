﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExternalServiceLogin.ascx.cs" Inherits="Netpay.Admin.Modules.Accounts.ExternalServiceLogin" %>
<admin:FormPanel runat="server" ID="pnlExternalLogin" Title="External Service Logins" OnCommand="ESL_Command" EnableAdd="true">
    <NP:PagedRepeater runat="server" ID="rptExternalLogin" OnItemCommand="ESL_Command">
        <HeaderTemplate>
             <table class="table table-hover table-customize">
                <thead>
                    <th>ID</th>
                     <th>Service Type</th>
                     <th>Is Active</th>
                     <th>Method</th>
                     <th>Username</th>
               </thead>
        </HeaderTemplate>
        <ItemTemplate>
            <tbody>
                <td><asp:LinkButton runat="server" Text='<%# Eval("ID") %>' CommandName="Edit" CommandArgument='<%# Eval("ID") %>' /></td>
                <td><%# Eval("ServiceType") %></td> 
                <td><%# Eval("IsActive") %></td> 
                <td><%# Eval("Method") %></td>
                <td><%# Eval("Username") %></td>
            </tbody>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        <EmptyContainer><div class="alert alert-info"><strong>Info!</strong> No record information</div></EmptyContainer>
    </NP:PagedRepeater>
</admin:FormPanel>
<admin:ModalDialog runat="server" ID="dlgExternalLogin" Title="Edit External Service Login" OnCommand="ESL_Command">
    <Body>
        <div class="row ">
            <div class="col-lg-4 col-md-4">
                <div class="form-group">
                    ServiceType
                <netpay:DropDownBase runat="server" ID="ddlServiceType" CssClass="form-control" DataSource='<%# GetServiceTypes() %>' Value='<%# ItemData.ServiceType %>' Enabled='<%# ItemData.ID == 0 %>' />
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="form-group">
                    Method
                    <netpay:DropDownBase runat="server" ID="ddlMethod" CssClass="form-control" DataSource='<%# GetMethodTypes() %>' Value='<%# ItemData.Method %>' />
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <br />
                <asp:CheckBox runat="server" ID="chkIsActive" Value='<%# ItemData.IsActive  %>' Text="IsActive" />
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="form-group">
                    Service Address
                    <asp:TextBox CssClass="form-control" runat="server" ID="txtServerURL" Text='<%# ItemData.ServerURL %>' />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    Username
                    <asp:TextBox CssClass="form-control" runat="server" ID="txtUsername" Text='<%# ItemData.Username %>' />
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    Password
                    <asp:TextBox CssClass="form-control" runat="server" ID="txtPassword" Text='<%# ItemData.Password %>' />
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    Reference 1
                    <asp:TextBox CssClass="form-control" runat="server" ID="txtReference1" Text='<%# ItemData.Reference1 %>' />
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <br />
                <asp:Button runat="server" ID="btnTestConnection" CssClass="btn btn-primary" CommandName="TestCon" CommandArgument='<%# ItemData.ID %>' Text="Start connection Testing" />
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="form-group">
                    <asp:Label CssClass="form-control" runat="server" Style="height: 200px; overflow: scroll; white-space: pre; display: none; margin-top: 15px;" ID="lblTestResult" ViewStateMode="Disabled" />
                </div>
            </div>
            <div class="col-md-12">
                <div class="alert alert-info">
                    (this action may take few minutes to complete, please be patient)
                </div>
            </div>
        </div>

    </Body>
    <Footer>
        <asp:Button runat="server" ID="btnSaveMethod" CssClass="btn btn-primary" CommandName="Save" CommandArgument='<%# ItemData.ID %>' Text="Save" />
        <asp:Button runat="server" ID="btnDeleteMethod" CssClass="btn btn-danger" CommandName="Delete" CommandArgument='<%# ItemData.ID %>' Text="Delete" />
    </Footer>
</admin:ModalDialog>
