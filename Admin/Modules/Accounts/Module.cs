﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Accounts
{
    public class Module : Admin.CoreBasedModule
    {
        public ApplicationMenu filesArchiveMenuItem;
        public override string Name { get { return "Account"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return "Adds FilesAndNotes and StoredPaymentMethods to accounts, also add FilesArchive"; } }

        public Module() : base(Bll.Accounts.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu item
            filesArchiveMenuItem = new ApplicationMenu("Files Archive", "~/FilesArchive/0", null, 3);

            //Register the route
            Application.RegisterRoute("FilesArchive/{*id}", "FilesArchive", typeof(Controls.DataTablePage), module: this);

            //Register the InitTemplate page event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Merchants", filesArchiveMenuItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            filesArchiveMenuItem.Parent.RemoveChild(filesArchiveMenuItem);
            base.OnUninstallAdmin(e);
        }

        public static bool TemplateIsAccountType(string templateName)
        {
            string[] items = { "Merchant", "DebitCompany", "Customer", "Affiliate" };
            return (items.Contains(templateName));
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;

            if (TemplateIsAccountType(page.TemplateName) && (page.ViewName == null))
            {
                page.AddControlToForm("File & Notes", page.LoadControl("~/Modules/Accounts/FilesAndNotes.ascx"), this, "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject, Bll.Accounts.File.SecuredObject).GetActivObject);

                var editLogin = page.LoadControl("~/Modules/Security/EditLogin.ascx");
                editLogin.DataBinding += Login_DataBinding;

                if (page.TemplateName != "DebitCompany" && page.TemplateName != "Affiliate")
                {
                    page.AddControlToForm("Data", editLogin, this, "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject, Infrastructure.Security.Login.SecuredObject).GetActivObject);
                }

                if (page.TemplateName != "DebitCompany")
                {
                    page.AddControlToForm("Stored PM", page.LoadControl("~/Modules/Accounts/StoredPaymentMethods.ascx"), this, "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject, Bll.Accounts.StoredPaymentMethod.SecuredObject).GetActivObject);
                }
                
                if (page.TemplateName == "DebitCompany")
                {
                    page.AddControlToForm("Data", page.LoadControl("~/Modules/Accounts/ExternalServiceLogin.ascx"), page.CurrentModule, "", page.CurrentPageSecuredObject);
                }
            }
            else if (page.TemplateName == "FilesArchive")
            {
                page.PageController.LoadItem += PageController_LoadItem;
                page.AddControlToList(page.LoadControl("~/Modules/Accounts/FilesArchive/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Accounts/FilesArchive/Filter.ascx"));
                page.AddControlToForm("Data", page.LoadControl("~/Modules/Accounts/FilesArchive/Data.ascx"));
            }
        }

        void PageController_LoadItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            page.ItemID = page.PageController.DataKey["ID"].ToNullableInt();
            if (page.ItemID != null) page.SetItemData(Netpay.Bll.Accounts.File.Load(page.ItemID.GetValueOrDefault()));
        }

        private void Login_DataBinding(object sender, EventArgs e)
        {
            var control = sender as Security.EditLogin;

            if ((control.Page as Controls.AccountPage).Account != null)
                control.ItemData = (control.Page as Controls.AccountPage).Account.Login;

            if ((control.Page as Controls.AccountPage).Account != null && (control.Page as Controls.AccountPage).Account.AccountID > 0)
                control.IsVisible = true;
            else
                control.IsVisible = false;
        }
    }
}