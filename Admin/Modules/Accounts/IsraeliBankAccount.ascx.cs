﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Accounts
	{
	public partial class IsraeliBankAccount : System.Web.UI.UserControl
	{
		public string IbanTextField;

		private string IbanText{
			get{ return (this.NamingContainer.FindControl(IbanTextField) as TextBox).Text; }
			set{ (this.NamingContainer.FindControl(IbanTextField) as TextBox).Text = value; }
		}

		public override void DataBind()
		{
			string txt = IbanText;
			if (string.IsNullOrEmpty(txt)) return;
			if (txt.Length != 23 || !txt.StartsWith("IL", StringComparison.InvariantCultureIgnoreCase)) return;
			txt = txt.Replace(" " , "");
			if (ddlBankCode.Items.FindByValue(txt.Substring(4, 3)) != null)
				ddlBankCode.SelectedValue = txt.Substring(4, 3);
			txtBranch.Text = txt.Substring(7, 3);
			txtAccount.Text = txt.Substring(10).TrimStart(new char[] { '0' });
			//base.DataBind();
		}

		protected void UpdateClick(object sender, EventArgs e)
		{
			string prechecked = string.Format("{0}{1}{2}{3}", ddlBankCode.SelectedValue.PadLeft(3, '0'), txtBranch.Text.ToInt32().ToString("000"), txtAccount.Text.ToInt32().ToString("D13"), "IL00");
			string checksumStr = "";
			for (int i = 0; i < prechecked.Length; i++) {
				var c = prechecked[i];
				if((c >= '0') && (c <= '9')) checksumStr += c;
				else if((c >= 'A') && (c <= 'Z')) checksumStr +=  ((c - 'A') + 10);
				else if((c >= 'a') && (c <= 'z')) checksumStr +=  ((c - 'a') + 10);
			}

			int reminder = 0;
			while(checksumStr.Length >= 7){
				reminder = int.Parse(reminder + checksumStr.Substring(0, 7)) % 97;
				checksumStr = checksumStr.Substring(7);
			}
			reminder = int.Parse(reminder + checksumStr) % 97;
			long checksum = (98 - reminder);
			IbanText = string.Format("IL{0}{1}{2}{3}", checksum.ToString("00"), ddlBankCode.SelectedValue.PadLeft(3, '0'), txtBranch.Text.ToInt32().ToString("000"), txtAccount.Text.ToInt32().ToString("D13"));
		}

	}
}