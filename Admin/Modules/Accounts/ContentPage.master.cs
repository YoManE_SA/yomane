﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Accounts
{
	public partial class ContentPage : System.Web.UI.MasterPage
	{
		protected override void OnInit(EventArgs e)
		{
			(Page as Netpay.Web.Admin_AccountPage).AccountChanged += ContentPage_AccountChanged;
			base.OnInit(e);
		}

		private void ContentPage_AccountChanged(object sender, EventArgs e)
		{
			var accountType = (sender as Netpay.Web.Admin_AccountPage).AccountType;
			if (phAccountHeader.Controls.Count > 0) phAccountHeader.Controls.Clear();
			switch (accountType)
			{
				case Netpay.Bll.Accounts.AccountType.Customer:
					phAccountHeader.Controls.Add(LoadControl("../Customers/Header.ascx"));
					break;
				case Netpay.Bll.Accounts.AccountType.Merchant:
					phAccountHeader.Controls.Add(LoadControl("../Merchants/Header.ascx"));
					break;
				case Netpay.Bll.Accounts.AccountType.Affiliate:
					phAccountHeader.Controls.Add(LoadControl("../Affiliates/Header.ascx"));
					break;
				case Netpay.Bll.Accounts.AccountType.DebitCompany:
					phAccountHeader.Controls.Add(LoadControl("../DebitCompany/Header.ascx"));
					break;
			}
		}
	}
}