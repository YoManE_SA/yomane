﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="IsraeliBankAccount.ascx.cs" Inherits="Netpay.Admin.Accounts.IsraeliBankAccount" %>
<input type="button" value="..." class="btn btn-primary" onclick="document.getElementById('IsraeliBankAccount').style.display = ''" />
<div id="IsraeliBankAccount">

    <admin:FormSections runat="server" Title="Israeli IBAN composer">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    Bank
				<asp:DropDownList ID="ddlBankCode" CssClass="form-control" runat="server">
                    <asp:ListItem Value="001">01 - בנק יורו טרייד בע&quot;מ</asp:ListItem>
                    <asp:ListItem Value="004">04 - בנק יהב לעובדי המדינה בע&quot;מ</asp:ListItem>
                    <asp:ListItem Value="007">07 - בנק לפתוח התעשיה בישראל בע&quot;מ</asp:ListItem>
                    <asp:ListItem Value="009">09 - בנק הדואר</asp:ListItem>
                    <asp:ListItem Value="010">10 - בנק לאומי לישראל בע&quot;מ</asp:ListItem>
                    <asp:ListItem Value="011">11 - בנק דיסקונט לישראל בע&quot;מ</asp:ListItem>
                    <asp:ListItem Value="012">12 - בנק הפועלים בע&quot;מ</asp:ListItem>
                    <asp:ListItem Value="013">13 - בנק אגוד לישראל בע&quot;מ</asp:ListItem>
                    <asp:ListItem Value="014">14 - בנק אוצר החייל בע&quot;מ</asp:ListItem>
                    <asp:ListItem Value="017">17 - בנק מרכנתיל דיסקונט בע&quot;מ</asp:ListItem>
                    <asp:ListItem Value="019">19 - בנק החקלאות לישראל בע&quot;מ</asp:ListItem>
                    <asp:ListItem Value="020">20 - בנק המזרחי המאוחד בע&quot;מ</asp:ListItem>
                    <asp:ListItem Value="021">21 - Standard Chartered Bank Ltd</asp:ListItem>
                    <asp:ListItem Value="022">22 - Citibank N.A</asp:ListItem>
                    <asp:ListItem Value="023">23 - HSBC  Bank plc</asp:ListItem>
                    <asp:ListItem Value="026">26 - בנק אינווסטק (ישראל) בע&quot;מ</asp:ListItem>
                    <asp:ListItem Value="028">28 - בנק קונטיננטל לישראל בע&quot;מ</asp:ListItem>
                    <asp:ListItem Value="030">30 - בנק למסחר בע&quot;מ (לבנק מונה מפרק זמני)</asp:ListItem>
                    <asp:ListItem Value="031">31 - הבנק הבינלאומי הראשון לישראל בע&quot;מ</asp:ListItem>
                    <asp:ListItem Value="034">34 - בנק ערבי ישראלי בע&quot;מ</asp:ListItem>
                    <asp:ListItem Value="035">35 - בנק פולסקא קאסא אופיקי תל אביב (בנק פאקאו) בע&quot;מ</asp:ListItem>
                    <asp:ListItem Value="046">46 - בנק מסד בע&quot;מ</asp:ListItem>
                    <asp:ListItem Value="047">47 - בנק עולמי להשקעות (ב.ה.) בע&quot;מ</asp:ListItem>
                    <asp:ListItem Value="048">48 - קופת העובד הלאומי לאשראי וחיסכון נתניה</asp:ListItem>
                    <asp:ListItem Value="052">52 - בנק פועלי אגודת ישראל בע&quot;מ</asp:ListItem>
                    <asp:ListItem Value="054">54 - בנק ירושלים בע&quot;מ</asp:ListItem>
                </asp:DropDownList>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    Branch
		            <asp:TextBox CssClass="form-control" ID="txtBranch" runat="server" />
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    Account
			  <asp:TextBox CssClass="form-control" ID="txtAccount" runat="server" />
                </div>
            </div>
        </div>
    </admin:FormSections>
    <asp:Button runat="server" ID="btnUpdate" Text="Update" OnClick="UpdateClick" CssClass="btn btn-primary" />
</div>
