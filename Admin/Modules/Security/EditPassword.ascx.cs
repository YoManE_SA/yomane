﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Admin.Security
{
    public partial class EditPassword : System.Web.UI.UserControl
    {
        //This property is used in the flow of creating new account login during
        //The click of Add/Change and then the hiddenField is not updated so
        //It is needed to take the login account from the page account entity.
        public int CurrentLoginID
        {
            get
            {
                if (LoginID != 0)
                {
                    return LoginID;
                }
                else if (Page is Controls.AccountPage && (Page as Controls.AccountPage).Account != null)
                {
                    return ((Page as Controls.AccountPage).Account.Login.LoginID);
                }
                else
                {
                    return 0;
                }
            }
        }

        public int LoginID { get { return hfLoginID.Value.ToNullableInt().GetValueOrDefault(); } set { hfLoginID.Value = value.ToString(); } }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }


        protected override void DataBindChildren()
        {
            txtPassword.Text = "";
            txtConfirmPassword.Text = "";
            acnPasswordMessage.ClearMessage();

            base.DataBindChildren();
        }

        protected void ChangePassword_SaveClick(object sender, EventArgs e)
        {
            if (txtPassword.Text != txtConfirmPassword.Text)
            {
                ResetLockNotify.SetMessage("Passwords must be the same.", true);
                dlgResetLock.RegisterShow();
            }
            else
            {
                var ret = Infrastructure.Security.Login.SetPassword(CurrentLoginID, txtPassword.Text, Request.UserHostAddress);
                if (ret == Infrastructure.Security.Login.ChangePasswordResult.Success)
                {
                    acnPasswordMessage.SetSuccess("Password changed");
                    dlgChangePassword.BindAndUpdate();
                }
                else acnPasswordMessage.SetMessage("Unable to change password: " + ret.ToString(), true);
            }
        }

        protected void ResetLock_Click(object sender, EventArgs e)
        {
            if (CurrentLoginID == 0)
            {
                ResetLockNotify.SetMessage("Please insert email and username and add password.", true);
                dlgResetLock.RegisterShow();
                return;
            }

            Infrastructure.Security.Login.ResetLock(CurrentLoginID, Request.UserHostAddress);
            ResetLockNotify.SetSuccess("Lock was successfully reset.");
            dlgResetLock.RegisterShow();
        }

        protected void btnResetPassword_Click(object sender, EventArgs e)
        {
            if (CurrentLoginID == 0)
            {
                ResetLockNotify.SetMessage("Please insert email, username and add password.", true);
                dlgResetLock.RegisterShow();
                return;
            }

            //Get the login entity
            var login = Infrastructure.Security.Login.LoadLogin(CurrentLoginID);
            if (login == null)
            {
                ResetLockNotify.SetMessage("Login account wasn't found.", true);
                dlgResetLock.RegisterShow();
                return;
            }

            //Reset the password and send mail
            bool isReset = false;
            try
            {
                isReset = login.ResetPassword(Request.UserHostAddress);
            }
            catch
            {
                (Page as Controls.DataTablePage).SetFormViewMessage("Problem occured while trying to reset password", true);
            }
            if (isReset)
            {
                try
                {
                    login.SendLoginAccountPasswordResetMail();
                    (Page as Controls.DataTablePage).PageController.ListView.BindAndUpdate();
                    (Page as Controls.DataTablePage).PageController.FormView.BindAndUpdate();
                    (Page as Controls.DataTablePage).SetFormViewMessage("New password generated and sent by mail.", false);
                }
                catch (Exception ex)
                {
                    Logger.Log(ex);
                    (Page as Controls.DataTablePage).SetFormViewMessage("New password generated, but error occured while sending mail with the generated password.", true);
                }
            }
        }
               
        protected void AddChange_Click(object sender, EventArgs e)
        {
            if (!ObjectContext.Current.IsCurrentLoginAdminOfAdmins) return;

            if (CurrentLoginID == 0) //need to create new AccountLogin entity.
            {
                //Check if located on AdminUsers page
                if (Page is Controls.DataTablePage && (Page as Controls.DataTablePage).TemplateName == "AdminUsers")
                {
                    ResetLockNotify.SetMessage("You need to save the new AdminUser before setting password.", true);
                    dlgResetLock.RegisterShow();
                    return;
                }

                //Get params from Login Information form section
                //In order to create new AccountLogin entity.
                string email = (((sender as Admin.Controls.GlobalControls.LinkButton).Parent as Netpay.Admin.Security.EditPassword).Parent.FindControl("txtEmailAddress") as TextBox).Text;
                string username = (((sender as Admin.Controls.GlobalControls.LinkButton).Parent as Netpay.Admin.Security.EditPassword).Parent.FindControl("txtUserName") as TextBox).Text;
                bool isactive = (((sender as Admin.Controls.GlobalControls.LinkButton).Parent as Netpay.Admin.Security.EditPassword).Parent.FindControl("chkIsActive") as CheckBox).Checked;

                //Check for empty values
                if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(username))
                {
                    ResetLockNotify.SetMessage("You can't set password without username and email.", true);
                    dlgResetLock.RegisterShow();
                }
                else
                {
                    //Create a new AccountLogin entity issued with the current page Account entity.
                    if ((Page is Controls.AccountPage) && (Page as Controls.AccountPage).Account != null)
                    {
                        var CurrentAccount = (Page as Controls.AccountPage).Account;

                        try
                        {
                            CurrentAccount.Login.EmailAddress = email;
                            CurrentAccount.Login.UserName = username;
                            CurrentAccount.Login.IsActive = isactive;
                            CurrentAccount.SaveAccount();
                        }

                        catch (Exception ex)
                        {
                            ResetLockNotify.SetException(ex);
                            dlgResetLock.RegisterShow();
                            return;
                        }

                        dlgChangePassword.RegisterShow();
                    }
                }
            }

            else // AccountLogin entity already exist.
            {
                dlgChangePassword.RegisterShow();
            }
        }
    }
}