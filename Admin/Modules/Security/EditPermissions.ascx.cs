﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;


namespace Netpay.Admin.Security
{
    public partial class EditPermissions : System.Web.UI.UserControl
    {
        public enum ViewMode { ByModule, ByUser, ByGroup };

        public int? UserLoginId { get; set; }

        public string Filter { get { return hfFilter.Value; } set { hfFilter.Value = value; } }
        public ViewMode Mode { get; set; }

        private List<PermissionRow> _rows;

        public class PermissionRow
        {
            //Constructors
            public PermissionRow(PermissionRow obj, Infrastructure.Security.PermissionValue? value) { Name = obj.Name; ObjectID = obj.ObjectID; Value = value; }
            public PermissionRow(int objectId, string name) { ObjectID = objectId; Name = name; Value = null; }

            public int? UserLoginId { get; set; }
            public ViewMode Mode { get; set; }
            public Infrastructure.Security.PermissionValue? Value { get; set; }
            public bool IsGroupMode { get; set; }

            public int ObjectID { get; set; }
            public string Name { get; set; }

            public bool getValue(Infrastructure.Security.PermissionValue item) { return (Value.GetValueOrDefault(Infrastructure.Security.PermissionValue.None) & item) != Infrastructure.Security.PermissionValue.None; }
            public void setValue(Infrastructure.Security.PermissionValue item, bool value) { Value = value ? (Value.GetValueOrDefault(Infrastructure.Security.PermissionValue.None) | item) : (Value.GetValueOrDefault(Infrastructure.Security.PermissionValue.None) & ~item); }

            public bool IsInherit
            {
                get
                {
                    //Group mode never inherit
                    if (Mode == ViewMode.ByGroup)
                    {
                        return false;
                    }

                    //User inherit only if there is no securedObject issued with the user's login account id 
                    else if (Mode == ViewMode.ByUser && UserLoginId.HasValue)
                    {
                        if (Value == null) return true;

                        if (Infrastructure.Security.Permission.IsAnySecuredObjectForLogin(UserLoginId.Value, ObjectID))
                        {
                            var value = Infrastructure.Security.Permission.LoadForLogin(UserLoginId.Value).Where(x => x.ObjectID == ObjectID).SingleOrDefault();
                            if (value != null && value.Value == Value)
                            {
                                return false;
                            }
                            else
                            {
                                return Infrastructure.Security.Group.GetAdminUserPermissionsForSecuredObjectByGroups(UserLoginId.Value, ObjectID) == Value;
                            }
                        }
                        else
                        {
                            return Infrastructure.Security.Group.GetAdminUserPermissionsForSecuredObjectByGroups(UserLoginId.Value, ObjectID) == Value;
                        }
                    }
                    else
                    {
                        return (Value == null);
                    }
                }
                set
                {
                    Value = null;
                }
            }

            public bool IsExecute { get { return getValue(Infrastructure.Security.PermissionValue.Execute); } set { setValue(Infrastructure.Security.PermissionValue.Execute, value); } }
            public bool IsAdd { get { return getValue(Infrastructure.Security.PermissionValue.Add); } set { setValue(Infrastructure.Security.PermissionValue.Add, value); } }
            public bool IsRead
            {
                get
                {
                    return getValue(Infrastructure.Security.PermissionValue.Read);
                }
                set
                {
                    setValue(Infrastructure.Security.PermissionValue.Read, value);
                }
            }

            public bool IsEdit
            {
                get
                {
                    return getValue(Infrastructure.Security.PermissionValue.Edit);
                }
                set
                {
                    setValue(Infrastructure.Security.PermissionValue.Edit, value);
                }
            }

            public bool IsDelete { get { return getValue(Infrastructure.Security.PermissionValue.Delete); } set { setValue(Infrastructure.Security.PermissionValue.Delete, value); } }
            public override string ToString()
            {
                if (Value == null) return null;
                return string.Format("{0}={1}", ObjectID, Infrastructure.Security.SecuredObject.ValueToString(Value.Value));
            }
            public static string ToString(List<PermissionRow> values) { return string.Join(",", values); }
            public static void Parse(List<PermissionRow> objects, string value)
            {
                var values = value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var o in objects) o.Value = null;
                foreach (var v in values)
                {
                    var data = v.Split('=');
                    var obj = objects.Where(o => o.ObjectID == data[0].ToNullableInt().GetValueOrDefault()).SingleOrDefault();
                    if (obj != null) obj.Value = Infrastructure.Security.SecuredObject.ParseString(data[1]);
                }
            }
        }

        private List<PermissionRow> Rows
        {
            get
            {
                if (_rows != null) return _rows;
                if (Mode == ViewMode.ByModule)
                {
                    List<Infrastructure.Security.SecuredObject> lst = null;
                    if (string.IsNullOrEmpty(Filter)) lst = Infrastructure.Security.SecuredObject.AdminSecuredObjects;
                    else lst = Infrastructure.Security.SecuredObject.LoadForModule(Filter);
                    _rows = lst.Select(s => new PermissionRow(s.ID, (string.IsNullOrEmpty(Filter) ? Infrastructure.Module.Get(s.ModuleID).Name + " - " : "") + s.Name)).ToList();
                }
                else if (Mode == ViewMode.ByGroup)
                {
                    List<Infrastructure.Security.SecuredObject> lst = Infrastructure.Security.SecuredObject.AdminSecuredObjects;
                    _rows = lst.Select(s => new PermissionRow(s.ID, (Infrastructure.Module.Get(s.ModuleID).Name + " - ") + s.Name) { Mode = Mode }).ToList();
                }
                else if (Mode == ViewMode.ByUser)
                {
                    List<Infrastructure.Security.SecuredObject> lst = Infrastructure.Security.SecuredObject.AdminSecuredObjects;
                    _rows = lst.Select(s => new PermissionRow(s.ID, (Infrastructure.Module.Get(s.ModuleID).Name + " - ") + s.Name) { Mode = Mode, UserLoginId = UserLoginId }).ToList();
                }

                if (IsPostBack)
                {
                    foreach (RepeaterItem item in rptPermissions.Items)
                    {
                        var itemId = (item.FindControl("hfID") as HiddenField).Value.ToNullableInt().GetValueOrDefault();
                        int? hfLoginUserId = null;

                        if (Mode == ViewMode.ByUser)
                        {
                            if ((Page as Controls.DataTablePage).ItemID.HasValue)
                            {
                                hfLoginUserId = Infrastructure.Security.AdminUser.Load((Page as Controls.DataTablePage).ItemID.Value).Login.LoginID;
                            }
                        }
                        var value = Rows.Where(o => o.ObjectID == itemId).SingleOrDefault();
                        if (value == null) continue;

                        //Check if no row has been selected
                        else if ((!(item.FindControl("chkAdd") as CheckBox).Checked) &&
                                 (!(item.FindControl("chkRead") as CheckBox).Checked) &&
                                 (!(item.FindControl("chkWrite") as CheckBox).Checked) &&
                                 (!(item.FindControl("chkDelete") as CheckBox).Checked))
                        {
                            value.Value = null;
                            value.UserLoginId = hfLoginUserId;
                            continue;
                        }

                        value.IsAdd = (item.FindControl("chkAdd") as CheckBox).Checked;
                        value.IsRead = ((item.FindControl("chkAdd") as CheckBox).Checked || (item.FindControl("chkWrite") as CheckBox).Checked) ? true : (item.FindControl("chkRead") as CheckBox).Checked;                                                    //(item.FindControl("chkRead") as CheckBox).Checked;
                        value.IsEdit = (item.FindControl("chkAdd") as CheckBox).Checked ? true : (item.FindControl("chkWrite") as CheckBox).Checked;
                        value.IsDelete = (item.FindControl("chkDelete") as CheckBox).Checked;
                        value.Mode = Mode;
                        value.UserLoginId = hfLoginUserId;
                    }
                }

                return _rows;
            }
        }

        protected override void DataBindChildren()
        {
            rptPermissions.DataSource = Rows;
            base.DataBindChildren();
        }

        public string GetDataAsString()
        {
            return PermissionRow.ToString(Rows);
        }

        public void SetData(string value)
        {
            PermissionRow.Parse(Rows, value);
            DataBindChildren();
        }

        public System.Collections.Generic.Dictionary<int, Infrastructure.Security.PermissionValue?> GetData(int? loginid = null)
        {
            if (Mode == ViewMode.ByUser)
            {
                //Considering this function is only invoked from save operation and now in ByUser mode.
                //If there is a row that IsInherit = true and also has an object for that user with the same id
                //This object for the user should be deleted , beacuse the value should come from the group.

                if (loginid.HasValue)
                {
                    //Get current user objects ids.
                    List<int> UserObjectsIds = Infrastructure.Security.Permission.LoadForLogin(loginid.Value).Select(x => x.ObjectID).ToList();
                    if (UserObjectsIds.Count > 0)
                    {
                        //Check if there is a row that IsInherit = true and the user have an object with the same id
                        //If that row "IsInherit=true" that means that the user object and the group object
                        //Are the same , so the user object can be deleted and stay only with the group.
                        if (Rows.Any(x => x.IsInherit && UserObjectsIds.Contains(x.ObjectID)))
                        {
                            //Delete the objects that should inherit and not be declared for the user.
                            Infrastructure.Security.Permission.DeleteUserObjects(Rows.Where(x => x.IsInherit && UserObjectsIds.Contains(x.ObjectID)).Select(x => x.ObjectID).ToList());
                        }
                    }
                }

                return Rows.Where(x => x.IsInherit == false || x.Value == null).ToDictionary(n => n.ObjectID, n => n.Value);
            }
            else
            {
                return Rows.ToDictionary(n => n.ObjectID, n => n.Value);
            }
        }

        public void SetData(System.Collections.Generic.Dictionary<int, Infrastructure.Security.PermissionValue?> values, int? loginId = null)
        {
            foreach (var r in Rows)
            {
                Infrastructure.Security.PermissionValue? value;
                if (!values.TryGetValue(r.ObjectID, out value))
                {
                    if (Mode == ViewMode.ByUser)
                    {
                        //Loading user data, if no row was found at the SecurityObjectToLoginAccount 
                        //Table , should load the value of the groups that the current user issued
                        //With and Check the inherit checkbox.
                        //value = Infrastructure.Security.Group.GetAdminUserPermissionsForSecuredObjectByGroups(loginId.Value, r.ObjectID);
                        value = null;
                    }
                    else
                    {
                        value = null;
                    }
                }
                r.Value = value;
            }
            DataBindChildren();
        }

        //by module
        public void LoadToUser(int loginId)
        {
            if (Mode != ViewMode.ByModule && Mode != ViewMode.ByUser) throw new Exception("wrong ViewMode");
            SetData(Infrastructure.Security.Permission.LoadForLogin(loginId).ToDictionary(d => d.ObjectID, d => (Infrastructure.Security.PermissionValue?)d.Value), loginId);
        }

        public void LoadToGroup(int groupId)
        {
            if (Mode != ViewMode.ByModule && Mode != ViewMode.ByGroup) throw new Exception("wrong ViewMode");
            SetData(Infrastructure.Security.Permission.LoadForGroup(groupId).ToDictionary(d => d.ObjectID, d => (Infrastructure.Security.PermissionValue?)d.Value));
        }

        public void SaveToUser(int loginId, string value = null)
        {
            if (Mode != ViewMode.ByModule && Mode != ViewMode.ByUser) throw new Exception("wrong ViewMode");
            if (value != null) PermissionRow.Parse(Rows, value);
            Infrastructure.Security.Permission.SetToLogin(loginId, GetData(loginId));
        }

        public void SaveToGroup(int loginId, string value = null)
        {
            if (Mode != ViewMode.ByModule && Mode != ViewMode.ByGroup) throw new Exception("wrong ViewMode");
            if (value != null) PermissionRow.Parse(Rows, value);
            Infrastructure.Security.Permission.SetToGroup(loginId, GetData());
        }

        protected void CheckedChanged(object sender, EventArgs e)
        {
            UpdatePanel currentRowUpdatePanel = (sender as CheckBox).Parent.Parent as UpdatePanel;
            CheckBox currentCheckBoxChecked = (sender as CheckBox);
            RepeaterItem currentRepeaterItem = (sender as CheckBox).Parent.NamingContainer as RepeaterItem;

            if (Mode == ViewMode.ByUser)
            {
                //If no checkbox is checked - check the inherit
                if (!GetRepeaterItemCheckBoxs(currentRepeaterItem).Any(x => x.Checked))
                {
                    (currentRepeaterItem.FindControl("chkInherit") as CheckBox).Checked = true;
                }

                //If Inherit was checked - uncheck all others
                else if (currentCheckBoxChecked.ID == "chkInherit" && currentCheckBoxChecked.Checked)
                {
                    foreach (var checkbox in GetRepeaterItemCheckBoxs(currentRepeaterItem))
                    {
                        if (checkbox.ID != "chkInherit") checkbox.Checked = false;
                    }
                }

                //If other check box was checked - and its not inherit checkbox , set the checkbo'x logic and also uncheck the isInherit
                else if (currentCheckBoxChecked.ID != "chkInherit" && GetRepeaterItemCheckBoxsExceptInherit(currentRepeaterItem).Any(x => x.Checked))
                {
                    (currentRepeaterItem.FindControl("chkInherit") as CheckBox).Checked = false;
                    SetRepeaterItemPermissions(currentRepeaterItem);
                }
            }
            else if (Mode == ViewMode.ByGroup)
            {
                SetRepeaterItemPermissions(currentRepeaterItem);
            }

            currentRowUpdatePanel.Update();
        }

        public static List<string> permissionCheckBoxIds
        {
            get
            {
                return new List<string>() { "chkAdd", "chkRead", "chkWrite", "chkDelete" };
            }
        }

        public static List<string> AllPermissionCheckBoxIds
        {
            get
            {
                return new List<string>() { "chkInherit", "chkAdd", "chkRead", "chkWrite", "chkDelete" };
            }
        }

        public List<CheckBox> GetRepeaterItemCheckBoxs(RepeaterItem item)
        {
            var list = new List<CheckBox>();
            foreach (var id in AllPermissionCheckBoxIds)
            {
                list.Add(item.FindControl(id) as CheckBox);
            }
            return list;
        }

        public List<CheckBox> GetRepeaterItemCheckBoxsExceptInherit(RepeaterItem item)
        {
            var list = new List<CheckBox>();
            foreach (var id in permissionCheckBoxIds)
            {
                list.Add(item.FindControl(id) as CheckBox);
            }
            return list;
        }

        public bool IsInheritChecked(RepeaterItem item)
        {
            return (item.FindControl("chkInherit") as CheckBox).Checked;
        }

        protected void General_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Inherit":

                    foreach (RepeaterItem item in rptPermissions.Items)
                    {
                        (item.FindControl("chkInherit") as CheckBox).Checked = true;

                        foreach (var checkbox in GetRepeaterItemCheckBoxsExceptInherit(item))
                        {
                            checkbox.Checked = false;
                        }
                    }
                    break;

                case "Add":

                    if (Mode == ViewMode.ByGroup)
                    {
                        foreach (RepeaterItem item in rptPermissions.Items)
                        {
                            (item.FindControl("chkAdd") as CheckBox).Checked = true;
                            SetRepeaterItemPermissions(item);
                        }
                    }
                    else if (Mode == ViewMode.ByUser)
                    {
                        foreach (RepeaterItem item in rptPermissions.Items)
                        {
                            (item.FindControl("chkInherit") as CheckBox).Checked = false;
                            (item.FindControl("chkAdd") as CheckBox).Checked = true;
                            SetRepeaterItemPermissions(item);
                        }
                    }
                    break;
                case "Read":
                    if (Mode == ViewMode.ByGroup)
                    {
                        foreach (RepeaterItem item in rptPermissions.Items)
                        {
                            (item.FindControl("chkRead") as CheckBox).Checked = true;
                            SetRepeaterItemPermissions(item);
                        }
                    }
                    else if (Mode == ViewMode.ByUser)
                    {
                        foreach (RepeaterItem item in rptPermissions.Items)
                        {
                            (item.FindControl("chkInherit") as CheckBox).Checked = false;
                            (item.FindControl("chkRead") as CheckBox).Checked = true;
                            SetRepeaterItemPermissions(item);
                        }
                    }
                    break;

                case "Edit":

                    if (Mode == ViewMode.ByGroup)
                    {
                        foreach (RepeaterItem item in rptPermissions.Items)
                        {
                            (item.FindControl("chkWrite") as CheckBox).Checked = true;
                            SetRepeaterItemPermissions(item);
                        }
                    }
                    else if (Mode == ViewMode.ByUser)
                    {
                        foreach (RepeaterItem item in rptPermissions.Items)
                        {
                            (item.FindControl("chkInherit") as CheckBox).Checked = false;
                            (item.FindControl("chkWrite") as CheckBox).Checked = true;
                            SetRepeaterItemPermissions(item);
                        }
                    }
                    break;

                case "Delete":
                    if (Mode == ViewMode.ByGroup)
                    {
                        foreach (RepeaterItem item in rptPermissions.Items)
                        {
                            (item.FindControl("chkDelete") as CheckBox).Checked = true;
                            SetRepeaterItemPermissions(item);
                        }
                    }
                    else if (Mode == ViewMode.ByUser)
                    {
                        foreach (RepeaterItem item in rptPermissions.Items)
                        {
                            (item.FindControl("chkInherit") as CheckBox).Checked = false;
                            (item.FindControl("chkDelete") as CheckBox).Checked = true;
                            SetRepeaterItemPermissions(item);
                        }
                    }
                    break;
            }
            
            UpInherit.Update();
        }

        public void SetRepeaterItemPermissions(RepeaterItem item)
        {
            (item.FindControl("chkRead") as CheckBox).Checked = ((item.FindControl("chkAdd") as CheckBox).Checked || (item.FindControl("chkWrite") as CheckBox).Checked) ? true : (item.FindControl("chkRead") as CheckBox).Checked;                                                    //(item.FindControl("chkRead") as CheckBox).Checked;
            (item.FindControl("chkWrite") as CheckBox).Checked = (item.FindControl("chkAdd") as CheckBox).Checked ? true : (item.FindControl("chkWrite") as CheckBox).Checked;
            (item.FindControl("chkDelete") as CheckBox).Checked = (item.FindControl("chkDelete") as CheckBox).Checked;
        }
    }
}