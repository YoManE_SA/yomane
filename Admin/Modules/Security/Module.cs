﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Security
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuAdminUsers, menuAdminGroups;
        public Module() : base(Infrastructure.Security.SecurityModule.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu
            menuAdminUsers = new ApplicationMenu("Admin Users", "~/Settings/AdminUsers/0", null, 210);
            menuAdminGroups = new ApplicationMenu("Admin Groups", "~/Settings/AdminGroups/0", null, 215);

            //Register route
            //Changing the Template name "AdminUsers" will cause bugs ! Don't change it.
            Application.RegisterRoute("Settings/AdminUsers/{*id}", "AdminUsers", typeof(Controls.DataTablePage), module: this);
            Application.RegisterRoute("Settings/AdminGroups/{*id}", "AdminGroups", typeof(Controls.DataTablePage), module: this);

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Settings", menuAdminUsers);
            Application.AddMenuItem("Settings", menuAdminGroups);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuAdminUsers.Parent.RemoveChild(menuAdminUsers);
            menuAdminGroups.Parent.RemoveChild(menuAdminGroups);
            base.OnUninstallAdmin(e);
        }

        private void Application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
            {
                return;
            }

            var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "AdminUsers")
            {
                page.PageController.LoadItem += PageController_LoadItem;
                page.PageController.NewItem += PageController_NewAdminUserItem;
                page.PageController.PostSaveItem += PageController_PostSaveAdminUserItem;
                page.AddControlToList(page.LoadControl("~/Modules/Security/AdminUsers/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Security/AdminUsers/Filter.ascx"));
                page.AddControlToForm("Data", page.LoadControl("~/Modules/Security/AdminUsers/Data.ascx"));
                page.AddControlToForm("Permissions", page.LoadControl("~/Modules/Security/AdminUsers/Permissions.ascx"));
            }
            else if (page.TemplateName == "AdminGroups")
            {
                page.PageController.LoadItem += PageController_LoadAdminGroupItem;
                page.PageController.NewItem += PageController_NewAdminGroupItem;
                page.PageController.PostSaveItem += PageController_PostSaveAdminGroupItem;
                page.AddControlToList(page.LoadControl("~/Modules/Security/AdminGroups/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Security/AdminGroups/Filter.ascx"));
                page.AddControlToForm("Data", page.LoadControl("~/Modules/Security/AdminGroups/Data.ascx"));
                page.AddControlToForm("Permissions", page.LoadControl("~/Modules/Security/AdminGroups/Permissions.ascx"));
            }
        }

        private void PageController_PostSaveAdminUserItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            var item = page.GetItemData<Infrastructure.Security.AdminUser>();
            if (item == null) return;

            page.PageController.DataKey = new System.Web.UI.WebControls.DataKey(new System.Collections.Specialized.OrderedDictionary() { { "ID", item.ID } });
        }

        private void PageController_PostSaveAdminGroupItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            var item = page.GetItemData<Infrastructure.Security.Group>();
            if (item == null) return;

            page.PageController.DataKey = new System.Web.UI.WebControls.DataKey(new System.Collections.Specialized.OrderedDictionary() { { "ID", item.ID } }); 
        }

        private void PageController_LoadItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            page.ItemID = page.PageController.DataKey["ID"].ToNullableInt();
            if (page.ItemID != null) page.SetItemData(Infrastructure.Security.AdminUser.Load(page.ItemID.GetValueOrDefault()));
            //if (page.GetItemData<Bll.Transaction.RefundRequest>() == null) page.SetItemData(new Bll.Transaction.RefundRequest());
        }

        private void PageController_LoadAdminGroupItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            page.ItemID = page.PageController.DataKey["ID"].ToNullableInt();
            if (page.ItemID != null)
            {
                page.SetItemData(Infrastructure.Security.Group.Load(page.ItemID.GetValueOrDefault()));
            }
            if (page.GetItemData<Infrastructure.Security.Group>() == null) page.SetItemData(new Infrastructure.Security.Group(""));
        }

        private void PageController_NewAdminGroupItem(object sender, EventArgs e)
        {
            if (!ObjectContext.Current.IsCurrentLoginAdminOfAdmins) throw new MethodAccessDeniedException();

            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            page.SetItemData(new Infrastructure.Security.Group(""));
        }

        private void PageController_NewAdminUserItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            page.SetItemData(new Infrastructure.Security.AdminUser());
        }
    }
}