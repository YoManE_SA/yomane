﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditPermissions.ascx.cs" Inherits="Netpay.Admin.Security.EditPermissions" %>
<asp:HiddenField runat="server" ID="hfFilter" />
<asp:UpdatePanel ID="UpInherit" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
    <ContentTemplate>
        <netpay:DynamicRepeater runat="server" ID="rptPermissions">
            <HeaderTemplate>
                <asp:MultiView runat="server" ActiveViewIndex='<%# Mode == ViewMode.ByGroup? 1 : 0 %>'>
                    <asp:View runat="server">
                        <div class="row inner-table-titles">
                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">Select all</div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <asp:Button CssClass="btn btn-default" CommandName="Inherit" OnCommand="General_Command" runat="server" Text="All" />
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <asp:Button CssClass="btn btn-default" CommandName="Add" OnCommand="General_Command" runat="server" Text="All" />
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <asp:Button CssClass="btn btn-default" CommandName="Read" OnCommand="General_Command" runat="server" Text="All" />
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <asp:Button CssClass="btn btn-default" CommandName="Edit" OnCommand="General_Command" runat="server" Text="All" />
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <asp:Button CssClass="btn btn-default" CommandName="Delete" OnCommand="General_Command" runat="server" Text="All" />
                            </div>
                        </div>
                        <div class="row inner-table-titles">
                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">Item</div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">Inherit</div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">Add</div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">Read</div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">Edit</div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">Delete</div>
                        </div>
                    </asp:View>
                    <asp:View runat="server">
                        <div class="row inner-table-titles">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">Select all</div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <asp:Button CssClass="btn btn-default" CommandName="Add" OnCommand="General_Command" runat="server" Text="All" />
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <asp:Button CssClass="btn btn-default" CommandName="Read" OnCommand="General_Command" runat="server" Text="All" />
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <asp:Button CssClass="btn btn-default" CommandName="Edit" OnCommand="General_Command" runat="server" Text="All" />
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <asp:Button CssClass="btn btn-default" CommandName="Delete" OnCommand="General_Command" runat="server" Text="All" />
                            </div>
                        </div>
                        <div class="row inner-table-titles">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">Item</div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">Add</div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">Read</div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">Edit</div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">Delete</div>
                        </div>
                    </asp:View>
                </asp:MultiView>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                    <ContentTemplate>
                        <div class="row table-ui">
                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                <asp:Label runat="server" Text='<%# Eval("Name") %>' />
                                <asp:HiddenField runat="server" ID="hfID" Value='<%# Eval("ObjectID") %>' />
                                <asp:HiddenField runat="server" ID="hfLoginUserId" Value='<%# UserLoginId %>' />
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <asp:CheckBox AutoPostBack="true" OnCheckedChanged="CheckedChanged" Text=" " runat="server" ID="chkInherit" Checked='<%# Eval("IsInherit") %>' Visible='<%# Mode != ViewMode.ByGroup %>' />
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <asp:CheckBox AutoPostBack="true" OnCheckedChanged="CheckedChanged" Text=" " runat="server" ID="chkAdd" Checked='<%# Eval("IsAdd") %>' />
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <asp:CheckBox AutoPostBack="true" OnCheckedChanged="CheckedChanged" Text=" " runat="server" ID="chkRead" Checked='<%# Eval("IsRead") %>' />
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <asp:CheckBox AutoPostBack="true" OnCheckedChanged="CheckedChanged" Text=" " runat="server" ID="chkWrite" Checked='<%# Eval("IsEdit") %>' />
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <asp:CheckBox AutoPostBack="true" OnCheckedChanged="CheckedChanged" Text=" " runat="server" ID="chkDelete" Checked='<%# Eval("IsDelete") %>' />
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ItemTemplate>
        </netpay:DynamicRepeater>
    </ContentTemplate>
</asp:UpdatePanel>


