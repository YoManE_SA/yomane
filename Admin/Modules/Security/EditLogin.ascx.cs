﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Security
{
	public partial class EditLogin : Controls.TemplateControlBase , IValidator 
    {
		public Infrastructure.Security.Login ItemData { get; set; }

        public bool IsVisible { get; set; }

        public bool IsValid
        {
            get
            {
                return ((IValidator)RegularExpressionValidatortxtEmailAddress).IsValid;
            }

            set
            {
                ((IValidator)RegularExpressionValidatortxtEmailAddress).IsValid = value;
            }
        }

        public string ErrorMessage
        {
            get
            {
                return ((IValidator)RegularExpressionValidatortxtEmailAddress).ErrorMessage;
            }

            set
            {
                ((IValidator)RegularExpressionValidatortxtEmailAddress).ErrorMessage = value;
            }
        }

        protected override void OnLoad(EventArgs e)
		{
            base.OnLoad(e);
		}

		protected override void DataBindChildren()
		{
			//ItemData = Account as Netpay.Bll.Customers.Customer;
			if (ItemData == null) return;
			base.DataBindChildren();
		}

        public void Save()
        {
            if (ItemData != null)
            {
                ItemData.UserName = txtUserName.Text;
                ItemData.EmailAddress = txtEmailAddress.Text;
                ItemData.IsActive = chkIsActive.Checked;
            }
        }

        public void Validate()
        {
            ((IValidator)RegularExpressionValidatortxtEmailAddress).Validate();
        }
    }
}