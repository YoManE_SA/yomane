﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Security.AdminUsers
{
    public partial class Data : Controls.TemplateControlBase
    {
        public Infrastructure.Security.AdminUser ItemData { get { return TemplatePage.GetItemData<Infrastructure.Security.AdminUser>(); } set { TemplatePage.SetItemData(value); } }
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += PageController_SaveItem;
            TemplatePage.PageController.DeleteItem += PageController_DeleteItem;
            base.OnLoad(e);
        }

        public Infrastructure.Security.AdminUser CurrentAdminUser
        {
            get
            {
                return Infrastructure.Security.AdminUser.Current;
            }
        }

        protected override void DataBindChildren()
        {
            ItemData = TemplatePage.GetItemData<Infrastructure.Security.AdminUser>();
            if (ItemData == null)
            {
                return;
            }
            else
            {
                chkGroups.DataSource = Infrastructure.Security.Group.Cache.Where(x => x.IsActive == true).ToList();
                base.DataBindChildren();
                foreach (var g in ItemData.Groups)
                {
                    var item = chkGroups.Items.FindByValue(g.ID.ToString());
                    item.Selected = true;
                }
            }
        }

        protected void PageController_SaveItem(object sender, EventArgs e)
        {
            ItemData = Infrastructure.Security.AdminUser.Load(TemplatePage.ItemID.GetValueOrDefault());
            if (ItemData == null)
            {
                //Flow of adding new AdminUser
                ItemData = new Infrastructure.Security.AdminUser();
            }

            ucLogin.ItemData = ItemData.Login;
            ucLogin.Save();

            ItemData.FullName = txtFullName.Text;
            ItemData.NotifyEmail = txtNotifyEmail.Text;
            ItemData.NotifyCellPhone = txtNotifyCellPhone.Text;
            ItemData.IsAdmin = chkIsAdmin.Checked;
            ItemData.IsActive = chkIsActive.Checked;
            ItemData.MultiFactorMode = ddlMultiFactorMode.Text;
            ItemData.Groups.Clear();
            foreach (ListItem i in chkGroups.Items)
                if (i.Selected)
                    ItemData.Groups.Add(Infrastructure.Security.Group.Get(i.Value.ToNullableInt().GetValueOrDefault()));
            ItemData.Save();
            TemplatePage.SetItemData(ItemData);
        }

        private void PageController_DeleteItem(object sender, EventArgs e)
        {
            ItemData = Infrastructure.Security.AdminUser.Load(TemplatePage.ItemID.GetValueOrDefault());
            if (ItemData != null)
                ItemData.Delete();
        }

        protected void chkGroups_SelectedIndexChanged(object sender, EventArgs e)
        {
            ItemData = Infrastructure.Security.AdminUser.Load(TemplatePage.ItemID.GetValueOrDefault());
            if (ItemData == null) return;

            ItemData.Groups.Clear();
            foreach (ListItem i in chkGroups.Items)
                if (i.Selected)
                    ItemData.Groups.Add(Infrastructure.Security.Group.Get(i.Value.ToNullableInt().GetValueOrDefault()));
            ItemData.Save();
            TemplatePage.SetItemData(ItemData);

            if (CurrentAdminUser != null && CurrentAdminUser.ID == ItemData.ID && CurrentAdminUser.Login != null)
            {
                CurrentAdminUser.Groups = Infrastructure.Security.Group.LoadForAdminUser(CurrentAdminUser);
            }

           
            (Page as Controls.DataTablePage).PageController.FormView.BindAndUpdate();
        }
    }
}