﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Data.ascx.cs" Inherits="Netpay.Admin.Security.AdminUsers.Data" %>
<%@ Register Src="~/Modules/Security/EditLogin.ascx" TagPrefix="CUC" TagName="EditLogin" %>
<admin:FormSections runat="server" Title="Details">
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                Full Name
				<asp:TextBox ID="txtFullName" CssClass="form-control" runat="server" Text='<%# ItemData.FullName %>' />
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                Notify Email
				<asp:TextBox ID="txtNotifyEmail" CssClass="form-control" runat="server" Text='<%# ItemData.NotifyEmail %>' />
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                Cell Phone
				<asp:TextBox ID="txtNotifyCellPhone" runat="server" CssClass="form-control" Text='<%# ItemData.NotifyCellPhone %>' />
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                MultiFactor Mode
				<netpay:DropDownBase ID="ddlMultiFactorMode" CssClass="form-control" runat="server" DataSource='<%# new string[] { "Phone", "SMS" } %>' Value='<%# ItemData.MultiFactorMode %>' />
            </div>
        </div>
        <div class="col-lg-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsAdmin" runat="server" Text="IsAdmin" Checked='<%# ItemData.IsAdmin %>' />
                </li>
            </ul>
        </div>
        <div class="col-lg-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsActive" runat="server" Text="IsActive" Checked='<%# ItemData.IsActive.HasValue? ItemData.IsActive : false %>' />
                </li>
            </ul>
        </div>
    </div>

</admin:FormSections>
<admin:FormSections ID="FormSections1" runat="server" Title="Groups">
    <div class="alert alert-info">
        Any change in Groups will be automatically saved and permissions will be changed(if the AdminUser was already saved).
    </div>
    <asp:CheckBoxList AutoPostBack="true" OnSelectedIndexChanged="chkGroups_SelectedIndexChanged" CssClass="checkbox-list" RepeatLayout="OrderedList" runat="server" ID="chkGroups" DataTextField="Name" DataValueField="ID" />
</admin:FormSections>
<CUC:EditLogin ID="ucLogin" IsVisible="true" ItemData='<%# ItemData.Login %>' runat="server" />
