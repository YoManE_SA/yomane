﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Permissions.ascx.cs" Inherits="Netpay.Admin.Security.AdminUsers.Permissions" %>
<%@ Register Src="~/Modules/Security/EditPermissions.ascx" TagPrefix="uc1" TagName="EditPermissions" %>
<admin:FormSections runat="server" Title="Permissions">
    <uc1:EditPermissions Mode="ByUser" runat="server" ID="editPermissions" />
</admin:FormSections>
