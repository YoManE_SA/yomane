﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Security.AdminUsers.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Admin Users">
    <Header>
    </Header>
    <Body>
        <admin:AdminList runat="server" ID="rptList" SaveAjaxState="true" DataKeyNames="ID" AutoGenerateColumns="false" BubbleLoadEvent="true">
            <Columns>
                <asp:BoundField HeaderText="ID" DataField="ID" SortExpression="ID" />
                <asp:BoundField HeaderText="Full Name" DataField="FullName" SortExpression="FullName" />
                <asp:BoundField HeaderText="Notify Email" DataField="NotifyEmail" SortExpression="NotifyEmail" />
                <asp:CheckBoxField Text=" " HeaderText="Is Admin" DataField="IsAdmin" SortExpression="IsAdmin" />
            </Columns>
        </admin:AdminList>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd="true" />
    </Footer>
</admin:ListSection>
