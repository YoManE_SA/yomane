﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Security.AdminUsers
{
	public partial class List : Controls.TemplateControlBase
	{
		protected override void OnLoad(EventArgs e)
		{
			rptList.DataBinding += List_DataBinding;
			base.OnLoad(e);
		}

		private void List_DataBinding(object sender, EventArgs e)
		{
			rptList.DataSource = Infrastructure.Security.AdminUser.Search(TemplatePage.GetFilter<Infrastructure.Security.AdminUser.SearchFilters>(), rptList);
		}
	}
}