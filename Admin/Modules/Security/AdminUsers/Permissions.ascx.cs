﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Security.AdminUsers
{
    public partial class Permissions : Controls.TemplateControlBase
    {
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += PageController_SaveItem;
            base.OnLoad(e);
        }

        public Infrastructure.Security.AdminUser CurrentAdminUser
        {
            get
            {
                return Infrastructure.Security.AdminUser.Current;
            }
        }


        protected override void DataBindChildren()
        {
            var itemData = TemplatePage.GetItemData<Infrastructure.Security.AdminUser>();
            if (itemData != null)
            {
                editPermissions.UserLoginId = itemData.Login.LoginID;
                editPermissions.LoadToUser(itemData.Login.LoginID);
            }
            base.DataBindChildren();
        }

        public void PageController_SaveItem(object sender, EventArgs e)
        {
            var itemData = TemplatePage.GetItemData<Infrastructure.Security.AdminUser>();
            if (itemData != null)
            {
                editPermissions.SaveToUser(itemData.Login.LoginID);
                
                if (CurrentAdminUser!=null && CurrentAdminUser.ID == itemData.ID && CurrentAdminUser.Login != null)
                {
                    Infrastructure.Security.Login.Current.Items.Remove("Security.ObjectPermissions");
                }
            }
        }
    }
}