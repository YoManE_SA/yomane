﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Security.AdminUsers
{
	public partial class Filter : Controls.TemplateControlBase
	{
		protected Infrastructure.Security.Group EditGroup = null;
        protected override void OnInit(EventArgs e)
        {
            if (!ObjectContext.Current.IsCurrentLoginAdminOfAdmins)
            {
                Response.Redirect(Web.WebUtils.RootUrl);
            }
        }

        protected override void OnLoad(EventArgs e)
		{
			TemplatePage.PageController.Search += PageController_Search;
			dlgAdminGroups.DataBinding += delegate { if (EditGroup == null) EditGroup = new Infrastructure.Security.Group(""); };
			ddlGroup.DataBinding += delegate { ddlGroup.DataSource = Infrastructure.Security.Group.Cache; };
            UpdateSearchFilter();
            base.OnLoad(e);
		}

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
		{
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Infrastructure.Security.AdminUser.SearchFilters();
            sf.ID = rngID.Value;
            sf.StartDate = rngDate.Value;
            sf.GroupID = ddlGroup.Value.ToNullableInt();
            sf.Text = txtName.Text.NullIfEmpty();
            TemplatePage.SetFilter(sf);
        }

        protected void ManageGroups_Click(object sender, EventArgs e)
		{
			rptGroupList.DataSource = Infrastructure.Security.Group.Cache.ToList();
			dlgAdminGroups.DataBind();
			dlgAdminGroups.RegisterShow();
		}

		protected void Group_Command(object sender, CommandEventArgs e)
		{
			switch (e.CommandName)
			{
			case "LoadItem":
				EditGroup = Infrastructure.Security.Group.Get((e.CommandArgument as DataKey)[0].ToNullableInt().GetValueOrDefault());
				break;
			case "SaveItem":
				EditGroup = Infrastructure.Security.Group.Get(hfGroupID.Value.ToNullableInt().GetValueOrDefault());
				if (EditGroup == null) EditGroup = new Infrastructure.Security.Group(txtGroupName.Text);
				EditGroup.Description = txtGroupDescription.Text;
				EditGroup.IsActive = chkGroupIsActive.Checked;
				EditGroup.Save();
				EditGroup = null;
				break;
			case "DeleteItem":
				EditGroup = Infrastructure.Security.Group.Get(hfGroupID.Value.ToNullableInt().GetValueOrDefault());
				if (EditGroup != null) EditGroup.Delete();
				EditGroup = null;
				break;
			}
			rptGroupList.DataSource = Infrastructure.Security.Group.Cache.ToList();
			dlgAdminGroups.DataBind();
		}
	}
}