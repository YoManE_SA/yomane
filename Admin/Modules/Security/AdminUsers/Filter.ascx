﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Security.AdminUsers.Filter" %>
<admin:FilterSection runat="server" Title="General">
    <div class="form-group">
        <asp:Label ID="lblName" runat="server" Text="Name" />
        <asp:TextBox runat="server" CssClass="form-control" ID="txtName" />
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblID" runat="server" Text="ID Range" />
                <JQ:IntRange runat="server" ID="rngID" />
            </div>
        </div>
    </div>
    <div class="form-group">
        <asp:Label ID="Label1" runat="server" Text="Group" />
        <div class="input-group">
            <span class="input-group-btn">
                <NP:Button runat="server" Text="Manage" CssClass="btn btn-primary" RegisterAsync="true" OnClick="ManageGroups_Click" /></span>
            <netpay:DropDownBase runat="server" CssClass="form-control" ID="ddlGroup" DataTextField="Name" DataValueField="ID" EnableBlankSelection="true" />
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblDate" runat="server" Text="Date Range" />
                <JQ:DateRange runat="server" ID="rngDate" />
            </div>
        </div>
    </div>
</admin:FilterSection>
<admin:ModalDialog runat="server" ID="dlgAdminGroups" Title="Groups" OnCommand="Group_Command">
    <Body>
        <admin:AdminList runat="server" DataKeyNames="ID" ID="rptGroupList" AutoGenerateColumns="false" BubbleLoadEvent="true" IsInListView="false">
            <Columns>
                <asp:BoundField HeaderText="Name" DataField="Name" />
                <asp:BoundField HeaderText="Description" DataField="Description" />
                <asp:CheckBoxField HeaderText="IsActive" DataField="IsActive" />
            </Columns>
        </admin:AdminList>
        <hr />
        <div class="row">

            <asp:HiddenField runat="server" ID="hfGroupID" Value='<%# EditGroup.ID %>' />
            <div class="col-lg-3">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" Text="Name" />
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtGroupName" Text='<%# EditGroup.Name %>' Enabled='<%# EditGroup.ID == 0 %>' />
                </div>
            </div>
            <div class="col-lg-9">
                <div class="form-group">
                    <asp:Label ID="Label3" runat="server" Text="Description" />
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtGroupDescription" Text='<%# EditGroup.Description %>' />
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <asp:CheckBox runat="server" ID="chkGroupIsActive" Text=" IsActive" Checked='<%# EditGroup.IsActive %>' />
                </div>
            </div>

        </div>
    </Body>
    <Footer>
        <admin:DataButtons ID="DataButtons1" runat="server" EnableAdd="true" EnableDelete="true" EnableSave="true" />
    </Footer>
</admin:ModalDialog>

















