﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Security
{
	public partial class SetPermissions : System.Web.UI.UserControl
	{
		public string ModuleName { get { return editPermissions.Filter; } set{ editPermissions.Filter = value; } }
		public class IdentityItem {
			public int ObjectID { get; set; }
			public string Name { get; set; }
			public string RoleName { get; set; }
			public IdentityItem(Infrastructure.Security.Login login){ ObjectID = login.LoginID; Name = login.UserName; RoleName = login.Role.ToString() + " User"; }
			public IdentityItem(Infrastructure.Security.Group group){ ObjectID = group.ID; Name = group.Name; RoleName = "Group"; }
		}

		protected override void DataBindChildren()
		{
			rptUsers.DataSource = Infrastructure.Security.AdminUser.Cache.Select(u => new IdentityItem(u.Login)).Union(Infrastructure.Security.Group.Cache.Select(g => new IdentityItem(g))).ToList();
			base.DataBindChildren();
		}

		protected void Users_SelectedIndexChanging(object sender, EventArgs e)
		{
			if (rptUsers.SelectedRow == null) return;
			(rptUsers.SelectedRow.FindControl("hfPermissions") as HiddenField).Value = editPermissions.GetDataAsString();
		}

		protected void Users_SelectedIndexChanged(object sender, EventArgs e)
		{
			var existing = (rptUsers.SelectedRow.FindControl("hfPermissions") as HiddenField).Value.NullIfEmpty();
			if (existing != null) editPermissions.SetData(existing);
			else {
				bool isGroup = rptUsers.SelectedDataKey[1].ToString() == "Group";
				var identityId = rptUsers.SelectedDataKey[0].ToNullableInt().GetValueOrDefault();
				if (isGroup) editPermissions.LoadToGroup(identityId);
				else editPermissions.LoadToUser(identityId);
			}
		}

		public void Save(object sender, EventArgs e)
		{
			Users_SelectedIndexChanging(this, EventArgs.Empty);
			foreach (GridViewRow v in rptUsers.Rows)
			{
				var value = (v.FindControl("hfPermissions") as HiddenField).Value.NullIfEmpty();
				if (value == null) continue;
				var identityId = rptUsers.DataKeys[v.DataItemIndex][0].ToNullableInt().GetValueOrDefault();
				bool isGroup = rptUsers.SelectedDataKey[1].ToString() == "Group";
				if (isGroup) editPermissions.SaveToGroup(identityId, value);
				else editPermissions.SaveToUser(identityId, value);
			}
		}

	}
}