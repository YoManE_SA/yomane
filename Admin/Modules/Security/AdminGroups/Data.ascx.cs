﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Modules.Security.AdminGroups
{
    public partial class Data : Controls.TemplateControlBase
    {
        public Infrastructure.Security.Group ItemData { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += PageController_SaveItem;
            TemplatePage.PageController.DeleteItem += PageController_DeleteItem;
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            ItemData = TemplatePage.GetItemData<Infrastructure.Security.Group>();

            if (ItemData == null)
            {
                ItemData = new Infrastructure.Security.Group("");
            }
            base.DataBindChildren();
        }

        protected void PageController_SaveItem(object sender, EventArgs e)
        {
            //ItemData = Infrastructure.Security.Group.Load(TemplatePage.ItemID.GetValueOrDefault());
            ItemData = TemplatePage.GetItemData<Infrastructure.Security.Group>();
            if (ItemData == null) return;

            ItemData.Name = txtName.Text;
            ItemData.IsActive = chkIsActive.Checked;
            ItemData.Description = txtDescription.Text;

            ItemData.Save();

            TemplatePage.SetItemData<Infrastructure.Security.Group>(ItemData);
        }

        private void PageController_DeleteItem(object sender, EventArgs e)
        {           
            ItemData = Infrastructure.Security.Group.Load(TemplatePage.ItemID.GetValueOrDefault());
            if (ItemData != null)
                ItemData.Delete();
        }
    }
}