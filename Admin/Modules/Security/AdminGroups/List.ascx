﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Modules.Security.AdminGroups.List" %>
<admin:ListSection runat="server" Title="Admin Groups">
    <Header>

    </Header>
    <Body>
        <admin:AdminList runat="server" ID="rptList" SaveAjaxState="true" DataKeyNames="ID" AutoGenerateColumns="false" BubbleLoadEvent="true">
            <Columns>
                <asp:BoundField HeaderText="ID" DataField="ID" />
                <asp:BoundField HeaderText="Name" DataField="Name" />
                <asp:BoundField HeaderText="Description" DataField="Description" />
                <asp:CheckBoxField Text=" " HeaderText="Is Active" DataField="IsActive" /> 
            </Columns>
        </admin:AdminList>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableAdd="true" />
    </Footer>
</admin:ListSection>