﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Modules.Security.AdminGroups
{
    public partial class Permissions : Controls.TemplateControlBase
    {
        public Infrastructure.Security.Group ItemData { get; set; }

        public Infrastructure.Security.AdminUser CurrentAdminUser
        {
            get
            {
                return Infrastructure.Security.AdminUser.Current;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += PageController_SaveItem;
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            ItemData = TemplatePage.GetItemData<Infrastructure.Security.Group>();

            if (ItemData != null)
            {
                editPermissions.LoadToGroup(ItemData.ID);
            }

            base.DataBindChildren();
        }

        public void PageController_SaveItem(object sender, EventArgs e)
        {
            ItemData = TemplatePage.GetItemData<Infrastructure.Security.Group>();

            if (ItemData != null)
            {
                editPermissions.SaveToGroup(ItemData.ID);
         
                //Check if the current adminuser has the current group that is saved now and if so
                //Remove that current user groups objects.
                if (CurrentAdminUser != null && CurrentAdminUser.Groups.Any(x => x.ID == ItemData.ID))
                {
                    CurrentAdminUser.Groups.Where(x => x.ID == ItemData.ID).SingleOrDefault().Items.Remove("Security.ObjectPermissions");
                }

                TemplatePage.SetItemData(ItemData);
            }
        }
    }
}