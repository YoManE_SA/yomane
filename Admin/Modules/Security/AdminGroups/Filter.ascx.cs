﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Modules.Security.AdminGroups
{
    public partial class Filter : Controls.TemplateControlBase
    {
        protected override void OnInit(EventArgs e)
        {
            if (!ObjectContext.Current.IsCurrentLoginAdminOfAdmins)
            {
                Response.Redirect(Web.WebUtils.RootUrl);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
        {
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Infrastructure.Security.Group.SearchFilters();
            sf.IsActive = ddlIsActive.BoolValue;
            TemplatePage.SetFilter(sf);
        }

    }
}