﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Permissions.ascx.cs" Inherits="Netpay.Admin.Modules.Security.AdminGroups.Permissions" %>
<%@ Register Src="~/Modules/Security/EditPermissions.ascx" TagPrefix="uc1" TagName="EditPermissions" %>
<admin:FormSections runat="server" Title="Permissions">
    <uc1:EditPermissions Mode="ByGroup" runat="server" ID="editPermissions" />
</admin:FormSections>