﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Modules.Security.AdminGroups.Filter" %>
<admin:FilterSection runat="server" Title="Admin Groups Filter">
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <asp:Label ID="lblName" runat="server" Text="Is Active" />
                <netpay:BoolDropDown CssClass="form-control" runat="server" ID="ddlIsActive" TrueText="Active" FalseText="Not Active" />
            </div>
        </div>
    </div>
</admin:FilterSection>
