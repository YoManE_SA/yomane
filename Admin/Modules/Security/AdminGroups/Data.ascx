﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Data.ascx.cs" Inherits="Netpay.Admin.Modules.Security.AdminGroups.Data" %>
<admin:FormSections Flexible="false" runat="server" Title="Details">
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                Name
                <asp:TextBox ReadOnly='<%# ItemData.ID != 0 %>' runat="server" ID="txtName" CssClass="form-control" Text='<%# ItemData.Name %>'></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                Description
                <asp:TextBox runat="server" ID="txtDescription" CssClass="form-control" Text='<%# ItemData.Description %>'></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsActive" runat="server" Text="IsActive" Checked='<%# ItemData.IsActive %>' />
                </li>
            </ul>
        </div>
    </div>
</admin:FormSections>
