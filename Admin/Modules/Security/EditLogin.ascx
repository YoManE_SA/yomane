﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditLogin.ascx.cs" Inherits="Netpay.Admin.Security.EditLogin" %>
<%@ Register Src="EditPassword.ascx" TagPrefix="CUC" TagName="EditPassword" %>
<admin:FormSections ID="test" runat="server" Title="Login Information" Visible='True'>
    <asp:MultiView ID="EditLoginMultiView" runat="server" ActiveViewIndex='<%# IsVisible ? 0 : 1%>'>
        <asp:View ID="View1" runat="server">

            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="form-group">
                        Username:
            <asp:TextBox runat="server" CssClass="form-control" ID="txtUserName" Text='<%# ItemData.UserName %>' MaxLength="30" />
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="form-group">
                        Email Address:

            <asp:TextBox runat="server" CssClass="form-control" ID="txtEmailAddress" Text='<%# ItemData.EmailAddress %>' MaxLength="50" ReadOnly='<%#TemplatePage.TemplateName=="Customer" %>' />
                        <asp:RegularExpressionValidator
                            ForeColor="red"
                            ControlToValidate="txtEmailAddress"
                            ID="RegularExpressionValidatortxtEmailAddress"
                            runat="server"
                            ErrorMessage="Insert a correct email address."
                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                        </asp:RegularExpressionValidator>

                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="form-group">
                        <br />
                        <asp:CheckBox runat="server" ID="chkIsActive" Text="Enabled" Checked='<%# ItemData.IsActive %>' />
                    </div>
                </div>
                <div class="col-lg-12 col-md-12">
                    Password:
            <CUC:EditPassword ID="ucPassword" LoginID='<%# ItemData.LoginID %>' runat="server" />
                </div>
            </div>

        </asp:View>
        <asp:View ID="View2" runat="server">
            <div class="alert alert-info">
                You need to save your details in order to edit this section.
            </div>
        </asp:View>
    </asp:MultiView>
</admin:FormSections>
