﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditPassword.ascx.cs" Inherits="Netpay.Admin.Security.EditPassword" %>
<asp:HiddenField runat="server" ID="hfLoginID" />

<asp:PlaceHolder runat="server" Visible='<%# ObjectContext.Current.IsCurrentLoginAdminOfAdmins %>'>
    <NP:LinkButton runat="server" ID="btnChange" UseSubmitBehavior="false" Text="Add/Change" OnClick="AddChange_Click" RegisterAsync="true" />
    <asp:Literal runat="server">|</asp:Literal>
</asp:PlaceHolder>

<NP:LinkButton runat="server" ID="btnResetLock" UseSubmitBehavior="false" Text="Reset Lock" OnClick="ResetLock_Click" RegisterAsync="true" />
|
<asp:LinkButton
    runat="server"
    OnClientClick='<%# ConfirmationModalDialog.ShowJSCommand %>'
    ID="btnResetPassword"
    Text="Forgot Password?" />

<asp:PlaceHolder runat="server" Visible='<%# ObjectContext.Current.IsCurrentLoginAdminOfAdmins %>'>
    <admin:ModalDialog runat="server" ID="dlgChangePassword" Title="Change Password">
        <Body>
            <netpay:ActionNotify runat="server" ID="acnPasswordMessage" />
            <div class="form-group">
                New Password
            <asp:TextBox runat="server" CssClass="form-control" ID="txtPassword" TextMode="Password" />
            </div>
            <div class="form-group">
                Confirm Password
            <asp:TextBox runat="server" CssClass="form-control" ID="txtConfirmPassword" TextMode="Password" />
            </div>
        </Body>
        <Footer>
            <asp:Button runat="server" OnClientClick='<%# dlgChangePassword.HideJSCommand %>' ID="btnClose" CssClass="btn btn-inverse btn-cons-short" Text="Cancel" CommandName="Close" />
            <asp:Button runat="server" ID="btnSave" CssClass="btn btn-primary btn-cons-short" Text="Save" OnClick="ChangePassword_SaveClick" />
        </Footer>
    </admin:ModalDialog>
</asp:PlaceHolder>

<admin:ModalDialog runat="server" ID="dlgResetLock" Title="Password Actions">
    <Body>
        <netpay:ActionNotify runat="server" ID="ResetLockNotify" />
    </Body>
</admin:ModalDialog>

<admin:ModalDialog ID="ConfirmationModalDialog" Title="Forgot your password?" runat="server">
    <Body>
         Are you sure you want to generate a new password,
         And send it via email?
    </Body>
    <Footer>
        <asp:Button OnClientClick='<%# ConfirmationModalDialog.HideJSCommand %>' UseSubmitBehavior="false" OnClick="btnResetPassword_Click" runat="server" Text="Confirm" />
        <asp:Button OnClientClick='<%# ConfirmationModalDialog.HideJSCommand %>' runat="server" Text="Cancel" />
    </Footer>
</admin:ModalDialog>
