﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SetPermissions.ascx.cs" Inherits="Netpay.Admin.Security.SetPermissions" %>
<%@ Register Src="~/Modules/Security/EditPermissions.ascx" TagPrefix="CUC" TagName="EditPermissions" %>
<admin:AdminList runat="server" ID="rptUsers" DataKeyNames="ObjectID,RoleName" AutoGenerateColumns="false" OnSelectedIndexChanged="Users_SelectedIndexChanged" OnSelectedIndexChanging="Users_SelectedIndexChanging"  IsInListView="false">
	<Columns>
		<asp:TemplateField HeaderText="ID" SortExpression="ObjectID" >
			<ItemTemplate>
				<%# Eval("ObjectID") %>
				<asp:HiddenField runat="server" ID="hfPermissions" />
			</ItemTemplate>
		</asp:TemplateField>
		<asp:BoundField HeaderText="Name" DataField="Name" SortExpression="Name" />
		<asp:BoundField HeaderText="Role" DataField="RoleName" SortExpression="RoleName" />
	</Columns>
</admin:AdminList>
<CUC:EditPermissions runat="server" ID="editPermissions" />
