﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;

namespace Netpay.Admin.Invoices
{
    public partial class Filter : Controls.TemplateControlBase
    {
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
        {
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Bll.Invoices.InvoiceDocument.SearchFilters();

            //Hidden ID filter 
            if (!string.IsNullOrEmpty(SpecificInvoiceDocumentID.Text))
            {
                sf.ID = int.Parse(SpecificInvoiceDocumentID.Text);
            }

            //Date filter
            sf.DateRange = rngInvoiceDate.Value;

            //Document Type filter
            if (!string.IsNullOrEmpty(ddlInvoiceType.SelectedValue) && (ddlInvoiceType.SelectedValue != "-1"))
            {
                sf.DocumentType = int.Parse(ddlInvoiceType.SelectedValue);
            }

            //Issuance filter(Is Manual)
            if ( !string.IsNullOrEmpty(ddlIsManual.SelectedValue) && (ddlIsManual.SelectedValue != "-1"))
            {
                sf.IsManual = ddlIsManual.BoolValue;
            }
                       
                        
            //Billing company(Invoice issuer)
            if ( !string.IsNullOrEmpty(ddlBillingCompany.SelectedValue) && (ddlBillingCompany.SelectedValue != "-1"))
            {
                sf.BillingCompanyID = int.Parse(ddlBillingCompany.SelectedValue);
            }

            //Invoice number filter
            sf.InvoiceNumberRange = rngInvoiceNumber.Value;

            TemplatePage.SetFilter(sf);
        }
    }
}