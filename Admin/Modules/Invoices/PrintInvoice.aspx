﻿<%@ Page Title="PrintInvoice" MasterPageFile="~/PrintInvoiceMaster.Master" Language="C#" AutoEventWireup="True" CodeBehind="PrintInvoice.aspx.cs" Inherits="Netpay.Admin.Invoices.PrintInvoice" %>

<asp:Content ContentPlaceHolderID="BodyContentPrintWindow" ClientIDMode="Static" runat="server">
    <script type="text/javascript">

        var shouldReload = true;

        function setReloaderFlag() {
            shouldReload = false;
        }

        function reloadOpener() {

            if (shouldReload) {
                     window.opener.location.reload(1);
            }
        }

        window.addEventListener('unload', function (e) {
            reloadOpener();
        });

    </script>

    <asp:MultiView runat="server" ID="IsNullMultiView" ActiveViewIndex='<%# IPD != null ? 0 : 1 %>'>
        <asp:View runat="server">
            <%--Menu section panel --%>
            <asp:Panel runat="server" ID="PanelSectionMenu" ClientIDMode="Static">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <asp:Label runat="server" Font-Size="Large">Print invoice number - <%# nBillingNumber %></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                         <asp:Label runat="server" Font-Size="Large">Issuing date - <%# IPD!=null? IPD.InsertDate.Value.ToString() : "" %></asp:Label>
                    </div>
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-xs-4 col-md-4">
                                <asp:RadioButtonList onchange="setReloaderFlag();" AutoPostBack="true" OnSelectedIndexChanged="PreviewTypeRadioButtonList_SelectedIndexChanged" runat="server" OnDataBinding="PreviewTypeRadioButtonList_DataBinding" ID="PreviewTypeRadioButtonList">
                                </asp:RadioButtonList><br />
                                <br />

                                <asp:Button runat="server" Text="Preview" CssClass="btn-default" OnClientClick="setReloaderFlag();" OnCommand="Preview_Command" CommandArgument='<%# IPD.DocumentID %>' />
                                <asp:Button runat="server" Enabled="false" Text="Print" CssClass="btn-default" ID="PrintButton" OnCommand="PrintButton_Command" CommandArgument='<%# IPD.DocumentID %>' OnClientClick="window.print();" Style="cursor: no-drop" />

                            </div>

                            <asp:PlaceHolder runat="server" Visible='<%# IPD.MerchantID > 0 %>'>
                                <div class="col-xs-4 col-md-4" style="border-right: 1px solid #ccc; border-left: 1px solid #ccc;">
                                    <span><b>Company Name</b> :</span> <span><%#IPD.MerchantID > 0 && IPD.CompanyLegalName.Length <= 1 ? IPD.FirstName + " " + IPD.LastName : IPD.CompanyLegalName %></span><br />
                                    <span><b>Pay Date</b> : </span><span><%# IPD.payDate %></span><br />
                                    <span><b>Pay Type</b> : </span><span><%# IPD.PaymentMethod %></span><br />
                                    <b>Promotional date</b> :<br />
                                    <%# IPD.MerchantID > 0 && IPD.PayingDates1.Length == 6 ? string.Format("From:{0}, To:{1}, Credit on:{2}",
                                    IPD.PayingDates1.Trim().Substring(0,2) ,IPD.PayingDates1.Trim().Substring(2,2), IPD.PayingDates1.Trim().Substring(4,2)) + "<br/>" : "" %>

                                    <%# IPD.MerchantID > 0 && IPD.PayingDates2.Length == 6 ? string.Format("From:{0}, To:{1}, Credit on:{2}",
                                    IPD.PayingDates2.Trim().Substring(0,2) ,IPD.PayingDates2.Trim().Substring(2,2), IPD.PayingDates2.Trim().Substring(4,2)) + "<br/>" : "" %>

                                    <%# IPD.MerchantID > 0 && IPD.PayingDates3.Length == 6 ? string.Format("From:{0}, To:{1}, Credit on:{2}",
                                    IPD.PayingDates3.Trim().Substring(0,2) ,IPD.PayingDates3.Trim().Substring(2,2), IPD.PayingDates3.Trim().Substring(4,2)) + "<br/>" : "" %>
                                </div>

                                <div class="col-xs-4 col-md-4">
                                    <b>Pay Type</b> : <span><%#IPD.MerchantID > 0 ? IPD.PaymentMethod : "" %><br />
                                    </span>
                                    <b>Paying To</b> : <%#IPD.MerchantID > 0 ? IPD.PaymentPayeeName : "" %><br />
                                    <b>Bank Number</b> : <%#IPD.MerchantID > 0 ?  IPD.PaymentBank : -1 %><br />
                                    <b>Branch Number</b> : <%#IPD.MerchantID > 0 ? IPD.PaymentBranch : "" %><br />
                                    <b>Account Number</b> : <%# IPD.PaymentAccount %><br />
                                </div>
                            </asp:PlaceHolder>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <%-- Print Preview section pannel --%>
            <asp:UpdatePanel ID="PreviewUpdatePanel" ClientIDMode="Static" runat="server" Visible="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:MultiView runat="server" ID="PreviewMultiView">
                        <asp:View runat="server" ID="CopyOrOriginalView">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <span style="float: left"><%# InvoiceDocumentItem != null? InvoiceDocumentItem.BillingCompanyName : "" %></span><br />
                                            <span style="float: left"><%# InvoiceDocumentItem != null? InvoiceDocumentItem.BillingCompanyAddress : "" %></span><br />
                                        </div>

                                        <div class="col-xs-6">
                                            <span style="float: right"><%# InvoiceDocumentItem != null? InvoiceDocumentItem.BillingCompanyNumber : "" %></span><br />
                                            <span style="float: right"><%#  InvoiceDocumentItem != null? InvoiceDocumentItem.BillingCompanyEmail : "" %></span><br />
                                        </div>
                                    </div>

                                </div>
                                <div class="panel-body">
                                    <div style="margin-left: auto; margin-right: auto; text-align: center;">
                                        <asp:Label runat="server"
                                            Text='<%# sInvoiceTypeTextEng + " " + nBillingNumber + "-" + sBillTypeTextEng + "<br/>" %>'
                                            Font-Bold="true"
                                            Font-Size="Medium">
                                        </asp:Label>
                                    </div>
                                    <br />

                                    <div class="row">
                                        <div class="col-sx-6" style="padding-left: 10px;">
                                            <div style="float: left;">
                                                <asp:Label Font-Bold="true" runat="server" Text='<%# sTextDispaly_1 %>'></asp:Label>
                                            </div>
                                            <br />
                                            <div style="float: left;">
                                                <asp:Label runat="server" Text='<%# InvoiceDocumentItem!=null? InvoiceDocumentItem.BillToName : "" %>'></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-sx-6" style="padding-right: 10px;">
                                            <div style="float: right;">
                                                <asp:Label Font-Bold="true" runat="server" Text='<%# sTextDispaly_2 %>'></asp:Label>
                                            </div>
                                            <br />
                                            <div style="float: right;">
                                                <asp:Label runat="server" Text='<%# InvoiceDocumentItem!=null? InvoiceDocumentItem.InsertDate.ToShortDateString() : "" %>'></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <br />

                                    <div class="row inner-table-titles">
                                        <div class="col-xs-3"><%# sHeadTextDispaly_1 %></div>
                                        <div class="col-xs-3"><%# sHeadTextDispaly_2 %></div>
                                        <div class="col-xs-3"><%# sHeadTextDispaly_3 %></div>
                                        <div class="col-xs-3"><%# sHeadTextDispaly_4 %></div>
                                    </div>

                                    <NP:PagedRepeater runat="server" ID="rptList" DataSource='<%# InvoiceLines %>'>
                                        <ItemTemplate>
                                            <div class="row table-ui">
                                                <div class="col-xs-3"><%# InvoiceLines!=null? Eval("Text") : "" %></div>
                                                <div class="col-xs-3"><%# ((InvoiceLines!=null) && (IPD.TransPayId.HasValue && IPD.TransPayId.Value > 0)) ? "---" : Eval("Price").ToString() %>  </div>
                                                <div class="col-xs-3"><%# InvoiceLines!=null? Eval("Quantity").ToString() : "" %>  </div>
                                                <div class="col-xs-3"><%# (nCurrency>=0 && InvoiceLines!=null)? ((decimal)Eval("Amount")).ToIsoAmountFormat(nCurrency) : "" %> </div>
                                            </div>
                                        </ItemTemplate>
                                    </NP:PagedRepeater>
                                    <hr />
                                    <div class="row-table-ui">
                                        <div class="col-xs-9"><b><%# sTextDispaly_5 %></b></div>
                                        <div class="col-xs-3"><%# InvoiceDocumentItem!=null ? InvoiceDocumentItem.TotalLines.ToIsoAmountFormat(nCurrency) : "" %> </div>
                                    </div>
                                    <div class="row-table-ui">
                                        <div class="col-xs-9"><b><%# sTextDispaly_6 + "(" +(sVATamount*100) +"%)" %></b></div>
                                        <div class="col-xs-3"><%# InvoiceDocumentItem!=null ? InvoiceDocumentItem.TotalVat.ToIsoAmountFormat(nCurrency) : "" %> </div>
                                    </div>
                                    <div class="row-table-ui">
                                        <div class="col-xs-9"><b><%# sTextDispaly_7  %></b></div>
                                        <div class="col-xs-3"><%# InvoiceDocumentItem!=null ? InvoiceDocumentItem.TotalDocument.ToIsoAmountFormat(nCurrency) : "" %> </div>
                                    </div>
                                </div>
                            </div>
                        </asp:View>
                        <asp:View runat="server" ID="TransactionsListView">
                        </asp:View>
                    </asp:MultiView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:View>

        <asp:View runat="server">
            <div class="alert alert-info">
                <strong>Info!</strong> No records found
            </div>
        </asp:View>
    </asp:MultiView>
</asp:Content>
