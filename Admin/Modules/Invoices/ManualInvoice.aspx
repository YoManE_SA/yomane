﻿<%@ Page Title="Manual Issuance" Language="C#" MasterPageFile="~/PrintInvoiceMaster.Master" AutoEventWireup="true" CodeBehind="ManualInvoice.aspx.cs" Inherits="Netpay.Admin.Invoices.ManualInvoice" %>

<asp:Content ContentPlaceHolderID="BodyContentPrintWindow" runat="server">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-4 col-md-4">
                    <asp:Label runat="server" Text="Document Type" Font-Bold="true">
                    </asp:Label>
                    <netpay:InvoiceTypeDropDown
                        runat="server"
                        ID="rblInvoiceType"
                        EnableBlankSelection="false"
                        CssClass="form-control">
                    </netpay:InvoiceTypeDropDown>
                </div>

                <div class="col-xs-4 col-md-4">
                    <asp:Label runat="server" Text="Issuer" Font-Bold="true">
                    </asp:Label>
                    <netpay:BillingCompanyDropDown
                        AutoPostBack="true"
                        OnSelectedIndexChanged="ResetVATPercent"
                        runat="server"
                        ID="rblBillingCompany"
                        EnableBlankSelection="false"
                        CssClass="form-control">
                    </netpay:BillingCompanyDropDown>
                </div>

                <div class="col-xs-4 col-md-4">
                    <asp:Label runat="server" Text="VAT - " Font-Bold="true">
                    </asp:Label>
                    <asp:Label runat="server" ID="lblVATPercent"></asp:Label>
                    <asp:RadioButtonList ID="rblVATUsage" runat="server">
                        <asp:ListItem Value="1" Text="Prices include VAT" />
                        <asp:ListItem Value="0" Text="Prices do not include VAT" Selected="True" />
                        <asp:ListItem Value="2" Text="The document does not need VAT" />
                    </asp:RadioButtonList>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-6 col-md-6">
                    <asp:Label runat="server" Text="Issue To" Font-Bold="true">
                    </asp:Label>
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <asp:DropDownList Style="float: left; margin-right: 5px" Width="100" CssClass="form-control" ID="ddlIssueToMode" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlIssueToMode_SelectedIndexChanged">
                                <asp:ListItem Value="0" Text="Merchant" />
                                <asp:ListItem Value="1" Text="Other" />
                            </asp:DropDownList>
                            <asp:MultiView ID="IssueToMultiView" runat="server" ActiveViewIndex='<%# ddlIssueToMode.SelectedIndex == 0? 0 : 1 %>'>
                                <asp:View runat="server">
                                    <admin:AccountPicker ButtonClientIdToDisable="ContinueButton" DisableButtonOnKeyDown="true" OnClientClick="__doPostBack('','');" runat="server" Width="200" ID="apMerchant" UseTargetID="false" PlaceHolder="Select merchant ..." LimitToType="Merchant" />
                                </asp:View>
                                <asp:View runat="server">
                                    <asp:TextBox runat="server" AutoPostBack="true" OnTextChanged="txtOther_TextChanged" Width="200" ID="txtOther" CssClass="form-control" placeholder="<%# DEFAULT_ISSUE_TO %>"></asp:TextBox>
                                </asp:View>
                            </asp:MultiView>
                        </div>
                    </div>
                </div>
                <div class="col-xs-3 col-md-3">
                    <asp:Label runat="server" Text="Date" Font-Bold="true"></asp:Label><br />
                    <JQ:DatePicker class="form-control" Value='<%# DateTime.Now %>' runat="server" ID="DatePickerValue"></JQ:DatePicker>
                </div>
                <div class="col-xs-3 col-md-3">
                    <asp:Label runat="server" Text="Currency" Font-Bold="true"></asp:Label>
                    <netpay:CurrencyDropDown EnableBlankSelection="false" CssClass="form-control" runat="server" ID="ddlCurrency"></netpay:CurrencyDropDown>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4 col-md-4">
                    <asp:Button
                        runat="server"
                        ID="ContinueButton"
                        Text="Continue"
                        OnClick="Continue_Click"
                        CssClass="btn btn-primary"
                        ClientIDMode="Static" />
                </div>
            </div>
        </div>
    </div>

    <%-- Add new line section. --%>
    <asp:PlaceHolder ID="AddLinePlaceHolder" runat="server" Visible="false">
        <div class="panel panel-default">
            <div class="panel-heading">
                Add new item
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <asp:Label runat="server" Font-Bold="true" Text="Description"></asp:Label><br />
                        <asp:TextBox CssClass="form-control" runat="server" ID="txtNewDescription"></asp:TextBox>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <asp:Label runat="server" Font-Bold="true" Text="Quantity"></asp:Label><br />
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtNewQuantity"></asp:TextBox>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <asp:Label runat="server" Font-Bold="true" Text="Price"></asp:Label><br />
                        <asp:TextBox CssClass="form-control" runat="server" ID="txtNewPrice"></asp:TextBox>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2">
                        <asp:Label runat="server" Font-Bold="true" Text="Currency"></asp:Label><br />
                        <netpay:CurrencyDropDown EnableBlankSelection="false" runat="server" CssClass="form-control" ID="ddlNewCurrency"></netpay:CurrencyDropDown>
                    </div>
                    <div class="col-xs-1 col-sm-1 col-md-1">
                        <br />
                        <asp:Button runat="server" OnCommand="AddNewLine" Text="Add" CssClass="btn btn-default" />
                    </div>
                </div>
            </div>
        </div>
        <br />
        <netpay:ActionNotify runat="server" ID="CreateLineAction" />
    </asp:PlaceHolder>

    <asp:PlaceHolder Visible="false" runat="server" ID="phInvoiceLines">
        <div class="panel panel-default">
            <div class="panel-heading">
                Invoice items
            </div>
            <div class="panel-body">
                <admin:AdminList DisableRowSelect="true" OnDataBound="Recalculate" AutoGenerateColumns="false" DataSource='<%# GetInvoiceLines() %>' runat="server" ID="gvLines">
                    <Columns>
                        <asp:TemplateField HeaderText="Actions">
                            <ItemTemplate>
                                <asp:Button
                                    runat="server"
                                    CssClass="btn btn-danger"
                                    Text="Delete"
                                    OnCommand="Delete_Command"
                                    OnClientClick="return confirm('Are you sure?');"
                                    CommandArgument='<%# Eval("Line.ID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="#" DataField="Line.ID" />
                        <asp:BoundField HeaderText="Description" DataField="Line.Text" />
                        <asp:BoundField HeaderText="Rate" DataField="Line.CurrecnyRate" />
                        <asp:BoundField HeaderText="Qty" DataField="Line.Quantity" />
                        <asp:BoundField HeaderText="Price" DataField="Line.Price" />
                        <asp:BoundField HeaderText="Currency" DataField="CUR_ISOName" />
                        <asp:BoundField HeaderText="Amount" DataField="Line.Amount" />
                        <asp:BoundField HeaderText="Converted Amount" DataField="AmountConverted" />
                    </Columns>
                </admin:AdminList>

                <hr />
                <div class="row-table-ui">
                    <div class="col-xs-9"><b>Total</b></div>
                    <div class="col-xs-3">
                        <asp:Label runat="server" ID="lblTotal" Text="0.00" />
                    </div>
                </div>
                <div class="row-table-ui">
                    <div class="col-xs-9"><b>Total VAT</b></div>
                    <div class="col-xs-3">
                        <asp:Label runat="server" ID="lblVAT" Text="0.00" />
                    </div>
                </div>
                <div class="row-table-ui">
                    <div class="col-xs-9"><b>Total including VAT</b></div>
                    <div class="col-xs-3">
                        <asp:Label runat="server" ID="lblTotalVAT" Text="0.00" />
                    </div>
                </div>
            </div>
        </div>

        <div class="panel-footer">
            <asp:Button ID="btnRecalc" runat="server" Text="Recalculate Totals" CssClass="btn btn-default" OnClick="Recalculate" />
            <asp:Button ID="btnCreate" runat="server" Text="Create Document" CssClass="btn btn-default" OnClick="CreateDocument" />
            &nbsp

            <asp:PlaceHolder runat="server" Visible="false" ClientIDMode="Static" ID="phNewDocumentCreated">
                <asp:Literal runat="server" ID="NewDocLiteral" Text='<%# rblInvoiceType.SelectedItem.Text + " #" + NewDocInvoiceNumber + " has been successfully created." %>' />&nbsp
            <asp:HyperLink
                ID="PrintInvoice"
                ClientIDMode="Static"
                runat="server"
                ForeColor="#428bca"
                Style="cursor: pointer;"
                Text="Print"
                NavigateUrl='<%# WebUtils.RootUrl + "Modules/Invoices/PrintInvoice.aspx?"+ string.Format("BillingCompanysID={0}&billingNumber={1}&invoiceType={2}", NewDocBillingCompanyID, NewDocInvoiceNumber, NewDocType) %>'
                Target="_self">
            </asp:HyperLink>

                &nbsp | &nbsp

                <asp:HyperLink
                    ID="HyperLink1"
                    ClientIDMode="Static"
                    runat="server"
                    ForeColor="#428bca"
                    Style="cursor: pointer;"
                    Text="Create another"
                    NavigateUrl='<%# WebUtils.RootUrl + "Modules/Invoices/ManualInvoice.aspx" %>'
                    Target="_self">
                </asp:HyperLink>
            </asp:PlaceHolder>
        </div>

        <asp:TextBox runat="server" ID="Focus" Visible="false">
        </asp:TextBox>

    </asp:PlaceHolder>
</asp:Content>
