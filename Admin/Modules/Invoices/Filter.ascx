﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Invoices.Filter" %>
<admin:FilterSection runat="server" Title="Invoices Filter">
    <asp:Label runat="server" Text="Manual Issuance" Font-Underline="true" ForeColor="#003366"></asp:Label><br />
    <asp:Literal runat="server" Text="For manual issuance - "></asp:Literal>
    <asp:HyperLink
        runat="server"
        ForeColor="#428bca"
        Style="cursor: pointer;"
        Text="Click Here"
        onclick='<%# "OpenPop(" + "\"" + WebUtils.RootUrl + "Modules/Invoices/ManualInvoice.aspx" + "\"" + ",\"Manual Issuance\",\"750\",\"500\",\"1\"); return false;" %>'
        Target="_blank">
    </asp:HyperLink><br /><hr />
    <asp:Label runat="server" Text="Invoice Details" Font-Underline="true" ForeColor="#003366"></asp:Label><br />
    <br />

    <%-- Hidden filter for ID , comes from the query string --%>
    <asp:TextBox runat="server" ID="SpecificInvoiceDocumentID" Visible="false">
    </asp:TextBox>

    <%-- Invoice Number filter --%>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label runat="server" Text="Invoice Number"></asp:Label>
                <JQ:IntRange runat="server" ID="rngInvoiceNumber"></JQ:IntRange>
            </div>
        </div>
    </div>

    <%-- Invoice Date filter --%>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label runat="server" Text="Date"></asp:Label>
                <JQ:DateRange runat="server" ID="rngInvoiceDate"></JQ:DateRange>
            </div>
        </div>
    </div>

    <%-- Document Type filter --%>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <asp:Label runat="server" Text="Document Type">
                </asp:Label>
                <netpay:InvoiceTypeDropDown
                    CssClass="form-control"
                    runat="server"
                    ID="ddlInvoiceType"
                    EnableBlankSelection="true"
                    BlankSelectionValue="-1">
                </netpay:InvoiceTypeDropDown>
            </div>
        </div>

        <%-- Issuance filter --%>
        <div class="col-lg-6">
            <div class="form-group">
                <asp:Label runat="server" Text="Issuance"></asp:Label>
                <netpay:BoolDropDown CssClass="form-control" runat="server" ID="ddlIsManual" TrueText="Manual" FalseText="Required" EnableBlankSelection="true" BlankSelectionValue="-1">
                </netpay:BoolDropDown>
            </div>
        </div>

        <%-- Invoice Issuer - Billing Company --%>
        <div class="col-lg-6">
            <div class="form-group">
                <asp:Label runat="server" Text="Invoice Issuer">
                </asp:Label>
                <netpay:BillingCompanyDropDown
                    CssClass="form-control"
                    runat="server"
                    ID="ddlBillingCompany"
                    EnableBlankSelection="true"
                    BlankSelectionValue="-1">
                </netpay:BillingCompanyDropDown>
            </div>
        </div>
    </div>

</admin:FilterSection>
