﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Invoices
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuInvoices;

        public override string Name { get { return "Invoices"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return "InvoiceDocument managment"; } }

        public Module() : base(Bll.Invoices.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu item
            menuInvoices = new ApplicationMenu("Invoices", "~/Invoices/0", null, 140);

            //Register route
            Application.RegisterRoute("Invoices/{*id}", "Invoices", typeof(Controls.DataTablePage), module:this);

            Application.RegisterRoute("Invoices/ManualInvoice.aspx", "ManualInvoice", typeof(ManualInvoice));
            Application.RegisterRoute("Invoices/PrintInvoice.aspx", "PrintInvoice", typeof(PrintInvoice));

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {                    
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {        
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Finance", menuInvoices);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuInvoices.Parent.RemoveChild(menuInvoices);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;

            if (page.TemplateName == "Invoices")
            {
                page.AddControlToFilter("Test", page.LoadControl("~/Modules/Invoices/Filter.ascx"));
                page.AddControlToList(page.LoadControl("~/Modules/Invoices/List.ascx"));
            }
        }
    }
}