﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.CommonTypes;


namespace Netpay.Admin.Invoices
{
    public partial class ManualInvoice : Controls.Page
    {
        public string DEFAULT_ISSUE_TO = "Type a recepient name here ...";

        public int NewDocBillingCompanyID { get; set; }
        public int NewDocInvoiceNumber { get; set; }

        public int NewDocType { get; set; }


        protected override void OnLoad(EventArgs e)
        {
            //Bind the data to billingcompany drop down so it will have a value
            rblBillingCompany.DataBind();

            //DataBind on Continue button in order to check if that is enabled or not ,
            //Happens after merchant was checked.
            //if (apMerchant.Value.HasValue) ContinueButton.DataBind();
            ContinueButton.Enabled = (ddlIssueToMode.SelectedValue == "0" && apMerchant.Value.HasValue) || (ddlIssueToMode.SelectedValue == "1" && txtOther.Text != string.Empty && txtOther.Text != "Type a recepient name here ...");

            //Get the vat percent of the Selected billing company
            ResetVATPercent(null, null);

            base.OnLoad(e);
        }

        protected void ResetVATPercent(object sender, EventArgs e)
        {
            lblVATPercent.Text = (Bll.BillingCompany.GetBillingCompanyVat(int.Parse(rblBillingCompany.SelectedValue)) * 100).ToString() + "%";
        }

        protected void ddlIssueToMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtOther.Text = "";
            apMerchant.Clear();
            IssueToMultiView.DataBind();
        }

        protected void txtOther_TextChanged(object sender, EventArgs e)
        {
            ContinueButton.DataBind();
        }

        protected void Continue_Click(object sender, EventArgs e)
        {
            AddLinePlaceHolder.Visible = true;
            phInvoiceLines.Visible = true;
            gvLines.DataBind();
        }

        protected void AddNewLine(object sender, CommandEventArgs e)
        {
            int nTransactionPay = 0;
            string sBillTo = (ddlIssueToMode.SelectedIndex == 1) ? (txtOther.Text == DEFAULT_ISSUE_TO ? string.Empty : txtOther.Text) : (string.Empty);
            int nMerchant = (ddlIssueToMode.SelectedIndex == 0) ? (apMerchant.Value.HasValue ? Bll.Merchants.Merchant.LoadByAccountId(apMerchant.Value.Value).ID : 0) : 0;

            string sText = txtNewDescription.Text.Trim();
            float nQuantity = float.Parse(txtNewQuantity.Text);
            decimal nPrice = decimal.Parse(txtNewPrice.Text);
            int nCurrency = int.Parse(ddlNewCurrency.SelectedValue);
            string sUserName = Web.WebUtils.IsLoggedin ? Web.WebUtils.LoggedUser.UserName : "";

            if (string.IsNullOrEmpty(sText)) { CreateLineAction.SetMessage("Please insert description.", true); };
            if (nQuantity <= 0) { CreateLineAction.SetMessage("Please insert quantity.", true); };
            if (nPrice <= 0) { CreateLineAction.SetMessage("Please insert price.", true); };
            int NewInvoiceResult = -1;

            try
            {
                NewInvoiceResult = Bll.Invoices.Local.InvoiceLineCreate(nTransactionPay, sBillTo, nMerchant, sText, nQuantity, nPrice, nCurrency, Bll.Currency.ConvertRate(Bll.Currency.Get(nCurrency), Bll.Currency.Get(0)), sUserName);
            }
            catch (Exception ex)
            {
                CreateLineAction.SetMessage(ex.Message, true);
                return;
            }

            CreateLineAction.SetSuccess("Create new line succeeded , ID:" + NewInvoiceResult);

            phInvoiceLines.Visible = true;
            gvLines.DataBind();
        }

        public List<Bll.Invoices.InvoiceLine.UnattendedLines> GetInvoiceLines()
        {
            if (ddlIssueToMode.SelectedIndex == 0 && apMerchant.Value.HasValue)
            {
                int merchantId = Bll.Accounts.Account.LoadAccount(apMerchant.Value.Value).MerchantID.Value;
                return Bll.Invoices.InvoiceLine.InvoiceGetUnattendedLines(merchantId, null, int.Parse(ddlCurrency.Value));
            }
            else
            {
                return Bll.Invoices.InvoiceLine.InvoiceGetUnattendedLines(null, txtOther.Text == DEFAULT_ISSUE_TO ? string.Empty : txtOther.Text, int.Parse(ddlCurrency.Value));
            }
        }

        protected void Delete_Command(object sender, CommandEventArgs e)
        {
            int id = int.Parse(e.CommandArgument.ToString());
            Bll.Invoices.InvoiceLine.Delete(id);
            gvLines.DataBind();
        }

        protected void Recalculate(object sender, EventArgs e)
        {
            decimal nBaseRate = Bll.Currency.Get(int.Parse(ddlCurrency.SelectedValue)).BaseRate;
            decimal nVATPercent = decimal.Parse(lblVATPercent.Text.Replace("%", ""));
            decimal nTotal = 0;
            decimal nAmount;

            for (int i = 0; i <= gvLines.Rows.Count - 1; i++)
            {
                nAmount = ((decimal.Parse(gvLines.Rows[i].Cells[gvLines.Rows[i].Cells.Count - 2].Text)) * (decimal.Parse(gvLines.Rows[i].Cells[gvLines.Rows[i].Cells.Count - 6].Text))) / nBaseRate;
                if (rblVATUsage.SelectedValue == "1") nAmount = nAmount / ((100m + nVATPercent) * 100m);
                gvLines.Rows[i].Cells[gvLines.Rows[i].Cells.Count - 1].Text = nAmount.ToString("0.00") + " (Converted to " + ddlCurrency.SelectedCurrencyIso+")";
                nTotal += decimal.Parse(nAmount.ToString("0.00"));
            }

            lblTotal.Text = nTotal.ToString("0.00");
            decimal nVAT = rblVATUsage.SelectedValue == "2" ? 0m : (nTotal / 100m * nVATPercent);
            lblVAT.Text = nVAT.ToString("0.00");
            decimal nTotalVAT = nTotal + nVAT;
            lblTotalVAT.Text = nTotalVAT.ToString("0.00");
        }

        protected void CreateDocument(object sender, EventArgs e)
        {
            Recalculate(null, null);
            if (gvLines.Rows.Count == 0)
            {
                CreateLineAction.SetMessage("Cannot create document: no line is specified", true);
            }
            else if (lblTotal.Text == "0")
            {
                CreateLineAction.SetMessage("Cannot create document: no total calculated", true);
            }
            else
            {
                int nDocumentType = int.Parse(rblInvoiceType.SelectedValue);
                int nBillingCompany = int.Parse(rblBillingCompany.SelectedValue);
                int nApplyVAT = rblVATUsage.SelectedValue == "2" ? 0 : 1;
                int nLinesReduceVAT = rblVATUsage.SelectedValue == "1" ? 1 : 0;
                string sBillTo = ddlIssueToMode.SelectedIndex == 1 ? (txtOther.Text == DEFAULT_ISSUE_TO ? string.Empty : txtOther.Text) : string.Empty;
                int nMerchant = ddlIssueToMode.SelectedIndex == 0 ? Bll.Accounts.Account.LoadAccount(apMerchant.Value.Value).MerchantID.Value : 0;
                int nCurrency = int.Parse(ddlCurrency.SelectedValue);
                string sUsername = Web.WebUtils.UserName;
                DateTime dtDocument = DatePickerValue.Value.HasValue ? DatePickerValue.Value.Value : DateTime.Now;
                int newDocumentInvoiceNumber = -1;
                try
                {
                    newDocumentInvoiceNumber = Bll.Invoices.Local.InvoiceDocumentCreate(0, nDocumentType, nBillingCompany, Convert.ToBoolean(nApplyVAT), Convert.ToBoolean(nLinesReduceVAT), nMerchant, sBillTo, nCurrency, Bll.Currency.ConvertRate(Bll.Currency.Get(nCurrency), Bll.Currency.Get(0)), sUsername, dtDocument.ToString("dd/MM/yyyy HH:mm:ss.fff"), nDocumentType == 4 ? "eng" : null);
                }
                catch (Exception ex)
                {
                    CreateLineAction.SetMessage(ex.Message, true);
                    return;
                }

                //Declare new document created message needed properties
                NewDocInvoiceNumber = newDocumentInvoiceNumber;
                NewDocBillingCompanyID = nBillingCompany;
                NewDocType = nDocumentType;

                //Show the place holder
                phNewDocumentCreated.Visible = true;
                phNewDocumentCreated.DataBind();
                ViewState["FocusPh"] = true;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["FocusPh"] != null && ((bool)ViewState["FocusPh"]))
                {
                    btnCreate.Focus();
                    ViewState["FocusPh"] = null;
                }
            }
            catch
            {

            }
        }
    }
}