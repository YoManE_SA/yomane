﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Invoices
{
    public partial class List : Controls.TemplateControlBase
    {
        public int? InvoiceToUpdateID { get; set; }

        public static List<Bll.Invoices.InvoiceDocument.TotalsResult> TotalsResult { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            rptList.DataBinding += List_DataBinding;
            TemplatePage.PageController.ShowTotals += PageController_ShowTotals;
            base.OnLoad(e);
        }

        private void List_DataBinding(object sender, EventArgs e)
        {
            rptList.DataSource = Netpay.Bll.Invoices.InvoiceDocument
                .Search(TemplatePage.GetFilter<Bll.Invoices.InvoiceDocument.SearchFilters>(), rptList);
        }


        private void PageController_ShowTotals(object sender, EventArgs e)
        {
            var page = ((sender as Controls.DataPageController).Page as Controls.DataTablePage);

            //Get the search filters.
            var sf = page.GetFilter<Bll.Invoices.InvoiceDocument.SearchFilters>();

            //Get totals result by the filter
            TotalsResult = Bll.Invoices.InvoiceDocument.GetTotalsResult(sf);

            //Show totals result
            TotalsModalDialog.BindAndShow();
        }

      
        protected void FirstUpdateClick_Command(object sender, CommandEventArgs e)
        {
            int itemId = int.Parse((e.CommandArgument).ToString());
            InvoiceToUpdateID = itemId;
            TemplatePage.PageController.ListView.BindAndUpdate();
        }

        protected void Cancel_Command(object sender, CommandEventArgs e)
        {
            InvoiceToUpdateID = null;
            TemplatePage.PageController.ListView.BindAndUpdate();
        }

        protected void Update_Command(object sender, CommandEventArgs e)
        {
            int itemId = int.Parse((e.CommandArgument).ToString());
            string newName = (((sender as Button).NamingContainer as GridViewRow).FindControl("NewNameTextBox") as TextBox).Text;

            Bll.Invoices.InvoiceDocument.UpdateBillToName(itemId, newName);
            InvoiceToUpdateID = null;

            TemplatePage.PageController.ListView.BindAndUpdate();
        }
    }
}