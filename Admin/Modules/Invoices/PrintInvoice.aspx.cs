﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.Admin.Invoices
{
    public partial class PrintInvoice : Controls.Page
    {
        public int fExchangeRate = 1;

        public int nPayID { get; set; }

        public int nCurrency { get; set; }

        public int nCompanyID { get; set; }

        public int nBillingCompanysID { get; set; }

        public int nBillingNumber { get; set; }

        public int nInvoiceType { get; set; }

        public bool sIsChargeVAT { get; set; }

        public float sVATamount { get; set; }

        public List<Bll.Invoices.InvoiceLine> InvoiceLines { get; set; }
        
        public Bll.Invoices.InvoiceDocument InvoiceDocumentItem { get; set; }

        public string sInvoiceTypeTextEng
        {
            get
            {
                return Bll.Invoices.InvoiceType.GetInvoiceTypeText(nInvoiceType);
            }
        }

        public string sBillTypeTextEng { get; set; }


        public string sTextDispaly_1 = "To";
        public string sTextDispaly_2 = "Date";
        public string sHeadTextDispaly_1 = "Description";
        public string sHeadTextDispaly_2 = "Amount";
        public string sHeadTextDispaly_3 = "Qty";
        public string sHeadTextDispaly_4 = "Total";
        public string sFeeTextDispaly_1 = "Authorized transaction fee";
        public string sFeeTextDispaly_2 = "Refund transaction fee";
        public string sFeeTextDispaly_3 = "Processing fee";
        public string sFeeTextDispaly_4 = "Financing fees";
        public string sFeeTextDispaly_5 = "Transaction clarification fee - credit cards";
        public string sFeeTextDispaly_6 = "Transaction clarification fee - eCheck";
        public string sFeeTextDispaly_7 = "Transaction Chargeback fee - credit cards";
        public string sFeeTextDispaly_8 = "Transaction Chargeback fee - eCheck";
        public string sFeeTextDispaly_9 = "System transaction";
        public string sTextDispaly_5 = "Total Amount";
        public string sTextDispaly_6 = "VAT";
        public string sTextDispaly_7 = "Total Amount Inc VAT";

        public Bll.Invoices.InvoiceDocument.PrintInvoiceDetails IPD { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            //Get query string params
            var queryString = Page.Request.QueryString;

            if (queryString.AllKeys.Contains("BillingCompanysID")) nBillingCompanysID = int.Parse(queryString["BillingCompanysID"]);
            if (queryString.AllKeys.Contains("billingNumber")) nBillingNumber = int.Parse(queryString["billingNumber"]);
            if (queryString.AllKeys.Contains("invoiceType")) nInvoiceType = int.Parse(queryString["invoiceType"]);

            IPD = Bll.Invoices.InvoiceDocument.GetInvoicePrintDetails(nBillingCompanysID, nBillingNumber, nInvoiceType);
        }
        
        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
        }

        protected void PreviewTypeRadioButtonList_DataBinding(object sender, EventArgs e)
        {
            PreviewTypeRadioButtonList.Items.Add(new ListItem()
            {
                Text = (IPD.IsPrinted.HasValue && IPD.IsPrinted.Value == false) ? "Original Invoice" : "Original Invoice(printed)",
                Enabled = IPD.IsPrinted.HasValue && IPD.IsPrinted.Value == false,
                Value = "PrintOriginal",
                Selected = IPD.IsPrinted.HasValue && IPD.IsPrinted.Value == false
            });

            PreviewTypeRadioButtonList.Items.Add(new ListItem()
            {
                Text = "Invoice Copy",
                Value = "PrintCopy",
                Enabled = true,
                Selected = IPD.IsPrinted.HasValue && IPD.IsPrinted.Value == true
            });
                    
        }
                     

        protected void Preview_Command(object sender, CommandEventArgs e)
        {
            int documentID;
            if ((e.CommandArgument != null) && (int.TryParse(e.CommandArgument.ToString(), out documentID)))
            {
                InvoiceDocumentItem = Bll.Invoices.InvoiceDocument.GetInvoiceDocumentByID(documentID);
                InvoiceLines = Bll.Invoices.InvoiceLine.Search(new Bll.Invoices.InvoiceLine.SearchFilters() { DocumentID = documentID });

                nPayID = InvoiceDocumentItem.TransactionPayID;
                nCurrency = InvoiceDocumentItem.Currency;
                nCompanyID = InvoiceDocumentItem.MerchantID;
                nBillingCompanysID = InvoiceDocumentItem.BillingCompanyID;
                nBillingNumber = InvoiceDocumentItem.InvoiceNumber;
                nInvoiceType = InvoiceDocumentItem.Type;
                sIsChargeVAT = InvoiceDocumentItem.ApplyVat;
                sVATamount = sIsChargeVAT ? InvoiceDocumentItem.VatPercent : 0;

                switch (PreviewTypeRadioButtonList.SelectedValue)
                {
                    case "PrintOriginal":
                        sBillTypeTextEng = "Origin";
                        PreviewUpdatePanel.Visible = true;
                        PreviewMultiView.ActiveViewIndex = 0;
                        PrintButton.Enabled = true;
                        PrintButton.Attributes.Add("style", "cursor:pointer");
                        PreviewUpdatePanel.DataBind();
                        PreviewUpdatePanel.Update();

                        break;

                    case "PrintCopy":
                        sBillTypeTextEng = "Copy";
                        PreviewUpdatePanel.Visible = true;
                        PrintButton.Enabled = true;
                        PrintButton.Attributes.Add("style", "cursor:pointer");
                        PreviewMultiView.ActiveViewIndex = 0;
                        PreviewUpdatePanel.DataBind();
                        PreviewUpdatePanel.Update();
                        break;

                    case "PrintTransactions":
                        break;
                }
            }
        }


        protected void PrintButton_Command(object sender, CommandEventArgs e)
        {
            if (PreviewTypeRadioButtonList.SelectedValue != "PrintOriginal" || e.CommandArgument.ToString() == "-1") return;

            int documentID = int.Parse(e.CommandArgument.ToString());

            Bll.Invoices.InvoiceDocument.SetPrintDateValue(documentID, DateTime.Now);

        }

        protected void PreviewTypeRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
        {
            PrintButton.Enabled = false;
            PrintButton.Attributes.Add("style", "cursor:no-drop");
        }
    }
}