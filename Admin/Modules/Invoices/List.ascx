﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Invoices.List" %>

<admin:ListSection runat="server" Title="Invoices">
    <Header>
        <admin:LegendColors runat="server" Items='<%# Netpay.Bll.Invoices.InvoiceDocument.InvoiceColor %>' ItemsText='<%# Netpay.Bll.Invoices.InvoiceDocument.InvoiceColorText %>' FloatRight="true" />
    </Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" SaveAjaxState="true" ID="rptList" DataKeyNames="ID" AutoGenerateColumns="false" DisableRowSelect="true" BubbleLoadEvent="false">
                <Columns>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <span style="background-color: <%# Netpay.Bll.Invoices.InvoiceDocument.InvoiceColor.MapHtmlColor((bool)Eval("IsManual")) %>;" class="legend-item"></span>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Number">
                        <ItemTemplate>
                           <%# Eval("InvoiceNumber") + " " + Eval("BillingCompanyFirstLetter") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Date">
                        <ItemTemplate>
                            <%# ((DateTime)Eval("InsertDate"))%>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Type">
                        <ItemTemplate>
                            <%# Eval("DocumentTypeText") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Amount">
                        <ItemTemplate>
                            <%# (string)Eval("CurrencySymbol") + ((decimal)Eval("TotalLines")).ToString("#,0.00") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="VAT">
                        <ItemTemplate>
                            <%# (string)Eval("CurrencySymbol") + ((decimal)Eval("TotalVat")).ToString("#,0.00") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Total">
                        <ItemTemplate>
                            <%# (string)Eval("CurrencySymbol") + ((decimal)Eval("TotalDocument")).ToString("#,0.00") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Merchant">
                        <ItemTemplate>
                            <%# Eval("MerchantName").ToString().Length < 20 ? Eval("MerchantName") : Eval("MerchantName").ToString().Substring(0 , 18) + "..."  %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- TBD - implement sOnClick in AdminCash --%>
                    <asp:TemplateField HeaderText="Bill To">
                        <ItemTemplate>
                            <asp:MultiView runat="server" ActiveViewIndex='<%# InvoiceToUpdateID.HasValue && InvoiceToUpdateID.Value == (int)Eval("ID") ? 1 : 0 %>'>
                                <asp:View runat="server">
                                    <asp:ImageButton
                                        OnCommand="FirstUpdateClick_Command"
                                        CommandName="FirstUpdateClick"
                                        CommandArgument='<%# Eval("ID") %>'
                                        ImageAlign="Left"
                                        runat="server"
                                        ImageUrl='<%# ((bool?)(Eval("IsPrinted"))).Value? "~/Images/icon_edit_gray.gif" : "~/Images/icon_edit.gif"  %>'
                                        Enabled='<%# ((bool?)(Eval("IsPrinted"))).Value == false %>' />
                                    <%# Eval("sBillTo") %>
                                </asp:View>
                                <asp:View runat="server">
                                    <asp:TextBox
                                        ID="NewNameTextBox"
                                        Width="200px"
                                        runat="server"
                                        Text='<%# Eval("BillToName") %>'>
                                    </asp:TextBox><br />
                                    <asp:Button CssClass="btn btn-default" runat="server" Text="Cancel" OnCommand="Cancel_Command" />
                                    <asp:Button CssClass="btn btn-success" runat="server" Text="Update" OnCommand="Update_Command" CommandName="Update" CommandArgument='<%# Eval("ID") %>' />
                                </asp:View>
                            </asp:MultiView>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Original </br> Printed">
                        <ItemTemplate>
                            <asp:Image runat="server"
                                ImageUrl='<%# (bool)Eval("IsPrinted")? "~/Images/checkbox.gif" : "~/Images/checkbox_off.gif" %>' />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Print">
                        <ItemTemplate>
                            <asp:HyperLink 
                                ID="PrintInvoice"
                                ClientIDMode="Static"
                                runat="server"
                                ForeColor="#428bca"
                                Style="cursor: pointer;"
                                Text="Print"
                                onclick='<%# "OpenPop(" + "\"" + WebUtils.ApplicationPath + "/Modules/Invoices/PrintInvoice.aspx?"+ string.Format("BillingCompanysID={0}&billingNumber={1}&invoiceType={2}", Eval("BillingCompanyID"), Eval("InvoiceNumber"), Eval("Type")) + "\"" + ",\"PrintInvoice\",\"750\",\"500\",\"1\"); return false;" %>'
                                Target="_blank">
                            </asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Trans. </br> List">
                        <ItemTemplate>
                            <asp:HyperLink
                                Style="cursor: pointer"
                                Target="_blank"
                                runat="server"
                                onclick='<%# "OpenPop(" + "\"" + string.Format("../Settlements/0?ctl00.SpecificSettlementID={0}&Search=1&SelectedIndex=0&NoLayout=1&OpenTab=true#BodyContent_PageController_FormView_FormPanel_Transactions", Eval("TransactionPayID")) + "\"" + ",\"Transactions\",\"1330\",\"820\" , \"1\"); return false;" %>'
                                Text='<%# (int)Eval("TransactionPayID") > 0? Eval("TransactionPayID") : "" %>'>
                            </asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd="false" EnableTotals="true"></admin:DataButtons>
    </Footer>
</admin:ListSection>

<admin:ModalDialog runat="server" ID="TotalsModalDialog" Title="Totals Result">
    <Body runat="server">
        <asp:Repeater runat="server" Visible='<%# TotalsResult!=null && TotalsResult.Any(x=>x.DocumentsTypeID == 0) %>' DataSource='<%# (TotalsResult!=null && TotalsResult.Any(x=>x.DocumentsTypeID == 0)) ? TotalsResult.Where(x=>x.DocumentsTypeID == 0).ToList() : new List<Netpay.Bll.Invoices.InvoiceDocument.TotalsResult>() %>'>
            <HeaderTemplate>
                <b><%# TotalsResult != null && TotalsResult.Any(x=>x.DocumentsTypeID == 0)? TotalsResult.Where(x=>x.DocumentsTypeID == 0).FirstOrDefault().DocumentsTypeName + " Totals" : "" %></b><br />
            </HeaderTemplate>
            <ItemTemplate>
                <b>Documents(<%# Eval("DocumentsCurrencyIso") %>) - <%# Eval("DocumentsCount") %></b><br />
                Amount - <%# Eval("DocumentsCurrencyIso") + " " + ((decimal)Eval("Amount")).ToAmountFormat() %><br />
                VAT - <%# Eval("DocumentsCurrencyIso") + " " + ((decimal)Eval("VAT")).ToAmountFormat() %><br />
                Total - <%# Eval("DocumentsCurrencyIso") + " " + ((decimal)Eval("Total")).ToAmountFormat() %><br />
            </ItemTemplate>
            <FooterTemplate>
                <hr />
            </FooterTemplate>
        </asp:Repeater>

        <asp:Repeater runat="server" Visible='<%# TotalsResult!=null && TotalsResult.Any(x=>x.DocumentsTypeID == 1) %>' DataSource='<%# (TotalsResult!=null && TotalsResult.Any(x=>x.DocumentsTypeID == 1))? TotalsResult.Where(x => x.DocumentsTypeID == 1).ToList() : new List<Netpay.Bll.Invoices.InvoiceDocument.TotalsResult>() %>'>
            <HeaderTemplate>
                <b><%# TotalsResult != null && TotalsResult.Any(x => x.DocumentsTypeID ==1)? TotalsResult.Where(x=>x.DocumentsTypeID == 1).FirstOrDefault().DocumentsTypeName + " Totals" : "" %></b><br />
            </HeaderTemplate>
            <ItemTemplate>
                <b>Documents(<%# Eval("DocumentsCurrencyIso") %>) - <%# Eval("DocumentsCount") %></b><br />
                Amount - <%# Eval("DocumentsCurrencyIso") + " " + ((decimal)Eval("Amount")).ToAmountFormat() %><br />
                VAT - <%# Eval("DocumentsCurrencyIso") + " " + ((decimal)Eval("VAT")).ToAmountFormat() %><br />
                Total - <%# Eval("DocumentsCurrencyIso") + " " + ((decimal)Eval("Total")).ToAmountFormat() %><br />
            </ItemTemplate>
            <FooterTemplate>
                <hr />
            </FooterTemplate>
        </asp:Repeater>

        <asp:Repeater runat="server" Visible='<%# TotalsResult!=null && TotalsResult.Any(x=>x.DocumentsTypeID == 2) %>' DataSource='<%# (TotalsResult!=null && TotalsResult.Any(x=>x.DocumentsTypeID == 2))? TotalsResult.Where(x => x.DocumentsTypeID == 2).ToList() : new List<Netpay.Bll.Invoices.InvoiceDocument.TotalsResult>() %>'>
            <HeaderTemplate>
                <b><%# TotalsResult != null && TotalsResult.Any(x => x.DocumentsTypeID ==2)? TotalsResult.Where(x=>x.DocumentsTypeID == 2).FirstOrDefault().DocumentsTypeName + " Totals" : "" %></b><br />
            </HeaderTemplate>
            <ItemTemplate>
                <b>Documents(<%# Eval("DocumentsCurrencyIso") %>) - <%# Eval("DocumentsCount") %></b><br />
                Amount - <%# Eval("DocumentsCurrencyIso") + " " + ((decimal)Eval("Amount")).ToAmountFormat() %><br />
                VAT - <%# Eval("DocumentsCurrencyIso") + " " + ((decimal)Eval("VAT")).ToAmountFormat() %><br />
                Total - <%# Eval("DocumentsCurrencyIso") + " " + ((decimal)Eval("Total")).ToAmountFormat() %><br />
            </ItemTemplate>
            <FooterTemplate>
                <hr />
            </FooterTemplate>
        </asp:Repeater>

        <asp:Repeater runat="server" Visible='<%# TotalsResult!=null && TotalsResult.Any(x=>x.DocumentsTypeID == 3) %>' DataSource='<%# (TotalsResult!=null && TotalsResult.Any(x=>x.DocumentsTypeID == 3))? TotalsResult.Where(x => x.DocumentsTypeID == 3).ToList() : new List<Netpay.Bll.Invoices.InvoiceDocument.TotalsResult>() %>'>
            <HeaderTemplate>
                <b><%# TotalsResult != null && TotalsResult.Any(x => x.DocumentsTypeID == 3)? TotalsResult.Where(x=>x.DocumentsTypeID == 3).FirstOrDefault().DocumentsTypeName + " Totals" : "" %></b><br />
            </HeaderTemplate>
            <ItemTemplate>
                <b>Documents(<%# Eval("DocumentsCurrencyIso") %>) - <%# Eval("DocumentsCount") %></b><br />
                Amount - <%# Eval("DocumentsCurrencyIso") + " " + ((decimal)Eval("Amount")).ToAmountFormat() %><br />
                VAT - <%# Eval("DocumentsCurrencyIso") + " " + ((decimal)Eval("VAT")).ToAmountFormat() %><br />
                Total - <%# Eval("DocumentsCurrencyIso") + " " + ((decimal)Eval("Total")).ToAmountFormat() %><br />
            </ItemTemplate>
            <FooterTemplate>
                <hr />
            </FooterTemplate>
        </asp:Repeater>

        <asp:Repeater runat="server" Visible='<%# TotalsResult!=null && TotalsResult.Any(x=>x.DocumentsTypeID == 4) %>' DataSource='<%# (TotalsResult!=null && TotalsResult.Any(x=>x.DocumentsTypeID == 4))? TotalsResult.Where(x => x.DocumentsTypeID == 4).ToList() : new List<Netpay.Bll.Invoices.InvoiceDocument.TotalsResult>() %>'>
            <HeaderTemplate>
                <b><%# TotalsResult != null && TotalsResult.Any(x => x.DocumentsTypeID == 4)? TotalsResult.Where(x=>x.DocumentsTypeID == 4).FirstOrDefault().DocumentsTypeName + " Totals" : "" %></b><br />
            </HeaderTemplate>
            <ItemTemplate>
                <b>Documents(<%# Eval("DocumentsCurrencyIso") %>) - <%# Eval("DocumentsCount") %></b><br />
                Amount - <%# Eval("DocumentsCurrencyIso") + " " + ((decimal)Eval("Amount")).ToAmountFormat() %><br />
                VAT - <%# Eval("DocumentsCurrencyIso") + " " + ((decimal)Eval("VAT")).ToAmountFormat() %><br />
                Total - <%# Eval("DocumentsCurrencyIso") + " " + ((decimal)Eval("Total")).ToAmountFormat() %><br />
            </ItemTemplate>
            <FooterTemplate>
                <hr />
            </FooterTemplate>
        </asp:Repeater>
    </Body>
</admin:ModalDialog>
