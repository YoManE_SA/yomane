﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Modules.Utils
{
    public static class PaymentMethodMixins
    {
        public static string GetCardName(int paymentMethod)
        {
            return Bll.PaymentMethods.PaymentMethod.Cache.Where(p => p.ID == paymentMethod).Select(p => p.Name).FirstOrDefault();
        }
    }
}