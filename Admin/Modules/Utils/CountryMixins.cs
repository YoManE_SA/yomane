﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Modules.Utils
{
    public static class CountryMixins
    {
        public static string GetCountryName(string countryIsoCode2)
        {
            return Bll.Country.Cache.Where(c => c.IsoCode2.Equals(countryIsoCode2)).Select(c => c.Name).FirstOrDefault();
        }
    }
}