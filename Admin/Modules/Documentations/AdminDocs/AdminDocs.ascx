﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdminDocs.ascx.cs" Inherits="Netpay.Admin.Modules.Documentaions.AdminDocs" %>

<admin:PanelSection runat="server" Title="Upload docs" Icon="fa-upload">
    <Header>
    </Header>
    <Body>
        <div class="row">
            <div class="col-lg-12">
                Upload new files to server (more then one can be selected)
                <hr />
                <div class="form-group">
                    <div class="input-group multi-search">
                        <span class="input-group-btn">
                            <span class="btn btn-primary btn-file">Browse…
                                 <%--<asp:FileUpload ID="fuNewDoc" accept="application/pdf" Multiple="true" runat="server" />--%>
                                <asp:FileUpload ID="fuNewDoc" runat="server" AllowMultiple="true" accept="application/pdf" onchange='<%# "AdminDocsSelectedFilesHandler(this," + Netpay.Infrastructure.Application.MaxAdminFileManagerUploadSizeBytes + ")" %>' />
                            </span>
                        </span>
                        <input class="form-control" id="fileNames" readonly="" type="text">
                    </div>
                </div>
            </div>
        </div>
    </Body>
    <Footer>
        <asp:Button ID="btnUpload" ClientIDMode="Static" Text="Upload" OnClick="btnUpload_Click" runat="server" CssClass="btn btn-primary pull-right" />
    </Footer>
</admin:PanelSection>

<admin:ListSection ID="ListSection1" runat="server" Title="Admin Docs">
    <Header></Header>
    <Body>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Document name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <asp:Repeater ID="repeaterDocs" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td>
                                <NP:FileUpload ReadOnly="true" runat="server" LocalPath='<%# (Container.DataItem as Netpay.Bll.Content.AdminDocs.DocFile).LocalPath %>' />
                                <%--<a href="<%# (Container.DataItem as Netpay.Bll.Content.AdminDocs.DocFile).Url%>" target="_blank"><%# (Container.DataItem as Netpay.Bll.Content.AdminDocs.DocFile).FileName%></a>--%>
                            </td>
                            <td>
                                <asp:LinkButton OnClientClick="return confirm('Are you sure?');" ID="btnDelete" CommandName="deleteFile" CommandArgument="<%# (Container.DataItem as Netpay.Bll.Content.AdminDocs.DocFile).FileName%>" OnCommand="btnDelete_Command" runat="server" CssClass="btn btn-danger">Delete</asp:LinkButton></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="false" EnableAdd="false" />
    </Footer>

</admin:ListSection>
