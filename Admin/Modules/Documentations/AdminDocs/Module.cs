﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Documentations.AdminDocs
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu AdminDocsMenu;

        public override string Name { get { return "Admin Docs"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return "Admin Docs management"; } }
        public Module() : base(Bll.Content.AdminDocsMng.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu item
            AdminDocsMenu = new ApplicationMenu("Admin Docs", "~/Merchants/Documentations", "fa-suitcase", 6);

            //Register route
            Application.RegisterRoute("Merchants/Documentations", "AdminDocs", typeof(Controls.Page), module:this);

            //Register event
            Application.InitPage += Application_InitPage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {                        
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {                     
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Merchants", AdminDocsMenu);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            AdminDocsMenu.Parent.RemoveChild(AdminDocsMenu);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitPage(object sender, EventArgs e)
        {
            if (!CoreModule.IsActive)
                return;

            var page = sender as Controls.Page;
            if (page.TemplateName == "AdminDocs")
            {
                if (IsActive)
                {
                    if (Bll.Content.AdminDocs.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Read))
                    {
                        if (!Bll.Content.AdminDocs.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Edit))
                        {
                            var control = page.LoadControl("~/Modules/Documentations/AdminDocs/AdminDocs.ascx");
                            Controls.DataTablePage.DisableAllChidControls(control);
                            page.ContentContainer.Controls.Add(control);
                        }
                        else
                        {
                            page.ContentContainer.Controls.Add(page.LoadControl("~/Modules/Documentations/AdminDocs/AdminDocs.ascx"));
                        }
                    }
                    else
                    {
                        page.ContentContainer.Controls.Add(page.ModuleNotAuthorizedControl);
                    }
                }
                else
                {
                    var ctl = page.ModuleNotActivatedControl;
                    (ctl as Modules.ModuleNotActivated.ModuleNotActivated).ModuleName = Name;
                    page.ContentContainer.Controls.Add(ctl);
                }
            }
        }
    }
}