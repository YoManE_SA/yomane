﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Modules.Documentaions
{
    public partial class AdminDocs : System.Web.UI.UserControl
    {
        protected override void OnPreRender(EventArgs e)
        {
            Admin.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterScript(Page, "Plugins/Netpay-Plugin/UploadFile.js");
            base.OnPreRender(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                BindFilesRepeater();
        }

        private void BindFilesRepeater()
        {
            List<Netpay.Bll.Content.AdminDocs.DocFile> docs = Bll.Content.AdminDocs.GetDocs(null);
            repeaterDocs.DataSource = docs;
            repeaterDocs.DataBind();
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (fuNewDoc.PostedFiles.Count > 0)
            {
                for (int idx = 0; idx < fuNewDoc.PostedFiles.Count; idx++)
                {
                    HttpPostedFile file = fuNewDoc.PostedFiles[idx];
                    byte[] data = ReadFile(file.InputStream);
                    Bll.Content.AdminDocs.SaveDoc(file.FileName, data);
                }
                BindFilesRepeater();
            }
        }

        public static byte[] ReadFile(Stream input)
        {
            byte[] buffer = new byte[input.Length];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        protected void btnDelete_Command(object sender, CommandEventArgs e)
        {
            string fileName = e.CommandArgument as string;
            Bll.Content.AdminDocs.DeleteDoc(fileName);
            BindFilesRepeater();
        }
    }
}