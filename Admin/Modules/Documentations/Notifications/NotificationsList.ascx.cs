﻿using Netpay.Bll.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Modules.Documentations
{
    public partial class NotificationsList : Controls.TemplateControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            rptList.DataBinding += RepeaterNotifications_DataBinding;
        }

        private void RepeaterNotifications_DataBinding(object sender, EventArgs e)
        {
            Bulletin.SearchFilters filters = TemplatePage.GetFilter<Bulletin.SearchFilters>();
            List<Bulletin> dataSource = Bulletin.Search(filters, null);
            rptList.DataSource = dataSource;
        }
    }
}