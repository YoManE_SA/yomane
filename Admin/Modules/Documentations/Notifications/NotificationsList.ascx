﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NotificationsList.ascx.cs" Inherits="Netpay.Admin.Modules.Documentations.NotificationsList" %>

<admin:ListSection ID="ListSection1" runat="server" Title="Notifications">
    <Header></Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" SaveAjaxState="true" ID="rptList" DataKeyNames="ID" AutoGenerateColumns="false" BubbleLoadEvent="true"  IsInListView="false">
                <Columns>
                    <asp:BoundField HeaderText="ID" DataField="ID" SortExpression="ID" />
                    <asp:BoundField HeaderText="Type" DataField="TypeFriendlyName" SortExpression="Type" />
                    <asp:BoundField HeaderText="Text" DataField="Text" SortExpression="Text" />
                    <asp:BoundField HeaderText="Date" DataField="InsertDate" SortExpression="MessageDate" />
                    <asp:BoundField HeaderText="Exp. Date" DataField="ExpirationDate" SortExpression="MessageExpirationDate" />
                    <asp:TemplateField HeaderText="Solution">
                        <ItemTemplate>
                            <asp:Literal runat="server" Text='<%# Eval("SolutionName") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd="true" />
    </Footer>
</admin:ListSection>