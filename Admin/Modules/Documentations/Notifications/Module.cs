﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Documentations.Notifications
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu NotificationsMenuItem;

        public override string Name { get { return "Notifications"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return "Notifications management"; } }

        public Module() : base(Bll.Content.NotificationsMng.Module.Current){}

        protected override void OnInit(EventArgs e)
        {
            //Create menu item
            NotificationsMenuItem = new ApplicationMenu("Notifications", "~/Merchants/Notifications", "fa-suitcase", 5);

            //Register route
            Application.RegisterRoute("Merchants/Notifications", "Notifications", typeof(Controls.DataTablePage), module:this);

            //Register event
            Application.InitDataTablePage += Application_InitDataTablePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {              
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Merchants", NotificationsMenuItem);
            base.OnInstallAdmin(e);
        }


        protected override void OnUninstallAdmin(EventArgs e)
        {
            NotificationsMenuItem.Parent.RemoveChild(NotificationsMenuItem);
            base.OnUninstallAdmin(e);
        }

        private void Application_InitDataTablePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "Notifications")
            {
                page.PageController.LoadItem += PageController_LoadItem;
                page.PageController.NewItem += PageController_NewItem;
                page.PageController.PostSaveItem += PageController_PostSaveItem;

                page.AddControlToFilter("Filters", page.LoadControl("~/Modules/Documentations/Notifications/NotificationsFilter.ascx"));
                page.AddControlToList(page.LoadControl("~/Modules/Documentations/Notifications/NotificationsList.ascx"));
                page.AddControlToForm("Edit", page.LoadControl("~/Modules/Documentations/Notifications/NotificationsData.ascx"), this, "", Bll.Content.Bulletin.SecuredObject);
            }
        }
        
        private void PageController_LoadItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            page.ItemID = page.PageController.DataKey["ID"].ToNullableInt();
            if (page.ItemID != null)
            {
                page.SetItemData(Bll.Content.Bulletin.Load(page.ItemID.GetValueOrDefault()));
            }
            if (page.GetItemData<Bll.Content.Bulletin>() == null) page.SetItemData(new Bll.Content.Bulletin());

        }

        private void PageController_NewItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            page.SetItemData(new Bll.Content.Bulletin());
        }

        private void PageController_PostSaveItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            var item = page.GetItemData<Bll.Content.Bulletin>();
            if (item == null) return;

            page.PageController.DataKey = new System.Web.UI.WebControls.DataKey(new System.Collections.Specialized.OrderedDictionary() { { "ID", item.ID } });
        }
    }
}