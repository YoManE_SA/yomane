﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NotificationsFilter.ascx.cs" Inherits="Netpay.Admin.Modules.Documentations.NotificationsFilter" %>

<admin:FilterSection runat="server" Title="Project">
    <asp:Label ID="Label1" runat="server" Text="Project Name" />
    <div class="form-group">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-flag"></i></span>
            <netpay:DropDownBase ID="ddlSolutions" CssClass="form-control" EnableBlankSelection="true" runat="server"></netpay:DropDownBase>
        </div>
    </div>
</admin:FilterSection>

