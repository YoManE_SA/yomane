﻿using Netpay.Bll.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Modules.Documentations
{
    public partial class NotificationsFilter : Controls.TemplateControlBase
    {
        protected override void OnLoad(EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlSolutions.DataSource = Bulletin.GetSolutions();
                ddlSolutions.DataTextField = "value";
                ddlSolutions.DataValueField = "key";
                ddlSolutions.DataBind();
            }

            TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Bulletin.SearchFilters();
            if (ddlSolutions.IsSelected)
            {
                sf.SolutionId = byte.Parse(ddlSolutions.SelectedValue);
            }
            TemplatePage.SetFilter(sf);
        }

        private void PageController_Search(object sender, EventArgs e)
        {
            UpdateSearchFilter();
        }
    }
}