﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NotificationsData.ascx.cs" Inherits="Netpay.Admin.Modules.Documentations.NotificationsData" %>
<admin:FormSections runat="server" Title="Edit Notification">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <i class="fa fa-flag"></i>Project name:
                <asp:DropDownList ID="ddlSolutions" CssClass="form-control" runat="server"></asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <i class="fa fa-flag"></i>Type:
                <asp:DropDownList ID="ddlType" CssClass="form-control" runat="server">
                    <asp:ListItem Value="GEN_INFO">General Info</asp:ListItem>
                    <asp:ListItem Value="SYS_INFO">System Info</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <i class="fa fa-calendar"></i>
                <asp:Label runat="server" Text="Date"></asp:Label>
                <JQ:DateRange ID="rangeDate" runat="server" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <i class="fa fa-pencil-square-o"></i>Notification:
                <asp:TextBox ID="txtNotification" CssClass="form-control" TextMode="MultiLine" Text='<%# ItemData.Text %>' Rows="6" Columns="40" runat="server"></asp:TextBox>
            </div>
        </div>
    </div>

</admin:FormSections>
