﻿using Netpay.Bll.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Modules.Documentations
{
    public partial class NotificationsData : Controls.TemplateControlBase
    {
        protected Bulletin ItemData;
        //private bool IsNew = false;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            TemplatePage.PageController.SaveItem += PageController_SaveItem;
            TemplatePage.PageController.DeleteItem += PageController_DeleteItem;
        }
           
        
        protected override void DataBindChildren()
        {
            if (TemplatePage.ItemID == null || TemplatePage.ItemID == 0)
                ItemData = new Bulletin();
            else
                ItemData = Bulletin.Load(TemplatePage.ItemID.Value);

            ddlSolutions.DataSource = Bulletin.GetSolutions();
            ddlSolutions.DataTextField = "value";
            ddlSolutions.DataValueField = "key";
            ddlSolutions.SelectedValue = ItemData.SolutionID.ToString();
            ddlType.SelectedValue = ItemData.Type;
            rangeDate.Value = new Infrastructure.Range<DateTime?>(ItemData.InsertDate, ItemData.ExpirationDate);

            base.DataBindChildren();
        }

        private void PageController_DeleteItem(object sender, EventArgs e)
        {
            Bulletin.Load(TemplatePage.ItemID.Value).Delete();

            TemplatePage.PageController.ListView.DataBind();
            TemplatePage.PageController.ListView.Update();
        }

        private void PageController_SaveItem(object sender, EventArgs e)
        {
            if (txtNotification.Text.Trim() == string.Empty)
                return;

            if (rangeDate.Value.From == null || rangeDate.Value.To == null)
                return;

            ItemData = TemplatePage.GetItemData<Bll.Content.Bulletin>();
            if (ItemData == null) return;

            ItemData.Text = txtNotification.Text;
            ItemData.Type = ddlType.SelectedValue;
            ItemData.SolutionID = byte.Parse(ddlSolutions.SelectedValue);
            ItemData.InsertDate = rangeDate.Value.From.Value;
            ItemData.ExpirationDate = rangeDate.Value.To.Value;
            ItemData.Save();

            TemplatePage.PageController.FilterView.DataBind();
            TemplatePage.PageController.ListView.DataBind();
            TemplatePage.PageController.ListView.Update();
        }
    }
}