﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Notifications.ascx.cs" Inherits="Netpay.Admin.Merchants.Notifications" %>

<asp:DropDownList ID="ddlSolutions" OnSelectedIndexChanged="ddlSolutions_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:DropDownList>
<br />
<asp:TextBox ID="txtNotification" TextMode="MultiLine" Rows="8" Columns="40" runat="server"></asp:TextBox>
<br />
<asp:Button ID="btnAdd" Text="Add" OnClick="btnAdd_Click" runat="server" />
<br />
<asp:Repeater ID="repeaterNotifications" runat="server">
    <ItemTemplate>
        <div>
            <%# (Container.DataItem as Netpay.Bll.Content.Bulletin).Text%>
             - 
            <%# (Container.DataItem as Netpay.Bll.Content.Bulletin).Date%>
             - 
            <asp:LinkButton ID="btnDelete" CommandName="deleteFile" CommandArgument="<%# (Container.DataItem as Netpay.Bll.Content.Bulletin).ID%>" OnCommand="btnDelete_Command" runat="server">Delete</asp:LinkButton>
        </div>
        <br />
        <br />
    </ItemTemplate>
</asp:Repeater>