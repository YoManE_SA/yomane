﻿using Netpay.Bll;
using Netpay.Bll.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Merchants
{
    public partial class Notifications : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlSolutions.DataSource = Bulletin.GetSolutions();
                ddlSolutions.DataTextField = "value";
                ddlSolutions.DataValueField = "key";
                ddlSolutions.DataBind();
                BindRepeater();
            }
        }

        private void BindRepeater() 
        {
            Bulletin.SearchFilters filters = new Bulletin.SearchFilters();
            filters.SolutionId = byte.Parse(ddlSolutions.SelectedValue);
            List<Bulletin> dataSource = Bulletin.Search(filters, null);
            repeaterNotifications.DataSource = dataSource;
            repeaterNotifications.DataBind();
        }
        
        protected void btnDelete_Command(object sender, CommandEventArgs e)
        {
            int bulletinId = int.Parse(e.CommandArgument.ToString());
            Bulletin.Load(bulletinId).Delete();
            BindRepeater();
        }

        protected void ddlSolutions_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindRepeater();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            string text = txtNotification.Text.Trim();
            if (text == string.Empty)
                return;

            byte solutionId = byte.Parse(ddlSolutions.SelectedValue);
            Bulletin bulletin = new Bulletin();
            bulletin.SolutionID = solutionId;
            bulletin.Date = DateTime.Now;
            bulletin.Text = text;
            bulletin.Save();

            BindRepeater();
        }
    }
}