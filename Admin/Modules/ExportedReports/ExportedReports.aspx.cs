﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Bll.Reports;
using Netpay.Infrastructure.Tasks;
using Netpay.Admin.Controls.GlobalControls;

namespace Netpay.Admin.Modules.ExportedReports
{
    public partial class ExportedReports : Controls.ModulePage
    {
        public ExportedReports() : base(Module.ModuleName) { }

        protected override void OnLoad(EventArgs e)
        {
            if (!IsPostBack)
            {
                GetGeneratedReportList();
                GetQueuedReportList();
            }
        }
        protected void GetGeneratedReportList()
        {
            List<GeneratedReportInfo> exportedAdminListReports = FiledReports.GetGeneratedAdminListExports();
            repeaterGeneratedReports.DataSource = exportedAdminListReports;
            repeaterGeneratedReports.DataBind();

            SetRepeaterVisibility();
        }

        protected void GetQueuedReportList()
        {
            List<TaskInfo> queuedReports = FiledReports.GetQueuedAdminListExports();
            repeaterQueuedReports.DataSource = queuedReports;
            repeaterQueuedReports.DataBind();

            phQueuedTasks.Visible = queuedReports.Count > 0;
            SetRepeaterVisibility();
        }

        protected void btnDownloadFile_click(object source, CommandEventArgs e)
        {
            string fileName = e.CommandArgument.ToString();

            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + "\"" + fileName + "\"");
            Response.ContentType = "text/csv";
            Response.TransmitFile(FiledReports.GetAdminListExportFullName(fileName));

            //string clientid = (Page.Master as Site).UpdateProgress.ClientID;
            //Admin.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQueryStartUp(this, "$('#" + clientid + "').hide();");

            Response.End();
        }

        protected void btnDeleteFile_click(object source, CommandEventArgs e)
        {
            string fileName = e.CommandArgument.ToString();
            FiledReports.DeleteExportedCsv(fileName);
            GetGeneratedReportList();

            SetRepeaterVisibility();
        }

        protected void btnCancelReport_click(object source, CommandEventArgs e)
        {
            Guid taskID = new Guid(e.CommandArgument.ToString());
            FiledReports.CancelReport(taskID);
            GetQueuedReportList();

            SetRepeaterVisibility();
        }

        protected void btnRefresh_click(object source, EventArgs e)
        {
            GetGeneratedReportList();
            GetQueuedReportList();

            SetRepeaterVisibility();
        }

        public void SetRepeaterVisibility()
        {
            if (repeaterGeneratedReports.Items.Count > 0 || repeaterQueuedReports.Items.Count > 0)
            {
                phGeneratedReports.Visible = true;
            }
            else
            {
                phGeneratedReports.Visible = false; 
            }
        }

    }
}