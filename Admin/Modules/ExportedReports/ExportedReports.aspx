﻿<%@ Page Title="Exported Reports" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ExportedReports.aspx.cs" Inherits="Netpay.Admin.Modules.ExportedReports.ExportedReports" %>

<asp:Content ContentPlaceHolderID="BodyContent" runat="server">
    <asp:PlaceHolder ID="phQueuedTasks" runat="server">
        <div class="panel panel-info">
            <div class="panel-heading">Please wait while exporting</div>
            <div class="panel-body">
                <div class="pull-left">
                    <asp:Label ID="Label1" Text="Your requested exports are being generated.<br/>Depending on the system workload, This may take from a few minutes to up to an hour." runat="server" />
                </div>
                <div class="pull-right">
                    <asp:Button Text="Refresh" OnClick="btnRefresh_click" CssClass="btn btn-default" runat="server" />
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </asp:PlaceHolder>
    <div class="panel panel-default">
        <div class="panel-heading">
            <asp:Localize Text="Exported Reports" runat="server" />
        </div>
        <div class="panel-body">
            <asp:PlaceHolder ID="phGeneratedReports" runat="server">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>
                                <asp:Literal Text="FileName" runat="server" /></th>
                            <th>
                                <asp:Literal Text="Size" runat="server" /></th>
                            <th>
                                <asp:Literal Text="Created" runat="server" /></th>
                            <th>&nbsp;</th>
                        </tr>
                        <asp:Repeater ID="repeaterQueuedReports" runat="server">
                            <ItemTemplate>
                                <tr style="background-color: #FFFFCC;">
                                    <td>
                                        <asp:Literal Text="Requested" runat="server" />: 
                                        <%# ((Netpay.Infrastructure.Tasks.TaskInfo)Container.DataItem).Description %> &nbsp;
										<%# ((Netpay.Infrastructure.Tasks.TaskInfo)Container.DataItem).InsertDate.ToString("dd/MM/yy HH:MM") %>
                                    </td>
                                    <td>---</td>
                                    <td>
                                        <asp:Literal Text="Waiting" runat="server" />
                                    </td>
                                    <td>
                                        <asp:LinkButton
                                            ID="btnCancelReport"
                                            CommandArgument="<%# ((Netpay.Infrastructure.Tasks.TaskInfo)Container.DataItem).TaskID.ToString() %>"
                                            OnCommand="btnCancelReport_click"
                                            runat="server"
                                            Text="Cancel"
                                            OnClientClick="return confirm('Are you sure you want to cancel that export task?');">
                                            <i class="fa fa-trash"></i>
                                        </asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:Repeater ID="repeaterGeneratedReports" runat="server">
                            <ItemTemplate>
                                <tr onmouseover="this.style.backgroundColor='#F5F5F5';" onmouseout="this.style.backgroundColor='#ffffff';">
                                    <td>
                                        <img alt="" align="absmiddle" src="/NPCommon/ImgFileExt/14X14/<%# ((Netpay.Bll.Reports.GeneratedReportInfo)Container.DataItem).ReportFile.Extension.Remove(0,1) %>.gif" />
                                        <asp:LinkButton
                                            ID="btnDownloadFile"
                                            CommandArgument='<%# ((Netpay.Bll.Reports.GeneratedReportInfo)Container.DataItem).ReportFile.Name %>'
                                            OnCommand="btnDownloadFile_click"
                                            runat="server"
                                            ForeColor="#808080">
                                            <%# ((Netpay.Bll.Reports.GeneratedReportInfo)Container.DataItem).ReportFile.Name %> <i class="fa fa-download" style="color: #4A885B"></i>
                                        </asp:LinkButton>
                                    </td>
                                    <td><%# ((Netpay.Bll.Reports.GeneratedReportInfo)Container.DataItem).ReportFile.Length.ToFileSize()%>
                                    </td>
                                    <td><%# ((Netpay.Bll.Reports.GeneratedReportInfo)Container.DataItem).ReportFile.CreationTime.ToString()%>
                                    </td>
                                    <td>
                                        <asp:LinkButton
                                            ID="btnDeleteFile"
                                            CommandArgument='<%# ((Netpay.Bll.Reports.GeneratedReportInfo)Container.DataItem).ReportFile.Name %>'
                                            OnCommand="btnDeleteFile_click"
                                            OnClientClick="return confirm('Are you sure you want to delete that csv file?');"
                                            runat="server"
                                            Text="Delete">
                                            <i class="fa fa-trash"></i>
                                        </asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </asp:PlaceHolder>
        </div>
    </div>
</asp:Content>

