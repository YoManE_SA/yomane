﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Modules.ExportedReports
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuExport;

        public static readonly string ModuleName = "Export from AdminList";
        public override string Name { get { return ModuleName; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return ""; } }

        public Module() : base(Netpay.Bll.Reports.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu item
            menuExport = new ApplicationMenu("Exported Reports", "~/Modules/ExportedReports/ExportedReports.aspx", null, 500);

            //Register route
            Application.RegisterRoute("Logs/ExportedReports", "ExportedReports", typeof(ExportedReports), module: this);

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Logs", menuExport);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuExport.Parent.RemoveChild(menuExport);
            base.OnUninstallAdmin(e);
        }
    }
}