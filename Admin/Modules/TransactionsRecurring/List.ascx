﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.TransactionsRecurring.List" %>
<admin:ListSection ID="ListSectionRecurringTransactions" runat="server" Title="Recurring Series">
    <Header></Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" ID="rptList" SaveAjaxState="true" DataKeyNames="ID" AutoGenerateColumns="false" BubbleLoadEvent="true">
                <Columns>
                    <%-- ID --%>
                    <asp:BoundField HeaderText="ID" DataField="ID" />

                    <%-- Merchant --%>
                    <asp:BoundField HeaderText="Merchant" DataField="Merchant.Name" />

                    <%-- Charges --%>
                    <asp:BoundField HeaderText="Charges" DataField="Charges" />

                    <%-- Type --%>
                    <asp:TemplateField HeaderText="Type">
                        <ItemTemplate>
                            <asp:Literal runat="server" Text="TBD"></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Interval --%>
                    <asp:BoundField HeaderText="Interval" DataField="IntervalDisplayText" />

                    <%-- Amount --%>
                    <asp:TemplateField HeaderText="Amount">
                        <ItemTemplate>
                            <%# new Money((decimal)Eval("Amount"), ((int?)Eval("Currency")).GetValueOrDefault()).ToIsoString() %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Start Date --%>
                    <asp:BoundField HeaderText="Start Date" DataField="StartDate" />

                    <%-- Paid --%>
                    <asp:BoundField HeaderText="Paid" DataField="Paid" />

                    <%-- Blocked --%>
                    <asp:BoundField HeaderText="Blocked" DataField="Blocked" />

                    <%-- Suspended --%>
                    <asp:BoundField HeaderText="Suspended" DataField="Suspended" />

                    <%-- Deleted --%>
                    <asp:BoundField HeaderText="Deleted" DataField="Deleted" />
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" />
    </Footer>
</admin:ListSection>

