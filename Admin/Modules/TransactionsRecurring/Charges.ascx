﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Charges.ascx.cs" Inherits="Netpay.Admin.TransactionsRecurring.Charges" %>
<admin:FormSections runat="server" Title="Charges">
    <NP:UpdatePanel runat="server" ID="upChargesList" RenderMode="Block" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:MultiView runat="server" ActiveViewIndex='<%# (rptChargesList.Items.Count > 0) ? 0 : 1 %>'>
                <asp:View runat="server">
                    <NP:PagedRepeater runat="server" ID="rptChargesList" PageSize="20" OnSelectItem="List_OnSelectItem">
                        <ItemTemplate>
                            <div class="panel panel-default">
                                <div class="panel-heading font-small" style="background-color: #fcfcfc;">
                                    <div class="row">
                                        <div class="col-lg-8 col-md-8">
                                             <div class="<%# Container.ItemIndex % 2 == 0 ? "alternate" : "" %>" onclick="<%# rptChargesList.GetSelectItemEventReference(Container.ItemIndex) %>">Date <%# Eval("Date") %></div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 text-right font-small">
                                           <NP:Button ID="ButtonShowAttempts" runat="server" CssClass="btn btn-info btn-xs text-uppercase" OnCommand="OnCommand" CommandName="ShowAttempts" CommandArgument='<%# Eval("ID") %>' Text="Attempts" />
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <ul class="list-group">
                                                <li class="list-group-item">ID:
                                                    <asp:Literal runat="server" ID="ltId" Text='<%# Eval("ID") %>' /></li>
                                                <li class="list-group-item">Charge Number: <%# Eval("ChargeNumber") %></li>
                                                <li class="list-group-item">Date: <%# Eval("Date") %></li>

                                            </ul>
                                        </div>
                                        <div class="col-md-4">
                                            <ul class="list-group">
                                                <li class="list-group-item">Credit Card Id:  <%# Eval("CreditCard") %></li>
                                                <li class="list-group-item">Amount: <%# new Money((decimal)Eval("Amount"), ((int?)Eval("Currency")).GetValueOrDefault()).ToIsoString() %></li>
                                                <li class="list-group-item">Attempts: <%# Eval("Attempts") %></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-4">
                                            <ul class="list-group">
                                                <li class="list-group-item">Blocked: <%# Eval("Paid") %></li>
                                                <li class="list-group-item">Suspended: <%# Eval("Blocked") %></li>
                                                <li class="list-group-item">Pending:  <%# Eval("Pending") %></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </NP:PagedRepeater>
                </asp:View>
                <asp:View runat="server">
                    <p class="text-center">No charges found</p>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
    </NP:UpdatePanel>
</admin:FormSections>
<admin:ModalDialog runat="server" ID="dlgShowChargeHistory" Title="Charge Attempts">
    <Body>
        <NP:UpdatePanel runat="server" ID="upAttemptsList" RenderMode="Block" ChildrenAsTriggers="false" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:MultiView runat="server" ActiveViewIndex='<%# (rptAttemptsList.Items.Count > 0) ? 0 : 1 %>'>
                    <asp:View runat="server">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Date</th>
                                        <th>Transaction Status</th>
                                        <th>Transaction Id</th>
                                        <th>Comment</th>
                                    </tr>
                                </thead>
                                <NP:PagedRepeater runat="server" ID="rptAttemptsList" PageSize="20">
                                    <ItemTemplate>
                                        <tr class="<%# Container.ItemIndex % 2 == 0 ? "alternate" : "" %>">
                                            <td><asp:Literal runat="server" ID="ltId" Text='<%# Eval("ID") %>' /></td>
                                            <td><%# Eval("Date") %></td>
                                            <td><%# Eval("TransactionStatus") %> </td>
                                            <td><%# Eval("TransactionId") %></td>
                                            <td><%# Eval("Comment") %></td>
                                        </tr>
                                    </ItemTemplate>
                                </NP:PagedRepeater>
                            </table>
                        </div>
                    </asp:View>
                    <asp:View runat="server">
                        <p class="text-center">No charge attempts found</p>
                    </asp:View>
                </asp:MultiView>
            </ContentTemplate>
        </NP:UpdatePanel>
    </Body>
</admin:ModalDialog>
