﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Data.ascx.cs" Inherits="Netpay.Admin.TransactionsRecurring.Data" %>
<admin:FormSections runat="server" Title="General">
    <asp:HiddenField runat="server" ID="hfItemID" Value='<%# Item.ID %>' />
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <ul class="list-group">
                    <li class="list-group-item">Merchant:
                <asp:Label runat="server" ID="lblMerchant" Text='<%# Item.Merchant != null ? Item.Merchant.Name : "" %>' />
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <ul class="list-group">
                    <li class="list-group-item">Series Number:
                <asp:Label runat="server" Text='<%# Item.SeriesNumber %>' />
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-12">
            <ul class="list-group">
                <li class="list-group-item">Comments:
                <asp:Label runat="server" Text='<%# Item.Comments %>' />
                </li>
            </ul>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Payment">
    <div class="row">
        <div class="col-md-4">
            <ul class="list-group">
                <li class="list-group-item">Amount:
                <asp:Label runat="server" Text='<%# new Money(Item.Amount, Item.Currency.GetValueOrDefault()).ToIsoString() %>' /></li>
                <li class="list-group-item">Start Date:
                <asp:Label runat="server" Text='<%# Item.StartDate %>' /></li>
            </ul>
        </div>
        <div class="col-md-4">
            <ul class="list-group">
                <li class="list-group-item">Charges:
                <asp:Label runat="server" Text='<%# Item.Charges %>' /></li>
                <li class="list-group-item">Interval:
                <asp:Label runat="server" Text='<%# Item.IntervalDisplayText %>' /></li>
            </ul>
        </div>
        <div class="col-md-4">
            <ul class="list-group">
                <li class="list-group-item">Series Total:
                <asp:Label runat="server" Text='<%# new Money(Item.Total, Item.Currency.GetValueOrDefault()).ToIsoString() %>' /></li>

            </ul>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Status">
    <div class="row">
        <div class="col-md-4">
            <ul class="list-group">
                <li class="list-group-item">Paid: 
                    <asp:Label runat="server" Text='<%# Item.Paid %>' /></li>
                <li class="list-group-item">Suspended:
                    <asp:Label runat="server" Text='<%# Item.Suspended %>' /></li>
                <li class="list-group-item">Blocked:
                    <asp:Label runat="server" Text='<%# Item.Blocked %>' /></li>

            </ul>
        </div>
        <div class="col-md-4">
            <ul class="list-group">
                <li class="list-group-item">Deleted:
                    <asp:Label runat="server" Text='<%# Item.Deleted %>' /></li>
                <li class="list-group-item">Flexible:
                    <asp:Label runat="server" Text='<%# Item.IsFlexible %>' /></li>
                <li class="list-group-item">PreAuthorized:
                    <asp:Label runat="server" Text='<%# Item.IsPreAuthorized %>' /></li>

            </ul>
        </div>
        <div class="col-md-4">
            <ul class="list-group">
                <li class="list-group-item">Precreated:
                    <asp:Label runat="server" Text='<%# Item.IsPrecreated %>' /></li>
                <li class="list-group-item">First Approval: 
                    <asp:Label runat="server" Text='<%# Item.IsFirstApproval %>' /></li>
                <li class="list-group-item">IP:
                    <asp:Label runat="server" Text='<%# Item.IP %>' /></li>
            </ul>
        </div>
    </div>

</admin:FormSections>
