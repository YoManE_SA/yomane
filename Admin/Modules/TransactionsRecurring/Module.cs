﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;
using Netpay.Web;

namespace Netpay.Admin.TransactionsRecurring
{
	public class Module : Admin.CoreBasedModule
	{
		private ApplicationMenu menuItem;
		public override string Name { get { return "TransactionsRecurring"; } }
		public override decimal Version { get { return 1.0m; } }
		public override string Author { get { return "OBL ltd."; } }
		public override string Description { get { return ""; } }

        public Module(): base(Bll.Transactions.Recurring.Module.Current) { }

		protected override void OnInit(EventArgs e)
		{
            //Create menu item
            menuItem = new ApplicationMenu("Recurring", "~/Transactions/Recurring/0", null, 70);

            //Register route
            Application.RegisterRoute("Transactions/Recurring/{*id}", "TransactionsRecurring", typeof(Controls.DataTablePage), module : this);

            //Register event 
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
		}

		protected override void OnActivate(EventArgs e)
		{
			base.OnActivate(e);
		}

		protected override void OnDeactivate(EventArgs e)
		{			
			base.OnDeactivate(e);
		}

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Transactions", menuItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuItem.Parent.RemoveChild(menuItem);
            base.OnUninstallAdmin(e);
        }

        private void Application_InitTemplatePage(object sender, EventArgs e)
		{
            if (!CoreModule.IsInstalled)
                return;

			var page = sender as Controls.DataTablePage;
			if (page.TemplateName == "TransactionsRecurring")
			{
                page.PageController.LoadItem += PageController_LoadItem;
                page.AddControlToList(page.LoadControl("~/Modules/TransactionsRecurring/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/TransactionsRecurring/Filter.ascx"));
                page.AddControlToForm("Data", page.LoadControl("~/Modules/TransactionsRecurring/Data.ascx"), this, "", Bll.Transactions.Recurring.Series.SecuredObject);
                page.AddControlToForm("Charges", page.LoadControl("~/Modules/TransactionsRecurring/Charges.ascx"), this, "", Bll.Transactions.Recurring.Series.SecuredObject);
            }
        }

		void PageController_LoadItem(object sender, EventArgs e)
		{
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
			page.ItemID = page.PageController.DataKey["ID"].ToNullableInt();
            page.FormButtons.EnableSave = false;
            page.FormButtons.EnableDelete = false;
            if (page.ItemID != null) page.SetItemData(Bll.Transactions.Recurring.Series.Load(page.ItemID.GetValueOrDefault()));
		}
	}
}