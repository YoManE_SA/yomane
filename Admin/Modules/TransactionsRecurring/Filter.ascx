﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.TransactionsRecurring.Filter" %>

<admin:FilterSection runat="server" Title="Status">
    <div class="form-group">
        <asp:Label ID="lblAccount" runat="server" Text="Name" />
        <admin:AccountPicker runat="server" ID="apAccount" LimitToType="Merchant" UseTargetID="false" />
    </div>
    <hr />
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label runat="server" Text="Series Start Date" />
                <JQ:DateRange runat="server" ID="rng_StartDate" />
            </div>
        </div>
    </div>
    <hr />
    <div class="form-group">
        <asp:Label runat="server" Text="Series Total Amount" />
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
            <netpay:CurrencyDropDown CssClass="form-control" runat="server" ID="ddlCurrency" />
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                From Amount
                <JQ:DecimalRange runat="server" ID="rng_Amount" />
            </div>
        </div>
    </div>
    <hr />

    <div class="form-group">
        <asp:Label ID="Label1" runat="server" Text="Status" />
        <netpay:EnumDropDown runat="server" CssClass="form-control" ViewEnumName="Netpay.Bll.Transactions.Recurring.SeriesStatus" ID="ddlStatus" />
    </div>
</admin:FilterSection>

<admin:FilterSection runat="server" Title="Record Information">
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblID" runat="server" Text="ID Range" />
                <JQ:IntRange runat="server" ID="rngID" />
            </div>
        </div>
    </div>
</admin:FilterSection>
