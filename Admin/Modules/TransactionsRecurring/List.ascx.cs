﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Admin.Modules.Utils;
using Netpay.Infrastructure;

namespace Netpay.Admin.TransactionsRecurring
{
	public partial class List : Controls.TemplateControlBase
    {
        protected Dictionary<int, string> m_accountNames;

        protected override void OnLoad(EventArgs e)
        {
            rptList.DataBinding += List_DataBinding;
            base.OnLoad(e);
        }

        protected void List_DataBinding(object sender, EventArgs e)
        {
            var list = Bll.Transactions.Recurring.Series.Search(TemplatePage.GetFilter<Bll.Transactions.Recurring.Series.SearchFilters>(), rptList, true);
            m_accountNames = Bll.Accounts.Account.GetNames(list.Where(i => i.MerchantId != null).Select(i => (int)i.MerchantId).ToList());
            rptList.DataSource = list;
        }
    }
}