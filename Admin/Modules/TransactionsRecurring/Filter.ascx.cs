﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.Admin.TransactionsRecurring
{
	public partial class Filter : Controls.TemplateControlBase
	{
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
        }

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
        {
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Bll.Transactions.Recurring.Series.SearchFilters();
            sf.ID = rngID.Value;
            if (apAccount.Value.HasValue)
            {
                var merchant = Bll.Merchants.Merchant.LoadByAccountId(apAccount.Value.Value);
                if (merchant != null)
                {
                    sf.MerchantId = new Range<int?>(merchant.ID);
                }
            }
            sf.Date = rng_StartDate.Value;
            sf.Amount = rng_Amount.Value;
            sf.CurrencyId = (Netpay.CommonTypes.Currency?)ddlCurrency.SelectedValue.ToNullableInt();
            sf.Status = (Netpay.Bll.Transactions.Recurring.SeriesStatus?)ddlStatus.SelectedValue.ToNullableInt();
            TemplatePage.SetFilter(sf);
        }
    }
}