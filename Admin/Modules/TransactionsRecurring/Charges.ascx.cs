﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.TransactionsRecurring
{
    public partial class Charges : Controls.TemplateControlBase
    {
        public Bll.Transactions.Recurring.Series Item { get { return TemplatePage.GetItemData<Bll.Transactions.Recurring.Series>(); } set { TemplatePage.SetItemData(value); } }
        public List<Bll.Transactions.Recurring.Charge> ChargesList { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }


        protected override void DataBindChildren()
        {
            if (Item == null) Item = new Bll.Transactions.Recurring.Series();
            LoadChargesList();
            base.DataBindChildren();
        }
        

        protected void LoadChargesList()
        {
            if (Item == null)
                return;
            Netpay.Bll.Transactions.Recurring.Charge.SearchFilters chargesFilter = new Bll.Transactions.Recurring.Charge.SearchFilters();
            chargesFilter.SeriesId = Item.ID;
            ChargesList = Netpay.Bll.Transactions.Recurring.Charge.Search(chargesFilter, null);

            rptChargesList.DataSource = ChargesList;
            rptChargesList.DataBind();

            upChargesList.Update();
        }

        protected void List_OnSelectItem(object sender, RepeaterCommandEventArgs e)
        {
            if (e == null || e.Item == null)
                return;

            // Get selected Charge Id
            var rptItem = e.Item;
            var chargeId = (rptItem.FindControl("ltId") as System.Web.UI.WebControls.Literal).Text.ToNullableInt().GetValueOrDefault();

            ShowAttempts(chargeId);
        }

        protected List<Bll.Transactions.Recurring.ChargeHistory> LoadChargeHistory(int chargeId)
        {
            Netpay.Bll.Transactions.Recurring.ChargeHistory.SearchFilters filter = new Bll.Transactions.Recurring.ChargeHistory.SearchFilters();
            filter.ChargeID = chargeId;
            List<Bll.Transactions.Recurring.ChargeHistory> chargeHistory;
            chargeHistory = Netpay.Bll.Transactions.Recurring.ChargeHistory.Search(filter, null);

            return chargeHistory;
        }

        protected void OnCommand(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "ShowAttempts":
                    var selectedChargeId = e.CommandArgument.ToNullableInt().GetValueOrDefault();
                    ShowAttempts(selectedChargeId);
                    break;
            }
        }

        protected void ShowAttempts(int chargeId)
        {
            // Load charge's history
            var chargeHistory = LoadChargeHistory(chargeId);

            rptAttemptsList.DataSource = chargeHistory;
            rptAttemptsList.DataBind();

            dlgShowChargeHistory.BindAndShow();
            upAttemptsList.Update();
        }

    }
}