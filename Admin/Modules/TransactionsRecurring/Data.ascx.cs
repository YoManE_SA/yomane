﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.TransactionsRecurring
{
    public partial class Data : Controls.TemplateControlBase
    {
        public Bll.Transactions.Recurring.Series Item { get { return TemplatePage.GetItemData<Bll.Transactions.Recurring.Series>(); } set { TemplatePage.SetItemData(value); } }

        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += Save_Click;
            TemplatePage.PageController.DeleteItem += Delete_Click;
            base.OnLoad(e);
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            Item = Netpay.Bll.Transactions.Recurring.Series.Load(hfItemID.Value.ToNullableInt().GetValueOrDefault());
            Item.Delete();
        }

        protected override void DataBindChildren()
        {
            if (Item == null) Item = new Bll.Transactions.Recurring.Series();
            base.DataBindChildren();
        }
        
        protected void Save_Click(object sender, EventArgs e)
        {
            if (Item == null)
                Item = new Netpay.Bll.Transactions.Recurring.Series();
        }

    }
}