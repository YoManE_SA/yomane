﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Modules.Wire.WireProviders.Hellenic
{
    public class Module : Admin.CoreBasedModule
    {
        public override string Name { get { return "WireProvider Hellenic"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return "Hellenic wire provider"; } }

        public Module() : base(Bll.Wires.WireProviders.HellenicModule.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {
            Bll.Wires.Provider.RemoveCache();
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {
            Bll.Wires.Provider.RemoveCache();
            base.OnDeactivate(e);
        }
    }
}