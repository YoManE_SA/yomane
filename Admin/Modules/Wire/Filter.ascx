﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Wire.Filter" %>
<admin:FilterSection runat="server" Title="Status">
    <div class="form-group">
        <asp:Label ID="lblAccount" runat="server" Text="Account" />
        <admin:AccountPicker runat="server" ID="apAccount" />
    </div>
    <div class="form-group">
        <h4>
            <asp:Label ID="lblStatus" runat="server" Text="Wire Status" /></h4>
        <asp:CheckBoxList runat="server" ID="chklStatus" CssClass="checkbox-list" RepeatLayout="UnorderedList" DataSource='<%# System.Enum.GetValues(typeof(Netpay.Bll.Wires.Wire.WireStatus)) %>' />
    </div>
    <hr />
    <div class="form-group">
        <h4>
            <asp:Label ID="lblAppronalStatus" runat="server" Text="Approval Status" /></h4>
        <asp:CheckBoxList runat="server" ID="chklApprovalStatus" CssClass="checkbox-list" RepeatLayout="UnorderedList" DataSource='<%# System.Enum.GetValues(typeof(Netpay.Bll.Wires.Wire.WireApprovalStatus)) %>' />
    </div>
    <div class="form-group">
        <asp:Label ID="lblProvider" runat="server" Text="Provider" />
        <netpay:DropDownBase CssClass="form-control" runat="server" ID="ddlProvider" DataSource='<%# Netpay.Bll.Wires.Provider.Cache %>' DataTextField="FriendlyName" DataValueField="Name" EnableBlankSelection="true" />
    </div>
    <div class="form-group">
        <asp:Label ID="lblPaymentOrderID" runat="server" Text="Payment Request ID: " />
        <asp:TextBox runat="server" ID="txtPaymentOrderID" CssClass="form-control" />
    </div>
    <div class="form-group">
        <asp:Label ID="lblMerchantSettlementID" runat="server" Text="Merchant Settlement ID: " />
        <asp:TextBox runat="server" ID="txtMerchantSettlementID" CssClass="form-control" />
    </div>
    <div class="form-group">
        <asp:Label ID="lblAffiliateSettlementID" runat="server" Text="Affiliate Settlement ID: " />
        <asp:TextBox runat="server" ID="txtAffiliateSettlementID" CssClass="form-control" />
    </div>
    <div class="form-group">
        <asp:Label ID="lblCurrency" runat="server" Text="Currency & Amount" />
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
            <netpay:CurrencyDropDown CssClass="form-control" runat="server" ID="ddlCurrency" />
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                From Amount
                <JQ:DecimalRange runat="server" ID="rngAmount" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblInsertDate" runat="server" Text="Date Range" />
                <JQ:DateRange runat="server" ID="rngInsertDate" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblID" runat="server" Text="ID Range" />
                <JQ:IntRange runat="server" ID="rngID" />
            </div>
        </div>
    </div>
</admin:FilterSection>
