﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Wire
{
    public partial class Filter : Controls.TemplateControlBase
    {
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
                            
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
        {
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var queryString = TemplatePage.Request.QueryString.ToString();
            var sf = new Netpay.Bll.Wires.Wire.SearchFilters();

            if (queryString.Contains("ApprovedAndWaitingForTransfer") && Request.Params["__EVENTTARGET"] == null)
            {
                sf.IsShow = true;
                sf.Status = new List<Bll.Wires.Wire.WireStatus>() { Bll.Wires.Wire.WireStatus.Pending };
                sf.ApprovalStatus = new List<Bll.Wires.Wire.WireApprovalStatus>() { Bll.Wires.Wire.WireApprovalStatus.Approved };

                TemplatePage.SetFilter(sf);

                //Set the ViewMode.
                (Page as Controls.DataTablePage).PageController.ForceModeUpdate = Admin.Controls.DataPageController.ViewMode.List;
            }
            else
            {
                if (apAccount.Value.ToNullableInt() != null)
                    sf.AccountIDs = new List<int> { apAccount.Value.GetValueOrDefault() };

                sf.ApprovalStatus = new List<Netpay.Bll.Wires.Wire.WireApprovalStatus>();
                foreach (ListItem i in chklApprovalStatus.Items)
                    if (i.Selected) sf.ApprovalStatus.Add(i.Value.ToNullableEnumByName<Netpay.Bll.Wires.Wire.WireApprovalStatus>().GetValueOrDefault());

                sf.Status = new List<Netpay.Bll.Wires.Wire.WireStatus>();
                foreach (ListItem i in chklStatus.Items)
                    if (i.Selected) sf.Status.Add(i.Value.ToNullableEnumByName<Netpay.Bll.Wires.Wire.WireStatus>().GetValueOrDefault());

                sf.Provider = ddlProvider.Value.NullIfEmpty();
                sf.MerchantSettlementID = txtMerchantSettlementID.Text.ToNullableInt();
                sf.AffiliateSettlementID = txtAffiliateSettlementID.Text.ToNullableInt();
                sf.PayeeID = txtPaymentOrderID.Text.ToNullableInt();

                sf.Currency = ddlCurrency.Value.ToNullableEnumByValue<Netpay.CommonTypes.Currency>();
                sf.ID = rngID.Value;
                sf.InsertDate = rngInsertDate.Value;
                sf.Amount = rngAmount.Value;
                TemplatePage.SetFilter(sf);
            }
        }
    }
}