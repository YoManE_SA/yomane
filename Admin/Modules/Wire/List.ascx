﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Wire.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Wires">
    <Header>
        <admin:LegendColors runat="server" Items='<%# Netpay.Bll.Wires.Wire.WireStatusColor %>' FloatRight="true" />
    </Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" ID="rptList" SaveAjaxState="true" DataKeyNames="ID" AutoGenerateColumns="false" BubbleLoadEvent="true">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <span style="background-color: <%# Netpay.Bll.Wires.Wire.WireStatusColor.MapHtmlColor((Netpay.Bll.Wires.Wire.WireStatus)Eval("Status")) %>;" class="legend-item"></span>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Wire Num. Create Date --%>
                    <asp:TemplateField HeaderText="Wire Num.<br/>Create Date">
                        <ItemTemplate>
                            <%# Eval("ID") %><br />
                            <%# Eval("Date", "{0:d}") %>
                            <asp:HiddenField runat="server" ID="hfID" Value='<%# Eval("ID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%--Due Date - Last Action  --%>
                    <asp:TemplateField HeaderText="Due Date<br />Last Action">
                        <ItemTemplate>
                            <%# Eval("InsertDate", "{0:d}") %><br />
                            <%# Eval("LastLogDate", "{0:d}") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Wire Amount --%>
                    <asp:TemplateField HeaderText="Wire Amount">
                        <ItemTemplate>
                            <%# (new Money((decimal)Eval("Amount"), (int)Eval("Currency"))).ToIsoString() %><br />
                            <%# (new Money((decimal)Eval("TransferAmount"), (int)Eval("ProcessingCurrency"))).ToIsoString() %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Fee Taken - Fee Expected --%>
                    <asp:TemplateField HeaderText="Fee Taken<br />Fee Expected">
                        <ItemTemplate>
                            <%# (new Money((decimal)Eval("Fee"), (int)Eval("Currency"))).ToIsoString() %><br />
                            --
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Account Name - Payee Name --%>
                    <asp:TemplateField HeaderText="Account Name <br />Payee Name">
                        <ItemTemplate>
                            <%# GetAccountName((int)Eval("AccountID")) %><br />
                            <%# Eval("PayeeName") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Merchant Balance --%>
                    <asp:TemplateField HeaderText="Merchant<br />Balance">
                        <ItemTemplate>
                            <%# GetBalances(Container as GridViewRow) %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Currency to use --%>
                    <asp:TemplateField HeaderText="Currency<br />To Use">
                        <ItemTemplate>
                            <netpay:CurrencyDropDown CssClass="form-control" runat="server" ID="ddlProcessingCurrency" Value='<%# (int) Eval("ProcessingCurrency") %>' EnableBlankSelection="false" AutoPostBack="true" OnSelectedIndexChanged="ProcessingCurrency_SelectedIndexChanged" Enabled='<%# Eval("CanUpdateProcessingCurrency") %>' onclick="event.cancelBubble=true;" />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Status --%>
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <span class="smallCircle" style="background-color: <%# Netpay.Bll.Wires.Wire.WireApprovalStatusColor.MapHtmlColor((Netpay.Bll.Wires.Wire.WireApprovalStatus)Eval("ApprovalStatus")) %>;"></span><%# Eval("ApprovalStatus") %><br />
                            <%# Eval("Status") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Process --%>
                    <asp:TemplateField HeaderText="Process">
                        <ItemTemplate>
                            <netpay:DropDownBase runat="server" ID="ddlProvider" CssClass="form-control" Value='<%# Eval("CommitProvider") %>' DataSource='<%# Netpay.Bll.Wires.Provider.Cache %>' DataTextField="FriendlyName" DataValueField="Name" Enabled='<%# Eval("CanSetProvider") %>' onclick="event.cancelBubble=true;" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd="false" />
    </Footer>
</admin:ListSection>
<asp:PlaceHolder runat="server" ID="phProcess">
    <div class="panel panel-default">
        <div class="panel-heading">
            Off line generated wire files
        </div>

        <div class="panel-body">
            <div class="row">
                <NP:PagedRepeater runat="server" ID="rptWireProviders">
                    <ItemTemplate>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Provider
                                   <%# Eval("FriendlyName") %>
                                    <asp:HiddenField runat="server" ID="hfID" Value='<%# Eval("Name") %>' />
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <i class="fa fa-university fa-fw"></i>Account
                                            <netpay:DropDownBase runat="server" CssClass="form-control" ID="ddlSourceAccount" DataSource='<%# (Container.DataItem as Netpay.Bll.Wires.Provider).GetAccounts() %>' DataTextField="Value" DataValueField="Key" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <i class="fa fa-calendar fa-fw"></i>Wire Date
                                            <JQ:DatePicker runat="server" CssClass="form-control" ID="dtWireDate" />
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <asp:Button CssClass="btn btn-primary pull-right" ID="btnProcessSpecific" runat="server" Text='<%# "Process " + Eval("ShortName").ToString() %>' CommandName="Specific" OnCommand="Process_Command" />
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <asp:Repeater runat="server" ID="rptRelatedFiles" DataSource='<%# Eval("LastRelatedBatchFiles") %>'>
                                                <ItemTemplate>
                                                    <asp:PlaceHolder runat="server" Visible='<%# Container.ItemIndex == 0 %>'>Files: <%# System.IO.File.GetLastWriteTimeUtc((string)Container.DataItem) %><br />
                                                    </asp:PlaceHolder>
                                                    <NP:LinkButton runat="server" RegisterAsync="false" OnCommand="DownloadFile_Click" CommandName='<%# DataBinder.Eval((Container.Parent.Parent as RepeaterItem).DataItem, "Name") %>' CommandArgument='<%# Container.ItemIndex %>'>
                                                        <%# System.IO.Path.GetFileName((string)Container.DataItem) %>
                                                    </NP:LinkButton>
                                                </ItemTemplate>
                                                <SeparatorTemplate>, </SeparatorTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                    <EmptyContainer>
                        <div class="col-md-6">
                            <div class="alert alert-info">
                                No wire providers found, try to activate wire providers modules.
                            </div>
                        </div>
                    </EmptyContainer>
                </NP:PagedRepeater>
            </div>
        </div>

        <div class="panel-footer">
            <asp:Button Enabled='<%# IsWireCreatable %>' runat="server" ID="btnProcessAll" CssClass="btn btn-primary pull-right" Text="Process" CommandName="All" OnCommand="Process_Command" />
            <div class="clearfix"></div>
        </div>
    </div>
</asp:PlaceHolder>
