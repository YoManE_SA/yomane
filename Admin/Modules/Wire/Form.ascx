﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Form.ascx.cs" Inherits="Netpay.Admin.Wire.Form" %>
<admin:FormSections runat="server" Title="Unsettled transactions">
    <div class="col-md-12">
        <asp:MultiView runat="server" ActiveViewIndex='<%# rptUnsettled.Items.Count > 0 ? 0 : 1 %>'>
            <asp:View runat="server">
                <asp:Repeater runat="server" ID="rptUnsettled">
                    <HeaderTemplate>
                        <div class="row inner-table-titles">
                            <div class="col-md-4">Currency</div>
                            <div class="col-md-4">Count</div>
                            <div class="col-md-4">Amount</div>
                        </div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="row table-ui">
                            <div class="col-md-4"><span style='<%#(Netpay.CommonTypes.Currency)Eval("Key") == WireData.Currency ? "font-weight:bold; color:black;": "" %>'><%# Currency.Get((int)Eval("Key")).IsoCode %></span></div>
                            <div class="col-md-4"><%# Eval("Value.Count") %></div>
                            <div class="col-md-4"><%# new Money((decimal)Eval("Value.Amount"), (int) Eval("Key")).ToIsoString() %></div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </asp:View>
            <asp:View runat="server">
                <div class="alert alert-info"><strong>Info!</strong> No record information</div>
            </asp:View>
        </asp:MultiView>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Bank Account Details">
    <asp:MultiView runat="server" ActiveViewIndex='<%# IsCurrencyHasBankAccount? 0 : 1 %>'>
        <asp:View runat="server">
            <asp:MultiView runat="server" ActiveViewIndex='<%# PayeeBankAccount != null ? 0 : 1  %>'>
                <%-- Payee bank account details --%>
                <asp:View runat="server">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                ID
			                    <asp:TextBox ReadOnly="true" runat="server" Text='<%# PayeeBankAccount != null ? PayeeBankAccount.ID.ToString() : "" %>' />
                            </div>
                            <div class="form-group">
                                Account Name
			                    <asp:TextBox ReadOnly="true" CssClass="form-control" runat="server" Text='<%# PayeeBankAccount != null ? PayeeBankAccount.AccountName : "" %>' />
                            </div>
                            <div class="form-group">
                                Account Number
			                    <asp:TextBox ReadOnly="true" CssClass="form-control" runat="server" Text='<%# PayeeBankAccount != null ? PayeeBankAccount.AccountNumber  : ""%>' />
                            </div>
                            <div class="form-group">
                                ABA
			                    <asp:TextBox ReadOnly="true" CssClass="form-control" runat="server" Text='<%# PayeeBankAccount != null ? PayeeBankAccount.ABA : "" %>' />
                            </div>
                            <div class="form-group">
                                IBAN
                                <asp:TextBox ReadOnly="true" CssClass="form-control" runat="server" Text='<%# PayeeBankAccount != null ? PayeeBankAccount.IBAN : "" %>' />
                            </div>
                            <div class="form-group">
                                Swift Number
					            <asp:TextBox ReadOnly="true" CssClass="form-control" runat="server" Text='<%# PayeeBankAccount != null ? PayeeBankAccount.SwiftNumber : "" %>' />
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                Bank Name
					        <asp:TextBox ReadOnly="true" CssClass="form-control" runat="server" Text='<%# PayeeBankAccount != null ? PayeeBankAccount.BankName : "" %>' />
                            </div>
                            <div class="form-group">
                                Bank Address Line 1
					        <asp:TextBox ReadOnly="true" CssClass="form-control" runat="server" Text='<%# PayeeBankAccount != null ? PayeeBankAccount.BankStreet1 : "" %>' />
                            </div>
                            <div class="form-group">
                                Bank Address Line 2
					            <asp:TextBox ReadOnly="true" CssClass="form-control" runat="server" Text='<%# PayeeBankAccount != null ? PayeeBankAccount.BankStreet2 : "" %>' />
                            </div>
                            <div class="form-group">
                                Bank Address City
					            <asp:TextBox ReadOnly="true" CssClass="form-control" runat="server" Text='<%# PayeeBankAccount != null ? PayeeBankAccount.BankCity : "" %>' />
                            </div>
                            <div class="form-group">
                                Bank Address Country
					            <asp:TextBox ReadOnly="true" CssClass="form-control" runat="server"  Text='<%# PayeeBankAccount != null ? PayeeBankAccount.BankCountryISOCode : "" %>' />
                            </div>
                            <div class="form-group">
                                Currency Iso
                                <asp:TextBox ReadOnly="true" CssClass="form-control" runat="server" Text='<%# PayeeBankAccount != null ? PayeeBankAccount.CurrencyISOCode : "" %>'></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </asp:View>

                <%-- Merchant bank account details --%>
                <asp:View runat="server">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                ID
			                    <asp:TextBox ReadOnly="true" CssClass="form-control" runat="server" Text='<%# MerchantBankAccount != null ? MerchantBankAccount.ID.ToString() : "" %>' />
                            </div>
                            <div class="form-group">
                                Account Name
			                    <asp:TextBox ReadOnly="true" CssClass="form-control" runat="server" Text='<%# MerchantBankAccount != null ?  MerchantBankAccount.AccountName : "" %>' />
                            </div>
                            <div class="form-group">
                                Account Number
			                    <asp:TextBox ReadOnly="true" CssClass="form-control" runat="server" Text='<%# MerchantBankAccount != null ? MerchantBankAccount.AccountNumber : "" %>' />
                            </div>
                            <div class="form-group">
                                ABA
			                    <asp:TextBox ReadOnly="true" CssClass="form-control" runat="server" Text='<%# MerchantBankAccount != null ? MerchantBankAccount.ABA : "" %>' />
                            </div>
                            <div class="form-group">
                                IBAN
                                <asp:TextBox ReadOnly="true" CssClass="form-control" runat="server" Text='<%# MerchantBankAccount != null ? MerchantBankAccount.IBAN : "" %>' />
                            </div>
                            <div class="form-group">
                                Swift Number
					            <asp:TextBox ReadOnly="true" CssClass="form-control" runat="server" Text='<%# MerchantBankAccount != null ? MerchantBankAccount.SwiftNumber : "" %>' />
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                Bank Name
					        <asp:TextBox ReadOnly="true" CssClass="form-control" runat="server" Text='<%# MerchantBankAccount != null ? MerchantBankAccount.BankName : "" %>' />
                            </div>
                            <div class="form-group">
                                Bank Address Line 1
					        <asp:TextBox ReadOnly="true" CssClass="form-control" runat="server" Text='<%# MerchantBankAccount != null ? MerchantBankAccount.BankStreet1 : "" %>' />
                            </div>
                            <div class="form-group">
                                Bank Address Line 2
					        <asp:TextBox ReadOnly="true" CssClass="form-control" runat="server" Text='<%# MerchantBankAccount != null ? MerchantBankAccount.BankStreet2 : "" %>' />
                            </div>
                            <div class="form-group">
                                Bank Address City
					            <asp:TextBox ReadOnly="true" CssClass="form-control" runat="server" Text='<%# MerchantBankAccount != null ? MerchantBankAccount.BankCity : "" %>' />
                            </div>
                            <div class="form-group">
                                Bank Address Country
					            <asp:TextBox ReadOnly="true" CssClass="form-control" runat="server"  Text='<%# MerchantBankAccount != null ? MerchantBankAccount.BankCountryISOCode : "" %>' />
                            </div>
                            <div class="form-group">
                                Currency Iso
                                <asp:TextBox ReadOnly="true" CssClass="form-control" runat="server" Text='<%# MerchantBankAccount != null ? MerchantBankAccount.CurrencyISOCode : "" %>'></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </asp:View>
        <asp:View runat="server">
            <div class="alert alert-danger">
                The merchant <%# Netpay.Bll.Accounts.Account.GetName(WireData.AccountID) %> or any of his payees has no bank account that recieves Currency: <%# WireData.Currency.ToString() %>
            </div>
        </asp:View>
    </asp:MultiView>
</admin:FormSections>
<admin:FormSections runat="server" Title="Wire Approval">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                        <%# Domain.Current.FinanceLevel1Group %>
                        <netpay:BoolDropDown runat="server" ID="ddlApproval1" Enabled="<%# IsApproval1Enabled %>" CssClass="form-control" TrueText="Approve" FalseText="Reject" EnableBlankSelection="true" BoolValue='<%# WireData.ApprovalLevel1 %>' />
                        <br />
                        <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary" CommandArgument="1" Enabled="<%# IsApproval1Enabled %>" Text="Approve" CommandName="Approve" OnCommand="Wire_Command" />
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                        <%# Domain.Current.FinanceLevel2Group %>
                        <netpay:BoolDropDown runat="server" ID="ddlApproval2" Enabled="<%# IsApproval2Enabled %>" CssClass="form-control" TrueText="Approve" FalseText="Reject" EnableBlankSelection="true" BoolValue='<%# WireData.ApprovalLevel2 %>' />
                        <br />
                        <asp:Button ID="Button2" runat="server" CssClass="btn btn-primary" CommandArgument="2" Enabled="<%# IsApproval2Enabled %>" Text="Approve" CommandName="Approve" OnCommand="Wire_Command" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Comment">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                Confirmation Number
        <asp:TextBox CssClass="form-control" ID="txtConfirmation" runat="server" Text='<%# WireData.ConfirmationNumber %>' />
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                Comment
        <asp:TextBox CssClass="form-control" TextMode="MultiLine" ID="txtComment" runat="server" />
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                Browse
        <span class="btn btn-default btn-file">
            <NP:FileUpload ID="ufFile1" runat="server" />
        </span>
            </div>
        </div>
        <div class="col-md-12">
            <asp:Button ID="btmSave" class="btn btn-primary pull-right" runat="server" OnCommand="Wire_Command" CommandName="Log" Text="Update" />
            <div class="clearfix"></div>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Action Log & Files">
    <div class="col-md-12">
        <asp:MultiView runat="server" ActiveViewIndex='<%# rptLog.Items.Count > 0 ? 0 : 1 %>'>
            <asp:View runat="server">
                <asp:Repeater runat="server" ID="rptLog">
                    <HeaderTemplate>
                        <div class="row inner-table-titles">
                            <div class="col-md-3">Date & Time</div>
                            <div class="col-md-3">User</div>
                            <div class="col-md-3">Description</div>
                            <div class="col-md-3">File</div>
                        </div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="row table-ui">
                            <div class="col-md-3"><%# Eval("InsertDate") %></div>
                            <div class="col-md-3"><%# Eval("UserName") %></div>
                            <div class="col-md-3"><%# Eval("Description") %></div>
                            <div class="col-md-3">
                                <NP:LinkButton RegisterAsync="false" runat="server" OnCommand="Wire_Command" CommandName="GetFile" CommandArgument='<%# Eval("PhysicalFileName") %>' Text='<%# Eval("FileName") %>' />
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </asp:View>
            <asp:View runat="server">
                <div class="alert alert-info"><strong>Info!</strong> No record information</div>
            </asp:View>
        </asp:MultiView>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Actions">
    <asp:LinkButton runat="server" ID="btnReset" Text="RESET" CssClass="btn btn-default" CommandName="Reset" OnCommand="Wire_Command" Enabled='<%# WireData.IsResetable && IsWireEditable %>' />
    <asp:LinkButton runat="server" ID="btnCancel" CssClass="btn btn-primary" Text="CANCEL" CommandName="Cancel" OnCommand="Wire_Command" Enabled='<%# WireData.IsCalcelable && IsWireEditable %>' />
</admin:FormSections>
