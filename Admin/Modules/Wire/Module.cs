﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;
using Netpay.Web;

namespace Netpay.Admin.Wire
{
	public class Module : Admin.CoreBasedModule
	{
		private ApplicationMenu menuWire, menuWireBatch;
		public override string Name { get { return "Wire"; } }
		public override decimal Version { get { return 1.0m; } }
		public override string Author { get { return "OBL ltd."; } }
		public override string Description { get { return "Wires and WiresBatch"; } }

        public Module() : base(Bll.Wires.Module.Current) { }

		protected override void OnInit(EventArgs e)
		{
            //Create menu items 
            menuWire = new ApplicationMenu("Wires", "~/Wires/0", "fa-money", 90);
            menuWireBatch = new ApplicationMenu("Wire Batch", "~/Wires/Batches/0", null, 100);

            //Register routes
            Application.RegisterRoute("Wires/Batches/{*id}", "WireBatch", typeof(Controls.DataTablePage), module : this);
			Application.RegisterRoute("Wires/{*id}", "Wire", typeof(Controls.DataTablePage), module: this);

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
		}

		protected override void OnActivate(EventArgs e)
		{			
            base.OnActivate(e);
		}

		protected override void OnDeactivate(EventArgs e)
		{				
			base.OnDeactivate(e);
		}

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Finance", menuWire);
            Application.AddMenuItem("Finance", menuWireBatch);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuWire.Parent.RemoveChild(menuWire);
            menuWireBatch.Parent.RemoveChild(menuWireBatch);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
		{
            if (!CoreModule.IsInstalled)
                return;

			var page = sender as Controls.DataTablePage;
			if (page.TemplateName == "Wire")
			{
				page.PageController.LoadItem += PageController_LoadItem;
                page.AddControlToList(page.LoadControl("~/Modules/Wire/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Wire/Filter.ascx"));
                page.AddControlToForm("Data", page.LoadControl("~/Modules/Wire/Form.ascx"), this, "", Bll.Wires.Wire.SecuredObject);
			} 
			else if (page.TemplateName == "WireBatch")
			{
                page.AddControlToList(page.LoadControl("~/Modules/Wire/WiresBatch/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Wire/WiresBatch/Filter.ascx"));
                page.AddControlToForm("Data", page.LoadControl("~/Modules/Wire/WiresBatch/Details.ascx"),this ,"" , Bll.Wires.WiresBatch.SecuredObject);
			}
		}

		void PageController_LoadItem(object sender, EventArgs e)
		{
			var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
			page.ItemID = page.PageController.DataKey["ID"].ToNullableInt();
			if (page.ItemID != null) page.SetItemData(Netpay.Bll.Wires.Wire.Load(page.ItemID.GetValueOrDefault()));
			if (page.GetItemData<Netpay.Bll.Wires.Wire>() == null) page.SetItemData(new Netpay.Bll.Wires.Wire(0, null));
		}
	}
}