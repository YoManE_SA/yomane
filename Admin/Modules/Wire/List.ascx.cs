﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Wire
{
    public partial class List : Controls.TemplateControlBase
    {
        public bool IsWireCreatable { get; set; }
        public bool IsWireEditable { get; set; }

        public Bll.Wires.Wire.SearchFilters Filters { get
            {
                if (TemplatePage.GetFilter<Bll.Wires.Wire.SearchFilters>() != null)
                    return TemplatePage.GetFilter<Bll.Wires.Wire.SearchFilters>();
                else return new Bll.Wires.Wire.SearchFilters();
            }
            set { Filters = value; } }
        private Dictionary<int, string> AccountNames { get; set; }
        private Dictionary<int, List<Bll.Accounts.Balance.BalanceStatus>> AccountBalaces { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            rptList.DataBinding += List_DataBinding;
            IsWireCreatable = Bll.Wires.Wire.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Add);
            IsWireEditable = Bll.Wires.Wire.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Edit);
            base.OnLoad(e);
        }

        private void List_DataBinding(object sender, EventArgs e)
        {
            var items = Netpay.Bll.Wires.Wire.Search(Filters, rptList);
            rptList.DataSource = items;
            AccountNames = Bll.Accounts.Account.GetNames(items.Select(v => v.AccountID).ToList());
            AccountBalaces = Bll.Accounts.Balance.GetStatus(items.Select(v => v.AccountID).ToList(), null);
            rptWireProviders.DataSource = Netpay.Bll.Wires.Provider.Cache.Where(p => !p.IsOnline).ToList();
        }

        //Make the <br> tag in the Template Fields - Header text affective.
        protected override void OnPreRender(EventArgs e)
        {
            if(rptList!=null && rptList.HeaderRow!=null)            
            foreach (TableCell cell in rptList.HeaderRow.Cells)
            {
                cell.Text = HttpUtility.HtmlDecode(cell.Text);
            }
        }

        public string GetBalances(GridViewRow rpt)
        {
            var wire = rpt.DataItem as Netpay.Bll.Wires.Wire;
            List<Bll.Accounts.Balance.BalanceStatus> res;
            if (AccountBalaces == null) res = Bll.Accounts.Balance.GetStatus(wire.AccountID);
            else if (!AccountBalaces.TryGetValue(wire.AccountID, out res)) return null;
            var wc = res.Where(v => v.CurrencyIso == Bll.Currency.Get(wire.Currency).IsoCode).Select(v => v.Current).SingleOrDefault();
            var pc = res.Where(v => v.CurrencyIso == Bll.Currency.Get(wire.ProcessingCurrency).IsoCode).Select(v => v.Current).SingleOrDefault();
            //if (res == null) return new Netpay.Bll.Money(0, wire.Currency);
            return string.Format("{0}<br />{1}", new Netpay.Bll.Money(wc, wire.Currency).ToIsoString(), new Netpay.Bll.Money(pc, wire.ProcessingCurrency).ToIsoString());
        }

        public string GetAccountName(int accountId)
        {
            string ret;
            if (AccountNames.TryGetValue(accountId, out ret)) return ret;
            return null;
        }

        protected void ProcessingCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            var curDropDown = sender as Netpay.Web.Controls.CurrencyDropDown;
            var wireId = ((curDropDown.NamingContainer as GridViewRow).FindControl("hfID") as HiddenField).Value.ToNullableInt();
            var selectedRow = curDropDown.NamingContainer as GridViewRow;
            if (wireId == null) return;
            var wire = Bll.Wires.Wire.Load(wireId.Value);

            wire.ProcessingCurrency = (CommonTypes.Currency)curDropDown.Value.ToNullableInt().GetValueOrDefault();
            selectedRow.DataItem = wire;
            AccountNames = Bll.Accounts.Account.GetNames(new List<int> { wire.AccountID });

            (selectedRow.Page as Controls.DataTablePage).PageController.ListView.Update();
        }

        
        protected void Details_DetailsUpdated(object sender, EventArgs e)
        {
            var item = ((sender as Control).NamingContainer as RepeaterItem);
            var wireId = (item.FindControl("hfID") as HiddenField).Value.ToNullableInt();
            if (wireId == null) return;
            item.DataItem = Bll.Wires.Wire.Load(wireId.Value);
            item.DataBind();
        }

        public Dictionary<string, List<Netpay.Bll.Wires.Wire>> UpdatedGroupedItems(string providerName = null)
        {
            bool isCheckProviderName = providerName != null ? true : false;
            var groupedItems = new Dictionary<string, List<Netpay.Bll.Wires.Wire>>();
            var dataItems = Netpay.Bll.Wires.Wire.Search(Filters, rptList);
            foreach (GridViewRow item in rptList.Rows)
            {
                var itemId = (item.FindControl("hfID") as HiddenField).Value.ToNullableInt().GetValueOrDefault();
                var provider = (item.FindControl("ddlProvider") as Netpay.Web.Controls.DropDownBase).Value;
                if (!string.IsNullOrEmpty(provider) && (isCheckProviderName? provider == providerName : true))
                {
                    var dataItem = dataItems.Where(i => i.ID == itemId).SingleOrDefault();
                    dataItem.ProcessingCurrency = (Netpay.CommonTypes.Currency)(item.FindControl("ddlProcessingCurrency") as Netpay.Web.Controls.CurrencyDropDown).Value.ToNullableInt().GetValueOrDefault();
                    if (dataItem.Status != Bll.Wires.Wire.WireStatus.Pending) continue;
                    List<Netpay.Bll.Wires.Wire> groupList = null;
                    if (!groupedItems.TryGetValue(provider, out groupList))
                    {
                        groupList = new List<Netpay.Bll.Wires.Wire>();
                        groupedItems.Add(provider, groupList);
                    }
                    groupList.Add(dataItem);
                }
            }
            return groupedItems;
        }

        public void DownloadFile_Click(object sender, CommandEventArgs e)
        {
            var providerName = e.CommandName;
            var downloadIndex = e.CommandArgument.ToNullableInt().GetValueOrDefault();
            var files = Netpay.Bll.Wires.Provider.Get(providerName).LastRelatedBatchFiles;
            if (files == null || files.Count < downloadIndex) return;
            Netpay.Web.WebUtils.SendFile(files[downloadIndex], null, "attachment");
        }

        public void Process_Command(object sender, CommandEventArgs e)
        {
            bool isSpecificProcess = e.CommandName == "Specific";
            RepeaterItem currentItem = isSpecificProcess ? (sender as Button).Parent as RepeaterItem : null;

            var providerSettings = new Dictionary<string, KeyValuePair<string, DateTime>>();
            if (isSpecificProcess)
            {    
                providerSettings.Add((currentItem.FindControl("hfID") as HiddenField).Value,
                        new KeyValuePair<string, DateTime>((currentItem.FindControl("ddlSourceAccount") as Netpay.Web.Controls.DropDownBase).Value, (currentItem.FindControl("dtWireDate") as Controls.GlobalControls.JQueryControls.DatePicker).Value.GetValueOrDefault()));
            }
            else
            {
                foreach (RepeaterItem item in rptWireProviders.Items)
                {
                    providerSettings.Add((item.FindControl("hfID") as HiddenField).Value,
                        new KeyValuePair<string, DateTime>((item.FindControl("ddlSourceAccount") as Netpay.Web.Controls.DropDownBase).Value, (item.FindControl("dtWireDate") as Controls.GlobalControls.JQueryControls.DatePicker).Value.GetValueOrDefault()));
                }
            }

            var groupedItems = isSpecificProcess? UpdatedGroupedItems((currentItem.FindControl("hfID") as HiddenField).Value) : UpdatedGroupedItems();

            try
            {
                foreach (var p in groupedItems)
                {
                    KeyValuePair<string, DateTime> providerSet;
                    if (!providerSettings.TryGetValue(p.Key, out providerSet))
                        providerSet = new KeyValuePair<string, DateTime>(null, DateTime.Now);
                    Netpay.Bll.Wires.Wire.Commit(p.Value, providerSet.Value, p.Key, providerSet.Key);
                }
                TemplatePage.PageController.ListView.BindAndUpdate();
            }
            catch (Exception ex)
            {
                TemplatePage.SetListViewMessage(ex.Message, true);
            }
        }
    }
}