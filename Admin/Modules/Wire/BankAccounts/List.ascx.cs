﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Admin.Modules.Utils;

namespace Netpay.Admin.BankAccounts
{
    public partial class List : Controls.TemplateControlBase
    {
        public void List_DataBinding(object sender, EventArgs e)
        {
            var list = Bll.Wires.WireBankAccount.Search(TemplatePage.GetFilter<Bll.Wires.WireBankAccount.SearchFilters>(), rptList);
            rptList.DataSource = list;
        }
    }
}