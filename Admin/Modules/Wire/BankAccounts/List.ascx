﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.BankAccounts.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Bank Accounts">
    <Header>
    </Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList
                runat="server"
                ID="rptList"
                SaveAjaxState="true"
                DataKeyNames="ID"
                OnDataBinding="List_DataBinding"
                AutoGenerateColumns="false"
                BubbleLoadEvent="true"
                DisableRowSelect="false">
                <Columns>
                    <%--The ID coloum--%>
                    <asp:TemplateField HeaderText="ID" SortExpression="WireAccount_id">
                        <ItemTemplate>
                            <%# Eval("ID") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%--The Branch Number coloum--%>
                    <asp:TemplateField HeaderText="Branch Number" SortExpression="AccountBranch">
                        <ItemTemplate>
                            <%# Eval("AccountBranch") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%--The Account Name coloum--%>
                    <asp:TemplateField HeaderText="Account Name" SortExpression="AccountName">
                        <ItemTemplate>
                            <%# Eval("AccountName") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%--The Account Number coloum--%>
                    <asp:TemplateField HeaderText="Account Number" SortExpression="AccountNumber">
                        <ItemTemplate>
                            <%# Eval("AccountNumber") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%--The CurrencyISOCode coloum--%>
                    <asp:TemplateField HeaderText="Currency" SortExpression="CurrencyISOCode">
                        <ItemTemplate>
                            <%# Eval("CurrencyISOCode") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd="true" />
    </Footer>
</admin:ListSection>
