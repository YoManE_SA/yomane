﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.BankAccounts.Filter" %>
<admin:FilterSection runat="server" Title="Provider ID">
     <div class="form-group">
        <asp:Label ID="lblProvider" runat="server" Text="Provider" />
        <netpay:DropDownBase CssClass="form-control" runat="server" ID="ProviderIdFilter" DataSource='<%# Netpay.Bll.Wires.Provider.Cache %>' DataTextField="FriendlyName" DataValueField="Name" EnableBlankSelection="true" />
    </div>
</admin:FilterSection>