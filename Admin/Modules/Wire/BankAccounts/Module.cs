﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;
using Netpay.Web;
using Netpay.Infrastructure.Security;

namespace Netpay.Admin.BankAccounts
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuItem;
        public override string Name { get { return "BankAccount"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return "Wires bank accounts."; } }

        public Module() : base(Bll.Wires.WiresBankAccounts.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu
            menuItem = new ApplicationMenu("Bank Accounts", "~/Finance/BankAccounts", null, 130);

            //Register route
            Application.RegisterRoute("Finance/BankAccounts", "BankAccounts", typeof(Controls.DataTablePage), module:this);

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {            
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {            
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Finance", menuItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuItem.Parent.RemoveChild(menuItem);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "BankAccounts")
            {
                page.PageController.LoadItem += PageController_LoadItem;
                page.PageController.NewItem += PageController_BankAccounts_NewItem;
                page.PageController.PostSaveItem += PageController_BankAccounts_PostSaveItem;
                page.AddControlToList(page.LoadControl("~/Modules/Wire/BankAccounts/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Wire/BankAccounts/Filter.ascx"));
                page.AddControlToForm("Data", page.LoadControl("~/Modules/Wire/BankAccounts/Data.ascx"), this, "", Bll.Wires.WireBankAccount.SecuredObject);
            }
        }

        private void PageController_LoadItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            page.ItemID = page.PageController.DataKey["ID"].ToNullableInt();
            if (page.ItemID != null)
            {
                page.SetItemData(Bll.Wires.WireBankAccount.Load(page.ItemID.GetValueOrDefault()));
            }
            if (page.GetItemData<Bll.Wires.WireBankAccount>() == null) page.SetItemData(new Bll.Wires.WireBankAccount());
        }

        private void PageController_BankAccounts_PostSaveItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            var item = page.GetItemData<Bll.Wires.WireBankAccount>();
            if (item == null) return;

            page.PageController.DataKey = new System.Web.UI.WebControls.DataKey(new System.Collections.Specialized.OrderedDictionary() { { "ID", item.ID } });
        }

        void PageController_BankAccounts_NewItem(object sender, EventArgs e)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Wires.WireBankAccount.SecuredObject, PermissionValue.Add);

            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            page.SetItemData(new Bll.Wires.WireBankAccount());
            page.FormButtons.EnableDelete = false;
        }
    }
}