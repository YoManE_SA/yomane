﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Data.ascx.cs" Inherits="Netpay.Admin.BankAccounts.Data" %>
<admin:FormSections runat="server" Title="Add Bank Account">

    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                Provider 
                <netpay:DropDownBase Value='<%# Item != null ? Item.ProviderID : "" %>' CssClass="form-control" runat="server" ID="ddlProvider" DataSource='<%# Netpay.Bll.Wires.Provider.Cache %>' DataTextField="FriendlyName" DataValueField="Name" EnableBlankSelection="true" />
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                Branch Number:
                <asp:TextBox Text='<%# Item != null ? Item.AccountBranch : "" %>' ID="BranchNumberInput" runat="server" CssClass="form-control" />
            </div>
        </div>
          <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Account Name:
                <asp:TextBox Text='<%# Item != null? Item.AccountName : "" %>' ID="AccountNameInput" runat="server" CssClass="form-control" />
            </div>
        </div>
          <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Account Number:
                <asp:TextBox Text='<%# Item != null ? Item.AccountNumber : "" %>' ID="AccountNumberInput" runat="server" CssClass="form-control" />
            </div>
        </div>
         <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Currency
                <netpay:CurrencyDropDown SelectedCurrencyIso='<%# Item != null? Item.CurrencyISOCode : "" %>' ID="ddlCurrency" CssClass="form-control" runat="server"/>
            </div>
        </div>
    </div>
</admin:FormSections>
