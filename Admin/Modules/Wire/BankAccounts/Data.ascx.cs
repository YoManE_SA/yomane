﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.BankAccounts
{
    public partial class Data : Controls.TemplateControlBase
    {
        public Bll.Wires.WireBankAccount Item { get ; set ; }

        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += Save_Click;
            TemplatePage.PageController.DeleteItem += PageController_DeleteItem;
            base.OnLoad(e);
        }

        private void PageController_DeleteItem(object sender, EventArgs e)
        {
            Item = TemplatePage.GetItemData<Bll.Wires.WireBankAccount>();
            if (Item != null && Item.ID != 0) Item.Delete();
        }

        protected override void DataBindChildren()
        {
            Item = TemplatePage.GetItemData<Bll.Wires.WireBankAccount>();
            if (Item == null) return;

            base.DataBindChildren();
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            Item = TemplatePage.GetItemData<Bll.Wires.WireBankAccount>();
            if (Item == null) return;

            //Provider ID field
            Item.ProviderID = ddlProvider.Value.NullIfEmpty();

            //Branch Number Field
            if (BranchNumberInput.Text!= null && BranchNumberInput.Text!="")
            Item.AccountBranch=BranchNumberInput.Text;

            //Account Name Field
            if (AccountNameInput.Text!=null && AccountNameInput.Text!="")
            Item.AccountName = AccountNameInput.Text;

            //Account Number Field
            if (AccountNumberInput.Text != null && AccountNumberInput.Text!="")
            Item.AccountNumber = AccountNumberInput.Text;

            //ISO currency Field
            if (ddlCurrency.Value != null)
                Item.CurrencyISOCode = ddlCurrency.SelectedCurrencyIso;
                   
           
            Item.Save();

            ClearFormFields();
        }

        private void ClearFormFields()
        {
            ddlProvider.Value = null;

            BranchNumberInput.Text = null;

            AccountNameInput.Text = null;

            AccountNumberInput.Text = null;

            ddlCurrency.Value = null;
        }
    }
}