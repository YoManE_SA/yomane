﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Wire.WiresBatch
{
	public partial class Filter : Controls.TemplateControlBase
	{
		protected override void OnLoad(EventArgs e)
		{
			TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
            base.OnLoad(e);
		}

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
		{
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Netpay.Bll.Wires.WiresBatch.SearchFilters();
            sf.WireID = txtWireID.Text.ToNullableInt();
            sf.Currency = ddlCurrency.Value.ToNullableEnumByValue<Netpay.CommonTypes.Currency>();
            sf.Date = rngDate.Value;
            TemplatePage.SetFilter(sf);
        }
    }
}