﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Wire.WiresBatch.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Wires Batch">
    <Header>
        <admin:LegendColors runat="server" Items='<%# Netpay.Infrastructure.Enums.BoolColorMap %>' FloatRight="true" />
    </Header>
    <Body>
        <admin:AdminList runat="server" ID="rptList" SaveAjaxState="true" DataKeyNames="ID" AutoGenerateColumns="false" BubbleLoadEvent="true">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <span style="background-color: <%# Netpay.Infrastructure.Enums.BoolColorMap[(bool)Eval("Completed")].ToHtmlColor() %>;" class="legend-item"></span>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Date" SortExpression="Date">
                    <ItemTemplate>
                        <%# Eval("Date") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="UserName" HeaderText="UserName" SortExpression="UserName" />
                <asp:BoundField DataField="ProviderID" HeaderText="ProviderID" SortExpression="ProviderID" />
                <asp:BoundField DataField="SourceBankAccount" HeaderText="SourceBankAccount" SortExpression="SourceBankAccount" />
                <asp:BoundField DataField="Count" HeaderText="Count" SortExpression="Count" />
                <asp:BoundField DataField="Amount" HeaderText="Amount" SortExpression="Amount" />
                <asp:BoundField DataField="Currency" HeaderText="Currency" SortExpression="Currency" />
                <asp:TemplateField HeaderText="Flag">
                    <ItemTemplate>
                        <netpay:MultiStateButton runat="server" ID="msbFlag" Values='<%# Netpay.Infrastructure.Enums.TreeSateColor.ToList() %>' OnValueChanged="Flag_ValueChanged" AutoCallBack="true" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </admin:AdminList>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" />
    </Footer>

</admin:ListSection>
