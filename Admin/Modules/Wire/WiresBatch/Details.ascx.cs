﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Wire.WiresBatch
{
	public partial class Details : Controls.TemplateControlBase
	{
		public int BatchID { get { return TemplatePage.ItemID.GetValueOrDefault(); } }

		public override void DataBind()
		{
			rptList.DataSource = Bll.Wires.Wire.Search(new Bll.Wires.Wire.SearchFilters() { BatchID = BatchID }, null);
			base.DataBind();
		}
		
		protected void Save_Click(object sender, EventArgs e)
		{
			var wireBatch = Bll.Wires.WiresBatch.Load(BatchID);
			if(fuFile.HasFile) {
				wireBatch.AttachFile(txtComment.Text, fuFile.FileName, fuFile.FileContent);
			} else {
				wireBatch.AddComment(txtComment.Text);
			}
			txtComment.Text = string.Empty;
		}

		protected void List_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			var itemId = (e.Item.FindControl("hfID") as HiddenField).Value.ToNullableInt().GetValueOrDefault();
			var item = Bll.Wires.Wire.Load(itemId);
			switch (e.CommandName){
			case "Update":
				var comment = (e.Item.FindControl("txtComment") as TextBox).Text;
				var fuAttach = e.Item.FindControl("fuAttach") as FileUpload;
				if (fuAttach.HasFile) Bll.Wires.WireLog.AddFile(item.ID, comment, fuAttach.FileName, fuAttach.FileContent);
				if (comment != item.Comment) item.SetComment(comment);
				break;
			case "MarkAsDone":
				//item.MarkAsDone();	
				break;
			}
			e.Item.DataItem = item;
			e.Item.DataBind();
		}
	}
}