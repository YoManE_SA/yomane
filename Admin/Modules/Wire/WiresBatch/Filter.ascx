﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Wire.WiresBatch.Filter" %>
<admin:FilterSection runat="server" Title="Status">
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblInsertDate" runat="server" Text="Date Range" />
                <JQ:DateRange runat="server" ID="rngDate" />
            </div>
        </div>
    </div>
    <div class="form-group">
        <asp:Label ID="lblAcceptor" runat="server" Text="Acceptor" />
        <asp:TextBox runat="server" CssClass="form-control" ID="txtAcceptor" />
    </div>

    <div class="form-group">
        <asp:Label ID="lblCurrency" runat="server" Text="Currency: " />
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
            <netpay:CurrencyDropDown CssClass="form-control" runat="server" ID="ddlCurrency" />
        </div>
    </div>
    <div class="form-group">
        <asp:Label ID="lblWireID" runat="server" Text="Wire ID: " />
        <asp:TextBox runat="server" CssClass="form-control" ID="txtWireID" />
    </div>
</admin:FilterSection>
