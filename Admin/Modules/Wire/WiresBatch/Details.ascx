﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Details.ascx.cs" Inherits="Netpay.Admin.Wire.WiresBatch.Details" %>
<admin:FormSections runat="server" Title="Batch Items">
    <asp:Repeater runat="server" ID="rptList" OnItemCommand="List_ItemCommand">
        <ItemTemplate>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Wire ID / Src ID <%# Eval("ID") %><asp:HiddenField runat="server" ID="hfID" Value='<%# Eval("ID") %>' />
                    <%# Eval("PayeeName") %>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <ul class="list-group">
                            <li class="list-group-item"><b>Account Name / Destenation Account:</b> <%# Eval("AccountInfo") %></li>
                            <li class="list-group-item"><b>Amount:</b>  <%# new Money((decimal)Eval("Amount"), (int)Eval("Currency")).ToIsoString() %></li>
                        </ul>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <span class="btn btn-primary btn-file">Browse…
                                <asp:FileUpload runat="server" ID="fuAttach" Enabled='<%# !(bool)Eval("IsDone") %>' />
                                </span>
                            </span>
                            <input class="form-control" readonly="" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        Comment
                        <asp:TextBox runat="server" ID="txtComment" TextMode="MultiLine" CssClass="form-control" Text='<%# Eval("Comment") %>' Enabled='<%# !(bool)Eval("IsDone") %>' />
                    </div>
                </div>
                <div class="panel-footer">
                    <asp:Button runat="server" CssClass="btn btn-info" CommandName="Update" Text="Update" />
                    <asp:MultiView runat="server" ActiveViewIndex='<%# (bool)Eval("IsDone") ? 1 : 0 %>'>
                        <asp:View runat="server">
                            <asp:Button runat="server" CssClass="btn btn-default" ID="btnExecute" Text="Execute" CommandName="MarkAsDone" Enabled='<%# !(bool)Eval("IsDone") %>' />
                        </asp:View>
                        <asp:View runat="server">
                            Done By: <asp:Literal runat="server" Text='<%# Eval("CommitUser") %>' />
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>

        </ItemTemplate>
    </asp:Repeater>
</admin:FormSections>
<admin:FormSections runat="server" Title="Batch Commands">
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                Comment
                <asp:TextBox ID="txtComment" TextMode="MultiLine" CssClass="form-control" runat="server" />
            </div>
            <div class="form-group">
                Upload File
                <div class="input-group">
                    <span class="input-group-btn">
                        <span class="btn btn-primary btn-file">Browse…
                            <asp:FileUpload runat="server" ID="fuFile" />
                        </span>
                    </span>
                    <input class="form-control" readonly="" type="text">
                </div>
            </div>
            <div class="form-group">
                <asp:Button ID="btmSave" runat="server" CssClass="btn btn-primary" OnClick="Save_Click" Text="Update" />
            </div>
        </div>
    </div>
</admin:FormSections>
