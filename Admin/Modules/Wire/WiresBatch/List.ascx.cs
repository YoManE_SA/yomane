﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Wire.WiresBatch
{
	public partial class List : Controls.TemplateControlBase
	{		
		protected override void OnLoad(EventArgs e)
		{
			rptList.DataBinding += List_DataBinding;
			base.OnLoad(e);
		}

		private void List_DataBinding(object sender, EventArgs e)
		{
			rptList.DataSource = Netpay.Bll.Wires.WiresBatch.Search(TemplatePage.GetFilter<Bll.Wires.WiresBatch.SearchFilters>(), rptList);
		}

		protected void Details_DetailsUpdated(object sender, EventArgs e)
		{
			var item = ((sender as Control).NamingContainer as RepeaterItem);
			var itemId = (item.FindControl("hfID") as HiddenField).Value.ToNullableInt();
			if (itemId == null) return;
			item.DataItem = Bll.Wires.WiresBatch.Load(itemId.Value);
			item.DataBind();
		}
		
		protected void Flag_ValueChanged(object sender, EventArgs e)
		{
			var msbFlag = (sender as Web.Controls.MultiStateButton);
			var item = Bll.Wires.WiresBatch.Load((msbFlag.NamingContainer.FindControl("hfID") as HiddenField).Value.ToNullableInt().GetValueOrDefault());
			//item.SetFlag((byte)msbFlag.Value.ToNullableInt().GetValueOrDefault());
		}
	}
}