﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Wire
{
    public partial class Form : Controls.TemplateControlBase
    {
        protected bool IsApproval1Enabled { get; private set; }
        protected bool IsApproval2Enabled { get; private set; }

        protected Netpay.Bll.Wires.Wire WireData;

        protected Bll.Accounts.BankAccount PayeeBankAccount { get; set; }

        protected Bll.Accounts.BankAccount MerchantBankAccount { get; set; }

        protected CommonTypes.Currency currencyShouldBeSupported { get; set; }

        public bool IsCurrencyHasBankAccount { get; set; }

        public bool IsWireEditable { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            IsWireEditable = Bll.Wires.Wire.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Edit);
        }

        public override void DataBind()
        {
            WireData = TemplatePage.GetItemData<Netpay.Bll.Wires.Wire>();
            if (WireData == null) return;
            var account = WireData.Account;

            //Set default value.
            IsCurrencyHasBankAccount = false;

            //Declare the currency that the bank account should support
            currencyShouldBeSupported = WireData.Currency;

            //Money should go to Payee bank account
            if (WireData.PayeeID.HasValue)
            {
                var merchantPayee = Bll.Accounts.Payee.Load(WireData.PayeeID.Value);
                if (merchantPayee.BankAccount.CurrencyISOCode == Bll.Currency.GetIsoCode(currencyShouldBeSupported))
                {
                    IsCurrencyHasBankAccount = true;
                    PayeeBankAccount = merchantPayee.BankAccount;
                }
            }

            else //Money should go to the merchant bank account
            {
                MerchantBankAccount = Bll.Accounts.BankAccount.LoadForAccount(WireData.AccountID, currencyShouldBeSupported);
                if (MerchantBankAccount != null && MerchantBankAccount.CurrencyISOCode == Bll.Currency.GetIsoCode(currencyShouldBeSupported))
                {
                    IsCurrencyHasBankAccount = true;
                }
            }


            IsApproval1Enabled = Netpay.Infrastructure.Security.AdminUser.Current.IsInGroup(Domain.Current.FinanceLevel1Group) && WireData.CanApprove;
            IsApproval2Enabled = Netpay.Infrastructure.Security.AdminUser.Current.IsInGroup(Domain.Current.FinanceLevel2Group) && WireData.CanApprove;
            rptUnsettled.DataSource = Netpay.Bll.Transactions.Transaction.GetUnsettledBalance(new Netpay.Bll.Transactions.Transaction.SearchFilters() { merchantIDs = new int[] { account.MerchantID.GetValueOrDefault() }.ToList() });
            rptLog.DataSource = Netpay.Bll.Wires.WireLog.Search(WireData.ID, null);
            base.DataBind();
        }

        protected void Wire_Command(object sender, CommandEventArgs e)
        {
            TemplatePage.PageController.LoadActiveItem();
            WireData = TemplatePage.GetItemData<Netpay.Bll.Wires.Wire>();
            switch (e.CommandName)
            {
                case "Approve":
                    if ((string)e.CommandArgument == "1") WireData.Approve(1, ddlApproval1.BoolValue);
                    else if ((string)e.CommandArgument == "2") WireData.Approve(2, ddlApproval2.BoolValue);
                    break;
                case "Reset":
                    if (WireData.IsResetable) WireData.Reset();
                    break;
                case "Cancel":
                    if (WireData.IsCalcelable) WireData.Cancel();
                    break;
                case "Log":
                    if (!string.IsNullOrEmpty(txtConfirmation.Text) && WireData.ConfirmationNumber != txtConfirmation.Text)
                        WireData.SetConfirmationNumber(txtConfirmation.Text);
                    if (ufFile1.HasFile) Bll.Wires.WireLog.AddFile(WireData.ID, txtComment.Text, ufFile1.FileName, new System.IO.MemoryStream(ufFile1.FileBytes));
                    else if (!string.IsNullOrEmpty(txtComment.Text)) WireData.SetComment(txtComment.Text);
                    txtComment.Text = "";
                    break;
                case "GetFile":
                    Netpay.Web.WebUtils.SendFile(e.CommandArgument.ToString(), null, "attachment");
                    return;
            }
            TemplatePage.PageController.ListView.BindAndUpdate();
            TemplatePage.PageController.FormView.BindAndUpdate();
        }
    }
}