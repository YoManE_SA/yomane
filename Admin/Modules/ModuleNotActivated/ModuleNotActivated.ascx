﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModuleNotActivated.ascx.cs" Inherits="Netpay.Admin.Modules.ModuleNotActivated.ModuleNotActivated" %>
<admin:FormSections runat="server" IsForSecurityMessage="true" ID="ModuleNotActive" Title='<%# ModuleName %>'>
    <div class="alert alert-danger">
        <asp:Label runat="server" Font-Bold="true" Text='<%# ModuleName + " Module not active" %>'>
        </asp:Label>
    </div>
</admin:FormSections>
