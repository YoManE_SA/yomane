﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.TerminalsWithoutFees
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuItem;
        public override string Name { get { return "TerminalsWithoutFees"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return "Terminals without fees log."; } }

        public Module() : base(Bll.DebitCompanies.TerminalWithoutFees.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu item
            menuItem = new ApplicationMenu("Terminals Without Fees", "~/Logs/TerminalsWithoutFees/0", null, 150);

            //Register route
            Application.RegisterRoute("Logs/TerminalsWithoutFees/{id}", "TerminalsWithoutFees", typeof(Controls.AccountPage), module:this);

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {                     
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {                     
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Logs", menuItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuItem.Parent.RemoveChild(menuItem);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "TerminalsWithoutFees")
            {
                page.PageController.LoadItem += PageController_TerminalsWithoutFees_LoadItem;
                page.AddControlToList(page.LoadControl("~/Modules/TerminalsWithoutFees/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/TerminalsWithoutFees/Filter.ascx"));

                var control = page.LoadControl("~/Modules/Merchants/TerminalSettings.ascx");
                page.Items.Add("TerminalsWithoutFees_TerminalSettingsControl", control);
                page.AddControlToForm("Terminal Settings", control, this, "", new Controls.SecuredObjectSelector(Bll.TerminalsWithZeroFees.SecuredObject, Bll.Merchants.ProcessTerminals.SecuredObject).GetActivObject);
            }
        }


        void PageController_TerminalsWithoutFees_LoadItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            page.ItemID = page.PageController.DataKey["CcfId"].ToNullableInt();

            var terminalWithZeroFeesitem = Netpay.Bll.TerminalsWithZeroFees.Load(page.ItemID.GetValueOrDefault());

            var item = terminalWithZeroFeesitem.ProcessTerminal;

            if (item == null)
                return;

            // Set Account
            var account = Bll.Merchants.Merchant.Load(item.MerchantID);
            if (account != null)
            {
                (page as Controls.AccountPage).Account = account;
                page.PageController.DataKey = new System.Web.UI.WebControls.DataKey(new System.Collections.Specialized.OrderedDictionary() { { "AccountID", account.AccountID } });
            }

            if (page.ItemID != null) page.SetItemData(item);
            page.FormButtons.EnableSave = false;
            page.FormButtons.EnableDelete = false;

            var control = (page as Controls.AccountPage).Items["TerminalsWithoutFees_TerminalSettingsControl"] as Netpay.Admin.Merchants.TerminalSettings;
            if (control != null)
            {
                control.ItemData = item;
            }
        }
    }
}