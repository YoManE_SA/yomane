﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.TerminalsWithoutFees.Filter" %>
<admin:FilterSection runat="server" Title="Filter The Table">
    <div class="row">
        <div class="col-lg-12">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox ID="TransFeeCheckBox" AutoPostBack="False" Text="Trans Fee" runat="server" /></li>
                <li class="list-group-item">
                    <asp:CheckBox ID="ClearingFeeCheckBox" AutoPostBack="False" Text="Clearing Fee" runat="server" /></li>
                <li class="list-group-item">
                    <asp:CheckBox ID="PreAuthFeeCheckBox" AutoPostBack="False" Text="PreAuth Fee" runat="server" /></li>
                <li class="list-group-item">
                    <asp:CheckBox ID="RefundFeeCheckBox" AutoPostBack="False" Text="Refund Fee" runat="server" /></li>
                <li class="list-group-item">
                    <asp:CheckBox ID="CopyRFeeCheckBox" AutoPostBack="False" Text="CopyR Fee" runat="server" /></li>
                <li class="list-group-item">
                    <asp:CheckBox ID="CHBFeeCheckBox" AutoPostBack="False" Text="CHB Fee" runat="server" /></li>
                <li class="list-group-item">
                    <asp:CheckBox ID="FailFeeCheckBox" AutoPostBack="False" Text="Fail Fee" runat="server" /></li>
            </ul>
        </div>
    </div>
</admin:FilterSection>
