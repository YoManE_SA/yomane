﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.TerminalsWithoutFees.List" %>
<admin:ListSection runat="server" Title="Terminals Without Fees">
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" ID="rptList" SaveAjaxState="true" DataKeyNames="CcfId" AutoGenerateColumns="false" BubbleLoadEvent="true" SortKey="MerchantName">
                <Columns>
                    <asp:BoundField HeaderText="Merchant" DataField="MerchantName" SortExpression="MerchantName" />
                    
                    <asp:TemplateField HeaderText="What Payment method do I want to configure" SortExpression="PaymentMethodName">
                        <ItemTemplate>
                            <asp:Image runat="server" ImageUrl='<%# "/NPCommon/ImgPaymentMethod/23X12/" + Eval("PaymentMethodID") + ".gif" %>' ImageAlign="Middle" />
                            <%# Eval("PaymentMethodName") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Debit Company" SortExpression="DebitCompanyName">
                        <ItemTemplate>
                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# "/NPCommon/ImgDebitCompanys/23X12/" + Eval("DebitCompanyID") + ".gif" %>' ImageAlign="Middle" Visible='<%# ((int?) Eval("DebitCompanyID")) != null %>' />
                            <%# Eval("DebitCompanyName") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Terminal Name" DataField="TerminalName" SortExpression="TerminalName" />
                    <asp:BoundField HeaderText="Terminal No." DataField="TerminalNumber" SortExpression="TerminalNumber" />
                    
                    <asp:TemplateField HeaderText="Trans. Fee" SortExpression="TransFee">
                        <ItemTemplate>                            
                            <asp:Label runat="server" Text='<%# Eval("TransFee") %>' ForeColor='<%# GetColor((decimal)Eval("TransFee")) %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Clearing Fee" SortExpression="ClearingFee">
                        <ItemTemplate>                            
                            <asp:Label runat="server" Text='<%# Eval("ClearingFee") %>' ForeColor='<%# GetColor((decimal)Eval("ClearingFee")) %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Pre Auth Fee" SortExpression="PreAuthFee">
                        <ItemTemplate>                            
                            <asp:Label runat="server" Text='<%# Eval("PreAuthFee") %>' ForeColor='<%# GetColor((decimal)Eval("PreAuthFee")) %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Refund Fee" SortExpression="RefundFee">
                        <ItemTemplate>                            
                            <asp:Label runat="server" Text='<%# Eval("RefundFee") %>' ForeColor='<%# GetColor((decimal)Eval("RefundFee")) %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Copy R. Fee" SortExpression="CopyRequestFee">
                        <ItemTemplate>                            
                            <asp:Label runat="server" Text='<%# Eval("CopyRequestFee") %>' ForeColor='<%# GetColor((decimal)Eval("CopyRequestFee")) %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="CHB Fee" SortExpression="ChbFee">
                        <ItemTemplate>                            
                            <asp:Label runat="server" Text='<%# Eval("ChbFee") %>' ForeColor='<%# GetColor((decimal)Eval("ChbFee")) %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Fail Fee" SortExpression="FailFee">
                        <ItemTemplate>                            
                            <asp:Label runat="server" Text='<%# Eval("FailFee") %>' ForeColor='<%# GetColor((decimal)Eval("FailFee")) %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd="false" />
    </Footer>
</admin:ListSection>