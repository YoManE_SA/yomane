﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.TerminalsWithoutFees
{
    public partial class List : Controls.TemplateControlBase
    {
        protected override void OnLoad(EventArgs e)
        {
            rptList.DataBinding += List_DataBinding;
            base.OnLoad(e);
        }

        private void List_DataBinding(object sender, EventArgs e)
        {
            var list = Bll.TerminalsWithZeroFees.Search(TemplatePage.GetFilter<Bll.TerminalsWithZeroFees.SearchFilters>(), rptList);
            if (list != null)
                rptList.DataSource = list;
        }

      
        protected Color GetColor(decimal value)
        {
            if (value == 0)
                return Color.Red;
            return Color.Black;
        }
    }
}