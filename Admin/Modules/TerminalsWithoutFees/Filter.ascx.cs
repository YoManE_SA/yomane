﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.TerminalsWithoutFees
{

    public partial class Filter : Controls.TemplateControlBase
    {
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
        {
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Bll.TerminalsWithZeroFees.SearchFilters();
            if (TransFeeCheckBox.Checked) sf.TransFee = true;
            if (ClearingFeeCheckBox.Checked) sf.ClearingFee = true;
            if (PreAuthFeeCheckBox.Checked) sf.PreAuthFee = true;
            if (RefundFeeCheckBox.Checked) sf.RefundFee = true;
            if (CopyRFeeCheckBox.Checked) sf.CopyRFee = true;
            if (CHBFeeCheckBox.Checked) sf.CHBFee = true;
            if (FailFeeCheckBox.Checked) sf.FailFee = true;
            TemplatePage.SetFilter(sf);
        }
    }
}