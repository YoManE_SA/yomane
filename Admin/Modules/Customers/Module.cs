﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Admin.Customers
{
	public class Module : Admin.CoreBasedModule
	{
		private ApplicationMenu ManagmentMenuItem, searchBarItem;

        public override string Name { get { return "Customer"; } }
		public override decimal Version { get { return 1.0m; } }
		public override string Author { get { return "OBL ltd."; } }
		public override string Description { get { return "Customer management UI"; } }

		public Module() : base(Bll.Customers.Module.Current) { }

		protected override void OnInit(EventArgs e)
		{
            //Create menu item
            ManagmentMenuItem = new ApplicationMenu("Customers Management", "~/Customers/0", "fa-group",1);
            //searchBarItem = new ApplicationMenu("Customer", "~/Customers/0?Search=1&SelectedIndex=0&ctl00.apCustomer.hdID=", "fa-credit-card");

            //Register route
            Application.RegisterRoute("Customers/{*id}", "Customer", typeof(Controls.AccountPage), module:this);//, new System.Web.Routing.RouteValueDictionary() { { "id", @"\d*" } }

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
		}
		
		protected override void OnActivate(EventArgs e)
		{			
            base.OnActivate(e);
		}

		protected override void OnDeactivate(EventArgs e)
		{			
		    base.OnDeactivate(e);
		}

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Customers", ManagmentMenuItem);
            //Application.QuickSearch.AddChild(searchBarItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            ManagmentMenuItem.Parent.RemoveChild(ManagmentMenuItem);
            //Application.QuickSearch.RemoveChild(searchBarItem);
            base.OnUninstallAdmin(e);
        }
        
        protected void Application_InitTemplatePage(object sender, EventArgs e)
		{
            if (!CoreModule.IsInstalled)
                return;

			var page = sender as Controls.DataTablePage;
			if (page.TemplateName == "Customer")
			{
                (page as Controls.AccountPage).AccountChanged += delegate
                {
                    if ((page as Controls.AccountPage).Account != null)
                        (page as Controls.AccountPage).SetStatusColor(Bll.Customers.Customer.ActiveStatusColor.MapColor(((page as Controls.AccountPage).Account as Bll.Customers.Customer).ActiveStatus));
                };

                (page as Controls.AccountPage).PageController.NewItem += PageController_NewItem;
                (page as Controls.AccountPage).PageController.LoadItem += PageController_LoadItem;
                (page as Controls.AccountPage).PageController.PreSaveItem += PageController_PreSaveItem;
                (page as Controls.AccountPage).PageController.PostSaveItem += PageController_PostSaveItem;

                page.AddControlToForm("Summary", page.LoadControl("~/Modules/Customers/CustomerStatus.ascx"), this, "", page.CurrentPageSecuredObject);
                page.AddControlToForm("Summary", page.LoadControl("~/Modules/Customers/TransactionCount.ascx"), this, "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject, Bll.Transactions.Transaction.SecuredObject).GetActivObject);
                page.AddControlToList(page.LoadControl("~/Modules/Customers/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Customers/Filter.ascx"));
                page.AddControlToForm("Data", page.LoadControl("~/Modules/Customers/Data.ascx"), this, "", page.CurrentPageSecuredObject);
                page.AddControlToForm("Shipping Addresses", page.LoadControl("~/Modules/Customers/ShippingAddresses.ascx"), this, "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject,Bll.Customers.ShippingAddress.SecuredObject).GetActivObject);
                page.AddControlToForm("Device List", page.LoadControl("~/Modules/Customers/DeviceList.ascx"),this, "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject,Bll.Accounts.MobileDevice.SecuredObject).GetActivObject);
			}
		}

        private void PageController_PreSaveItem(object sender, EventArgs e)
        {
            var page = (sender as Controls.DataPageController).Page as Controls.AccountPage;
            VerifyItemData(page);
        }

        private void PageController_NewItem(object sender, EventArgs e)
        {
            if (Netpay.Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Customers.Customer.SecuredObject, PermissionValue.Add);

            var page = (sender as Controls.DataPageController).Page as Controls.AccountPage;
            page.FormButtons.EnableDelete = false;
            (page as Controls.AccountPage).Account = new Bll.Customers.Customer();
        }

        void PageController_LoadItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.AccountPage;

            page.FormButtons.EnableDelete = false;
            page.FormButtons.EnableSave = page.CurrentPageSecuredObject.HasPermission(PermissionValue.Edit);
        }
        /// <summary>
        /// Set the item data for the Customer tabs.
        /// If new entity then SetItemData on new Customer.
        /// Otherwise, in DataPageController's OnSaveItem, it called LoadItem which already loaded 
        /// the Account and the Customer entities. Thus only thing left to do is to SetItemData 
        /// on the Account as Custmer.
        /// </summary>
        /// <param name="page"></param>
        private void VerifyItemData(Netpay.Admin.Controls.AccountPage page)
        {
            var newItem = page.GetItemData<Bll.Customers.Customer>();
            if (newItem == null)
            {
                // Check if account was already loaded
                if (page.Account != null)
                {
                    page.SetItemData<Bll.Customers.Customer>(page.Account as Bll.Customers.Customer);
                }
                else
                {
                    newItem = new Bll.Customers.Customer();
                    page.SetItemData<Bll.Customers.Customer>(newItem);
                }
            }
        }

        private void PageController_PostSaveItem(object sender, EventArgs e)
        {   
            var page = (sender as System.Web.UI.Control).Page as Controls.AccountPage;

            var itemToSave = page.GetItemData<Bll.Customers.Customer>();
            if (itemToSave == null) return;

            var isNewCustomer = (itemToSave.AccountID == 0);
            itemToSave.Save();


            if (isNewCustomer) page.Account = itemToSave;
            //Try to get the Customer after save because now it has a new ID
            //And it is needed to load the account after it was saved 
            //In order to show new details that were generated by the database.
            if (page != null)
            {
                if (page.Account.AccountType == Bll.Accounts.AccountType.Customer)
                {
                    if (page.Account == null) return;
                    {
                        int customerid = (int)page.Account.CustomerID;
                        var CustomerAfterSave = Bll.Customers.Customer.Load(customerid);
                        page.Account = CustomerAfterSave;
                        page.PageController.DataKey = new System.Web.UI.WebControls.DataKey(new System.Collections.Specialized.OrderedDictionary() { { "ID", CustomerAfterSave.AccountID } });
                        page.PageController.FormView.Update();
                    }
                }
            }
        }
    }
}