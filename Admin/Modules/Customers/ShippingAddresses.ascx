﻿<%@ Control Language="C#" CodeBehind="ShippingAddresses.ascx.cs" Inherits="Netpay.Admin.Customers.ShippingAddresses" %>
<%@ Register Src="../Accounts/Address.ascx" TagPrefix="CUC" TagName="AccountAddress" %>
<admin:FormPanel runat="server" ID="pnlList" Title="Shipping Addresses" EnableAdd="true" OnCommand="Dialog_Command">
    <NP:PagedRepeater runat="server" ID="rptList">
        <HeaderTemplate>
            <table class="table table-hover table-customize">
                <thead>
                     <th>ID</th>
                     <th>Address1</th>
                     <th>Address2</th>
                     <th>City</th>
                    <th>Country</th>
                    <th>State</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </thead>
        </HeaderTemplate>
        <ItemTemplate>
            <tbody>
                <td><%# Eval("ID") %> </td>
                <td><%# Eval("AddressLine1") %></td>
                <td><%# Eval("AddressLine2") %></td>
                <td><%# Eval("City") %></td>
                <td><%# Eval("CountryISOCode")==null? "" : Netpay.Bll.Country.Get(Eval("CountryISOCode").ToString()).Name %></td>
                <td><%# Eval("StateISOCode")==null? "" : Netpay.Bll.State.GetName(Eval("StateISOCode").ToString()) %></td>
                <td> <asp:LinkButton ID="DeleteButton" CssClass="btn btn-danger" runat="server" Text='Delete' CommandName="DeleteItem" CommandArgument='<%# Eval("ID") %>' OnCommand="Dialog_Command" /></td>
                <td> <asp:LinkButton ID="EditButton" CssClass="btn btn-primary" runat="server" Text='Edit' CommandName="EditItem" CommandArgument='<%# Eval("ID") %>' OnCommand="Dialog_Command"/> </td>
            </tbody>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        <EmptyContainer><div class="alert alert-info"><strong>Info!</strong> No record information</div></EmptyContainer>
    </NP:PagedRepeater>
</admin:FormPanel>

<admin:ModalDialog runat="server" ID="dlgEditForm" Title="Edit Shipping Address" OnCommand="Dialog_Command">
    <Body>
        <asp:HiddenField runat="server" ID="hfShippingAddressID" Value='<%# ItemData.ID %>' />
        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    Title <span class="help">e.g "My work"</span>
                    <asp:TextBox CssClass="form-control" runat="server" ID="txtTitle" Text='<%# ItemData.Title %>' />
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <CUC:AccountAddress runat="server" ID="caAddress" AddressData='<%# ItemData %>' ValidationGroup="Customers_ShippingAddress_ddl" />
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    Comment
					<asp:TextBox runat="server" CssClass="form-control" ID="txtComment" TextMode="MultiLine" Text='<%# ItemData.Comment %>' />
                </div>
            </div>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" EnableSave="true" EnableDelete="false" />
    </Footer>
</admin:ModalDialog>

<admin:ModalDialog runat="server" ID="dlgCountryNotSelectedError" Title="Save Shipping Address Error Msg">
    <Body>
        <netpay:ActionNotify runat="server" ID="SaveShippingAddressErrorMsg" />
    </Body>
</admin:ModalDialog>
