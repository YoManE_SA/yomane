﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Customers.Filter" %>
<admin:FilterSection runat="server" Title="Status">
    <div class="form-group">
        <asp:Label ID="lblCustomer" runat="server" Text="Name/ID" />
        <admin:AccountPicker runat="server" ID="apAccount" LimitToType="Customer"  />
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblID" runat="server" Text="ID Range" />
                <JQ:IntRange runat="server" ID="rngID" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblInsertDate" runat="server" Text="Registration Date Range" />
                <JQ:DateRange runat="server" ID="rngRegisterDate" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <asp:Label ID="Label1" runat="server" Text="Status" />
                <netpay:EnumDropDown
                     runat="server" 
                    CssClass="form-control" 
                    ViewEnumName="Netpay.Bll.Customers.ActiveStatus" 
                    ID="ddlStatus" 
                    EnableBlankSelection="true" 
                    BlankSelectionText="[All]" >
                </netpay:EnumDropDown>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <asp:Label ID="Identity" runat="server" Text="Application Identity" />
                <netpay:ApplicationIdentityDropDown
                     CssClass="form-control" 
                     runat="server" 
                     ID="ddlAppIdentitySearch"
                     BlankSelectionText="[All]"
                     EnableNaSelection="true"
                     NaSelectionText="[N/A]"
                     NaSelectionValue="NA"
                      />
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <asp:Label ID="NameOrEmailLabel" runat="server" Text="Name/Email that contains name" />
                <asp:TextBox CssClass="form-control" runat="server" ID="txtTextSearch" />
            </div>
        </div>
        <%--<div class="col-lg-6">
            <div class="form-group">
                <asp:Label ID="EmailLabel" runat="server" Text="Email Address" />
                <asp:TextBox CssClass="form-control" runat="server" ID="txtEmailFilter" />
            </div>
        </div>--%>
        <div class="col-lg-6">
            <div class="form-group">
                <asp:Label ID="Label3" runat="server" Text="Facebook User Id" />
                <asp:TextBox CssClass="form-control" runat="server" ID="txtFaceBookUserId" />
            </div>
        </div>

    </div>

</admin:FilterSection>
