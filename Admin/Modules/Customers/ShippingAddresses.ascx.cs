﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;
using Netpay.Infrastructure.Security;

namespace Netpay.Admin.Customers
{
	public partial class ShippingAddresses : Controls.AccountControlBase
	{
		protected Netpay.Bll.Customers.ShippingAddress ItemData;
        protected override void OnInit(EventArgs e)
        {
            rptList.DataBinding += delegate { if (Account != null && Account.AccountID!=0) rptList.DataSource = Netpay.Bll.Customers.ShippingAddress.LoadForCustomer(Account.CustomerID); };
            base.OnInit(e);
        }

        protected override void DataBindChildren()
		{
			rptList.DataBind();
			//base.DataBindChildren();
		}

		//protected void List_Command(object sender, RepeaterCommandEventArgs e)
		//{
		//	if (e.CommandName == "Select")
		//	{
		//		ItemData = Netpay.Bll.Customers.ShippingAddress.Load(((string)e.CommandArgument).EmptyIfNull().ToInt32(0));
		//		if (ItemData != null) dlgEditForm.BindAndShow();
		//	}
		//}

        protected void Dialog_Command(object sender, CommandEventArgs e)
		{

            if (e.CommandName == "EditItem")
            {
                ItemData = Netpay.Bll.Customers.ShippingAddress.Load(((string)e.CommandArgument).EmptyIfNull().ToInt32(0));
                if (ItemData != null) dlgEditForm.BindAndShow();

            }  else if (e.CommandName == "AddItem") {

                if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Customers.ShippingAddress.SecuredObject, PermissionValue.Add);

                ItemData = new Netpay.Bll.Customers.ShippingAddress();
                if (ItemData != null) dlgEditForm.BindAndShow();

            }  else if (e.CommandName == "SaveItem") {
                ItemData = Netpay.Bll.Customers.ShippingAddress.Load(hfShippingAddressID.Value.ToInt32(0));
                if (ItemData == null)
                {
                    ItemData = new Netpay.Bll.Customers.ShippingAddress();
                    ItemData.Customer_id = Account.CustomerID.Value;
                }
                
                

                ItemData.Title = txtTitle.Text;
                ItemData.Comment = txtComment.Text;
                caAddress.AddressData = ItemData;
                
                var check = caAddress.Save();
                if (check == null)
                {
                    dlgEditForm.RegisterHide();
                    SaveShippingAddressErrorMsg.SetMessage("You can't save Shipping Address without choosing country", true);
                    dlgCountryNotSelectedError.RegisterShow();
                }
                else
                {
                    ItemData.Save();
                    dlgEditForm.RegisterHide();
                    pnlList.BindAndUpdate();
                }

            } else if (e.CommandName == "DeleteItem") {
                
                ItemData = Netpay.Bll.Customers.ShippingAddress.Load(((string)e.CommandArgument).EmptyIfNull().ToInt32(0));
                ItemData.Delete();
                pnlList.BindAndUpdate();
            }
		}
	}
}