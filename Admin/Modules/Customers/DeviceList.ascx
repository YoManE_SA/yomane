﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DeviceList.ascx.cs" Inherits="Netpay.Admin.Modules.Customers.DeviceList" %>
<admin:FormSections runat="server" Title="Device List">
    <asp:MultiView runat="server" ID="dlMultiView">
        <asp:View runat="server">
            <asp:Repeater runat="server" ID="rptList">
                <ItemTemplate>
                    <div class="panel panel-default">
                        <div class="panel-heading font-small" style="background-color: #fcfcfc;">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                  <i class="fa fa-phone-square"></i> Device Phone Number: <%# Eval("DevicePhoneNumber") %>  
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <ul class="list-group">
                                        <li class="list-group-item">Device Identity: <%# Eval("DeviceIdentity") %></li>
                                        <li class="list-group-item"><span class="BreakRow">Push Token: <%# Eval("PushToken") %></span></li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <ul class="list-group">
                                        <li class="list-group-item">Account ID: <%# ItemData.AccountID %></li>
                                        <li class="list-group-item">Account Number: <%# ItemData.AccountNumber %></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </asp:View>
        <asp:View runat="server">
            <div class="alert alert-info">
                No devices found
            </div>
        </asp:View>
    </asp:MultiView>
</admin:FormSections>
