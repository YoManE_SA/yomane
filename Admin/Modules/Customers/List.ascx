﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Customers.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Customers">
    <Header>
        <admin:LegendColors runat="server" Items='<%# Netpay.Bll.Customers.Customer.ActiveStatusColor %>' FloatRight="true" />
    </Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" ID="rptList" SaveAjaxState="true" DataKeyNames="AccountID" AutoGenerateColumns="false" BubbleLoadEvent="true">
                <Columns>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <span style="background-color: <%# Netpay.Bll.Customers.Customer.ActiveStatusColor.MapHtmlColor((Netpay.Bll.Customers.ActiveStatus)Eval("ActiveStatus")) %>;" class="legend-item"></span>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ID" SortExpression="ID">
                        <ItemTemplate>
                            <%# Eval("AccountID") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Full Name" DataField="FullName" SortExpression="FullName" />
                    <asp:BoundField HeaderText="Country" DataField="PersonalAddress.CountryISOCode" SortExpression="PersonalAddress.CountryISOCode" />
                    <asp:BoundField HeaderText="Registration Date" DataField="RegistrationDate" SortExpression="RegistrationDate" DataFormatString="{0:d}" />
                    <asp:BoundField HeaderText="App Identity" DataField="RefIdentity.Name" SortExpression="RefIdentityID" />
                    <asp:BoundField HeaderText="Email Address" DataField="EmailAddress" SortExpression="EmailAddress" />

                    <%--<asp:TemplateField HeaderText="Pass Count">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" Text='<%# Eval("ProcessStatus.PassCount") %>' NavigateUrl='<%# "~/Transactions/Search?Status=0&CustomerID=" + Eval("ID") %>' /></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Fail Count">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" Text='<%# Eval("ProcessStatus.FailCount") %>' NavigateUrl='<%# "~/Transactions/Search?Status=1&CustomerID=" + Eval("ID") %>' /></ItemTemplate>
                    </asp:TemplateField>--%>
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd='<%# Netpay.Bll.Customers.Customer.SecuredObject.HasPermission(Netpay.Infrastructure.Security.PermissionValue.Add) %>' />
    </Footer>
</admin:ListSection>
