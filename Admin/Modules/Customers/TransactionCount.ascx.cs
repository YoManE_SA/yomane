﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;

namespace Netpay.Admin.Modules.Customers
{
    public partial class TransactionCount : Controls.AccountControlBase
    {
        protected Netpay.Bll.Customers.Customer ItemData;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            ItemData=Account as Netpay.Bll.Customers.Customer;
            if (ItemData == null) return;
            base.DataBindChildren();
        }
    }
}