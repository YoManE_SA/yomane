﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Customers
{
	public partial class Filter : Controls.TemplateControlBase
	{
		protected override void OnLoad(EventArgs e)
		{
			TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
            base.OnLoad(e);
		}

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
		{
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Netpay.Bll.Customers.Customer.SearchFilters();

            //The customer account search filter , includ  id range filter - [V]
            sf.ID = apAccount.Value.HasValue ? new Range<int?>(apAccount.Value) : rngID.Value;

            //Registeration date range filter - [V]
            sf.RegistrationDate = rngRegisterDate.Value;

            //Status dropdown filter - [V]
            sf.Status = ddlStatus.SelectedValue.ToNullableEnumByValue<Netpay.Bll.Customers.ActiveStatus>();

            //Application Identity filter - [V]
            if (ddlAppIdentitySearch.Value.ToNullableInt() != null)
            {
                sf.RefIdentityId = new List<int> { ddlAppIdentitySearch.Value.ToNullableInt().Value };
            }
            else if (ddlAppIdentitySearch.Value == "NA")
            {
                sf.SearchNullAppIdentity = true;
            }

            //Name or Email search:
            sf.Name = txtTextSearch.Text.Trim();

            //Email filter
            //if (!string.IsNullOrEmpty(txtEmailFilter.Text)) sf.EmailAddress = txtEmailFilter.Text;

            //Facebook user filter
            if (!string.IsNullOrEmpty(txtFaceBookUserId.Text)) sf.FacebookUserID = txtFaceBookUserId.Text;

            TemplatePage.SetFilter(sf);
            //var pi = new Bll.PagingInfo() { CurrentPage = 1, PageSize = ddlPageSize.Text.ToInt32(25) };
        }
    }
}