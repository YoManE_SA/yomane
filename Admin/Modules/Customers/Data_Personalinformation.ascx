﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Data_Personalinformation.ascx.cs" Inherits="Netpay.Admin.Modules.Customers.Data_Personalinformation" %>
<admin:FormSections runat="server" Title="Personal information" ID="PersonalInformationFormSection" TabIndex="0">
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                Customer Number : 
				<asp:Literal runat="server" ID="ltNumber" 
                    Text='<%# ItemData.AccountNumber %>' />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                First Name
				<asp:TextBox runat="server" CssClass="form-control" ID="pitxtFirstName" Text='<%# ItemData.FirstName %>' />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                Last Name
				<asp:TextBox runat="server" CssClass="form-control" ID="pitxtLastName" Text='<%# ItemData.LastName %>' />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                Personal ID
				<asp:TextBox runat="server" CssClass="form-control" ID="pitxtPersonalID" Text='<%# ItemData.PersonalNumber %>' />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                Phone
				<asp:TextBox runat="server" CssClass="form-control" ID="pitxtPhone" Text='<%# ItemData.CellNumber %>' />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                Cellular
				<asp:TextBox runat="server" CssClass="form-control" ID="pitxtCellular" Text='<%# ItemData.CellNumber %>' />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                FaceBook user ID
				<asp:TextBox runat="server" CssClass="form-control" ID="pitxtFaceBookUserId" Text='<%# ItemData.FacebookUserID %>'  MaxLength="20"/>
            </div>
        </div>
    </div>
</admin:FormSections>