﻿<%@ Control Language="C#" CodeBehind="Data.ascx.cs" Inherits="Netpay.Admin.Customers.Data" %>
<%@ Register Src="~/Modules/Accounts/Address.ascx" TagPrefix="CUC" TagName="AccountAddress" %>
<%--<admin:FormSections runat="server" Title="Personal information" ID="PersonalInformationFormSection" TabIndex="0">
    //This section is commented because it was moved to outer control "Data_PersonalInformation.ascx"
    //In order to change its order in the UI
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                Customer Number
				<asp:Literal runat="server" ID="ltNumber" Text='<%# ItemData.AccountNumber %>' />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                First Name
				<asp:TextBox runat="server" CssClass="form-control" ID="txtFirstName" Text='<%# ItemData.FirstName %>' />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                Last Name
				<asp:TextBox runat="server" CssClass="form-control" ID="txtLastName" Text='<%# ItemData.LastName %>' />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                Personal ID
				<asp:TextBox runat="server" CssClass="form-control" ID="txtPersonalID" Text='<%# ItemData.PersonalNumber %>' />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                Phone
				<asp:TextBox runat="server" CssClass="form-control" ID="txtPhone" Text='<%# ItemData.CellNumber %>' />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                Cellular
				<asp:TextBox runat="server" CssClass="form-control" ID="txtCellular" Text='<%# ItemData.CellNumber %>' />
            </div>
        </div>
    </div>
</admin:FormSections>--%>
<admin:FormSections runat="server" Title="Address">
    <CUC:AccountAddress runat="server" ID="caAddress" AddressData='<%# ItemData.PersonalAddress %>' ValidationGroup="Customers_Data_ddl" />
</admin:FormSections>
<admin:FormSections runat="server" Title="Security Settings">
    <div class="row">
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Identity
                <netpay:ApplicationIdentityDropDown OnSelectedIndexChanged="ddlAppIdentity_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server" ID="ddlAppIdentity" EnableBlankSelection="true" BlankSelectionValue="" Value='<%# ItemData.RefIdentityID.HasValue? ItemData.RefIdentityID.Value.ToString() : "" %>' />
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Email
				<asp:TextBox runat="server" CssClass="form-control" ID="txtEmailAddress" Text='<%# ItemData.EmailAddress %>' />
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                PinCode
				<asp:TextBox runat="server" CssClass="form-control" ID="txtPinCode" Text="****" MaxLength="4" />
            </div>
        </div>
    </div>
</admin:FormSections>
<admin:ModalDialog runat="server" ID="dlgCustomers" Title="Save Customer Error Msg">
    <Body>
        <netpay:ActionNotify runat="server" ID="SaveCustomerErrorMessage" />
    </Body>
</admin:ModalDialog>
