﻿ <%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TransactionCount.ascx.cs" Inherits="Netpay.Admin.Modules.Customers.TransactionCount" %>
<admin:FormSections runat="server" Title="Transaction Count">
   <div class="row">
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Fail Count :
                <asp:HyperLink
                     Style="cursor: pointer"
                     runat="server" 
                     Text='<%# ItemData.TransFailCount  %>' 
                     onclick='<%# "OpenPop(" + "\"" +  string.Format("../Transactions/0?ctl00.chklStatus$1=Declined&ctl00.apCustomer.hdID={0}&NoDateRange=1&NoLayout=1&Search=1", ItemData.ID) + "\"" + ",\"Trans fail history\",\"1330\",\"820\" , \"1\"); return false;"  %>'
                     Target="_blank"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Pass Count :
                <asp:HyperLink 
                    Style="cursor: pointer"
                    runat="server" 
                    Text='<%# ItemData.TransPassCount  %>' 
                    onclick='<%# "OpenPop(" + "\"" +  string.Format("../Transactions/0?ctl00.chklStatus$0=Captured&ctl00.apCustomer.hdID={0}&NoDateRange=1&NoLayout=1&Search=1", ItemData.ID) + "\"" + ",\"Trans fail history\",\"1330\",\"820\" , \"1\"); return false;"  %>'
                    Target="_blank" />
            </div>
        </div>
    </div>
</admin:FormSections>
