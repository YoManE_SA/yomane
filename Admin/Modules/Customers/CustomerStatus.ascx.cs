﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;


namespace Netpay.Admin.Modules.Customers
{
    public partial class CustomerStatus : Controls.AccountControlBase
    {
        protected Netpay.Bll.Customers.Customer CustomerItemData;
        

        protected override void OnLoad(EventArgs e)
        {
            
            TemplatePage.PageController.SaveItem += PageController_SaveItem;
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            CustomerItemData = Account as Bll.Customers.Customer;
            if (CustomerItemData == null) CustomerItemData = new Bll.Customers.Customer();

            base.DataBindChildren();
        }

        protected void PageController_SaveItem(object sender, EventArgs e)
        {
            var CustomerItemData = TemplatePage.GetItemData<Bll.Customers.Customer>();
            if (CustomerItemData == null)
                // Item was supposed to be initialized in the Module's LoadItem event handler
                return;

            var value = byte.Parse(ddlStatus.SelectedValue);
            CustomerItemData.ActiveStatus = (Bll.Customers.ActiveStatus)value;
        }           
    }
}