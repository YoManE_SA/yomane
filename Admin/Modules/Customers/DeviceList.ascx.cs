﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;

namespace Netpay.Admin.Modules.Customers
{
    public partial class DeviceList : Controls.AccountControlBase
    {

        protected Netpay.Bll.Customers.Customer ItemData;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            ItemData = Account as Netpay.Bll.Customers.Customer;
            if (ItemData != null)
            {
                var deviceList = Bll.Accounts.MobileDevice.Search(new Netpay.Bll.Accounts.MobileDevice.SearchFilters() { AccountType = Netpay.Bll.Accounts.AccountType.Customer, AccountID=ItemData.AccountID}, null);
                if (deviceList.Count > 0)
                {
                    rptList.DataSource = deviceList;
                    dlMultiView.ActiveViewIndex = 0;
                }
                else
                {
                    dlMultiView.ActiveViewIndex = 1;
                }
            }
            else
            {
                dlMultiView.ActiveViewIndex = 1;
            } 
            
            base.DataBindChildren();
        }
               
    }
}