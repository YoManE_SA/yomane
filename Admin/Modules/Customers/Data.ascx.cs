﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;

namespace Netpay.Admin.Customers
{
    public partial class Data : Controls.AccountControlBase
    {
        protected Netpay.Bll.Customers.Customer ItemData;

        protected override void OnInit(EventArgs e)
        {
            var tab = (TemplatePage.Page as Controls.DataTablePage).FormTabs.FindControl("Data") as Controls.GlobalControls.JQueryControls.TabView;
            if (tab.Controls.OfType<Netpay.Admin.Modules.Customers.Data_Personalinformation>().Count() == 0)
            {
                var personalInformation = LoadControl("~/Modules/Customers/Data_Personalinformation.ascx");
                tab.Controls.AddAt(0, personalInformation);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += PageController_SaveItem;

            /// I get the "Data" tab , inside it there is "Controls" property where the first control
            /// Is the editLogin , that is need to be moved to the 3rd place.
            var tab = TemplatePage.FormTabs.FindControl("Data") as Controls.GlobalControls.JQueryControls.TabView;
            var controlToChange = tab.Controls;
            ////

            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            ItemData = Account as Netpay.Bll.Customers.Customer;
            if (ItemData == null) ItemData = new Bll.Customers.Customer();
            base.DataBindChildren();
        }

        protected void PageController_SaveItem(object sender, EventArgs e)
        {
            //ItemData = Account as Netpay.Bll.Customers.Customer;
            ItemData = TemplatePage.GetItemData<Bll.Customers.Customer>();
            if (ItemData == null)
                // Item was supposed to be initialized in the Module's LoadItem event handler
                return;

            // Get the outside control that is inside the "Data" tab.
            var tab = TemplatePage.FormTabs.FindControl("Data") as Controls.GlobalControls.JQueryControls.TabView;
            var dataPersonalinformation = tab.Controls[0];

            // Get the values from the outside control Data_Personalinformation:
            // Into the current ItemData "Customer"
            // pi prefix stands for - personal informaion , it's the outside control.
            ItemData.FirstName = (dataPersonalinformation.FindControl("pitxtFirstName") as TextBox).Text;
            ItemData.LastName = (dataPersonalinformation.FindControl("pitxtLastName") as TextBox).Text;
            ItemData.PersonalNumber = (dataPersonalinformation.FindControl("pitxtPersonalID") as TextBox).Text;
            ItemData.PhoneNumber = (dataPersonalinformation.FindControl("pitxtPhone") as TextBox).Text;
            ItemData.CellNumber = (dataPersonalinformation.FindControl("pitxtCellular") as TextBox).Text;
            ItemData.FacebookUserID = (dataPersonalinformation.FindControl("pitxtFaceBookUserId") as TextBox).Text;
            ///

            //Get the values from the outside control "EditLogin"
            Security.EditLogin editlogin = tab.Controls.OfType<Netpay.Admin.Security.EditLogin>().SingleOrDefault();

            //Customer account login user name 
            ItemData.Login.UserName = string.IsNullOrEmpty((editlogin.FindControl("txtUserName") as TextBox).Text.Trim())? null : (editlogin.FindControl("txtUserName") as TextBox).Text.Trim();
            //

            ItemData.EmailAddress = txtEmailAddress.Text;
            ItemData.RefIdentityID = ddlAppIdentity.SelectedValue.ToNullableInt();
            if (txtPinCode.Text.Length == 4 && txtPinCode.Text.IndexOf('*') == -1)
            {
                ItemData.SetPinCode(txtPinCode.Text);
                txtPinCode.Text = "****";
            }

            ItemData.PersonalAddress = caAddress.Save(ItemData.PersonalAddress);
            if (ItemData.PersonalAddress == null || string.IsNullOrEmpty(ItemData.EmailAddress) || ItemData.RefIdentityID == null)
            {
                if (string.IsNullOrEmpty(ItemData.EmailAddress) && ItemData.PersonalAddress == null)
                {
                    //Throw exception in order to stop execution of the "SaveItem" event.
                    throw new Exception("Country was not selected and Invalid email address");
                }
                else if (string.IsNullOrEmpty(ItemData.EmailAddress))
                {
                    //Throw exception in order to stop execution of the "SaveItem" event.
                    throw new Exception("Customer Invalid email address");
                }
                else if (ItemData.PersonalAddress == null)
                {
                    //Throw exception in order to stop execution of the "SaveItem" event.
                    throw new Exception("Country was not selected.");
                }
                else if (ItemData.RefIdentityID == null)
                {                  
                    //Throw exception in order to stop execution of the "SaveItem" event.
                    throw new Exception("App identity was not selected.");
                }
            }
        }

        protected void ddlAppIdentity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlAppIdentity.IsSelected)
            {
                TemplatePage.PageController.FormView.Update();
            }
            else
            {
                return;
            }
        }
    }
}