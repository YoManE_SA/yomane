﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerStatus.ascx.cs" Inherits="Netpay.Admin.Modules.Customers.CustomerStatus" %>
<admin:FormSections runat="server" Title="Customer Status" Visible ="<%# Account!=null && Account.AccountID!=0 %>">
    <div class="form-group">
        <div class="form-group">
            <asp:Label ID="Label1" runat="server" Text="Status" />
            <netpay:EnumDropDown runat="server" CssClass="form-control" ViewEnumName="Netpay.Bll.Customers.ActiveStatus" ID="ddlStatus" EnableBlankSelection="False" Value="<%#(int)CustomerItemData.ActiveStatus%>" />
        </div>
    </div>
</admin:FormSections>
