﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.Admin.ApplicationIdentities
{
    public partial class References : Controls.TemplateControlBase
    {
        protected Bll.ApplicationIdentity Item { get { return TemplatePage.GetItemData<Bll.ApplicationIdentity>(); } set { TemplatePage.SetItemData(value); } }
		protected override void OnLoad(EventArgs e)
		{
			TemplatePage.PageController.FormView.DataBound += FormView_DataBound;
            TemplatePage.PageController.SaveItem += Save_Click;
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            if (Item == null)
            {
                return;
            }

            var sf = new Bll.PaymentMethods.PaymentMethod.SearchFilters();
            sf.Group = (int)PaymentMethodGroupEnum.Prepaid;
            cblOwnPaymentMethods.DataSource = Bll.PaymentMethods.PaymentMethod.Search(sf, null);
            base.DataBindChildren();
        }

        private void FormView_DataBound(object sender, EventArgs e)
        {
            foreach (var c in Item.OwnedPaymentMethods)
            {
                var li = cblOwnPaymentMethods.Items.FindByValue(((int)c).ToString());
                if (li != null) li.Selected = true;
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            var pmList = new List<int>();
            foreach (ListItem i in cblOwnPaymentMethods.Items)
                if (i.Selected) pmList.Add(i.Value.ToNullableInt().GetValueOrDefault());
            Item.OwnedPaymentMethods = pmList;
            Item.Save();
        }
    }
}