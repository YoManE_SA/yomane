﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Data.ascx.cs" Inherits="Netpay.Admin.ApplicationIdentities.Data" %>
<script type="text/javascript">
    function DeleteText(IdName) {
        var el = document.getElementById(IdName);
        el.value = null;
    }

    function OnCheckBoxClick(checkBoxId) {
        if (document.getElementById(checkBoxId).checked == true) {
            if (confirm('Are you sure you wish to change the hash? Any change to the current will result in a new hash being generated and any app/service using it will have to be updated'))
            {
                document.getElementById('CheckBoxFunctionButton').click();
            }
            else
            {
                document.getElementById(checkBoxId).checked = false;
            }
            
        } else {
            if (confirm('Are you sure you wish to change the hash? Any change to the current will result in a new hash being generated and any app/service using it will have to be updated'))
            {
                document.getElementById('CheckBoxFunctionButton').click();
            }
            else
            {
                document.getElementById(checkBoxId).checked = true;
            }
        }
    }

</script>

<admin:FormSections runat="server" Title="Details">
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                Name
				<asp:TextBox runat="server" CssClass="form-control" ID="txtName" Text='<%# Item.Name %>' />
            </div>
            <div class="form-group">
                BrandName
				<asp:TextBox runat="server" CssClass="form-control" ID="txtBrandName" Text='<%# Item.BrandName %>' />
            </div>
            <div class="form-group">
                CompanyName
				<asp:TextBox runat="server" CssClass="form-control" ID="txtCompanyName" Text='<%# Item.CompanyName %>' />
            </div>
            <div class="form-group">
                DomainName
				<asp:TextBox runat="server" CssClass="form-control" ID="txtDomainName" Text='<%# Item.DomainName %>' />
            </div>
        </div>
        <div class="col-lg-6">

            <div class="form-group">
                Parent Company
                <netpay:ParentCompanyDropDown runat="server" CssClass="form-control" ID="ddlParentCompany" Value='<%# !string.IsNullOrEmpty(Item.ThemeName) && Netpay.Bll.DataManager.ParentCompany.IsNameExist(Item.ThemeName)? Netpay.Bll.DataManager.ParentCompany.Get(Item.ThemeName).ID.ToString() : Item.ThemeName %>' EnableBlankSelection="true" BlankSelectionValue="" BlankSelectionText=""></netpay:ParentCompanyDropDown>
            </div>
            <div class="form-group">
                ContentFolder
				<asp:TextBox runat="server" CssClass="form-control" ID="txtContentFolder" Text='<%# Item.ContentFolder %>' />
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-12">
                        Wire Account ID
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <netpay:WireAccountDropDown runat="server" CssClass="form-control" ID="ddlWireAcountId" EnableBlankSelection="true" Text='<%# Item.WireAccountID %>' />
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                Process Merchant Number
                <admin:MerchantProcessPicker runat="server" ClientIDMode="Static" ID="processnumber" Value='<%# string.IsNullOrEmpty(Item.ProcessMerchantNumber)? "" : Item.ProcessMerchantNumber %>' PlaceHolder="Enter text to search..." onclick="DeleteText('processnumber_txtSearch');" />

            </div>
        </div>
        <div class="col-lg-12">
            <div class="form-group">
                CopyRightText
				<asp:TextBox runat="server" CssClass="form-control" ID="txtCopyRightText" TextMode="MultiLine" Text='<%# Item.CopyRightText %>' />
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <ul class="list-group">
                    <li class="list-group-item">ID
                        <asp:Literal runat="server" ID="ltID" Text='<%# Item.ID %>' /></li>
                    <li class="list-group-item">
                        <asp:CheckBox runat="server" ID="chkIsActive" Text="IsActive" Checked='<%# Item.IsActive %>' /></li>
                </ul>
            </div>

        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <ul class="list-group">
                    <li class="list-group-item">Personal Hash Key:
                        <asp:Label runat="server" ID="lblHashKey" Text='<%# Item.HashKey %>' /></li>
                    <li class="list-group-item">
                        <asp:CheckBox onchange="OnCheckBoxClick('chkEnableHash')" runat="server" ID="chkEnableHash" ClientIDMode="static" Checked='<%# Item.EnableTokenHash %>' Text="Enable Token Hash" /></li>
                        <asp:Button runat="server" ID="CheckBoxFunctionButton" style="display:none;" ClientIDMode="Static" Text="" OnClick="EnableHash_CheckedChanged" />
                </ul>
            </div>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Email Settings">
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                Smtp Server
				<asp:TextBox runat="server" CssClass="form-control" ID="txtSmtpServer" Text='<%# Item.SmtpServer %>' />
            </div>
            <div class="form-group">
                Smtp Username
				<asp:TextBox runat="server" CssClass="form-control" ID="txtSmtpUsername" Text='<%# Item.SmtpUsername %>' />
            </div>
            <div class="form-group">
                Smtp Password
				<asp:TextBox runat="server" CssClass="form-control" ID="txtSmtpPassword" Text='<%# Item.SmtpPassword %>' />
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                Email From
				<asp:TextBox runat="server" CssClass="form-control" ID="txtEmailFrom" Text='<%# Item.EmailFrom %>' />
            </div>
            <div class="form-group">
                Email ContactTo
				<asp:TextBox runat="server" CssClass="form-control" ID="txtEmailContactTo" Text='<%# Item.EmailContactTo %>' />
            </div>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="External Links">
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                URL DevCenter
				<asp:TextBox runat="server" CssClass="form-control" ID="txtURLDevCenter" Text='<%# Item.URLDevCenter %>' />
            </div>
            <div class="form-group">
                URL Process
				<asp:TextBox runat="server" CssClass="form-control" ID="txtURLProcess" Text='<%# Item.URLProcess %>' />
            </div>
            <div class="form-group">
                URL MerchantCP
				<asp:TextBox runat="server" CssClass="form-control" ID="txtURLMerchantCP" Text='<%# Item.URLMerchantCP %>' />
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                URL Wallet
				<asp:TextBox runat="server" CssClass="form-control" ID="txtURLWallet" Text='<%# Item.URLWallet %>' />
            </div>
            <div class="form-group">
                URL Website
				<asp:TextBox runat="server" CssClass="form-control" ID="txtURLWebsite" Text='<%# Item.URLWebsite %>' />
            </div>
        </div>
    </div>
</admin:FormSections>

<admin:FormPanel runat="server" ID="LogoFormPanel" Title="Upload Logo">
    <asp:MultiView runat="server" ActiveViewIndex='<%# Item != null && Item.IsAppIdentityHaveLogo ? 1 : 0 %>'>
        <%-- View if no file exist --%>
        <asp:View runat="server">
             <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                    <div class="input-group">
                        <div class="alert alert-info">Please upload logo with size 50 x 30 or bigger</div>
					    <NP:FileUpload runat="server" ID="fuDocument" />
				    </div>
                    </div>
                     <div class="form-group">
                        <asp:Button ID="btnUpload" OnClick="btnUpload_Click" Text="Upload File" CssClass="btn btn-info" runat="server" />
                    </div>
                </div>
            </div>
        </asp:View>

        <%-- View if file exist --%>
        <asp:View runat="server">
            <div class="row">
                <div class="col-md-12">
                    <asp:Literal runat="server" ID="litFileNotFound" Visible='<%# Item != null && !Item.IsAppIdentityHaveLogo %>' Text="File not found. Please delete and upload again."></asp:Literal>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <ul class="list-group">
                            <li class="list-group-item"> <i class="fa fa-picture-o fa-fw"></i> <asp:Literal runat="server" Text='<%# fiFile!=null? fiFile.Name : "" %>'/></li>
                            <li class="list-group-item"> <i class="fa fa-file-image-o fa-fw"></i> <asp:Literal runat="server" Text='<%# fiFile!=null? fiFile.Length + " bytes " +imgFile.Width + "px width x " + imgFile.Height + "px height" : "" %>' /></li>       
                             <asp:PlaceHolder runat="server" Visible='<%# (Item != null && Item.IsAppIdentityHaveLogo)  %>'>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-md-6">
                                        <% if (imgFile != null) imgFile.Dispose(); %>
                                        <img runat="server" class="img-thumbnail" src='<%# Item != null && Item.IsAppIdentityHaveLogo? Item.GetAppIdentityVirtualLogoPath + "?" + DateTime.Now.Millisecond.ToString() : "" %>' height='<%# (imgFile == null || imgFile.Height > 80)? 80 : imgFile.Height  %>' />
                                    </div>
                                    <div class="col-md-6 text-left">
                                       <asp:Linkbutton ID="btnDeleteFile" OnClientClick="return confirm('Are you sure?');" OnClick="btnDeleteFile_Click" CssClass="btn btn-danger" runat="server"><i class="fa fa-trash"></i> Delete File</asp:Linkbutton>
                                    </div>
                                </div>
                            </li>
                            </asp:PlaceHolder>
                        </ul>
                    </div>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>
</admin:FormPanel>

