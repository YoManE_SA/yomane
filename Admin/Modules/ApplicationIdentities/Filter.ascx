﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.ApplicationIdentities.Filter" %>
<admin:FilterSection runat="server" Title="Search">
   
    <div class="form-group">
        <ul class="list-group">
            <li class="list-group-item">
                <asp:CheckBox runat="server"  ID="chkActiveOnly" Text="Show only active" AutoPostBack="true" Checked="true" />
            </li>
        </ul>
    </div>

    <netpay:AppIdentityPicker runat="server" />
</admin:FilterSection>
