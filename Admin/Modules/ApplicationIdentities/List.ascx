﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.ApplicationIdentities.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Application Identities">
    <Header></Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" ID="rptList" SaveAjaxState="true" DataKeyNames="ID" AutoGenerateColumns="false" BubbleLoadEvent="true">
                <Columns>
                    <asp:BoundField HeaderText="ID" DataField="ID" SortExpression="ID" />
                    <asp:BoundField HeaderText="Name" DataField="Name" SortExpression="Name" />
                    <asp:BoundField HeaderText="Is Active" DataField="IsActive" SortExpression="IsActive" />
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd='<%#Netpay.Bll.ApplicationIdentity.SecuredObject.HasPermission(Netpay.Infrastructure.Security.PermissionValue.Add) %>' />
    </Footer>
</admin:ListSection>

