﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;
using Netpay.CommonTypes;


namespace Netpay.Admin.ApplicationIdentities
{
    public partial class Currencies :  Controls.TemplateControlBase
    {
        protected Bll.ApplicationIdentity Item { get { return TemplatePage.GetItemData<Bll.ApplicationIdentity>(); } set { TemplatePage.SetItemData(value); } }
        protected void Page_Load(object sender, EventArgs e)
        {
            TemplatePage.PageController.FormView.DataBound += FormView_DataBound1;
            TemplatePage.PageController.SaveItem += Save_Click;
        }

        protected override void DataBindChildren()
        {
            if (Item == null)
            {
                return;
            }

            cblCurrencies.DataSource = Bll.Currency.Cache;
            base.DataBindChildren();
        }
        private void FormView_DataBound1(object sender, EventArgs e)
        {
            foreach (var c in Item.SupportedCurrencies)
            {
                var li = cblCurrencies.Items.FindByValue(c);
                if (li != null) li.Selected = true;
            }
        }

        public bool IsAnyCurrencySelected =>
            cblCurrencies.Items.OfType<ListItem>().Any(x => x.Selected == true);
        

        protected void Save_Click(object sender, EventArgs e)
        {
            var curList = new List<string>();

            foreach (ListItem i in cblCurrencies.Items)
            if (i.Selected) curList.Add(i.Value);
            Item.SupportedCurrencies = curList;
            Item.Save();
        }
    }
}