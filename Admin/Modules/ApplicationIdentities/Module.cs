﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.ApplicationIdentities
{
	public class Module : Admin.CoreBasedModule
	{
		private ApplicationMenu mainMenuItem;

		public override string Name { get { return "ApplicationIdentity"; } }
		public override decimal Version { get { return 1.0m; } }
		public override string Author { get { return "OBL ltd."; } }
		public override string Description { get { return ""; } }

        public Module() : base(Bll.ApplicationIdentityModule.Current) { }

		protected override void OnInit(EventArgs e)
		{
            //Create menu item
            mainMenuItem = new ApplicationMenu("Application Identities", "~/ApplicationIdentities/0", null, 220);

            //Register route
            Application.RegisterRoute("ApplicationIdentities/{*id}", "ApplicationIdentity", typeof(Controls.DataTablePage), module:this);

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
		}

		protected override void OnActivate(EventArgs e)
		{
			base.OnActivate(e);
		}
		
		protected override void OnDeactivate(EventArgs e)
		{		
			base.OnDeactivate(e);
		}

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Partners", mainMenuItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            mainMenuItem.Parent.RemoveChild(mainMenuItem);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
		{
            if (!CoreModule.IsInstalled)
                return;

			var page = sender as Controls.DataTablePage;
			if (page.TemplateName == "ApplicationIdentity")
			{
                page.PageController.LoadItem += PageController_LoadItem;
                page.PageController.NewItem += PageController_NewItem;
                page.PageController.PostSaveItem += PageController_PostSaveItem;
                page.AddControlToList(page.LoadControl("~/Modules/ApplicationIdentities/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/ApplicationIdentities/Filter.ascx"));
                page.AddControlToForm("Data", page.LoadControl("~/Modules/ApplicationIdentities/Data.ascx"), this, "", Bll.ApplicationIdentity.SecuredObject);
                page.AddControlToForm("Currencies", page.LoadControl("~/Modules/ApplicationIdentities/Currencies.ascx"), this, "", Bll.ApplicationIdentity.SecuredObject);
                page.AddControlToForm("Payment Methods", page.LoadControl("~/Modules/ApplicationIdentities/PaymentMethods.ascx"), this, "", Bll.ApplicationIdentity.SecuredObject);
                page.AddControlToForm("Owned Payment Methods", page.LoadControl("~/Modules/ApplicationIdentities/References.ascx"), this, "", Bll.ApplicationIdentity.SecuredObject);
                page.AddControlToForm("Merchant Groups", page.LoadControl("~/Modules/ApplicationIdentities/merchantgroups.ascx"), this ,"", Bll.ApplicationIdentity.SecuredObject);
                page.AddControlToForm("Partners", page.LoadControl("~/Modules/ApplicationIdentities/affilates.ascx"), this , "", Bll.ApplicationIdentity.SecuredObject);
                page.AddControlToForm("Tokens", page.LoadControl("~/Modules/ApplicationIdentities/Tokens.ascx"), this, "", Bll.ApplicationIdentity.SecuredObject);

                page.PreRender += Page_PreRender;
            }
        }

        private void PageController_PostSaveItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            var item = page.GetItemData<Bll.ApplicationIdentity>();
            if (item != null)
            {
                // Check if it's a new item, if so,
                // update the DataKey with the new id of 
                // the created item and load it again
                // in order to update the form panel title
                // if the itemID is null, it means that a new item that was saved.
                if (page.ItemID == null) 
                {
                    page.PageController.DataKey = new System.Web.UI.WebControls.DataKey(new System.Collections.Specialized.OrderedDictionary() { { "ID", item.ID } });
                    PageController_LoadItem(sender, e);
                }
            }
        }

        private void PageController_NewItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            page.SetItemData(new Bll.ApplicationIdentity());
        }

        private void Page_PreRender(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterScript(page, "Plugins/Netpay-Plugin/AppIdSearchFilter.js");
        }

        void PageController_LoadItem(object sender, EventArgs e)
        {
            //declare the app identity variable
            Netpay.Bll.ApplicationIdentity currentItem = null;

            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            page.ItemID = page.PageController.DataKey["ID"].ToNullableInt();

            if (page.ItemID != null) // item selected from list
            {
                currentItem = Netpay.Bll.ApplicationIdentity.Load(page.ItemID.GetValueOrDefault());
                page.SetItemData(currentItem);
            }

            page.FormButtons.EnableDelete = false;
            page.FormButtons.EnableSave = Bll.ApplicationIdentity.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Edit);
            page.FormPanel.Title = currentItem != null ? string.Format("App Identity #{0} - {1}", currentItem.ID, currentItem.Name) : "---";
        }
    }
}