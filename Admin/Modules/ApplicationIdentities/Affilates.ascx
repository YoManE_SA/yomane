﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Affilates.ascx.cs" Inherits="Netpay.Admin.ApplicationIdentities.Affilates" %>
<admin:FormSections runat="server" Title="Affilates">
    <div class="alert alert-info">
        Affiliate which is associated with another app identity can't be
        selected, hover over the affiliate name to see the app identity name.
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
       <asp:Literal  Text="filter" ID="Literal4" runat="server"></asp:Literal> 
                 <asp:TextBox onkeyup="ApplicationIdentities_Filter('TextBox5','cblAffiliates','BodyContent_PageController_FormView_FormPanel_ctl05_');" runat="server" ID="TextBox5" ClientIDMode="Static" Width="60"  Height="18" />
                <br/>
                <br/>
                <asp:CheckBoxList  CssClass="chkList application-id-references-list" runat="server" ID="cblAffiliates" DataTextField="Name" DataValueField="ID" RepeatLayout="UnorderedList" />
            </div>
        </div>
    </div>
</admin:FormSections>