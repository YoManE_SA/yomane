﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Tokens.ascx.cs" Inherits="Netpay.Admin.ApplicationIdentities.Tokens" %>
<admin:FormSections runat="server" Title="External Application Tokens">
    <NP:UpdatePanel runat="server" ID="upListTokens" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        Add new token with name
			            <asp:TextBox runat="server" ID="newTokenName" CssClass="form-control" />
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        GUID (can leave empty or auto generate)
                        <asp:TextBox runat="server" ID="newToken" CssClass="form-control" />
                    </div>
                </div>
                <div class="col-md-2">
                    <br />
                    <asp:LinkButton runat="server" CssClass="btn btn-primary" OnClick="AddToken_Click"><i class="fa fa-plus-circle"></i> Add</asp:LinkButton>
                </div>
                <div class="col-md-12">
                    <hr />
                </div>
            </div>

            <div class="col-md-12">
                <div class="row inner-table-titles">
                    <div class="col-md-4">
                        Title
                    </div>
                    <div class="col-md-6">
                        Token
                    </div>
                    <div class="col-md-2">
                        Delete
                    </div>
                </div>
            </div>
            <asp:Repeater runat="server" ID="rptTokens">
                <ItemTemplate>
                    <div class="col-md-12">
                        <div class="row table-ui">


                            <div class="col-md-4"><%# Eval("Value") %></div>
                            <div class="col-md-6"><%# Eval("Key") %></div>
                            <div class="col-md-2">
                                <asp:LinkButton runat="server" CommandArgument='<%# Eval("Key") %>' OnCommand="RemoveToken_Click" CssClass="btn btn-danger"> <i class="fa fa-trash"></i> Remove</asp:LinkButton>
                            </div>
                        </div>
                    </div>

                </ItemTemplate>
            </asp:Repeater>
        </ContentTemplate>
    </NP:UpdatePanel>
</admin:FormSections>
