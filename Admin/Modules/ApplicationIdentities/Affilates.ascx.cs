﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;
using Netpay.CommonTypes;


namespace Netpay.Admin.ApplicationIdentities
{
    public partial class Affilates : Controls.TemplateControlBase
    {
        protected Bll.ApplicationIdentity Item { get { return TemplatePage.GetItemData<Bll.ApplicationIdentity>(); } set { TemplatePage.SetItemData(value); } }
        protected void Page_Load(object sender, EventArgs e)
        {
            TemplatePage.PageController.FormView.DataBound += FormView_DataBound4;
            TemplatePage.PageController.SaveItem += Save_Click;
        }
        protected override void DataBindChildren()
        {
            var affiliates = Netpay.Bll.Affiliates.Affiliate.GetAffiliatesList();
            affiliates = affiliates.Where(i => !string.IsNullOrEmpty(i.Name)).ToList();

            cblAffiliates.DataSource = affiliates;
            if (Item == null) Item = new Netpay.Bll.ApplicationIdentity();
            base.DataBindChildren();
        }
        private void FormView_DataBound4(object sender, EventArgs e)
        {
            //Check the affiliates that are issued with the current app identity,
            foreach (var c in Item.Affiliates)
            {
                var li = cblAffiliates.Items.FindByValue(((int)c).ToString());
                if (li != null) li.Selected = true;
            }

            //Check if the affiliate is issued with other app identity 
            //Then only if it doesn't issued with the currenct app identity,
            //Make it disabled. Beacause if it does , give the option to un select it
            foreach (ListItem af in cblAffiliates.Items)
            {
                if (!af.Selected)
                {
                    if (Bll.ApplicationIdentity.IsAffiliateIssuedWithOtherAppIdentity(int.Parse(af.Value), Item.ID))
                    {
                        af.Enabled = false;
                        List<string> appsNames = Bll.ApplicationIdentity.LoadForAffiliate(int.Parse(af.Value)).Select(x => x.Name).ToList();
                        string names = string.Join(",", appsNames);
                        af.Attributes["title"] = string.Format("Affiliate issued with '{0}'", names);
                    }
                }
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            var pmList = new List<int>();
            foreach (ListItem i in cblAffiliates.Items)
                if (i.Selected) pmList.Add(i.Value.ToNullableInt().GetValueOrDefault());

            //Check if any of the selected affiliates are issued with another app identity, 
            //If so , throw exception that says which affiliates with which app identity.

            //Create dictionary that will map between the affiliate id and the app identity he is issued with.
            Dictionary<int, List<int>> affiliatesIssuedWithOtherAppIdentities = new Dictionary<int, List<int>>();
            
            //Check if any of the selected affiliates is issued with another app identity,
            //If so, add it to the dictionary.  
            foreach (int affiliateId in pmList)
            {
                if (Bll.ApplicationIdentity.IsAffiliateIssuedWithOtherAppIdentity(affiliateId, Item.ID))
                {
                    affiliatesIssuedWithOtherAppIdentities.Add(affiliateId, Bll.ApplicationIdentity.GetAffiliateAppIdentitiesIdsList(affiliateId));
                }
            }

            
            if (affiliatesIssuedWithOtherAppIdentities.Count > 0)
            {
                System.Text.StringBuilder errorMessage = new System.Text.StringBuilder();
                errorMessage.AppendFormat("Affiliate can be issued only with one app identity,<br>There are affiliates issued with other app identities.<br>");

                foreach (var aff in affiliatesIssuedWithOtherAppIdentities)
                {
                    errorMessage.AppendFormat("Affiliate:{0} issued with AppIdentites:{1}<br>", Bll.Affiliates.Affiliate.GetAffiliatesList().Where(x => x.ID ==aff.Key).Single().Name, Bll.ApplicationIdentity.GetAppIdentityNames(aff.Value));
                }

                throw new Exception(errorMessage.ToString());
            }

            Item.Affiliates = pmList;
            Item.Save();
        }
    }
}