﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentMethods.ascx.cs" Inherits="Netpay.Admin.ApplicationIdentities.PaymentMethods" %>
<admin:FormSections runat="server" Title="Payment Methods">
    
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">

                <asp:Literal  Text="filter" ID="Literal1" runat="server"></asp:Literal> 
                 <asp:TextBox onkeyup="ApplicationIdentities_Filter('TextBox2','cblPaymentMethods','BodyContent_PageController_FormView_FormPanel_ctl02_');" runat="server" ID="TextBox2" ClientIDMode="Static" Width="60"  Height="18" />
                <br/>
                <br/>
           
                <asp:CheckBoxList CssClass="chkList application-id-references-list" runat="server" ID="cblPaymentMethods" DataTextField="Name" DataValueField="ID" RepeatLayout="UnorderedList"  />
            </div>
        </div>
    </div>
</admin:FormSections>