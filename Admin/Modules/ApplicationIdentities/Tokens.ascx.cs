﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.ApplicationIdentities
{
    public partial class Tokens : Controls.TemplateControlBase
    {
        protected Bll.ApplicationIdentity Item { get { return TemplatePage.GetItemData<Bll.ApplicationIdentity>(); } set { TemplatePage.SetItemData(value); } }
        protected override void DataBindChildren()
        {
            if (Item == null)
            {
                return;
            }

            rptTokens.DataSource = Item.GetTokens();
            base.DataBindChildren();
        }

        protected void AddToken_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(newTokenName.Text)) return;
            TemplatePage.PageController.LoadActiveItem();
            if (Item == null) return;

            //Check if token exist with the same app identity
            if (!string.IsNullOrEmpty(newToken.Text))
            {
                Guid tokenToCheck = new Guid(newToken.Text);
                bool isTokenExistWithSameIdentity = Bll.ApplicationIdentity.IsTokenExistWithSameAppIdentity(Item.ID, tokenToCheck);

                if (isTokenExistWithSameIdentity)
                {
                    TemplatePage.FormViewGlobalModalDialog.Title = "Error";
                    TemplatePage.FormViewGlobalActionNotify.SetMessage("Token already exist in the current app identity.", true);
                    TemplatePage.FormViewGlobalModalDialog.RegisterShow();
                                       
                    return;
                }
            }

            Item.AddToken(newTokenName.Text,newToken.Text.ToNullableGuid());
            rptTokens.DataSource = Item.GetTokens();
            rptTokens.DataBind();
            upListTokens.Update();
        }

        protected void RemoveToken_Click(object sender, CommandEventArgs e)
        {
            TemplatePage.PageController.LoadActiveItem();
            if (Item == null) return;
            Item.RemoveToken(e.CommandArgument.ToNullableGuid().GetValueOrDefault());
            rptTokens.DataSource = Item.GetTokens();
            rptTokens.DataBind();
            upListTokens.Update();
        }
    }
}