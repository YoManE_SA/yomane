﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;
using System.Drawing;

namespace Netpay.Admin.ApplicationIdentities
{
    public partial class Data : Controls.TemplateControlBase
    {
        protected Bll.ApplicationIdentity Item { get { return TemplatePage.GetItemData<Bll.ApplicationIdentity>(); } set { TemplatePage.SetItemData(value); } }
        public System.IO.FileInfo fiFile { get; set; }
        public System.Drawing.Bitmap imgFile { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += Save_Click;
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            if (Item == null)
            {
                return;
            }

            if (Item != null && Item.IsAppIdentityHaveLogo)
            {
                imgFile = new System.Drawing.Bitmap(Item.GetAppIdentityLogoPath);
                fiFile = new System.IO.FileInfo(Item.GetAppIdentityLogoPath);
            }

            base.DataBindChildren();
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            if (Item == null) Item = new Netpay.Bll.ApplicationIdentity();

            // get reference to currencies tab, save operation will not invoke
            // if not currency is selected
            // consider that currencies tab have only one form section inside it
            var currenciesTab = TemplatePage.GetTabViewControlById("Currencies");
            if (currenciesTab == null) throw new Exception("app identity currencies tab not found.");
            if (!currenciesTab.Controls.OfType<Currencies>().SingleOrDefault().IsAnyCurrencySelected)
            {
                //Throw exception in order to stop execution of the "SaveItem" event.
                throw new Exception("You must select at least one currency in order to save.");
            }

            Item.Name = txtName.Text;
            Item.BrandName = txtBrandName.Text;
            Item.CompanyName = txtCompanyName.Text;
            Item.DomainName = txtDomainName.Text;
            //Item.ThemeName = txtThemeName.Text;
            Item.ThemeName = ddlParentCompany.SelectedItem.Text;
            Item.ContentFolder = txtContentFolder.Text;
            Item.URLDevCenter = txtURLDevCenter.Text;
            Item.URLProcess = txtURLProcess.Text;
            Item.URLMerchantCP = txtURLMerchantCP.Text;
            Item.URLWallet = txtURLWallet.Text;
            Item.URLWebsite = txtURLWebsite.Text;
            Item.SmtpServer = txtSmtpServer.Text;
            Item.SmtpUsername = txtSmtpUsername.Text;
            Item.SmtpPassword = txtSmtpPassword.Text;
            Item.EmailFrom = txtEmailFrom.Text;
            Item.EmailContactTo = txtEmailContactTo.Text;
            Item.CopyRightText = txtCopyRightText.Text;
            Item.IsActive = chkIsActive.Checked;
            Item.ProcessMerchantNumber = processnumber.Value;
            Item.WireAccountID = ddlWireAcountId.Value.ToNullableInt();
            Item.Save();

            ltID.Text = Item.ID.ToString();
        }

        protected void EnableHash_CheckedChanged(object sender, EventArgs e)
        {
            TemplatePage.PageController.LoadActiveItem();
            Item.EnableTokenHash = chkEnableHash.Checked;
            Item.Save();
            lblHashKey.Text = Item.HashKey;

            //Update FormView.
            if (System.Web.UI.ScriptManager.GetCurrent(Page).IsInAsyncPostBack)
                Admin.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQueryStartUp(TemplatePage.PageController.FormView, "CreateTwoColumnsIfNeeded('" + TemplatePage.PageController.ClientID + "');");
            TemplatePage.PageController.FormView.Update();
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            TemplatePage.PageController.LoadActiveItem();
            if (Item == null) return;

            //If no file chosen, return;
            if (!fuDocument.HasFile) return;

            var sExt = System.IO.Path.GetExtension(fuDocument.FileName).ToLower();
            if (".gif;.jpg;.jpe;.jpeg;.png".Contains(sExt))
            {
                string fileName = Item.ID.ToString() + ".png";

                //Check if directory exist, if not create
                if (!System.IO.Directory.Exists(Item.PublicDirectoryPath))
                {
                    System.IO.Directory.CreateDirectory(Item.PublicDirectoryPath);
                }

                //Get path
                string fullFileName = System.IO.Path.Combine(Item.PublicDirectoryPath, fileName);

                //Create image from file stream
                Bitmap pic = new System.Drawing.Bitmap(fuDocument.FileStream);

                //Change size
                pic = new Bitmap(pic, new Size(50, 30));

                //Save
                pic.Save(fullFileName, System.Drawing.Imaging.ImageFormat.Png);

                //Refresh page
                DataBindChildren();
                LogoFormPanel.BindAndUpdate();
            }
        }

        protected void btnDeleteFile_Click(object sender, EventArgs e)
        {
            TemplatePage.PageController.LoadActiveItem();
            if (Item == null) return;

            if (System.IO.File.Exists(Item.GetAppIdentityLogoPath))
            {
                System.IO.File.Delete(Item.GetAppIdentityLogoPath);
            }
            fuDocument.DeleteTemporaryFile();

            LogoFormPanel.BindAndUpdate();
        }
    }
}