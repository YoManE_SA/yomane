﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.Admin.ApplicationIdentities
{
    public partial class PaymentMethods : Controls.TemplateControlBase
    {
        protected Bll.ApplicationIdentity Item { get { return TemplatePage.GetItemData<Bll.ApplicationIdentity>(); } set { TemplatePage.SetItemData(value); } }
        protected void Page_Load(object sender, EventArgs e)
        {
            TemplatePage.PageController.FormView.DataBound += FormView_DataBound2;
            TemplatePage.PageController.SaveItem += Save_Click;
        }

        protected override void DataBindChildren()
        {
            if (Item == null)
            {
                return;
            }

            cblPaymentMethods.DataSource = Bll.PaymentMethods.PaymentMethod.Search(null, null);
            base.DataBindChildren();
        }
        private void FormView_DataBound2(object sender, EventArgs e)
        {
            foreach (var c in Item.SupportedPaymentMethods)
            {
                var li = cblPaymentMethods.Items.FindByValue(((int)c).ToString());
                if (li != null) li.Selected = true;
            }
        }
        protected void Save_Click(object sender, EventArgs e)
        {
            var pmList = new List<int>();
            foreach (ListItem i in cblPaymentMethods.Items)
                if (i.Selected) pmList.Add(i.Value.ToNullableInt().GetValueOrDefault());
            Item.SupportedPaymentMethods = pmList;
            Item.Save();
        }

    }
}