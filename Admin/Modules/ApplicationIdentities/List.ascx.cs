﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;

namespace Netpay.Admin.ApplicationIdentities
{
    public partial class List : Controls.TemplateControlBase
    {

        protected override void OnLoad(EventArgs e)
        {
            rptList.DataBinding += List_DataBinding;
            base.OnLoad(e);
        }

        private void List_DataBinding(object sender, EventArgs e)
        {
            Bll.ApplicationIdentity.SearchFilters filters = TemplatePage.GetFilter<Bll.ApplicationIdentity.SearchFilters>();
            if (filters == null)
            {
                filters = new Netpay.Bll.ApplicationIdentity.SearchFilters();
                filters.IsActive = true;
            }
            var list = Netpay.Bll.ApplicationIdentity.Search(filters, null);
            rptList.DataSource = list;
        }
    }
}
