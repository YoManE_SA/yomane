﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MerchantGroups.ascx.cs" Inherits="Netpay.Admin.ApplicationIdentities.MerchantGroups" %>
<admin:FormSections runat="server" Title="Merchant Groups">
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                
              <asp:Literal  Text="filter" ID="Literal3" runat="server"></asp:Literal> 
                 <asp:TextBox onkeyup="ApplicationIdentities_Filter('TextBox4','cblMerchantGroups','BodyContent_PageController_FormView_FormPanel_ctl04_');" runat="server" ID="TextBox4" ClientIDMode="Static" Width="60"  Height="18" />
                <br/>
                <br/>
                <asp:CheckBoxList  CssClass="chkList application-id-references-list" runat="server" ID="cblMerchantGroups" DataTextField="Name" DataValueField="ID" RepeatLayout="UnorderedList" />
            </div>
        </div>
    </div>
</admin:FormSections>