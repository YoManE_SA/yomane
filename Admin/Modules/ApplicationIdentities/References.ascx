﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="References.ascx.cs" Inherits="Netpay.Admin.ApplicationIdentities.References" %>

<admin:FormSections runat="server" Title="Owned Payment Methods">
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">

                 <asp:Literal  Text="filter" ID="Literal2" runat="server"></asp:Literal> 
                 <asp:TextBox onkeyup="ApplicationIdentities_Filter('TextBoxReferencesFilter','cblOwnPaymentMethods','BodyContent_PageController_FormView_FormPanel_ctl03_');" runat="server" ID="TextBoxReferencesFilter" ClientIDMode="Static" Width="60"  Height="18" />
                <br/>
                <br/>
                <asp:CheckBoxList  CssClass="chkList application-id-references-list" runat="server" ID="cblOwnPaymentMethods" DataTextField="Name" DataValueField="ID" RepeatLayout="UnorderedList" />
            </div>
        </div>
    </div>
</admin:FormSections>

