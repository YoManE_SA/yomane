﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Currencies.ascx.cs" Inherits="Netpay.Admin.ApplicationIdentities.Currencies" %>
<%-- Should have only one form section here, 
     If there is a need to add a form section, 
     Should update data tab code behind Save_Click
--%>
<admin:FormSections runat="server" Title="Currencies">
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">

                <asp:Literal Text="filter" ID="Literal5" runat="server"></asp:Literal>
                <asp:TextBox onkeyup="ApplicationIdentities_Filter('TextBoxCurrenciesFilter','cblCurrencies','BodyContent_PageController_FormView_FormPanel_ctl01_');" runat="server" ID="TextBoxCurrenciesFilter" ClientIDMode="Static" Width="60" Height="18" />
                <br />
                <br />
                <asp:CheckBoxList CssClass="chkList application-id-references-list" runat="server" ID="cblCurrencies" DataTextField="Name" DataValueField="IsoCode" RepeatLayout="UnorderedList" />
            </div>
        </div>
    </div>
</admin:FormSections>
