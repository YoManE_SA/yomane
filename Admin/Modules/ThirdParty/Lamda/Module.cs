﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Modules.ThirdParty.Lamda
{
    public class Module : CoreBasedModule
    {
        public Module() : base(Bll.ThirdParty.Lamda.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //No menu

            //No route

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {            
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {            
            base.OnDeactivate(e);
        }
                        
        protected void Application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "ApplicationIdentity") {
                page.AddControlToForm("Lamda", page.LoadControl("~/Modules/ThirdParty/Lamda/AppIdentityTab.ascx"), this, "", Bll.ApplicationIdentity.SecuredObject);
            }
        }
    }
}