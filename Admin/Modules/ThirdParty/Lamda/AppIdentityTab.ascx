﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AppIdentityTab.ascx.cs" Inherits="Netpay.Admin.Modules.ThirdParty.Lamda.AppIdentityTab" %>
<admin:FormSections runat="server" Title="Account Info">
    <div class="row">
        <div class="col-md-12">
            <asp:Label runat="server" ID="lblRequestError" EnableViewState="false" />
        </div>
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-body">
                   <b>Current Balance: </b> 
                    <asp:Label runat="server" Text='<%# MerchantBalance.Current  %>' />
                    <asp:Label runat="server" Text='<%# MerchantBalance.CurrencyIso  %>' />
                </div>
            </div>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Login Details">
    <div class="row">
        <div class="col-lg-4">
            <div class="form-group">
                User Name
			
                <asp:TextBox runat="server" CssClass="form-control" ID="txtUserName" Text='<%# ServiceLogin.Username %>' />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                User ID
			
                <asp:TextBox runat="server" CssClass="form-control" ID="txtReference1" Text='<%# ServiceLogin.Reference1  %>' />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                Password
			
                <asp:TextBox runat="server" CssClass="form-control" ID="txtPassword" Text='<%# ServiceLogin.Password  %>' />
            </div>
        </div>
    </div>

</admin:FormSections>
