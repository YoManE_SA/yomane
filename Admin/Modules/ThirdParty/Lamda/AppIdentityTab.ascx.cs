﻿using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Modules.ThirdParty.Lamda
{
    public partial class AppIdentityTab : Admin.Controls.TemplateControlBase
    {
        protected Bll.Accounts.Balance.BalanceStatus MerchantBalance;
        protected Bll.Accounts.ExternalServiceLogin ServiceLogin;

        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += Save;
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            var identity = TemplatePage.GetItemData<Bll.ApplicationIdentity>();
            if (identity != null) { 
                var merchant = Bll.Accounts.Account.LoadAccount(Bll.Accounts.AccountType.Merchant, identity.ProcessMerchantNumber);
                if (merchant != null && merchant.MerchantID.HasValue) {
                    ServiceLogin = Bll.Accounts.ExternalServiceLogin.LoadForAccount(merchant.AccountID, Bll.ThirdParty.Lamda.PM_ProviderName);
                    if (ServiceLogin  != null) { 
                        lblRequestError.Text = "";
                        try { MerchantBalance = Bll.ThirdParty.Lamda.Current.GetMerchantBalance(merchant.MerchantID.Value); }
                        catch (Exception ex) { lblRequestError.Text = "Error: " + ex.Message; }
                    }
                }
            }
            if (MerchantBalance == null) MerchantBalance =  new Bll.Accounts.Balance.BalanceStatus();
            if (ServiceLogin == null) ServiceLogin = new Bll.Accounts.ExternalServiceLogin(0, "");
            base.DataBindChildren();
        }

        private void Save(object sender, EventArgs e)
        {
            if (Netpay.Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.ApplicationIdentity.SecuredObject, PermissionValue.Edit);

            var identity = TemplatePage.GetItemData<Bll.ApplicationIdentity>();
            if (identity == null) return;
            var merchant = Bll.Accounts.Account.LoadAccount(Bll.Accounts.AccountType.Merchant, identity.ProcessMerchantNumber);
            if (merchant == null) return;
            ServiceLogin = Bll.Accounts.ExternalServiceLogin.LoadForAccount(merchant.AccountID, Bll.ThirdParty.Lamda.PM_ProviderName);
            if (ServiceLogin == null) {
                if (string.IsNullOrEmpty(txtUserName.Text)) return;
                ServiceLogin = new Bll.Accounts.ExternalServiceLogin(merchant.AccountID, Bll.ThirdParty.Lamda.PM_ProviderName);
                ServiceLogin.ServerURL = "https://test.lamdacardservices.com/";
                ServiceLogin.Method = Bll.Accounts.ExternalServiceLogin.Protocols.HTTPS;
                ServiceLogin.IsActive = true;
            }
            ServiceLogin.Username = txtUserName.Text;
            ServiceLogin.Reference1 = txtReference1.Text;
            ServiceLogin.Password = txtPassword.Text;
            ServiceLogin.Save();
        }
    }
}