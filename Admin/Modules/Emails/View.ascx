﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="Netpay.Admin.Emails.View" %>
<asp:HiddenField runat="server" ID="hfID" Value='<%# ItemData.ID %>' />


<admin:FormSections ID="dlgItemMessage" runat="server" Icon="fa fa-envelope" Title="Email Message">

    <NP:UpdatePanel runat="server" ID="upMessage" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row">

                <div class=" tab-panel">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <b>Subject :  </b><%# ItemData.Subject %>
                                </div>
                            </div>
                            <div class="col-lg-6 text-right">
                                <asp:HyperLink
                                    onclick='<%#"OpenTabByClientId(" + "\"" + GetComposeTabClientId() + "\"" + ")" %>'
                                    runat="server"
                                    CssClass="btn btn-default">
                        <i class="fa fa-reply "></i> Reply</asp:HyperLink>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6">
                                    <b>From : </b>
                                    <i><%# ItemData.AdmincashUser %></i><i><%# ItemData.EmailFrom %></i>
                                </div>
                                <div class="col-lg-6 text-right">
                                    <i><%# ItemData.Date %></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6">
                                    <b>To :</b> <i><%# ItemData.EmailTo %></i>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="email-content tab-panel">
                            <asp:Literal runat="server" Text='<%# ItemData.MimeMessage!=null ? ItemData.MimeMessage.HTMLText : ItemData.Text %>' Mode="PassThrough" />
                            <%--<asp:Literal runat="server" Text='<%# ItemData !=null ? ItemData.Text : "" %>' Mode="PassThrough" />--%>
                        </div>

                        <asp:PlaceHolder runat="server" Visible='<%# ItemData.MimeMessage != null && ItemData.MimeMessage.Attachments.Count > 0 %>'>
                            <div>
                                Attachment:  
	        			            <asp:Repeater ID="rptAttachments" runat="server" DataSource='<%# ItemData.MimeMessage !=null ? ItemData.MimeMessage.Attachments:null %>'>
                                        <ItemTemplate>
                                            <%--  <asp:LinkButton runat="server" Text='<%# Eval("FileName") %>' />--%>
                                            <asp:LinkButton ID="btnDownload" runat="server" OnCommand="DownloadAttacment" CommandArgument='<%# Container.ItemIndex %>' Text='<%# Eval("FileName") %>' CausesValidation="false" />
                                        </ItemTemplate>
                                        <SeparatorTemplate>, </SeparatorTemplate>
                                    </asp:Repeater>
                            </div>
                        </asp:PlaceHolder>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </NP:UpdatePanel>
</admin:FormSections>

<admin:FormSections runat="server" Icon="fa fa-envelope" Title="Conversation History">
    <header></header>
    <body>
        <div class="table-responsive">
            <admin:AdminList IsInListView="false" SaveAjaxState="true" runat="server" DataKeyNames="ID" ID="rptListConversationHistory" ShowHeader="true" AutoGenerateColumns="false" BubbleLoadEvent="false">
                <Columns>
                    <asp:BoundField HeaderText="Date" DataField="Date" SortExpression="Date" />
                    <asp:BoundField HeaderText="Subject" DataField="Subject" SortExpression="Subject" />
                    <asp:BoundField HeaderText="Replied/Sent By" DataField="AdmincashUser" SortExpression="AdmincashUser" />
                </Columns>
            </admin:AdminList>
        </div>
    </body>
</admin:FormSections>

<admin:FormSections runat="server" Icon="fa fa-envelope" Title="Status History">

    <asp:Repeater runat="server" DataSource='<%# ItemData.StatusLog %>'>
        <ItemTemplate>
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-6">
                        <i class="fa fa-calendar"></i><i><%# Eval("Date") %></i>

                        <i><%# Eval("StatusOldText") %></i>, <i><span style="color: #FF3D3D"><%# Eval("StatusNewText") %></span></i>
                    </div>
                    <div class="col-lg-6 text-right  text-uppercase">
                        <i class="fa fa-user"></i><%# Eval("UserName") %>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <i><%# Eval("Message") %></i>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <hr />

    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <asp:Label ID="lblAccount" runat="server" Text="Assign To  :  " />
                <admin:AccountPicker runat="server" ID="apAccount" LimitToType="Merchant" PlaceHolder="select Merchant" Text=" <%#GetMerchantName(ItemData.MerchantID) %>" />
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <asp:MultiView runat="server" ActiveViewIndex='<%#ItemData.IsSent? 1 : 0 %>'>
                    <asp:View runat="server">
                        Change status    
                        <netpay:MessageStatusDropDown runat="server" ID="ChangeStatus1" EnableBlankSelection="false" CssClass="form-control" >
                        </netpay:MessageStatusDropDown>
                    </asp:View>
                    <asp:View runat="server">
                        <br />
                        Status : Sent
                    </asp:View>
                </asp:MultiView>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                Comment 
        <asp:TextBox runat="server" TextMode="MultiLine" CssClass="form-control" ID="Comment" />
            </div>
        </div>
    </div>
</admin:FormSections>
