﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Form.ascx.cs" Inherits="Netpay.Admin.Emails.Form" %>
<script type="text/javascript">
    function SelectAddAddress(s, tt, tv, v) {
        var val = $(tv).val();

        if ($(s).text().toLowerCase().indexOf("other") >= 0) {
            $(tv).val("");
        }
        else {
            if (val != '' && val[val.length - 1] != ';') val += '; ';
            $(tv).val(val + v);
        }
    }
    function SelectSetAddress(s, tt, tv, v) {
        $(tt).val(v);
        $(tv).val($(s).text());
    }
</script>

<admin:FormSections ID="dlgComposeMessage" runat="server" Icon="fa fa-envelope">

    <asp:UpdatePanel ID="UpSendMessage" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>
            <netpay:ActionNotify runat="server" ID="SendMessageNotify" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:HiddenField runat="server" ID="hfID" Value='<%# ItemData.ID%>' />
    <asp:HiddenField runat="server" ID="hfLogStatusId" Value='<%# ItemDataLogStatus.ID%>' />
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <admin:MultiSearch runat="server" CssClass="btn btn-default" ID="rblFrom" SelectWidth="80px" TypePlaceHolder="From" OnClientSelect="SelectSetAddress" ReadOnlyText="true" />
            </div>
            <div class="form-group">
                <admin:MultiSearch CssClass="btn btn-default" runat="server" ID="rblTo" SelectWidth="80px" TypePlaceHolder="To" OnClientSelect="SelectAddAddress" />
            </div>
            <div class="form-group">
                <admin:MultiSearch CssClass="btn btn-default" runat="server" ID="rblCC" SelectWidth="80px" TypePlaceHolder="CC" OnClientSelect="SelectAddAddress" />
            </div>
            <div class="form-group">
                <admin:MultiSearch CssClass="btn btn-default" runat="server" ID="rblSubject" SelectWidth="80px" TypePlaceHolder="Subject" OnClientSelect="void" />
            </div>
            <div class="form-group">
                <JQ:HtmlEdit ID="RadEditor1" runat="server" Width="100%" Height="130" Text='<%#ItemData.MimeMessage !=null ? ItemData.MimeMessage.HTMLText : ""  %>' />
            </div>
            <NP:UpdatePanel ID="upAttachments" runat="server" UpdateMode="Conditional" RenderMode="Block" ChildrenAsTriggers="false">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <NP:FileUpload runat="server" ID="fuDocument" IsDownloadButtonVisible="false" />
                                    <div class="text-right" style="cursor: pointer;">
                                        <asp:LinkButton runat="server" ID="btnUpload" OnClick="Upload_Click" Style="color: #333; text-decoration: none;" CssClass="text-right">Add New File <i class="fa fa-plus-circle"></i></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                <netpay:DynamicRepeater runat="server" ID="rptAttachments" DataSource='<%# ItemData.MimeMessage !=null ?ItemData.MimeMessage.Attachments:null %>'>
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="FileName" Text='<%# Eval("FileName") %>' CssClass="muted"></asp:Label>
                                        <asp:HiddenField runat="server" ID="LocalPath" Value='<%# Eval("LocalPath") != null ? Eval("LocalPath") : "" %>' />
                                        <%--<span class="muted"><i><%# Eval("FileName") %></i></span>--%>
                                        <i class="fa fa-check-circle" style="color: #1EA177;"></i>
                                        <asp:LinkButton runat="server" CommandName="RemoveAttachment" OnCommand="RemoveAttachment" OnClientClick="confirm('Are you sure?');">
											<i class="fa fa-times-circle" style="color: #ff3d3d; cursor: pointer;"></i>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </netpay:DynamicRepeater>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnUpload" />
                </Triggers>
            </NP:UpdatePanel>
        </div>
    </div>
    <asp:Button runat="server" Text="Send" CssClass="btn btn-primary btn-cons-short" OnClick="Send_Click" />
</admin:FormSections>




