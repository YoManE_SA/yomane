﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Emails
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu mainMenuItem;

        public override string Name { get { return "Emails"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return ""; } }

        public Module() : base(Netpay.Emails.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu item
            mainMenuItem = new ApplicationMenu("Inbox", "~/Emails/0", "fa-envelope", 5);

            //Register route
            Application.RegisterRoute("Emails/{id}", "Email", typeof(Controls.DataTablePage), module:this);

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {			
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {         
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.UserMenu.AddChild(mainMenuItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            Application.UserMenu.RemoveChild(mainMenuItem);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "Email")
            {
				page.PageController.LoadItem += PageController_LoadItem;
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Emails/Filter.ascx"));
                page.AddControlToList(page.LoadControl("~/Modules/Emails/List.ascx"));
                page.AddControlToForm("Message", page.LoadControl("~/Modules/Emails/View.ascx"), this, "", Netpay.Emails.Message.SecuredObject);
                page.AddControlToForm("Compose", page.LoadControl("~/Modules/Emails/Form.ascx"), this, "", Netpay.Emails.Message.SecuredObject);
            }
        }

		private void PageController_LoadItem(object sender, EventArgs e)
		{
			var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
			page.ItemID = page.PageController.DataKey["ID"].ToNullableInt();
			if (page.ItemID != null) page.SetItemData(Netpay.Emails.Message.Load(page.ItemID.GetValueOrDefault()));
			if (page.GetItemData<Netpay.Emails.Message>() == null) page.SetItemData(new Netpay.Emails.Message());
		}
    }
}