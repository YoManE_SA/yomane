﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using System.Collections;
using Netpay.Emails;

namespace Netpay.Admin.Emails
{
    public partial class List : Controls.TemplateControlBase
    {
        protected Dictionary<int, string> m_merchantNames;
        protected override void OnLoad(EventArgs e)
		{
			rptList.DataBinding += List_DataBinding;
            rptList.SelectedIndexChanged += RptList_SelectedIndexChanged;
			base.OnLoad(e);
		}

        private void RptList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Set the selected index of Conversation History list on "Message" tab to -1
            //If not, That list saves the last selected index and it causes bug.

            //Check if "message" tab exist
            if (!TemplatePage.FormTabs.Tabs.Any(x => x.Title.ToLower() == "message")) throw new Exception("\"Message\" tab not exist on Emails form section.");
            
            //Get the inner "View" control inside Message tab.
            var messageTabViewControl = TemplatePage.FormTabs.Tabs.Where(x => x.Title.ToLower() == "message").SingleOrDefault().Controls.OfType<View>().SingleOrDefault();

            messageTabViewControl.ConversationHistoryAsAdminList.SelectedIndex = -1;
        }

        private void List_DataBinding(object sender, EventArgs e)
        {

            var list = Netpay.Emails.Message.Search(TemplatePage.GetFilter<Netpay.Emails.Message.SearchFilters>(), rptList);
            if (list != null)
            {
               //list=RemoveSearchDuplicates(list);
                m_merchantNames = Bll.Accounts.Account.GetMerchantNames((list.Where(i => i.MerchantID != null).Select(i => i.MerchantID.Value).ToList()));
                rptList.DataSource = list;
            }     
            
        }

        private List<Message> RemoveSearchDuplicates(List<Message> SearchResults)
        {
            List<Message> TempList = new List<Message>();

            foreach (Message  u1 in SearchResults)
            {
                bool duplicatefound = false;
                foreach (Message u2 in TempList)
                    if (u1.ThreadID == u2.ThreadID || (u1.EmailFrom==u2.EmailFrom && u1.EmailTo == u2.EmailTo) )
                        duplicatefound = true;

                if (!duplicatefound)
                    TempList.Add(u1);
            }
            return TempList;
        }

        protected string GetMerchantName(int? id)
        {
            if (id == null) return null;
            string ret;
             if (!m_merchantNames.TryGetValue((int)id, out ret)) return null;
            return   ret;
        }

        protected string GetStatusName(int? id)
        {
            if (id == null) return null;
            return Netpay.Emails.MessageStatus.Load((int)id).Name;

        }
    }
}


