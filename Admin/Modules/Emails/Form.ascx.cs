﻿using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Emails
{
    public partial class Form : Controls.TemplateControlBase
    {

        public Netpay.Emails.Message ItemData;
        protected Netpay.Emails.LogStatus ItemDataLogStatus;


        protected override void OnPreRender(EventArgs e)
        {
            Admin.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterScript(Page, "Plugins/Netpay-Plugin/UploadFile.js");
            DataBindChildren();
            base.OnPreRender(e);
        }
        protected override void DataBindChildren()
        {
            ItemData = TemplatePage.GetItemData<Netpay.Emails.Message>();
            if (ItemData == null) ItemData = new Netpay.Emails.Message();

            ItemDataLogStatus = TemplatePage.GetItemData<Netpay.Emails.LogStatus>();
            if (ItemDataLogStatus == null) { ItemDataLogStatus = new Netpay.Emails.LogStatus(); }

            ItemDataLogStatus.Date = DateTime.Now;
            ItemDataLogStatus.UserName = Web.WebUtils.LoggedUser.UserName;
            ItemDataLogStatus.MessageId = ItemData.ID;

            if (ItemData.EmailFrom != null)
            {
                rblSubject.DataSource = Netpay.Emails.Templates.GetTemplates(Web.WebUtils.DomainHost);

                rblFrom.Items.Clear();
                rblFrom.ClearSelection();
                rblFrom.Value = "";

                rblFrom.DataSource = Netpay.Emails.MailBox.Load();
                rblFrom.DataTextField = "EmailAddress"; rblFrom.DataValueField = "ID";
                ItemData.AdmincashUser = Web.WebUtils.LoggedUser.UserName;

                rblTo.Items.Clear();
                rblCC.Items.Clear();
                rblSubject.Value = "RE: " + ItemData.Subject;
                if (!string.IsNullOrEmpty(Request["ReplyToAddress"]))
                {
                    rblTo.Items.Add(new ListItem(Request["ReplyToAddress"]));
                }
                else
                {
                    rblTo.Items.Add(new ListItem(ItemData.EmailFrom));
                }

                //Set the rblTo Selected index
                rblTo.Value = rblTo.Items[0].Value;

                var mb = Netpay.Emails.MailBox.Load(ItemData.MailBoxID);

                if (mb != null)
                {
                    rblFrom.Items.Add(new ListItem(mb.EmailAddress, mb.ID.ToString()));
                    rblFrom.Value = Netpay.Web.WebUtils.LoggedUser.UserName + " <" + mb.EmailAddress + ">";
                }

                rblTo.Items.Add(new ListItem("Other:", string.Empty));
                rblCC.Items.Add(new ListItem(Netpay.Web.WebUtils.LoggedUser.EmailAddress, Netpay.Web.WebUtils.LoggedUser.EmailAddress));
                rblCC.Items.Add(new ListItem("Other:", string.Empty));
            }
            else
            {
                rblFrom.Items.Clear();
                rblTo.Items.Clear();
                rblCC.Items.Clear();

                //Clear selected value.
                rblTo.ValueType = null;
                rblCC.ValueType = null;
                foreach (var mb in GetMailBoxList1()) rblFrom.Items.Add(new ListItem(mb.Key, mb.Value.ToString()));
                rblCC.Items.Add(new ListItem(Netpay.Web.WebUtils.LoggedUser.EmailAddress, Netpay.Web.WebUtils.LoggedUser.EmailAddress));
                rblCC.Items.Add(new ListItem("Other:", string.Empty));
            }
            base.DataBindChildren();
        }

        public class FileToSend
        {
            public string FileName { get; set; }

            public string LocalPath { get; set; }
        }

        protected void Upload_Click(object sender, EventArgs e)
        {
            if (!fuDocument.HasFile)
            {
                return;
            }
            else
            {
                rptAttachments.AddItem(new FileToSend() { LocalPath = fuDocument.LocalPath, FileName = fuDocument.FileName });
                //Clear file name.
                fuDocument.LocalPath = "";
                fuDocument.DataBind();

                upAttachments.Update();
            }
        }

        public void RemoveAttachment(object sender, EventArgs e)
        {
            rptAttachments.RemoveItem(((sender as LinkButton).Parent as System.Web.UI.WebControls.RepeaterItem).ItemIndex);
            upAttachments.Update();
        }


        protected void Send_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(rblFrom.Value))
            {
                SendMessageNotify.SetMessage("Please choose From value.", true);
                UpSendMessage.Update();
                return;
            }

            var Dic = new Dictionary<string, System.IO.Stream>();
            if (rptAttachments.Items.Count > 0)
            {
                foreach (RepeaterItem item in rptAttachments.Items)
                {
                    string fileName = (item.FindControl("FileName") as Label).Text;
                    string filePath = (item.FindControl("LocalPath") as HiddenField).Value;
                    System.IO.Stream fileStream = new MemoryStream(File.ReadAllBytes(filePath));
                    Dic.Add(fileName, fileStream);
                }
            }


            int? hfID = (FindControl("hfID") as HiddenField).Value.ToNullableInt();
            int? hfLogStatusId = (FindControl("hfLogStatusId") as HiddenField).Value.ToNullableInt();

            //Compose new message flow, comes from Compose button on filter section
            if (hfID.HasValue && hfID.Value == 0)
            {
                short mailBoxId;
                List<string> mailBoxAddress = new List<string>() { rblFrom.Value };
                Netpay.Emails.MailBox mailBox = Netpay.Emails.MailBox.Load(mailBoxAddress).SingleOrDefault();
                if (mailBox != null)
                {
                    mailBoxId = mailBox.ID;
                }
                else // mailbox wasn't found ...
                {
                    SendMessageNotify.SetMessage("Mail box wasn't found.", true);
                    UpSendMessage.Update();
                    return;
                }

                //Send message 
                Netpay.Emails.Execute.SendMessage(Web.WebUtils.DomainHost, mailBoxId, null, rblFrom.Value, Web.WebUtils.LoggedUser.UserName, rblTo.Value, rblCC.Value, rblSubject.Value, RadEditor1.Text, null, Dic);
            }
            else if (hfID.HasValue)  // Existing item flow (comes from message tab)
            {
                ItemData = Netpay.Emails.Message.Load(hfID.Value);
                Netpay.Emails.Execute.SendMessage(Web.WebUtils.DomainHost, (short)ItemData.MailBoxID, ItemData.ID, rblFrom.Value, Web.WebUtils.LoggedUser.UserName, rblTo.Value, rblCC.Value, rblSubject.Value, RadEditor1.Text, ItemData.MimeMessage != null ? ItemData.MimeMessage.Text : null, Dic);
            }
            
            /*
            ItemData = TemplatePage.GetItemData<Netpay.Emails.Message>();
            if (ItemData == null) ItemData = new Netpay.Emails.Message();
            ItemDataLogStatus = TemplatePage.GetItemData<Netpay.Emails.LogStatus>();
            ItemData = Netpay.Emails.Message.Load(hfID.GetValueOrDefault());
            */

            //Compose existing item flow
            //ItemDataLogStatus.Save();
        }

        protected List<KeyValuePair<string, int>> GetMailBoxList1()
        {
            var list = Netpay.Emails.MailBox.LoadAll();
            var list1 = (from ccft in list select new KeyValuePair<string, int>(ccft.EmailAddress, ccft.ID))
                           .Distinct()
                           .ToList();

            var FirstItem = new KeyValuePair<string, int>("All", 0);

            //  list1.Insert(0, FirstItem);
            return list1;
        }

        public static byte[] ReadFile(Stream input)
        {
            byte[] buffer = new byte[input.Length];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}