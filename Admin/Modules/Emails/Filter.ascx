﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Emails.Filter" %>
<%@ Register Src="~/Modules/Emails/Form.ascx" TagPrefix="Compose" TagName="Email" %>
<admin:FilterSection runat="server" Title="Advanced search">

    <div class="form-group">
            Mail Box 
            <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control" DataSource="<%#GetMailBoxList()%>" DataTextField="Key" DataValueField="Value" Visible="true">
            </asp:DropDownList>
    </div>

    <div class="form-group">
        Search Text 
        <asp:TextBox runat="server" ID="MessageId" class="form-control" Visible="false"></asp:TextBox>
        <asp:TextBox runat="server" ID="SearchInput" class="form-control"></asp:TextBox>
    </div>
    <div class="form-group">
        Search Folder
            <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control">
                <asp:ListItem>Inbox</asp:ListItem>
                <asp:ListItem>Sent Items</asp:ListItem>
                <asp:ListItem>Deleted Items</asp:ListItem>
            </asp:DropDownList>
    </div>
    <div class="form-group">
        Status
            <asp:DropDownList ID="Status" runat="server" CssClass="form-control">
                <asp:ListItem></asp:ListItem>
                <asp:ListItem>Unread</asp:ListItem>
                <asp:ListItem>Read</asp:ListItem>
                <asp:ListItem>Waiting</asp:ListItem>
                <asp:ListItem>Replied</asp:ListItem>
                <asp:ListItem>Archived</asp:ListItem>
            </asp:DropDownList>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                Date Range
                <JQ:DateRange ID="DateRange" runat="server" />
            </div>
        </div>
    </div>
    <div class="form-group">
        By User
        <asp:DropDownList CssClass="form-control" runat="server" ID="ddlAdminUser" DataTextField="FullName" DataValueField="ID" AppendDataBoundItems="true">
            <asp:ListItem Text="[All]" Value="" />
        </asp:DropDownList>
    </div>

    <div class="form-group">
        To Manage mailboxes press
        <NP:Button runat="server" Text="Manage" CssClass="btn btn-primary" RegisterAsync="true" CommandName="OpenManage" OnCommand="Manage_Command" />
    </div>

    <div class="form-group">
        To Send new Email press
        <NP:Button runat="server" Text="Compose" CssClass="btn btn-primary" RegisterAsync="true" CommandName="Compose" OnCommand="Manage_Command" />
    </div>
</admin:FilterSection>


<admin:ModalDialog runat="server" ID="dlgManageEmails" Title="Manage Mailboxes" OnCommand="Manage_Command">
    <Body>
        <admin:FormSections runat="server" Title="List">
            <div class="table-responsive">
                <admin:AdminList runat="server" SaveAjaxState="true" ID="rptList" DataKeyNames="ID" AutoGenerateColumns="false" BubbleLoadEvent="true" OnSelectedIndexChanged="rptList_SelectedIndexChanged" IsInListView="false">
                    <Columns>
                        <asp:BoundField HeaderText="ID" DataField="ID" SortExpression="ID" />
                        <asp:BoundField HeaderText="Email Address" DataField="EmailAddress" SortExpression="EmailAddress" />
                        <asp:BoundField HeaderText="Mail Server" DataField="MailServer" SortExpression="MailServer" />
                        <asp:BoundField HeaderText="User Name" DataField="UserName" SortExpression="UserName" />
                        <asp:BoundField HeaderText="Is Blocked" DataField="IsBlocked" SortExpression="IsBlocked" />
                        <asp:BoundField HeaderText="Is AutoScan" DataField="IsAutoScan" SortExpression="IsAutoScan" />
                        <asp:BoundField HeaderText="Is SendFrom" DataField="IsSendFrom" SortExpression="IsSendFrom" />
                    </Columns>
                </admin:AdminList>
            </div>
        </admin:FormSections>

        <asp:MultiView runat="server" ActiveViewIndex='<%# ShouldShowManagedItem() ? 0 : 1 %>'>
            <asp:View runat="server">
                <admin:FormSections runat="server" Title="Item Data">
                    <asp:HiddenField runat="server" ID="hfManagedItemID" Value='<%# ShouldShowManagedItem() ? ManagedItem.ID : 0 %>' />
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                Email Address
                            </div>
                            <div class="col-lg-9 col-md-9">
                                <asp:TextBox runat="server" CssClass="form-control" ID="txtEmailAddress" Text='<%# ShouldShowManagedItem() ? ManagedItem.EmailAddress : "" %>' />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                Mail Server
                            </div>
                            <div class="col-lg-9 col-md-9">
                                <asp:TextBox runat="server" CssClass="form-control" ID="txtMailServer" Text='<%# ShouldShowManagedItem() ? ManagedItem.MailServer : "" %>' />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                User Name
                            </div>
                            <div class="col-lg-9 col-md-9">
                                <asp:TextBox runat="server" CssClass="form-control" ID="txtUserName" Text='<%# ShouldShowManagedItem() ? ManagedItem.UserName : "" %>' />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                Password
                            </div>
                            <div class="col-lg-9 col-md-9">
                                <asp:TextBox runat="server" TextMode="Password" CssClass="form-control" ID="txtPassword" Text='<%# ShouldShowManagedItem() ? ManagedItem.Password : "" %>' />
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:CheckBox runat="server" ID="chkIsBlocked" Checked='<%# ShouldShowManagedItem() ? ManagedItem.IsBlocked : false %>' Text="Is Blocked" />
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:CheckBox runat="server" ID="chkIsAutoScan" Checked='<%# ShouldShowManagedItem() ? ManagedItem.IsAutoScan : false %>' Text="Is AutoScan" />
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:CheckBox runat="server" ID="chkIsSendFrom" Checked='<%# ShouldShowManagedItem() ? ManagedItem.IsSendFrom : false %>' Text="Is SendFrom" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:CheckBox runat="server" ID="chkIsSSL" Checked='<%# ShouldShowManagedItem() ? ManagedItem.IsSSL: false %>' Text="Is SSL" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                Signature
                            </div>
                            <div class="col-lg-9 col-md-9">
                                <asp:TextBox runat="server" TextMode="MultiLine" CssClass="form-control" ID="txtSignature" Text='<%# ShouldShowManagedItem() ? ManagedItem.Signature : "" %>' />
                            </div>
                        </div>
                    </div>

                </admin:FormSections>
            </asp:View>
            <asp:View runat="server">
            </asp:View>
        </asp:MultiView>

    </Body>
    <Footer>
        <admin:DataButtons ID="dataButtons" runat="server" EnableNew='<%# IsNewButtonEnabled %>' EnableDelete='<%# ShouldShowDeleteButton() %>' EnableSave='<%# ShouldShowManagedItem() %>' />
    </Footer>
</admin:ModalDialog>


<admin:ModalDialog runat="server" ID="ComposeEmailMD" Title="Compose">
    <Body>
        <Compose:Email runat="server" ID="ComposeEmail" />
    </Body>
</admin:ModalDialog>
