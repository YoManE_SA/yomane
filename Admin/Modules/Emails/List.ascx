﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Emails.List" %>
<style>
    .email-list {
        width: 100%;
    }

        .email-list th {
            background: #cec9c9;
            font-size: 14px;
        }

        .email-list td {
            padding: 10px;
            font-size: 14px;
            color: #576475;
        }

    .muted {
        color: #95989A !important;
    }

    .email-list tr:nth-child(even) {
        background: #F5F5F5;
    }

    .email-list tr:nth-child(odd) {
        background: #fff;
    }
</style>

<admin:ListSection ID="ListSection1" runat="server" Icon="fa fa-envelope-o" Title="Email">
    <Header></Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList SaveAjaxState="true" runat="server" DataKeyNames="ID" ID="rptList" ShowHeader="true" AutoGenerateColumns="false" BubbleLoadEvent="true" sorSortExpression="DATE">
                <Columns>
                    <asp:BoundField HeaderText="From" DataField="EmailFrom" SortExpression="EmailFrom" />
                    <asp:BoundField HeaderText="Date" DataField="Date" SortExpression="Date" />
                    <asp:BoundField HeaderText="Subject" DataField="Subject" SortExpression="Subject" />

                    <asp:TemplateField HeaderText="Asign To">
                        <ItemTemplate>
                            <%# GetMerchantName((int?)Eval("MerchantID")) %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField HeaderText="To" DataField="EmailTo" SortExpression="EmailTo" />
                    <%--  <asp:BoundField HeaderText="Status" DataField="Status"  />--%>
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <%# (bool)Eval("IsSent")? "Sent" : GetStatusName((int?)Eval("Status")) %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField HeaderText="User" DataField="AdmincashUser" SortExpression="AdmincashUser" />

                    <%-- <asp:TemplateField HeaderText="Reply">
                        <ItemTemplate>
                        <i runat="server" visible='<%# Eval("IsSent") %>' class="fa fa-reply"></i>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="false" EnableAdd="false" />
    </Footer>
</admin:ListSection>


