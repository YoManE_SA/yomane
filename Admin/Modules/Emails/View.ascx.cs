﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Admin.Emails;
using System.IO;

namespace Netpay.Admin.Emails
{
    public partial class View : Controls.TemplateControlBase
    {

        protected Netpay.Emails.Message ItemData { get { return TemplatePage.GetItemData<Netpay.Emails.Message>(); } set { TemplatePage.SetItemData<Netpay.Emails.Message>(value); } }
        protected Netpay.Emails.LogStatus ItemDataLogStatus { get { return TemplatePage.GetItemData<Netpay.Emails.LogStatus>(); } set { TemplatePage.SetItemData<Netpay.Emails.LogStatus>(value); } }

        public Controls.AdminList ConversationHistoryAsAdminList
        {
            get
            {
                return rptListConversationHistory;
            }
        }

        protected override void DataBindChildren()
        {
            if (ItemDataLogStatus == null) ItemDataLogStatus = new Netpay.Emails.LogStatus();
            if (ItemData == null) ItemData = new Netpay.Emails.Message();
            else
            {
                if (ItemData.MerchantID.HasValue)
                {
                    Bll.Merchants.Merchant merchant = Bll.Merchants.Merchant.Load(ItemData.MerchantID.Value);
                    apAccount.Value = merchant != null? merchant.AccountID.ToNullableInt() : null;
                }
                else
                {
                    apAccount.Value = null;
                }

                if (ItemData.Status.HasValue) ChangeStatus1.Value = ItemData.Status.Value.ToString();
            }

            base.DataBindChildren();
        }

        public string GetComposeTabClientId()
        {
            return (Page as Controls.DataTablePage).FormTabs.Tabs.Where(x => x.Title == "Compose").FirstOrDefault().ClientID;
        }

        protected override void OnLoad(EventArgs e)
        {         
            TemplatePage.PageController.SaveItem += Save_Click;
            TemplatePage.PageController.DeleteItem += Delete_Click;
            rptListConversationHistory.DataBinding += RptListConversationHistory_DataBinding;
            rptListConversationHistory.SelectedIndexChanged += rptListConversationHistory_SelectedIndexChanged;
            rptAttachments.ItemDataBound += RptAttachments_ItemDataBound;

            base.OnLoad(e);
        }

        /// <summary>
        /// Need to add RegisterPostBackControl on the attachment file download link buttons
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RptAttachments_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton btnDownload = (LinkButton)e.Item.FindControl("btnDownload");
            if (btnDownload != null)
            {
                ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(btnDownload);
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                if (scriptManager != null)
                    scriptManager.RegisterPostBackControl(btnDownload);
            }
        }

        private void rptListConversationHistory_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Get the selected value which holds the message id
            var messageId = rptListConversationHistory.SelectedValue.ToNullableInt();
            if (!messageId.HasValue)
                return;

            ItemData = Netpay.Emails.Message.Load(messageId.Value);

            // Update the view
            dlgItemMessage.DataBind();
            upMessage.Update();
        }

        private void RptListConversationHistory_DataBinding(object sender, EventArgs e)
        {
            var sf = new Netpay.Emails.Message.SearchFilters();
            sf.threadID = ItemData.ThreadID;

            var list = Netpay.Emails.Message.Search(sf, rptListConversationHistory);
            if (list != null)
            {
                rptListConversationHistory.DataSource = list.OrderByDescending(x => x.Date).ToList();
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            if (ItemData == null) return;

            if (apAccount.Value.HasValue)
            {
                var merchant = Bll.Merchants.Merchant.LoadByAccountId(apAccount.Value.Value);
                if (merchant != null) ItemData.MerchantID = merchant.ID;
            }

            if (!ItemData.Status.HasValue && !ItemData.IsSent)
            {
                ItemData.Status = byte.Parse(ChangeStatus1.SelectedValue);
            }
            else if (ItemData.Status.HasValue && (byte.Parse(ChangeStatus1.SelectedValue) != ItemData.Status.Value) && !ItemData.IsSent)
            {
                // Change Message status 
                byte newStatus = byte.Parse(ChangeStatus1.SelectedValue);
                byte oldStatus = (byte)ItemData.Status;
                ItemData.Status = newStatus;
                ItemData.AdmincashUser = Web.WebUtils.LoggedUser.UserName;

                // Save LogStatus
                if (ItemDataLogStatus == null) ItemDataLogStatus = new Netpay.Emails.LogStatus();
                ItemDataLogStatus.StatusOld = oldStatus;
                ItemDataLogStatus.StatusNew = newStatus;
                ItemDataLogStatus.Date = DateTime.Now;
                ItemDataLogStatus.UserName = Web.WebUtils.LoggedUser.UserName;
                ItemDataLogStatus.Message = Comment.Text;
                ItemDataLogStatus.MessageId = ItemData.ID;
                ItemDataLogStatus.Save();
            }

            //Save item
            ItemData.Save();
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            var repeaterCommand = (e as RepeaterCommandEventArgs);
            int? hfID = (FindControl("hfID") as HiddenField).Value.ToNullableInt();
            ItemData = Netpay.Emails.Message.Load(hfID.GetValueOrDefault());
            if (ItemData == null) return;
            ItemData.IsDeleted = true;

            ItemData.Save();
        }

        protected string GetMerchantName(int? id)
        {
            if (id == null)
            {
                if (ItemData == null) TemplatePage.PageController.LoadActiveItem();
                if (ItemData != null && ItemData.MerchantID.HasValue)
                {
                    return Bll.Accounts.Account.GetMerchantNameByMerchantId(ItemData.MerchantID.Value);
                }
                else
                {
                    return null;
                }
            }
            return Bll.Accounts.Account.GetMerchantNameByMerchantId((int)id);
        }


        //protected void ChangeStatus1_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (apAccount.Value != null && ItemData != null) ItemData.MerchantID = apAccount.Value;
        //    if (ChangeStatus1.SelectedValue != null && ItemData != null)
        //    {
        //        string valueStr = ChangeStatus1.SelectedValue;

        //        // Change Message status and save in DB
        //        byte newStatus = (byte)(ChangeStatus1.SelectedIndex + 1);
        //        byte oldStatus = (byte)ItemData.Status;
        //        //ItemData.ChangeStatus(newStatus, Comment.Text);

        //        // Save LogStatus
        //        if (ItemDataLogStatus == null) ItemDataLogStatus = new Netpay.Emails.LogStatus();

        //        ItemDataLogStatus.StatusOld = oldStatus;
        //        ItemDataLogStatus.StatusNew = newStatus;
        //        ItemDataLogStatus.Date = DateTime.Now;
        //        ItemDataLogStatus.UserName = Web.WebUtils.LoggedUser.UserName;
        //        ItemDataLogStatus.Message = Comment.Text;
        //        ItemDataLogStatus.MessageId = ItemData.ID;

        //        //ItemDataLogStatus.Save();
        //    }
        //}

        protected void DownloadAttacment(object sender, CommandEventArgs e)
        {
            int? hfID = (FindControl("hfID") as HiddenField).Value.ToNullableInt();
            ItemData = Netpay.Emails.Message.Load(hfID.GetValueOrDefault());
            if (ItemData == null) return;

            var data = ItemData.MimeMessage.Attachments[e.CommandArgument.ToNullableInt().GetValueOrDefault()].DecodedData;
            var fileName = ItemData.MimeMessage.Attachments[e.CommandArgument.ToNullableInt().GetValueOrDefault()].FileName;

            if (string.IsNullOrEmpty(fileName)) return;
            Response.Clear();
            Response.ContentType = "application/octet-stream"; //export.ContentType;
            Response.Headers.Add("Content-Description", "File Transfer");
            Response.Headers.Add("Content-Transfer-Encoding", "binary");
            Response.Headers.Add("Expires", "0");
            Response.Headers.Add("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            Response.Headers.Add("Content-Disposition", "attachment; filename=" + System.IO.Path.GetFileName(fileName));

            Stream stream = new MemoryStream(data);
            Int16 bufferSize = 1024;
            byte[] buffer = new byte[bufferSize + 1];
            int count = stream.Read(buffer, 0, bufferSize);

            while (count > 0)
            {
                Response.OutputStream.Write(buffer, 0, count);
                count = stream.Read(buffer, 0, bufferSize);
            }

            Response.End();
        }
    }
}