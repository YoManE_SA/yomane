﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Admin.Controls;
using Netpay.Infrastructure;
using Netpay.Emails;

namespace Netpay.Admin.Emails
{
    public partial class Filter : Controls.TemplateControlBase
    {
        protected Netpay.Emails.MailBox ManagedItem { get { return TemplatePage.GetItemData<Netpay.Emails.MailBox>(); } set { TemplatePage.SetItemData(value); } }

        protected bool IsNewButtonEnabled { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            ddlAdminUser.DataBinding += delegate { ddlAdminUser.DataSource = Infrastructure.Security.AdminUser.Cache; };

            dlgManageEmails.DataBinding += ManageList_DataBinding;

            TemplatePage.PageController.Search += PageController_Search;

            IsNewButtonEnabled = true;

            UpdateSearchFilter();

            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
        {
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Netpay.Emails.Message.SearchFilters();
            if (Status.SelectedIndex != 0) sf.status = Status.SelectedIndex;
            if (ddlAdminUser.SelectedItem.Value != "") sf.user = ddlAdminUser.SelectedItem.Text;
            if (MessageId.Text != "") sf.MessageId = MessageId.Text.ToNullableInt();
            sf.Text = SearchInput.Text;
            if (DropDownList1.SelectedIndex == 0)
            {
                sf.sent = false;
                sf.deleted = false;
            }
            if (DropDownList1.SelectedIndex == 1)
            {
                sf.sent = true;
                sf.deleted = false;

            }
            if (DropDownList1.SelectedIndex == 2)
            {
                sf.deleted = true;
            }


            if (DropDownList2.SelectedValue != "" && DropDownList2.SelectedValue != "0") sf.mailboxId = int.Parse(DropDownList2.SelectedValue);
            //   if (DropDownList1.SelectedIndex !=0) sf.mailboxId = DropDownList1.SelectedIndex;
            sf.Date = DateRange.Value;

            TemplatePage.SetFilter(sf);
        }

        private void ManageList_DataBinding(object sender, EventArgs e)
        {
            rptList.DataSource = Netpay.Emails.MailBox.LoadAll();
        }

        protected List<KeyValuePair<string, int>> GetMailBoxList()
        {
            var list = Netpay.Emails.MailBox.LoadAll();
            var list1 = (from ccft in list select new KeyValuePair<string, int>(ccft.EmailAddress, ccft.ID))
                           .Distinct()
                           .ToList();

            var FirstItem = new KeyValuePair<string, int>("All", 0);

            list1.Insert(0, FirstItem);
            return list1;


        }

        protected void Manage_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "OpenManage":
                    dlgManageEmails.BindAndShow();
                    IsNewButtonEnabled = true;
                    break;
                case "NewItem":
                    ManagedItem = new Netpay.Emails.MailBox();
                    rptList.SelectedIndex = -1;
                    dlgManageEmails.BindAndUpdate();
                    IsNewButtonEnabled = true;
                    break;
                case "SaveItem":
                    ManagedItem = Netpay.Emails.MailBox.Load(hfManagedItemID.Value.ToNullableInt().GetValueOrDefault(-1));
                    if (ManagedItem == null) ManagedItem = new Netpay.Emails.MailBox();

                    ManagedItem.EmailAddress = txtEmailAddress.Text;
                    ManagedItem.MailServer = txtMailServer.Text;
                    ManagedItem.UserName = txtUserName.Text;
                    ManagedItem.Password = txtPassword.Text;
                    ManagedItem.IsBlocked = chkIsBlocked.Checked;
                    ManagedItem.IsAutoScan = chkIsAutoScan.Checked;
                    ManagedItem.IsSendFrom = chkIsSendFrom.Checked;
                    ManagedItem.IsSSL = chkIsSSL.Checked;
                    ManagedItem.Signature = txtSignature.Text;
                    if (ManagedItem.ID == 0)
                        // TBD: Need to fix MessageStoragePath
                        ManagedItem.MessageStoragePath = @"C:\Netpay\Files\GlobalData\EmailsCRM\Sales";/*Bll.Accounts.Account.MapPrivatePath(ObjectContext.Current.User.LoginID, ObjectContext.Current.User.UserName);*/

                    ManagedItem.Save();
                    ManagedItem = null;
                    rptList.SelectedIndex = -1;
                    IsNewButtonEnabled = true;
                    dlgManageEmails.BindAndUpdate();
                    break;
                case "DeleteItem":
                    ManagedItem = Netpay.Emails.MailBox.Load(hfManagedItemID.Value.ToNullableInt().GetValueOrDefault(-1));
                    if (ManagedItem != null) ManagedItem.Delete();

                    rptList.SelectedIndex = -1;
                    IsNewButtonEnabled = true;
                    dlgManageEmails.BindAndUpdate();
                    break;

                case "Compose":
                    TemplatePage.SetItemData<Netpay.Emails.Message>(new Netpay.Emails.Message());
                    ComposeEmailMD.BindAndShow();
                    break;
            }
        }

        protected void rptList_SelectedIndexChanged(object sender, EventArgs e)
        {
            AdminList adminList = (AdminList)sender;
            short selectedValue = (short)adminList.SelectedValue;

            ManagedItem = Netpay.Emails.MailBox.Load(selectedValue);

            dlgManageEmails.BindAndUpdate();
        }

        protected bool ShouldShowManagedItem()
        {
            if (ManagedItem != null)
                return true;
            return false;
        }

        protected bool ShouldShowDeleteButton()
        {
            if (ManagedItem != null && ManagedItem.ID != 0)
                return true;
            return false;
        }

    }
}