﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Admin.PeriodicFees
{
    public partial class AccountFees : Controls.AccountControlBase
    {
        protected Netpay.Bll.PeriodicFees.AccountFees ItemData;

        protected Netpay.Bll.PeriodicFees.PeriodicFeeType TypeData;

        public Bll.Accounts.AccountType? CurrentAccountType
        {
            get
            {
                if (Account != null) return Account.AccountType;
                else return null;
            }
            set { }
        }

        public int GetDefaultValue(int currenttypeid)
        {
            if (ddlFeeTypeID.AccountTypeToShowInList == null)
            {
                return currenttypeid;
            }

            else
            {
                var sf = new Bll.PeriodicFees.PeriodicFeeType.SearchFilters();
                sf.isActive = true;
                sf.AccountTypeToSearch = ddlFeeTypeID.AccountTypeToShowInList;
                var list = Bll.PeriodicFees.PeriodicFeeType.Search(sf, null);

                if (list.Select(x => x.ID).ToList().Contains(currenttypeid))
                {
                    return currenttypeid;
                }
                else
                {
                    return list.First().ID;
                }
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            dlgEdit.DataBinding += delegate
            {
                ddlFeeTypeID.AccountTypeToShowInList = CurrentAccountType;
                
            };

            rptList.DataBinding += delegate
            {
                var sf = new Netpay.Bll.PeriodicFees.AccountFees.SearchFilters();
                sf.accountId = AccountID.GetValueOrDefault();
                rptList.DataSource = Netpay.Bll.PeriodicFees.AccountFees.Search(sf, null);
            };
            ddlAccountStoredPaymentMethodID.DataBinding += delegate
            {
                ddlAccountStoredPaymentMethodID.DataSource = Bll.Accounts.StoredPaymentMethod.NewLoadForAccount(AccountID.Value);
            };
        }

        protected override void DataBindChildren()
        {
            pnlList.DataBind();

            ddlFeeTypeID.AccountTypeToShowInList = CurrentAccountType;
            ddlFeeTypeID.DataBind();

            if (ItemData == null) return;

            base.DataBindChildren();
        }

        protected void Dialog_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "AddItem":

                    if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                        ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.PeriodicFees.AccountFees.SecuredObject, PermissionValue.Add);

                    ItemData = new Netpay.Bll.PeriodicFees.AccountFees(AccountID.Value) { DateNextCharge = DateTime.Now };
                    TypeData = Bll.PeriodicFees.PeriodicFeeType.Load(1); // By default load the first fee type
                    if (TypeData == null)
                    {
                        throw new Exception("No periodic fee exist, please declare one under Settings -> Periodic Fees");
                    }

                    ddlChargeType.SelectedValue = string.Empty;
                    dlgEdit.BindAndShow();
                    break;

                case "EditItem":

                    ItemData = Netpay.Bll.PeriodicFees.AccountFees.Load(e.CommandArgument.ToNullableInt().GetValueOrDefault());
                    if (ItemData != null)
                    {
                        ddlChargeType.SelectedValue = ItemData.ChargeType.HasValue ? ItemData.ChargeType.Value.ToString() : string.Empty;
                        TypeData = Bll.PeriodicFees.PeriodicFeeType.Load(ItemData.FeeTypeID);
                        dlgEdit.BindAndShow();
                    };
                    break;

                case "SaveItem":
                    ItemData = Netpay.Bll.PeriodicFees.AccountFees.Load(hfID.Value.ToNullableInt().GetValueOrDefault());
                    if (ItemData == null) ItemData = new Netpay.Bll.PeriodicFees.AccountFees(AccountID.Value);
                    ItemData.FeeTypeID = ddlFeeTypeID.Value.ToNullableInt().GetValueOrDefault();
                    ItemData.CurrencyISOCode = ddlCurrency.SelectedCurrencyIso;
                    ItemData.Amount = txtAmount.Text.ToNullableDecimal().GetValueOrDefault();
                    ItemData.IsActive = chkIsActive.Checked;
                    ItemData.AccountStoredPaymentMethodID = ddlAccountStoredPaymentMethodID.Value == "-1" ? null : ddlAccountStoredPaymentMethodID.Value.ToNullableInt();
                    ItemData.ChargeType = string.IsNullOrEmpty(ddlChargeType.SelectedValue)? null : ddlChargeType.SelectedValue.ToNullableByte();

                    DateTime DateNextChargeVar;
                    ItemData.DateNextCharge = DateTime.TryParse(txtNextChargeDate.Text, out DateNextChargeVar) ? DateNextChargeVar : DateTime.Now;

                    ItemData.Save();
                    dlgEdit.RegisterHide();
                    pnlList.BindAndUpdate();
                    break;

                case "DeleteItem":
                    ItemData = Netpay.Bll.PeriodicFees.AccountFees.Load(hfID.Value.ToNullableInt().GetValueOrDefault());
                    if (ItemData != null) ItemData.Delete();
                    dlgEdit.RegisterHide();
                    pnlList.BindAndUpdate();
                    break;
            }
        }

        protected void FeeTypeID_SelectedIndexChanged(object sender, EventArgs e)
        {
            TypeData = Bll.PeriodicFees.PeriodicFeeType.Load(ddlFeeTypeID.Value.ToNullableInt().GetValueOrDefault());
            if (TypeData == null) return;
            if (TypeData.Amount != null) txtAmount.Text = TypeData.Amount.Value.ToString("0.00");
            if (!string.IsNullOrEmpty(TypeData.CurrencyISOCode)) ddlCurrency.SelectedCurrencyIso = TypeData.CurrencyISOCode;

            var itemDataId = int.Parse(((sender as DropDownList).Parent.FindControl("hfID") as HiddenField).Value);
            if (itemDataId != 0)
            {
                ItemData = Netpay.Bll.PeriodicFees.AccountFees.Load(itemDataId);
            }
            else
            {
                ItemData = new Netpay.Bll.PeriodicFees.AccountFees(AccountID.Value);
            }

            dlgEdit.BindAndUpdate();
        }

        public string GetChargeTypeText(object value)
        {
            if (value == null) return "None";
            PeriodicFeeChargeType EValue = (PeriodicFeeChargeType)(byte.Parse(value.ToString()));

            switch (EValue)
            {
                case PeriodicFeeChargeType.OnlyCC:
                    return "Credit card";
                case PeriodicFeeChargeType.OnlyProcessing:
                    return "Processing";
                case PeriodicFeeChargeType.ProcessingOrCC:
                    return "Processing or CC";
                default:
                    return "None";
            }
        }

        protected void ExecuteFees(object sender, EventArgs e)
        {
            Netpay.Bll.PeriodicFees.AccountFees.Execute();
        }
    }
}