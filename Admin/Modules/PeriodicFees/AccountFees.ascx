﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="AccountFees.ascx.cs" Inherits="Netpay.Admin.PeriodicFees.AccountFees" %>
<admin:FormPanel runat="server" ID="pnlList" Title="Periodic Fees" OnCommand="Dialog_Command" EnableAdd="<%#(TemplatePage.Page is Netpay.Admin.Controls.AccountPage) && TemplatePage.ItemID != null && TemplatePage.ItemID != 0 && (Netpay.Bll.PeriodicFees.AccountFees.SecuredObject.HasPermission(Netpay.Infrastructure.Security.PermissionValue.Add))%>">
<asp:MultiView runat="server" ActiveViewIndex="<%#(TemplatePage.Page is Netpay.Admin.Controls.AccountPage) && TemplatePage.ItemID != null && TemplatePage.ItemID != 0 ? 0 : 1 %>">
    <asp:View runat="server">
    <netpay:DynamicRepeater runat="server" ID="rptList">
        <HeaderTemplate>
             <table class="table table-hover table-customize">
            <thead>
                <th>Active</th>
                <th>Name</th>
                <th>Amount</th>
                <th>Pay With</th>
                <th>Charge type</th>
                <th>Next Charge</th>
            </thead>
        </HeaderTemplate>
        <ItemTemplate>
            <tbody>
                <td><asp:Literal runat="server" ID="chkIsActive" Text='<%# Eval("IsActive") %>' /></td>
                <td><asp:LinkButton runat="server" ID="ltFeeTypeID" Text='<%# Eval("RefFeeType.Name") %>' CommandName="EditItem" OnCommand="Dialog_Command" CommandArgument='<%# Eval("ID") %>' /></td>
                <td><asp:Literal runat="server" Text='<%# Eval("Amount").ToNullableDecimal().GetValueOrDefault().ToIsoAmountFormat((string)Eval("CurrencyISOCode")) %>' /></td>
                <td><asp:Literal runat="server" Text='<%# Eval("StoredPaymentMethodName")  %>' /></td>
                <td><asp:Literal runat="server" Text='<%# GetChargeTypeText(Eval("ChargeType")) %>'> </asp:Literal></td>
                <td><asp:Literal runat="server" Text='<%# Eval("DateNextCharge", "{0:d}")=="01/01/0001"? "None" : Eval("DateNextCharge", "{0:d}") %>' /></td>
            </tbody>
        </ItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
        <EmptyContainer><div class="alert alert-info"><strong>Info!</strong> No record information</div></EmptyContainer>
    </netpay:DynamicRepeater>
        <asp:Button Text="Charge" runat="server" CssClass="btn btn-primary" OnClick="ExecuteFees"/>
        </asp:View>
    <asp:View runat="server">
        <div class="alert alert-info">
            Save account in order to edit this section.
        </div>
        
    </asp:View>
    </asp:MultiView>
</admin:FormPanel>
<admin:ModalDialog runat="server" ID="dlgEdit" OnCommand="Dialog_Command" Title="Edit Account Fee">
    <Body>
        <asp:HiddenField runat="server" ID="hfID" Value='<%# ItemData.ID %>' />
        <div class="row">
            <div class="col-lg-6">
                Fee Type
                <div class="form-group">
                    <%-- ItemData.FeeTypeID==0? 1 : ItemData.FeeTypeID --%>
                    <netpay:PeriodicFeeTypeDropDown runat="server" ID="ddlFeeTypeID" CssClass="form-control" Value='<%# GetDefaultValue(TypeData.ID) %>' AutoPostBack="true" OnSelectedIndexChanged="FeeTypeID_SelectedIndexChanged" EnableBlankSelection="False" />
                </div>
            </div>
            <div class="col-lg-6">
                <br />
                <asp:CheckBox runat="server" ID="chkIsActive" Text="Active" Checked='<%# ItemData.IsActive %>' />

            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                Amount           
                <div class="form-group">
                    <asp:TextBox runat="server" ReadOnly="true" ID="txtAmount" CssClass="form-control" Text='<%# TypeData != null && TypeData.Amount.HasValue ? TypeData.Amount.Value.ToString("0.00") : "" %>' />
                </div>
            </div>
            <div class="col-lg-6">
                Currency           
                <div class="form-group">
                    <netpay:CurrencyDropDown Enabled="false" runat="server" ID="ddlCurrency" CssClass="form-control" SelectedCurrencyIso='<%# TypeData.CurrencyISOCode %>' EnableBlankSelection="false" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                Pay With           
                <div class="form-group">
                    <netpay:DropDownBase DataSource="<%#ItemData.AccountStoredPaymentMethods%>" runat="server" ID="ddlAccountStoredPaymentMethodID" CssClass="form-control" DataTextField="Key" DataValueField="Value" Value="<%#ItemData.AccountStoredPaymentMethodID==null? ItemData.AccountStoredPaymentMethods[0].Value : ItemData.AccountStoredPaymentMethodID%>" EnableBlankSelection="false" />
                </div>
            </div>
            <div class="col-lg-6">
                Next Charge           
                <div class="form-group">
                    <asp:TextBox ID="txtNextChargeDate" runat="server" Text='<%# ItemData.DateNextCharge.ToString("d")=="01/01/0001"? "None" : ItemData.DateNextCharge.ToString("d") %>' CssClass="form-control" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                Charge type
                <div class="form-group">
                    <asp:DropDownList ID="ddlChargeType" runat="server" CssClass="form-control">
                        <asp:ListItem Value="" Text=""></asp:ListItem>
                        <asp:ListItem Value="1" Text="Processing"></asp:ListItem>
                        <asp:ListItem Value="2" Text="Processing or CC"></asp:ListItem>
                        <asp:ListItem Value="3" Text="Credit card"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" EnableSave="true" EnableDelete="true" />
    </Footer>
</admin:ModalDialog>


