﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Admin.PeriodicFees
{
    public partial class Types : Controls.ModulePage
    {
        public Types() : base(Admin.PeriodicFees.Module.ModuleName) { }

        protected Netpay.Bll.PeriodicFees.PeriodicFeeType ItemData;
        protected override void OnLoad(EventArgs e)
        {
            rptList.DataBinding += delegate { rptList.DataSource = Netpay.Bll.PeriodicFees.PeriodicFeeType.Search(null, null); };

            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            if (IsModuleActive)
            {
                MVPeriodicFees.ActiveViewIndex = 0;
                pnlList.DataBind();
            }
            else
            {
                MVPeriodicFees.ActiveViewIndex = 1;
            }

            base.DataBindChildren();
        }

        protected void Dialog_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "AddItem":
                    //Check permission
                    if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                        ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.PeriodicFees.PeriodicFeeType.SecuredObject, PermissionValue.Add);

                    ItemData = new Netpay.Bll.PeriodicFees.PeriodicFeeType();
                    dlgEdit.BindAndShow();
                    break;

                case "EditItem":
                    if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                        ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.PeriodicFees.PeriodicFeeType.SecuredObject, PermissionValue.Edit);

                    ItemData = Netpay.Bll.PeriodicFees.PeriodicFeeType.Load(e.CommandArgument.ToNullableInt().GetValueOrDefault());
                    if (ItemData != null) dlgEdit.BindAndShow();
                    break;

                case "SaveItem":
                    ItemData = Netpay.Bll.PeriodicFees.PeriodicFeeType.Load(hfID.Value.ToNullableInt().GetValueOrDefault());
                    if (ItemData == null) ItemData = new Netpay.Bll.PeriodicFees.PeriodicFeeType();
                    ItemData.Name = txtName.Text;
                    //dataItem.Description = (item.FindControl("txtDescription") as TextBox).Text;
                    if (ddlProcessMerchantID.Value.HasValue)
                    {
                        var merchant = Bll.Merchants.Merchant.LoadByAccountId(ddlProcessMerchantID.Value.Value);
                        if (merchant != null)
                            ItemData.ProcessMerchantID = merchant.ID;
                    }
                    else
                    {
                        ItemData.ProcessMerchantID = null;
                    }
                    ItemData.AccountType = ddlAccountType.Value.ToNullableEnumByValue<Netpay.Bll.Accounts.AccountType>().GetValueOrDefault();
                    ItemData.IsActive = chkIsActive.Checked;
                    ItemData.Amount = txtAmount.Text.ToDecimal(0);
                    ItemData.CurrencyISOCode = ddlCurrency.SelectedCurrencyIso;
                    ItemData.TimeInterval = ddlTimeInterval.Value.ToNullableEnumByValue<PeriodicInterval>().GetValueOrDefault();
                    ItemData.Save();
                    dlgEdit.RegisterHide();
                    pnlList.BindAndUpdate();
                    break;

                case "DeleteItem":
                    ItemData = Netpay.Bll.PeriodicFees.PeriodicFeeType.Load(hfID.Value.ToNullableInt().GetValueOrDefault());
                    if (ItemData != null) ItemData.Delete();
                    dlgEdit.RegisterHide();
                    pnlList.BindAndUpdate();
                    break;
            }
        }
    }
}
