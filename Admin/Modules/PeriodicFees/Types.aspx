﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="True" CodeBehind="Types.aspx.cs" Inherits="Netpay.Admin.PeriodicFees.Types" %>

<asp:Content ContentPlaceHolderID="PeriodicFeesContent" runat="Server">
    <asp:MultiView runat="server" ID="MVPeriodicFees">
        <asp:View runat="server">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Periodic Fees
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <admin:FormPanel ID="pnlList" runat="server" Title="Periodic Fees" OnCommand="Dialog_Command" EnableAdd="true">
                                <netpay:DynamicRepeater runat="server" ID="rptList">
                                    <HeaderTemplate>
                                        <div class="row inner-table-titles ">
                                            <div class="col-lg-4 col-md-4">
                                                Name
                                            </div>
                                            <div class="col-lg-3 col-md-3">
                                                Processing Merchant
                                            </div>
                                            <div class="col-lg-1 col-md-1">
                                                Currency
                                            </div>
                                            <div class="col-lg-1 col-md-1">
                                                Amount
                                            </div>
                                            <div class="col-lg-1 col-md-1">
                                                Interval
                                            </div>
                                            <div class="col-lg-1 col-md-1">
                                                Active
                                            </div>
                                            <div class="col-lg-1 col-md-1">
                                                Account Type
                                            </div>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="row table-ui">
                                            <div class="col-lg-4 col-md-4">
                                                <asp:LinkButton runat="server" Text='<%# Eval("Name") %>' OnCommand="Dialog_Command" CommandName="EditItem" CommandArgument='<%# Eval("ID") %>' />
                                            </div>
                                            <div class="col-lg-3 col-md-3">
                                                <asp:Literal runat="server" Text='<%# Eval("ProcessMerchantName") %>' />
                                            </div>
                                            <div class="col-lg-1 col-md-1">
                                                <asp:Literal runat="server" Text='<%# Eval("CurrencyISOCode") %>' />
                                            </div>
                                            <div class="col-lg-1 col-md-1">
                                                <asp:Literal runat="server" Text='<%# Eval("Amount") %>' />
                                            </div>
                                            <div class="col-lg-1 col-md-1">
                                                <asp:Literal runat="server" Text='<%# Eval("TimeInterval") %>' />
                                            </div>
                                            <div class="col-lg-1 col-md-1">
                                                <asp:Literal runat="server" Text='<%# Eval("IsActive") %>' />
                                            </div>
                                            <div class="col-lg-1 col-md-1">
                                                <asp:Literal runat="server" Text='<%# Eval("AccountType") %>' />
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                    <EmptyContainer>
                                        <div class="alert alert-info"><strong>Info!</strong> No record information</div>
                                    </EmptyContainer>
                                </netpay:DynamicRepeater>
                        </admin:FormPanel>

                        <admin:ModalDialog runat="server" ID="dlgEdit" OnCommand="Dialog_Command" Title="Edit Periodic Fee">
                            <Body>
                                <asp:HiddenField runat="server" ID="hfID" Value='<%# ItemData!=null? ItemData.ID : 0 %>' />
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            Name
                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtName" Text='<%#ItemData!=null? ItemData.Name : ""%>' />
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <br />
                                        <%--<span class="btn btn-success">Active <i class="fa fa-check-circle" style="color: #fff"></i></span>--%>
                                        <div class="form-group">
                                            <asp:CheckBox Text="Is Active" runat="server" ID="chkIsActive" Checked='<%#ItemData!=null? ItemData.IsActive : false %>' />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            Processing Merchant
                                    <admin:AccountPicker runat="server" ID="ddlProcessMerchantID" Value='<%#ItemData!=null && ItemData.ProcessMerchantID.HasValue? Netpay.Bll.Merchants.Merchant.Load(ItemData.ProcessMerchantID).AccountID : 0 %>' UseTargetID="false" LimitToType="Merchant" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            Amount
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtAmount" Text='<%#ItemData!=null? ItemData.Amount : 0 %>' />
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            Currency
                                    <netpay:CurrencyDropDown CssClass="form-control" runat="server" ID="ddlCurrency" SelectedCurrencyIso='<%#ItemData!=null? ItemData.CurrencyISOCode : "" %>' />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            Interval
                                    <netpay:EnumDropDown CssClass="form-control" runat="server" ID="ddlTimeInterval" Value='<%#ItemData!=null? (int)ItemData.TimeInterval : 0 %>' ViewEnumName="Netpay.Infrastructure.PeriodicInterval" />
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            Account Type
                                    <netpay:EnumDropDown runat="server" CssClass="form-control" ID="ddlAccountType" Value='<%# ItemData!=null? (int)ItemData.AccountType : 0 %>' ViewEnumName="Netpay.Bll.Accounts.AccountType" />
                                        </div>
                                    </div>
                                </div>
                            </Body>
                            <Footer>
                                <admin:DataButtons runat="server" EnableSave="true" EnableDelete="true" />
                            </Footer>
                        </admin:ModalDialog>
                    </div>
                </div>
            </div>
        </asp:View>
        <asp:View runat="server">
            <div class="alert alert-danger">
                Module isn't active.
            </div>
        </asp:View>
    </asp:MultiView>
</asp:Content>

