﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.PeriodicFees
{
	public class Module : Admin.CoreBasedModule
	{
		private ApplicationMenu menuItem;

        public static readonly string ModuleName = "PeriodicFeeTypes";

        public override string Name { get { return ModuleName; } }

		public Module() : base(Bll.PeriodicFees.Module.Current) { }

		protected override void OnInit(EventArgs e)
		{
            //Create menu item
            menuItem = new ApplicationMenu("Periodic Fees", "~/Modules/PeriodicFees/Types.aspx", null, 230);

            //No route here , it's an aspx page.
            Application.RegisterRoute("Settings/PeriodicFees", "PeriodicFees", typeof(Netpay.Admin.PeriodicFees.Types), module:this);

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;
                        
            base.OnInit(e);
		}

		protected override void OnActivate(EventArgs e)
		{			
			base.OnActivate(e);
		}

		protected override void OnDeactivate(EventArgs e)
		{			
			base.OnDeactivate(e);
		}

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Settings", menuItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuItem.Parent.RemoveChild(menuItem);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
		{
            if (!CoreModule.IsInstalled)
                return;

			var page = sender as Controls.DataTablePage;
            if (Admin.Accounts.Module.TemplateIsAccountType(page.TemplateName) && (page.ViewName == null) && (page.TemplateName!="DebitCompany"))
			{
				if (Bll.PeriodicFees.AccountFees.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Read))
                    page.AddControlToForm("Fees", page.LoadControl("~/Modules/PeriodicFees/AccountFees.ascx"), this, "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject, Bll.PeriodicFees.AccountFees.SecuredObject).GetActivObject);
			}
		}
	}
}