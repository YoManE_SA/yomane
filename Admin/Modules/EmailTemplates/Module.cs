﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.EmailTemplates
{
	public class Module : Admin.CoreBasedModule
	{
		private ApplicationMenu mainMenuItem;

		public override string Name { get { return "EmailTemplates"; } }
		public override decimal Version { get { return 1.0m; } }
		public override string Author { get { return "OBL ltd."; } }
		public override string Description { get { return ""; } }

        public Module() : base(Infrastructure.Email.Module.Current) { }

		protected override void OnInit(EventArgs e)
		{
            //Create menu item
            mainMenuItem = new ApplicationMenu("Email Templates", "~/EmailTemplates/0", null, 280);

            //Register route
            Application.RegisterRoute("EmailTemplates/{*id}", "EmailTemplates", typeof(Controls.DataTablePage), module:this);

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
		}

		protected override void OnActivate(EventArgs e)
		{
			base.OnActivate(e);
		}
		
		protected override void OnDeactivate(EventArgs e)
		{			
			base.OnDeactivate(e);
		}

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Settings", mainMenuItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            mainMenuItem.Parent.RemoveChild(mainMenuItem);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
		{
            if (!CoreModule.IsInstalled)
                return;

			var page = sender as Controls.DataTablePage;
			if (page.TemplateName == "EmailTemplates")
			{
                page.PageController.HideFilter = true;
                page.PageController.LoadItem += PageController_LoadItem;
                page.AddControlToList(page.LoadControl("~/Modules/EmailTemplates/List.ascx"));
                page.AddControlToForm("Data", page.LoadControl("~/Modules/EmailTemplates/Data.ascx"), this, "", Infrastructure.Email.Template.SecuredObject);
            }
		}

        void PageController_LoadItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            string fileName = (string) page.PageController.DataKey["FileName"];
            if (fileName != null) page.SetItemData(Infrastructure.Email.Template.Get(fileName));

            page.FormButtons.EnableSave = Infrastructure.Email.Template.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Edit);
        }
    }
}