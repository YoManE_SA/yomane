﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Data.ascx.cs" Inherits="Netpay.Admin.EmailTemplates.Data" %>
<admin:FormSections runat="server" Title="Details" Flexible="false">
    <div class="row">
        <div class="col-lg-8">
            <div class="form-group">
                Name
				<asp:TextBox runat="server" CssClass="form-control" ID="txtName" Text='<%# Item.Name %>' />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                Language
				<netpay:DropDownBase runat="server" CssClass="form-control" ID="ddlLanguage" DataSource='<%# Item.Languages %>' Value='<%# Item.Language %>' AutoPostBack="true" OnSelectedIndexChanged="Language_SelectedIndexChanged" BlankSelectionText="[Default]" BlankSelectionValue="" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <JQ:HtmlEdit runat="server" ID="heHtmlData" Text='<%# Item.HtmlData %>' />
            </div>
        </div>
    </div>
</admin:FormSections>
<admin:ModalDialog runat="server" Title="Available merged fields" ID="dlgParamteres">
    <Body>
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-info">
                    *paramters should be formated as [paramName]
                </div>
            </div>
        </div>
        <div class="row">
            <asp:Repeater runat="server" id="rptFields1" DataSource="<%# silceFields(0) %>" >
                <HeaderTemplate><div class="col-lg-6"><ul class="list-group"></HeaderTemplate>
                <ItemTemplate>
                    <li class="list-group-item"><%# Container.DataItem %></li>
                </ItemTemplate>
                <FooterTemplate></ul></div></FooterTemplate>
            </asp:Repeater>
            <asp:Repeater runat="server" id="rptFields2" DataSource="<%# silceFields(1) %>" >
                <HeaderTemplate><div class="col-lg-6"><ul class="list-group"></HeaderTemplate>
                <ItemTemplate>
                    <li class="list-group-item"><%# Container.DataItem %></li>
                </ItemTemplate>
                <FooterTemplate></ul></div></FooterTemplate>
            </asp:Repeater>
            <asp:Repeater runat="server" id="rptFields3" DataSource="<%# silceFields(2) %>" >
                <HeaderTemplate><div class="col-lg-6"><ul class="list-group"></HeaderTemplate>
                <ItemTemplate>
                    <li class="list-group-item"><%# Container.DataItem %></li>
                </ItemTemplate>
                <FooterTemplate></ul></div></FooterTemplate>
            </asp:Repeater>
             <asp:Repeater runat="server" id="Repeater4" DataSource="<%# silceFields(3) %>" >
                <HeaderTemplate><div class="col-lg-6"><ul class="list-group"></HeaderTemplate>
                <ItemTemplate>
                    <li class="list-group-item"><%# Container.DataItem %></li>
                </ItemTemplate>
                <FooterTemplate></ul></div></FooterTemplate>
            </asp:Repeater>
           
           
        </div>
    </Body>
    <Footer>
         <asp:Button runat="server" Text="Close"  CssClass="btn btn-default" CommandName="Close" />
    </Footer>
</admin:ModalDialog>
<admin:ModalDialog runat="server" Title="Send Test Transactions" ID="dlgTempateTest" OnCommand="Test_Command">
    <Body>
        <div class="row">
        <netpay:ActionNotify runat="server" ID="acTest" />
          <div class="col-lg-6">
                <div class="form-group">
                   Test With <asp:Literal runat="server" Text='<%# Item.ParamName %>' />:
                    <asp:TextBox CssClass="form-control" runat="server" ID="txtTestData" />
                </div>
            </div>
          <div class="col-lg-6">
                <div class="form-group">
                   Type your email: 
                    <asp:TextBox runat="server" CssClass="form-control"  ID="txtTestEmailAddress" />
                </div>
            </div>
        </div>
    </Body>
    <Footer>
        <asp:Button runat="server" Text="Close"  CssClass="btn btn-default" CommandName="Close" />
         <asp:Button runat="server" Text="Send Email" CssClass="btn btn-primary" CommandName="SendTest" />
    </Footer>
</admin:ModalDialog>


