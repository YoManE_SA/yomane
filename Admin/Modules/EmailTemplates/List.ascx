﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.EmailTemplates.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Email Templates">
    <Header></Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" ID="rptList" SaveAjaxState="true" DataKeyNames="FileName" AutoGenerateColumns="false" BubbleLoadEvent="true">
                <Columns>
                    <asp:BoundField HeaderText="Name" DataField="Name" SortExpression="Name" />
                    <asp:BoundField HeaderText="FileName" DataField="FileName" SortExpression="FileName" />
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd="false">
            <asp:Button runat="server" Text="Test Emails" OnClientClick='<%# mdlResults.ShowJSCommand %>'  CssClass="btn btn-primary pull-right"/>
        </admin:DataButtons>
    </Footer>
</admin:ListSection>
<admin:ModalDialog runat="server" ID="mdlResults" Title="Email Templates Tester">
    <Body>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <asp:CheckBoxList runat="server" ID="chkTestTemplates" ClientIDMode="Static" OnDataBound="TestTemplates_DataBound" DataTextField="Name" DataValueField="FileName" RepeatLayout="Flow" CssClass="checkboxList" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    Send To Email:
                    <asp:TextBox runat="server" ID="txtTestEmail" CssClass="form-control" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <asp:PlaceHolder runat="server" ID="phResult" Visible="false">
                    <h3>Results:</h3>
                    <div class="alert alert-info" style="height: 200px; overflow-y: auto; overflow-x: hidden; white-space: pre-line; padding: 5px;">
                        <asp:Literal runat="server" ID="ltResult" Mode="Encode" />
                    </div>
                </asp:PlaceHolder>
            </div>
        </div>

    </Body>
    <Footer>
        <asp:Button runat="server" Text="Close" OnClientClick='<%# mdlResults.HideJSCommand %>' CssClass="btn btn-default" CommandName="Close" />
        <asp:Button runat="server" Text="Start" OnClick="Test_Click" CssClass="btn btn-primary" />
    </Footer>
</admin:ModalDialog>

