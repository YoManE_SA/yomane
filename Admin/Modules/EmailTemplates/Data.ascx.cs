﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;

namespace Netpay.Admin.EmailTemplates
{
    public partial class Data : Controls.TemplateControlBase
    {
        protected Infrastructure.Email.Template Item { get { return TemplatePage.GetItemData<Infrastructure.Email.Template>(); } set { TemplatePage.SetItemData(value); } }
        private Button mTestButton = null, mParametersButton = null;
        protected override void OnInit(EventArgs e)
        {
            mTestButton = new Button() { Text = "Test Email", CommandName = "ShowTestDialog", OnClientClick = dlgTempateTest.ShowJSCommand, CssClass = "btn btn-primary margin-left-5 pull-left" };
            mParametersButton = new Button() { Text = "Show Paramters", CommandName = "ShowParameters", OnClientClick = dlgParamteres.ShowJSCommand, CssClass = "btn btn-primary margin-left-5 pull-left" };
            base.OnInit(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.FormButtons.Controls.Add(mTestButton);
            TemplatePage.FormButtons.Controls.Add(mParametersButton);
            TemplatePage.PageController.SaveItem += Save_Click;
            base.OnLoad(e);
        }

        protected void Language_SelectedIndexChanged(object sender, EventArgs e)
        {
            TemplatePage.PageController.LoadActiveItem();
            if (Item == null) return;
            Item.HtmlData = null;
            Item.Language = ddlLanguage.Text;
            DataBindChildren();
            TemplatePage.PageController.FormView.Update();
        }

        protected override void DataBindChildren()
        {
            if (Item == null) return;
            mTestButton.Enabled = Item.SupportTest;
            base.DataBindChildren();
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            if (Item == null) throw new Exception("Item not exist");
            Item.HtmlData = heHtmlData.Text;
            Item.Save();
        }

        protected void Test_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName != "SendTest") return;
            TemplatePage.PageController.LoadActiveItem();
            if (Item == null) return;
            try {
                Item.Test(txtTestEmailAddress.Text, txtTestData.Text);
                acTest.SetSuccess("Send OK to: " + txtTestEmailAddress.Text);
            } catch (Exception ex) {
                acTest.SetException(ex);
            }
        }

        protected string[] silceFields(int sliceIndex)
        {
            var count = Item.Fields.Length / 4;
            var reminder = Item.Fields.Length % 4;
            return Item.Fields.Skip(sliceIndex * count).Take(count + ((sliceIndex == 2 || sliceIndex ==3) ? reminder : 0)).ToArray();
        }

    }
}