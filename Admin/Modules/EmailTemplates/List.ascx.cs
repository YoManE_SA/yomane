﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;

namespace Netpay.Admin.EmailTemplates
{
	public partial class List : Controls.TemplateControlBase
	{
		protected override void DataBindChildren()
		{
            rptList.DataSource = Infrastructure.Email.Template.Items;
            chkTestTemplates.DataSource = Infrastructure.Email.Template.Items.Where(v=> v.SupportTest);
            base.DataBindChildren();
		}

        protected void TestTemplates_DataBound(object sender, EventArgs e)
        {
            foreach (ListItem v in chkTestTemplates.Items) v.Selected = true;
        }

        protected void Test_Click(object sender, EventArgs e)
        {
            int errorCount = 0;
            var sb = new System.Text.StringBuilder();
            sb.AppendLine("Start Test...");
            foreach (ListItem v in chkTestTemplates.Items) {
                if (v.Selected) {
                    var item = Infrastructure.Email.Template.Get(v.Value);
                    sb.AppendFormat("{0}\r\n{1}: ", DateTime.Now, item.Name);
                    try { 
                        item.Test(txtTestEmail.Text);
                        sb.AppendLine("Success.");
                    } catch (Exception ex) {
                        errorCount++;
                        sb.AppendLine("Fail with error: " + ex.Message);
                    }
                    sb.AppendLine("");
                }
            }
            sb.AppendLine("Finished Test, " + errorCount + " errors.");
            ltResult.Text = sb.ToString();
            phResult.Visible = true;
            mdlResults.UpdatePanel.Update();
        }

    }
}