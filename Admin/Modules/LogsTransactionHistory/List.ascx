﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.LogsTransactionHistory.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Transaction History">
    <Header></Header>

    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" ID="rptList" SaveAjaxState="true" DisableRowSelect="true" AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundField HeaderText="ID" DataField="ID" SortExpression="TransHistory_id" />
                    <asp:BoundField HeaderText="Date" DataField="InsertDate" SortExpression="InsertDate" />
                    <asp:TemplateField HeaderText="Trans ID" SortExpression="">
                        <ItemTemplate>
                            <%# Netpay.Bll.Transactions.History.GetTransID((int)Eval("ID")).Value.ToString() %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Trans Source" SortExpression="">
                        <ItemTemplate>
                            <%# Netpay.Bll.Transactions.History.GetTransID((int)Eval("ID")).Key %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Merchant" SortExpression="Merchant_id">
                        <ItemTemplate>
                            <%# GetMerchantName((int)Eval("Merchant_id")) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Type" DataField="HistoryType" SortExpression="TransHistoryType_id" />
                    <asp:BoundField HeaderText="Succeeded" DataField="IsSucceeded" SortExpression="IsSucceeded" />
                    <asp:BoundField HeaderText="Reference" DataField="ReferenceNumber" SortExpression="ReferenceNumber" />
                    <asp:BoundField HeaderText="Description" DataField="Description" SortExpression="Description" />
                </Columns>
            </admin:AdminList>
        </div>
    </Body>

    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd="false" />
    </Footer>
</admin:ListSection>
