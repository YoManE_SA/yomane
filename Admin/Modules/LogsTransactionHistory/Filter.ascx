﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.LogsTransactionHistory.Filter" %>
<admin:FilterSection runat="server" Title="Status">

    <div class="form-group">
        <asp:Label ID="lblAccount" runat="server" Text="Merchant Name/ID" />
        <admin:AccountPicker runat="server" ID="apAccount" LimitToType="Merchant" UseTargetID="false" />
    </div>

    <div class="form-group">
        Event Type  
        <netpay:EnumDropDown runat="server" ID="EventType" CssClass="form-control" ViewEnumName="Netpay.CommonTypes.TransactionHistoryType" />
    </div>

    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblStatus" runat="server" Text="Date Range" />
                <JQ:DateRange runat="server" ID="rngDate" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label1" runat="server" Text="ID" />
                <JQ:IntRange runat="server" ID="TransHistoryIdRange" />
            </div>
        </div>
    </div>

    <div class="form-group">
        <asp:Label ID="Label2" runat="server" Text="Trans ID" />
        <asp:TextBox runat="server" ID="TransID" CssClass="form-control" />
        <asp:RangeValidator ID="TransIDRangeValidator" runat="server" ControlToValidate="TransID" ErrorMessage="Please insert a number bigger than 0" MinimumValue="1" MaximumValue="<%#Int32.MaxValue%>" Type="Integer" />
    </div>


</admin:FilterSection>
