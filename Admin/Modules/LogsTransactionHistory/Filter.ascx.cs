﻿using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.LogsTransactionHistory
{
    
    public partial class Filter : Controls.TemplateControlBase
    {
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
        {
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Bll.Transactions.History.SearchFilters();
            //The Account ID/NAME input:
            if (apAccount.Value.HasValue)
            {
                var merchant = Bll.Merchants.Merchant.LoadByAccountId(apAccount.Value.Value);
                if (merchant != null)
                sf.MerchantId = new Range<int?>(merchant.ID);
            }

            //The Date Range input:
            sf.Date = rngDate.Value;

            //The TransHistory_id (TransHistory_id coloum in "TransHistory" table) input:
            sf.ID = TransHistoryIdRange.Value;

            //The Trans ID input represents TransPass_id/TransPreAuth_id/TransPending_id/TransFail_id (from TransHistory table).
            int TransIdInt;
            bool tryParse = int.TryParse(TransID.Text, out TransIdInt);
            if (tryParse) sf.TransID = TransIdInt;

            //The Event Type input
            sf.EventType = EventType.Value.ToNullableEnum<Netpay.CommonTypes.TransactionHistoryType>();

            TemplatePage.SetFilter(sf);
        }
    }
}