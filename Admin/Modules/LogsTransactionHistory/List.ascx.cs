﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.LogsTransactionHistory
{
    public partial class List : Controls.TemplateControlBase
    {
        protected Dictionary<int, string> m_merchantNames;

        protected override void OnLoad(EventArgs e)
        {
            rptList.DataBinding += List_DataBinding;
            base.OnLoad(e);
        }

        private void List_DataBinding(object sender, EventArgs e)
        {
            var list = Bll.Transactions.History.Search(TemplatePage.GetFilter<Bll.Transactions.History.SearchFilters>(), rptList);
            if (list != null)
                m_merchantNames = Bll.Accounts.Account.GetMerchantNames((list.Where(i => i.Merchant_id>0).Select(i => i.Merchant_id).ToList()));
            rptList.DataSource = list;
        }

        protected string GetMerchantName(int id)
        {
            if (id <= 0)
                return null;
            string ret;
            if (!m_merchantNames.TryGetValue(id, out ret)) return null;
            return ret;
        }
    }
}