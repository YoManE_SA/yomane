﻿using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.LogsTransactionHistory
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuItem;
        public override string Name { get { return "LogsTransactionHistory"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return ""; } }
        public Module() : base(Bll.Transactions.HistoryLog.Module.Current) { }
        protected override void OnInit(EventArgs e)
        {
            //Create menu item
            menuItem = new ApplicationMenu("Transaction History", "~/Logs/LogsTransactionHistory", null, 160);

            //Register route
            Application.RegisterRoute("Logs/LogsTransactionHistory", "LogsTransactionHistory", typeof(Controls.DataTablePage), module:this);

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
        }
        protected override void OnActivate(EventArgs e)
        {                        
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {                     
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Logs", menuItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuItem.Parent.RemoveChild(menuItem);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "LogsTransactionHistory")
            {
                page.AddControlToList(page.LoadControl("~/Modules/LogsTransactionHistory/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/LogsTransactionHistory/Filter.ascx"));
            }
        }
    }
}