﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Merchants
{
    public partial class Filter : Controls.TemplateControlBase
    {
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.Search += PageController_Search;

            if (!IsPostBack && Page.Request.QueryString.Keys.Count == 0) chklStatus.SelectedValue = "Processing";

            UpdateSearchFilter();
            base.OnLoad(e);
        }

        public List<Infrastructure.MerchantStatus> StatusOptions
        {
            get
            {
                var ret = new List<Infrastructure.MerchantStatus>();
                foreach (ListItem item in chklStatus.Items)
                {
                    if (item.Selected) ret.Add(item.Value.ToNullableEnum<Netpay.Infrastructure.MerchantStatus>().GetValueOrDefault());
                }
                return ret;
            }
        }

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
        {
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {

            var sf = new Bll.Merchants.Merchant.SearchFilters();
            if (Page.Request.QueryString.ToString().Contains("MerchantBlackListID"))
            {
                sf.BlackListSearchFilters = new Bll.Merchants.Merchant.BlackListSearchFilters();

                int Id = int.Parse(Page.Request.QueryString["MerchantBlackListID"]);
                Bll.MerchantBlackList.MerchantBlackListSettings item = Bll.MerchantBlackList.MerchantBlackListSettings.Load(Id);
                if (item.CompanyId != null) sf.BlackListSearchFilters.CompanyID = item.CompanyId;
                sf.BlackListSearchFilters.CompanyName = DataContext.Reader.GetCleanText(item.CompanyName);
                sf.BlackListSearchFilters.CompanyLegalNumber = DataContext.Reader.GetCleanText(item.CompanyLegalNumber);
                sf.BlackListSearchFilters.IDNumber = DataContext.Reader.GetCleanText(item.IDNumber);
                sf.BlackListSearchFilters.FirstName = DataContext.Reader.GetCleanText(item.FirstName);
                sf.BlackListSearchFilters.LastName = DataContext.Reader.GetCleanText(item.LastName);
                sf.BlackListSearchFilters.Phone = DataContext.Reader.GetCleanText(item.Phone);
                sf.BlackListSearchFilters.CompanyFax = DataContext.Reader.GetCleanText(item.CompanyFax);
                sf.BlackListSearchFilters.Cellular = DataContext.Reader.GetCleanText(item.Cellular);
                sf.BlackListSearchFilters.Mail = DataContext.Reader.GetCleanText(item.Mail);
                sf.BlackListSearchFilters.URL = DataContext.Reader.GetCleanText(item.URL);
                TemplatePage.SetFilter(sf);
            }

            else
            {
                //Account picker input - include id range ipnut [V]           
                sf.ID = apAccount.Value.HasValue ? new Range<int?>(apAccount.Value) : rngID.Value;

                //Status checkboxes input - [V] - paging doesn't work good
                //sf.Status = new List<MerchantStatus>();

                //foreach (ListItem l in chklStatus.Items)
                //    if (l.Selected) sf.Status.Add(l.Value.ToNullableEnum<Netpay.Infrastructure.MerchantStatus>().GetValueOrDefault());

                sf.Status = StatusOptions;

                //if (!IsPostBack && Page.Request.QueryString.Keys.Count == 0)
                //{
                //    sf.Status.Add(Netpay.Infrastructure.MerchantStatus.Processing);
                //}

                //Open date range input [V]
                sf.OpenDate = rndOpenDate.Value;

                //Close date range input[V]
                sf.CloseDate = rndCloseDate.Value;

                //Industry dropdown input [V]
                sf.IndustryID = ddlIndustry.Value.ToNullableInt();

                //Department dropdown input [V]
                sf.Department = ddlDepartment.Value.ToNullableInt();

                TemplatePage.SetFilter(sf);
            }
        }

        protected void chklStatus_DataBound(object sender, EventArgs e)
        {
            if (TemplatePage.Request.QueryString.Count == 0)
            {
                foreach (ListItem item in chklStatus.Items)
                {
                    if (item.Text == "Processing") item.Selected = true;
                }

                UpdateSearchFilter();
                var filter = TemplatePage.GetFilter<Bll.Merchants.Merchant.SearchFilters>();
                filter.Status = StatusOptions;
                TemplatePage.SetFilter(filter);
            }
            else
            {
                var url = TemplatePage.Request.QueryString.ToString();
                foreach (ListItem item in chklStatus.Items)
                {
                    if (url.Contains("=" + item.Text))
                        item.Selected = true;
                }

                var filter = TemplatePage.GetFilter<Bll.Merchants.Merchant.SearchFilters>();
                filter.Status = StatusOptions;
                TemplatePage.SetFilter(filter);
            }
        }
    }
}