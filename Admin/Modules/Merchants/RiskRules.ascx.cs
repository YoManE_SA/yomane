﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Admin.Merchants
{
    public partial class RiskRules : Controls.AccountControlBase
    {
        protected Bll.Risk.MerchantRiskRule ItemData { get; set; }

        protected override void DataBindChildren()
        {
            if (ItemData == null) ItemData = new Bll.Risk.MerchantRiskRule(0);
            if (Account != null && Account.MerchantID != null)
                rptMerchantRules.DataSource = Bll.Risk.MerchantRiskRule.Search(Account.MerchantID.Value);
                                    
            base.DataBindChildren();
        }

        protected void Edit_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "AddItem":
                    //When pressing "Add" before saving the merchant there was an exception "null reference" 
                    //so here when Account is null just return back.
                    if (Account == null)
                    {
                        return;
                    }

                    //Permission check
                    if (Netpay.Infrastructure.Security.Login.Current != null && Netpay.Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                        ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Risk.MerchantRiskRule.SecuredObject, PermissionValue.Add);

                    ItemData = new Netpay.Bll.Risk.MerchantRiskRule(Account.MerchantID.GetValueOrDefault());
                    ItemData.CreditType = 255; // Set the initial CreditType value to [All]
                    dlgEdit.DataBind();
                    dlgEdit.RegisterShow();
                    break;

                case "Edit":
                    ItemData = Netpay.Bll.Risk.MerchantRiskRule.Load(e.CommandArgument.ToNullableInt().GetValueOrDefault());
                    dlgEdit.DataBind();
                    dlgEdit.RegisterShow();
                    break;

                case "Save":
                    ItemData = Netpay.Bll.Risk.MerchantRiskRule.Load(hfID.Value.ToNullableInt().GetValueOrDefault(0));
                    if (ItemData == null) ItemData = new Netpay.Bll.Risk.MerchantRiskRule(Account.MerchantID.GetValueOrDefault());
                                    
                    ItemData.ReplySource = ddlReplySource.Value.ToInt32();
                    ItemData.CCRM_Hours = int.Parse(CCRM_HourNumber.Text) * int.Parse(CCRM_HourFactor.SelectedValue);
                    ItemData.CCRM_BlockHours = int.Parse(CCRM_BlockHourNumber.Text) * int.Parse(CCRM_BlockHourFactor.SelectedValue);
                    ItemData.PaymentMethod = (short?)ddlPaymentMethod.Value.ToNullableInt();
                    ItemData.CreditType = (byte)ddlCreditType.Value.ToInt32();
                    ItemData.WhitelistLevel = ddlWhiteListLevel.Value == "-1" ? null : ddlWhiteListLevel.Value.ToNullableInt32();
                    ItemData.MaxAmount = txtMaxAmount.Text.ToDecimal(0);
                    ItemData.CurrencyID = ddlCurrencyID.Value.ToInt32();
                    ItemData.MaxCount = txtMaxCount.Text.ToInt32();
                    ItemData.MaxAmountReplyCode = ddlMaxAmountReplyCode.Text;
                    ItemData.MaxCountReplyCode = ddlMaxCountReplyCode.Text;
                    ItemData.ApplyVT = chkApplyVT.Checked;
                    ItemData.IsActive = chkIsActive.Checked;
                    ItemData.Save();
                    DataBindChildren();
                    dlgEdit.RegisterHide();
                    upList.Update();
                    break;

                case "Delete":
                    ItemData = Netpay.Bll.Risk.MerchantRiskRule.Load(hfID.Value.ToNullableInt().GetValueOrDefault(0));
                    if (ItemData == null) return;
                    ItemData.Delete();
                    DataBindChildren();
                    dlgEdit.RegisterHide();
                    upList.Update();

                    break;
            }
        }

        protected void dlgEdit_DataBinding(object sender, EventArgs e)
        {
            if (ItemData == null) return;

            if ((ItemData.CCRM_DAYS * 24) - (ItemData.CCRM_Hours) == 0)
            {
                CCRM_HourFactor.SelectedValue = "24";
                CCRM_HourNumber.Value = ItemData.CCRM_DAYS;
            }
            else
            {
                CCRM_HourFactor.SelectedValue = "1";
                CCRM_HourNumber.Value = ItemData.CCRM_Hours;
            }

            if ((ItemData.CCRM_BLOCKDAYS * 24) - (ItemData.CCRM_BlockHours) == 0)
            {
                CCRM_BlockHourFactor.SelectedValue = "24";
                CCRM_BlockHourNumber.Value = ItemData.CCRM_BLOCKDAYS;
            }
            else
            {
                CCRM_BlockHourFactor.SelectedValue = "1";
                CCRM_BlockHourNumber.Value = ItemData.CCRM_BlockHours;
            }
        }
    }
}