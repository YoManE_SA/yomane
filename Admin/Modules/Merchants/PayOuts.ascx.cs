﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;

namespace Netpay.Admin.Merchants
{
    public partial class PayOuts : Controls.AccountControlBase
    {
        public string ErrorText { get; set; }

        protected Netpay.Bll.Merchants.Merchant ItemData
        {
            get
            {
                return AccountPage.Account as Netpay.Bll.Merchants.Merchant;
            }
            private set
            {
                AccountPage.Account = value;
            }
        }

        public bool bSOValid
        {
            get
            {
                return Bll.Transactions.Transaction.CheckIFMerchantAlreadyHadReserveOrRelease(ItemData.ID, ItemData.SecurityPeriod);
            }
        }

        public bool IsPayingDatesHaveValue
        {
            get
            {
                if (ItemData == null || ItemData.AccountID == 0)
                    return false;
                else
                {
                    if (ItemData.PayingDates1.Length == 6 || ItemData.PayingDates2.Length == 6 || ItemData.PayingDates3.Length == 6)
                    {
                        return true;
                    }
                    else return false;
                }
            }
        }

        public bool bChange { get; set; }

        public static Bll.Merchants.Merchant.MerchantPayDate MerchantPayDate1 { get; set; }
        public static Bll.Merchants.Merchant.MerchantPayDate MerchantPayDate2 { get; set; }
        public static Bll.Merchants.Merchant.MerchantPayDate MerchantPayDate3 { get; set; }

        public static Dictionary<int, Bll.Settlements.MerchantSettings> MerchantSettings { get; set; }
        public static List<int> MerchantCurrenciesIds { get; set; }


        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += PageController_SaveItem;

            if (ItemData != null)
            {
                //Get the currenct merchant settings.
                MerchantSettings = Bll.Settlements.MerchantSettings.LoadForMerchant(ItemData.ID);

                //Get the currencies list that have settings for this merchant
                MerchantCurrenciesIds = MerchantSettings.Select(x => x.Key).ToList();
            }

            base.OnLoad(e);
        }

        protected override void DataBind(bool raiseOnDataBinding)
        {

            if (ItemData == null) return;

            //Get the currenct merchant settings.
            MerchantSettings = Bll.Settlements.MerchantSettings.LoadForMerchant(ItemData.ID);

            //Get the currencies list that have settings for this merchant
            MerchantCurrenciesIds = MerchantSettings.Select(x => x.Key).ToList();

            ddlCurrency.IncludeCurrencyIDs = string.Join(",", Netpay.Bll.Currency.Cache.Where(x => (!MerchantCurrenciesIds.Contains(x.ID))).Select(y => y.ID.ToString()).ToList());

            //Bind the Payout schedule radio button list option
            if (IsPayingDatesHaveValue)
            {
                rblPayoutSchedule.SelectedValue = "TransactionsBetween";
            }
            else
            {
                rblPayoutSchedule.SelectedValue = "HoldingMargin";
            }

            // Payout schedule databind
            if (ItemData.PayingDates1 != null && ItemData.PayingDates1.Length == 6)
            {
                MerchantPayDate1 = Bll.Merchants.Merchant.MerchantPayDate.Parse(new string[] { ItemData.PayingDates1 }).SingleOrDefault();

                //PayingDatesBetween1
                if (PayingDatesBetween1.Items.Cast<ListItem>().Select(x => x.Text).Contains(MerchantPayDate1.From.ToString("00")))
                {
                    PayingDatesBetween1.SelectedValue = MerchantPayDate1.From.ToString("00");
                }
                else
                {
                    ErrorText += "MerchantPayDate1 \"From\" holds wrong date, please choose correct date values and save.<br>";
                }

                //PayingDatesAnd1
                if (PayingDatesAnd1.Items.Cast<ListItem>().Select(x => x.Text).Contains(MerchantPayDate1.To.ToString("00")))
                {
                    PayingDatesAnd1.SelectedValue = MerchantPayDate1.To.ToString("00");
                }
                else
                {
                    ErrorText += "MerchantPayDate1 \"To\" holds wrong date, please choose correct date values and save.<br>";
                }

                //CreditOn1
                if (CreditOn1.Items.Cast<ListItem>().Select(x => x.Text).Contains(MerchantPayDate1.When.ToString("00")))
                {
                    CreditOn1.SelectedValue = MerchantPayDate1.When.ToString("00");
                }
                else
                {
                    ErrorText += "MerchantPayDate1 \"Credit On\" holds wrong date, please choose correct date values and save.<br>";
                }
            }
            else
            {
                PayingDatesBetween1.SelectedValue = "";
                PayingDatesAnd1.SelectedValue = "";
                CreditOn1.SelectedValue = "";
            }

            if (ItemData.PayingDates2 != null && ItemData.PayingDates2.Length == 6)
            {
                MerchantPayDate2 = Bll.Merchants.Merchant.MerchantPayDate.Parse(new string[] { ItemData.PayingDates2 }).SingleOrDefault();
                if (PayingDatesBetween2.Items.Cast<ListItem>().Select(x => x.Text).Contains(MerchantPayDate2.From.ToString("00")))
                {
                    PayingDatesBetween2.SelectedValue = MerchantPayDate2.From.ToString("00");
                }
                else
                {
                    ErrorText += "MerchantPayDate2 \"From\" holds wrong date, please choose correct date values and save.<br>";
                }

                if (PayingDatesAnd2.Items.Cast<ListItem>().Select(x => x.Text).Contains(MerchantPayDate2.To.ToString("00")))
                {
                    PayingDatesAnd2.SelectedValue = MerchantPayDate2.To.ToString("00");
                }
                else
                {
                    ErrorText += "MerchantPayDate2 \"To\" holds wrong date, please choose correct date values and save.<br>";
                }

                if (CreditOn2.Items.Cast<ListItem>().Select(x => x.Text).Contains(MerchantPayDate2.When.ToString("00")))
                {
                    CreditOn2.SelectedValue = MerchantPayDate2.When.ToString("00");
                }
                else
                {
                    ErrorText += "MerchantPayDate2 \"CreditOn\" holds wrong date, please choose correct date values and save.<br>";
                }
            }
            else
            {
                PayingDatesBetween2.SelectedValue = "";
                PayingDatesAnd2.SelectedValue = "";
                CreditOn2.SelectedValue = "";
            }

            if (ItemData.PayingDates3 != null && ItemData.PayingDates3.Length == 6)
            {
                MerchantPayDate3 = Bll.Merchants.Merchant.MerchantPayDate.Parse(new string[] { ItemData.PayingDates3 }).SingleOrDefault();

                if (PayingDatesBetween3.Items.Cast<ListItem>().Select(x => x.Text).Contains(MerchantPayDate3.From.ToString("00")))
                {
                    PayingDatesBetween3.SelectedValue = MerchantPayDate3.From.ToString("00");
                }
                else
                {
                    ErrorText += "MerchantPayDate3 \"From\" holds wrong date, please choose correct date values and save.<br>";
                }

                if (PayingDatesAnd3.Items.Cast<ListItem>().Select(x => x.Text).Contains(MerchantPayDate3.To.ToString("00")))
                {
                    PayingDatesAnd3.SelectedValue = MerchantPayDate3.To.ToString("00");
                }
                else
                {
                    ErrorText += "MerchantPayDate3 \"To\" holds wrong date, please choose correct date values and save.<br>";
                }

                if (CreditOn3.Items.Cast<ListItem>().Select(x => x.Text).Contains(MerchantPayDate3.When.ToString("00")))
                {
                    CreditOn3.SelectedValue = MerchantPayDate3.When.ToString("00");
                }
                else
                {
                    ErrorText += "MerchantPayDate3 \"CreditOn\" holds wrong date, please choose correct date values and save.<br>";
                }
            }
            else
            {
                PayingDatesBetween3.SelectedValue = "";
                PayingDatesAnd3.SelectedValue = "";
                CreditOn3.SelectedValue = "";
            }


            //Action to take radio button list databind
            //Check if "None"
            if (ItemData.RRState == 0) rblActionToTake.SelectedValue = "0"; //None

            //Disable "Hold a single occurrence" if needed
            if (bSOValid) (rblActionToTake.Items.Cast<ListItem>().Where(x => x.Value == "2").SingleOrDefault() as ListItem).Enabled = false;

            //Check if "Hold Dynamically"
            if (ItemData.RRState == 1)
            {
                rblActionToTake.SelectedValue = "1";
                ActionMultiView.ActiveViewIndex = 1;
            }

            //Check if "Hold a fixed amount"
            if (ItemData.RRState == 3)
            {
                rblActionToTake.SelectedValue = "3";
                ActionMultiView.ActiveViewIndex = 2;
            }

            //Check if "Record an External Hold"
            if (ItemData.RRState == 4)
            {
                rblActionToTake.SelectedValue = "4";
                ActionMultiView.ActiveViewIndex = 3;
            }

            if (!string.IsNullOrEmpty(ErrorText))
            {
                if (string.IsNullOrEmpty((Page as Controls.DataTablePage).FormActionNotify.Message))
                    (Page as Controls.DataTablePage).FormActionNotify.SetMessage(ErrorText, true);
            }
            base.DataBind(raiseOnDataBinding);
        }


        protected void PageController_SaveItem(object sender, EventArgs e)
        {

            if (Bll.Settlements.MerchantSettings.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Edit))
            {

                if (ItemData == null)
                    ItemData = new Netpay.Bll.Merchants.Merchant();

                //Set variables used during save
                //bChange property
                if (string.IsNullOrEmpty(rblActionToTake.SelectedValue)) throw new Exception("Error - Action to take drop down in Payouts tab has no selected value. Save didn't invoked.");

                if (ItemData.RRState != int.Parse(rblActionToTake.SelectedValue))
                {
                    bChange = true;
                }
                else if (rblActionToTake.SelectedValue == "3")
                {
                    if (ItemData.RRKeepAmount != decimal.Parse(txtFixedAmount.Text) || (ItemData.RRKeepCurrency != byte.Parse(ddlFixedAmountCur.SelectedValue)))
                    {
                        bChange = true;
                    }
                }
                else if (rblActionToTake.SelectedValue == "4")
                {
                    if (ItemData.RRKeepAmount != decimal.Parse(txtExternalHoldAmount.Text) || (ItemData.RRKeepCurrency != byte.Parse(ddlExternalHoldAmountCur.SelectedValue)))
                    {
                        bChange = true;
                    }
                }

                //Save the Settlements Settings section.
                if (ItemData.ID > 0)
                {
                    //If there is not value to MerchantSettings try to load again.
                    if (MerchantSettings == null || MerchantSettings.Count == 0)
                    {
                        MerchantSettings = Bll.Settlements.MerchantSettings.LoadForMerchant(ItemData.ID);
                    }

                    //Invoke the MerchantSettings save operation only if there is at least one item at MerchantSettings List.
                    if (MerchantSettings.Count > 0)
                    {
                        foreach (RepeaterItem ri in rptRolling.Items)
                        {
                            int ItemCurrencyId = int.Parse((ri.FindControl("hdCurrency") as HiddenField).Value);

                            MerchantSettings.Where(x => x.Key == ItemCurrencyId).Single().Value.IsShowToSettle = (ri.FindControl("chkShowInSettlePayouts") as CheckBox).Checked;
                            MerchantSettings.Where(x => x.Key == ItemCurrencyId).Single().Value.IsAutoInvoice = (ri.FindControl("chkIsAutoInvoice") as CheckBox).Checked;
                            MerchantSettings.Where(x => x.Key == ItemCurrencyId).Single().Value.IsWireExcludeDebit = (ri.FindControl("chkIsWireExcludeDebit") as CheckBox).Checked;
                            MerchantSettings.Where(x => x.Key == ItemCurrencyId).Single().Value.IsWireExcludeRefund = (ri.FindControl("chkIsWireExcludeRefund") as CheckBox).Checked;
                            MerchantSettings.Where(x => x.Key == ItemCurrencyId).Single().Value.IsWireExcludeFee = (ri.FindControl("chkIsWireExcludeFee") as CheckBox).Checked;
                            MerchantSettings.Where(x => x.Key == ItemCurrencyId).Single().Value.IsWireExcludeChb = (ri.FindControl("chkIsWireExcludeChb") as CheckBox).Checked;
                            MerchantSettings.Where(x => x.Key == ItemCurrencyId).Single().Value.IsWireExcludeCashback = (ri.FindControl("chkIsWireExcludeCashback") as CheckBox).Checked;

                            decimal MinSetAmount;
                            if (decimal.TryParse((ri.FindControl("txtMinSettlementAmount") as TextBox).Text, out MinSetAmount))
                            {
                                //currentItem.MinSettlementAmount = MinSetAmount;
                                MerchantSettings.Where(x => x.Key == ItemCurrencyId).Single().Value.MinSettlementAmount = MinSetAmount;
                            }
                            else
                            {
                                throw new Exception("Payouts tab Settlement amount cannot be parsed to decimal value.");
                            }
                        }

                        TemplatePage.SetItemData(MerchantSettings);
                    }

                    //Save the Payout Settings section


                    AccountPage.Account.SetWireProvider(ddlProvider.SelectedValue); //The wire provider is saved in the Account table.
                    ItemData.IsChargeVAT = BoolDropDownChargeVat.BoolValue.Value;
                    if (!(ddlInvoiceProvider.SelectedValue == "0")) ItemData.BillingCompanysId = int.Parse(ddlInvoiceProvider.SelectedValue);
                    decimal setPer;
                    if (decimal.TryParse(txtPayPercent.Text, out setPer))
                    {
                        ItemData.PayPercent = setPer;
                    }
                    ItemData.PaymentRecieveCurrency = int.Parse(ddlDefaultCurrency.SelectedValue);


                    //Save the Payout Schedule section
                    //Holding Margin ...
                    if (rblPayoutSchedule.SelectedValue == "HoldingMargin")
                    {
                        ItemData.PayingDaysMarginInitial = (byte)InitialHoldingMarginSpinner.Value.Value;
                        ItemData.PayingDaysMargin = (byte)HoldingMarginSpinner.Value.Value;
                        ItemData.PayingDates1 = "";
                        ItemData.PayingDates2 = "";
                        ItemData.PayingDates3 = "";
                    }

                    //Transaction between ... 
                    else if (rblPayoutSchedule.SelectedValue == "TransactionsBetween")
                    {
                        if (PayingDatesBetween1.SelectedValue != "" && PayingDatesAnd1.SelectedValue != "" && CreditOn1.SelectedValue != "")
                        {
                            ItemData.PayingDates1 = PayingDatesBetween1.SelectedValue + PayingDatesAnd1.SelectedValue + CreditOn1.SelectedValue;
                        }

                        if (PayingDatesBetween2.SelectedValue != "" && PayingDatesAnd2.SelectedValue != "" && CreditOn2.SelectedValue != "")
                        {
                            ItemData.PayingDates2 = PayingDatesBetween2.SelectedValue + PayingDatesAnd2.SelectedValue + CreditOn2.SelectedValue;
                        }

                        if (PayingDatesBetween3.SelectedValue != "" && PayingDatesAnd3.SelectedValue != "" && CreditOn3.SelectedValue != "")
                        {
                            ItemData.PayingDates3 = PayingDatesBetween3.SelectedValue + PayingDatesAnd3.SelectedValue + CreditOn3.SelectedValue;
                        }
                    }


                    //Save the Rolling Reserve section
                    ItemData.SecurityDeposit = decimal.Parse(txtHoldPercentage.Text);
                    ItemData.SecurityPeriod = (short)SpinnerHoldPeriod.Value.Value;
                    ItemData.RREnabled = rblActionToTake.SelectedValue == "1" && RREnable.Checked;
                    ItemData.RRAutoRet = rblActionToTake.SelectedValue == "1" && RRAutoRet.Checked;
                    ItemData.RRState = byte.Parse(rblActionToTake.SelectedValue);
                    if (rblActionToTake.SelectedValue == "3")
                    {
                        ItemData.RRKeepAmount = decimal.Parse(txtFixedAmount.Text);
                        ItemData.RRKeepCurrency = byte.Parse(ddlFixedAmountCur.SelectedValue);
                    }
                    else if (rblActionToTake.SelectedValue == "4")
                    {
                        ItemData.RRKeepAmount = decimal.Parse(txtExternalHoldAmount.Text);
                        ItemData.RRKeepCurrency = byte.Parse(ddlExternalHoldAmountCur.SelectedValue);
                    }
                    else
                    {
                        ItemData.RRKeepAmount = 0;
                        ItemData.RRKeepCurrency = 0;
                    }
                    ItemData.RRComment = txtRRComment.Text;

                    ItemData.Save(false);


                    //Create payment if needed.
                    if (bChange)
                    {
                        if (ItemData.RRPayID != 0)
                        {
                            Bll.Totals.CPayment xPayment = Bll.Totals.CPayment.Create(ItemData.ID, ItemData.RRKeepCurrency);
                            xPayment.AddRetReserve(ItemData.RRPayID, true);
                            xPayment.UpdatePayment(xPayment.PayDate, null, true);
                            Bll.Merchants.Merchant.UpdateMerchantRollingReserveID(ItemData.ID, xPayment.RRID);
                        }
                        else if (ItemData.RRState == 4 && ItemData.RRPayID == 0)
                        {
                            Bll.Totals.CPayment xPayment = Bll.Totals.CPayment.Create(ItemData.ID, ItemData.RRKeepCurrency);
                            xPayment.RollingReserve = ItemData.RRKeepAmount;
                            xPayment.UpdatePayment(xPayment.PayDate, null, true);
                            Bll.Merchants.Merchant.UpdateMerchantRollingReserveID(ItemData.ID, xPayment.RRID);
                        }
                    }
                }
            }
            //End of save
        }

        protected void AddCurrencySettings(object sender, CommandEventArgs e)
        {
            int currencyId = int.Parse(ddlCurrency.SelectedValue);
            var newMerchantSettings = new Bll.Settlements.MerchantSettings(ItemData.ID, (CommonTypes.Currency)currencyId);
            newMerchantSettings.Save();

            DataBind();
            
            //DataBind Fees tab , because that tab is using the same "MerchantSettings" object as Payouts tab 
            TemplatePage.FormTabs.Tabs.Where(x => x.ID == "Fees").SingleOrDefault().DataBind();

            //Create 2 columns if neened 
            if (System.Web.UI.ScriptManager.GetCurrent(Page).IsInAsyncPostBack)
                Admin.Controls.GlobalControls.JQueryControls.JQueryControlHelper.RegisterJQueryStartUp(TemplatePage.PageController.FormView, "CreateTwoColumnsIfNeeded('" + TemplatePage.PageController.ClientID + "');");

            TemplatePage.PageController.FormView.Update();
        }

        protected void UpdateTransactions(object sender, CommandEventArgs e)
        {
            int nCompanyID = int.Parse(e.CommandArgument.ToString());
            Bll.Transactions.Transaction.RecalcMerchantPo(nCompanyID, 0);
        }

        protected void rblActionToTake_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (rblActionToTake.SelectedValue)
            {
                case "0":
                    ActionMultiView.ActiveViewIndex = 0;
                    rblActionToTake.SelectedIndex = 0;
                    upActionToTake.Update();
                    break;

                case "2":
                    ActionMultiView.ActiveViewIndex = 0;
                    rblActionToTake.SelectedIndex = 1;
                    upActionToTake.Update();
                    break;

                case "1":
                    ActionMultiView.ActiveViewIndex = 1;
                    rblActionToTake.SelectedIndex = 2;
                    upActionToTake.Update();
                    break;

                case "3":
                    ActionMultiView.ActiveViewIndex = 2;
                    rblActionToTake.SelectedIndex = 3;
                    upActionToTake.Update();
                    break;

                case "4":
                    ActionMultiView.ActiveViewIndex = 3;
                    rblActionToTake.SelectedIndex = 4;
                    upActionToTake.Update();
                    break;
            }
        }

        protected void rblPayoutSchedule_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblPayoutSchedule.SelectedValue == "HoldingMargin")
            {
                mvPayoutSchedule.ActiveViewIndex = 0;
                upPayoutSchedule.Update();
            }
            else
            {
                mvPayoutSchedule.ActiveViewIndex = 1;
                upPayoutSchedule.Update();
            }
        }
    }
}