﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TransactionHistory.ascx.cs" Inherits="Netpay.Admin.Modules.Merchants.TransactionHistory" %>
<admin:ListSection ID="lstSection" runat="server" Title="Transactions">
    <Header>
        <admin:LegendColors runat="server" Items='<%# Netpay.Bll.Transactions.Transaction.TransactionStatusColor %>' FloatRight="true" />
    </Header>
    <Body>
        <div class="table-responsive">
            <table class="table table-striped">
                <NP:PagedRepeater runat="server" ID="rptListT" PageSize="20">
                    <HeaderTemplate>
                        <thead>
                            <tr>
                                <th>&nbsp</th>
                                <th>ID</th>
                                <th>Date</th>
                                <th>Merchant</th>
                                <th>Metdhod</th>
                                <th>Type</th>
                                <th>Ins</th>
                                <th>Amount</th>
                                <th>Source</th>
                                <th>Status</th>
                                <th>Debit</th>
                                <th>Reply</th>
                            </tr>
                        </thead>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%--<tr class="<%# Container.ItemIndex % 2 == 0 ? "alternate" : "" %>" onclick="<%# rptListTransaction.GetSelectItemEventReference(Container.ItemIndex) %>" style="cursor: pointer;">--%>
                        <td><span style="background-color: <%# Netpay.Bll.Transactions.Transaction.TransactionStatusColor.MapHtmlColor((TransactionStatus)Eval("Status")) %>;" class="legend-item"><%# (bool)Eval("IsTest") ? "X" :"&nbsp;" %></span></td>
                        <td>
                        <asp:Literal runat="server" ID="ltId" Text='<%# Eval("ID") %>' />
                            <asp:HiddenField runat="server" ID="hfStatus" Value='<%# Eval("Status") %>' />
                        </td>
                        <td><%# Eval("InsertDate") %></td>
                        <td><%# Eval("Merchant.Name") %></td>
                        <td>
                            <asp:Image runat="server" ImageUrl='<%# "/NPCommon/ImgPaymentMethod/23X12/" + (int)Eval("PaymentMethodID") + ".gif" %>' ImageAlign="Middle" />
                            <asp:Image runat="server" ImageUrl='<%# "/NPCommon/ImgCountry/18X12/" + Eval("IssuerCountryIsoCode") + ".gif" %>' ImageAlign="Middle" Visible='<%# !string.IsNullOrEmpty((string)Eval("IssuerCountryIsoCode")) %>' />
                            <%# Eval("PaymentMethodDisplay") %>
                        </td>
                        <td><%# Eval("CreditType") %></td>
                        <td><%# Eval("Installments") %></td>
                        <td class="<%# (decimal)Eval("SignedAmount") < 0 ? "numericMinus" : "" %>"><%# new Money((decimal)Eval("SignedAmount"), (int)Eval("CurrencyID")).ToIsoString() %></td>
                        <td><%# Eval("TransactionSource") %></td>
                        <td><%# Eval("MoneyTransferText") %></td>
                        <td>
                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# "/NPCommon/ImgDebitCompanys/23X12/" + Eval("DebitCompanyID") + ".gif" %>' ImageAlign="Middle" Visible='<%# ((int?) Eval("DebitCompanyID")) != null %>' />
                            <%# Eval("DebitCompany.Name") %>
                        </td>
                        <td><%# Eval("ReplyCode") %></td>
                        </tr>
                    </ItemTemplate>
                </NP:PagedRepeater>
            </table>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" ID="transactionhistorydatabuttons" PagedControlID="rptListT" EnableExport="false" EnableAdd="false" />
    </Footer>
</admin:ListSection>
