﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Merchants.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Merchants">
    <Header>
        <admin:LegendColors runat="server" Items='<%# Netpay.Bll.Merchants.Merchant.MerchantStatusColor %>' FloatRight="true" />
    </Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList OnRowDataBound="rptList_RowDataBound" runat="server" SaveAjaxState="true" ID="rptList" DataKeyNames="AccountID" AutoGenerateColumns="false" BubbleLoadEvent="true">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Label ID="LegendLabel" runat="server" BackColor='<%# System.Drawing.ColorTranslator.FromHtml(Netpay.Bll.Merchants.Merchant.MerchantStatusColor.MapHtmlColor((MerchantStatus)Eval("ActiveStatus"))) %>' CssClass="legend-item" />
                            <asp:HiddenField runat="server" ID="hfOwnedBy" Value='<%# (bool)Eval("IsCompanyTerminal") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ID" SortExpression="ID">
                        <ItemTemplate>
					    <%# Eval("AccountID") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Name" DataField="Name" SortExpression="Name" />
                    <asp:BoundField HeaderText="Unsettled T'" DataField="DateLastTransPass" SortExpression="DateLastTransPass" DataFormatString="{0:d}" NullDisplayText="---" />
                    <asp:BoundField HeaderText="Pending T'" DataField="DateLastTransPending" SortExpression="DateLastTransPending" DataFormatString="{0:d}" NullDisplayText="---" />
                    <asp:BoundField HeaderText="Pre-Authorized T'" DataField="DateLastTransPreAuth" SortExpression="DateLastTransPreAuth" DataFormatString="{0:d}" NullDisplayText="---" />
                    <asp:BoundField HeaderText="Rejected T'" DataField="DateLastTransFail" SortExpression="DateLastTransFail" DataFormatString="{0:d}" NullDisplayText="---" />
                    <%--<asp:BoundField HeaderText="Number" DataField="Number" SortExpression="c.CustomerNumber" />--%>
                    <%--<asp:BoundField HeaderText="Contact First Name" DataField="ContactFirstName" SortExpression="ContactFirstName" />
                    <asp:BoundField HeaderText="Contact Last Name" DataField="ContactLastName" SortExpression="ContactLastName" />--%>
                    <%--<asp:BoundField HeaderText="Account Manager" DataField="AccountManager" SortExpression="AccountManager" />--%>
                    <asp:TemplateField HeaderText="Transaction History">
                        <ItemTemplate>
                            <asp:HyperLink  CssClass="btn-xs btn-info" runat="server" 
                                Text="Show" 
                                NavigateUrl='<%# GetTransactionHistory(Container.DataItem as Netpay.Bll.Merchants.Merchant) %>' 
                                Target="_blank"
                                onclick ="event.cancelBubble=true;"/>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd='<%# Netpay.Bll.Merchants.Merchant.SecuredObject.HasPermission(Netpay.Infrastructure.Security.PermissionValue.Add) %>' />
    </Footer>
</admin:ListSection>
