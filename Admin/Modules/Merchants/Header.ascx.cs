﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;

namespace Netpay.Admin.Merchants
{
	public partial class Header : System.Web.UI.UserControl
	{
		protected Netpay.Web.Admin_AccountPage AccountPage { get { return Page as Netpay.Web.Admin_AccountPage; } }
		protected override void OnInit(EventArgs e)
		{
			if (AccountPage.Account == null && Request["id"] != null)
				AccountPage.Account = Netpay.Bll.Merchants.Merchant.Load(Request["id"].ToNullableInt());
			base.OnInit(e);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack) {
				var ItemData = AccountPage.Account as Netpay.Bll.Merchants.Merchant;
				//tbTop.LoadMerchantTabs(ItemData.AccountID);
				if (ItemData != null) {
					ltMerchantName.Text = string.Format("{0} | {1}", ItemData.ID, ItemData.Name);
					hlEmail.NavigateUrl = string.Format("mailto:{0}", ItemData.EmailAddress);
					hlDeposits.NavigateUrl = string.Format("../charge_browser.aspx?CompanyId={0}", ItemData.ID);
					ddlStatus.Value = ((int)ItemData.ActiveStatus).ToString();
				}
			}
		}

		protected void Status_Changes(object sender, EventArgs e)
		{
			var ItemData = AccountPage.Account as Netpay.Bll.Merchants.Merchant;
			ItemData.ActiveStatus = ddlStatus.SelectedValue.ToNullableEnumByValue<Netpay.Infrastructure.MerchantStatus>().GetValueOrDefault();
			ItemData.Save();
		}
	
		protected void MerchantUI_Click(object sender, EventArgs e)
		{
			ExternalLogin(AccountPage.Account.MerchantID.GetValueOrDefault(), false, false);
		}


        public static void ExternalLogin(int loginId, bool devCenter, bool isTest)
		{
            HttpContext.Current.Response.Write("<script>window.open('" + Module.ExternalLoginUrl(loginId, devCenter, isTest) + "');</script>");
        }
    }
}