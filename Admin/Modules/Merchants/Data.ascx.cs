﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;

namespace Netpay.Admin.Merchants
{
    public partial class Data : Controls.AccountControlBase
    {
        //protected Netpay.Bll.Merchants.Merchant ItemData { get { return Account as Netpay.Bll.Merchants.Merchant; } }
        // Get the value of the global account account into the ItemData property
        protected Netpay.Bll.Merchants.Merchant ItemData
        {
            get
            {
                return AccountPage.Account as Netpay.Bll.Merchants.Merchant;
            }
            private set
            {
                AccountPage.Account = value;
            }
        }
        protected Netpay.Bll.Merchants.Group EditGroup;

        protected override void OnLoad(EventArgs e)
        {
            EditGroup = new Netpay.Bll.Merchants.Group();
            TemplatePage.PageController.SaveItem += PageController_SaveItem;

            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            if (ItemData == null) return;
           
            base.DataBindChildren();
            //if (ItemData.RefIdentityID.HasValue) ddlAppIdentity.SelectedValue = ItemData.RefIdentityID.Value.ToString();
        }

        protected void PageController_SaveItem(object sender, EventArgs e)
        {            
            if (!Page.IsValid)
                return;

            if (ItemData == null)
                ItemData = new Netpay.Bll.Merchants.Merchant();

            //Edit Login
            var dataTab = TemplatePage.FormTabs.FindControl("Data") as Controls.GlobalControls.JQueryControls.TabView;
            var editLoginControl = dataTab.Controls.OfType<Netpay.Admin.Security.EditLogin>().SingleOrDefault();
            if (editLoginControl == null) throw new Exception("Edit login control wasn't found during merchant save.");dataTab.Controls.OfType<Netpay.Admin.Security.EditLogin>().SingleOrDefault();

            //Set UserName and EmailAddress in the Login entity 
            //And also in the Merchant entity - should be identical values
            ItemData.Login.UserName = (editLoginControl.FindControl("txtUserName") as TextBox).Text;
            ItemData.UserName = ItemData.Login.UserName;

            ItemData.Login.EmailAddress = (editLoginControl.FindControl("txtEmailAddress") as TextBox).Text;
            ItemData.EmailAddress = ItemData.Login.EmailAddress;

            ItemData.Login.IsActive = (editLoginControl.FindControl("chkIsActive") as CheckBox).Checked;
            
            //Merchant Information:
            ItemData.Name = txtName.Text;
            ItemData.BillingName = txtBillingName.Text;
            ItemData.LegalName = txtLegalName.Text;
            ItemData.LegalNumber = txtLegalNumber.Text;
            ItemData.Descriptor = txtDescriptor.Text;
            ItemData.Industry = ddlIndustry.Value.ToInt32();
            ItemData.Identifier = txtIdentifier.Text;
            ItemData.Url = txtUrl.Text;
            ItemData.SupportPhone = txtSupportPhone.Text;
            ItemData.SupportEmail = txtSupportEmail.Text;

            ItemData.BusinessAddress = acBusinessAddress.Save(ItemData.BusinessAddress);
            ItemData.PersonalAddress = acPersonalAddress.Save(ItemData.PersonalAddress);
            if (ItemData.BusinessAddress == null || ItemData.PersonalAddress==null)
            {
                if (ItemData.BusinessAddress == null && ItemData.PersonalAddress == null)
                {
                    SaveMerchantErrorMessage.SetMessage("You can't save merchant without choosing personal and business address country",true);
                    dlgMerchants.RegisterShow();
                    throw new Exception("Personal and business address country wasn't selected");
                }
                else if (ItemData.BusinessAddress == null)
                {
                    SaveMerchantErrorMessage.SetMessage("You can't save merchant without choosing  business address country", true);
                    dlgMerchants.RegisterShow();
                    throw new Exception("Business address country wasn't selected");
                }
                else if (ItemData.PersonalAddress == null)
                {
                    SaveMerchantErrorMessage.SetMessage("You can't save merchant without choosing personal address country", true);
                    dlgMerchants.RegisterShow();
                    throw new Exception("Personal address country wasn't selected");
                }
            }

            //Managment:
            if (ddlPrefferedLanguage.Text == "Hebrew") ItemData.LanguagePreference = "heb";
            if (ddlPrefferedLanguage.Text == "English") ItemData.LanguagePreference = "eng";
                        
            ItemData.ParentCompany = ddlParentCompany.SelectedValue != "Default" ? ddlParentCompany.Value.ToNullableInt() : Bll.DataManager.ParentCompany.GetDefaultParentCompanyIdValue();
            ItemData.AccountManager = ddlAccountManager.SelectedValue != "0" ? ddlAccountManager.SelectedValue : "";
            ItemData.MerchantGroup = dllMerchantGroup.Value.ToNullableInt();
            ItemData.Department = ddlDepartment.Value.ToNullableInt();
            ItemData.LinkName = txtLinkName.Text;
            if (ItemData.OpenDate.ToString() == "01/01/0001 00:00:00")
            {
                ItemData.OpenDate = DateTime.Now;
            }
            ItemData.CloseDate = txtCloseDate.Value;
            ItemData.Comment = txtComment.Text;

            //Contact Details
            ItemData.ContactFirstName = txtContactFirstName.Text;
            ItemData.ContactLastName = txtContactLastName.Text;
            ItemData.ContactPhone = txtContactPhone.Text;
            ItemData.ContactMobilePhone = txtContactMobilePhone.Text;
            ItemData.ContactEmail = txtContactEmail.Text;
            //PersonalAddress save moved up next to business address save.
            ItemData.IsInterestedInNewsletter = chkIsInterestedInNewsletter.Checked;
            ItemData.ShowSensativeData = chkShowSensativeData.Checked;
                                   
            if (!((e as CommandEventArgs).CommandName == "SaveDuplicateMerchant")) ItemData.CheckDuplicates();

            ItemData.Save(false);
        }

        protected void Group_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "ShowDialog":
                    rptGroupList.DataBinding += delegate { rptGroupList.DataSource = Bll.Merchants.Group.Cache; };
                    dlgMerchantGroups.BindAndShow();
                    return;

                case "DialogClose":
                    //clean the list from the empty field that was added when the user clicked "add".
                    List<Netpay.Bll.Merchants.Group> groupList = Bll.Merchants.Group.Cache.Where(x => x.Name == "").ToList();
                    if (groupList.Count() > 0)
                    {
                        foreach (var group in groupList)
                        {
                            if (group.Name == "") group.Delete();
                        }
                    }

                    Bll.Merchants.Group.Cache.RemoveAll(x => x.Name == "");

                    dllMerchantGroup.ListDataBind();
                    upGroups.Update();

                    var button = (Button)DataButtons1.FindControl("btnSave");
                    button.Style.Add("opacity", "1");
                    button.Style.Add("cursor", "pointer");
                    button.Attributes.Remove("disabled");
                    return;

                case "LoadItem":
                    EditGroup = Bll.Merchants.Group.Get((e.CommandArgument as DataKey)[0].ToNullableInt().GetValueOrDefault());

                    break;

                case "AddItem":
                    if (!Bll.Merchants.Group.Cache.Any(x => x.Name == txtGroupName.Text))
                    {
                        EditGroup = new Bll.Merchants.Group();
                        EditGroup.Name = txtGroupName.Text;
                        EditGroup.Save();
                    }
                    EditGroup = new Bll.Merchants.Group();

                    break;

                case "SaveItem":
                    EditGroup = Bll.Merchants.Group.Get(hfGroupID.Value.ToNullableInt().GetValueOrDefault());
                    if (EditGroup == null) EditGroup = new Bll.Merchants.Group();
                    if (txtGroupName.Text != "")
                    {
                        EditGroup.Name = txtGroupName.Text;
                        EditGroup.Save();
                    }

                    break;

                case "DeleteItem":
                    EditGroup = Bll.Merchants.Group.Get(hfGroupID.Value.ToNullableInt().GetValueOrDefault());
                    if (EditGroup != null) EditGroup.Delete();
                    EditGroup = new Bll.Merchants.Group();

                    var button1 = (Button)DataButtons1.FindControl("btnSave");
                    button1.Style.Add("opacity", "1");
                    button1.Style.Add("cursor", "pointer");
                    button1.Attributes.Remove("disabled");

                    break;
            }
            rptGroupList.DataSource = Bll.Merchants.Group.Cache.ToList();
            dlgMerchantGroups.DataBind();

            if (e.CommandName != "SaveItem" && e.CommandName != "DeleteItem")
                Page.Validate("manage");


        }

        protected void CustomValidatortxtGroupName_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = (args.Value != "");

            if (args.IsValid == false)
            {
                var button = (Button)DataButtons1.FindControl("btnSave");

                button.Style.Add("opacity", "0.2");
                button.Style.Add("cursor", "default");
                button.Attributes.Add("disabled", "true");
            }
            else
            {
                var button = (Button)DataButtons1.FindControl("btnSave");
                button.Style.Add("opacity", "1");
                button.Style.Add("cursor", "pointer");
                button.Attributes.Remove("disabled");
            }
        }
    }
}