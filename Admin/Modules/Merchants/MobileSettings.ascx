﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="MobileSettings.ascx.cs" Inherits="Netpay.Admin.Merchants.MobileSettings" %>
<admin:FormSections runat="server" Title="Merchant Mobile POS App Settings ">
    <div class="row">
        <div class="col-lg-4 col-md-4">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsMobileAppEnabled" Text="Allow mobile POS App" Checked='<%# MerchantSettings.IsMobileAppEnabled %>' /></li>
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsInstallments" Text="Allow Installments" Checked='<%# MerchantSettings.IsInstallments %>' /></li>
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsAuthorization" Text="Allow Authorization" Checked='<%# MerchantSettings.IsAuthorization %>' /></li>
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsRefund" Text="Allow Refunds" Checked='<%# MerchantSettings.IsRefund %>' /></li>
            </ul>
        </div>
        <div class="col-lg-4 col-md-4">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsAllowTaxRateChange" Text="Allow Tax Rate Change" Checked='<%# MerchantSettings.IsAllowTaxRateChange %>' /></li>
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsFullNameRequired" Text="Require Full Name" Checked='<%# MerchantSettings.IsFullNameRequired %>' /></li>
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsPersonalNumberRequired" Text="Require Personal ID" Checked='<%# MerchantSettings.IsPersonalNumberRequired %>' /></li>
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsCVVRequired" Text="Require Cvv" Checked='<%# MerchantSettings.IsCVVRequired %>' /></li>
            </ul>
        </div>
        <div class="col-lg-4 col-md-4">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsEmailRequired" Text="Require Email" Checked='<%# MerchantSettings.IsEmailRequired %>' /></l>
                 <li class="list-group-item">
                     <asp:CheckBox runat="server" ID="chkIsPhoneRequired" Text="Require Phone" Checked='<%# MerchantSettings.IsPhoneRequired %>' /></li>
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsOwnerSignRequired" Text="Require Cardholder Signature" Checked='<%# MerchantSettings.IsOwnerSignRequired %>' /></li>
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsCardNotPresent" Text="Allow card not present Transactions" Checked='<%# MerchantSettings.IsCardNotPresent %>' /></li>
            </ul>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Tax Rate %
                <div class="input-group">
                    <span class="input-group-addon">%</span>
                    <asp:TextBox runat="server" ID="txtValueAddedTax" CssClass="form-control" Text='<%# MerchantSettings.ValueAddedTax %>' />
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Maximum active devices
                 <div class="input-group">
                     <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                     <asp:TextBox runat="server" ID="txtMaxDeviceCount" CssClass="form-control" Text='<%# MerchantSettings.MaxDeviceCount %>' />
                 </div>
            </div>
        </div>
    </div>
</admin:FormSections>
<%-- Make the Orange Settings form section false - it's not relevant anymore --%>
<admin:FormSections runat="server" Title="Orange Settings" Visible="false">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                Phone Number: 
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
            <asp:TextBox CssClass="form-control" runat="server" ID="txtPhoneNumber" />
        </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <asp:Literal runat="server" ID="lblMessage" EnableViewState="false" />
            Next Sync: 
            <asp:Literal runat="server" ID="ltNextSync" />
        </div>
    </div>

    <div class="row inner-table-titles ">
        <div class="col-lg-4 col-md-4">Service</div>
        <div class="col-lg-4 col-md-4">Current</div>
        <div class="col-lg-4 col-md-4">Update</div>
    </div>
    <div class="row table-ui">
        <div class="col-lg-4 col-md-4">Main</div>
        <div class="col-lg-4 col-md-4">
            <asp:CheckBox ID="chkServiceMain_Confirm" runat="server" Enabled="false" />
        </div>
        <div class="col-lg-4 col-md-4">
            <asp:CheckBox ID="chkServiceMain" runat="server" />
        </div>
    </div>
    <div class="row table-ui">
        <div class="col-lg-4 col-md-4">Invoices</div>
        <div class="col-lg-4 col-md-4">
            <asp:CheckBox ID="chkServiceInv_Confirm" runat="server" Enabled="false" />
        </div>
        <div class="col-lg-4 col-md-4">
            <asp:CheckBox ID="chkServiceInv" runat="server" />
        </div>
    </div>
    <div class="row table-ui">
        <div class="col-lg-4 col-md-4">Devices</div>
        <div class="col-lg-4 col-md-4">
            <asp:TextBox ID="txtServiceDevices_Confirm" runat="server" Enabled="false" CssClass="form-control" />
        </div>
        <div class="col-lg-4 col-md-4">
            <asp:TextBox ID="txtServiceDevices" runat="server" CssClass="form-control " />
        </div>
    </div>
</admin:FormSections>


<admin:FormSections runat="server" Title="All Merchant Registered POS Devices" Flexible="false">
    <NP:UpdatePanel runat="server" ID="upDeviceList" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <NP:PagedRepeater runat="server" ID="rptDevices">
                <HeaderTemplate>
                    <div class="table-responsive">
                        <table class="table table-hover table-customize">
                            <thead>
                                <th class="text-nowrap">Phone Number:</th>
                                <th class="text-nowrap">Identity / User Agent / App version</th>
                                <th class="text-nowrap">IsActive</th>
                                <th class="text-nowrap">Insert Date:</th>
                                <th class="text-nowrap">PassCode:</th>
                                <th class="text-nowrap">Last Login:</th>
                                <th class="text-nowrap">IsActivated:</th>
                                <th class="text-nowrap">Sign Fail Count:</th>
                            </thead>
                </HeaderTemplate>
                <ItemTemplate>
                    <tbody>
                        <td>+ <%# Eval("DevicePhoneNumber")%></td>
                        <td>
                            <%# string.Format("Identity: {0}\r\nUserAgent: {1}\r\nAppVersion: {2}", Eval("DeviceIdentity"), Eval("DeviceUserAgent"), Eval("AppVersion"))  %>
                        </td>
                        <td class="text-nowrap">
                            <asp:Literal runat="server" ID="ltID" Text='<%# Eval("id") %>' Visible="false" />
                            <asp:CheckBox runat="server" Text="Enabled" ID="chkEnabled" Checked='<%# Eval("IsActive") %>' />
                            <%--<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex='<%# ((bool)Eval("IsActive")) ? 1 : 0 %>'>
                                <asp:View ID="View1" runat="server"><span style="color: #29af00;"><i class="fa fa-check"></i>&nbsp;&nbsp; Enabled</span></asp:View>
                                <asp:View ID="View2" runat="server"><span style="color: #af0000;"><i class="fa fa-times "></i>&nbsp;&nbsp; Disabled </span></asp:View>
                            </asp:MultiView>--%>
                        </td>
                        <td><%# Eval("InsertDate", "{0:d}") %></td>
                        <td><%# Eval("PassCode")%></td>
                        <td><%# Eval("LastLogin")%></td>
                        <td><%# Eval("IsActivated")%></td>
                        <td><%# Eval("SignatureFailCount")%></td>
                    </tbody>

                </ItemTemplate>
                <FooterTemplate>
                    </table>
                    </div>
                </FooterTemplate>
                <EmptyContainer>
                    <div class="alert alert-info"><strong>Info!</strong> No record information</div>
                </EmptyContainer>
            </NP:PagedRepeater>
        </ContentTemplate>
    </NP:UpdatePanel>
    <hr />
    <asp:LinkButton CssClass="btn btn-primary text-uppercase" ID="LinkButton1" runat="server" Text="Reset signature lock count" OnCommand="Reset_Signature" />
</admin:FormSections>

<admin:ModalDialog runat="server" ID="dlgResetSignature" Title="Reset Signature Lock Count">
    <Body>
        <netpay:ActionNotify runat="server" ID="ResetSignatureNotify" />
    </Body>
</admin:ModalDialog>
