﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GradedFees.ascx.cs" Inherits="Netpay.Admin.Merchants.GradedFees" %>

<admin:FormPanel ID="FeesByCurrencyPanel" runat="server" Title="FEES BY CURRENCY">
    <div class="row"> 
        <div class="col-md-4">
            <div class="form-group">
                <netpay:CurrencyDropDown EnableBlankSelection="false" CssClass="form-control" runat="server" ID="ddlCurrency" /> 
            </div>
        </div>
        <div class="col-md-4">
            <asp:Button runat="server" Text="Show" CssClass="btn btn-primary" OnCommand="Add_Command"/>
        </div>
    </div>
    <hr />
    <NP:PagedRepeater runat="server" ID="rptMerchantCurrencyFees" DataSource='<%# MerchantSettings.OrderByDescending(x=>x.Value.ID) %>'>
                        <HeaderTemplate>
                            <div class="table-responsive">
                            <table class="table table-hover table-customize">
                                <thead>
                                    <th class="text-nowrap">Currency</th>
                                    <th class="text-nowrap">Wire Fee</th>
                                    <th class="text-nowrap">Storage<br />Fee</th>
                                    <th class="text-nowrap">Handling<br />Fee</th>
                                    <th class="text-nowrap">Min. Merchant<br />Payment Req.</th>
                                </thead>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <asp:HiddenField runat="server" ID="hdCurrency" Value='<%#Eval("Key")%>' />
                                <td>
                                    <asp:Literal runat="server" Text='<%#Netpay.Bll.Currency.GetIsoCode((Netpay.CommonTypes.Currency)((int)Eval("Key"))) %>'></asp:Literal>
                                </td>
                                <td>
                                    <asp:TextBox style="display:inline-block" Width="30%" runat="server" CssClass="form-control" ID="txtWireFee" Text='<%# ((decimal)Eval("Value.WireFee")).ToString("#,0.00") %>' /><div style="display:inline-block">+</div>
                                    <asp:TextBox style="display:inline-block" Width="30%" runat="server" CssClass="form-control" ID="txtWireFeePercent" Text='<%# ((decimal)Eval("Value.WireFeePercent")).ToString("#,0.00")%>' /><div style="display:inline-block">%</div>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtStorageFee" Text='<%# ((decimal)Eval("Value.StorageFee")).ToString("#,0.00") %>' />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtHandlingFee" Text='<%# ((decimal)Eval("Value.HandlingFee")).ToString("#,0.00")%>' />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtMinPayoutAmount" Text='<%# ((decimal)Eval("Value.MinPayoutAmount")).ToString("#,0.00") %>' />
                                </td>
                           </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                            </div>
                        </FooterTemplate>
                    </NP:PagedRepeater>
</admin:FormPanel>

<admin:FormPanel Title="GRADED CLEARING FEES" runat="server" ID="GradedClearingFeesPanel">
            <div class="row">
                <div class="col-md-4 col-lg-4">
                    <asp:RadioButtonList runat="server" CssClass="radiobutton-list" RepeatLayout="UnorderedList" ID="rblGradedFeesMode" OnSelectedIndexChanged="rblGradedFeesMode_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem  Text="Use settings in terminals" Value="1" />
                        <asp:ListItem  Text="Use the following graded method" Value="2" />
                    </asp:RadioButtonList>
                </div>
            </div>
            <hr />
            <asp:MultiView ID="upGraded" runat="server" ActiveViewIndex='<%# ItemData != null && ItemData.GradedFeeCurrency.HasValue || IsShowGradedList ? 1 : 0 %>'>
                <asp:View runat="server" ID="EmptyView">

                </asp:View>
                <asp:View runat="server" ID="RepeaterView">
                    <div class="row"> 
                        <div class="col-md-4">
                            <div class="form-group">
                                <asp:Literal runat="server" Text="Currency to use" />
                                <netpay:CurrencyDropDown EnableBlankSelection="false" CssClass="form-control" runat="server" ID="ddlGradedCurrency" /> 
                            </div>
                        </div>
                        <div class="col-md-4">
                              <br />
                              <asp:Button runat="server" Text="Add" CssClass="btn btn-primary" OnCommand="Add_Graded_Value" />
                        </div>
                    </div>
                    <NP:PagedRepeater runat="server" ID="rptGradedFees" DataSource='<%# GradedFeesList %>' OnItemCommand="rptGradedFees_ItemCommand">
                        <HeaderTemplate>
                            <div class="table-responsive">
                                <table class="table table-hover table-customize">
                                    <thead>
                                        <th class="text-nowrap">Minimum Total</th>
                                        <th class="text-nowrap">Fee Percent</th>
                                        <th class="text-nowrap">Action</th>
                                    </thead>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <asp:HiddenField runat="server" ID="hdGradedFeeId" Value='<%# Eval("ID") %>' />
                                <td>
                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtTotalMin" Text='<%# Eval("TotalMin") != null ? ((decimal)Eval("TotalMin")).ToString("#,0.00") : 0.ToString("#,0.00") %>'></asp:TextBox>
                                </td>                                
                                <td>
                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtFeePercent" Text='<%# Eval("FeePercent") != null ? ((decimal)Eval("FeePercent")).ToString("#,0.00") : 0.ToString("#,0.00") %>'></asp:TextBox>
                                </td>
                                <td>
                                    <asp:MultiView runat="server" ActiveViewIndex='<%# Eval("ID")!= null && ((int)Eval("ID")) != 0 ? 0 : 1 %>'>
                                            <asp:View runat="server">
                                                <asp:Button CssClass="btn btn-danger" runat="server" OnClientClick="return confirm('Are you sure?');"  CommandName="Delete" CommandArgument='<%# Eval("ID") %>' Text="Delete" />
                                            </asp:View>
                                            <asp:View runat="server">
                                                <asp:Button CssClass="btn btn-default" runat="server" CommandName="Save" Text="Save" />
                                            </asp:View>
                                    </asp:MultiView>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                           </table>
                            </div>
                        </FooterTemplate>
                    </NP:PagedRepeater>
                </asp:View>
            </asp:MultiView>
</admin:FormPanel>