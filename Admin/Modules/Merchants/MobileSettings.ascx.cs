﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Merchants
{
    public partial class MobileSettings : Controls.AccountControlBase
    {
        protected Netpay.Bll.Merchants.Mobile.MobileAppSettings MerchantSettings { get; set; }
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += PageController_SaveItem;
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            if (Account != null) MerchantSettings = Netpay.Bll.Merchants.Mobile.MobileAppSettings.Load(Account.MerchantID.GetValueOrDefault());
            if (MerchantSettings == null) MerchantSettings = new Netpay.Bll.Merchants.Mobile.MobileAppSettings(0);
            if (Account != null) rptDevices.DataSource = Netpay.Bll.Accounts.MobileDevice.Search(Account.AccountID);
            base.DataBindChildren();
            Netpay.Bll.Affiliates.MerchantSettings orangeSettings = null;
            ///// Check this with Eran
            if ((Account != null) && (Account.MerchantID != null)) orangeSettings = Netpay.Bll.Affiliates.MerchantSettings.Load(Netpay.Bll.ThirdParty.Orange.PartnerId, Account.MerchantID.Value);
            if (orangeSettings != null)
            {
                chkServiceMain_Confirm.Checked = orangeSettings.ConfigurationValues[(Netpay.Bll.ThirdParty.Orange.ServiceCodeMaster + Netpay.Bll.ThirdParty.Orange.ServiceConfirmPostfix)].ToInt32(0) == 1;
                chkServiceMain.Checked = orangeSettings.ConfigurationValues[Netpay.Bll.ThirdParty.Orange.ServiceCodeMaster].ToInt32(0) == 1;
                chkServiceInv_Confirm.Checked = orangeSettings.ConfigurationValues[(Netpay.Bll.ThirdParty.Orange.ServiceCodeInvoices + Netpay.Bll.ThirdParty.Orange.ServiceConfirmPostfix)].ToInt32(0) == 1;
                chkServiceInv.Checked = orangeSettings.ConfigurationValues[Netpay.Bll.ThirdParty.Orange.ServiceCodeInvoices].ToInt32(0) == 1;
                txtServiceDevices_Confirm.Text = orangeSettings.ConfigurationValues[(Netpay.Bll.ThirdParty.Orange.ServiceCodeChild + Netpay.Bll.ThirdParty.Orange.ServiceConfirmPostfix)].ToInt32(0).ToString();
                txtServiceDevices.Text = orangeSettings.ConfigurationValues[Netpay.Bll.ThirdParty.Orange.ServiceCodeChild].ToInt32(0).ToString();
                txtPhoneNumber.Text = orangeSettings.UserID;
                txtPhoneNumber.Enabled = !(chkServiceMain_Confirm.Checked
                            || (chkServiceInv_Confirm.Checked
                            || (txtServiceDevices_Confirm.Text.ToInt32(0) > 0)));
                ltNextSync.Text = ((orangeSettings.SyncDate == null) ? "Not Needed" : orangeSettings.SyncDate.ToString());
            }
        }

        protected void Orange_Save(object sender, System.EventArgs e)
        {
            Netpay.Bll.Affiliates.MerchantSettings orangeSettings;
            if (txtPhoneNumber.Enabled)
            {
                orangeSettings = Netpay.Bll.Affiliates.MerchantSettings.Load(Netpay.Bll.ThirdParty.Orange.PartnerId, Account.MerchantID.Value);
                if ((orangeSettings == null))
                    orangeSettings = new Netpay.Bll.Affiliates.MerchantSettings(Account.MerchantID.GetValueOrDefault(), Netpay.Bll.ThirdParty.Orange.PartnerId);
                orangeSettings.UserID = txtPhoneNumber.Text;
                try { orangeSettings.Save(); }
                catch (Exception ex)
                {
                    lblMessage.Text = ex.Message;
                    return;
                }
            }
            Netpay.Bll.ThirdParty.Orange.setMerchantServices(Account.MerchantID.GetValueOrDefault(), chkServiceMain.Checked, chkServiceInv.Checked, txtServiceDevices.Text.ToInt32(0));
            orangeSettings = Netpay.Bll.Affiliates.MerchantSettings.Load(Netpay.Bll.ThirdParty.Orange.PartnerId, Account.MerchantID.Value);
            ltNextSync.Text = ((orangeSettings == null || orangeSettings.SyncDate == null) ? "Not Needed" : orangeSettings.SyncDate.ToString());
        }

        protected void Save_Devices(object sender, System.EventArgs e)
        {
            var devices = Bll.Accounts.MobileDevice.Search(Account.AccountID);
            foreach (RepeaterItem item in rptDevices.Items)
            {
                int id = (item.FindControl("ltID") as Literal).Text.ToInt32(0);
                var device = devices.Where(d => d.ID == id).SingleOrDefault();
                if (device != null)
                {
                    bool isActive = (item.FindControl("chkEnabled") as CheckBox).Checked;
                    if (device.IsActive != isActive)
                        Bll.Accounts.MobileDevice.BlockMobileDevice(id, isActive);
                }
            }
        }

        protected void PageController_SaveItem(object sender, System.EventArgs e)
        {
            if (Bll.Merchants.Mobile.MobileAppSettings.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Edit))
            {
                MerchantSettings = Netpay.Bll.Merchants.Mobile.MobileAppSettings.Load(Account.MerchantID.GetValueOrDefault());
                if (MerchantSettings == null) MerchantSettings = new Netpay.Bll.Merchants.Mobile.MobileAppSettings(Account.MerchantID.GetValueOrDefault());

                MerchantSettings.IsMobileAppEnabled = chkIsMobileAppEnabled.Checked;
                MerchantSettings.IsInstallments = chkIsInstallments.Checked;
                MerchantSettings.IsAuthorization = chkIsAuthorization.Checked;
                MerchantSettings.IsRefund = chkIsRefund.Checked;
                MerchantSettings.IsAllowTaxRateChange = chkIsAllowTaxRateChange.Checked;
                MerchantSettings.ValueAddedTax = txtValueAddedTax.Text.ToNullableDecimal();
                MerchantSettings.MaxDeviceCount = (short)txtMaxDeviceCount.Text.ToInt32();

                MerchantSettings.IsFullNameRequired = chkIsFullNameRequired.Checked;
                MerchantSettings.IsPersonalNumberRequired = chkIsPersonalNumberRequired.Checked;
                MerchantSettings.IsCVVRequired = chkIsCVVRequired.Checked;
                MerchantSettings.IsEmailRequired = chkIsEmailRequired.Checked;
                MerchantSettings.IsPhoneRequired = chkIsPhoneRequired.Checked;
                MerchantSettings.IsOwnerSignRequired = chkIsOwnerSignRequired.Checked;
                MerchantSettings.IsCardNotPresent = chkIsCardNotPresent.Checked;

                MerchantSettings.Save();
                Save_Devices(sender, e);
            }
            //Orange_Save(sender, e);
        }

        protected void Reset_Signature(object sender, System.EventArgs e)
        {
            if (Account.AccountID != 0)
            {
                Netpay.Bll.Accounts.MobileDevice.ResetSignatureLockCount(Account.AccountID);
                rptDevices.DataSource = Netpay.Bll.Accounts.MobileDevice.Search(Account.AccountID);
                rptDevices.DataBind();
                upDeviceList.Update();
            }
        }
    }
}