﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Merchants
{
    public partial class GradedFees : Controls.AccountControlBase
    {
        //protected Netpay.Bll.Merchants.Merchant ItemData { get { return Account as Netpay.Bll.Merchants.Merchant; } }
        // Get the value of the global account account into the ItemData property
        protected Netpay.Bll.Merchants.Merchant ItemData
        {
            get
            {
                return AccountPage.Account as Netpay.Bll.Merchants.Merchant;
            }
            private set
            {
                AccountPage.Account = value;
            }
        }

        public bool IsShowGradedList { get; set; }

        private int nMerchant { get; set; }
        private bool bErrorsCleared { get; set; }

        public static Dictionary<int, Bll.Settlements.MerchantSettings> MerchantSettings { get; set; }

        public static List<Bll.Merchants.GradedFees> GradedFeesList { get; set; }

        public static List<int> MerchantCurrenciesIds { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += PageController_SaveItem;

            base.OnLoad(e);
        }

        protected override void DataBind(bool raiseOnDataBinding)
        {
            if (ItemData == null) return;

            //Bind Merchant Settings section
            //Get the currenct merchant settings.
            MerchantSettings = Bll.Settlements.MerchantSettings.LoadForMerchant(ItemData.ID);

            //Check if "Add" button was clicked , if so add the selected item
            if (ViewState["IsNewItemAdded"] != null && (bool)ViewState["IsNewItemAdded"])
            {
                int currencyId = int.Parse((string)ViewState["CurrencyId"]);
                MerchantSettings.Add(currencyId, new Bll.Settlements.MerchantSettings(ItemData.ID, (CommonTypes.Currency)currencyId));
                ClearViewState();
            }

            //Get the currencies list that have settings for this merchant
            MerchantCurrenciesIds = MerchantSettings.Select(x => x.Key).ToList();

            ddlCurrency.IncludeCurrencyIDs = string.Join(",", Netpay.Bll.Currency.Cache.Where(x => (!MerchantCurrenciesIds.Contains(x.ID))).Select(y => y.ID.ToString()).ToList());


            //Bind Graded Fees section(if needed)
            if ((ItemData.GradedFeeCurrency.HasValue || IsShowGradedList)) 
            {
                rblGradedFeesMode.SelectedValue = "2";
                GradedFeesList = Bll.Merchants.GradedFees.LoadForMerchant(ItemData.ID);
                ddlGradedCurrency.SelectedCurrencyIso = Bll.Currency.GetIsoCode(ItemData.GradedFeeCurrency.HasValue ? ItemData.GradedFeeCurrency.Value : 0);

                if (GradedFeesList.Count == 0 || ViewState["AddGradeFee"] != null && (bool)ViewState["AddGradeFee"] == true)
                {
                    GradedFeesList.Add(new Bll.Merchants.GradedFees() { CompanyID = ItemData.ID, FeePercent = 0, TotalMin = 0 });
                    ViewState["AddGradeFee"] = null;
                }
            }
            else
            {
                rblGradedFeesMode.SelectedValue = "1";
            }

            base.DataBind(raiseOnDataBinding);
        }


        protected void Add_Command(object sender, CommandEventArgs e)
        {
            ViewState["IsNewItemAdded"] = true;
            ViewState["CurrencyId"] = ddlCurrency.SelectedValue;

            DataBind();
            FeesByCurrencyPanel.Update();
        }

        protected void ClearViewState()
        {
            ViewState["IsNewItemAdded"] = null;
            ViewState["CurrencyId"] = null;
        }

        protected void PageController_SaveItem(object sender, EventArgs e)
        {
            if (ItemData == null)
                ItemData = new Netpay.Bll.Merchants.Merchant();

            //Currency settings section
            if (Bll.Settlements.MerchantSettings.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Edit))
            {
                //Save the currency fees section
                if (ItemData.ID > 0)
                {
                    //Load MerchantSettings ItemData after it was updated in Payouts tab.
                    MerchantSettings = TemplatePage.GetItemData<Dictionary<int, Bll.Settlements.MerchantSettings>>();

                    //If there is not value to MerchantSettings try to load again.
                    if (MerchantSettings == null || MerchantSettings.Count == 0)
                    {
                        MerchantSettings = Bll.Settlements.MerchantSettings.LoadForMerchant(ItemData.ID);
                    }

                    if (rptMerchantCurrencyFees.Items.Count > 0)
                    {
                        foreach (RepeaterItem ri in rptMerchantCurrencyFees.Items)
                        {
                            int ItemCurrencyId = int.Parse((ri.FindControl("hdCurrency") as HiddenField).Value);
                            //Check if the current item of the repeater is new item or it's an existing item
                            if (MerchantSettings != null && MerchantSettings.Any(x => x.Key == ItemCurrencyId))
                            {
                                decimal test;
                                if (decimal.TryParse(((ri.FindControl("txtWireFee") as TextBox).Text), out test))
                                {
                                    MerchantSettings.Where(x => x.Key == ItemCurrencyId).SingleOrDefault().Value.WireFee = decimal.Parse((ri.FindControl("txtWireFee") as TextBox).Text);
                                }

                                if (decimal.TryParse(((ri.FindControl("txtWireFeePercent") as TextBox).Text), out test))
                                {
                                    MerchantSettings.Where(x => x.Key == ItemCurrencyId).SingleOrDefault().Value.WireFeePercent = decimal.Parse((ri.FindControl("txtWireFeePercent") as TextBox).Text);
                                }

                                if (decimal.TryParse(((ri.FindControl("txtStorageFee") as TextBox).Text), out test))
                                {
                                    MerchantSettings.Where(x => x.Key == ItemCurrencyId).SingleOrDefault().Value.StorageFee = decimal.Parse((ri.FindControl("txtStorageFee") as TextBox).Text);
                                }

                                if (decimal.TryParse(((ri.FindControl("txtHandlingFee") as TextBox).Text), out test))
                                {
                                    MerchantSettings.Where(x => x.Key == ItemCurrencyId).SingleOrDefault().Value.HandlingFee = decimal.Parse((ri.FindControl("txtHandlingFee") as TextBox).Text);
                                }

                                if (decimal.TryParse(((ri.FindControl("txtMinPayoutAmount") as TextBox).Text), out test))
                                {
                                    MerchantSettings.Where(x => x.Key == ItemCurrencyId).SingleOrDefault().Value.MinPayoutAmount = decimal.Parse((ri.FindControl("txtMinPayoutAmount") as TextBox).Text);
                                }

                                //Save current item after it was update in Payouts tab and also in Fees tab.
                                MerchantSettings.Where(x => x.Key == ItemCurrencyId).SingleOrDefault().Value.Save();
                            }
                            else
                            {
                                //new item flow
                                Bll.Settlements.MerchantSettings newItem = new Bll.Settlements.MerchantSettings(ItemData.ID, (CommonTypes.Currency)ItemCurrencyId);
                                newItem.WireFee = decimal.Parse((ri.FindControl("txtWireFee") as TextBox).Text);
                                newItem.WireFeePercent = decimal.Parse((ri.FindControl("txtWireFeePercent") as TextBox).Text);
                                newItem.StorageFee = decimal.Parse((ri.FindControl("txtStorageFee") as TextBox).Text);
                                newItem.HandlingFee = decimal.Parse((ri.FindControl("txtHandlingFee") as TextBox).Text);
                                newItem.MinPayoutAmount = decimal.Parse((ri.FindControl("txtMinPayoutAmount") as TextBox).Text);

                                if (newItem.WireFee != 0 || newItem.WireFeePercent != 0 || newItem.StorageFee != 0 || newItem.HandlingFee != 0 || newItem.MinPayoutAmount != 0)
                                {
                                    newItem.Save();
                                }
                            }
                        }
                    }
                }


                //Graded fees section
                if (rblGradedFeesMode.SelectedValue == "2")
                {
                    if (rptGradedFees.Items.Count > 0)
                    {
                        foreach (RepeaterItem ri in rptGradedFees.Items)
                        {
                            int currentItemId = int.Parse((ri.FindControl("hdGradedFeeId") as HiddenField).Value);
                            decimal totalMin;
                            decimal feePercent;
                            bool checkTotalMin;
                            bool checkFeePercent;

                            checkTotalMin = decimal.TryParse((ri.FindControl("txtTotalMin") as TextBox).Text, out totalMin);
                            checkFeePercent = decimal.TryParse((ri.FindControl("txtFeePercent") as TextBox).Text, out feePercent);

                            if (checkTotalMin && checkFeePercent)
                            {
                                var currentItem = Bll.Merchants.GradedFees.Load(currentItemId);
                                if (currentItem.TotalMin != totalMin || currentItem.FeePercent != feePercent)
                                {
                                    currentItem.TotalMin = totalMin;
                                    currentItem.FeePercent = feePercent;
                                    currentItem.Save();
                                }
                            }
                            else
                            {
                                continue;
                            }
                        }

                        ItemData.SetMerchantGradedFeeCurrency(int.Parse(ddlGradedCurrency.SelectedValue));
                    }
                }
                else
                {
                    ItemData.SetMerchantGradedFeeCurrency(null);
                }
            }
        }

       
        protected void Add_Graded_Value(object sender, CommandEventArgs e)
        {
            IsShowGradedList = true;
            ViewState["AddGradeFee"] = true;

            DataBind();
            GradedClearingFeesPanel.Update();
        }

        protected void rptGradedFees_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Delete":
                    int gradeFeeId;
                    if (int.TryParse(e.CommandArgument.ToString(), out gradeFeeId))
                    {
                        Bll.Merchants.GradedFees gradeFee = Bll.Merchants.GradedFees.Load(gradeFeeId);
                        gradeFee.Delete();
                    }

                    DataBind();
                    GradedClearingFeesPanel.Update();
                    break;

                case "Save":
                    bool checkTotalMin = false;
                    bool checkFeePercent = false;
                    decimal totalMin;
                    decimal feePercent;

                    checkTotalMin = decimal.TryParse((e.Item.FindControl("txtTotalMin") as TextBox).Text, out totalMin);
                    checkFeePercent = decimal.TryParse((e.Item.FindControl("txtFeePercent") as TextBox).Text, out feePercent);

                    if (checkTotalMin && checkFeePercent)
                    {
                        Bll.Merchants.GradedFees newGradeFee = new Bll.Merchants.GradedFees();
                        newGradeFee.CompanyID = ItemData.ID;
                        newGradeFee.TotalMin = totalMin;
                        newGradeFee.FeePercent = feePercent;
                        newGradeFee.Save();
                    }
                    else
                    {
                        throw new Exception("Please insert a valid decimal value into TotalMin and FeePercent.");
                    }

                    //Save the selected GradedFeeCurrency for the merchat
                    ItemData.SetMerchantGradedFeeCurrency(int.Parse(ddlGradedCurrency.SelectedValue));

                    DataBind();
                    GradedClearingFeesPanel.Update();
                    break;
            }
        }

        protected void rblGradedFeesMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblGradedFeesMode.SelectedValue == "1")
            {
                upGraded.ActiveViewIndex = 0;
                GradedClearingFeesPanel.Update();
            }
            else
            {
                upGraded.ActiveViewIndex = 1;
                GradedFeesList = Bll.Merchants.GradedFees.LoadForMerchant(ItemData.ID);
                if (GradedFeesList.Count == 0)
                {
                    GradedFeesList.Add(new Bll.Merchants.GradedFees() { CompanyID = ItemData.ID, FeePercent = 0, TotalMin = 0 });
                }

                if (ItemData.GradedFeeCurrency.HasValue)
                {
                    ddlGradedCurrency.SelectedCurrencyIso = Bll.Currency.GetIsoCode(ItemData.GradedFeeCurrency.Value);
                }
                else
                {
                    ddlGradedCurrency.SelectedCurrencyIso = "ILS";
                }

                rptGradedFees.DataBind();
                GradedClearingFeesPanel.Update();
            }
        }
    }
}