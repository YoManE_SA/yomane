﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Emails;
using Netpay.Admin.Controls;


namespace Netpay.Admin.Merchants
{


    public partial class Emails : Controls.AccountControlBase
    {
        protected Dictionary<int, string> m_merchantNames;
        protected override void OnLoad(EventArgs e)
        {

            rptList.DataBinding += List_DataBinding;
            //    rptList.SelectedIndexChanged += rptList_SelectedIndexChanged;
            base.OnLoad(e);
        }



        private void List_DataBinding(object sender, EventArgs e)
        {
            var sf = new Netpay.Emails.Message.SearchFilters();
            if (Account != null && Account.MerchantID != null) sf.MerchantId = Account.MerchantID;

            var list = Netpay.Emails.Message.Search(sf, rptList);
            if (list != null)
                m_merchantNames = Bll.Accounts.Account.GetMerchantNames((list.Where(i => i.MerchantID != null).Select(i => i.MerchantID.Value).ToList()));
            rptList.DataSource = list;

        }
        protected string GetMerchantName(int? id)
        {
            if (id == null) return null;
            string ret;
            if (!m_merchantNames.TryGetValue((int)id, out ret)) return null;
            return ret;
        }

        private void rptList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //TemplatePage.PageController.LoadActiveItem();
            //     if (ItemData == null) return;
            //

            // Get the selected value which holds the message id
            var messageId = rptList.SelectedValue;

            var ItemData = Netpay.Emails.Message.Load((int)messageId);
            // Update the view
            //  ListSection1.DataBind();


        }
    }
}
