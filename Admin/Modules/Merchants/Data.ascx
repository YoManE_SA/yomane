﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Data.ascx.cs" Inherits="Netpay.Admin.Merchants.Data" %>
<%@ Register Src="../Accounts/Address.ascx" TagPrefix="CUC" TagName="AccountAddress" %>

<admin:FormSections runat="server" Title="Merchant Information">
    <script type="text/javascript">
        function ValidateDataPage() {

            if (typeof (Page_ClientValidate) == 'function') {
                var check = Page_ClientValidate("Merchants_data");

                if (check) {
                    var button = document.getElementById("BodyContent_PageController_FormView_FormPanel_DataButtons_btnSave");
                    button.removeAttribute("disabled");
                    button.style.opacity = "1.0";
                }

                else {
                    var button = document.getElementById("BodyContent_PageController_FormView_FormPanel_DataButtons_btnSave");
                    button.disabled = true;
                    button.style.opacity = "0.2";
                    Page_BlockSubmit = false;
                }
            }
        }

        function ValidateManageSection() {
            if (typeof (Page_ClientValidate) == 'function') {
                var checkManage = Page_ClientValidate("manage");
                if (checkManage) {

                    var button = document.getElementById("BodyContent_PageController_FormView_FormPanel_ctl05_dlgMerchantGroups_DataButtons1_btnSave");
                    button.removeAttribute("disabled");
                    button.style.opacity = "1.0";
                    button.style.cursor = "pointer";
                }

                else {
                    var button = document.getElementById("BodyContent_PageController_FormView_FormPanel_ctl05_dlgMerchantGroups_DataButtons1_btnSave");
                    button.disabled = true;
                    button.style.opacity = "0.2";
                    Page_BlockSubmit = false;
                }
            }
        }

        function CheckName(oSrc, args) {
            if (args.Value == "") {
                args.IsValid = false;
                var button = document.getElementById("BodyContent_PageController_FormView_FormPanel_ctl05_dlgMerchantGroups_DataButtons1_btnSave");
                button.removeAttribute("disabled");
                button.style.opacity = "1.0";
            }
            else {
                args.IsValid = true;
                var button = document.getElementById("BodyContent_PageController_FormView_FormPanel_ctl05_dlgMerchantGroups_DataButtons1_btnSave");
                button.disabled = true;
                button.style.opacity = "0.2";
                Page_BlockSubmit = false;
            }
        }

    </script>

    <div class="row">
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
           Trading Company Name 
                <asp:TextBox runat="server" CssClass="form-control" ID="txtName" Text='<%# ItemData.Name %>' MaxLength="200" onkeyup="ValidateDataPage();" CausesValidation="True" />
                <asp:RequiredFieldValidator
                    ForeColor="red"
                    ControlToValidate="txtName"
                    ID="RequiredFieldValidatortxtText"
                    runat="server"
                    ErrorMessage="This field is required , minimum 6 characters."
                    Display="Dynamic"
                    ValidationGroup="Merchants_data">
                </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator
                    ValidationExpression="^(?:.{6,}|)$"
                    ControlToValidate="txtName"
                    ForeColor="red"
                    ID="RegularExpressionValidatortxtText"
                    runat="server"
                    ErrorMessage="This field is required , minimum 6 characters."
                    ValidationGroup="Merchants_data"
                    Display="Dynamic">
                </asp:RegularExpressionValidator>
            </div>
            <div class="form-group">
                Billing name
                <asp:TextBox runat="server" CssClass="form-control" ID="txtBillingName" Text='<%# ItemData.BillingName %>' MaxLength="200" CausesValidation="True" onkeyup="ValidateDataPage();" />
                <asp:RequiredFieldValidator
                    ForeColor="red"
                    ControlToValidate="txtBillingName"
                    ID="RequiredFieldValidator1"
                    runat="server"
                    ErrorMessage="This field is required , minimum 6 characters."
                    Display="Dynamic"
                    ValidationGroup="Merchants_data">
                </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator
                    ValidationExpression="^(?:.{6,}|)$"
                    ControlToValidate="txtBillingName"
                    ForeColor="red"
                    ID="RegularExpressionValidator1"
                    runat="server"
                    ErrorMessage="This field is required , minimum 6 characters."
                    ValidationGroup="Merchants_data"
                    Display="Dynamic">
                </asp:RegularExpressionValidator>
            </div>
            <div class="form-group">
               Registered Company Name 
                <asp:TextBox CssClass="form-control" runat="server" ID="txtLegalName" Text='<%# ItemData.LegalName %>' MaxLength="200" />
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Registration Number
                <asp:TextBox CssClass="form-control" runat="server" ID="txtLegalNumber" Text='<%# ItemData.LegalNumber %>' MaxLength="15" />
            </div>
            <div class="form-group">
                Describe Business 
                <asp:TextBox runat="server" CssClass="form-control" ID="txtDescriptor" Text='<%# ItemData.Descriptor %>' MaxLength="150" />
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Industry
                <netpay:IndustryDropDown CssClass="form-control" runat="server" ID="ddlIndustry" Value='<%# ItemData.Industry %>' BlankSelectionValue="0" CausesValidation="True" BlankSelectionText="Choose Industry type" onchange="ValidateDataPage();" />
                <asp:RequiredFieldValidator
                    ForeColor="red"
                    ControlToValidate="ddlIndustry"
                    ID="RequiredFieldValidatorddlCurrency"
                    runat="server"
                    ErrorMessage="Choose industry type."
                    InitialValue="0"
                    Display="Dynamic" ValidationGroup="Merchants_data">
                </asp:RequiredFieldValidator>
            </div>
            <div class="form-group">
                Identifier
                <asp:TextBox runat="server" CssClass="form-control" ID="txtIdentifier" Text='<%# ItemData.Identifier %>' MaxLength="20" />
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Website URL
                     <asp:TextBox runat="server" ID="txtUrl" Text='<%# ItemData.Url %>' CssClass="form-control" MaxLength="500" onkeyup="ValidateDataPage();" CausesValidation="True" />
               <%-- <asp:RequiredFieldValidator
                    ForeColor="red"
                    ControlToValidate="txtUrl"
                    ID="RequiredFieldValidatortxtUrl"
                    runat="server"
                    ErrorMessage="This field is required."
                    Display="Dynamic"
                    ValidationGroup="Merchants_data">
                </asp:RequiredFieldValidator>--%>
                <asp:RegularExpressionValidator
                    ControlToValidate="txtUrl"
                    ForeColor="red"
                    ID="RegularExpressionValidatortxtUrl"
                    runat="server"
                    ErrorMessage="Insert a correct URL"
                    ValidationExpression="(http(s)?://)?([\w-]+\.)+([\w-]+\.)+[\w-]+(/[/?%&=]*)?"
                    ValidationGroup="Merchants_data">
                </asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Support Phone
                <asp:TextBox runat="server" ID="txtSupportPhone" Text='<%# ItemData.SupportPhone %>' CssClass="form-control" MaxLength="20" onkeyup="ValidateDataPage();" />
                <%--<asp:RequiredFieldValidator
                    ForeColor="red"
                    ControlToValidate="txtSupportPhone"
                    ID="RequiredFieldValidator2"
                    runat="server"
                    ErrorMessage="This field is required , minimum 9 characters."
                    Display="Dynamic"
                    ValidationGroup="Merchants_data">
                </asp:RequiredFieldValidator>--%>
                <asp:RegularExpressionValidator
                    ValidationExpression="^(?:.{9,}|)$"
                    ControlToValidate="txtSupportPhone"
                    ForeColor="red"
                    ID="RegularExpressionValidator2"
                    runat="server"
                    ErrorMessage="This field is required , minimum 9 characters."
                    ValidationGroup="Merchants_data"
                    Display="Dynamic">
                </asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group clear-form-group">
                Support Email
                <asp:TextBox runat="server" ID="txtSupportEmail" Text='<%# ItemData.SupportEmail %>' CssClass="form-control" MaxLength="80" onkeyup="ValidateDataPage();" CausesValidation="True" />
                <asp:RequiredFieldValidator ForeColor="red" ControlToValidate="txtSupportEmail"  ID="RequiredFieldValidator3" runat="server" ErrorMessage="This field is required." Display="Dynamic" ValidationGroup="Merchants_data" />
                <asp:RegularExpressionValidator ForeColor="red" ControlToValidate="txtSupportEmail" ID="RegularExpressionValidatortxtSupportEmail" runat="server" ErrorMessage="Insert a correct email address." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Merchants_data" />
             
            </div>
        </div>
    </div>
</admin:FormSections>

<admin:FormSections runat="server" Title="Management">
    <div class="row">
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Preferred language
                <asp:DropDownList CssClass="form-control" runat="server" ID="ddlPrefferedLanguage">
                    <asp:ListItem Text="Hebrew" />
                    <asp:ListItem Text="English" Selected="True" Value="0" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Parent company
                <netpay:ParentCompanyDropDown CssClass="form-control" ID="ddlParentCompany" runat="server" EnableBlankSelection="true" BlankSelectionText="Default" BlankSelectionValue="Default" Value='<%#ItemData.ParentCompany.HasValue? ItemData.ParentCompany.Value.ToString() : "Default" %>'></netpay:ParentCompanyDropDown>
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
           Internal Account Manager 
                <netpay:AccountManagersDropDown runat="server" CssClass="form-control" ID="ddlAccountManager" Value='<%# ItemData.GetAccountManagerIdAsString %>' BlankSelectionValue="0" BlankSelectionText="Choose manager" CausesValidation="True" />
                <asp:RequiredFieldValidator
                    ForeColor="red"
                    ControlToValidate="ddlAccountManager"
                    ID="RequiredFieldValidatorddlAccountManager"
                    runat="server"
                    ErrorMessage="Choose account manager."
                    InitialValue="0"
                    Display="Dynamic" ValidationGroup="Merchants_data">
                </asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Affiliate Group 
                    <NP:UpdatePanel runat="server" ID="upGroups" UpdateMode="Conditional" ChildrenAsTriggers="false" RenderMode="Block">
                        <ContentTemplate>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <NP:Button runat="server" Text="Manage" CssClass="btn btn-primary" RegisterAsync="true" CommandName="ShowDialog" OnCommand="Group_Command" CausesValidation="True" /></span>
                                <netpay:MerchantGroupsDropDown runat="server" CssClass="form-control" ID="dllMerchantGroup" Value='<%# ItemData.MerchantGroup %>' />
                            </div>
                        </ContentTemplate>
                    </NP:UpdatePanel>
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Department
                <netpay:MerchantDepartmentDropDown runat="server" CssClass="form-control" ID="ddlDepartment" Value='<%# ItemData.Department %>' />
            </div>

        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Link Name
                <asp:TextBox CssClass="form-control" runat="server" ID="txtLinkName" Text='<%# ItemData.LinkName %>' MaxLength="80" />
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Open date
                <asp:TextBox CssClass="form-control" ID="Opendatetxt" runat="server" Text='<%#ItemData.OpenDate.ToString() == "01/01/0001 00:00:00" ? "Merchant was not saved yet." : ItemData.OpenDate.ToString()%>' ReadOnly="True"></asp:TextBox>
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Close date
                <JQ:DatePicker Enabled='<%# ItemData.CloseDate.ToString() != "01/01/1900 00:00:00" && ItemData.ID != 0%>' runat="server" ID="txtCloseDate" CssClass="form-control"  Text='<%# ItemData.CloseDate.ToString() == "01/01/1900 00:00:00" || ItemData.CloseDate.ToString() == "01/01/0001 00:00:00" ? "" : ItemData.CloseDate.ToString() %>' />
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="form-group clear-form-group">
                Comment for internal purposes
                <asp:TextBox runat="server" ID="txtComment" CssClass="form-control" Text='<%# ItemData.Comment %>' TextMode="MultiLine" Rows="3" MaxLength="150" />
            </div>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Business Address">
    <CUC:AccountAddress runat="server" ID="acBusinessAddress" AddressData='<%# ItemData.BusinessAddress %>' ValidationGroup="Merchants_data" OnSelectedIndexChanged="ValidateDataPage();" />
</admin:FormSections>

<admin:FormSections runat="server" Title="Contact details">
    <div class="row">
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                First Name
                <asp:TextBox CssClass="form-control" runat="server" ID="txtContactFirstName" Text='<%# ItemData.ContactFirstName %>' MaxLength="150" CausesValidation="True" onkeyup="ValidateDataPage();" />
                <asp:RequiredFieldValidator
                    ForeColor="red"
                    ControlToValidate="txtContactFirstName"
                    ID="RequiredFieldValidator4"
                    runat="server"
                    ErrorMessage="Insert a First Name text with minimum 3 characters."
                    Display="Dynamic"
                    ValidationGroup="Merchants_data">
                </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator
                    ValidationExpression="^(?:.{3,}|)$"
                    ControlToValidate="txtContactFirstName"
                    ForeColor="red"
                    ID="RegularExpressionValidator3"
                    runat="server"
                    ErrorMessage="Insert a First Name text with minimum 3 characters."
                    ValidationGroup="Merchants_data"
                    Display="Dynamic">
                </asp:RegularExpressionValidator>
            </div>
            <div class="form-group">
                Last Name
               <asp:TextBox CssClass="form-control" runat="server" ID="txtContactLastName" Text='<%# ItemData.ContactLastName %>' MaxLength="150" CausesValidation="True" onkeyup="ValidateDataPage();" />
                <asp:RequiredFieldValidator
                    ForeColor="red"
                    ControlToValidate="txtContactLastName"
                    ID="ReqContactLastName"
                    runat="server"
                    ErrorMessage="Insert a Last Name text with minimum 3 characters."
                    Display="Dynamic"
                    ValidationGroup="Merchants_data">
                </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator
                    ValidationExpression="^(?:.{3,}|)$"
                    ControlToValidate="txtContactLastName"
                    ForeColor="red"
                    ID="RegContactLastName"
                    runat="server"
                    ErrorMessage="Insert a Last Name text with minimum 3 characters."
                    ValidationGroup="Merchants_data"
                    Display="Dynamic">
                </asp:RegularExpressionValidator>
            </div>

        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Phone
                <asp:TextBox CssClass="form-control" runat="server" ID="txtContactPhone" Text='<%# ItemData.ContactPhone %>' MaxLength="50" onkeyup="ValidateDataPage();" />
                <asp:RequiredFieldValidator
                    ForeColor="red"
                    ControlToValidate="txtContactPhone"
                    ID="RequiredFieldValidator5"
                    runat="server"
                    ErrorMessage="Insert phone number."
                    Display="Dynamic"
                    ValidationGroup="Merchants_data">
                </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator
                    ValidationExpression="^(?:.{9,}|)$"
                    ControlToValidate="txtContactPhone"
                    ForeColor="red"
                    ID="RegularExpressionValidator4"
                    runat="server"
                    ErrorMessage="Insert phone number."
                    ValidationGroup="Merchants_data"
                    Display="Dynamic">
                </asp:RegularExpressionValidator>
            </div>
            <div class="form-group">
                Mobile Phone
                  <asp:TextBox runat="server" CssClass="form-control" ID="txtContactMobilePhone" Text='<%# ItemData.ContactMobilePhone %>' MaxLength="50" CausesValidation="True" onkeyup="ValidateDataPage();" />
                <asp:RequiredFieldValidator
                    ForeColor="red"
                    ControlToValidate="txtContactMobilePhone"
                    ID="RequiredFieldValidator6"
                    runat="server"
                    ErrorMessage="Insert mobile-phone number."
                    Display="Dynamic"
                    ValidationGroup="Merchants_data">
                </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator
                    ValidationExpression="^(?:.{9,}|)$"
                    ControlToValidate="txtContactMobilePhone"
                    ForeColor="red"
                    ID="RegularExpressionValidator5"
                    runat="server"
                    ErrorMessage="Insert mobile-phone number."
                    ValidationGroup="Merchants_data"
                    Display="Dynamic">
                </asp:RegularExpressionValidator>
            </div>

        </div>
        <div class="col-lg-4 col-md-4">
            <div class="form-group">
                Email
                 <asp:TextBox CssClass="form-control" runat="server" ID="txtContactEmail" Text='<%# ItemData.ContactEmail %>' onkeyup="ValidateDataPage();" />
                <asp:RequiredFieldValidator
                    ForeColor="red"
                    ControlToValidate="txtContactEmail"
                    ID="RequiredFieldValidatortxtContactEmail"
                    runat="server"
                    ErrorMessage="This field is required."
                    Display="Dynamic"
                    ValidationGroup="Merchants_data">
                </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator
                    ForeColor="red"
                    ControlToValidate="txtContactEmail"
                    ID="RegularExpressionValidatortxtContactEmail"
                    runat="server"
                    ErrorMessage="Insert a correct email address."
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                    ValidationGroup="Merchants_data">
                </asp:RegularExpressionValidator>
            </div>
        </div>
    </div>
    <asp:CheckBox ID="chkIsInterestedInNewsletter" runat="server" Text="Mailing - Agree to receive newsletters" Checked="<%# ItemData.IsInterestedInNewsletter %>" />
</admin:FormSections>

<admin:FormSections runat="server" Title="Contact details address">
    <CUC:AccountAddress runat="server" ID="acPersonalAddress" AddressData='<%# ItemData.PersonalAddress %>' ValidationGroup="Merchants_data" OnSelectedIndexChanged="ValidateDataPage();" />
</admin:FormSections>

<!--<admin:FormSections runat="server" Title="Security Settings (Integration)">
    <div class="row">
        <div class="col-lg-4 col-md-4">
            <h4>Hash Key</h4>
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:Literal ID="lblHashKey" runat="server" Text='' /></li>
            </ul>
        </div>
        <div class="col-lg-4 col-md-4">
            <h4>Security Key</h4>
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:Literal ID="lblSecurityKey" runat="server" Text='' /></li>
            </ul>
        </div>
        <div class="col-lg-4 col-md-4">
            <h4>Security Key</h4>
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox ID="chkShowSensativeData" runat="server" Text="Display sensitive information" Checked="" /></li>
            </ul>
        </div>
    </div>
</admin:FormSections>-->

<admin:ModalDialog runat="server" ID="dlgMerchantGroups" Title="Groups" OnCommand="Group_Command" OnDialogClose="Group_Command">
    <Body>
        <admin:AdminList runat="server" DataKeyNames="ID" ID="rptGroupList" AutoGenerateColumns="false" BubbleLoadEvent="true" IsInListView="false">
            <Columns>
                <asp:BoundField HeaderText="Name" DataField="Name" />
            </Columns>
        </admin:AdminList>
        <hr />
        <div class="row">
            <asp:HiddenField runat="server" ID="hfGroupID" Value='<%# EditGroup.ID %>' />
            <div class="col-lg-12">
                <div class="form-group">
                    Name:
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtGroupName" Text='<%# EditGroup.Name %>' MaxLength="50" CausesValidation="True" onfocus="ValidateManageSection();" onkeyup="ValidateManageSection();" onblur="ValidateManageSection();" />
                    <asp:CustomValidator ControlToValidate="txtGroupName" ForeColor="red" ID="CustomValidatortxtGroupName" runat="server" ErrorMessage="This field is required." ValidationGroup="manage" ClientValidationFunction="CheckName" ValidateEmptyText="True" OnServerValidate="CustomValidatortxtGroupName_ServerValidate" />
                </div>
            </div>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons ID="DataButtons1" runat="server" EnableAdd="true" EnableDelete="true" EnableSave="true" />
    </Footer>
</admin:ModalDialog>

<admin:ModalDialog runat="server" ID="dlgMerchants" Title="Save Merchant Error Msg">
    <Body>
        <netpay:ActionNotify runat="server" ID="SaveMerchantErrorMessage" />
    </Body>
</admin:ModalDialog>

