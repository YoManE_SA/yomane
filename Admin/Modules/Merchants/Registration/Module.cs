﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Merchants.Registration
{
	public class Module : Admin.CoreBasedModule
	{
		private ApplicationMenu menuItem;
		public override string Name { get { return "Merchant.Registration"; } }
		public override decimal Version { get { return 1.0m; } }
		public override string Author { get { return "OBL ltd."; } }
		public override string Description { get { return "Merchant registration management UI"; } }
		
        public Module() : base(Bll.Merchants.Registrations.Module.Current) { }

		protected override void OnInit(EventArgs e)
		{
            //Create menu item
            menuItem = new ApplicationMenu("Registrations", "~/Merchants/Registrations/0", null, 2);

            //Register route
			Application.RegisterRoute("Merchants/Registrations/{*id}", "Merchant.Registration", typeof(Controls.DataTablePage), module:this);

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
		}

		protected override void OnActivate(EventArgs e)
		{						
			base.OnActivate(e);
		}

		protected override void OnDeactivate(EventArgs e)
		{						
			base.OnDeactivate(e);
		}

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Merchants", menuItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuItem.Parent.RemoveChild(menuItem);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
		{
            if (!CoreModule.IsInstalled)
                return;

			var page = sender as Controls.DataTablePage;
			if (page.TemplateName == "Merchant.Registration")
			{
                page.AddControlToList(page.LoadControl("~/Modules/Merchants/Registration/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Merchants/Registration/Filter.ascx"));
                page.AddControlToForm("Data", page.LoadControl("~/Modules/Merchants/Registration/Form.ascx"),this, "", Bll.Merchants.Registration.SecuredObject);
                page.AddControlToForm("Integrations", page.LoadControl("~/Modules/Merchants/Registration/Integrations.ascx"), this, "", Bll.Merchants.Registration.SecuredObject);
			}
		}
	}
}