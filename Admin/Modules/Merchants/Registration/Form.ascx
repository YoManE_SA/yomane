﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Form.ascx.cs" Inherits="Netpay.Admin.Merchants.Registration.Form" %>
<admin:FormSections runat="server" Title="Contact Details">
    <div class="row">
        <asp:UpdatePanel runat="server" ID="UpAction" UpdateMode="Conditional" ChildrenAsTriggers="false">
            <ContentTemplate>
                <netpay:ActionNotify runat="server" ID="RegActionNotify" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div class="row">
        <div class="col-lg-6">
            ID
            <asp:Literal runat="server" Text="<%# Item.ID %>" />
        </div>
        <div class="col-lg-6">
            Account ID
            <asp:HyperLink runat="server" Text="<%# Item.MerchantID.HasValue? Netpay.Bll.Accounts.Account.GetAccountIdByMerchantId(Item.MerchantID.Value) : Item.MerchantID %>" NavigateUrl='<%#"~/Merchants/0?Search=1&SelectedIndex=0&ctl00.apAccount.hdID=" + (Item.MerchantID.HasValue? Netpay.Bll.Accounts.Account.GetAccountIdByMerchantId(Item.MerchantID.Value) : Item.MerchantID) %>' Target="_blank"></asp:HyperLink>
        </div>
    </div>
</admin:FormSections>

<admin:FormSections runat="server" Title="Contact Details">
    <div class="row">
        <div class="col-lg-4">
            <div class="form-group">
                Merchant Name
			    <asp:TextBox CssClass="form-control" runat="server" ID="txtDbaName" Text="<%# Item.DbaName %>" />
            </div>
            <div class="form-group">
                Legal business name
		    	<asp:TextBox CssClass="form-control" runat="server" ID="txtLegalBusinessName" Text="<%# Item.LegalBusinessName %>" />
            </div>
            <div class="form-group">
                Legal business number
				<asp:TextBox MaxLength="15" CssClass="form-control" runat="server" ID="txtLegalBusinessNumber" Text="<%# Item.LegalBusinessNumber %>" />
            </div>
            <div class="form-group">
                Country
				<netpay:CountryDropDown CssClass="form-control" runat="server" ID="ddlContactDetailsCountry" BlankSelectionText="Choose country" BlankSelectionValue="-1" Value='<%# string.IsNullOrEmpty(Item.ContactDetailsCountryIsoCode)? "-1" : Netpay.Bll.Country.Get(Item.ContactDetailsCountryIsoCode).ID.ToString() %>'></netpay:CountryDropDown>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                Url
				<asp:TextBox CssClass="form-control" runat="server" ID="txtUrl" Text="<%# Item.Url %>" />
            </div>
            <div class="form-group">
                Physical address
				<asp:TextBox CssClass="form-control" runat="server" ID="txtPhysicalAddress" Text="<%# Item.PhisicalAddress %>" />
            </div>
            <div class="form-group">
                Physical city
				<asp:TextBox CssClass="form-control" runat="server" ID="txtPhisicalCity" Text="<%# Item.PhisicalCity %>" />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <div class="form-group">
                    Physical state
					<netpay:StateDropDown runat="server" CssClass="form-control" ID="ddlPhisicalState" BlankSelectionText="Choose state" BlankSelectionValue="-1" CountryID="228" />
                </div>
                <div class="form-group">
                    Physical zip
					<asp:TextBox CssClass="form-control" runat="server" ID="txtPhisicalZip" Text="<%# Item.PhisicalZip %>" />
                </div>
                <div class="form-group">
                    State of incorporation
					<netpay:StateDropDown runat="server" CssClass="form-control" ID="ddlStateOfIncorporation" BlankSelectionText="Choose state" BlankSelectionValue="-1" CountryID="228" />
                </div>
            </div>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Personal Info">
    <div class="row">
        <div class="col-lg-4">
            <div class="form-group">
                First name
		        <asp:TextBox CssClass="form-control" runat="server" ID="txtFirstName" Text="<%# Item.FirstName %>" />
            </div>
            <div class="form-group">
                Last name
        		<asp:TextBox CssClass="form-control" runat="server" ID="txtLastName" Text="<%# Item.LastName %>" />
            </div>
            <div class="form-group">
                Phone
		        <asp:TextBox CssClass="form-control" runat="server" ID="txtPhone" Text="<%# Item.Phone %>" />
            </div>
            <div class="form-group">
                Fax
			    <asp:TextBox CssClass="form-control" runat="server" ID="txtFax" Text="<%# Item.Fax %>" />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                Email
				<asp:TextBox CssClass="form-control" runat="server" ID="txtEmail" Text="<%# Item.Email %>" />
            </div>
            <div class="form-group">
                Address
				<asp:TextBox CssClass="form-control" runat="server" ID="txtAddress" Text="<%# Item.Address %>" />
            </div>
            <div class="form-group">
                City
				<asp:TextBox CssClass="form-control" runat="server" ID="txtCity" Text="<%# Item.City %>" />
            </div>
            <div class="form-group">
                Province
				<netpay:StateDropDown runat="server" CssClass="form-control" ID="ddlState" BlankSelectionText="Choose state" BlankSelectionValue="-1" CountryID="228" />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                Zipcode
				<asp:TextBox CssClass="form-control" runat="server" ID="txtZipcode" Text="<%# Item.Zipcode %>" />
            </div>
            <div class="form-group">
                Owner dob
				<JQ:DatePicker runat="server" CssClass="form-control" ID="txtOwnerDob" Value="<%# Item.OwnerDob %>" />
            </div>
            <div class="form-group">
                Owner ID Number
				<asp:TextBox CssClass="form-control" runat="server" ID="txtOwnerSsn" Text="<%# Item.OwnerSsn %>" />
            </div>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Business Info">
    <div class="row">
        <div class="col-lg-4">
            <div class="form-group">
                Type of business
				<asp:DropDownList ID="ddlTypeOfBusiness" CssClass="form-control" runat="server">
                    <asp:ListItem Text="Choose business type" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Association" Value="7"></asp:ListItem>
                    <asp:ListItem Text="Corporation" Value="3"></asp:ListItem>
                    <asp:ListItem Text="Estate/Trust" Value="8"></asp:ListItem>
                    <asp:ListItem Text="Government" Value="10"></asp:ListItem>
                    <asp:ListItem Text="LLC" Value="4"></asp:ListItem>
                    <asp:ListItem Text="LP" Value="5"></asp:ListItem>
                    <asp:ListItem Text="Non Profit Org" Value="21"></asp:ListItem>
                    <asp:ListItem Text="Other" Value="6"></asp:ListItem>
                    <asp:ListItem Text="Partnership" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Private Corporation" Value="13"></asp:ListItem>
                    <asp:ListItem Text="Public Corporation" Value="12"></asp:ListItem>
                    <asp:ListItem Text="Tax Exempt" Value="11"></asp:ListItem>
                    <asp:ListItem Text="School" Value="22"></asp:ListItem>
                    <asp:ListItem Text="Sole Proprietor" Value="1"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form-group">
                Business start date
				<JQ:DatePicker runat="server" CssClass="form-control" ID="txtBusinessStartDate" Value="<%# Item.BusinessStartDate %>" />
            </div>
            <div class="form-group">
                Industry
                <netpay:IndustryDropDown runat="server" ID="ddlIndustry" CssClass="form-control" Value='<%#Item.Industry.HasValue? Item.Industry.Value.ToString() : "0"  %>' EnableNone="false" EnableBlankSelection="true" BlankSelectionValue="0" BlankSelectionText="Choose industry"></netpay:IndustryDropDown>
            </div>
            <div class="form-group">
                Business description
				<asp:TextBox CssClass="form-control" runat="server" ID="txtBusinessDescription" Text="<%# Item.BusinessDescription %>" />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                Anticipated monthly volume
		    	<asp:TextBox CssClass="form-control" runat="server" ID="txtAnticipatedMonthlyVolume" Text="<%# Item.AnticipatedMonthlyVolume %>" />
            </div>
            <div class="form-group">
                Anticipated average transaction amount
			    <asp:TextBox CssClass="form-control" runat="server" ID="txtAnticipatedAverageTransactionAmount" Text="<%# Item.AnticipatedAverageTransactionAmount %>" />
            </div>
            <div class="form-group">
                Anticipated largest transaction amount
			    <asp:TextBox CssClass="form-control" runat="server" ID="txtAnticipatedLargestTransactionAmount" Text="<%# Item.AnticipatedLargestTransactionAmount %>" />
            </div>
            <div class="form-group">
                % of Goods Delivered in 0 to 7 days
			    <asp:TextBox CssClass="form-control" runat="server" ID="txtPercentDelivery0to7" Text="<%# Item.PercentDelivery0to7 %>" />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                % of Goods Delivered in 15 to 30 days
				<asp:TextBox CssClass="form-control" runat="server" ID="txtPercentDelivery15to30" Text="<%# Item.PercentDelivery15to30 %>" />
            </div>
            <div class="form-group">
                % of Goods Delivered in 8 to 14 days
				<asp:TextBox CssClass="form-control" runat="server" ID="txtPercentDelivery8to14" Text="<%# Item.PercentDelivery8to14 %>" />
            </div>
            <div class="form-group">
                % of Goods Delivered over 30 days
				<asp:TextBox CssClass="form-control" runat="server" ID="txtPercentDeliveryOver30" Text="<%# Item.PercentDeliveryOver30 %>" />
            </div>
            <div class="form-group">
                Country
				<netpay:CountryDropDown CssClass="form-control" BlankSelectionText="Choose country" BlankSelectionValue="-1" runat="server" ID="ddlBusinessInfoCountry" Value='<%# string.IsNullOrEmpty(Item.BusinessInfoCountryIsoCode)? "-1" : Netpay.Bll.Country.Get(Item.BusinessInfoCountryIsoCode).ID.ToString() %>'></netpay:CountryDropDown>
            </div>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Bank Details">
    <div class="row">
        <div class="col-lg-4">
            <div class="form-group">
                Bank routing number
				<asp:TextBox CssClass="form-control" runat="server" ID="txtBankRoutingNumber" Text="<%# Item.BankRoutingNumber %>" />
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                Bank account number
				<asp:TextBox CssClass="form-control" runat="server" ID="txtBankAccountNumber" Text="<%# Item.BankAccountNumber %>" />
            </div>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Canceled Check Image">
    <asp:MultiView runat="server" ActiveViewIndex='<%#Item!=null && Item.MerchantID==null? 0 : 1 %>'>
        <asp:View runat="server">
            <asp:PlaceHolder runat="server" Visible='<%#Item!=null && !string.IsNullOrEmpty(Item.CanceledCheckImageFileName) %>'>File name <b><%# Item.CanceledCheckImageFileName %></b> was already chosen and saved<br />
                You can choose other file before registering locally.
                <br />
            </asp:PlaceHolder>
            <br />
        </asp:View>
        <asp:View runat="server">
            Registration item was already registered locally.<br />
            <asp:PlaceHolder runat="server" Visible='<%#Item!=null && !string.IsNullOrEmpty(Item.CanceledCheckImageFileName) %>'>File name <b><%# Item.CanceledCheckImageFileName %></b> was already chosen and saved<br />
                You can choose file that will be sent when connecting to third party.
                <br />
            </asp:PlaceHolder>
        </asp:View>
    </asp:MultiView>
    <div>
        <asp:FileUpload runat="server" ID="fuCanceledCheckImage" />
    </div>
</admin:FormSections>

