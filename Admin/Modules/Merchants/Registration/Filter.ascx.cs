﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Bll;
using Netpay.Infrastructure;

namespace Netpay.Admin.Merchants.Registration
{
	public partial class Filter : Controls.TemplateControlBase
	{
		protected override void OnLoad(EventArgs e)
		{
			TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
            base.OnLoad(e);
		}

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
		{
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Bll.Merchants.Registration.SearchFilters();
            sf.RegisteredAtCardConnect = chbRegisteredAtCardConnect.Checked;
            sf.RegisteredLocally = chbRegisteredLocaly.Checked;
            sf.ShowAll = chbShowAll.Checked;
            TemplatePage.SetFilter(sf);
        }

        protected void Export_Click(object sender, EventArgs e)
		{
			/*
			var list = GetFilteredList();
			var listIndustries = Netpay.Bll.ThirdParty.CardConnect.GetIndustries(WebUtils.DomainHost);
			var listBusinessTypes = Netpay.Bll.ThirdParty.CardConnect.GetBusinessTypes(WebUtils.DomainHost);
			Response.ContentType = "application/vnd.ms-excel";
			Response.AddHeader("Content-Disposition", "attachment;filename=report.xls");
			Response.Write(list.ToHtmlTable(new System.Collections.Generic.HashSet<string>(new string[] { "CanceledCheckImageContent" }), true, 
				delegate(System.Reflection.PropertyInfo p, object item, ref object value) {
					if (value == null) value = string.Empty;
					if (p.Name == "Industry" && listIndustries.ContainsKey(value.ToString())) value = listIndustries[value.ToString()];
					else if (p.Name == "TypeOfBusiness" && listBusinessTypes.ContainsKey(value.ToString())) value = listBusinessTypes[value.ToString()];
				}
			));
			Response.End();
			*/
		}
	}
}