﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;

namespace Netpay.Admin.Merchants.Registration.CardConnect
{
    public partial class CardConnectIntegration : Controls.TemplateControlBase
    {
        public Bll.Merchants.Registration Item { get { return TemplatePage.GetItemData<Bll.Merchants.Registration>(); } set { TemplatePage.SetItemData(value); } }

        protected void RegisterAtCardConnect_Command(object sender, CommandEventArgs e)
        {
            TemplatePage.PageController.OnSaveItem(EventArgs.Empty);
            string regResultMessage = null;
            bool result = Netpay.Bll.ThirdParty.CardConnect.CreateMerchant(Item.ID, out regResultMessage);

            string attachmentResultMessage = null;
            if (result)
                result = Netpay.Bll.ThirdParty.CardConnect.SendAttachment(Item.ID, out attachmentResultMessage);
            TemplatePage.FormActionNotify.SetMessage(regResultMessage + "<br>" + attachmentResultMessage);
            TemplatePage.PageController.FormView.DataBind();
        }
    }
}