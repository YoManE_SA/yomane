﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CardConnectIntegration.ascx.cs" Inherits="Netpay.Admin.Merchants.Registration.CardConnect.CardConnectIntegration" %>
<admin:FormSections runat="server" Title="Card Connect Integration">
    <asp:Button ID="btnRegisterAtCardConnect" OnCommand="RegisterAtCardConnect_Command" Text="Register at CardConnect" CssClass="btn btn-primary" runat="server" Enabled='<%# Item!=null && Item.ID != Guid.Empty && !Item.IntegrationData.ContainsKey("CardConnectID") %>' />
</admin:FormSections>
