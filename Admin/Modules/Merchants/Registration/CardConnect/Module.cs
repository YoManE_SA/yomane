﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Merchants.Registration.CardConnect
{
    public class Module : Admin.CoreBasedModule
    {
        public override string Name { get { return "Card Connect"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return "Card Connect Integration"; } }

        public Module() : base(Netpay.Bll.ThirdParty.CardConnects.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //No menu item

            //No route

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {            
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {            
            base.OnDeactivate(e);
        }
        

        private void Application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "Merchant.Registration")
            {
                //var IntegrationsControl = page.LoadControl("~/Modules/Merchants/Registration/Integrations.ascx");
                //var actionsFormSection = IntegrationsControl.FindControl("ActionsFormSection");
                //actionsFormSection.Controls.Add(page.LoadControl("~/Modules/Merchants/Registration/CardConnect/CardConnectIntegration.ascx"));
                page.AddControlToForm("Integrations", page.LoadControl("~/Modules/Merchants/Registration/CardConnect/CardConnectIntegration.ascx"),this, "", new Controls.SecuredObjectSelector(Bll.Merchants.Registration.SecuredObject, Bll.ThirdParty.CardConnect.SecuredObject).GetActivObject);
            }
        }
    }
}