﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Merchants.Registration.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Registrations">
    <Header></Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" SaveAjaxState="true" ID="rptList" DataKeyNames="ID" AutoGenerateColumns="false" BubbleLoadEvent="true">
                <Columns>
                    <asp:TemplateField HeaderText="Registration Date">
                        <ItemTemplate>
                            <asp:Label runat="server" Text='<%# CreationDates != null ?CreationDates.Where(x => x.Key == (Guid)Eval("ID")).SingleOrDefault().Value.ToShortDateString() : "" %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="ID" DataField="ID" SortExpression="ID" />
                    <asp:BoundField HeaderText="Legal Business Name" DataField="LegalBusinessName" SortExpression="LegalBusinessName" />
                    <asp:BoundField HeaderText="First Name" DataField="FirstName" SortExpression="FirstName" />
                    <asp:BoundField HeaderText="Last Name" DataField="LastName" SortExpression="LastName" />
                    <asp:BoundField HeaderText="Phone" DataField="Phone" SortExpression="Phone" />
                    <asp:TemplateField HeaderText="Account ID">
                        <ItemTemplate>
                            <%# Eval("GetAccountID") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd="true" />
    </Footer>

</admin:ListSection>
