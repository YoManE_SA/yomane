﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Merchants.Registration
{
	public partial class List : Controls.TemplateControlBase
	{
        public Dictionary<Guid, DateTime> CreationDates { get; set; }
             
        protected override void OnLoad(EventArgs e)
		{
			rptList.DataBinding += List_DataBinding;
			base.OnLoad(e);
		}

		private void List_DataBinding(object sender, EventArgs e)
		{
            var result = Bll.Merchants.Registration.Search(TemplatePage.GetFilter<Bll.Merchants.Registration.SearchFilters>());
            rptList.DataSource = result.List;
            CreationDates = result.CreationDates;
        }
	}
}