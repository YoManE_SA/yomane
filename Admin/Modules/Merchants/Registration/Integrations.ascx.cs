﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;

namespace Netpay.Admin.Merchants.Registration
{
	public partial class Integrations : Controls.TemplateControlBase
	{
		public Bll.Merchants.Registration Item { get { return TemplatePage.GetItemData<Bll.Merchants.Registration>(); } set { TemplatePage.SetItemData(value); } }

		protected void RegisterLocally_Command(object sender, CommandEventArgs e)
		{
            //Save operation should be invoked only when pressing "Save" 
            //And not when pressing RegisterLocally.
            //TemplatePage.PageController.OnSaveItem(EventArgs.Empty);
            //--------------------------------------------------------
            TemplatePage.PageController.LoadActiveItem();
            bool isCreated = false;
            try
            {
                 isCreated = Item.CreateMerchantLocally();
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("?NotAuthorized"))
                {
                    throw ex;
                }
                else
                {
                    TemplatePage.FormActionNotify.SetException(ex);
                    TemplatePage.PageController.ListView.BindAndUpdate();
                    TemplatePage.PageController.FormView.BindAndUpdate();
                    return;
                }
            }
            if (isCreated) TemplatePage.FormActionNotify.SetSuccess("RegisterLocally OK");
            else TemplatePage.FormActionNotify.SetMessage("RegisterLocally failed , duplicates found",true);

            TemplatePage.PageController.ListView.DataBind();
            TemplatePage.PageController.ListView.Update();
            
			TemplatePage.PageController.FormView.DataBind();
            TemplatePage.PageController.FormView.Update();
        }
	}
}