﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;
using Netpay.Bll;

namespace Netpay.Admin.Merchants.Registration
{
    public partial class Form : Controls.TemplateControlBase
    {
        public Bll.Merchants.Registration Item { get { return TemplatePage.GetItemData<Bll.Merchants.Registration>(); } set { TemplatePage.SetItemData(value); } }
        protected void Page_Load(object sender, EventArgs e)
        {
            TemplatePage.PageController.LoadItem += PageController_LoadItem;
            TemplatePage.PageController.FormView.DataBinding += FormView_DataBinding;
            TemplatePage.PageController.FormView.DataBound += FormView_DataBound;
            TemplatePage.PageController.SaveItem += PageController_SaveItem;
            TemplatePage.PageController.DeleteItem += PageController_DeleteItem;

            //Get reference to Save button.
            var saveButton = (TemplatePage.FormPanel.Footer.Controls[0].Controls.OfType<Button>().Where(x => x.Text == "Save")).FirstOrDefault();

            //Get reference to the currenct Script Manager
            var sm = System.Web.UI.ScriptManager.GetCurrent(TemplatePage);

            //Register the Save Button as PostBack control in order to invoke Full PostBack when clicking
            //Save so the Details of the File in the FileUpload control will be sent to the server
            if (saveButton != null) sm.RegisterPostBackControl(saveButton);
        }

        private void PageController_DeleteItem(object sender, EventArgs e)
        {
            Item.Delete();
        }

        private void PageController_LoadItem(object sender, EventArgs e)
        {
            var itemID = TemplatePage.PageController.DataKey["ID"].ToNullableGuid();
            if (itemID == null) // That means you in context of new item so create one.
            {
                Item = new Bll.Merchants.Registration();
            }
            else
            {
                Item = Bll.Merchants.Registration.Load(itemID.Value);
            }
        }

        private void FormView_DataBinding(object sender, EventArgs e)
        {
            if (Item == null) Item = new Bll.Merchants.Registration();

            //Take the industry from List.SICCode
            //ddlIndustry.DataSource = Netpay.Bll.ThirdParty.CardConnect.GetIndustries();

            //TypeOfBusiness is hardcoded for now.
            //ddlTypeOfBusiness.DataSource = Netpay.Bll.ThirdParty.CardConnect.GetBusinessTypes();
            //base.DataBindChildren();
        }

        private void FormView_DataBound(object sender, EventArgs e)
        {
            if (Item == null) return;

            //ddlState 
            if (!string.IsNullOrEmpty(Item.State))
            {
                ddlState.ISOCode = Item.State;
            }
            else
            {
                ddlState.SelectedValue = "-1";
            }
            
            //ddlPhisicalState
            if (!string.IsNullOrEmpty(Item.PhisicalState))
            {
                ddlPhisicalState.ISOCode = Item.PhisicalState;
            }
            else
            {
                ddlPhisicalState.SelectedValue = "-1";
            }

            //ddlStateOfIncorporation
            if (!string.IsNullOrEmpty(Item.StateOfIncorporation))
            {
                ddlStateOfIncorporation.ISOCode = Item.StateOfIncorporation;
            }
            else
            {
                ddlStateOfIncorporation.SelectedValue = "-1";
            }
            
            //Type of business
            if (Item.TypeOfBusiness != null)
            {
                ddlTypeOfBusiness.SelectedValue = Item.TypeOfBusiness.Value.ToString();
            }
            else
            {
                ddlTypeOfBusiness.SelectedValue = "0";
            }
        }

        protected void PageController_SaveItem(object sender, EventArgs e)
        {
            if (ddlContactDetailsCountry.SelectedValue == "-1" || ddlBusinessInfoCountry.SelectedValue == "-1")
            {
                RegActionNotify.SetMessage("You must select contact details and business info countries", true);
                UpAction.Update();
                return;
            }

            Item.DbaName = txtDbaName.Text;
            Item.LegalBusinessName = txtLegalBusinessName.Text;
            Item.LegalBusinessNumber = txtLegalBusinessNumber.Text;
            Item.FirstName = txtFirstName.Text;
            Item.LastName = txtLastName.Text;
            Item.Email = txtEmail.Text;
            Item.Address = txtAddress.Text;
            Item.City = txtCity.Text;
            Item.State = ddlState.SelectedValue != "-1" ? ddlState.ISOCode : ""; 
            Item.ContactDetailsCountryIsoCode = ddlContactDetailsCountry.ISOCode;
            Item.Zipcode = txtZipcode.Text;
            Item.OwnerDob = txtOwnerDob.Value;
            Item.OwnerSsn = txtOwnerSsn.Text;
            Item.Phone = txtPhone.Text;
            Item.Fax = txtFax.Text;
            Item.Url = txtUrl.Text;
            Item.PhisicalAddress = txtPhysicalAddress.Text;
            Item.PhisicalCity = txtPhisicalCity.Text;
            Item.PhisicalState = ddlPhisicalState.SelectedValue!= "-1" ?  ddlPhisicalState.ISOCode : "";
            Item.PhisicalZip = txtPhisicalZip.Text;
            Item.StateOfIncorporation = ddlStateOfIncorporation.SelectedValue != "-1"? ddlStateOfIncorporation.ISOCode : "";
            Item.BusinessStartDate = txtBusinessStartDate.Value;
            Item.TypeOfBusiness = ddlTypeOfBusiness.SelectedValue == "0" ? null : ddlTypeOfBusiness.SelectedValue.ToNullableInt();
            Item.BusinessInfoCountryIsoCode = ddlBusinessInfoCountry.ISOCode;
            Item.Industry = ddlIndustry.SelectedValue.ToNullableInt();
            Item.BusinessDescription = txtBusinessDescription.Text;
            Item.AnticipatedMonthlyVolume = txtAnticipatedMonthlyVolume.Text.ToNullableDecimal();
            Item.AnticipatedAverageTransactionAmount = txtAnticipatedAverageTransactionAmount.Text.ToNullableDecimal();
            Item.AnticipatedLargestTransactionAmount = txtAnticipatedLargestTransactionAmount.Text.ToNullableDecimal();
            Item.BankRoutingNumber = txtBankRoutingNumber.Text;
            Item.BankAccountNumber = txtBankAccountNumber.Text;
            Item.PercentDelivery0to7 = txtPercentDelivery0to7.Text.ToInt32(0);
            Item.PercentDelivery15to30 = txtPercentDelivery15to30.Text.ToInt32(0);
            Item.PercentDelivery8to14 = txtPercentDelivery8to14.Text.ToInt32(0);
            Item.PercentDeliveryOver30 = txtPercentDeliveryOver30.Text.ToInt32(0);

            if (fuCanceledCheckImage.HasFile)
            {
                Item.CanceledCheckImageFileName = fuCanceledCheckImage.PostedFile.FileName;
                Item.CanceledCheckImageMimeType = fuCanceledCheckImage.PostedFile.ContentType;
                Item.CanceledCheckImageContent = new System.IO.BinaryReader(fuCanceledCheckImage.PostedFile.InputStream).ReadBytes((int)fuCanceledCheckImage.PostedFile.InputStream.Length).ToBase64();
            }

            Item.Save();
        }

        protected void Delete_Command(object sender, CommandEventArgs e)
        {
            
        }

    }
}