﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Integrations.ascx.cs" Inherits="Netpay.Admin.Merchants.Registration.Integrations" %>
<admin:FormSections ID="FormSections1" runat="server" Title="Values">
	<asp:Repeater runat="server" ID="rptIntegrationData" DataSource='<%# Item.IntegrationData %>'>
		<ItemTemplate>
		    <div class="row">
				<div class="col-lg-4"><%# Eval("Key") %></div>
				<div class="col-lg-8"><%# Eval("Value") %></div>
			</div>
		</ItemTemplate>
	</asp:Repeater>
</admin:FormSections>
<admin:FormSections ID="ActionsFormSection" runat="server" Title="Register Locally">
	<asp:Button ID="btnRegisterAtNetpay" OnCommand="RegisterLocally_Command" Text="Register Locally" runat="server" CssClass="btn btn-primary" Enabled="<%# Item.ID != default(Guid) && Item.MerchantID == null %>" />
</admin:FormSections>
