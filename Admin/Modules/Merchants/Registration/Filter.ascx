﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Merchants.Registration.Filter" %>
<admin:FilterSection runat="server" Title="Status">
    <div class="row">
        <div class="col-lg-12">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox ID="chbShowAll" runat="server" Checked="true" Text="Show All" /></li>
                <li class="list-group-item">
                    <asp:CheckBox ID="chbRegisteredAtCardConnect" runat="server" Checked="false" Text="Registered At CardConnect" /></li>
                <li class="list-group-item">
                    <asp:CheckBox ID="chbRegisteredLocaly" runat="server" Checked="false" Text="Registered Localy" /></li>
            </ul>
        </div>
    </div>
</admin:FilterSection>

