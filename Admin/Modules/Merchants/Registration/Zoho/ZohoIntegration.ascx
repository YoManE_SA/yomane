﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ZohoIntegration.ascx.cs" Inherits="Netpay.Admin.Merchants.Registration.Zoho.ZohoIntegration" %>
<admin:FormSections runat="server" Title="Zoho Integration">
    <asp:Button runat="server" ID="btnRegisterAtZoho" OnCommand="RegisterAtZoho_Command" Text="Register at Zoho" CssClass="btn btn-primary"  Enabled="<%# Item != null && Item.ID != Guid.Empty %>"/>
</admin:FormSections>