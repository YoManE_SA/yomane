﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Merchants.Registration.Zoho
{
    public class Module : Admin.CoreBasedModule
    {
        public override string Name { get { return "Zoho"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return "Zoho Integration"; } }

        public Module() : base(Bll.ThirdParty.Zoho.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //No menu item

            //No route

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {            
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {            
            base.OnDeactivate(e);
        }

        private void Application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "Merchant.Registration")
            {
                page.AddControlToForm("Integrations", page.LoadControl("~/Modules/Merchants/Registration/Zoho/ZohoIntegration.ascx"), this, "", new Controls.SecuredObjectSelector(Bll.Merchants.Registration.SecuredObject, Bll.ThirdParty.ZohoCRM.SecuredObject).GetActivObject);
            }
        }
    }
}