﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Merchants.Registration.Zoho
{
    public partial class ZohoIntegration : Controls.TemplateControlBase
    {
        public Bll.Merchants.Registration Item { get { return TemplatePage.GetItemData<Bll.Merchants.Registration>(); } set { TemplatePage.SetItemData(value); } }

        protected void RegisterAtZoho_Command(object sender, CommandEventArgs e)
        {
            TemplatePage.PageController.OnSaveItem(EventArgs.Empty);
            string regResultMessage = null;
            Netpay.Bll.ThirdParty.ZohoCRM.CreateLead(Item.ID, out regResultMessage);
            TemplatePage.FormActionNotify.SetMessage(regResultMessage);
            TemplatePage.PageController.FormView.DataBind();
        }
    }
}