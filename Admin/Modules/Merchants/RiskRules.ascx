﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RiskRules.ascx.cs" Inherits="Netpay.Admin.Merchants.RiskRules" %>
<admin:FormSections runat="server" Title="Limitation By Payment Method" Flexible="false" OnCommand="Edit_Command" EnableAdd='<%#(Account != null && Account.AccountID != 0) %>' Visible="true">
    <asp:MultiView ID="AccountTransactionMultiView" runat="server" ActiveViewIndex='<%# (Account != null && Account.AccountID != 0) ? 0 : 1%>'>
        <asp:View ID="ViewIfVisible" runat="server">
            <div class="row">
                <NP:UpdatePanel runat="server" ID="upList" RenderMode="Block" ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>
                        <netpay:DynamicRepeater runat="server" ID="rptMerchantRules">
                            <HeaderTemplate>
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-customize">
                                            <thead>
                                                <th class="text-nowrap">Action</th>
                                                <th class="text-nowrap">Rules</th>
                                                <th class="text-nowrap">Status</th>
                                                <th class="text-nowrap">Time Frame:</th>
                                                <th class="text-nowrap">Block Duration:</th>
                                                <th class="text-nowrap">Payment Method:</th>
                                                <th class="text-nowrap">Credit Type:</th>
                                                <th class="text-nowrap">Whitelist Level:</th>
                                                <th class="text-nowrap">Amount Processed:</th>
                                                <th class="text-nowrap">Trans Count:</th>
                                                <th class="text-nowrap">Reply (Amount): </th>
                                                <th class="text-nowrap">Reply (Trans Count):</th>
                                                <th class="text-nowrap">Apply in Virtual Terminal:</th>
                                            </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tbody>
                                    <td>
                                        <NP:Button ID="Button1" runat="server" CssClass="btn btn-primary text-uppercase" OnCommand="Edit_Command" CommandName="Edit" CommandArgument='<%# Eval("ID") %>' Text="Edit" />
                                    </td>
                                    <td class="text-nowrap">
                                        <asp:Literal runat="server" ID="Literal1" Text='<%# Eval("ID") %>' />
                                        -
                                        <asp:Literal runat="server" ID="ddlReplySource" Text='<%# ((Netpay.Bll.Risk.MerchantRiskRule.ReplySources) Eval("ReplySource")).EnumText() %>' />
                                    </td>
                                    <td class="text-nowrap">
                                        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex='<%# ((bool)Eval("IsActive")) ? 1 : 0 %>'>
                                            <asp:View ID="View1" runat="server"><span style="color: #29af00;"><i class="fa fa-check"></i></span></asp:View>
                                            <asp:View ID="View2" runat="server"><span style="color: #af0000;"><i class="fa fa-times "></i></span></asp:View>
                                        </asp:MultiView>
                                    </td>
                                    <td>
                                        <asp:Literal ID="txtTimeFrameDays" runat="server" Text='<%# Eval("TimeFrameText") %>' />
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" Text='<%# Eval("BlockDurationText") %>' />
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" ID="ddlPaymentMethod" Text='<%#Eval("PaymentMethod")==null? "[All]": Netpay.Bll.PaymentMethods.PaymentMethod.Get((short)Eval("PaymentMethod")).Name%>' />
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" ID="ddlCreditType" Text='<%# (byte)Eval("CreditType")==255? "[All]" : Netpay.Bll.Merchants.CreditType.GetCreditName((byte)Eval("CreditType"),WebUtils.CurrentLanguage) %>' />
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" ID="ddlWhiteListLevel" Text='<%# Eval("WhiteListLevel") == null? "[Not Listed]" : ((Netpay.Bll.Risk.MerchantRiskRule.WhiteListLevel?) (int?)Eval("WhiteListLevel")).EnumText() %>' />
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" ID="txtMaxAmount" Text='<%# Eval("MaxAmount", "{0:0.00}") + " " + Eval("CurrencyIso") %>' />
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" ID="txtMaxCount" Text='<%# Eval("MaxCount") %>' />
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" Text='<%# Eval("MaxAmountReplyCode").ToString()==""? "586 - " + Netpay.Bll.DebitCompanies.ResponseCode.GetItem(1,"586").Description : Eval("MaxAmountReplyCode") +" - " +  Netpay.Bll.DebitCompanies.ResponseCode.GetItem(1,Eval("MaxAmountReplyCode").ToString()).Description%>' />
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" Text='<%# Eval("MaxCountReplyCode").ToString()==""? "585 - " + Netpay.Bll.DebitCompanies.ResponseCode.GetItem(1,"585").Description : Eval("MaxCountReplyCode") +" - " +  Netpay.Bll.DebitCompanies.ResponseCode.GetItem(1,Eval("MaxCountReplyCode").ToString()).Description %>' />
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" ID="chkApplyVT" Text='<%# Eval("ApplyVT") %>' />
                                    </td>
                                </tbody>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                                </div>
                                </div>
                            </FooterTemplate>
                            <EmptyContainer>
                                <div class="alert alert-info">
                                    <strong>Info!</strong> No record found.
                                </div>
                            </EmptyContainer>
                        </netpay:DynamicRepeater>
                    </ContentTemplate>
                </NP:UpdatePanel>
            </div>
        </asp:View>

        <asp:View ID="ViewIfNotVisible" runat="server">
            <div class="alert alert-info">
                You need to save your details in order to edit this section.
            </div>
        </asp:View>
    </asp:MultiView>
</admin:FormSections>

<admin:ModalDialog runat="server" Title="Edit Rule" ID="dlgEdit" OnDataBinding="dlgEdit_DataBinding" OnCommand="Edit_Command">
    <Body>
        <asp:HiddenField runat="server" ID="hfID" Value='<%# ItemData.ID %>' />
        <div class="row">
            <div class="col-lg-6 col-md-6">
                Status:
                <div class="form-group">
                    <netpay:EnumDropDown CssClass="form-control" runat="server" ID="ddlReplySource" ViewEnumName="Netpay.Bll.Risk.MerchantRiskRule+ReplySources" Value='<%# ItemData.ReplySource %>' EnableBlankSelection="False" />
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                Time Frame:
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <JQ:Spinner runat="server" ID="CCRM_HourNumber" MinimumValue="0" MaximumValue="60" Width="60px"></JQ:Spinner>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <asp:DropDownList CssClass="form-control" runat="server" ID="CCRM_HourFactor">
                                <asp:ListItem Value="1">Hours</asp:ListItem>
                                <asp:ListItem Value="24">Days</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                Payment Method:
                <div class="form-group">
                    <netpay:PaymentMethodDropDown CssClass="form-control" runat="server" ID="ddlPaymentMethod" Value='<%# (int?)ItemData.PaymentMethod %>' BlankSelectionText="[All]" />
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                Credit Type:
                <div class="form-group">
                    <netpay:CreditTypeDropDown CssClass="form-control" runat="server" ID="ddlCreditType" Value='<%# (int)ItemData.CreditType %>' EnableBlankSelection="true" BlankSelectionValue="255" BlankSelectionText="[All]" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                Limited to WhiteList Level:
                <div class="form-group">
                    <netpay:EnumDropDown CssClass="form-control" runat="server" ID="ddlWhiteListLevel" ViewEnumName="Netpay.Bll.Risk.MerchantRiskRule+WhiteListLevel" Value='<%# !ItemData.WhitelistLevel.HasValue? -1 : ItemData.WhitelistLevel %>' EnableBlankSelection="True" BlankSelectionText="[Not Listed]" BlankSelectionValue="-1" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                Limit Processed Amount:
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <netpay:CurrencyDropDown runat="server" CssClass="form-control" ID="ddlCurrencyID" Value='<%# (int)ItemData.CurrencyID %>' BlankSelectionValue="-1" />
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtMaxAmount" Text='<%# ItemData.MaxAmount.ToString("0.00") %>' />
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                Reply For Amount Limitation:
                <div class="form-group">
                    <netpay:MerchantRiskMngReplyCodeDropDown ID="ddlMaxAmountReplyCode" runat="server" CssClass="form-control" Value='<%# string.IsNullOrEmpty(ItemData.MaxAmountReplyCode)? "586" : ItemData.MaxAmountReplyCode %>' EnableBlankSelection="false"></netpay:MerchantRiskMngReplyCodeDropDown>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                Limit Transaction Count:
                <div class="form-group">
                    <asp:TextBox runat="server" ID="txtMaxCount" Text='<%# ItemData.MaxCount %>' CssClass="form-control" />
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                Reply For Trans Count Limitation:
                <div class="form-group">
                    <netpay:MerchantRiskMngReplyCodeDropDown ID="ddlMaxCountReplyCode" runat="server" CssClass="form-control" Value='<%# string.IsNullOrEmpty(ItemData.MaxCountReplyCode)? "585" : ItemData.MaxCountReplyCode %>' EnableBlankSelection="false"></netpay:MerchantRiskMngReplyCodeDropDown>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 margin-bottom-10">
                Block Duration:
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <JQ:Spinner runat="server" ID="CCRM_BlockHourNumber" MinimumValue="0" MaximumValue="60" Width="60px"></JQ:Spinner>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <asp:DropDownList CssClass="form-control" runat="server" ID="CCRM_BlockHourFactor">
                                <asp:ListItem Value="1">Hours</asp:ListItem>
                                <asp:ListItem Value="24">Days</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <asp:CheckBox runat="server" ID="chkIsActive" Checked='<%# ItemData.IsActive %>' Text="Is Active" />
                <asp:CheckBox runat="server" ID="chkApplyVT" Checked='<%# ItemData.ApplyVT %>' Text="Active in Virtual terminal" />
            </div>
        </div>
    </Body>
    <Footer>
        <asp:Button runat="server" ID="btnDelete" CssClass="btn btn-danger" Text="Delete" CommandName="Delete" />
        <asp:Button runat="server" ID="btnUpdate" CssClass="btn btn-primary" Text="Save" CommandName="Save" />
    </Footer>
</admin:ModalDialog>
