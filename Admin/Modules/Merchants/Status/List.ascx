﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Modules.Merchants.Status.List" %>
<admin:ListSection runat="server" Title="Merchants Status">
    <Header>
        <admin:LegendColors runat="server" Items='<%# Netpay.Bll.Merchants.Merchant.MerchantStatusColor %>' FloatRight="true" />
    </Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" SaveAjaxState="true" ID="rptList" DataKeyNames="AccountID" AutoGenerateColumns="false" BubbleLoadEvent="false" DisableRowSelect="true">
                <Columns>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <span style="background-color: <%# Netpay.Bll.Merchants.Merchant.MerchantStatusColor.MapHtmlColor((MerchantStatus)Eval("ActiveStatus")) %>;" class="legend-item"></span>&nbsp;
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Status" SortExpression="ID">
                        <ItemTemplate>
                            <%# (MerchantStatus)Eval("ActiveStatus") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="ID" DataField="AccountID" />

                    <%-- A link that sends the user to merchants management section with the selected merchant opened --%>
                    <asp:TemplateField runat="server" HeaderText="Name">
                        <ItemTemplate>
                            <asp:HyperLink
                                runat="server"
                                Text='<%# Eval("Name") %>'
                                NavigateUrl='<%# WebUtils.ApplicationPath + "/Merchants/0?ctl00.apAccount.hdID="+Eval("AccountID")+"&Search=1&SelectedIndex=0" %>'
                                Target="_self" />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Group" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Literal runat="server" Text='<%# ((int?)Eval("MerchantGroup")).HasValue? (Netpay.Bll.Merchants.Group.Get(((int?)Eval("MerchantGroup")).Value).Name) : "---" %>'>
                            </asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField HeaderText="Signup Date" DataField="InsertDate" />
                    <asp:BoundField HeaderText="Unsettled T'" DataField="DateLastTransPass" DataFormatString="{0:d}" NullDisplayText="---" />
                    <asp:BoundField HeaderText="Rejected T'" DataField="DateLastTransFail" DataFormatString="{0:d}" NullDisplayText="---" />

                    <asp:TemplateField HeaderText="Account Manager">
                        <ItemTemplate>
                            <asp:Literal runat="server" Text='<%# int.TryParse((string)Eval("AccountManager"), out number)? Netpay.Infrastructure.Security.AdminUser.GetAdminUserNameById(int.Parse((string)Eval("AccountManager"))) : (((string)Eval("AccountManager")) != "" ? Eval("AccountManager") : "---") %>'>
                            </asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField HeaderText="Last Settlement" DataField="DateLastSettlement" DataFormatString="{0:d}" NullDisplayText="---" />

                    <asp:TemplateField HeaderText="History">
                        <ItemTemplate>
                            <%--NavigateUrl='<%# GetTransactionHistory((int)Eval("ID")) %>'--%>
                            <asp:HyperLink 
                                Style="cursor: pointer"
                                runat="server"
                                Text="Show"
                                Target="_blank"
                                onclick='<%# "OpenPop(" + "\"" + GetTransactionHistory((int)Eval("AccountID")) + "\"" + ",\"Transaction history\",\"1330\",\"820\" , \"1\"); event.cancelBubble=true; return false;" %>' />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Update Status">
                        <ItemTemplate>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-btn"></span>
                                    <netpay:MerchantStatusDropDown ID="ddlRowMerchantStatus" CssClass="form-control" EnableBlankSelection="false" Value='<%# (int)Eval("ActiveStatus") %>' runat="server" /><span class="input-group-btn">
                                        <asp:Button Text="Update" CssClass="btn btn-info" runat="server" OnCommand="UpdateMerchantStatus" CommandArgument='<%#Eval("ID")+"~"+(int)Eval("ActiveStatus")+"~"+Container.DataItemIndex%>' />
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd="false" />
    </Footer>
</admin:ListSection>
