﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Admin.Modules.Merchants.Status
{
    public partial class List : Controls.TemplateControlBase
    {
        public DateTime LastTransactionMadeByMerchant { get; set; }
        public int number;

        protected override void OnLoad(EventArgs e)
        {
            rptList.DataBinding += List_DataBinding;
            base.OnLoad(e);
        }

        private void List_DataBinding(object sender, EventArgs e)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Merchants.Status.Module.SecuredObject, PermissionValue.Read);

            rptList.DataSource = Bll.Merchants.Merchant.Search(TemplatePage.GetFilter<Bll.Merchants.Merchant.SearchFilters>(), rptList);
            TemplatePage.PageController.ListView.Update();
        }

        public string GetTransactionHistory(int merchantid)
        {
            if (merchantid <= 0) return "";

            //Get the current merchant by ID
            var sf = new Bll.Merchants.Merchant.SearchFilters();
            var IdRange = new Infrastructure.Range<int?> { From = merchantid, To = merchantid };
            sf.ID = IdRange;
            var merchant = Bll.Merchants.Merchant.Search(sf, null).FirstOrDefault();

            //Get the latest transaction made:
            List<DateTime?> MerchantLastTransactions = new List<DateTime?>();
            MerchantLastTransactions.Add(merchant.DateLastTransFail);
            MerchantLastTransactions.Add(merchant.DateLastTransPass);
            MerchantLastTransactions.Add(merchant.DateLastTransPending);
            MerchantLastTransactions.Add(merchant.DateLastTransPreAuth);

            if (MerchantLastTransactions.Any(x => x.HasValue))
            {
                LastTransactionMadeByMerchant = MerchantLastTransactions.Where(x => x.HasValue).Max(x => x.Value);
            }
            else
            {
                LastTransactionMadeByMerchant = DateTime.Now;
            }

            //string url = Web.WebUtils.ApplicationPath + "/Transactions/0?";
            string url ="../../Transactions/0?";
            string query = "ctl00.apMerchant.hdID=" + merchantid + "&ctl00.chklStatus$0=Captured&ctl00.chklStatus$1=Declined&ctl00.chklStatus$2=Authorized&ctl00.chklStatus$3=Pending&ctl00.chklStatus$4=DeclinedArchived&ctl00.BankDropDownList=0&Search=1&ctl00.rng_InsertDate.From=" + Server.UrlEncode(LastTransactionMadeByMerchant.AddMonths(-3).ToShortDateString()) + "&ctl00.rng_InsertDate.To=" + Server.UrlEncode(LastTransactionMadeByMerchant.ToShortDateString())+ "&NoLayout=1";
            url += query;
            return url;
        }

        protected void UpdateMerchantStatus(object sender, CommandEventArgs e)
        {
            //Get the command event arguments.
            string[] CommandArgs = e.CommandArgument.ToString().Split('~');
            //Get the selected merchant Id
            int merchantId = int.Parse(CommandArgs[0]);
            //Get the original MerchantStatus - before the user changed it - so if the user just pressed Update without changing the value nothing will happen.
            int OriginalMerchantStatusId = int.Parse(CommandArgs[1]);
            //Get the selected row index - in order to get the value of the status drop down
            int selectedRowIndex = int.Parse(CommandArgs[2]);

            //Get the new Status the user chose.
            int NewSelectedStatusId = int.Parse((((DropDownList)rptList.Rows[selectedRowIndex].FindControl("ddlRowMerchantStatus")).SelectedValue));

            //If nothing was changed and the user just pressed Update - do nothing.
            if (NewSelectedStatusId == OriginalMerchantStatusId) return;

            Bll.Merchants.Merchant.UpdateMerchantStatus(merchantId, (byte)NewSelectedStatusId);

            TemplatePage.PageController.FilterView.BindAndUpdate();
            TemplatePage.PageController.ListView.BindAndUpdate();
        }
    }
}