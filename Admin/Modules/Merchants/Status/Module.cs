﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Merchants.Status
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuItem;
        public override string Name { get { return "Merchant.Status"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return "Merchant status managment UI"; } }

        public Module() : base(Bll.Merchants.Status.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu item
            menuItem = new ApplicationMenu("Merchants Status", "~/Merchants/MerchantsStatus/0", null, 4);

            //Register route
            Application.RegisterRoute("Merchants/MerchantsStatus/{*id}", "Merchant.Status", typeof(Controls.DataTablePage), module : this);

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {            
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {            
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Merchants", menuItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuItem.Parent.RemoveChild(menuItem);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "Merchant.Status")
            {
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Merchants/Status/Filter.ascx"));
                page.AddControlToList(page.LoadControl("~/Modules/Merchants/Status/List.ascx"));
            }
        }
    }
}