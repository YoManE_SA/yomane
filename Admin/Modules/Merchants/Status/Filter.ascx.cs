﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Modules.Merchants.Status
{
    public partial class Filter : Controls.TemplateControlBase
    {
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
        }

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
        {
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Bll.Merchants.Merchant.SearchFilters();

            //Get the ID value , comes only from the URL , when the user is located at Merchants Managment and wants
            //To change the status , the link brings the user the the merchant status screen and invokes the filter with
            //The merchant id.
            sf.ID = apAccount.Value.HasValue ? new Range<int?>(apAccount.Value) : new Range<int?>(null);

            //Show only inactive Merchants , that means all merchants that there status
            //Is not Processing wil show.
            sf.ShowOnlyInactiveMerchants = ShowAllInactiveMerchants.Checked;

            //Status drop down input , if there is a status chosed get its value 
            if (ddlMerchantStatus.SelectedValue != "-1" && ddlMerchantStatus.SelectedValue!="")
            {
                sf.Status = new List<MerchantStatus>();
                sf.Status.Add(ddlMerchantStatus.SelectedValue.ToNullableEnum<Netpay.Infrastructure.MerchantStatus>().GetValueOrDefault());
            }


            //Group dropdown input 
            if (ddlMerchantGroup.SelectedValue != "-1" && ddlMerchantGroup.SelectedValue != "")
            {
                if (ddlMerchantGroup.SelectedValue == "NA")
                {
                    sf.Group = 0; 
                }
                else
                {
                    sf.Group = ddlMerchantGroup.Value.ToNullableInt();
                }
            }

            //Account Manager drop down input
            if (ddlAccountManager.SelectedValue != "-1" && ddlAccountManager.SelectedValue != "")
            {
                sf.AccountManager = ddlAccountManager.SelectedValue;
            }
                                                
            TemplatePage.SetFilter(sf);
        }
    }
}