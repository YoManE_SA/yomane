﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Modules.Merchants.Status.Filter" %>
<admin:FilterSection ID="MerchantStatusFilter" runat="server" Title="Merchant Status Filter">

    <%-- Hidden control , only used for getting value from the Query String --%>
    <admin:AccountPicker Visible="false" runat="server" ID="apAccount" LimitToType="Merchant" UseTargetID="false" />

    <div class="row">
        <div class="col-lg-12">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox Text="Show Only Inactive Merchants" runat="server" ID="ShowAllInactiveMerchants" />
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <asp:Label runat="server" Text="Merchant Status" />
                <netpay:MerchantStatusDropDown runat="server" ID="ddlMerchantStatus" CssClass="form-control" EnableBlankSelection="true" BlankSelectionText="[All]" BlankSelectionValue="-1" />
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <asp:Label runat="server" Text="Merchant Group" />
                <netpay:MerchantGroupsDropDown
                    runat="server"
                    ID="ddlMerchantGroup"
                    CssClass="form-control"
                    EnableBlankSelection="true" 
                    BlankSelectionText="[All]" 
                    BlankSelectionValue="-1" 
                    EnableNaSelection="true"
                    NaSelectionText="[N/A]"
                    NaSelectionValue="NA"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <asp:Label runat="server" Text="Account Manager" />
                <netpay:AccountManagersDropDown runat="server" CssClass="form-control" ID="ddlAccountManager" BlankSelectionValue="-1" BlankSelectionText="[All]" />
            </div>
        </div>
    </div>
</admin:FilterSection>
