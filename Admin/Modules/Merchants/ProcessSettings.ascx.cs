﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Merchants
{
	public partial class ProcessSettings : Controls.AccountControlBase
	{
        protected Netpay.Bll.Merchants.Merchant ItemData
        {
            get
            {
                return AccountPage.Account as Netpay.Bll.Merchants.Merchant;
            }
            private set
            {
                AccountPage.Account = value;
            }
        }

        protected Netpay.Bll.Merchants.ProcessSettings MerchantSettings { get; private set; }
		protected Netpay.Bll.Transactions.Recurring.MerchantSettings RecurringSettings { get; private set; }
		protected Netpay.Bll.Invoices.MerchantSettings InvoiceSettings { get; private set; }

        protected override void OnLoad(EventArgs e)
		{
			TemplatePage.PageController.SaveItem += PageController_SaveItem;
            
            base.OnLoad(e);
		}

		protected override void DataBindChildren()
		{
			if (Account != null) MerchantSettings = (Account as Netpay.Bll.Merchants.Merchant).ProcessSettings;
            if (MerchantSettings == null) MerchantSettings = new Bll.Merchants.ProcessSettings();

            if (Account != null) RecurringSettings = Netpay.Bll.Transactions.Recurring.MerchantSettings.Load(Account.MerchantID.GetValueOrDefault());
			if (RecurringSettings == null) RecurringSettings = new Netpay.Bll.Transactions.Recurring.MerchantSettings(0);
			if (Account != null) InvoiceSettings = Netpay.Bll.Invoices.MerchantSettings.Load(Account.MerchantID.GetValueOrDefault());
			if (InvoiceSettings == null) InvoiceSettings = new Netpay.Bll.Invoices.MerchantSettings(0);
            ddlInvoiceProvider.SelectedIndex = InvoiceSettings.ExternalProviderID;

            //Terminal owned by binding
            if (ItemData != null && ddlTerminalOwnedBy.Items.Count == 0)
            {
                ddlTerminalOwnedBy.Items.Add(new ListItem(Domain.Current.BrandName, "1"));
                ddlTerminalOwnedBy.Items.Add(new ListItem("Merchant", "0"));
                ddlTerminalOwnedBy.SelectedValue = ItemData.IsCompanyTerminal ? "1" : "0";
            }
            if (ItemData != null) affiliatesList.DataSource = Bll.Affiliates.MerchantSettings.ForMerchant(ItemData).OrderBy(x => x.AffiliateName).ToList();

            base.DataBindChildren();
		}

		protected void PageController_SaveItem(object sender, EventArgs e)
		{
            if (Bll.Merchants.ProcessSettings.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Edit))
            {
                MerchantSettings = (Account as Netpay.Bll.Merchants.Merchant).ProcessSettings;
                MerchantSettings.IsCcStorageCharge = chkIsCcStorageCharge.Checked;
                MerchantSettings.IsCaptureApprovalOnly = chkIsCaptureApprovalOnly.Checked;
                MerchantSettings.AutoCaptureHours = txtAutoCaptureHours.Text.ToInt32();
                MerchantSettings.IsApprovalOnly = chkIsApprovalOnly.Checked;
                MerchantSettings.MultiChargeProtectionMins = txtMultiChargeProtectionMins.Text.ToInt32();
                MerchantSettings.IsAllowRemotePull = chkIsAllowRemotePull.Checked;
                MerchantSettings.RemotePullIPs = txtRemotePullIPs.Text;
                MerchantSettings.IsRefund = chkIsRefund.Checked;
                MerchantSettings.IsAskRefund = chkIsAskRefund.Checked;
                MerchantSettings.IsAskRefundRemote = chkIsAskRefundRemote.Checked;
                MerchantSettings.RemoteRefundRequestIPs = txtRemoteRefundRequestIPs.Text;
                MerchantSettings.IsSystemPayECheck = chkIsSystemPayECheck.Checked;
                MerchantSettings.IsSystemPayCreditCard = chkIsSystemPayCreditCard.Checked;
                MerchantSettings.IsSystemPayPersonalNumber = chkIsSystemPayPersonalNumber.Checked;
                MerchantSettings.IsSystemPayCVV2 = chkIsSystemPayCVV2.Checked;
                MerchantSettings.IsSystemPayPhoneNumber = chkIsSystemPayPhoneNumber.Checked;
                MerchantSettings.IsSystemPayEmail = chkIsSystemPayEmail.Checked;
                MerchantSettings.IsRemoteCharge = chkIsRemoteCharge.Checked;
                MerchantSettings.IsRemoteChargePersonalNumber = chkIsRemoteChargePersonalNumber.Checked;
                MerchantSettings.IsRemoteChargeCVV2 = chkIsRemoteChargeCVV2.Checked;
                MerchantSettings.IsRemoteChargePhoneNumber = chkIsRemoteChargePhoneNumber.Checked;
                MerchantSettings.IsRemoteChargeEmail = chkIsRemoteChargeEmail.Checked;
                MerchantSettings.IsRemoteChargeEcheck = chkIsRemoteChargeEcheck.Checked;
                MerchantSettings.IsRemoteChargeEcheckPersonalNumber = chkIsRemoteChargeEcheckPersonalNumber.Checked;
                MerchantSettings.IsRemoteChargeEcheckPhoneNumber = chkIsRemoteChargeEcheckPhoneNumber.Checked;
                MerchantSettings.IsRemoteChargeEcheckEmail = chkIsRemoteChargeEcheckEmail.Checked;
                MerchantSettings.IsAutoPersonalSignup = chkIsAutoPersonalSignup.Checked;
                MerchantSettings.IsCcStorage = chkIsCcStorage.Checked;
                MerchantSettings.IsTerminalProbLog = chkIsTerminalProbLog.Checked;
                MerchantSettings.IsConnectionProbLog = chkIsConnectionProbLog.Checked;
                MerchantSettings.IsTransLookup = chkIsTransLookup.Checked;
                MerchantSettings.IsAllowMakePayments = chkIsAllowMakePayments.Checked;
                MerchantSettings.IsAllowBatchFiles = chkIsAllowBatchFiles.Checked;
                MerchantSettings.MerchantNotifyEmails = txtMerchantNotifyEmails.Text;
                MerchantSettings.IsPassNotificationSentToMerchant = chkIsPassNotificationSentToMerchant.Checked;
                MerchantSettings.IsFailNotificationSentToMerchant = chkIsFailNotificationSentToMerchant.Checked;
                MerchantSettings.IsPassNotificationSentToPayer = chkIsPassNotificationSentToPayer.Checked;
                MerchantSettings.IsRequiredClientIP = chkIsRequiredClientIP.Checked;
                MerchantSettings.IsAllowSilentPostCcDetails = chkIsAllowSilentPostCcDetails.Checked;

                ItemData.IsCompanyTerminal = ddlTerminalOwnedBy.SelectedValue == "1";
                (Account as Netpay.Bll.Merchants.Merchant).Save(false);
                        
                RecurringSettings = Netpay.Bll.Transactions.Recurring.MerchantSettings.Load(Account.MerchantID.GetValueOrDefault());
                if (RecurringSettings == null) RecurringSettings = new Netpay.Bll.Transactions.Recurring.MerchantSettings(Account.MerchantID.GetValueOrDefault());
                RecurringSettings.IsEnabled = chkIsAllowRecurring.Checked;
                RecurringSettings.MaxCharges = txtRecurringLimitCharges.Text.ToInt32();
                RecurringSettings.MaxStages = txtRecurringLimitStages.Text.ToInt32();
                RecurringSettings.MaxYears = txtRecurringLimitYears.Text.ToInt32();
                RecurringSettings.IsEnabledFromTransPass = chkIsAllowRecurringFromTrans.Checked;
                RecurringSettings.IsEnabledModify = chkIsAllowRecurringModify.Checked;
                RecurringSettings.Save();
            }

            if (Bll.Invoices.MerchantSettings.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Edit))
            {
                InvoiceSettings = Netpay.Bll.Invoices.MerchantSettings.Load(Account.MerchantID.GetValueOrDefault());
                if (InvoiceSettings == null) InvoiceSettings = new Netpay.Bll.Invoices.MerchantSettings(Account.MerchantID.GetValueOrDefault());
                InvoiceSettings.IsEnable = chkInv_IsEnable.Checked;
                InvoiceSettings.ExternalProviderID = (byte)ddlInvoiceProvider.SelectedValue.ToInt32();
                InvoiceSettings.ExternalUserName = txtInv_ExternalUseName.Text;
                InvoiceSettings.ExternalUserID = txtInv_ExternalUserID.Text;
                InvoiceSettings.ExternalPassword = txtInv_ExternalPassword.Text;
                InvoiceSettings.Save();
            }
		}
	}
}