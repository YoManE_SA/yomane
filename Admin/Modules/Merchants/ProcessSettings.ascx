﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ProcessSettings.ascx.cs" Inherits="Netpay.Admin.Merchants.ProcessSettings" %>

<admin:FormSections runat="server" Title="Charging">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsCcStorageCharge" Text="Allow to charge a stored credit cards" Checked="<%# MerchantSettings.IsCcStorageCharge %>" /></li>
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsApprovalOnly" Text="Allow to charge an approved transaction" Checked="<%# MerchantSettings.IsApprovalOnly %>" /></li>
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsCaptureApprovalOnly" Text="Allow to process a Pre-Authorization" Checked="<%# MerchantSettings.IsCaptureApprovalOnly %>" /></li>
            </ul>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                Process Pre-Authorization transaction automatically after :
                     <div class="input-group">
                         <asp:TextBox runat="server" class="form-control" ID="txtAutoCaptureHours" Text="<%# MerchantSettings.AutoCaptureHours %>" />
                         <asp:RangeValidator ForeColor="red" ControlToValidate="txtAutoCaptureHours" runat="server" ErrorMessage="Please insert a number bigger or equal to 0." MinimumValue="0" MaximumValue="<%#Int32.MaxValue%>" ValidationGroup="Charging" Display="Dynamic" Type="Integer" />
                         <span class="input-group-addon min-width-60">Hours</span>
                     </div>
            </div>
            <div class="form-group clear-form-group">
                Multiple charge protection:
                 <div class="input-group">
                     <asp:TextBox runat="server" ID="txtMultiChargeProtectionMins" class="form-control" Text="<%# MerchantSettings.MultiChargeProtectionMins %>" />
                     <asp:RangeValidator ForeColor="red" ControlToValidate="txtMultiChargeProtectionMins" ID="RangeValidatortxtMultiChargeProtectionMins" runat="server" ErrorMessage="Please insert a number bigger or equal to 0." MinimumValue="0" MaximumValue="<%#short.MaxValue%>" ValidationGroup="Charging" Display="Dynamic" Type="Integer" />
                     <span class="input-group-addon min-width-60">Minutes</span>
                 </div>
            </div>
        </div>

    </div>


</admin:FormSections>
<admin:FormSections runat="server" Title="Recurring">
    <div class="row">
        <div class="col-lg-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsAllowRecurring" Text="Allow Recurring Settings" Checked="<%# RecurringSettings.IsEnabled %>" /></li>
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsAllowRecurringFromTrans" runat="server" Text="Allow to create recurring series from transaction" Checked="<%# RecurringSettings.IsEnabledFromTransPass %>" /></li>
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsAllowRecurringModify" runat="server" Text="Allow modifying recurring series remotely" Checked="<%# RecurringSettings.IsEnabledModify %>" /></li>
            </ul>

        </div>
        <div class="col-lg-6">
            Limit recurring series to:
                <div class="form-group">
                    <div class="input-group">
                        <asp:TextBox runat="server" ID="txtRecurringLimitStages" CssClass="form-control" Text="<%# RecurringSettings.MaxStages %>" />
                        <asp:RangeValidator ForeColor="red" ControlToValidate="txtRecurringLimitStages" ID="RangeValidatortxtRecurringLimitStages" runat="server" ErrorMessage="Please insert a number bigger or equal to 0." MinimumValue="0" MaximumValue="<%#Int32.MaxValue%>" ValidationGroup="Charging" Display="Dynamic" Type="Integer" />
                        <span class="input-group-addon min-width-60">Years</span>
                    </div>
                </div>
            <div class="form-group">
                <div class="input-group">
                    <asp:TextBox runat="server" ID="txtRecurringLimitYears" CssClass="form-control" Text="<%# RecurringSettings.MaxYears %>" />
                    <asp:RangeValidator ForeColor="red" ControlToValidate="txtRecurringLimitYears" ID="RangeValidatortxtRecurringLimitYears" runat="server" ErrorMessage="Please insert a number bigger or equal to 0." MinimumValue="0" MaximumValue="<%#Int32.MaxValue%>" ValidationGroup="Charging" Display="Dynamic" Type="Integer" />
                    <span class="input-group-addon min-width-60">Stages</span>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <asp:TextBox runat="server" ID="txtRecurringLimitCharges" CssClass="form-control" Text="<%# RecurringSettings.MaxCharges %>" />
                    <asp:RangeValidator ForeColor="red" ControlToValidate="txtRecurringLimitCharges" ID="RangeValidatortxtRecurringLimitCharges" runat="server" ErrorMessage="Please insert a number bigger or equal to 0." MinimumValue="0" MaximumValue="<%#Int32.MaxValue%>" ValidationGroup="Charging" Display="Dynamic" Type="Integer" />
                    <span class="input-group-addon min-width-60">Overall charges</span>
                </div>
            </div>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Data Pulling">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsAllowRemotePull" runat="server" Text="Allow 3rd party data pulling for this merchant " Checked="<%# MerchantSettings.IsAllowRemotePull %>" />
                </li>
            </ul>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <%-- the regular expression for the ip address allows this formats of ip addresses with ";" sign between them
                     11.22.33.44 
                     11.22.33.44/12
                     11.22.33 --%>

                <i class="fa fa-laptop"></i>IP Address Allowed List:
                    <asp:TextBox runat="server" ID="txtRemotePullIPs" CssClass="form-control" TextMode="MultiLine" Text="<%# MerchantSettings.RemotePullIPs %>" MaxLength="50" />
                <asp:RegularExpressionValidator ForeColor="red" ControlToValidate="txtRemotePullIPs" ID="RegularExpressionValidatortxtRemotePullIPs"
                    runat="server"
                    ErrorMessage="Please insert IP addresses with ';' sign between them."
                    ValidationExpression="^((\b((25[0-5]|2[0-4]\d|[01]?\d{1,2})\.){3}(25[0-5]|2[0-4]\d|[01]?\d{1,2})\b((\/(3[0-2]|[012]?\d))|;\b((25[0-5]|2[0-4]\d|[01]?\d{1,2})\.){3}(25[0-5]|2[0-4]\d|[01]?\d{1,2})\b)?);\s*)*(\b((25[0-5]|2[0-4]\d|[01]?\d{1,2})\.){3}(25[0-5]|2[0-4]\d|[01]?\d{1,2})\b((\/(3[0-2]|[012]?\d))|;\b((25[0-5]|2[0-4]\d|[01]?\d{1,2})\.){3}(25[0-5]|2[0-4]\d|[01]?\d{1,2})\b)?)?"
                    ValidationGroup="Charging" />

            </div>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Refunding">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsRefund" runat="server" Text="Allow Merchant to process a refund " Checked="<%# MerchantSettings.IsRefund %>" /></li>
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsAskRefund" runat="server" Text="Allow Merchant to request a refund " Checked="<%# MerchantSettings.IsAskRefund %>" />
                </li>
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsAskRefundRemote" runat="server" Text="Allow Merchant to request a refund remotely" Checked="<%# MerchantSettings.IsAskRefundRemote %>" /></li>
            </ul>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <i class="fa fa-laptop"></i>IP Address List:
              
                    <asp:TextBox runat="server" ID="txtRemoteRefundRequestIPs" CssClass="form-control" TextMode="MultiLine" Text="<%# MerchantSettings.RemoteRefundRequestIPs %>" MaxLength="500" />
                <asp:RegularExpressionValidator ForeColor="red" ControlToValidate="txtRemoteRefundRequestIPs" ID="RegularExpressionValidatortxtRemoteRefundRequestIPs" runat="server" ErrorMessage="Please insert IP addresses with ';' sign between them." ValidationExpression="^((\b((25[0-5]|2[0-4]\d|[01]?\d{1,2})\.){3}(25[0-5]|2[0-4]\d|[01]?\d{1,2})\b((\/(3[0-2]|[012]?\d))|;\b((25[0-5]|2[0-4]\d|[01]?\d{1,2})\.){3}(25[0-5]|2[0-4]\d|[01]?\d{1,2})\b)?);\s*)*(\b((25[0-5]|2[0-4]\d|[01]?\d{1,2})\.){3}(25[0-5]|2[0-4]\d|[01]?\d{1,2})\b((\/(3[0-2]|[012]?\d))|;\b((25[0-5]|2[0-4]\d|[01]?\d{1,2})\.){3}(25[0-5]|2[0-4]\d|[01]?\d{1,2})\b)?)?" ValidationGroup="Charging" />

            </div>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Invoice Service">
    <div class="row">
        <div class="col-lg-12">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox ID="chkInv_IsEnable" runat="server" Text="Allow Invoice Service" Checked="<%# InvoiceSettings.IsEnable %>" />
                </li>
            </ul>
        </div>
        <div class="col-lg-3 col-md-3 spacer">
            <div class="form-group">
                Service Provider
                <%-- Comes from Setting.SetMerchantInvoice --%>
                <asp:DropDownList runat="server" ID="ddlInvoiceProvider" CssClass="form-control">
                    <asp:ListItem Text="" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Invoice4u" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Tamal" Value="2"></asp:ListItem>
                    <asp:ListItem Text="iCount" Value="3"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 spacer">
            <div class="form-group">
                Username
                <asp:TextBox ID="txtInv_ExternalUseName" runat="server" CssClass="form-control" Text="<%# InvoiceSettings.ExternalUserName %>" />
            </div>
        </div>

        <div class="col-lg-3 col-md-3 spacer">
            <div class="form-group">
                User ID
                 <asp:TextBox ID="txtInv_ExternalUserID" runat="server" Text="<%# InvoiceSettings.ExternalUserID %>" CssClass="form-control" />
            </div>
        </div>
        <div class="col-lg-3 col-md-3 spacer">
            <div class="form-group">
                Password
                <asp:TextBox ID="txtInv_ExternalPassword" CssClass="form-control" runat="server" Text="<%# InvoiceSettings.ExternalPassword %>" />
            </div>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="VIRTUAL TERMINAL">
    <div class="row">
        <div class="col-lg-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsSystemPayECheck" Text="Allow eCheck (Israel only)" Checked="<%# MerchantSettings.IsSystemPayECheck %>" /></li>
            </ul>
        </div>
        <div class="col-lg-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsSystemPayCreditCard" Text="Allow credit card" Checked="<%# MerchantSettings.IsSystemPayCreditCard %>" />
                </li>
                <li class="list-group-item">
                    <ul class="child-list-group">
                        <li>
                            <asp:CheckBox runat="server" ID="chkIsSystemPayPersonalNumber" Checked="<%# MerchantSettings.IsSystemPayPersonalNumber %>" Text="Require ID/SSN" /></li>
                        <li>
                            <asp:CheckBox runat="server" ID="chkIsSystemPayCVV2" Checked="<%# MerchantSettings.IsSystemPayCVV2 %>" Text="Require CVV2/CID" /></li>
                        <li>
                            <asp:CheckBox runat="server" ID="chkIsSystemPayPhoneNumber" Checked="<%# MerchantSettings.IsSystemPayPhoneNumber %>" Text="Require Phone" /></li>
                        <li>
                            <asp:CheckBox runat="server" ID="chkIsSystemPayEmail" Checked="<%# MerchantSettings.IsSystemPayEmail %>" Text="Require Email" /></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Silent Post: Server to Server eg Process card over the phone ">
    <div class="row">

        <div class="col-lg-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsRemoteCharge" Text="Allow Credit card" Checked="<%# MerchantSettings.IsRemoteCharge %>" /></li>
                <li class="list-group-item">
                    <admin:DisabledGroup runat="server" CheckboxID="chkIsRemoteCharge">
                        <ul class="child-list-group">
                            <li>
                                <asp:CheckBox runat="server" ID="chkIsRemoteChargePersonalNumber" Checked="<%# MerchantSettings.IsRemoteChargePersonalNumber %>" Text="Require ID/SSN" /></li>
                            <li>
                                <asp:CheckBox runat="server" ID="chkIsRemoteChargeCVV2" Checked="<%# MerchantSettings.IsRemoteChargeCVV2 %>" Text="Require CVV2/CID" /></li>
                            <li>
                                <asp:CheckBox runat="server" ID="chkIsRemoteChargePhoneNumber" Checked="<%# MerchantSettings.IsRemoteChargePhoneNumber %>" Text="Require Phone" /></li>
                            <li>
                                <asp:CheckBox runat="server" ID="chkIsRemoteChargeEmail" Checked="<%# MerchantSettings.IsRemoteChargeEmail %>" Text="Require Email" /></li>
                        </ul>
                    </admin:DisabledGroup>
                </li>
            </ul>
        </div>
        <div class="col-lg-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkIsRemoteChargeEcheck" Text="Allow eCheck/direct debit" Checked="<%# MerchantSettings.IsRemoteChargeEcheck %>" /></li>
                <li class="list-group-item">
                    <ul class="child-list-group">
                        <li>
                            <asp:CheckBox runat="server" ID="chkIsRemoteChargeEcheckPersonalNumber" Checked="<%# MerchantSettings.IsRemoteChargeEcheckPersonalNumber %>" Text="Require ID/SSN" /></li>
                        <li>
                            <asp:CheckBox runat="server" Enabled="false" Text="Require CVV2/CID" /></li>
                        <li>
                            <asp:CheckBox runat="server" ID="chkIsRemoteChargeEcheckPhoneNumber" Checked="<%# MerchantSettings.IsRemoteChargeEcheckPhoneNumber %>" Text="Require Phone" /></li>
                        <li>
                            <asp:CheckBox runat="server" ID="chkIsRemoteChargeEcheckEmail" Checked="<%# MerchantSettings.IsRemoteChargeEcheckEmail %>" Text="Require Email" /></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="row">


        <div class="col-lg-6 spacer">

            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsAllowSilentPostCcDetails" runat="server" Text="Allow to send full credit card details" Checked='<%# MerchantSettings.IsAllowSilentPostCcDetails %>' /></li>
            </ul>

        </div>

        <div class="col-lg-6 spacer">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsRequiredClientIP" runat="server" Text="Required ClientIP parameter" Checked='<%# MerchantSettings.IsRequiredClientIP %>' /></li>
            </ul>
        </div>
    </div>

</admin:FormSections>

<admin:FormSections runat="server" Title="General">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsAutoPersonalSignup" runat="server" Text="Show transaction details to customer on confirmation page" Checked="<%# MerchantSettings.IsAutoPersonalSignup %>" /></li>
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsCcStorage" runat="server" Text="Allow Merchant to store credit cards" Checked="<%# MerchantSettings.IsCcStorage %>" /></li>
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsTerminalProbLog" runat="server" Text="Show &quot;Terminal Problems&quot; report" Checked="<%# MerchantSettings.IsTerminalProbLog %>" /></li>
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsConnectionProbLog" runat="server" Text="Show &quot;Communication Problems&quot; report" Checked="<%# MerchantSettings.IsConnectionProbLog %>" /></li>

            </ul>
        </div>
        <div class="col-lg-6 col-md-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsTransLookup" runat="server" Text="Show transaction details when clarifying" Checked="<%# MerchantSettings.IsTransLookup %>" /></li>
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsAllowMakePayments" runat="server" Text="Allow payment orders to employees/vendors" Checked="<%# MerchantSettings.IsAllowMakePayments %>" /></li>
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsAllowBatchFiles" runat="server" Text="Allow Bulk Upload of transactions" Checked="<%# MerchantSettings.IsAllowBatchFiles %>" /></li>
                <li class="list-group-item">Terminal owned by
                <asp:DropDownList CssClass="form-control" runat="server" ID="ddlTerminalOwnedBy">
                </asp:DropDownList>
                </li>
            </ul>
        </div>
    </div>

</admin:FormSections>
<admin:FormSections runat="server" Title="Notifications">
    <div class="row">
        <div class="col-lg-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsPassNotificationSentToMerchant" runat="server" Text="Notify merchant about successful transaction" Checked="<%# MerchantSettings.IsPassNotificationSentToMerchant %>" /></li>
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsFailNotificationSentToMerchant" runat="server" Text="Notify merchant about decliend transaction" Checked="<%# MerchantSettings.IsFailNotificationSentToMerchant %>" /></li>
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsPassNotificationSentToPayer" runat="server" Text="Notify customer about successful transaction" Checked="<%# MerchantSettings.IsPassNotificationSentToPayer %>" /></li>
            </ul>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                Email address(es) for transaction notifications:
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        <asp:TextBox runat="server" ID="txtMerchantNotifyEmails" CssClass="form-control" Text="<%# MerchantSettings.MerchantNotifyEmails %>" />
                    </div>
            </div>
            <div class="form-group">
                Email address(es) for Chargebacks:

                 <div class="input-group">
                     <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                     <asp:TextBox ID="TextBox1" runat="server" Text="<%# MerchantSettings.MerchantNotifyEmails %>" CssClass="form-control" />
                 </div>

            </div>
            <div class="form-group">
                Email address(es) for  System alert:
                 <div class="input-group">
                     <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                     <asp:TextBox ID="TextBox2" runat="server" Text="<%# MerchantSettings.MerchantNotifyEmails %>" CssClass="form-control" />
                 </div>
            </div>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Affiliates accepted by this merchant ">
    <div class="row">
        <div class="col-md-6 col-lg-6 col-xs-6">
            <admin:AdminList runat="server" ID="affiliatesList" AutoGenerateColumns="false" DisableRowSelect="true">
                <Columns>
                    <asp:TemplateField HeaderText="ID">
                        <ItemTemplate>
                            <asp:Literal runat="server" Text='<%# Eval("PartnerId") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Name">
                        <ItemTemplate>
                            <asp:Literal runat="server" Text='<%# Eval("AffiliateName") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </admin:AdminList>
        </div>
    </div>
</admin:FormSections>
