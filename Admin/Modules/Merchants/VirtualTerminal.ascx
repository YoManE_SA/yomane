﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="VirtualTerminal.ascx.cs" Inherits="Netpay.Admin.Merchants.VirtualTerminal" %>
<admin:FormSections runat="server" Title="Use Storage Card">
    <div class="row">
        <div class="col-md-4">
            Use Storage Card
            <asp:TextBox runat="server" Class="form-control" />
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Credit card details">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                Credit card number 
                <asp:TextBox runat="server" CssClass="form-control" />
            </div>
            <div class="form-group">
                Card Expiraדtion
                <asp:TextBox runat="server" CssClass="form-control" />
            </div>
            <div class="form-group">
                Card Verification Code*
                <asp:TextBox runat="server" CssClass="form-control" />
            </div>

        </div>
        <div class="col-md-4">
            <div class="form-group">
                Cardholder Name
                <asp:TextBox runat="server" CssClass="form-control" />
            </div>
            <div class="form-group">
                Cardholder Phone 
                <asp:TextBox runat="server" CssClass="form-control" />
            </div>
            <div class="form-group">
                Cardholder Email
                <asp:TextBox runat="server" CssClass="form-control" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                IP Address (needed for risk management)
                <asp:TextBox runat="server" CssClass="form-control" />
            </div>
            <div class="form-group">
                Cardholder ID 
                <asp:TextBox runat="server" CssClass="form-control" />
            </div>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Transaction details">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                Transaction type:
                <asp:RadioButtonList CssClass="TransactionList" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                    <asp:ListItem>Regular</asp:ListItem>
                    <asp:ListItem>Authorization</asp:ListItem>
                    <asp:ListItem>Capture</asp:ListItem>
                    <asp:ListItem>Confirmed by phone</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                Amount 
                <asp:TextBox runat="server" CssClass="form-control" />
            </div>
            <div class="form-group">
                Credit Type
                  <asp:DropDownList runat="server" CssClass="form-control" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                Currency conversion fee may be added. 
                  <asp:DropDownList runat="server" CssClass="form-control" />
            </div>
            <div class="form-group">
                Installments
                  <asp:DropDownList runat="server" CssClass="form-control" />
            </div>

        </div>
        <div class="col-md-4">
            <div class="form-group">
                Confirmation No.
                     <asp:TextBox runat="server" CssClass="form-control" />
            </div>
        </div>
    </div>


</admin:FormSections>
<admin:FormSections runat="server" Title="Recurring Transaction">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <asp:CheckBox runat="server" />
                Create recurring series based on this transaction 
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            Charge every
           <asp:TextBox runat="server" CssClass="form-control" />
        </div>
        <div class="col-md-3">
            Month
                 <asp:TextBox runat="server" CssClass="form-control" />
        </div>
        <div class="col-md-3">
            Overall number of charges
                 <asp:TextBox runat="server" CssClass="form-control" />
        </div>
    </div>

</admin:FormSections>
<admin:FormSections runat="server" Title="Comment">
    <div class="row">
        <div class="col-md-12">
            Comment
                 <asp:TextBox runat="server" TextMode="MultiLine" CssClass="form-control" />
        </div>
    </div>
</admin:FormSections>
