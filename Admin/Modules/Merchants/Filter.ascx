﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Merchants.Filter" %>
<admin:FilterSection runat="server" Title="DETAILS">
    <div class="form-group">
        <asp:Label ID="lblAccount" runat="server" Text="Name" />
        <admin:AccountPicker runat="server" ID="apAccount" LimitToType="Merchant" UseTargetID="false" />
    </div>
    <hr />
    <h4>
        <asp:Label ID="lblStatus" runat="server" Text="Status" /></h4>
    <asp:CheckBoxList runat="server" CssClass="checkbox-list" ID="chklStatus" RepeatLayout="UnorderedList" OnDataBound="chklStatus_DataBound" DataSource='<%# System.Enum.GetValues(typeof(Netpay.Infrastructure.MerchantStatus)) %>' />
    <hr />
    <div class="form-group">
        <asp:Label ID="Label3" runat="server" Text="Industry" />
        <netpay:IndustryDropDown runat="server" ID="ddlIndustry" CssClass="form-control" EnableBlankSelection="true" />
    </div>
    <div class="form-group">
        <asp:Label ID="Label4" runat="server" Text="Department" />
        <netpay:MerchantDepartmentDropDown runat="server" ID="ddlDepartment" CssClass="form-control" EnableBlankSelection="true" />
    </div>
</admin:FilterSection>
<admin:FilterSection runat="server" Title="Record Information">
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblID" runat="server" Text="ID Range" />
                <JQ:IntRange runat="server" ID="rngID" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label1" runat="server" Text="Open Date" />
                <JQ:DateRange runat="server" ID="rndOpenDate" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label2" runat="server" Text="Close Date" />
                <JQ:DateRange runat="server" ID="rndCloseDate" />
            </div>
        </div>
    </div>
</admin:FilterSection>
