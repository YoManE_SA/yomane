﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Merchants
{
    public partial class List : Controls.TemplateControlBase
    {
        public DateTime LastTransactionMadeByMerchant { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            rptList.DataBinding += List_DataBinding;
            base.OnLoad(e);
        }

        private void List_DataBinding(object sender, EventArgs e)
        {
            rptList.DataSource = Bll.Merchants.Merchant.Search(TemplatePage.GetFilter<Bll.Merchants.Merchant.SearchFilters>(), rptList);
            TemplatePage.PageController.ListView.Update();
        }

        public string GetTransactionHistory(Bll.Merchants.Merchant merchant)
        {
            if (merchant == null) return "";

            //Get the current merchant by ID
            //var sf = new Bll.Merchants.Merchant.SearchFilters();
            //sf.ID = new Infrastructure.Range<int?>(merchantid);
            //var merchant = Bll.Merchants.Merchant.Search(sf, null).FirstOrDefault();

            //Get the latest transaction made:
            List<DateTime?> MerchantLastTransactions = new List<DateTime?>();
            MerchantLastTransactions.Add(merchant.DateLastTransFail);
            MerchantLastTransactions.Add(merchant.DateLastTransPass);
            MerchantLastTransactions.Add(merchant.DateLastTransPending);
            MerchantLastTransactions.Add(merchant.DateLastTransPreAuth);

            if (MerchantLastTransactions.Any(x => x.HasValue))
            {
                LastTransactionMadeByMerchant = MerchantLastTransactions.Where(x => x.HasValue).Max(x => x.Value);
            }
            else
            {
                LastTransactionMadeByMerchant = DateTime.Now;
            }

            string url = Web.WebUtils.ApplicationPath + "/Transactions/0?";
            string query = "ctl00.apMerchant.hdID=" + merchant.AccountID + "&ctl00.chklStatus$0=Captured&ctl00.chklStatus$1=Declined&ctl00.chklStatus$2=Authorized&ctl00.chklStatus$3=Pending&ctl00.chklStatus$4=DeclinedArchived&ctl00.BankDropDownList=0&Search=1&ctl00.rng_InsertDate.From=" + Server.UrlEncode(LastTransactionMadeByMerchant.AddMonths(-3).ToShortDateString()) + "&ctl00.rng_InsertDate.To=" + Server.UrlEncode(LastTransactionMadeByMerchant.ToShortDateString());
            url += query;
            return url;
        }

        protected void rptList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                bool isCompanyTerminal = bool.Parse((e.Row.FindControl("hfOwnedBy") as HiddenField).Value);
                if (!isCompanyTerminal)
                {
                    Label legend = (e.Row.FindControl("LegendLabel") as Label);
                    legend.BorderColor = legend.BackColor;
                    legend.BorderStyle = BorderStyle.Solid;
                }
            }
        }
    }
}

