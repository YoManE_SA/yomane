﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Netpay.Admin.ModuleManager.List" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Modules/Security/SetPermissions.ascx" TagPrefix="CUC" TagName="SetPermissions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <div class="row">
        <div class="form-group">
            <div class="row">
                <div class="col-lg-3">
                    <asp:Label runat="server" Text="Search by text" />
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtSearch">
                    </asp:TextBox>
                </div>
                <div class="col-lg-6 col-md-6">
                    <asp:Button Text="Search" runat="server" CssClass="btn btn-info" OnCommand="Search_Command" CommandName="Search"/>
                    <asp:Button Text="Clear" runat="server" CssClass="btn btn-info" OnCommand="Search_Command" CommandName="Clear"/>
                </div>
            </div>
        </div>
    </div>
    <hr />
    <div class="row">
        <asp:UpdatePanel runat="server" ID="upModules" ChildrenAsTriggers="false" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Repeater runat="server" ID="rptList" OnItemCommand="rptList_ItemCommand">
                    <ItemTemplate>
                        <div class="col-lg-4">
                            <asp:HiddenField runat="server" ID="hfModuleName" Value='<%# Eval("Name") %>' />
                            <admin:PanelSection CssClass="Module" runat="server" Title='<%# Eval("Name") %>' Icon="fa-code">
                                <Body>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="module-manager-box">
                                                <div class="form-group">
                                                    <asp:Literal runat="server" Text="Version" />: <%# Eval("Version") %>
                                                </div>
                                                <div class="form-group">
                                                    <asp:Literal runat="server" Text="Author" />: <%# Eval("Author") %>
                                                </div>
                                                <div class="form-group">
                                                    <asp:Literal runat="server" Text="Description" />: <%# Eval("Description") %>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Body>
                                <Footer>
                                    <asp:Button UseSubmitBehavior="false" ID="ActivateButton" runat="server" Text="Activate" CommandName="Activate" CssClass="btn btn-primary" Enabled='<%# !(bool)Eval("IsActive") && ((Container.NamingContainer as RepeaterItem).DataItem as Netpay.Admin.CoreBasedModule).CoreModule.IsInstalled && IsEditable %>' />
                                    <asp:Button UseSubmitBehavior="false" ID="DeactivateButton" runat="server" Text="Deactivate" CommandName="Deactivate" CssClass="btn btn-primary" Enabled='<%# (bool)Eval("IsActive") && IsEditable %>' />
                                    <asp:Button UseSubmitBehavior="false" ID="InstallButton" runat="server" Text="Install" CommandName="Install" CssClass="btn btn-primary" Enabled='<%# !((Container.NamingContainer as RepeaterItem).DataItem as Netpay.Admin.CoreBasedModule).CoreModule.IsInstalled && IsEditable %>' />
                                    <asp:Button UseSubmitBehavior="false" ID="UnInstallButton" runat="server" Text="Uninstall" CommandName="UnInstall" CssClass="btn btn-primary" Enabled='<%# ((Container.NamingContainer as RepeaterItem).DataItem as Netpay.Admin.CoreBasedModule).CoreModule.IsInstalled && IsEditable %>' />
                                    <NP:Button Visible="false" runat="server" CssClass="btn btn-primary" CommandName="Permissions" Text="Permissions" RegisterAsync="true" Enabled='<%# (bool)Eval("IsActive") && ((Container.NamingContainer as RepeaterItem).DataItem as Netpay.Admin.CoreBasedModule) != null %>' />
                                </Footer>
                            </admin:PanelSection>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <admin:ModalDialog runat="server" ID="dlgPermissions" Title="Manage Permmisions to module">
        <Body>
            <CUC:SetPermissions runat="server" ID="setPermissions" />
        </Body>
        <Footer>
            <asp:Button ID="btnOk" runat="server" Text="OK" CommandName="OK" CssClass="btn btn-primary" OnClick="PermissionsSave_Click" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CommandName="Close" CssClass="btn btn-primary" />
        </Footer>
    </admin:ModalDialog>
</asp:Content>
