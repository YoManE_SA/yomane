﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.ModuleManager
{
    public class Module : Admin.Module
    {
        private ApplicationMenu ModuleManagerMenuItem;

        public override string Name { get { return "ModuleManager"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return ""; } }
        

        protected override void OnInit(EventArgs e)
        {
            ModuleManagerMenuItem = new ApplicationMenu("Module Manager", "~/Modules/Merchants/ModuleManager/List.aspx", null, 205);
            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {
            if (IsActive) return;
            
            Application.AddMenuItem("Settings", ModuleManagerMenuItem);
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {
            ModuleManagerMenuItem.Parent.RemoveChild(ModuleManagerMenuItem);
            base.OnDeactivate(e);
        }
    }
}