﻿using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.ModuleManager
{
    public partial class List : Controls.Page
    {
        public static string[] ButtonIds = { "ActivateButton", "DeactivateButton", "InstallButton", "UnInstallButton" };

        public static bool IsEditable
        {
            get
            {
                return Infrastructure.Security.SecurityModule.ModuleManagerSecuredObject.HasPermission(PermissionValue.Edit);
            }
        }

        public static List<Admin.Module> GetAdminModules
        {
            get
            {
                return Admin.Application.Current.Modules.Where(x => x is CoreBasedModule && (x as CoreBasedModule).CoreModule.Name != "Security").ToList();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            //Check if there is any filter
            if (Page.IsPostBack && Request.Params["__EVENTTARGET"] != null && !ButtonIds.Any((Request.Params["__EVENTTARGET"]).Contains))
            {
                //Check if there is any filter
                if (txtSearch == null || string.IsNullOrEmpty(txtSearch.Text))
                {
                    rptList.DataSource = GetAdminModules;
                }
                else
                {
                    rptList.DataSource = FilterByText(txtSearch.Text);
                }

                rptList.DataBind();
                upModules.Update();

            }

            base.OnLoad(e);
        }

        protected override void DataBind(bool raiseOnDataBinding)
        {
            base.DataBind(raiseOnDataBinding);
        }

        protected override void DataBindChildren()
        {
            //Load only CoreBasedModules that has class on bll ,
            //Only those modules status can be saved to DB.
            //So even after restart the appliaction the status is saved.

            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                Infrastructure.ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecurityModule.ModuleManagerSecuredObject, PermissionValue.Read);

            //Check if there is any filter
            if (txtSearch == null || string.IsNullOrEmpty(txtSearch.Text))
            {
                rptList.DataSource = GetAdminModules;
            }
            else
            {
                rptList.DataSource = FilterByText(txtSearch.Text);
            }

            base.DataBindChildren();
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                Infrastructure.ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecurityModule.ModuleManagerSecuredObject, PermissionValue.Edit);

            Admin.Module mod = Admin.Application.Current.Modules.Where(m => m.Name == (e.Item.FindControl("hfModuleName") as HiddenField).Value).SingleOrDefault();
            if (mod == null) return;
            switch (e.CommandName)
            {
                case "Activate":
                    if (!mod.IsActive) mod.Activate(EventArgs.Empty);
                    break;

                case "Deactivate":
                    if (mod.IsActive) mod.Deactivate(EventArgs.Empty);
                    break;

                case "Install":
                    mod.InstallAdmin(EventArgs.Empty); // Triggers also install of BLL Module
                    break;

                case "UnInstall":
                    if (mod.IsActive) mod.Deactivate(EventArgs.Empty);
                    mod.UninstallAdmin(EventArgs.Empty); // Triggers also uninstall of BLL Module
                    break;

                case "Permissions":
                    setPermissions.ModuleName = (mod as Admin.CoreBasedModule).CoreModule.Name;
                    dlgPermissions.DataBind();
                    dlgPermissions.RegisterShow();
                    break;
            }
            e.Item.DataItem = mod;
            e.Item.DataBind();
            DataBindChildren();
            upModules.Update();
        }

        protected void PermissionsSave_Click(object sender, EventArgs e)
        {
            setPermissions.Save(this, e);
            dlgPermissions.RegisterHide();
        }

        protected void Search_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
                {
                case "Search":

                    if (string.IsNullOrEmpty(txtSearch.Text))
                    {
                        DataBindChildren();
                        upModules.Update();
                    }

                    string textToSearch = txtSearch.Text;
                    rptList.DataSource = FilterByText(textToSearch);
                    break;

                case "Clear":
                    txtSearch.Text = "";
                    rptList.DataSource = FilterByText("");
                    break;
                }
                        
            upModules.DataBind();
            upModules.Update();
        }

        public static List<Admin.Module> FilterByText(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return GetAdminModules;
            }
            else
            {
                return GetAdminModules
                .Where
                (x => (x.Description != null && x.Description.ToLower().Contains(text.ToLower())) ||
                      (x.Author != null && x.Author.ToLower().Contains(text.ToLower())) ||
                      (x.Name != null && x.Name.ToLower().Contains(text.ToLower())))
                      .ToList();
            }
        }
    }
}