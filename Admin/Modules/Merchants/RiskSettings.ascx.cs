﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Merchants
{
    public partial class RiskSettings : Controls.AccountControlBase
    {
        protected Netpay.Bll.Merchants.RiskSettings MerchantSettings { get; private set; }

        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += PageController_SaveItem;
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            if (Account != null) MerchantSettings = Netpay.Bll.Merchants.RiskSettings.Load(Account.MerchantID.GetValueOrDefault());
            if (MerchantSettings == null) MerchantSettings = new Netpay.Bll.Merchants.RiskSettings(0);
            base.DataBindChildren();
        }

        public void PageController_SaveItem(object sender, EventArgs e)
        {
            //Billing Address section: All these properties located at tblCompany table.
            //Get the current merchant in order to update the fields that are located
            //at the tblCompany table.                        
            Netpay.Bll.Merchants.Merchant currentMerchant = Account as Netpay.Bll.Merchants.Merchant;
            if (currentMerchant != null)
            {
                currentMerchant.IsBillingAddressMust = chkIsForceBillingAddress.Checked;
                currentMerchant.IsBillingCityOptional = chkIsForceBillingAddressCity.Checked;
                currentMerchant.IsBillingAddressMustIDebit = chkIsForceInstantDebitBillingAddress.Checked;
                currentMerchant.IsAllow3DTrans = chkIsForce3dSecure.Checked;
                currentMerchant.Save(false);
            }

            if (Bll.Merchants.RiskSettings.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Edit))
            {
                MerchantSettings = Netpay.Bll.Merchants.RiskSettings.Load(Account.MerchantID.GetValueOrDefault());
                if (MerchantSettings == null) MerchantSettings = new Netpay.Bll.Merchants.RiskSettings(Account.MerchantID.GetValueOrDefault());

                MerchantSettings.IsCreditCardWhitelistEnabled = chkIsCreditCardWhitelistEnabled.Checked;
                MerchantSettings.CountryBlacklist = scCountryBlacklist.Value;
                MerchantSettings.CountryWhitelist = scCountryWhitelist.Value;
                MerchantSettings.StateBlacklist = ssStateBlacklist.Value;
                MerchantSettings.StateWhitelist = ssStateWhitelist.Value;

                MerchantSettings.LimitCcForEmailAllowedCount = (byte)ddlLimitCcForEmailAllowedCount.Text.ToInt32();
                MerchantSettings.IsLimitCcForEmailDeclineTrans = chkIsLimitCcForEmailDeclineTrans.Checked;
                MerchantSettings.IsLimitCcForEmailBlockEmail = chkIsLimitCcForEmailBlockEmail.Checked;
                MerchantSettings.IsLimitCcForEmailBlockNewCc = chkIsLimitCcForEmailBlockNewCc.Checked;
                MerchantSettings.IsLimitCcForEmailBlockAllCc = chkIsLimitCcForEmailBlockAllCc.Checked;
                MerchantSettings.IsLimitCcForEmailNotifyByEmail = chkIsLimitCcForEmailNotifyByEmail.Checked;
                MerchantSettings.LimitCcForEmailNotifyByEmailList = txtLimitCcForEmailNotifyByEmailList.Text;

                MerchantSettings.LimitEmailForCcAllowedCount = (byte)ddlLimitEmailForCcAllowedCount.Text.ToInt32();
                MerchantSettings.IsLimitEmailForCcDeclineTrans = chkIsLimitEmailForCcDeclineTrans.Checked;
                MerchantSettings.IsLimitEmailForCcBlockCc = chkIsLimitEmailForCcBlockCc.Checked;
                MerchantSettings.IsLimitEmailForCcBlockNewEmail = chkIsLimitEmailForCcBlockNewEmail.Checked;
                MerchantSettings.IsLimitEmailForCcBlockAllEmails = chkIsLimitEmailForCcBlockAllEmails.Checked;
                MerchantSettings.IsLimitEmailForCcNotifyByEmail = chkIsLimitEmailForCcNotifyByEmail.Checked;
                MerchantSettings.LimitEmailForCcNotifyByEmailList = txtLimitEmailForCcNotifyByEmailList.Text;

                //convert the txtAllowedAmounts text to list of decimals.
                if (txtAllowedAmounts.Text != "" && txtAllowedAmounts.Text != null)
                {
                    var decimalList = new List<decimal>();
                    string[] decimalSeperatedList = txtAllowedAmounts.Text.Split(',');
                    foreach (var decimalValue in decimalSeperatedList)
                    {
                        decimal num;
                        if (decimal.TryParse(decimalValue, out num))
                        {
                            decimalList.Add(num);
                        }
                    }
                    MerchantSettings.AllowedAmounts = decimalList;
                }

                MerchantSettings.Save();
            }

            DataBindChildren();
        }
    }
}