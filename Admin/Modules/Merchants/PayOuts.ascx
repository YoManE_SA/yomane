﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PayOuts.ascx.cs" Inherits="Netpay.Admin.Merchants.PayOuts" %>

<admin:FormPanel ID="SetSettingsPanel" runat="server" Flexible="false" Title="SETTLEMENT SETTINGS">
    <div class="row"> 
    <div class="col-md-4">
        <div class="form-group">
            <asp:Literal runat="server" Text="Add Currency Setting" />
            <netpay:CurrencyDropDown EnableBlankSelection="false" CssClass="form-control" runat="server" ID="ddlCurrency" /> 
        </div>
    </div>
        <div class="col-md-4">
            <br />
            <asp:Button runat="server" Text="Add" CssClass="btn btn-primary" OnCommand="AddCurrencySettings" />
        </div>
    </div>
    <div class="row">
       <div class="12">
           <hr />
       </div> 
    </div>
    <NP:PagedRepeater runat="server" ID="rptRolling" DataSource='<%# MerchantSettings.OrderByDescending(x=>x.Value.ID) %>'>
        <HeaderTemplate>
            <div class="table-responsive">    
            <table class="table table-hover table-customize">
                <thead>
                    <th class="text-nowrap">Currency</th>
                    <th class="text-nowrap">Display in settlements  menu</th>
                    <th class="text-nowrap">Minimum Settled Payouts</th>
                    <th class="text-nowrap">Auto Create Invoice</t>
                    <th class="text-nowrap">Exclude Debit</th>
                    <th class="text-nowrap">Exclude Refund</th>
                    <th class="text-nowrap">Exclude Fees</th>
                    <th class="text-nowrap">Exclude Chb</th>
                    <th class="text-nowrap">Exclude Cashback</th>
                </thead>
        </HeaderTemplate>
        <ItemTemplate>
            <tbody>
                <asp:HiddenField runat="server" ID="hdCurrency" Value='<%# Eval("Key") %>' />
                <td><%# Netpay.Bll.Currency.GetIsoCode((Netpay.CommonTypes.Currency)((int)Eval("Key"))) %> </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkShowInSettlePayouts" Text=" " Checked='<%# Eval("Value.IsShowToSettle") %>' />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtMinSettlementAmount" CssClass="form-control" Text='<%# ((decimal)Eval("Value.MinSettlementAmount")).ToAmountFormat() %>' /></td>
                <td>
                    <asp:CheckBox runat="server" ID="chkIsAutoInvoice" Text=" " Checked='<%# Eval("Value.IsAutoInvoice") %>' /></td>
                <td>
                    <asp:CheckBox runat="server" ID="chkIsWireExcludeDebit" Text=" " Checked='<%# Eval("Value.IsWireExcludeDebit") %>' /></td>
                <td>
                    <asp:CheckBox runat="server" ID="chkIsWireExcludeRefund" Text=" " Checked='<%# Eval("Value.IsWireExcludeRefund") %>' /></td>
                <td>
                    <asp:CheckBox runat="server" ID="chkIsWireExcludeFee" Text=" " Checked='<%# Eval("Value.IsWireExcludeFee") %>' /></td>
                <td>
                    <asp:CheckBox runat="server" ID="chkIsWireExcludeChb" Text=" " Checked='<%# Eval("Value.IsWireExcludeChb") %>' /></td>
                <td>
                    <asp:CheckBox runat="server" ID="chkIsWireExcludeCashback" Text=" " Checked='<%# Eval("Value.IsWireExcludeCashback") %>' /></td>

            </tbody>
        </ItemTemplate>
        <FooterTemplate>
            </table>
             </div>
        </FooterTemplate>
        <EmptyContainer>
            <div class="alert alert-info"><strong>Info!</strong> No record information</div>
        </EmptyContainer>
    </NP:PagedRepeater>
</admin:FormPanel>

<admin:FormSections runat="server" Title="PAYOUT SETTINGS">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <asp:Literal runat="server" Text="How do we want to settle this merchant" />
                <netpay:DropDownBase runat="server" ID="ddlProvider" CssClass="form-control" DataSource='<%# Netpay.Bll.Wires.Provider.Cache %>' DataTextField="FriendlyName" DataValueField="Name" Value='<%# AccountPage.Account.PreferredWireProvider %>' />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <asp:Literal runat="server" Text="Charge VAT" />
                <netpay:BoolDropDown Value='<%# ItemData.IsChargeVAT %>' CssClass="form-control" runat="server" ID="BoolDropDownChargeVat" TrueText="Yes" FalseText="No" EnableBlankSelection="false" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <asp:Literal runat="server" Text="Invoice Provider" />
                <netpay:BillingCompanyDropDown CssClass="form-control" runat="server" ID="ddlInvoiceProvider" Value='<%# ItemData.BillingCompanysId %>' EnableBlankSelection="true" BlankSelectionValue="0" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <asp:Literal runat="server" Text="Settlement Percentage" />
                <asp:TextBox runat="server" ID="txtPayPercent" CssClass="form-control" Value='<%# ((decimal)ItemData.PayPercent).ToAmountFormat() %>' />
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <asp:Literal runat="server" Text="Default Currency" />
                <netpay:CurrencyDropDown runat="server" CssClass="form-control" ID="ddlDefaultCurrency" Value='<%#ItemData.PaymentRecieveCurrency.HasValue? ItemData.PaymentRecieveCurrency.Value : 1 %>' EnableBlankSelection="false" />
            </div>
        </div>
        <div class="col-md-12">
            <div class="alert alert-warning">
                All settlements will be paid out in the default currency.
            </div>
        </div>
    </div>
</admin:FormSections>

<admin:FormSections runat="server" Title="PAYOUT SCHEDULE">
    <asp:UpdatePanel runat="server" ID="upPayoutSchedule" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row">
                <div class="col-md-4 col-lg-4">
                    <h4>Choose payout schedule:</h4>
                    <asp:RadioButtonList runat="server" CssClass="radiobutton-list" RepeatLayout="UnorderedList" ID="rblPayoutSchedule" OnSelectedIndexChanged="rblPayoutSchedule_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Text="Holding Margin" Value="HoldingMargin" />
                        <asp:ListItem Text="Transactions Between" Value="TransactionsBetween" />
                    </asp:RadioButtonList>
                </div>
            </div>
            <hr />
            <asp:MultiView runat="server" ActiveViewIndex='<%# IsPayingDatesHaveValue? 1 : 0 %>' ID="mvPayoutSchedule">
                <asp:View runat="server">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <asp:Literal runat="server" Text="Holding Margin therafter (number of days)" />
                                <JQ:Spinner runat="server" ID="InitialHoldingMarginSpinner" Value='<%# ItemData.PayingDaysMarginInitial %>' />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <asp:Literal runat="server" Text="Holding margin" />
                                <JQ:Spinner runat="server" MinimumValue="1" ID="HoldingMarginSpinner" Value='<%# ItemData.PayingDaysMargin %>' />
                            </div>
                        </div>
                    </div>
                </asp:View>
                <asp:View runat="server">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                Transactions between date
                                <asp:DropDownList CssClass="form-control" runat="server" ID="PayingDatesBetween1">
                                    <asp:ListItem Text="" Value="" />
                                    <asp:ListItem Text="01" Value="01" />
                                    <asp:ListItem Text="11" Value="11" />
                                    <asp:ListItem Text="16" Value="16" />
                                    <asp:ListItem Text="21" Value="21" />
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                and date
                                <asp:DropDownList CssClass="form-control" runat="server" ID="PayingDatesAnd1">
                                    <asp:ListItem Text="" Value="" />
                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                    <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                    <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                    <asp:ListItem Text="31" Value="31"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                credit on
                                <asp:DropDownList CssClass="form-control" runat="server" ID="CreditOn1">
                                    <asp:ListItem Text="" Value="" />
                                    <asp:ListItem Text="05" Value="05" />
                                    <asp:ListItem Text="10" Value="10" />
                                    <asp:ListItem Text="11" Value="11" />
                                    <asp:ListItem Text="15" Value="15" />
                                    <asp:ListItem Text="20" Value="20" />
                                    <asp:ListItem Text="25" Value="25" />
                                    <asp:ListItem Text="01" Value="01" />
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                Transactions between 
                                <asp:DropDownList CssClass="form-control" runat="server" ID="PayingDatesBetween2">
                                    <asp:ListItem Text="" Value="" />
                                    <asp:ListItem Text="01" Value="01" />
                                    <asp:ListItem Text="11" Value="11" />
                                    <asp:ListItem Text="16" Value="16" />
                                    <asp:ListItem Text="21" Value="21" />
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                and
                                <asp:DropDownList runat="server" ID="PayingDatesAnd2" CssClass="form-control">
                                    <asp:ListItem Text="" Value="" />
                                    <asp:ListItem Text="10" Value="10" />
                                    <asp:ListItem Text="15" Value="15" />
                                    <asp:ListItem Text="20" Value="20" />
                                    <asp:ListItem Text="31" Value="31" />
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                credit on
                                <asp:DropDownList runat="server" ID="CreditOn2" CssClass="form-control">
                                    <asp:ListItem Text="" Value="" />
                                    <asp:ListItem Text="05" Value="05" />
                                    <asp:ListItem Text="10" Value="10" />
                                    <asp:ListItem Text="11" Value="11" />
                                    <asp:ListItem Text="15" Value="15" />
                                    <asp:ListItem Text="20" Value="20" />
                                    <asp:ListItem Text="25" Value="25" />
                                    <asp:ListItem Text="01" Value="01" />
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                Transactions between 
                                <asp:DropDownList runat="server" ID="PayingDatesBetween3" CssClass="form-control">
                                    <asp:ListItem Text="" Value="" />
                                    <asp:ListItem Text="01" Value="01" />
                                    <asp:ListItem Text="11" Value="11" />
                                    <asp:ListItem Text="16" Value="16" />
                                    <asp:ListItem Text="21" Value="21" />
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                and
                                <asp:DropDownList runat="server" ID="PayingDatesAnd3" CssClass="form-control">
                                    <asp:ListItem Text="" Value="" />
                                    <asp:ListItem Text="10" Value="10" />
                                    <asp:ListItem Text="15" Value="15" />
                                    <asp:ListItem Text="20" Value="20" />
                                    <asp:ListItem Text="31" Value="31" />
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                credit on
                                <asp:DropDownList runat="server" ID="CreditOn3" CssClass="form-control">
                                    <asp:ListItem Text="" Value="" />
                                    <asp:ListItem Text="05" Value="05" />
                                    <asp:ListItem Text="10" Value="10" />
                                    <asp:ListItem Text="11" Value="11" />
                                    <asp:ListItem Text="15" Value="15" />
                                    <asp:ListItem Text="20" Value="20" />
                                    <asp:ListItem Text="25" Value="25" />
                                    <asp:ListItem Text="01" Value="01" />
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-6">
                            <asp:LinkButton runat="server" Text="Update Transactions" OnCommand="UpdateTransactions" CommandArgument='<%# ItemData.ID %>'></asp:LinkButton>
                            (please note: this update will apply to unsettled trasnaction only)
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>
</admin:FormSections>
<%-- End of Payout Schedule form section --%>

<admin:FormSections runat="server" Title="ROLLING RESERVE">
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <asp:Literal runat="server" Text="Comment" />
            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" ID="txtRRComment" Text='<%# ItemData.RRComment %>' />
        </div>
    </div>
    <hr />
    <asp:UpdatePanel runat="server" ID="upActionToTake" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row">
                <div class="col-md-4 col-lg-4">
                    <div class="form-group">
                        <asp:Literal runat="server" Text="Hold Percentage" />
                        <div class="input-group">
                            <asp:TextBox runat="server" CssClass="form-control" ID="txtHoldPercentage" Text='<%# ItemData.SecurityDeposit.ToAmountFormat() %>' />
                            <span class="input-group-addon">%</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4">
                    <div class="form-group">
                        <asp:Literal runat="server" Text="Hold Period(Months)" />
                        <JQ:Spinner ID="SpinnerHoldPeriod" runat="server" Value='<%# ItemData.SecurityPeriod %>' />
                    </div>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-md-4 col-lg-4">
                    <div class="form-group">
                        <h4>Action to Take</h4>
                        <asp:RadioButtonList runat="server" ID="rblActionToTake" RepeatLayout="UnorderedList" CssClass="radiobutton-list" OnSelectedIndexChanged="rblActionToTake_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Text="None" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Hold a Single Occurrence" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Hold Dynamically" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Hold a Fixed Amount" Value="3"></asp:ListItem>
                            <asp:ListItem Text="Record an External Hold" Value="4"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4">
                    <div class="form-group">

                        <asp:MultiView ID="ActionMultiView" runat="server">
                            <asp:View runat="server" ID="view0">
                            </asp:View>

                            <asp:View runat="server" ID="view1">
                                <h4>Choose one:</h4>
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <asp:CheckBox Checked='<%# ItemData.RREnabled  %>' Text="Enable Reserve" runat="server" ID="RREnable" /></li>
                                    <li class="list-group-item">
                                        <asp:CheckBox Checked='<%# ItemData.RRAutoRet.HasValue ? ItemData.RRAutoRet : false %>' Text="Enable Release" runat="server" ID="RRAutoRet" /></li>
                                </ul>
                                <span class="label label-warning">One checkbox must be checked!</span>
                            </asp:View>

                            <asp:View runat="server" ID="view2">
                                <h4>Choose Amount</h4>
                                <div class="form-group">
                                    Amount:
                                    <asp:TextBox runat="server" ID="txtFixedAmount" Value='<%# ItemData.RRState == 3? ItemData.RRKeepAmount.ToAmountFormat() : "" %>' CssClass="form-control" />
                                </div>
                                <div class="form-group">
                                    Currency:
                                    <netpay:CurrencyDropDown SelectedCurrencyIso='<%# Netpay.Bll.Currency.GetIsoCode((Netpay.CommonTypes.Currency)ItemData.RRKeepCurrency) %>' EnableBlankSelection="true" BlankSelectionText="Each" BlankSelectionValue="255" runat="server" ID="ddlFixedAmountCur" CssClass="form-control" />
                                </div>
                            </asp:View>

                            <asp:View runat="server" ID="view3">
                                <h4>Choose Amount</h4>
                                <div class="form-group">
                                    Amount:
                                    <asp:TextBox runat="server" ID="txtExternalHoldAmount" CssClass="form-control" Value='<%# ItemData.RRState == 4? ItemData.RRKeepAmount.ToAmountFormat() : "" %>' />
                                </div>
                                <div class="form-group">
                                    Currency:
                                    <netpay:CurrencyDropDown CssClass="form-control" SelectedCurrencyIso='<%# Netpay.Bll.Currency.GetIsoCode((Netpay.CommonTypes.Currency)ItemData.RRKeepCurrency) %>' EnableBlankSelection="false" runat="server" ID="ddlExternalHoldAmountCur" />
                                </div>

                            </asp:View>
                        </asp:MultiView>

                    </div>
                </div>

            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</admin:FormSections>

