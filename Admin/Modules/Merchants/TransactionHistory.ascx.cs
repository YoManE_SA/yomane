﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Modules.Merchants
{
    public partial class TransactionHistory : Controls.AccountControlBase
    {
        protected Netpay.Bll.Merchants.Merchant ItemData
        {
            get
            {
                return AccountPage.Account as Netpay.Bll.Merchants.Merchant;
            }
            private set
            {
                AccountPage.Account = value;
            }
        }

        public Bll.Transactions.Transaction Item { get { return TemplatePage.GetItemData<Bll.Transactions.Transaction>(); } set { TemplatePage.SetItemData(value); } }

        public List<Bll.Transactions.Transaction> TransactionList { get; set; }

        public Netpay.Bll.Transactions.Transaction.LoadOptions LoadOptions { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            transactionhistorydatabuttons.PagedControlID= "rptListT";
            
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {            
            if (Item == null) Item = new Bll.Transactions.Transaction();
            LoadTransactionList();
            base.DataBindChildren();
        }

        protected void LoadTransactionList()
        {
            if (Item == null) return;
            Netpay.Bll.Transactions.Transaction.SearchFilters TransactionListSearchFilter = new Bll.Transactions.Transaction.SearchFilters();

            ItemData = Account as Netpay.Bll.Merchants.Merchant;
            //Declare the search filter
            if (ItemData == null) ItemData = new Bll.Merchants.Merchant();
            TransactionListSearchFilter.merchantIDs = new List<int>() {ItemData.ID};

            //Declare the load options
            LoadOptions = new Bll.Transactions.Transaction.LoadOptions();
            LoadOptions.dataStores.Add(Infrastructure.TransactionStatus.Captured);
            LoadOptions.loadPaymentData = false;
            LoadOptions.loadDebitCompany = true;
            LoadOptions.loadMerchant = true;
            LoadOptions.loadPayerData = false;
            LoadOptions.sortAndPage = rptListT ;

            //Load the list
            TransactionList = Netpay.Bll.Transactions.Transaction.Search(LoadOptions, TransactionListSearchFilter);
            rptListT.DataSource = TransactionList;
            rptListT.DataBind();
        }
    }
}