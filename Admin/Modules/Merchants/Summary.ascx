﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Summary.ascx.cs" Inherits="Netpay.Admin.Merchants.Summary" %>


<admin:FormPanel ID="AddToBlackListFormPanel" runat="server" Title="Add to black list" Visible='<%# ItemData.ActiveStatus == MerchantStatus.Blocked %>'>
    <div class="row">
        <div class="col-md-12">
            <asp:MultiView runat="server" ActiveViewIndex='<%# ItemData.IsInBlackList? 0 : 1 %>'>
                <asp:View runat="server">
                    This merchant is already listed in Merchant Black List.
                </asp:View>

                <asp:View runat="server">
                    Status: <span class="label label-danger">BLOCKED</span>
                    <asp:LinkButton runat="server" ID="LinkButton1" Text="Add to Merchant Black List" OnCommand="AddToBlackList" CommandArgument='<%# ItemData.ID %>' OnClientClick='return confirm("Are You Sure you want to Add this merchant to BlackList ?");'>
                    </asp:LinkButton>
                </asp:View>
            </asp:MultiView>
        </div>
    </div>
</admin:FormPanel>



<admin:FormSections runat="server" Title="General Info">
    <asp:PlaceHolder runat="server" Visible="false" ID="pnlTransactionTest">

        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-info">
                    <strong>
                        <asp:Literal runat="server" ID="lblTestTransactionsCount" /></strong> Test Transactions made
                 
                    &nbsp;
                    <asp:HyperLink runat="server" ID="hViewTestTransactions" CssClass="btn btn-primary btn-cons-short" Text="View Test Transactions" NavigateUrl="../TransAdminRegularData.asp"><i class="fa fa-credit-card"></i> Test Transactions</asp:HyperLink>
                    &nbsp;
                    <asp:LinkButton runat="server" ID="btnDeleteTestTransactions" CssClass="btn btn-danger btn-cons-short"><i class="fa fa-trash-o"></i> Delete</asp:LinkButton>
                    &nbsp;
                     <a href="#" class="close" data-dismiss="alert">&times;</a>
                </div>
            </div>
        </div>

    </asp:PlaceHolder>

    <div class="row">
        <div class="col-lg-6 col-md-6">
            <ul class="list-group">
                <li class="list-group-item">Status: 
                    <asp:Literal runat="server" Text='<%#ItemData.ActiveStatus %>' />
                    <asp:HyperLink runat="server" Target="_self" Text="Change Status" NavigateUrl='<%# WebUtils.ApplicationPath + "/Merchants/MerchantsStatus/0?ctl00.apAccount.hdID="+ItemData.AccountID+"&Search=1" %>' />
                </li>
                <li class="list-group-item">Merchant Number:
		            <asp:Literal runat="server" Text="<%# ItemData.AccountNumber %>" />
                </li>
                <asp:PlaceHolder runat="server" Visible='<%# ItemData.ParentCompany.GetValueOrDefault() > 0 %>'>
                    <li class="list-group-item">Parent Company:
		            <asp:Literal runat="server" Text="<%# ItemData.ParentCompany.HasValue? Netpay.Bll.DataManager.ParentCompany.Get(ItemData.ParentCompany.Value).Code : Netpay.Bll.DataManager.ParentCompany.Get(Netpay.Bll.DataManager.ParentCompany.GetDefaultParentCompanyIdValue()).Code %>" ID="lblParentCompany" />
                    </li>
                </asp:PlaceHolder>
                <asp:PlaceHolder runat="server" Visible='<%# ItemData.MerchantGroup != null %>'>
                    <li class="list-group-item">Merchant Group:
			        <asp:Literal runat="server" Text="<%# Netpay.Bll.Merchants.Group.Get(ItemData.MerchantGroup.GetValueOrDefault()) %>" ID="lblMerchantGroup" />
                    </li>
                </asp:PlaceHolder>
            </ul>
        </div>
        <div class="col-lg-6 col-md-6">
            <ul class="list-group">
                <asp:PlaceHolder runat="server" Visible='<%# ItemData.Industry > 0 %>'>
                    <li class="list-group-item">Industry:
			        <asp:Literal runat="server" Text="<%# Netpay.Bll.Merchants.Industry.GetNameById(ItemData.Industry) %>" ID="lblIndustry" />
                    </li>
                </asp:PlaceHolder>
                <asp:PlaceHolder runat="server" Visible='<%# !string.IsNullOrEmpty(ItemData.AccountManager) %>'>
                    <li class="list-group-item">Account manager:
			            <asp:Literal runat="server" Text="<%# int.TryParse(ItemData.AccountManager, out AccountManagerID)? Netpay.Infrastructure.Security.AdminUser.GetAdminUserNameById(AccountManagerID) : ItemData.AccountManager %>" />
                    </li>
                </asp:PlaceHolder>
                <asp:PlaceHolder runat="server">
                    <li class="list-group-item">Signup:
			        <asp:Literal runat="server" Text='<%# ItemData.OpenDate.GetValueOrDefault().ToString("d")=="01/01/0001"||ItemData.OpenDate.GetValueOrDefault().ToString("d")=="01/01/1900"? "" : ItemData.OpenDate.GetValueOrDefault().ToString("d") %>' />
                    </li>
                </asp:PlaceHolder>
                <asp:PlaceHolder runat="server" Visible='<%# !string.IsNullOrEmpty(ItemData.Url) %>'>
                    <li class="list-group-item">Websites:
				         <asp:HyperLink runat="server" Target="_blank" NavigateUrl="<%# GetNavigateUrlValue() %>" Text="<%# ItemData.Url %>" />
                    </li>
                </asp:PlaceHolder>
            </ul>
        </div>
    </div>
</admin:FormSections>

<admin:FormSections runat="server" Title="Links">
    <div class="row">

        <div class="col-lg-6 col-md-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:HyperLink runat="server" Text="Merchant Control Panel" NavigateUrl='<%# GetExternalLoginUrl(false, false) %>' Target="_blank" />
                    (<asp:HyperLink runat="server" Text="Test" NavigateUrl='<%# GetExternalLoginUrl(false, true) %>' Target="_blank" />)
                </li>
                <li class="list-group-item">
                    <asp:HyperLink runat="server" Text="Merchant Developer Center" NavigateUrl='<%# GetExternalLoginUrl(true, false) %>' Target="_blank" />
                    (<asp:HyperLink runat="server" Text="Test" NavigateUrl='<%# GetExternalLoginUrl(true, true) %>' Target="_blank" />)
                </li>
                <li class="list-group-item">
                    <%--<asp:HyperLink Style="cursor: pointer" runat="server" onclick='<%# "OpenPop(" + "\"" +  string.Format("../Transactions/0?ctl00.apMerchant.hdID={0}&ctl00.chkPaymentStatus$1=Unsettled&Search=1&NoLayout=1", ItemData.AccountID) + "\"" + ",\"Unsettled Balance\",\"1330\",\"820\" , \"1\"); return false;"  %>' Text="Unsettled Balance" Target="_blank" />--%>
                    <asp:HyperLink Style="cursor: pointer" runat="server" onclick='<%# "OpenPop(" + "\"" +  string.Format("../Settlements/Upcoming/0?ctl00.apMerchant.hdID={0}&Search=1", ItemData.MerchantID.HasValue? ItemData.MerchantID.Value : 0) + "\"" + ",\"Unsettled Balance\",\"1330\",\"820\" , \"1\"); return false;"  %>' Text="Unsettled Balance" Target="_blank" />
                    <br />
                </li>
                <li class="list-group-item">
                    <asp:HyperLink Style="cursor: pointer" runat="server" onclick='<%# "OpenPop(" + "\"" +  string.Format("../Balance/0?ctl00.apAccount.hdID={0}&ctl00.ddlResults=0&Search=1&Page=0%2c0&SortKey=InsertDate&SortDesc=True&NoLayout=1", ItemData.AccountID) + "\"" + ",\"Account Balance\",\"1330\",\"820\" , \"1\"); return false;"  %>' Text="Account Balance" Target="_blank" />
                </li>
            </ul>
        </div>
        <div class="col-lg-6 col-md-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:HyperLink
                        Style="cursor: pointer"
                        runat="server"
                        onclick='<%# "OpenPop(" + "\"" +  string.Format("../Settlements/0?ctl00.apMerchant.hdID={0}&ctl00.chkPaymentStatus$1=Unsettled&Search=1&NoLayout=1", ItemData.MerchantID) + "\"" + ",\"Settlement Report\",\"1330\",\"820\" , \"1\"); return false;"  %>'
                        Text="Settlement Report"
                        Target="_blank" />
                <li class="list-group-item">
                    <asp:HyperLink runat="server" NavigateUrl='<%# string.Format("~/VirtualTerminal/0?MerchantID={0}", ItemData.MerchantID) %>' Text="Virtual Terminal" Target="_blank" /></li>
                <li class="list-group-item">
                    <asp:HyperLink
                        Style="cursor: pointer"
                        runat="server"
                        onclick='<%# "OpenPop(" + "\"" +  string.Format("../Transactions/0?ctl00.apMerchant.hdID={0}&ctl00.chklStatus$0=Captured&ctl00.DeniedStatusDropDown=1&Search=1&NoDateRange=1&NoLayout=1", ItemData.MerchantID) + "\"" + ",\"Chargeback history\",\"1330\",\"820\" , \"1\"); return false;"  %>'
                        Text="Chargeback history"
                        Target="_blank" />
            </ul>

        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Active Terminals" SecuredObject='<%# Netpay.Bll.Merchants.ProcessTerminals.SecuredObject %>'>
    <asp:MultiView runat="server" ActiveViewIndex='<%# rptTerminals.Items.Count > 0 ? 0 : 1 %>'>
        <asp:View runat="server">
    <asp:Repeater runat="server" ID="rptTerminals">
        <HeaderTemplate>
            <table class="table table-hover table-customize">
        </HeaderTemplate>
        <ItemTemplate>
            <tbody>
                <td>
                    <img src="/NPCommon/ImgPaymentMethod/23X12/<%# Eval("PaymentMethodId") %>.gif" align="middle" border="0" />
                    &nbsp;<%# Eval("PaymentMethod.Name") %> (<%# Currency.Get((int)Eval("CurrencyId")).IsoCode %>)</td>
                <td>
                    <img src="/NPCommon/ImgDebitCompanys/23X12/<%# Eval("DebitCompanyId") %>.gif" align="middle" border="0" />
                    &nbsp;<%# Eval("DebitCompanyName") %></td>
                <td><%# Eval("TerminalName") %></td>
            </tbody>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
            </asp:View>
        <asp:View runat="server">
            <div class="alert alert-info">
                <strong>info!</strong> No record information
            </div>
        </asp:View>
        </asp:MultiView>
</admin:FormSections>
<admin:FormSections runat="server" Title="Notes" SecuredObject='<%# Netpay.Bll.Accounts.Note.SecuredObject %>'>
    <asp:Repeater runat="server" ID="rptNotes">
        <ItemTemplate>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-info">
                        <h4><i class="fa fa-user"></i><%# Eval("UserName") %> (<%# Eval("InsertDate") %>) </h4>
                        <i>"<%# Eval("Text") %>" </i>
                    </div>
                </div>
            </div>
        </ItemTemplate>
        <FooterTemplate>
            <asp:HyperLink runat="server"
                Style="cursor: pointer"
                Text="See all notes"
                onclick='<%#"OpenTabByClientId(" + "\"" + GetNotesTabClientId() + "\"" + ")" %>'>
            </asp:HyperLink>
        </FooterTemplate>
    </asp:Repeater>
</admin:FormSections>
