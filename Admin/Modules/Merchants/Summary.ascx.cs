using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Merchants
{
    public partial class Summary : Controls.AccountControlBase
    {
        protected Netpay.Bll.Merchants.Merchant ItemData;

        public int AccountManagerID;

        protected override void DataBindChildren()
        {
            ItemData = Account as Netpay.Bll.Merchants.Merchant;
            if (ItemData == null) return;

            int testTransactionCount = Netpay.Bll.Transactions.Transaction.TestTransactionsCount(ItemData.ID);
            lblTestTransactionsCount.Text = testTransactionCount.ToString();
            pnlTransactionTest.Visible = (testTransactionCount > 0);

            if (Bll.Accounts.Note.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Read))
            {
                rptNotes.DataSource = Netpay.Bll.Accounts.Note.LoadForAccount(ItemData.AccountID).Take(5);
            }

            if (Bll.Merchants.ProcessTerminals.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Read))
            {
                rptTerminals.DataSource = Netpay.Bll.Merchants.ProcessTerminals.GetTerminalSummary(ItemData.ID, true);
            }

            var mg = Bll.Merchants.Group.Get(ItemData.MerchantGroup.GetValueOrDefault());
            if (mg != null) lblMerchantGroup.Text = mg.Name;
                        
            if (Bll.Merchants.Industry.Cache.Any(x => x.ID == ItemData.Industry))
                 lblIndustry.Text = Bll.Merchants.Industry.GetNameById(ItemData.Industry);

            if (ItemData.ParentCompany != null && Bll.DataManager.ParentCompany.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Read))
                lblParentCompany.Text = Bll.DataManager.ParentCompany.Get(ItemData.ParentCompany.Value).Code;

            base.DataBindChildren();
        }

        public string GetExternalLoginUrl(bool devCenter, bool isTest)
        {
            if (ItemData == null || ItemData.MerchantID == null) return null;
            return Merchants.Module.ExternalLoginUrl(ItemData.MerchantID.GetValueOrDefault(), devCenter, isTest);
        }

        public string GetBalanceUrl()
        {
            if ((ItemData == null) || ItemData.MerchantID == null) return null;

            if (ItemData.AccountID > 0)
            {
                string hdID = ItemData.AccountID.ToString();
                string url = Web.WebUtils.ApplicationPath;
                return url + "/Balance/0?ctl00.apAccount.hdID=" + hdID + "&ctl00.ddlResults=0&Search=1&Page=0%2c0&SortKey=InsertDate&SortDesc=True";
            }
            return "";
        }

        public string GetNotesTabClientId()
        {
            if ((Page as Controls.DataTablePage).FormTabs.Tabs.Any(x => x.Title == "File & Notes"))
            {
                return (Page as Controls.DataTablePage).FormTabs.Tabs.Where(x => x.Title == "File & Notes").FirstOrDefault().ClientID;
            }
            else
            {
                return "";
            }

        }

        protected void AddToBlackList(object sender, CommandEventArgs e)
        {
            if (((sender as LinkButton).Page as Controls.AccountPage).Account != null)
            {
                int? merchantId = ((sender as LinkButton).Page as Controls.AccountPage).Account.MerchantID;
                if (merchantId.HasValue)
                {
                    Bll.Merchants.Merchant.AddMerchantToBlackList(merchantId.Value, Web.WebUtils.IsLoggedin ? Web.WebUtils.LoggedUser.UserName : "");
                    ItemData = Bll.Merchants.Merchant.Load(merchantId);
                }
                AddToBlackListFormPanel.BindAndUpdate();
            }

        }

        public string GetNavigateUrlValue()
        {
            if (ItemData == null || string.IsNullOrEmpty(ItemData.Url)) return "";
            if (ItemData.Url.Contains("http://") || ItemData.Url.Contains("https://"))
            {
                return ItemData.Url;
            }
            else
            {
                return "http://" + ItemData.Url;
            }
        }
    }
}