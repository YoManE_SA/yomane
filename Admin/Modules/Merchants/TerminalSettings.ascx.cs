using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using Netpay.Web;

namespace Netpay.Admin.Merchants
{
    public partial class TerminalSettings : Controls.AccountControlBase
    {
        public Netpay.Bll.Merchants.ProcessTerminals ItemData;

        protected override void OnLoad(EventArgs e)
        {
            //TemplatePage.PageController.SaveItem += PageController_SaveItem;
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            if (ItemData == null) ItemData = new Bll.Merchants.ProcessTerminals(0);
            rptTerminals.DataSource = getTerminalList().OrderBy(x => x.PaymentMethod.ID).ToList();
            base.DataBindChildren();
        }

        protected void OnFilterChange(object sender, EventArgs e)
        {
            DataBindChildren();
            upList.Update();
        }

        private List<Netpay.Bll.Merchants.ProcessTerminals> getTerminalList()
        {
            if (Account == null) return new List<Bll.Merchants.ProcessTerminals>();
            return Netpay.Bll.Merchants.ProcessTerminals.LoadForMerchant(Account.MerchantID.GetValueOrDefault(), (chkShowInactive.Checked ? null : (bool?)true));
        }

        private void setDecFieldColor(ITextControl tb)
        {
            (tb as WebControl).ForeColor = (tb.Text.ToDecimal(0) <= 0 ? System.Drawing.Color.Red : System.Drawing.Color.Black);
        }

        protected List<Netpay.Bll.Merchants.ProcessTerminals.TerminalReference> SaveRefTerminals(Netpay.Web.Controls.DynamicRepeater rptRefTerminals, Netpay.Bll.Merchants.ProcessTerminals processTerminals)
        {
            var ret = new List<Netpay.Bll.Merchants.ProcessTerminals.TerminalReference>();
            foreach (RepeaterItem item in rptRefTerminals.Items)
            {
                var itemId = (item.FindControl("hfID") as HiddenField).Value.ToNullableInt().GetValueOrDefault(0);
                var dataItem = processTerminals.Terminals.Where(t => t.ID == itemId).SingleOrDefault();
                if (dataItem == null) dataItem = new Netpay.Bll.Merchants.ProcessTerminals.TerminalReference();
                dataItem.TerminalNumber = (item.FindControl("ddlTerminalNumber") as Controls.TerminalPicker).Value;
                dataItem.Ratio = (item.FindControl("txtRatio") as TextBox).Text.ToInt32(1);
                ret.Add(dataItem);
            }
            processTerminals.TSelMode = (byte)ddlSelectionMode.Value.ToInt32(0);
            return ret;
        }

        protected void rptTerminals_OnItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Delete":
                    rptTerminals.RemoveItem(e.Item.ItemIndex);
                    break;

                case "DeleteRefTerminal":
                    int refTerminalIdToDelete;

                    if (int.TryParse(e.CommandArgument.ToString(), out refTerminalIdToDelete))
                    {
                        Bll.Merchants.ProcessTerminals.TerminalReference currentTerminalRef = Bll.Merchants.ProcessTerminals.TerminalReference.Get(refTerminalIdToDelete);
                        currentTerminalRef.Delete();
                    }

                    //Load the ProcessTerminal ItemData again
                    ItemData = Bll.Merchants.ProcessTerminals.Load(int.Parse(hfID.Value));

                    //Bind and update the modal dialog again , in order to update the ref terminals list.
                    dlgEdit.BindAndUpdate();
                    break;
            }
        }

        protected void Edit_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "AddItem":
                    if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                        ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Netpay.Bll.Merchants.ProcessTerminals.SecuredObject, PermissionValue.Add);

                    ItemData = new Netpay.Bll.Merchants.ProcessTerminals(Account.MerchantID.GetValueOrDefault());

                    dlgEdit.DataBind();
                    dlgEdit.RegisterShow();
                    break;
                case "Edit":
                    ItemData = Netpay.Bll.Merchants.ProcessTerminals.Load(e.CommandArgument.ToNullableInt().GetValueOrDefault());
                    dlgEdit.DataBind();
                    setDecFieldColor(txtPercentFee); setDecFieldColor(txtFixedFee); setDecFieldColor(txtApproveFixedFee);
                    setDecFieldColor(txtRefundFixedFee); setDecFieldColor(txtClarificationFee); setDecFieldColor(txtCBFixedFee); setDecFieldColor(txtFailFixedFee); setDecFieldColor(txtPartialRefundFixedFee);
                    setDecFieldColor(txtFraudRefundFixedFee); setDecFieldColor(txtCashbackPercent);
                    dlgEdit.RegisterShow();
                    break;
                case "Save":
                    //Get the current merchant process terminal list in order to check if a terminal with
                    //The same PaymentMethod, Currencies, CountryBin list and RefTerminal number exist , if yes don't allow save.
                    bool IsDuplicate = false;

                    if (Account.MerchantID.HasValue)
                    {

                        List<Bll.Merchants.ProcessTerminals> CurrentMerchantTerminals = Bll.Merchants.ProcessTerminals.LoadForMerchant(Account.MerchantID.Value, null);
                        if (CurrentMerchantTerminals.Count > 0)
                        {
                            string binsListToSearch = scListBINs.Value.Count > 0 ? string.Format(",{0},", string.Join(",", scListBINs.Value)) : "";
                            if (CurrentMerchantTerminals.Exists(x =>
                                (x.ListBINs == binsListToSearch) &&
                                ((int)x.PaymentMethodId == ddlPaymentMethod.Value.ToNullableInt().GetValueOrDefault(0)) &&
                                (x.CurrencyID == ddlCurrency.Value.ToNullableInt().GetValueOrDefault(0)) &&
                                (x.ExchangeTo == ddlExchangeTo.Value.ToInt32(0)) &&
                                (x.ID != hfID.Value.ToNullableInt().GetValueOrDefault()))
                                )
                            {
                                SaveActionNotify.SetMessage("A terminal with the same PaymentMethod, Currencies<br>And Bin countries already exist.(include InActive terminals).", true);
                                SaveActionNotify.DataBind();
                                IsDuplicate = true;
                            }
                        }
                    }

                    if (!IsDuplicate)
                    {

                        ItemData = Netpay.Bll.Merchants.ProcessTerminals.Load(hfID.Value.ToNullableInt().GetValueOrDefault());
                        if (ItemData == null) ItemData = new Bll.Merchants.ProcessTerminals(Account.MerchantID.GetValueOrDefault());
                        ItemData.PaymentMethodId = (CommonTypes.PaymentMethodEnum)ddlPaymentMethod.Value.ToNullableInt().GetValueOrDefault(0);
                        ItemData.CurrencyID = ddlCurrency.Value.ToNullableInt().GetValueOrDefault(0);
                        ItemData.ExchangeTo = (short)ddlExchangeTo.Value.ToInt32(0);
                        ItemData.IsDisabled = !chkIEnabled.Checked;
                        ItemData.ListBINs = scListBINs.Value.Count > 0? string.Format(",{0},", string.Join(",", scListBINs.Value)) : "";
                        ItemData.FixedFee = txtFixedFee.Text.ToDecimal(0);
                        ItemData.PercentFee = txtPercentFee.Text.ToDecimal(0);

                        ItemData.ApproveFixedFee = txtApproveFixedFee.Text.ToDecimal(0);
                        ItemData.RefundFixedFee = txtRefundFixedFee.Text.ToDecimal(0);
                        ItemData.PartialRefundFixedFee = txtPartialRefundFixedFee.Text.ToDecimal(0);
                        ItemData.FraudRefundFixedFee = txtFraudRefundFixedFee.Text.ToDecimal(0);
                        ItemData.ClarificationFee = txtClarificationFee.Text.ToDecimal(0);
                        ItemData.CBFixedFee = txtCBFixedFee.Text.ToDecimal(0);
                        ItemData.FailFixedFee = txtFailFixedFee.Text.ToDecimal(0);
                        ItemData.CashbackPercent = txtCashbackPercent.Text.ToDecimal(0);
                        ItemData.Terminals = SaveRefTerminals(rptRefTerminals, ItemData);

                        ItemData.Save();
                        DataBindChildren();
                        dlgEdit.RegisterHide();
                        upList.Update();
                    }
                    break;

                case "AddRefTerminal":
                    if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                        ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Netpay.Bll.Merchants.ProcessTerminals.SecuredObject, PermissionValue.Add);

                    rptRefTerminals.AddItem(new Netpay.Bll.Merchants.ProcessTerminals.TerminalReference() { Ratio = 1 });
                    break;

                case "DialogClose":
                    DataBindChildren();
                    upList.Update();
                    break;
            }
        }
    }
}