﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="TerminalSettings.ascx.cs" Inherits="Netpay.Admin.Merchants.TerminalSettings" %>
<%@ Register Src="~/Controls/SelectCountry.ascx" TagPrefix="CUC" TagName="SelectCountry" %>
<admin:ModalDialog runat="server" ID="dlgEdit" OnCommand="Edit_Command" Title="Edit Terminal Settings">
    <Body>
        <netpay:ActionNotify runat="server" ID="SaveActionNotify" />
        <asp:HiddenField runat="server" ID="hfID" Value='<%# ItemData.ID %>' />
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <ul class="list-group">
                    <li class="list-group-item">
                        <asp:CheckBox runat="server" ID="chkIEnabled" Checked='<%# !ItemData.IsDisabled %>' Text="Enabled" /></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4">
                Payment Method:
                   <div class="form-group">
                       <div class="input-group">
                           <span class="input-group-addon">
                               <img src="/NPCommon/ImgPaymentMethod/23X12/<%# (int)ItemData.PaymentMethodId.GetValueOrDefault(0) %>.gif" height="10"></span>
                           <netpay:PaymentMethodDropDown CssClass="form-control" runat="server" ID="ddlPaymentMethod" BlankSelectionValue="-1" Value='<%# ItemData.PaymentMethodId == null ? -1 : (int)ItemData.PaymentMethodId %>' />
                       </div>
                   </div>
            </div>
            <div class="col-lg-4 col-md-4">
                Source Currency:
                    <div class="form-group">
                        <netpay:CurrencyDropDown runat="server" ID="ddlCurrency" Value='<%# ItemData.CurrencyID %>' CssClass="form-control" />
                    </div>
            </div>
            <div class="col-lg-4 col-md-4">
                Exchange To Currency:
                    <div class="form-group">
                        <netpay:CurrencyDropDown runat="server" ID="ddlExchangeTo" Value='<%# ItemData.ExchangeTo %>' CssClass="form-control" />
                    </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12">
                Limit To bin Countries:
                    <div class="form-group">
                        <CUC:SelectCountry runat="server" ID="scListBINs" Value='<%# ItemData.ListBINs.EmptyIfNull().Split((char)44).ToList() %>' CssClass="form-control-multiline" />
                    </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3">
                Transaction (Value):
                <div class="form-group">
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtFixedFee" Text='<%# ItemData.FixedFee.ToString("0.00") %>' />
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                Processing (%):
                <div class="form-group">
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtPercentFee" Text='<%# ItemData.PercentFee.ToString("0.00") %>' />
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                Authorization(Value):
                <div class="form-group">
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtApproveFixedFee" Text='<%# ItemData.ApproveFixedFee.ToString("0.00") %>' />
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                Refund(Value):
                <div class="form-group">
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtRefundFixedFee" Text='<%# ItemData.RefundFixedFee.ToString("0.00") %>' />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3">
                R.Partial(Value):
                <div class="form-group">
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtPartialRefundFixedFee" Text='<%# ItemData.PartialRefundFixedFee.ToString("0.00") %>'></asp:TextBox>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                R.Fraud (Value):
                <div class="form-group">
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtFraudRefundFixedFee" Text='<%# ItemData.FraudRefundFixedFee.ToString("0.00") %>'></asp:TextBox>
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                Clarification(Value):
                <div class="form-group">
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtClarificationFee" Text='<%# ItemData.ClarificationFee.ToString("0.00") %>' />
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                Chargeback(Value):
                <div class="form-group">
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtCBFixedFee" Text='<%# ItemData.CBFixedFee.ToString("0.00") %>' />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3 col-md-3">
                Decline(Value):
                <div class="form-group">
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtFailFixedFee" Text='<%# ItemData.FailFixedFee.ToString("0.00") %>' />
                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                Cashback (%):
                <div class="form-group">
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtCashbackPercent" Text='<%# ItemData.CashbackPercent.ToString("0.00") %>'></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-md-6">
                Terminal References:
                <div class="form-group">
                    <asp:Button runat="server" ID="btnAdd" CssClass="btn btn-primary" Text="Add terminal" CommandName="AddRefTerminal" />
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                Selection Mode:
                <div class="form-group">
                    <netpay:EnumDropDown CssClass="form-control" runat="server" ID="ddlSelectionMode" ViewEnumName="Netpay.Bll.Merchants.ProcessTerminals+RefTerminalSelectionMode" EnableBlankSelection="false" Value='<%# ItemData.TSelMode %>' Enabled='<%# ItemData.Terminals.Count > 1 %>' />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <netpay:DynamicRepeater runat="server" ID="rptRefTerminals" DataSource='<%# ItemData.Terminals %>' OnItemCommand="rptTerminals_OnItemCommand">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="pull-left">Terminal</div>
                                    <div class="pull-right">
                                        <asp:Button runat="server" ID="btnDelete" CssClass="btn btn-danger" Text="Delete" CommandName="DeleteRefTerminal" CommandArgument='<%# Eval("ID") %>'/>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body">
                                    <asp:HiddenField runat="server" ID="hfID" Value='<%# Eval("ID") %>' Visible="false" />
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-8">
                                                Terminal Details:
                                 <admin:TerminalPicker runat="server" ID="ddlTerminalNumber" Value='<%# Eval("TerminalNumber") %>' Text='<%# Eval("Text") %>' />
                                            </div>
                                            <div class="col-md-2">
                                                Ratio:
                            <asp:TextBox runat="server" ID="txtRatio" Text='<%# Eval("Ratio") %>' CssClass="form-control" />
                                            </div>
                                            <div class="col-md-2">
                                                <br />
                                                Usage: <%# Eval("UseCount") %>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                    <EmptyContainer>
                        <div class="alert alert-info"><strong>Info!</strong> No record information</div>
                    </EmptyContainer>
                </netpay:DynamicRepeater>
            </div>
        </div>
    </Body>
    <Footer>
        <asp:Button runat="server" CssClass="btn btn-primary" CommandName="Save" Text="Save" />
        <asp:Button runat="server" CssClass="btn btn-danger" CommandName="Delete" Text="Delete" Enabled="false" />
    </Footer>
</admin:ModalDialog>
<admin:FormSections runat="server" Title="Terminal Settings" Flexible="false" EnableAdd="true" OnCommand="Edit_Command">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox runat="server" Text="Show Inactive terminals" ID="chkShowInactive" AutoPostBack="true" OnCheckedChanged="OnFilterChange" />
                </li>
            </ul>

        </div>
    </div>
    <hr />
    <NP:UpdatePanel runat="server" ID="upList" RenderMode="Block" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>
            <netpay:DynamicRepeater runat="server" ID="rptTerminals" OnItemCommand="rptTerminals_OnItemCommand">
                <HeaderTemplate>
                    <div class="table-responsive">
                        <table class="table table-hover table-customize">
                            <thead>
                                <th class="text-nowrap">Payment Method:</th>
                                <th class="text-nowrap">Terminal:</th>
                                <th class="text-nowrap">Is Active:</th>
                                <th class="text-nowrap">Currency:</th>
                                <th class="text-nowrap">Territories:</th>
                                <th class="text-nowrap">Processing:</th>
                                <th class="text-nowrap">Transaction:</th>
                                <th class="text-nowrap">Authorization:</th>
                                <th class="text-nowrap">Refund:</th>
                                <th class="text-nowrap">Clarification:</th>
                                <th class="text-nowrap">Chargeback:</th>
                                <th class="text-nowrap">Decline:</th>
                                <th class="text-nowrap">Cashback:</th>
                                <th class="text-nowrap">Action:</th>
                            </thead>
                            <tbody>
                </HeaderTemplate>
                <ItemTemplate>


                    <tr>
                        <td class="text-nowrap">
                            <img src="/NPCommon/ImgPaymentMethod/23X12/<%# (int)Eval("PaymentMethodId") %>.gif">&nbsp; <%# Eval("PaymentMethod.Name") %></td>
                        <td class="text-nowrap">
                            <asp:Repeater runat="server" DataSource='<%# (Container.DataItem as Netpay.Bll.Merchants.ProcessTerminals).Terminals %>'>
                                <ItemTemplate>
                                    <asp:Literal runat="server" Text='<%# Eval("Text") %>' />
                                    <br />
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>

                        <td class="text-nowrap">
                            <asp:MultiView runat="server" ActiveViewIndex='<%# ((bool)Eval("IsDisabled")) ? 1 : 0 %>'>
                                <asp:View runat="server"><span style="color: #29af00;"><i class="fa fa-check"></i></asp:View>
                                <asp:View runat="server"><span style="color: #af0000;"><i class="fa fa-times "></i></asp:View>
                            </asp:MultiView>
                        </td>

                        <td class="text-nowrap">
                            <asp:Literal runat="server" Text='<%# Currency.Get((int)Eval("CurrencyID")).IsoCode %>' />
                            to
                                    <asp:Literal runat="server" Text='<%# Currency.GetIsoCode((Netpay.CommonTypes.Currency)Eval("ExchangeTo")) %>' />
                        </td>
                        <td class="text-nowrap">
                            <asp:Literal runat="server" ID="txtListBINs" Text='<%# Eval("ListBINs") %>' />
                        </td>
                        <td class="text-nowrap">
                            <asp:Literal runat="server" Text='<%# ((decimal)Eval("PercentFee")).ToString("0.00") %>' />
                        </td>
                        <td class="text-nowrap">
                            <asp:Literal runat="server" Text='<%# ((decimal)Eval("FixedFee")).ToString("0.00") %>' /></td>
                        <td class="text-nowrap">
                            <asp:Literal runat="server" Text='<%# ((decimal)Eval("ApproveFixedFee")).ToString("0.00") %>' /></td>
                        <td class="text-nowrap">
                            <asp:Literal runat="server" Text='<%# ((decimal)Eval("RefundFixedFee")).ToString("0.00") %>' /></td>
                        <td class="text-nowrap">
                            <asp:Literal runat="server" Text='<%# ((decimal)Eval("ClarificationFee")).ToString("0.00") %>' /></td>
                        <td class="text-nowrap">
                            <asp:Literal runat="server" Text='<%# ((decimal)Eval("CBFixedFee")).ToString("0.00") %>' /></td>
                        <td class="text-nowrap">
                            <asp:Literal runat="server" Text='<%# ((decimal)Eval("FailFixedFee")).ToString("0.00") %>' />
                        </td>
                        <td class="text-nowrap">
                            <asp:Literal runat="server" Text='<%# ((decimal)Eval("CashbackPercent")).ToString("0.00") %>' />
                        </td>
                        <td class="text-nowrap">
                            <NP:Button runat="server" CssClass="btn btn-primary text-uppercase" OnCommand="Edit_Command" CommandName="Edit" CommandArgument='<%# Eval("ID") %>' Text="Edit" />
                        </td>
                    </tr>


                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                    </table>
                  </div>
                </FooterTemplate>
            </netpay:DynamicRepeater>
        </ContentTemplate>
    </NP:UpdatePanel>
</admin:FormSections>
