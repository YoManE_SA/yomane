﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="RiskSettings.ascx.cs" Inherits="Netpay.Admin.Merchants.RiskSettings" %>
<%@ Register Src="~/Controls/SelectCountry.ascx" TagPrefix="CUC" TagName="SelectCountry" %>
<%@ Register Src="~/Controls/SelectState.ascx" TagPrefix="CUC" TagName="SelectState" %>

<admin:FormSections runat="server" Title="Billing Address">
    <div class="row">
        <div class="col-lg-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsForceBillingAddress" runat="server" Text="Billing address must be verified" Checked="<%# MerchantSettings.IsForceBillingAddress %>" /></li>
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsForceBillingAddressCity" runat="server" Text="Only Country and Zip are required" Checked="<%# MerchantSettings.IsForceBillingAddressOnlyZipcodeAndCountryAreRequired %>" />
                </li>
            </ul>
        </div>
        <div class="col-lg-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsForce3dSecure" runat="server" Text="Process using 3D secure" Checked="<%# MerchantSettings.IsForce3dSecure %>" /></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 spacer">
            <ul class="list-group spacer">
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsForceInstantDebitBillingAddress" runat="server" Text="Billing address must be verified (Instant Debit)" Checked="<%# MerchantSettings.IsForceInstantDebitBillingAddress %>" /></li>
            </ul>
        </div>
        <div class="col-lg-6 spacer">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox ID="chkIsCreditCardWhitelistEnabled" runat="server" Text="Enable Credit Card Whitelist" Checked="<%# MerchantSettings.IsCreditCardWhitelistEnabled %>" /></li>
            </ul>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="CC \ Email Limitation">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <h4>Limit CC/s for a single email address</h4>
            <div class="form-group">
                Number of credit cards
                    <netpay:NumericDropDown ID="ddlLimitCcForEmailAllowedCount" CssClass="form-control" runat="server" MinValue="0" MaxValue="5" EnableBlankSelection="False" Value="<%# MerchantSettings.LimitCcForEmailAllowedCount %>" />
            </div>
            <h4>When the limit is exceeded:</h4>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <asp:CheckBox ID="chkIsLimitCcForEmailDeclineTrans" runat="server" Text="Decline transaction" Checked="<%# MerchantSettings.IsLimitCcForEmailDeclineTrans %>" /></li>
                        <li class="list-group-item">
                            <asp:CheckBox ID="chkIsLimitCcForEmailBlockEmail" runat="server" Text="Blacklist email address" Checked="<%# MerchantSettings.IsLimitCcForEmailBlockEmail %>" /></li>
                        <li class="list-group-item">
                            <asp:CheckBox ID="chkIsLimitCcForEmailBlockNewCc" runat="server" Text="Blacklist new CC" Checked="<%# MerchantSettings.IsLimitCcForEmailBlockNewCc %>" /></li>
                        <li class="list-group-item">
                            <asp:CheckBox ID="chkIsLimitCcForEmailBlockAllCc" runat="server" Text="Blacklist all CCs" Checked="<%# MerchantSettings.IsLimitCcForEmailBlockAllCc %>" /></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 spacer">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <asp:CheckBox ID="chkIsLimitCcForEmailNotifyByEmail" runat="server" Text="Notify by email/s" Checked="<%# MerchantSettings.IsLimitCcForEmailNotifyByEmail %>" /></li>
                        <li>
                        <li class="list-group-item">
                            <ul class="child-list-group">
                                <li>
                                    <asp:TextBox ID="txtLimitCcForEmailNotifyByEmailList" CssClass="form-control" runat="server" Text="<%# MerchantSettings.LimitCcForEmailNotifyByEmailList %>" />
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <h4>Limit Email/s for a single Credit Card</h4>
            <div class="form-group">
                Number of Emails
                  <netpay:NumericDropDown ID="ddlLimitEmailForCcAllowedCount" CssClass="form-control" runat="server" MinValue="0" MaxValue="5" EnableBlankSelection="False" BlankSelectionValue="0" Value="<%# MerchantSettings.LimitEmailForCcAllowedCount %>" />
            </div>
            <h4>When the limit is exceeded:</h4>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <asp:CheckBox ID="chkIsLimitEmailForCcDeclineTrans" runat="server" Text="Decline transaction" Checked="<%# MerchantSettings.IsLimitEmailForCcDeclineTrans %>" /></li>
                        <li class="list-group-item">
                            <asp:CheckBox ID="chkIsLimitEmailForCcBlockCc" runat="server" Text="Blacklist CC" Checked="<%# MerchantSettings.IsLimitEmailForCcBlockCc %>" /></li>
                        <li class="list-group-item">
                            <asp:CheckBox ID="chkIsLimitEmailForCcBlockNewEmail" runat="server" Text="Blacklist new Email" Checked="<%# MerchantSettings.IsLimitEmailForCcBlockNewEmail %>" /></li>
                        <li class="list-group-item">
                            <asp:CheckBox ID="chkIsLimitEmailForCcBlockAllEmails" runat="server" Text="Blacklist all Emails" Checked="<%# MerchantSettings.IsLimitEmailForCcBlockAllEmails %>" /></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 spacer">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <asp:CheckBox ID="chkIsLimitEmailForCcNotifyByEmail" runat="server" Text="Notify by email/s" Checked="<%# MerchantSettings.IsLimitEmailForCcNotifyByEmail %>" /></li>
                        <li>
                        <li class="list-group-item">
                            <ul class="child-list-group">
                                <li>
                                    <asp:TextBox ID="txtLimitEmailForCcNotifyByEmailList" CssClass="form-control" runat="server" Text="<%# MerchantSettings.LimitEmailForCcNotifyByEmailList %>" />
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="Transaction Amounts">
    <div class="form-group clear-form-group">
        Predefined Transaction Amounts (comma-separated)
        <asp:TextBox runat="server" ID="txtAllowedAmounts" TextMode="MultiLine" CssClass="form-control" Rows="2" Text='<%# string.Join(",", MerchantSettings.AllowedAmounts.Select(v => v.ToString())) %>' />
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="White list/Black List IP's">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>Geo Ip/Bin</h4>
                    <div class="form-group">
                        BlackList State:
			<CUC:SelectState runat="server" ID="ssStateBlacklist" CssClass="form-control-multiline" Value="<%# MerchantSettings.StateBlacklist %>" />
                    </div>
                    <div class="form-group">
                        WhiteList State:
			<CUC:SelectState runat="server" ID="ssStateWhitelist" CssClass="form-control-multiline" Value="<%# MerchantSettings.StateWhitelist %>" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</admin:FormSections>
<admin:FormSections runat="server" Title="White\Black List Country">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>State</h4>
                    <div class="form-group">
                        BlackList Country:
			  <CUC:SelectCountry runat="server" ID="scCountryBlacklist" CssClass="form-control-multiline" Value="<%# MerchantSettings.CountryBlacklist %>" />
                    </div>
                    <div class="form-group">
                        WhiteList Country:
			 <CUC:SelectCountry runat="server" ID="scCountryWhitelist" CssClass="form-control-multiline" Value="<%# MerchantSettings.CountryWhitelist %>" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</admin:FormSections>
