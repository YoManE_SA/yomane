﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Emails.ascx.cs" Inherits="Netpay.Admin.Merchants.Emails" %>
<style>
    .email-list {
        width: 100%;
    }

        .email-list th {
            background: #cec9c9;
        }

        .email-list td {
            padding: 10px;
            font-size: 16px;
            color: #576475;
        }

    .muted {
        color: #95989A !important;
    }

    .email-list tr:nth-child(even) {
        background: #F5F5F5;
    }

    .email-list tr:nth-child(odd) {
        background: #fff;
    }
</style>

<admin:ListSection ID="ListSection1" runat="server" Icon="fa fa-envelope-o" Title="Email">
    <Header></Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList SaveAjaxState="true" runat="server" DataKeyNames="ID" ID="rptList" ShowHeader="true" AutoGenerateColumns="false" BubbleLoadEvent="false" SortExpression="DATE" DisableRowSelect='<%# Page is AccountPage && (Page as AccountPage).TemplateName == "Merchant" %>'>
                <Columns>
                        <asp:TemplateField HeaderText="Open">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" CssClass="btn-xs btn-info" Text="open" OnClientClick='<%# "OpenPop(" + "\"" +  string.Format("../Emails/0?ctl00.MessageId={0}&SelectedIndex=0&Search1=1&Page=0&NoLayout=1", Eval("ID")) + "\"" + ",\"Emails\",\"1330\",\"820\" , \"1\"); return false;"  %>'></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="From" DataField="EmailFrom" SortExpression="EmailFrom" />
                    <%--  Text='<%# Eval("EmailFrom") %>'--%>
                    <asp:BoundField HeaderText="Subject" DataField="Subject" SortExpression="Subject" ItemStyle-CssClass="muted" />
                    <asp:BoundField HeaderText="Date" DataField="Date" SortExpression="Date" />

                    <asp:TemplateField HeaderText="Asign To">
                        <ItemTemplate>
                            <%# GetMerchantName((int?)Eval("MerchantID")) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Replied By" DataField="EmailTo" SortExpression="EmailTo" />
                    <asp:BoundField HeaderText="User" DataField="AdmincashUser" SortExpression="AdmincashUser" />
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="false" EnableAdd="false" />
    </Footer>
</admin:ListSection>
