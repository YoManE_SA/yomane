using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Admin.Controls;
using System.Net.Mail;
using Netpay.Bll.Accounts;
using Netpay.Bll.Merchants;
using Netpay.Bll.Risk;
using System.Text;
using System.Threading.Tasks;
using Netpay.FraudDetection;

namespace Netpay.Admin.Merchants
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu MerchantsManagmentMenuItem, searchBarItem;

        public override string Name { get { return "Merchant"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return "Merchant account management, Process settings, Terminal settings"; } }

        public Module() : base(Bll.Merchants.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu itmes
            MerchantsManagmentMenuItem = new ApplicationMenu("Merchants Management", "~/Merchants/0", "fa-suitcase", 1);

            //Make the quick search operate the filter Search function with the Merchant ID - so
            //The list will show only the selected merchant.
            searchBarItem = new ApplicationMenu("Merchant", "~/Merchants/0?Search=1&SelectedIndex=0&ctl00.apAccount.hdID=", "fa-credit-card");

            //Register  route
            Application.RegisterRoute("Merchants/{*id}", "Merchant", typeof(Controls.AccountPage), new System.Web.Routing.RouteValueDictionary() { { "id", @"\d*" } }, module : this);

            //Register Application_InitTemplatePage event
            Application.InitDataTablePage += Application_InitTemplatePage;
            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {            
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {                        
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Merchants", MerchantsManagmentMenuItem);
            //Application.QuickSearch.AddChild(searchBarItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            MerchantsManagmentMenuItem.Parent.RemoveChild(MerchantsManagmentMenuItem);
            //Application.QuickSearch.RemoveChild(searchBarItem);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "Merchant")
            {
                (page as Controls.AccountPage).AccountChanged += delegate
                {
                    if ((page as Controls.AccountPage).Account != null)
                        (page as Controls.AccountPage).SetStatusColor(Bll.Merchants.Merchant.MerchantStatusColor.MapColor(((page as Controls.AccountPage).Account as Bll.Merchants.Merchant).ActiveStatus));
                };
                (page as Controls.AccountPage).PageController.NewItem += PageController_NewItem;
                (page as Controls.AccountPage).PageController.LoadItem += PageController_LoadItem;
                if (!page.IsPostBack) page.Load += Page_Load;
                (page as Controls.AccountPage).PageController.PostSaveItem += PageController_PostSaveItem;
                page.AddControlToForm("Summary", page.LoadControl("~/Modules/Merchants/Summary.ascx"), this, "", page.CurrentPageSecuredObject);
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Merchants/Filter.ascx"));
                page.AddControlToList(page.LoadControl("~/Modules/Merchants/List.ascx"));
                if (page.ViewName != null) return;
                page.AddControlToForm("Data", page.LoadControl("~/Modules/Merchants/Data.ascx"), this ,"ValidateDataPage();", page.CurrentPageSecuredObject);
                page.AddControlToForm("Process", page.LoadControl("~/Modules/Merchants/ProcessSettings.ascx"), this, "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject,Bll.Merchants.ProcessSettings.SecuredObject).GetActivObject);
                page.AddControlToForm("Risk", page.LoadControl("~/Modules/Merchants/RiskSettings.ascx"), this, "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject,Bll.Merchants.RiskSettings.SecuredObject).GetActivObject);
                page.AddControlToForm("Terminals", page.LoadControl("~/Modules/Merchants/TerminalSettings.ascx"), this , "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject,Bll.Merchants.ProcessTerminals.SecuredObject).GetActivObject);
                page.AddControlToForm("Merchant Mobile POS  App", page.LoadControl("~/Modules/Merchants/MobileSettings.ascx"), this, "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject,Bll.Merchants.Mobile.MobileAppSettings.SecuredObject).GetActivObject);
                page.AddControlToForm("Risk", page.LoadControl("~/Modules/Merchants/RiskRules.ascx"), this, "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject,Bll.Risk.MerchantRiskRule.SecuredObject).GetActivObject);
                page.AddControlToForm("PayOut", page.LoadControl("~/Modules/Merchants/PayOuts.ascx"), this, "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject, Bll.Settlements.MerchantSettings.SecuredObject).GetActivObject);
                page.AddControlToForm("Emails", page.LoadControl("~/Modules/Merchants/Emails.ascx"), this ,"", Netpay.Emails.Message.SecuredObject);
                page.AddControlToForm("Fees", page.LoadControl("~/Modules/Merchants/GradedFees.ascx"), this, "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject, Bll.Settlements.MerchantSettings.SecuredObject).GetActivObject);
                //page.AddControlToForm("Transaction History", page.LoadControl("~/Modules/Merchants/TransactionHistory.ascx"));
            }
        }

        private void PageController_LoadItem(object sender, EventArgs e)
        {
            ((sender as Controls.DataPageController).Page as Controls.DataTablePage).FormButtons.EnableSave = Bll.Merchants.Merchant.SecuredObject.HasPermission(Infrastructure.Security.PermissionValue.Edit);
            ((sender as Controls.DataPageController).Page as Controls.DataTablePage).FormButtons.EnableDelete = false;
        }

        private void PageController_NewItem(object sender, EventArgs e)
        {
            // Create a new root Merchant for a tabs. Happens when pressing Add 
            // in order to create a new merchant.
             var page = (sender as Controls.DataPageController).Page as Controls.AccountPage;
             (page as Controls.AccountPage).Account = new Bll.Merchants.Merchant();

            //Set visibility of Delete button.
            ((sender as Controls.DataPageController).Page as Controls.DataTablePage).FormButtons.EnableDelete = false;
        }

        private void Page_Load(object sender, EventArgs e)
        {
            Bll.Accounts.Account account = null;
            var page = sender as Controls.DataTablePage;
            var searchValue = page.RouteData.Values["id"];
            if (searchValue != null)
            {
                var itemId = searchValue.ToNullableInt();
                if (itemId != null && itemId.Value == 0) return;
                if (itemId != null && itemId.Value != 0) account = Bll.Merchants.Merchant.Load(itemId);
                else account = Bll.Merchants.Merchant.Search(new Bll.Merchants.Merchant.SearchFilters() { Name = searchValue.ToString() }, new Infrastructure.SortAndPage(0, 1)).FirstOrDefault();
            }
            if (account != null)
            {
                (page as Controls.AccountPage).Account = account;
                page.PageController.Mode = Controls.DataPageController.ViewMode.Form;
                page.PageController.DataKey = new System.Web.UI.WebControls.DataKey(new System.Collections.Specialized.OrderedDictionary() { { "AccountID", account.AccountID } });
            }
        }

        public static string ExternalLoginUrl(int merchantId, bool devCenter, bool isTest)
        {
            string url;

            if (devCenter) url = Web.WebUtils.CurrentDomain.DevCentertUrl + "website/Login.aspx";

            else url = Web.WebUtils.CurrentDomain.MerchantUrl + "website/logout.aspx";
            if (isTest) url = url.Replace("netpay-intl.com", "test.netpay-intl.com");

            var itemData = Netpay.Bll.Merchants.Merchant.Load(merchantId);
            string password = Netpay.Infrastructure.Security.Encryption.Encrypt(Netpay.Infrastructure.Security.Login.GetPassword(itemData.LoginID.GetValueOrDefault(), HttpContext.Current.Request.UserHostAddress)).ToBase64UrlString();
            string username = Netpay.Infrastructure.Security.Encryption.Encrypt(itemData.Login.UserName).ToBase64UrlString();
            string email = Netpay.Infrastructure.Security.Encryption.Encrypt(itemData.Login.EmailAddress).ToBase64UrlString();

            return string.Format("{0}?login=outer&mail={1}&username={2}&userpassword={3}", url, email, username, password);
        }

        private void PageController_PostSaveItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.AccountPage;

            //Try to get the Merchant after save because now it has a new ID
            //And it is needed to load the account after it was saved 
            //In order to show new details that were generated by the database.
            if (page != null)
            {
                if (page.Account.AccountType == Bll.Accounts.AccountType.Merchant)
                {
                    if (page.Account == null) return;
                    int merchantid = (int)page.Account.MerchantID;
                    var MerchantAfterSave = Bll.Merchants.Merchant.Load(merchantid);
                    page.Account = MerchantAfterSave;
                    page.PageController.DataKey = new System.Web.UI.WebControls.DataKey(new System.Collections.Specialized.OrderedDictionary() { { "ID", MerchantAfterSave.AccountID } });


                    //Check if the merchant has values that are in black list,
                    //If so show them on a modal dialog ...
                    var BlackListSearchFilters = new Bll.MerchantBlackList.MerchantBlackListSettings.SearchFilters();
                    BlackListSearchFilters.CompanyID = MerchantAfterSave.ID;
                    BlackListSearchFilters.MerchantNo = MerchantAfterSave.Number;
                    BlackListSearchFilters.CompanyName = MerchantAfterSave.Name;
                    BlackListSearchFilters.CompanyLegalNumber = MerchantAfterSave.LegalNumber;
                    BlackListSearchFilters.IDNumber = MerchantAfterSave.OwnerSSN;
                    BlackListSearchFilters.FirstName = MerchantAfterSave.ContactFirstName;
                    BlackListSearchFilters.LastName = MerchantAfterSave.ContactLastName;
                    BlackListSearchFilters.Phone = MerchantAfterSave.ContactPhone;
                    BlackListSearchFilters.Fax = MerchantAfterSave.Fax;
                    BlackListSearchFilters.Cellular = MerchantAfterSave.ContactMobilePhone;
                    BlackListSearchFilters.Mail = MerchantAfterSave.Mail;
                    BlackListSearchFilters.URL = MerchantAfterSave.Url;
                    BlackListSearchFilters.IsOrCondition = true;
                         
                    //Get the list of merchant black list items that contains fields of the Merchant that was saved
                    var list = Bll.MerchantBlackList.MerchantBlackListSettings.Search(BlackListSearchFilters, null);    
                                        
                    
                    page.PageController.FormView.Update();
                    

                    //If items were found show a link to them on a modal dialog
                    if (list.Count > 0)
                    {
                        // ActionMixins.SendEmail();

                        SendAlert(MerchantAfterSave.ID.ToString());


                        page.FormViewGlobalModalDialog.Title = "Merchant saved , Merchat details found in black list.";
                        page.FormViewGlobalModalDialog.Body.Controls.Add(new HyperLink()
                        {
                            Text = "View list",
                            Target = "_blank",
                            NavigateUrl = "~/Risk/MerchantBlackList/0?SearchMerchantID=" + MerchantAfterSave.ID.ToString()
                        });
                        page.FormViewGlobalModalDialog.BindAndShow();
                    }
                }
            }
        }
       private void SendAlert(string MerchantID)
        {
            StringBuilder emailBody = new StringBuilder();

            string path = System.Web.HttpContext.Current.Request.Url.Authority+System.Web.HttpContext.Current.Request.ApplicationPath + "/Risk/MerchantBlackList/0?SearchMerchantID= " + MerchantID;
            emailBody.Append("Merchat details found in BlackList, Use this link to view :  "); 
            emailBody.Append(path);
   
            MailMessage email = new MailMessage();

            // Get mail address from
            string emailAddressFrom = Netpay.Infrastructure.Application.GetParameter("mailAddressFrom");

            email.From = new MailAddress(emailAddressFrom, "Merchant-Blacklist"/*Ask Eliad*/);

            foreach (MailAddress emailAddress in FraudDetectionManager.AlertRecipients/*Ask Eliad*/)

                email.To.Add(emailAddress);
                // test email email.To.Add("yaromeran@gmail.com");

            email.Subject = "Detected Merchant with parameters in Blacklist";
            email.BodyEncoding = Encoding.UTF8;
            email.IsBodyHtml = true;
            email.Body = emailBody.ToString();

            try
            {
                Netpay.Infrastructure.Email.SmtpClient.Send(email);
            }
            catch (Exception ex)
            {
                Logger.Log(LogSeverity.Error, LogTag.Fraud, ex.Message, ex.StackTrace);
            }
        }

    }
}