﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.PendingEvents.Filter" %>
<admin:FilterSection runat="server" Title="Status">
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblID" runat="server" Text="ID Range" />
                <JQ:IntRange runat="server" ID="rngID" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label1" runat="server" Text="Date Range" />
                <JQ:DateRange runat="server" ID="rngDate" />
            </div>
        </div>
    </div>

    <div class="form-group">
        <asp:Label ID="LabelEventType" runat="server" Text="Event Type" />
        <netpay:EnumDropDown runat="server" CssClass="form-control" ViewEnumName="Netpay.CommonTypes.PendingEventType" ID="ddlEventType" />
    </div>


</admin:FilterSection>

