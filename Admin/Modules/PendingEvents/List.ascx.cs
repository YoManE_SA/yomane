﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;

namespace Netpay.Admin.PendingEvents
{
	public partial class List : Controls.TemplateControlBase
	{
		protected override void OnLoad(EventArgs e)
		{
			//TemplatePage.ListButtons.EnableAdd = false;
			rptList.DataBinding += List_DataBinding;
			base.OnLoad(e);
		}

		private void List_DataBinding(object sender, EventArgs e)
		{
			rptList.DataSource = Bll.PendingEvents.Search(TemplatePage.GetFilter<Bll.PendingEvents.SearchFilters>(), rptList);
		}

        protected void RefreshTryCount(object sender, CommandEventArgs e)
        {
            int id = int.Parse(e.CommandArgument.ToString());
            var currentItem = Bll.PendingEvents.Load(id);
            currentItem.UpdateTryCount(3);

            TemplatePage.PageController.ListView.BindAndUpdate();
        }
    }
}