﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.PendingEvents.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Pending Events">
    <Header></Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" ID="rptList" SaveAjaxState="true" DisableRowSelect="true" AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundField HeaderText="ID" DataField="ID" SortExpression="ID" />
                    <asp:BoundField HeaderText="Event Type" DataField="EventType" SortExpression="EventType" />
                    <asp:BoundField HeaderText="Insert Date" DataField="InsertDate" SortExpression="InsertDate" />
                    <asp:BoundField HeaderText="Pass ID" DataField="TransPassID" SortExpression="TransPassID" />
                    <asp:BoundField HeaderText="PreAuth ID" DataField="TransPreAuthID" SortExpression="TransPreAuthID" />
                    <asp:BoundField HeaderText="Pending ID" DataField="TransPendingID" SortExpression="TransPendingID" />
                    <asp:BoundField HeaderText="Fail ID" DataField="TransFailID" SortExpression="TransFailID" />
                    <asp:BoundField HeaderText="Merchant ID" DataField="MerchantID" SortExpression="MerchantID" />
                    <asp:BoundField HeaderText="Customer ID" DataField="CustomerID" SortExpression="CustomerID" />
                    <asp:BoundField HeaderText="Parameters" DataField="Parameters" SortExpression="Parameters" />
                    <asp:TemplateField HeaderText="TryCount">
                        <ItemTemplate>
                            <asp:Button runat="server" CommandArgument='<%# Eval("ID") %>' OnCommand="RefreshTryCount" CssClass="btn btn-default" Text="Refresh" Visible='<%# (byte)Eval("TryCount") == 0 %>' />
                            <asp:Literal runat="server" Text='<%# Eval("TryCount") %>'></asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd="false" />
    </Footer>

</admin:ListSection>
