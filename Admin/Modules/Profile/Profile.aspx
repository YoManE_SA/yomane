﻿<%@ Page Title="Profile" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="Netpay.Admin.Profile.Profile" %>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <netpay:ActionNotify runat="server" ID="acNotify" />
    <div class="row">
        <div class="col-lg-6">
            <admin:PanelSection runat="server" Title="Personal Detalis" Icon="fa-user">
                <Body>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">

                                <div class="col-lg-4">
                                    <img src="http://placehold.it/250x250" class="profile-image" />
                                </div>
                                <div class="col-lg-8">
                                    <div class="profile-image-button">

                                        <ul>
                                            <li>
                                                <asp:HyperLink runat="server" CssClass="btn btn-primary btn-large col-lg-4">Upload Photo</asp:HyperLink></li>
                                            <li>
                                                <asp:HyperLink runat="server" CssClass="btn btn-info btn-large col-lg-4">Edit Photo</asp:HyperLink></li>
                                            <li>
                                                <asp:HyperLink runat="server" CssClass="btn btn-danger btn-large col-lg-4">Delete Photo</asp:HyperLink></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row spacer">
                        <div class="col-lg-12">
                            <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>success!</strong>  Your Profile image has been changed
                            </div>
                            <div class="alert alert-info">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>Error!</strong> The file must to be JPG / PNG over 500kb.
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="form-group">
                        <label for="inputEmail">Full Name</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <span class="fa fa-user"></span>
                            </span>
                            <input type="text" class="form-control" placeholder="Username">
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="inputEmail">Email</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <span class="fa fa-envelope"></span>
                            </span>
                            <input type="text" class="form-control" placeholder="Email">
                        </div>
                    </div>
                </Body>
                <Footer>
                    <div class="pull-right">
                        <asp:Button runat="server" ID="btnSaveDetails" class="btn btn-primary" Text="Save" OnClick="SaveDetails_Click" />
                        <a href="#" class="btn btn-default">Go Back</a>
                    </div>
                </Footer>
            </admin:PanelSection>
        </div>

        <div class="col-lg-6">
            <admin:PanelSection runat="server" Title="Change Password" Icon="fa-key">
                <Body>
                    <div class="form-group">
                        <label for="inputPassword">Old Password</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <span class="fa fa-key"></span>
                            </span>
                            <asp:TextBox runat="server" ID="txtOldPassword" class="form-control" placeholder="Password" />
                        </div>
                    </div>
                    <div class="alert alert-warning">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong>Attention! The password is subject to the following restrictions </strong>
                        <ul>
                            <li>The password length must be at least 8 characters</li>
                            <li>The password must contain only Latin letters (A-Z) and decimal digits (0-9)</li>
                            <li>The password must containt at least 2 decimal digits (0-9)</li>
                            <li>The password must contain at least 2 Latin letters (A-Z)</li>
                            <li>You cannot return back to three last passwords you used</li>

                        </ul>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword">New Password</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <span class="fa fa-key"></span>
                            </span>
                            <asp:TextBox runat="server" ID="txtPassword" class="form-control" placeholder="Password" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword">Confirm Password</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <span class="fa fa-key"></span>
                            </span>
                            <asp:TextBox runat="server" ID="txtPasswordConfirm" class="form-control" placeholder="Confirm Password" />
                        </div>
                    </div>
                </Body>
                <Footer>
                    <div class="pull-right">
                        <asp:Button runat="server" ID="btnSavePassword" class="btn btn-primary" Text="Save" OnClick="SavePassword_Click" />
                        <a href="#" class="btn btn-default">Go Back</a>
                    </div>
                </Footer>
            </admin:PanelSection>


        </div>
    </div>
</asp:Content>
