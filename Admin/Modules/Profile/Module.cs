﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Profile
{
	public class Module : Admin.Module
	{
		public override string Name { get { return "User Profile Page"; } }
		public override decimal Version { get { return 1.0m; } }
		public override string Author { get { return "OBL ltd."; } }
		public override string Description { get { return ""; } }

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
		}

		protected override void OnActivate(EventArgs e)
		{
			base.OnActivate(e);
		}

		protected override void OnDeactivate(EventArgs e)
		{
			base.OnDeactivate(e);
		}
	}
}