﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountBalance.ascx.cs" Inherits="Netpay.Admin.Balance.AccountBalance" %>
<admin:FormPanel ID="fsBalanceStatus" runat="server" Title="Balance" OnCommand="Balance_Command" EnableAdd="<%#IsVisible%>">
     <script type="text/javascript">
         function ValidateBalancePage() {

             if (typeof (Page_ClientValidate) == 'function') {
                 var check = Page_ClientValidate("Balance_AccountBalance");
             }

             if (check) {
                 var button = document.getElementById("BalanceSaveButton");
                 button.removeAttribute("disabled");
                 button.style.opacity = "1.0";
                 button.style.cursor = "pointer";
             }

             else {
                 var button = document.getElementById("BalanceSaveButton");
                 button.disabled = true;
                 button.style.opacity = "0.2";
                 Page_BlockSubmit = false;
             }
         }
       </script>
    <asp:MultiView ID="AccountBalanceMultiView" runat="server" ActiveViewIndex='<%# IsVisible? 0 : 1%>'>
        <asp:View ID="ViewIfVisible" runat="server">
    <NP:PagedRepeater runat="server" ID="rptBalance" PageSize="20">
        <HeaderTemplate>
             <table class="table table-hover table-customize">
                <thead>
                     <th>Currency</th>
                     <th>Current</th>
                     <th>Pending</th>
                     <th>Expected</th>
                </thead>
        </HeaderTemplate>
        <ItemTemplate>
            <tbody>
                 <td><NP:LinkButton runat="server" Text='<%# Eval("CurrencyIso") %>' UseSubmitBehavior="false" OnCommand="Balance_Command" CommandName="ShowHistory" CommandArgument='<%# Eval("CurrencyIso") %>' RegisterAsync="true" /></td>
                 <td><%# ((decimal)Eval("Current")).ToString("0.00") %></td>
                 <td><%# ((decimal)Eval("Pending")).ToString("0.00") %></td>
                 <td><%# ((decimal)Eval("Expected")).ToString("0.00") %></td>
            </tbody>
        </ItemTemplate>
        <FooterTemplate>
              </table>
        </FooterTemplate>
        <EmptyContainer>
            <div class="alert alert-info"><strong>Info!</strong> No record information</div>
        </EmptyContainer>
    </NP:PagedRepeater>
              </asp:View>

                <asp:View ID="ViewIfNotVisible" runat="server">
            <div class="alert alert-info">
                You need to save your details in order to edit this section.
                </div>
        </asp:View>
        </asp:MultiView>
</admin:FormPanel>
<admin:ModalDialog runat="server" ID="mdHistory" Title="Balance History" OnCommand="Balance_Command">
    <Header>
        <asp:Label runat="server" ID="ltCurrency" Style="float: right;" Text='<%# CurrencyIso %>' />
    </Header>
    <Body>
        <admin:AdminList runat="server" ID="rptHistory" PageSize="10" AutoGenerateColumns="false" AllowSorting="false" SortDesc="true" SortKey="InsertDate" IsInListView="false" DisableRowSelect="true">
            <Columns>
                <asp:BoundField HeaderText="InsertDate" DataField="InsertDate" />
                <asp:BoundField HeaderText="Amount" DataField="Amount" />
                <asp:BoundField HeaderText="Total" DataField="Total" />

                <asp:TemplateField HeaderText="Text">
                    <ItemTemplate>
                        <%# (SpecialSourceTypes.Contains(Eval("SourceType").ToString())) ? GetCorrectBalanceDetails((int)Eval("ID")) : Eval("Text") %>
                        <asp:HyperLink Visible='<%# Eval("SourceType").ToString()=="System.Purchase" %>' ID="TransactionLink" runat="server"
                            Text='<%# Eval("SourceType").ToString()=="System.Purchase"?"Go":""%>'
                            NavigateUrl='<%# Eval("ID")!=null && Eval("SourceType").ToString()=="System.Purchase"? GetTransactionUrl((int)Eval("ID")): "" %>'>
                        </asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField HeaderText="IsPending" DataField="IsPending" />
            </Columns>
        </admin:AdminList>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptHistory"></admin:DataButtons>
        <%--<admin:Pager ID="Pager1" runat="server" PagedControlID="rptHistory" />--%>
    </Footer>
</admin:ModalDialog>
<admin:ModalDialog runat="server" ID="mdAddRow" Title="Add Balance">
    <Body>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    Amount:
                    <div class="input-group">
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtAmount" CausesValidation="True" onkeyup="ValidateBalancePage();" />
                        <span class="input-group-addon"><i class="fa fa-money"></i></span>
                    </div>
                    <asp:RequiredFieldValidator ForeColor="Red" ControlToValidate="txtAmount" ID="RequiredFieldValidatortxtAmount" runat="server" ErrorMessage="Insert amount bigger than 0." Display="Dynamic" ValidationGroup="Balance_AccountBalance" />
                    <%--<asp:RangeValidator ForeColor="Red" ControlToValidate="txtAmount" ID="RangeValidatortxtAmount" runat="server" ErrorMessage="Insert amount bigger than 0." MinimumValue="0.001" MaximumValue="<%# Int32.MaxValue %>" Type="Double" Display="Dynamic" ValidationGroup="Balance_AccountBalance" />--%>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    Choose Currency
                    <netpay:CurrencyDropDown runat="server" CssClass="form-control" ID="ddlCurrency" CausesValidation="True" BlankSelectionText="Choose Currency" BlankSelectionValue="0" onchange="ValidateBalancePage();" />
                    <asp:RequiredFieldValidator ForeColor="red" ControlToValidate="ddlCurrency" ID="RequiredFieldValidatorddlCurrency" runat="server" ErrorMessage="Choose currency type." InitialValue="0" Display="Dynamic" ValidationGroup="Balance_AccountBalance" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    Text
                    <asp:TextBox runat="server" ID="txtText" CssClass="form-control" MaxLength="200" onkeyup="ValidateBalancePage();" CausesValidation="True" TextMode="MultiLine" />
                    <asp:RequiredFieldValidator ForeColor="red" ControlToValidate="txtText" ID="RequiredFieldValidatortxtText" runat="server" ErrorMessage="Insert a Pay-For text with minimum 6 characters." Display="Dynamic" ValidationGroup="Balance_AccountBalance" />
                    <asp:RegularExpressionValidator ValidationExpression="^(?:.{6,}|)$" ControlToValidate="txtText" ForeColor="red" ID="RegularExpressionValidatortxtText" runat="server" ErrorMessage="Insert a Pay-For text with minimum 6 characters." ValidationGroup="Balance_AccountBalance" Display="Dynamic" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <ul class="list-group">
                    <li class="list-group-item">
                        <asp:CheckBox runat="server" ID="chkIsPending" Text="Is Pending" />
                    </li>
                </ul>
            </div>
        </div>
    </Body>
    <Footer>
        <asp:Button ID="BalanceSaveButton" runat="server" Text="Save" OnCommand="Balance_Command" CommandName="SaveItem" CssClass="btn btn-cons-short btn-info margin-left-5 + CssFloatRight" ClientIDMode="Static" />
        <%--<admin:DataButtons  ID="BalanceControlDataButtons" runat="server" EnableBack="false" EnableSave="false" />--%>
    </Footer>
</admin:ModalDialog>
