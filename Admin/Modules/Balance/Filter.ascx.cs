﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Balance
{
	public partial class Filter : Controls.TemplateControlBase
	{
		protected override void OnLoad(EventArgs e)
		{
			TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
            base.OnLoad(e);
		}

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
		{
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            bool isBalanceStatus = ddlResults.SelectedValue.ToNullableInt().GetValueOrDefault(apAccount.Value == null ? 1 : 0) == 1;
            dynamic sf;
            if (isBalanceStatus) sf = new Netpay.Bll.Accounts.Balance.BalanceStatus.SearchFilters();
            else sf = new Netpay.Bll.Accounts.Balance.SearchFilters();
            sf.AccountID = apAccount.Value;
            sf.InsertDate = rngDate.Value;
            sf.CurrencyIso = ddlCurrency.SelectedCurrencyIso;
            sf.Amount = rngAmount.Value;
            sf.IsPending = ddlPending.SelectedValue.ToNullableBool();
            sf.OnlyCurrentValues = ddlResults.SelectedValue.ToNullableInt().GetValueOrDefault(sf.AccountID == null ? 1 : 0) == 1;
            TemplatePage.SetFilter(sf);
        }
    }
}