﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Balance.Filter" %>
<admin:FilterSection runat="server" Title="General">
    <div class="form-group">
        <asp:Label ID="lblCustomer" runat="server" Text="Account" />
        <admin:AccountPicker runat="server" ID="apAccount" />
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblInsertDate" runat="server" Text="Date" />
                <JQ:DateRange runat="server" ID="rngDate" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <asp:Label ID="Label1" runat="server" Text="Currency" />
                <netpay:CurrencyDropDown runat="server" CssClass="form-control" ID="ddlCurrency" />
            </div>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="Label2" runat="server" Text="Amount" />
                <JQ:DecimalRange runat="server" ID="rngAmount" />
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <asp:Label runat="server" Text="Status" />
            <div class="form-group">
                <asp:DropDownList runat="server" ID="ddlPending" CssClass="form-control">
                    <asp:ListItem Text="All" Value="" />
                    <asp:ListItem Text="Processed" Value="false" />
                    <asp:ListItem Text="Pending" Value="true" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-lg-6">
            <asp:Label runat="server" Text="Results" />
            <div class="form-group">
                <asp:DropDownList runat="server" ID="ddlResults" CssClass="form-control">
                    <asp:ListItem Text="Account Status" Value="1" />
                    <asp:ListItem Text="Balance Rows" Value="0" />
                </asp:DropDownList>
            </div>
        </div>
    </div>
</admin:FilterSection>
