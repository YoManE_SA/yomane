﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Balance
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu mainMenuItem;
        public override string Name { get { return "Balance"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return ""; } }

        public Module() : base(Bll.Accounts.BalanceModule.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu item
            mainMenuItem = new ApplicationMenu("Balance", "~/Balance/0", "fa-group", 120);

            //Register route
            Application.RegisterRoute("Balance/{id}", "Balance", typeof(Controls.DataTablePage), module:this);

            //Register event
            Application.InitDataTablePage += application_InitTemplatePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {                     
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {                     
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Finance", mainMenuItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            mainMenuItem.Parent.RemoveChild(mainMenuItem);
            base.OnUninstallAdmin(e);
        }

        protected void application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "Balance")
            {
                page.AddControlToList(page.LoadControl("~/Modules/Balance/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Balance/Filter.ascx"));
            }
            else if (page.TemplateName == "Wire" && (page.ViewName == null))
            {
                var acBalance = page.LoadControl("~/Modules/Balance/AccountBalance.ascx") as AccountBalance;
                acBalance.DataBinding += delegate { var wire = acBalance.TemplatePage.GetItemData<Bll.Wires.Wire>(); if (wire != null) acBalance.AccountID = wire.AccountID; };
                page.AddControlToForm("Data", acBalance, this, "", new Controls.SecuredObjectSelector(Bll.Wires.Wire.SecuredObject, Bll.Accounts.Balance.SecuredObject).GetActivObject);
            }
            else if (Accounts.Module.TemplateIsAccountType(page.TemplateName) && 
                        page.TemplateName != "DebitCompany" &&
                        (page.ViewName == null))
            {

                var acBalance = page.LoadControl("~/Modules/Balance/AccountBalance.ascx");
                acBalance.DataBinding += acBalance_DataBinding;
                page.AddControlToForm("Summary", acBalance, this, "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject, Bll.Accounts.Balance.SecuredObject).GetActivObject);
                page.AddControlToForm("Summary", page.LoadControl("~/Modules/Balance/AccountBalanceRequests.ascx"), this, "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject, Bll.Accounts.BalanceRequest.SecuredObject).GetActivObject);
            }
        }

        public void acBalance_DataBinding(object sender, EventArgs e)
        {
            var control = sender as Balance.AccountBalance;
            if ((control.Page as Controls.AccountPage).Account != null && (control.Page as Controls.AccountPage).Account.AccountID > 0)
                control.IsVisible = true;
            else
                control.IsVisible = false;
        }
    }
}