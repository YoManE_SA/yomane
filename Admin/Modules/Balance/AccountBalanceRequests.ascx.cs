﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Modules.Balance
{
    public partial class AccountBalanceRequests : Controls.TemplateControlBase
    {
        public  List<Netpay.Bll.Accounts.BalanceRequest> rptRequestsList;

        //protected override void OnLoad(EventArgs e)
        //{
        //    rptBalanceRequests.DataBinding += delegate { rptBalanceRequests.DataSource = Bll.Accounts.BalanceRequest.Search((TemplatePage as Controls.AccountPage).ItemID.GetValueOrDefault()); };
        //}

        protected override void OnDataBinding(EventArgs e)
        {
            int accountID = (TemplatePage as Controls.AccountPage).ItemID.GetValueOrDefault();
            rptRequestsList = Netpay.Bll.Accounts.BalanceRequest.Search(new Bll.Accounts.BalanceRequest.SearchFilters() { AnyAccountID = accountID }, rptRequests);
            rptRequests.DataSource = rptRequestsList;
            base.OnDataBinding(e);
        }

        //protected void BalanceRequests_Command(object sender, CommandEventArgs e)
        //{

        //}

        public string GetText(List<int> list)
        {
            var sourceaccountid = list[0];
            var targetaccountid = list[1];
            Dictionary<int,Netpay.Bll.Accounts.AccountInfo> SourceAccount = Netpay.Bll.Accounts.Account.GetAccountInfo(new List<int> { sourceaccountid });
            Dictionary<int, Netpay.Bll.Accounts.AccountInfo> TargetAccount = Netpay.Bll.Accounts.Account.GetAccountInfo(new List<int> { targetaccountid });

            return "";
        }

    }
}