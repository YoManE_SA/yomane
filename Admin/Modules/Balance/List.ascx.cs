﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Balance
{
	public partial class List : Controls.TemplateControlBase
	{
        private Dictionary<int, string> accountNames;

        public Bll.Accounts.Balance.SearchFilters filters
        {
            get
            {
                return TemplatePage.GetFilter<Bll.Accounts.Balance.SearchFilters>();
            }
        }

		protected override void OnLoad(EventArgs e)
		{
			rptList.DataBinding += List_DataBinding;
            rptTotals.DataBinding += List_DataBinding;
			base.OnLoad(e);
		}

		private void List_DataBinding(object sender, EventArgs e)
		{
            var filters = TemplatePage.GetFilter<Bll.Accounts.Balance.SearchFilters>();
            if (filters == null) filters = TemplatePage.GetFilter<Bll.Accounts.Balance.BalanceStatus.SearchFilters>();

            if (filters == null || filters.OnlyCurrentValues) {
                rptList.Visible = false;
                rptTotals.Visible = true;
                var list = Netpay.Bll.Accounts.Balance.GetStatus(filters, rptTotals);
                rptTotals.DataSource = list;
                accountNames = Bll.Accounts.Account.GetNames(list.Select(v => v.AccountId).ToList());
            } else {
                rptTotals.Visible = false;
                rptList.Visible = true;
                var list = Netpay.Bll.Accounts.Balance.Search(filters, rptList);
                rptList.DataSource = list;
                accountNames = Bll.Accounts.Account.GetNames(list.Select(v => v.AccountID).ToList());
            }
        }

        protected string GetAccountName(int accountId)
        {
            string ret;
            if (accountNames.TryGetValue(accountId, out ret)) return ret;
            return string.Empty;
        }

        protected string formatAmount(decimal amount, string currencyIso)
        {
            if (currencyIso == "ils")
            {
                currencyIso = "ILS";
            }

            string format = amount.ToString("0.00");
            if (!string.IsNullOrEmpty(currencyIso)) format = new Bll.Money(amount, currencyIso).ToIsoString();
            if (amount < 0) format = "<span style=\"color:red;\">" + format + "</span>";
            return format;
        }

        protected string formatSource(string source)
        {
            var items = source.EmptyIfNull().Split(new char[] { '.' }, 2);
            if (items.Length < 2) return source;
            if (items[0].ToUpper() == "SYSTEM") return "<span style=\"color:blue;\">" + items[1] + "</span>";
            else return string.Format("{1} ({0})", items[0], items[1]);
        }
    }
}