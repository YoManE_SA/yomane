﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Balance.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Balance">
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" ID="rptList" SaveAjaxState="true" DataKeyNames="ID" AutoGenerateColumns="false" BubbleLoadEvent="false" DisableRowSelect="true">
                <Columns>
                    <asp:TemplateField HeaderText="ID" SortExpression="ID">
                        <ItemTemplate>
                            <i class="fa fa-arrow-circle-<%# (decimal)Eval("Total") > 0 ? "right" : "left" %>"></i>
                            <%# Eval("ID") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="AccountName" SortExpression="AccountID">
                        <ItemTemplate>
                            <%# GetAccountName((int)Eval("AccountID")) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="InsertDate" DataField="InsertDate" SortExpression="InsertDate" />
                    <asp:TemplateField HeaderText="SourceType" SortExpression="SourceType" >
                        <ItemTemplate><%# formatSource((string)Eval("SourceType")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="SourceID" DataField="SourceID" SortExpression="SourceID" Visible="false" />
                    <asp:BoundField HeaderText="Text" DataField="Text" />
                    <asp:TemplateField HeaderText="Amount" SortExpression="Amount" >
                        <ItemTemplate><%# formatAmount((decimal)Eval("Amount"), null) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Total" SortExpression="Total" >
                        <ItemTemplate><%# formatAmount((decimal)Eval("Total"), (string)Eval("CurrencyIso")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="IsPending" DataField="IsPending" />
                </Columns>
            </admin:AdminList>

            <admin:AdminList runat="server" ID="rptTotals" SaveAjaxState="true" AutoGenerateColumns="false" BubbleLoadEvent="false" DisableRowSelect="true">
                <Columns>
                    <asp:TemplateField HeaderText="AccountID" SortExpression="AccountID">
                        <ItemTemplate><%# GetAccountName((int)Eval("AccountId")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="CurrencyIso" DataField="CurrencyIso" SortExpression="CurrencyIso" />
                    <asp:BoundField HeaderText="Current" DataField="Current" />
                    <asp:BoundField HeaderText="Pending" DataField="Pending" />
                    <asp:TemplateField HeaderText="Expected" SortExpression="Expected" >
                        <ItemTemplate><%# formatAmount((decimal)Eval("Expected"), (string)Eval("CurrencyIso")) %></ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptTotals" EnableExport="true" Visible='<%# filters == null || filters.OnlyCurrentValues %>'/>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" Visible='<%# filters != null && !filters.OnlyCurrentValues %>'/>
    </Footer>
</admin:ListSection>
