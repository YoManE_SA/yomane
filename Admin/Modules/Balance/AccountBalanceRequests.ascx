﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountBalanceRequests.ascx.cs" Inherits="Netpay.Admin.Modules.Balance.AccountBalanceRequests" %>
<%--<admin:FormPanel ID="fpBalanceRequestsStatus" runat="server" Title="Balance Requests" OnCommand="BalanceRequests_Command"  EnableAdd="true">
    <NP:PagedRepeater runat="server" ID="rptBalanceRequests" PageSize="20">
        <HeaderTemplate>
            <div class="row inner-table-titles">
                <div class="col-lg-3">
                    Currency
                </div>
                <div class="col-lg-3">
                    Amount
                </div>
            </div>
        </HeaderTemplate>
        <ItemTemplate>
            <%--<div class="row" style='<%#Eval("CurrencyISOCode") == BoldCurrency.ToString().ToLower() ? "font-weight:bold; color:black;": "" %>'
                <div class="col-lg-3">
                   <%-- <NP:LinkButton runat="server" Text='<%# Eval("CurrencyISOCode") %>' UseSubmitBehavior="false" OnCommand="Balance_Command" CommandName="ShowHistory" CommandArgument='<%# Eval("CurrencyISOCode") %>' RegisterAsync="true" />
                </div>
                <div class="col-lg-3">
                    <%# ((decimal)Eval("Amount")).ToString("0.00") %>
                </div>
        </ItemTemplate>
        <EmptyContainer>
            <div class="alert alert-info"><strong>Info!</strong> No record information</div>
        </EmptyContainer>
    </NP:PagedRepeater>
</admin:FormPanel>--%>

<admin:FormSections ID="FormSections1" runat="server" Title="Balance Requests">
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex='<%# rptRequestsList.Count > 0 ? 0 : 1  %>'>
        <asp:View runat="server">
            <admin:AdminList runat="server" ID="rptRequests" PageSize="10" AutoGenerateColumns="false" AllowSorting="false" SortDesc="true" SortKey="RequestDate" IsInListView="false">
                <Columns>
                    <asp:BoundField HeaderText="RequestDate" DataField="RequestDate" />
                    <asp:BoundField HeaderText="SourceAccountID" DataField="SourceAccountID" />
                    <asp:BoundField HeaderText="SourceText" DataField="SourceText" />

                    <asp:BoundField HeaderText="IsPush" DataField="IsPush" />
                    <asp:BoundField HeaderText="Amount" DataField="Amount" />
                    <asp:BoundField HeaderText="CurrencyISOCode" DataField="CurrencyISOCode" />

                    <asp:BoundField HeaderText="IsApproved" DataField="IsApproved" />
                    <asp:BoundField HeaderText="ConfirmDate" DataField="ConfirmDate" />

                    <asp:TemplateField HeaderText="From-To">
                        <ItemTemplate>
                            From:<%# GetText(new List<int> {(int)Eval("SourceAccountID"), (int)Eval("TargetAccountID")}) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </admin:AdminList>
        </asp:View>

        <asp:View runat="server">
            <div class="alert alert-info"><strong>Info!</strong> No record information</div>
        </asp:View>
    </asp:MultiView>
</admin:FormSections>
