﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;

namespace Netpay.Admin.Balance
{
    public partial class AccountBalance : Controls.TemplateControlBase
    {
        public string[] SpecialSourceTypes = { "System.Purchase", "System.Transfer" };

        public bool IsVisible { get; set; } = true;
        private int _accountID;
        protected Netpay.CommonTypes.Currency BoldCurrency { get; set; }
        public string CurrencyIso { get; set; }
        public int AccountID
        {
            get
            {
                if (_accountID == 0)
                {
                    if (TemplatePage is Controls.AccountPage) _accountID = (TemplatePage as Controls.AccountPage).ItemID.GetValueOrDefault();
                    else if (TemplatePage.TemplateName == "Wire" && TemplatePage.GetItemData<Bll.Wires.Wire>() != null) _accountID = TemplatePage.GetItemData<Bll.Wires.Wire>().AccountID;
                }
                return _accountID;
            }
            set { _accountID = value; }
        }

        protected override void OnLoad(EventArgs e)
        {
            rptBalance.DataBinding += delegate { rptBalance.DataSource = Bll.Accounts.Balance.GetStatus(AccountID); };
            rptHistory.DataBinding += delegate 
            {
                if (AccountID != 0 && (!string.IsNullOrEmpty(CurrencyIso) || !string.IsNullOrEmpty((string)ViewState["CurrencyIso"])))
                {
                    rptHistory.DataSource = Netpay.Bll.Accounts.Balance.Search(new Netpay.Bll.Accounts.Balance.SearchFilters() { AccountID = this.AccountID, CurrencyIso = string.IsNullOrEmpty(CurrencyIso)? (string)ViewState["CurrencyIso"] : CurrencyIso }, rptHistory);
                }
                
            };
            base.OnLoad(e);
        }

        protected void Balance_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "ShowHistory":
                    ViewState["CurrencyIso"] = (string)e.CommandArgument;
                    CurrencyIso = (string)e.CommandArgument;
                    mdHistory.BindAndShow();
                    break;

                case "AddItem":

                    Page.Validate("Balance_AccountBalance");
                    if (Page.IsValid == false)
                    {
                        BalanceSaveButton.Style.Add("opacity", "0.2");
                        BalanceSaveButton.Style.Add("cursor", "default");
                        BalanceSaveButton.Attributes.Add("disabled", "true");
                    }
                    else
                    {
                        BalanceSaveButton.Style.Add("opacity", "1");
                        BalanceSaveButton.Style.Add("cursor", "pointer");
                        BalanceSaveButton.Attributes.Remove("disabled");
                    }

                    mdAddRow.RegisterShow();
                    break;

                case "SaveItem":
                    if (Page.IsValid)
                    {
                        Bll.Accounts.Balance.Create(AccountID, ddlCurrency.SelectedCurrencyIso, txtAmount.Text.ToNullableDecimal().GetValueOrDefault(), txtText.Text, Bll.Accounts.Balance.SOURCE_ADMIN, null, chkIsPending.Checked);
                    }
                    //clean the input fields in the 'Add-balance' pop up after saving the data
                    ddlCurrency.SelectedIndex = 0;
                    txtAmount.Text = "";
                    txtText.Text = "";

                    mdAddRow.RegisterHide();
                    IsVisible = true;
                    fsBalanceStatus.BindAndUpdate();
                    break;

                case "DialogClose":
                    ViewState["CurrencyIso"] = "";
                    rptHistory.PageCurrent = 0;
                    break;
            }
        }

        public string GetCorrectBalanceDetails(int accountbalanceid)
        {
            if (accountbalanceid <= 0) return "";

            var sf = new Bll.Accounts.Balance.SearchFilters();
            sf.ID.From = accountbalanceid;
            sf.ID.To = accountbalanceid;

            Bll.Accounts.Balance balance = Bll.Accounts.Balance.Search(sf, null).SingleOrDefault();
            if (balance != null)
            {
                if (balance.SourceType == "System.Purchase")
                {
                    if (balance.SourceID != null)
                    {                        
                        var vo = Bll.Transactions.Transaction.GetTranasctionByApprovalNumber(balance.ID.ToString(), TransactionStatus.Captured);
                        if (vo == null)
                        {
                            vo = Bll.Transactions.Transaction.GetTransaction(null, (int)balance.SourceID, TransactionStatus.Captured);
                        }

                        if (vo == null)
                        {
                            return "Transaction not found.";
                        }
                        var merchant = Bll.Merchants.MerchantPublicInfo.Load(vo.MerchantID.GetValueOrDefault());
                        return "Merchant Name:" + merchant.Name + "<br />Transaction ID:" + vo.ID.ToString();
                    }
                }

                else if (balance.SourceType == "System.Transfer")
                {
                    return "";
                }
            }

            return "";
        }

        public string GetTransactionUrl(int accountbalanceid)
        {
            if (accountbalanceid <= 0) return "";

            var sf = new Bll.Accounts.Balance.SearchFilters();
            sf.ID.From = accountbalanceid;
            sf.ID.To = accountbalanceid;

            Bll.Accounts.Balance balance = Bll.Accounts.Balance.Search(sf, null).SingleOrDefault();
            if (balance != null)
            {
                var vo = Bll.Transactions.Transaction.GetTranasctionByApprovalNumber(balance.ID.ToString(), TransactionStatus.Captured);

                if (vo == null)
                {
                    vo = Bll.Transactions.Transaction.GetTransaction(null, (int)balance.SourceID, TransactionStatus.Captured);
                }

                if (vo == null)
                {
                    return "";
                }

                string url = "~/Transactions/0?ctl00.chklStatus$0=Captured&ctl00.rng_ID.From=" + vo.ID.ToString() + "&ctl00.rng_ID.To=" + vo.ID.ToString() + "&Search=1";
                return url;
            }
            else
            {
                return "";
            }
        }
    }
}