﻿using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Notifications
{
    public class Module : Admin.CoreBasedModule
    {
        public override string Name { get { return "Home Page Notifications"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return ""; } }

        public Module() : base(Bll.Notifications.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //No menu item

            //No Route

            //Register event
            Application.InitPage += Application_InitPage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {
            base.OnDeactivate(e);
        }

        private void Application_InitPage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Default;
            if (page != null && page.TemplateName == "Home")
            {
                if (IsActive) //Check for permission also
                {
                    if (
                        Bll.Notifications.Module.SecuredObject.HasPermission(PermissionValue.Read) &&
                        Bll.PendingEvents.SecuredObject.HasPermission(PermissionValue.Read) &&
                        Bll.Process.ChargeAttemptLog.SecuredObject.HasPermission(PermissionValue.Read) &&
                        Bll.Process.ConnectionErrorLog.SecuredObject.HasPermission(PermissionValue.Read) &&
                        Bll.Currency.SecuredObject.HasPermission(PermissionValue.Read) &&
                        Bll.Transactions.Recurring.Series.SecuredObject.HasPermission(PermissionValue.Read) &&
                        Bll.Wires.Wire.SecuredObject.HasPermission(PermissionValue.Read) &&
                        Bll.Transactions.Transaction.SecuredObject.HasPermission(PermissionValue.Read)&&
                        Bll.Transactions.RefundRequest.SecuredObject.HasPermission(PermissionValue.Read)
                        ) 
                    {
                        page.Notifications.Controls.Add(page.LoadControl("~/Modules/Notifications/Notifications.ascx"));
                    }
                    else
                    {
                        var ctl = page.ModuleNotAuthorized;
                        (ctl as Admin.Modules.ModuleNotAuthorized.ModuleNotAuthorized).ModuleName = Name;
                        (ctl as Admin.Modules.ModuleNotAuthorized.ModuleNotAuthorized).Permission = "Read";

                        page.Notifications.Controls.Add(ctl);
                    }
                }
                else
                {
                    var ctl = page.ModuleNotActivated;
                    (ctl as Modules.ModuleNotActivated.ModuleNotActivated).ModuleName = Name;
                    page.Notifications.Controls.Add(ctl);
                }
            }
        }
    }
}