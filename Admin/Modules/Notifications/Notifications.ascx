﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Notifications.ascx.cs" Inherits="Netpay.Admin.Modules.Notifications.Notifications" %>
<asp:Repeater runat="server" ID="AdminRepeater" DataSource='<%#AdminNotificationsList%>' Visible='<%# AdminNotificationsList.Count > 0 %>'>
    <HeaderTemplate>
        <asp:Label runat="server" Text="Admin notifications" Font-Bold="true"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <div class="alert alert-info">
            <%--Blue--%>
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong><%#Eval("TitleText") %></strong><br />
            <%#Eval("MessageText")%><br />
            <asp:HyperLink runat="server" Visible='<%#Eval("NavigateUrl")!=null %>' NavigateUrl='<%#Eval("NavigateUrl")%>' Text='<%# Eval("LinkText") %>'></asp:HyperLink>
        </div>
    </ItemTemplate>
</asp:Repeater><br />


<asp:Repeater runat="server" ID="WarningsRepeater" DataSource='<%#WarningsList%>' Visible='<%# WarningsList.Count > 0 %>'>
    <HeaderTemplate>
        <asp:Label runat="server" Text="Warnings" Font-Bold="true"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <div class="alert alert-danger">
            <%--Red--%>
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong><%#Eval("TitleText") %></strong><br />
            <%#Eval("MessageText")%><br />
            <asp:HyperLink runat="server" Visible='<%#Eval("NavigateUrl")!=null %>' NavigateUrl='<%#Eval("NavigateUrl")%>' Text='<%# Eval("LinkText") %>'></asp:HyperLink>
        </div>
    </ItemTemplate>
</asp:Repeater><br />

<asp:Repeater runat="server" ID="RemindersRepeater" DataSource='<%#RemindersList%>' Visible='<%# RemindersList.Count > 0 %>'>
    <HeaderTemplate>
        <asp:Label runat="server" Text="Reminders" Font-Bold="true"></asp:Label>
    </HeaderTemplate>
    <ItemTemplate>
        <div class="alert alert-warning">
            <%--Red--%>
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong><%#Eval("TitleText") %></strong><br />
            <%#Eval("MessageText")%><br />
            <asp:HyperLink runat="server" Visible='<%#Eval("NavigateUrl")!=null %>' NavigateUrl='<%#Eval("NavigateUrl")%>' Text='<%# Eval("LinkText") %>'></asp:HyperLink>
        </div>
    </ItemTemplate>
</asp:Repeater>

