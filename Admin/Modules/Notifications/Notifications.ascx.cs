﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Web;
using Netpay.Infrastructure.Security;

namespace Netpay.Admin.Modules.Notifications
{
    public partial class Notifications : System.Web.UI.UserControl
    {
        public static List<Controls.Notification> AdminNotificationsList { get; set; }

        public static List<Controls.Notification> WarningsList { get; set; }

        public static List<Controls.Notification> RemindersList { get; set; }


        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void DataBindChildren()
        {
            //Here should be a check for security permission , using Bll.HomePage.Notifications.SecuredObject.
            
            if (
                Bll.Notifications.Module.SecuredObject.HasPermission(PermissionValue.Read) &&
                Bll.PendingEvents.SecuredObject.HasPermission(PermissionValue.Read) &&
                Bll.Process.ChargeAttemptLog.SecuredObject.HasPermission(PermissionValue.Read) &&
                Bll.Process.ConnectionErrorLog.SecuredObject.HasPermission(PermissionValue.Read) &&
                Bll.Currency.SecuredObject.HasPermission(PermissionValue.Read) &&
                Bll.Transactions.Recurring.Series.SecuredObject.HasPermission(PermissionValue.Read) &&
                Bll.Wires.Wire.SecuredObject.HasPermission(PermissionValue.Read) &&
                Bll.Transactions.Transaction.SecuredObject.HasPermission(PermissionValue.Read) &&
                Bll.Transactions.RefundRequest.SecuredObject.HasPermission(PermissionValue.Read)
                )
            {
                //Get admin notifications data
                AdminNotificationsList = new List<Controls.Notification>();
                List<Bll.Content.Bulletin> AdminNotificationsBulletin = Bll.Content.Bulletin.GetAdminNotifications();
                if (AdminNotificationsBulletin.Count > 0)
                {
                    foreach(var notif in AdminNotificationsBulletin)
                    {
                        AdminNotificationsList.Add(new Controls.Notification() { TitleText = notif.TypeFriendlyName, MessageText = notif.Text });
                    }
                }

                //Get the warnings data
                WarningsList = new List<Controls.Notification>();

                // Get failed periodic fees messages.
                var adminFeesNotificationsBulletin = Bll.Content.Bulletin.GetAdminFeesNotifications();
                if (adminFeesNotificationsBulletin.Count > 0)
                {
                    var ctlNotification = new Controls.Notification() { TitleText = "Failed periodic fees" };
                    foreach(var notif in adminFeesNotificationsBulletin)
                    {
                        ctlNotification.MessageText += notif.Text + Environment.NewLine;
                    }
                    WarningsList.Add(ctlNotification);
                }

                //Get unprocessed pending events
                int unProcessedPendingEvents = Bll.PendingEvents.GetUnProcessedPendingEvents();
                if (unProcessedPendingEvents > 0) WarningsList.Add(new Controls.Notification() { TitleText = "Unprocessed Pending Events", MessageText = string.Format("Pending Events table contains <b>{0}</b> events, that should already be processed.", unProcessedPendingEvents) });

                //Get charge attempts without transactions
                var chargeAttemptSf = new Bll.Process.ChargeAttemptLog.SearchFilters();
                chargeAttemptSf.Date = new Range<DateTime?>() { From = DateTime.Now.AddDays(-3), To = DateTime.Now.AddMinutes(-5) };
                chargeAttemptSf.WithoutTransaction = true;
                //List<Bll.Process.ChargeAttemptLog> chargeAttemptsList = Bll.Process.ChargeAttemptLog.Search(chargeAttemptSf, null);
                Bll.Process.ChargeAttemptLog.ChargeAttemptsWithoutTransactions chargeAttemptsData = Bll.Process.ChargeAttemptLog.GetChargeAttemptsWithoutTransaction(chargeAttemptSf);

                if (chargeAttemptsData.Count > 0)
                {
                    if (chargeAttemptsData.Count == 1)
                    {
                        //Bll.Process.ChargeAttemptLog chargeAttempt = chargeAttemptsList.First();
                        var notification = new Controls.Notification()
                        {
                            TitleText = "Uncompleted Charge Attempts",
                            MessageText = string.Format("One charge attempt was not completed in the last 3 days <br/>ID: {0} , Date:{1}", chargeAttemptsData.FirstID.Value, chargeAttemptsData.FirstDate.Value),
                            LinkText = "See Log",
                            NavigateUrl = "~/Logs/ChargeAttemptsLog?Search=1&ctl00.SpecificChargeAttemptLogID=" + chargeAttemptsData.FirstID.Value
                        };
                        WarningsList.Add(notification);
                    }
                    else
                    {
                        var notification = new Controls.Notification()
                        {
                            TitleText = "Uncompleted Charge Attempts",
                            MessageText = string.Format("<b>{0}</b> charge attempts were not completed in the last 3 days <br/>First Attempt ID: {1} , Date:{2} <br/> Last Attempt ID:{3} , Date:{4}", chargeAttemptsData.Count, chargeAttemptsData.FirstID.Value, chargeAttemptsData.FirstDate.Value, chargeAttemptsData.LastID.Value, chargeAttemptsData.LastDate.Value),
                            LinkText = "See Log",
                            NavigateUrl = "~/Logs/ChargeAttemptsLog?ChargeAttemptsWithoutTransaction"
                        };
                        WarningsList.Add(notification);
                    }
                }

                //Get connection error logs from last 6 hours
                int lastSixHoursCount = Bll.Process.ConnectionErrorLog.GetLastSixHoursErrorsCount();
                int lastThreeHoursCount = Bll.Process.ConnectionErrorLog.GetLastThreeHoursErrorsCount();
                int lastHourCount = Bll.Process.ConnectionErrorLog.GetLastHourErrorsCount();

                string sixHoursMsg = lastSixHoursCount > 0 ? string.Format("There are {0} connection failures in last 6 hours.", lastSixHoursCount) : "";
                string threeHoursMsg = lastThreeHoursCount > 0 ? string.Format("<br/>There are {0} connection failures in last 3 hours.", lastThreeHoursCount) : "";
                string oneHourMsg = lastHourCount > 0 ? string.Format("<br/>There are {0} connection failures in last 6 hours.", lastHourCount) : "";

                if (lastSixHoursCount > 0)
                {
                    string lastConnectionMsg = lastHourCount > 0 ? string.Format("<br/>The last connection error occoured at {0}", Bll.Process.ConnectionErrorLog.GetLastErrorTime().ToShortTimeString()) : "";
                    var notification = new Controls.Notification()
                    {
                        TitleText = "Connection Failures",
                        MessageText = sixHoursMsg + threeHoursMsg + oneHourMsg + lastConnectionMsg,
                        LinkText = "See Log",
                        NavigateUrl = "~/Logs/ConnectionErrorLog?LastSixHours"
                    };
                    WarningsList.Add(notification);
                }

                //Get connection errors without refund attempt
                var conErrorSf = new Bll.Process.ConnectionErrorLog.SearchFilters();
                conErrorSf.Date.From = DateTime.Now.AddDays(-3);
                conErrorSf.Date.To = DateTime.Now.AddHours(-8);
                conErrorSf.AttemptedRefund = false;

                int errorsWithoutRefunds = Bll.Process.ConnectionErrorLog.Count(conErrorSf);
                if (errorsWithoutRefunds > 0)
                {
                    var notification = new Controls.Notification()
                    {
                        TitleText = "Unhandle refunds for connection failures",
                        MessageText = string.Format("There are <b>{0}</b> connection failure without a refund attempt.", errorsWithoutRefunds),
                        LinkText = "See Log",
                        NavigateUrl = "~/Logs/ConnectionErrorLog?ErrorsWithoutRefund"
                    };
                    WarningsList.Add(notification);
                }

                //Get exchange rate
                var currency = Bll.Currency.Get(1);
                int nDateDiff = (int)System.Math.Round(((TimeSpan)(DateTime.Now - currency.RateRequestDate.Value)).TotalDays);
                int nBankDateDiff = ((TimeSpan)(DateTime.Now - currency.RateValueDate)).Days;
                if (nDateDiff > 1 || nBankDateDiff > 3)
                {
                    var notification = new Controls.Notification()
                    {
                        TitleText = "Currency Exchange Rates",
                        MessageText = string.Format("Exchange rates were updated <b>{0}</b> days ago, on {1}.<br/>Bank last update was on {2}.", nDateDiff, currency.RateRequestDate.Value.ToShortDateString(), currency.RateValueDate.Value.ToShortDateString())
                    };
                    WarningsList.Add(notification);
                }
                else
                {
                    var notification = new Controls.Notification()
                    {
                        TitleText = "Currency Exchange Rates",
                        MessageText = "No data on currency exchange rates could be retrieved."
                    };
                    WarningsList.Add(notification);
                }


                //Get failed reccuring charges
                int unProcessedReccuring = Bll.Transactions.Recurring.Series.GetUnprocessedRecurringCharges();
                if (unProcessedReccuring > 0)
                {
                    var notification = new Controls.Notification()
                    {
                        TitleText = "Recurring Transactions",
                        MessageText = string.Format("There are <b>{0}</b> unprocessed charges.", unProcessedReccuring)
                    };
                    WarningsList.Add(notification);
                }

                //---------------------------------------------

                //Get reminders data
                RemindersList = new List<Controls.Notification>();

                //Merchants Bank Transfers - Ask Eran if create a new Bll entity for tblWireMoney
                var wiresSf = new Bll.Wires.Wire.SearchFilters();
                wiresSf.ApprovalLevel1 = true;
                wiresSf.ApprovalLevel2 = true;
                wiresSf.IsShow = true;
                wiresSf.Status = new List<Bll.Wires.Wire.WireStatus> { Bll.Wires.Wire.WireStatus.Pending };
                int waitingForTransferWires = Bll.Wires.Wire.Count(wiresSf);
                if (waitingForTransferWires > 0)
                {
                    var notification = new Controls.Notification
                    {
                        TitleText = "Merchants - Bank Transfers",
                        MessageText = String.Format("There are <b>{0}</b> transfers approved and waiting for transfer.", waitingForTransferWires),
                        LinkText = "Go To Wires",
                        NavigateUrl = "~/Wires/0?ApprovedAndWaitingForTransfer"
                    };
                    RemindersList.Add(notification);
                }

                //Merchants - Refund requests
                var sf = new Bll.Transactions.RefundRequest.SearchFilters();
                sf.Status = new List<Bll.Transactions.RefundRequest.RequestStatus>() { Bll.Transactions.RefundRequest.RequestStatus.Pending };
                int refundRequestsCount = Bll.Transactions.RefundRequest.Count(sf);

                if (refundRequestsCount > 0)
                {
                    var lastRefundRequest = Bll.Transactions.RefundRequest.Max().Refund;
                    string sDate = lastRefundRequest.RefundAskDate.ToShortDateString();
                    int nDiff = (int)System.Math.Round(((TimeSpan)(DateTime.Now - lastRefundRequest.RefundAskDate)).TotalDays);

                    var notification = new Controls.Notification()
                    {
                        TitleText = "Merchants - Refund Requests",
                        MessageText = string.Format("There are <b>{0}</b> unhandled refund requests.<br/> The last was on {1} {2}", refundRequestsCount, sDate, nDiff > 0 ? "(" + nDiff.ToString() + " days ago)" : "(today)")
                    };
                    RemindersList.Add(notification);
                }
            }
            else
            {
                //prevent null values.
                WarningsList = new List<Admin.Controls.Notification>();
                RemindersList = new List<Admin.Controls.Notification>();
            }
            
            base.DataBindChildren();
        }
    }
}