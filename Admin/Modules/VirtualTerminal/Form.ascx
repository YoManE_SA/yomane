﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Form.ascx.cs" Inherits="Netpay.Admin.VirtualTerminal.Form" %>
<admin:FormSections runat="server" ID="MainFormSection" SurroundOnly="true">
    <div class="panel panel-default">
        <div class="panel-heading">Virtual Terminal</div>
        <div class="panel-body">

            <div class="col-md-12">
                <admin:FormSections runat="server" Title="Select Merchant">
                    <div class="row">
                        <div class="col-lg-4">
                            Merchant
                       <asp:UpdatePanel runat="server" ID="UpAccountPicker" UpdateMode="Conditional">
                           <ContentTemplate>
                               <admin:AccountPicker
                                   ID="apMerchant" runat="server" UseTargetID="false" LimitToType="Merchant" />
                           </ContentTemplate>
                       </asp:UpdatePanel>
                        </div>
                        <div class="col-lg-4">
                            Use Storage Card
            <div class="form-group">
                <asp:TextBox runat="server" Class="form-control" />

            </div>
                        </div>
                        <div class="col-lg-4">
                            <br />
                            <asp:Button runat="server" Text="Load" CssClass="btn btn-primary" />
                        </div>
                    </div>
                </admin:FormSections>
                <admin:FormSections runat="server" Title="Credit card details">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                Credit card number
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtCardNumber" />
                            </div>
                            <div class="form-group">
                                Card Expiration
                            <div class="row">
                                <div class="col-md-6">
                                    <netpay:MonthDropDown runat="server" ID="txtExpMonth" CssClass="form-control" />
                                </div>
                                <div class="col-md-6">
                                    <netpay:NumericDropDown runat="server" ID="txtExpYear" MinValue="2010" MaxValue="2030" CssClass="form-control" />
                                </div>
                            </div>


                            </div>
                            <div class="form-group">
                                Card Verification Code*
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtCvv" />
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                Cardholder Name
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtPayerName" />
                            </div>
                            <div class="form-group">
                                Cardholder Phone
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtPayerPhone" />
                            </div>
                            <div class="form-group">
                                Cardholder Email
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtPayerEmail" />
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                IP Address (needed for risk management)
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtIPAddress" />
                            </div>
                            <div class="form-group">
                                Cardholder ID
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtPayerPersonalID" />
                            </div>
                        </div>
                    </div>
                </admin:FormSections>
                <admin:FormSections runat="server" Title="Transaction details">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                Transaction type:
                                    <asp:RadioButtonList CssClass="TransactionList" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" ID="rblTransType">
                                        <asp:ListItem>Regular</asp:ListItem>
                                        <asp:ListItem>Authorisation</asp:ListItem>
                                        <asp:ListItem>Capture</asp:ListItem>
                                        <asp:ListItem>Confirmed by phone</asp:ListItem>
                                    </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                Amount 
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtAmount" />
                            </div>
                            <div class="form-group">
                                Credit Type
                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlCreditType" />
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                Currency conversion fee may be added. 
                                    <netpay:CurrencyDropDown runat="server" CssClass="form-control" ID="ddlCurrency" />
                            </div>
                            <div class="form-group">
                                Installments
                                    <netpay:NumericDropDown runat="server" CssClass="form-control" ID="ddlInstallments" MinValue="1" MaxValue="36" />
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                Confirmation No.
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtConfirmation" />
                            </div>
                        </div>
                    </div>
                </admin:FormSections>
                <admin:FormSections runat="server" Title="Recurring Transaction">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <asp:CheckBox runat="server" />
                                Create recurring series based on this transaction 
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            Charge every
                                <asp:TextBox runat="server" CssClass="form-control" ID="txtRecurringDuration" />
                        </div>
                        <div class="col-lg-3">
                            Month
                                <asp:TextBox runat="server" CssClass="form-control" ID="txtRecurringMonth" />
                        </div>
                        <div class="col-lg-3">
                            Overall number of charges
                                <asp:TextBox runat="server" CssClass="form-control" ID="txtRecurringCharges" />
                        </div>
                    </div>
                </admin:FormSections>
                <admin:FormSections runat="server" Title="Comment">
                    <div class="row">
                        <div class="col-lg-12">
                            Comment
            <asp:TextBox runat="server" TextMode="MultiLine" CssClass="form-control" ID="txtComment" />
                        </div>

                    </div>
                </admin:FormSections>
            </div>

        </div>
        <div class="panel-footer clearfix text-right">
            <asp:Button runat="server" CssClass="btn btn-primary" Text="Process" OnClick="Process_Click" />
        </div>
    </div>
</admin:FormSections>

<netpay:ModalDialog Title="Process Result" runat="server" ID="mdProcess">
    <Body>
        <netpay:ActionNotify runat="server" ID="acTransProcess" />
    </Body>
</netpay:ModalDialog>
