﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Admin.VirtualTerminal
{
    public partial class Form : System.Web.UI.UserControl
    {
        protected override void OnDataBinding(EventArgs e)
        {
            if (Page.Request.QueryString["MerchantID"] != null)
            {
                var merchant = Bll.Merchants.Merchant.Load(int.Parse(Page.Request.QueryString["MerchantID"]));

                if (merchant != null)
                {
                    (apMerchant.FindControl("hdID") as HiddenField).Value = merchant.AccountID.ToString();
                    (apMerchant.FindControl("txtSearch") as TextBox).Text = merchant.AccountName;
                }

                //apMerchant.DataBind();
                UpAccountPicker.DataBind();
                UpAccountPicker.Update();
            }
            base.OnDataBinding(e);
        }

        protected void Process_Click(object sender, EventArgs e)
        {
            //Check for permission of adding transaction
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Transactions.Transaction.SecuredObject, PermissionValue.Add);

            var trans = new Bll.Transactions.Transaction();
            if (apMerchant.Value.HasValue)
            {
                var merchant = Bll.Merchants.Merchant.LoadByAccountId(apMerchant.Value.Value);
                if (merchant != null)
                    trans.MerchantID = merchant.ID;
            }
            trans.PayerData.FullName = txtPayerName.Text;
            trans.PayerData.EmailAddress = txtPayerEmail.Text;
            trans.PayerData.PhoneNumber = txtPayerPhone.Text;
            trans.PayerData.PersonalNumber = txtPayerPersonalID.Text;

            trans.PaymentData.MethodInstance.PaymentMethodId = CommonTypes.PaymentMethodEnum.CC_MIN;
            var creditCard = trans.PaymentData.MethodInstance as Bll.PaymentMethods.CreditCard;
            creditCard.CardNumber = txtCardNumber.Text;
            creditCard.SetExpirationMonth(txtExpYear.Value.ToNullableInt().GetValueOrDefault(), txtExpMonth.Value.ToNullableInt().GetValueOrDefault());
            creditCard.Cvv = txtCvv.Text;

            trans.Amount = txtAmount.Text.ToNullableDecimal().GetValueOrDefault();
            trans.CurrencyIsoCode = ddlCurrency.SelectedCurrencyIso;
            trans.Installments = (byte)ddlInstallments.Value.ToNullableInt().GetValueOrDefault();
            trans.ApprovalNumber = txtConfirmation.Text;
            trans.Comment = txtComment.Text;

            try
            {
                var values = trans.GetProcessParamters();
                values = Bll.Transactions.Transaction.ProcessTransaction(CommonTypes.PaymentMethodGroupEnum.CreditCard, values);

                var retId = values["TransID"].ToNullableInt();
                if (values["Reply"] == "000")
                {
                    acTransProcess.SetSuccess(string.Format("Approved TransactionID: {0}", retId));
                }
                else if (values["Reply"] == "001")
                {
                    acTransProcess.SetMessage(string.Format("Pending TransactionID: {0}", retId), true);
                }
                else
                {
                    acTransProcess.SetMessage(string.Format("Declined TransactionID: {0}", retId), true);
                }
            }
            catch (Exception ex)
            {
                acTransProcess.SetException(ex);
            }
                        
            mdProcess.RegisterShow();
        }
    }
}