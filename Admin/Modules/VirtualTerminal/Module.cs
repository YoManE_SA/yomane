﻿using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.VirtualTerminal
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu mainMenuItem;

        public override string Name { get { return "VirtalTerminal"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return "Admin Payment form"; } }

        public Module() : base(Bll.Merchants.VirtualTerminal.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu
            mainMenuItem = new ApplicationMenu("Virtual Terminal", "~/VirtualTerminal/0", "fa-credit-card", 7);

            //Register route
            Application.RegisterRoute("VirtualTerminal/{id}", "VirtualTerminal", typeof(Controls.Page), module: this);

            //Register event
            Application.InitPage += Application_InitPage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Merchants", mainMenuItem);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            mainMenuItem.Parent.RemoveChild(mainMenuItem);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitPage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.Page;
            if (page.TemplateName == "VirtualTerminal")
            {
                if (IsActive)
                {
                    if (Bll.Merchants.VirtualTerminal.Module.SecuredObject.HasPermission(PermissionValue.Read))
                    {
                        if (!Bll.Merchants.VirtualTerminal.Module.SecuredObject.HasPermission(PermissionValue.Edit))
                        {
                            var control = page.LoadControl("~/Modules/VirtualTerminal/Form.ascx");
                            Admin.Controls.DataTablePage.DisableAllChidControls(control);
                            page.ContentContainer.Controls.Add(control);
                        }
                        else
                        {
                            page.ContentContainer.Controls.Add(page.LoadControl("~/Modules/VirtualTerminal/Form.ascx"));
                        }
                    }
                    else
                    {
                        page.ContentContainer.Controls.Add(page.ModuleNotAuthorizedControl);
                    }
                }
                else
                {
                    var ctl = page.ModuleNotActivatedControl;
                    (ctl as Modules.ModuleNotActivated.ModuleNotActivated).ModuleName = Name;
                    page.ContentContainer.Controls.Add(ctl);
                }
            }
        }
    }
}