﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RulesAndRegulations.ascx.cs" Inherits="Netpay.Admin.Modules.Hosted.RulesAndRegulations" %>
<admin:FormSections Flexible="false" runat="server" Title="Rules And Regulations">
    <asp:MultiView runat="server" ActiveViewIndex='<%# ((TemplatePage.Page is AccountPage) && TemplatePage.ItemID!=null && TemplatePage.ItemID!=0) ? 0 : 1 %>'>
        <asp:View runat="server">
            <JQ:HtmlEdit runat="server" ID="HtmlRulesAndRegulations">
            </JQ:HtmlEdit>
        </asp:View>
        <asp:View runat="server">
            <div class="alert alert-info">
                You need to save your details in order to edit this section.
            </div>
        </asp:View>
    </asp:MultiView>
</admin:FormSections>
