﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Admin.Modules.Hosted.PaymentPageLog
{
    public class Module : Admin.CoreBasedModule
    {
        private ApplicationMenu menuPaymentPageLog;
        public override string Name { get { return "PaymentPageLog"; } }
        public override decimal Version { get { return 1.0m; } }
        public override string Author { get { return "OBL ltd."; } }
        public override string Description { get { return ""; } }

        public Module() : base(Bll.Merchants.Hosted.PaymentPageLogs.Module.Current) { }

        protected override void OnInit(EventArgs e)
        {
            //Create menu
            menuPaymentPageLog = new ApplicationMenu("Payment Page", "~/Logs/PaymentPageLog", null, 110);

            //Register route
            Application.RegisterRoute("Logs/PaymentPageLog", "PaymentPageLog", typeof(Controls.DataTablePage), module:this);

            //Register event
            Application.InitDataTablePage += Application_InitTemplatePage;

            base.OnInit(e);
        }

        protected override void OnActivate(EventArgs e)
        {               
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {               
            base.OnDeactivate(e);
        }

        protected override void OnInstallAdmin(EventArgs e)
        {
            Application.AddMenuItem("Logs", menuPaymentPageLog);
            base.OnInstallAdmin(e);
        }

        protected override void OnUninstallAdmin(EventArgs e)
        {
            menuPaymentPageLog.Parent.RemoveChild(menuPaymentPageLog);
            base.OnUninstallAdmin(e);
        }

        protected void Application_InitTemplatePage(object sender, EventArgs e)
        {
            if (!CoreModule.IsInstalled)
                return;

            var page = sender as Controls.DataTablePage;
            if (page.TemplateName == "PaymentPageLog")
            {
                page.PageController.LoadItem += PageController_PaymentPageLog_LoadItem;
                page.AddControlToList(page.LoadControl("~/Modules/Hosted/PaymentPageLog/List.ascx"));
                page.AddControlToFilter("Data", page.LoadControl("~/Modules/Hosted/PaymentPageLog/Filter.ascx"));
                page.AddControlToForm("Data", page.LoadControl("~/Modules/Hosted/PaymentPageLog/Data.ascx"), this, "", Bll.Merchants.Hosted.PaymentPageLog.SecuredObject);
            }
        }

        void PageController_PaymentPageLog_LoadItem(object sender, EventArgs e)
        {
            var page = (sender as System.Web.UI.Control).Page as Controls.DataTablePage;
            page.ItemID = page.PageController.DataKey["ID"].ToNullableInt();
            var item = Netpay.Bll.Merchants.Hosted.PaymentPageLog.Load(page.ItemID.GetValueOrDefault());
            if (page.ItemID != null) page.SetItemData(item);
            page.FormButtons.EnableSave = false;
            page.FormButtons.EnableDelete = false;
        }
    }
}