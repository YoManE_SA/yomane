﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Infrastructure;


namespace Netpay.Admin.Modules.Hosted.PaymentPageLog
{
    public partial class Data : Controls.TemplateControlBase
    {
        protected Bll.Merchants.Hosted.PaymentPageLog Item { get { return TemplatePage.GetItemData<Bll.Merchants.Hosted.PaymentPageLog>(); } set { TemplatePage.SetItemData(value); } }
        protected List<Bll.Merchants.Hosted.PaymentPageLog.TransactionLog> TransLogList { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            if (Item == null) Item = new Bll.Merchants.Hosted.PaymentPageLog();
            TransLogList = Bll.Merchants.Hosted.PaymentPageLog.TransactionLog.Search(
                new Bll.Merchants.Hosted.PaymentPageLog.TransactionLog.SearchFilters() { PaymentPageLogId = Item.ID } , new SortAndPage(0, 100, "LogPaymentPageTrans_id", false));
            base.DataBindChildren();
        }

        protected string HPPUrlForItem
        {
            get
            {
                List<string> parameters = new List<string>();
                if (Item.RequestForm != null)
                    parameters.Add(Item.RequestForm);
                if (Item.QueryString != null)
                    parameters.Add(Item.QueryString);

                return Domain.Current.ProcessV2Url + "hosted/?" + String.Join("&", parameters);
            }
        }

        protected void OnCommand(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "ShowRequestResponse":
                    var ID = e.CommandArgument.ToNullableInt().GetValueOrDefault();
                    ShowStrings(ID);
                    break;
            }
        }

        protected void ShowStrings(int id)
        {
            var list = Bll.Merchants.Hosted.PaymentPageLog.TransactionLog.Search(
                new Bll.Merchants.Hosted.PaymentPageLog.TransactionLog.SearchFilters() { ID = Range<int?>.FromValue(id) }, new SortAndPage(0, 100, "LogPaymentPageTrans_id", false));

            rptStringList.DataSource = list;
            rptStringList.DataBind();

            dlgShowStrings.BindAndShow();
            upStrings.Update();
        }

    }
}
