﻿using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Hosted.PaymentPageLog
{
    public partial class Filter : Controls.TemplateControlBase
    {
        public int number;
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.Search += PageController_Search;
            UpdateSearchFilter();
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            // Call update search so that search filter will be updated on paging
            UpdateSearchFilter();

            base.DataBindChildren();
        }

        private void PageController_Search(object sender, EventArgs e)
        {
            UpdateSearchFilter();
        }

        private void UpdateSearchFilter()
        {
            var sf = new Bll.Merchants.Hosted.PaymentPageLog.SearchFilters();
            if (apAccount.Value.HasValue)
            {
                var merchant = Bll.Merchants.Merchant.LoadByAccountId(apAccount.Value.Value);
                if (merchant != null)
                    sf.MerchantId = new Range<int?>(merchant.ID);
            }

            sf.LogIdRange = rngID.Value;
            sf.DateRange = rngDate.Value;
            sf.ReplyCode = txtReplyCode.Text.NullIfEmpty();
            sf.BigOrSmall = ddlBigOrSmall.Value.ToNullableEnum<Netpay.Bll.Merchants.Hosted.EBigOrSmall>();
            sf.IsSuccess = ddlStatus.BoolValue;

            int seconds;
            bool tryParse = int.TryParse(iProcessDuration.Text, out seconds);
            if (tryParse) sf.Duration = TimeSpan.FromSeconds(seconds);

            TemplatePage.SetFilter(sf);
        }

        protected void CheckEnumOnServer(object source, ServerValidateEventArgs args)
        {
            if (iProcessDuration.Text == "" || !int.TryParse(iProcessDuration.Text, out number))
            {
                args.IsValid = true;
            }
            else
            {
                if (ddlBigOrSmall.Value != "-1")
                {
                    args.IsValid = true;
                }
                else
                {
                    args.IsValid = false;
                }
            }
        }
    }
}