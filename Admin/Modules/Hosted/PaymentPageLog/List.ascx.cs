﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Hosted.PaymentPageLog
{
	public partial class List : Controls.TemplateControlBase
	{
        protected Dictionary<string, string> m_merchantNames;

        protected override void OnLoad(EventArgs e)
		{
			rptList.DataBinding += List_DataBinding;
			base.OnLoad(e);
		}

        private void List_DataBinding(object sender, EventArgs e)
        {
            var list = Bll.Merchants.Hosted.PaymentPageLog.Search(TemplatePage.GetFilter<Bll.Merchants.Hosted.PaymentPageLog.SearchFilters>(), rptList);
            if (list != null)
            {
                m_merchantNames = Bll.Accounts.Account.GetMerchantNamesByAccountNumbers((list.Where(i => i.MerchantNumber != null).Select(i => i.MerchantNumber).ToList()));
                rptList.DataSource = list;
            }
        }

        protected string GetMerchantName(string merchantnumber)
        {
            if (merchantnumber == null)
                return null;
            string ret;
            if (!m_merchantNames.TryGetValue(merchantnumber, out ret)) return null;
            return ret;
        }
    }
}