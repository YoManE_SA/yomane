﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Data.ascx.cs" Inherits="Netpay.Admin.Modules.Hosted.PaymentPageLog.Data" %>
<admin:FormSections runat="server" Title="Details">
    <asp:HiddenField runat="server" ID="hfItemID" Value='<%# Item.ID %>' />

    <!-- QueryString -->
    <asp:Repeater ID="rptQueryStringItems" runat="server" DataSource="<%# (Item != null && Item.QueryString != null ) ? Item.QueryString.Split(new char[] { '&' }) : new string[] { } %>">
        <HeaderTemplate>
            <div class="row">
                <div class="col-lg-12">
                    <h4>Query String</h4>
                    <ul class="list-group">
        </HeaderTemplate>
        <ItemTemplate>
            <span class="BreakRow">
                <li class="list-group-item"><b><%# !((string)Container.DataItem).Contains('=') ? "" : ((string)Container.DataItem).Substring(0, ((string)Container.DataItem).IndexOf('=')) %></b><%# !((string)Container.DataItem).Contains('=') ? "" : ((string)Container.DataItem).Substring(((string)Container.DataItem).IndexOf('=')) %></li>
            </span>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
                </div>
             </div>
            <hr />
        </FooterTemplate>
    </asp:Repeater>
    <!-- SessionContents -->
    <asp:Repeater ID="rptSessionContents" runat="server" DataSource="<%# (Item != null && Item.SessionContents != null ) ? Item.SessionContents.Split(new char[] { '&' }) : new string[] { } %>">
        <HeaderTemplate>
            <div class="row">
                <div class="col-lg-12">
                    <h4>Session Contents</h4>
                    <ul class="list-group">
        </HeaderTemplate>
        <ItemTemplate>
            <li class="list-group-item"><b><%# !((string)Container.DataItem).Contains('=') ? "" : ((string)Container.DataItem).Substring(0, ((string)Container.DataItem).IndexOf('=')) %></b><%# !((string)Container.DataItem).Contains('=') ? "" : ((string)Container.DataItem).Substring(((string)Container.DataItem).IndexOf('=')) %></li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
                </div>
              </div>
             <hr />
        </FooterTemplate>
    </asp:Repeater>
    <!--  Http Prefer -->
    <asp:Repeater runat="server" DataSource="<%# (Item != null && Item.HttpReferer != null ) ? Item.HttpReferer.Split(new char[] { '&' }) : new string[] { } %>">
        <HeaderTemplate>
            <div class="row">
                <div class="col-lg-12">
                    <h4>Http Prefer</h4>
                    <ul class="list-group">
        </HeaderTemplate>
        <ItemTemplate>
            <li class="list-group-item"><b><%# !((string)Container.DataItem).Contains('=') ? "" : ((string)Container.DataItem).Substring(0, ((string)Container.DataItem).IndexOf('=')) %></b><%# !((string)Container.DataItem).Contains('=') ? "" : ((string)Container.DataItem).Substring(((string)Container.DataItem).IndexOf('=')) %></li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
              </div>
             </div>
            <hr />
        </FooterTemplate>
    </asp:Repeater>

    <div class="row">
        <div class="col-lg-12">
            <h4>General</h4>
            <ul class="list-group">
                <li class="list-group-item"><b>Http Host:</b>  <%# Item.HttpHost %></li>
                <li class="list-group-item"><b>Request Method:</b> <%# Item.RequestMethod %></li>
                <li class="list-group-item"><b>Local Address:</b> <%# Item.LocalIPAddress %></li>
                <li class="list-group-item"><b>Open HPP with this parameters:</b> <a href="<%# HPPUrlForItem %>" target="_blank">Click to open</a> </li>
            </ul>
        </div>
    </div>

</admin:FormSections>
<admin:FormSections runat="server" Title="Process History">
    <div class="col-md-12">

        <asp:MultiView runat="server" ID="mvStatus" ActiveViewIndex='<%# TransLogList.Count()>0 ? 0 : 1 %>'>
            <asp:View runat="server">
                <div class="row inner-table-titles">
                    <div class="col-lg-1">
                        ID
                    </div>
                    <div class="col-lg-4">
                        Date
                    </div>
                    <div class="col-lg-3">
                        Payment Method
                    </div>
                    <div class="col-lg-2">
                        Trans Num
                    </div>
                    <div class="col-lg-2">
                    </div>

                </div>

                <asp:Repeater runat="server" ID="TransLogListItems" DataSource="<%#TransLogList%>">
                    <HeaderTemplate>
                        <div class="row">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="col-md-1"><%#Eval("ID") %></div>
                        <div class="col-md-4"><%#Eval("Date") %></div>
                        <div class="col-md-3"><%#Eval("PaymentMethodTypeId")%></div>
                        <div class="col-md-2"><%#Eval("TransactionId")%></div>
                        <div class="col-md-2">
                            <NP:Button ID="ButtonRequestResponse" runat="server" OnCommand="OnCommand" CssClass="btn btn-default" CommandName="ShowRequestResponse" CommandArgument='<%# Eval("ID") %>' Text="Details" />
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>

            </asp:View>
            <asp:View runat="server">
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        No History Found
                    </div>
                </div>
            </asp:View>
        </asp:MultiView>

    </div>
</admin:FormSections>

<admin:ModalDialog runat="server" ID="dlgShowStrings" Title="Details">
    <Body>
        <NP:UpdatePanel runat="server" ID="upStrings" RenderMode="Block" ChildrenAsTriggers="false" UpdateMode="Conditional">
            <ContentTemplate>
                <NP:PagedRepeater runat="server" ID="rptStringList" PageSize="20">
                    <ItemTemplate>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    Log ID   <%# Eval("ID") %>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    Date     
                                    <%# Eval("Date") %>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading">Request String</div>
                            <div class="panel-body">
                                <span class="BreakRow">
                                    <%# Eval("RequestString") %>
                                </span>
                            </div>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading">Response String</div>
                            <div class="panel-body">
                                <span class="BreakRow">
                                    <%# Eval("ResponseString") %>
                                </span>
                            </div>
                        </div>
                        </div>
                    </ItemTemplate>
                </NP:PagedRepeater>
            </ContentTemplate>
        </NP:UpdatePanel>
    </Body>
</admin:ModalDialog>

