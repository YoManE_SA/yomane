﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Filter.ascx.cs" Inherits="Netpay.Admin.Hosted.PaymentPageLog.Filter" %>
<script type="text/javascript">
    function CheckEnumOnClient(source, e) {
        var textbox = document.getElementById("iProcessDuration");
        var enumText = document.getElementById("ddlBigOrSmall");

        if ((textbox.value == "") && (enumText.value == "-1") || isNaN(textbox.value)) {
            e.IsValid = true;
        }
        else {
            if (enumText.value != "-1")
            { e.IsValid = true; }
            else
            { e.IsValid = false; }
        }
    }


    function Validate() {

        // If no group name provided the whole page gets validated
        //Page_ClientValidate('duration');
        //var textbox = document.getElementById("iProcessDuration");
        //var value = textbox.value;
        //var check = !isNaN(value);
        //if (check)
        Page_ClientValidate('enum');
    }
</script>

<admin:FilterSection runat="server" Title="Status">
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblAccount" runat="server" Text="Name" />
                <admin:AccountPicker runat="server" ID="apAccount" LimitToType="Merchant" UseTargetID="false" />
            </div>
        </div>
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label runat="server" Text="Status" />
                <netpay:BoolDropDown CssClass="form-control" runat="server" ID="ddlStatus" TrueText="Success" FalseText="Fail" />
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <asp:Label ID="lblStatus" runat="server" Text="Date Range" />
                <JQ:DateRange runat="server" ID="rngDate" />
            </div>
        </div>
    </div>
    <div class="form-group">
        <asp:Label ID="Label1" runat="server" Text="Reply Code" />
        <asp:TextBox runat="server" ID="txtReplyCode" CssClass="form-control" MaxLength="50" />
    </div>
    <div class="form-group">
        <asp:Label ID="Label3" runat="server" Text="" />
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <asp:Label ID="Label4" runat="server" Text="Process Duration" />
                    <netpay:EnumDropDown runat="server" ID="ddlBigOrSmall" CssClass="form-control" ViewEnumName="Netpay.Bll.Merchants.Hosted.EBigOrSmall" CausesValidation="True" BlankSelectionValue="-1" ControlToValidate="ddlBigOrSmall" ClientIDMode="Static" />
                    <asp:CustomValidator ForeColor="red" ID="ddlBigOrSmallValidator" Display="Dynamic" runat="server" ErrorMessage="Please select Big or Small" ValidateEmptyText="True" ControlToValidate="ddlBigOrSmall" ClientValidationFunction="CheckEnumOnClient" OnServerValidate="CheckEnumOnServer" ValidationGroup="enum" />
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <asp:Label ID="Label2" runat="server" Text="Sec" />
                    <asp:TextBox runat="server" ClientID="textbox" ID="iProcessDuration" CssClass="form-control" CausesValidation="True" ClientIDMode="Static" onblur="Validate();" />
                </div>
                <asp:CompareValidator ForeColor="red" ID="iProcessDurationValidator" runat="server" ErrorMessage="Please enter a number." ControlToValidate="iProcessDuration" Operator="DataTypeCheck" Type="Integer" ValidationGroup="enum" Display="Dynamic" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                ID Range
                <JQ:IntRange runat="server" ID="rngID" />
            </div>
        </div>
    </div>
</admin:FilterSection>



