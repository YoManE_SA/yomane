﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="List.ascx.cs" Inherits="Netpay.Admin.Hosted.PaymentPageLog.List" %>
<admin:ListSection ID="ListSection1" runat="server" Title="Payment Page">
    <Header>
        <admin:LegendColors runat="server" Items='<%# Netpay.Bll.Merchants.Hosted.PaymentPageLog.SuccessOrFailStatus %>' FloatRight="true" />
    </Header>
    <Body>
        <div class="table-responsive">
            <admin:AdminList runat="server" SaveAjaxState="true" ID="rptList" DataKeyNames="ID" AutoGenerateColumns="false" PageSize="25" BubbleLoadEvent="true">
                <Columns>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <span style="background-color: <%# Netpay.Infrastructure.Enums.BoolColorMap.MapHtmlColor((bool)Eval("IsSuccess")) %>;" class="legend-item"></span>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ID" SortExpression="LogPaymentPage_id">
                        <ItemTemplate>
                            <%# Eval("ID") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField HeaderText="Date Start" DataField="DateStart" SortExpression="Lpp_DateStart" />
                    <asp:TemplateField HeaderText="Merchant Name" SortExpression="Lpp_MerchantNumber">
                        <ItemTemplate>
                            <%# GetMerchantName((string)Eval("MerchantNumber")) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="IP Address" DataField="RemoteIPAddress" SortExpression="Lpp_RemoteAddress" />
                    <asp:BoundField HeaderText="Source" DataField="RequestMethod" SortExpression="Lpp_RequestMethod" />
                    <asp:BoundField HeaderText="Reply" DataField="ReplyCode" SortExpression="Lpp_ReplyCode" />
                    <asp:BoundField HeaderText="TransID" DataField="TransactionID" SortExpression="Lpp_TransNum" />
                </Columns>
            </admin:AdminList>
        </div>
    </Body>
    <Footer>
        <admin:DataButtons runat="server" PagedControlID="rptList" EnableExport="true" EnableAdd="false" />
    </Footer>

</admin:ListSection>
