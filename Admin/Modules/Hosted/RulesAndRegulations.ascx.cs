﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin.Modules.Hosted
{
    public partial class RulesAndRegulations : Controls.AccountControlBase
    {
        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += PageController_SaveItem;
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            if (Account != null)
            {
                HtmlRulesAndRegulations.Text = Netpay.Bll.Merchants.Merchant.GetContent(Account.AccountID, Netpay.CommonTypes.Language.English, "RulesAndRegulations.html");
            }
            base.DataBindChildren();
        }


        public void PageController_SaveItem(object sender, EventArgs e)
        {
            if (Account != null && Account.AccountID != 0)
            {
                if (string.IsNullOrEmpty(HtmlRulesAndRegulations.Text))
                {
                    if (Netpay.Bll.Merchants.Merchant.GetContent(Account.AccountID, Netpay.CommonTypes.Language.English, "RulesAndRegulations.html") != null)
                    Netpay.Bll.Merchants.Merchant.SaveContent(Account.AccountID, Netpay.CommonTypes.Language.English, "RulesAndRegulations.html", HtmlRulesAndRegulations.Text);
                }
                else
                {
                    Netpay.Bll.Merchants.Merchant.SaveContent(Account.AccountID, Netpay.CommonTypes.Language.English, "RulesAndRegulations.html", HtmlRulesAndRegulations.Text);
                }
            }
        }
    }
}