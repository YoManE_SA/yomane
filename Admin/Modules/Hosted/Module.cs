﻿using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netpay.Admin.Hosted
{
	public class Module : Admin.CoreBasedModule
	{
		public override string Name { get { return "Hosted"; } }
		public override decimal Version { get { return 1.0m; } }
		public override string Author { get { return "OBL ltd."; } }
		public override string Description { get { return ""; } }

		public Module() : base(Bll.Merchants.Hosted.Module.Current) { }

		protected override void OnInit(EventArgs e)
		{
			//No menu item

			//No route

			//Register event
			Application.InitDataTablePage += Application_InitTemplatePage;

			base.OnInit(e);
		}

		protected override void OnActivate(EventArgs e)
		{			
			base.OnActivate(e);
		}

		protected override void OnDeactivate(EventArgs e)
		{		
			base.OnDeactivate(e);
		}

		protected void Application_InitTemplatePage(object sender, EventArgs e)
		{
			if (!CoreModule.IsInstalled)
				return;

			var page = sender as Controls.DataTablePage;
			if (page.TemplateName == "Merchant" && (page.ViewName == null))
			{
				page.AddControlToForm("Hosted Payment Page", page.LoadControl("~/Modules/Hosted/MerchantSettings.ascx"), this, "", new Controls.SecuredObjectSelector(page.CurrentPageSecuredObject, Bll.Merchants.Hosted.PaymentPage.SecuredObject).GetActivObject);
				page.AddControlToForm("Regulations", page.LoadControl("~/Modules/Hosted/RulesAndRegulations.ascx"), this, "", page.CurrentPageSecuredObject);
			}
		}
	}
}