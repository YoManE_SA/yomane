﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MerchantSettings.ascx.cs" Inherits="Netpay.Admin.Hosted.MerchantSettings" %>
<admin:FormSections ID="PaymentPageFormPanel" runat="server" Title="Payment Page">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox runat="server" AutoPostBack="true" OnCheckedChanged="chkHPP_CreditCard_CheckedChanged" ID="chkHPP_CreditCard" Text="Allow credit card" Checked="<%# PaymentPageSettings.IsCreditCard %>" /></li>
                <asp:UpdatePanel ID="Up_chkHPP_CreditCard" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>
                            <li class="list-group-item">
                                <ul class="child-list-group">
                                    <li>
                                        <asp:CheckBox Enabled='<%# chkHPP_CreditCard.Checked %>' runat="server" ID="chkHPP_CreditCardSSN" Text="Require ID/SSN" Checked="<%# PaymentPageSettings.IsCreditCardRequiredID %>" /></li>
                                    <li>
                                        <asp:CheckBox Enabled='<%# chkHPP_CreditCard.Checked %>' runat="server" ID="chkHPP_CreditCardCVV" Text="Require CVV2/CID" Checked="<%# PaymentPageSettings.IsCreditCardRequiredCVV %>" /></li>
                                    <li>
                                        <asp:CheckBox Enabled='<%# chkHPP_CreditCard.Checked %>' runat="server" ID="chkHPP_CreditCardPhone" Text="Require Phone" Checked="<%# PaymentPageSettings.IsCreditCardRequiredPhone %>" /></li>
                                    <li>
                                        <asp:CheckBox Enabled='<%# chkHPP_CreditCard.Checked %>' runat="server" ID="chkHPP_CreditCardEmail" Text="Require eMail" Checked="<%# PaymentPageSettings.IsCreditCardRequiredEmail %>" /></li>
                                </ul>
                            </li>
                     </ContentTemplate>
                </asp:UpdatePanel>

            </ul>
        </div>
        <div class="col-lg-6 col-md-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox runat="server" AutoPostBack="true" OnCheckedChanged="chkHPP_Echeck_CheckedChanged" ID="chkHPP_Echeck" Text="Allow eCheck" Checked="<%# PaymentPageSettings.IsEcheck %>" />
                </li>
                <asp:UpdatePanel runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional" ID="Up_chkHPP_Echeck">
                    <ContentTemplate>
                        <li class="list-group-item">
                            <ul class="child-list-group">
                                <li>
                                    <asp:CheckBox Enabled='<%# chkHPP_Echeck.Checked %>' runat="server" Text="Require ID/SSN" ID="chkHPP_EcheckSSN" Checked="<%# PaymentPageSettings.IsEcheckRequiredID %>" /></li>
                                <li>
                                    <asp:CheckBox Enabled='<%# chkHPP_Echeck.Checked %>' runat="server" ID="chkHPP_EcheckPhone" Text="Require Phone " Checked="<%# PaymentPageSettings.IsEcheckRequiredPhone %>" /></li>
                                <li>
                                    <asp:CheckBox Enabled='<%# chkHPP_Echeck.Checked %>' runat="server" ID="chkHPP_EcheckEmail" Text="Require eMail" Checked="<%# PaymentPageSettings.IsEcheckRequiredEmail %>" /></li>
                            </ul>
                        </li>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ul>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox runat="server" AutoPostBack="true" OnCheckedChanged="chkHPP_DirectDebit_CheckedChanged" ID="chkHPP_DirectDebit" Checked="<%# PaymentPageSettings.IsDirectDebit %>" Text="Allow direct debit" /></li>
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false" ID="Up_chkHPP_DirectDebit">
                        <ContentTemplate>
                            <li class="list-group-item">
                                <ul class="child-list-group">
                                    <li>
                                        <asp:CheckBox Enabled='<%# chkHPP_DirectDebit.Checked %>' runat="server" ID="chkHPP_DirectDebitPhone" Text="Require Phone" Checked="<%# PaymentPageSettings.IsDirectDebitRequiredPhone %>" /></li>
                                    <li>
                                        <asp:CheckBox Enabled='<%# chkHPP_DirectDebit.Checked %>' runat="server" ID="chkHPP_DirectDebitEmail" Text="Require eMail" Checked="<%# PaymentPageSettings.IsDirectDebitRequiredEmail %>" /></li>
                                </ul>
                            </li>
                        </ContentTemplate>
                    </asp:UpdatePanel>
            </ul>
        </div>
        <div class="col-lg-6 col-md-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox runat="server" AutoPostBack="true" OnCheckedChanged="chkHPP_Phone_CheckedChanged" ID="chkHPP_Phone" Text="Allow phone debit" Checked="<%# PaymentPageSettings.IsPhoneDebit %>" /></li>
                <asp:UpdatePanel runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional" ID="Up_chkHPP_Phone">
                    <ContentTemplate>
                        <li class="list-group-item">
                            <ul class="child-list-group">
                                <li>
                                    <asp:CheckBox Enabled='<%# chkHPP_Phone.Checked %>' runat="server" ID="chkHPP_PhonePhone" Text="Require Phone" Checked="<%# PaymentPageSettings.IsPhoneDebitRequiredPhone %>" />
                                </li>
                                <li>
                                    <asp:CheckBox Enabled='<%# chkHPP_Phone.Checked %>' runat="server" ID="chkHPP_PhoneEmail" Text="Require eMail" Checked="<%# PaymentPageSettings.IsPhoneDebitRequiredEmail %>" />
                                </li>
                            </ul>
                        </li>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ul>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox runat="server" AutoPostBack="true" OnCheckedChanged="chkHPP_WebMoney_CheckedChanged" ID="chkHPP_WebMoney" Text="Allow WebMoney" Checked="<%# PaymentPageSettings.IsWebMoney %>" /></li>
                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false" ID="Up_chkHPP_WebMoney">
                    <ContentTemplate>
                        <li class="list-group-item">
                            <ul class="child-list-group">
                                <li>Merchant Purse:
                             <asp:TextBox runat="server" Enabled='<%# chkHPP_WebMoney.Checked  %>' CssClass="form-control" ID="txtHPP_WebMoneyPurse" Checked="<%# PaymentPageSettings.WebMoneyPurse %>" />
                                </li>
                            </ul>
                        </li>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ul>
            <hr />
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox runat="server" AutoPostBack="true" OnCheckedChanged="chkHPP_PayPal_CheckedChanged" ID="chkHPP_PayPal" Text="Allow PayPal" Checked="<%# PaymentPageSettings.IsPayPal %>" /></li>
                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false" ID="Up_chkHPP_PayPal">
                    <ContentTemplate>
                        <li class="list-group-item">
                            <ul class="child-list-group">
                                <li>Merchant ID:
                                    <asp:TextBox runat="server" Enabled='<%# chkHPP_PayPal.Checked  %>' CssClass="form-control" ID="txtHPP_PayPalMerchantID" Text="<%# PaymentPageSettings.PayPalMerchantID %>" />
                                </li>
                            </ul>
                        </li>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ul>
        </div>


        <div class="col-lg-6 col-md-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox runat="server" AutoPostBack="true" OnCheckedChanged="chkHPP_GoogleCheckout_CheckedChanged" ID="chkHPP_GoogleCheckout" Text="Allow Google Checkout" Checked="<%# PaymentPageSettings.IsGoogleCheckout %>" /></li>
                <asp:UpdatePanel runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional" ID="Up_chkHPP_GoogleCheckout">
                    <ContentTemplate>
                        <li class="list-group-item">
                            <ul class="child-list-group">
                                <li>Merchant ID:
                                    <asp:TextBox CssClass="form-control" Enabled='<%# chkHPP_GoogleCheckout.Checked %>' runat="server" ID="txtHPP_GoogleCheckoutMerchantID" Text="<%# PaymentPageSettings.GoogleCheckoutMerchantID %>" />
                                </li>
                            </ul>
                        </li>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ul>
            <hr />
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox runat="server" AutoPostBack="true" OnCheckedChanged="chkHPP_MoneyBookers_CheckedChanged" ID="chkHPP_MoneyBookers" Text="Allow Money Bookers" Checked="<%# PaymentPageSettings.IsMoneyBookers %>" /></li>
                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="Up_chkHPP_MoneyBookers_CheckedChanged" ChildrenAsTriggers="false">
                    <ContentTemplate>
                        <li class="list-group-item">
                            <ul class="child-list-group">
                                <li>Account:
                                    <asp:TextBox runat="server" Enabled='<%# chkHPP_MoneyBookers.Checked %>' CssClass="form-control" ID="txtHPP_MoneyBookersAccount" Text="<%# PaymentPageSettings.MoneyBookersAccount %>" />
                                </li>
                            </ul>
                        </li>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ul>
        </div>
    </div>
    <hr />
    <ul class="list-group">
        <li class="list-group-item">
            <asp:CheckBox runat="server" ID="chkHPP_Wallet" Text="Allow Wallet login" Checked="<%# PaymentPageSettings.IsCustomer %>" />
        </li>
        <li class="list-group-item">
            <asp:CheckBox runat="server" ID="chkHPP_WhiteLabel" Text="Use White-Label view" Checked="<%# PaymentPageSettings.IsWhiteLabel %>" />

        </li>
        <li class="list-group-item">
            <asp:CheckBox runat="server" ID="chkHPP_RequireMD5" Text="Don't require signature" Checked="<%# PaymentPageSettings.IsSignatureOptional %>" />
        </li>
    </ul>
</admin:FormSections>
<admin:FormSections runat="server" Title="Public Page / Shops">
    <div class="row">
        <div class="col-lg-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox  runat="server" ID="chkPP_Enable" Text="Allow public page" Checked="<%# ShopSettings.IsEnabled %>" /></li>
                             <li class="list-group-item">
                                <asp:CheckBox runat="server" ID="chkPP_IsKeepCart" Text="Save cart between visits" Checked="<%# ShopSettings.IsKeepCart %>" />
                             </li>
            </ul>
        </div>
        <div class="col-lg-6">
            <ul class="list-group">
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkPP_IsAllowPreAuth" Text="Allow Authorization Only products" Checked="<%# ShopSettings.IsAllowPreAuth %>" /></li>
                <li class="list-group-item">
                    <asp:CheckBox runat="server" ID="chkPP_IsAllowDynamicProduct" Text="Allow Dynamic price products" Checked="<%# ShopSettings.IsAllowDynamicProduct %>" /></li>
            </ul>
        </div>
    </div>
</admin:FormSections>
