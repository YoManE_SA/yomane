﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;

namespace Netpay.Admin.Hosted
{
    public partial class MerchantSettings : Controls.AccountControlBase
    {
        protected Bll.Merchants.Hosted.PaymentPage PaymentPageSettings { get; private set; }
        protected Bll.Shop.Cart.MerchantSettings ShopSettings { get; private set; }

        protected override void OnLoad(EventArgs e)
        {
            TemplatePage.PageController.SaveItem += PageController_SaveItem;
            base.OnLoad(e);
        }

        protected override void DataBindChildren()
        {
            if (Account != null) PaymentPageSettings = Netpay.Bll.Merchants.Hosted.PaymentPage.Load(Account.MerchantID.GetValueOrDefault());
            if (PaymentPageSettings == null) PaymentPageSettings = new Netpay.Bll.Merchants.Hosted.PaymentPage(0);
            if (Account != null) ShopSettings = Bll.Shop.Cart.MerchantSettings.Load(Account.MerchantID.GetValueOrDefault());
            if (ShopSettings == null) ShopSettings = new Bll.Shop.Cart.MerchantSettings(0);
            base.DataBindChildren();
        }

        protected void UpdateValues()
        {
            if (Account != null) PaymentPageSettings = Netpay.Bll.Merchants.Hosted.PaymentPage.Load(Account.MerchantID.GetValueOrDefault());
            if (PaymentPageSettings == null) PaymentPageSettings = new Netpay.Bll.Merchants.Hosted.PaymentPage(0);
            if (Account != null) ShopSettings = Bll.Shop.Cart.MerchantSettings.Load(Account.MerchantID.GetValueOrDefault());
            if (ShopSettings == null) ShopSettings = new Bll.Shop.Cart.MerchantSettings(0);
        }


        protected virtual void PageController_SaveItem(object sender, EventArgs e)
        {
            PaymentPageSettings = Netpay.Bll.Merchants.Hosted.PaymentPage.Load(Account.MerchantID.GetValueOrDefault());
            if (PaymentPageSettings == null) PaymentPageSettings = new Netpay.Bll.Merchants.Hosted.PaymentPage(Account.MerchantID.GetValueOrDefault());
            PaymentPageSettings.UIVersion = 2;
            PaymentPageSettings.IsCreditCard = chkHPP_CreditCard.Checked;
            PaymentPageSettings.IsCreditCardRequiredID = chkHPP_CreditCardSSN.Checked;
            PaymentPageSettings.IsCreditCardRequiredCVV = chkHPP_CreditCardCVV.Checked;
            PaymentPageSettings.IsCreditCardRequiredPhone = chkHPP_CreditCardPhone.Checked;
            PaymentPageSettings.IsCreditCardRequiredEmail = chkHPP_CreditCardEmail.Checked;

            PaymentPageSettings.IsEcheck = chkHPP_Echeck.Checked;
            PaymentPageSettings.IsEcheckRequiredID = chkHPP_EcheckSSN.Checked;
            PaymentPageSettings.IsEcheckRequiredPhone = chkHPP_EcheckPhone.Checked;
            PaymentPageSettings.IsEcheckRequiredEmail = chkHPP_EcheckEmail.Checked;

            PaymentPageSettings.IsDirectDebit = chkHPP_DirectDebit.Checked;
            PaymentPageSettings.IsDirectDebitRequiredPhone = chkHPP_DirectDebitPhone.Checked;
            PaymentPageSettings.IsDirectDebitRequiredEmail = chkHPP_DirectDebitEmail.Checked;

            PaymentPageSettings.IsPhoneDebit = chkHPP_Phone.Checked;
            PaymentPageSettings.IsPhoneDebitRequiredPhone = chkHPP_PhonePhone.Checked;
            PaymentPageSettings.IsPhoneDebitRequiredEmail = chkHPP_PhoneEmail.Checked;

            PaymentPageSettings.IsWebMoney = chkHPP_WebMoney.Checked;
            PaymentPageSettings.WebMoneyPurse = txtHPP_WebMoneyPurse.Text;
            PaymentPageSettings.IsPayPal = chkHPP_PayPal.Checked;
            PaymentPageSettings.PayPalMerchantID = txtHPP_PayPalMerchantID.Text;
            PaymentPageSettings.IsGoogleCheckout = chkHPP_GoogleCheckout.Checked;
            PaymentPageSettings.GoogleCheckoutMerchantID = txtHPP_GoogleCheckoutMerchantID.Text;
            PaymentPageSettings.IsMoneyBookers = chkHPP_MoneyBookers.Checked;
            PaymentPageSettings.MoneyBookersAccount = txtHPP_MoneyBookersAccount.Text;
            PaymentPageSettings.IsSignatureOptional = chkHPP_RequireMD5.Checked;
            PaymentPageSettings.IsCustomer = chkHPP_Wallet.Checked;
            PaymentPageSettings.IsWhiteLabel = chkHPP_WhiteLabel.Checked;


            ShopSettings = Bll.Shop.Cart.MerchantSettings.Load(Account.MerchantID.GetValueOrDefault());
            if (ShopSettings == null) ShopSettings = new Bll.Shop.Cart.MerchantSettings(Account.MerchantID.GetValueOrDefault());

            ShopSettings.IsEnabled = chkPP_Enable.Checked;
            ShopSettings.IsKeepCart = chkPP_IsKeepCart.Checked;
            ShopSettings.IsAllowPreAuth = chkPP_IsAllowPreAuth.Checked;
            ShopSettings.IsAllowDynamicProduct = chkPP_IsAllowDynamicProduct.Checked;

            //Netpay.Bll.Merchants.Merchant.SaveContent(Account.AccountID.GetValueOrDefault(), CommonTypes.Language.English, "RulesAndRegulations.html", .Text);
            PaymentPageSettings.Save();
            ShopSettings.Save();
        }

       
        protected void chkHPP_CreditCard_CheckedChanged(object sender, EventArgs e)
        {
            UpdateValues();
            Up_chkHPP_CreditCard.DataBind();
            Up_chkHPP_CreditCard.Update();
        }

        protected void chkHPP_Echeck_CheckedChanged(object sender, EventArgs e)
        {
            UpdateValues();
            Up_chkHPP_Echeck.DataBind();
            Up_chkHPP_Echeck.Update();
        }

        protected void chkHPP_DirectDebit_CheckedChanged(object sender, EventArgs e)
        {
            UpdateValues();
            Up_chkHPP_DirectDebit.DataBind();
            Up_chkHPP_DirectDebit.Update();
        }

        protected void chkHPP_Phone_CheckedChanged(object sender, EventArgs e)
        {
            UpdateValues();
            Up_chkHPP_Phone.DataBind();
            Up_chkHPP_Phone.Update();
        }

        protected void chkHPP_WebMoney_CheckedChanged(object sender, EventArgs e)
        {
            UpdateValues();
            Up_chkHPP_WebMoney.DataBind();
            Up_chkHPP_WebMoney.Update();
        }

        protected void chkHPP_PayPal_CheckedChanged(object sender, EventArgs e)
        {
            UpdateValues();
            Up_chkHPP_PayPal.DataBind();
            Up_chkHPP_PayPal.Update();
        }

        protected void chkHPP_GoogleCheckout_CheckedChanged(object sender, EventArgs e)
        {
            UpdateValues();
            Up_chkHPP_GoogleCheckout.DataBind();
            Up_chkHPP_GoogleCheckout.Update();
        }

        protected void chkHPP_MoneyBookers_CheckedChanged(object sender, EventArgs e)
        {
            UpdateValues();
            Up_chkHPP_MoneyBookers_CheckedChanged.DataBind();
            Up_chkHPP_MoneyBookers_CheckedChanged.Update();
        }

    }
}