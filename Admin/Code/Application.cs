﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Admin
{
    public abstract class Module
    {
        protected Application Application { get; private set; }
        public bool IsActive { get; protected set; }
        public abstract string Name { get; }
        public abstract decimal Version { get; }
        public abstract string Author { get; }
        public abstract string Description { get; }

        protected virtual void OnInit(EventArgs e) { }
        protected virtual void OnActivate(EventArgs e) { IsActive = true; }
        protected virtual void OnDeactivate(EventArgs e) { IsActive = false; }
        protected virtual void OnUninstallAdmin(EventArgs e) { }
        protected virtual void OnInstallAdmin(EventArgs e) { }

        protected virtual void OnUnload(EventArgs e) { }

        public virtual void Init(Application application, EventArgs e) { Application = application; OnInit(e); }
        public virtual void Activate(EventArgs e) { if (IsActive) throw new Exception("Wrong module state"); OnActivate(e); }
        public virtual void Deactivate(EventArgs e) { if (!IsActive) throw new Exception("Wrong module state"); OnDeactivate(e); }

        public virtual void InstallAdmin(EventArgs e) { OnInstallAdmin(e); }
        public virtual void UninstallAdmin(EventArgs e) { OnUninstallAdmin(e); }
        public virtual void Unload(EventArgs e) { OnUnload(e); Application = null; }
    }

    public abstract class CoreBasedModule : Module
    {
        public Infrastructure.Module CoreModule { get; private set; }
        public CoreBasedModule(Infrastructure.Module coreModule) { CoreModule = coreModule; }
        public override string Name { get { return CoreModule.Name; } }
        public override decimal Version { get { return CoreModule.Version; } }
        public override string Author { get { return CoreModule.Author; } }
        public override string Description { get { return CoreModule.Description; } }

        protected virtual void OnInstall(EventArgs e) { CoreModule.Install(e); }
        protected virtual void OnUninstall(EventArgs e) { CoreModule.UnInstall(e); }
        public virtual void Install(EventArgs e) { if (CoreModule.IsInstalled) throw new Exception("Wrong module state"); Install(e); }
        public virtual void UnInstall(EventArgs e) { if (!CoreModule.IsInstalled) throw new Exception("Wrong module state"); UnInstall(e); }

        protected override void OnActivate(EventArgs e) { if (!CoreModule.IsActive) CoreModule.Activate(e); base.OnActivate(e); }
        protected override void OnDeactivate(EventArgs e) { if (CoreModule.IsActive) CoreModule.Deactivate(e); base.OnDeactivate(e); }

        public override void InstallAdmin(EventArgs e) { base.InstallAdmin(e); CoreModule.Install(EventArgs.Empty); }
        public override void UninstallAdmin(EventArgs e) { base.UninstallAdmin(e); CoreModule.UnInstall(EventArgs.Empty);  }
    }

    public class ApplicationMenu
    {
        public ApplicationMenu(string title, string url, string cssClass = null, int prioriry = 0)
        {
            this.Title = title;
            this.Url = url;
            this.CssClass = cssClass;
            this.Priority = prioriry;
            ChildItems = new ApplicationMenu[] { };
        }
        public ApplicationMenu Parent { get; private set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string CssClass { get; set; }
        public int Priority { get; private set; }

        public void AddChild(ApplicationMenu child)
        {
            child.Parent = this;
            var lst = ChildItems.ToList();
            lst.Add(child);
            ChildItems = lst.OrderBy(t => t.Priority).ToArray();
        }

        public void RemoveChild(ApplicationMenu child)
        {
            child.Parent = null;
            var lst = ChildItems.ToList();
            lst.Remove(child);
            ChildItems = lst.ToArray();
        }

        public void ReplaceChild(ApplicationMenu oldChild, ApplicationMenu newChild, bool preserveChildren)
        {
            if (newChild.Parent != null) newChild.Parent.RemoveChild(newChild);
            oldChild.Parent = null;
            var lst = ChildItems.ToList();
            lst[lst.IndexOf(oldChild)] = newChild;
            newChild.Parent = this;
            ChildItems = lst.ToArray();
            if (preserveChildren)
            {
                foreach (var v in oldChild.ChildItems) v.Parent = newChild;
                newChild.ChildItems = oldChild.ChildItems;
            }
        }

        public ApplicationMenu[] ChildItems { get; set; }

        public ApplicationMenu FindByUrl(string url)
        {
            if (Url != null && Url.StartsWith(url, StringComparison.CurrentCultureIgnoreCase)) return this;
            foreach (var c in ChildItems)
            {
                var ret = c.FindByUrl(url);
                if (ret != null) return ret;
            }
            return null;
        }

        public string GetPath(char seperator = '/')
        {
            var sb = new System.Text.StringBuilder();
            for (var p = this; p.Parent != null; p = p.Parent)
            {
                if (sb.Length > 0) sb.Insert(0, seperator);
                sb.Insert(0, p.Title);
            }
            return sb.ToString();
        }
        public override string ToString() { return GetPath(); }
    }

    public class Application
    {
        public string InstanceName { get; private set; }
        public List<Module> Modules { get; private set; }
        public ApplicationMenu Menu { get; private set; }
        public ApplicationMenu UserMenu { get; private set; }
        public ApplicationMenu QuickSearch { get; private set; }
        public event EventHandler InitPage;
        public event EventHandler InitDataTablePage;

        private static object _syncLock = new object();
        private static Application _currentApp = null;
        public static Application Current
        {
            get
            {
                if (_currentApp == null)
                {
                    lock (_syncLock)
                    {
                        if (_currentApp == null)
                            _currentApp = new Application();
                    }
                }

                return _currentApp;
            }
        }

        /// <summary>
        /// Get the list of installed modules.
        /// Installed modules are CoreBasedModule only, therefore
        /// converting to CoreBasedModule.
        /// </summary>
        public List<CoreBasedModule> InstalledModules
        {
            get
            {
                var installedModules = Modules
                    .Where(i => (i is CoreBasedModule) &&
                                ((i as CoreBasedModule).CoreModule.IsInstalled))
                    .Select(i => i as CoreBasedModule).ToList();

                return installedModules;
            }
        }
                
        public List<Module> OnlyAdminModules
        {
            get
            {
                var adminmodules = Modules.Where(x => (!(x is CoreBasedModule))).ToList();
                return adminmodules;
            }
        }

        private Application()
        {
            //Netpay.Infrastructure.Application.Init("AdminWeb");
            Modules = new List<Module>();
            Menu = new ApplicationMenu("Home", "~/", null);
            QuickSearch = new ApplicationMenu(null, "Menu", null);
            UserMenu = new ApplicationMenu(null, "UserMenu", null);
            InstanceName = "AdminWeb";
            AddLocalModules();
            ActivateModules();
        }

        private void AddLocalModules()
        {
            //Add static menu's
            AddMenuItem(null, new ApplicationMenu("Merchants", null, "fa-suitcase", 1));
            AddMenuItem(null, new ApplicationMenu("Debit Companies", null, "fa fa-university", 2));
            AddMenuItem(null, new ApplicationMenu("Customers", null, "fa-group", 3));
            AddMenuItem(null, new ApplicationMenu("Partners", null, "fa-share-alt", 4));
            AddMenuItem(null, new ApplicationMenu("Transactions", null, "fa-credit-card", 5));
            AddMenuItem(null, new ApplicationMenu("Risk", null, "fa-shield", 50));
            AddMenuItem(null, new ApplicationMenu("Finance", null, "fa-money", 60));
            AddMenuItem(null, new ApplicationMenu("Logs", null, "fa-file-text", 100));
            AddMenuItem(null, new ApplicationMenu("Settings", null, "fa-cog", 200));
            AddMenuItem(null, new ApplicationMenu("Data Manager", null, "fa-globe", 300));


            UserMenu.AddChild(new ApplicationMenu("Log Out", "~/Login.aspx?DoLogout", "fa-power-off", 100));

            Modules.Add(new Security.Module());
            Modules.Add(new Merchants.Module());
            Modules.Add(new DebitCompanies.Module());
            Modules.Add(new Modules.DebitCompanies.Terminals.Module());
            Modules.Add(new Customers.Module());
            Modules.Add(new Affiliates.Module());
            Modules.Add(new Accounts.Module());
            Modules.Add(new RefundRequests.Module());
            Modules.Add(new TransactionsRecurring.Module());
            Modules.Add(new Transactions.Module());
            Modules.Add(new Settlements.Module());
            Modules.Add(new Wire.Module());

            //Wire providers 
            Modules.Add(new Modules.Wire.WireProviders.Balance.Module());
            Modules.Add(new Modules.Wire.WireProviders.Manual.Module());
            Modules.Add(new Modules.Wire.WireProviders.Masav.Module());
            Modules.Add(new Modules.Wire.WireProviders.BOG.Module());
            Modules.Add(new Modules.Wire.WireProviders.FNB.Module());
            Modules.Add(new Modules.Wire.WireProviders.Hellenic.Module());


            Modules.Add(new ModuleManager.Module());
            Modules.Add(new ApplicationIdentities.Module());
            Modules.Add(new PeriodicFees.Module());
            Modules.Add(new Merchants.Registration.Module());
            Modules.Add(new Merchants.Registration.CardConnect.Module());
            Modules.Add(new Merchants.Registration.Zoho.Module());
            Modules.Add(new Merchants.Status.Module());
            Modules.Add(new RollingReserve.Module());
            Modules.Add(new Hosted.Module());
            Modules.Add(new Modules.Hosted.PaymentPageLog.Module());
            Modules.Add(new Process.Module());
            Modules.Add(new Risk.Module());
            Modules.Add(new Risk.BinNumbers.Module());
            Modules.Add(new PendingEvents.Module());
            Modules.Add(new FileMananger.Module());
            Modules.Add(new FileManager.EpaModule());
            Modules.Add(new Emails.Module());
            Modules.Add(new Faqs.Module());

            //Settings modules
            Modules.Add(new Modules.Settings.PendingEventTypes.Module());
            Modules.Add(new Currencies.Module());
            Modules.Add(new Modules.Settings.PaymentMethodsMng.Module());
            Modules.Add(new Modules.Settings.PrepaidPaymentMethods.Module());

            Modules.Add(new Risk.MaxMind.Module());
            Modules.Add(new Risk.MerchantBlackList.Module());
            Modules.Add(new Content.Module());
            Modules.Add(new Balance.Module());
            Modules.Add(new VirtualTerminal.Module());
            Modules.Add(new Documentations.Notifications.Module());
            Modules.Add(new Documentations.AdminDocs.Module());
            Modules.Add(new EmailTemplates.Module());
            Modules.Add(new TerminalsWithoutFees.Module());
            Modules.Add(new LogsTransactionHistory.Module());
            Modules.Add(new LogsHistory.Module());
            Modules.Add(new BankAccounts.Module());
            Modules.Add(new LogsDebitRules.Module());
            Modules.Add(new Fraud.Module());
            Modules.Add(new Modules.ThirdParty.Lamda.Module());
            
            //DataManager modules.
            Modules.Add(new Modules.DataManager.CountryListModule.Module());
            Modules.Add(new Modules.DataManager.ParentCompanyModule.Module());
            Modules.Add(new Modules.DataManager.MerchantDepartmentModule.Module());
            Modules.Add(new Modules.DataManager.ProductTypeModule.Module());
            Modules.Add(new Modules.DataManager.ProductPropertyTypeModule.Module());
            Modules.Add(new Modules.DataManager.PaymentMethodGroupModule.Module());
            Modules.Add(new Modules.DataManager.PeopleRelationTypeModule.Module());
            Modules.Add(new Modules.DataManager.ProductCategoryModule.Module());
            Modules.Add(new Modules.DataManager.StateListModule.Module());
            Modules.Add(new Modules.DataManager.FileItemTypeModule.Module());


            Modules.Add(new Notifications.Module());
            Modules.Add(new ActionPage.Module());
            Modules.Add(new Invoices.Module());
            Modules.Add(new Admin.Modules.Sample.Module());
            Modules.Add(new Modules.ExportedReports.Module());

            //Perform init on all modules
            foreach (var module in Modules)
                module.Init(this, EventArgs.Empty);

            // Perform OnAdminInstall only on installed corebased modules
            foreach (var module in InstalledModules)
            {
                module.InstallAdmin(EventArgs.Empty);
            }
        }

        private void ActivateModules()
        {
            // Iterate over the installed corebased modules
            foreach (var module in InstalledModules)
            {
                if (module.CoreModule.IsActive)
                    module.Activate(EventArgs.Empty);
            }

            // Iterate over the admin modules
            foreach (var module in OnlyAdminModules)
            {
                module.Activate(EventArgs.Empty);
            }
        }

        public void AddMenuItem(string menuPath, ApplicationMenu item)
        {
            var curItem = Menu;
            menuPath += "/" + item.Title;
            var pathItems = menuPath.Split(new char[] { '/', '\\' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < pathItems.Length; i++)
            {
                var ret = curItem.ChildItems.Where(c => c.Title == pathItems[i]).SingleOrDefault();
                if (ret == null) curItem.AddChild(ret = new ApplicationMenu(pathItems[i], null, null, item.Priority));
                curItem = ret;
            }
            curItem.Parent.ReplaceChild(curItem, item, true);

            Menu.ChildItems = Menu.ChildItems.OrderBy(x => x.Priority).ToArray();            
        }

        public void RegisterRoute(string routeAddress, string templateName, Type templatePageType, System.Web.Routing.RouteValueDictionary constraints = null, System.Web.Routing.RouteValueDictionary defaultValues = null, Admin.Module module = null)
        {
            lock (this)
            {
                if (!System.Web.Routing.RouteTable.Routes.Any(x => (x as System.Web.Routing.Route).Url == routeAddress))
                System.Web.Routing.RouteTable.Routes.Add(routeAddress, new System.Web.Routing.Route(routeAddress, new Controls.PageRouteHandler(templateName, templatePageType, module)) { Defaults = defaultValues, Constraints = constraints });
            }
        }

        public void UnRegisterRoute(string routeAddress)
        {
            System.Web.Routing.RouteBase routeToRemove = System.Web.Routing.RouteTable.Routes.Where(x => (x as System.Web.Routing.Route).Url == routeAddress).SingleOrDefault();

            if (routeToRemove != null)
            lock(this)
            {
               System.Web.Routing.RouteTable.Routes.Remove(routeToRemove);
            }
        }

        public void raiseInitPage(System.Web.UI.Page page, EventArgs e)
        {
            if (InitPage != null) InitPage(page, e);
        }

        public void raiseInitDataTablePage(System.Web.UI.Page page, EventArgs e)
        {
            if (InitDataTablePage != null) InitDataTablePage(page, e);
        }

        public string MapAction(string controller, bool refCurrent, string refTitle = null, int? itemId = null, string viewName = null, string parameters = null)
        {
            var ret = controller;
            if (itemId != null)
            {
                if (!ret.EndsWith("/")) ret += "/";
                ret += itemId;
            }
            if (!string.IsNullOrEmpty(viewName))
            {
                if (!ret.EndsWith("/")) ret += "/";
                ret += viewName;
            }
            if (!string.IsNullOrEmpty(parameters))
            {
                ret += (ret.IndexOf('?') == -1) ? "?" : "&";
                ret += parameters;
            }
            if (refCurrent)
            {
                ret += (ret.IndexOf('?') == -1) ? "?" : "&";
                ret += "RefTitle=" + System.Web.HttpUtility.UrlEncode(refTitle.EmptyIfNull().ValueIfNull("UnTitled"));
                ret += "&Ref=" + System.Web.HttpUtility.UrlEncode(HttpContext.Current.Request.Url.PathAndQuery);
            }
            return ret;
        }

    }
}