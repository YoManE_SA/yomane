﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FullPage.aspx.cs" Inherits="Netpay.Admin.FullPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#myModal").modal('show');
        }); 
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    Logged in User Info goes here
	<div class="row">
               <div class="col-lg-6">
                   	<div class="wrap-panel">
                           <div class="box-sub-title">
                               <i class="fa fa-code fa-2x box-size-font"></i> CONTENT BOX LEFT
                           </div>
                           <div class="box-content-wrap">
                               <h1>What is lorem ipsum </h1>
                               <p>
                                   Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
								   into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including.
                               </p>
                           </div>
                           </div>
                       </div>
               <div class="col-lg-6">
                   	<div class="wrap-panel">
                           <div class="box-sub-title">
                               <i class="fa fa-code fa-2x box-size-font"></i> CONTENT BOX LEFT
                           </div>
                           <div class="box-content-wrap">
                               <h1>What is lorem ipsum </h1>
                               <p>
                                   Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
								   into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including.
                               </p>
                           </div>
                           </div>
                       </div>        
     </div>
    <div class="row">
        <div class="col-lg-4">
            	<div class="wrap-panel">
            <div class="box-sub-title">
                <i class="fa fa-code fa-2x box-size-font"></i> CONTENT BOX LEFT
            </div>
            <div class="box-content-wrap">
                <h1>What is lorem ipsum </h1>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
					into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including.
                </p>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            	<div class="wrap-panel">
            <div class="box-sub-title">
                <i class="fa fa-code fa-2x box-size-font"></i> CONTENT BOX LEFT
            </div>
            <div class="box-content-wrap">
                <h1>What is lorem ipsum </h1>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
					into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including.
                </p>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            	<div class="wrap-panel">
            <div class="box-sub-title">
                <i class="fa fa-code fa-2x box-size-font"></i> CONTENT BOX LEFT
            </div>
            <div class="box-content-wrap">
                <h1>What is lorem ipsum </h1>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
					into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including.
                </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            	<div class="wrap-panel">
            <div class="box-sub-title">
                <i class="fa fa-code fa-2x box-size-font"></i> CONTENT BOX LEFT
            </div>
            <div class="box-content-wrap">
                <h1>What is lorem ipsum </h1>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
					into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including.
                </p>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            	<div class="wrap-panel">
            <div class="box-sub-title">
                <i class="fa fa-code fa-2x box-size-font"></i> CONTENT BOX LEFT
            </div>
            <div class="box-content-wrap">
                <h1>What is lorem ipsum </h1>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
					into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including.
                </p>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            	<div class="wrap-panel">
            <div class="box-sub-title">
                <i class="fa fa-code fa-2x box-size-font"></i> CONTENT BOX LEFT
            </div>
            <div class="box-content-wrap">
                <h1>What is lorem ipsum </h1>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
					into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including.
                </p>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            	<div class="wrap-panel">
            <div class="box-sub-title">
                <i class="fa fa-code fa-2x box-size-font"></i> CONTENT BOX LEFT
            </div>
            <div class="box-content-wrap">
                <h1>What is lorem ipsum </h1>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
					into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including.
                </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2">
            	<div class="wrap-panel">
            <div class="box-sub-title">
                <i class="fa fa-code fa-2x box-size-font"></i> CONTENT BOX LEFT
            </div>
            <div class="box-content-wrap">
                <h1>What is lorem ipsum </h1>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
					into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including.
                </p>
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            	<div class="wrap-panel">
            <div class="box-sub-title">
                <i class="fa fa-code fa-2x box-size-font"></i> CONTENT BOX LEFT
            </div>
            <div class="box-content-wrap">
                <h1>What is lorem ipsum </h1>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
					into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including.
                </p>
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            	<div class="wrap-panel">
            <div class="box-sub-title">
                <i class="fa fa-code fa-2x box-size-font"></i> CONTENT BOX LEFT
            </div>
            <div class="box-content-wrap">
                <h1>What is lorem ipsum </h1>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
					into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including.
                </p>
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            	<div class="wrap-panel">
            <div class="box-sub-title">
                <i class="fa fa-code fa-2x box-size-font"></i> CONTENT BOX LEFT
            </div>
            <div class="box-content-wrap">
                <h1>What is lorem ipsum </h1>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
					into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including.
                </p>
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            	<div class="wrap-panel">
            <div class="box-sub-title">
                <i class="fa fa-code fa-2x box-size-font"></i> CONTENT BOX LEFT
            </div>
            <div class="box-content-wrap">
                <h1>What is lorem ipsum </h1>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
					into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including.
                </p>
            </div>
                    </div>
        </div>
        <div class="col-lg-2">
            	<div class="wrap-panel">
            <div class="box-sub-title">
                <i class="fa fa-code fa-2x box-size-font"></i> CONTENT BOX LEFT
            </div>
            <div class="box-content-wrap">
                <h1>What is lorem ipsum </h1>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
					into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including.
                </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            	<div class="wrap-table">
            <div class="box-sub-title">
                <i class="fa fa-credit-card fa-2x box-size-font"></i> TRANSACTION TABLE
            </div>
            <div class="box-content-wrap" style="padding: 0;">
                <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Header</th>
                    <th>Header</th>
                    <th>Header</th>
                    <th>Header</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1,001</td>
                    <td>Lorem</td>
                    <td>ipsum</td>
                    <td>dolor</td>
                    <td>sit</td>
                </tr>
                <tr>
                    <td>1,002</td>
                    <td>amet</td>
                    <td>consectetur</td>
                    <td>adipiscing</td>
                    <td>elit</td>
                </tr>
                 <tr>
                    <td>1,003</td>
                    <td>Integer</td>
                    <td>nec</td>
                    <td>odio</td>
                    <td>Praesent</td>
                </tr>
                 <tr>
                    <td>1,003</td>
                    <td>libero</td>
                    <td>Sed</td>
                    <td>cursus</td>
                    <td>ante</td>
                </tr>
                 <tr>
                    <td>1,004</td>
                    <td>dapibus</td>
                    <td>diam</td>
                    <td>Sed</td>
                    <td>nisi</td>
                </tr>
                 <tr>
                    <td>1,005</td>
                    <td>Nulla</td>
                    <td>quis</td>
                    <td>sem</td>
                    <td>at</td>
                </tr>
                 <tr>
                    <td>1,006</td>
                    <td>nibh</td>
                    <td>elementum</td>
                    <td>imperdiet</td>
                    <td>Duis</td>
                </tr>
                 <tr>
                    <td>1,007</td>
                    <td>sagittis</td>
                    <td>ipsum</td>
                    <td>Praesent</td>
                    <td>mauris</td>
                </tr>
                 <tr>
                    <td>1,008</td>
                    <td>Fusce</td>
                    <td>nec</td>
                    <td>tellus</td>
                    <td>sed</td>
                </tr>
                 <tr>
                    <td>1,009</td>
                    <td>augue</td>
                    <td>semper</td>
                    <td>porta</td>
                    <td>Mauris</td>
                </tr>
                 <tr>
                    <td>1,010</td>
                    <td>massa</td>
                    <td>Vestibulum</td>
                    <td>lacinia</td>
                    <td>arcu</td>
                </tr>
                 <tr>
                    <td>1,011</td>
                    <td>eget</td>
                    <td>nulla</td>
                    <td>Class</td>
                    <td>aptent</td>
                </tr>
                 <tr>
                    <td>1,012</td>
                    <td>taciti</td>
                    <td>sociosqu</td>
                    <td>ad</td>
                    <td>litora</td>
                </tr>
                 <tr>
                    <td>1,013</td>
                    <td>torquent</td>
                    <td>per</td>
                    <td>conubia</td>
                    <td>nostra</td>
                </tr>
                 <tr>
                    <td>1,014</td>
                    <td>per</td>
                    <td>inceptos</td>
                    <td>himenaeos</td>
                    <td>Curabitur</td>
                </tr>
                 <tr>
                    <td>1,015</td>
                    <td>sodales</td>
                    <td>ligula</td>
                    <td>in</td>
                    <td>libero</td>
                </tr>
            </tbody>
        </table>
            </div>
                    </div>
        </div>
    </div>
      <div class="row">
         <div class="col-lg-12">
          <ul class="pagination">
            <li class="disabled"><a href="#">&laquo;</a></li>
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">&raquo;</a></li>
         </ul>
        </div>
     </div>
    <div class="row">
      <div class="col-lg-6">
            <div class="filter">
                <section class="margin-bottom-10">
                    <div class="head-filter">
                        <div class="pull-left">Credit Type</div>
                        <div class="pull-right"><i class="fa fa-angle-down"></i></div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="content-filter">
                        <asp:CheckBoxList ClientIDMode="Static" ID="Chklist" runat="server" CellPadding="30" CellSpacing="30" RepeatLayout="Table" RepeatColumns="2" Style="width: 300px;">
                            <asp:ListItem>Refund</asp:ListItem>
                            <asp:ListItem>Regular</asp:ListItem>
                            <asp:ListItem>Delayed Charge</asp:ListItem>
                            <asp:ListItem>Credit Charge</asp:ListItem>
                            <asp:ListItem>Installments</asp:ListItem>
                            <asp:ListItem>Debit</asp:ListItem>
                            <asp:ListItem>Credit</asp:ListItem>
                            <asp:ListItem>Unknown</asp:ListItem>
                        </asp:CheckBoxList>
                    </div>
                </section>
                <section class="margin-bottom-10">
                    <div class="head-filter">
                        <div class="pull-left">Amount & Currency</div>
                        <div class="pull-right"><i class="fa fa-angle-down"></i></div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="content-filter">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                    <input type="text" class="form-control" placeholder="Date from">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                    <input type="text" class="form-control" placeholder="Date from">
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label for="inputEmail">Email</label>
                                    <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label for="inputPassword">Password</label>
                                    <input type="text" class="form-control" id="inputName" placeholder="Payer Name">
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label for="inputPassword">Currency</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Currency">
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                            <ul class="dropdown-menu pull-right">
                                                <li><a href="#">USD</a></li>
                                                <li><a href="#">GBP</a></li>
                                                <li><a href="#">ILS</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label for="inputEmail">Method 4</label>
                                    <input type="text" class="form-control" id="inputEnvelope" placeholder="Method 4">
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label for="inputPassword">Method 6</label>
                                    <input type="text" class="form-control" id="inputFirst" placeholder="Method 6">
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label for="inputPayment">Payment Method</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Method">
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                            <ul class="dropdown-menu pull-right">
                                                <li><a href="#">AMEX</a></li>
                                                <li><a href="#">CAL</a></li>
                                                <li><a href="#">ILS</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="margin-bottom-10">
                    <div class="head-filter">
                        <div class="pull-left">Terminal</div>
                        <div class="pull-right"><i class="fa fa-angle-down"></i></div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="content-filter">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                    <input type="text" class="form-control" placeholder="Username">
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Amount">
                                    <span class="input-group-addon">.00</span>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input type="text" class="form-control" placeholder="USD">
                                    <span class="input-group-addon">.00</span>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-user"></span></span>
                            <input type="text" class="form-control" placeholder="Username">
                        </div>
                        <br>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Amount">
                            <span class="input-group-addon">.00</span>
                        </div>
                        <br>
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="text" class="form-control" placeholder="US Dollar">
                            <span class="input-group-addon">.00</span>
                        </div>
                    </div>
                </section>
                <section class="margin-bottom-10">
                    <div class="head-filter">
                        <div class="pull-left">Payment Method</div>
                        <div class="pull-right"><i class="fa fa-angle-right"></i></div>
                        <div class="clearfix"></div>
                    </div>
                </section>
                <section class="margin-bottom-10">
                    <div class="head-filter">
                        <div class="pull-left">Refernce</div>
                        <div class="pull-right"><i class="fa fa-angle-right"></i></div>
                        <div class="clearfix"></div>
                    </div>
                </section>
                <section class="margin-bottom-10">
                    <div class="head-filter">
                        <div class="pull-left">Debit Company</div>
                        <div class="pull-right"><i class="fa fa-angle-right"></i></div>
                        <div class="clearfix"></div>
                    </div>
                </section>
                <section class="text-right">
                    <a href="#" class="btn btn-primary"><i class="fa fa-search"></i> Search</a>
                </section>
            </div>
            <div class="wrap_search_filter">
                <div class="search_filter"><i class="fa fa-chevron-right"></i></div>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-lg-12">
        <div class="notification attention">
            <div>
                Attention notification. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vulputate, sapien quis fermentum luctus, libero. 
            </div>
        </div>
        </div>
        <div class="col-lg-12">
        <div class="notification success">
            <div>
                Attention notification. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vulputate, sapien quis fermentum luctus, libero. 
            </div>
        </div>
            </div>
        <div class="col-lg-12">
        <div class="notification error">
            <div>
                Attention notification. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vulputate, sapien quis fermentum luctus, libero. 
            </div>
        </div>
        </div>
        <div class="col-lg-12">
        <div class="notification information">
            <div>
                Attention notification. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vulputate, sapien quis fermentum luctus, libero. 
			
            </div>
        </div>
            </div>
   </div>
   <div class="row">
       <div class="col-lg-12">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#sectionA">Section A</a></li>
            <li><a data-toggle="tab" href="#sectionB">Section B</a></li>
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">Dropdown <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a data-toggle="tab" href="#dropdown1">Dropdown1</a></li>
                    <li><a data-toggle="tab" href="#dropdown2">Dropdown2</a></li>
                </ul>
            </li>
        </ul>
        <div class="tab-content">
            <div id="sectionA" class="tab-pane fade in active">
                <h3>Section A</h3>
                <p>Aliquip placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui. Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth.</p>
            </div>
            <div id="sectionB" class="tab-pane fade">
                <h3>Section B</h3>
                <p>Vestibulum nec erat eu nulla rhoncus fringilla ut non neque. Vivamus nibh urna, ornare id gravida ut, mollis a magna. Aliquam porttitor condimentum nisi, eu viverra ipsum porta ut. Nam hendrerit bibendum turpis, sed molestie mi fermentum id. Aenean volutpat velit sem. Sed consequat ante in rutrum convallis. Nunc facilisis leo at faucibus adipiscing.</p>
            </div>
            <div id="dropdown1" class="tab-pane fade">
                <h3>Dropdown 1</h3>
                <p>WInteger convallis, nulla in sollicitudin placerat, ligula enim auctor lectus, in mollis diam dolor at lorem. Sed bibendum nibh sit amet dictum feugiat. Vivamus arcu sem, cursus a feugiat ut, iaculis at erat. Donec vehicula at ligula vitae venenatis. Sed nunc nulla, vehicula non porttitor in, pharetra et dolor. Fusce nec velit velit. Pellentesque consectetur eros.</p>
            </div>
            <div id="dropdown2" class="tab-pane fade">
                <h3>Dropdown 2</h3>
                <p>Donec vel placerat quam, ut euismod risus. Sed a mi suscipit, elementum sem a, hendrerit velit. Donec at erat magna. Sed dignissim orci nec eleifend egestas. Donec eget mi consequat massa vestibulum laoreet. Mauris et ultrices nulla, malesuada volutpat ante. Fusce ut orci lorem. Donec molestie libero in tempus imperdiet. Cum sociis natoque penatibus et magnis dis parturient.</p>
            </div>
        </div>
      </div>
   </div>
   <div class="row">
       <div class="col-lg-12">
       <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">1. What is Netpay International?</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <p>HTML stands for HyperText Markup Language. HTML is the main markup language for describing the structure of Web pages. <a href="http://www.tutorialrepublic.com/html-tutorial/" target="_blank">Learn more.</a></p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">2. What is Netpay International?</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Twitter Bootstrap is a powerful front-end framework for faster and easier web development. It is a collection of CSS and HTML conventions. <a href="http://www.tutorialrepublic.com/twitter-bootstrap-tutorial/" target="_blank">Learn more.</a></p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">3. What is Netpay International?</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>CSS stands for Cascading Style Sheet. CSS allows you to specify various style properties for a given HTML element such as colors, backgrounds, fonts etc. <a href="http://www.tutorialrepublic.com/css-tutorial/" target="_blank">Learn more.</a></p>
                </div>
            </div>
        </div>
    </div>
       </div>
  </div>
   <div class="row">
           <div class="col-lg-12">
            <div class="progress progress-striped">
                <div class="progress-bar" style="width: 60%;">
                    <span class="sr-only">60% Complete</span>
                </div>
            </div>
            </div>
       </div>
 
   <div class="row">
        <div class="col-lg-12">
            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Products</a></li>
                <li class="active">Accessories</li>
            </ul>
       </div>
   </div>
   <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">301 Moved Permanently</h3>
                    </div>
                    <div class="panel-body">The requested page has been permanently moved to a new location.</div>
                </div>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">200 OK</h3>
                </div>
                <div class="panel-body">The server successfully processed the request.</div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">100 Continue</h3>
                </div>
                <div class="panel-body">The client should continue with its request.</div>
            </div>
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title">400 Bad Request</h3>
                </div>
                <div class="panel-body">The request cannot be fulfilled due to bad syntax.</div>
            </div>
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">503 Service Unavailable</h3>
                </div>
                <div class="panel-body">The server is temporarily unable to handle the request.</div>
            </div>
         </div>
       </div>

</asp:Content>
