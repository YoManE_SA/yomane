﻿function DataTableController_ChangeMode(clientId, toMode)
{
    var imagePath = '../../../Images/';
    switch(toMode){
        case 0:
            if ($('#' + clientId + '_FilterTd').css('display') != 'none'){
                DataTableController_ChangeMode(clientId, 1);
                return;
            }
            $('#' + clientId + '_FilterTd').show('fade');
            $('#' + clientId + '_FilterTdSeperatorImage').attr('src', imagePath + 'DataTableArrowRight.png');
            $('#' + clientId + '_ListTd').removeClass('col-lg-6 col-lg-8 col-lg-12').addClass('col-lg-8').show('fade');
            $('#' + clientId + '_ContentTd').hide('fade');
            $('#' + clientId + '_ContentTdSeperatorImage').attr('src', 'DataTableArrowRight.png');
            break;
        case 1:
            $('#' + clientId + '_FilterTd').hide('fade');
            $('#' + clientId + '_FilterTdSeperatorImage').attr('src', imagePath + 'DataTableArrowLeft.png');
            $('#' + clientId + '_ListTd').removeClass('col-lg-6 col-lg-8 col-lg-12').addClass('col-lg-12').show('fade');
            $('#' + clientId + '_ContentTd').hide('fade');
            $('#' + clientId + '_ContentTdSeperatorImage').attr('src', imagePath + 'DataTableArrowRight.png');
            break;
        case 2:
            $('#' + clientId + '_FilterTd').hide('fade');
            $('#' + clientId + '_ListTd').removeClass('col-lg-6 col-lg-8 col-lg-12').addClass('col-lg-6').show('fade');
            $('#' + clientId + '_ContentTdSeperatorTd').show('fade');
            $('#' + clientId + '_ContentTd').removeClass('col-lg-4 col-lg-6 col-lg-12').addClass('col-lg-6').show('fade');
            //$('#' + clientId + '_ContentTd .tab-data').removeClass('FlexContainer');
            //$('#' + clientId + '_ContentTd .PageCon-Flexible').removeClass('FormSectionPC').addClass('row');
            $('#' + clientId + '_ContentTdSeperatorImage').attr('src', imagePath + 'DataTableArrowRight.png');
            $('#' + clientId + '_FilterTdSeperatorImage').attr('src', imagePath + 'DataTableArrowLeft.png');
            DataTableController_Remove2Columns(clientId);
            break;
        case 3: //full screen
            if ($('#' + clientId + '_ListTd').css('display') == 'none' || $('#' + clientId + '_FilterTd').css('display') != 'none'){
                DataTableController_ChangeMode(clientId, 2);
                return;
            }
            $('#' + clientId + '_FilterTd').hide('fade');
            $('#' + clientId + '_ListTd').hide('fade');
            $('#' + clientId + '_ContentTdSeperatorTd').show('fade');
            $('#' + clientId + '_ContentTd').removeClass('col-lg-4 col-lg-6 col-lg-12').addClass('col-lg-12').show('fade');
            //$('#' + clientId + '_ContentTd .tab-data').addClass('FlexContainer');
            //$('#' + clientId + '_ContentTd .PageCon-Flexible').addClass('FormSectionPC').removeClass('row');
            $('#' + clientId + '_ContentTdSeperatorImage').attr('src', imagePath + 'DataTableArrowLeft.png');
            $('#' + clientId + '_FilterTdSeperatorImage').attr('src', imagePath + 'DataTableArrowLeft.png');
            DataTableController_Create2Columns(clientId);
            break;
    }
    var notifyProc = window[clientId + '_Change'];
    if (notifyProc) notifyProc(clientId, toMode);
}

function DataTableController_Create2Columns(clientId)
{
    $('#' + clientId + '_ContentTd .tab-pane').each(function (tIndex) {
        var row = $('<div class="DataController-row row"></div>')
        var c1 = $('<div class="DataController-column col-lg-6" style="padding-right:22px;"></div>');
        var c2 = $('<div class="DataController-column col-lg-6" style="padding-left:22px;"></div>');
        row.append(c1); row.append(c2);
        $(this).find('.PageCon-Flexible').each(function (pIndex) {
            var element = $(this).detach();
            $(pIndex % 2 ? c2 : c1).append(element);
        });
        $(this).append(row);
    });
}

function DataTableController_Remove2Columns(clientId)
{
    $('#' + clientId + '_ContentTd .tab-pane').each(function (tIndex) {
        var tabObj = $(this);
        $(this).find('.DataController-column').each(function (pindex) {
            $(this).find('.PageCon-Flexible').each(function (cIndex) {
                var revItems = tabObj.children().eq((cIndex * (pindex + 1)) + pindex);
                if (revItems == null) tabObj.append($(this).detach());
                else $(this).detach().insertAfter(revItems);
            });
        });
    });
    $('#' + clientId + '_ContentTd .tab-pane .DataController-row').remove();
}

function CreateTwoColumnsIfNeeded(clientId) {
    if ($('#' + clientId + '_ListTd').css('display') == 'none' && $('#' + clientId + '_FilterTd').css('display') == 'none')
    {
        $('#' + clientId + '_ContentTd .tab-pane').each(function (tIndex) {
            var row = $('<div class="DataController-row row"></div>')
            var c1 = $('<div class="DataController-column col-lg-6" style="padding-right:22px;"></div>');
            var c2 = $('<div class="DataController-column col-lg-6" style="padding-left:22px;"></div>');
            row.append(c1); row.append(c2);
            $(this).find('.PageCon-Flexible').each(function (pIndex) {
                var element = $(this).detach();
                $(pIndex % 2 ? c2 : c1).append(element);
            });
            $(this).append(row);
        });
    }
}
    