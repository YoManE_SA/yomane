﻿$(function () { AppUpdatePanel.init(); });

var AppUpdatePanel =
{
	init: function () {
		var _callQueue = new Array();
		var _prm = Sys.WebForms.PageRequestManager.getInstance();

		_prm.add_initializeRequest(initializeRequest);
		_prm.add_beginRequest(BeginRequestHandler);
		_prm.add_endRequest(EndRequestHandler);
		_prm.add_pageLoaded(PageLoadedHandler);

		function getUpdatePanels(sender) {
			var panelsUpdate = null;
			var pbItem = sender._activeElement;
			while (pbItem) {
				if (pbItem.id) {
					panelsUpdate = window['PanelsUpdate_' + pbItem.id];
					if (panelsUpdate) break;
				}
				pbItem = pbItem.parentNode;
			}
			return panelsUpdate;
		}

		function initializeRequest(sender, args) {
			var target = sender._postBackSettings.asyncTarget;
			if (target == _callQueue[0]) return;
			Array.enqueue(_callQueue, target);
			if (_callQueue.length > 1) args.set_cancel(true);
		}

		function EndRequestHandler(sender, args) {
			Array.dequeue(_callQueue);
			if (_callQueue.length > 0)
				_prm._doPostBack(_callQueue[0], '');
		}

		function BeginRequestHandler(sender, args) {
			var panelsUpdate = getUpdatePanels(sender);
			if (!panelsUpdate) return;
			for (var i = 0; i < panelsUpdate.length; i++) {
				panelToShadow = $('#' + panelsUpdate[i]);
				if (panelToShadow.attr('ShowShadow') == 'true') {
					var panelShadow = document.getElementById(panelsUpdate[i] + '_updatePanelShadow');
					if (!panelShadow) {
						panelShadow = document.createElement('div');
						panelShadow.id = panelsUpdate[i] + '_updatePanelShadow';
						panelShadow.className = 'UpdatePanelShadow';
						document.body.appendChild(panelShadow);
						panelShadow = $(panelShadow);

						panelShadow.width(panelToShadow.width());
						panelShadow.height(panelToShadow.height());
						panelShadow.position({ my: 'right top', at: "right top", of: panelToShadow });

					} else panelShadow = $(panelShadow);
					panelShadow.fadeIn();
				}
				if (panelToShadow.attr('NoStartAnimation') == 'true') return;

				if (panelToShadow.attr('Animation') == 'Slide') panelToShadow.slideUp();
				else if (panelToShadow.attr('Animation') == 'Fade') panelToShadow.fadeOut();
			}
		}

		function PageLoadedHandler(sender, args) {
			var panelsUpdate = args._panelsUpdated;  //getUpdatePanels(sender);
			if (!panelsUpdate) return;
			for (var i = 0; i < panelsUpdate.length; i++) {
				panelToShadow = $(panelsUpdate[i]);
				if (panelToShadow.attr('Animation') == 'Slide') panelToShadow.slideUp().finish().slideDown();
				else if (panelToShadow.attr('Animation') == 'Fade') panelToShadow.fadeOut().finish().fadeIn();
				var panelShadow = document.getElementById(panelsUpdate[i] + '_updatePanelShadow');
				if (panelShadow) panelShadow.fadeOut();
			}
		}

	}
}