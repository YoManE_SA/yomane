﻿function ApplicationIdentities_Filter(id, elementid,elementname) {
    var tmr = false;
    var list = document.getElementById(elementname + elementid).getElementsByTagName('li');
    if (tmr) 
        clearTimeout(tmr);
    tmr = setTimeout(function () {
        var regx = new RegExp(document.getElementById(id).value, 'i');
        for (var i = 0, size = list.length; i < size; i++)
            if (document.getElementById(id).value.length > 0) {
                if (list[i].getElementsByTagName('label')[0].textContent.match(regx, "i")) setItemVisibility(i, true);
                else setItemVisibility(i, false);
            }
            else
                setItemVisibility(i, true);
    }, 500);

    function setItemVisibility(position, visible) {
        if (visible) {
            list[position].style.display = 'inline-block';

        }
        else {
            list[position].style.display = 'none';
        }
    }
};
