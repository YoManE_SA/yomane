function tiny_mce_IZWebFileManager(field_name, url, type, win) 
{
    //alert("Field_Name: " + field_name + "nURL: " + url + "nType: " + type + "nWin: " + win); // debug/testing    
    var cmsURL = document.location.href.substring(0, document.location.href.lastIndexOf('/')) + '/FileManager.aspx?IsDialog=true';    // script URL - use an absolute path!    
    if (cmsURL.indexOf("?") < 0) cmsURL = cmsURL + "?type=" + type;
    else cmsURL = cmsURL + "&type=" + type;
    tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'File Browser',
        width : 600,  // Your dimensions may differ - toy around with them!
        height : 550,
        resizable : "no",
        inline : "yes", // This parameter only has an effect if you use the inlinepopups plugin!        
        close_previous: "no"
    }, {
        window: win,
        input: field_name 
    });
    return false;  
}