﻿   function adminFileManagerSelectedFilesHandler(input, maxFileUploadSizeBytes) {

       var Msg = "";
       var fileSize = 0;
       for (var i = 0; i < input.files.length; i++)
       {
           Msg=Msg+input.files[i].name +" ; ";
           fileSize=fileSize+input.files[i].size;     
       }
       if (fileSize > maxFileUploadSizeBytes)
       {   alert("Error: File Exceeds Size limit , reduce number of files or their size ");
           Msg = "";
           $('#BodyContent_ctl01_btnUpload')[0].disabled = true;   
       }
       else { $('#BodyContent_ctl01_btnUpload')[0].disabled = false; }

       $('.form-control')[2].placeholder = Msg;
   }

   function AdminDocsSelectedFilesHandler(input, maxFileUploadSizeBytes) {
       var Msg = "";
       var fileSize = 0;
       for (var i = 0; i < input.files.length; i++) {
           Msg = Msg + input.files[i].name + " ; ";
           fileSize = fileSize + input.files[i].size;
       }
       if (fileSize > maxFileUploadSizeBytes) {
           alert("Error: File Exceeds Size limit , reduce number of files or their size ");
           Msg = "";
           $('#btnUpload')[0].disabled = true;
       }
       else { $('#btnUpload')[0].disabled = false; }

       $('#fileNames')[0].placeholder = Msg;
   }

   function adminFileEmailSelectedFilesHandler(input, maxFileUploadSizeBytes) {

       var Msg = "";
       var fileSize = 0;
       for (var i = 0; i < input.files.length; i++) {
           Msg = Msg + input.files[i].name + " ; ";
           fileSize = fileSize + input.files[i].size;
       }
       if (fileSize > maxFileUploadSizeBytes)
       {
           alert("Error: File Exceeds Size limit , reduce number of files or their size ");
           Msg = "";
       }
      

       $('#BodyContent_PageController_FormView_FormPanel_ctl01_upAttachments .form-control')[0].value= Msg;
   }
