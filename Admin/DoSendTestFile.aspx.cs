﻿using Netpay.Bll.BankHandler;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Netpay.Admin
{
    public partial class DoSendTestFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        { 

            Infrastructure.ObjectContext.Current.Impersonate(Infrastructure.Domain.Current.ServiceCredentials);
        }

        protected void btnTestFileGeneration_Click(object sender, EventArgs e)
        {
            try
            {
                var ibb = new Iso8583BankHandlerBase(Netpay.Infrastructure.DebitCompany.eCentric, "test");
                ibb.GenerateFile();
            }
            catch (Exception ex)
            {
                lblOutput.Text = ex.ToString();
            }
        }

        protected void btnKickOffWinServices_Click(object sender, EventArgs e)
        {
            try
            {
                Netpay.Bll.Application.Init();
                Netpay.Process.Application.Init();
                Monitoring.Application.Init();
                SetEnable(true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }


        

        private static void SetEnable(bool bEnable)
        {
            System.Diagnostics.Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.High;

            //Start monitors
            AuthHandlerManager.Current.EnableFilesMonitor = bEnable;
            AuthHandlerManager.Current.EnableGenerateTimer = bEnable;
            ChbHandlerManager.Current.EnableFilesMonitor = bEnable;
            EpaHandlerManager.Current.EnableFilesMonitor = bEnable;
            FraudHandlerManager.Current.EnableFilesMonitor = bEnable;

            FNBAuthHandler.EnableRealTimeFetch = bEnable;
            Netpay.Bll.Wires.WiresBatch.EnableRealTimeSend = bEnable;
            Netpay.Infrastructure.Domain.EnableClearTempDirectory = bEnable;

            Netpay.Bll.ThirdParty.eCentric.EnableServers(bEnable);
            Netpay.Bll.ThirdParty.ACS.EnableServers(bEnable);
        }

        protected void btnEndWindowsServices_Click(object sender, EventArgs e)
        {
            try
            { 
                SetEnable(false);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        protected void btnECentric_Click(object sender, EventArgs e)
        {
            try
            {
                Netpay.Bll.BankHandler.eCentricAuthHandler x = new eCentricAuthHandler();
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}