﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CreateQrWizard.aspx.vb" Inherits="Netpay.MerchantControlPanel.CreateQrWizard" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript"  src="../../../Website/Plugins/jquery-ui-1.8.10.custom/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="Plugins/QrWizard/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="Plugins/liteAccordion/liteaccordion.jquery.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300,700' rel='stylesheet' type='text/css' />
    <link href="Styles/liteaccordion.css" rel="stylesheet" />
 
 <script type="text/javascript">
 $(document).keydown(function(objEvent) {
    if (objEvent.keyCode == 9) { 
        objEvent.preventDefault();
    }
})
</script>
</head>
<body style="padding:none;margin:none;">
    <form id="aspnetForm" runat="server" style="padding:none;margin:none;">
		<asp:MultiView runat="server" ID="mvView" ActiveViewIndex="0">
			<asp:View runat="server" ID="viewWizard">
				<asp:ValidationSummary runat="server" ID="vsValidationSummary" DisplayMode="BulletList" ShowMessageBox="true" ShowSummary="false" HeaderText="The following fields need to be fixed:" />
				<asp:RequiredFieldValidator runat="server" ControlToValidate="txtText" ErrorMessage="<%$Resources: CreateQrWizard.aspx, Textisrequired %>" Display="None" />
				<asp:RequiredFieldValidator runat="server" ControlToValidate="txtAmount" ErrorMessage="<%$Resources: CreateQrWizard.aspx, Amountrequired %>" Display="None" />
				<asp:RegularExpressionValidator runat="server" ControlToValidate="txtAmount" ValidationExpression="^\d+(\.\d+)?" ErrorMessage="<%$Resources: CreateQrWizard.aspx, Amountrequired %>" Display="None" />
				<asp:RequiredFieldValidator runat="server" ControlToValidate="ddlCurrency" ErrorMessage="<%$Resources: CreateQrWizard.aspx, Currencyrequired %>" Display="None" />
				<asp:RequiredFieldValidator runat="server" ControlToValidate="txtQtyAvailable" ErrorMessage="<%$Resources: CreateQrWizard.aspx, Availablerequired %>" Display="None" />
				<asp:RequiredFieldValidator runat="server" ControlToValidate="txtQtyStart" ErrorMessage="<%$Resources: CreateQrWizard.aspx, Startrequired %>" Display="None" />
				<asp:RegularExpressionValidator runat="server" ControlToValidate="txtQtyStart" ValidationExpression="^\d+" ErrorMessage="<%$Resources: CreateQrWizard.aspx, Startrequired %>" Display="None" />
				<asp:RequiredFieldValidator runat="server" ControlToValidate="txtQtyEnd" ErrorMessage="<%$Resources: CreateQrWizard.aspx, Quantityrequired %>" Display="None" />
				<asp:RegularExpressionValidator runat="server" ControlToValidate="txtQtyEnd" ValidationExpression="^\d+" ErrorMessage="<%$Resources: CreateQrWizard.aspx, Quantityrequired %>" Display="None" />
				<asp:RequiredFieldValidator runat="server" ControlToValidate="txtQtyStep" ErrorMessage="<%$Resources: CreateQrWizard.aspx, Steprequired %>" Display="None" />
				<asp:RegularExpressionValidator runat="server" ControlToValidate="txtQtyStep" ValidationExpression="^\d+" ErrorMessage="<%$Resources: CreateQrWizard.aspx, Steprequired %>" Display="None" />
				<div id="one">
					<ol>
						<li>
							<h2><span>Create SQR</span></h2>
							<div>
								 <figure>
								  <img src="../Templates/Tmp_MahalaUS/Images/QrWizard/1.jpg" alt="image" />
								 <figcaption class="ap-caption" onclick="$('#one').liteAccordion('next');">Next >></figcaption>
								</figure>
							</div>
						</li>
						<li>
							<h2><span>Insert Product</span></h2>
							<div>
								<figure>

									<div class="slide-one-wrap">
										<div class="spacing">
											<h1>Insert Product</h1>
											<div class="slide-one-form">
												<table>
                                                	<tr>
														<td>Language:</td>
														<td>
														   <asp:DropDownList runat="server" ID="ddlLanguage" CssClass="field_230" AppendDataBoundItems="true">
															<asp:ListItem Text="Show in all languages" Value="" />
														   </asp:DropDownList>
														</td>
													</tr>
													<tr>
														<td>Product Name: </td>
														<td><asp:TextBox runat="server" id="txtText" CssClass="field_230" required="required" MaxLength="28"  /></td>
													</tr>
													<tr>
														<td>SKU: </td>
														<td><asp:TextBox runat="server" id="txtSKU" CssClass="field_230" required="required"   /></td>
													</tr>
													<tr>
														<td>Description:</td>
														<td><asp:TextBox runat="server" id="txtDescription" class="field-text_230" TextMode="MultiLine" Rows="3" /><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtDescription" ErrorMessage="The notes has exceeded 145 maximum length." Display="Dynamic"  ValidationExpression=".{0,145}" >*</asp:RegularExpressionValidator></td>
													</tr>
                                                    <tr>
                                                   <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtDescription" ErrorMessage="The notes has exceeded 145 maximum length." Display="Dynamic"  ValidationExpression=".{0,145}" >*</asp:RegularExpressionValidator>
                                                   </tr>
												
												</table>
											</div>
										</div>
									</div>
									<figcaption class="ap-caption" onclick="$('#one').liteAccordion('next');">Next >></figcaption>
								</figure>
							</div>
						</li>
						<li>
							<h2><span>Insert Price</span></h2>
							<div>
								<figure>
									<div class="slide-one-wrap">
										<div class="spacing">
											<h1>Insert Price</h1>
											<div class="slide-one-form">
												<table>
													<tr>
														<td>Price: </td>
														<td><asp:TextBox runat="server" ID="txtAmount" CssClass="field_105" required="required"  /></td>
													</tr>
													<tr>
														<td>Currency: </td>
														<td><netpay:CurrencyDropDown runat="server" ID="ddlCurrency" CssClass="field_115" /></td>
													</tr>
													<tr>
														<td>Installments:</td>
														<td><netpay:NumericDropDown runat="server" id="ddlInstallments" CssClass="field_115" MinValue="1" MaxValue="12" /></td>
													</tr>
												</table>
											</div>
										</div>
									</div>
									<figcaption class="ap-caption" onclick="$('#one').liteAccordion('next');" >Next >></figcaption>
								</figure>
							</div>
						</li>
						<li>
							<h2><span>Stock Managment</span></h2>
							<div>
								<figure>
									<div class="slide-one-wrap">
										<div class="spacing">
											<h1>Stock Managment</h1>
											<div class="slide-one-form">
												<table>
													<tr>
														<td>Available : </td>
														<td><asp:TextBox runat="server" CssClass="field_75" onkeydown="return IsKeyDigit();" id="txtQtyAvailable" /></td>
													</tr>
													<tr>
													
														<td >
															Item Quantity From:</td>
                                                            <td>
															<asp:TextBox runat="server" CssClass="field_75"  onkeydown="return IsKeyDigit();" id="txtQtyStart" title="Min" />
															&nbsp;&nbsp;To:&nbsp;
															<asp:TextBox runat="server" CssClass="field_75" onkeydown="return IsKeyDigit();" id="txtQtyEnd" title="Max" />
														</td>
													</tr>
													<tr>
														<td>Step:</td>
														<td><asp:TextBox runat="server" CssClass="field_75" onkeydown="return IsKeyDigit();" id="txtQtyStep" title="Step" /></td>
													</tr>
												</table>
											</div>
										</div>
									</div>
									<figcaption class="ap-caption" onclick="$('#one').liteAccordion('next');" >Next >></figcaption>
								</figure>
							</div>
						</li>
						<li>
							<h2><span>Upload image</span></h2>
							<div>
								<figure>
									<div class="slide-one-wrap">
										<div class="spacing">
											<h1>Upload Image</h1>
											<div class="slide-one-form">
												<table>
													<tr>
														<td>Image: </td>
														<td>
															<asp:FileUpload ID="ImageUpload1" runat="server" />
                                                        </td> 
                                                    </tr>
													<tr>
														<td colspan="2">
															<h3>Image Recommended Size:</h3>
															1200 (px) Width<br />
															371 (px) Height  
														</td>
													</tr>
												</table>
											</div>
										</div>
									</div>
									<asp:LinkButton runat="server" style="text-decoration: none;" CssClass="ap-caption" onClick="CreateItem" Text="Next >>" />
								</figure>
							</div>
						</li>
					</ol>
				</div>
				<script>
					(function ($, d) {
						$('#one').liteAccordion({
							onTriggerSlide: function () {
								this.find('figcaption').fadeOut();
							},
							onSlideAnimComplete: function () {
								this.find('figcaption').fadeIn();
							},
							autoPlay: false,
							pauseOnHover: true,
							theme: 'stitch',
							rounded: true,
							enumerateSlides: true,
							linkable: true
						}).find('figcaption:first').show();
					})(jQuery, document);
				</script>
			</asp:View>
			<asp:View runat="server" ID="viewResult">
				<div id="frame-five">
					<div class="frame-five-left">
						<h1>Generate SQR</h1>
						<div class="copylink">
							COPY LINK
							<img src="../Templates/Tmp_MahalaUS/Images/QrWizard/arrow.png" alt="SQR-CODE" />
						</div>
						<div>
							<asp:TextBox ID="txtLink1" ClientIDMode="Static" runat="server" CssClass="link-input" />
						</div>
						<div class="clipboard">
							<a href="javascript:document.getElementById('txtLink1').select();alert('use Ctrl C to copy the link');/*window.clipboardData.setData('Text', document.getElementById('txtLink1').value);void();*/"><img src="../Templates/Tmp_MahalaUS/Images/QrWizard/Copy-File.png" />COPY TO CLIPBOARD</a>
						</div>
					</div>
					<div class="frame-five-right">
						<div class="qrcode">
							<asp:Image runat="server" ID="imgQrCode" ClientIDMode="Static" ImageUrl="../Templates/Tmp_MahalaUS/Images/QrWizard/qrcode.png" Width="168" />
						</div>
						<div class="sqr-download">
							<a target="_blank" onclick="this.href=document.getElementById('imgQrCode').src;" href="#" >DOWNLOAD SQR </a>
							<img src="../Templates/Tmp_MahalaUS/Images/QrWizard/arrow-top.png" alt="SQR-CODE" />
						</div>
						<div class="create-sqr"><asp:LinkButton runat="server" Text="START AGAIN >>" ID="btnStartAgain" OnClick="StartAgain_Click" /></div>
					</div>
				</div>
			</asp:View>
		</asp:MultiView>
    </form>
    <script type="text/javascript" src="js/netpay.merchantControlPanel.Page.js"></script>
</body>
</html>
