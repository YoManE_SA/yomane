﻿Imports Netpay.Web
Imports Netpay.Infrastructure 

Public Class CreateQrWizard
    Inherits IFramePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then 
			Dim hppLanguages = System.Configuration.ConfigurationManager.AppSettings("HppV2Languages").Split(",")
			Dim allLanguages = Bll.International.Language.Cache.Where(function(l) hppLanguages.Contains(l.Culture)).ToList()
			ddlLanguage.DataTextField = "Name"
			ddlLanguage.DataValueField ="ID"
			ddlLanguage.DataSource = allLanguages
			ddlLanguage.DataBind()
		End If
    End Sub

	Public Sub CreateItem(ByVal sender As Object, ByVal e As System.EventArgs)
		Page.Validate()
		If Not Page.IsValid Then Exit Sub
        Dim value = New Bll.Shop.Products.Product(Merchant.ID)
		
		value.SKU = txtSKU.Text
		value.Price = txtAmount.Text.ToDecimal(0)
		value.CurrencyISOCode = ddlCurrency.SelectedValue.ToInt32(0)
		value.Installments = ddlInstallments.SelectedValue.ToInt32(0)
		value.QtyStart = txtQtyStart.Text.ToInt32(1)
		value.QtyEnd = txtQtyEnd.Text.ToInt32(1)
		value.QtyStep = txtQtyStep.Text.ToInt32(1)
		value.QtyAvailable = txtQtyAvailable.Text.ToNullableInt32()
		'value.IsAuthorize = CreditType.Debit
		value.IsActive = True
		If ImageUpload1.HasFile Then
			value.ImageFileName = Bll.Accounts.Account.Current.AccountNumber & "_PP_" & DateTime.Now.Ticks & System.IO.Path.GetExtension(ImageUpload1.FileName)
            ImageUpload1.SaveAs(value.ImagePhysicalPath)
		End If
		value.Save()
		Dim text = New Bll.Shop.Products.ProductText(value.ID, ddlLanguage.SelectedValue)
		text.Name = txtText.Text
		text.Description = txtDescription.Text
		text.Save()
		
		Dim pppParams As String = "merchantID=" & Bll.Accounts.Account.Current.AccountNumber & "&item=" & value.ID
		imgQrCode.ImageUrl = String.Format("{0}QrCodes.asmx/RenderGenerateCode?target=PublicPaymentPageV2&urlParams={1}", WebUtils.CurrentDomain.WebServicesUrl, HttpUtility.UrlEncode(pppParams))
		txtLink1.Text = WebUtils.CurrentDomain.ProcessV2Url + "Public/?merchantID=" & Bll.Accounts.Account.Current.AccountNumber & "&item=" & value.ID
		mvView.SetActiveView(viewResult)
	End Sub

	Public Sub StartAgain_Click(ByVal sender As Object, ByVal e As System.EventArgs)
		mvView.SetActiveView(viewWizard)
	End Sub
End Class