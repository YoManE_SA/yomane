﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports.Master" AutoEventWireup="True" CodeBehind="TransactionsReport.aspx.cs" Inherits="Netpay.AdminReports.TransactionsReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        div.innerContent {
            width: 100%;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            jQuery("input[type='checkbox'][id^='ctl00_body_cbGroupBy']").button();
        });

        function validate() {
            return netpay.webControls.DateRangePicker.validate();
        }

        function checkGroups() {
            // disable go button if no group selected
            var isNotGroupChecked = jQuery("input:checkbox[id^='ctl00_body_cbGroupBy']:checked").length == 0;
            //jQuery("#ctl00_body_btnGo").button( "option", "disabled", isGroupChecked);
            jQuery("#ctl00_body_btnGo").attr("disabled", isNotGroupChecked);
        }

        function rowHighlight(rowElementID) {
            jQuery(rowElementID).css("background-color", "#FFFFAA");
        }

        function rowNormal(rowElementID, normalBackgroundColor) {
            jQuery(rowElementID).css("background-color", normalBackgroundColor);
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <table style="width: 100%;">
        <tr>
            <td class="pageMainHeading">Transactions report</td>
            <td style="text-align: right;">
                <netpay:ReportUpdateInfo ID="wcReportUpdateInfo" runat="server" Report="AdminTransactionsReport" />
                &nbsp;|&nbsp;
				<asp:Image ImageUrl="Images/ImgFileExt/xls.gif" runat="server" />
                <asp:LinkButton ID="lnkBtnExcel" Text="EXPORT TO XLS" OnClick="CreateExcel" CssClass="ExportExcelTxt" runat="server" />
            </td>
        </tr>
    </table>
    <br />

    <!-- filters -->
    <table style="width: 100%;" class="formFilter" border="0">
        <tr>
            <td>
                <table border="0">
                    <tr>
                        <td valign="top">
                            <netpay:DateRangePicker ID="wcDateRangePicker" runat="server" />
                        </td>
                        <td valign="top">
                            <table border="0">
                                <tr>
                                    <td>Merchant<br />
                                        <netpay:AutoComplete ID="wcMerchantAutoComplete" IsMultiselect="false" Function="GetMerchantNameAutoComplete" runat="server" />
                                    </td>
                                    <td>Debit Company<br />
                                        <netpay:DebitCompanyDropDown ID="ddlDebitCompany" EnableBlankSelection="true" Width="120px" runat="server"></netpay:DebitCompanyDropDown></td>
                                    <td>Merchant Group<br />
                                        <netpay:MerchantGroupsDropDown ID="ddMerchantGroups" EnableBlankSelection="true" Width="120px" runat="server"></netpay:MerchantGroupsDropDown></td>
                                    <td>Status<br />
                                        <netpay:MerchantStatusDropDown ID="ddMerchantStatus" EnableBlankSelection="true" runat="server"></netpay:MerchantStatusDropDown></td>
                                    <td>Industry<br />
                                        <netpay:IndustryDropDown ID="ddIndustry" EnableBlankSelection="true" EnableNone="true" runat="server"></netpay:IndustryDropDown></td>
                                    <td>Country by IP<br />
                                        <netpay:CountryDropDown ID="ddIPCountry" EnableBlankSelection="true" runat="server"></netpay:CountryDropDown></td>
                                    <td>Affiliates<br />
                                        <netpay:DropDownBase ID="ddAffiliates" EnableBlankSelection="true" runat="server"></netpay:DropDownBase></td>
                                    <td>Departments<br />
                                        <netpay:DropDownBase ID="ddMerchantsDepartments" EnableBlankSelection="true" runat="server"></netpay:DropDownBase></td>
                                </tr>
                                <tr>
                                    <td>Terminal<br />
                                        <netpay:AutoComplete ID="wcTerminalAutoComplete" IsMultiselect="false" Function="GetDebitTerminalAutoComplete" runat="server" />
                                    </td>
                                    <td>Terminal Tag<br />
                                        <netpay:AutoComplete ID="wcTerminalTagAutoComplete" IsMultiselect="false" Function="GetDebitTerminalTagAutoComplete" runat="server" />
                                    </td>
                                    <td>Payment Method<br />
                                        <netpay:PaymentMethodDropDown ID="ddlPaymentMethodID" runat="server" Width="120px"></netpay:PaymentMethodDropDown></td>
                                    <td>Transactions<br />
                                        <asp:DropDownList ID="ddlPayout" runat="server">
                                            <asp:ListItem Text="" />
                                            <asp:ListItem Text="Processed - All" Selected="True" />
                                            <asp:ListItem Text="Processed - Settled" />
                                            <asp:ListItem Text="Processed - Unsettled" />
                                            <asp:ListItem Text="Gateway Only" />
                                        </asp:DropDownList>
                                    </td>
                                    <td>Currency<br />
                                        <netpay:CurrencyDropDown ID="ddCurrency" EnableBlankSelection="true" Width="100%" runat="server"></netpay:CurrencyDropDown></td>
                                    <td>Account Manager<br />
                                        <netpay:AccountManagersDropDown ID="ddAccountManager" Width="100%" EnableBlankSelection="true" runat="server"></netpay:AccountManagersDropDown></td>
                                    <td>Country by BIN<br />
                                        <netpay:CountryDropDown ID="ddBinCountry" EnableBlankSelection="true" runat="server"></netpay:CountryDropDown></td>
                                    <td>Transaction Sources<br />
                                        <netpay:DropDownBase ID="ddTransSources" EnableBlankSelection="true" runat="server"></netpay:DropDownBase></td>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td>Show:<br />
                                    </td>
                                    <td valign="top" nowrap="nowrap">
                                        <asp:CheckBox ID="cbShowFailed" runat="server" Text="Failed" CssClass="option" /><br />
                                    </td>
                                    <td valign="top" nowrap="nowrap">
                                        <asp:CheckBox ID="cbShowFees" runat="server" Text="Fees" CssClass="option" /><br />
                                    </td>
                                    <td valign="top" nowrap="nowrap">
                                        <asp:CheckBox ID="cbShowBankFees" runat="server" Text="Bank Fees" CssClass="option" /><br />
                                    </td>
                                    <td valign="top" nowrap="nowrap">
                                        <asp:CheckBox ID="cbShowAdminTrans" runat="server" Text="Admin Trans." CssClass="option" /><br />
                                    </td>
                                    <td style="width: 30px;"></td>
                                    <td>Calculation %:<br />
                                    </td>
                                    <td valign="top" nowrap="nowrap">
                                        <asp:RadioButton ID="rbPercentCalcModeCount" Text="By Count" GroupName="percentCalcMode" Checked="true" CssClass="option" runat="server" />
                                    </td>
                                    <td valign="top" nowrap="nowrap">
                                        <asp:RadioButton ID="rbPercentCalcModeAmount" Text="By Amount" GroupName="percentCalcMode" CssClass="option" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="border-top: dotted 1px gray; padding: 4px;">
                <table>
                    <tr>
                        <td>Group By  &nbsp;&nbsp;
							<asp:CheckBox ID="cbGroupByCurrency" onclick="checkGroups();" runat="server" Text="Currency" CssClass="option" />
                            <asp:CheckBox ID="cbGroupByMerchant" onclick="checkGroups()" runat="server" Text="Merchant" CssClass="option" />
                            <asp:CheckBox ID="cbGroupByTerminal" onclick="checkGroups()" runat="server" Text="Terminal" CssClass="option" />
                            <asp:CheckBox ID="cbGroupByTerminalTag" onclick="checkGroups()" runat="server" Text="Terminal Tag" CssClass="option" />
                            <asp:CheckBox ID="cbGroupByPaymentMethod" onclick="checkGroups()" runat="server" Text="Payment Method" />
                            <asp:CheckBox ID="cbGroupByMerchantGroup" onclick="checkGroups()" runat="server" Text="Merchant Group" CssClass="option" />
                            <asp:CheckBox ID="cbGroupByBank" onclick="checkGroups()" runat="server" Text="Bank" CssClass="option" />
                            <asp:CheckBox ID="cbGroupByWeek" onclick="checkGroups()" runat="server" Text="Week" CssClass="option" />
                            <asp:CheckBox ID="cbGroupByMonth" onclick="checkGroups()" runat="server" Text="Month" CssClass="option" />
                            <asp:CheckBox ID="cbGroupByIndustry" onclick="checkGroups()" runat="server" Text="Industry" CssClass="option" />
                            <asp:CheckBox ID="cbGroupByIPCountry" onclick="checkGroups()" runat="server" Text="Country by IP" CssClass="option" />
                            <asp:CheckBox ID="cbGroupByBinCountry" onclick="checkGroups()" runat="server" Text="Country by BIN" CssClass="option" />
                            <asp:CheckBox ID="cbGroupByDepartment" onclick="checkGroups()" runat="server" Text="Department" CssClass="option" />
                            <asp:CheckBox ID="cbGroupByMcc" onclick="checkGroups()" runat="server" Text="MCC" CssClass="option" />
                        </td>
                        <td style="width: 30px;"></td>
                        <td>
                            <input type="reset" value="RESET" style="width: 70px;" />
                            &nbsp;
							<asp:Button ID="btnGo" Text="SEARCH" Width="80px" OnClientClick="return validate();" OnClick="GetReport" UseSubmitBehavior="true" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <netpay:SearchFiltersView ID="wcFiltersView" runat="server" />
    <br />
    <!-- data -->
    <asp:PlaceHolder ID="phResults" Visible="false" runat="server">
        <table style="width: 100%;" cellspacing="0" class="formNormal">
            <netpay:SortableColumns ID="wcSorting" VOType="TransactionReportVO" OnSortChange="GetReport" runat="server">
                <netpay:SortableColumn ID="wcSortMonth" ColumnTitle="Month" ColumnName="TransactionMonth" runat="server" CssStyle="text-align:left;" />
                <netpay:SortableColumn ID="wcSortWeek" ColumnTitle="Week In Year" ColumnName="WeekID" runat="server" CssStyle="text-align:left;" />
                <netpay:SortableColumn ID="wcSortCurrencyCode" ColumnTitle="Currency" ColumnName="TransactionCurrencyIsoCode" runat="server" CssStyle="text-align:left" />
                <netpay:SortableColumn ID="wcSortBankName" ColumnTitle="Debit Company" ColumnName="AcquiringBankName" runat="server" CssStyle="text-align:left" />
                <netpay:SortableColumn ID="wcSortTerminalName" ColumnTitle="Terminal" ColumnName="TerminalName" runat="server" CssStyle="text-align:left" />
                <netpay:SortableColumn ID="wcSortTerminalTag" ColumnTitle="Terminal Tag" ColumnName="TerminalTag" runat="server" CssStyle="text-align:left" />
                <netpay:SortableColumn ID="wcSortPaymentMethod" ColumnTitle="Payment Method" ColumnName="PaymentMethodID" runat="server" CssStyle="text-align:left" />
                <netpay:SortableColumn ID="wcSortMerchantName" ColumnTitle="Merchant" ColumnName="MerchantName" runat="server" CssStyle="text-align:left" />
                <netpay:SortableColumn ID="wcSortCompanyGroup" ColumnTitle="Merchant Group" ColumnName="MerchantGroupID" runat="server" CssStyle="text-align:left" />
                <netpay:SortableColumn ID="wcSortIndustry" ColumnTitle="Industry" ColumnName="IndustryName" runat="server" CssStyle="text-align:left" />
                <netpay:SortableColumn ID="wcSortIPCountry" ColumnTitle="Country by IP" ColumnName="IPCountry" runat="server" CssStyle="text-align:left" />
                <netpay:SortableColumn ID="wcSortBinCountry" ColumnTitle="Country by BIN" ColumnName="BinCountry" runat="server" CssStyle="text-align:left" />
                <netpay:SortableColumn ID="wcSortDepartments" ColumnTitle="Departments" ColumnName="DepartmentName" runat="server" CssStyle="text-align:right" />
                <netpay:SortableColumn ID="wcSortMcc" ColumnTitle="MCC" ColumnName="MCCName" runat="server" CssStyle="text-align:right" />
                <netpay:SortableColumn ID="wcSortChargeAttempts" ColumnTitle="Charge Attempts" ColumnName="TotalAmount" runat="server" CssStyle="text-align:right" />
                <th style="background-color: White; border-bottom: 0px;">&nbsp;</th>
                <netpay:SortableColumn ID="wcSortPassedTransactions" ColumnTitle="Captured Transactions" ColumnName="PassedAmount" runat="server" CssStyle="text-align:right" />
                <netpay:SortableColumn ID="wsSortRefunds" ColumnTitle="Refunds" ColumnName="RefundsAmount" runat="server" CssStyle="text-align:right" />
                <netpay:SortableColumn ID="wsSortChargebacks" ColumnTitle="CHB" ColumnName="ChargebacksAmount" runat="server" CssStyle="text-align:right" />
                <netpay:SortableColumn ID="wsSortPendingChargebacks" ColumnTitle="Pending CHB" ColumnName="PendingChargebacksAmount" runat="server" CssStyle="text-align:right" />
                <netpay:SortableColumn ID="wsSortFrauds" ColumnTitle="Frauds" ColumnName="FraudsAmount" runat="server" CssStyle="text-align:right" />
                <th style="background-color: White; border-bottom: 0px;">&nbsp;</th>
                <netpay:SortableColumn ID="wsSortTransSubtotal" ColumnTitle="Trans. Subtotal" Enabled="false" runat="server" CssStyle="text-align:right" />
                <th id="thFeesSeperator" style="background-color: White; border-bottom: 0px;" runat="server">&nbsp;</th>
                <netpay:SortableColumn ID="wsSortCapturedFee" ColumnTitle="Captured Fee" ColumnName="PassedTransactionsFee" runat="server" CssStyle="text-align:right" />
                <netpay:SortableColumn ID="wsSortRatioFee" ColumnTitle="Ratio Fee" ColumnName="RatioFee" runat="server" CssStyle="text-align:right" />
                <netpay:SortableColumn ID="wsSortDeclinedFee" ColumnTitle="Declined Fee" ColumnName="FailedTransactionsFee" runat="server" CssStyle="text-align:right" />
                <netpay:SortableColumn ID="wsSortRefundFee" ColumnTitle="Refund Fee" ColumnName="RefundFee" runat="server" CssStyle="text-align:right" />
                <netpay:SortableColumn ID="wsSortChargebackFee" ColumnTitle="CHB Fee" ColumnName="ChargebackFee" runat="server" CssStyle="text-align:right" />
                <netpay:SortableColumn ID="wsSortClarificationFee" ColumnTitle="Clarification Fee" ColumnName="ClarificationFee" runat="server" CssStyle="text-align:right" />
                <netpay:SortableColumn ID="wsSortHandlingFee" ColumnTitle="Handling Fee" ColumnName="HandlingFee" runat="server" CssStyle="text-align:right" />
                <netpay:SortableColumn ID="wsSortManualFee" ColumnTitle="Manual Fee" ColumnName="ManualFee" runat="server" CssStyle="text-align:right" />
                <netpay:SortableColumn ID="wsSortFraudFee" ColumnTitle="Fraud Fee" ColumnName="FraudFee" runat="server" CssStyle="text-align:right" />
                <th style="background-color: White; border-bottom: 0px;">&nbsp;</th>
                <netpay:SortableColumn ID="wsSortFeesSubtotal" ColumnTitle="Fees Subtotal" Enabled="false" runat="server" CssStyle="text-align:right" />
                <th style="background-color: White; border-bottom: 0px;">&nbsp;</th>
                <netpay:SortableColumn ID="wsSortCapturedDebitFee" ColumnTitle="Bank {br/} Captured Fee" ColumnName="CapturedDebitFee" runat="server" CssStyle="text-align:left" />
                <netpay:SortableColumn ID="wsSortDeclinedDebitFee" ColumnTitle="Bank {br/} Declined Fee" ColumnName="DeclinedDebitFee" runat="server" CssStyle="text-align:left" />
                <netpay:SortableColumn ID="wsSortRefundDebitFee" ColumnTitle="Bank {br/} Refund Fee" ColumnName="RefundDebitFee" runat="server" CssStyle="text-align:left" />
                <netpay:SortableColumn ID="wsSortCapturedDebitFeeCHB" ColumnTitle="Bank {br/} CHB Fee" ColumnName="CapturedDebitFeeCHB" runat="server" CssStyle="text-align:left" />
                <netpay:SortableColumn ID="wsSortAuthorizedDebitFee" ColumnTitle="Bank {br/} Authorized Fee" ColumnName="AuthorizedDebitFee" runat="server" CssStyle="text-align:left" />
                <th style="background-color: White; border-bottom: 0px;">&nbsp;</th>
                <netpay:SortableColumn ID="wcSortBankFeesSubtotal" ColumnTitle="Bank {br/} Fees Subtotal" Enabled="false" runat="server" CssStyle="text-align:left" />
                <th style="background-color: White; border-bottom: 0px;">&nbsp;</th>
                <netpay:SortableColumn ID="wcSortGrandSubtotal" ColumnTitle="Subtotal" Enabled="false" runat="server" CssStyle="text-align:right" />
                <th style="background-color: White; border-bottom: 0px;">&nbsp;</th>
                <netpay:SortableColumn ID="wcSortFailedTransactions" ColumnTitle="Failed Transactions" ColumnName="FailedAmount" runat="server" CssStyle="text-align:right" />
                <th style="background-color: White; border-bottom: 0px;">&nbsp;</th>
                <netpay:SortableColumn ID="wcSortCaptureProfit" ColumnTitle="Capture Profit" Enabled="false" runat="server" CssStyle="text-align:right" />
                <netpay:SortableColumn ID="wcSortRefundProfit" ColumnTitle="Refund Profit" Enabled="false" runat="server" CssStyle="text-align:right" />
                <netpay:SortableColumn ID="wcSortChbProfit" ColumnTitle="CHB Profit" Enabled="false" runat="server" CssStyle="text-align:right" />
                <netpay:SortableColumn ID="wcSortDeclinedProfit" ColumnTitle="Declined Profit" Enabled="false" runat="server" CssStyle="text-align:right" />
                <netpay:SortableColumn ID="wcSortTotalProfit" ColumnTitle="Total Profit" Enabled="false" runat="server" CssStyle="text-align:right" />

                <netpay:SortableColumn ID="wcSortAdminTransDebit" ColumnTitle="Admin Trans. Debit" Enabled="false" runat="server" CssStyle="text-align:right" />
                <netpay:SortableColumn ID="wcSortAdminTransCredit" ColumnTitle="Admin Trans. Credit" Enabled="false" runat="server" CssStyle="text-align:right" />
            </netpay:SortableColumns>

            <tbody>
                <netpay:DynamicRepeater ID="repeaterResults" EnableViewState="false" runat="server">
                    <ItemTemplate>
                        <tr onmouseover="rowHighlight(this)" onmouseout="rowNormal(this, '#ffffff')">
                            <td id="tdTransactionMonth" runat="server">
                                <%# ((TransactionReportVO)Container.DataItem).TransactionMonth %>
                            </td>
                            <td id="tdTransactionWeek" runat="server">
                                <%# ((TransactionReportVO)Container.DataItem).WeekID %>: &nbsp;<%# ((TransactionReportVO)Container.DataItem).WeekText %>
                            </td>
                            <td id="tdCurrencyCode" runat="server">
                                <%# ((TransactionReportVO)Container.DataItem).TransactionCurrencyIsoCode %>
                            </td>
                            <td id="tdBankName" runat="server">
                                <netpay:BankView BankID="<%# ((TransactionReportVO)Container.DataItem).AcquiringBankID %>" BankName="<%# ((TransactionReportVO)Container.DataItem).AcquiringBankName %>" runat="server" />
                            </td>
                            <td id="tdTerminalName" runat="server">
                                <%# ((TransactionReportVO)Container.DataItem).TerminalName %>
                            </td>
                            <td id="tdTerminalTag" runat="server">
                                <%# ((TransactionReportVO)Container.DataItem).TerminalSearchTag %>
                            </td>
                            <td id="tdPaymentMethod" runat="server">
                                <netpay:PaymentMethodView PaymentMethodID="<%# ((TransactionReportVO)Container.DataItem).PaymentMethodID %>" runat="server" />
                            </td>
                            <td id="tdMerchantName" runat="server">
                                <%# ((TransactionReportVO)Container.DataItem).MerchantName %>
                            </td>
                            <td id="tdMerchantGroup" runat="server">
                                <%# ((TransactionReportVO)Container.DataItem).MerchantGroupName %>
                            </td>
                            <td id="tdIndustry" runat="server">
                                <%# ((TransactionReportVO)Container.DataItem).IndustryText %>
                            </td>
                            <td id="tdIPCountry" runat="server">
                                <%# GetCountryFlag(((TransactionReportVO)Container.DataItem).IPCountry) %>
                                <%# GetCountryName(((TransactionReportVO)Container.DataItem).IPCountry)%>
                            </td>
                            <td id="tdBinCountry" runat="server">
                                <%# GetCountryFlag(((TransactionReportVO)Container.DataItem).BinCountry) %>
                                <%# GetCountryName(((TransactionReportVO)Container.DataItem).BinCountry)%>
                            </td>
                            <td id="tdDepartments" runat="server">
                                <%# ((TransactionReportVO)Container.DataItem).SICCodeNumber%>
                            </td>
                            <td id="tdMcc" runat="server">
                                <%# ((TransactionReportVO)Container.DataItem).SICCodeNumber%>
                            </td>
                            <td class="numericCell">
                                <span style="color: #999999;">(<%# ((TransactionReportVO)Container.DataItem).TotalCount.ToString("#,0") %>)</span>
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).TotalAmount)%>
                            </td>
                            <td style="background-color: #ffffff!important;">&nbsp;</td>
                            <td class="numericCell">
                                <%# GetPassedTransactionsLine((TransactionReportVO)Container.DataItem) %>
                            </td>
                            <td class="numericCell">
                                <%# GetRefundsLine((TransactionReportVO)Container.DataItem) %>
                            </td>
                            <td class="numericCell">
                                <%# GetChargebacksLine((TransactionReportVO)Container.DataItem) %>
                            </td>
                            <td class="numericCell">
                                <%# GetPendingChargebacksLine((TransactionReportVO)Container.DataItem)%>
                            </td>
                            <td class="numericCell">
                                <%# GetFraudsLine((TransactionReportVO)Container.DataItem)%>
                            </td>
                            <td style="background-color: #ffffff!important;">&nbsp;</td>
                            <td class="numericCell">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, GetTransSubtotal((TransactionReportVO)Container.DataItem)) %>
                            </td>
                            <td style="background-color: #ffffff!important;">&nbsp;</td>
                            <td id="tdCapturedFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).PassedTransactionFee) %>
                            </td>
                            <td id="tdRatioFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).RatioFee) %>
                            </td>
                            <td id="tdDeclinedFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).FailedTransactionFee) %>
                            </td>
                            <td id="tdRefundFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).RefundFee) %>
                            </td>
                            <td id="tdChargebackFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).ChargebackFee) %>
                            </td>
                            <td id="tdClarificationFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).ClarificationFee)%>
                            </td>
                            <td id="tdHandlingFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).HandlingFee) %>
                            </td>
                            <td id="tdManualFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).ManualFee) %>
                            </td>
                            <td id="tdFraudFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).FraudFee) %>
                            </td>
                            <td style="background-color: #ffffff!important;">&nbsp;</td>
                            <td id="tdFeesSubtotal" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, GetFeesSubtotal((TransactionReportVO)Container.DataItem))%>
                            </td>
                            <td id="tdFeesSeperator" style="background-color: #ffffff!important;" runat="server">&nbsp;</td>
                            <td id="tdCapturedDebitFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).CapturedDebitFee) %>
                            </td>
                            <td id="tdDeclinedDebitFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).DeclinedDebitFee) %>
                            </td>
                            <td id="tdRefundDebitFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).RefundDebitFee)%>
                            </td>
                            <td id="tdCapturedDebitFeeCHB" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).CapturedDebitFeeCHB)%>
                            </td>
                            <td id="tdAuthorizedDebitFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).AuthorizedDebitFee) %>
                            </td>
                            <td style="background-color: #ffffff!important;">&nbsp;</td>
                            <td id="tdBankFeesSubtotal" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, GetBankFeesSubtotal((TransactionReportVO)Container.DataItem))%>
                            </td>
                            <td style="background-color: #ffffff!important;">&nbsp;</td>
                            <td class="numericCell">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, GetLineSubtotal((TransactionReportVO)Container.DataItem))%>
                            </td>
                            <td style="background-color: #ffffff!important;">&nbsp;</td>
                            <td id="tdFailedTransactions" class="numericCell" runat="server">
                                <%# GetFailedTransactionsLine((TransactionReportVO)Container.DataItem) %>
                            </td>
                            <td style="background-color: #ffffff!important;">&nbsp;</td>
                            <td id="tdCaptureProfit" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).CaptureProfit)%>
                            </td>
                            <td id="tdRefundProfit" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).RefundProfit)%>
                            </td>
                            <td id="tdCHBProfit" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).CHBProfit)%>
                            </td>
                            <td id="tdDeclinedProfit" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).DeclinedProfit)%>
                            </td>
                            <td id="tdTotalProfit" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).TotalProfit)%>
                            </td>

                            <td id="tdAdminTransDebit" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).AdminTransDebit)%>
                            </td>
                            <td id="tdAdminTransCredit" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).AdminTransCredit)%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <tr style="background-color: #F7F7F7;" onmouseover="rowHighlight(this)" onmouseout="rowNormal(this, '#F7F7F7')">
                            <td id="tdTransactionMonth" runat="server">
                                <%# ((TransactionReportVO)Container.DataItem).TransactionMonth %>
                            </td>
                            <td id="tdTransactionWeek" runat="server">
                                <%# ((TransactionReportVO)Container.DataItem).WeekID %>: &nbsp;<%# ((TransactionReportVO)Container.DataItem).WeekText %>
                            </td>
                            <td id="tdCurrencyCode" runat="server">
                                <%# ((TransactionReportVO)Container.DataItem).TransactionCurrencyIsoCode %>
                            </td>
                            <td id="tdBankName" runat="server">
                                <netpay:BankView ID="BankView1" BankID="<%# ((TransactionReportVO)Container.DataItem).AcquiringBankID %>" BankName="<%# ((TransactionReportVO)Container.DataItem).AcquiringBankName %>" runat="server" />
                            </td>
                            <td id="tdTerminalName" runat="server">
                                <%# ((TransactionReportVO)Container.DataItem).TerminalName %>
                            </td>
                            <td id="tdTerminalTag" runat="server">
                                <%# ((TransactionReportVO)Container.DataItem).TerminalSearchTag %>
                            </td>
                            <td id="tdPaymentMethod" runat="server">
                                <netpay:PaymentMethodView ID="PaymentMethodView1" PaymentMethodID="<%# ((TransactionReportVO)Container.DataItem).PaymentMethodID %>" runat="server" />
                            </td>
                            <td id="tdMerchantName" runat="server">
                                <%# ((TransactionReportVO)Container.DataItem).MerchantName %>
                            </td>
                            <td id="tdMerchantGroup" runat="server">
                                <%# ((TransactionReportVO)Container.DataItem).MerchantGroupName%>
                            </td>
                            <td id="tdIndustry" runat="server">
                                <%# ((TransactionReportVO)Container.DataItem).IndustryText %>
                            </td>
                            <td id="tdIPCountry" runat="server">
                                <%# GetCountryFlag(((TransactionReportVO)Container.DataItem).IPCountry) %>
                                <%# GetCountryName(((TransactionReportVO)Container.DataItem).IPCountry)%>
                            </td>
                            <td id="tdBinCountry" runat="server">
                                <%# GetCountryFlag(((TransactionReportVO)Container.DataItem).BinCountry) %>
                                <%# GetCountryName(((TransactionReportVO)Container.DataItem).BinCountry)%>
                            </td>
                            <td id="tdDepartments" runat="server">
                                <%# ((TransactionReportVO)Container.DataItem).MerchantDepartmentName%>
                            </td>
                            <td id="tdMcc" runat="server">
                                <%# ((TransactionReportVO)Container.DataItem).SICCodeNumber%>
                            </td>
                            <td class="numericCell">
                                <span style="color: #999999;">(<%# ((TransactionReportVO)Container.DataItem).TotalCount.ToString("#,0") %>)</span>
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).TotalAmount) %>
                            </td>
                            <td style="background-color: #ffffff!important;">&nbsp;</td>
                            <td class="numericCell">
                                <%# GetPassedTransactionsLine((TransactionReportVO)Container.DataItem) %>
                            </td>
                            <td class="numericCell">
                                <%# GetRefundsLine((TransactionReportVO)Container.DataItem) %>
                            </td>
                            <td class="numericCell">
                                <%# GetChargebacksLine((TransactionReportVO)Container.DataItem) %>
                            </td>
                            <td class="numericCell">
                                <%# GetPendingChargebacksLine((TransactionReportVO)Container.DataItem)%>
                            </td>
                            <td class="numericCell">
                                <%# GetFraudsLine((TransactionReportVO)Container.DataItem)%>
                            </td>
                            <td style="background-color: #ffffff!important;">&nbsp;</td>
                            <td class="numericCell">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, GetTransSubtotal((TransactionReportVO)Container.DataItem)) %>
                            </td>
                            <td style="background-color: #ffffff!important;">&nbsp;</td>
                            <td id="tdCapturedFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).PassedTransactionFee) %>
                            </td>
                            <td id="tdRatioFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).RatioFee) %>
                            </td>
                            <td id="tdDeclinedFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).FailedTransactionFee) %>
                            </td>
                            <td id="tdRefundFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).RefundFee) %>
                            </td>
                            <td id="tdChargebackFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).ChargebackFee) %>
                            </td>
                            <td id="tdClarificationFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).ClarificationFee)%>
                            </td>
                            <td id="tdHandlingFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).HandlingFee) %>
                            </td>
                            <td id="tdManualFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).ManualFee) %>
                            </td>
                            <td id="tdFraudFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).FraudFee) %>
                            </td>
                            <td style="background-color: #ffffff!important;">&nbsp;</td>
                            <td id="tdFeesSubtotal" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, GetFeesSubtotal((TransactionReportVO)Container.DataItem))%>
                            </td>
                            <td id="tdFeesSeperator" style="background-color: #ffffff!important;" runat="server">&nbsp;</td>
                            <td id="tdCapturedDebitFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).CapturedDebitFee) %>
                            </td>
                            <td id="tdDeclinedDebitFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).DeclinedDebitFee) %>
                            </td>
                            <td id="tdRefundDebitFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).RefundDebitFee)%>
                            </td>
                            <td id="tdCapturedDebitFeeCHB" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).CapturedDebitFeeCHB)%>
                            </td>
                            <td id="tdAuthorizedDebitFee" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).AuthorizedDebitFee) %>
                            </td>
                            <td style="background-color: #ffffff!important;">&nbsp;</td>
                            <td id="tdBankFeesSubtotal" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, GetBankFeesSubtotal((TransactionReportVO)Container.DataItem))%>
                            </td>
                            <td style="background-color: #ffffff!important;">&nbsp;</td>
                            <td class="numericCell">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, GetLineSubtotal((TransactionReportVO)Container.DataItem))%>
                            </td>
                            <td style="background-color: #ffffff!important;">&nbsp;</td>
                            <td id="tdFailedTransactions" class="numericCell" runat="server">
                                <%# GetFailedTransactionsLine((TransactionReportVO)Container.DataItem) %>
                            </td>
                            <td style="background-color: #ffffff!important;">&nbsp;</td>
                            <td id="tdCaptureProfit" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).CaptureProfit)%>
                            </td>
                            <td id="tdRefundProfit" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).RefundProfit)%>
                            </td>
                            <td id="tdCHBProfit" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).CHBProfit)%>
                            </td>
                            <td id="tdDeclinedProfit" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).DeclinedProfit)%>
                            </td>
                            <td id="tdTotalProfit" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).TotalProfit)%>
                            </td>

                            <td id="tdAdminTransDebit" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).AdminTransDebit)%>
                            </td>
                            <td id="tdAdminTransCredit" class="numericCell" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).AdminTransCredit)%>
                            </td>
                        </tr>
                    </AlternatingItemTemplate>
                </netpay:DynamicRepeater>

                <netpay:DynamicRepeater ID="repeaterSummary" EnableViewState="false" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td id="tdTransactionWeek" style="border-top: 1px solid black; text-align: right;" runat="server"></td>
                            <td id="tdTransactionMonth" style="border-top: 1px solid black; text-align: right;" runat="server"></td>
                            <td id="tdBankName" style="border-top: 1px solid black;" runat="server"></td>
                            <td id="tdTerminalName" style="border-top: 1px solid black;" runat="server"></td>
                            <td id="tdTerminalTag" style="border-top: 1px solid black;" runat="server"></td>
                            <td id="tdCurrencyCode" style="border-top: 1px solid black;" runat="server"></td>
                            <td id="tdPaymentMethod" style="border-top: 1px solid black;" runat="server"></td>
                            <td id="tdMerchantName" style="border-top: 1px solid black;" runat="server"></td>
                            <td id="tdMerchantGroup" style="border-top: 1px solid black;" runat="server"></td>
                            <td id="tdIndustry" style="border-top: 1px solid black;" runat="server"></td>
                            <td id="tdIPCountry" style="border-top: 1px solid black;" runat="server"></td>
                            <td id="tdBinCountry" style="border-top: 1px solid black;" runat="server"></td>
                            <td id="tdDepartments" style="border-top: 1px solid black;" runat="server"></td>
                            <td id="tdMcc" style="border-top: 1px solid black;" runat="server"></td>
                            <td class="numericCell" style="font-weight: bold; border-top: 1px solid black;">
                                <span style="color: #999999;">(<%# ((TransactionReportVO)Container.DataItem).TotalCount.ToString("#,0") %>)</span>
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).TotalAmount) %>
                            </td>
                            <td style="border-top-width: 0px!important;">&nbsp;</td>
                            <td class="numericCell" style="font-weight: bold; border-top: 1px solid black;">
                                <%# GetPassedTransactionsLine((TransactionReportVO)Container.DataItem) %>
                            </td>
                            <td class="numericCell" style="font-weight: bold; border-top: 1px solid black;">
                                <span style="color: #999999;">(<%# ((TransactionReportVO)Container.DataItem).RefundsCount.ToString("#,0") %>)</span>
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).RefundsAmount) %>
                            </td>
                            <td class="numericCell" style="font-weight: bold; border-top: 1px solid black;">
                                <%# GetChargebacksLine((TransactionReportVO)Container.DataItem) %>
                            </td>
                            <td class="numericCell" style="font-weight: bold; border-top: 1px solid black;">
                                <%# GetPendingChargebacksLine((TransactionReportVO)Container.DataItem)%>
                            </td>
                            <td class="numericCell">
                                <%# GetFraudsLine((TransactionReportVO)Container.DataItem)%>
                            </td>
                            <td style="border-top-width: 0px!important;">&nbsp;</td>
                            <td class="numericCell" style="font-weight: bold; border-top: 1px solid black;">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, GetTransSubtotal((TransactionReportVO)Container.DataItem)) %>
                            </td>
                            <td style="border-top-width: 0px!important;">&nbsp;</td>
                            <td id="tdCapturedFee" class="numericCell" style="font-weight: bold; border-top: 1px solid black;" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).PassedTransactionFee) %>
                            </td>
                            <td id="tdRatioFee" class="numericCell" style="font-weight: bold; border-top: 1px solid black;" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).RatioFee) %>
                            </td>
                            <td id="tdDeclinedFee" class="numericCell" style="font-weight: bold; border-top: 1px solid black;" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).FailedTransactionFee) %>
                            </td>
                            <td id="tdRefundFee" class="numericCell" style="font-weight: bold; border-top: 1px solid black;" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).RefundFee) %>
                            </td>
                            <td id="tdChargebackFee" class="numericCell" style="font-weight: bold; border-top: 1px solid black;" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).ChargebackFee) %>
                            </td>
                            <td id="tdClarificationFee" class="numericCell" style="font-weight: bold; border-top: 1px solid black;" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).ClarificationFee)%>
                            </td>
                            <td id="tdHandlingFee" class="numericCell" style="font-weight: bold; border-top: 1px solid black;" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).HandlingFee) %>
                            </td>
                            <td id="tdManualFee" class="numericCell" style="font-weight: bold; border-top: 1px solid black;" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).ManualFee) %>
                            </td>
                            <td id="tdFraudFee" class="numericCell" style="font-weight: bold; border-top: 1px solid black;" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).FraudFee) %>
                            </td>
                            <td style="border-top-width: 0px!important;">&nbsp;</td>
                            <td id="tdFeesSubtotal" class="numericCell" style="font-weight: bold; border-top: 1px solid black;" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, GetFeesSubtotal((TransactionReportVO)Container.DataItem))%>
                            </td>
                            <td id="tdFeesSeperator" style="background-color: #ffffff!important;" runat="server">&nbsp;</td>
                            <td id="tdCapturedDebitFee" class="numericCell" style="font-weight: bold; border-top: 1px solid black;" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).CapturedDebitFee) %>
                            </td>
                            <td id="tdDeclinedDebitFee" class="numericCell" style="font-weight: bold; border-top: 1px solid black;" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).DeclinedDebitFee) %>
                            </td>
                            <td id="tdRefundDebitFee" class="numericCell" style="font-weight: bold; border-top: 1px solid black;" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).RefundDebitFee) %>
                            </td>
                            <td id="tdCapturedDebitFeeCHB" class="numericCell" style="font-weight: bold; border-top: 1px solid black;" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).CapturedDebitFeeCHB)%>
                            </td>
                            <td id="tdAuthorizedDebitFee" class="numericCell" style="font-weight: bold; border-top: 1px solid black;" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).AuthorizedDebitFee) %>
                            </td>
                            <td style="border-top-width: 0px!important;">&nbsp;</td>
                            <td id="tdBankFeesSubtotal" class="numericCell" style="font-weight: bold; border-top: 1px solid black;" runat="server">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, GetBankFeesSubtotal((TransactionReportVO)Container.DataItem))%>
                            </td>
                            <td style="border-top-width: 0px!important;">&nbsp;</td>
                            <td class="numericCell" style="font-weight: bold; border-top: 1px solid black;">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, GetLineSubtotal((TransactionReportVO)Container.DataItem))%>
                            </td>
                            <td style="border-top-width: 0px!important;">&nbsp;</td>
                            <td id="tdFailedTransactions" class="numericCell" style="font-weight: bold; border-top: 1px solid black;" runat="server">
                                <%# GetFailedTransactionsLine((TransactionReportVO)Container.DataItem) %>
                            </td>
                            <td style="background-color: #ffffff!important;">&nbsp;</td>
                            <td id="tdCaptureProfit" class="numericCell" runat="server" style="font-weight: bold; border-top: 1px solid black;">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).CaptureProfit)%>
                            </td>
                            <td id="tdRefundProfit" class="numericCell" runat="server" style="font-weight: bold; border-top: 1px solid black;">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).RefundProfit)%>
                            </td>
                            <td id="tdCHBProfit" class="numericCell" runat="server" style="font-weight: bold; border-top: 1px solid black;">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).CHBProfit)%>
                            </td>
                            <td id="tdDeclinedProfit" class="numericCell" runat="server" style="font-weight: bold; border-top: 1px solid black;">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).DeclinedProfit)%>
                            </td>
                            <td id="tdTotalProfit" class="numericCell" runat="server" style="font-weight: bold; border-top: 1px solid black;">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).TotalProfit)%>
                            </td>
                            <td id="tdAdminTransDebit" class="numericCell" runat="server" style="font-weight: bold; border-top: 1px solid black;">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).AdminTransDebit)%>
                            </td>
                            <td id="tdAdminTransCredit" class="numericCell" runat="server" style="font-weight: bold; border-top: 1px solid black;">
                                <%# GetFormattedAmount(((TransactionReportVO)Container.DataItem).TransactionCurrencyID, ((TransactionReportVO)Container.DataItem).AdminTransCredit)%>
                            </td>
                        </tr>
                    </ItemTemplate>
                </netpay:DynamicRepeater>
            </tbody>
        </table>
    </asp:PlaceHolder>
</asp:Content>
