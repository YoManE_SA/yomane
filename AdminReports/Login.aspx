﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Netpay.AdminReports.Login" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="robots" content="noindex, nofollow" />

   <!-- login css -->
    <link type="text/css" rel="stylesheet" href="Styles/login.css" />

    <!-- Bootstrap -->
    <link href="Scripts/Bootstrap/bootstrap.css" rel="stylesheet" />
    <script type="text/javascript" src="Scripts/Bootstrap/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="Scripts/Bootstrap/bootstrap.js"></script>
</head>
<body>
    <form runat="server" id="form1" autocomplete="off">
        <asp:ScriptManager runat="server" ID="ScriptManager"></asp:ScriptManager>
        <div class="container">
            <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <div class="panel-text" style="line-height: 35px;">Sign in - Admin Reports</div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <netpay:ActionNotify runat="server" ID="acLogin" />
                        <%--<div class="form-group">
                            Username
                            <asp:TextBox CssClass="form-control" ID="txtUser" runat="server"></asp:TextBox>
                        </div>--%>
                        <div class="form-group">
                            Email
                        <asp:TextBox CssClass="form-control" ID="txtEmail" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            Password
                        <input style="display: none" type="password" id="txtPassword">
                            <asp:TextBox CssClass="form-control" type="password" ClientIDMode="Static" ID="txtPassword" TextMode="Password" runat="server" AutoCompleteType="Disabled" autocomplete="off" name="Password"></asp:TextBox>
                        </div>
                        <asp:Button CssClass="btn btn-primary" ID="btnlogin" Text="Sign in" OnClick="btnlogin_Click" runat="server" />
                    </div>
                </div>
            </div>
        </div>

        <%-- enforce change password modal dialog --%>
        <netpay:ModalDialog runat="server" ID="dlgChangePassword" Title="Change Password">
            <Body>
                <div class="alert alert-danger">
                    Password is too old. Policy requires changing it.
                    <br />
                    Please insert a new password and than log in.
                </div>
                <netpay:ActionNotify runat="server" ID="acnPasswordMessage" />
                <div class="form-group">
                    Old Password
            <asp:TextBox runat="server" CssClass="form-control" ID="txtOldPassword" TextMode="Password" />
                </div>
                <div class="form-group">
                    New Password
            <asp:TextBox runat="server" CssClass="form-control" ID="txtNewPassword" TextMode="Password" />
                </div>
                <div class="form-group">
                    Confirm Password
            <asp:TextBox runat="server" CssClass="form-control" ID="txtConfirmPassword" TextMode="Password" />
                </div>
            </Body>
            <Footer>
                <asp:Button runat="server" ID="btnClose" ClientIDMode="Static" CssClass="btn btn-inverse btn-cons-short" Text="Cancel" />
                <asp:Button runat="server" ID="btnSave" CssClass="btn btn-primary btn-cons-short" Text="Save" OnClick="UpdatePassword_Click" />
            </Footer>
        </netpay:ModalDialog>
    </form>
</body>
</html>




