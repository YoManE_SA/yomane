﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Bll;
using Netpay.Infrastructure;
using System.IO;
using Netpay.AdminReports.Code;
using Netpay.Bll.Reports;

namespace Netpay.AdminReports
{
	public partial class RiskReportsByTerminal : NetpayPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			LoadReportList();
		}

		protected void LoadReportList()
		{
			FileInfoList1.LoadReportList(ReportType.AdminDailyRiskByTerminal);
			List<FileInfo> dataSource = FileInfoList1.dataSource;

			if (dataSource.Count > 0)
			{
				FileInfo currentFile = dataSource.FirstOrDefault();
                imgCurFileIcon.ImageUrl = "Images/ImgFileExt/" + currentFile.Extension.Replace(".", "") + ".gif";
				lblCurFileDate.Text = currentFile.LastWriteTime.ToString("dddd, MMMM d, yyyy HH:mm");
				lblCurFileSize.Text = currentFile.Length.ToFileSize();
				btnCurFile.CommandArgument = currentFile.FullName; 
			}
			
			phFiles.Visible = dataSource.Count > 0;
		}

		protected void DownloadReport(object sender, CommandEventArgs e)
		{
			DownloadFile(e.CommandArgument.ToString(), "text/csv");
		}
	}
}
