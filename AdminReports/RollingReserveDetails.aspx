﻿<%@ Page Title="Admin Reports - Rolling Reserve Details" Language="C#" MasterPageFile="~/Reports.Master" AutoEventWireup="true" CodeBehind="RollingReserveDetails.aspx.cs" Inherits="Netpay.AdminReports.RollingReserveDetails" %>

<asp:Content ID="content2" ContentPlaceHolderID="head" runat="server">
<style>
	div.innerContent
	{
		width: 75%;
	}
</style>
</asp:Content>

<asp:Content ID="content1" ContentPlaceHolderID="body" runat="server">
	<table style="width:100%;" border="0">
		<tr>
			<td class="pageMainHeading" colspan="2">Rolling Reserves</td>
			<td style="text-align:right;">
				<netpay:ReportUpdateInfo ID="wcReportUpdateInfo" runat="server" Report="AdminRollingReserves" /> &nbsp;|&nbsp;
				<asp:Image ID="Image1" ImageUrl="Images/ImgFileExt/xls.gif" runat="server" />
				<asp:LinkButton ID="lnkBtnExcel" Text="EXPORT TO XLS" OnClick="CreateExcel" CssClass="ExportExcelTxt" runat="server" />
			</td>
		</tr>
		<tr><td colspan="2" class="pageSubHeading">Merchant Detail</td></tr>	
	</table>
	<br /><br />
	<netpay:SearchFiltersView ID="wcFiltersView" runat="server" />	
	<table style="width:100%;" class="formNormal">		
		<netpay:SortableColumns ID="ccSorting" VOType="RollingReserveReportDetailVO" runat="server">
			<netpay:SortableColumn ID="SortableColumn8" Enabled="false" runat="server" CssStyle="width:20px" />
			<netpay:SortableColumn ID="SortableColumn1" ColumnTitle="Date" ColumnName="InsertDate" runat="server" CssStyle="text-align:left" />
			<netpay:SortableColumn ID="SortableColumn2" ColumnTitle="Merchant" Enabled = "false" runat="server" CssStyle="text-align:left" />
			<netpay:SortableColumn ID="SortableColumn3" ColumnTitle="Trans" ColumnName="TransactionID" runat="server" CssStyle="text-align:left" />
			<%--<netpay:SortableColumn ID="SortableColumn4" ColumnTitle="State" ColumnName="RollingReserveState" Enabled="true" runat="server" CssStyle="text-align:left" />--%>
			<netpay:SortableColumn ID="SortableColumn5" ColumnTitle="Settlement" ColumnName="SettlementID" Enabled="true" runat="server" CssStyle="text-align:left" />
			<netpay:SortableColumn ID="SortableColumn6" ColumnTitle="Comment" Enabled="false" runat="server" CssStyle="text-align:left" />
			<netpay:SortableColumn ID="SortableColumn7" ColumnTitle="Amount" ColumnName="Amount" Enabled="true" runat="server" CssStyle="text-align:right" />
		</netpay:SortableColumns>
		<tbody>
			<asp:Repeater ID="repeaterReport" runat="server">
				<ItemTemplate>
					<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='#ffffff';">
						<td class="reportCell">
							<asp:Literal runat="server" Text='<%# string.Format( ((RollingReserveReportDetailVO)Container.DataItem).ReserveCreditType == 0 ? "<img src=\"Templates/{0}/Images/icon_ArrowR.gif\" border=\"0\" />" : "<img src=\"Templates/{0}/Images/icon_ArrowG.gif\" border=\"0\" />", WebUtils.CurrentDomain.ThemeFolder)%>' />
						</td>
						<td class="reportCell">
							<%# ((RollingReserveReportDetailVO)Container.DataItem).InsertDate.ToString("dd/MM/yyyy")%>
						</td>
						<td class="reportCell">
							<%# ((RollingReserveReportDetailVO)Container.DataItem).CompanyName%>
						</td>
						<td class="reportCell">
							<%# ((RollingReserveReportDetailVO)Container.DataItem).TransactionID%>
						</td>
						<%--<td class="reportCell">
							<%# Netpay.Bll.WebUtils.DomainCache.GetGlobalData(GlobalDataGroup.RollingReserveState, Netpay.CommonTypes.Language.English, ((RollingReserveReportDetailVO)Container.DataItem).RollingReserveState.Value) %>
						</td>--%>
						<td class="reportCell">
							<%# ((RollingReserveReportDetailVO)Container.DataItem).SettlementID%>
						</td>
						<td class="reportCell">
							&nbsp;<%# Convert.IsDBNull(((RollingReserveReportDetailVO)Container.DataItem).ReserveComment) ? "" : ((RollingReserveReportDetailVO)Container.DataItem).ReserveComment%>
						</td>
						<td class="reportCell numericCell" style="text-align:right">
							<asp:Label runat="server" ForeColor='<%# ((RollingReserveReportDetailVO)Container.DataItem).ReserveCreditType == 0 ? System.Drawing.Color.Red : System.Drawing.Color.Black %>'
							 Text='<%# FormatAmount((RollingReserveReportDetailVO)Container.DataItem)%>' />
						</td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
			<tr><td colspan="7" style="font-weight:bold; text-align:right;">Total: &nbsp; <asp:Label ID="lblTotal" runat="server" /></td></tr>
		</tbody>
	</table>	
</asp:Content>
