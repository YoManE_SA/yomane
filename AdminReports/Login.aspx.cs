﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.AdminReports.Code;
using Netpay.Web;
using Netpay.Infrastructure.Security;

namespace Netpay.AdminReports
{
	public partial class Login : NetpayPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            if (Page.Request.QueryString.ToString() == "PasswordChanged")
            {
                acLogin.SetSuccess("Password changed, login with the new one.");
                DataBindChildren();
            }
        }

        protected void btnlogin_Click(object sender, EventArgs e)
        {
            WebUtils.SessionKeyNotUsePath = true;
            var result = WebUtils.Login(new Infrastructure.Security.UserRole[] { Infrastructure.Security.UserRole.Admin }, null, txtEmail.Text, txtPassword.Text);

            if (result != Infrastructure.Security.LoginResult.Success)
            {
                //Handle case that the password was inserted more than
                //Three months ago.
                if (result == Infrastructure.Security.LoginResult.ForcePasswordChange)
                {
                    dlgChangePassword.RegisterShow();
                }
                else
                {
                    //acLogin.SetMessage(result.ToString(), true);
                    acLogin.SetMessage("Unknown username or bad password", true);
                }
            }

            if (result == Infrastructure.Security.LoginResult.Success)
                Response.Redirect("default.aspx");
        }

        protected void UpdatePassword_Click(object sender, EventArgs e)
        {
            //Handle the case that the user inserted the old password again.
            if (txtOldPassword.Text == txtNewPassword.Text)
            {
                acnPasswordMessage.SetMessage("Please insert a new password.", true);
                dlgChangePassword.BindAndUpdate();
            }

            //Handle case when both passwords not equale
            if (txtNewPassword.Text != txtConfirmPassword.Text)
            {
                acnPasswordMessage.SetMessage("Passwords must be the same.", true);
                dlgChangePassword.BindAndUpdate();
            }
            else
            {
                //Set the new password
                var ret = Infrastructure.Security.Login.SetNewPasswordAfterExpiredOrReset(new UserRole[] { UserRole.Admin }, null, txtEmail.Text, txtOldPassword.Text, txtNewPassword.Text, Request.UserHostAddress);
                if (ret == Infrastructure.Security.Login.ChangePasswordResult.Success)
                {
                    Response.Redirect(Request.RawUrl + "?PasswordChanged");
                }
                else
                {
                    acnPasswordMessage.SetMessage("Unable to change password: " + ret.ToString(), true);
                    //acnPasswordMessage.SetMessage("Unable to change password: " + "Unknown username or bad password", true);
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            btnClose.OnClientClick = dlgChangePassword.HideJSCommand;
            base.OnPreRender(e);
        }
    }
}
