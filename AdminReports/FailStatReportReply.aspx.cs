﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Bll;
using Netpay.AdminReports.Code;
using Netpay.Web;
using Netpay.Bll.Reports;
using Netpay.Bll.Reports.VO;

namespace Netpay.AdminReports
{
	public partial class FailStatReportReply : NetpayPage
	{		
		SearchFilters filters = new SearchFilters();
		List<FailStatReplyGroupVO> dataSource;
		string replyCode="";

		protected class NumberStringStructComparer : IEqualityComparer<NumberStringStruct>
		{
			public bool Equals(NumberStringStruct x, NumberStringStruct y)
			{
				return (x.Name.ToLower() == y.Name.ToLower()) && (x.ID == y.ID);
			}

			public int GetHashCode(NumberStringStruct obj)
			{
				return obj.Name.GetHashCode();
			}
		}

		protected struct NumberStringStruct
		{
			public int ID { get; set; }
			public string Name { get; set; }
		}

		protected void GetDataSourceSorted(string propertyName, string sortDirection)
		{
			if (sortDirection == "Desc")
				repeaterReport.DataSource = dataSource.OrderByDescending(a => Netpay.Bll.Reports.ImmediateReports.GetPropertyValue(a, propertyName));
			else
				repeaterReport.DataSource = dataSource.OrderBy(a => Netpay.Bll.Reports.ImmediateReports.GetPropertyValue(a, propertyName));
		}

		protected string GetDebitCompanyName(int debitCompanyID)
		{
			return Bll.DebitCompanies.DebitCompany.GetCache().Values.Where(c => c.ID == debitCompanyID).Select(c => c.Name).First();
		}

		protected void GetDataSourceGroupedFiltered()
		{			
			dataSource = Netpay.Bll.Reports.ImmediateReports.GetFailStatReplyReportGrouped(filters);
		}

		protected void SortReport(Object o, CommandEventArgs e)
		{
			if (e.CommandArgument.ToString() == hdSortField.Value)
			{
				if (hdSortOrder.Value == "Desc")
				{
					hdSortOrder.Value = "Asc";
					string buttonText = ((LinkButton)o).Text;
					buttonText = buttonText.Replace("arrowTreeD", "arrowTreeU");
					((LinkButton)o).Text = buttonText;
				}
				else
				{
					hdSortOrder.Value = "Desc";
					string buttonText = ((LinkButton)o).Text;
					buttonText = buttonText.Replace("arrowTreeU", "arrowTreeD");
					((LinkButton)o).Text = buttonText;
				}
			}
			else
			{
				foreach (Control ctrl in ((Control)o).NamingContainer.Controls)
				{
					if (ctrl is LinkButton)
					{
						if (((LinkButton)ctrl).Text.IndexOf("arrowTreeD") >= 0)
							((LinkButton)ctrl).Text = ((LinkButton)ctrl).Text.Replace(string.Format(" <img src='Templates/{0}/Images/arrowTreeD.gif' border='0'>", WebUtils.CurrentDomain.ThemeFolder), "");
						if (((LinkButton)ctrl).Text.IndexOf("arrowTreeU") >= 0)
							((LinkButton)ctrl).Text = ((LinkButton)ctrl).Text.Replace(string.Format(" <img src='Templates/{0}/Images/arrowTreeU.gif' border='0'>", WebUtils.CurrentDomain.ThemeFolder), "");
					}
				}
				hdSortOrder.Value = "Desc";
				string buttonText = ((LinkButton)o).Text;
				buttonText += string.Format(" <img src='Templates/{0}/Images/arrowTreeD.gif' border='0'>", WebUtils.CurrentDomain.ThemeFolder);
				((LinkButton)o).Text = buttonText;
				hdSortField.Value = e.CommandArgument.ToString();
			}

			GetDataSourceSorted(hdSortField.Value, hdSortOrder.Value);			
			repeaterReport.DataBind();
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				lbtnCount.Text = string.Format("Count <img src='Templates/{0}/Images/arrowTreeD.gif' border='0'>", WebUtils.CurrentDomain.ThemeFolder);
				replyCode = HttpContext.Current.Request["reply"];
				if (replyCode == null) Response.Redirect("FailStatReport.aspx",true);
				hdReplyCode.Value = replyCode;				
			}
			else
			{
				replyCode = hdReplyCode.Value;
				if (replyCode == null) Response.Redirect("FailStatReport.aspx", true);
			}
			
			ltReplyCode.Text = replyCode;			
			wcFiltersView.Add("Reply code", replyCode);
			NumberStringStructComparer numberStringStructComparer = new NumberStringStructComparer();
			List<NumberStringStruct> paymentMethods = Bll.PaymentMethods.PaymentMethod.Cache.Select(c => new NumberStringStruct() { ID = c.ID, Name = c.Name }).Distinct(numberStringStructComparer).OrderBy(c => c.Name).ToList<NumberStringStruct>();			
			List<NumberStringStruct> currencies = Bll.Currency.Cache.Select(c => new NumberStringStruct() { ID = c.ID, Name = c.IsoCode }).Distinct(numberStringStructComparer).OrderBy(c => c.ID).ToList<NumberStringStruct>();
			if (!IsPostBack)
			{				
				int companyID;
				if (Request["companyId"] != null && Request["companyId"].Length > 0 && int.TryParse(Request["companyId"], out companyID))
				{
					wcMerchantAutoComplete.SelectedValue = companyID.ToString();
					wcFiltersView.Add("Merchant", wcMerchantAutoComplete.SelectedValue);
				}

				ddlPaymentMethodID.DataSource = paymentMethods;
				ddlPaymentMethodID.DataTextField = "Name";
				ddlPaymentMethodID.DataValueField = "ID";
				ddlPaymentMethodID.DataBind();
				ddlPaymentMethodID.Items.Insert(0, new System.Web.UI.WebControls.ListItem("", "-1"));
				int paymentMethodId;
				if (Request["paymentMethodId"] != null && Request["paymentMethodId"].Length > 0 && int.TryParse(Request["paymentMethodId"], out paymentMethodId))
				{
					ddlPaymentMethodID.SelectedValue = paymentMethodId.ToString();					
					wcFiltersView.Add("Payment method", ddlPaymentMethodID.SelectedItem.Text);
				}

				ddlCurrencyID.DataSource = currencies;
				ddlCurrencyID.DataTextField = "Name";
				ddlCurrencyID.DataValueField = "ID";
				ddlCurrencyID.DataBind();
				ddlCurrencyID.Items.Insert(0, new System.Web.UI.WebControls.ListItem("", "-1"));
				int currencyID;
				if (Request["currencyId"] != null && Request["currencyId"].Length > 0 && int.TryParse(Request["currencyId"], out currencyID))
				{
					ddlCurrencyID.SelectedValue = currencyID.ToString();					
					wcFiltersView.Add("Currency", ddlCurrencyID.SelectedItem.Text);
				}
				int debitCompanyId;
				if (Request["debitCompanyId"] != null && Request["debitCompanyId"].Length > 0 && int.TryParse(Request["debitCompanyId"], out debitCompanyId))
				{
					hdDebitCompanyID.Value = debitCompanyId.ToString();					
					wcFiltersView.Add("Debit company", GetDebitCompanyName(debitCompanyId));
				}
				if (Request["terminalNumber"] != null && Request["terminalNumber"].Length > 0)
				{
					hdTerminalNumber.Value = Request["terminalNumber"];
					wcFiltersView.Add("Terminal number", Request["terminalNumber"]);
				}

				DateTime dateFrom,dateTo;
				if (Request["dateFrom"] != null && Request["dateFrom"].Length > 0 && DateTime.TryParse(Request["dateFrom"], out dateFrom))
				{
					datepickerFrom.Value = dateFrom.ToString("dd-MM-yyyy");					
					wcFiltersView.Add("From date", dateFrom.ToString("dd/MM/yyyy"));
				}

				if (Request["dateTo"] != null && Request["dateTo"].Length > 0 && DateTime.TryParse(Request["dateTo"], out dateTo))
				{
					datepickerTo.Value = dateTo.ToString("dd-MM-yyyy");					
					wcFiltersView.Add("To date", dateTo.ToString("dd/MM/yyyy"));
				}
				if (Request["excludeMultiple"] == "1")
				{
					chkExcludeMultiple.Checked = true;					
					wcFiltersView.Add("Exclude multiple tries", "");
				}
				if (Request["excludeMultiple"] == "0") chkExcludeMultiple.Checked = false;
				if (Request["transTypeDebit"] == "1")
				{
					chkTransTypeDebit.Checked = true;
					wcFiltersView.Add("TransType: debit", "");
				}
				else
				{
					chkTransTypeDebit.Checked = false;
				}
				if (Request["transTypePreauth"] == "1")
				{
					chkTransTypePreAuth.Checked = true;
					wcFiltersView.Add("TransType: preauth", "");
				}
				else
				{
					chkTransTypePreAuth.Checked = false;
				}
				if (Request["transTypeCapture"] == "1")
				{
					chkTransTypeCapture.Checked = true;
					wcFiltersView.Add("TransType: capture", "");
				}
				else
				{
					chkTransTypeCapture.Checked = false;
				}
			}			
			
			PopulateFilter();
			GetDataSourceGroupedFiltered();
			repeaterReport.DataSource = dataSource;
			repeaterReport.DataBind();
		}

		protected void PopulateFilter()
		{			
			try
			{
				DateTime dateFrom, dateTo;				
				if (!IsPostBack)
				{
					int companyID;
					if (Request["companyId"] != null && Request["companyId"].Length > 0 && int.TryParse(Request["companyId"], out companyID))
					{
						filters.merchantIDs = new List<int>();
						filters.merchantIDs.Add(companyID);					
					}
					int paymentMethodID;
					if (Request["paymentMethodId"] != null && Request["paymentMethodId"].Length > 0 && int.TryParse(Request["paymentMethodId"], out paymentMethodID))
					{
						filters.paymentMethodID = paymentMethodID;					
					}
					byte currencyID;
					if (Request["currencyId"] != null && Request["currencyId"].Length > 0 && byte.TryParse(Request["currencyId"], out currencyID))
					{
						filters.currencyID = currencyID;
					}
					if (Request["terminalNumber"] != null && Request["terminalNumber"].Length > 0)
					{
						filters.terminalNumber = Request["terminalNumber"];
					}
					int debitCompanyID;
					if (Request["debitCompanyId"] != null && Request["debitCompanyId"].Length > 0 && int.TryParse(Request["debitCompanyId"], out debitCompanyID))
					{
						filters.debitCompanyID = debitCompanyID;						
					}
					if (Request["dateFrom"] != null && Request["dateFrom"].Length > 0 && DateTime.TryParse(Request["dateFrom"], out dateFrom))
					{
						filters.dateFrom = dateFrom;
					}
					if (Request["dateTo"] != null && Request["dateTo"].Length > 0 && DateTime.TryParse(Request["dateTo"], out dateTo))
					{
						filters.dateTo = dateTo + new TimeSpan(23, 59, 59);
					}
					if (Request["excludeMultiple"] == "1")
					{
						filters.failStatExcludeMultiples = true;
					}
					else
					{
						filters.failStatExcludeMultiples = false;
					}
					if (Request["transTypeDebit"] == "1")
					{
						filters.transTypeDebit = true;
					}
					else
					{
						filters.transTypeDebit = false;
					}
					if (Request["transTypePreauth"] == "1")
					{
						filters.transTypePreAuth = true;
					}
					else
					{
						filters.transTypePreAuth = false;
					}
					if (Request["transTypeCapture"] == "1")
					{
						filters.transTypeCapture = true;
					}
					else
					{
						filters.transTypeCapture = false;
					}
				}
				else
				{					
					if (wcMerchantAutoComplete.IsSelected)
					{
						filters.merchantIDs = wcMerchantAutoComplete.GetSelectedValues<int>();
						if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
							wcFiltersView.Add("Merchant", wcMerchantAutoComplete.SelectedValue);
					}
					if (int.Parse(ddlPaymentMethodID.SelectedValue) >= 0)
					{
						filters.paymentMethodID = int.Parse(ddlPaymentMethodID.SelectedValue);						
						wcFiltersView.Add("Payment method", ddlPaymentMethodID.SelectedItem.Text);
					}
					if (int.Parse(ddlCurrencyID.SelectedValue) >= 0)
					{
						filters.currencyID = byte.Parse(ddlCurrencyID.SelectedValue);						
						wcFiltersView.Add("Currency", ddlCurrencyID.SelectedItem.Text);
					}
					if (hdDebitCompanyID.Value.Length > 0)
					{
						filters.debitCompanyID = int.Parse(hdDebitCompanyID.Value);						
						wcFiltersView.Add("Debit company", GetDebitCompanyName(int.Parse(hdDebitCompanyID.Value)));
					}
					if (hdTerminalNumber.Value.Length > 0)
					{
						filters.terminalNumber = hdTerminalNumber.Value;						
						wcFiltersView.Add("Terminal number", hdTerminalNumber.Value);
					}
					if (DateTime.TryParse(datepickerFrom.Value, out dateFrom))
					{
						filters.dateFrom = dateFrom;						
						wcFiltersView.Add("From date", dateFrom.ToString("dd/MM/yyyy"));
					}
					if (DateTime.TryParse(datepickerTo.Value, out dateTo))
					{
						filters.dateTo = dateTo + new TimeSpan(23, 59, 59);						
						wcFiltersView.Add("To date", dateTo.ToString("dd/MM/yyyy"));
					}
					if (chkExcludeMultiple.Checked)
					{						
						wcFiltersView.Add("Exclude multiple tries", "");
						filters.failStatExcludeMultiples = true;
					}
					else
					{
						filters.failStatExcludeMultiples = false;
					}
					if (chkTransTypeDebit.Checked)
					{
						wcFiltersView.Add("TransType: debit", "");
						filters.transTypeDebit = true;
					}
					else
					{
						filters.transTypeDebit = false;
					}
					if (chkTransTypePreAuth.Checked)
					{
						wcFiltersView.Add("TransType: preauth", "");
						filters.transTypePreAuth = true;
					}
					else
					{
						filters.transTypePreAuth = false;
					}
					if (chkTransTypeCapture.Checked)
					{
						wcFiltersView.Add("TransType: capture", "");
						filters.transTypeCapture = true;
					}
					else
					{
						filters.transTypeCapture = false;
					}
				}
			}
			catch
			{
			
			}			
			
			filters.replyCode = replyCode;
			if (filters.failStatExcludeMultiples == null) filters.failStatExcludeMultiples = false;
		}

		protected void FilterReport(object sender, EventArgs e)
		{

			GetDataSourceGroupedFiltered();
			repeaterReport.DataSource = dataSource;
			repeaterReport.DataBind();
		}

		protected void CreateExcel(object sender, EventArgs e)
		{
			List<ReportGeneratorBase.FieldInfo> fields = new List<ReportGeneratorBase.FieldInfo>();
			fields.Add(new ReportGeneratorBase.FieldInfo("Count", "GroupCount"));
			fields.Add(new ReportGeneratorBase.FieldInfo("Terminal Number", "TerminalNumber"));
			fields.Add(new ReportGeneratorBase.FieldInfo("Contract Number", "ContractNumber"));

            var generator = new ReportGeneratorExcel();
            var result = generator.Generate(fields, dataSource, null, wcFiltersView.RenderText());
            DownloadExcel(result);
		}
	}
}
