﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Netpay.Bll;
using Netpay.AdminReports.Code;
using Netpay.Web;
using Netpay.Bll.Reports;
using Netpay.Infrastructure;
using Netpay.Bll.Reports.VO;

namespace Netpay.AdminReports
{

	public partial class RollingReserveSum : NetpayPage
	{
		protected struct Total
		{
			public string Currency;
			public decimal ReservedTotal;
			public decimal ReleasedTotal;
			public decimal NPTotal;
		}

		List<RollingReserveReportVO> dataSource;
		IEnumerable<Total> currencyTotals;

		protected IEnumerable<Total> GetTotals()
		{
			currencyTotals = from r in dataSource group r by r.CurrencySymbol into g select new Total { Currency = g.Key, ReservedTotal = g.Sum(r => r.Reserved), ReleasedTotal = g.Sum(r => r.Released), NPTotal = g.Sum(r => (r.Reserved - r.Released)) };
			return currencyTotals;
		}

		protected string FormatReserved(RollingReserveReportVO item)
		{
			return ((item.ReservedCount <= 0) ? "---" : "<span class=\"count\">(" + item.ReservedCount + ")</span>&nbsp; " + item.CurrencySymbol + item.Reserved.ToString("#,##0.00"));
		}

		protected string FormatReleased(RollingReserveReportVO item)
		{
			return ((item.ReleasedCount <= 0) ? "---" : "<span class=\"count\">(" + item.ReleasedCount + ")</span>&nbsp; " + item.CurrencySymbol + item.Released.ToString("#,##0.00"));
		}

		protected void Page_LoadComplete(object sender, EventArgs e)
		{
			dataSource = Netpay.Bll.Reports.ImmediateReports.GetRollingReserveSummaryReport(GetFilters(), ccSorting.Info);
			phResults.Visible = dataSource.Count > 0;
			var merchants = dataSource.Select(c => new KeyValuePair<int, string>(c.CompanyID, c.CompanyName)).Distinct().OrderBy(c => c.Value).ToList();

			if (!IsPostBack)
			{
				ddlMerchant.DataSource = merchants;
				ddlMerchant.DataTextField = "Value";
				ddlMerchant.DataValueField = "Key";
				ddlMerchant.DataBind();

				ddlCurrency.DataSource = Bll.Currency.Cache;
				ddlCurrency.DataTextField = "Name";
				ddlCurrency.DataValueField = "ID";
				ddlCurrency.DataBind();

				ddlTransType.Items.Insert(0, new System.Web.UI.WebControls.ListItem("", ""));
				ddlTransType.Items.Insert(1, new System.Web.UI.WebControls.ListItem("Reserve", "0"));
				ddlTransType.Items.Insert(2, new System.Web.UI.WebControls.ListItem("Release", "1"));
				ddlTransType.Items.Insert(3, new System.Web.UI.WebControls.ListItem("Balance", "2"));

				ddlCondition.Items.Insert(0, new System.Web.UI.WebControls.ListItem("", ""));
				ddlCondition.Items.Insert(1, new System.Web.UI.WebControls.ListItem("<", "0"));
				ddlCondition.Items.Insert(2, new System.Web.UI.WebControls.ListItem(">", "1"));
			}


			repeaterReport.DataSource = dataSource;
			repeaterReport.DataBind();

			GetTotals();
			repeaterTotals.DataSource = currencyTotals;
			repeaterTotals.DataBind();
		}

		protected string FormatTotal(RollingReserveReportVO item)
		{
			if (item.TotalCount != null && item.Total != null && item.CurrencySymbol != null)
				return "<span class=count>(" + item.TotalCount + ")</span>&nbsp; " + item.CurrencySymbol + ((decimal)item.Total).ToString("#,##0.00");
			else
			{
				return "---";
			}
		}

		SearchFilters _filters = null;
		protected SearchFilters GetFilters()
		{
			if(_filters == null)
			{
				_filters = new SearchFilters();
				if (ddlMerchant.IsSelected)
				{
					_filters.merchantIDs = new List<int>();
					_filters.merchantIDs.Add(int.Parse(ddlMerchant.SelectedValue));
					wcFiltersView.Add("Merchant", ddlMerchant.SelectedItem.Text);
				}
				if (ddMerchantGroup.IsSelected)
				{
					_filters.merchantGroupID = int.Parse(ddMerchantGroup.SelectedValue);
					wcFiltersView.Add("Merchant Group", ddMerchantGroup.SelectedItem.Text);
				}
				if(ddlCurrency.SelectedValue != null && ddlCurrency.SelectedValue != "")
				{
					_filters.currencyID = byte.Parse(ddlCurrency.SelectedValue);
					wcFiltersView.Add("Currency", ddlCurrency.SelectedItem.Text);
				}
				decimal reportAmount = 0;
				string reportCondition = "";
				bool reportReserved = false;
				bool reportReleased = false;
				bool reportTotal = false;
				if(decimal.TryParse(txAmount.Text, out reportAmount))
				{
					if(int.Parse(ddlCondition.SelectedValue) == 0) reportCondition = "lt";
					if(int.Parse(ddlCondition.SelectedValue) == 1) reportCondition = "gt";
					if(int.Parse(ddlTransType.SelectedValue) == 0) reportReserved = true;
					if(int.Parse(ddlTransType.SelectedValue) == 1) reportReleased = true;
					if(int.Parse(ddlTransType.SelectedValue) == 2) reportTotal = true;

					if(reportAmount > 0 && reportCondition != "" && (reportReserved || reportReleased || reportTotal))
					{
						if(reportReserved)
						{
							_filters.reportReserved = true;
							wcFiltersView.Add("Type", "Reserved");
						}
						if(reportReleased)
						{
							_filters.reportRelased = true;
							wcFiltersView.Add("Type", "Released");
						}
						if(reportTotal)
						{
							_filters.reportTotal = true;
							wcFiltersView.Add("Type", "Balance");
						}
						_filters.reportCondition = reportCondition;
						if(reportCondition == "lt")
						{
							wcFiltersView.Add("Condition", "less than");
						}
						else
						{
							wcFiltersView.Add("Condition", "greater than");
						}
						_filters.reportAmount = reportAmount;
						wcFiltersView.Add("Amount", reportAmount.ToString());
					}
				} 
			}

			return _filters;
		}

		protected void FilterReport(object sender, EventArgs e)
		{
			dataSource = Netpay.Bll.Reports.ImmediateReports.GetRollingReserveSummaryReport(GetFilters(), ccSorting.Info);
			repeaterReport.DataSource = dataSource;
			repeaterReport.DataBind();

			GetTotals();
			repeaterTotals.DataSource = currencyTotals;
			repeaterTotals.DataBind();
		}

		protected decimal GetFutureRollingReserve(RollingReserveReportVO data)
		{
			if (data.RollingReservePercent == null || data.UnsettledAmount == null || data.RollingReserveState == null)
					return 0;

			if (data.RollingReserveState == (int)Netpay.Infrastructure.RollingReserveState.Dynamic || data.RollingReserveState == (int)Netpay.Infrastructure.RollingReserveState.Fixed)
				return Netpay.Infrastructure.Math.GetPercentageOf(data.RollingReservePercent.GetValueOrDefault(), data.UnsettledAmount.GetValueOrDefault());
				else
					return 0;
		}

		protected void CreateExcel(object sender, EventArgs e)
		{
			dataSource = Netpay.Bll.Reports.ImmediateReports.GetRollingReserveSummaryReport(GetFilters(), ccSorting.Info);
			
			// fields
			List<ReportGeneratorBase.FieldInfo> fields = new List<ReportGeneratorBase.FieldInfo>();
			fields.Add(new ReportGeneratorBase.FieldInfo("Company Name", "CompanyName"));
			fields.Add(new ReportGeneratorBase.FieldInfo("Company ID", "CompanyID"));
			fields.Add(new ReportGeneratorBase.FieldInfo("Currency", "CurrencySymbol"));
			fields.Add(new ReportGeneratorBase.FieldInfo("Released", (object item) => ((RollingReserveReportVO)item).Released.ToAmountFormat()));
			fields.Add(new ReportGeneratorBase.FieldInfo("Reserved", (object item) => ((RollingReserveReportVO)item).Reserved.ToAmountFormat()));
			fields.Add(new ReportGeneratorBase.FieldInfo("Total", (object item) => ((RollingReserveReportVO)item).Total.GetValueOrDefault().ToAmountFormat()));
            
            string header = (wcFiltersView.Visible ? wcFiltersView.RenderText() : null);
            var generator = new ReportGeneratorExcel();
            var result = generator.Generate(fields, dataSource, null, header);
            DownloadExcel(result);
        }
	}
}
