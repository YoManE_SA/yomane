﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Netpay.CommonTypes;
using Netpay.Infrastructure;

using Netpay.Bll;
using Netpay.Bll.Reports;
using Netpay.Bll.Reports.VO;
using Netpay.Web;
using Netpay.AdminReports.Code;

namespace Netpay.AdminReports
{
    public partial class TransactionsReport : NetpayPage
    {
        protected string minDate = "";
        protected string maxDate = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // set date picker
                wcDateRangePicker.MinDate = IntegrationService.DenormalizedMinDate;
                wcDateRangePicker.MaxDate = IntegrationService.DenormalizedMaxDate;
                wcDateRangePicker.FromDate = IntegrationService.DenormalizedMaxDate.AddDays(-7);
                wcDateRangePicker.ToDate = IntegrationService.DenormalizedMaxDate;

                // set defaults
                cbGroupByCurrency.Checked = true;

                // set drop downs
                ddAffiliates.DataSource = Bll.Affiliates.Affiliate.Search(null, null);
                ddAffiliates.DataValueField = "ID";
                ddAffiliates.DataTextField = "Name";
                ddAffiliates.DataBind();

                ddTransSources.DataSource = Bll.Process.TransactionSource.Cache;
                ddTransSources.DataValueField = "ID";
                ddTransSources.DataTextField = "Name";
                ddTransSources.DataBind();

                ddMerchantsDepartments.DataSource = Bll.Merchants.Department.Cache;
                ddMerchantsDepartments.DataValueField = "ID";
                ddMerchantsDepartments.DataTextField = "Name";
                ddMerchantsDepartments.DataBind();
            }

            //cbShowFees.Enabled = cbShowBankFees.Enabled = (cbGroupByCurrency.Checked || ddCurrency.IsSelected);
        }

        protected void GetReport()
        {
            SearchFilters filters = GetFilters();
            List<VOPropertyInfo> aggregates = GetAggregates();
            List<VOPropertyInfo> groups = GetGroups();
            List<TransactionReportVO> results = ImmediateReports.GetTransactionsReport(filters, aggregates, groups, wcSorting.Info);
            repeaterResults.DataSource = results;
            repeaterResults.DataBind();

            if (results.Count > 0 && (!cbGroupByCurrency.Checked))
            {
                List<TransactionReportVO> summary = new List<TransactionReportVO>();
                summary.Add(results.Aggregate((current, next) => current + next));
                repeaterSummary.DataSource = summary;
                repeaterSummary.DataBind();
            }

            phResults.Visible = results.Count > 0;
        }

        protected void GetReport(object sender, EventArgs e)
        {
            GetReport();
        }

        /// <summary>
        /// Gets the grouping list
        /// </summary>
        /// <returns></returns>
        protected List<VOPropertyInfo> GetGroups()
        {
            List<VOPropertyInfo> infos = new List<VOPropertyInfo>();
            if (cbGroupByWeek.Checked)
            {
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "WeekID" });
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "WeekText" });
                wcSortWeek.Visible = true;
            }
            else
            {
                repeaterResults.AddHiddenColumnID("tdTransactionWeek");
                repeaterSummary.AddHiddenColumnID("tdTransactionWeek");
                wcSortWeek.Visible = false;
            }

            if (cbGroupByMonth.Checked)
            {
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "TransactionMonth" });
                wcSortMonth.Visible = true;
            }
            else
            {
                repeaterResults.AddHiddenColumnID("tdTransactionMonth");
                repeaterSummary.AddHiddenColumnID("tdTransactionMonth");
                wcSortMonth.Visible = false;
            }

            if (cbGroupByBank.Checked)
            {
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "AcquiringBankID" });
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "AcquiringBankName" });
                wcSortBankName.Visible = true;
            }
            else
            {
                repeaterResults.AddHiddenColumnID("tdBankName");
                repeaterSummary.AddHiddenColumnID("tdBankName");
                wcSortBankName.Visible = false;
            }

            if (cbGroupByTerminal.Checked)
            {
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "TerminalID" });
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "TerminalName" });
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "TerminalNumber" });
                wcSortTerminalName.Visible = true;
            }
            else
            {
                repeaterResults.AddHiddenColumnID("tdTerminalName");
                repeaterSummary.AddHiddenColumnID("tdTerminalName");
                wcSortTerminalName.Visible = false;
            }

            if (cbGroupByTerminalTag.Checked)
            {
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "TerminalSearchTag" });
                wcSortTerminalTag.Visible = true;
            }
            else
            {
                repeaterResults.AddHiddenColumnID("tdTerminalTag");
                repeaterSummary.AddHiddenColumnID("tdTerminalTag");
                wcSortTerminalTag.Visible = false;
            }

            if (cbGroupByMerchant.Checked)
            {
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "MerchantID" });
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "MerchantNumber" });
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "MerchantName" });
                wcSortMerchantName.Visible = true;
            }
            else
            {
                repeaterResults.AddHiddenColumnID("tdMerchantName");
                repeaterSummary.AddHiddenColumnID("tdMerchantName");
                wcSortMerchantName.Visible = false;
            }

            if (cbGroupByCurrency.Checked)
            {
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "TransactionCurrencyID" });
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "TransactionCurrencyIsoCode" });
                wcSortCurrencyCode.Visible = true;
            }
            else
            {
                repeaterResults.AddHiddenColumnID("tdCurrencyCode");
                repeaterSummary.AddHiddenColumnID("tdCurrencyCode");
                wcSortCurrencyCode.Visible = false;
            }

            if (cbGroupByPaymentMethod.Checked)
            {
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "PaymentMethodID" });
                wcSortPaymentMethod.Visible = true;
            }
            else
            {
                repeaterResults.AddHiddenColumnID("tdPaymentMethod");
                repeaterSummary.AddHiddenColumnID("tdPaymentMethod");
                wcSortPaymentMethod.Visible = false;
            }

            if (cbGroupByMerchantGroup.Checked)
            {
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "MerchantGroupID" });
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "MerchantGroupName" });
                wcSortCompanyGroup.Visible = true;
            }
            else
            {
                repeaterResults.AddHiddenColumnID("tdMerchantGroup");
                repeaterSummary.AddHiddenColumnID("tdMerchantGroup");
                wcSortCompanyGroup.Visible = false;
            }

            if (cbGroupByIndustry.Checked)
            {
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "IndustryID" });
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "IndustryText" });
                wcSortIndustry.Visible = true;
            }
            else
            {
                repeaterResults.AddHiddenColumnID("tdIndustry");
                repeaterSummary.AddHiddenColumnID("tdIndustry");
                wcSortIndustry.Visible = false;
            }

            if (cbGroupByIPCountry.Checked)
            {
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "IPCountry" });
                wcSortIPCountry.Visible = true;
            }
            else
            {
                repeaterResults.AddHiddenColumnID("tdIPCountry");
                repeaterSummary.AddHiddenColumnID("tdIPCountry");
                wcSortIPCountry.Visible = false;
            }

            if (cbGroupByBinCountry.Checked)
            {
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "BinCountry" });
                wcSortBinCountry.Visible = true;
            }
            else
            {
                repeaterResults.AddHiddenColumnID("tdBinCountry");
                repeaterSummary.AddHiddenColumnID("tdBinCountry");
                wcSortBinCountry.Visible = false;
            }

            if (cbGroupByDepartment.Checked)
            {
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "MerchantDepartmentName" });
                wcSortDepartments.Visible = true;
            }
            else
            {
                repeaterResults.AddHiddenColumnID("tdDepartments");
                repeaterSummary.AddHiddenColumnID("tdDepartments");
                wcSortDepartments.Visible = false;
            }

            if (cbGroupByMcc.Checked)
            {
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "SICCodeNumber" });
                wcSortMcc.Visible = true;
            }
            else
            {
                repeaterResults.AddHiddenColumnID("tdMcc");
                repeaterSummary.AddHiddenColumnID("tdMcc");
                wcSortMcc.Visible = false;
            }

            return infos;
        }

        /// <summary>
        /// Gets the aggregate list
        /// </summary>
        /// <returns></returns>
        protected List<VOPropertyInfo> GetAggregates()
        {
            List<VOPropertyInfo> infos = new List<VOPropertyInfo>();
            infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "TotalAmount" });
            infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "TotalCount" });
            infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "PassedCount" });
            infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "PassedAmount" });
            infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "RefundsCount" });
            infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "RefundsAmount" });
            infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "ChargebacksCount" });
            infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "ChargebacksAmount" });
            infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "PendingChargebacksCount" });
            infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "PendingChargebacksAmount" });
            infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "FraudCount" });
            infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "FraudAmount" });

            // failed
            if (cbShowFailed.Checked)
            {
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "FailedCount" });
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "FailedAmount" });
                wcSortFailedTransactions.Visible = true;

            }
            else
            {
                repeaterResults.AddHiddenColumnID("tdFailedTransactions");
                repeaterSummary.AddHiddenColumnID("tdFailedTransactions");
                wcSortFailedTransactions.Visible = false;
            }

            // fees
            if (cbShowFees.Checked)
            {
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "PassedTransactionFee" });
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "RefundFee" });
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "FailedTransactionFee" });
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "RatioFee" });
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "ChargebackFee" });
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "ClarificationFee" });
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "HandlingFee" });
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "ManualFee" });
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "FraudFee" });

                thFeesSeperator.Visible = true;
                wsSortCapturedFee.Visible = true;
                wsSortRefundFee.Visible = true;
                wsSortDeclinedFee.Visible = true;
                wsSortRatioFee.Visible = true;
                wsSortChargebackFee.Visible = true;
                wsSortClarificationFee.Visible = true;
                wsSortHandlingFee.Visible = true;
                wsSortManualFee.Visible = true;
                wsSortFraudFee.Visible = true;
                wsSortFeesSubtotal.Visible = true;
            }
            else
            {
                thFeesSeperator.Visible = false;
                repeaterResults.AddHiddenColumnID("tdFeesSeperator");
                repeaterSummary.AddHiddenColumnID("tdFeesSeperator");
                repeaterResults.AddHiddenColumnID("tdCapturedFee");
                repeaterSummary.AddHiddenColumnID("tdCapturedFee");
                wsSortCapturedFee.Visible = false;
                repeaterResults.AddHiddenColumnID("tdRefundFee");
                repeaterSummary.AddHiddenColumnID("tdRefundFee");
                wsSortRefundFee.Visible = false;
                repeaterResults.AddHiddenColumnID("tdDeclinedFee");
                repeaterSummary.AddHiddenColumnID("tdDeclinedFee");
                wsSortDeclinedFee.Visible = false;
                repeaterResults.AddHiddenColumnID("tdRatioFee");
                repeaterSummary.AddHiddenColumnID("tdRatioFee");
                wsSortRatioFee.Visible = false;
                repeaterResults.AddHiddenColumnID("tdChargebackFee");
                repeaterSummary.AddHiddenColumnID("tdChargebackFee");
                wsSortChargebackFee.Visible = false;
                repeaterResults.AddHiddenColumnID("tdClarificationFee");
                repeaterSummary.AddHiddenColumnID("tdClarificationFee");
                wsSortClarificationFee.Visible = false;
                repeaterResults.AddHiddenColumnID("tdHandlingFee");
                repeaterSummary.AddHiddenColumnID("tdHandlingFee");
                wsSortHandlingFee.Visible = false;
                repeaterResults.AddHiddenColumnID("tdManualFee");
                repeaterSummary.AddHiddenColumnID("tdManualFee");
                wsSortManualFee.Visible = false;
                repeaterResults.AddHiddenColumnID("tdFraudFee");
                repeaterSummary.AddHiddenColumnID("tdFraudFee");
                wsSortFraudFee.Visible = false;
                repeaterResults.AddHiddenColumnID("tdFeesSubtotal");
                repeaterSummary.AddHiddenColumnID("tdFeesSubtotal");
                wsSortFeesSubtotal.Visible = false;
            }

            if (cbShowBankFees.Checked)
            {
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "CapturedDebitFee" });
                wsSortCapturedDebitFee.Visible = true;

                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "DeclinedDebitFee" });
                wsSortDeclinedDebitFee.Visible = true;

                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "AuthorizedDebitFee" });
                wsSortAuthorizedDebitFee.Visible = true;

                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "RefundDebitFee" });
                wsSortRefundDebitFee.Visible = true;

                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "CapturedDebitFeeCHB" });
                wsSortCapturedDebitFeeCHB.Visible = true;

                wcSortBankFeesSubtotal.Visible = true;
            }
            else
            {
                repeaterResults.AddHiddenColumnID("tdCapturedDebitFee");
                repeaterSummary.AddHiddenColumnID("tdCapturedDebitFee");
                wsSortCapturedDebitFee.Visible = false;

                repeaterResults.AddHiddenColumnID("tdDeclinedDebitFee");
                repeaterSummary.AddHiddenColumnID("tdDeclinedDebitFee");
                wsSortDeclinedDebitFee.Visible = false;

                repeaterResults.AddHiddenColumnID("tdAuthorizedDebitFee");
                repeaterSummary.AddHiddenColumnID("tdAuthorizedDebitFee");
                wsSortAuthorizedDebitFee.Visible = false;

                repeaterResults.AddHiddenColumnID("tdRefundDebitFee");
                repeaterSummary.AddHiddenColumnID("tdRefundDebitFee");
                wsSortRefundDebitFee.Visible = false;

                repeaterResults.AddHiddenColumnID("tdCapturedDebitFeeCHB");
                repeaterSummary.AddHiddenColumnID("tdCapturedDebitFeeCHB");
                wsSortCapturedDebitFeeCHB.Visible = false;

                repeaterResults.AddHiddenColumnID("tdBankFeesSubtotal");
                repeaterSummary.AddHiddenColumnID("tdBankFeesSubtotal");
                wcSortBankFeesSubtotal.Visible = false;
            }

            // totals
            if (cbShowFees.Checked && cbShowBankFees.Checked)
            {
                wcSortCaptureProfit.Visible = true;
                wcSortRefundProfit.Visible = true;
                wcSortChbProfit.Visible = true;
                wcSortDeclinedProfit.Visible = true;
                wcSortTotalProfit.Visible = true;
            }
            else
            {
                repeaterResults.AddHiddenColumnID("tdCaptureProfit");
                repeaterSummary.AddHiddenColumnID("tdCaptureProfit");
                wcSortCaptureProfit.Visible = false;

                repeaterResults.AddHiddenColumnID("tdRefundProfit");
                repeaterSummary.AddHiddenColumnID("tdRefundProfit");
                wcSortRefundProfit.Visible = false;

                repeaterResults.AddHiddenColumnID("tdCHBProfit");
                repeaterSummary.AddHiddenColumnID("tdCHBProfit");
                wcSortChbProfit.Visible = false;

                repeaterResults.AddHiddenColumnID("tdDeclinedProfit");
                repeaterSummary.AddHiddenColumnID("tdDeclinedProfit");
                wcSortDeclinedProfit.Visible = false;

                repeaterResults.AddHiddenColumnID("tdTotalProfit");
                repeaterSummary.AddHiddenColumnID("tdTotalProfit");
                wcSortTotalProfit.Visible = false;

            }

            // admin transactions
            if (cbShowAdminTrans.Checked)
            {
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "AdminTransDebit" });
                infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionReportVO), Property = "AdminTransCredit" });
                wcSortAdminTransDebit.Visible = true;
                wcSortAdminTransCredit.Visible = true;
            }
            else
            {
                repeaterResults.AddHiddenColumnID("tdAdminTransDebit");
                repeaterSummary.AddHiddenColumnID("tdAdminTransDebit");
                repeaterResults.AddHiddenColumnID("tdAdminTransCredit");
                repeaterSummary.AddHiddenColumnID("tdAdminTransCredit");
                wcSortAdminTransDebit.Visible = false;
                wcSortAdminTransCredit.Visible = false;
            }

            return infos;
        }

        /// <summary>
        /// Populates filters control and returns search filters 
        /// </summary>
        /// <param name="filters"></param>
        protected SearchFilters GetFilters()
        {
            SearchFilters filters = new SearchFilters();
            switch (ddlPayout.SelectedIndex)
            {
                case 1: filters.isGateway = false; filters.isPaidOut = null; wcFiltersView.Add("Transactions", "Processed"); break;
                case 2: filters.isGateway = false; filters.isPaidOut = true; wcFiltersView.Add("Transactions", "Settled"); break;
                case 3: filters.isGateway = false; filters.isPaidOut = false; wcFiltersView.Add("Transactions", "Unsettled"); break;
                case 4: filters.isGateway = true; filters.isPaidOut = null; wcFiltersView.Add("Transactions", "Gateway"); break;
                default: filters.isGateway = null; filters.isPaidOut = null; break;
            }
            if (ddMerchantStatus.IsSelected)
            {
                filters.merchantStatus = int.Parse(ddMerchantStatus.SelectedValue);
                wcFiltersView.Add("Merchant Status", ddMerchantStatus.SelectedItem.Text);
            }

            if (ddCurrency.IsSelected)
            {
                filters.currencyID = byte.Parse(ddCurrency.SelectedValue);
                wcFiltersView.Add("Currency", ddCurrency.SelectedItem.Text);
            }

            if (ddMerchantGroups.IsSelected)
            {
                filters.merchantGroupID = int.Parse(ddMerchantGroups.SelectedValue);
                wcFiltersView.Add("Merchant Group", ddMerchantGroups.SelectedItem.Text);
            }

            if (wcMerchantAutoComplete.IsSelected)
            {
                filters.merchantIDs = wcMerchantAutoComplete.GetSelectedValues<int>();
                if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
                    wcFiltersView.Add("Merchant", wcMerchantAutoComplete.SelectedText);
            }

            if (ddlPaymentMethodID.IsSelected)
            {
                filters.paymentMethodID = int.Parse(ddlPaymentMethodID.SelectedValue);
                wcFiltersView.Add("Payment Method", ddlPaymentMethodID.SelectedItem.Text);
            }

            if (ddlDebitCompany.IsSelected)
            {
                filters.debitCompanyID = int.Parse(ddlDebitCompany.SelectedValue);
                wcFiltersView.Add("Debit Company", ddlDebitCompany.SelectedItem.Text);
            }

            if (wcTerminalAutoComplete.IsSelected)
            {
                filters.terminalNumber = wcTerminalAutoComplete.SelectedValue;
                wcFiltersView.Add("Terminal Number", wcTerminalAutoComplete.SelectedText);
            }

            if (wcTerminalTagAutoComplete.IsSelected)
            {
                filters.terminalSearchTag = wcTerminalTagAutoComplete.SelectedText;
                wcFiltersView.Add("Terminal Tag", wcTerminalTagAutoComplete.SelectedText);
            }

            if (wcDateRangePicker.IsFromDateSelected)
            {
                filters.dateFrom = wcDateRangePicker.FromDate.Value.MinTime();
                wcFiltersView.Add("From Date", wcDateRangePicker.FromDate.Value.ToString("dd/MM/yyyy"));
            }
            if (wcDateRangePicker.IsToDateSelected)
            {
                filters.dateTo = wcDateRangePicker.ToDate.Value.MaxTime();
                wcFiltersView.Add("To Date", wcDateRangePicker.ToDate.Value.ToString("dd/MM/yyyy"));
            }
            if (ddIndustry.IsSelected)
            {
                filters.industryID = ddIndustry.SelectedItem.Value.ToNullableInt32();
                wcFiltersView.Add("Industry", ddIndustry.SelectedItem.Text);
            }
            if (ddIPCountry.IsSelected)
            {
                filters.ipCountryIso2 = ddIPCountry.SelectedCountry.IsoCode2.ToUpper();
                wcFiltersView.Add("Country by IP", ddIPCountry.SelectedItem.Text);
            }
            if (ddBinCountry.IsSelected)
            {
                filters.binCountryIso2 = ddBinCountry.SelectedCountry.IsoCode2.ToUpper();
                wcFiltersView.Add("Country by BIN", ddBinCountry.SelectedItem.Text);
            }

            if (ddAccountManager.IsSelected)
            {
                filters.accountManager = ddAccountManager.SelectedValue;
                wcFiltersView.Add("Account Manager", ddAccountManager.SelectedValue);
            }

            if (ddAffiliates.IsSelected)
            {
                filters.affiliateId = ddAffiliates.SelectedValue.ToNullableInt();
                wcFiltersView.Add("Affiliate", ddAffiliates.SelectedItem.Text);
            }

            if (ddTransSources.IsSelected)
            {
                filters.transactionSourceId = ddTransSources.SelectedValue.ToNullableByte();
                wcFiltersView.Add("Transaction Source", ddAccountManager.SelectedItem.Text);
            }

            if (ddMerchantsDepartments.IsSelected)
            {
                filters.merchantDepartmentId = ddMerchantsDepartments.SelectedValue.ToNullableByte();
                wcFiltersView.Add("Department", ddMerchantsDepartments.SelectedItem.Text);
            }

            return filters;
        }

        protected string GetPassedTransactionsLine(TransactionReportVO dataItem)
        {
            decimal percentage = rbPercentCalcModeAmount.Checked ? GetCapturedAmountPercentage(dataItem) : GetCapturedCountPercentage(dataItem);
            StringBuilder lineBuilder = new StringBuilder();
            lineBuilder.AppendFormat("<span style=\"color: #999999;\">({0}) </span>", dataItem.PassedCount.ToString("#,0"));
            lineBuilder.AppendFormat("<span style=\"color: #999999;\">({0}) </span>", (percentage / 100).ToString("P"));
            lineBuilder.Append(GetFormattedAmount(dataItem.TransactionCurrencyID, dataItem.PassedAmount));

            return lineBuilder.ToString();
        }

        protected string GetFailedTransactionsLine(TransactionReportVO dataItem)
        {
            decimal percentage = rbPercentCalcModeAmount.Checked ? GetDeclinedAmountPercentage(dataItem) : GetDeclinedCountPercentage(dataItem);
            StringBuilder lineBuilder = new StringBuilder();
            lineBuilder.AppendFormat("<span style=\"color: #999999;\">({0}) </span>", dataItem.FailedCount.ToString("#,0"));
            lineBuilder.AppendFormat("<span style=\"color: #999999;\">({0}) </span>", (percentage / 100).ToString("P"));
            lineBuilder.Append(GetFormattedAmount(dataItem.TransactionCurrencyID, dataItem.FailedAmount));

            return lineBuilder.ToString();
        }

        protected string GetChargebacksLine(TransactionReportVO dataItem)
        {
            decimal percentage = rbPercentCalcModeAmount.Checked ? GetChargebacksAmountsPercentage(dataItem) : GetChargebacksCountPercentage(dataItem);
            StringBuilder lineBuilder = new StringBuilder();
            lineBuilder.AppendFormat("<span style=\"color: #999999;\">({0}) </span>", dataItem.ChargebacksCount.ToString("#,0"));
            lineBuilder.AppendFormat("<span style=\"color: #999999;\">({0}) </span>", (percentage / 100).ToString("P"));
            lineBuilder.Append(GetFormattedAmount(dataItem.TransactionCurrencyID, dataItem.ChargebacksAmount));

            return lineBuilder.ToString();
        }

        protected string GetPendingChargebacksLine(TransactionReportVO dataItem)
        {
            decimal percentage = rbPercentCalcModeAmount.Checked ? GetPendingChargebacksAmountsPercentage(dataItem) : GetPendingChargebacksCountPercentage(dataItem);
            StringBuilder lineBuilder = new StringBuilder();
            lineBuilder.AppendFormat("<span style=\"color: #999999;\">({0}) </span>", dataItem.PendingChargebacksCount.ToString("#,0"));
            lineBuilder.AppendFormat("<span style=\"color: #999999;\">({0}) </span>", (percentage / 100).ToString("P"));
            lineBuilder.Append(GetFormattedAmount(dataItem.TransactionCurrencyID, dataItem.PendingChargebacksAmount));
            return lineBuilder.ToString();
        }

        protected string GetFraudsLine(TransactionReportVO dataItem)
        {
            decimal percentage = rbPercentCalcModeAmount.Checked ? GetFraudsAmountsPercentage(dataItem) : GetFraudsCountPercentage(dataItem);
            StringBuilder lineBuilder = new StringBuilder();
            lineBuilder.AppendFormat("<span style=\"color: #999999;\">({0}) </span>", dataItem.FraudCount.ToString("#,0"));
            lineBuilder.AppendFormat("<span style=\"color: #999999;\">({0}) </span>", (percentage / 100).ToString("P"));
            lineBuilder.Append(GetFormattedAmount(dataItem.TransactionCurrencyID, dataItem.FraudAmount));
            return lineBuilder.ToString();
        }

        public decimal GetRefundsCountPercentage(TransactionReportVO transaction)
        {
            return Netpay.Infrastructure.Math.GetPercentage(transaction.RefundsCount, transaction.PassedCount);
        }

        public decimal GetRefundAmountsPercentage(TransactionReportVO transaction)
        {
            return Netpay.Infrastructure.Math.GetPercentage(System.Math.Abs(transaction.RefundsAmount), transaction.PassedAmount);
        }

        protected string GetRefundsLine(TransactionReportVO dataItem)
        {
            decimal percentage = rbPercentCalcModeAmount.Checked ? GetRefundAmountsPercentage(dataItem) : GetRefundsCountPercentage(dataItem);
            StringBuilder lineBuilder = new StringBuilder();
            lineBuilder.AppendFormat("<span style=\"color: #999999;\">({0}) </span>", dataItem.RefundsCount.ToString("#,0"));
            lineBuilder.AppendFormat("<span style=\"color: #999999;\">({0}) </span>", (percentage / 100).ToString("P"));
            lineBuilder.Append(GetFormattedAmount(dataItem.TransactionCurrencyID, dataItem.RefundsAmount));

            return lineBuilder.ToString();
        }

        /// <summary>
        /// Gets the formatted amount.
        /// If no currency is provided, the currency is taken from the currency dropdown.
        /// If currency is not selected, no currency is assumed.
        /// The amount if painted red if less than zero.
        /// </summary>
        /// <param name="currencyID"></param>
        /// <param name="amount"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        protected string GetFormattedAmount(int? currencyID, decimal? amount)
        {
            if (currencyID == null)
                currencyID = (int)CommonTypes.Currency.USD;

            if (ddCurrency.IsSelected)
                currencyID = int.Parse(ddCurrency.SelectedValue);

            if (amount == null)
                return "<span>null</span>";
            else if (amount < 0)
                return string.Format("<span class=\"negativeNumber\">{0}</span>", amount.Value.ToAmountFormat(currencyID.Value));
            else
                return string.Format("<span>{0}</span>", amount.Value.ToAmountFormat(currencyID.Value));
        }

        protected void CreateExcel(object sender, EventArgs e)
        {
            List<TransactionReportVO> results = ImmediateReports.GetTransactionsReport(GetFilters(), GetAggregates(), GetGroups(), wcSorting.Info);
            List<ReportGeneratorBase.FieldInfo> fields = new List<ReportGeneratorBase.FieldInfo>();
            if (cbGroupByMonth.Checked) fields.Add(new ReportGeneratorBase.FieldInfo("Month", "TransactionMonth"));
            if (cbGroupByWeek.Checked) fields.Add(new ReportGeneratorBase.FieldInfo("Week", "WeekText"));
            if (cbGroupByBank.Checked) fields.Add(new ReportGeneratorBase.FieldInfo("Bank", "AcquiringBankName"));
            if (cbGroupByTerminal.Checked) fields.Add(new ReportGeneratorBase.FieldInfo("Terminal", "TerminalName"));
            if (cbGroupByTerminalTag.Checked) fields.Add(new ReportGeneratorBase.FieldInfo("Terminal", "TerminalSearchTag"));
            if (cbGroupByCurrency.Checked) fields.Add(new ReportGeneratorBase.FieldInfo("Currency", "TransactionCurrencyIsoCode"));
            if (cbGroupByPaymentMethod.Checked) fields.Add(new ReportGeneratorBase.FieldInfo("Payment Method", (object item) => GetPaymentMethod(((TransactionReportVO)item))));
            if (cbGroupByMerchant.Checked)
            {
                fields.Add(new ReportGeneratorBase.FieldInfo("ID", "MerchantID"));
                fields.Add(new ReportGeneratorBase.FieldInfo("Merchant", "MerchantName"));
            }
            if (cbGroupByIndustry.Checked) fields.Add(new ReportGeneratorBase.FieldInfo("Industry", "IndustryText"));
            if (cbGroupByMerchantGroup.Checked) fields.Add(new ReportGeneratorBase.FieldInfo("Merchant Group", "MerchantGroupName"));
            if (cbGroupByIPCountry.Checked) fields.Add(new ReportGeneratorBase.FieldInfo("Country by IP", "IPCountry"));
            if (cbGroupByBinCountry.Checked) fields.Add(new ReportGeneratorBase.FieldInfo("Country by BIN", "BinCountry"));

            fields.Add(new ReportGeneratorBase.FieldInfo("Charge Attempts Count", "TotalCount"));
            fields.Add(new ReportGeneratorBase.FieldInfo("Charge Attempts Amount", (object item) => ((TransactionReportVO)item).TotalAmount.ToAmountFormat()));
            fields.Add(new ReportGeneratorBase.FieldInfo("Passed Count", "PassedCount"));
            fields.Add(new ReportGeneratorBase.FieldInfo("Passed Amount", (object item) => ((TransactionReportVO)item).PassedAmount.ToAmountFormat()));
            fields.Add(new ReportGeneratorBase.FieldInfo("Refunds Count", "RefundsCount"));
            fields.Add(new ReportGeneratorBase.FieldInfo("Refunds Amount", (object item) => ((TransactionReportVO)item).RefundsAmount.ToAmountFormat()));
            fields.Add(new ReportGeneratorBase.FieldInfo("Chargebacks Count", "ChargebacksCount"));
            fields.Add(new ReportGeneratorBase.FieldInfo("Chargebacks Amount", (object item) => ((TransactionReportVO)item).ChargebacksAmount.ToAmountFormat()));
            fields.Add(new ReportGeneratorBase.FieldInfo("Pending Chargebacks Count", "PendingChargebacksCount"));
            fields.Add(new ReportGeneratorBase.FieldInfo("Pending Chargebacks Amount", (object item) => ((TransactionReportVO)item).PendingChargebacksAmount.ToAmountFormat()));
            fields.Add(new ReportGeneratorBase.FieldInfo("Trans. Subtotal", (object item) => GetTransSubtotal((TransactionReportVO)item).ToAmountFormat()));
            // failed
            if (cbShowFailed.Checked)
            {
                fields.Add(new ReportGeneratorBase.FieldInfo("Failed Count", "FailedCount"));
                fields.Add(new ReportGeneratorBase.FieldInfo("Failed Amount", (object item) => ((TransactionReportVO)item).FailedAmount.ToAmountFormat()));
            }

            if (cbShowFees.Checked)
            {
                fields.Add(new ReportGeneratorBase.FieldInfo("Transaction Fee", (object item) => ((TransactionReportVO)item).PassedTransactionFee.ToAmountFormat()));
                fields.Add(new ReportGeneratorBase.FieldInfo("Processing Fee", (object item) => ((TransactionReportVO)item).RatioFee.ToAmountFormat()));
                fields.Add(new ReportGeneratorBase.FieldInfo("Capture Fee(T+P)", (object item) => ((TransactionReportVO)item).CaptureFee.ToAmountFormat()));
                fields.Add(new ReportGeneratorBase.FieldInfo("Declined Fee", (object item) => ((TransactionReportVO)item).FailedTransactionFee.ToAmountFormat()));
                fields.Add(new ReportGeneratorBase.FieldInfo("Refund Fee", (object item) => ((TransactionReportVO)item).RefundFee.ToAmountFormat()));
                fields.Add(new ReportGeneratorBase.FieldInfo("CHB Fee", (object item) => ((TransactionReportVO)item).ChargebackFee.ToAmountFormat()));
                fields.Add(new ReportGeneratorBase.FieldInfo("Crlf Fee", (object item) => ((TransactionReportVO)item).ClarificationFee.ToAmountFormat()));
                fields.Add(new ReportGeneratorBase.FieldInfo("Handling Fee", (object item) => ((TransactionReportVO)item).HandlingFee.ToAmountFormat()));
                fields.Add(new ReportGeneratorBase.FieldInfo("Manual Fee", (object item) => ((TransactionReportVO)item).ManualFee.ToAmountFormat()));
                fields.Add(new ReportGeneratorBase.FieldInfo("Fraud Fee", (object item) => ((TransactionReportVO)item).FraudFee.ToAmountFormat()));
                fields.Add(new ReportGeneratorBase.FieldInfo("Fees Subtotal", (object item) => GetFeesSubtotal((TransactionReportVO)item).ToAmountFormat()));
            }

            if (cbShowBankFees.Checked)
            {
                fields.Add(new ReportGeneratorBase.FieldInfo("Bank Captured Fee", (object item) => ((TransactionReportVO)item).CapturedDebitFee.GetValueOrDefault().ToAmountFormat()));
                fields.Add(new ReportGeneratorBase.FieldInfo("Bank Declined Fee", (object item) => ((TransactionReportVO)item).DeclinedDebitFee.GetValueOrDefault().ToAmountFormat()));
                fields.Add(new ReportGeneratorBase.FieldInfo("Bank Refund Fee", (object item) => ((TransactionReportVO)item).RefundDebitFee.GetValueOrDefault().ToAmountFormat()));
                fields.Add(new ReportGeneratorBase.FieldInfo("Bank CHB Fee", (object item) => ((TransactionReportVO)item).CapturedDebitFeeCHB.GetValueOrDefault().ToAmountFormat()));
                fields.Add(new ReportGeneratorBase.FieldInfo("Bank Authorized Fee", (object item) => ((TransactionReportVO)item).AuthorizedDebitFee.GetValueOrDefault().ToAmountFormat()));
                fields.Add(new ReportGeneratorBase.FieldInfo("Bank Fees Subtotal", (object item) => GetBankFeesSubtotal((TransactionReportVO)item).ToAmountFormat()));
            }

            if (cbShowFees.Checked && cbShowBankFees.Checked)
            {
                fields.Add(new ReportGeneratorBase.FieldInfo("Capture Profit", (object item) => ((TransactionReportVO)item).CaptureProfit.ToAmountFormat()));
                fields.Add(new ReportGeneratorBase.FieldInfo("Refund Profit", (object item) => ((TransactionReportVO)item).RefundProfit.ToAmountFormat()));
                fields.Add(new ReportGeneratorBase.FieldInfo("CHB Profit", (object item) => ((TransactionReportVO)item).CHBProfit.ToAmountFormat()));
                fields.Add(new ReportGeneratorBase.FieldInfo("Declined Profit", (object item) => ((TransactionReportVO)item).DeclinedProfit.ToAmountFormat()));
                fields.Add(new ReportGeneratorBase.FieldInfo("Total Profit", (object item) => ((TransactionReportVO)item).TotalProfit.ToAmountFormat()));
            }

            //fields.Add(new ReportGeneratorBase.FieldInfo("Sub Total", (object item) => GetLineSubtotal((TransactionReportVO)item).ToAmountFormat()));

            string header = (wcFiltersView.Visible ? wcFiltersView.RenderText() : null);
            var generator = new ReportGeneratorExcel();
            var result = generator.Generate(fields, results, null, header);
            DownloadExcel(result);
        }

        #region HelperFunctions

        public decimal GetTransSubtotal(TransactionReportVO transaction)
        {
            return transaction.PassedAmount + transaction.RefundsAmount + transaction.ChargebacksAmount;
        }

        public decimal GetFeesSubtotal(TransactionReportVO transaction)
        {
            return transaction.PassedTransactionFee + transaction.FailedTransactionFee + transaction.RefundFee + transaction.ChargebackFee + transaction.RatioFee + transaction.ClarificationFee + transaction.HandlingFee + transaction.ManualFee + transaction.FraudFee;
        }

        public decimal GetBankFeesSubtotal(TransactionReportVO transaction)
        {
            return transaction.CapturedDebitFee.GetValueOrDefault(0) +
                    transaction.DeclinedDebitFee.GetValueOrDefault(0) +
                    transaction.RefundDebitFee.GetValueOrDefault(0) +
                    transaction.CapturedDebitFeeCHB.GetValueOrDefault(0) +
                    transaction.AuthorizedDebitFee.GetValueOrDefault(0);
        }

        public decimal GetLineSubtotal(TransactionReportVO transaction)
        {
            decimal transSubTotal = transaction.PassedAmount + transaction.RefundsAmount + transaction.ChargebacksAmount;
            decimal feesSubTotal = transaction.PassedTransactionFee + transaction.FailedTransactionFee + transaction.RefundFee + transaction.ChargebackFee + transaction.RatioFee + transaction.ClarificationFee + transaction.HandlingFee + transaction.ManualFee + transaction.FraudFee;
            decimal bankFeesSubTotal = transaction.CapturedDebitFee.GetValueOrDefault(0) + transaction.DeclinedDebitFee.GetValueOrDefault(0) + transaction.RefundDebitFee.GetValueOrDefault(0) + transaction.CapturedDebitFeeCHB.GetValueOrDefault(0) + transaction.AuthorizedDebitFee.GetValueOrDefault(0);

            return transSubTotal + feesSubTotal + bankFeesSubTotal;
        }

        public decimal GetCapturedCountPercentage(TransactionReportVO transaction)
        {
            return Netpay.Infrastructure.Math.GetPercentage(transaction.PassedCount, transaction.TotalCount);
        }

        public decimal GetDeclinedCountPercentage(TransactionReportVO transaction)
        {
            return Netpay.Infrastructure.Math.GetPercentage(transaction.FailedCount, transaction.TotalCount);
        }

        public decimal GetChargebacksCountPercentage(TransactionReportVO transaction)
        {
            return Netpay.Infrastructure.Math.GetPercentage(transaction.ChargebacksCount, transaction.PassedCount);
        }

        public decimal GetPendingChargebacksCountPercentage(TransactionReportVO transaction)
        {
            return Netpay.Infrastructure.Math.GetPercentage(transaction.PendingChargebacksCount, transaction.PassedCount);
        }

        public decimal GetFraudsCountPercentage(TransactionReportVO transaction)
        {
            return Netpay.Infrastructure.Math.GetPercentage(transaction.FraudCount, transaction.PassedCount);
        }

        public decimal GetCapturedAmountPercentage(TransactionReportVO transaction)
        {
            return Netpay.Infrastructure.Math.GetPercentage(transaction.PassedAmount, transaction.TotalAmount);
        }

        public decimal GetDeclinedAmountPercentage(TransactionReportVO transaction)
        {
            return Netpay.Infrastructure.Math.GetPercentage(transaction.FailedAmount, transaction.TotalAmount);
        }

        public decimal GetChargebacksAmountsPercentage(TransactionReportVO transaction)
        {
            return Netpay.Infrastructure.Math.GetPercentage(System.Math.Abs(transaction.ChargebacksAmount), transaction.PassedAmount);
        }

        public decimal GetPendingChargebacksAmountsPercentage(TransactionReportVO transaction)
        {
            return Netpay.Infrastructure.Math.GetPercentage(System.Math.Abs(transaction.PendingChargebacksAmount), transaction.PassedAmount);
        }

        public decimal GetFraudsAmountsPercentage(TransactionReportVO transaction)
        {
            return Netpay.Infrastructure.Math.GetPercentage(System.Math.Abs(transaction.FraudAmount), transaction.PassedAmount);
        }

        public decimal GetAmountSubtotal(TransactionReportVO transaction)
        {
            return transaction.PassedAmount + transaction.RefundsAmount + transaction.ChargebacksAmount + transaction.PassedTransactionFee + transaction.FailedTransactionFee + transaction.RatioFee + transaction.ChargebackFee + transaction.ClarificationFee + transaction.HandlingFee + transaction.ManualFee + transaction.FraudFee;
        }

        public string GetPaymentMethod(TransactionReportVO transaction)
        {
            if (transaction.PaymentMethodID == null)
                return "";

            var paymetMethod = Bll.PaymentMethods.PaymentMethod.Get((int)transaction.PaymentMethodID);
            if (paymetMethod == null)
                return "";

            return paymetMethod.Name;
        }

        public string GetCountryFlag(string country)
        {
            if (country == null || country.Trim() == "")
                country = "--";

            return string.Format("<img border=\"0\" src=\"/NPCommon/ImgCountry/18X12/{0}.gif\" alt=\"{0}\" />", country);
        }

        public string GetCountryCode(string country)
        {
            if (country == null || country.Trim() == "")
                country = "--";

            return country;
        }

        public string GetCountryName(string countryCode)
        {
            if (countryCode == null || countryCode.Trim() == "")
                return "--";

            string countryName = Bll.Country.Get(countryCode).Name;
            if (countryName == "")
                return "--";

            return countryName;
        }

        #endregion
    }
}
