<%@ Page Title="Admin Reports - Bll Log" Language="C#" MasterPageFile="~/Reports.Master" AutoEventWireup="true" CodeBehind="BllLogs.aspx.cs" Inherits="Netpay.AdminReports.BllLogs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<style type="text/css">
		div.innerContent
		{
			width: 75%;
		}
	</style>
	<script type="text/javascript">	
		$(function() {
			$("#ctl00_body_datepickerFrom").datepicker({ dateFormat: 'dd-mm-yy' });
			//$("#ctl00_body_datepickerFrom").datepicker('setDate', -1);
			$("#ctl00_body_datepickerTo").datepicker({ dateFormat: 'dd-mm-yy' });
			//$("#ctl00_body_datepickerTo").datepicker('setDate', new Date());
		});
	</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
	<table style="width:100%;">		
		<tr>
			<td class="pageMainHeading">Log</td>			
		</tr>		
	</table>
	<br />
	<table class="formFilter" width="100%" border="0">
		<tr style="text-align:left;margin:0;padding:0">
			<td>From date:<br /><input id="datepickerFrom" type="text" runat="server" /></td>
			<td>To date:<br /><input id="datepickerTo" type="text" runat="server" /></td>
			<td>Severity:<br /><asp:DropDownList ID="ddlSeverityID" runat="server" EnableViewState="true" /></td>
			<td>Tag:<br /><asp:DropDownList ID="ddlTag" runat="server" EnableViewState="true" /></td>
			<td>Text:<br /><asp:TextBox ID="txtTextSearch" runat="server" EnableViewState="true" /></td>
			<td><asp:Button ID="btnFilter" runat="server" Text=" FILTER " UseSubmitBehavior="true" /></td>
			<!--<td><asp:LinkButton ID="btnDeleteLogs" Text="Delete Logs" OnClientClick="return confirm('Delete all log entries?')" OnClick="DeleteLogs" runat="server"></asp:LinkButton></td>-->
		</tr>
	</table>
	<br />
	<netpay:SearchFiltersView ID="wcFiltersView" runat="server" />
	<table border="0" width="100%" class="formNormal">
		<netpay:SortableColumns ID="ccSorting" VOType="BllLogVO" runat="server">
			<netpay:SortableColumn ColumnTitle="" Enabled="false" runat="server" />			
			<netpay:SortableColumn ColumnTitle="Severity" ColumnName="SeverityID" runat="server" CssStyle="text-align:left" />
			<netpay:SortableColumn ColumnTitle="Date" ColumnName="InsertDate" runat="server" CssStyle="text-align:left" />			
			<netpay:SortableColumn ColumnTitle="Instance" ColumnName="Source" Enabled="false" runat="server" CssStyle="text-align:left" />		
			<netpay:SortableColumn ColumnTitle="Tag" ColumnName="Tag" runat="server" CssStyle="text-align:left" />			
			<netpay:SortableColumn ColumnTitle="Info" Enabled="false" runat="server" CssStyle="text-align:left" />			
		</netpay:SortableColumns>
		<asp:Repeater ID="repeaterLogs" runat="server">
			<ItemTemplate>
				<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='#ffffff';">
					<td class="reportCell"><img src="Templates/<%# WebUtils.CurrentDomain.ThemeFolder%>/Images/tree_expand.gif" style="visibility:<%# ((Logger)Container.DataItem).LongMessage == null ? "hidden" : "visible" %>;" id='img_<%# ((Logger)Container.DataItem).ID %>' onclick='netpay.adminReports.Utils.ExpandRow(<%# ((Logger)Container.DataItem).ID %>);' style="cursor:pointer" /></td>					
					<td class="reportCell" nowrap="nowrap">
						<span class="logSeverity<%# ((Logger)Container.DataItem).Severity %>" style="width:15px;height:5px;margin-bottom:2px;">&nbsp;&nbsp;&nbsp;&nbsp;</span>
						<%# ((Logger)Container.DataItem).Severity %>
					</td>
					<td class="reportCell" nowrap="nowrap">
						<%# ((Logger)Container.DataItem).InsertDate %>
					</td>
					<td class="reportCell" nowrap="nowrap">
						<%# ((Logger)Container.DataItem).Source %>					
					</td>	
					<td class="reportCell" nowrap="nowrap">
						<%# ((Logger)Container.DataItem).Tag %>					
					</td>					
					<td class="reportCell">
						<%# ((Logger)Container.DataItem).Message %>					
					</td>										
				</tr>
				<tr style="margin:0;padding:0; display:none;" id='div_<%# ((Logger)Container.DataItem).ID %>'>
					<td colspan="6" style="margin:0;padding:0">
						<table cellpadding="0" cellspacing="0">	
							<tr>
								<td>
									<pre style="width:0px;"><asp:Literal ID="ltMoreInfo" Text='<%# FormatInfo((Logger)Container.DataItem) %>' runat="server" /></pre>							
								</td>
							</tr>
						</table>
					</td>
				</tr>				
			</ItemTemplate>
		</asp:Repeater>
	</table>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td style="text-align:left;margin:0;padding:0">
				<netpay:Pager ID="pager" runat="server" />
			</td>
		</tr>
	</table>
</asp:Content>