﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Netpay.Bll;
using Netpay.Infrastructure;

namespace Netpay.AdminReports
{
	public class Global : System.Web.HttpApplication
	{
		protected void Application_Start(object sender, EventArgs e)
		{
            Netpay.Web.ContextModule.AppName = "Admin Reports";
		}

        void Application_BeginRequest(object sender, EventArgs e)
        {
            var application = sender as HttpApplication;
            if (application != null && application.Context != null)
            {
                application.Context.Response.Headers.Remove("Server");
            }
        }

        protected void Session_Start(object sender, EventArgs e)
		{
            if (!Web.WebUtils.IsLoggedin) Web.WebUtils.Login();
        }

        protected void Session_End(object sender, EventArgs e)
        {
            //if (Web.WebUtils.IsLoggedin) Web.WebUtils.Logout(); //does not have domain context
        }
        
		protected void Application_EndRequest(object sender, EventArgs e)
		{
			/*
			if (HttpContext.Current.Request.Path.ToLower().EndsWith(".aspx"))
			{
				DateTime startTime = (DateTime)Context.Items["requestStart"];
				TimeSpan duration = DateTime.Now - startTime;
				Response.Write(string.Format("<center class=\"requestTimer\">Request processed in {0} seconds.</center>", duration.TotalSeconds.ToString("0.00"))); 
			}
			*/
		}
		
		protected void Application_Error(object sender, EventArgs e)
		{
			Exception ex = Server.GetLastError();
			if (ex is System.Web.HttpException) {
				if (ex.Message.StartsWith("Validation of viewstate MAC failed.")){
					Response.Redirect("~/Login.aspx");
					return;
				}
			}
			Netpay.Infrastructure.Logger.Log(LogTag.NetpayAdmin, ex);
		}

        protected void Application_AcquireRequestState(Object sender, EventArgs e)
        {
            if (Request.IsSecureConnection) return;
            if (Request.ServerVariables["HTTP_HOST"].StartsWith("192.168") || Request.ServerVariables["HTTP_HOST"].StartsWith("10.0") || Request.ServerVariables["HTTP_HOST"] == "localhost") return;
            if (Web.WebUtils.CurrentDomain.ForceSSL)
                Response.Redirect("https://" + Request.Url.ToString().Substring(7));
        }

        protected void Application_End(object sender, EventArgs e)
		{

		}
	}
}