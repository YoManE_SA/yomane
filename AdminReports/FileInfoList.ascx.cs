﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Bll.Reports;

namespace Netpay.AdminReports
{
	public partial class FileInfoList : System.Web.UI.UserControl
	{
		public ReportType ReportType { get; set; }
		public List<FileInfo> dataSource { get; set; }

		private Netpay.AdminReports.Code.NetpayPage NPPage { get { return base.Page as Netpay.AdminReports.Code.NetpayPage; } }
		public void LoadReportList(ReportType reportType)
		{
			ReportType = reportType;
            try {
                Infrastructure.ObjectContext.Current.Impersonate(Infrastructure.Domain.Current.ServiceCredentials);
                dataSource = Netpay.Bll.Reports.FiledReports.GetGeneratedReports(ReportType);
            } finally {
                Infrastructure.ObjectContext.Current.StopImpersonate();
            }
			if (Request["Archive"] == "1") {
				repeaterGeneratedReports.DataSource = dataSource.Skip(30);
				lnkShowArchive.InnerText = "Show Last";
				lnkShowArchive.HRef = "?";
			} else repeaterGeneratedReports.DataSource = dataSource.Take(30);
			repeaterGeneratedReports.DataBind();
			lnkShowArchive.Visible = (dataSource.Count > 30);
		}

		/// <summary>
		/// Need to do this through bll when admin login is complete
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void DeleteReport(object sender, CommandEventArgs e)
		{
			string filePath = e.CommandArgument.ToString();

			if (File.Exists(filePath)){
				File.Delete(filePath);
				LoadReportList(ReportType);
			}
		}

		protected void DownloadReport(object sender, CommandEventArgs e)
		{
			Netpay.AdminReports.Code.NetpayPage.DownloadFile(e.CommandArgument.ToString(), "text/csv");
		}
	}
}