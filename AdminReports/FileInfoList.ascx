﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FileInfoList.ascx.cs" Inherits="Netpay.AdminReports.FileInfoList" %>
<%@ Import Namespace="System.IO" %>
<table>
	<tr>
		<th style="text-align:left;"><asp:Literal Text="Date Created" runat="server"></asp:Literal></th>
		<th style="text-align:left;"><asp:Literal Text="File Name" runat="server"></asp:Literal></th>
		<th style="text-align:left;"><asp:Literal Text="File Size" runat="server"></asp:Literal></th>
		<th style="text-align:left;"><asp:Literal Text="Action" runat="server"></asp:Literal></th>
	</tr>
	<asp:Repeater ID="repeaterGeneratedReports" runat="server">
		<ItemTemplate>
			<tr onmouseover="rowHighlight(this)" onmouseout="rowNormal(this, '#ffffff')">
				<td><%# ((FileInfo)Container.DataItem).LastWriteTime.ToString("dd/MM/yyyy HH:mm")%></td>
				<td><%# ((FileInfo)Container.DataItem).Name%></td>
				<td><%# ((FileInfo)Container.DataItem).Length.ToFileSize()%></td>
				<td>
					<asp:LinkButton ID="LinkButton1" Text="Download" CommandArgument="<%# ((FileInfo)Container.DataItem).FullName %>" OnCommand="DownloadReport" runat="server" />&nbsp;|
					<asp:LinkButton ID="LinkButton2" Text="Delete" CommandArgument="<%# ((FileInfo)Container.DataItem).FullName %>" OnCommand="DeleteReport" OnClientClick="return confirm('Are you sure?')" runat="server" />
				</td>
			</tr>
		</ItemTemplate>
	</asp:Repeater>
</table>
<br />
<a id="lnkShowArchive" runat="server" href="?Archive=1">Archive</a>
