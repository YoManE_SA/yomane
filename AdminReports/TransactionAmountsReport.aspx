﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports.Master" AutoEventWireup="true" CodeBehind="TransactionAmountsReport.aspx.cs" Inherits="Netpay.AdminReports.TransactionAmountsReport" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<style type="text/css">
		div.innerContent { width: 100%; }
	</style>
	<script type="text/javascript">
		$(function()
		{
			jQuery("input[type='checkbox'][id^='ctl00_body_cbGroupBy']").button();
		});
		
		function validate()
		{
			if (!netpay.webControls.DateRangePicker.validate())
				return false;

			// group selection
			var isGroupChecked = jQuery("input[type='checkbox'][id^='ctl00_body_cbGroupBy']:checked").length > 0;
			if (!isGroupChecked)
			{
				netpay.Common.alert("Please select a group.");
				return false;
			}
			
			// currency group / selection
			var isCurrencySelected = jQuery("#ctl00_body_ddCurrency option:selected").text() != "";
			var isCurrencyGrouped = jQuery("#ctl00_body_cbGroupByCurrency").attr("checked");
			if(!isCurrencySelected && !isCurrencyGrouped)
			{
				netpay.Common.alert("Please select a currency or group by currency.");
				return false;			
			}

			return true;
		}

		function rowHighlight(rowElementID)
		{
			jQuery(rowElementID).css("background-color", "#FFFFAA");
		}

		function rowNormal(rowElementID, normalBackgroundColor)
		{
			jQuery(rowElementID).css("background-color", normalBackgroundColor);
		}	
	</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
	<table style="width:100%;">		
		<tr>
			<td class="pageMainHeading">Transactions report</td>
			<td style="text-align:right;">
				<netpay:ReportUpdateInfo ID="wcReportUpdateInfo" runat="server" Report="AdminTransactionsReport"/>
				&nbsp;|&nbsp;
				<asp:Image ID="Image1" ImageUrl="Images/ImgFileExt/xls.gif" runat="server" />
				<asp:LinkButton ID="lnkBtnExcel" Text="EXPORT TO XLS" OnClick="CreateExcel" CssClass="ExportExcelTxt" runat="server" />		
			</td>
		</tr>
	</table>
	<br />
	
	<!-- filters -->
	<table style="width:100%;" class="formFilter" border="0">
		<tr>
			<td>
				<table border="0">
					<tr>
						<td valign="top">
							Amounts<br />
							<netpay:DragDropSelector ID="ddsAmountTypes" Height="60" runat="server" />
						</td>
						<td valign="top">
							<netpay:DateRangePicker ID="wcDateRangePicker" runat="server" />
						</td>
						<td valign="top">
							<table>
								<tr>	
									<td>Merchant<br /><netpay:AutoComplete ID="wcMerchantAutoComplete" IsMultiselect="true" Function="GetMerchantNameAutoComplete" runat="server" /></td>
									<td>Bank<br /><netpay:DebitCompanyDropDown ID="ddlDebitCompany" EnableBlankSelection="true" Width="120px"  runat="server"></netpay:DebitCompanyDropDown></td>				
								</tr>
								<tr>
									<td>Terminal<br /><netpay:AutoComplete ID="wcTerminalAutoComplete" Function="GetDebitTerminalAutoComplete" runat="server" /></td>
									<td>Currency<br /><netpay:CurrencyDropDown ID="ddCurrency" EnableBlankSelection="true" Width="100%" runat="server"></netpay:CurrencyDropDown></td>		
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<!-- groups -->
		<tr>
			<td style="border-top:dotted 1px gray; padding:4px;">
				<table>
					<tr>
						<td>
							Group By  &nbsp;&nbsp;
							<asp:CheckBox ID="cbGroupByCurrency" runat="server" Text="Currency" CssClass="option" /> 
							<asp:CheckBox ID="cbGroupByMerchant" runat="server" Text="Merchant" CssClass="option" /> 
							<asp:CheckBox ID="cbGroupByTerminal" runat="server" Text="Terminal" CssClass="option" /> 			
							<asp:CheckBox ID="cbGroupByBank" runat="server" Text="Bank" CssClass="option" /> 
							<asp:CheckBox ID="cbGroupByAmountType" runat="server" Text="Amount" CssClass="option" /> 
						</td>
						<td style="width:30px;"></td>
						<td>
							<input type="reset" value="RESET" style="width:70px;" /> &nbsp;
							<asp:Button ID="btnGo" Text="SEARCH" Width="80px" OnClientClick="return validate();" OnClick="GetReport" UseSubmitBehavior="true" runat="server" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br />
	<netpay:SearchFiltersView ID="wcFiltersView" runat="server" />
	<br />
	<!-- data -->
	<asp:PlaceHolder ID="phResults" Visible="false" runat="server">
		<table style="width:100%;" cellspacing="0" class="formNormal">
			<tr>
				<th id="thCurrency" runat="server">Currency</th>
				<th id="thBank" runat="server">Bank</th>
				<th id="thTerminal" runat="server">Terminal</th>
				<th id="thMerchant" runat="server">Merchant</th>
				<th id="thAmountType" runat="server">Amount Type</th>
				<th style="background-color:White; border-bottom:0px;">&nbsp;</th>
				<th>Amount</th>
				<th style="background-color:White; border-bottom:0px;">&nbsp;</th>
				<asp:Literal ID="literalAmountsHeaders" runat="server"></asp:Literal>
			</tr>
			<tbody>
				<netpay:DynamicRepeater ID="repeaterResults" EnableViewState="false" runat="server">
					<ItemTemplate>
						<tr onmouseover="rowHighlight(this)" onmouseout="rowNormal(this, '#ffffff')">
							<td id="tdCurrencyCode" runat="server">
								<%# ((DataRowView)Container.DataItem).ToString("TransactionCurrencyIsoCode") %>
							</td>
							<td id="tdBankName" runat="server">
								<netpay:BankView ID="BankView1" BankID='<%# ((DataRowView)Container.DataItem).ToNullableInt32("AcquiringBankID") %>' BankName='<%# ((DataRowView)Container.DataItem).ToString("AcquiringBankName") %>' runat="server" />				
							</td>
							<td id="tdTerminalName" runat="server">
								<%# ((DataRowView)Container.DataItem).ToString("TerminalName") %>
							</td>
							<td id="tdMerchantName" runat="server">
								<%# ((DataRowView)Container.DataItem).ToString("MerchantName") %>
							</td>
							<td id="tdAmountTypeName" runat="server">
								<%# ((DataRowView)Container.DataItem).ToString("AmountTypeName") %>
							</td>
							<td style="background-color:#ffffff!important;">&nbsp;</td>
							<td class="numericCell">
								<span style="color: #999999;">(<%# ((DataRowView)Container.DataItem).ToInt32("TotalCount").ToString("#,0")%>)</span> 
								<%# GetFormattedAmount(((DataRowView)Container.DataItem).ToNullableInt32("TransactionCurrencyID"), ((DataRowView)Container.DataItem).ToNullableDecimal("TotalAmount"))%>
							</td>
							<th style="background-color:White; border-bottom:0px;">&nbsp;</th>
							<%# GetAmountsData((DataRowView)Container.DataItem) %>
						</tr>				
					</ItemTemplate>
					<AlternatingItemTemplate>
						<tr style="background-color:#F7F7F7;" onmouseover="rowHighlight(this)" onmouseout="rowNormal(this, '#F7F7F7')">
							<td id="tdCurrencyCode" runat="server">
								<%# ((DataRowView)Container.DataItem).ToString("TransactionCurrencyIsoCode") %>
							</td>
							<td id="tdBankName" runat="server">
								<netpay:BankView ID="BankView1" BankID='<%# ((DataRowView)Container.DataItem).ToNullableInt32("AcquiringBankID") %>' BankName='<%# ((DataRowView)Container.DataItem).ToString("AcquiringBankName") %>' runat="server" />				
							</td>
							<td id="tdTerminalName" runat="server">
								<%# ((DataRowView)Container.DataItem).ToString("TerminalName") %>
							</td>
							<td id="tdMerchantName" runat="server">
								<%# ((DataRowView)Container.DataItem).ToString("MerchantName") %>
							</td>
							<td id="tdAmountTypeName" runat="server">
								<%# ((DataRowView)Container.DataItem).ToString("AmountTypeName") %>
							</td>
							<td style="background-color:#ffffff!important;">&nbsp;</td>
							<td class="numericCell">
								<span style="color: #999999;">(<%# ((DataRowView)Container.DataItem).ToInt32("TotalCount").ToString("#,0") %>)</span> 
								<%# GetFormattedAmount(((DataRowView)Container.DataItem).ToNullableInt32("TransactionCurrencyID"), ((DataRowView)Container.DataItem).ToNullableDecimal("TotalAmount"))%>
							</td>
							<th style="background-color:White; border-bottom:0px;">&nbsp;</th>
							<%# GetAmountsData((DataRowView)Container.DataItem) %>
						</tr>			
					</AlternatingItemTemplate>
				</netpay:DynamicRepeater>
				
				<netpay:DynamicRepeater ID="repeaterSummary" EnableViewState="false" runat="server">
					<ItemTemplate>
						<tr>
							<td id="tdBankName" style="border-top:1px solid black;" runat="server"></td>
							<td id="tdTerminalName" style="border-top:1px solid black;" runat="server"></td>
							<td id="tdCurrencyCode" style="border-top:1px solid black;" runat="server"></td>
							<td id="tdMerchantName" style="border-top:1px solid black;" runat="server"></td>
							<td id="tdAmountTypeName" style="border-top:1px solid black;" runat="server"></td>
							<td style="border-top:0px solid white!important;">&nbsp;</td>
							<td class="numericCell" style="font-weight:bold; border-top:1px solid black;">
								<span style="color: #999999;">(<%# ((DataRowView)Container.DataItem).ToInt32("TotalCount").ToString("#,0")%>)</span> 
								<%# GetFormattedAmount(((DataRowView)Container.DataItem).ToNullableInt32("TransactionCurrencyID"), ((DataRowView)Container.DataItem).ToNullableDecimal("TotalAmount"))%>
							</td>
						</tr>
					</ItemTemplate>
				</netpay:DynamicRepeater>
			</tbody>
		</table>
	</asp:PlaceHolder>
</asp:Content>
