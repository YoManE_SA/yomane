﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure;
using Netpay.Bll;
using Netpay.Bll.Reports;
using Netpay.Bll.Reports.VO;
using Netpay.Web;
using Netpay.AdminReports.Code;

namespace Netpay.AdminReports
{
	public partial class BankTransfers : NetpayPage
	{
		protected void Page_LoadComplete(object sender, EventArgs e)
		{
			if (! IsPostBack)
			{
				// set date picker
				wcDateRangePicker.MinDate = IntegrationService.DenormalizedMinDate;
				wcDateRangePicker.MaxDate = IntegrationService.DenormalizedMaxDate;
				wcDateRangePicker.FromDate = IntegrationService.DenormalizedMaxDate.AddDays(-7);
				wcDateRangePicker.ToDate = IntegrationService.DenormalizedMaxDate;
			}

            List<DenormalizedTransactionVO> results = Netpay.Bll.Reports.ImmediateReports.GetDenormalizedTransactions(GetFilters(), pager.Info);
			repeaterReport.DataSource = results;
			repeaterReport.DataBind();

			phResults.Visible = results.Count > 0;
		}

		protected SearchFilters GetFilters()
		{
			SearchFilters filters = new SearchFilters();
			filters.transactionStatus = TransactionStatus.Captured;
			if (wcDateRangePicker.IsFromDateSelected)
			{
				wcFiltersView.Add("From Date", wcDateRangePicker.FromDate.Value.ToString("dd/MM/yyyy"));
				filters.dateFrom = wcDateRangePicker.FromDate;
			}
			if (wcDateRangePicker.IsToDateSelected)
			{
				wcFiltersView.Add("To Date", wcDateRangePicker.ToDate.Value.ToString("dd/MM/yyyy"));
				filters.dateTo = wcDateRangePicker.ToDate;
			}
			
			if (rbTransferred.Checked)
			{
				wcFiltersView.Add("Transfer Status", "Transferred");
				filters.isBankTransferReceived = true;
			}
			else if (rbNotTransferred.Checked)
			{
				wcFiltersView.Add("Transfer Status", "Not Transferred");
				filters.isBankTransferReceived = false;
			}
			else
			{
				wcFiltersView.Add("Transfer Status", "Both");
			}
			
			if (txtTransferID.Text != "")
			{
				wcFiltersView.Add("Transfer ID", txtTransferID.Text);
				filters.bankTransferID = txtTransferID.Text;
			}

			if (wcTerminalAutoComplete.IsSelected)
			{
				filters.terminalNumber = wcTerminalAutoComplete.SelectedValue;
				wcFiltersView.Add("Terminal Number", wcTerminalAutoComplete.SelectedValue);
			}

			if (wcMerchantAutoComplete.IsSelected)
			{
				filters.merchantIDs = wcMerchantAutoComplete.GetSelectedValues<int>();
				if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
					wcFiltersView.Add("Merchant", wcMerchantAutoComplete.SelectedValue);
			}
			
			if (ddlDebitCompanyID.SelectedItem != null)
			{
				wcFiltersView.Add("Bank", ddlDebitCompanyID.SelectedItem.Text);
				filters.debitCompanyID = int.Parse(ddlDebitCompanyID.SelectedItem.Value);
			}
				
			return filters;		
		}

		protected void CreateExcel(object sender, EventArgs e)
		{
			List<DenormalizedTransactionVO> data = Netpay.Bll.Reports.ImmediateReports.GetDenormalizedTransactions(GetFilters(), null);
			if (data.Count > 10000)
				return;

			// fields
			List<ReportGeneratorBase.FieldInfo> fields = new List<ReportGeneratorBase.FieldInfo>();
			fields.Add(new ReportGeneratorBase.FieldInfo("Transaction", "TransactionID"));
			fields.Add(new ReportGeneratorBase.FieldInfo("Status", "TransactionStatus"));
			fields.Add(new ReportGeneratorBase.FieldInfo("Date", "TransactionDate"));
			fields.Add(new ReportGeneratorBase.FieldInfo("Currency", "TransactionCurrencyIsoCode"));
			fields.Add(new ReportGeneratorBase.FieldInfo("Amount", "TransactionAmount"));
			fields.Add(new ReportGeneratorBase.FieldInfo("Transfer ID", "BankTransferID"));

            var generator = new ReportGeneratorExcel();
            var result = generator.Generate(fields, data, null, wcFiltersView.RenderText());
			DownloadExcel(result);
		}
	}
}
