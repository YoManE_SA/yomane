﻿<%@ Page Title="Admin Reports - Fail Stat" Language="C#" MasterPageFile="~/Reports.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="FailStatReport.aspx.cs" Inherits="Netpay.AdminReports.FailStatReport"%>

<asp:Content ID="content2" ContentPlaceHolderID="head" runat="server">
	<style type="text/css">
		div.innerContent { width: 75%; }
	</style>
	<script type="text/javascript">
		function validate()
		{
			return netpay.webControls.DateRangePicker.validate();
		}

		function ClearFormLocal(formElement) 
		{
			Netpay.AdminReports.Utils.ClearForm(formElement);
			for (i = 0; i < formElement.elements.length; i++) 
			{
				if (formElement.elements[i].type == "hidden" && formElement.elements[i].id.indexOf("hidValue") > 0) 
				{
					formElement.elements[i].value = "";
				}
			}
		}		
	</script>
</asp:Content>

<asp:Content ID="content1" ContentPlaceHolderID="body" runat="server">
	<table style="width:100%;">		
		<tr>
			<td class="pageMainHeading">Failure stats</td>
			<td rowspan="2" style="text-align:right;">
				<netpay:ReportUpdateInfo ID="wcReportUpdateInfo" runat="server" Report="AdminFailStats" /> 
				&nbsp;|&nbsp;
				<asp:Image ID="Image1" ImageUrl="Images/ImgFileExt/xls.gif" runat="server" />
				<asp:LinkButton ID="lnkBtnExcel" Text="EXPORT TO XLS" OnClick="CreateExcel" CssClass="ExportExcelTxt" runat="server" />
			</td>
		</tr>		
	</table>
	<br />	
	<table style="width:100%;" class="formFilter" border="0">
		<tr>
			<td>
				<table border="0">
					<tr>
						<td valign="top">
							<netpay:DateRangePicker ID="wcDateRangePicker" runat="server" />
						</td>
						<td valign="top">
							<table>
								<tr style="text-align:left;margin:0;padding:0">		
									<td>&nbsp;Merchant<br /><netpay:AutoComplete ID="wcMerchantAutoComplete" Function="GetMerchantNameAutoComplete" IsMultiselect="false" runat="server" /></td>
									<td>Debit Company<br /><netpay:DebitCompanyDropDown ID="ddlDebitCompany" EnableBlankSelection="true" Width="150px" runat="server"></netpay:DebitCompanyDropDown></td>
									<td>Source<br />
										<netpay:DropDownBase ID="ddRejectionSource" Width="150px" EnableBlankSelection="true" runat="server">
											<asp:ListItem Text="" Value=""></asp:ListItem>
											<asp:ListItem Text="Issuer" Value="0"></asp:ListItem>
											<asp:ListItem Text="Risk" Value="1"></asp:ListItem>
											<asp:ListItem Text="Gateway" Value="2"></asp:ListItem>
										</netpay:DropDownBase>
									</td>
									<%--<td>Requests<br /><asp:CheckBox ID="chkExcludeMultiple" Enabled="false" runat="server" Checked="false" Text="Exclude multiple tries" /></td>--%>
								</tr>
								<tr style="vertical-align:top;text-align:left;margin:0;padding:0">
									<td>&nbsp;Terminal<br /><netpay:AutoComplete ID="wcTerminalAutoComplete" Function="GetDebitTerminalAutoComplete" runat="server" /></td>
									<td>Payment Method<br /><netpay:PaymentMethodDropDown ID="ddlPaymentMethodID" Width="150px" runat="server"></netpay:PaymentMethodDropDown></td>
									<td>Currency<br /><netpay:CurrencyDropDown ID="ddlCurrencyID" Width="150px" runat="server"></netpay:CurrencyDropDown></td>
								</tr>
							</table>
						</td>
						<td valign="top" style="padding-top:4px;">
							Transaction Type<br />
							<asp:CheckBox ID="chkTransTypeDebit" runat="server" Checked="true" Text="Debit" /><br />
							<asp:CheckBox ID="chkTransTypePreAuth" runat="server" Checked="true" Text="PreAuth" /><br />
							<asp:CheckBox ID="chkTransTypeCapture" runat="server" Checked="true" Text="Capture" /><br />
						</td>
						<td style="padding-left:40px;">
							<input type="button" value="RESET" style="width:80px;" onclick="ClearFormLocal(this.form)" /><br /><br />
							<asp:Button ID="btnFilter" OnClientClick="return validate();" runat="server" Text="SEARCH" Width="80px" OnClick="FilterReport" UseSubmitBehavior="true" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br />
	<netpay:SearchFiltersView ID="wcFiltersView" runat="server" />
	<asp:PlaceHolder ID="phResults" Visible="false" runat="server">
		<table style="width:100%;" class="formNormal" border="0">
			<thead>
				<netpay:SortableColumns ID="ccSorting" OnSortChange="SortChange" runat="server">			
					<netpay:SortableColumn ColumnTitle="Count" ColumnName="count" runat="server" CssStyle="text-align:left" />
					<netpay:SortableColumn ColumnTitle="Amount" ColumnName="amount" Enabled="true" runat="server" CssStyle="text-align:left" />	
					<netpay:SortableColumn ColumnTitle="Code" ColumnName="code" runat="server" CssStyle="text-align:left" />			
					<netpay:SortableColumn ColumnTitle="Description" Enabled="false" runat="server" CssStyle="text-align:left" />			
					<netpay:SortableColumn ColumnTitle="Source" ColumnName="source" runat="server" CssStyle="text-align:left" />					
					<netpay:SortableColumn ColumnTitle="Details" Enabled="false" runat="server" CssStyle="text-align:left" />		
				</netpay:SortableColumns>
			</thead>	
			<tbody>
				<asp:Repeater ID="repeaterReport" runat="server">
					<ItemTemplate>
						<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
							<td class="reportCell numericCell">
								<%# ((FailStatGroupVO)Container.DataItem).GroupCount %>
							</td>
							<td class="reportCell" style="text-align:right">
								<%# GetAmount((FailStatGroupVO)Container.DataItem) %>&nbsp;
							</td>
							<td class="reportCell numericCell">
								<%# ((FailStatGroupVO)Container.DataItem).RejectCode %>&nbsp;							
							</td>
							<td class="reportCell">
								<%# ((FailStatGroupVO)Container.DataItem).RejectText %>&nbsp;
							</td>
							<td class="reportCell">
								<%# ((FailStatGroupVO)Container.DataItem).GetRejectedSourceText() %>&nbsp;
							</td>
							<td class="reportCell numericCell">
								<asp:Label ID="Label1" runat="server" Text='<%# FormatLink((FailStatGroupVO)Container.DataItem)%>' />
							</td>
						</tr>
					</ItemTemplate>
				</asp:Repeater>				
				<tr>
					<td colspan="6" style="border-bottom-width:1px; border-bottom-color:Black; border-bottom-style:solid;">&nbsp;</td>
				</tr>
				<tr>
					<td class="numericCell" style="font-weight:bold;"> <%= GetTotalCount() %> </td>
					<td class="numericCell" style="font-weight:bold;"> <%= GetTotalAmount() %> </td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</tbody>
		</table>		
	</asp:PlaceHolder>
	<asp:HiddenField ID="hdSortField" Value="GroupCount" runat="server" />	
	<asp:HiddenField ID="hdSortOrder" Value="Desc" runat="server" />	
</asp:Content>