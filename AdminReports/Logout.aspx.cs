﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.AdminReports.Code;
using Netpay.Web;
using Netpay.Infrastructure.Security;

namespace Netpay.AdminReports
{
    public partial class Logout : NetpayPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Logout();
            Response.Redirect("Login.aspx");
        }
    }
}