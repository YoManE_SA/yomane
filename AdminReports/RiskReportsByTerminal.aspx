﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="RiskReportsByTerminal.aspx.cs" Inherits="Netpay.AdminReports.RiskReportsByTerminal" %>
<%@ Register TagPrefix="NP" TagName="FileInfoList" Src="~/FileInfoList.ascx" %>
<%@ Import Namespace="System.IO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<style type="text/css">
		div.innerContent { width: 75%; }
	</style>
	<script type="text/javascript">
		function rowHighlight(rowElementID) {
			jQuery(rowElementID).css("background-color", "#FFFFAA");
		}

		function rowNormal(rowElementID, normalBackgroundColor) {
			jQuery(rowElementID).css("background-color", normalBackgroundColor);
		}	
	</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
	<table style="width:100%;">		
		<tr>
			<td class="pageMainHeading">Risk - Tendency by Terminal</td>
			<td style="text-align:right;"><netpay:ReportUpdateInfo ID="wcReportUpdateInfo" runat="server" Report="AdminDailyRiskByTerminal" /></td>
		</tr>
	</table>
	<br /><br />
	<asp:PlaceHolder ID="phFiles" runat="server">
		<table style="width:100%; height:100%;">
			<tr>
				<td class="fileListRecent" height="100%" valign="top">
					<table>
					<tr><th>MOST RECENT</th></tr>
					<tr><td><asp:Label ID="lblCurFileDate" runat="server" /></td></tr>
					<tr><td><asp:Image ID="imgCurFileIcon" ImageAlign="AbsMiddle" runat="server" /> &nbsp; <asp:Label ID="lblCurFileSize" runat="server" /></td></tr>
					<tr><td><br /><asp:Button ID="btnCurFile" Text="DOWNLOAD" OnCommand="DownloadReport" CssClass="btnDownload" runat="server" /></td></tr>
					</table>		
				</td>
				<td style="width:30px;"></td>
				<td class="fileListHistory" valign="top">
					<NP:FileInfoList runat="server" id="FileInfoList1" />
				</td>
			</tr>
		</table>	
	</asp:PlaceHolder>
	<br /><br />
</asp:Content>
