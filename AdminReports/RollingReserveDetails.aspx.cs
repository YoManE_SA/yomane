﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Netpay.AdminReports.Code;
using Netpay.Web;
using Netpay.Bll.Reports;
using Netpay.Bll.Reports.VO;

namespace Netpay.AdminReports
{
	public partial class RollingReserveDetails : NetpayPage
	{
		List<RollingReserveReportDetailVO> dataSource;		

		protected void Page_LoadComplete(object sender, EventArgs e)
		{
			bool companyIDParseResult, currencyIDParseResult;
			int companyID, currencyID;
			companyIDParseResult = int.TryParse(HttpContext.Current.Request["CompanyID"].ToString(), out companyID);
			currencyIDParseResult = int.TryParse(HttpContext.Current.Request["CurrencyID"].ToString(), out currencyID);
			if (currencyIDParseResult && companyIDParseResult)
			{
				dataSource = Netpay.Bll.Reports.ImmediateReports.GetRollingReserveDetailReport(companyID, currencyID, ccSorting.Info);
				repeaterReport.DataSource = dataSource;
				repeaterReport.DataBind();
				lblTotal.Text = Netpay.Bll.Reports.ImmediateReports.GetRollingReserveDetailTotal(companyID, currencyID);
                string merchantName;
                if (!Netpay.Bll.Merchants.Merchant.CachedNamesForDomain().TryGetValue(companyID, out merchantName)) merchantName = companyID.ToString();
                wcFiltersView.Add("Merchant", merchantName);
				wcFiltersView.Add("Currency", Bll.Currency.Get(currencyID).IsoCode);
			}
		}

		protected string FormatAmount(RollingReserveReportDetailVO item)
		{
			return item.CurrencySymbol + (item.ReserveCreditType == 0 ? "-" : "") + item.Amount.ToString("#,##0.00");
		}

		protected void CreateExcel(object sender, EventArgs e)
		{
			bool companyIDParseResult, currencyIDParseResult;
			int companyID, currencyID;
			companyIDParseResult = int.TryParse(HttpContext.Current.Request["CompanyID"].ToString(), out companyID);
			currencyIDParseResult = int.TryParse(HttpContext.Current.Request["CurrencyID"].ToString(), out currencyID);
			if (companyIDParseResult && currencyIDParseResult)
				dataSource = Netpay.Bll.Reports.ImmediateReports.GetRollingReserveDetailReport(companyID, currencyID, ccSorting.Info);
			else 
				return;

			// fields
			List<ReportGeneratorBase.FieldInfo> fields = new List<ReportGeneratorBase.FieldInfo>();
			fields.Add(new ReportGeneratorBase.FieldInfo("Insert Date", "InsertDate"));
			fields.Add(new ReportGeneratorBase.FieldInfo("Company Name", "CompanyName"));
			fields.Add(new ReportGeneratorBase.FieldInfo("Company ID", "CompanyID"));
			fields.Add(new ReportGeneratorBase.FieldInfo("Transaction ID", "TransactionID"));
			fields.Add(new ReportGeneratorBase.FieldInfo("Settlement ID", "SettlementID"));
			fields.Add(new ReportGeneratorBase.FieldInfo("Currency", "CurrencySymbol"));
			fields.Add(new ReportGeneratorBase.FieldInfo("Amount", "Amount"));
			fields.Add(new ReportGeneratorBase.FieldInfo("Reserved/Released", "ReserveCreditTypeTitle"));
			fields.Add(new ReportGeneratorBase.FieldInfo("Comment", "ReserveComment"));

            var generator = new ReportGeneratorExcel();
            var result = generator.Generate(fields, dataSource, null, wcFiltersView.RenderText());
            DownloadExcel(result);
		}
	}
}
