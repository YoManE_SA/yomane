﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Bll;
using Netpay.AdminReports.Code;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.AdminReports
{
	public partial class BllLogs : NetpayPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{			

		}

		protected void DeleteLogs(object sender, EventArgs e)
		{
			Infrastructure.Logger.DeleteLogs(null);
		}	
	
		protected void Page_LoadComplete(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{				
				ddlSeverityID.DataSource = Enums.GetEnumDataSource<LogSeverity>();
				ddlSeverityID.DataTextField = "Key";
				ddlSeverityID.DataValueField = "Value";
				ddlSeverityID.DataBind();
				ddlSeverityID.Items.Insert(0, new System.Web.UI.WebControls.ListItem("", "-1"));

				ddlTag.DataSource =  Enums.GetEnumDataSource<LogTag>();
				ddlTag.DataTextField = "Key";
				ddlTag.DataValueField = "Key";
				ddlTag.DataBind();
				ddlTag.Items.Insert(0, new System.Web.UI.WebControls.ListItem("", "-1"));
				
				// set default dates
				datepickerFrom.Value = DateTime.Now.AddDays(-2).ToString("dd-MM-yyyy");
				datepickerTo.Value = DateTime.Now.ToString("dd-MM-yyyy");				
			}
			
			var logs = Infrastructure.Logger.GetLogs(GetFilters(), pager.Info.CopySortFrom(ccSorting.Info));
			repeaterLogs.DataSource = logs;
			repeaterLogs.DataBind();
		}

		private Infrastructure.Logger.SearchFilters GetFilters()
		{
			var filters = new Infrastructure.Logger.SearchFilters();
			DateTime dateFrom, dateTo;
			if (DateTime.TryParse(datepickerFrom.Value, out dateFrom))
			{
				filters.Date.From = dateFrom.MinTime();
				wcFiltersView.Add("From date", dateFrom.ToString("dd/MM/yyyy"));
			}
			if (DateTime.TryParse(datepickerTo.Value, out dateTo))
			{
				filters.Date.To = dateTo.MaxTime();
				wcFiltersView.Add("To date", dateTo.ToString("dd/MM/yyyy"));
			}
			if (int.Parse(ddlSeverityID.SelectedValue) >= 0)
			{
				filters.Severity = ddlSeverityID.SelectedValue.ToNullableEnumByValue<Infrastructure.LogSeverity>();
				wcFiltersView.Add("Severity", ddlSeverityID.SelectedItem.Text);
			}
			if (ddlTag.SelectedValue != "-1")
			{
				filters.Tag = ddlTag.SelectedValue;
				wcFiltersView.Add("Tag", ddlTag.SelectedItem.Text);
			}
            if(!string.IsNullOrEmpty(txtTextSearch.Text)){
                filters.Text = txtTextSearch.Text;
                wcFiltersView.Add("Text", txtTextSearch.Text);
            }
			return filters;
		}

		protected string FormatInfo(Infrastructure.Logger item)
		{
			if (item.LongMessage == null) 
				return string.Empty;
			else
				return "<strong>More info</strong></br/>" + item.LongMessage;
		}
		
	}
}
