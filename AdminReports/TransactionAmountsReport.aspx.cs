﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Bll;
using Netpay.Bll.Reports;
using Netpay.CommonTypes;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Domains;
using Netpay.Web;
using Netpay.AdminReports.Code;
using System.Data;
using System.Web.UI.WebControls;
using Netpay.Bll.Reports.VO;

namespace Netpay.AdminReports
{
	public partial class TransactionAmountsReport : NetpayPage
	{
		protected string minDate = "";
		protected string maxDate = "";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				// set date picker
				wcDateRangePicker.MinDate = WebUtils.DomainCache.DenormalizedMinDate;
				wcDateRangePicker.MaxDate = WebUtils.DomainCache.DenormalizedMaxDate;
				wcDateRangePicker.FromDate = WebUtils.DomainCache.DenormalizedMaxDate.AddDays(-7);
				wcDateRangePicker.ToDate = WebUtils.DomainCache.DenormalizedMaxDate;

				// set defaults
				cbGroupByCurrency.Checked = true;

				// amount types
				ddsAmountTypes.AvailableItems = WebUtils.CurrentDomain.Cache.TransactionAmountTypes.Select(et => new ListItem(et.Value.Text, et.Value.ID.ToString())).ToList();
			}
		}

		protected void GetReport()
		{
			List<TransactionAmountType> amounts = ddsAmountTypes.SelectedItems.Select(item => (TransactionAmountType)int.Parse(item.Value)).ToList();
			SearchFilters filters = GetFilters();
			List<VOPropertyInfo> aggregates = GetAggregates();
			List<VOPropertyInfo> groups = GetGroups();
			DataTable results = ImmediateReports.GetTransactionAmountsReport(amounts, filters, aggregates, groups);
			repeaterResults.DataSource = results;
			repeaterResults.DataBind();
			PopulateAmountsHeaders(results.Columns);

			phResults.Visible = results.Rows.Count > 0;
		}

		protected void GetReport(object sender, EventArgs e)
		{
			GetReport();
		}

		/// <summary>
		/// Gets the grouping list
		/// </summary>
		/// <returns></returns>
		protected List<VOPropertyInfo> GetGroups()
		{
			List<VOPropertyInfo> infos = new List<VOPropertyInfo>();

			if (cbGroupByBank.Checked)
			{
				infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionAmountsReportVO), Property = "AcquiringBankID" });
				infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionAmountsReportVO), Property = "AcquiringBankName" });
				thBank.Visible = true;
			}
			else
			{
				repeaterResults.AddHiddenColumnID("tdBankName");
				repeaterSummary.AddHiddenColumnID("tdBankName");
				thBank.Visible = false;
			}

			if (cbGroupByTerminal.Checked)
			{
				infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionAmountsReportVO), Property = "TerminalID" });
				infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionAmountsReportVO), Property = "TerminalName" });
				infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionAmountsReportVO), Property = "TerminalNumber" });
				thTerminal.Visible = true;
			}
			else
			{
				repeaterResults.AddHiddenColumnID("tdTerminalName");
				repeaterSummary.AddHiddenColumnID("tdTerminalName");
				thTerminal.Visible = false;
			}

			if (cbGroupByMerchant.Checked)
			{
				infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionAmountsReportVO), Property = "MerchantID" });
				infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionAmountsReportVO), Property = "MerchantNumber" });
				infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionAmountsReportVO), Property = "MerchantName" });
				thMerchant.Visible = true;
			}
			else
			{
				repeaterResults.AddHiddenColumnID("tdMerchantName");
				repeaterSummary.AddHiddenColumnID("tdMerchantName");
				thMerchant.Visible = false;
			}

			if (cbGroupByAmountType.Checked)
			{
				infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionAmountsReportVO), Property = "AmountTypeID" });
				infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionAmountsReportVO), Property = "AmountTypeName" });
				thAmountType.Visible = true;
			}
			else
			{
				repeaterResults.AddHiddenColumnID("tdAmountTypeName");
				repeaterSummary.AddHiddenColumnID("tdAmountTypeName");
				thAmountType.Visible = false;
			}

			if (cbGroupByCurrency.Checked)
			{
				infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionAmountsReportVO), Property = "TransactionCurrencyID" });
				infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionAmountsReportVO), Property = "TransactionCurrencyIsoCode" });
				thCurrency.Visible = true;
			}
			else
			{
				repeaterResults.AddHiddenColumnID("tdCurrencyCode");
				repeaterSummary.AddHiddenColumnID("tdCurrencyCode");
				thCurrency.Visible = false;
			}

			return infos;
		}

		/// <summary>
		/// Gets the aggregate list
		/// </summary>
		/// <returns></returns>
		protected List<VOPropertyInfo> GetAggregates()
		{
			List<VOPropertyInfo> infos = new List<VOPropertyInfo>();
			infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionAmountsReportVO), Property = "TotalCount" });
			infos.Add(new VOPropertyInfo() { VOType = typeof(TransactionAmountsReportVO), Property = "TotalAmount" });

			return infos;
		}

		/// <summary>
		/// Populates filters control and returns search filters 
		/// </summary>
		/// <param name="filters"></param>
		protected SearchFilters GetFilters()
		{
			SearchFilters filters = new SearchFilters();

			if (ddsAmountTypes.IsSelected) 
			{
				filters.transactionAmountTypeIDs = ddsAmountTypes.SelectedItems.Select(i => int.Parse(i.Value)).ToList();
				wcFiltersView.Add("Amounts", ddsAmountTypes.SelectedItems.Select(i => i.Text).Aggregate((current, next) => current + ", " + next));
			}

			if (ddCurrency.IsSelected)
			{
				filters.currencyID = byte.Parse(ddCurrency.SelectedValue);
				wcFiltersView.Add("Currency", ddCurrency.SelectedItem.Text);
			}

			if (wcMerchantAutoComplete.IsSelected)
			{
				filters.merchantIDs = wcMerchantAutoComplete.GetSelectedValues<int>();
				if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
					wcFiltersView.Add("Merchant", wcMerchantAutoComplete.SelectedValue);
			}

			if (ddlDebitCompany.IsSelected)
			{
				filters.debitCompanyID = int.Parse(ddlDebitCompany.SelectedValue);
				wcFiltersView.Add("Debit Company", ddlDebitCompany.SelectedItem.Text);
			}

			if (wcTerminalAutoComplete.IsSelected)
			{
				filters.terminalNumber = wcTerminalAutoComplete.SelectedValue;
				wcFiltersView.Add("Terminal Number", wcTerminalAutoComplete.SelectedValue);
			}

			if (wcDateRangePicker.IsFromDateSelected)
			{
				filters.dateFrom = wcDateRangePicker.FromDate.Value.MinTime();
				wcFiltersView.Add("From Date", wcDateRangePicker.FromDate.Value.ToString("dd/MM/yyyy"));
			}
			if (wcDateRangePicker.IsToDateSelected)
			{
				filters.dateTo = wcDateRangePicker.ToDate.Value.MaxTime();
				wcFiltersView.Add("To Date", wcDateRangePicker.ToDate.Value.ToString("dd/MM/yyyy"));
			}

			return filters;
		}

		/// <summary>
		/// Gets the formatted amount.
		/// If no currency is provided, the currency is taken from the currency dropdown.
		/// If currency is not selected, no currency is assumed.
		/// The amount if painted red if less than zero.
		/// </summary>
		/// <param name="currencyID"></param>
		/// <param name="amount"></param>
		/// <param name="format"></param>
		/// <returns></returns>
		protected string GetFormattedAmount(int? currencyID, decimal? amount)
		{
			if (currencyID == null)
				currencyID = (int)CommonTypes.Currency.USD;

			if (ddCurrency.IsSelected)
				currencyID = int.Parse(ddCurrency.SelectedValue);

			if (amount == null)
				return "<span>null</span>";
			else if (amount < 0)
				return string.Format("<span class=\"negativeNumber\">{0}</span>", amount.Value.ToAmountFormat(currencyID.Value));
			else
				return string.Format("<span>{0}</span>", amount.Value.ToAmountFormat(currencyID.Value));
		}

		protected void PopulateAmountsHeaders(DataColumnCollection columns)
		{
			StringBuilder sb = new StringBuilder();
			foreach (DataColumn currentColumn in columns)
			{
				if (!currentColumn.ColumnName.Contains("_amount"))
					continue;
				
				sb.Append("<th>");
				sb.Append(currentColumn.ColumnName.Replace("_amount", ""));
				sb.Append("</th>");
			}

			literalAmountsHeaders.Text = sb.ToString();
		}

		protected string GetAmountsData(DataRowView row) 
		{
			StringBuilder sb = new StringBuilder();
			foreach (DataColumn currentColumn in row.Row.Table.Columns) 
			{
				if (!currentColumn.ColumnName.Contains("_amount"))
					continue;
				
				sb.Append("<td class=\"numericCell\">");
				sb.Append(GetFormattedAmount(row.ToNullableInt32("TransactionCurrencyID"), row.ToNullableDecimal(currentColumn.ColumnName)));
				sb.Append("</td>");
			}

			return sb.ToString();
		}

		protected void CreateExcel(object sender, EventArgs e)
		{
			List<TransactionAmountType> amounts = ddsAmountTypes.SelectedItems.Select(item => (TransactionAmountType)int.Parse(item.Value)).ToList();
			DataTable results = ImmediateReports.GetTransactionAmountsReport(amounts, GetFilters(), GetAggregates(), GetGroups());

			/*
			List<ReportFieldInfo> fields = new List<ReportFieldInfo>();
			if (cbGroupByBank.Checked) fields.Add(new ReportFieldInfo("Bank", "AcquiringBankName"));
			if (cbGroupByTerminal.Checked) fields.Add(new ReportFieldInfo("Terminal", "TerminalName"));
			if (cbGroupByCurrency.Checked) fields.Add(new ReportFieldInfo("Currency", "TransactionCurrencyIsoCode"));
			if (cbGroupByMerchant.Checked)
			{
				fields.Add(new ReportFieldInfo("ID", "MerchantID"));
				fields.Add(new ReportFieldInfo("Merchant", "MerchantName"));
			}
			fields.Add(new ReportFieldInfo("Count", "TotalCount"));
			fields.Add(new ReportFieldInfo("Amount", "TotalAmount"));
			*/

			WriteExcel(ReportHelper.CreateExcel(results));
		}
	}
}