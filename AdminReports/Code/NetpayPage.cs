﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using System.Threading;
using Netpay.Bll;
using System.IO;
using Netpay.Web;
using Netpay.Infrastructure.Security;

namespace Netpay.AdminReports.Code
{

    public class SecurityModule : Infrastructure.Module 
    {
        public override string Name { get { return "Reports.Security"; } }
        public static SecurityModule Current { get { return Infrastructure.Module.Get("Reports.Security") as SecurityModule; } }
	    public override Decimal Version { get { return 1.0m; } }

	    public static Infrastructure.Security.SecuredObject GetSecuredObject(string name, bool addIfNotExist = false)
        {
		    //name = name.ToLower()
		    var obj = Infrastructure.Security.SecuredObject.Get(SecurityModule.Current, name);
		    if (obj == null && addIfNotExist) obj = Infrastructure.Security.SecuredObject.Create(SecurityModule.Current, name, PermissionGroup.Execute);
		    return obj;
	    }

	    public static Infrastructure.Security.PermissionValue  GetPermissionValue(Dictionary<int, Infrastructure.Security.PermissionValue?> values, string name)
        {
		    var sobj = GetSecuredObject(name);
		    if (sobj == null) return PermissionValue.None;
		    if (!values.ContainsKey(sobj.ID)) return PermissionValue.None;
            return values[sobj.ID].GetValueOrDefault(PermissionValue.None);
	    }
    }

	public class NetpayPage : BasePage
	{
		private static List<string> _freeAccessList = null;
		private bool? _isFreeAccess = null;
		private bool? _isManaged = null;
		private bool? _isAccessible = null;

		protected override void OnPreInit(EventArgs e)
		{
            if (!IsLoggedin && !Request.Url.AbsolutePath.ToLower().EndsWith("/login.aspx")) //Login();
                Response.Redirect("Login.aspx", true);
			
			if (!IsAuthorized)
				Response.Redirect("NotAuthorized.aspx", true);
			
			base.OnPreInit(e);
		}

		public static List<string> FreeAccessList
		{
			get 
			{ 
				if (_freeAccessList == null)
				{
					_freeAccessList = new List<string>();
					_freeAccessList.Add("NotAuthorized.aspx".ToLower());
					_freeAccessList.Add("Default.aspx".ToLower());
                    _freeAccessList.Add("Login.aspx".ToLower());
				}
				
				return NetpayPage._freeAccessList; 
			}
		}
		
		public bool IsAuthorized
		{
			get
			{
				if (IsFreeAccess)
					return true;

				if (!IsLoggedin)
					return false;

				if (!IsManaged)
					return true;

				return IsAccessible; 
			}
		}
		
		public bool IsFreeAccess
		{
			get
			{
				if (_isFreeAccess == null)
				{
					string page = Path.GetFileName(Request.Path).ToLower();
					_isFreeAccess = FreeAccessList.Contains(page);				
				}
				
				return _isFreeAccess.Value;
			}
		}

		public bool IsManaged
		{
			get
			{
                return false;
				//if (_isManaged == null)
				//{
				//	string page = Path.GetFileName(Request.Path).ToLower();
                //   _isManaged = SecuredObject.Get(SecurityModule.Current, page) != null; 
				//}
				//return _isManaged.Value;
			}
		}
		
		public bool IsAccessible
		{
			get
			{
				if (_isAccessible == null)
				{
					string page = Path.GetFileName(Request.Path).ToLower();
                    var obj = Infrastructure.Security.SecuredObject.Get(SecurityModule.Current, page);
            		_isAccessible = obj.HasPermission(PermissionValue.Execute);
					//_isAccessible = SecuredObject.Get("AdminReports", page).HasPermission(PermissionValue.Read); 
					//LoggedUser.AccessList.Where(po => po.Name.ToLower() == "admin_reports_" + page).SingleOrDefault() != null;
				}

				return _isAccessible.Value;
			}
		}		
		
		public static void DownloadFile(string path, string contentType)
		{
			HttpContext context = HttpContext.Current;
			context.Response.Clear();
			context.Response.ClearHeaders();
			context.Response.ContentType = contentType;
			context.Response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(path));
			context.Response.Cache.SetCacheability(HttpCacheability.Private);
			context.Response.TransmitFile(path);
			context.Response.End();
		}

		public static void DownloadExcel(byte[] data)
		{
			HttpContext context = HttpContext.Current;
			context.Response.Clear();
			context.Response.ClearHeaders();
			context.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
			context.Response.AddHeader("Content-Disposition", "attachment;filename=report.xlsx");
			context.Response.Cache.SetCacheability(HttpCacheability.Private);
			context.Response.BinaryWrite(data);
			context.Response.End();
		}

		public Infrastructure.Domain Domain { get { return Infrastructure.Domain.Current; } }
	}
}
