﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Netpay.CommonTypes;
using Netpay.Infrastructure;
using Netpay.Bll;
using Netpay.Bll.Reports;
using Netpay.Bll.Reports.VO;
using Netpay.Web;
using Netpay.AdminReports.Code;

namespace Netpay.AdminReports
{	
	public partial class FailStatReport : NetpayPage
	{
		private List<FailStatGroupVO> _dataSource = null;
		
		protected void SortChange(object sender, EventArgs e)
		{
			repeaterReport.DataSource = GetDataSource();
			repeaterReport.DataBind();
		}

		protected void Page_LoadComplete(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				// set date picker
				wcDateRangePicker.MinDate = IntegrationService.DenormalizedMinDate;
				wcDateRangePicker.MaxDate = IntegrationService.DenormalizedMaxDate;
				wcDateRangePicker.FromDate = IntegrationService.DenormalizedMaxDate.AddDays(-7);
				wcDateRangePicker.ToDate = IntegrationService.DenormalizedMaxDate;
			}
		}

		protected string GetTotalAmount()
		{
			int currencyID = (int)CommonTypes.Currency.USD;
			if (ddlCurrencyID.IsSelected)
				currencyID = int.Parse(ddlCurrencyID.SelectedValue);
			
			decimal totalAmount = GetDataSource().Where(fsg => fsg.Amount.HasValue).Sum(fsg => fsg.Amount.Value);
			return totalAmount.ToAmountFormat(currencyID);
		}

		protected string GetTotalCount()
		{
			decimal totalCount = GetDataSource().Where(fsg => fsg.GroupCount.HasValue).Sum(fsg => fsg.GroupCount.Value);
			return totalCount.ToString();
		}

		protected string GetAmount(FailStatGroupVO group)
		{
			if (group.Amount == null)
				return "&nbsp;";

			if (ddlCurrencyID.IsSelected)
				return group.Amount.Value.ToAmountFormat(int.Parse(ddlCurrencyID.SelectedValue));
			else
				return group.Amount.Value.ToAmountFormat((int)CommonTypes.Currency.USD);
		}

		protected List<FailStatGroupVO> GetDataSource()
		{
			if (_dataSource == null)
			{
				SearchFilters filters = PopulateFilter();
				_dataSource = Netpay.Bll.Reports.ImmediateReports.GetFailedTransactionsReport(filters, ccSorting.Info);
				phResults.Visible = _dataSource.Count > 0;			
			}
			
			return _dataSource; 
		}

		protected void FilterReport(object sender, EventArgs e)
		{
			repeaterReport.DataSource = GetDataSource();
			repeaterReport.DataBind();
		}

		/// <summary>
		/// Populates filters control and returns search filters 
		/// </summary>
		/// <param name="filters"></param>
		protected SearchFilters PopulateFilter()
		{					
			SearchFilters filters = new SearchFilters();
			
			if (ddRejectionSource.IsSelected)
			{
				filters.rejectingSource = (RejectingSource)Enum.Parse(typeof(RejectingSource), ddRejectionSource.SelectedValue);
			}

			if (wcMerchantAutoComplete.IsSelected)
			{
				filters.merchantIDs = wcMerchantAutoComplete.GetSelectedValues<int>();
				if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
					wcFiltersView.Add("Merchant", wcMerchantAutoComplete.SelectedValue);
			}

			if (ddlPaymentMethodID.IsSelected)
			{
				filters.paymentMethodID = int.Parse(ddlPaymentMethodID.SelectedValue);				
				wcFiltersView.Add("Payment method", ddlPaymentMethodID.SelectedItem.Text);
			}

			if (ddlDebitCompany.IsSelected)
			{
				filters.debitCompanyID = int.Parse(ddlDebitCompany.SelectedValue);				
				wcFiltersView.Add("Debit company", ddlDebitCompany.SelectedItem.Text);
			}	
					
			if (wcTerminalAutoComplete.IsSelected)
			{
				filters.terminalNumber = wcTerminalAutoComplete.SelectedValue;
				wcFiltersView.Add("Terminal number", wcTerminalAutoComplete.SelectedValue);
			}

			if (ddlCurrencyID.IsSelected)
			{
				filters.currencyID = byte.Parse(ddlCurrencyID.SelectedValue);				
				wcFiltersView.Add("Currency", ddlCurrencyID.SelectedItem.Text);
			}

			if (wcDateRangePicker.IsFromDateSelected)
			{
				filters.dateFrom = wcDateRangePicker.FromDate.Value.MinTime();
				wcFiltersView.Add("From Date", wcDateRangePicker.FromDate.Value.ToString("dd/MM/yyyy"));
			}

			if (wcDateRangePicker.IsToDateSelected)
			{
				filters.dateTo = wcDateRangePicker.ToDate.Value.MaxTime();
				wcFiltersView.Add("To Date", wcDateRangePicker.ToDate.Value.ToString("dd/MM/yyyy"));
			}
			else
			{
				filters.failStatExcludeMultiples = false;
			}

			string transTypeText = "";
			if (chkTransTypeDebit.Checked)
			{
				filters.transTypeDebit = true;
				transTypeText += "debit";
			}
			else
			{
				filters.transTypeDebit = false;
			}
			if (chkTransTypePreAuth.Checked)
			{
				filters.transTypePreAuth = true;
				if (transTypeText != "") transTypeText += ",";
				transTypeText += " preauth";
			}
			else
			{
				filters.transTypePreAuth = false;
			}
			if (chkTransTypeCapture.Checked)
			{
				filters.transTypeCapture = true;
				if (transTypeText != "") transTypeText += ",";
				transTypeText += " capture";
			}
			else
			{
				filters.transTypeCapture = false;
			}
			if (transTypeText != "") 
				wcFiltersView.Add("TransType",transTypeText);
			
			return filters;
		}
			
		protected string FormatLink(FailStatGroupVO item)
		{
			string linkHtml = "<a href=\"FailStatReportReply.aspx?reply=" + item.RejectCode;
			if (wcDateRangePicker.IsFromDateSelected)
			{
				linkHtml = linkHtml + "&dateFrom=" + System.Web.HttpContext.Current.Server.HtmlEncode(wcDateRangePicker.FromDate.ToString());
			}
			if (wcDateRangePicker.IsToDateSelected)
			{
				linkHtml = linkHtml + "&dateTo=" + System.Web.HttpContext.Current.Server.HtmlEncode(wcDateRangePicker.ToDate.ToString());
			}			
			if (wcMerchantAutoComplete.IsSelected)
			{
				linkHtml = linkHtml + "&companyId=" + wcMerchantAutoComplete.SelectedValue;
			}			 
			if (ddlPaymentMethodID.IsSelected)
			{
				linkHtml = linkHtml + "&paymentMethodId=" + int.Parse(ddlPaymentMethodID.SelectedValue);
			}
			if (ddlDebitCompany.IsSelected)
			{
				linkHtml = linkHtml + "&debitCompanyId=" + int.Parse(ddlDebitCompany.SelectedValue);
			}
			if (wcTerminalAutoComplete.IsSelected)
			{
				linkHtml = linkHtml + "&terminalNumber=" + wcTerminalAutoComplete.SelectedValue;
			}
			if (ddlCurrencyID.IsSelected)
			{
				linkHtml = linkHtml + "&currencyId=" + int.Parse(ddlCurrencyID.SelectedValue);
			}
			else
			{
				linkHtml = linkHtml + "&excludeMultiple=0";
			}
			if (chkTransTypeDebit.Checked)
			{
				linkHtml = linkHtml + "&transTypeDebit=1";
			}
			else
			{
				linkHtml = linkHtml + "&transTypeDebit=0";
			}
			if (chkTransTypePreAuth.Checked)
			{
				linkHtml = linkHtml + "&transTypePreauth=1";
			}
			else
			{
				linkHtml = linkHtml + "&transTypePreauth=0";
			}
			if (chkTransTypeCapture.Checked)
			{
				linkHtml = linkHtml + "&transTypeCapture=1";
			}
			else
			{
				linkHtml = linkHtml + "&transTypeCapture=0";
			}
			linkHtml = linkHtml + "\">Details</a>";
			return linkHtml;
		}

		protected void CreateExcel(object sender, EventArgs e)
		{
			List<ReportGeneratorBase.FieldInfo> fields = new List<ReportGeneratorBase.FieldInfo>();
			fields.Add(new ReportGeneratorBase.FieldInfo("Count", "GroupCount"));
			fields.Add(new ReportGeneratorBase.FieldInfo("Amount", (object item) => ((FailStatGroupVO)item).Amount.GetValueOrDefault().ToAmountFormat()));
			fields.Add(new ReportGeneratorBase.FieldInfo("Code", "RejectCode"));
			fields.Add(new ReportGeneratorBase.FieldInfo("Description", "RejectText"));
			fields.Add(new ReportGeneratorBase.FieldInfo("Source", (object item) => ((FailStatGroupVO)item).GetRejectedSourceText()));

            string header = (wcFiltersView.Visible ? wcFiltersView.RenderText() : null);
            var generator = new ReportGeneratorExcel();
            var result = generator.Generate(fields, GetDataSource(), null, header);
            DownloadExcel(result);
        }
	}
}
