﻿<%@ Page Title="Admin Reports - Bank Transfers" Language="C#" MasterPageFile="~/Reports.Master" AutoEventWireup="True" CodeBehind="BankTransfers.aspx.cs" Inherits="Netpay.AdminReports.BankTransfers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<script type="text/javascript">
		function validate()
		{
			return netpay.webControls.DateRangePicker.validate();
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
	<table style="width:100%;" border="0">
		<tr>
			<td class="pageMainHeading" colspan="2">Bank Transfers</td>
			<td style="text-align:right;">
				<netpay:ReportUpdateInfo ID="wcReportUpdateInfo" runat="server" Report="AdminRollingReserves" /> &nbsp;|&nbsp;
				<asp:Image ID="Image1" ImageUrl="Images/ImgFileExt/xls.gif" runat="server" />
				<asp:LinkButton ID="lnkBtnExcel" Text="EXPORT TO XLS" OnClick="CreateExcel" CssClass="ExportExcelTxt" runat="server" />
			</td>
		</tr>
	</table>
	
	<!-- filters -->
	<table style="width:100%;" class="formFilter" border="0">
		<tr>
			<td>
				<table>
					<tr>
						<td style="font-weight:bold;">Filter</td>
					</tr>
					<tr>
						<td valign="top">
							<netpay:DateRangePicker ID="wcDateRangePicker" runat="server" />
						</td>
						<td valign="top">
							<table>
								<tr>
									<td valign="top" nowrap="nowrap">
										Transfer ID <br />
										<asp:TextBox ID="txtTransferID" runat="server"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td valign="top" nowrap="nowrap">
										Bank <br />
										<asp:DropDownList ID="ddlDebitCompanyID" runat="server" Width="200px" EnableViewState="true">
											<asp:ListItem Selected="True" Text="B&S" Value="18"></asp:ListItem>
										</asp:DropDownList>
									</td>
								</tr>
							</table>
						</td>
						<td valign="top">
							<table>
								<tr>
									<td valign="top" nowrap="nowrap">
										Merchant <br />
										<netpay:AutoComplete ID="wcMerchantAutoComplete" IsMultiselect="false" Function="GetMerchantNameAutoComplete" runat="server" />
									</td>
								</tr>
								<tr>
									<td valign="top" nowrap="nowrap">
										Terminal <br />
										<netpay:AutoComplete ID="wcTerminalAutoComplete" Function="GetDebitTerminalAutoComplete" runat="server" />
									</td>
								</tr>
							</table>
						</td>
						<td valign="top">
							<table>
								<tr>
									<td valign="top" nowrap="nowrap">
										<br />
										<asp:RadioButton ID="rbTransferred" GroupName="rbgTransferMode" Text="Transferred" Checked="true" runat="server" />
									</td>
								</tr>
								<tr>
									<td valign="top" nowrap="nowrap">
										<asp:RadioButton ID="rbNotTransferred" GroupName="rbgTransferMode" Text="Not Transferred" runat="server" />
									</td>
								</tr>
								<tr>
									<td valign="top" nowrap="nowrap">
										<asp:RadioButton ID="rbBoth" GroupName="rbgTransferMode" Text="Both" runat="server" />
									</td>
								</tr>
							</table>
						</td>
						<td style="padding-left:40px;">
							<br />
							<input type="reset" value="RESET" style="width:80px;" /><br /><br />
							<input type="submit" onclick="return validate();" value="GO" style="width:80px;" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
	<br />
	<br />
	
	<!-- results -->
	<netpay:SearchFiltersView ID="wcFiltersView" runat="server" />	
	<asp:PlaceHolder ID="phResults" Visible="false" runat="server">
		<table visible="false" style="width:100%;" class="formNormal">		
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th>Transaction</th>
					<th>Date</th>
					<th>PaymentMethod</th>
					<th>Amount</th>	
					<th>Transfer ID</th>			
				</tr>
			</thead>
			<tbody>
				<asp:Repeater ID="repeaterReport" runat="server">
					<ItemTemplate>
						<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='#ffffff';">
							<td class="reportCell">
								<netpay:DenormalizedTransactionRowView Transaction="<%# ((DenormalizedTransactionVO)Container.DataItem) %>" ShowStatus="false" ShowExpandButton="true" ShowLegend="false" runat="server" />
							</td>
						</tr>
					</ItemTemplate>
				</asp:Repeater>
			</tbody>
		</table>		
	</asp:PlaceHolder>
	<table class="pager" align="center">
		<tr>
			<td><netpay:Pager ID="pager" runat="server" /></td>
		</tr>
	</table>
</asp:Content>
