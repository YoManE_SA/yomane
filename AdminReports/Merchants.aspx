﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports.Master" AutoEventWireup="true" CodeBehind="Merchants.aspx.cs" Inherits="Netpay.AdminReports.Merchants" %>
<%@ Register Src="~/Controls/MerchantDetails.ascx" TagPrefix="Admin" TagName="MerchantDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    Merchant
    <br />
    <netpay:AutoComplete ID="wcMerchantAutoComplete" IsMultiselect="false" Function="GetMerchantNameAutoComplete" runat="server" />
    <asp:Button ID="btnGo" Text="GO" Width="80px" OnClick="GetMerchant" UseSubmitBehavior="true" runat="server" />

    <Admin:MerchantDetails ID="merchantDetails" Visible="false" runat="server" />
</asp:Content>
