﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.AdminReports.Code;
using Netpay.Bll;
using System.IO;
using Netpay.Infrastructure.Security;
using Netpay.Web;
using Netpay.Infrastructure;

namespace Netpay.AdminReports
{
    public partial class Reports : System.Web.UI.MasterPage
	{
		protected DateTime _processStart = DateTime.Now;
		
		protected void Page_Load(object sender, EventArgs e)
		{
            if (NPPage != null && NPPage.IsLoggedin)
			{
				literalUserName.Text = NPPage.LoggedUser.UserName;

				string page = Path.GetFileName(Request.Path);
                if (ObjectContext.Current.IsUserOfType(UserRole.Admin, Infrastructure.Security.SecuredObject.Get(Code.SecurityModule.Current, "ManagePermission"), PermissionValue.Execute, false))
				{
					btnAddManagedPage.Visible = true;
					btnAddManagedPage.OnClientClick = "return confirm('" + (NPPage.IsManaged ? "Sure to remove security to this page ?" : "Sure to add security to this page ?") + "');";
					btnAddManagedPage.Text = NPPage.IsManaged ? "-" : "+";
				}
			}
		}

		protected void Page_PreRender(object sender, EventArgs e)
		{
			literalProcessTimer.Text = string.Format("Request processed in {0} seconds.", (DateTime.Now - _processStart).TotalSeconds.ToString("0.00"));
			Page.Header.Controls.Add(new System.Web.UI.LiteralControl(string.Format("<link type=\"text/css\" href=\"Templates/{0}/styles.css\" rel=\"Stylesheet\" />", WebUtils.CurrentDomain.ThemeFolder)));
			Page.Header.Controls.Add(new System.Web.UI.LiteralControl(string.Format("<link type=\"text/css\" href=\"Templates/{0}/ddsmoothmenu.css\" rel=\"Stylesheet\" />", WebUtils.CurrentDomain.ThemeFolder)));
            Page.Header.Controls.Add(new System.Web.UI.LiteralControl(string.Format("<link href=\"Templates/{0}/favicon.ico\" rel=\"icon\" type=\"image/x-icon\" />", WebUtils.CurrentDomain.ThemeFolder)));
		}

		protected void ToggleManagedPage(object sender, EventArgs e)
		{
			string page = Path.GetFileName(Request.Path);
			if (NPPage.IsManaged)
			{
                SecuredObject.Get(Code.SecurityModule.Current, page).Delete();
				btnAddManagedPage.Text = "+";
			}
			else
			{
				var obj = new SecuredObject(0, page, PermissionGroup.ReadEditDelete) { Description = NPPage.Title };
				obj.Save();
				btnAddManagedPage.Text = "-";
			}
		}

		protected void Logout(object sender, EventArgs e)
		{
			NPPage.Logout();
			Response.Redirect("Default.aspx", true);
		}
	
		protected NetpayPage NPPage
		{
			get
			{
				return (NetpayPage)Page;
			}
		}
    }
}
