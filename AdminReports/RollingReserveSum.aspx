﻿<%@ Page Title="Admin Reports - Rolling Reserve Sum" Language="C#" MasterPageFile="~/Reports.Master" AutoEventWireup="true" CodeBehind="RollingReserveSum.aspx.cs" Inherits="Netpay.AdminReports.RollingReserveSum" %>

<asp:Content ID="content2" ContentPlaceHolderID="head" runat="server">
<style>
	div.innerContent { width: 75%; }
</style>
</asp:Content>

<asp:Content ID="content1" ContentPlaceHolderID="body" runat="server">
	<table style="width:100%;">		
		<tr>
			<td class="pageMainHeading">Rolling Reserves</td>
			<td style="text-align:right;">
				<netpay:ReportUpdateInfo ID="wcReportUpdateInfo" runat="server" Report="AdminRollingReserves" /> &nbsp;|&nbsp;
				<asp:Image ID="Image1" ImageUrl="Images/ImgFileExt/xls.gif" runat="server" />
				<asp:LinkButton ID="lnkBtnExcel" Text="EXPORT TO XLS" OnClick="CreateExcel" CssClass="ExportExcelTxt" runat="server" />
			</td>
		</tr>
		<tr><td colspan="2" class="pageSubHeading">Merchant Summary</td></tr>	
	</table>
	<br />
	<table style="width:100%;" class="formFilter">
		<tr>
			<td>
				<table>
					<thead>
						<tr>
							<th>Merchant</th>
							<th>Merchant group</th>
							<th>Currency</th>
							<th>Totals</th>
							<th></th>
							<th></th>
						</tr>
					</thead>		
					<tbody>
						<tr>	
							<td> <netpay:MerchantsDropDown ID="ddlMerchant" runat="server"></netpay:MerchantsDropDown></td>
							<td><netpay:MerchantGroupsDropDown ID="ddMerchantGroup" EnableBlankSelection="true" runat="server"></netpay:MerchantGroupsDropDown></td>
							<td><netpay:CurrencyDropDown ID="ddlCurrency" EnableBlankSelection="true" runat="server"></netpay:CurrencyDropDown></td>
							<td>
								<asp:DropDownList ID="ddlTransType" runat="server" EnableViewState = "true" />
								<asp:DropDownList ID="ddlCondition" runat="server" EnableViewState = "true" />
								<asp:TextBox ID="txAmount" runat="server" EnableViewState="true" Width="100" />
							</td>
							<td style="padding-left:40px;"><input type="button" value="RESET" onclick="Netpay.AdminReports.Utils.ClearForm(this.form)" /></td>
							<td><asp:Button ID="btnFilter" runat="server" Text=" FILTER " OnClick="FilterReport" /></td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</table>
	<br />
	<netpay:SearchFiltersView ID="wcFiltersView" runat="server" />
	<asp:PlaceHolder ID="phResults" Visible="false" runat="server">
		<table style="width:100%;" class="formNormal">
			<netpay:SortableColumns ID="ccSorting" VOType="RollingReserveReportVO" runat="server">
				<netpay:SortableColumn ColumnTitle="Merchant" ColumnName="CompanyName" runat="server" CssStyle="text-align:left" />
				<netpay:SortableColumn ColumnTitle="State" ColumnName="RollingReserveState" Enabled="false" runat="server" CssStyle="text-align:left" />
				<netpay:SortableColumn ColumnTitle="Reserved" ColumnName="Reserved" runat="server" CssStyle="text-align:right" />
				<netpay:SortableColumn ColumnTitle="Released" ColumnName="Released" runat="server" CssStyle="text-align:right" />
				<netpay:SortableColumn ColumnTitle="Balance" ColumnName="Total" Enabled="true" runat="server" CssStyle="text-align:right" />
				<netpay:SortableColumn ColumnTitle="Future Takes" ColumnName="FutureTakes" Enabled="false" runat="server" CssStyle="text-align:right" />
				<netpay:SortableColumn ColumnTitle="Details" Enabled="false" runat="server" />			
			</netpay:SortableColumns>
			<tbody>
				<asp:Repeater ID="repeaterReport" runat="server">
					<ItemTemplate>
						<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
							<td class="reportCell">
								<%# ((RollingReserveReportVO)Container.DataItem).CompanyID + " - " + ((RollingReserveReportVO)Container.DataItem).CompanyName %>
							</td>
							<td class="reportCell">
								<%# GlobalData.GetValue(GlobalDataGroup.RollingReserveState, Netpay.CommonTypes.Language.English, ((RollingReserveReportVO)Container.DataItem).RollingReserveState.Value)%>
							</td>
							<td class="reportCell numericCell" style="text-align:right;">							
								<asp:Label ID="lblReserved" runat="server" Text='<%# FormatReserved((RollingReserveReportVO)Container.DataItem)%>' />
							</td>
							<td class="reportCell numericCell" style="text-align:right;">							 
								 <asp:Label ID="lblReleased" runat="server" Text='<%# FormatReleased((RollingReserveReportVO)Container.DataItem)%>' />
							</td>
							<td class="reportCell numericCell" style="text-align:right;">
								<asp:Label runat="server" ForeColor='<%# ((RollingReserveReportVO)Container.DataItem).Total<0 ? System.Drawing.Color.Red : System.Drawing.Color.Black %>' Text='<%# FormatTotal((RollingReserveReportVO)Container.DataItem)%>' />							
							</td>
							<td class="reportCell numericCell" style="text-align:right;">							 
								 <asp:Label ID="Label1" runat="server" Text='<%#  GetFutureRollingReserve((RollingReserveReportVO)Container.DataItem).ToString("#,0.00") %>' />
							</td>
							<td class="reportCell" style="text-align:center;">
								<asp:Literal runat="server" Text='<%# "<a href=\"RollingReserveDetails.aspx?CompanyID=" + ((RollingReserveReportVO)Container.DataItem).CompanyID + "&CurrencyID=" + ((RollingReserveReportVO)Container.DataItem).CurrencyID + "\">Details</a>" %>' />
							</td>
						</tr>
					</ItemTemplate>
				</asp:Repeater>
			</tbody>
		</table>
		<div class="title">Totals</div>
		<table style="width:100%;" class="formSummary">
			<tr>
				<th style="text-align:left;">Currency</th>
				<th>Total Reserved</th>
				<th>Total Released</th>
				<th>Total Balance</th>			
			</tr>
			<asp:Repeater ID="repeaterTotals" runat="server">
				<ItemTemplate>
					<tr>
						<td style="text-align:left;"><%# ((Total)Container.DataItem).Currency %></td>
						<td><%# ((Total)Container.DataItem).Currency + " " + ((Total)Container.DataItem).ReservedTotal.ToString("#,##0.00") %></td>
						<td><%# ((Total)Container.DataItem).Currency + " " + ((Total)Container.DataItem).ReleasedTotal.ToString("#,##0.00") %></td>
						<td><%# ((Total)Container.DataItem).Currency + " " + ((Total)Container.DataItem).NPTotal.ToString("#,##0.00") %></td>					
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>	
	</asp:PlaceHolder>
</asp:Content>
