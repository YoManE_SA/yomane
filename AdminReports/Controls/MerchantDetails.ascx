﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MerchantDetails.ascx.cs" Inherits="Netpay.AdminReports.Controls.MerchantDetails" %>

<%= Merchant.Name %>
<br />
<br />
<br />
<br />

<table cellspacing="2" cellpadding="0" border="0" width="95%" align="center">
    <tbody>
        <tr>
            <td height="10">
            </td>
        </tr>
        <tr>
            <td width="25%" valign="top">
                <table cellspacing="0" cellpadding="1" border="0">
                    <tbody>
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="4" align="left" style="border: 1px solid silver;">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <img border="0" alt="" src="/NPCommon/images/alert_medium.png"><br/>
                                            </td>
                                            <td>
                                                <span style="color: #bc3a4f;" class="txt13">208 Test transactions made</span><br/>
                                                <a class="largeB" href="Trans_admin_regularData.aspx?onlyTestTrans=1&amp;ShowCompanyID=35">
                                                    View &amp; Delete</a> &nbsp;|&nbsp; <a class="largeB" href="?Action=DelTestOnly&amp;companyID=35">
                                                        Delete All</a><br/>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br/>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt11b">
                                <span style="background-color: #66cc66;">&nbsp;&nbsp;&nbsp;</span><%= Merchant.Status %><br/>
                            </td>
                        </tr>
                        <tr>
                            <td height="6">
                            </td>
                        </tr>
                        <tr>
                            <td class="txt11">
                                <span class="txt14"><%= MerchantExtended.ParentCompanyName %><span><br/>
                                    <span style="color: #006699;">Parent company:</span> <%= MerchantExtended.ParentCompanyName %><br/>
                                    <span style="color: #006699;">Merchant group:</span> <%= MerchantExtended.MerchantGroupName %><br/>
                                    <span style="color: #006699;">Merchant number:</span> <%= Merchant.Number %><br/>
                                    <span style="color: #006699;">Industry:</span> <%= MerchantExtended.IndustryText %><br/>
                                    <span style="color: #006699;">Account manager:</span> <%= MerchantExtended.AccountManager %><br/>
                                    <span style="color: #006699;">Signup:</span> <%= MerchantExtended.MerchantOpeningDate %><br/>
                                    <span style="color: #006699;">Website:</span> <a class="go" target="_blank" href="<%= MerchantExtended.MerchantUrl %>"><%= MerchantExtended.MerchantUrl %></a><br/>
                                </span></span>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br/>
                <br/>
                <table cellspacing="0" cellpadding="1" border="0" class="formThin">
                    <tbody>
                        <tr>
                            <td colspan="3" class="MerchantSubHead">
                                PAYOUTS<br/>
                                <br/>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" colspan="2">
                                <span style="color: #006699;">Initial holding margin:</span> 14 Days<br/>
                                <span style="color: #006699;">Holding margin:</span> 7 Days<br/>
                                <span style="color: #006699;">Transactions Period:</span><br/>
                                Between 01 and 10 Pay on 21<br/>
                                Between 11 and 20 Pay on 01<br/>
                                Between 21 and 31 Pay on 11<br/>
                                <br/>
                                <div style="width: 250px; white-space: normal;">
                                    Currencies hidden from payout list: ILS, USD, EUR, GBP, AUD, CAD, JPY, NOK, PLN,
                                    MXN, ZAR, TRY, CHF, INR, DKK, SEK.
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br/>
                <br/>
                <table cellspacing="0" cellpadding="1" border="0" class="formThin">
                    <tbody>
                        <tr>
                            <td colspan="3" class="MerchantSubHead">
                                LINKS<br/>
                                <br/>
                            </td>
                        </tr>
                        <tr>
                            <td class="txt10">

                                <asp:LinkButton runat="server" OnCommand="RedirectLogin" CommandArgument="merchant">MERCHANT CONTROL PANEL</asp:LinkButton>
                                <br />
                                <asp:LinkButton runat="server" OnCommand="RedirectLogin" CommandArgument="merchantTest">(TEST)</asp:LinkButton>  
                                <br />
                                <asp:LinkButton runat="server" OnCommand="RedirectLogin" CommandArgument="devCenter">MERCHANT DEVELOPER CENTER</asp:LinkButton>
                                <br />
                                <asp:LinkButton runat="server" OnCommand="RedirectLogin" CommandArgument="devCenterTest">(TEST)</asp:LinkButton>



                                <li type="circle" style="padding-bottom: 2px;"><a onclick="top.fraBrowser.frmBody.location.href='merchant_transPassFilter.asp?companyID=35&amp;CompanyStatus=30&amp;numTransToPay=0&amp;ShowName=N-etpay'; parent.fraButton.FrmResize(); return false;"
                                    href="#" class="go">Unsettled Balance</a></li><br/>
                                <li type="circle" style="padding-bottom: 2px;"><a onclick="top.fraBrowser.frmBody.location.href='Merchant_BalanceData.asp?ShowCompanyID=35&amp;isAllCompanys=0&amp;PageSize=25'; return false;"
                                    href="#" class="go">Account Balance</a></li><br/>
                                &nbsp;&nbsp;&nbsp;&nbsp;<a onclick="top.fraBrowser.frmBody.location.href='Merchant_BalanceData.asp?ShowCompanyID=35&amp;isAllCompanys=0&amp;PageSize=25&amp;Currency=0';return false;"
                                    href="#" class="go"><span style="white-space: nowrap;">ILS&nbsp;19,831.59</span></a>,
                                <a onclick="top.fraBrowser.frmBody.location.href='Merchant_BalanceData.asp?ShowCompanyID=35&amp;isAllCompanys=0&amp;PageSize=25&amp;Currency=1';return false;"
                                    href="#" class="go"><span style="white-space: nowrap;">USD&nbsp;1,037.05</span></a>,
                                <a onclick="top.fraBrowser.frmBody.location.href='Merchant_BalanceData.asp?ShowCompanyID=35&amp;isAllCompanys=0&amp;PageSize=25&amp;Currency=2';return false;"
                                    href="#" class="go"><span style="white-space: nowrap;">EUR&nbsp;2.00</span></a><li
                                        type="circle" style="padding-bottom: 2px;"><a href="javascript:top.fraBrowser.frmBody.location.href='Merchant_transArchiveFilter.asp?companyID=35&amp;isClickCount=true&amp;numTransArchive=192&amp;ShowName='; undefined;"
                                            class="go">Archive</a> (192) </li>
                                <br/>
                                <li type="circle" style="padding-bottom: 2px;"><a href="javascript:top.fraBrowser.frmBody.location.href='Merchant_transSettledListData.asp?Source=filter&amp;ShowCompanyID=35'; undefined;"
                                    class="go">Settlement Reports</a></li><br/>
                                &nbsp;&nbsp;&nbsp;&nbsp;Count: 143, Last: 24/04/2012<li type="circle" style="padding-bottom: 2px;">
                                    <a class="go" target="fraBrowser" href="charge_browser.asp?companyID=35">Virtual terminal</a></li><br/>
                                <li type="circle" style="padding-bottom: 2px;"><a class="go" href="javascript:top.fraBrowser.frmBody.location.href='Report_Merchant.aspx?companyID=35'; undefined;">
                                    Monthly Report</a></li><br/>
                                <li type="circle" style="padding-bottom: 2px;"><a class="go" target="frmBody" href="MerchantChbHistory.aspx?ID=35">
                                    Chargeback History</a></li><br/>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td bgcolor="#484848" width="1">
            </td>
            <td width="35%" valign="top">
                <table width="96%" align="center" dir="ltr" class="formThin">
                    <tbody>
                        <tr>
                            <td colspan="3" class="MerchantSubHead">
                                ACTIVE TERMINALS<br/>
                                <br/>
                            </td>
                        </tr>
                        <tr>
                            <th style="font-size: 10px; font-weight: normal; color: #000000;">
                                Payment Method
                            </th>
                            <th style="font-size: 10px; font-weight: normal; color: #000000;">
                                Debit Company
                            </th>
                            <th style="font-size: 10px; font-weight: normal; color: #000000;">
                                Terminal Name
                            </th>
                        </tr>
                        <tr>
                            <td style="font-size: 10px;">
                                <img align="middle" src="/NPCommon/ImgPaymentMethod/25.gif">
                                Mastercard ($)<br/>
                            </td>
                            <td style="font-size: 10px;">
                                <img border="0" align="middle" src="/NPCommon/ImgDebitCompanys/1.gif">
                                &nbsp;Netpay Local<br/>
                            </td>
                            <td style="font-size: 10px;">
                                NetpayTest2<br/>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px;">
                                <img align="middle" src="/NPCommon/ImgPaymentMethod/25.gif">
                                Mastercard ($)<br/>
                            </td>
                            <td style="font-size: 10px;">
                                <br/>
                            </td>
                            <td style="font-size: 10px;">
                                <span title="">..</span><br/>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px;">
                                <img align="middle" src="/NPCommon/ImgPaymentMethod/22.gif">
                                Visa (₪)<br/>
                            </td>
                            <td style="font-size: 10px;">
                                <img border="0" align="middle" src="/NPCommon/ImgDebitCompanys/11.gif">
                                &nbsp;Shva<br/>
                            </td>
                            <td style="font-size: 10px;">
                                Dev Test 1<br/>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px;">
                                <img align="middle" src="/NPCommon/ImgPaymentMethod/20.gif">
                                CC Unknown (₪)<br/>
                            </td>
                            <td style="font-size: 10px;">
                                <img border="0" align="middle" src="/NPCommon/ImgDebitCompanys/1.gif">
                                &nbsp;Netpay Local<br/>
                            </td>
                            <td style="font-size: 10px;">
                                Testing DIRECT<br/>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px;">
                                <img align="middle" src="/NPCommon/ImgPaymentMethod/20.gif">
                                CC Unknown (₪)<br/>
                            </td>
                            <td style="font-size: 10px;">
                                <img border="0" align="middle" src="/NPCommon/ImgDebitCompanys/1.gif">
                                &nbsp;Netpay Local<br/>
                            </td>
                            <td style="font-size: 10px;">
                                <span title="TestManipulation">TestManipulatio..</span><br/>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px;">
                                <img align="middle" src="/NPCommon/ImgPaymentMethod/25.gif">
                                Mastercard (₪)<br/>
                            </td>
                            <td style="font-size: 10px;">
                                <img border="0" align="middle" src="/NPCommon/ImgDebitCompanys/1.gif">
                                &nbsp;Netpay Local<br/>
                            </td>
                            <td style="font-size: 10px;">
                                Testing DIRECT<br/>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px;">
                                <img align="middle" src="/NPCommon/ImgPaymentMethod/22.gif">
                                Visa ($)<br/>
                            </td>
                            <td style="font-size: 10px;">
                                <img border="0" align="middle" src="/NPCommon/ImgDebitCompanys/40.gif">
                                &nbsp;AllCharge<br/>
                            </td>
                            <td style="font-size: 10px;">
                                Test (GW)<br/>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px;">
                                <img align="middle" src="/NPCommon/ImgPaymentMethod/100.gif">
                                eCheck ($)<br/>
                            </td>
                            <td style="font-size: 10px;">
                                <img border="0" align="middle" src="/NPCommon/ImgDebitCompanys/1.gif">
                                &nbsp;Netpay Local<br/>
                            </td>
                            <td style="font-size: 10px;">
                                NetpayTest2<br/>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px;">
                                <img align="middle" src="/NPCommon/ImgPaymentMethod/149.gif">
                                Online Bank Transfer ($)<br/>
                            </td>
                            <td style="font-size: 10px;">
                                <img border="0" align="middle" src="/NPCommon/ImgDebitCompanys/1.gif">
                                &nbsp;Netpay Local<br/>
                            </td>
                            <td style="font-size: 10px;">
                                Test pending (GW)<br/>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px;">
                                <img align="middle" src="/NPCommon/ImgPaymentMethod/149.gif">
                                Online Bank Transfer (€)<br/>
                            </td>
                            <td style="font-size: 10px;">
                                <img border="0" align="middle" src="/NPCommon/ImgDebitCompanys/42.gif">
                                &nbsp;InPay<br/>
                            </td>
                            <td style="font-size: 10px;">
                                TestInPay<br/>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px;">
                                <img align="middle" src="/NPCommon/ImgPaymentMethod/22.gif">
                                Visa (SFr)<br/>
                            </td>
                            <td style="font-size: 10px;">
                                <img border="0" align="middle" src="/NPCommon/ImgDebitCompanys/1.gif">
                                &nbsp;Netpay Local<br/>
                            </td>
                            <td style="font-size: 10px;">
                                NetpayTest2<br/>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px;">
                                <img align="middle" src="/NPCommon/ImgPaymentMethod/101.gif">
                                Giropay (€)<br/>
                            </td>
                            <td style="font-size: 10px;">
                                <img border="0" align="middle" src="/NPCommon/ImgDebitCompanys/38.gif">
                                &nbsp;PPro<br/>
                            </td>
                            <td style="font-size: 10px;">
                                test<br/>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px;">
                                <img align="middle" src="/NPCommon/ImgPaymentMethod/121.gif">
                                Lastschrift (€)<br/>
                            </td>
                            <td style="font-size: 10px;">
                                <img border="0" align="middle" src="/NPCommon/ImgDebitCompanys/1.gif">
                                &nbsp;Netpay Local<br/>
                            </td>
                            <td style="font-size: 10px;">
                                Test pending (GW)<br/>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px;">
                                <img align="middle" src="/NPCommon/ImgPaymentMethod/144.gif">
                                TP OBT (€)<br/>
                            </td>
                            <td style="font-size: 10px;">
                                TrustPay<br/>
                            </td>
                            <td style="font-size: 10px;">
                                Test<br/>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px;">
                                <img align="middle" src="/NPCommon/ImgPaymentMethod/21.gif">
                                Isracard (₪)<br/>
                            </td>
                            <td style="font-size: 10px;">
                                <img border="0" align="middle" src="/NPCommon/ImgDebitCompanys/1.gif">
                                &nbsp;Netpay Local<br/>
                            </td>
                            <td style="font-size: 10px;">
                                NetpayTest2<br/>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px;">
                                <img align="middle" src="/NPCommon/ImgPaymentMethod/145.gif">
                                SEPA (€)<br/>
                            </td>
                            <td style="font-size: 10px;">
                                <img border="0" align="middle" src="/NPCommon/ImgDebitCompanys/37.gif">
                                &nbsp;Webbiling<br/>
                            </td>
                            <td style="font-size: 10px;">
                                Test<br/>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px;">
                                <img align="middle" src="/NPCommon/ImgPaymentMethod/22.gif">
                                Visa (¥)<br/>
                            </td>
                            <td style="font-size: 10px;">
                                <img border="0" align="middle" src="/NPCommon/ImgDebitCompanys/18.gif">
                                &nbsp;B+S Direct<br/>
                            </td>
                            <td style="font-size: 10px;">
                                <span title="247videos DIRECT">247videos DIREC..</span><br/>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px;">
                                <img align="middle" src="/NPCommon/ImgPaymentMethod/200.gif">
                                SMS IN (€)<br/>
                            </td>
                            <td style="font-size: 10px;">
                                <img border="0" align="middle" src="/NPCommon/ImgDebitCompanys/43.gif">
                                &nbsp;Atlas Interactive<br/>
                            </td>
                            <td style="font-size: 10px;">
                                Test Atlas (GW)<br/>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px;">
                                <img align="middle" src="/NPCommon/ImgPaymentMethod/201.gif">
                                SMS OUT (€)<br/>
                            </td>
                            <td style="font-size: 10px;">
                                <img border="0" align="middle" src="/NPCommon/ImgDebitCompanys/43.gif">
                                &nbsp;Atlas Interactive<br/>
                            </td>
                            <td style="font-size: 10px;">
                                Test Atlas (GW)<br/>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px;">
                                <img align="middle" src="/NPCommon/ImgPaymentMethod/146.gif">
                                China UnionPay ($)<br/>
                            </td>
                            <td style="font-size: 10px;">
                                <img border="0" align="middle" src="/NPCommon/ImgDebitCompanys/49.gif">
                                &nbsp;DeltaPay<br/>
                            </td>
                            <td style="font-size: 10px;">
                                Test<br/>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px;">
                                <img align="middle" src="/NPCommon/ImgPaymentMethod/146.gif">
                                China UnionPay (圓)<br/>
                            </td>
                            <td style="font-size: 10px;">
                                <img border="0" align="middle" src="/NPCommon/ImgDebitCompanys/49.gif">
                                &nbsp;DeltaPay<br/>
                            </td>
                            <td style="font-size: 10px;">
                                <span title="Test China U Pay">Test China U Pa..</span><br/>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 10px;">
                                <img align="middle" src="/NPCommon/ImgPaymentMethod/34.gif">
                                LiveCash ($)<br/>
                            </td>
                            <td style="font-size: 10px;">
                                LiveCash<br/>
                            </td>
                            <td style="font-size: 10px;">
                                Test<br/>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br/>
                <br/>
                <table width="96%" align="center" dir="ltr" class="formThin">
                    <tbody>
                        <tr>
                            <td colspan="3" class="MerchantSubHead">
                                PROCESSING METHODS<br/>
                                <br/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span style="width: 26px; border: 1px solid #66cc66; font-size: 8px; margin: 2px;
                                    padding: 1px; text-align: center; background-color: #ffffff;">YES</span> <span style="vertical-align: middle;">
                                        Virtual terminal - Credit card</span><br/>
                                <span style="width: 26px; border: 1px solid #66cc66; font-size: 8px; margin: 2px;
                                    padding: 1px; text-align: center; background-color: #ffffff;">YES</span> <span style="vertical-align: middle;">
                                        Virtual terminal - E-Check</span><br/>
                                <span style="width: 26px; border: 1px solid #66cc66; font-size: 8px; margin: 2px;
                                    padding: 1px; text-align: center; background-color: #ffffff;">YES</span> <span style="vertical-align: middle;">
                                        Silent post transactions - Credit card</span><br/>
                                <span style="width: 26px; border: 1px solid #66cc66; font-size: 8px; margin: 2px;
                                    padding: 1px; text-align: center; background-color: #ffffff;">YES</span> <span style="vertical-align: middle;">
                                        Silent post transactions - E-Check</span><br/>
                                <span style="width: 26px; border: 1px solid #66cc66; font-size: 8px; margin: 2px;
                                    padding: 1px; text-align: center; background-color: #ffffff;">YES</span> <span style="vertical-align: middle;">
                                        Public payment page</span><br/>
                                <span style="width: 26px; border: 1px solid #66cc66; font-size: 8px; margin: 2px;
                                    padding: 1px; text-align: center; background-color: #ffffff;">YES</span> <span style="vertical-align: middle;">
                                        Hosted payment page - Credit card</span><br/>
                                <span style="width: 26px; border: 1px solid #66cc66; font-size: 8px; margin: 2px;
                                    padding: 1px; text-align: center; background-color: #ffffff;">YES</span> <span style="vertical-align: middle;">
                                        Hosted payment page - E-Check</span><br/>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td bgcolor="#484848" width="1">
            </td>
            <td valign="top">
                <table width="96%" align="center" dir="ltr">
                    <tbody>
                        <tr>
                            <td colspan="3" class="MerchantSubHead">
                                NOTES<br/>
                                <br/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0" width="100%" align="center" class="tblNote">
                                    <tbody>
                                        <tr>
                                            <td style="color: #484848; padding: 6px;">
                                                <span class="txt11b">16/10/08&nbsp;&nbsp;16:46</span> &nbsp; (lavi)<br/>
                                                test 1 2 3<br/>
                                                <br/>
                                            </td>
                                            <td valign="top" style="text-align: right;">
                                                <img src="../images/img/pageCurl_gray.gif"><br/>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="6">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0" width="100%" align="center" class="tblNote">
                                    <tbody>
                                        <tr>
                                            <td style="color: #484848; padding: 6px;">
                                                <span class="txt11b">01/01/00&nbsp;&nbsp;00:00</span>
                                                <br/>
                                                12333fgfgdfgdfg33<br/>
                                                <br/>
                                            </td>
                                            <td valign="top" style="text-align: right;">
                                                <img src="../images/img/pageCurl_gray.gif"><br/>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="6">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a style="font-weight: bold;" href="merchant_dataNotes.asp?CompanyID=35">See All / Add
                                    New ...</a><br/>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
