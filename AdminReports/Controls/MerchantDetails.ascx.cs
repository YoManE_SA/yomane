﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.Configuration;

namespace Netpay.AdminReports.Controls
{
    public partial class MerchantDetails : System.Web.UI.UserControl
    {
        public Bll.Merchant.Merchant Merchant {get; set;}
        public MerchantExtendedVO MerchantExtended { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void RedirectLogin(object sender, CommandEventArgs e) 
        {
            string location;
            switch (e.CommandArgument.ToString())
            {
                case "merchant":
                    location = ConfigurationManager.AppSettings["merchantControlPanelUrl"];
                    break;
                case "merchantTest":
                    location = ConfigurationManager.AppSettings["merchantControlPanelTestUrl"];
                    break;
                case "devCenter":
                    location = ConfigurationManager.AppSettings["merchantDevCenterUrl"];
                    break;
                case "devCenterTest":
                    location = ConfigurationManager.AppSettings["merchantDevCenterTestUrl"];
                    break;
                default:
                    throw new ArgumentException("invalid location type");
            }

            string url = string.Format("{0}website/default.aspx?login=outer&mail={1}&username={2}&userpassword={3}", location, Merchant.EmailAddress, MerchantExtended.UserName, MerchantExtended.Password);
            Response.Redirect(url);
        }
    }
}