﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.AdminReports.Code;
using Netpay.Infrastructure.VO;

namespace Netpay.AdminReports
{
    public partial class Merchants : NetpayPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GetMerchant(35);
        }

        protected void GetMerchant(object sender, EventArgs e) 
        {
            int merchantID = 0;
            if (!int.TryParse(wcMerchantAutoComplete.SelectedValue, out merchantID))
                return;

            GetMerchant(merchantID);
        }

        protected void GetMerchant(int merchantID)
        {
            var merchant = Netpay.Bll.Merchant.Merchant.Load(Web.WebUtils.DomainHost, merchantID);
            MerchantExtendedVO merchantExtended = Netpay.Bll.Merchants.GetMerchantExtended(Web.WebUtils.DomainHost, merchantID);

            merchantDetails.Merchant = merchant;
            merchantDetails.MerchantExtended = merchantExtended;
            merchantDetails.Visible = true;
        }
    }
}