﻿<%@ Page Title="Admin Reports - Fail Stat Reply" Language="C#" MasterPageFile="~/Reports.Master" AutoEventWireup="true" CodeBehind="FailStatReportReply.aspx.cs" Inherits="Netpay.AdminReports.FailStatReportReply" %>

<asp:Content ID="content2" ContentPlaceHolderID="head" runat="server">
	<style type="text/css">
		div.innerContent
		{
			width: 75%;
		}
	</style>
	<script type=text/javascript>
		$(function () 
		{
			$("#ctl00_body_datepickerFrom").datepicker({ minDate: 0, maxDate: 7, dateFormat: 'yy-mm-dd' });
			$("#ctl00_body_datepickerTo").datepicker({ minDate: 0, maxDate: 7, dateFormat: 'yy-mm-dd' });
		});
		
	</script>
</asp:Content>

<asp:Content ID="content1" ContentPlaceHolderID="body" runat="server">
	<table style="width:100%;" border="0">		
		<tr>
			<td class="pageMainHeading">Failure stats</td>
			<td style="text-align:right;">
				<netpay:ReportUpdateInfo ID="wcReportUpdateInfo" runat="server" Report="AdminFailStats" /> &nbsp;|&nbsp;
				<asp:Image ID="Image1" ImageUrl="Images/ImgFileExt/xls.gif" runat="server" />
				<asp:LinkButton ID="lnkBtnExcel" Text="EXPORT TO XLS" OnClick="CreateExcel" CssClass="ExportExcelTxt" runat="server" />
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:left;margin:0;padding:0">Failure stats per reply code <asp:Literal ID="ltReplyCode" runat="server" /></td>
		</tr>		
	</table>
	<br />
	<table style="width:100%;" class="formFilter">
		<tr>
			<td>From date:<br /><input id="datepickerFrom" type="text" runat="server"/></td>			
			<td>Currency:<br /><asp:DropDownList ID="ddlCurrencyID" runat="server" EnableViewState="true" /></td>
			<td>TransType <asp:CheckBox ID="chkTransTypeDebit" runat="server" Text="Debit" /><asp:CheckBox ID="chkTransTypePreAuth" runat="server" Text="PreAuth" /><asp:CheckBox ID="chkTransTypeCapture" runat="server"  Text="Capture" /></td>			
			<td><input type="button" value="RESET" onclick="Netpay.AdminReports.Utils.ClearForm(this.form)" /></td>
		</tr>
		<tr>
			<td>To date:<br /><input id="datepickerTo" type="text" runat="server"/></td>			
			<td>Payment method:<br /><asp:DropDownList ID="ddlPaymentMethodID" runat="server" EnableViewState="true" /></td>
			<td><asp:CheckBox ID="chkExcludeMultiple" runat="server" Checked="true" Text="Exclude multiple tries" /></td>
			<td><asp:Button ID="btnFilter" runat="server" Text=" FILTER " OnClick="FilterReport" UseSubmitBehavior="true" /></td>			
		</tr>
		<tr>
			<td colspan="5" style="text-align:left;margin:0;padding:0">&nbsp;Merchant:<br /><netpay:AutoComplete ID="wcMerchantAutoComplete" IsMultiselect="false" Function="GetMerchantNameAutoComplete" runat="server" /></td>			
		</tr>
	</table>
	<div class="title">Results</div>
	<netpay:SearchFiltersView ID="wcFiltersView" runat="server" />
	<table style="width:100%;" class="formNormal">
		<thead>
			<tr>								
				<th><asp:LinkButton ID="lbtnTerminalNumber" runat="server" Text="Terminal Number" CommandArgument="TerminalNumber" OnCommand="SortReport" /></th>
				<th><asp:LinkButton ID="LinkButton1" runat="server" Text="Contract Number" CommandArgument="ContractNumber" OnCommand="SortReport" /></th>				
				<th><asp:LinkButton ID="lbtnCount" runat="server" Text="Count" CommandArgument="GroupCount" OnCommand="SortReport" /></th>
			</tr>
		</thead>
		<tbody>
			<asp:Repeater ID="repeaterReport" runat="server">
				<ItemTemplate>
					<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
						<td class="reportCell">
							<%# ((FailStatReplyGroupVO)Container.DataItem).TerminalNumber%>&nbsp;
						</td>
						<td class="reportCell">
							<%# ((FailStatReplyGroupVO)Container.DataItem).ContractNumber%>&nbsp;
						</td>
						<td class="reportCell numericCell">
							<%# ((FailStatReplyGroupVO)Container.DataItem).GroupCount%>&nbsp;
						</td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</tbody>
	</table>
	<asp:HiddenField ID="hdReplyCode" runat="server" />
	<asp:HiddenField ID="hdDebitCompanyID" runat="server" />
	<asp:HiddenField ID="hdTerminalNumber" runat="server" />
	<asp:HiddenField ID="hdSortField" Value="GroupCount" runat="server" />	
	<asp:HiddenField ID="hdSortOrder" Value="Desc" runat="server" />	
</asp:Content>
