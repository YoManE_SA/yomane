﻿using Netpay.Bll.Content;
using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Xsl;

namespace Netpay.DevDocsGenerator
{
    public class Generator
    {
        private string _xsl;

        /// <summary>
        /// Generates docs for all domains
        /// </summary>
        public void Generate()
        {
            // get embedded xsl
            using (StreamReader streamReader = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("Netpay.DevDocsGenerator.wsdl-viewer.xsl")))
                _xsl = streamReader.ReadToEnd();
            
            // iterate domains
            Netpay.Infrastructure.Application.Init("DevDocsGenerator");
            foreach (string domainKey in Domain.Domains.Keys)
            {
                Console.WriteLine("domain " + domainKey);
                Domain.Current = Domain.Get(domainKey);
                string lockKey = "DevDocsGenerator" + domainKey;

                try
                {
                    Netpay.Infrastructure.ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);
                    if (!Infrastructure.Tasks.Lock.TryLock(lockKey, DateTime.Now.AddMinutes(-15)))
                        continue;

                    GenerateServices();
                    GenerateStatic();
                    DeleteZipFiles();
                }
                catch (Exception e)
                {
                    Logger.Log(LogSeverity.Error, LogTag.DevDocs, e.Message, e.ToString());
                    throw e;
                }
                finally
                {
                    Netpay.Infrastructure.ObjectContext.Current.StopImpersonate();
                    Infrastructure.Tasks.Lock.Release(lockKey);
                }
            }
        }

        private string ApplyTemplate(string template, string content)
        {
            string html = template.Replace("%%content%%", content);
            html = html.Replace("%%domainWebServiceUrl%%", Domain.Current.WebServicesUrl);
            return html;
        }

        private string DomainCachePath
        {
            get
            {
                string domainCachePath = "cache\\" + Domain.Current.Host + "\\";
                if (!Directory.Exists(domainCachePath))
                    Directory.CreateDirectory(domainCachePath);

                return domainCachePath;
            }
        }

        private void GeneratePdf(string html, string outputFile)
        {
            // create pdf
            string log = string.Format("creating pdf: " + outputFile);
            Console.WriteLine(log);
            Logger.Log(LogSeverity.Info, LogTag.WebService, log);
            WebSupergoo.ABCpdf10.Doc pdf = new WebSupergoo.ABCpdf10.Doc();
            pdf.HtmlOptions.Paged = true;
            pdf.HtmlOptions.AddLinks = true;
            pdf.Encryption.Type = 2;
            pdf.Encryption.CanCopy = false;
            pdf.Encryption.CanEdit = false;
            pdf.Encryption.Password = "GWDOCS";
            pdf.Encryption.OwnerPassword = "GWDOCS9367234";

            int pageId = pdf.AddImageHtml(html);
            while (true)
            {
                if (!pdf.Chainable(pageId))
                    break;
                pdf.Page = pdf.AddPage();
                pageId = pdf.AddImageToChain(pageId);
            }

            pdf.Flatten();
            pdf.HtmlOptions.LinkPages();
            pdf.Save(outputFile);
            pdf.Clear();
        }

        /// <summary>
        /// Generates static docs for the current domain
        /// </summary>
        private void GenerateStatic()
        {
            // get template
            string template = null;
            string templatePath = Domain.Current.MapPrivateDataPath("DevDocTemplates\\StaticTemplate.html");
            if (File.Exists(templatePath))
                template = File.ReadAllText(templatePath);
            else
                template = "%%content%%";

            // iterate static html files
            string inputPath = Domain.Current.CommonPhisicalPath + "StaticDevDocs";
            string[] staticFiles = Directory.GetFiles(inputPath, "*.html");
            foreach (string file in staticFiles)
            {
                string html = File.ReadAllText(file);
                string cacheFilePath = DomainCachePath + Path.GetFileName(file);
                if (File.Exists(cacheFilePath))
                {
                    string stored = File.ReadAllText(cacheFilePath);
                    if (html == stored)
                    {
                        Console.WriteLine("skipping " + Path.GetFileName(file));
                        continue;
                    }
                }

                string templatedHtml = ApplyTemplate(template, html);
                string outputFile = DeveloperDoc.DocsPath + Path.GetFileNameWithoutExtension(file) + ".pdf";
                GeneratePdf(templatedHtml, outputFile);

                // cache html 
                File.WriteAllText(cacheFilePath, html);
            }
        }

        private string GetServiceFileName(string service, string version)
        {
            return string.Format("Service.V{0}.{1}.pdf", version, Path.GetFileNameWithoutExtension(service));
        }

        private string LastServicesVersion
        {
            get
            {
                string filePath = DomainCachePath + "servicesLastVersion.txt";
                if (!File.Exists(filePath))
                    return "0.00";
                return File.ReadAllText(filePath);
            }
            set
            {
                string filePath = DomainCachePath + "servicesLastVersion.txt";
                File.WriteAllText(filePath, value);
            }
        }

        /// <summary>
        /// Generates services docs for the current domain
        /// </summary>
        private void GenerateServices()
        {
            // check services version
            string lastVersion = LastServicesVersion;
            string currentVersion = new WebClient().DownloadString(Domain.Current.WebServicesUrl + "v2/Global.svc/GetVersion");
            if (lastVersion == currentVersion)
            {
                string log = string.Format("Skipping services dev docs, last version equals currnt version, {0}", lastVersion);
                Console.WriteLine(log);
                Logger.Log(LogSeverity.Info, LogTag.WebService, log);
                return;
            }

            // get services
            string serviceList = new WebClient().DownloadString(Domain.Current.WebServicesUrl + "v2/Global.svc/GetServices");
            string[] services = serviceList.Split(',');

            // archive last ver docs
            foreach (var service in services)
            {
                string servicePath = DeveloperDoc.DocsPath + GetServiceFileName(service, lastVersion);
                string serviceArchivePath = DeveloperDoc.DocsArchivePath + GetServiceFileName(service, lastVersion);
                if (File.Exists(serviceArchivePath))
                    File.Delete(serviceArchivePath);
                if (File.Exists(servicePath))
                    File.Move(servicePath, serviceArchivePath);
            }

            // get template
            string template = null;
            string templatePath = Domain.Current.MapPrivateDataPath("DevDocTemplates\\ServiceTemplate.html");
            if (File.Exists(templatePath))
                template = File.ReadAllText(templatePath);
            else
                template = "%%content%%";

            // render services
            foreach (string service in services)
            {
                // check for changes
                string inputUrl = Domain.Current.WebServicesUrl + "V2/" + service + "?wsdl";
                string currentWsdl = new WebClient().DownloadString(inputUrl);

                // transform wsdl to html
                Console.WriteLine("transforming " + service);
                XmlTextReader reader = new XmlTextReader(new StringReader(currentWsdl));
                StringBuilder outputBuilder = new StringBuilder();
                XmlWriter writer = XmlWriter.Create(outputBuilder);
                XslCompiledTransform xslt = new XslCompiledTransform();
                XsltSettings settings = new XsltSettings(true, false);
                xslt.Load(new XmlTextReader(new StringReader(_xsl)), settings, new XmlUrlResolver());
                xslt.Transform(reader, writer);
                string html = outputBuilder.ToString();
                reader.Close();
                writer.Close();

                // create pdf
                string templatedHtml = ApplyTemplate(template, html);
                string outputFile = DeveloperDoc.DocsPath + GetServiceFileName(service, currentVersion);
                GeneratePdf(templatedHtml, outputFile);
            }

            // save last ver 
            LastServicesVersion = currentVersion;
        }

        private void DeleteZipFiles()
        {
            string[] zipFiles = Directory.GetFiles(DeveloperDoc.DocsPath, "*.zip", SearchOption.TopDirectoryOnly);
            foreach (string filePath in zipFiles)
                if (File.Exists(filePath))
                    File.Delete(filePath);
        }
    }
}
