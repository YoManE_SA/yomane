﻿using Netpay.Bll.BankHandler;
using Netpay.Bll.DebitCompanies;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {

                Netpay.Infrastructure.Application.Init("Win Service");
                SendTestEmail();


                // GetTimeZonesOnSystem();

                // Central Asia Standard Time
                // 
                GetCurrentTimeZone("South Africa Standard Time");
                //    SetPinCode("1234");
                // EncryptDecrypt();
                // TestWinServices();
                //  SetEnablefal 
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                Console.WriteLine("Test complted");
                Console.ReadLine();
            }
        }

        private static void SendTestEmail()
        {
            var attList = new List<System.Net.Mail.Attachment>();
            attList.Add(new System.Net.Mail.Attachment(@"C:\Users\User\Desktop\Lorem ipsum dolor sit amet.docx"));
            var message = new System.Net.Mail.MailMessage();
            message.From = new System.Net.Mail.MailAddress("support@yomane.com", "Support | YoManE");
            message.To.Add(new System.Net.Mail.MailAddress("peter@afrolynx.co.za", "Peter Mugisha | Afrolynx IT Solution"));
            message.Subject = "This is a test email";
            message.Body = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vitae ligula semper, dictum purus vel, faucibus tellus. Fusce suscipit congue libero, nec suscipit ex laoreet vel. Nam facilisis lacus nec imperdiet volutpat. Donec et turpis elit. Aliquam risus felis, porta vel arcu sed, porttitor pulvinar purus. Sed quis auctor magna. Sed vel elit tristique, elementum lectus ut, ornare leo. Morbi fringilla ac magna imperdiet dapibus. Nam porta lacus ut pellentesque imperdiet. Sed tristique dapibus nulla id tempus. In id ornare enim, a vestibulum massa.";
            var list = new List<string>();
            list.Add(@"C:\Users\User\Desktop\Lorem ipsum dolor sit amet.docx");
            Netpay.Infrastructure.Email.SmtpClient.Send(message, list);
        }

        private static void GetTimeZonesOnSystem()
        {
            foreach (TimeZoneInfo z in TimeZoneInfo.GetSystemTimeZones())
                Console.WriteLine(z.Id);
        }

        private static void GetCurrentTimeZone(string timeZone)
        {
            TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
            DateTime now = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tz);
            Console.WriteLine(now.ToString("F"));
        }

        private static void EncryptDecrypt()
        {
            var encstr = @"61D052E1DA6798B9E082615093D2DC82A0D52C1F68089683232CA6690A0BDA10F8415279F4CBC3BF8F164B9BF4E13238A839FFF11F786BFD3A184089EA939D39790A9D057F9994CB66DEBF17AEDBFE002B712E54AF0D83F668C08D0968D7EDC67A1DCA785D42D020FC17970D70619BB57586ECA480183CCDD880E3CBEDA3DE86";
            // var encstr = @"102770798868E5136DCE761CC663C69D";
            var decStr = DecryptText(encstr);
            Debug.WriteLine(decStr);
        }

        private static void SetPinCode(string newPincode)
        {
            var encryptedPin = Netpay.Infrastructure.Security.Encryption.GetSHA256Hash("PNC:" + newPincode);

            Debug.WriteLine(encryptedPin);
        }

        private static void TestWinServices()
        {
            try
            {
                Netpay.Bll.Application.Init();
                Netpay.Process.Application.Init();
                Netpay.Monitoring.Application.Init();
                SetEnable(true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private static void SetEnable(bool bEnable)
        {
            System.Diagnostics.Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.High;

            //Start monitors
            Netpay.Bll.BankHandler.AuthHandlerManager.Current.EnableFilesMonitor = bEnable;
            Netpay.Bll.BankHandler.AuthHandlerManager.Current.EnableGenerateTimer = bEnable;
            Netpay.Bll.BankHandler.ChbHandlerManager.Current.EnableFilesMonitor = bEnable;
            Netpay.Bll.BankHandler.EpaHandlerManager.Current.EnableFilesMonitor = bEnable;
            Netpay.Bll.BankHandler.FraudHandlerManager.Current.EnableFilesMonitor = bEnable;

            Netpay.Bll.BankHandler.FNBAuthHandler.EnableRealTimeFetch = bEnable;
            Netpay.Bll.Wires.WiresBatch.EnableRealTimeSend = bEnable;
            Netpay.Infrastructure.Domain.EnableClearTempDirectory = bEnable;

            Netpay.Bll.ThirdParty.eCentric.EnableServers(bEnable);
            Netpay.Bll.ThirdParty.ACS.EnableServers(bEnable);
        }

        private static void TestSmsService()
        {
            //Yomane.WebServices.Mobile client = new Yomane.WebServices.Mobile();
            //var result = client.Login("peter@afrolynx.co.za", "ptah", "ptah@test", "12345", "123");
            //client.SendSms(result.CredentialsToken, "blah", 123, "27719129418", "this is a test");
        }

        private static void TestSms()
        {
            Netpay.Bll.SMS.SMSService.SendSms("27719129418", "this is another one of those tests");
        }

        private static string DecryptText(string encryptedString)
        {

            var str = string.Empty;
            var ct = Netpay.Crypt.SymEncryption.GetKey(5);
            var elBlob = new Netpay.Crypt.Blob { Hex = encryptedString };
            str = ct.Decrypt(elBlob).Text;

            return str;
        }
    }
}
