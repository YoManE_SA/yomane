﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Logs
{
    public class LogHistory : BaseDataObject
    {
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Log.Module.Current, SecuredObjectName); } }

        public class SearchFilters
        {
            public Infrastructure.Range<int?> MerchantId;
            public Infrastructure.Range<int?> HistoryId;
            public Infrastructure.Range<DateTime?> DateRange;
            public int? SourceIdentity;
            public Netpay.CommonTypes.LogHistoryType? LogHistoryType;
        }

        
        private Dal.Netpay.History _entity;

        public int HistoryId { get { return _entity.History_id; }  }

        public int HistoryTypeId { get { return _entity.HistoryType_id; } }

        public string HistoryTypeName { get; private set; }

        public int? MerchantId { get { return _entity.Merchant_id; } }

        public string MerchantName { get; private set; }

      
        public int? SourceIdentity { get { return _entity.SourceIdentity; } }

        public DateTime InsertDate { get { return _entity.InsertDate; } }
      
        public string InsertUserName { get { return _entity.InsertUserName; }  }

        public string InsertIPAddress { get { return _entity.InsertIPAddress; } }

        public string VariableChar { get { return _entity.VariableChar; } }

        public System.Xml.Linq.XElement VariableXML { get { return _entity.VariableXML; } } 
      
        private LogHistory(Dal.Netpay.History entity)
        {
            _entity = entity;
        }

        public LogHistory()
        {
            _entity = new Dal.Netpay.History();
        }

        public static List<LogHistory> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var exp = (from h in DataContext.Reader.Histories orderby h.History_id descending
                      join ht in DataContext.Reader.HistoryTypes on h.HistoryType_id equals ht.HistoryType_id
                      join c in DataContext.Reader.tblCompanies on h.Merchant_id equals c.ID into cm
                      from c in cm.DefaultIfEmpty()
                      select new { h, ht, c });

            if (filters != null)
            {
                //Check dates range
                if (filters.DateRange.To.HasValue)
                {
                    // Set To date to EOD
                    filters.DateRange.To = filters.DateRange.To.Value.Date.AddDays(1).AddMilliseconds(-1);
                }

                //The date Range search
                if (filters.DateRange.From.HasValue) exp = exp.Where(x => x.h.InsertDate >= filters.DateRange.From);
                if (filters.DateRange.To.HasValue) exp = exp.Where(x => x.h.InsertDate <= filters.DateRange.To);


                //The merchant id search
                if (filters.MerchantId.From.HasValue) exp = exp.Where(x => x.h.Merchant_id >= filters.MerchantId.From);
                if (filters.MerchantId.To.HasValue) exp = exp.Where(x => x.h.Merchant_id <= filters.MerchantId.To);

                //The History id search
                if (filters.HistoryId.From.HasValue) exp = exp.Where(x => x.h.History_id >= filters.HistoryId.From);
                if (filters.HistoryId.To.HasValue) exp = exp.Where(x => x.h.History_id <= filters.HistoryId.To);


                //The Source Identity search
                if (filters.SourceIdentity != null) exp = exp.Where(x => x.h.SourceIdentity == filters.SourceIdentity);


                //The LogHistoryType search 
                if (filters.LogHistoryType != null) exp = exp.Where(x => x.h.HistoryType_id == (int)filters.LogHistoryType);

            }

            return exp.ApplySortAndPage(sortAndPage).Select(x => new LogHistory(x.h) { MerchantName = x.c.CompanyName, HistoryTypeName=x.ht.Name }).ToList();
        }
        public static LogHistory Load(int historyid)
        {
            return Search(new SearchFilters() { HistoryId = new Range<int?>(historyid) }, null).SingleOrDefault();
        }
    }
}
