﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;

using Netpay.Dal.Netpay;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll
{
    public class PendingEvents : BaseDataObject
    {
		public class SearchFilters
		{
			public Infrastructure.Range<int?> ID;
			public Infrastructure.Range<DateTime?> InsertDate;
			public CommonTypes.PendingEventType? EventType;
		}

        public const string SecuredObjectName = "Manage";

        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Log.PendingEvents.Module.Current, SecuredObjectName); } }

        private Dal.Netpay.EventPending _entity;

		public int ID { get { return _entity.EventPending_id; } }
		public CommonTypes.PendingEventType EventType { get { return (CommonTypes.PendingEventType) _entity.EventPendingType_id; } }
		public int? TransPassID { get { return _entity.TransPass_id; } }
		public int? TransPreAuthID { get { return _entity.TransPreAuth_id; } }
		public int? TransPendingID { get { return _entity.TransPending_id; } }
		public int? TransFailID { get { return _entity.TransFail_id; } }
		public int? MerchantID { get { return _entity.Merchant_id; } }
		public int? CustomerID { get { return _entity.Customer_id; } }
		public string Parameters { get { return _entity.Parameters; } }
		public System.DateTime InsertDate { get { return _entity.InsertDate; } }
		public byte TryCount { get { return _entity.TryCount; } }

		private PendingEvents(Dal.Netpay.EventPending entity)
		{
			_entity = entity;
		}

		public static List<PendingEvents> Search(SearchFilters filters, ISortAndPage sortAndPage)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var exp = (from p in DataContext.Reader.EventPendings select p);
			if (filters != null) {
				if (filters.ID.From.HasValue) exp = exp.Where(p => p.EventPending_id >= filters.ID.From);
				if (filters.ID.To.HasValue) exp = exp.Where(p => p.EventPending_id <= filters.ID.To);
				if (filters.InsertDate.From.HasValue) exp = exp.Where(p => p.InsertDate >= filters.InsertDate.From);
				if (filters.InsertDate.To.HasValue) exp = exp.Where(p => p.InsertDate <= filters.InsertDate.To);
				if (filters.EventType.HasValue) exp = exp.Where(p => p.EventPendingType_id == (short) filters.EventType);
			}
			return exp.OrderByDescending(x => x.EventPending_id).ApplySortAndPage(sortAndPage).Select(p => new PendingEvents(p)).ToList();
		}

        public static PendingEvents Load(int id)
        {
            return Search(new SearchFilters() { ID = new Range<int?> { From = id, To = id } }, null).SingleOrDefault();
        }

        public static void CreateForMerchant(PendingEventType type, int merchantId, string parameters)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            EventPending pendingEvent = new EventPending();
            pendingEvent.InsertDate = DateTime.Now;
            pendingEvent.EventPendingType_id = (short)type;
            pendingEvent.TryCount = 3;
            pendingEvent.Parameters = parameters;
            pendingEvent.Merchant_id = merchantId;
            DataContext.Writer.EventPendings.InsertOnSubmit(pendingEvent);
            DataContext.Writer.SubmitChanges();
        }

        public static void CreateForCustomer(PendingEventType type, int customerId, string parameters)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            EventPending pendingEvent = new EventPending();
            pendingEvent.InsertDate = DateTime.Now;
            pendingEvent.EventPendingType_id = (short)type;
            pendingEvent.TryCount = 3;
            pendingEvent.Parameters = parameters;
            pendingEvent.Customer_id = customerId;
            DataContext.Writer.EventPendings.InsertOnSubmit(pendingEvent);
            DataContext.Writer.SubmitChanges();
        }

        public static void Create(PendingEventType type, int transactionID, TransactionStatus status, string parameters)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            EventPending pendingEvent = new EventPending();
            pendingEvent.InsertDate = DateTime.Now;
			pendingEvent.EventPendingType_id = (short)type;
			pendingEvent.TryCount = 3;
			pendingEvent.Parameters = parameters;
            switch (status)
            {
                case TransactionStatus.Captured:
					pendingEvent.TransPass_id = transactionID;
                    break;
                case TransactionStatus.Declined:
					pendingEvent.TransFail_id = transactionID;
                    break;
                case TransactionStatus.Authorized:
					pendingEvent.TransPreAuth_id = transactionID;
                    break;
                case TransactionStatus.Pending:
					pendingEvent.TransPending_id = transactionID;
                    break;
                default:
                    throw new ApplicationException("Invalid transaction status");
            }

			DataContext.Writer.EventPendings.InsertOnSubmit(pendingEvent);
			DataContext.Writer.SubmitChanges();
        }

        public static int GetUnProcessedPendingEvents()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            int result = (from ept in DataContext.Reader.EventPendingTypes
                          join ep in DataContext.Reader.EventPendings on ept.EventPendingType_id equals ep.EventPendingType_id
                          where ep.InsertDate < DateTime.Now.AddMinutes(-ept.MinutesForWarning.Value) && ept.MinutesForWarning > 0
                          select ep.EventPending_id).Count();
            return result; 
        }

        public void UpdateTryCount(byte trycount)
        {
            if (trycount <= 0) return;

            _entity.TryCount = trycount;
            DataContext.Writer.EventPendings.Update(_entity, _entity.EventPending_id != 0);
            DataContext.Writer.SubmitChanges();
        }
    }
}
