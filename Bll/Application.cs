﻿using System;
using Netpay.Bll.BankHandler;
using Netpay.Bll.Risk;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Tasks;

namespace Netpay.Bll
{
	/// <summary>
	/// Bll application
	/// </summary>
	public static class Application
	{
		public static bool IsInit { get; set; }

        /// <summary>
		/// <para>Inits the application.</para>
		/// <para>Sets up various parts of the application such as <see cref="Netpay.Infrastructure.Logger"/>.</para>
		/// <para>The application will not run properly without initialization.</para>
		/// </summary>
		public static void Init()
		{
			bool isLoggerInit = false;

            if (!IsInit)
			{
				// subscribe UnhandledException
				AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
				
				try
				{
                    var dummyEpa = EpaHandlerManager.Current;
                    var dummyChb = ChbHandlerManager.Current;
                    var dummyFraud = FraudHandlerManager.Current;
                    var dummyAuth = AuthHandlerManager.Current;
                    Logger.Log(LogSeverity.Info, LogTag.AppInit, "BankHandlers initialized.");

					//FraudDetection.Init();

                    //Infrastructure.Module.Init();
                    //Logger.Log(LogSeverity.Info, LogTag.AppInit, "Modules initialized.");

                    Domain.Current = null;
                    Logger.Log(LogSeverity.Info, LogTag.AppInit, "Bll Application initialized.");
                    IsInit = true;
				}
				catch (Exception ex)
				{
                    IsInit = false;
					if (isLoggerInit)
						Logger.Log(ex);
					throw new ApplicationException("Bll Application failed to initialize, see log file for more details.");
				}
			}
		}

		private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			Exception ex = (Exception)e.ExceptionObject;
			Logger.Log(ex);
		}
	}
}
