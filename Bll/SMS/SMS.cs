﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.SMS
{
	public interface ISMSProvider
	{
		bool SendSms(Infrastructure.Domain domain, string fromText, string toPhoneNumber, string message);
	}
	
	public static class SMSService
	{
        public static string TemplateDirectory { get { return Domain.Current.MapPrivateDataPath("SMS Templates"); } }

        public static void SendSms(int transID, string toPhoneNumber, string merchantText)
		{
			//User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited, UserType.NetpayUser, UserType.NetpayAdmin });
			//var user = Infrastructure.Security.SecurityManager.GetInternalUser(credentialsToken);
			var ctp = Transactions.Transaction.GetTransaction(null, transID, TransactionStatus.Captured);
			if (ctp == null) throw new ApplicationException("transaction not found");
			if (ctp.Merchant == null) throw new ApplicationException("transaction.Merchant not found");
            if (string.IsNullOrEmpty(toPhoneNumber)) toPhoneNumber = ctp.PayerData.PhoneNumber;
            var dataObj = new { Transaction = ctp, Merchant = ctp.Merchant, ExtraText = merchantText };
            string templateName = null;
            switch (ctp.Status) { 
                case TransactionStatus.Declined:
                case TransactionStatus.DeclinedArchived:
                    templateName = "CustomerNotify_DeclinedTrans"; break;
                default:
                    templateName = "CustomerNotify_ApprovedTrans"; break;
            }
            if (ctp.PaymentData.IssuerCountryIsoCode == "IL") templateName += "_he";
            templateName += ".txt";
            if (SendSms(toPhoneNumber, templateName, dataObj, null, null))
				Transactions.History.Create(ctp, CommonTypes.TransactionHistoryType.InfoSmsSendClient, null);
		}

        public static bool SendSms(string toPhoneNumber, string templateName, object dataObj, System.Resources.ResourceManager rm, System.Globalization.CultureInfo cultureInfo)
		{
			string subject, body = Infrastructure.Email.FormatMesage.GetMessageText(System.IO.Path.Combine(TemplateDirectory, templateName), out subject);
            body = Infrastructure.Email.FormatMesage.FormatMessage(body, dataObj, rm, cultureInfo);
            return SendSms(toPhoneNumber, body);
        }

        public static bool SendSms(string toPhoneNumber, string message)
		{
            bool result = false;
            try
            {
                var domain = Domain.Current;
                ISMSProvider provider = null;
                switch (domain.SmsServiceProvider)
                {
                    case "SmsCenter":
                        provider = new SmsCenter();
                        break;
                    case "Nexmo":
                        provider = new Nexmo();
                        break;
                    case "InfoBip":
                        provider = new InfoBipSmsManager();
                        break;
                    default:
                        throw new Exception("Unable to determine SMS provider.");
                         
                }
                result = provider.SendSms(domain, domain.SmsServiceFrom, toPhoneNumber, message);
            } 
              catch (Exception ex)
            {
                Logger.Log(LogTag.Exception, new Exception("Send SMS", ex));
                result = false;
            }
            return result;
 
        }
        
    }
}
