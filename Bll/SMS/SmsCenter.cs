﻿using System;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.SMS
{
	public class SmsCenter : ISMSProvider
	{
		public bool SendSms(Infrastructure.Domain domain, string fromText, string toPhoneNumber, string message)
		{
			string retValue, serverAddress = "http://www.smscenter.co.il/Web/pushsms.aspx";
			Encoding serviceEncoding = Encoding.UTF8;
			toPhoneNumber = toPhoneNumber.Replace("-", "").Replace(" ", "");
			if (toPhoneNumber.StartsWith("972")) toPhoneNumber = "0" + toPhoneNumber.Substring(3);
			StringBuilder sendParams = new StringBuilder();
			sendParams.Append("&Username=" + System.Web.HttpUtility.UrlEncode(domain.SmsServiceUserName, serviceEncoding));
			sendParams.Append("&Password=" + System.Web.HttpUtility.UrlEncode(domain.SmsServicePassword, serviceEncoding));
			sendParams.Append("&Sender=" + System.Web.HttpUtility.UrlEncode(fromText, serviceEncoding));
			sendParams.Append("&ToPhoneNumber=" + System.Web.HttpUtility.UrlEncode(toPhoneNumber, serviceEncoding));
			sendParams.Append("&Message=" + System.Web.HttpUtility.UrlEncode(message, serviceEncoding));
			sendParams.Append("&EnableChaining=1");

			System.Net.HttpStatusCode hsc = Infrastructure.HttpClient.SendHttpRequest(serverAddress, out retValue, sendParams.ToString(), serviceEncoding);
			if (hsc == System.Net.HttpStatusCode.OK)
			{
				if (retValue == "OK")
					return true;
				else 
					throw new System.Net.WebException(string.Format("Send sms failed, request={0}, response={1}", sendParams, retValue));
			}

			throw new System.Net.WebException(string.Format("Unable to connect sms service:{0}, return:{1}", sendParams, retValue));
		}
	}
}
