﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using System.Collections.Generic;
using Netpay.Infrastructure;

namespace Netpay.Bll.SMS
{

    public class InfoBipSmsManager: ISMSProvider
    {
        private const string SingleTextMessageUri = @"https://api.infobip.com/sms/1/text/single";
        private const string MultipleTextMessageUri = @"https://api.infobip.com/sms/1/text/multi";
       

        /// <summary>
        /// The constructor initializes the user credentials to authorize the text message
        /// </summary>
        /// <param name="username">account username</param>
        /// <param name="password">account password</param>
        public InfoBipSmsManager( )
        {
        }

        private TextMessageResponse SendText(Domain domain, WebRequest req, byte[] data)
        {

            var username = DecryptText(domain.SmsServiceUserName);
            var password = DecryptText(domain.SmsServicePassword); 
            var  ecnryptedCredentials =  Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Format("{0}:{1}", username, password)));
             

            TextMessageResponse tmr = null;
            req.ContentLength = data.Length;
            req.Method = "POST";
            req.Headers.Add("Authorization", string.Format("Basic {0}", ecnryptedCredentials));
            req.Headers.Add("Accept-Encoding", "application/json");
            req.Timeout = 10 * 60 * 1000; //10 minutes time out
            using (var sr = req.GetRequestStream())
            {
                sr.Write(data, 0, data.Length);
            }
            var res = req.GetResponse();
            var strm = res.GetResponseStream();
            if (strm != null)
            {

                var responseText = new StreamReader(strm).ReadToEnd();
                tmr = JsonConvert.DeserializeObject<TextMessageResponse>(responseText);

            }
            return tmr;
        }

        private string DecryptText(string smsServiceUserName)
        { 
            var ct = Netpay.Crypt.SymEncryption.GetKey(5);
            return ct.Decrypt(new Netpay.Crypt.Blob() { Hex = smsServiceUserName }).Text;
        }

        /// <summary>
        /// Sends a single text message.
        /// </summary>
        /// <param name="message">The object includes the 'from' number, 'to' number and the text message</param>
        /// <param name="baseUrl">The base URL of the API</param>
        /// <returns>true if the message is sent otherwise false</returns>
        public bool SendSingleText(Domain domain, TextMessage message, string baseUrl)
        {
            var serializedJson = JsonConvert.SerializeObject(message);
            var enc = new UTF8Encoding(false);
            var data = enc.GetBytes(serializedJson);

            var tmr = SendText(domain, WebRequest.Create(baseUrl), data);
            return tmr != null && tmr.Messages.Any();
        }
        /// <summary>
        /// Sends multiple text messages.
        /// </summary>
        /// <param name="messages">The object includes the 'from' number, 'to' number and the text message</param>
        /// <param name="baseUrl">The base URL of the API</param>
        /// <returns>the number of messages that were sent</returns>
        public int SendMultipleMessages(Domain domain, MultipleMessages messages, string baseUrl)
        {
            var sentItems = 0;
            var serializedJson = JsonConvert.SerializeObject(messages);
            var enc = new UTF8Encoding(false);
            var data = enc.GetBytes(serializedJson);


            var tmr = SendText(domain, WebRequest.Create(baseUrl), data);
            if (tmr != null && tmr.Messages != null)
                sentItems = tmr.Messages.Count;
            return sentItems;
        }

        
        public bool SendSms(Domain domain, string fromText, string toPhoneNumber, string message)
        {


            toPhoneNumber = toPhoneNumber.GetNumbers();
            ///TODO: update code to cater for other countries. Default is SA.
            ///

            if (toPhoneNumber.Length == 10)
            {
                toPhoneNumber = string.Format("27{0}", toPhoneNumber.Substring(1));
            }
            else if (toPhoneNumber.StartsWith("+") && toPhoneNumber.Length == 10)
            {
                toPhoneNumber = toPhoneNumber.Substring(1);
            }
            return SendSingleText(domain,new TextMessage
            {
                from = fromText,
                text = message,
                to = toPhoneNumber
            }, SingleTextMessageUri);
        }
    }
    public class TextMessageResponse
    {
        public List<Messages> Messages { get; set; }
    }
    public class Messages
    {
        public string BulkId { get; set; }
        public string To { get; set; }
        public MessageStatus Status { get; set; }
        public string MessageId { get; set; }
        public int SmsCount { get; set; }
    }
    public class MultipleMessages
    {
        public List<TextMessage> messages { get; set; }
    }
    public class MessageStatus
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
    public class TextMessage
    {
        public string from { get; set; }
        public string to { get; set; }
        public string text { get; set; }
    }
}
