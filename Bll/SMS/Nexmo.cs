﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.SMS
{
	public class Nexmo : ISMSProvider
	{
		public bool SendSms(Infrastructure.Domain domain, string fromText, string toPhoneNumber, string message)
		{
			string retValue, serverAddress = "https://rest.nexmo.com/sms/xml";
			Encoding serviceEncoding = Encoding.UTF8;
			toPhoneNumber = toPhoneNumber.Replace("-", "").Replace(" ", "");
			//if (toPhoneNumber.StartsWith("972")) toPhoneNumber = "0" + toPhoneNumber.Substring(3);
			StringBuilder sendParams = new StringBuilder();
			sendParams.Append("&api_key=" + System.Web.HttpUtility.UrlEncode(domain.SmsServiceUserName, serviceEncoding));
			sendParams.Append("&api_secret=" + System.Web.HttpUtility.UrlEncode(domain.SmsServicePassword, serviceEncoding));
			sendParams.Append("&from=" + System.Web.HttpUtility.UrlEncode(fromText, serviceEncoding));
			sendParams.Append("&to=" + System.Web.HttpUtility.UrlEncode(toPhoneNumber, serviceEncoding));
			sendParams.Append("&text=" + System.Web.HttpUtility.UrlEncode(message, serviceEncoding));
			//sendParams.Append("&EnableChaining=1");

			System.Net.HttpStatusCode hsc = Infrastructure.HttpClient.SendHttpRequest(serverAddress, out retValue, sendParams.ToString(), serviceEncoding);
			if (hsc == System.Net.HttpStatusCode.OK)
			{
				var xmlDoc = new System.Xml.XmlDocument();
				xmlDoc.LoadXml(retValue);
				var status = xmlDoc.SelectSingleNode("//status");
				if (status != null && status.InnerText == "0") return true;
				throw new System.Net.WebException(string.Format("Send Nexmo sms failed, request={0}, response={1}", sendParams, retValue));
			}

			throw new System.Net.WebException(string.Format("Unable to connect Nexmo service:{0}, return:{1}", sendParams, retValue));
		}
	}
}
