﻿using System;
using System.Collections.Generic;
using System.Linq;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Domains;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure.VO;
using Netpay.Dal.ErrorNet;

namespace Netpay.Bll
{
	/// <summary>
	/// Contains database stored logs logic.
	/// </summary>
	public static class Logs
	{
        public static void LogHistory(Guid credentialsToken, HistoryLogType logType, string request, string response) 
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary });
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            dc.Log_SpInsertHistory((int)logType, user.ID, null, null, request, response, null, null, null, null);
        }
        
        public static List<BllLogVO> GetLogs(Guid credentialsToken, SearchFilters filters, ISortAndPage sortAndPage)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser });

			ErrorNetDataContext dc = new ErrorNetDataContext(Configuration.GetParameter("errorNetConnectionString"));
			var expression = from l in dc.tblBllLogs select l;

			if (filters.dateFrom != null)
				expression = expression.Where(l => l.InsertDate >= filters.dateFrom);
			if (filters.dateTo != null)
				expression = expression.Where(l => l.InsertDate <= filters.dateTo);
			if (filters.logSeverityID != null)
				expression = expression.Where(l => l.SeverityID == filters.logSeverityID);
			if (filters.logTag != null)
				expression = expression.Where(l => l.Tag.ToLower() == filters.logTag.ToLower());

			if (sortAndPage == null) expression = expression.OrderByDescending(l => l.InsertDate);
			var exp = expression.ApplySortAndPage(sortAndPage).Select(l => new BllLogVO(l)); 
			return exp.ToList();
		}

		public static void DeleteLogs(Guid credentialsToken)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser });

			ErrorNetDataContext dc = new ErrorNetDataContext(Configuration.GetParameter("errorNetConnectionString"));
			dc.ExecuteCommand("DELETE FROM [Errors].[dbo].[tblBllLog]");
		}

		public static void DeleteLogs(SearchFilters filters)
		{
			ErrorNetDataContext dc = new ErrorNetDataContext(Configuration.GetParameter("errorNetConnectionString"));
			var expression = (IQueryable<tblBllLog>)from l in dc.tblBllLogs select l;

			if (filters.dateFrom != null)
				expression = expression.Where(l => l.InsertDate >= filters.dateFrom);
			if (filters.dateTo != null)
				expression = expression.Where(l => l.InsertDate <= filters.dateTo);
			if (filters.logSeverityID != null)
				expression = expression.Where(l => l.SeverityID == filters.logSeverityID);
			if (filters.logTag != null)
				expression = expression.Where(l => l.Tag.ToLower() == filters.logTag.ToLower());

			var logs = expression.ToList<tblBllLog>();
			dc.tblBllLogs.DeleteAllOnSubmit(logs);
			dc.SubmitChanges();
		}

		public static List<FailedConnectionLogVO> SearchFailedConnections(Guid credentialsToken, DateTime? from, DateTime? to, ISortAndPage sortAndPage)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary });
			
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var expression = (IQueryable<tblLog_NoConnection>)(from fcl in dc.tblLog_NoConnections where fcl.Lnc_CompanyID == user.ID orderby fcl.Lnc_InsertDate descending select fcl);

			// add filters
			if (from != null)
				expression = expression.Where(t => t.Lnc_InsertDate >= from.Value.MinTime());
			if (to != null)
				expression = expression.Where(t => t.Lnc_InsertDate <= to.Value.MaxTime());

			return expression.ApplySortAndPage(sortAndPage).Select(fcl => new FailedConnectionLogVO(fcl)).ToList<FailedConnectionLogVO>();
		}

		public static LogPaymentPageVO GetPaymentLog(string domainHost, int paymentLogID)
		{
			Domain domain = DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);

			tblLogPaymentPage entity = (from lpp in dc.tblLogPaymentPages where lpp.LogPaymentPage_id == paymentLogID select lpp).SingleOrDefault();
			if (entity == null)
				return null;

			return new LogPaymentPageVO(entity);
		}
		
		public static List<LogPaymentPageVO> SearchPaymentPageLog(Guid credentialsToken, DateTime? from, DateTime? to, ISortAndPage sortAndPage)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var expression = (IQueryable<tblLogPaymentPage>)(from lpp in dc.tblLogPaymentPages orderby lpp.Lpp_DateStart descending select lpp);

			// add filters
			if (from != null)
				expression = expression.Where(t => t.Lpp_DateStart >= from.Value.MinTime());
			if (to != null)
				expression = expression.Where(t => t.Lpp_DateEnd <= to.Value.MaxTime());

			// run query and return collection
			List<LogPaymentPageVO> results = expression.ApplySortAndPage(sortAndPage).Select(lpp => new LogPaymentPageVO(lpp)).ToList<LogPaymentPageVO>();
			return results;
		}

		public static void UpdatePaymentPageLog(string domainHost, LogPaymentPageVO paymentPageLog)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			tblLogPaymentPage entity = (from lpp in dc.tblLogPaymentPages where lpp.LogPaymentPage_id == paymentPageLog.PaymentPage_id select lpp).SingleOrDefault();
			if (entity == null) return;
			entity.Lpp_ReplyCode = paymentPageLog.ReplyCode;
			entity.Lpp_ReplyDesc = paymentPageLog.ReplyDesc;
			entity.Lpp_TransNum = paymentPageLog.TransNum;
			entity.Lpp_DateEnd = paymentPageLog.DateEnd;
			dc.SubmitChanges();
		}

		public static void AddPaymentPageLog(string domainHost, LogPaymentPageVO paymentPageLog)
		{
			tblLogPaymentPage entity = new tblLogPaymentPage();
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			entity.Lpp_MerchantNumber = paymentPageLog.MerchantNumber.Truncate(7);
			entity.Lpp_RemoteAddress = paymentPageLog.RemoteAddress.Truncate(20);
			entity.Lpp_RequestMethod = paymentPageLog.RequestMethod.Truncate(12);
			entity.Lpp_PathTranslate = paymentPageLog.PathTranslate.Truncate(4000);
			entity.Lpp_HttpHost = paymentPageLog.HttpHost.Truncate(400);
			entity.Lpp_HttpReferer = paymentPageLog.HttpReferer.Truncate(4000);
			entity.Lpp_QueryString = paymentPageLog.QueryString.Truncate(4000);
			entity.Lpp_RequestForm = paymentPageLog.RequestForm.Truncate(4000);
			entity.Lpp_SessionContents = paymentPageLog.SessionContents.Truncate(4000);
			entity.Lpp_ReplyCode = paymentPageLog.ReplyCode.Truncate(50);
			entity.Lpp_ReplyDesc = paymentPageLog.ReplyDesc.Truncate(500);
			entity.Lpp_TransNum = paymentPageLog.TransNum;
			entity.Lpp_DateStart = paymentPageLog.DateStart;
			entity.Lpp_DateEnd = paymentPageLog.DateEnd;
			entity.Lpp_LocalAddr = paymentPageLog.LocalAddr.Truncate(20);
			entity.Lpp_IsSecure = paymentPageLog.IsSecure;
			dc.tblLogPaymentPages.InsertOnSubmit(entity);

			dc.SubmitChanges();
			paymentPageLog.PaymentPage_id = entity.LogPaymentPage_id;
		}

		public static void SaveGenericLog(Guid credentialsToken, CommonTypes.LogHistoryType logType, int? merchantId, string action, string requestData, string responseData, string successCode)
		{
			Infrastructure.Domains.Domain domain = Infrastructure.Domains.DomainsManager.GetDomain(credentialsToken);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			dc.Log_SpInsertHistory((int)logType, merchantId, action, successCode, requestData, responseData, null, null, null, null);
		}

		public static void SaveGenericLog(string domainHost, CommonTypes.LogHistoryType logType, int? merchantId, string action, string requestData, string responseData, string successCode)
		{
            Infrastructure.Domains.Domain domain = Infrastructure.Domains.DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			dc.Log_SpInsertHistory((int)logType, merchantId, action, successCode, requestData, responseData, null, null, null, null);
		}

		public static void SavePaymentPageTransLog(string domainHost, LogPaymentPageTranVO paymentPageTransLog)
		{
            tblLogPaymentPageTran entity = null;
            NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
            if (paymentPageTransLog.LogPaymentPageTrans_id != 0)
                entity = (from t in dc.tblLogPaymentPageTrans where t.LogPaymentPageTrans_id == paymentPageTransLog.LogPaymentPageTrans_id select t).SingleOrDefault();
            if (entity == null) {
                entity = new tblLogPaymentPageTran();
                dc.tblLogPaymentPageTrans.InsertOnSubmit(entity);
            }
			//entity.LogPaymentPageTrans_id = paymentPageTransLog.LogPaymentPageTrans_id;
			entity.Lppt_Date = paymentPageTransLog.Date;
			entity.Lppt_LogPaymentPage_id = paymentPageTransLog.LogPaymentPage_id;
			entity.Lppt_PaymentMethodType = (short)paymentPageTransLog.PaymentMethodType;
			entity.Lppt_RequestString = paymentPageTransLog.RequestString.Truncate(3800); ;
			entity.Lppt_ResponseString = paymentPageTransLog.ResponseString.Truncate(3800); ;
			entity.Lppt_TransNum = paymentPageTransLog.TransNum;
			dc.SubmitChanges();
		}

        public static LogPaymentPageTranVO GetPaymentPageLastTransLog(string domainHost, int paymentPageLogId)
        {
            NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
            return (from t in dc.tblLogPaymentPageTrans where t.Lppt_LogPaymentPage_id == paymentPageLogId orderby t.LogPaymentPageTrans_id descending select new LogPaymentPageTranVO(t)).FirstOrDefault();
        }

        public static IntegrationServicesLogVO GetPackageLastUpdateLog(string domainHost, string packageName)
		{
			ReportsDataContext dc = new ReportsDataContext(DomainsManager.GetDomain(domainHost).ReportsConnectionString);
			packageName = packageName.ToLower().Replace(" ", string.Empty).Replace(".dtsx", string.Empty);
			return (from l in dc.tblIntegrationServicesLogs where (l.PackageName.ToLower().Replace(" ", string.Empty).Replace(".dtsx", string.Empty) == packageName && l.IsSuccessful) orderby l.StartDate descending select new IntegrationServicesLogVO(l)).FirstOrDefault();
		}
		
		public static List<ProcessRequestLogVO> GetProcessRequestsLog(Guid credentialsToken, SearchFilters filters, ISortAndPage sortAndPage)
		{
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser, UserType.MerchantLimited, UserType.MerchantPrimary });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var expression = (IQueryable<tblLogChargeAttempt>)from pr in dc.tblLogChargeAttempts orderby pr.Lca_DateStart descending select pr;
            if (user.Type == UserType.MerchantLimited || user.Type == UserType.MerchantPrimary) expression = expression.Where(e => e.Lca_MerchantNumber == user.Number);
			if (filters != null)
			{
				if (filters.dateFrom != null)
					expression = expression.Where(e => e.Lca_DateStart >= filters.dateFrom.Value.MinTime());
				if (filters.dateTo != null)
					expression = expression.Where(e => e.Lca_DateStart <= filters.dateTo.Value.MaxTime());
				if (filters.replyCode != null)
					expression = expression.Where(e => e.Lca_ReplyCode == filters.replyCode);
                if (filters.freeText != null)
                    expression = expression.Where(e => e.Lca_RequestString.IndexOf(filters.freeText) > -1 || e.Lca_ResponseString.IndexOf(filters.freeText) > -1);
            }
			return expression.ApplySortAndPage(sortAndPage).Select(pr => new ProcessRequestLogVO(user.Domain.Host, pr)).ToList<ProcessRequestLogVO>();
		}
	}
}
