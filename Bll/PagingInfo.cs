﻿using System;
using System.Text;

namespace Netpay.Bll
{
	/// <summary>
	/// Contains paging information and logic.
	/// </summary>
	public class PagingInfo
	{
		private int _pageSize = 0;
		private int _currentPage = 0;
		private int _totalItems = 0;				
		
		public bool CountOnly { get; set; }
		public Func<Guid, PagingInfo, System.Collections.IEnumerable> DataFunction { get; set; }

		/// <summary>
		/// Number of items per page
		/// </summary>
		public int PageSize
		{
			get { return _pageSize; }
			set { _pageSize = value; }
		}

		/// <summary>
		/// One-based current page number
		/// </summary>
		public int CurrentPage
		{
			get { return _currentPage; }
			set { _currentPage = value; }
		}

		/// <summary>
		/// Total number of items returned without paging
		/// </summary>
		public int TotalItems
		{
			get { return _totalItems; }
			set { _totalItems = value; }
		}

		/// <summary>
		/// Returns true if there are results (TotalItems > 0)
		/// </summary>
		public bool HasResults
		{
			get { return _totalItems > 0; }
		}

		/// <summary>
		/// One-based total number of pages
		/// </summary>		
		public int TotalPages
		{
			get 
			{ 
				double pages = TotalItems / (double)PageSize;
				return (int)System.Math.Ceiling(pages); 
			}
		}

		/// <summary>
		/// Returns true if there are more than 1 page
		/// </summary>		
		public bool HasPages
		{
			get
			{
				return TotalPages > 1;
			}
		}

		public int Skip
		{
			get
			{
				return (_currentPage - 1) * _pageSize;
			}
		}

		public int Take
		{
			get
			{
				return _pageSize;
			}
		}

		public int RecordFrom
		{
			get
			{
				return (_currentPage - 1) * _pageSize + 1;
			}
		}

		public int RecordTo
		{
			get
			{
				return _currentPage * _pageSize > _totalItems ? _totalItems : _currentPage * _pageSize;
			}
		}

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("\n PageSize: " + PageSize);
			sb.Append("\n CurrentPage: " + CurrentPage);
			sb.Append("\n TotalItems: " + TotalItems);
			sb.Append("\n TotalPages: " + TotalPages);
			sb.Append("\n HasPages: " + HasPages);
			
			return sb.ToString();
		}
	}
}
