﻿using System;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure.Domains;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.Bll
{
	/// <summary>
	/// Represents a credit card.
	/// </summary>
	public class CreditCard
	{
		public CreditCard(string q1, string q2, string q3, string q4) : this(q1 + q2 + q3 + q4)
		{
			
		}

		public CreditCard(string cardNumber)
		{
			_cardNumber = Regex.Replace(cardNumber.EmptyIfNull(), @"[^0-9*]", "");
		}

		public CreditCard(string domainHost, byte[] encryptedCardNumber)
		{
			_cardNumber = Netpay.Infrastructure.Security.Encryption.Decrypt(domainHost, encryptedCardNumber);
			_cardNumber = Regex.Replace(_cardNumber.EmptyIfNull(), @"[^0-9]", "");
		}
		
		private string _cardNumber = null;
		private DateTime _expirationDate = DateTime.MinValue;
		private BinNumberVO _bin = null;

		public DateTime ExpirationDate
		{
			get { return _expirationDate; }
			set { _expirationDate = value; }
		}

		public string[] ToArray()
		{
			string[] parsedCard = new string[4];
			if (_cardNumber.Length == 8)
			{
				parsedCard[0] = _cardNumber.Substring(0, 4);
				parsedCard[1] = _cardNumber.Substring(4, 4);
				parsedCard[2] = "";
				parsedCard[3] = "";	
			}
			else if (_cardNumber.Length == 9)
			{
				parsedCard[0] = _cardNumber.Substring(0, 4);
				parsedCard[1] = _cardNumber.Substring(4, 4);
				parsedCard[2] = _cardNumber.Substring(8, 1);
				parsedCard[3] = "";				
			}
			else if (_cardNumber.Length >= 12)
			{
				parsedCard[0] = _cardNumber.Substring(0, 4);
				parsedCard[1] = _cardNumber.Substring(4, 4);
				parsedCard[2] = _cardNumber.Substring(8, 4);
				parsedCard[3] = _cardNumber.Substring(12, 4 - (16 - _cardNumber.Length));					
			}

			return parsedCard;		
		}

		public string Last4Digits
		{
			get
			{
				return _cardNumber.Substring(_cardNumber.Length - 4);
			}
		}
		
		public override string ToString()
		{
			return _cardNumber.Trim();
		}
		
        /*
		public string ToFormattedString()
		{
			string[] arr = ToArray();
			return (arr[0] + " " + arr[1] + " " + arr[2] + " " + arr[3]).Trim(); 
		}
        */

		/// <summary>
		/// A string which reveals only the last 4 digits of the number
        /// Should be 458000 XX XXXX 0000
		/// </summary>
		/// <returns></returns>
		public string ToDisplayableString()
		{
            return _cardNumber.ToSafeCCString();
		}

		public byte[] ToEncryptedArray(string domainHost)
		{
			byte[] encrypted;
			Encryption.Encrypt(domainHost, _cardNumber, out encrypted);

			return encrypted;
		}

		/// <summary>
		/// Validates the credit card.
		/// A card is invalid if it fails basic validation or does not pass luhn / isracart validation.
		/// </summary>
		public bool IsValid
		{
			get
			{
				return Validate(_cardNumber);
			}
		}

		public static bool Validate(string cardNumber)
		{
			if (cardNumber == null)
					return false;
			if (cardNumber.Trim() == "")
					return false;
			if (cardNumber.Length == 8 || cardNumber.Length == 9)
				return ValidateIsracart(cardNumber);
			if (cardNumber.Length >= 13 && cardNumber.Length <= 16)
				return ValidateLuhn(cardNumber);
				
			return false;		
		}
		
		private static bool ValidateIsracart(string cardNumber)
		{
			if (cardNumber.Length == 8)
				cardNumber = "0" + cardNumber;
			
			int total = 0;
			int index = 0;
			for (sbyte reverseIndex = 8; reverseIndex >= 0; reverseIndex--)
			{
				index++;
				sbyte currentDigit = sbyte.Parse(cardNumber[reverseIndex].ToString());
				total += index * currentDigit;
			}

			bool isValid = ((total > 0) && (System.Math.Round((double)total / 11) * 11 == total));
			return isValid;
		}
		
		private static bool ValidateLuhn(string cardNumber)
		{
			// validate by luhn
			int cardSize = cardNumber.Length;

			//Creditcard number length must be between 13 and 16
			if (cardSize >= 13 && cardSize <= 16)
			{
				int odd = 0;
				int even = 0;
				char[] cardNumberArray = new char[cardSize];

				//Read the creditcard number into an array
				cardNumberArray = cardNumber.ToCharArray();

				//Reverse the array
				Array.Reverse(cardNumberArray, 0, cardSize);

				//Multiply every second number by two and get the sum. 
				//Get the sum of the rest of the numbers.
				for (int i = 0; i < cardSize; i++)
				{
					if (i % 2 == 0)
					{
						odd += (Convert.ToInt32(cardNumberArray.GetValue(i)) - 48);
					}
					else
					{
						int temp = (Convert.ToInt32(cardNumberArray[i]) - 48) * 2;
						//if the value is greater than 9, substract 9 from the value
						if (temp > 9)
						{
							temp = temp - 9;
						}
						even += temp;
					}
				}
				if ((odd + even) % 10 == 0)
					return true;
				else
					return false;
			}
			else
				return false;	
		}

		/// <summary>
		/// Gets the card bin number entry.
		/// This method is not intended to operate on collections as it searches the database.
		/// </summary>
		/// <returns></returns>
		public BinNumberVO GetBin()
		{
			User user = SecurityManager.GetInternalUser(credentialsToken);
			return GetBin(user.Domain.Host);	
		}

		/// <summary>
		/// Gets the card bin number entry.
		/// This method is not intended to operate on collections as it searches the database.
		/// </summary>
		/// <returns></returns>
		public BinNumberVO GetBin(string domainHost)
		{
			if (_bin == null)
			{
				Domain domain = DomainsManager.GetDomain(domainHost);
				NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
				_bin = (from bin in dc.tblCreditCardBINs where SqlMethods.Like(_cardNumber, bin.BIN + "%") select new BinNumberVO(bin)).FirstOrDefault();
			}

			return _bin;
		}

		/// <summary>
		/// Gets the card type.
		/// </summary>
		/// <returns></returns>
		public CreditCardType GetCardType(string domainHost)
		{
			if (GetBin(domainHost) == null)
			{
				if (_cardNumber.Length == 8 || _cardNumber.Length == 9)
					return CreditCardType.Isracard;
				else
					return CreditCardType.Unknown;
			}
			else
				return (CreditCardType)GetBin(domainHost).CreditCardTypeID;
		}

		/// <summary>
		/// Gets the card type.
		/// </summary>
		/// <returns></returns>
		public CreditCardType GetCardType()
		{
			if (GetBin(credentialsToken) == null)
			{
				if (_cardNumber.Length == 8 || _cardNumber.Length == 9)
					return CreditCardType.Isracard;
				else
					return CreditCardType.Unknown;
			}
			else
				return (CreditCardType)GetBin(credentialsToken).CreditCardTypeID;	
		}

		/// <summary>
		/// Gets the card payment method.
		/// </summary>
		/// <returns></returns>
		public PaymentMethodEnum GetPaymentMethod()
		{
			if (GetBin(credentialsToken) == null)
				return PaymentMethodEnum.Unknown;
			else
				return (PaymentMethodEnum)GetBin(credentialsToken).PaymentMethodID;			
		}

		public PaymentMethodEnum GetPaymentMethod(string domainHost)
		{
			if (GetBin(domainHost) == null)
				return PaymentMethodEnum.Unknown;
			else
				return (PaymentMethodEnum)GetBin(domainHost).PaymentMethodID;
		}
		
		public string GetPaymentMethodDisplay(string domainHost)
		{
			Domain domain = DomainsManager.GetDomain(domainHost);
			GlobalDataVO entry = domain.Cache.GetGlobalData(GlobalDataGroup.PaymentMethodType, Language.English, (int)GetPaymentMethod(domainHost));
			if (GetBin(domainHost) == null || entry == null)
				return Last4Digits;
			else
				return entry.Value + " .... " + Last4Digits;		
		}	

		public void SetExpirationDate(int month, int year)
		{
			ExpirationDate = new DateTime(year, month, 1);
		}

		public void SetExpirationDate(string month, string year)
		{
			int typedMonth = 1;
			int.TryParse(month, out typedMonth);
			int typedYear = 1;
			int.TryParse(year, out typedYear);			

			SetExpirationDate(typedMonth, typedYear);
		}	
	}
}
