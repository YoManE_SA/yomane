using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure;

namespace Netpay.Bll
{
    public class ApplicationIdentityModule : Infrastructure.Module
    {
        public const string ModuleName = "ApplicationIdentity";
        public override string Name { get { return ModuleName; } }
        public override decimal Version { get { return 1.0m; } }
        public static Module Current { get { return Infrastructure.Module.Get(ModuleName) as Module; } }
        protected override void OnInstall(EventArgs e)
        {
            Infrastructure.Security.SecuredObject.Create(this, ApplicationIdentity.SecuredObjectName, PermissionGroup.ReadEditDelete);
            base.OnInstall(e);
        }

        protected override void OnUninstall(EventArgs e)
        {
            Infrastructure.Security.SecuredObject.Remove(this);
            base.OnUninstall(e);
        }
    }

    public class ApplicationIdentity : BaseDataObject
    {
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(ApplicationIdentityModule.Current, SecuredObjectName); } }

        protected Netpay.Dal.Netpay.ApplicationIdentity _entity;

        public int ID { get { return _entity.ApplicationIdentity_id; } }
        public string Name { get { return _entity.Name; } set { _entity.Name = value; } }
        public string BrandName { get { return _entity.BrandName; } set { _entity.BrandName = value; } }
        public string CompanyName { get { return _entity.CompanyName; } set { _entity.CompanyName = value; } }
        public string DomainName { get { return _entity.Domain; } set { _entity.Domain = value; } }
        public string ThemeName { get { return _entity.ThemeName; } set { _entity.ThemeName = value; } }
        public string ContentFolder { get { return _entity.ContentFolder; } set { _entity.ContentFolder = value; } }
        public string URLDevCenter { get { return _entity.URLDevCenter; } set { _entity.URLDevCenter = value; } }
        public string URLProcess { get { return _entity.URLProcess; } set { _entity.URLProcess = value; } }
        public string URLMerchantCP { get { return _entity.URLMerchantCP; } set { _entity.URLMerchantCP = value; } }
        public string URLWallet { get { return _entity.URLWallet; } set { _entity.URLWallet = value; } }
        public string URLWebsite { get { return _entity.URLWebsite; } set { _entity.URLWebsite = value; } }
        public string SmtpServer { get { return _entity.SmtpServer; } set { _entity.SmtpServer = value; } }
        public string SmtpUsername { get { return _entity.SmtpUsername; } set { _entity.SmtpUsername = value; } }
        public string SmtpPassword { get { return _entity.SmtpPassword; } set { _entity.SmtpPassword = value; } }
        public string EmailFrom { get { return _entity.EmailFrom; } set { _entity.EmailFrom = value; } }
        public string EmailContactTo { get { return _entity.EmailContactTo; } set { _entity.EmailContactTo = value; } }
        public string CopyRightText { get { return _entity.CopyRightText; } set { _entity.CopyRightText = value; } }
        public bool IsActive { get { return _entity.IsActive; } set { _entity.IsActive = value; } }
        public string HashKey { get { return _entity.HashKey; } set { _entity.HashKey = value; } }
        public int? WireAccountID { get { return _entity.WireAccount_id; } set { _entity.WireAccount_id = (short?)value; } }
        public string ProcessMerchantNumber { get { return _entity.ProcessMerchantNumber; } set { _entity.ProcessMerchantNumber = value; } }

        public ApplicationIdentity() { _entity = new Dal.Netpay.ApplicationIdentity(); }

        protected ApplicationIdentity(Dal.Netpay.ApplicationIdentity entity) { _entity = entity; }
        public static ApplicationIdentity Default
        {
            get
            {
                var domain = Domain.Current;
                return domain.GetCachData("ApplicationIdentity.Default", () =>
                {
                    var ret = new ApplicationIdentity(new Dal.Netpay.ApplicationIdentity()
                    {
                        Name = "Default",
                        BrandName = domain.BrandName,
                        CompanyName = domain.LegalName,
                        Domain = domain.Host,
                        ThemeName = domain.ThemeFolder,
                        //ContentFolder = domain.ContentUrl,
                        URLDevCenter = domain.DevCentertUrl,
                        URLProcess = domain.ProcessUrl,
                        URLMerchantCP = domain.MerchantUrl,
                        URLWallet = domain.WalletUrl,
                        URLWebsite = domain.SignupUrl,
                        //SmtpServer = domain.SmtpServer,
                        //SmtpUsername = domain.SmtpUsername,
                        //SmtpPassword = domain.SmtpPassword,
                        EmailFrom = domain.MailAddressFrom,
                        EmailContactTo = domain.MailAddressTo,
                        //CopyRightText = domain.,
                        IsActive = true,
                    });
                    return new Domain.CachData(ret, DateTime.Now.AddHours(12));
                }) as ApplicationIdentity;
            }
        }

        public static ApplicationIdentity Load(int id)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var ret = (from c in DataContext.Reader.ApplicationIdentities
                       where c.ApplicationIdentity_id == id
                       select c).SingleOrDefault();
            if (ret == null) return null;
            return new ApplicationIdentity(ret);
        }

        public class AppIdentityInfo
        {
            public int ID { get; set; }
            public string Name { get; set; }
            public string VirtualImageUrl
            {
                get
                {
                    return GetLogoVirtualUrl(ID);
                }
            }

            public string RealImageUrl
            {
                get
                {
                    return GetLogoRealUrl(ID);
                }
            }

            public bool IsImageExist
            {
                get
                {
                    return IsLogoExist(RealImageUrl);
                }
            }
        }

        public static List<AppIdentityInfo> AutoComplete(string text)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            int numValue = text.ToNullableInt().GetValueOrDefault(-1);
            return (from identities in DataContext.Reader.ApplicationIdentities
                    where
                          identities.IsActive &&
                          (identities.ApplicationIdentity_id == numValue ||
                          identities.Name.Contains(text) ||
                          identities.CompanyName.Contains(text))
                    select new AppIdentityInfo()
                    {
                        ID = identities.ApplicationIdentity_id,
                        Name = identities.Name
                    })
                    .Take(5)
                    .ToList();
        }

        public static List<string> GetAppIdentitiesLogos
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                var result = (from identities in DataContext.Reader.ApplicationIdentities
                              where identities.IsActive == true
                              select new AppIdentityInfo()
                              {
                                  ID = identities.ApplicationIdentity_id,
                                  Name = identities.Name
                              })
                              .ToList()
                             .Where(x => x.IsImageExist)
                             .Select(x => x.VirtualImageUrl)
                             .ToList();

                return result;
            }
        }

        public static List<ApplicationIdentity> LoadForAffiliate(int id)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
            {
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Affiliates.Affiliate.SecuredObject, PermissionValue.Read);
            }

            return (from c in DataContext.Reader.ApplicationIdentities
                    join af in DataContext.Reader.tblAffiliatesToApplicationIdentities
                    on c.ApplicationIdentity_id equals af.ApplicationIdentity_id
                    where af.Affiliates_id == id
                    select new ApplicationIdentity(c)).ToList();
        }

        public static List<ApplicationIdentity> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var exp = (from c in DataContext.Reader.ApplicationIdentities select c);
            if (filters != null)
            {
                if (filters.IsActive.HasValue) exp = exp.Where(c => c.IsActive == filters.IsActive.Value);
                if (filters.MerchantGroups != null && filters.MerchantGroups.Count > 0)
                {
                    //Get a list of identities that supports the groups in the filter
                    var identitiesSupportGroupIds = (from identityToGroup in DataContext.Reader.ApplicationIdentityToMerchantGroups
                                                      where filters.MerchantGroups.Contains(identityToGroup.MerchantGroup_id)
                                                      select identityToGroup.ApplicationIdentity_id)
                                                      .Distinct().ToList();

                    //Get a list of identities that does not have any group, so they support all the groups exist
                    var identitiesWithAnyGroup = (from identityToGroup in DataContext.Reader.ApplicationIdentityToMerchantGroups
                                                    select identityToGroup.ApplicationIdentity_id)
                                                    .Distinct().ToList();

                    //Get the identities that are not inside the identityWithAnyGroup list, means they don't have any group, so they support ALL
                    var identitiesWithoutGroupIds = DataContext.Reader.ApplicationIdentities
                                                    .Where(x => !identitiesWithAnyGroup.Contains(x.ApplicationIdentity_id))
                                                    .Select(x => x.ApplicationIdentity_id)
                                                    .ToList();

                    //Search for identities that are inside the identitiesSupportGroupIds list or inside the identitiesWithoutGroupIds list     
                    exp = exp.Where(c => identitiesSupportGroupIds.Contains(c.ApplicationIdentity_id) || identitiesWithoutGroupIds.Contains(c.ApplicationIdentity_id));
                }
                if (filters.OnlyAppIdentitiesWithoutGroup.HasValue && filters.OnlyAppIdentitiesWithoutGroup.Value)
                {
                    //Get a list of identities that does not have any group, so they support all the groups exist
                    var identitiesWithAnyGroup = (from identityToGroup in DataContext.Reader.ApplicationIdentityToMerchantGroups
                                                  select identityToGroup.ApplicationIdentity_id)
                                                    .Distinct().ToList();

                    exp = exp.Where(c => !identitiesWithAnyGroup.Contains(c.ApplicationIdentity_id));
                }
            }

            var result = exp.ApplySortAndPage(sortAndPage).Select(n => new ApplicationIdentity(n)).ToList();
            return result;
        }

        public class SearchFilters
        {
            public bool? IsActive;
            public List<int> MerchantGroups;
            public bool? OnlyAppIdentitiesWithoutGroup;
        }

        public void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            DataContext.Writer.ApplicationIdentities.Update(_entity, (_entity.ApplicationIdentity_id != 0));
            DataContext.Writer.SubmitChanges();
        }

        public void Delete()
        {
            if (_entity.ApplicationIdentity_id == 0) return;

            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            DataContext.Writer.ApplicationIdentities.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }

        public bool EnableTokenHash
        {
            get { return !string.IsNullOrEmpty(_entity.HashKey); }
            set { _entity.HashKey = value ? Infrastructure.Security.Login.GeneratePassword() : null; }
        }

        private static Dictionary<int, ApplicationIdentity> _cache;
        public static void EnsureCache(bool forceRefresh)
        {
            lock (typeof(ApplicationIdentity))
            {
                if (forceRefresh) _cache = null;
                if (_cache == null)
                {
                    _cache = (from a in DataContext.Reader.ApplicationIdentities select new ApplicationIdentity(a)).ToDictionary(a => a.ID);
                }
            }
        }

        public static ApplicationIdentity GetIdentity(int id)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            ApplicationIdentity app;
            EnsureCache(false);
            if (_cache.TryGetValue(id, out app)) return app;
            return null;
        }

        public static ApplicationIdentity GetIdentity(Guid token)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            if (token == Guid.Empty) return Default;
            var appId = (from at in DataContext.Reader.ApplicationIdentityTokens where at.Token == token select at.ApplicationIdentity_id).FirstOrDefault();
            if (appId == 0) return null;
            EnsureCache(false);
            ApplicationIdentity app;
            if (_cache.TryGetValue(appId, out app)) return app;
            EnsureCache(true); //reload cach
            return _cache[appId];
        }

        public Guid AddToken(string name, Guid? token = null)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            if (_entity.ApplicationIdentity_id == 0) throw new Exception("wrong state, unable to save references before saving parent row");
            var newtoken = new Dal.Netpay.ApplicationIdentityToken();
            newtoken.Title = name;
            newtoken.Token = token.HasValue ? token.Value : Guid.NewGuid();
            newtoken.ApplicationIdentity_id = _entity.ApplicationIdentity_id;

            //Check if the new token exist on db - if so , don't save it
            if (DataContext.Writer.ApplicationIdentityTokens.Any
                (
                    entity => entity.Token == newtoken.Token && entity.ApplicationIdentity_id == newtoken.ApplicationIdentity_id
                ))
            {
                throw new Exception("Token already exist with the same app identity.");
            }
            
                DataContext.Writer.ApplicationIdentityTokens.InsertOnSubmit(newtoken);
                DataContext.Writer.SubmitChanges();
            
            return newtoken.Token;
        }

        public static bool IsTokenExistWithSameAppIdentity(int appidentityid, Guid token)
        {
            return DataContext.Reader.ApplicationIdentityTokens.Any
                (
                    x => x.ApplicationIdentity_id == appidentityid && x.Token == token
                );
        }

        public void RemoveToken(Guid token)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            DataContext.Writer.ApplicationIdentityTokens.DeleteAllOnSubmit((from t in DataContext.Writer.ApplicationIdentityTokens where t.ApplicationIdentity_id == ID && t.Token == token select t));
            DataContext.Writer.SubmitChanges();
        }

        public Dictionary<Guid, string> GetTokens()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            if (_entity.ApplicationIdentity_id == 0) return null;
            return (from t in DataContext.Writer.ApplicationIdentityTokens where t.ApplicationIdentity_id == _entity.ApplicationIdentity_id select t).ToDictionary(t => t.Token, t => t.Title);
        }

        public List<int> SupportedPaymentMethods
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                {
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                }

                return (from p in DataContext.Reader.ApplicationIdentityToPaymentMethods where p.ApplicationIdentity_id == ID && p.IsAllowProcess select (int)p.PaymentMethod_id).ToList();
            }
            set
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

                if (_entity.ApplicationIdentity_id == 0) throw new Exception("wrong state, unable to save references before saving parent row");
                var existing = (from cr in DataContext.Writer.ApplicationIdentityToPaymentMethods where cr.ApplicationIdentity_id == _entity.ApplicationIdentity_id select cr).ToList();
                existing.ForEach(v => v.IsAllowProcess = false);
                if (value != null)
                {
                    foreach (var item in value)
                    {
                        var jnc = existing.Where(v => v.PaymentMethod_id == (int)item).FirstOrDefault();
                        if (jnc == null)
                        {
                            DataContext.Writer.ApplicationIdentityToPaymentMethods.InsertOnSubmit(new Dal.Netpay.ApplicationIdentityToPaymentMethod()
                            {
                                IsAllowProcess = true,
                                ApplicationIdentity_id = _entity.ApplicationIdentity_id,
                                PaymentMethod_id = (short)item,
                            });
                        }
                        else
                        {
                            if (!jnc.IsAllowProcess) jnc.IsAllowProcess = true;
                            existing.Remove(jnc);
                        }
                    }
                }
                existing.RemoveAll(v => (v.IsAllowProcess || v.IsAllowCreate));
                DataContext.Writer.ApplicationIdentityToPaymentMethods.DeleteAllOnSubmit(existing);
                DataContext.Writer.SubmitChanges();
            }
        }

        public List<int> OwnedPaymentMethods
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return (from p in DataContext.Reader.ApplicationIdentityToPaymentMethods where p.ApplicationIdentity_id == ID && p.IsAllowCreate select (int)p.PaymentMethod_id).ToList();
            }
            set
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);


                if (_entity.ApplicationIdentity_id == 0) throw new Exception("wrong state, unable to save references before saving parent row");
                var existing = (from cr in DataContext.Writer.ApplicationIdentityToPaymentMethods where cr.ApplicationIdentity_id == _entity.ApplicationIdentity_id select cr).ToList();
                existing.ForEach(v => v.IsAllowCreate = false);
                if (value != null)
                {
                    foreach (var item in value)
                    {
                        var jnc = existing.Where(v => v.PaymentMethod_id == (int)item).FirstOrDefault();
                        if (jnc == null)
                        {
                            DataContext.Writer.ApplicationIdentityToPaymentMethods.InsertOnSubmit(new Dal.Netpay.ApplicationIdentityToPaymentMethod()
                            {
                                IsAllowCreate = true,
                                ApplicationIdentity_id = _entity.ApplicationIdentity_id,
                                PaymentMethod_id = (short)item,
                            });
                        }
                        else
                        {
                            jnc.IsAllowCreate = true;
                            existing.Remove(jnc);
                        }
                    }
                }
                existing.RemoveAll(v => (v.IsAllowProcess || v.IsAllowCreate));
                DataContext.Writer.ApplicationIdentityToPaymentMethods.DeleteAllOnSubmit(existing);
                DataContext.Writer.SubmitChanges();
            }
        }

        public List<string> SupportedCurrencies
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return (from p in DataContext.Reader.ApplicationIdentityToCurrencies where p.ApplicationIdentity_id == ID select p.CurrencyISOCode).ToList();
            }
            set
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

                if (_entity.ApplicationIdentity_id == 0) throw new Exception("wrong state, unable to save references before saving parent row");
                DataContext.Writer.ApplicationIdentityToCurrencies.DeleteAllOnSubmit((from cr in DataContext.Writer.ApplicationIdentityToCurrencies where cr.ApplicationIdentity_id == _entity.ApplicationIdentity_id select cr));
                if (value != null)
                {
                    foreach (var item in value)
                    {
                        var jnc = new Dal.Netpay.ApplicationIdentityToCurrency();
                        jnc.ApplicationIdentity_id = ID;
                        jnc.CurrencyISOCode = item;
                        DataContext.Writer.ApplicationIdentityToCurrencies.InsertOnSubmit(jnc);
                    }
                }
                DataContext.Writer.SubmitChanges();
            }
        }

        public static int? GetPaymentMethodsOwner(int pmId)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from p in DataContext.Reader.ApplicationIdentityToPaymentMethods where p.PaymentMethod_id == pmId && p.IsAllowCreate select (int?)p.ApplicationIdentity_id).FirstOrDefault();
        }

        public List<int> RelatedMerchantGroups
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return (from p in DataContext.Reader.ApplicationIdentityToMerchantGroups where p.ApplicationIdentity_id == ID select p.MerchantGroup_id).ToList();
            }
            set
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

                if (_entity.ApplicationIdentity_id == 0) throw new Exception("wrong state, unable to save references before saving parent row");
                DataContext.Writer.ApplicationIdentityToMerchantGroups.DeleteAllOnSubmit((from cr in DataContext.Writer.ApplicationIdentityToMerchantGroups where cr.ApplicationIdentity_id == _entity.ApplicationIdentity_id select cr));
                if (value != null)
                {
                    foreach (var item in value)
                    {
                        var jnc = new Dal.Netpay.ApplicationIdentityToMerchantGroup();
                        jnc.ApplicationIdentity_id = ID;
                        jnc.MerchantGroup_id = item;
                        DataContext.Writer.ApplicationIdentityToMerchantGroups.InsertOnSubmit(jnc);
                    }
                }
                DataContext.Writer.SubmitChanges();
            }
        }

        public List<int> Affiliates
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return (from p in DataContext.Reader.tblAffiliatesToApplicationIdentities where p.ApplicationIdentity_id == ID select p.Affiliates_id).ToList();
            }
            set
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

                if (_entity.ApplicationIdentity_id == 0) throw new Exception("wrong state, unable to save references before saving parent row");
                DataContext.Writer.tblAffiliatesToApplicationIdentities.DeleteAllOnSubmit((from cr in DataContext.Writer.tblAffiliatesToApplicationIdentities where cr.ApplicationIdentity_id == _entity.ApplicationIdentity_id select cr));
                if (value != null)
                {
                    foreach (var item in value)
                    {
                        var jnc = new Dal.Netpay.tblAffiliatesToApplicationIdentity();
                        jnc.ApplicationIdentity_id = ID;
                        jnc.Affiliates_id = item;
                        DataContext.Writer.tblAffiliatesToApplicationIdentities.InsertOnSubmit(jnc);
                    }
                }
                DataContext.Writer.SubmitChanges();
            }
        }

        public string GetContent(string contentName)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            string fileName = System.IO.Path.Combine(PrivateDirectoryPath, contentName);
            if (!System.IO.File.Exists(fileName)) return null;
            return System.IO.File.ReadAllText(fileName);
        }

        public string PrivateDirectoryPath
        {
            get
            {
                return System.IO.Path.Combine(Domain.Current.MapPrivateDataPath("AppIdentityFiles"), _entity.ApplicationIdentity_id.ToString());
            }
        }

        public string PublicDirectoryPath
        {
            get
            {
                return System.IO.Path.Combine(Domain.Current.MapPublicDataPath("AppIdentityFiles"), _entity.ApplicationIdentity_id.ToString());
            }
        }

        public string PublicVirtualDirectoryPath
        {
            get
            {
                return System.IO.Path.Combine(Domain.Current.MapPublicDataVirtualPath("AppIdentityFiles/") , _entity.ApplicationIdentity_id.ToString());
            }
        }

        public static string GetLogoVirtualUrl(int id)
        {
            return System.IO.Path.Combine(Domain.Current.MapPublicDataVirtualPath("AppIdentityFiles/"), id.ToString() + "/", id.ToString() + ".png");
        }

        public static string GetLogoRealUrl(int id)
        {
            return System.IO.Path.Combine(Domain.Current.MapPublicDataPath("AppIdentityFiles"), id.ToString(), id.ToString() + ".png");
        }

        public static bool IsLogoExist(string logoUrl)
        {
            return System.IO.File.Exists(logoUrl);
        }
        
        public string GetAppIdentityLogoPath
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return System.IO.Path.Combine(PublicDirectoryPath, _entity.ApplicationIdentity_id.ToString() + ".png");
            }
        }

        public string GetAppIdentityVirtualLogoPath
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return System.IO.Path.Combine(PublicVirtualDirectoryPath + "/", _entity.ApplicationIdentity_id.ToString() + ".png");
            }
        }

        public bool IsAppIdentityHaveLogo
        {
            get
            {
                return System.IO.File.Exists(GetAppIdentityLogoPath);
            }
        }

        public void SendEmail(string from, string subject, string body)
        {
            Infrastructure.Email.SmtpClient.Send(new System.Net.Mail.MailAddress(from), new System.Net.Mail.MailAddress(EmailContactTo), subject, body);
        }

        public static bool IsAffiliateIssuedWithOtherAppIdentity(int affiliateid, int appidentityid)
        {
            return DataContext.Reader.tblAffiliatesToApplicationIdentities.Any(x =>
            x.Affiliates_id == affiliateid && x.ApplicationIdentity_id != appidentityid);
        }

        public static List<int> GetAffiliateAppIdentitiesIdsList(int affiliateid)
        {
            if (affiliateid <= 0) return null;

            return DataContext.Reader.tblAffiliatesToApplicationIdentities
                .Where(x => x.Affiliates_id == affiliateid)
                .Select(x => x.ApplicationIdentity_id)
                .ToList();
        }

        public static string GetAppIdentityNames(List<int> ids)
        {
            if (ids.Count <= 0) return null;

            var names = DataContext.Reader.ApplicationIdentities
                .Where(x => ids.Contains(x.ApplicationIdentity_id))
                .Select(x => x.Name)
                .ToList();

            return string.Join(",", names);
        }
    }
}
