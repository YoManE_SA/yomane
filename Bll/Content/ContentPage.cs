﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Content
{
	public class StaticFiles : ContentType
	{
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Content.Module.Current, SecuredObjectName); } }

        public class SearchFilters : ContentType { }
		public string Title { get; set; }
		public string HtmlData { get; set; }
		public void WriteFile()
		{ 
		}

		public static string MapPath(ContentType type)
		{
			string basePath = null;
			if (type.Source == UserRole.Merchant) // Come from merchants
            {
				if (type.IsPublic) basePath = Accounts.Account.MapPublicPath(type.SourceID, null);
                else basePath = Accounts.Account.MapAccountContentPrivatePath(type.SourceID.ToNullableInt().GetValueOrDefault(), null);
			}
            else if (type.Source == UserRole.Admin)//Come from app identity
            {
				basePath = Accounts.Account.MapAppIdentityContentPrivatePath(type.SourceID.ToNullableInt().GetValueOrDefault(), null);
            }
			if (!string.IsNullOrEmpty(type.FileName)) basePath = System.IO.Path.Combine(basePath, type.FileName);
			return basePath;
		}

        public static bool IsExist(ContentType type)
        {
            string fileName = MapPath(type);
            return System.IO.File.Exists(fileName);
        }

        public static List<ContentType> Search(SearchFilters filters, Infrastructure.ISortAndPage sortAndPage)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            if (filters == null) throw new Exception("filters can't be null");
			string fileName = filters.FileName;
			filters.FileName = null;
			var basePath = MapPath(filters);

            if ((basePath != "/")&&(System.IO.Directory.Exists(basePath)))
            {
                var result = System.IO.Directory.GetFiles(basePath).Select(s => new ContentType() { FileName = System.IO.Path.GetFileName(s), IsPublic = filters.IsPublic, Source = filters.Source, SourceID = filters.SourceID }).ToList();
                return result;
            }
           
            return new List<ContentType>() { };
		}

		private StaticFiles(string fileName)
		{
			var reader = new System.Xml.XmlDocument();
			reader.Load(fileName);
			Title = reader.DocumentElement.GetElementsByTagName("Title")[0].InnerText;
			HtmlData = reader.DocumentElement.GetElementsByTagName("HtmlData")[0].InnerText;
		}

		public StaticFiles() { }

		public static StaticFiles Load(ContentType type)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var basePath = MapPath(type);
			if (!System.IO.File.Exists(basePath)) return null;
			return new StaticFiles(basePath) {FileName=type.FileName,SourceID=type.SourceID,IsPublic=type.IsPublic,Source=type.Source};
		}

		public void Save()
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            var doc = new System.Xml.XmlDocument();
			doc.AppendChild(doc.CreateElement("Content"));
			var el = doc.CreateElement("Title"); el.InnerText = Title;
			doc.DocumentElement.AppendChild(el);
			el = doc.CreateElement("HtmlData"); el.AppendChild(doc.CreateCDataSection(HtmlData));
			doc.DocumentElement.AppendChild(el);

            //Meir - check if file path exist
            string path = MapPath(this);
            if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(path)))
                    System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(path));

            doc.Save(path);
		}

		public void Delete()
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            string fileName = MapPath(this);
			if (System.IO.File.Exists(fileName))
				System.IO.File.Delete(fileName);
		}
	}
}
