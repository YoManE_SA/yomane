﻿using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Netpay.Bll.Content
{
    public class DeveloperDoc
    {
        public string Url { get; set; }
        public string FileName { get; set; }
        public DateTime LastUpdated { get; set; }
        public string Size { get; set; }

        public static string DocsPath 
        {
            get 
            {
                string path = Domain.Current.DataPath + "Public\\DevDocs\\";
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                return path;
            }
        }

        public static string DocsUrl
        {
            get
            {
                return Domain.Current.CalcPublicDataVirtualPath + "DevDocs/";
            }
        }

        public static string DocsArchivePath
        {
            get
            {
                string path = DocsPath + "Archive\\";
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                return path;
            }
        }

        public class Filters
        {
            public int? Take { get; set; }
            public List<string> FileNames { get; set; }
        }

        /// <summary>
        /// Zipped files
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        public static byte[] GetZipped(Filters filters)
        {
            string[] filesPaths = Directory.GetFiles(DocsPath, "*.pdf", SearchOption.TopDirectoryOnly);
            List<FileInfo> infos = new List<FileInfo>();
            for (int idx = 0; idx < filesPaths.Length; idx++)
                infos.Add(new FileInfo(filesPaths[idx]));
            infos = infos.OrderBy(i => i.Name).ToList();
            if (filters != null)
            {
                if (filters.FileNames != null)
                    infos = infos.Where(inf => filters.FileNames.Any(fn => inf.Name.StartsWith(fn))).ToList();
                if (filters.Take != null)
                    infos = infos.Take(filters.Take.Value).ToList();
            }

            byte[] archive = Infrastructure.Utils.Compress(infos);
            return archive;
        }

        /// <summary>
        /// Cached zip file
        /// The cached zip files are cleared when the dev docs generator runs
        /// </summary>
        /// <param name="filters"></param>
        /// <param name="cachedFileName"></param>
        /// <returns></returns>
        public static byte[] GetZipped(Filters filters, string cachedFileName)
        {
            if (string.IsNullOrEmpty(cachedFileName))
                throw new ArgumentException("Invalid file name");

                string cachedFilePath = DocsPath + cachedFileName;
                if (!File.Exists(cachedFilePath))
                {
                    lock (typeof(DeveloperDoc))
                    {
                        if (!File.Exists(cachedFilePath))
                        {
                            byte[] zipFile = GetZipped(filters);
                            File.WriteAllBytes(cachedFilePath, zipFile);
                        }
                    }
                }
                
                return File.ReadAllBytes(cachedFilePath);
        }

        public static List<DeveloperDoc> Get(Filters filters) 
        {
            string[] filesPaths = Directory.GetFiles(DocsPath, "*.pdf", SearchOption.TopDirectoryOnly);
            List<FileInfo> infos = new List<FileInfo>();
            for (int idx = 0; idx < filesPaths.Length; idx++)
                infos.Add(new FileInfo(filesPaths[idx]));
            infos = infos.OrderBy(i => i.Name).ToList();
            if (filters != null)
            {
                if (filters.FileNames != null)
                    infos = infos.Where(inf => filters.FileNames.Any(fn => inf.Name.StartsWith(fn))).ToList();
                if (filters.Take != null)
                    infos = infos.Take(filters.Take.Value).ToList(); 
            }

            List<DeveloperDoc> virtualFiles = new List<DeveloperDoc>();
            foreach (FileInfo info in infos)
            {
                DeveloperDoc docFile = new DeveloperDoc();
                docFile.LastUpdated = info.LastWriteTime;
                docFile.Url = DocsUrl + info.Name;
                docFile.FileName = info.Name;
                docFile.Size = info.Length.ToFileSize();
                virtualFiles.Add(docFile);
            }

            return virtualFiles;
        }

        public static void Save(string fileName, byte[] data)
        {
            string filePath = DocsPath + fileName;
            File.WriteAllBytes(filePath, data);
        }

        public static void Delete(string fileName)
        {
            string filePath = DocsPath + fileName;
            File.Delete(filePath);
        }
    }
}
