﻿using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Netpay.Bll.Content
{
    public class AdminDocs
    {
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Content.AdminDocsMng.Module.Current, SecuredObjectName); } }


        public static string DocsPath 
        {
            get 
            {
                string path = Domain.Current.DataPath + "Public\\AdminDocs\\";
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                return path;
            }
        }

        public static string DocsUrl
        {
            get
            {                   
                return Domain.Current.CalcPublicDataVirtualPath + "AdminDocs/";
            }
        }

        private static string DocsArchivePath
        {
            get
            {
                string path = DocsPath + "Archive\\";
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                return path;
            }
        }

        public class DocFile 
        {
            public string LocalPath { get; set; }
            public string Url { get; set; }
            public string FileName { get; set; }
            public DateTime LastUpdated { get; set; }
            public string Size { get; set; }
        }

        public static List<DocFile> GetDocs(int? take) 
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            string[] filesPaths = Directory.GetFiles(DocsPath, "*.pdf", SearchOption.TopDirectoryOnly);
            List<FileInfo> infos = new List<FileInfo>();
            for (int idx = 0; idx < filesPaths.Length; idx++)
                infos.Add(new FileInfo(filesPaths[idx]));
            infos = infos.OrderByDescending(i => i.LastWriteTime).ToList();
            if (take != null)
                infos = infos.Take(take.Value).ToList();

            List<DocFile> virtualFiles = new List<DocFile>();
            foreach (FileInfo info in infos)
            {
                DocFile docFile = new DocFile();
                docFile.LastUpdated = info.LastWriteTime;
                docFile.LocalPath = DocsPath + info.Name;
                docFile.Url = DocsUrl + info.Name;
                docFile.FileName = info.Name;
                docFile.Size = info.Length.ToFileSize();
                virtualFiles.Add(docFile);
            }

            return virtualFiles;
        }

        public static void SaveDoc(string fileName, byte[] data)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            string filePath = DocsPath + fileName;
            if (File.Exists(filePath))
                ArchiveDoc(fileName);

            File.WriteAllBytes(filePath, data);
        }

        public static void DeleteDoc(string fileName)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            ArchiveDoc(fileName); 
        }

        private static void ArchiveDoc(string fileName)
        {
            string filePath = DocsPath + fileName;
            if (File.Exists(filePath))
            {
                string guid = Guid.NewGuid().ToString();
                string fileArchivePath = DocsArchivePath + guid + "_" + fileName;
                File.Move(filePath, fileArchivePath);
            }
        }
    }
}
