﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.Content
{
	public class ContentType
	{
		public string FileName { get; set; }
		public bool IsPublic { get; set; }
		public Infrastructure.Security.UserRole? Source { get; set; }
		public string SourceID { get; set; }

		private static List<ContentType> _knownTypes;
		public static List<ContentType> KnownTypes
		{
			get
			{
				if (_knownTypes == null) _knownTypes = new List<ContentType>();
				return _knownTypes;
			}
		}
	}

}
