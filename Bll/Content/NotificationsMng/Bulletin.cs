﻿using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.Content
{
    public class Bulletin
    {
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Content.NotificationsMng.Module.Current, SecuredObjectName); } }

        public const string TypeGeneralInfo = "GEN_INFO";
        public const string TypeSystemInfo = "SYS_INFO";

        private Dal.Netpay.SolutionBulletin _entity;

        public Bulletin()
        {
            _entity = new Dal.Netpay.SolutionBulletin();
            _entity.BulletinType = TypeGeneralInfo;
            _entity.IsVisible = false;
            _entity.MessageDate = DateTime.Now;
            _entity.MessageExpirationDate = DateTime.Now.AddMonths(3);
            _entity.SolutionList_id = (byte)Solution.MerchantControlPanel;
        }

        internal Bulletin(Dal.Netpay.SolutionBulletin entity)
        {
            _entity = entity;
        }

        public int ID { get { return _entity.SolutionBulletin_id; } }
        public byte? SolutionID { get { return _entity.SolutionList_id; } set { _entity.SolutionList_id = value; } }
        public string Text { get { return _entity.MessageText; } set { _entity.MessageText = value; } }
        public DateTime InsertDate { get { return _entity.MessageDate; } set { _entity.MessageDate = value; } }
        public DateTime ExpirationDate
        {
            get
            {
                return _entity.MessageExpirationDate == null ? _entity.MessageDate.AddMonths(3) : _entity.MessageExpirationDate.Value;
            }
            set { _entity.MessageExpirationDate = value; }
        }

        public string SolutionName
        {
            get
            {
                if (SolutionID.HasValue)
                {
                    return ((Solution)SolutionID).ToString();
                }
                else
                {
                    return "";
                }
            }
        }

        public bool IsVisible { get { return _entity.IsVisible; } set { _entity.IsVisible = value; } }
        public string Type { get { return _entity.BulletinType; } set { _entity.BulletinType = value; } }
        public string TypeFriendlyName
        {
            get
            {
                switch (Type)
                {
                    case Bulletin.TypeGeneralInfo:
                        return "General Info";
                    case Bulletin.TypeSystemInfo:
                        return "System Info";
                    default:
                        return "Not defined";
                }
            }
        }

        public class SearchFilters
        {
            public int? ID;
            public Solution? Solution;
            public byte? SolutionId;
            public Range<DateTime>? InsertDate;
            public bool? IncludeExpired;
            public bool? OnlyNotExpired;
        }

        public static List<KeyValuePair<byte, string>> GetSolutions()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var solutions = (from s in DataContext.Reader.SolutionLists select new KeyValuePair<byte, string>(s.SolutionList_id, s.Name)).ToList();
            return solutions;
        }

        public static Bulletin Load(int id)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var entity = (from b in DataContext.Writer.SolutionBulletins where b.SolutionBulletin_id == id select b).SingleOrDefault();
            if (entity == null)
                return null;

            return new Bulletin(entity);
        }

        public void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            if (ID == 0)
                DataContext.Writer.SolutionBulletins.InsertOnSubmit(_entity);
            DataContext.Writer.SubmitChanges();
        }

        public void Delete()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            if (ID == 0)
                return;
            DataContext.Writer.SolutionBulletins.DeleteOnSubmit(_entity);
            DataContext.Writer.SubmitChanges();
        }

        public static List<Bulletin> GetAdminFeesNotifications()
        {
            var sf = new SearchFilters();
            sf.Solution = Solution.AdminFees;
            return Search(sf, null);
        }

        public static List<Bulletin> GetAdminNotifications()
        {
            var sf = new SearchFilters();
            sf.Solution = Solution.Admin;
            return Search(sf, null);
        }

        public static List<Bulletin> Search(SearchFilters filters, SortAndPage sp)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var expression = from sb in DataContext.Reader.SolutionBulletins where sb.SolutionList_id.HasValue select sb;
            if (filters != null) 
            {
                if (filters.ID != null)
                    expression = expression.Where(sb => sb.SolutionBulletin_id == filters.ID.Value);
                if (filters.Solution != null)
                    expression = expression.Where(sb => sb.SolutionList_id == (byte)filters.Solution.Value);
                if (filters.SolutionId != null)
                    expression = expression.Where(sb => sb.SolutionList_id == filters.SolutionId);
                if (filters.InsertDate != null)
                    expression = expression.Where(sb => sb.MessageDate >= filters.InsertDate.Value.From && sb.MessageDate <= filters.InsertDate.Value.To);
                if (filters.IncludeExpired != null && !filters.IncludeExpired.Value)
                    expression = expression.Where(sb => sb.MessageExpirationDate <= DateTime.Now);
                if (filters.OnlyNotExpired.HasValue && filters.OnlyNotExpired.Value)
                    expression = expression.Where(sb => sb.MessageExpirationDate >= DateTime.Now);
            }

            if (sp != null)
                expression = expression.ApplySortAndPage(sp);

            List<Bulletin> bulletins = expression.Select(sb => new Bulletin(sb)).ToList();
            return bulletins;
        }
    }
}
