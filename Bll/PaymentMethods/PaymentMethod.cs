using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.PaymentMethods
{
	public class PaymentMethod : BaseDataObject
	{
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Module.Current, SecuredObjectName); } }
        
        public class SearchFilters
		{
			public Infrastructure.Range<int?> ID;
			public int? Group;
			public int? Type;
			public bool? IsPopular;
		}

		private Dal.Netpay.PaymentMethod _entity;

		public int ID { get { return (int)_entity.PaymentMethod_id; } set { _entity.PaymentMethod_id = (short)value; } }
		public string Name { get { return _entity.Name; } set { _entity.Name = value; } }
		public string Abbreviation { get { return _entity.Abbreviation; } set { _entity.Abbreviation = value; } }

		public PaymentMethodGroupEnum Group { get { return (PaymentMethodGroupEnum)_entity.PaymentMethodGroup_id.GetValueOrDefault(); } set { _entity.PaymentMethodGroup_id = (byte)value; } }
		public PaymentMethodType Type { get { return (PaymentMethodType)_entity.pm_Type.GetValueOrDefault((int)PaymentMethodType.Unknown); } set { _entity.pm_Type = (int)value; } }
		//public byte? PaymentMethodType_id { get { return _entity.PaymentMethodType_id; } set { _entity.PaymentMethodType_id = value; } }

		public bool IsPopular { get { return _entity.IsPopular; } set { _entity.IsPopular = value; } }
		public bool IsBillingAddressMandatory { get { return _entity.IsBillingAddressMandatory; } set { _entity.IsBillingAddressMandatory = value; } }
		public bool IsPull { get { return _entity.IsPull; } set { _entity.IsPull = value; } }
		public bool IsPMInfoMandatory { get { return _entity.IsPMInfoMandatory; } set { _entity.IsPMInfoMandatory = value; } }
		public bool IsTerminalRequired { get { return _entity.IsTerminalRequired; } set { _entity.IsTerminalRequired = value; } }
		public bool IsExpirationDateMandatory { get { return _entity.IsExpirationDateMandatory; } set { _entity.IsExpirationDateMandatory = value; } }
		public bool IsPersonalIDRequired { get { return _entity.IsPersonalIDRequired; } set { _entity.IsPersonalIDRequired = value; } }

		public string Value1EncryptedCaption { get { return _entity.Value1EncryptedCaption; } set { _entity.Value1EncryptedCaption = value; } }
		public string Value2EncryptedCaption { get { return _entity.Value2EncryptedCaption; } set { _entity.Value2EncryptedCaption = value; } }
		public string Value1EncryptedValidationRegex { get { return _entity.Value1EncryptedValidationRegex; } set { _entity.Value1EncryptedValidationRegex = value; } }
		public string Value2EncryptedValidationRegex { get { return _entity.Value2EncryptedValidationRegex; } set { _entity.Value2EncryptedValidationRegex = value; } }
        public string BaseBin
        {
            get
            {
                if (IsPrepaidGroup)
                {
                    var result = DataContext.Reader.tblCreditCardBINs
                        .Where(bin => bin.PaymentMethod == _entity.PaymentMethod_id)
                        .Select(bin => bin.BIN)
                        .ToList();
                    if (result.Count == 1) return result.First();
                    else throw new Exception("more then one bin found for payment method:" + _entity.PaymentMethod_id);
                }
                else
                {
                    return _entity.BaseBIN;
                }
            }
            set
            {
                _entity.BaseBIN = value;
            }
        }

        public bool IsPrepaid
        {
            get
            {
                return (_entity.PaymentMethod_id >= (short)PaymentMethodEnum.PP_MIN && _entity.PaymentMethod_id <= (short)PaymentMethodEnum.PP_MAX || _entity.PaymentMethod_id == 29);
            }
        }

        public bool IsPrepaidGroup
        {
            get
            {
                return IsPrepaid && string.IsNullOrEmpty(_entity.BaseBIN);
            }
        }

        public bool IsPrepaidMethod
        {
            get
            {
                return IsPrepaid && !string.IsNullOrEmpty(_entity.BaseBIN);
            }
        }

        public int? PendingKeepAliveMinutes { get { return _entity.PendingKeepAliveMinutes; } set { _entity.PendingKeepAliveMinutes = value; } }

        public short? PendingCleanUpDays { get { return _entity.PendingCleanupDays; } set { _entity.PendingCleanupDays = value; } }

		public virtual ValidationResult ValidateAccountValue1(string value) 
		{
			if (!string.IsNullOrEmpty(Value1EncryptedValidationRegex))
				if (!System.Text.RegularExpressions.Regex.Match(value, Value1EncryptedValidationRegex).Success)
					return ValidationResult.Invalid_AccountValue1;
			return ValidationResult.Success;
		}

		public virtual ValidationResult ValidateAccountValue2(string value) 
		{
            if (!string.IsNullOrEmpty(Value2EncryptedValidationRegex))
                if (!System.Text.RegularExpressions.Regex.Match(value, Value2EncryptedValidationRegex).Success)
                {
                    if (!Infrastructure.Application.IsProduction)
                        Logger.Log("Could not match '"+ value + "' against '"+ Value2EncryptedValidationRegex + "'");
                    return ValidationResult.Invalid_AccountValue2;
                }
			return ValidationResult.Success;
		}

        // A public constructor , used in DataManager menu in order to add a new Prepaid Payment method item.
        public PaymentMethod()
        {
            _entity = new Dal.Netpay.PaymentMethod();
        }

		protected PaymentMethod(Dal.Netpay.PaymentMethod entity)
		{ 
			_entity = entity;
		}

		public static List<PaymentMethod> Cache
		{
			get
			{
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                var items = Domain.Current.GetCachData("PaymentMethod", () => {
                    return new Domain.CachData((from c in DataContext.Reader.PaymentMethods select new PaymentMethod(c)).ToList(), DateTime.Now.AddHours(6));
                }) as List<PaymentMethod>;

                //if the user is not authorized to see prepaid filter except of prepaid
                return items;
			}
		}

        public static List<PaymentMethod> CachePrepaid
        {
            //Check for prepaid security issue.

            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Prepaid.Module.SecuredObject, PermissionValue.Read);

                return Cache.Where(i => i.Group == PaymentMethodGroupEnum.Prepaid).ToList();
            }
        }

        public class CurrencyCountry
		{
			public CurrencyCountry(Currency currency, Country country)
			{
				this.Currency = currency;
				this.Country = country;
			}
			public Currency Currency { get; set; }
			public Country Country { get; set; }
		}

		private List<CurrencyCountry> _currencyCountry;
		public List<CurrencyCountry> CountriesAndCurrencies
		{
			get
			{
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                if (_currencyCountry == null) _currencyCountry = DataContext.Reader.PaymentMethodToCountryCurrencies.Where(c => c.PaymentMethod_id == _entity.PaymentMethod_id).Select(c => new CurrencyCountry(Currency.Get(c.Currency_id.GetValueOrDefault()), Country.Get(c.Country_id.GetValueOrDefault()))).ToList();
				return _currencyCountry;
			}
		}

		public List<Country> Countries { get { return CountriesAndCurrencies.Where(c => c.Country != null).Select(c=> c.Country).ToList(); } }
		public List<Currency> Currencies { get { return CountriesAndCurrencies.Where(c => c.Currency != null).Select(c=> c.Currency).ToList(); } }

		public static PaymentMethod Get(int id)
		{
			return Cache.Where(c => c.ID == (short)id).SingleOrDefault();
		}

		public static PaymentMethod Get(CommonTypes.PaymentMethodEnum id)
		{
			return Cache.Where(c => c.ID == (short)id).SingleOrDefault();
		}

		public static PaymentMethod Get(string abbreviation)
		{
			return Cache.Where(c => c.Abbreviation == abbreviation).SingleOrDefault();
		}

		public static PaymentMethod Load(CommonTypes.PaymentMethodEnum id)
		{
			return Search(new SearchFilters() { ID = new Infrastructure.Range<int?>((int)id) }, null).FirstOrDefault();
		}

		public static List<PaymentMethod> Search(SearchFilters filters, ISortAndPage sortAndPage)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var expression = from p in DataContext.Reader.PaymentMethods select p;
			if(filters != null)
			{
				if (filters.ID.From != null) expression = expression.Where(p => p.PaymentMethod_id >= filters.ID.From);
				if (filters.ID.To != null) expression = expression.Where(p => p.PaymentMethod_id <= filters.ID.To);

				if (filters.Group != null) expression = expression.Where(p => p.PaymentMethodGroup_id == filters.Group);
				if (filters.Type != null) expression = expression.Where(p => p.pm_Type == filters.Type);
				if (filters.IsPopular != null) expression = expression.Where(p => p.IsPopular == filters.IsPopular);
			}
			return expression.ApplySortAndPage(sortAndPage).AsEnumerable().Select(p => new PaymentMethod(p)).ToList();

		}

		public static List<Country> GetPaymentMethodCountries(int? currencyID, PaymentMethodType[] paymentMethodType, PaymentMethodGroupEnum[] paymentMethodGroup)
		{
			IEnumerable<PaymentMethod> pMethods = Cache.ToArray();
			if (paymentMethodType != null) pMethods = pMethods.Where(pm => paymentMethodType.Contains(pm.Type));
			if (paymentMethodGroup != null) pMethods = pMethods.Where(pm => paymentMethodGroup.Contains(pm.Group));
			List<CurrencyCountry> pmcc = null;

            if (currencyID != null) pmcc = pMethods.SelectMany(pm => pm.CountriesAndCurrencies).Where(cc => cc.Currency == null || cc.Currency.ID == currencyID).ToList();
            else pmcc = pMethods.SelectMany(pm => pm.CountriesAndCurrencies).ToList();

			if (pmcc.Exists(cc => cc.Country == null)) return Country.Cache.ToList();
			return pmcc.Select(p => p.Country).Distinct().ToList();
		}

		public static List<PaymentMethod> GetCountryPaymentMethods(int countryID, int? currencyID, PaymentMethodType[] paymentMethodType, PaymentMethodGroupEnum[] paymentMethodGroup)
		{
			var exp = PaymentMethod.Cache.Where(pm => pm.Countries.Exists(cc => cc.ID == countryID));
			if (paymentMethodType != null) exp = exp.Where(pm => paymentMethodType.Contains(pm.Type));
			if (paymentMethodGroup != null) exp = exp.Where(pm => paymentMethodGroup.Contains(pm.Group));
			if (currencyID != null) exp = exp.Where(pm => pm.Currencies.Exists(cc => cc.ID == currencyID));
			return exp.ToList();
		}

        public void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            DataContext.Writer.PaymentMethods.Update(_entity, !(_entity.Name == null || _entity.Name == ""));
            DataContext.Writer.SubmitChanges();
        }

        //A new save function for prepaid paymentmethod that has assumption point
        //That a new item will have id with value of 0 and existing item will have id different from 0
        public void SavePrePaidItem(bool rowexist)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Prepaid.Module.SecuredObject, PermissionValue.Edit);

            DataContext.Writer.PaymentMethods.Update(_entity, rowexist);
            DataContext.Writer.SubmitChanges();
        }

        //Function that gets the biggest ID of PaymentMethod from Prepaid group
        //And returns a value bigger in 1.
        public static int GetNewPrepaidPaymentMethodIdValue()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Prepaid.Module.SecuredObject, PermissionValue.Add);

            //Check if there is an empty place in the range of 5000-20000
            if (DataContext.Reader.PaymentMethods.Any(x => x.PaymentMethod_id == ((int)PaymentMethodEnum.PP_MAX)-1)) return -1; // No empty values


            //Check if any value exist in the range of the Prepaid Payment Methods.
            else if (!DataContext.Reader.PaymentMethods.Any(x => (x.PaymentMethod_id > (int)PaymentMethodEnum.PP_MIN && x.PaymentMethod_id < (int)PaymentMethodEnum.PP_MAX)))
            {
                return (int)PaymentMethodEnum.PP_MIN + 1;
            }
            
            else 
            {
                return DataContext.Reader.PaymentMethods.Where(x => (x.PaymentMethod_id > (int)PaymentMethodEnum.PP_MIN && x.PaymentMethod_id < (int)PaymentMethodEnum.PP_MAX)).Max(x => x.PaymentMethod_id) + 1;
            }
                
            //TBD - check the case that all values were added and after that values were deleted. so there are empty places 
            //Between 5000 and 20000
        }

        public static bool IsIdExist(int id)
        {
            return (DataContext.Reader.PaymentMethods.Any(x => x.PaymentMethod_id == id));
        }


        //**********************************************//
        //Uploading icon for paymentmethod implementation:
        public static string MapPublicOffset(string prepaidPaymentmethodId, string fileName)
        {
            string path = "PrepaidIcons/";
            if (prepaidPaymentmethodId != null) path = System.IO.Path.Combine(path, prepaidPaymentmethodId + "/");
            if (fileName != null) path = System.IO.Path.Combine(path, fileName);
            return path;
        }

        public static string MapPublicPath(string prepaidPaymentmethodId, string fileName)
        {
            return Domain.Current.MapPublicDataPath(MapPublicOffset(prepaidPaymentmethodId, fileName));
        }

        public string MapPublicOffset(string fileName)
        {
            return MapPublicOffset(ID.ToString(), fileName);
        }

        public string MapPublicPath(string fileName = null)
        {
            return MapPublicPath(ID.ToString(), fileName);
        }

    }
}
