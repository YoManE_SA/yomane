﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.PaymentMethods
{
    public class CreditCard : MethodData
    {
        public CreditCard(IStoredMethod data) : base(data)
        {
            if (data.PaymentMethodId == (int)CommonTypes.PaymentMethodEnum.Unknown)
                data.PaymentMethodId = (int)CommonTypes.PaymentMethodEnum.CCUnknown;
            IgnoreValue2Validation = true;
        }


        public CreditCard(CommonTypes.PaymentMethodEnum pmId) : this(new StoredMethod() { PaymentMethodId = (int)pmId }) { }
        public CreditCard() : this(CommonTypes.PaymentMethodEnum.CCUnknown) { }

        public static CreditCard FromCardNumber(string cardNumber)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            var pmId = GetPaymentMethodId(cardNumber);
            return new CreditCard(new StoredMethod() { PaymentMethodId = (int)pmId }) { CardNumber = cardNumber };
        }

        public override void GetDecryptedData(out string value1, out string value2)
        {
            base.GetDecryptedData(out value1, out value2);
            value2 = Infrastructure.Security.Encryption.DecodeCvv(value2);
        }

        public override void SetEncryptedData(string value1, string value2)
        {
            value2 = Infrastructure.Security.Encryption.EncodeCvv(value2);
            base.SetEncryptedData(value1, value2);
        }

        public string CardNumber { get { return base.Value1; } set { base.Value1 = value; } }
        public string Cvv { get { return base.Value2; } set { base.Value2 = value; } }

        public string[] ToGroupedString()
        {
            return ExtractData(CardNumber);
        }

        public void FromGroupedString(string[] values)
        {
            CardNumber = string.Join(",", values);
        }

        public static string[] ExtractData(string value)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var ret = new List<string>();
            for (int i = 0; i < value.Length; i += 4)
                ret.Add(value.Substring(i, System.Math.Min(4, value.Length - i)));
            return ret.ToArray();
        }

        public override string GetLongString()
        {
            return string.Format("{0}\r\n{1}/{2}\r\n{3}", MaskedNumber, ExpirationMonth, ExpirationYear, new string('*', Cvv.EmptyIfNull().Length));
        }

        public string MaskedNumber
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                if (string.IsNullOrEmpty(CardNumber)) return string.Empty;
                if (CardNumber.Length > 10)
                    return CardNumber.Substring(0, 6) + (new string('X', CardNumber.Length - 10)) + CardNumber.Substring(CardNumber.Length - 4, 4);
                else if (CardNumber.Length > 6)
                    return CardNumber.Substring(0, 4) + (new string('X', CardNumber.Length - 6)) + CardNumber.Substring(CardNumber.Length - 2, 2);
                else return new string('X', CardNumber.Length);
            }
        }

        public override void GetMaskedData(out string value1, out string value2)
        {
            base.GetDecryptedData(out value1, out value2);
            value1 = MaskedNumber;
            if (!string.IsNullOrEmpty(value2)) value2 = new string('X', value2.Length);
        }

        public override ValidationResult Validate()
        {
            if (PaymentMethodId == CommonTypes.PaymentMethodEnum.CCUnknown || PaymentMethodId == CommonTypes.PaymentMethodEnum.Unknown)
                PaymentMethodId = GetPaymentMethodId(CardNumber);
            var ret = base.Validate();
            if (ret != ValidationResult.Success) return ret;
            var cvv = Cvv;
            if (Infrastructure.Application.IsProduction)
                if (!string.IsNullOrEmpty(cvv))
                    if ((ret = PaymentMethod.ValidateAccountValue2(cvv)) != ValidationResult.Success) return ret;
            if (!Validate(Value1)) return ValidationResult.Invalid_AccountValue1;
            return ret;
        }

        public static CommonTypes.PaymentMethodEnum GetPaymentMethodId(string cardNumber)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            //if (cardNumber.Length == 8 || cardNumber.Length == 9) return CommonTypes.PaymentMethodEnum.CCIsracard;
            var cardBin = CreditCardBin.Load(cardNumber);
            if (cardBin == null) return CommonTypes.PaymentMethodEnum.CCUnknown;
            return cardBin.PaymentMethodID;
        }

        public static string GetBinCountryIsoCode(string cardNumber)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var cardBin = CreditCardBin.Load(cardNumber);
            if (cardBin == null) return null;
            return cardBin.CountryIsoCode;
        }

        public static bool Validate(string cardNumber)
        {
            if (string.IsNullOrEmpty(cardNumber)) return false;
            if (cardNumber.Trim() == "") return false;
            if (cardNumber.Length == 8 || cardNumber.Length == 9)
                return ValidateIsracart(cardNumber);
            if (cardNumber.Length >= 13 && cardNumber.Length <= 16)
            {
                if (cardNumber.StartsWith("629971")/*LiveCash Does not support luhn*/) return true;
                try { return ValidateLuhn(cardNumber); } catch { return false; }
            }
            return false;
        }
        /*
		private static bool ValidateLuhnOld(string cardNumber)
		{
			int cardSize = cardNumber.Length;
			if (cardSize >= 13 && cardSize <= 16) {
				int odd = 0;
				int even = 0;
				char[] cardNumberArray = new char[cardSize];
				cardNumberArray = cardNumber.ToCharArray();
				Array.Reverse(cardNumberArray, 0, cardSize);
				for (int i = 0; i < cardSize; i++)
				{
					if (i % 2 == 0) {
						odd += (Convert.ToInt32(cardNumberArray.GetValue(i)) - 48);
					} else {
						int temp = (Convert.ToInt32(cardNumberArray[i]) - 48) * 2;
						if (temp > 9) temp = temp - 9;
						even += temp;
					}
				}
				if ((odd + even) % 10 == 0) return true;
				else return false;
			}
			else return false;
		}
        */
        private static char GetLuhnCheckDigit(string cardNumber)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            int sum = 0;
            bool mul = (cardNumber.Length % 2) == 1;
            for (int i = 0; i < cardNumber.Length; i++, mul = !mul)
            {
                var num = (cardNumber[i] - '0');
                if (num < 0 || num > 9) throw new ArgumentOutOfRangeException("cardNumber", "must be numeric");
                if (mul)
                {
                    num *= 2;
                    if (num > 9) num = (num % 10) + 1;
                }
                sum += num;
            }
            var ret = 10 - (sum % 10);
            if (ret == 10) return '0';
            return (char)('0' + ret);
        }

        private static bool ValidateLuhn(string cardNumber)
        {
            int cardSize = cardNumber.Length;
            if (cardSize < 13 || cardSize > 16) return false;
            var checkDigit = GetLuhnCheckDigit(cardNumber.Substring(0, cardSize - 1));
            return checkDigit == cardNumber[cardSize - 1];
        }

        private static bool ValidateIsracart(string cardNumber)
        {
            if (cardNumber.Length == 8) cardNumber = "0" + cardNumber;
            int total = 0;
            int index = 0;
            for (sbyte reverseIndex = 8; reverseIndex >= 0; reverseIndex--)
            {
                index++;
                sbyte currentDigit = sbyte.Parse(cardNumber[reverseIndex].ToString());
                total += index * currentDigit;
            }

            bool isValid = ((total > 0) && (System.Math.Round((double)total / 11) * 11 == total));
            return isValid;
        }

        public static string Generate(string cardPrefix)
        {
            var num = new Random().Next(0, int.MaxValue);
            var ret = cardPrefix + num.ToString().PadLeft(15, '0').TruncStart(15 - cardPrefix.Length);
            ret += GetLuhnCheckDigit(ret);
            return ret;
        }
    }
}
