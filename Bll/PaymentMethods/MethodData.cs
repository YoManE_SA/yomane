﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.PaymentMethods
{
    public class MethodData
    {
        public const string SecuredObjectName = "MethodData";
        public static SecuredObject SecuredObject { get { return SecuredObject.Get(Module.Current, SecuredObjectName); } }

        public MethodData(IStoredMethod data) { _data = data; }
        /*
		public MethodData(CommonTypes.PaymentMethodEnum methodType, string value1, string value2) 
		{
			PaymentMethodId = methodType;
			SetEncryptedData(value1, value2);
		}
		*/
        private IStoredMethod _data = null;
        protected bool IgnoreValue2Validation { get; set; }
        public CommonTypes.PaymentMethodEnum PaymentMethodId
        {
            get
            {
                return (CommonTypes.PaymentMethodEnum)_data.PaymentMethodId;
            }
            set
            {
                if (_data.PaymentMethodId == (int)value) return;
                _data.PaymentMethodId = (int)value;
                _data.MethodInstance = Create(_data);
            }
        }
        public PaymentMethod PaymentMethod { get { return PaymentMethods.PaymentMethod.Get(PaymentMethodId); } }
        public CommonTypes.PaymentMethodType PaymentMethodType { get { return PaymentMethod.Type; } }
        public CommonTypes.PaymentMethodGroupEnum PaymentMethodGroupId { get { return PaymentMethod.Group; } }

        public static readonly Dictionary<CommonTypes.PaymentMethodEnum, Type> MethodClasses = new Dictionary<CommonTypes.PaymentMethodEnum, Type>();
        public static readonly Dictionary<CommonTypes.PaymentMethodGroupEnum, Type> MethodGroupClasses = new Dictionary<CommonTypes.PaymentMethodGroupEnum, Type>();

        public static MethodData Create(IStoredMethod data)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            Type tp;
            if (!MethodClasses.TryGetValue((CommonTypes.PaymentMethodEnum)data.PaymentMethodId, out tp)) tp = null;
            var pmType = PaymentMethod.Get(data.PaymentMethodId);
            if (pmType != null)
                if (!MethodGroupClasses.TryGetValue(pmType.Group, out tp)) tp = null;
            if (tp == null) tp = typeof(MethodData);
            return tp.GetConstructor(new Type[] { typeof(IStoredMethod) }).Invoke(new object[] { data }) as MethodData;
        }

        public virtual void GetDecryptedData(out string value1, out string value2)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            //User user = Customer.validateCustomer(_entity.Customer_id);
            value1 = (_data.Value1Encrypted != null) ? Netpay.Infrastructure.Security.Encryption.Decrypt(_data.EncryptionKey, _data.Value1Encrypted.ToArray()) : null;
            value2 = (_data.Value2Encrypted != null) ? Netpay.Infrastructure.Security.Encryption.Decrypt(_data.EncryptionKey, _data.Value2Encrypted.ToArray()) : null;
        }

        public virtual void SetEncryptedData(string value1, string value2)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
            {
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);
            }
            
            //User user = Customer.validateCustomer(_entity.Customer_id);
            _data.EncryptionKey = (byte)Domain.Current.EncryptionKeyNumber;
            _data.Value1First6Data = (value1.EmptyIfNull().Length > 6) ? value1.Substring(0, 6) : null;
            _data.Value1Last4Data = (value1.EmptyIfNull().Length > 4) ? value1.Substring(value1.Length - 4) : null;
            _data.Value1Encrypted = Netpay.Infrastructure.Security.Encryption.Encrypt(_data.EncryptionKey, value1);
            _data.Value2Encrypted = Netpay.Infrastructure.Security.Encryption.Encrypt(_data.EncryptionKey, value2);
        }

        public virtual void GetMaskedData(out string value1, out string value2)
        {
            GetDecryptedData(out value1, out value2);
            if (!string.IsNullOrEmpty(value1)) value1 = new string('X', value1.Length);
            if (!string.IsNullOrEmpty(value2)) value2 = new string('X', value2.Length);
        }

        public byte[] GetEncyptedValue1()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
            
            return _data.Value1Encrypted.ToArray();
        }

        public byte[] GetEncryptedValue2()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);


            return _data.Value2Encrypted.ToArray();
        }

        protected string Value1
        {
            get { string value1, value2; GetDecryptedData(out value1, out value2); return value1; }
            set { SetEncryptedData(value, Value2); }
        }

        protected string Value2
        {
            get { string value1, value2; GetDecryptedData(out value1, out value2); return value2; }
            set { SetEncryptedData(Value1, value); }
        }

        private static DateTime MinExpirationDate = new DateTime(1901, 1, 31);
        public string Value1First6 { get { return _data.Value1First6Data; } }
        public string Value1Last4 { get { return _data.Value1Last4Data; } }
        public DateTime? ExpirationDate { get { return _data.ExpirationDate; } set { _data.ExpirationDate = value; } }

        public int ExpirationMonth { get { return ExpirationDate.GetValueOrDefault(MinExpirationDate).Month; } }
        public int ExpirationYear { get { return ExpirationDate.GetValueOrDefault(MinExpirationDate).Year; } }
        public void SetExpirationMonth(int year, int month) { ExpirationDate = new DateTime(year, month, 1).AddMonths(1).AddDays(-1); }

        public bool HasValue1 { get { return _data.Value1Encrypted != null && _data.Value1Encrypted.Length > 0; } }
        public bool HasValue2 { get { return _data.Value2Encrypted != null && _data.Value2Encrypted.Length > 0; } }

        public virtual string Display { get { return string.Format("{0}...{1}", PaymentMethod.Name, Value1Last4); } }

        public virtual ValidationResult Validate()
        {
            ValidationResult ret;
            //if (ret != ValidationResult.Success) return ret;
            string value1, value2;
            GetDecryptedData(out value1, out value2);
            var paymentMethod = PaymentMethod;
            if ((ret = paymentMethod.ValidateAccountValue1(value1)) != ValidationResult.Success) return ret;
            if (!IgnoreValue2Validation)
            {
                if (!string.IsNullOrEmpty(value2))
                    if ((ret = paymentMethod.ValidateAccountValue2(value2)) != ValidationResult.Success) return ret;
            }
            if (paymentMethod.IsExpirationDateMandatory && ExpirationDate != null)
                if (ExpirationDate < DateTime.Now) return ValidationResult.Invalid_ExpDate;
            return ValidationResult.Success;
        }

        public int EncryptionKey { get { return _data.EncryptionKey; } }
        public void CopyTo(MethodData destData)
        {
            string encValue1, encValue2;
            destData.PaymentMethodId = PaymentMethodId;
            destData.ExpirationDate = ExpirationDate;
            GetDecryptedData(out encValue1, out encValue2);
            destData.SetEncryptedData(encValue1, encValue2);
        }

        public virtual string GetLongString()
        {
            return PaymentMethod.Name;
        }
    }
}
