﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.PaymentMethods
{
	public class BankAccount : MethodData
	{
		public BankAccount(IStoredMethod data) : base(data) { }
		
		public string AccountNumber { get { return base.Value1; } set { base.Value1 = value; } }
		public string RoutingNumber{ get { return base.Value2; } set { base.Value2 = value; } }

		public override string GetLongString()
		{
			return string.Format("{0}\r\n{1}", AccountNumber, RoutingNumber);
		}


	}
}
