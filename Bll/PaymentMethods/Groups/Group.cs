﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.PaymentMethods
{
	public class Group : Infrastructure.BaseDataObject
	{
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Bll.PaymentMethods.Groups.Module.Current, SecuredObjectName); } }


        private Dal.Netpay.PaymentMethodGroup _entity;

		public int ID { get { return (int)_entity.PaymentMethodGroup_id; } }
		public CommonTypes.PaymentMethodType? Type { get { return (CommonTypes.PaymentMethodType)_entity.pmg_Type; } }
		public string Name { get { return _entity.Name; } set { _entity.Name = value; } }
		public string ShortName { get { return _entity.ShortName; } }
		public byte SortOrder { get { return _entity.SortOrder.GetValueOrDefault(); } set { _entity.SortOrder = value; } }
		public bool IsPopular { get { return _entity.IsPopular; } set { _entity.IsPopular = value; } }

		private Group(Dal.Netpay.PaymentMethodGroup entity) { _entity = entity; }


		private static List<Group> Cache
		{
			get
			{
                if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return Domain.Current.GetCachData("PaymentMethodGroups", () => {
                    return new Domain.CachData((from c in DataContext.Reader.PaymentMethodGroups select new Group(c)).ToList(), DateTime.Now.AddHours(12));
                }) as List<Group>;
			}
		}

		public static List<Group> Search(ISortAndPage sortAndPage)
		{
			return Cache; //.SortAndPage(sortAndPage).ToList();
		}

        public static List<Group> Search()
        {
            return Cache; 
        }

        public static Group Get(int id)
		{
			return Cache.Where(g => g.ID == id).SingleOrDefault();
		}

		public static Group Get(CommonTypes.PaymentMethodGroupEnum id)
		{
			return Get((int)id);
		}

        public void Save()
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            DataContext.Writer.PaymentMethodGroups.Update(_entity, true);
            DataContext.Writer.SubmitChanges();
        }
    }
}
