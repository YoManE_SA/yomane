﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.PaymentMethods
{
    public interface IStoredMethod
	{
		int PaymentMethodId { get; set; }
		System.Data.Linq.Binary Value1Encrypted { get; set; }
		System.Data.Linq.Binary Value2Encrypted { get; set; }
		byte EncryptionKey { get; set; }
		DateTime? ExpirationDate { get; set; }
		string Value1First6Data { get; set; }
		string Value1Last4Data { get; set; }
		MethodData MethodInstance { get; set; }
	}

	public class StoredMethod : IStoredMethod
	{
		public int PaymentMethodId { get; set; }
		public System.Data.Linq.Binary Value1Encrypted { get; set; }
		public System.Data.Linq.Binary Value2Encrypted { get; set; }
		public byte EncryptionKey { get; set; }
		public DateTime? ExpirationDate { get; set; }
		public string Value1First6Data { get; set; }
		public string Value1Last4Data { get; set; }
		public MethodData MethodInstance { get; set; }
	}

}
