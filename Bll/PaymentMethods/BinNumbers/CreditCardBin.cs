using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.PaymentMethods
{
    public class CreditCardBin : BaseDataObject
    {
        public const string SecuredObjectName = "Manage";
        public static SecuredObject SecuredObject { get { return SecuredObject.Get(BinNumbers.Module.Current, SecuredObjectName); } }

        public class SearchFilters
        {
            public Infrastructure.Range<int?> BINRange;
            public List<string> CountriesCode;
            public List<CommonTypes.PaymentMethodEnum> PaymentMethods;
            public bool? IsPrepaidBins;
        }

        public static List<CreditCardBin> PrepaidBins
        {
            get
            {
                var sf = new SearchFilters() { IsPrepaidBins = true };
                return Search(sf, null);
            }
        }

        private Dal.Netpay.tblCreditCardBIN _entity;
        public int ID { get { return _entity.BINID; } set { _entity.BINID = value; } }

        /// <summary>
        /// This property has values that are equale to the Enum - CommonTypes.PaymentMethodEnum
        /// Except of Payment Methods that belong to Prepaid that have values in the range of
        /// 5000 - 20000 , which are not included in the CommonTypes.PaymentMethodEnum
        /// If needed - when the PaymentMethodID value is not the the range of Prepaid it is possible
        /// To convert it to the CommonTypes.PaymentMethodEnum
        /// </summary>
        public CommonTypes.PaymentMethodEnum PaymentMethodID { get { return (CommonTypes.PaymentMethodEnum)_entity.PaymentMethod; } set { _entity.PaymentMethod = (short)value; } }
        public string BrandName { get { return _entity.CCName; } set { _entity.CCName = value; } }
        //public byte CCType { get { return _entity.CCType; } set { _entity.CCType = value; } }
        public DateTime? ImportDate { get { return _entity.ImportDate; } set { _entity.ImportDate = value; } }
        public DateTime InsertDate { get { return _entity.InsertDate; } set { _entity.InsertDate = value; } }
        public string BIN { get { return _entity.BIN; } set { _entity.BIN = value; } }
        public int? BINLen { get { return _entity.BINLen; } set { _entity.BINLen = value; } }
        public int? BINNumber { get { return _entity.BINNumber; } set { _entity.BINNumber = value; } }
        public string CountryIsoCode { get { return _entity.isoCode; } set { _entity.isoCode = value; } }
        public string CCName { get { return _entity.CCName; } set { _entity.CCName = value; } }
        public byte CCType { get { return _entity.CCType; } set { _entity.CCType = value; } }

        public CreditCardBin() { _entity = new Dal.Netpay.tblCreditCardBIN(); }

        private CreditCardBin(Dal.Netpay.tblCreditCardBIN entity)
        {
            _entity = entity;
        }

        public static List<CreditCardBin> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var expression = from p in DataContext.Reader.tblCreditCardBINs select p;
            if (filters != null)
            {
                if (filters.BINRange.From != null) expression = expression.Where(p => p.BINNumber >= filters.BINRange.From);
                if (filters.BINRange.To != null) expression = expression.Where(p => p.BINNumber <= filters.BINRange.To);
                if (filters.PaymentMethods != null && filters.PaymentMethods.Count > 0) expression = expression.Where((p) => filters.PaymentMethods.Contains((CommonTypes.PaymentMethodEnum)p.PaymentMethod));
                if (filters.CountriesCode != null && filters.CountriesCode.Count > 0) expression = expression.Where((p) => filters.CountriesCode.Contains(p.isoCode));
                if (filters.IsPrepaidBins.HasValue && filters.IsPrepaidBins.Value)
                {
                    expression = expression
                        .Where((p) => (p.PaymentMethod >= (short)CommonTypes.PaymentMethodEnum.PP_MIN && p.PaymentMethod <= (short)CommonTypes.PaymentMethodEnum.PP_MAX || p.PaymentMethod == (short)CommonTypes.PaymentMethodEnum.CCTogglecard))
                        .Distinct();
                }
            }
            return expression.ApplySortAndPage(sortAndPage).Select(p => new CreditCardBin(p)).ToList();
        }

        public static CreditCardBin Load(string cardNumber)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
            return (from bin in DataContext.Reader.tblCreditCardBINs where System.Data.Linq.SqlClient.SqlMethods.Like(cardNumber, bin.BIN + "%") select new CreditCardBin(bin)).FirstOrDefault();
        }

        public static CreditCardBin Load(string bin, bool isbin=false)
        {
            return (from bintable in DataContext.Reader.tblCreditCardBINs where bintable.BIN == bin select new CreditCardBin(bintable)).SingleOrDefault();
        }


        public static CreditCardBin Load(int id)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var entity = (from bin in DataContext.Reader.tblCreditCardBINs where bin.BINID == id select bin).SingleOrDefault();
            if (entity == null) return null;
            return new CreditCardBin(entity);
        }

        public void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
            {
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

                //Check for add permissions if needed
                if (_entity.BINID == 0)
                {
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);
                }
            }
            
            DataContext.Writer.tblCreditCardBINs.Update(_entity, (_entity.BINID != 0));
            DataContext.Writer.SubmitChanges();
        }

        public void Delete()
        {
            if (_entity.BINID == 0) return;
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            DataContext.Writer.tblCreditCardBINs.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }

        //
        public static bool CheckIfBinExist(string bintosearch)
        {
            bintosearch = AddZerosToBin(bintosearch);

            if (DataContext.Reader.tblCreditCardBINs.Any(x => (x.BIN == bintosearch)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string AddZerosToBin(string bin)
        {
            if (bin.Length == 6)
            {
                return bin;
            }
            else
            {
                int intformat = int.Parse(bin);
                bin = intformat.ToString("000000");
                return bin;
            }
        }

        //Function that creates a new item of CreditCardBin inside the DB without creating 
        //An object of type CreditCardBin because the property of PaymentMethodID is from type enum
        //That will not include the Prepaid paymentmethods.
        //This function will only be used when adding Payment method of type Prepaid.
        public static void CreatePrepaidBinItem(string bin, string countryisocode2, int paymentmethodid, string ccname)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            var newBinItem = new Dal.Netpay.tblCreditCardBIN();
            newBinItem.BIN = bin;
            newBinItem.isoCode = countryisocode2;
            newBinItem.CCName = ccname;
            newBinItem.CCType = 50; //OneCard type ...
            newBinItem.InsertDate = DateTime.UtcNow;
            newBinItem.PaymentMethod = (short)paymentmethodid; //TBD - change this to int value , need to change the DB column to smallint instead of tinyint.

            DataContext.Writer.tblCreditCardBINs.InsertOnSubmit(newBinItem);
            DataContext.Writer.SubmitChanges();
        }
    }
}
