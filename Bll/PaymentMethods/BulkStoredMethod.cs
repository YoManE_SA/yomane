﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll.PaymentMethods
{
    public class BulkStoredMethod : BaseDataObject, IStoredMethod, IPersonalInfo
    {
        private Dal.Netpay.PreCreatedPaymentMethod _entity;

        public int ID { get { return _entity.PreCreatedPaymentMethod_id; } }
        public int PaymentMethodId { get { return _entity.PaymentMethod_id; } set { _entity.PaymentMethod_id = (short)value; } }
        public System.Data.Linq.Binary Value1Encrypted { get { return _entity.Value1Encrypted; } set { _entity.Value1Encrypted = value; } }
        public System.Data.Linq.Binary Value2Encrypted { get { return _entity.Value2Encrypted; } set { _entity.Value2Encrypted = value; } }
        public byte EncryptionKey { get { return _entity.EncryptionKey; } set { _entity.EncryptionKey = value; } }
        public DateTime? ExpirationDate { get { return _entity.ExpirationDate; } set { _entity.ExpirationDate = value; } }
        public string Value1First6Data { get { return _entity.Value1First6Text; } set { _entity.Value1First6Text = value; } }
        public string Value1Last4Data { get { return _entity.Value1Last4Text; } set { _entity.Value1Last4Text = value; } }
        public PaymentMethods.MethodData MethodInstance { get; set; }

        private string firstName, lastName;
        public string FullName { get { return _entity.OwnerName; } set { _entity.OwnerName = value; PersonalInfo.SplitFullName(value, out firstName, out lastName); } }
        public string FirstName { get { return firstName; } set { firstName = value; FullName = PersonalInfo.MakeFullName(firstName, lastName); } }
        public string LastName { get { return lastName; } set { lastName = value; FullName = PersonalInfo.MakeFullName(firstName, lastName); } }

        public string EmailAddress { get { return null; } set { } }
        public DateTime? DateOfBirth { get { return _entity.OwnerDateOfBirth; } set { _entity.OwnerDateOfBirth = value; } }
        public string PhoneNumber { get { return _entity.OwnerPhoneNumber; } set { _entity.OwnerPhoneNumber = value; } }
        public string PersonalNumber { get { return _entity.OwnerPersonalID; } set { _entity.OwnerPersonalID = value; } }
        public string ProviderID { get { return _entity.PaymentMethodProvider_id; } set { _entity.PaymentMethodProvider_id = value; } }

        protected BulkStoredMethod(Dal.Netpay.PreCreatedPaymentMethod entity)
		{ 
			_entity = entity;
            PersonalInfo.SplitFullName(_entity.OwnerName, out firstName, out lastName);
			MethodInstance = PaymentMethods.MethodData.Create(this);
		}

        public BulkStoredMethod()
		{
            _entity = new Dal.Netpay.PreCreatedPaymentMethod();
			MethodInstance = PaymentMethods.MethodData.Create(this);
		}

        public override ValidationResult Validate()
        {
            if (string.IsNullOrEmpty(FullName))
                return ValidationResult.Invalid_OwnerName;
            return MethodInstance.Validate();
        }

        public void Save()
        {
            ValidationException.RaiseIfNeeded(Validate());
            DataContext.Writer.PreCreatedPaymentMethods.Update(_entity, (_entity.PreCreatedPaymentMethod_id != 0));
            DataContext.Writer.SubmitChanges();
        }

        public static BulkStoredMethod Load(int id)
        {
            return (from c in DataContext.Reader.PreCreatedPaymentMethods where c.PreCreatedPaymentMethod_id == id select  new BulkStoredMethod(c)).SingleOrDefault();
            
        }

        public static List<BulkStoredMethod> FindByPAN(string value1, string value2)
        {
            var ret = (from c in DataContext.Reader.PreCreatedPaymentMethods select c);
            ret = ret.Where(c => DataContext.Reader.GetDecrypted256(c.Value1Encrypted) == value1);
            if (value2 != null) ret = ret.Where(c => DataContext.Reader.GetDecrypted256(c.Value2Encrypted) == value2);
            return ret.ToList().Select(c => new BulkStoredMethod(c)).ToList();
        }

        public void AttachToAccount(int accountID) 
        {
            var dataObject = new Bll.Accounts.StoredPaymentMethod();
            dataObject.Account_id = Bll.Accounts.Account.Current.AccountID;
            MethodInstance.CopyTo(dataObject.MethodInstance);
            dataObject.OwnerName = FullName;
            dataObject.OwnerSSN = PersonalNumber;
            dataObject.ProviderID = ProviderID;
            dataObject.Save();
            Delete();
        }

        public void Delete()
        {
            if (_entity.PreCreatedPaymentMethod_id == 0) return;
            DataContext.Writer.PreCreatedPaymentMethods.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }
    
    }
}
