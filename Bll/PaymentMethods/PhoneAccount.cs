﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.PaymentMethods
{
	public class PhoneAccount : MethodData
	{
		public PhoneAccount(IStoredMethod data) : base(data) { }
		public string PhoneNumber { get { return base.Value1; } set { base.Value1 = value; } }
		public int CarrierID { get; set; }

		public override string GetLongString()
		{
			return string.Format("{0}\r\n{1}", PhoneNumber, CarrierID);
		}


	}
}
