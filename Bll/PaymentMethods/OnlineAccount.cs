﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.PaymentMethods
{
	public class OnlineAccount : MethodData
	{
		public OnlineAccount(IStoredMethod data) : base(data) { }
		public string UserName { get { return Value1; } }
		public string Password { get { return Value2; } }
	
	}
}
