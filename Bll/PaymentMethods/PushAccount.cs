﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.PaymentMethods
{
	public class PushAccount : MethodData
	{
		public PushAccount(IStoredMethod data) : base(data) { }
		public string CountryIso { get; set; }
		public string BankName { get; set; }
		public int BankID { get; set; }

		public override string GetLongString()
		{
			return string.Format("{0}\r\n{1}", BankName, CountryIso);
		}
	}

}
