﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.PaymentMethods
{
	public class Module : Infrastructure.Module
	{
        public const string ModuleName = "PaymentMethods";
        public override string Name { get { return ModuleName; } }
		public override decimal Version { get { return 1.0m; } }
		public static Module Current { get { return Infrastructure.Module.Get(ModuleName) as Module; } }

		protected override void OnLoad(EventArgs e)
		{
			if (MethodData.MethodGroupClasses.Count == 0) {
				MethodData.MethodGroupClasses.Add(CommonTypes.PaymentMethodGroupEnum.CreditCard, typeof(CreditCard));
                MethodData.MethodGroupClasses.Add(CommonTypes.PaymentMethodGroupEnum.Prepaid, typeof(CreditCard));
                MethodData.MethodGroupClasses.Add(CommonTypes.PaymentMethodGroupEnum.DirectDebit, typeof(BankAccount));
				MethodData.MethodGroupClasses.Add(CommonTypes.PaymentMethodGroupEnum.EuropeanDirectDebit, typeof(BankAccount));
				MethodData.MethodGroupClasses.Add(CommonTypes.PaymentMethodGroupEnum.InstantOnlinkBankTransfer, typeof(BankAccount));
				MethodData.MethodGroupClasses.Add(CommonTypes.PaymentMethodGroupEnum.Micropay, typeof(BankAccount));
				MethodData.MethodGroupClasses.Add(CommonTypes.PaymentMethodGroupEnum.OnlineBankTransfer, typeof(OnlineAccount));
				MethodData.MethodGroupClasses.Add(CommonTypes.PaymentMethodGroupEnum.Wallet, typeof(OnlineAccount));
				MethodData.MethodGroupClasses.Add(CommonTypes.PaymentMethodGroupEnum.Other, typeof(BankAccount));
			}
			base.OnLoad(e);
		}

        protected override void OnInstall(EventArgs e)
        {
            Infrastructure.Security.SecuredObject.Create(this, PaymentMethod.SecuredObjectName, Infrastructure.Security.PermissionGroup.ReadEditDelete);
            Infrastructure.Security.SecuredObject.Create(this, MethodData.SecuredObjectName, Infrastructure.Security.PermissionGroup.ReadEditDelete);
            base.OnInstall(e);
        }

        protected override void OnUninstall(EventArgs e)
        {
            Infrastructure.Security.SecuredObject.Remove(this);
            base.OnUninstall(e);
        }
    }
}
