﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Bll
{
    public class BillingCompany : BaseDataObject 
    {
        private Dal.Netpay.tblBillingCompany _entity;

        public int ID { get { return _entity.BillingCompanys_id; } }

        public string Name { get { return _entity.name; } }

        public string Address { get { return _entity.address; } }

        public string Number { get { return _entity.number; } }

        public double VatAmount { get { return _entity.VATamount; } }

        private BillingCompany(Dal.Netpay.tblBillingCompany entity)
        {
            _entity = entity;
        }

        public static List<BillingCompany> Cache
        {
            get
            {
                return Domain.Current.GetCachData("BillingCompany", () =>
                {
                    return new Domain.CachData((from bc in DataContext.Reader.tblBillingCompanies
                                                select new BillingCompany(bc)).ToList(), DateTime.Now.AddHours(6));
                }) as List<BillingCompany>;
            }
        }

        public static double GetBillingCompanyVat(int id)
        {
            return Cache.Where(x => x.ID == id).SingleOrDefault().VatAmount;
        }
                
    }
}
