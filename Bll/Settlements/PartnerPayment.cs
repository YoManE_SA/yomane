﻿using System;
using System.Collections.Generic;
using System.Linq;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Settlements
{
	public class PartnerPayment
	{
        public class SearchFilters
        {
            public int? AffiliateId;
            public int? PaymentID;
            public List<int> merchantIDs;
            public Range<DateTime?> Date;
        }

        private Dal.Netpay.tblAffiliatePayment _entity;
        public int ID { get { return _entity.AFP_ID; } }
        public DateTime InsertDate { get { return _entity.AFP_InsertDate; } }
        public int AffiliateID { get { return _entity.AFP_Affiliate_ID; } }
        public int MerchantID { get { return _entity.AFP_CompanyID; } }
        public int CurrencyID { get { return _entity.AFP_PaymentCurrency; } }
        public int TransPaymentID { get { return _entity.AFP_TransPaymentID; } }
        public decimal FeeRatio { get { return _entity.AFP_FeeRatio; } }
        public decimal Amount { get { return _entity.AFP_PaymentAmount; } }
        public string PaymentNote { get { return _entity.AFP_PaymentNote; } }

        public DateTime? PaymentDate { get; private set; }
        public decimal? MerchantPaidAmount { get; private set; }

        private PartnerPayment(tblAffiliatePayment entity, tblTransactionPay payment, SetMerchantAffiliate smf)
		{
            if (entity == null) {
                _entity = new tblAffiliatePayment() {
                    AFP_TransPaymentID = payment.id,
                    AFP_PaymentCurrency = payment.Currency.GetValueOrDefault(),
                    AFP_CompanyID = payment.CompanyID,
                    AFP_Affiliate_ID = (smf != null ? smf.Affiliate_id : 0),
                };
            } else _entity = entity;
			if (payment != null) {
				MerchantPaidAmount = (decimal) payment.transPayTotal;
				PaymentDate = payment.PayDate;
			}
		}

        public static List<PartnerPayment> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Partner, UserRole.Admin });
            if (filters == null) filters = new SearchFilters();
            Accounts.AccountFilter.Current.Validate(Accounts.AccountType.Affiliate, ref filters.AffiliateId);
            filters.merchantIDs = Accounts.AccountFilter.Current.FilterIDList(Accounts.AccountType.Merchant, filters.merchantIDs);

            var expression = from ap in DataContext.Reader.tblAffiliatePayments
                             join p in DataContext.Reader.tblTransactionPays on ap.AFP_TransPaymentID equals p.id
                             where ap.AFP_Affiliate_ID == filters.AffiliateId orderby ap.AFP_ID descending select new { AffiliatePay = ap, Payment = p };
            if (filters != null)
            {
                if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
                    expression = expression.Where(t => t.Payment != null && filters.merchantIDs.Contains(t.Payment.CompanyID));
                if (filters.Date.From != null) expression = expression.Where(t => t.AffiliatePay.AFP_InsertDate >= filters.Date.From.Value.MinTime());
                if (filters.Date.To != null) expression = expression.Where(t => t.AffiliatePay.AFP_InsertDate <= filters.Date.To.Value.MaxTime());
                if (filters.PaymentID != null) expression = expression.Where(p => p.AffiliatePay.AFP_TransPaymentID == filters.PaymentID.Value);
            }
            return expression.ApplySortAndPage(sortAndPage).Select(t => new PartnerPayment(t.AffiliatePay, t.Payment, null)).ToList();
        }


        public static List<PartnerPayment> GetUpcoming(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
            {
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Affiliates.Affiliate.SecuredObject, PermissionValue.Read);
            }

                var expression = from tp in DataContext.Reader.tblTransactionPays
                            join p in DataContext.Reader.tblAffiliatePayments on tp.id equals p.AFP_TransPaymentID into gp from jp in gp.DefaultIfEmpty()
                            join sma in DataContext.Reader.SetMerchantAffiliates.DefaultIfEmpty() on tp.CompanyID equals sma.Merchant_id into gsma from jsma in gsma.DefaultIfEmpty()
                            select new { AffiliatePay = jp, Payment = tp, sma = jsma };
            if (filters != null) {
                if (filters.AffiliateId != null) expression = expression.Where(p => (p.sma.Affiliate_id == filters.AffiliateId.Value));
                if (filters.PaymentID != null) expression = expression.Where(p => p.Payment.id == filters.PaymentID.Value);
                if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
                    expression = expression.Where(t => t.Payment != null && filters.merchantIDs.Contains(t.Payment.CompanyID));
            }
            return expression.ApplySortAndPage(sortAndPage, "Payment.").Select(t => new PartnerPayment(t.AffiliatePay, t.Payment, t.sma)).ToList();
        }

        public void Delete()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
            {
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Affiliates.Affiliate.SecuredObject, PermissionValue.Delete);
            }

                var wire = Wires.Wire.Search(new Wires.Wire.SearchFilters() { AffiliateSettlementID = _entity.AFP_ID }, null).SingleOrDefault();
            if (wire != null) wire.Delete();
            if (_entity.AFP_ID != 0) Totals.AffiliateFees.DeleteAffiliatePayment(_entity.AFP_ID);
            _entity.AFP_ID = 0;
        }

        public void Create(List<Totals.AffiliateTotalLine> lines = null)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
            {
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Affiliates.Affiliate.SecuredObject, PermissionValue.Add);
            }

            Wires.Wire wire = null;
            if(_entity.AFP_ID != 0) { 
                wire = Wires.Wire.Search(new Wires.Wire.SearchFilters() { AffiliateSettlementID = _entity.AFP_ID }, null).SingleOrDefault();
                if (wire != null && !wire.CanSetAmount) throw new Exception("Wire status is not pending");
            }
            Totals.CPayment AmountSum = Totals.Totals.CalcCompanyPaymentTotal(TransPaymentID, MerchantID, CurrencyID);
            Totals.AffiliateFees AffSum = Totals.AffiliateFees.CalcAffiliateFees(ID, AmountSum, AffiliateID);
            AffSum.UpdatePayment(lines);
            _entity.AFP_ID = AffSum.AFP_ID;
            if (wire == null) {   //create wire
                var account = Accounts.Account.LoadAccount(Accounts.AccountType.Affiliate, AffiliateID);
                wire = new Wires.Wire(account.AccountID, _entity.AFP_ID, true);
            }
            wire.SetAmount((CommonTypes.Currency) AmountSum.Currency, AffSum.Total);
        }
    }
}
