﻿using System;
using System.Linq;
using System.Collections.Generic;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Settlements
{
    public class Settlement
    {
        public class SearchFilters
        {
            public int? SpecificID;
            public Range<int?> ID;
            public Range<DateTime?> Date;
            public Range<decimal?> Amount;
            public List<int> MerchantIDs;
            public bool? VisibleToMerchant;
            //public bool? BankTransfer;
        }

        public const string SecuredObjectName = "Settlements";

        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Settlements.Module.Current, SecuredObjectName); } }

        private Dal.Netpay.tblTransactionPay _entity;

        public int ID { get { return _entity.id; } }
        public int CompanyID { get { return _entity.CompanyID; } }
        public DateTime PayDate { get { return _entity.PayDate; } }
        public bool isChargeVAT { get { return _entity.isChargeVAT; } }
        public double ExchangeRate { get { return _entity.ExchangeRate; } }
        public double TransTotal { get { return _entity.transTotal; } }
        public double TransChargeTotal { get { return _entity.transChargeTotal; } }
        public double TransPayTotal { get { return _entity.transPayTotal; } }
        public decimal SecurityDeposit { get { return _entity.SecurityDeposit; } }
        public decimal TransRollingReserve { get { return _entity.transRollingReserve; } }
        public string Comment { get { return _entity.Comment; } }
        public int BillingCompanys_id { get { return _entity.BillingCompanys_id; } }
        public string BillingLanguageShow { get { return _entity.BillingLanguageShow; } }
        public byte BillingCurrencyShow { get { return _entity.BillingCurrencyShow; } }
        public string BillingCompanyName { get { return _entity.BillingCompanyName; } }
        public string BillingCompanyAddress { get { return _entity.BillingCompanyAddress; } }
        public string BillingCompanyNumber { get { return _entity.BillingCompanyNumber; } }
        public string BillingCompanyEmail { get { return _entity.BillingCompanyEmail; } }
        public DateTime BillingCreateDate { get { return _entity.BillingCreateDate; } }
        public bool isBillingPrintOriginal { get { return _entity.isBillingPrintOriginal; } }
        public string BillingToInfo { get { return _entity.BillingToInfo; } }
        public byte PayType { get { return _entity.PayType; } }
        public short invoiceType { get { return _entity.invoiceType; } }
        public short TerminalType { get { return _entity.TerminalType; } }
        public double PrimePercent { get { return _entity.PrimePercent; } }
        public bool IsShow { get { return _entity.isShow; } }
        public int InvoiceDocumentID { get { return _entity.InvoiceDocumentID; } }
        public int InvoiceNumber { get { return _entity.InvoiceNumber; } }
        public string Tp_Note { get { return _entity.tp_Note; } }
        public decimal PayPercent { get { return _entity.PayPercent; } }
        //private decimal PayPrecent { get { return entity.PayPrecent; } }
        public decimal VatAmount { get { return _entity.VatAmount; } }
        public int? CurrencyID { get { return _entity.Currency; } }
        public bool IsGradedFees { get { return _entity.IsGradedFees; } }
        public bool IsTotalsCached { get { return _entity.IsTotalsCached; } }
        public short? PaymentMethod_id { get { return _entity.PaymentMethod_id; } }

        public int? TotalCaptureCount { get { return _entity.TotalCaptureCount; } }
        public decimal? TotalCaptureAmount { get { return _entity.TotalCaptureAmount; } }
        public int? TotalAdminCount { get { return _entity.TotalAdminCount; } }
        public decimal? TotalAdminAmount { get { return _entity.TotalAdminAmount; } }
        public int? TotalSystemCount { get { return _entity.TotalSystemCount; } }
        public decimal? TotalSystemAmount { get { return _entity.TotalSystemAmount; } }
        public int? TotalRefundCount { get { return _entity.TotalRefundCount; } }
        public decimal? TotalRefundAmount { get { return _entity.TotalRefundAmount; } }
        public int? TotalChbCount { get { return _entity.TotalChbCount; } }
        public decimal? TotalChbAmount { get { return _entity.TotalChbAmount; } }
        public decimal? TotalFeeProcessCapture { get { return _entity.TotalFeeProcessCapture; } }
        public decimal? TotalFeeClarification { get { return _entity.TotalFeeClarification; } }
        public decimal? TotalFeeFinancing { get { return _entity.TotalFeeFinancing; } }
        public decimal? TotalFeeHandling { get { return _entity.TotalFeeHandling; } }
        public decimal? TotalFeeBank { get { return _entity.TotalFeeBank; } }
        public decimal? TotalRollingReserve { get { return _entity.TotalRollingReserve; } }
        public decimal? TotalRollingRelease { get { return _entity.TotalRollingRelease; } }
        public decimal? TotalDirectDeposit { get { return _entity.TotalDirectDeposit; } }
        public decimal? TotalFeeChb { get { return _entity.TotalFeeChb; } }
        public decimal? TotalAmountTrans { get { return _entity.TotalAmountTrans; } }
        public decimal? TotalAmountFee { get { return _entity.TotalAmountFee; } }
        public decimal? TotalPayout { get { return _entity.TotalPayout; } }
        public int? TotalCashbackCount { get { return _entity.TotalCashbackCount; } }
        public decimal? TotalCashbackAmount { get { return _entity.TotalCashbackAmount; } }


        public string WireFileName
        {
            get
            {
                var result =  Wires.Wire.Search(new Wires.Wire.SearchFilters() { MerchantSettlementID = ID }, null).SingleOrDefault();
                if (result != null)
                {
                    var wireFile = Bll.Wires.WireLog.GetLastFile(result.ID);
                    return wireFile != null ? wireFile.FileName : "";
                }
                else
                {
                    return "";
                }
            }
        }

        public int? WireID { get; private set; } //set from join
        public byte? WireStatus { get; private set; } //set from join

        public DateTime? WireStatusDate { get; private set; } //set from join

        public decimal WireFee { get; set; } //set from join

        public int? ID_BillingCompanyID { get; set; } // set from join

        public int? ID_Type { get; set; } // set from join

        public bool IsDeleteable
        {
            get
            {
                if (InvoiceNumber == 0 && (WireStatus == null || WireStatus.Value <= 2))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        internal Settlement(Dal.Netpay.tblTransactionPay entity)
        {
            _entity = entity;
        }

        public static List<Settlement> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            if (filters == null) filters = new SearchFilters();

            filters.MerchantIDs = Bll.Accounts.AccountFilter.Current.FilterIDList(Accounts.AccountType.Merchant, filters.MerchantIDs);

            //var exp = (from t in DataContext.Reader.tblTransactionPays orderby t.PayDate descending select t) as IQueryable<Dal.Netpay.tblTransactionPay>;

            var exp = from set in DataContext.Reader.tblTransactionPays
                      orderby set.PayDate descending
                      join wire in DataContext.Reader.Wires on set.id equals wire.MerchantSettlement_id into SetWire
                      join invoice in DataContext.Reader.tblInvoiceDocuments on set.InvoiceDocumentID equals invoice.ID into SetInvoice
                      from SetInvoiceIncludeEmpty in SetInvoice.DefaultIfEmpty()
                      from SetWireIncludeEmpty in SetWire.DefaultIfEmpty()
                      select new { set, SetWireIncludeEmpty, SetInvoiceIncludeEmpty };

            if (filters.SpecificID.HasValue) exp = exp.Where((x) => x.set.id == filters.SpecificID.Value);
            if (filters.ID.From.HasValue) exp = exp.Where((x) => x.set.id >= filters.ID.From.Value);
            if (filters.ID.To.HasValue) exp = exp.Where((x) => x.set.id <= filters.ID.To.Value);

            if (filters.Date.From.HasValue) exp = exp.Where((x) => x.set.PayDate >= filters.Date.From.Value);
            if (filters.Date.To.HasValue) exp = exp.Where((x) => x.set.PayDate <= filters.Date.To.Value);

            if (filters.Amount.From.HasValue) exp = exp.Where((x) => x.set.transPayTotal >= (double)filters.Amount.From.Value);
            if (filters.Amount.To.HasValue) exp = exp.Where((x) => x.set.transPayTotal <= (double)filters.Amount.To.Value);

            if (filters.MerchantIDs != null) exp = exp.Where((x) => filters.MerchantIDs.Contains(x.set.CompanyID));
            if (filters.VisibleToMerchant.HasValue) exp = exp.Where((x) => x.set.isShow == filters.VisibleToMerchant.Value);

            //if (filters.BankTransfer.HasValue) exp = exp.Where((x) => x.isShow == filters.VisibleToMerchant.Value);
            List<Settlement> settlementList = new List<Settlement>();
            try
            {
                var spList = exp.ApplySortAndPage(sortAndPage);

                Logger.Log(LogSeverity.Info, LogTag.Process, spList == null ? "SpList is null" : "SpList is not null");
                if (spList != null && spList.Any())
                    foreach (var sp in spList)
                    {
                        var item = new Settlement(sp.set);
                        if (sp.SetWireIncludeEmpty != null)
                        {
                            item.WireID = sp.SetWireIncludeEmpty.Wire_id;
                            item.WireStatus = sp.SetWireIncludeEmpty.WireStatus;
                            if (sp.SetWireIncludeEmpty.ActionDate != null)
                                item.WireStatusDate = sp.SetWireIncludeEmpty.ActionDate;
                            item.WireFee = sp.SetWireIncludeEmpty.WireFee;

                        }
                        if (sp.SetInvoiceIncludeEmpty != null)
                        {
                            item.ID_BillingCompanyID = sp.SetInvoiceIncludeEmpty.id_BillingCompanyID;
                            item.ID_Type = sp.SetInvoiceIncludeEmpty.id_Type;

                        }

                        settlementList.Add(item);
                    }
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                throw;
            }
            return settlementList;
            //return exp.ApplySortAndPage(sortAndPage)
            //    .Select(v => new Settlement(v.set)
            //    {
            //        WireID = v.SetWireIncludeEmpty.Wire_id,
            //        WireStatus = v.SetWireIncludeEmpty.WireStatus,
            //        WireStatusDate = v.SetWireIncludeEmpty.ActionDate,
            //        WireFee = v.SetWireIncludeEmpty.WireFee,
            //        ID_BillingCompanyID = v.SetInvoiceIncludeEmpty.id_BillingCompanyID,
            //        ID_Type = v.SetInvoiceIncludeEmpty.id_Type
            //    }).ToList();
        }

        public static void SetVisible(int settlementId, bool isShow)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            var item = (from t in DataContext.Writer.tblTransactionPays where t.id == settlementId select t).SingleOrDefault();
            item.isShow = isShow;
            DataContext.Writer.SubmitChanges();
        }

        public static List<Settlement> LoadForMerchant(int merchantID, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var exp = (from t in DataContext.Writer.tblTransactionPays where t.CompanyID == merchantID select t);
            //var expression = (IQueryable<viewSettlementsReport>)from s in DataContext.Reader.viewSettlementsReports where s.CompanyID == merchantID orderby s.PayDate descending select s;
            return exp.ApplySortAndPage(sortAndPage).Select(t => new Settlement(t)).ToList();
        }

        public static Settlement Load(int settlementID)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);

            var value = (from t in DataContext.Writer.tblTransactionPays where t.CompanyID == Bll.Merchants.Merchant.Current.ID && t.id == settlementID select t).SingleOrDefault();
            //viewSettlementsReport settlement = (from s in DataContext.Reader.viewSettlementsReports where s.CompanyID == Bll.Merchants.Merchant.Current.ID && s.id == settlementID select s).SingleOrDefault();
            if (value == null) return null;
            return new Settlement(value);
        }


        /*
        public Settlement(viewSettlementsReport entity)
        {
            ID = entity.id;
            PayDate = entity.PayDate;
            TransPayTotal = entity.transPayTotal;
            TransChargeTotal = entity.transChargeTotal;
            CurrencyID = (int)entity.currency;
            WireFee = entity.wireFee;
            if (entity.WireStatus.HasValue)
                WireStatus = Wires.Wire.StatusOldToNew((Wires.Wire.OldWireStatus)entity.WireStatus.Value);
            WireStatusDate = entity.WireStatusDate;
            Tp_Note = entity.tp_Note;
            WireFileName = entity.WireFileName;
            CompanyID = entity.CompanyID;
            InvoiceNumber = entity.InvoiceNumber;
            ID_Type = entity.id_Type;
            ID_BillingCompanyID = entity.id_BillingCompanyID;
        }
        */
        //public decimal? WireFee { get; set; }
        //public Wires.Wire.WireStatus? WireStatus { get; set; }
        //public DateTime? WireStatusDate { get; set; }
        //public string WireFileName { get; set; }
        //public int? ID_Type { get; set; }
        //public int? ID_BillingCompanyID { get; set; }
        //public decimal? TotalFeeProcessDecline { get; set; }
        //public decimal? TotalFeeProcessAuth { get; set; }
        //public decimal? TotalVatRatio { get; set; }
    }
}
