﻿using System;
using System.Collections.Generic;
using System.Linq;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Settlements
{
    public class SettlementInfo
    {
        public int ID { get; set; }
        public DateTime? MinTrans { get; set; }
        public DateTime? MaxTrans { get; set; }
        public DateTime? MinTransPD { get; set; }
        public DateTime? MaxTransPD { get; set; }
        public int? MinTransID { get; set; }
        public int? MaxTransID { get; set; }


        public static SettlementInfo GetSettlementInfo(int settlementID)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Merchant, UserRole.Admin });
            var info = (from s in DataContext.Reader.tblCompanyTransPasses where s.companyID == Bll.Merchants.Merchant.Current.ID && s.PayID.Contains(";" + settlementID.ToString() + ";") group s by s.companyID into g select new { minDate = g.Min(s => s.InsertDate), minTrans = g.Min(s => s.ID), maxDate = g.Max(s => s.InsertDate), maxTrans = g.Max(s => s.ID), minDatePD = g.Min(s => s.MerchantPD), maxDatePD = g.Max(s => s.MerchantPD) }).SingleOrDefault();
            if (info == null) return null;
            SettlementInfo result = new SettlementInfo();
            result.MinTrans = info.minDate;
            result.MaxTrans = info.maxDate;
            result.MinTransPD = info.minDatePD;
            result.MaxTransPD = info.maxDatePD;
            result.MinTransID = info.minTrans;
            result.MaxTransID = info.maxTrans;
            return result;
        }

        public static SettlementInfo GetUnsettlementInfo(int companyId, CommonTypes.Currency currency)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Merchant, UserRole.Admin });
            var info = (from s in DataContext.Reader.tblCompanyTransPasses where s.companyID == companyId && s.UnsettledInstallments > 0 && s.Currency == (int)currency group s by s.companyID into g select new { minDate = g.Min(s => s.InsertDate), minTrans = g.Min(s => s.ID), maxDate = g.Max(s => s.InsertDate), maxTrans = g.Max(s => s.ID), minDatePD = g.Min(s => s.MerchantPD), maxDatePD = g.Max(s => s.MerchantPD) }).SingleOrDefault();
            if (info == null) return null;
            SettlementInfo result = new SettlementInfo();
            result.MinTrans = info.minDate;
            result.MaxTrans = info.maxDate;
            result.MinTransPD = info.minDatePD;
            result.MaxTransPD = info.maxDatePD;
            result.MinTransID = info.minTrans;
            result.MaxTransID = info.maxTrans;
            return result;
        }

    }

    public class UpcomingSettlement
    {
        public const string SecuredObjectName = "UpcomingSettlements";

        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Settlements.Module.Current, SecuredObjectName); } }

        public class SearchFilters
        {
            public CommonTypes.Currency currency;
            public DateTime toDate;
            public bool? partialPay;
            public bool? IsSpecificMerchant;
            public int? merchantId;
        }

        public int MerchantID { get; set; }
        public string MerchantName { get; set; }
        public decimal SecurityDeposit { get; set; }
        public int Currency { get; set; }
        public byte PayingDaysMargin { get; set; }
        public byte PayingDaysMarginInitial { get; set; }
        public int numTransToPay { get; set; }
        public decimal PayPercent { get; set; }
        public decimal SumFees { get; set; }
        public decimal SumTransToPay { get; set; }
        public decimal PayAmount { get { return SumTransToPay - SumFees; } set { } }
        public bool IsPartial { get { return PayPercent != 100; } set { } }

        public decimal FullAmount { get; set; }
        public decimal FullCount { get; set; }

        public decimal RollingReserveSum { get; set; }

        public DateTime? LastSettlement { get; set; }

        public static List<UpcomingSettlement> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            if (!filters.IsSpecificMerchant.HasValue)
            {
                var totalToPaySql = "(Sum(floor(UnsettledAmount / UnsettledInstallments) + (UnsettledAmount % UnsettledInstallments)))";
                var sql = "SET NOCOUNT ON;" + "\r\n" +
                    "DECLARE @tbl TABLE(ID int, minSettleAmount money);" + "\r\n" +
                    "INSERT INTO @tbl Select Merchant_ID, MinSettlementAmount From Setting.SetMerchantSettlement Where Currency_ID=" + (int)filters.currency + " And IsShowToSettle=1;" + "\r\n" +
                    "SELECT Top 1000 tblCompany.ID MerchantID, Currency, CompanyName as MerchantName, SecurityDeposit, PayingDaysMargin, PayingDaysMarginInitial, " + "\r\n" +
                    " Count(*) As numTransToPay, PayPercent, " + "\r\n" +
                    " Sum(netpayFee_transactionCharge + netpayFee_ratioCharge + netpayFee_chbCharge + netpayFee_ClrfCharge) As SumFees, " + "\r\n" +
                    " " + totalToPaySql + " As SumTransToPay" + "\r\n" +
                    " FROM @tbl t INNER JOIN tblCompany WITH (NOLOCK) ON t.ID=tblCompany.ID" + "\r\n" +
                    " INNER JOIN tblCompanyTransPass WITH (NOLOCK) ON tblCompany.ID=tblCompanyTransPass.CompanyID" + "\r\n" +
                    " Where IsTestOnly=0 And UnsettledInstallments <> 0 And Currency=" + (int)filters.currency + "  And MerchantPD <={0}" + "\r\n";
                if (filters.partialPay != null) sql += " And PayPercent" + (!filters.partialPay.Value ? "=100" : "<100");
                //If PageSecurity.IsLimitedMerchant Then sSQL = sSQL & " AND CompanyID IN (SELECT ID FROM GetPermittedMerchants('" & PageSecurity.Username & "'))" & vbCrLf
                sql += " Group By tblCompany.ID, CompanyName, Currency, SecurityDeposit, PayingDaysMargin, PayingDaysMarginInitial, PayPercent, t.minSettleAmount" + "\r\n" +
                    " Having " + totalToPaySql + " >= t.minSettleAmount" + "\r\n" +
                    " Order By numTransToPay Desc;" + "\r\n" +
                    " SET NOCOUNT OFF;";
                var items = DataContext.Reader.ExecuteQuery<UpcomingSettlement>(sql, filters.toDate.DayEnd()).ToList().ApplySortAndPage(sortAndPage).ToList();
                var merchantIds = items.Select(v => v.MerchantID).ToList();
                var fullValues = (from t in DataContext.Reader.tblCompanyTransPasses where t.Currency == (byte)filters.currency && !t.isTestOnly && t.UnsettledInstallments > 0 && merchantIds.Contains(t.companyID) group t by t.companyID into g select new { MerchantID = g.Key, fullAmount = g.Sum(v => v.UnsettledAmount), fullCount = g.Count() }).ToList();
                var RRSums = (from t in DataContext.Reader.tblCompanyTransPasses where t.Currency == (byte)filters.currency && t.PaymentMethod == (short)CommonTypes.PaymentMethodEnum.RollingReserve && merchantIds.Contains(t.companyID) group t by t.companyID into g select new { MerchantID = g.Key, amount = g.Sum(v => (v.CreditType == 0 ? 1 : -1) * v.Amount) }).ToList();
                var lPayDates = (from t in DataContext.Reader.tblTransactionPays where t.Currency == (byte)filters.currency && merchantIds.Contains(t.CompanyID) group t by t.CompanyID into g select new { MerchantID = g.Key, value = g.Max(v => v.PayDate) }).ToList();

                foreach (var v in items)
                {
                    var fullMerchant = fullValues.Where(f => f.MerchantID == v.MerchantID).SingleOrDefault();
                    if (fullMerchant != null)
                    {
                        v.FullAmount = fullMerchant.fullAmount.GetValueOrDefault();
                        v.FullCount = fullMerchant.fullCount;
                    }

                    var rrMerchant = RRSums.Where(f => f.MerchantID == v.MerchantID).SingleOrDefault();
                    if (rrMerchant != null) v.RollingReserveSum = rrMerchant.amount;

                    var lastPayMerchant = lPayDates.Where(f => f.MerchantID == v.MerchantID).SingleOrDefault();
                    if (lastPayMerchant != null) v.LastSettlement = lastPayMerchant.value;
                }
                return items;
            }
            else if (filters.IsSpecificMerchant.HasValue && filters.IsSpecificMerchant.Value)
            {
                if (!filters.merchantId.HasValue) throw new Exception("Must choose merchant");
                var items = (from t in DataContext.Reader.tblCompanyTransPasses
                             where (t.Currency.HasValue &&
                                    t.companyID == filters.merchantId.Value &&
                                    t.UnsettledInstallments > 0)
                             group t by t.Currency.Value into currencyGroups
                             select new UpcomingSettlement
                             {
                                 Currency = currencyGroups.Key,
                                 MerchantID = currencyGroups.Select(x => x.companyID).First(),
                                 numTransToPay = currencyGroups.Count(),
                             })
                            .ToList();

                return items;
            }
            else
            {
                return null;
            }
        }
    }
}
