﻿using System;
using System.Collections.Generic;
using System.Linq;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Settlements
{
    public class MerchantSettings
    {
        public const string SecuredObjectName = "Merchant Settings";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Settlements.Module.Current, SecuredObjectName); } }


        private Dal.Netpay.SetMerchantSettlement _entity;

        public int ID { get { return _entity.SetMerchantSettlement_id; } }
        public int MerchantID { get { return _entity.Merchant_id; } }
        public int CurrencyID { get { return _entity.Currency_id; } }
        public bool IsShowToSettle { get { return _entity.IsShowToSettle; } set { _entity.IsShowToSettle = value; } }
        public bool IsAutoInvoice { get { return _entity.IsAutoInvoice; } set { _entity.IsAutoInvoice = value; } }
        public decimal WireFee { get { return _entity.WireFee; } set { _entity.WireFee = value; } }
        public decimal WireFeePercent { get { return _entity.WireFeePercent; } set { _entity.WireFeePercent = value; } }
        public decimal StorageFee { get { return _entity.StorageFee; } set { _entity.StorageFee = value; } }
        public decimal HandlingFee { get { return _entity.HandlingFee; } set { _entity.HandlingFee = value; } }
        public decimal MinPayoutAmount { get { return _entity.MinPayoutAmount; } set { _entity.MinPayoutAmount = value; } }
        public decimal MinSettlementAmount { get { return _entity.MinSettlementAmount; } set { _entity.MinSettlementAmount = value; } }

        public bool IsWireExcludeDebit { get { return _entity.IsWireExcludeDebit; } set { _entity.IsWireExcludeDebit = value; } }
        public bool IsWireExcludeRefund { get { return _entity.IsWireExcludeRefund; } set { _entity.IsWireExcludeRefund = value; } }
        public bool IsWireExcludeCashback { get { return _entity.IsWireExcludeCashback; } set { _entity.IsWireExcludeCashback = value; } }
        public bool IsWireExcludeFee { get { return _entity.IsWireExcludeFee; } set { _entity.IsWireExcludeFee = value; } }
        public bool IsWireExcludeChb { get { return _entity.IsWireExcludeChb; } set { _entity.IsWireExcludeChb = value; } }

        public MerchantSettings(Dal.Netpay.SetMerchantSettlement entity)
        {
            _entity = entity;
        }

        public MerchantSettings(int merchantId, CommonTypes.Currency currencyId)
        {
            _entity = new Dal.Netpay.SetMerchantSettlement() { Merchant_id = merchantId, Currency_id = (int)currencyId };
        }

        public static Dictionary<int, MerchantSettings> LoadForMerchant(int merchantId)
        {
            //Check for Admin
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            //Check from other projects
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, null, PermissionValue.Read);
            Accounts.AccountFilter.Current.Validate(Accounts.AccountType.Merchant, merchantId);
            return (from s in DataContext.Reader.SetMerchantSettlements where s.Merchant_id == merchantId select new MerchantSettings(s)).ToDictionary(s => s.CurrencyID);
        }

        public static MerchantSettings LoadForMerchant(int merchantId, CommonTypes.Currency currency)
        {
            //Check for Admin
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            //Check for other projects.
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, null, PermissionValue.Read);
            Accounts.AccountFilter.Current.Validate(Accounts.AccountType.Merchant, merchantId);
            return (from s in DataContext.Reader.SetMerchantSettlements where s.Merchant_id == merchantId && s.Currency_id == (int)currency select new MerchantSettings(s)).FirstOrDefault();
        }

        public decimal GetWireAmount(Bll.Totals.CPayment payment)
        {
            //Check for Admin
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var wireTotal = payment.Total;
            if (IsWireExcludeDebit) wireTotal -= payment.Totals.NormAmount;
            if (IsWireExcludeRefund) wireTotal -= payment.Totals.RefAmount;
            if (IsWireExcludeCashback) wireTotal -= payment.Totals.CashbackAmount;
            if (IsWireExcludeFee) wireTotal += payment.TotalFeesAmount - payment.FeesAutoAndManual.TotalAmount;
            if (IsWireExcludeChb) wireTotal += payment.Totals.DenAmount;
            return wireTotal;
        }

        public void Save()
        {
            //Check for Admin
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);


            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant });
            DataContext.Writer.SetMerchantSettlements.Update(_entity, (_entity.SetMerchantSettlement_id != 0));
            DataContext.Writer.SubmitChanges();
        }
    }
}
