﻿using System.Collections.Generic;
using System.Linq;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Domains;
using Netpay.CommonTypes;

namespace Netpay.Bll
{
	public static class Faq
	{
		/*
		private static List<Language> _supportedLanguages = null;
		private static Language LanguageOrDefault(string domainHost, Language language) 
		{
			if (_supportedLanguages == null)
			{
				lock (typeof(Faq)) 
				{
					if (_supportedLanguages == null) 
					{
						_supportedLanguages = new List<Language>();
						NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
						var languagesIDs = from fc in dc.QNAGroups group fc by fc.Language_id into g select g.Key;
						foreach (int? currentLanguageID in languagesIDs)
						{
							if (currentLanguageID.HasValue)
								_supportedLanguages.Add((Language)currentLanguageID.Value);
						}
					}
				}
			}

			if (_supportedLanguages.Contains(language))
				return language;
			else
				return Language.English;
		}
		*/
        /// <summary>
		/// Returns all faq categories for the given language that are visible.
		/// The dictionary returned, contains the category id as the key and the category name as the value.
		/// </summary>
		/// <param name="domainHost"></param>
		/// <param name="id"></param>
		/// <returns></returns>

		public static string GetCategoryName(string domainHost, int id)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			return (from fc in dc.QNAGroups where fc.QNAGroup_id == id select fc.name).SingleOrDefault();
		}

		public static Dictionary<int, string> GetCategories(string domainHost, Language? language)
        {
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			var categories = (from fc in dc.QNAGroups select fc);
			if (language != null) categories = categories.Where(fc => fc.Language_id == (int)language);
			return categories.ToDictionary(k => k.QNAGroup_id, v => v.name);
		}

        public static Dictionary<int, string> GetCategories(string domainHost, Language language, string templateRestriction, bool isMerchant, bool isCustomer, bool isDeveloper, bool isWebSite)
        {
            NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			var expression = from f in dc.QNAGroups where f.Language_id == (int)language && f.IsVisible select f;
            if (isWebSite)
				expression = expression.Where(f => f.IsWebsite);
            if (isCustomer)
				expression = expression.Where(f => f.IsWallet);
            if (isDeveloper)
				expression = expression.Where(f => f.IsDevCenter);
            if (isMerchant)
				expression = expression.Where(f => f.IsMerchantCP);
			if (templateRestriction != null)
				expression = expression.Where(f => f.TemplateRestriction == templateRestriction || f.TemplateRestriction == null || f.TemplateRestriction == "");

            Dictionary<int, string> categories = expression.ToDictionary(k => k.QNAGroup_id, v => v.name);
            return categories;
        }

		/// <summary>
		/// Returns all faqs in a category that are visible.
		/// The list returned, contains the faq question as the key and the faq answer as the value.
		/// </summary>
		/// <param name="domainHost"></param>
		/// <param name="categoryID"></param>
		/// <returns></returns>
		public static List<KeyValuePair<string, string>> GetFaqs(string domainHost, int? categoryID)
		{
			Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			var fq = (from f in dc.QNAs where f.IsVisible == true select f);
			if (categoryID != null) fq = fq.Where(f => f.QNAGroup_id == categoryID);
			var faqs = fq.Select(f => new KeyValueVO<string, string>(f.Question, f.Answer)).ToList();
            foreach (var item in faqs)
            {
                item.Key = item.Key.Replace("COMPANY_NAME", domain.BrandName);
                item.Value = item.Value.Replace("COMPANY_NAME", domain.BrandName);
            }

            return faqs;	
		}

		public static List<QNAVo> GetFaqs(string domainHost, int categoryID, bool? isVisible)
		{
			Domain domain = DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			var query = (from f in dc.QNAs where f.QNAGroup_id == categoryID select f);
			if (isVisible != null) query = query.Where(f => (f.IsVisible == isVisible));
			List<QNAVo> faqs = query.Select(f => new QNAVo(f)).ToList();
			return faqs;
		}

		public static QNAVo GetFaq(string domainHost, int id)
		{
			Domain domain = DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			return (from f in dc.QNAs where f.QNA_id == id select new QNAVo(f)).SingleOrDefault();
		}

		public static void SaveFaq(string domainHost, QNAVo value)
		{
			Domain domain = DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			var entity = (from f in dc.QNAs where f.QNA_id == value.ID select f).SingleOrDefault();
			if (entity == null) {
				entity = new Dal.Netpay.QNA();
				dc.QNAs.InsertOnSubmit(entity);
			}
			entity.Question = value.Question;
			entity.Answer = value.Answer;
			entity.IsVisible = value.IsVisible;
			entity.Rating = value.Rating;
			entity.QNAGroup_id = value.GroupID;
			dc.SubmitChanges();
		}

		public static void DeleteFaq(string domainHost, int id)
		{
			Domain domain = DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			var entity = (from f in dc.QNAs where f.QNA_id == id select f).Single();
			dc.QNAs.DeleteOnSubmit(entity);
			dc.SubmitChanges();
		}

	}
}
