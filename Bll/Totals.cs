using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Netpay.Infrastructure;
using System.Data;
using System.Linq;
using Netpay.CommonTypes;
using System.Web.UI.WebControls;
using Netpay.Infrastructure.Security;
using System.Transactions;

namespace Netpay.Bll.Totals
{

    public static class Totals
    {
        public static CPayment CalcCompanyPaymentTotal(int xPayID, int xCompanyID, int xCurrency) // TBD , implement the sWhere parameter of this original function , that is located at common_totalTransShowAffiliate.asp line 58. in this case there was no use of the sWhere parameter so we didn't implemented it yet.
        {
            //Create the CPayment object with companyID and Currency
            //This Load function already invokes the AddTransactions method.
            var xPayment = CPayment.Load(xPayID, xCompanyID);

            //if (xPayID == 0) {xPayment.AddSpecialAdminTransAmount } TBD - Implement this condition , the original function located at AdminCash-CalcTransPayment.asp file - line 856 , search the string 'CalcCompanyPaymentTotal'
            return xPayment;
        }
    }

    public class CAmount
    {
        public int HandlingCount, FinanceCount, NormCount, RefCount, CashbackCount, ClrfCount, DenCount, BankPaidCount;
        public decimal FinanceFee, NormLineFee, NormFutInst;
        public decimal NormAmount, RefAmount, CashbackAmount, ClrfAmount, DenAmount, BankPaidAmount, HandlingFee;
        public decimal NormFees, RefFees, CashbackFees, ClrfFees, DenFees;
        public decimal NormDebitFee, RefDebitFee, DenDebitFees;

        public CAmount()
        {
            FinanceFee = NormFutInst = NormLineFee = 0m;
            FinanceCount = HandlingCount = 0;
            NormCount = RefCount = CashbackCount = ClrfCount = DenCount = 0;
            NormAmount = RefAmount = CashbackAmount = ClrfAmount = DenAmount = 0m;
            NormFees = RefFees = CashbackFees = ClrfFees = DenFees = 0m;
            NormDebitFee = RefDebitFee = DenDebitFees = 0m;
        }
        public decimal NormRatioFee { get { return NormFees - NormLineFee; } }
        public decimal TotalCount { get { return (NormCount + RefCount + CashbackCount + ClrfCount + DenCount); } }
        public decimal TotalAmount { get { return (NormAmount + RefAmount + CashbackAmount) - DenAmount; } }
        public decimal TotalFees { get { return (NormFees + RefFees + CashbackFees + ClrfFees + DenFees + FinanceFee); } }
        public decimal TotalPay { get { return TotalAmount - TotalFees; } }
        public decimal TotalDebitFees { get { return NormDebitFee + RefDebitFee + DenDebitFees; } }
        public decimal TotalProfit { get { return TotalFees - TotalDebitFees; } }

        public decimal TotalFeePercent
        {
            get
            {
                if (((NormFees - NormLineFee) == 0) || (NormAmount == 0)) return 0;
                return System.Math.Round(((NormFees - NormLineFee) / NormAmount) * 100, 2);
            }
        }

        internal decimal CalcDeniedTrans(IDataReader reader, int payID, bool addClrf, bool addDen)
        {
            decimal amount;
            if (Helpers.TestVar(reader["CreditType"], 0, -1, 0) == 8 && Helpers.TestVar(reader["DeniedStatus"], 0, -1, 0) == 2 && payID == 0)
            {
                amount = Helpers.TestVar(Helpers.ExecScalar("Select Sum(amount) From tblCompanyTransInstallments Where payID > 0 And transAnsID=" + reader["ID"]), 0m, -1m, 0m);
            }
            else
            {
                amount = Helpers.TestVar(reader["Amount"], 0m, -1m, 0m);
            }
            if (addClrf)
            {
                ClrfCount += 1;
                ClrfAmount += amount;
                ClrfFees += Helpers.TestVar(reader["netpayFee_ClrfCharge"], 0m, -1m, 0m);
            }
            if (addDen)
            {
                DenCount += 1;
                DenAmount += amount;
                DenFees += Helpers.TestVar(reader["netpayFee_chbCharge"], 0m, -1m, 0m);
                DenDebitFees += Helpers.TestVar(reader["DebitFee"], 0m, -1m, 0m);
            }

            return amount;
        }

        private void CalcFinanceFee(IDataReader reader, int payID, decimal prime)
        {
            if (Helpers.TestVar(reader["Interest"], 0m, -1m, 0m) != 0m)
            {
                FinanceCount = FinanceCount + 1;
                if (payID > 800) //a different calculation for old invoices
                    FinanceFee = FinanceFee + (Helpers.TestVar(reader["Amount"], 0m, -1m, 0m) * ((Helpers.TestVar(reader["Interest"], 0m, -1m, 0m) + prime) / 100));
                else
                    FinanceFee = FinanceFee + (Helpers.TestVar(reader["Amount"], 0m, -1m, 0m) * ((((Helpers.TestVar(reader["Interest"], 0m, -1m, 0m) + prime) / 12) * (Helpers.TestVar(reader["Payments"], 0, -1, 1) / 2)) / 100));
            }
        }

        internal void AddTransaction(CPayment.CreateSettings settings, IDataReader reader, int payID, DateTime payDate, decimal prime, bool apply, bool firstNPInts)
        {
            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
            ////Logger.Log(LogSeverity.Info, LogTag.Wires, "inside Totals.AddTransaction()");

            byte status;
            bool payTrans;
            decimal value, incCharge = 0;
            string netpayFee_chbCharge = "netpayFee_chbCharge", netpayFee_ClrfCharge = "netpayFee_ClrfCharge";
            if (payID == 0 && Helpers.TestVar(reader["isTestOnly"], false)) return;
            status = (byte)Helpers.TestVar(reader["DeniedStatus"], 0, -1, 0);
            if (apply)
            {
                if (status == (int)DeniedStatus.UnsettledInVerification) status = (byte)Helpers.TestVar(settings.ChbStatus[(int)reader["ID"]], 0, -1, (settings.AutoChb ? (int)DeniedStatus.UnsettledBeenSettledAndDeducted : status));
                else if (status == (int)DeniedStatus.SettledInVerification) status = (byte)Helpers.TestVar(settings.ChbStatus[(int)reader["ID"]], 0, -1, (settings.AutoChb ? (int)DeniedStatus.DuplicateDeducted : status));
            }
            payTrans = false;
            int insID = Helpers.TestVar(reader["InsID"], 0, -1, 0);
            int creditType = Helpers.TestVar(reader["CreditType"], 0, -1, 0);
            int oDeniedStatus = Helpers.TestVar(reader["DeniedStatus"], 0, -1, 0);
            switch ((DeniedStatus)status)
            {
                case DeniedStatus.ValidTransaction:
                case DeniedStatus.RefundedValid:
                    incCharge = 1;
                    break;
                case DeniedStatus.UnsettledInVerification: //מהמתנה בבירור
                    if (payID == 0)
                    {
                        if ((creditType != 8) || (insID == 1)) CalcDeniedTrans(reader, payID, true, true);
                    }
                    incCharge = 1;
                    break;
                case DeniedStatus.SettledInVerification: //שולם בבירור
                    if (payID == 0)
                    {
                        if ((creditType != 8) || (insID == 1)) CalcDeniedTrans(reader, payID, true, true);
                        incCharge = 0;
                    }
                    else
                    {
                        incCharge = 1;
                    }
                    break;
                case DeniedStatus.UnsettledBeenSettledAndValid:
                case DeniedStatus.SetFoundValidRefunded: //עבר לשולם ותקין
                    if ((creditType != 8) || (insID == 1)) CalcDeniedTrans(reader, payID, true, false);
                    netpayFee_chbCharge = "0";
                    if (creditType == 8) netpayFee_ClrfCharge = "0"; //do not charge
                    incCharge = 1;
                    break;
                case DeniedStatus.UnsettledBeenSettledAndDeducted: //עבר לשולם וקוזז
                    if ((creditType != 8) || (insID == 1))
                    {
                        CalcDeniedTrans(reader, payID, true, true);
                    }
                    incCharge = 1;
                    break;
                case DeniedStatus.DuplicateValid: //שיכפול של חיוב שנמצא תקין
                    if (creditType != 8)
                    {
                        CalcDeniedTrans(reader, payID, true, false);
                        if (apply && (oDeniedStatus != 5))
                        {
                            CopyTransaction(reader, payID, payDate, status, 0m);
                            Helpers.ExecSql("UPDATE tblCompanyTransPass SET DeniedStatus=11, netpayFee_chbCharge=0, netpayFee_ClrfCharge=0 WHERE ID=" + reader["ID"]);
                        }
                        payTrans = true;
                        incCharge = 0;
                    }
                    else
                    {
                        netpayFee_chbCharge = netpayFee_ClrfCharge = "0";
                        incCharge = 1;
                    }
                    break;
                case DeniedStatus.DuplicateDeducted: //שיכפול של חיוב שקוזז
                    incCharge = 0;
                    if ((creditType != 8) || (insID == 1 || insID == 0))
                    {
                        value = CalcDeniedTrans(reader, (apply ? 0 : payID), true, true);
                        if (apply)
                        {
                            if (oDeniedStatus == (int)DeniedStatus.DuplicateDeducted)
                            {
                                payTrans = true;
                            }
                            else
                            {
                                if (creditType == 8) Helpers.ExecSql("UPDATE tblCompanyTransPass SET PayID='" + Helpers.TestVar(reader["PayID"], -1, "").Replace("0;", "") + "' WHERE ID=" + reader["ID"]);
                                CopyTransaction(reader, payID, payDate, status, value);
                                Helpers.ExecSql("UPDATE tblCompanyTransPass SET DeniedStatus=10, netpayFee_chbCharge=0, netpayFee_ClrfCharge=0 WHERE ID=" + reader["ID"]);
                            }
                        }
                    }
                    break;
                case DeniedStatus.RefundedChargedBack: //הכחשה של עסקה רגילה שזוכתה
                    incCharge = 0;
                    payTrans = true;
                    break;
                case DeniedStatus.WasWorkedOut:
                case DeniedStatus.DuplicateTransactionWorkedOut:
                case DeniedStatus.DupWasWorkedOutRefunded:
                    incCharge = 1;
                    CalcDeniedTrans(reader, payID, true, false);
                    break;
            }
            //Response.Write(iRs("ID") + " " + NormAmount + " " + NormCount + " " + bIncCharge + "<br>")
            if (incCharge != 0)
            {
                payTrans = true;
                decimal amount = Helpers.TestVar(reader["Amount"], 0m, -1m, 0m);
                decimal handlingFee = Helpers.TestVar(reader["HandlingFee"], 0m, -1m, 0m);
                if (creditType == 0)
                {
                    if (reader["IsCashback"].ToNullableBool().GetValueOrDefault())
                    {
                        CashbackAmount -= (amount * incCharge);
                        if (incCharge > 0)
                        {
                            CashbackCount++;
                            CashbackFees += Helpers.TestVar(reader["netpayFee_transactionCharge"], 0m, -1m, 0m) + Helpers.TestVar(reader["netpayFee_ratioCharge"], 0m, -1m, 0m);
                        }
                    }
                    else
                    {
                        RefAmount -= (amount * incCharge);
                        if (incCharge > 0)
                        {
                            RefCount++;
                            RefFees += Helpers.TestVar(reader["netpayFee_transactionCharge"], 0m, -1m, 0m) + Helpers.TestVar(reader["netpayFee_ratioCharge"], 0m, -1m, 0m);
                            RefDebitFee += Helpers.TestVar(reader["DebitFee"], 0m, -1m, 0m);
                        }
                    }
                    if ((incCharge > 0) && (handlingFee > 0m))
                    {
                        HandlingCount++;
                        HandlingFee += handlingFee;
                    }
                }
                else if (creditType == 8)
                {
                    value = (int)(amount / Helpers.TestVar(reader["Payments"], 0, -1, 0));
                    if (insID == 1) value = (amount - (value * Helpers.TestVar(reader["Payments"], 0, -1, 0))) + value;
                    if ((Helpers.TestVar(reader["PrimaryPayedID"], 0, -1, 0) == (apply ? 0 : payID)) && (insID == 1))
                    {
                        NormCount++;
                        NormFees += Helpers.TestVar(reader["netpayFee_transactionCharge"], 0m, -1m, 0m) + Helpers.TestVar(reader["netpayFee_ratioCharge"], 0m, -1m, 0m); ;
                        NormLineFee += Helpers.TestVar(reader["netpayFee_transactionCharge"], 0m, -1m, 0m);
                        NormDebitFee += Helpers.TestVar(reader["DebitFee"], 0m, -1m, 0m);
                        CalcFinanceFee(reader, payID, prime);
                    }
                    payTrans = false;
                    if ((payID != 0 && Helpers.TestVar(reader["IPayID"], 0, -1, 0) == payID) || (status == 4))
                    {
                        NormAmount += (value * incCharge);
                        payTrans = (status == 4);
                    }
                    else if (Helpers.TestVar(reader["IPayID"], 0, -1, 0) == 0 && (payID == 0 || apply))
                    {
                        if (firstNPInts || settings.Installements.Contains((int)reader["IID"]))
                        {
                            payTrans = true;
                            NormAmount += (value * incCharge);
                        }
                        else
                        {
                            NormFutInst += (value * incCharge);
                        }
                    }
                }
                else
                {
                    NormAmount += (amount * incCharge);
                    if (Helpers.TestVar(reader["BankPaid"], false)) { BankPaidCount += 1; BankPaidAmount += amount; }
                    if (incCharge > 0)
                    {
                        NormCount++;
                        NormFees += Helpers.TestVar(reader["netpayFee_transactionCharge"], 0m, -1m, 0m) + Helpers.TestVar(reader["netpayFee_ratioCharge"], 0m, -1m, 0m); ;
                        NormLineFee += Helpers.TestVar(reader["netpayFee_transactionCharge"], 0m, -1m, 0m);
                        if (handlingFee > 0m)
                        {
                            HandlingCount++;
                            HandlingFee += handlingFee;
                        }
                        NormDebitFee += Helpers.TestVar(reader["DebitFee"], 0m, -1m, 0m);
                    }
                    CalcFinanceFee(reader, payID, prime);
                }
            }
            if (apply && payTrans)
            {
                string thisPayID, sUpdate = "";
                if (creditType == 8)
                {
                    thisPayID = Helpers.TestVar(Helpers.ExecScalar("Select PayID From tblCompanyTransPass Where ID=" + reader["ID"]), -1, "");
                    thisPayID = thisPayID.Replace(";0;", ";" + payID + ";"); /*Only One , 1, 1, 0*/
                    if (insID == 1) sUpdate = "PrimaryPayedID=" + payID + ",";
                    sUpdate = sUpdate + "MerchantPD='" + Helpers.TestVar(reader["IMerchantPD"], DateTime.MinValue, DateTime.MaxValue, DateTime.MinValue).AddMonths(1).ToString(CPayment.DateTimeFormat) + "',";
                    Helpers.ExecSql("Update tblCompanyTransInstallments SET PayID=" + payID + ", MerchantRealPD='" + payDate.ToString(CPayment.DateTimeFormat) + "' WHERE ID=" + reader["IID"]);
                }
                else
                {
                    thisPayID = ";" + payID + ";";
                    sUpdate = "PrimaryPayedID=" + payID + ",";
                }
                Helpers.ExecSql("UPDATE tblCompanyTransPass SET " + sUpdate + " payID='" + thisPayID + "', MerchantRealPD='" + payDate.ToString(CPayment.DateTimeFormat) + "'," +
                    " DeniedStatus=" + status + ", netpayFee_chbCharge=" + netpayFee_chbCharge + ", netpayFee_ClrfCharge=" + netpayFee_ClrfCharge +
                    " WHERE ID=" + reader["ID"]); //DeniedDate='" + xPayDate)
            }
        }

        private void CopyTransaction(IDataReader reader, int payID, DateTime payDate, Byte status, Decimal amount)
        {
            decimal netpayFee_chbCharge = Helpers.TestVar(reader["netpayFee_chbCharge"], 0m, -1m, 0m), netpayFee_ClrfCharge = Helpers.TestVar(reader["netpayFee_ClrfCharge"], 0m, -1m, 0m);
            if (status == 5) netpayFee_chbCharge = 0m;
            string sSQL = "INSERT INTO tblCompanyTransPass(CompanyID, DebitCompanyID, OriginalTransID, TransSource_id, CustomerID, InsertDate, PrimaryPayedID, PayID, DeniedDate," +
                "DeniedStatus, IPAddress, Amount, Currency, Payments, CreditType," +
                "OrderNumber, Interest, Comment, DeniedPrintDate, DeniedSendDate, TerminalNumber, paymentMethodId, paymentMethodDisplay, PaymentMethod, OCurrency, OAmount, netpayFee_chbCharge, netpayFee_ClrfCharge) VALUES (" +
                reader["CompanyID"] + "," + reader["DebitCompanyID"] + "," + reader["ID"] + "," + reader["TransSource_id"] +
                "," + reader["CustomerID"] + ", '" + DateTime.Now.ToString() + "', " + payID + ", ';" + payID + ";" +
                "','" + reader["DeniedDate"] + "', " + status + ", '" + reader["IPAddress"] +
                "'," + amount + ", " + reader["Currency"] + ", 1, " + (status == 6 ? 0 : reader["CreditType"]) +
                ",'" + reader["OrderNumber"] +
                "', " + reader["Interest"] + ", '" + reader["Comment"] + "', '" + reader["DeniedPrintDate"] + "', '" + reader["DeniedSendDate"] + "', '" + reader["TerminalNumber"] + "', " + reader["PaymentMethodID"] + ", '" + reader["paymentMethodDisplay"] + "'" +
                "," + reader["PaymentMethod"] + "," + reader["OCurrency"] + "," + amount + "," + netpayFee_chbCharge + "," + netpayFee_ClrfCharge + ")";
            Helpers.ExecSql(sSQL);
        }

        public void ApplyMultiplyer(decimal pValue)
        {
            NormAmount = NormAmount * pValue;
            RefAmount = RefAmount * pValue;
            CashbackAmount = CashbackAmount * pValue;
            ClrfAmount = ClrfAmount * pValue;
            DenAmount = DenAmount * pValue;

            NormFees = NormFees * pValue;
            RefFees = RefFees * pValue;
            ClrfFees = ClrfFees * pValue;
            DenFees = DenFees * pValue;
            FinanceFee = FinanceFee * pValue;
            NormLineFee = NormLineFee * pValue;
            NormDebitFee = NormDebitFee * pValue;
            RefDebitFee = RefDebitFee * pValue;
            CashbackFees = CashbackFees * pValue;
            DenDebitFees = DenDebitFees * pValue;

            NormCount = (int)(NormCount * pValue);
            RefCount = (int)(RefCount * pValue);
            CashbackCount = (int)(CashbackCount * pValue);
            ClrfCount = (int)(ClrfCount * pValue);
            DenCount = (int)(DenCount * pValue);
            FinanceCount = (int)(FinanceCount * pValue);

            HandlingCount = (int)(HandlingCount * pValue);
            HandlingFee = HandlingFee * pValue;
            NormFutInst = NormFutInst * pValue;
        }

        public void AddAmount(CAmount amount)
        {
            NormAmount += amount.NormAmount;
            RefAmount += amount.RefAmount;
            CashbackAmount += amount.CashbackAmount;
            ClrfAmount += amount.ClrfAmount;
            DenAmount += amount.DenAmount;

            NormFees += amount.NormFees;
            RefFees += amount.RefFees;
            CashbackFees += amount.CashbackFees;
            ClrfFees += amount.ClrfFees;
            DenFees += amount.DenFees;
            FinanceFee += amount.FinanceFee;
            NormLineFee += amount.NormLineFee;

            NormCount += amount.NormCount;
            RefCount += amount.RefCount;
            CashbackCount += amount.CashbackCount;
            ClrfCount += amount.ClrfCount;
            DenCount += amount.DenCount;

            HandlingCount += amount.HandlingCount;
            HandlingFee += amount.HandlingFee;
            NormFutInst += amount.NormFutInst;

            BankPaidAmount += amount.BankPaidAmount;
            BankPaidCount += amount.BankPaidCount;
        }
    }

    public class CPayment
    {
        public static string DateTimeFormat = "yyyy-dd-MM";
        public static string LongDateTimeFormat = "yyyy-dd-MM mm:HH:ss";
        public static bool useImprov = true;
        public int CompanyID { get; private set; }
        public int PayID { get; private set; }
        public int Currency { get; private set; }

        public string CompanyName { get; private set; }
        public DateTime PayDate { get; private set; }
        public decimal VAT { get; private set; }

        //public CreateSettings Settings { get; set; }
        private IDataReader reader;
        private int rrRetCount, secKeepCurrency, secFlags, rrTransID, rrState, secPeriod;
        private decimal prime, exchangeRate, rrRetAmount, rrAmount, secDeposit, secKeepAmount, payPercent, curRes;
        private bool apply, hasRows, incRR;
        private CAmount[] PaymentMethodAmount;

        public class CreateSettings
        {
            public Range<DateTime?> TransDate;
            public Range<DateTime?> TransPD;

            public Dictionary<int, DeniedStatus> ChbStatus;
            public List<int> Installements;

            public List<PaymentMethodEnum> PaymentMethods;

            public bool AutoChb = true;
            public bool AutoInstallements = true;
            public bool ForceEpaPaid = true;

            public bool IncTestTransactions = false;
            public bool IncRefund = true;
            public bool IncNormalCharge = true;
            public bool IncProcessTrans = true;
            public bool IncAdminTrans = true;

            public bool IncApprovalFee = true;
            public bool IncFailFee = true;
            public bool IncStorageFee = true;

            public bool IncRR;
            public bool RetRR;
            public bool PaymentPerPm;

            //public const CreateSettings Empty = new CreateSettings { AutoChb = AutoInstallements = ForceEpaPaid = IncTestTransactions = IncRefund }

            public static CreateSettings DefaultForMerchant(int merchantId, CommonTypes.Currency currencyId)
            {
                var ret = new CreateSettings();
                var unsettleStat = Bll.Settlements.SettlementInfo.GetUnsettlementInfo(merchantId, currencyId);
                if (unsettleStat == null) unsettleStat = new Bll.Settlements.SettlementInfo();
                ret.TransDate = new Range<DateTime?>(unsettleStat.MinTrans, unsettleStat.MaxTrans);
                ret.TransPD = new Range<DateTime?>(unsettleStat.MinTransPD, unsettleStat.MaxTransPD);

                var rrState = (from m in DataContext.Reader.tblCompanies where m.ID == merchantId select new { m.RRAutoRet, m.RREnable }).SingleOrDefault();
                if (rrState != null)
                {
                    ret.IncRR = rrState.RREnable;
                    ret.RetRR = rrState.RRAutoRet.GetValueOrDefault();
                }
                else ret.IncRR = ret.RetRR = true;
                return ret;
            }
        }

        private CPayment()
        {
            apply = false; hasRows = false;
            secKeepCurrency = 255;
            PaymentMethodAmount = new CAmount[(int)PaymentMethodEnum.PaymentMethodMax + 1];
        }

        private CPayment(IDataReader xRs) : this()
        {
            PayID = Helpers.TestVar(xRs["id"], 0, -1, 0);
            Currency = Helpers.TestVar(xRs["Currency"], 0, -1, 0);
            CompanyID = Helpers.TestVar(xRs["CompanyID"], 0, -1, 0);
            CompanyName = xRs["CompanyName"].ToString();
            VAT = (Helpers.TestVar(xRs["IsChargeVAT"], false) ? Helpers.TestVar(xRs["VATAmount"], 0m, -1m, 0m) : 0);
            PayDate = Helpers.TestVar(xRs["Paydate"], DateTime.MinValue, DateTime.MaxValue, DateTime.MinValue);
            secDeposit = Helpers.TestVar(xRs["SecurityDeposit"], 0m, -1m, 0m);
            secPeriod = Helpers.TestVar(xRs["SecurityPeriod"], 0, -1, 0);
            secKeepAmount = Helpers.TestVar(xRs["RRKeepAmount"], 0m, -1m, 0m);
            secKeepCurrency = Helpers.TestVar(xRs["RRKeepCurrency"], 0, -1, 0);
            rrState = Helpers.TestVar(xRs["RRState"], 0, -1, 0);
            payPercent = Helpers.TestVar(xRs["PayPercent"], 0m, 100m, 100m);
            secFlags = (Helpers.TestVar(xRs["RREnable"], false) ? 1 : 0) | (Helpers.TestVar(xRs["RRAutoRet"], false) ? 2 : 0);
            exchangeRate = Helpers.TestVar(xRs["ExchangeRate"], 0m, -1m, 0m);
            prime = Helpers.TestVar(xRs["PrimePercent"], 0m, -1m, 0m);
            //nCompanyName = xRs("BillingCompanyName");
            CalcCurReserve();
        }

        private CPayment(IDataReader xRs, int currency, DateTime payDate) : this()
        {
            PayID = 0;
            Currency = currency;
            CompanyID = Helpers.TestVar(xRs["ID"], 0, -1, 0);
            CompanyName = xRs["CompanyName"].ToString();
            VAT = (Helpers.TestVar(xRs["IsChargeVAT"], false) ? Helpers.TestVar(xRs["VATAmount"], 0m, -1m, 0m) : 0);
            PayDate = payDate;
            secDeposit = Helpers.TestVar(xRs["SecurityDeposit"], 0m, -1m, 0m);
            secPeriod = Helpers.TestVar(xRs["SecurityPeriod"], 0, -1, 0);
            secKeepAmount = Helpers.TestVar(xRs["RRKeepAmount"], 0m, -1m, 0m);
            secKeepCurrency = Helpers.TestVar(xRs["RRKeepCurrency"], 0, -1, 0);
            rrState = Helpers.TestVar(xRs["RRState"], 0, -1, 0);
            secFlags = (Helpers.TestVar(xRs["RREnable"], false) ? 1 : 0) | (Helpers.TestVar(xRs["RRAutoRet"], false) ? 2 : 0);
            payPercent = Helpers.TestVar(xRs["PayPercent"], 0m, 100m, 100m);
            //if (Currency != 0) mExchangeRate = ConvertCurrencyRate(Currency, 0); 
            //else mExchangeRate = ConvertCurrencyRate(1, Currency);
            prime = Helpers.TestVar(Helpers.ExecScalar("SELECT Prime FROM tblGlobalValues"), 0m, -1m, 0m);
            CalcCurReserve();
        }

        ~CPayment()
        {
            for (int i = 0; i < PaymentMethodAmount.Length; i++)
                if (PaymentMethodAmount[i] == null) PaymentMethodAmount[i] = null;
        }

        public int RReturnCount { get { return rrRetCount; } }

        private void CalcCurReserve()
        {
            curRes = 0;
            IDataReader reader = null;
            if (secKeepAmount > 0)
            {
                try
                {
                    reader = Helpers.ExecReader("Select Amount, Currency From tblCompanyTransPass Where " +
                    "CompanyID=" + CompanyID + (secKeepCurrency != 255 ? "" : " And Currency=" + Currency) + " And PaymentMethod=" + (int)PaymentMethodEnum.RollingReserve + " And CreditType=0 And OriginalTransID=0");
                    while (reader.Read())
                    {
                        if (secKeepCurrency != 255) curRes += (new Money(Helpers.TestVar(reader["Amount"], 0m, -1m, 0m), Bll.Currency.Get(Helpers.TestVar(reader["Currency"], 0, -1, 0)).ID)).ConvertTo(Bll.Currency.Get(secKeepCurrency).ID).Amount;
                        else curRes += Helpers.TestVar(reader["Amount"], 0m, -1m, 0m);
                    }
                }
                catch (Exception ex) { Logger.Log(ex); }
                finally
                {

                    if (reader != null) reader.Close();
                }
            }
        }

        public bool UpdatePayment(DateTime payDate, CreateSettings setting, bool bApply = false)
        {
            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
            ////Logger.Log(LogSeverity.Info, LogTag.Wires, "Inside Totals.UpdatePayment(public bool) function");

            apply = bApply;
            PayDate = payDate;
            incRR = (setting != null) ? setting.IncRR : false;
            if (PayID == 0 && apply)
            {
                Logger.Log(LogSeverity.Info, LogTag.Wires, "(PayID == 0 && apply)=TRUE");
                try
                {
                    using (reader = Helpers.ExecReader("SELECT tblBillingCompanys.* FROM tblBillingCompanys LEFT JOIN tblCompany ON(tblCompany.BillingCompanys_id = tblBillingCompanys.BillingCompanys_id) WHERE tblCompany.ID=" + CompanyID))
                    {
                        if (reader.Read())
                        {
                            string sSQL = "INSERT INTO tblTransactionPay " +
                                " (PayDate, CompanyID, TerminalType, PrimePercent, ExchangeRate, Currency, BillingCompanys_id, BillingLanguageShow, BillingCurrencyShow, BillingCompanyName, BillingCompanyAddress, BillingCompanyNumber, BillingCompanyEmail, IsChargeVAT, VATamount, SecurityDeposit, PayPercent)VALUES(" +
                                "'" + PayDate.ToString(DateTimeFormat) + "'," + CompanyID + "," + 1 + "," + prime + "," + exchangeRate + "," + Currency + "," + reader["BillingCompanys_id"] + ",'" +
                                reader["LanguageShow"] + "'," + reader["CurrencyShow"] + ",'" + reader["name"] + "','" + reader["address"] + "','" +
                                reader["number"] + "','" + reader["email"] + "'," + (VAT > 0 ? 1 : 0) + "," + VAT + "," + secDeposit + "," + payPercent + ")";
                            //Response.Write(sSQL + "<br>")

                            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
                            ////Logger.Log(LogSeverity.Info, LogTag.Wires, "", "Inside Totals.UpdatePayment(public bool), passed the if (PayID == 0 && apply), Executing query:" + sSQL);

                            Helpers.ExecSql(sSQL);
                            PayID = Helpers.TestVar(Helpers.ExecScalar("Select Max(id) From tblTransactionPay Where companyID=" + CompanyID), 0, -1, 0);

                            Logger.Log(LogSeverity.Info, LogTag.Wires, "Settlement creation started , ID - " + PayID);
                        }
                        reader.Close();
                    }
                    //Copy Fees From tblCompanyCreditFees To tblTransactionPayFees
                    Helpers.ExecSql("Insert Into tblTransactionPayFees (CCF_TransactionPayID, CCF_CompanyID, CCF_CurrencyID, CCF_PaymentMethod, CCF_ExchangeTo, CCF_MaxAmount, CCF_PercentFee, CCF_FixedFee, CCF_ApproveFixedFee, CCF_RefundFixedFee, CCF_ClarificationFee, CCF_CBFixedFee, CCF_ListBINs)" +
                        "Select " + PayID + ",CCF_CompanyID, CCF_CurrencyID, CCF_PaymentMethod, CCF_ExchangeTo, CCF_MaxAmount, CCF_PercentFee, CCF_FixedFee, CCF_ApproveFixedFee, CCF_RefundFixedFee, CCF_ClarificationFee, CCF_CBFixedFee, CCF_ListBINs From tblCompanyCreditFees Where CCF_CompanyID=" + CompanyID + " And CCF_ExchangeTo=" + Currency);
                }
                catch (Exception ex)
                {
                    Logger.Log(ex);
                }
            }
            if (setting != null)
            {
                AddTransactions(setting);
                AddSpecialAdminTransAmount(setting);
            }
            if (bApply) return UpdatePayment();
            return false;
        }

        public int RRID { get { return rrTransID; } }

        private bool UpdatePayment()
        {
            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
            ////Logger.Log(LogSeverity.Info, LogTag.Wires, "Inside Totals.UpdatePayment(private bool-line 586) function");

            if (rrTransID > 0)
            {
                if (RollingReserve != 0) Helpers.ExecSql("Update tblCompanyTransPass Set Amount=" + (-RollingReserve) + " Where ID=" + rrTransID);
                else Helpers.ExecSql("Delete From tblCompanyTransPass Where ID=" + rrTransID);
            }
            else
            {
                if (RollingReserve != 0) rrTransID = AddAdminTransAmount(null, RollingReserve, "Rolling Reserve (" + (rrState == 4 ? "fixed external" : secDeposit + "%") + ")", PaymentMethodEnum.RollingReserve, 5);
            }

            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
            ////Logger.Log(LogSeverity.Info, LogTag.Wires, "Inside Totals.UpdatePayment(private bool-line 600) function before hasrows condition, hasrows=" + hasRows.ToString());

            if (hasRows)
            {
                /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
                ////Logger.Log(LogSeverity.Info, LogTag.Wires, "Inside hasRows condition, hasrows=" + hasRows.ToString());
                try
                {
                    Helpers.ExecSql("UPDATE tblTransactionPay SET transTotal=" + TotalAmount + ", TransChargeTotal=" + TotalFeesAmount + ", TransPayTotal=" + Total + ", transRollingReserve=" + RollingReserve + " WHERE ID=" + PayID);
                    UpdateTotals();
                }
                catch (Exception ex)
                {
                    Logger.Log(ex);
                }
                //oledbData.execute "UPDATE tblWireMoney SET WireAmount=" + Total + "-wireFee WHERE WireType=1 AND WireSourceTbl_id=" + mPayID
                return true;
            }
            else
            {
                Logger.Log(LogSeverity.Info, LogTag.Wires, "After hasRows condition, hasRows was false, deleting settlement and wire.");

                Helpers.ExecSql("DELETE FROM tblTransactionPay WHERE ID=" + PayID);
                Helpers.ExecSql("DELETE FROM Finance.Wire WHERE MerchantSettlement_id=" + PayID);
                //Helpers.ExecSql("DELETE FROM tblWireMoney WHERE WireType=1 AND WireSourceTbl_id=" + PayID);

                //PayID = 0;
                return false;
            }
        }

        private void UpdateTotals()
        {
            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
            ////Logger.Log(LogSeverity.Info, LogTag.Wires, "Inside Totals.UpdateTotals() function.");

            var gPMTotal = PaymentMethodTotal(PaymentMethodEnum.CC_MIN, PaymentMethodEnum.EC_MAX);
            string updateSql = "UPDATE tblTransactionPay SET " +
                " transTotal=" + TotalAmount +
                ", TransChargeTotal=" + TotalFeesAmount +
                ", TransPayTotal=" + Total +
                ", transRollingReserve=" + RollingReserve +
                ", TotalCaptureCount=" + gPMTotal.NormCount +
                ", TotalCaptureAmount=" + gPMTotal.NormAmount +
                ", TotalAdminCount=" + Admin.TotalCount +
                ", TotalAdminAmount=" + Admin.TotalAmount +
                ", TotalSystemCount=" + FeesAutoAndManual.TotalCount +
                ", TotalSystemAmount=" + FeesAutoAndManual.TotalAmount +
                ", TotalRefundCount=" + gPMTotal.RefCount +
                ", TotalRefundAmount=" + -gPMTotal.RefAmount +
                ", TotalCashbackCount=" + gPMTotal.CashbackCount +
                ", TotalCashbackAmount=" + -gPMTotal.CashbackAmount +
                ", TotalChbCount=" + gPMTotal.DenCount +
                ", TotalChbAmount=" + gPMTotal.DenAmount +
                ", TotalFeeProcessCapture=" + gPMTotal.NormFees + gPMTotal.RefFees +
                ", TotalFeeClarification=" + Totals.ClrfFees +
                ", TotalFeeFinancing=" + Totals.FinanceFee +
                ", TotalFeeHandling=" + Totals.HandlingFee +
                ", TotalFeeBank=" + -BankFees.TotalAmount +
                ", TotalFeeChb=" + gPMTotal.DenFees +
                ", TotalRollingReserve=" + -RollingReserve +
                ", TotalRollingRelease=" + ReserveReturn +
                ", TotalDirectDeposit=" + TotalNotPayedAmount +
                " WHERE ID=" + PayID;
            //Response.Write(updateSql & "<br />")
            Helpers.ExecSql(updateSql);
        }

        public void Delete()
        {
            ClacDeletePayment(PayID, CompanyID, false);
            PayID = 0;
        }

        public int AddAdminTransAmount(CAmount pAmountT, decimal nAmount, string nCommant, PaymentMethodEnum nPaymentMethod, int transTypeID)
        {
            decimal xAmount;
            string amountField;
            if (nAmount != 0m)
            {
                switch (nPaymentMethod)
                {
                    case PaymentMethodEnum.SystemFees:
                    case PaymentMethodEnum.ManualFees:
                        if (pAmountT != null)
                        {
                            pAmountT.NormCount = pAmountT.NormCount + 1;
                            pAmountT.NormFees = pAmountT.NormFees + nAmount;
                            pAmountT.NormLineFee = pAmountT.NormLineFee + nAmount;
                        }
                        amountField = "netpayFee_transactionCharge";
                        break;
                    default: //PMD_RolRes
                        if (pAmountT != null)
                        {
                            pAmountT.NormCount = pAmountT.NormCount + 1;
                            pAmountT.NormAmount = pAmountT.NormAmount + nAmount;
                        }
                        amountField = "Amount";
                        break;
                }
                hasRows = true;
                if (apply)
                {
                    xAmount = (nAmount < 0 ? -nAmount : nAmount);
                    Helpers.ExecSql("INSERT INTO tblCompanyTransPass(companyID, InsertDate, PrimaryPayedID, payID, TransSource_id, CreditType, IPAddress, " + amountField + ", Currency, OCurrency, Comment, PaymentMethod, PaymentMethod_id, paymentMethodId, paymentMethodDisplay, UnsettledAmount, UnsettledInstallments)" +
                        " VALUES(" + CompanyID + ", getDate(), " + PayID + ",';" + PayID + ";', " + transTypeID + ", " + (nAmount < 0 ? 0 : 1) + ", '" + "', " + xAmount + ", " + Currency + "," + Currency + ", '', " + (int)nPaymentMethod + ", 3, 0, '" + nCommant.Replace("'", "''") + "', 0, 0)");
                    return Helpers.TestVar(Helpers.ExecScalar("Select Max(ID) From tblCompanyTransPass Where PrimaryPayedID=" + PayID), 0, -1, 0);
                }
            }

            return 0;
        }

        public int AddSqlAdminTransAmount(CAmount pAmountT, string nSQL, string nCommant, int transTypeID)
        {
            //oledbData.CommandTimeout = 600
            reader = Helpers.ExecReader(nSQL);
            try
            {
                if (reader.Read())
                {
                    nCommant = nCommant.Replace("%1", Helpers.TestVar(reader[1], -1, ""));
                    if (reader.FieldCount > 2) nCommant = nCommant.Replace("%2", Helpers.TestVar(reader[2], -1, ""));
                    return AddAdminTransAmount(pAmountT, Helpers.TestVar(reader[0], 0m, -1m, 0m), nCommant, PaymentMethodEnum.SystemFees, transTypeID);
                }
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                    reader.Close();
            }

            return 0;
        }

        private void AddSpecialAdminTransAmount(CreateSettings settings)
        {
            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
            ////Logger.Log(LogSeverity.Info, LogTag.Wires, "Inside Totals.AddSpecialAdminTransAmount function");

            try
            {
                if (settings.IncApprovalFee || !apply)
                {
                    AddSqlAdminTransAmount(GetAmountByPaymentMethod(PaymentMethodEnum.SystemFees, 0), "Select Sum(netpayFee_transactionCharge), Count(*) From tblCompanyTransApproval WHERE PayID=0 AND CompanyID=" + CompanyID + " And Currency=" + Currency,
                        "Pre-Authorized fee (%1)", 1);
                    if (apply) Helpers.ExecSql("Update tblCompanyTransApproval Set PayID=" + PayID + " WHERE PayID=0 AND CompanyID=" + CompanyID + " And Currency=" + Currency);
                }
                if (settings.IncFailFee || !apply)
                {
                    AddSqlAdminTransAmount(GetAmountByPaymentMethod(PaymentMethodEnum.SystemFees, 0), "Select Sum(netpayFee_transactionCharge), Count(*) From tblCompanyTransFail WHERE PayID=0 AND CompanyID=" + CompanyID + " And Currency=" + Currency + " And netpayFee_transactionCharge <> 0",
                        "Rejected fee (%1)", 25);
                    if (apply) Helpers.ExecSql("Update tblCompanyTransFail Set PayID=" + PayID + " WHERE PayID=0 AND CompanyID=" + CompanyID + " And Currency=" + Currency);
                }

                if (settings.IncStorageFee || !apply)
                {
                    AddSqlAdminTransAmount(GetAmountByPaymentMethod(PaymentMethodEnum.SystemFees, 0), "SELECT (Count(*) * StorageFee), Count(*), StorageFee FROM tblCCStorage Left Join Setting.SetMerchantSettlement mcs ON(tblCCStorage.companyID = mcs.Merchant_ID And mcs.Currency_ID=" + Currency + ") WHERE PayID=0 AND CompanyID=" + CompanyID + " Group By StorageFee",
                        "CC storage fee (" + Bll.Currency.Get(Currency).Name + "%2 X %1)", 2);
                    if (apply) Helpers.ExecSql("Update tblCCStorage Set PayID=" + PayID + " WHERE PayID=0 AND CompanyID=" + CompanyID);
                }
                if (settings.RetRR || !apply) CalcRetReserve();
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }
        }

        public void AddRetReserve(int nID, bool bApply)
        {
            bool oApply = apply;
            try
            {
                reader = Helpers.ExecReader("Select ID, Amount, InsertDate From tblCompanyTransPass Where " +
                "CompanyID=" + CompanyID + " And Currency=" + Currency + " And OriginalTransID=0 And ID=" + nID);
                if (reader.Read()) { apply = bApply; IAddRetReserve(reader); }
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }
            finally
            {
                reader.Close();
                apply = oApply;
            }
        }

        private int IAddRetReserve(IDataReader iRs)
        {
            int pRet = AddAdminTransAmount(null, Helpers.TestVar(iRs["Amount"], 0m, -1m, 0m), "Release Reserve (" + Helpers.TestVar(iRs["InsertDate"], DateTime.MinValue, DateTime.MaxValue, DateTime.MinValue).ToString(DateTimeFormat) + ")", PaymentMethodEnum.RollingReserve, 5);
            rrRetAmount += Helpers.TestVar(iRs["Amount"], 0m, -1m, 0m);
            rrRetCount++;
            if (apply) Helpers.ExecSql("Update tblCompanyTransPass Set OriginalTransID=" + pRet + " Where ID=" + iRs["ID"]);
            return pRet;
        }

        private void CalcRetReserve()
        {
            if ((rrState == 1 && (secFlags & 2) > 0) || rrState == 2 || rrState == 3)
            {
                try
                {
                    reader = Helpers.ExecReader("Select ID, Amount, Currency, InsertDate From tblCompanyTransPass Where " +
                    "CompanyID=" + CompanyID + " And Currency=" + Currency + " And PaymentMethod=" + (int)PaymentMethodEnum.RollingReserve + " And CreditType=0 And OriginalTransID=0 And Cast(InsertDate as date) <= convert(datetime, '" + PayDate.AddMonths(-secPeriod).ToString(DateTimeFormat) + "', 120)");
                    while (reader.Read())
                    {
                        if (rrState == 3)
                        {
                            decimal nItemValue;
                            if (secKeepCurrency != 255) nItemValue = new Money(Helpers.TestVar(reader["Amount"], 0m, -1m, 0m), Bll.Currency.Get(Helpers.TestVar(reader["Currency"], 0, -1, 0))).ConvertTo(Bll.Currency.Get(secKeepCurrency)).Amount;
                            else nItemValue = Helpers.TestVar(reader["Amount"], 0m, -1m, 0m);
                            if (curRes < secKeepAmount) break;
                            curRes = curRes - nItemValue;
                        }
                        IAddRetReserve(reader);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(ex);
                }
                finally
                {
                    reader.Close();
                }
            }
        }

        public CAmount GetAmountByPaymentMethod(PaymentMethodEnum pm, int oldPM)
        {
            if (pm == PaymentMethodEnum.Unknown)
            { //old style detection
                if (oldPM == 1) return GetAmountByPaymentMethod(PaymentMethodEnum.CC_MIN, 0);
                else if (oldPM == 2) return GetAmountByPaymentMethod(PaymentMethodEnum.EC_MIN, 0);
                else return GetAmountByPaymentMethod(PaymentMethodEnum.ManualFees, 0);
            }
            else
            {
                if (PaymentMethodAmount[(int)pm] == null) PaymentMethodAmount[(int)pm] = new CAmount();
                return PaymentMethodAmount[(int)pm];
            }
        }

        private void AddCountTransactions(string xWhere)
        {
            xWhere = "SELECT CreditType, IsCashback, PaymentMethod," +
                " Count(*) As tVal, Sum(Amount) As Amount," +
                " Sum(netpayFee_transactionCharge) As netpayFee_transactionCharge, Sum(netpayFee_ratioCharge) As netpayFee_ratioCharge, " + (PayID <= 0 ? "ep.IsPaid" : "0") + " as BankPaid, " +
                " Sum(DebitFee) As DebitFee" +
                " FROM tblCompanyTransPass " +
                " Left Join tblLogImportEPA As ep ON(ep.TransID = tblCompanyTransPass.id And ep.Installment = 1) " +
                " WHERE " + xWhere +
                " Group By CreditType, IsCashback, PaymentMethod" + (PayID <= 0 ? ", ep.IsPaid" : "");
            try
            {
                /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
                ////Logger.Log(LogSeverity.Info, LogTag.Wires, "", "Inside Totals.AddCountTransactions(), executing query:" + xWhere + ", if results found , hasRows will be true.");

                reader = Helpers.ExecReader(xWhere);
                while (reader.Read())
                {
                    hasRows = true;
                    CAmount pSumItem = GetAmountByPaymentMethod((PaymentMethodEnum)Helpers.TestVar(reader["PaymentMethod"], 0, -1, 0), 1);
                    if (Helpers.TestVar(reader["CreditType"], 0, -1, 0) == 0)
                    {
                        if (reader["IsCashback"].ToNullableBool().GetValueOrDefault())
                        {
                            pSumItem.CashbackAmount -= Helpers.TestVar(reader["Amount"], 0m, -1m, 0m);
                            pSumItem.CashbackCount += Helpers.TestVar(reader["tVal"], 0, -1, 0);
                            pSumItem.CashbackFees += Helpers.TestVar(reader["netpayFee_transactionCharge"], 0m, -1m, 0m) + Helpers.TestVar(reader["netpayFee_ratioCharge"], 0m, -1m, 0m);
                        }
                        else
                        {
                            pSumItem.RefAmount -= Helpers.TestVar(reader["Amount"], 0m, -1m, 0m);
                            pSumItem.RefCount += Helpers.TestVar(reader["tVal"], 0, -1, 0);
                            pSumItem.RefFees += Helpers.TestVar(reader["netpayFee_transactionCharge"], 0m, -1m, 0m) + Helpers.TestVar(reader["netpayFee_ratioCharge"], 0m, -1m, 0m);
                            pSumItem.RefDebitFee += Helpers.TestVar(reader["DebitFee"], 0, -1, 0);
                        }
                    }
                    else
                    {
                        pSumItem.NormAmount += Helpers.TestVar(reader["Amount"], 0m, -1m, 0m);
                        pSumItem.NormCount += Helpers.TestVar(reader["tVal"], 0, -1, 0);
                        pSumItem.NormFees += Helpers.TestVar(reader["netpayFee_transactionCharge"], 0m, -1m, 0m) + Helpers.TestVar(reader["netpayFee_ratioCharge"], 0m, -1m, 0m);
                        pSumItem.NormLineFee += Helpers.TestVar(reader["netpayFee_transactionCharge"], 0m, -1m, 0m);
                        pSumItem.NormDebitFee += Helpers.TestVar(reader["DebitFee"], 0, -1, 0);
                        if (Helpers.TestVar(reader["BankPaid"], false))
                        {
                            pSumItem.BankPaidCount += Helpers.TestVar(reader["tVal"], 0, -1, 0);
                            pSumItem.BankPaidAmount += Helpers.TestVar(reader["Amount"], 0m, -1m, 0m);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }
            finally
            {
                reader.Close();
            }
        }

        private string GetTransIDUpTo(string xWhere, decimal nAmount)
        {
            string pRet = "";
            if (xWhere != "") xWhere = " And " + xWhere;
            xWhere = "CompanyID=" + CompanyID + " And Currency=" + Currency + " And PayID LIKE ('%;" + (apply ? 0 : PayID) + ";%')" + xWhere;
            xWhere = "Select ID, UnsettledAmount, UnsettledInstallments From tblCompanyTransPass Where " + xWhere + " And CreditType IN(1,6,8) And PaymentMethod > " + (int)PaymentMethodEnum.MaxInternal + " And deniedStatus=0 Order By ID Desc";
            try
            {
                reader = Helpers.ExecReader(xWhere);
                while (reader.Read())
                {
                    if (Helpers.TestVar(reader["UnsettledInstallments"], 0, -1, 0) > 0) nAmount = nAmount - (Helpers.TestVar(reader["UnsettledAmount"], 0m, -1m, 0m) / Helpers.TestVar(reader["UnsettledInstallments"], 0m, -1m, 0m));
                    if (nAmount < 0) break;
                    pRet += "," + reader["ID"];
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }
            finally
            {
                reader.Close();
            }

            if (pRet.Length > 1) pRet = pRet.Substring(1);
            return pRet;
        }

        public void AddTransactions(CreateSettings settings)
        {
            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
            ////Logger.Log(LogSeverity.Info, LogTag.Wires, "Inside Totals.AddTransactions function");

            string sWhere, sWhereNot;
            int xPayID, lTransID = 0;
            bool bFirstUnpayIns = false;
            //oleDbData.CommandTimeout = 180
            xPayID = (apply ? 0 : PayID);
            var xWhere = "";
            if (settings != null) xWhere = GetWhereFilters(settings);
            if (!string.IsNullOrEmpty(xWhere)) xWhere = " And " + xWhere;
            sWhere = "tblCompanyTransPass.CompanyID=" + CompanyID + " And Currency=" + Currency + " And (tblCompanyTransPass.PayID LIKE ('%;" + xPayID + ";%')";
            if (useImprov)
            { //use improvments
                /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
                //Logger.Log(LogSeverity.Info, LogTag.Wires, "Inside Totals.AddTransactions(), inside useImprov condition, apply=" + apply.ToString());
                sWhereNot = " (deniedStatus=0 And CreditType<>8 And PaymentMethod > " + (int)PaymentMethodEnum.MaxInternal + " And HandlingFee=0 And Interest=0)";
                AddCountTransactions(sWhere + ") And " + sWhereNot + xWhere);
                if (apply)
                {
                    string sSql = "UPDATE tblCompanyTransPass SET PrimaryPayedID=" + PayID + ", payID=';" + PayID + ";', MerchantRealPD='" + PayDate.ToString(DateTimeFormat) + "'" +
                        " FROM tblCompanyTransPass Left Join tblLogImportEPA As ep ON(ep.TransID = tblCompanyTransPass.id And ep.Installment = 1)" +
                        " WHERE " + sWhere + ") And " + sWhereNot + xWhere;

                    Logger.Log(LogSeverity.Info, LogTag.Wires, "Invoke the Update query inside the 'if (apply)' condition", "After 'if (apply) condition, invoking update query:" + sSql);

                    Helpers.ExecSql(sSql);
                }
                xWhere = " And (Not " + sWhereNot + ")" + xWhere;
            }
            if (xPayID == 0) sWhere = sWhere + " OR DeniedStatus = 2";
            sWhere = sWhere + ")" + xWhere;
            if (apply) sWhere = sWhere + " And (p1.payID=0 Or p1.transAnsID Is Null)";

            try
            {
                string sql = "SELECT tblCompanyTransPass.*, p1.id As IID, p1.InsID, p1.payID As IPayID, p1.MerchantPD As IMerchantPD, p1.Amount As IAmount, " + (PayID <= 0 ? "ep.IsPaid" : "0") + " as BankPaid " +
                " FROM tblCompanyTransPass " +
                " Left Join tblCompanyTransInstallments As p1 ON(p1.transAnsID = tblCompanyTransPass.id) " +
                " Left Join tblLogImportEPA As ep ON(ep.TransID = tblCompanyTransPass.id And IsNull(p1.InsID, 1) = ep.Installment) " +
                " Where " + sWhere + " Order By tblCompanyTransPass.ID Asc";

                /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
                //Logger.Log(LogSeverity.Info, LogTag.Wires, "Inside Totals.AddTransactions(), execute Select query", "Inside Totals.AddTransactions(), executing query:" + sql + " if no transaction will be found hasRows is false");

                reader = Helpers.ExecReader(sql);
                while (reader.Read())
                {
                    /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
                    //Logger.Log(LogSeverity.Info, LogTag.Wires, "inside AddTransactions() - 'while (reader.Read())' loop");

                    PaymentMethodEnum paymentMethod = (PaymentMethodEnum)Helpers.TestVar(reader["PaymentMethod"], 0, -1, 0);
                    if (paymentMethod == PaymentMethodEnum.RollingReserve)
                    {
                        /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
                        //Logger.Log(LogSeverity.Info, LogTag.Wires, "inside AddTransactions() - passed 'if (paymentMethod == PaymentMethodEnum.RollingReserve)' condition");

                        if (Helpers.TestVar(reader["CreditType"], 0, -1, 0) == 0)
                        {
                            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
                            //Logger.Log(LogSeverity.Info, LogTag.Wires, "inside AddTransactions() - passed 'if (paymentMethod == PaymentMethodEnum.RollingReserve)' condition - Passed the 'if (Helpers.TestVar(reader['CreditType'], 0, -1, 0) == 0)'");

                            rrTransID = Helpers.TestVar(reader["ID"], 0, -1, 0);
                            rrAmount -= Helpers.TestVar(reader["Amount"], 0m, -1m, 0m);
                        }
                        else
                        {
                            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
                            //Logger.Log(LogSeverity.Info, LogTag.Wires, "inside AddTransactions() - passed 'if (paymentMethod == PaymentMethodEnum.RollingReserve)' condition - didn't Passed the 'if (Helpers.TestVar(reader['CreditType'], 0, -1, 0) == 0)', go to else, hasRows=true");

                            hasRows = true;
                            rrRetAmount += Helpers.TestVar(reader["Amount"], 0m, -1m, 0m);
                            rrRetCount++;
                        }
                    }
                    else
                    {
                        /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
                        //Logger.Log(LogSeverity.Info, LogTag.Wires, "inside AddTransactions() - didn't passed the 'if (paymentMethod == PaymentMethodEnum.RollingReserve)' condition, go to else, hasRows=true");

                        hasRows = true;
                        if (lTransID != Helpers.TestVar(reader["ID"], 0, -1, 0)) bFirstUnpayIns = true;
                        if (!((paymentMethod == PaymentMethodEnum.Admin) && (PayID == 0) && (Helpers.TestVar(reader["CreditType"], 0, -1, 0) == 1) && (Helpers.TestVar(reader["Amount"], 0m, -1m, 0m) > 100) && (Helpers.TestVar(reader["InsertDate"], DateTime.MinValue, DateTime.MaxValue, DateTime.MinValue) < (new DateTime(2012, 1, 1)))))
                            GetAmountByPaymentMethod(paymentMethod, Helpers.TestVar(reader["PaymentMethod_id"], 0, -1, 0)).AddTransaction(settings, reader, PayID, PayDate, prime, apply, bFirstUnpayIns);
                        if (Helpers.TestVar(reader["IPayID"], 0, -1, 0) == 0) bFirstUnpayIns = false;
                    }
                    lTransID = Helpers.TestVar(reader["ID"], 0, -1, 0);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }
            finally
            {
                reader.Close();
            }
        }

        public bool HasRows { get { return hasRows; } }
        public bool HasPaymentMethodTotal(PaymentMethodEnum lPM) { return PaymentMethodAmount[(int)lPM] != null; }

        public CAmount PaymentMethodTotal(PaymentMethodEnum lMin, PaymentMethodEnum lMax)
        {
            CAmount pRet = new CAmount();
            for (int i = (int)lMin; i <= (int)lMax; i++)
                if (PaymentMethodAmount[i] != null) pRet.AddAmount(PaymentMethodAmount[i]);


            //If invoking this function with the following values , need to add 
            //The loop through the Prepaid items.
            if (lMin == PaymentMethodEnum.CC_MIN && lMax == PaymentMethodEnum.EC_MAX)
            {
                for (int i = (int)PaymentMethodEnum.PP_MIN; i <= (int)PaymentMethodEnum.PP_MAX; i++)
                    if (PaymentMethodAmount[i] != null) pRet.AddAmount(PaymentMethodAmount[i]);
            }

            return pRet;
        }

        //public CAmount Totals
        //{
        //    get
        //    {
        //        var amount = PaymentMethodTotal(0, PaymentMethodEnum.EC_MAX);
        //        amount.AddAmount(PaymentMethodTotal(PaymentMethodEnum.PP_MIN, PaymentMethodEnum.PP_MAX));
        //        return amount;
        //    }
        //}

        public CAmount Totals
        {
            get
            {
                return PaymentMethodTotal(0, PaymentMethodEnum.PaymentMethodMax);
            }
        }


        public decimal RollingReserve
        {
            set { if (rrState == 4) rrAmount = -value; }
            get
            {
                decimal pRet = 0;
                if (PayID > 0 && !apply) pRet = rrAmount;
                else if (rrState == 4) pRet = rrAmount;
                else
                {
                    if (rrState == 2)
                    {
                        string sSQL = "Select TOP 1 ID From tblCompanyTransPass Where CompanyID=" + CompanyID +
                             " And PaymentMethod=" + (int)PaymentMethodEnum.RollingReserve + " And CreditType=0 And InsertDate < convert(datetime, '" + PayDate.AddMonths(-secPeriod).ToString(DateTimeFormat) + "', 120)";
                        if (Helpers.TestVar(Helpers.ExecScalar(sSQL), 0, -1, 0) > 0) secFlags &= ~1;
                        else secFlags |= 1;
                    }
                    var RRAmt = PaymentMethodTotal(PaymentMethodEnum.CC_MIN, PaymentMethodEnum.EC_MAX).NormAmount; //* (mPayPercent / 100)
                    if ((((rrState == 1 || rrState == 2) && ((secFlags & 1) == 1)) || rrState == 3) && (incRR || !apply) && RRAmt > 0)
                    {
                        if (rrState == 1)
                        {
                            pRet = -(RRAmt * (secDeposit / 100));
                        }
                        else
                        {
                            decimal nMax = (secKeepAmount - curRes);
                            if (nMax < 0)
                            {
                                pRet = 0;
                            }
                            else
                            {
                                pRet = (RRAmt * (secDeposit / 100));
                                if (nMax < pRet) pRet = nMax;
                                pRet = -pRet;
                            }
                        }
                    }
                }
                return pRet;
            }
        }

        public decimal ReserveReturn { get { return rrRetAmount; } }
        public decimal ReserveTotal { get { return RollingReserve + rrRetAmount; } }
        //public decimal TotalFees { get { return Totals.TotalFees + Totals.HandlingFee; } }
        public CAmount TotalProcess
        {
            get
            {
                var ret = PaymentMethodTotal(PaymentMethodEnum.CC_MIN, PaymentMethodEnum.EC_MAX);
                ret.AddAmount(BankFees);
                return ret;
            }
        }
        public CAmount Admin { get { return PaymentMethodTotal(PaymentMethodEnum.Admin, PaymentMethodEnum.Admin); } }

        public CAmount FeesAutoAndManual
        {
            get
            {
                CAmount pRet = PaymentMethodTotal(PaymentMethodEnum.ManualFees, PaymentMethodEnum.ManualFees);
                pRet.AddAmount(PaymentMethodTotal(PaymentMethodEnum.SystemFees, PaymentMethodEnum.SystemFees));
                return pRet;
            }
        }

        public CAmount BankFees { get { return PaymentMethodTotal(PaymentMethodEnum.BankFees, PaymentMethodEnum.BankFees); } }

        public decimal TotalFeesAmount { get { return Totals.TotalFees + Totals.HandlingFee; } }
        public decimal TotalProcessAmount { get { return TotalProcess.TotalAmount; } }
        public decimal TotalPayedAmount { get { return (TotalProcessAmount * (payPercent / 100)); } }
        public decimal TotalAmount { get { return TotalPayedAmount + ReserveTotal + Admin.TotalAmount + FeesAutoAndManual.TotalAmount; } }

        public decimal TotalNotPayedAmount
        {
            get
            {
                if (payPercent == 100m) return 0m;
                return (TotalProcessAmount * (NotPayPercent / 100));
            }
        }
        public decimal NotPayPercent { get { return 100 - payPercent; } }
        public decimal VATAmount { get { return TotalFeesAmount * (VAT + 1); } }
        public decimal Total { get { return TotalAmount - VATAmount; } }


        private string GetWhereFilters(CreateSettings create)
        {
            var sWhere = new System.Text.StringBuilder();
            //sWhere = " (DeniedStatus = 0) AND (Payments = 1) And (isTestOnly = 0)"
            if (!create.AutoChb) sWhere.Append(" And (DeniedStatus = 0) ");
            if (!create.AutoInstallements) sWhere.Append(" And (Payments = 1) ");
            if (!create.IncTestTransactions) sWhere.Append(" And (isTestOnly = 0) ");
            if (create.ForceEpaPaid)
                sWhere.Append(" And ((ep.IsPaid = 1 Or ep.IsRefunded = 1) Or PaymentMethod <= " + (int)CommonTypes.PaymentMethodEnum.MaxInternal + ") ");

            if (create.PaymentPerPm) sWhere.Append(" And (PaymentMethod=" + (int)create.PaymentMethods.First() + ") ");

            //if (Request("DateUsage") == "0") {
            if (create.TransDate.From.HasValue) sWhere.Append(" And tblCompanyTransPass.InsertDate >='" + create.TransDate.From.Value.Date.ToString(CPayment.DateTimeFormat) + " 00:00:00" + "'");
            if (create.TransDate.To.HasValue) sWhere.Append(" And tblCompanyTransPass.InsertDate <='" + create.TransDate.To.Value.Date.ToString(CPayment.DateTimeFormat) + " 23:59:59" + "'");
            //} else {
            if (create.TransPD.From.HasValue) sWhere.Append(" And tblCompanyTransPass.MerchantPD >='" + create.TransPD.From.Value.Date.ToString(CPayment.DateTimeFormat) + " 00:00:00" + "'");
            if (create.TransPD.To.HasValue) sWhere.Append(" And tblCompanyTransPass.MerchantPD <='" + create.TransPD.To.Value.Date.ToString(CPayment.DateTimeFormat) + " 23:59:59" + "'");
            //}
            if (!create.IncNormalCharge || !create.IncRefund)
            {
                sWhere.Append(" And tblCompanyTransPass.CreditType IN(");
                if (create.IncRefund) sWhere.Append("0,");
                else if (create.IncNormalCharge) sWhere.Append("1, 2, 6, 8,");
                else sWhere.Append("255,");
                sWhere.Remove(sWhere.Length - 1, 1).Append(")");
            }

            if (!create.IncProcessTrans || !create.IncAdminTrans)
            {
                if (create.IncProcessTrans) sWhere.Append(" And (tblCompanyTransPass.PaymentMethod > " + (int)CommonTypes.PaymentMethodEnum.MaxInternal + ")");
                else if (create.IncAdminTrans) sWhere.Append(" And (tblCompanyTransPass.PaymentMethod <= " + (int)CommonTypes.PaymentMethodEnum.MaxInternal + ")");
                else sWhere.Append(" And (tblCompanyTransPass.PaymentMethod < 0)");
            }
            if (sWhere.Length > 5) sWhere.Remove(0, 5);

            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
            //Logger.Log(LogSeverity.Info, LogTag.Wires, "Inside GetWhereFilters()", "WhereFilters=" + sWhere.ToString());

            return sWhere.ToString();
        }


        public static CPayment Load(int payID, int? companyID)
        {
            CPayment pRet = null;
            IDataReader reader = null;
            try
            {
                reader = Helpers.ExecReader("SELECT tblTransactionPay.*, tblCompany.CompanyName, tblCompany.SecurityPeriod, tblCompany.RREnable, tblCompany.RRAutoRet, tblCompany.RRKeepAmount, tblCompany.RRKeepCurrency, tblCompany.RRState " +
                    " FROM tblTransactionPay Left Join tblCompany ON(tblCompany.ID = tblTransactionPay.CompanyID)" +
                    " WHERE tblTransactionPay.id=" + payID);
                if (reader.Read()) pRet = new CPayment(reader);
            }

            catch (Exception ex)
            {
                Logger.Log(ex);
            }
            finally
            {
                if (reader != null) reader.Close();
            }
            if (pRet != null) pRet.AddTransactions(null);
            return pRet;
        }

        public static CPayment Create(int companyID, int currencyId)
        {
            CPayment pRet = null;
            IDataReader reader = null;
            try
            {
                reader = Helpers.ExecReader("SELECT tblCompany.*, tblBillingCompanys.VATamount " +
                    " FROM tblCompany Left Join tblBillingCompanys ON(tblCompany.BillingCompanys_id = tblBillingCompanys.BillingCompanys_id) WHERE ID=" + companyID);
                if (reader.Read()) pRet = new CPayment(reader, currencyId, DateTime.Now);
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }
            finally
            {
                reader.Close();
            }
            return pRet;
        }

        public static void ClacDeletePayment(int nPayID, int? nCompanyID, bool rowsOnly)
        {
            if (Netpay.Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
            {
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Settlements.Settlement.SecuredObject, PermissionValue.Delete);
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Settlements.UpcomingSettlement.SecuredObject, PermissionValue.Delete);
            }

            if (nCompanyID == null) nCompanyID = (from t in DataContext.Reader.tblTransactionPays where t.id == nPayID select t.CompanyID).SingleOrDefault();

            // use transaction scope, if an error happens in one of the statements below,
            // rollback will be performed.
            using (TransactionScope scope = new TransactionScope())
            {                    
                Helpers.ExecSql("DELETE From tblCompanyTransPass WHERE CompanyID=" + nCompanyID + " And PrimaryPayedID=" + nPayID + " And TransSource_id IN(1, 2, 25)");
                Helpers.ExecSql("UPDATE tblCompanyTransPass SET PrimaryPayedID=0, payID=';0;', MerchantRealPD=Null, Interest=0 WHERE CompanyID=" + nCompanyID + " And PrimaryPayedID=" + nPayID + " And PaymentMethod<>" + (int)CommonTypes.PaymentMethodEnum.RollingReserve + " And CreditType<>8");
                Helpers.ExecSql("UPDATE tblCompanyTransPass SET payID=Replace(Replace(payID, ';" + nPayID + ";', ';0;'), ';" + nPayID + ";', ';0;') WHERE CompanyID=" + nCompanyID + " And ID IN(Select TransAnsID From tblCompanyTransInstallments WHERE CompanyID=" + nCompanyID + " And payID=" + nPayID + ")");
                Helpers.ExecSql("UPDATE tblCompanyTransPass SET PrimaryPayedID=0 WHERE PrimaryPayedID=" + nPayID + " And CreditType=8");
                Helpers.ExecSql("UPDATE tblCompanyTransInstallments SET payID=0 WHERE CompanyID=" + nCompanyID + " And payID=" + nPayID);
                Helpers.ExecSql("UPDATE tblCompanyTransFail SET PayID=0 WHERE CompanyID=" + nCompanyID + " And PayID=" + nPayID);
                Helpers.ExecSql("UPDATE tblCompanyTransApproval SET PayID=0 WHERE CompanyID=" + nCompanyID + " And PayID=" + nPayID);
                Helpers.ExecSql("UPDATE tblCCStorage SET PayID=0 WHERE CompanyID=" + nCompanyID + " And PayID=" + nPayID);
                Helpers.ExecSql("UPDATE tblCompanyTransPass Set OriginalTransID=0 Where OriginalTransID IN(Select ID From tblCompanyTransPass Where PrimaryPayedID=" + nPayID + ")");
                Helpers.ExecSql("DELETE From tblCompanyTransPass WHERE PrimaryPayedID=" + nPayID + " And PaymentMethod=" + (int)CommonTypes.PaymentMethodEnum.RollingReserve);
                if (rowsOnly) return;

                
                Helpers.ExecSql("DELETE FROM Finance.Wire WHERE MerchantSettlement_id=" + nPayID);
                Helpers.ExecSql("DELETE FROM tblTransactionPay WHERE ID=" + nPayID);
                scope.Complete();
            }
        }
    }



    public class AffiliateAmount
    {
        public decimal Amount, FeePercent;
        public string CreditInfo;
        public decimal Total
        {
            get
            {
                if (FeePercent == 0m) return 0m;
                return Amount * (FeePercent / 100);
            }
        }
    }

    public class AffiliateFeeItem
    {
        public PaymentMethodEnum PaymentMethod;
        public System.Collections.Generic.List<AffiliateAmount> Items = new List<AffiliateAmount>();
        public decimal CreditAmount;

        public AffiliateAmount AddAmount(decimal nPercent, decimal nAmount, string creditInfo)
        {
            AffiliateAmount pItem = new AffiliateAmount();
            Items.Add(pItem);
            pItem.Amount = nAmount;
            pItem.FeePercent = nPercent;
            pItem.CreditInfo = creditInfo;
            return pItem;
        }

        public decimal Total
        {
            get
            {
                decimal pRet = 0;
                for (int i = 0; i < Items.Count; i++) pRet = pRet + Items[i].Total;
                return pRet;
            }
        }
    }

    public class AffiliateTotalLine
    {
        public string Text;
        public int Quantity;
        public decimal Amount;
        public decimal Total { get { return Quantity * Amount; } }
    }

    public class AffiliateFees
    {
        public int AffiliateID, AFP_ID;
        public CPayment Payment;
        public AffiliateFeeItem[] PaymentMethodAmount;
        public AffiliateTotalLine[] PaymentLines;

        private AffiliateFees(CPayment xPayment) { Payment = xPayment; PaymentMethodAmount = new AffiliateFeeItem[(int)PaymentMethodEnum.PaymentMethodMax + 1]; }
        public bool HasPaymentMethodAmount(PaymentMethodEnum lPM) { return PaymentMethodAmount[(int)lPM] != null; }

        public AffiliateFeeItem GetPaymentMethodAmount(PaymentMethodEnum lPM)
        {
            if (PaymentMethodAmount[(int)lPM] == null)
            {
                PaymentMethodAmount[(int)lPM] = new AffiliateFeeItem();
                PaymentMethodAmount[(int)lPM].PaymentMethod = lPM;
            }
            return PaymentMethodAmount[(int)lPM];
        }

        public decimal SubTotal
        {
            get
            {
                decimal pRet = 0;
                for (int i = 0; i < PaymentMethodAmount.Length; i++)
                    if (PaymentMethodAmount[i] != null) pRet += PaymentMethodAmount[i].Total;
                return pRet;
            }
        }

        public decimal Total
        {
            get
            {
                decimal pRet = 0;
                for (int i = 0; i < PaymentLines.Length; i++)
                    if (PaymentLines[i] != null) pRet += PaymentLines[i].Total;
                return pRet + SubTotal;
            }
        }

        public void UpdatePayment(List<AffiliateTotalLine> newLines = null)
        {
            int AFCompanyID;
            decimal feeRatio = Total / Payment.Total;
            if (AFP_ID == 0)
            {
                Helpers.ExecSql("Insert Into tblAffiliatePayments(AFP_Affiliate_ID, AFP_TransPaymentID, AFP_CompanyID, AFP_FeeRatio, AFP_PaymentAmount, AFP_PaymentCurrency)Values(" +
                AffiliateID + "," + Payment.PayID + "," + Payment.CompanyID + "," + feeRatio.ToString("0.00") + "," + Total.ToString("0.00") + "," + Payment.Currency + ")");
                AFP_ID = Helpers.TestVar(Helpers.ExecScalar("Select Max(AFP_ID) From tblAffiliatePayments Where AFP_Affiliate_ID=" + AffiliateID), 0, -1, 0);
                Helpers.ExecSql("INSERT INTO tblAffiliateFeeSteps(AFS_AffiliateID, AFS_UpToAmount, AFS_Currency, AFS_PercentFee, AFS_PaymentMethod, AFS_CompanyID, AFS_CalcMethod, AFS_SlicePercent, AFS_AFPID)" +
                    "Select AFS_AffiliateID, AFS_UpToAmount, AFS_Currency, AFS_PercentFee, AFS_PaymentMethod, AFS_CompanyID, AFS_CalcMethod, AFS_SlicePercent, " + AFP_ID + " From tblAffiliateFeeSteps Where AFS_AffiliateID=" + AffiliateID + " And AFS_AFPID is null And AFS_CompanyID=" + Payment.CompanyID + " And AFS_Currency=" + Payment.Currency);
            }
            if (newLines != null)
            {
                var tblNewLines = newLines.Select(l => new Dal.Netpay.tblAffiliatePaymentsLine() { AFPL_AFP_ID = AFP_ID, AFPL_Text = l.Text, AFPL_Amount = l.Amount, AFPL_Quantity = l.Quantity, AFPL_Total = l.Total }).ToList();
                DataContext.Writer.tblAffiliatePaymentsLines.InsertAllOnSubmit(tblNewLines);
                DataContext.Writer.SubmitChanges();
                var lineList = PaymentLines.ToList();
                lineList.AddRange(newLines);
                PaymentLines = lineList.ToArray();
            }
            Helpers.ExecSql("Update tblAffiliatePayments Set AFP_FeeRatio=" + feeRatio.ToString("0.00") + ", AFP_PaymentAmount=" + Total.ToString("0.00") + " Where AFP_ID=" + AFP_ID);

            /*
            AFCompanyID = Helpers.TestVar(Helpers.ExecScalar("Select AFCompanyID From tblAffiliates Where affiliates_id=" + AffiliateID), 0, -1, 0);
            if (AFCompanyID > 0)
            {
                //sSQL = "INSERT INTO tblCompanyTransPass(companyID, TransactionTypeID, CreditType, IPAddress, Amount, Currency, OCurrency, Comment, paymentMethod_Id, paymentMethodDisplay, MerchantPD, paymentMethod, UnsettledAmount, UnsettledInstallments)" + _
                //    " VALUES(" + AFCompanyID + ", 10, 1, '" + dbpages.DBText(Request.ServerVariables("REMOTE_ADDR")) + "', " + Round(xValue, 2) + "," + iReader("Currency") + "," + iReader("Currency") + ", '" + "Pay For Settlment #" + iReader("id") + " - " + dbpages.FormatCurr(iReader("Currency"), iReader("TransPayTotal")) + "', " + 4 + ", '" + "Affiliate Payment" + "', GETDATE(), " + ePaymentMethod.PMD_Admin + "," + Round(xValue, 2) + ", 1)"
                //dbPages.ExecSql(sSQL)
            }
            */
        }

        public void LoadLines()
        {
            System.Collections.Generic.List<AffiliateTotalLine> parr = new System.Collections.Generic.List<AffiliateTotalLine>();
            IDataReader mRs = null;
            try
            {
                mRs = Helpers.ExecReader("Select * From tblAffiliatePaymentsLines Where AFPL_AFP_ID=" + AFP_ID);
                while (mRs.Read())
                {
                    AffiliateTotalLine pItem = new AffiliateTotalLine();
                    pItem.Text = Helpers.TestVar(mRs["AFPL_Text"], -1, "");
                    pItem.Quantity = Helpers.TestVar(mRs["AFPL_Quantity"], 0, -1, 0);
                    pItem.Amount = Helpers.TestVar(mRs["AFPL_Amount"], 0m, -1m, 0m);
                    parr.Add(pItem);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }
            finally
            {
                mRs.Close();
            }

            PaymentLines = parr.ToArray();
        }

        public static bool AddSteps(AffiliateFees pRet, IDataReader mRs, PaymentMethodEnum PmdID, bool bRead)
        {
            int oldPmdID = -1;
            decimal lStepTotalAmount, lineSubTotal = 0, nPercentFee, nSlicePercent;
            AffiliateFeeItem pItem = pRet.GetPaymentMethodAmount(PmdID);
            CAmount lineTotal = pRet.Payment.PaymentMethodTotal(PmdID, PmdID);
            lStepTotalAmount = 0;
            if (bRead)
            {
                oldPmdID = Helpers.TestVar(mRs["AFS_PaymentMethod"], 0, -1, 0);
                switch ((PartnerFeeCalc)Helpers.TestVar(mRs["AFS_CalcMethod"], 0, -1, 0))
                {
                    case PartnerFeeCalc.Volume:
                        lineSubTotal = lineTotal.TotalAmount;
                        break;
                    case PartnerFeeCalc.Fees:
                        lineSubTotal = lineTotal.TotalFees;
                        break;
                    case PartnerFeeCalc.MercSale_TermBuy:
                        lineSubTotal = lineTotal.TotalProfit;
                        break;
                    case PartnerFeeCalc.MercSale_AffiBuy:
                        lineSubTotal = lineTotal.TotalAmount;
                        break;
                    case PartnerFeeCalc.MercSale_AffiBuySPercent:
                        lineSubTotal = lineTotal.TotalAmount;
                        break;
                }
            }
            pItem.CreditAmount = lineSubTotal;
            while (bRead)
            {
                string sCreditInfo = null;
                if (oldPmdID != Helpers.TestVar(mRs["AFS_PaymentMethod"], 0, -1, 0)) break;
                nPercentFee = Helpers.TestVar(mRs["AFS_PercentFee"], 0m, -1m, 0m);
                nSlicePercent = Helpers.TestVar(mRs["AFS_SlicePercent"], 0m, -1m, 0m) / 100;
                decimal nStepAmount = (lineSubTotal > Helpers.TestVar(mRs["AFS_UpToAmount"], 0m, -1m, 0m) ? Helpers.TestVar(mRs["AFS_UpToAmount"], 0m, -1m, 0m) : lineSubTotal) - lStepTotalAmount;
                if ((PartnerFeeCalc)Helpers.TestVar(mRs["AFS_CalcMethod"], 0, -1, 0) == PartnerFeeCalc.MercSale_AffiBuy)
                {
                    sCreditInfo = "(" + lineTotal.TotalFeePercent.ToString("0.00") + "% - " + nPercentFee.ToString("0.00") + "%) X " + (nSlicePercent * 100m).ToString("0.00") + "% = " + ((lineTotal.TotalFeePercent - nPercentFee) * nSlicePercent).ToString("0.00") + "%";
                    nPercentFee = lineTotal.TotalFeePercent - nPercentFee;
                }
                else if (Helpers.TestVar(mRs["AFS_CalcMethod"], 0, -1, 0) == (int)PartnerFeeCalc.MercSale_AffiBuySPercent)
                {
                    decimal nStepPercent = Helpers.TestVar(mRs["AFS_UpToAmount"], 0m, -1m, 0m) < lineTotal.TotalFeePercent ? Helpers.TestVar(mRs["AFS_UpToAmount"], 0m, -1m, 0m) : lineTotal.TotalFeePercent;
                    sCreditInfo = "(" + nStepPercent + "% - " + nPercentFee.ToString("0.00") + "%) X " + (nSlicePercent * 100).ToString("0.0") + "% = " + ((nStepPercent - nPercentFee) * nSlicePercent).ToString("0.00") + "%";
                    nPercentFee = nStepPercent - nPercentFee;
                    nStepAmount = lineSubTotal;
                }
                pItem.AddAmount(nPercentFee * nSlicePercent, nStepAmount, sCreditInfo);
                if (lineSubTotal < Helpers.TestVar(mRs["AFS_UpToAmount"], 0m, -1m, 0m)) break;
                lStepTotalAmount = Helpers.TestVar(mRs["AFS_UpToAmount"], 0m, -1m, 0m);
                bRead = mRs.Read();
            }
            return bRead;
        }

        public static bool AddAffiliatePMFees(AffiliateFees pRet, int? companyId, int transType)
        {
            IDataReader mRs = null;
            bool hadRows = false, bRead = false;
            try
            {
                mRs = Helpers.ExecReader("Select * From tblAffiliateFeeSteps Where AFS_AffiliateID=" + pRet.AffiliateID +
                    " And AFS_CompanyID" + (companyId != null ? "=" + pRet.Payment.CompanyID : " Is Null") +
                    " And AFS_Currency=" + pRet.Payment.Currency + " And AFS_AFPID" + (pRet.AFP_ID != 0 ? "=" + pRet.AFP_ID : " is null") + " And AFS_PaymentMethod <> 0 And AFS_TransType=" + transType + " Order By AFS_PaymentMethod Asc, AFS_UpToAmount Asc");
                hadRows = bRead = mRs.Read();
                for (int i = (int)PaymentMethodEnum.CC_MIN; i < (int)PaymentMethodEnum.PaymentMethodMax; i++)
                {
                    if (pRet.Payment.HasPaymentMethodTotal((PaymentMethodEnum)i))
                    {
                        if (bRead)
                        {
                            while (Helpers.TestVar(mRs["AFS_PaymentMethod"], 0, -1, 0) < i)
                            {
                                bRead = mRs.Read();
                                if (!bRead) break;
                            }
                        }
                        if (bRead) bRead = AddSteps(pRet, mRs, (PaymentMethodEnum)i, bRead);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }
            finally
            {
                mRs.Close();
            }
            return hadRows;
        }

        public static bool AddAffiliateNPMFees(AffiliateFees pRet, int? companyId, int transType)
        {
            IDataReader mRs = null;
            bool hadRows = false, bRead = false;
            for (int i = (int)PaymentMethodEnum.CC_MIN; i < (int)PaymentMethodEnum.PaymentMethodMax; i++)
            {
                if (pRet.Payment.HasPaymentMethodTotal((PaymentMethodEnum)i))
                {
                    if (!pRet.HasPaymentMethodAmount((PaymentMethodEnum)i))
                    {
                        try
                        {
                            mRs = Helpers.ExecReader("Select * From tblAffiliateFeeSteps Where AFS_AffiliateID=" + pRet.AffiliateID + " And AFS_CompanyID" + (companyId.HasValue ? "=" + companyId.Value : " Is Null") + " And AFS_Currency=" + pRet.Payment.Currency + " And AFS_AFPID" + (pRet.AFP_ID != 0 ? "=" + pRet.AFP_ID : " is null") + " And AFS_PaymentMethod = 0 And AFS_TransType=" + transType + " Order By AFS_PaymentMethod Asc, AFS_UpToAmount Asc");
                            hadRows = bRead = mRs.Read();
                            if (bRead) AddSteps(pRet, mRs, (PaymentMethodEnum)i, bRead);
                        }
                        catch (Exception ex)
                        {
                            Logger.Log(ex);
                        }
                        finally
                        {
                            mRs.Close();
                        }
                    }
                }
            }
            return hadRows;
        }

        public static void AddAffiliateFees(AffiliateFees pRet, int transType)
        {
            AffiliateFees.AddAffiliatePMFees(pRet, pRet.Payment.CompanyID, transType);
            if (!AffiliateFees.AddAffiliateNPMFees(pRet, pRet.Payment.CompanyID, transType))
            {
                AffiliateFees.AddAffiliatePMFees(pRet, null, transType);
                AffiliateFees.AddAffiliateNPMFees(pRet, null, transType);
            }
        }

        public static void DeleteAffiliatePayment(int xAFP_ID)
        {
            Helpers.ExecSql("Delete From tblAffiliatePaymentsLines Where AFPL_AFP_ID=" + xAFP_ID);
            Helpers.ExecSql("Delete From tblAffiliateFeeSteps Where AFS_AFPID=" + xAFP_ID);
            Helpers.ExecSql("Delete From tblAffiliatePayments Where AFP_ID=" + xAFP_ID);
        }

        public static AffiliateFees CalcAffiliateFees(int xAFP_ID, CPayment xPayment, int xAffID)
        {
            AffiliateFees pRet = new AffiliateFees(xPayment);
            pRet.AffiliateID = xAffID; pRet.AFP_ID = xAFP_ID;
            if (xAFP_ID != 0) pRet.LoadLines();
            else pRet.PaymentLines = new AffiliateTotalLine[0];
            AddAffiliateFees(pRet, 0);
            //AddAffiliateFees(pRet, 1);
            //AddAffiliateFees(pRet, 2);
            return pRet;
        }
    }

    internal static class Helpers
    {
        public static long ExecSql(string sqlStr)
        {
            var dbCon = new SqlConnection(Domain.Current.Sql1ConnectionString);
            var dbCom = new SqlCommand(sqlStr, dbCon);
            try
            {
                dbCon.Open();
                return dbCom.ExecuteNonQuery();
            }
            catch
            {
                throw;
            }
            finally
            {
                dbCon.Close();
                dbCom.Dispose();
                dbCon.Dispose();
            }
        }

        public static object ExecScalar(string sqlStr)
        {
            var dbCon = new SqlConnection(Domain.Current.Sql1ConnectionString);
            var dbCom = new SqlCommand(sqlStr, dbCon);
            dbCon.Open();
            try
            {
                return dbCom.ExecuteScalar();
            }
            catch
            {
                throw;
            }
            finally
            {
                dbCon.Close();
                dbCom.Dispose();
                dbCon.Dispose();
            }
        }

        public static IDataReader ExecReader(string sqlStr)
        {
            System.Diagnostics.Debug.Write("TOTALS:\r\n" + sqlStr);
            SqlConnection dbCon = new SqlConnection(Domain.Current.Sql1ConnectionString);
            SqlCommand dbCom = new SqlCommand(sqlStr, dbCon);
            try
            {
                dbCon.Open();
                return dbCom.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            }
            catch
            {
                if (dbCon.State != System.Data.ConnectionState.Closed) dbCon.Close();
                throw;
            }
            finally
            {
                dbCom.Dispose();
            }
        }

        public static int TestVar(object val, int nMin, int nMax, int nDefault)
        {
            int pRet;
            if (val == null || val == DBNull.Value) return nDefault;
            else if (val is int) return pRet = (int)val;
            if (!int.TryParse(val.ToString(), out pRet)) pRet = nDefault;
            if (nMin < nMax && (pRet < nMin || pRet > nMax)) pRet = nDefault;
            return pRet;
        }

        public static byte TestVar(object val, byte nMin, byte nMax, byte nDefault)
        {
            byte pRet;
            if (val == null || val == DBNull.Value) return nDefault;
            else if (val is byte) return pRet = (byte)val;
            if (!byte.TryParse(val.ToString(), out pRet)) pRet = nDefault;
            if (nMin < nMax && (pRet < nMin || pRet > nMax)) pRet = nDefault;
            return pRet;
        }

        public static decimal TestVar(object val, decimal nMin, decimal nMax, decimal nDefault)
        {
            decimal pRet;
            if (val == null || val == DBNull.Value) return nDefault;
            else if (val is int) return pRet = (decimal)val;
            if (!decimal.TryParse(val.ToString(), out pRet)) pRet = nDefault;
            if (nMin < nMax && (pRet < nMin || pRet > nMax)) pRet = nDefault;
            return pRet;
        }

        public static DateTime TestVar(object val, DateTime nMin, DateTime nMax, DateTime nDefault)
        {
            DateTime pRet;
            if (val == null || val == DBNull.Value) return nDefault;
            else if (val is DateTime) return pRet = (DateTime)val;
            if (!DateTime.TryParse(val.ToString(), out pRet)) pRet = nDefault;
            if (nMin < nMax && (pRet < nMin || pRet > nMax)) pRet = nDefault;
            return pRet;
        }

        public static string TestVar(object val, int nMax, string nDefault)
        {
            string pRet;
            if (val == null) return nDefault;
            else if (val is string) return pRet = (string)val;
            pRet = val.ToString();
            if (nMax > 0 && (pRet.Length > nMax)) pRet = pRet.Substring(0, nMax);
            return pRet;
        }

        public static bool TestVar(object val, bool nDefault)
        {
            bool pRet;
            if (val == null) return nDefault;
            else if (val is bool) return pRet = (bool)val;
            if (!bool.TryParse(val.ToString(), out pRet)) pRet = nDefault;
            return pRet;
        }
    }

}
