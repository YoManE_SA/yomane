﻿using System;
using System.Linq;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Domains;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure.VO;
using Netpay.Infrastructure;
using Netpay.CommonTypes;
using Netpay.Dal.Netpay;
using System.Threading;
using System.Collections.Generic;

namespace Netpay.Bll
{
    public static class Customers
    {
        public static CustomersVO GetCustomer(string domainHost, int customerID)
        {
            var domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            return (from c in dc.tblCustomers where c.ID == customerID select new CustomersVO(domain, c)).FirstOrDefault();
        }

		public static AddressVO GetShippingAddress(Guid credentialsToken, int addressID)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken);
			if (user == null || user.Type != UserType.Customer)
				return null;

			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(user.Domain.Host).Sql1ConnectionString);
			var entity = (from ctsd in dc.CustomerToShippingDetails where ctsd.Customer_id == user.ID && ctsd.ShippingDetail_id == addressID select ctsd.data_ShippingDetail).SingleOrDefault();
			if (entity == null)
				return null;

			return new AddressVO(entity);
		}

		public static List<AddressVO> GetShippingAddresses(Guid credentialsToken) 
		{
			User user = SecurityManager.GetInternalUser(credentialsToken);
			if (user == null || user.Type != UserType.Customer)
				return null;

			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(user.Domain.Host).Sql1ConnectionString);
			var addresses = (from cstd in dc.CustomerToShippingDetails where cstd.Customer_id == user.ID select cstd.data_ShippingDetail);
			return addresses.Select(a => new AddressVO(a)).ToList();
		}

		public static void AddShippingAddress(Guid credentialsToken, AddressVO address) 
		{
			User user = SecurityManager.GetInternalUser(credentialsToken);
			if (user == null || user.Type != UserType.Customer)
				return;

			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(user.Domain.Host).Sql1ConnectionString);
			ShippingDetail entity = new ShippingDetail();
			entity.Title = address.Title;
			entity.AddressCity = address.City;
			entity.AddressCountryISOCode = address.CountryIso;
			entity.AddressLine1 = address.Address1;
			entity.AddressLine2 = address.Address2;
			entity.AddressPostalCode = address.Zipcode;
			entity.AddressStateISOCode = address.StateIso;
			entity.TableList_id = 2;
			dc.ShippingDetails.InsertOnSubmit(entity);
			
			CustomerToShippingDetail customerToShipping = new CustomerToShippingDetail();
			customerToShipping.Customer_id = user.ID;
			customerToShipping.data_ShippingDetail = entity;
			dc.CustomerToShippingDetails.InsertOnSubmit(customerToShipping);

			dc.SubmitChanges();
		}

		public static void UpdateShippingAddress(Guid credentialsToken, AddressVO address)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken);
			if (user == null || user.Type != UserType.Customer)
				return;

			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(user.Domain.Host).Sql1ConnectionString);
			var entity = (from ctsd in dc.CustomerToShippingDetails where ctsd.Customer_id == user.ID && ctsd.ShippingDetail_id == address.ID select ctsd.data_ShippingDetail).SingleOrDefault();
			if (entity == null)
				return;

			entity.Title = address.Title;
			entity.AddressCity = address.City;
			entity.AddressCountryISOCode = address.CountryIso;
			entity.AddressLine1 = address.Address1;
			entity.AddressLine2 = address.Address2;
			entity.AddressPostalCode = address.Zipcode;
			entity.AddressStateISOCode = address.StateIso;
			dc.SubmitChanges();
		}

		public static void DeleteShippingAddress(Guid credentialsToken, int addressID)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken);
			if (user == null || user.Type != UserType.Customer)
				return;

			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(user.Domain.Host).Sql1ConnectionString);
			var entity = (from ctsd in dc.CustomerToShippingDetails where ctsd.Customer_id == user.ID && ctsd.ShippingDetail_id == addressID select ctsd).SingleOrDefault();
			if (entity == null)
				return;

			dc.ShippingDetails.DeleteOnSubmit(entity.data_ShippingDetail);
			dc.CustomerToShippingDetails.DeleteOnSubmit(entity);
			dc.SubmitChanges();
		}

        public static bool ValidatePinCode(string domainHost, int customerID, string pinCode)
        {
            NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
            var customerPinCode = (from c in dc.tblCustomers where c.ID == customerID select c.PinCode).FirstOrDefault();
            if (string.IsNullOrEmpty(customerPinCode) || string.IsNullOrEmpty(pinCode)) return false;
            return (customerPinCode == pinCode);
        }

		public enum UpdatePincodeResult 
		{
			GeneralError = 1,
			InvalidPincode = 2,
			WrongCurrentPincode = 3,
			Success = 4
		}

		public static UpdatePincodeResult UpdatePincode(Guid credentialsToken, string oldPincode, string newPincode)
		{
			oldPincode = oldPincode.Trim();
			newPincode = newPincode.Trim();
			if (oldPincode == null || oldPincode.Length != 4)
				return UpdatePincodeResult.InvalidPincode;
			if (newPincode == null || newPincode.Length != 4)
				return UpdatePincodeResult.InvalidPincode;

			User user = SecurityManager.GetInternalUser(credentialsToken);
			if (user == null || user.Type != UserType.Customer)
				return UpdatePincodeResult.GeneralError;

			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(user.Domain.Host).Sql1ConnectionString);
			var customer = (from c in dc.tblCustomers where c.ID == user.ID select c).SingleOrDefault();
			if (customer == null)
				return UpdatePincodeResult.GeneralError;
			if (customer.PinCode.Trim() != oldPincode)
				return UpdatePincodeResult.WrongCurrentPincode;

			customer.PinCode = newPincode;
			dc.SubmitChanges();

			return UpdatePincodeResult.Success;
		}

        public static RegisterUpdateResult UpdateCustomer(Guid credentialsToken, CustomersVO customer)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken);
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            var entity = (from c in dc.tblCustomers where c.ID == user.ID select c).SingleOrDefault();
            if (entity == null)
                return RegisterUpdateResult.CustomrNotFound;

            int countryId = 0;
            CountryVO fountCountry = user.Domain.Cache.GetCountryByName(customer.Country);
            if (fountCountry != null)
                countryId = fountCountry.ID;

            int stateId = 0;
            StateVO fountState = user.Domain.Cache.GetStateByName(customer.State);
            if (fountState != null)
                stateId = fountState.ID;

            entity.LastInsertDate = DateTime.Now;
            entity.FirstName = customer.FirstName;
            entity.LastName = customer.LastName;
            //entity.Mail = customer.Email;
            entity.Phone = customer.Phone;
			entity.CellPhone = customer.CellPhone;
			entity.Street = customer.Street;
            entity.City = customer.City;
            entity.State = stateId;
            entity.Country = countryId;
            entity.ZIP = customer.ZIP;
            //entity.PinCode = customer.Pincode;
            dc.SubmitChanges();

            return RegisterUpdateResult.Success;
        }

        public enum RegisterUpdateResult
        {
            Unknown = 0,
            Success = 1,
            EmailAlreadyRegistered = 2,
            InvalidEmail = 3,
            CustomrNotFound = 4
        }

        public static bool ResetPassword(string domainHost, string emailAddress, string clientIp)
        {
            var d = Netpay.Infrastructure.Domains.DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(d.Sql1ConnectionString);
            var customer = (from c in dc.tblCustomers where c.Mail.Trim().ToLower() == emailAddress.ToLower() select new CustomersVO(d, c)).FirstOrDefault();
            if (customer == null) return false;
            string password = SecurityManager.ResetPassword(domainHost, customer.ID, UserType.Customer, clientIp);

            System.Globalization.CultureInfo cultureInfo = System.Globalization.CultureInfo.GetCultureInfo("en-us");
            string subject, body = Infrastructure.Email.FormatMesage.GetMessageText(System.IO.Path.Combine(d.EmailTemplatePath, "Wallet_ResetPassword.htm"), out subject);
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();

            var dataObj = new { Domain = d, Customer = customer, CustomerPassword = password };
            message.IsBodyHtml = true;
            message.Subject = Infrastructure.Email.FormatMesage.FormatMessage(subject, dataObj, null, cultureInfo);
            message.Body = Infrastructure.Email.FormatMesage.FormatMessage(body, dataObj, null, cultureInfo);
            message.From = new System.Net.Mail.MailAddress(d.MailAddressFrom, d.MailNameFrom);
            message.To.Add(new System.Net.Mail.MailAddress(customer.Email, string.Format("{0} {1}", customer.FirstName, customer.LastName)));
            Infrastructure.Email.SmtpClient.Send(message);
            return true;
        }

        public static void SendRegistrationEmail(string domainHost, CustomersVO customer)
        {
            var d = Netpay.Infrastructure.Domains.DomainsManager.GetDomain(domainHost);
            System.Globalization.CultureInfo cultureInfo = System.Globalization.CultureInfo.GetCultureInfo("en-us");
            string subject, body = Infrastructure.Email.FormatMesage.GetMessageText(System.IO.Path.Combine(d.EmailTemplatePath, "Wallet_Register.htm"), out subject);
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();

            NetpayDataContext dc = new NetpayDataContext(d.Sql1ConnectionString);
            string walletpassword = "";
            var encPassword = (from h in dc.tblPasswordHistories where h.LPH_RefID == customer.ID && h.LPH_RefType == (int)UserType.Customer && h.LPH_ID == 1 select h.LPH_Password256).SingleOrDefault();
            if (encPassword != null) walletpassword = Infrastructure.Security.Encryption.Decrypt(domainHost, encPassword.ToArray());

            var dataObj = new { Domain = d, Customer = customer, CustomerPassword = walletpassword };
            message.IsBodyHtml = true;
            message.Subject = Infrastructure.Email.FormatMesage.FormatMessage(subject, dataObj, null, cultureInfo);
            message.Body = Infrastructure.Email.FormatMesage.FormatMessage(body, dataObj, null, cultureInfo);
            message.From = new System.Net.Mail.MailAddress(d.MailAddressFrom, d.MailNameFrom);
            message.To.Add(new System.Net.Mail.MailAddress(customer.Email, string.Format("{0} {1}", customer.FirstName, customer.LastName)));
            Infrastructure.Email.SmtpClient.Send(message);
        }

        public static RegisterUpdateResult CreateCustomer(string domainHost, CustomersVO customer, string password, string ip, string registeredFrom)
        {
            if (!customer.Email.IsEmail())
                return RegisterUpdateResult.InvalidEmail;

            Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            var found = from c in dc.tblCustomers where c.Mail.Trim().ToLower() == customer.Email.Trim().ToLower() select c;
            if (found.Count() > 0)
                return RegisterUpdateResult.EmailAlreadyRegistered;

            int countryId = 0;
            CountryVO fountCountry = domain.Cache.GetCountryByName(customer.Country);
            if (fountCountry != null)
                countryId = fountCountry.ID;

            int stateId = 0;
            StateVO fountState = domain.Cache.GetStateByName(customer.State);
            if (fountState != null)
                stateId = fountState.ID;

            tblCustomer entity = new tblCustomer();
            entity.InsertDate = DateTime.Now;
            entity.LastInsertDate = DateTime.Now;
			entity.FirstName = customer.FirstName.EmptyIfNull();
			entity.LastName = customer.LastName.EmptyIfNull();
            entity.Mail = customer.Email;
			entity.Phone = customer.Phone.EmptyIfNull(); ;
			entity.IDNumber = customer.IDNumber.EmptyIfNull(); ;
            entity.Street = customer.Street.EmptyIfNull();
			entity.City = customer.City.EmptyIfNull();
			entity.ZIP = customer.ZIP.EmptyIfNull();
            entity.PinCode = customer.Pincode;
            entity.Country = countryId;
            entity.State = stateId;
            entity.UserName = "";
            entity.CellPhone = "";
            entity.Question1 = "";
            entity.Question2 = 0;
            entity.Answer1 = "";
            entity.Answer2 = "";
            entity.Comment = "";
            entity.CustomerFrom = registeredFrom == null ? "" : registeredFrom.Truncate(50);
            entity.IpOnReg = ip == null ? "" : ip.Truncate(50);
            entity.CustomerNumber = "";
            entity.PaymentAccount = "";
            entity.PaymentBank = 0;
            entity.PaymentBranch = "";
            entity.PaymentIDNumber = "";
            entity.PaymentPayeeName = "";
            entity.RefCompanyMemberID = "";
            entity.RefCompanyData = "";
            entity.ECA = "";
            entity.ECS = "";
            dc.tblCustomers.InsertOnSubmit(entity);
            dc.SubmitChanges();

            customer.ID = entity.ID;
            byte[] encryptedPass;
            Netpay.Infrastructure.Security.Encryption.Encrypt(domainHost, password, out encryptedPass);

            tblPasswordHistory passwordHistory = new tblPasswordHistory();
            passwordHistory.LPH_ID = 1;
            passwordHistory.LPH_Insert = DateTime.Now;
            passwordHistory.LPH_LastFail = DateTime.Now;
            passwordHistory.LPH_LastSuccess = DateTime.Now;
            passwordHistory.LPH_RefType = (int)UserType.Customer;
            passwordHistory.LPH_RefID = entity.ID;
            passwordHistory.LPH_Password256 = encryptedPass;
            passwordHistory.LPH_IP = "";
            dc.tblPasswordHistories.InsertOnSubmit(passwordHistory);
            dc.SubmitChanges();

			Thread t = new Thread (() => SendRegistrationEmail(domainHost, customer));
			t.Start(); 
           
            return RegisterUpdateResult.Success;
        }

        public static bool DeleteCard(Guid credentialsToken, int cardId) 
        {
            User customer = SecurityManager.GetInternalUser(credentialsToken);
            if (customer == null || customer.Type != UserType.Customer)
                return false;
            
            NetpayDataContext dc = new NetpayDataContext(customer.Domain.Sql1ConnectionString);
            tblCustomerCC entity = (from c in dc.tblCustomerCCs where c.CustomerID == customer.ID && c.ID == cardId select c).SingleOrDefault();
            if (entity == null)
                return false;

            dc.tblCustomerCCs.DeleteOnSubmit(entity);
            dc.SubmitChanges();

            return true;
        }

		public static bool UpdateCard(Guid credentialsToken, CreditCardStorageVO vo)
        {
            User customer = SecurityManager.GetInternalUser(credentialsToken);
            if (customer == null || customer.Type != UserType.Customer)
                return false;

            NetpayDataContext dc = new NetpayDataContext(customer.Domain.Sql1ConnectionString);
            tblCustomerCC entity = (from c in dc.tblCustomerCCs where c.CustomerID == customer.ID && c.ID == vo.ID select c).SingleOrDefault();
            if (entity == null)
                return false;

            int countryId = 0;
            CountryVO fountCountry = customer.Domain.Cache.GetCountryByName(vo.Country);
            if (fountCountry != null)
                countryId = fountCountry.ID;

            int stateId = 0;
            StateVO fountState = customer.Domain.Cache.GetStateByName(vo.State);
            if (fountState != null)
                stateId = fountState.ID;

            entity.CustomerID = customer.ID;
            entity.Billing_City = vo.CardholderCity.Truncate(50);
            entity.Billing_Country = countryId;
            entity.Billing_State = stateId;
            entity.Billing_Street = vo.CardholderStreet1.Truncate(50);
            entity.Billing_Zipcode = vo.CardholderZipCode.Truncate(50);
            entity.CH_Email = vo.CardholderEmail.Truncate(80);
            entity.CH_FullName = vo.CardholderFullName.Truncate(100);
            entity.CH_PersonalNumber = vo.CardholderID == null ? "" : vo.CardholderID;
            entity.CH_PhoneNumber = vo.CardholderPhone.Truncate(50);
            entity.CCard_ExpMM = vo.ExpirationMonth.Truncate(10);
            entity.CCard_ExpYY = vo.ExpirationYear.Truncate(10);
            dc.SubmitChanges();

            return true;
        }

        public static bool AddCard(Guid credentialsToken, CreditCardStorageVO vo)
        {
            User customer = SecurityManager.GetInternalUser(credentialsToken);
            if (customer == null || customer.Type != UserType.Customer)
                return false;

            CreditCard creditcard = new CreditCard(vo.CardNumber);
            if (!creditcard.IsValid)
                return false;

            if (vo.CountryID.GetValueOrDefault() == 0) {
				CountryVO foundCountry = customer.Domain.Cache.GetCountryByName(vo.Country);
				if (foundCountry != null) vo.CountryID = foundCountry.ID;
			}
            if (vo.StateID.GetValueOrDefault() == 0) {
				StateVO fountState = customer.Domain.Cache.GetStateByName(vo.State);
				if (fountState != null) vo.StateID = fountState.ID;
			}

            NetpayDataContext dc = new NetpayDataContext(customer.Domain.Sql1ConnectionString);
            tblCustomerCC entity = new tblCustomerCC();
            entity.CustomerID = customer.ID;
            entity.Billing_City = vo.CardholderCity.Truncate(50);
			entity.Billing_Country = vo.CountryID.GetValueOrDefault();
			entity.Billing_State = vo.StateID.GetValueOrDefault();
            entity.Billing_Street = vo.CardholderStreet1.Truncate(50);
            entity.Billing_Zipcode = vo.CardholderZipCode.Truncate(50);
            entity.CH_Email = vo.CardholderEmail.Truncate(80);
            entity.CH_FullName = vo.CardholderFullName.Truncate(100);
            entity.CH_PersonalNumber = vo.CardholderID == null ? "" : vo.CardholderID;
            entity.CH_PhoneNumber = vo.CardholderPhone.Truncate(50);
            entity.IsBlocked = false;
            //entity.IsRemote = false;
            entity.IsShow = true;
            entity.CCard_ExpMM = vo.ExpirationMonth.Truncate(10);
            entity.CCard_ExpYY = vo.ExpirationYear.Truncate(10);
            entity.CCard_Last4 = creditcard.Last4Digits.Truncate(4);
            entity.BINCountry = creditcard.GetBin(customer.Domain.Host).BinCountryIsoCode2;
            entity.CCard_Number256 = Encryption.Encrypt(customer.Domain.Host, vo.CardNumber);
            entity.CCard_TypeID = (int)creditcard.GetCardType(customer.Domain.Host);
            entity.PaymentMethod = (short)creditcard.GetPaymentMethod(customer.Domain.Host);
            entity.CCard_Cui = vo.Cui == null ? "" : vo.Cui;
            dc.tblCustomerCCs.InsertOnSubmit(entity);
            dc.SubmitChanges();

            return true;
        }

        public static CreditCardStorageVO[] GetCards(Guid credentialsToken)
        {
            User customer = SecurityManager.GetInternalUser(credentialsToken);
            if (customer == null)
                return null;

            NetpayDataContext dc = new NetpayDataContext(customer.Domain.Sql1ConnectionString);
            return (from cc in dc.tblCustomerCCs where cc.CustomerID == customer.ID select new CreditCardStorageVO(cc, customer.Domain.Host)).ToArray();
        }

        public static CreditCardStorageVO GetCard(Guid credentialsToken, int id)
        {
            User customer = SecurityManager.GetInternalUser(credentialsToken);
            if (customer == null)
                return null;

            NetpayDataContext dc = new NetpayDataContext(customer.Domain.Sql1ConnectionString);
            return (from cc in dc.tblCustomerCCs where cc.CustomerID == customer.ID && cc.ID == id select new CreditCardStorageVO(cc, customer.Domain.Host)).SingleOrDefault();
        }

		public static bool AddEcheck(Guid credentialsToken, EcheckStorageVO vo)
		{
			User customer = SecurityManager.GetInternalUser(credentialsToken);
			if (customer == null || customer.Type != UserType.Customer)
				return false;

			NetpayDataContext dc = new NetpayDataContext(customer.Domain.Sql1ConnectionString);
			tblCustomerEcheckAccount entity = new tblCustomerEcheckAccount();
			entity.insertDate = DateTime.Now;
			entity.DateOfBirth = vo.DateOfBirth;
			entity.customerId = customer.ID;
			entity.accountName = vo.AccountName;
			entity.accountType = vo.AccountType;
			entity.phoneNumber = vo.PhoneNumber;
			entity.emailAddress = vo.EmailAddress;
			entity.accountNumber256 = Encryption.Encrypt(customer.Domain.Host, vo.AccountNumber);
			entity.routingNumber256 = Encryption.Encrypt(customer.Domain.Host, vo.RoutingNumber);
			dc.tblCustomerEcheckAccounts.InsertOnSubmit(entity);
			dc.SubmitChanges();
			return true;
		}

		public static bool DeleteEcheck(Guid credentialsToken, int achId)
		{
			User customer = SecurityManager.GetInternalUser(credentialsToken);
			if (customer == null || customer.Type != UserType.Customer)
				return false;

			NetpayDataContext dc = new NetpayDataContext(customer.Domain.Sql1ConnectionString);
			tblCustomerEcheckAccount entity = (from c in dc.tblCustomerEcheckAccounts where c.customerId == customer.ID && c.id == achId select c).SingleOrDefault();
			if (entity == null)
				return false;

			dc.tblCustomerEcheckAccounts.DeleteOnSubmit(entity);
			dc.SubmitChanges();

			return true;
		}

		public static bool UpdateEcheck(Guid credentialsToken, EcheckStorageVO vo)
		{
			User customer = SecurityManager.GetInternalUser(credentialsToken);
			if (customer == null || customer.Type != UserType.Customer)
				return false;

			NetpayDataContext dc = new NetpayDataContext(customer.Domain.Sql1ConnectionString);
			tblCustomerEcheckAccount entity = (from c in dc.tblCustomerEcheckAccounts where c.customerId == customer.ID && c.id == vo.Id select c).SingleOrDefault();
			if (entity == null)
				return false;

			//entity.customerId = vo.CustomerId;
			entity.accountName = vo.AccountName;
			entity.accountType = vo.AccountType;
			entity.phoneNumber = vo.PhoneNumber;
			entity.emailAddress = vo.EmailAddress;
			dc.SubmitChanges();
			return true;
		}

		public static EcheckStorageVO[] GetEchecks(Guid credentialsToken)
		{
			User customer = SecurityManager.GetInternalUser(credentialsToken);
			if (customer == null)
				return null;

			NetpayDataContext dc = new NetpayDataContext(customer.Domain.Sql1ConnectionString);
			return (from ach in dc.tblCustomerEcheckAccounts where ach.customerId == customer.ID select new EcheckStorageVO(ach, customer.Domain.Host)).ToArray();
		}

		public static EcheckStorageVO GetEcheck(Guid credentialsToken, int id)
		{
			User customer = SecurityManager.GetInternalUser(credentialsToken);
			if (customer == null)
				return null;

			NetpayDataContext dc = new NetpayDataContext(customer.Domain.Sql1ConnectionString);
			return (from ach in dc.tblCustomerEcheckAccounts where ach.customerId == customer.ID && ach.id == id select new EcheckStorageVO(ach, customer.Domain.Host)).SingleOrDefault();
		}
		
		/// <summary>
        ///	Returns array of CustomerBalanceVO, containing one record for each currency.
        ///	If baseCurrency is specified, returns only records with balance greater than baseAmount (converted to the record currency).
        /// </summary>
        /// <param name="credentialsToken"></param>
        /// <returns></returns>
        public static CustomerBalanceVO[] GetBalance(Guid credentialsToken)
        {
            User customer = SecurityManager.GetInternalUser(credentialsToken);
            if (customer == null) return null;

            NetpayDataContext dc = new NetpayDataContext(customer.Domain.Sql1ConnectionString);
            CustomerBalanceVO[] balanceTable = (from cb in dc.GetCustomerBalanceTable(customer.ID) select new CustomerBalanceVO(cb)).ToArray();
            return balanceTable;
        }

        /// <summary>
        ///	Returns array of CustomerBalanceVO, containing one record for each currency.
        ///	If baseCurrency is specified, returns only records with balance greater than baseAmount (converted to the record currency).
        /// </summary>
        /// <param name="credentialsToken"></param>
        /// <param name="baseCurrency" remarks="optional"></param>
        /// <param name="baseAmount" remarks="optional"></param>
        /// <returns></returns>
        public static CustomerBalanceVO[] GetBalance(Guid credentialsToken, Currency baseCurrency, decimal baseAmount)
        {
            User customer = SecurityManager.GetInternalUser(credentialsToken);
            if (customer == null) return null;

            CustomerBalanceVO[] balanceTable = GetBalance(credentialsToken);
            if (baseCurrency != Currency.Unknown)
                balanceTable = balanceTable.Where(bt => bt.Balance >= baseAmount * customer.Domain.Cache.GetCurrency((int)baseCurrency).BaseRate / customer.Domain.Cache.GetCurrency(bt.Iso).BaseRate).ToArray();

            return balanceTable;
        }

        public static int PayFromBalance(Guid credentialsToken, CurrencyVO customerCurrency, decimal customerAmount, string merchantNumber, CurrencyVO merchantCurrency, decimal merchantAmount, string comment)
        {
            User customer = SecurityManager.GetInternalUser(credentialsToken);
            if (customer == null)
                return -1;
            return -1;
            //NetpayDataContext dc = new NetpayDataContext(customer.Domain.Sql1ConnectionString);
            //return dc.PayFromBalanceCurrency(merchantNumber, merchantCurrency.ID, merchantAmount, comment, customer.ID, customerCurrency.ID, customerAmount);
        }

    }
}