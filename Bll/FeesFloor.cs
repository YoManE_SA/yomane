﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Domains;

namespace Netpay.Bll
{
	public enum SettlementType {
		Merchant = 1,
		DebitCompany = 2,
		MerchantAffiliate = 3,
		DebitCompanyAffiliate = 4
	}

	public enum FeeCalcMethod {
		TransactionAmount = 1,	
		MerchantFee = 2,
		BankFee = 3,
		RevenueFee = 4,
	}

	public enum AmountType {

		Authorization = 1,	
		Sale = 2,	
		Capture = 3,	
		Installment = 4,	
		Refund = 5,	
		Clarification = 6,	
		Chargeback = 7,	
		Finance = 8,	
		Decline = 9,	
		RollingReserve = 10,
			
		CCStorage = 13,	
		System = 14,	

		InvoiceTransaction = 15,	
		Transaction = 16,
		InstallmentItem = 17,
		//Tax = 11,
		//Wire = 12,

	}

	public class InvoiceIssuerVO
	{
		public byte ID { get; set; }
		public byte InvoiceProvider_id { get; set; }
		public string Name { get; set; }
		public string Address { get; set; }
		public string Email { get; set; }
		public string Number { get; set; }
		public string InvoiceCurrencyIsoCode { get; set; }
		public string LanguageShow { get; set; }
		public InvoiceIssuerVO(Netpay.Dal.Netpay.InvoiceIssuer entity)
		{
			ID = entity.InvoiceIssuer_id;
			InvoiceProvider_id = entity.InvoiceProvider_id;
			Name = entity.Name;
			Address = entity.Address;
			Email = entity.Email;
			Number = entity.Number;
			InvoiceCurrencyIsoCode = entity.InvoiceCurrencyISOCode;
			LanguageShow = entity.LanguageShow;
		}
	}

	public class SystemAmountVO
	{
		public int? TransPass_id { get; set; }
		public int? TransPreAuth_id { get; set; }
		public int? TransFail_id { get; set; }

		public int? Merchant_id { get; set; }
		public int? DebitCompany_id { get; set; }
		public int? Affiliate_id { get; set; }

		public int? SetSettlement_id { get; set; }
		public int? Settlement_id { get; set; }
		public DateTime SettlementDate { get; set; }

		public Bll.AmountType AmountType_id { get; set; }
		public int Installment { get; set; }
		public string CurrencyISOCode { get; set; }
		public decimal? PercentValue { get; set; }
		public decimal PercentAmount { get; set; }
		public decimal FixedAmount { get; set; }
		public bool IsFee { get; set; }

		public decimal Total { get { return FixedAmount + PercentAmount; } }
		public SystemAmountVO() { }
	}

	public class FloorFeeVO 
	{
		public int SetTransactionFloorFee_id { get; set; }
		public int SetTransactionFloor_id { get; set; }
		public decimal AmountTop { get; set; }
		public decimal? PercentValue { get; set; }
		public decimal? FixedAmount { get; set; }
		public FloorFeeVO() { }
		public FloorFeeVO(Netpay.Dal.Netpay.SetTransactionFloorFee entity)
		{
			SetTransactionFloorFee_id = entity.SetTransactionFloorFee_id;
			SetTransactionFloor_id = entity.SetTransactionFloor_id;
			AmountTop = entity.AmountTop;
			PercentValue = entity.PercentValue;
			FixedAmount = entity.FixedAmount;
		}
	}

	public class FloorVO
	{
		public int SetTransactionFloor_id { get; set; }
		public SettlementType SettlementType_id { get; set; }
		public int? Merchant_id { get; set; }
		public int? DebitCompany_id { get; set; }
		public int? Affiliate_id { get; set; }
		public string CurrencyISOCode { get; set; }
		public string Title { get; set; }
		public FloorVO() { }
		public FloorVO(Netpay.Dal.Netpay.SetTransactionFloor entity)
		{
			SetTransactionFloor_id = entity.SetTransactionFloor_id;
			SettlementType_id = (SettlementType) entity.SettlementType_id;
			Title = entity.Title;
			Merchant_id = entity.Merchant_id;
			DebitCompany_id = entity.DebitCompany_id;
			Affiliate_id = entity.Affiliate_id;
			CurrencyISOCode = entity.CurrencyISOCode;
		}
	}

	public class SettlementSettingsVO
	{
		public int SetSettelment_id { get; set; }
		public SettlementType SettelmentType_id { get; set; }
		public int? Merchant_id { get; set; }
		public int? DebitCompany_id { get; set; }
		public int? Affiliate_id { get; set; }
		public string CurrencyISOCode { get; set; }
		public bool IsShowToSettle { get; set; }
		public decimal? MinPayoutAmount { get; set; }
		public decimal? MinSettlementAmount { get; set; }
		public byte? SettleInitialHoldDays { get; set; }
		public byte? SettleHoldDays { get; set; }
		public byte? InvoiceIssuer_id { get; set; }
		public string WireCurrencyIsoCode { get; set; }
		public decimal? WireFeeAmount { get; set; }
		public decimal? WireFeePercent { get; set; }
		public decimal? StorageFeeAmount { get; set; }
		public string StorageFeeCurrencyIsoCode { get; set; }

		public SettlementSettingsVO() { }

		public SettlementSettingsVO(Netpay.Dal.Netpay.SetSettlement entity)
		{
			SetSettelment_id = entity.SetSettlement_id;
			SettelmentType_id = (SettlementType)entity.SettlementType_id;
			Merchant_id = entity.Merchant_id;
			DebitCompany_id = entity.DebitCompany_id;
			Affiliate_id = entity.Affiliate_id;
			CurrencyISOCode = entity.CurrencyISOCode;
			IsShowToSettle = entity.IsShowToSettle;
			MinPayoutAmount = entity.MinPayoutAmount;
			MinSettlementAmount = entity.MinSettlementAmount;
			SettleInitialHoldDays = entity.SettleInitialHoldDays;
			SettleHoldDays = entity.SettleHoldDays;
			InvoiceIssuer_id = entity.InvoiceIssuer_id;
			WireCurrencyIsoCode = entity.WireCurrencyIsoCode;
			WireFeeAmount = entity.WireFeeAmount;
			WireFeePercent = entity.WireFeePercent;
			StorageFeeAmount = entity.StorageFeeAmount;
			StorageFeeCurrencyIsoCode = entity.StorageFeeCurrencyIsoCode;

		}
	}

	public class SettlementScheduleVO
	{
		public int SetSettlementSchedule_id { get; set; }
		public int SetSettlement_id { get; set; }
		public byte DayTo { get; set; }
		public byte DaySettlement { get; set; }

		public SettlementScheduleVO() { }
		public SettlementScheduleVO(Netpay.Dal.Netpay.SetSettlementSchedule entity)
		{
			SetSettlementSchedule_id = entity.SetSettlementSchedule_id;
			SetSettlement_id = entity.SetSettlement_id;
			DayTo = entity.DayTo;
			DaySettlement = entity.DaySettlement;
		}
	}


	public class TransactionFeesVO
	{
		public int SetTransactionFee_id { get; set; }
		public FeeCalcMethod FeeCalcMethod_id { get; set; }
		public AmountType AmountType_id { get; set; }
		public SettlementType SettlementType_id { get; set; }
		public int? Merchant_id { get; set; }
		public int? DebitCompany_id { get; set; }
		public int? Affiliate_id { get; set; }
		public string CurrencyISOCode { get; set; }
		public CommonTypes.PaymentMethodEnum? PaymentMethod_id { get; set; }
		public string CountryISOCode { get; set; }
		public byte? CountryGroup_id { get; set; }
		public int? SetTransactionFloor_id { get; set; }
		public decimal? PercentValue { get; set; }
		public decimal? FixedAmount { get; set; }
		public decimal? SettlementPercentValue { get; set; }

		public TransactionFeesVO() {}

		public TransactionFeesVO(Netpay.Dal.Netpay.SetTransactionFee entity)
		{
			SetTransactionFee_id = entity.SetTransactionFee_id;
			FeeCalcMethod_id = (FeeCalcMethod) entity.FeeCalcMethod_id;
			AmountType_id = (AmountType) entity.AmountType_id;

			SettlementType_id = (SettlementType)entity.SettlementType_id;
			Merchant_id = entity.Merchant_id;
			DebitCompany_id = entity.DebitCompany_id;
			Affiliate_id = entity.Affiliate_id;
			CurrencyISOCode = entity.CurrencyISOCode;

			PaymentMethod_id = (CommonTypes.PaymentMethodEnum?) entity.PaymentMethod_id;
			CountryISOCode = entity.CountryISOCode;
			CountryGroup_id = entity.CountryGroup_id;
			SetTransactionFloor_id = entity.SetTransactionFloor_id;
			PercentValue = entity.PercentValue;
			FixedAmount = entity.FixedAmount;
			SettlementPercentValue = entity.SettlementPercentValue;
		}
	}

	public static class SettlementSettings
	{
		public class SettingsFilter
		{
			public SettlementType SettlementType_id;
			public int? Merchant_id;
			public int? DebitCompany_id;
			public int? Affiliate_id;
			public string CurrencyISOCode;
		}

		public static List<InvoiceIssuerVO> GetInvoiceIssuers(string domainHost)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			return (from ii in dc.InvoiceIssuers select new InvoiceIssuerVO(ii)).ToList();
		}

		public static void SaveFloorFee(string domainHost, FloorFeeVO ffv)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			var entity = (from f in dc.SetTransactionFloorFees where f.SetTransactionFloorFee_id == ffv.SetTransactionFloorFee_id select f).SingleOrDefault();
			if (entity == null) {
				entity = new Netpay.Dal.Netpay.SetTransactionFloorFee();
				entity.SetTransactionFloor_id = ffv.SetTransactionFloor_id;
				dc.SetTransactionFloorFees.InsertOnSubmit(entity);
			}
			entity.AmountTop = ffv.AmountTop;
			entity.PercentValue = ffv.PercentValue;
			entity.FixedAmount = ffv.FixedAmount;
			dc.SubmitChanges();
		}

		public static void DeleteFloorFee(string domainHost, int id)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			var entity = (from f in dc.SetTransactionFloorFees where f.SetTransactionFloorFee_id == id select f).SingleOrDefault();
			if (entity == null) throw new ApplicationException("row not found");
			dc.SetTransactionFloorFees.DeleteOnSubmit(entity);
			dc.SubmitChanges();
		}

		public static List<FloorFeeVO> GetFloorFees(string domainHost, int floorId)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			return (from f in dc.SetTransactionFloorFees where f.SetTransactionFloor_id == floorId orderby f.AmountTop ascending select new FloorFeeVO(f)).ToList();
		}

		public static FloorFeeVO GetFloorFee(string domainHost, int id)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			return (from f in dc.SetTransactionFloorFees where f.SetTransactionFloorFee_id == id select new FloorFeeVO(f)).SingleOrDefault();
		}

		public static void SaveFloor(string domainHost, FloorVO fv)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			var entity = (from f in dc.SetTransactionFloors where f.SetTransactionFloor_id == fv.SetTransactionFloor_id select f).SingleOrDefault();
			if (entity == null)
			{
				entity = new Netpay.Dal.Netpay.SetTransactionFloor();
				entity.SettlementType_id = (byte)fv.SettlementType_id;
				entity.Merchant_id = fv.Merchant_id;
				entity.DebitCompany_id = fv.DebitCompany_id;
				entity.Affiliate_id = fv.Affiliate_id;

				dc.SetTransactionFloors.InsertOnSubmit(entity);
			}
			entity.Title = fv.Title;
			entity.CurrencyISOCode = fv.CurrencyISOCode;
			dc.SubmitChanges();
		}

		public static void DeleteFloor(string domainHost, int id)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			var entity = (from f in dc.SetTransactionFloors where f.SetTransactionFloor_id == id select f).SingleOrDefault();
			if (entity == null) throw new ApplicationException("row not found");
			dc.SetTransactionFloors.DeleteOnSubmit(entity);
			dc.SubmitChanges();
		}

		public static List<FloorVO> GetFloors(string domainHost, SettingsFilter filters)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			var exp = (from f in dc.SetTransactionFloors select f);
			exp = exp.Where(f => f.SettlementType_id == (int)filters.SettlementType_id);
			if (filters.Merchant_id != null) exp = exp.Where(f => f.Merchant_id == filters.Merchant_id);
			if (filters.Affiliate_id != null) exp = exp.Where(f => f.Affiliate_id == filters.Affiliate_id);
			if (filters.DebitCompany_id != null) exp = exp.Where(f => f.DebitCompany_id == filters.DebitCompany_id);
			if (filters.CurrencyISOCode != null) exp = exp.Where(f => f.CurrencyISOCode == filters.CurrencyISOCode);
			return exp.Select(f => new FloorVO(f)).ToList();
		}

		public static void SaveTransactionFees(string domainHost, TransactionFeesVO fv)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			var entity = (from f in dc.SetTransactionFees where f.SetTransactionFee_id == fv.SetTransactionFee_id select f).SingleOrDefault();
			if (entity == null)
			{
				entity = new Netpay.Dal.Netpay.SetTransactionFee();
				dc.SetTransactionFees.InsertOnSubmit(entity);
			}

			entity.SettlementType_id = (byte)fv.SettlementType_id;
			entity.Merchant_id = fv.Merchant_id;
			entity.DebitCompany_id = fv.DebitCompany_id;
			entity.Affiliate_id = fv.Affiliate_id;

			entity.AmountType_id = (byte)fv.AmountType_id;
			entity.CurrencyISOCode = fv.CurrencyISOCode;
			entity.FeeCalcMethod_id = (byte) fv.FeeCalcMethod_id;

			entity.PaymentMethod_id = (short?) fv.PaymentMethod_id;
			entity.CountryISOCode = fv.CountryISOCode;
			entity.CountryGroup_id = fv.CountryGroup_id;

			entity.SetTransactionFloor_id = fv.SetTransactionFloor_id;
			entity.PercentValue = fv.PercentValue;
			entity.FixedAmount = fv.FixedAmount;
			entity.SettlementPercentValue = fv.SettlementPercentValue;
			dc.SubmitChanges();		
		}

		public static void DeleteTransactionFees(string domainHost, int id)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			var entity = (from f in dc.SetTransactionFees where f.SetTransactionFee_id == id select f).SingleOrDefault();
			if (entity == null) throw new ApplicationException("row not found");
			dc.SetTransactionFees.DeleteOnSubmit(entity);
			dc.SubmitChanges();
		}

		public static TransactionFeesVO GetTransactionFee(string domainHost, int id)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			return (from f in dc.SetTransactionFees where f.SetTransactionFee_id == id select new TransactionFeesVO(f)).SingleOrDefault();
		}

		public static List<TransactionFeesVO> GetTransactionFees(string domainHost, SettingsFilter filters)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			var exp = (from f in dc.SetTransactionFees select f);
			exp = exp.Where(f => f.SettlementType_id == (int)filters.SettlementType_id);
			if (filters.Merchant_id != null) exp = exp.Where(f => f.Merchant_id == filters.Merchant_id);
			if (filters.Affiliate_id != null) exp = exp.Where(f => f.Affiliate_id == filters.Affiliate_id);
			if (filters.DebitCompany_id != null) exp = exp.Where(f => f.DebitCompany_id == filters.DebitCompany_id);
			if (filters.CurrencyISOCode != null) exp = exp.Where(f => f.CurrencyISOCode == filters.CurrencyISOCode);
			return exp.Select(f => new TransactionFeesVO(f)).ToList();
		}

		public static SettlementSettingsVO GetSettelmentSetting(string domainHost, int id)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			return (from f in dc.SetSettlements where f.SetSettlement_id == id select new SettlementSettingsVO(f)).SingleOrDefault();
		}

		public static void SaveSettelmentSettings(string domainHost, SettlementSettingsVO fv)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			var entity = (from f in dc.SetSettlements where f.SetSettlement_id == fv.SetSettelment_id select f).SingleOrDefault();
			if (entity == null) {
				entity = new Netpay.Dal.Netpay.SetSettlement();
				dc.SetSettlements.InsertOnSubmit(entity);
			}
			entity.SettlementType_id = (byte) fv.SettelmentType_id;
			entity.Merchant_id = fv.Merchant_id;
			entity.DebitCompany_id = fv.DebitCompany_id;
			entity.Affiliate_id = fv.Affiliate_id;
			entity.CurrencyISOCode = fv.CurrencyISOCode;
			entity.IsShowToSettle = fv.IsShowToSettle;
			entity.MinPayoutAmount = fv.MinPayoutAmount;
			entity.MinSettlementAmount = fv.MinSettlementAmount;
			entity.SettleInitialHoldDays = fv.SettleInitialHoldDays;
			entity.SettleHoldDays = fv.SettleHoldDays;
			entity.InvoiceIssuer_id = fv.InvoiceIssuer_id;
			entity.WireCurrencyIsoCode = fv.WireCurrencyIsoCode;
			entity.WireFeeAmount = fv.WireFeeAmount;
			entity.WireFeePercent = fv.WireFeePercent;
			entity.StorageFeeAmount = fv.StorageFeeAmount;
			entity.StorageFeeCurrencyIsoCode = fv.StorageFeeCurrencyIsoCode;
			dc.SubmitChanges();
			fv.SetSettelment_id = entity.SetSettlement_id;
		}

		public static List<SettlementSettingsVO> GetSettelmentSettings(string domainHost, SettingsFilter filters)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			var exp = (from f in dc.SetSettlements select f);
			exp = exp.Where(f => f.SettlementType_id == (int) filters.SettlementType_id);
			if (filters.Merchant_id != null) exp = exp.Where(f => f.Merchant_id == filters.Merchant_id);
			if (filters.Affiliate_id != null) exp = exp.Where(f => f.Affiliate_id == filters.Affiliate_id);
			if (filters.DebitCompany_id != null) exp = exp.Where(f => f.DebitCompany_id == filters.DebitCompany_id);
			if (filters.CurrencyISOCode != null) exp = exp.Where(f => f.CurrencyISOCode == filters.CurrencyISOCode);
			return exp.Select(f => new SettlementSettingsVO(f)).ToList();
		}

		public static void DeleteSettelmentSettings(string domainHost, int id)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			var entity = (from f in dc.SetSettlements where f.SetSettlement_id == id select f).SingleOrDefault();
			if (entity == null) throw new ApplicationException("row not found");
			dc.SetSettlements.DeleteOnSubmit(entity);
			dc.SubmitChanges();
		}

		public static List<SettlementScheduleVO> GetSettelmentSchedule(string domainHost, int setSettlementId)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			var exp = (from f in dc.SetSettlementSchedules where f.SetSettlement_id == setSettlementId orderby f.DayTo select f);
			return exp.Select(f => new SettlementScheduleVO(f)).ToList();
		}

		public static void SetSettelmentSchedule(string domainHost, int setSettlementId, List<SettlementScheduleVO> values)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			dc.SetSettlementSchedules.DeleteAllOnSubmit((from f in dc.SetSettlementSchedules where f.SetSettlement_id == setSettlementId select f));
			dc.SubmitChanges();
			foreach (SettlementScheduleVO v in values)
			{
				var entity = new Netpay.Dal.Netpay.SetSettlementSchedule();
				dc.SetSettlementSchedules.InsertOnSubmit(entity);
				entity.SetSettlement_id = setSettlementId;
				entity.DayTo = v.DayTo;
				entity.DaySettlement = v.DaySettlement;
			}
			dc.SubmitChanges();
		}


	}
}
