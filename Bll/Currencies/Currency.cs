﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll
{
	public class Currency : BaseDataObject
	{
		private Dal.Netpay.CurrencyList _entity;

        public const string SecuredObjectName = "Manage";

        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Currencies.Module.Current, SecuredObjectName); } }

        public int ID { get { return _entity.CurrencyID.GetValueOrDefault(); } }
		public CommonTypes.Currency EnumValue { get { return (CommonTypes.Currency)_entity.CurrencyID.GetValueOrDefault(); } }
		public DateTime InsertDate { get { return _entity.InsertDate.GetValueOrDefault(); } }

		public string Name { get { return _entity.Name; } set { _entity.Name = value; } }
		public string Symbol { get { return _entity.Symbol; } set { _entity.Symbol = value; } }
		public string IsoCode { get { return _entity.CurrencyISOCode; } set { _entity.CurrencyISOCode = value; } }
		public string IsoNumber { get { return _entity.ISONumber; } set { _entity.ISONumber = value; } }
		public bool IsSymbolBeforeAmount { get { return _entity.IsSymbolBeforeAmount.GetValueOrDefault(); } set { _entity.IsSymbolBeforeAmount = value; } }

		public decimal BaseRate { get { return _entity.BaseRate.GetValueOrDefault(1); } set { _entity.BaseRate = value; } }
		public decimal ConversionFee { get { return _entity.ExchangeFeeInd.GetValueOrDefault(); } set { _entity.ExchangeFeeInd = value; } }
		public System.DateTime? RateRequestDate { get { return _entity.RateRequestDate; } set { _entity.RateRequestDate = value; } }
		public System.DateTime? RateValueDate { get { return _entity.RateValueDate; } set { _entity.RateValueDate = value; } }
		public decimal? MaxTransactionAmount { get { return _entity.MaxTransactionAmount; } set { _entity.MaxTransactionAmount = value; } }

		private Currency(Dal.Netpay.CurrencyList entity)
		{ 
			_entity = entity;
		}

        public Currency()
        {
            _entity = new Dal.Netpay.CurrencyList();
        }

		public static List<Currency> Search()
		{            
			return Cache;
		}

		public static List<Currency> Cache
		{
			get
			{
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return Domain.Current.GetCachData("Currencies", () => {
                    return new Domain.CachData((from c in DataContext.Reader.CurrencyLists select new Currency(c)).ToList(), DateTime.Now.AddHours(5));
                }) as List<Currency>;
			}
		}

		public void Save()
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
            {
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Edit);
                if (_entity.CurrencyID == 0) ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Add);
            }

            DataContext.Writer.CurrencyLists.Update(_entity, true);
			DataContext.Writer.SubmitChanges();
		}

		public static Currency Get(string isoCode)
		{
			return Cache.Where(c => c.IsoCode == isoCode).SingleOrDefault();
		}

        public static bool IsIsoCodeExist(string isoCode)
        {
            if (string.IsNullOrEmpty(isoCode)) return false;
            if (isoCode.Length != 3) return false;
            return Cache.Exists(x => x.IsoCode == isoCode);
        }

        public static List<int> GetCurrenciesIdsByIsoCodes(List<string> isocodelist)
        {
            if (isocodelist.Count == 0)
            {
                return new List<int> { -1 };
            }
            else
            {
                return Cache.Where(x => isocodelist.Contains(x.IsoCode)).Select(x => x.ID).ToList();
            }
        }

		public static Currency Get(int currencyId)
		{
			return Cache.Where(c => c.ID == (int)currencyId).SingleOrDefault();
		}
	
		public static Currency Get(CommonTypes.Currency currencyId)
		{
			return Get((int)currencyId);
		}

        public static string GetIsoCode(CommonTypes.Currency currencyId)
        {
            var v = Get((int)currencyId);
            if (v == null) return null;
            return v.IsoCode;
        }

        public static Currency GetByIsoNumber(int isoNumber)
		{
			return Cache.Where(c => c.IsoNumber == isoNumber.ToString()).SingleOrDefault();
		}
		
		/* conversion */
		public static decimal Convert(decimal amount, Currency fromCurrency, Currency toCurrency)
		{
			return amount * (fromCurrency.BaseRate / toCurrency.BaseRate);
		}

		public static decimal ConvertRate(Currency fromCurrency, Currency toCurrency)
		{
			return fromCurrency.BaseRate / toCurrency.BaseRate;
		}

		public static decimal ConvertRateWithFee(Currency fromCurrency, Currency toCurrency)
		{
			return fromCurrency.BaseRate * ((1 - toCurrency.ConversionFee) / toCurrency.BaseRate);
		}

		public static decimal Convert(decimal amount, string fromCurrency, string toCurrency)
		{
			return Convert(amount, Get(fromCurrency), Get(toCurrency));
		}

		public static decimal Convert(decimal amount, CommonTypes.Currency fromCurrency, CommonTypes.Currency toCurrency)
		{
			return Convert(amount, Get(fromCurrency), Get(toCurrency));
		}

		public static decimal ConvertRate(string fromCurrency, string toCurrency)
		{
			return ConvertRate(Get(fromCurrency), Get(toCurrency));
		}

	}

	public static partial class Extentions
	{
		public static string ToHtml(this decimal source, Currency currency)
		{
			if (source < 0)
                return string.Format("<span class=\"negativeNumber\">{0} {1}</span>", currency.Symbol, source.ToString(Infrastructure.Application.AmountFormat));

			return string.Format("<span>{0} {1}</span>", currency.Symbol, source.ToString(Infrastructure.Application.AmountFormat));
		}

		public static string ToHtml(this decimal source, int currencyID)
		{
			var currency = Currency.Get(currencyID);
			return ToHtml(source, currency);
		}

		public static string ToHtml(this decimal source, CommonTypes.Currency currencyID)
		{
			return ToHtml(source, (int)currencyID);
		}

		public static string ToAmountFormat(this decimal source, int currencyID, bool isNegative)
		{
			var currency = Currency.Get(currencyID);
			if (isNegative)
                return string.Format("{0} -{1}", currency.Symbol, source.ToString(Infrastructure.Application.AmountFormat));
			else
                return string.Format("{0} {1}", currency.Symbol, source.ToString(Infrastructure.Application.AmountFormat));
		}

		public static string ToAmountFormat(this decimal source, string currencyISO)
		{
			var currency = Currency.Get(currencyISO);
            return string.Format("{0} {1}", currency.Symbol, source.ToString(Infrastructure.Application.AmountFormat));
		}

		public static string ToAmountFormat(this decimal source, Currency currency)
		{
            return string.Format((currency.IsSymbolBeforeAmount ? "{0}{1}" : "{1} {0}"), currency.Symbol, source.ToString(Infrastructure.Application.AmountFormat));
		}

		public static string ToAmountFormatCustom(this decimal source, string currencySymbol)
		{
            return string.Format("{0} {1}", currencySymbol, source.ToString(Infrastructure.Application.AmountFormat));
		}
		public static string ToAmountFormat(this double source, int currencyID)
		{
			var currency = Currency.Get(currencyID);
            return string.Format("{0} {1}", currency.Symbol, source.ToString(Infrastructure.Application.AmountFormat));
		}

		public static string ToAmountFormat(this double source, string currencyIso)
		{
			var currency = Currency.Get(currencyIso);
            return string.Format("{0} {1}", currency.Symbol, source.ToString(Infrastructure.Application.AmountFormat));
		}

		public static string ToAmountFormat(this decimal source)
		{
            return source.ToString(Infrastructure.Application.AmountFormat);
		}

		public static string ToAmountFormat(this decimal source, int currencyID)
		{
			var currency = Currency.Get(currencyID);
            return string.Format("{0} {1}", currency.Symbol, source.ToString(Infrastructure.Application.AmountFormat));
		}

		public static string ToIsoAmountFormat(this decimal source, int currencyID)
		{
			var currency = Currency.Get(currencyID);
            return string.Format("{0} {1}", currency.IsoCode, source.ToString(Infrastructure.Application.AmountFormat));
		}

        public static string ToIsoAmountFormat(this decimal source, string currencyIso)
        {
            return string.Format("{0} {1}", currencyIso, source.ToString(Infrastructure.Application.AmountFormat));
        }

		public static string ToAmountFormat(this decimal source, CommonTypes.Currency currencyID)
		{
			var currency = Currency.Get((int)currencyID);
            return string.Format("{0} {1}", currency.Symbol, source.ToString(Infrastructure.Application.AmountFormat));
		}
	}

}


