﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll.BankHandler
{
	public class ShvaAuthHandler : BankHandlerBase
	{
        private DebitCompany _debitCompanyID;
        public override DebitCompany DebitCompanyID { get { return _debitCompanyID; } }
        public override string BankDirName { 
            get {
                if (_debitCompanyID == DebitCompany.ShvaCal) return "VisaCal";
                else if (_debitCompanyID == DebitCompany.ShvaIsracard) return "Isracard";
                return base.BankDirName;
            } 
        }

        public ShvaAuthHandler(DebitCompany debitCompanyID) { _debitCompanyID = debitCompanyID; }

        //private int[] DebitCompanyIds = { 11, 55, 56 };
        //private string ServiceAddress = "http://192.168.5.11:81/Shva"; //"https://www.shva-online.co.il/ash/abscheck/absrequest.asmx";

        public override void GenerateFile()
        {
            var terminalNumbers = Process.Batch.GetTerminalNumbers(new List<int>() { (int)DebitCompanyID });
			var terminals = (from t in Infrastructure.DataContext.Reader.tblDebitTerminals where terminalNumbers.Contains(t.terminalNumber) select new DebitCompanies.Terminal(t)).ToList();

            foreach (var g in terminals)
			{
                var batch = new Process.Batch(g.DebitCompany, g.TerminalNumber);
                batch.SetInternalID(0);
                batch.MarkTransationsForBatch();
                try
                {
                    var transactions = batch.GetTransactions();
                    if (transactions.Count == 0) continue;

                    string password, password3D;
                    g.GetDecryptedData(out password, out password3D);

                    string fileName = GetFilePath(EpaFolder.New, g.TerminalNumber + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xml");
                    fileName = Domain.GetUniqueFileName(fileName);
                    using (var fileWriter = new System.IO.StreamWriter(fileName))
                    {
                        fileWriter.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                        fileWriter.WriteLine("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
                        fileWriter.WriteLine(" <soap:Body>");
                        fileWriter.WriteLine("  <DepositTransactions xmlns=\"http://shva.co.il/xmlwebservices/\">");
                        fileWriter.WriteLine("  <MerchantNumber>{0}</MerchantNumber>\r\n", g.AccssesName1);
                        fileWriter.WriteLine("   <UserName>{0}</UserName>\r\n", g.AccssesName2);
                        fileWriter.WriteLine("   <Password>{0}</Password>\r\n", password);
                        fileWriter.WriteLine("   <TransArr>");
                        foreach (var v in transactions)
                            fileWriter.WriteLine("    <string>{0}</string>\r\n", v.BatchData);
                        fileWriter.WriteLine("   </TransArr>");
                        fileWriter.WriteLine("  </DepositTransactions>");
                        fileWriter.WriteLine(" </soap:Body>");
                        fileWriter.WriteLine("</soap:Envelope>");
                        fileWriter.Close();
                    }
                    batch.SetPrepared(System.IO.Path.GetFileName(fileName), transactions.Count, 0, 0);
                } catch(Exception ex) {
                    batch.UnMarkTransationsForBatch();
                    Logger.Log(LogTag.AuthBatch, ex);
                }
            }
        }

        public override void HandlePending(BankHandlerContext context)
        {
            var bank = base.LoadEpaLogin(Accounts.ExternalServiceLogin.ServiceTypes.Auth, Accounts.ExternalServiceLogin.Protocols.HTTPS);
            if (!bank.IsActive) return;

            var batch = Process.Batch.Load((int)DebitCompanyID, context.File.Name);
            var client = new System.Net.WebClient();
            var tmpFileName = context.CreateDecryptedTempFile(Manager);
            string serviceUrl =  bank.Method + "://" + bank.ServerURL;
            string responseData = null;
            try {
                int retValue = -1;
                client.Headers["Content-Type"] = "text/xml; charset=utf-8";
				client.Headers["SOAPAction"] = "http://shva.co.il/xmlwebservices/DepositTransactions";
                responseData = client.UploadString(serviceUrl, "POST", System.IO.File.ReadAllText(tmpFileName));
				if (!string.IsNullOrEmpty(responseData)) {
					System.Xml.XmlDocument retData = new System.Xml.XmlDocument();
                    retData.LoadXml(responseData);
                    retValue = retData.GetElementsByTagName("DepositTransactionsResult")[0].InnerText.ToNullableInt32().GetValueOrDefault(-1);
                    if (retValue == 0 && batch != null) batch.SetSent();
				}
				if (retValue != 0) throw new Exception("Response DepositTransactionsResult <> 0");
			} catch (Exception ex) {
                if (batch != null) batch.SetFailSent(ex.Message);
                var wex = ex as System.Net.WebException;
                if (wex != null && wex.Response != null)
                    using (var stream = wex.Response.GetResponseStream()) 
                        if(stream != null) using (var reader = new System.IO.StreamReader(stream)) responseData = reader.ReadToEnd();
                string extraData = string.Format("To server:{0}\r\nResponse:{1}", serviceUrl, responseData);
                throw new ExceptionWithInfo("Shve batch handler - send error for file:" + context.File.FullName, ex, extraData);
            } finally {
                if (System.IO.File.Exists(tmpFileName)) System.IO.File.Delete(tmpFileName);
            }
		}
	}
}
