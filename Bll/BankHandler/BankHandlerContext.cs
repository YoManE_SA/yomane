﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography;


namespace Netpay.Bll.BankHandler
{
	public class BankHandlerContext
	{
        private StringBuilder _rowErrors = null;
        public FileInfo File { get; set; }

		public BankHandlerContext(string fileName)
		{
            File = new FileInfo(fileName);
		}

        public override string ToString() { return File.FullName; }

        public Stream DecryptedStream
        {
            get {
                return Netpay.Crypt.SymEncryption.GetKey(Infrastructure.Domain.Current.EncryptionKeyNumber).GetDecryptedFileStream(File.FullName);
            }
        }
        
        public StreamReader DecryptedStreamReader
		{
			get { return new StreamReader(DecryptedStream); }
		}

        public void EncryptFile(string destName)
        {
            Netpay.Crypt.SymEncryption.GetKey(Infrastructure.Domain.Current.EncryptionKeyNumber).EncryptFile(File.FullName, destName);
        }

        public void DescryptFile(string destName)
        {
            Netpay.Crypt.SymEncryption.GetKey(Infrastructure.Domain.Current.EncryptionKeyNumber).DecryptFile(File.FullName, destName);
        }
        
        public string CreateDecryptedTempFile(BankHandlerManagerBase manager, bool keepFileName = false)
		{
            string fileName = manager.MapTempFile(File.Name);
            if (keepFileName) {
                if (System.IO.File.Exists(fileName)) System.IO.File.Delete(fileName);
            } else fileName = Infrastructure.Domain.GetUniqueFileName(fileName);
            DescryptFile(fileName);
            return fileName;
            /*
            using (var sr = DecryptedStream) {
                try {
                    using (var sw = new System.IO.FileStream(fileName, FileMode.Create))
                    {
                        try { 
                            int readlen;
                            byte[] buffer = new byte[1024];
                            do {
                                readlen = sr.Read(buffer, 0, buffer.Length);
                                sw.Write(buffer, 0, readlen);
                            } while (readlen == buffer.Length);
                        } finally { 
                            sw.Close();
                        }
                    }
                } finally {
                    sr.Close();
                }
            }
            */
		}

        public string MoveTo(BankHandlerBase handler, EpaFolder targetFolder)
        {
            string destinationPath = Infrastructure.Domain.GetUniqueFileName(handler.GetFilePath(targetFolder, File.Name));
            File.MoveTo(destinationPath);
            File = new System.IO.FileInfo(destinationPath);
            return destinationPath;
        }

        public void HandleError(BankHandlerBase handler, Exception ex) 
        {
            WriteRowErrors(handler);
            Infrastructure.Logger.Log(handler.Manager.LogTag, ex, string.Format("Domain: {0}\r\nEncKey:{1}\r\nBank File: {2}", Infrastructure.Domain.Current.Host, Infrastructure.Domain.Current.EncryptionKeyNumber, File.FullName));
            try { MoveTo(handler, EpaFolder.Error); }
            catch { }
        }

        public bool WriteRowErrors(BankHandlerBase handler)
        {
            if (_rowErrors == null) return false;
            Infrastructure.Logger.Log(Infrastructure.LogSeverity.Error, handler.Manager.LogTag, "File '" + File.FullName + "' Row errors:", _rowErrors.ToString());
            return true;
        }

        public void LogRowError(string line, string message)
        {
            if (_rowErrors == null) _rowErrors = new StringBuilder();
            _rowErrors.AppendFormat("Line:{0} - {1}.\r\n", line, message);
        }

        public void LogRowError(Exception ex, string line)
        {
            LogRowError(line, ex.ToString());
        }

        public void LogRowErrorSize(int exprectedSize, string line)
        {
            LogRowError(line, "Invalid record size should be " + exprectedSize);
        }


    }
}
