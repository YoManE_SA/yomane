﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.DataAccess;
using System.IO;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using Netpay.Dal.Netpay;
using Netpay.CommonTypes;

namespace Netpay.Bll.BankHandler
{
	public class IsracardBankHandler : BankHandlerBase
	{
        public override DebitCompany DebitCompanyID { get { return Infrastructure.DebitCompany.ShvaIsracard; } }
        public override string BankDirName { get { return "Isracard"; } }

        public override void HandlePending(BankHandlerContext context)
		{
            string fileName = context.File.Name;
			if (fileName.IndexOf("CHS") != -1)
                ParseChsFile(context);
			else if (fileName.IndexOf("SVA") != -1)
                ParseSvaYadFile(context);
			else if (fileName.IndexOf("YAD") != -1)
                ParseSvaYadFile(context);
			else if (fileName.IndexOf("RST") != -1 && fileName.StartsWith("5"))
                ParseRst5File(context);
			else throw new Exception("IsracardBankHandler File Not Supported '" + context.File.Name + "'");
		}

        private void ParseSvaYadFile(BankHandlerContext context)
		{
			List<tblEpaPending> rows = new List<tblEpaPending>();
            StreamReader reader = context.DecryptedStreamReader;
			string line = reader.ReadLine();

			while (line != null)
			{
				if (line.Length != 500)
				{
                    context.LogRowErrorSize(500, line);
					continue;
				}
				try
				{
					tblEpaPending row = new tblEpaPending();
                    row.StoredFileName = context.File.Name;
					row.InsertDate = DateTime.Now;
					string currency = line.Substring(82, 3);
					row.Currency = (int)(currency == "100" ? CommonTypes.Currency.EUR : (currency == "019" ? CommonTypes.Currency.USD : CommonTypes.Currency.ILS));
					row.TerminalNumber = line.Substring(8, 8).Trim();
					row.TransDate = DateTime.ParseExact(line.Substring(24, 8), "yyyyMMdd", null);
					string TransType = line.Substring(57, 3);
					if (TransType == "140")
						row.IsRefund = false;
					else if (TransType == "960")
						row.IsRefund = true;
					row.Amount = line.Substring(72, 9).ToDecimal(0) / 100m;
					row.Installment = line.Substring(112, 2).ToInt32(1, 9000, 1);
					row.PayoutDate = DateTime.ParseExact(line.Substring(189, 8), "yyyyMMdd", null);
					row.ApprovalNumber = line.Substring(203, 7).Trim();
					row.CardNumber256 = Encryption.Encrypt(line.Substring(210, 18).Replace(" ", ""));
					if (row.Amount < 0) row.Amount = -row.Amount;
					if (row.IsRefund != null) rows.Add(row);
				}
				catch (Exception ex)
				{
                    context.LogRowError(ex, line);
					continue;
				}

				if (rows.Count == rows.Capacity) 
				{
					InsertParsed(rows);
					rows.Clear();
				}
				line = reader.ReadLine();
			}
			reader.Close();
			InsertParsed(rows);
		}

        private void ParseChsFile(BankHandlerContext context)
		{
			List<tblEpaPending> rows = new List<tblEpaPending>();
            StreamReader reader = context.DecryptedStreamReader;
			string line = reader.ReadLine();

			while (line != null)
			{
				if (line.Length != 450)
				{
                    context.LogRowErrorSize(450, line);
					continue;
				}

				int rowCode = line.Substring(1, 2).Trim().ToInt32(0);
				if (rowCode == 35 || rowCode == 45) 
				{
					try
					{
						tblEpaPending row = new tblEpaPending();
                        row.StoredFileName = context.File.Name;
						row.InsertDate = DateTime.Now;
						row.TerminalNumber = line.Substring(238, 7).Trim();
						string currency = line.Substring(63, 3);
						row.Currency = (int)(currency == "100" ? CommonTypes.Currency.EUR : (currency == "019" ? CommonTypes.Currency.USD : CommonTypes.Currency.ILS));
						row.TransDate = DateTime.ParseExact("20" + line.Substring(186, 6), "yyyyMMdd", null);
						string TransType = line.Substring(59, 3);
						row.Amount = line.Substring(74, 14).ToDecimal(0m) / 100m;
						row.Installment = line.Substring(201, 2).ToInt32(1, 9000, 1);
						row.PayoutDate = DateTime.ParseExact("20" + line.Substring(38, 6), "yyyyMMdd", null);
						row.ApprovalNumber = line.Substring(262, 7).Trim();
						string CardNumber = line.Substring(169, 16).Replace(" ", "");
						if (CardNumber.StartsWith("3755")) CardNumber = CardNumber.Substring(0, 15);
						row.CardNumber256 = Encryption.Encrypt(CardNumber);
						if (TransType == "140") row.IsRefund = false;
						else if (TransType == "960") row.IsRefund = true;
						if (row.Amount < 0) row.Amount = -row.Amount;
						if (row.IsRefund != null) rows.Add(row);
					}
					catch (Exception ex)
					{
                        context.LogRowError(ex, line);
						continue;
					}
				}
				if (rows.Count == rows.Capacity) {
					InsertParsed(rows);
					rows.Clear();
				}
				line = reader.ReadLine();
			}
			reader.Close();
			InsertParsed(rows);
		}

        private void ParseRst5File(BankHandlerContext context)
		{
			List<tblEpaPending> rows = new List<tblEpaPending>();
            StreamReader reader = context.DecryptedStreamReader;
			string line = reader.ReadLine();

			while (line != null)
			{
				if (line.Length != 300)
				{
                    context.LogRowErrorSize(300, line);
					continue;
				}

				int rowCode = line.Substring(0, 1).Trim().ToInt32(0);
				if (rowCode == 3 || rowCode == 5)
				{
					try
					{
						tblEpaPending row = new tblEpaPending();
                        row.StoredFileName = context.File.Name;
						row.InsertDate = DateTime.Now;
						row.TerminalNumber = line.Substring(226, 7).Trim();
						row.Currency = (int)(line.Substring(58, 3) == "019" ? CommonTypes.Currency.USD : CommonTypes.Currency.ILS);
						row.TransDate = DateTime.ParseExact("20" + line.Substring(141, 6), "yyyyMMdd", null);
						string TransType = line.Substring(47, 3);
						row.Amount = line.Substring(65, 14).ToDecimal(0m) / 100m;
						row.Installment = line.Substring(156, 2).ToInt32(1, 9000, 1);
						row.PayoutDate = DateTime.ParseExact("20" + line.Substring(33, 6), "yyyyMMdd", null);
						row.ApprovalNumber = line.Substring(250, 7).Trim();
						string CardNumber = line.Substring(169, 16).Replace(" ", "");
						if (CardNumber.StartsWith("3755")) CardNumber = CardNumber.Substring(0, 15);
						row.CardNumber256 = Encryption.Encrypt(CardNumber);
						if (TransType == "140") row.IsRefund = false;
						else if (TransType == "960") row.IsRefund = true;
						if (row.Amount < 0) row.Amount = -row.Amount;
						if (row.IsRefund != null) rows.Add(row);
					}
					catch (Exception ex)
					{
                        context.LogRowError(ex, line);
						continue;
					}
				}

				if (rows.Count == rows.Capacity) 
				{
					InsertParsed(rows);
					rows.Clear();
				}
				line = reader.ReadLine();
			}
			reader.Close();
			InsertParsed(rows);
		}
	}
}
