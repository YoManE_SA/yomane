﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Email.Pop3;
using Netpay.Infrastructure.Email.Pop3.SharpMimeTools;
using Netpay.Infrastructure.Security;
using Netpay.CommonTypes;

namespace Netpay.Bll.BankHandler
{
	public class AtlasChbBankHandler : BankHandlerBase
	{
		private const int _recordBuffer = 100;
        public override DebitCompany DebitCompanyID { get { return Infrastructure.DebitCompany.Atlas; } }

		public override void FetchFile() //sftp
		{
            var bank = LoadEpaLogin(Accounts.ExternalServiceLogin.ServiceTypes.CHB, Accounts.ExternalServiceLogin.Protocols.SFTP);
            if (!bank.IsActive) return;

            string scriptFileName = Manager.MapTempFile("AtlasChbFtpScript.ftp");
			string logFile = Manager.MapTempFile("AtlasChbFtpLogFile.log");
            //create script file
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(scriptFileName)) {
                sw.WriteLine(bank.GetProtocolLoginCommand(GetPrivateKey(), GetHostKey()));
                sw.WriteLine("option batch continue");
                sw.WriteLine("option confirm off");
                sw.WriteLine("lcd " + GetPath(EpaFolder.New));
			    //sw.WriteLine("get -delete /from_bus/csv/TSPUK*.csv");
			    sw.WriteLine("get \"/home/netpay/Chargebak documents/*.*\"");
			    sw.WriteLine("exit");
                sw.Close();
            }
            var ret = bank.ExecuteFTPScript(scriptFileName, logFile);
            if (ret != 0) throw new ApplicationException("Atlas SFTP failed with code = " + ret);
        }

        public override void HandlePending(BankHandlerContext context)
		{
			string fileNameWithNoExt = System.IO.Path.GetFileNameWithoutExtension(context.File.FullName).ToLower();
            if (fileNameWithNoExt.IndexOf("ctfi-") > -1)
                HandleCTFIPending(context);
            else if (fileNameWithNoExt.IndexOf("iitf-") > -1)
                HandleIIFTPending(context);
            else throw new Exception("Unsupported file type:" + fileNameWithNoExt);
        }

        public void HandleCTFIPending(BankHandlerContext context)
		{
			List<tblChbPending> rows = new List<tblChbPending>();
			StreamReader reader = context.DecryptedStreamReader;
			try
			{
				/*
				The file ctfi also does not contain transaction ID
				- Card number is on position 5-21,
				- Transaction date 58-62,
				- Amount is on position 77-92 for the original currency,
				- Authorization code is on position 152-158.
				- Terminal number is in line 2 on the position 81-89.
				*/
				int rowNum = 0;
				tblChbPending entity = null;
				for (string currentLine = reader.ReadLine(); currentLine != null; currentLine = reader.ReadLine())
				{
					if (currentLine.Length == 0) continue;
					try
					{
						if (rowNum++ % 2 == 0) { //even line
							entity = new tblChbPending();
							entity.StoredFileName = context.File.Name;
							entity.Installment = 1;
							entity.InsertDate = DateTime.Now;
							entity.TransCount = null;
							entity.TransID = null;
							entity.IsChargeback = true;
							entity.IsPhotocopy = !entity.IsChargeback;

							entity.ApprovalNumber = currentLine.Substring(151, 6);
							entity.CardNumber256 = Netpay.Infrastructure.Security.Encryption.Encrypt(currentLine.Substring(4, 16));
							entity.Amount = currentLine.Substring(76, 12).ToNullableDecimal() / 100;
							entity.Currency = Currency.GetByIsoNumber(currentLine.Substring(88, 3).ToInt32()).ID;
							//entity.DebitReferenceNum = currentLine.Substring();
							//entity.ReasonCode = currentLineSplit[1].Trim();
							//if (string.IsNullOrWhiteSpace(entity.ReasonCode)) entity.ReasonCode = "0";
							//entity.CaseID = currentLineSplit[0].Trim();

							entity.TransDate = new DateTime(DateTime.Now.Year, currentLine.Substring(57, 2).ToInt32(), currentLine.Substring(59, 2).ToInt32());
							if (entity.TransDate > DateTime.Now) entity.TransDate = entity.TransDate.Value.AddYears(-1);

							//entity.ChargebackDate = parsedDate;
							//entity.DeadlineDate = parsedDate;
						} else { //odd line
							entity.TerminalNumber = currentLine.Substring(80, 8);
							rows.Add(entity);
						}
					}
					catch (Exception ex)
					{
                        context.LogRowError(ex, currentLine);
						continue;
					}
				}
			}
			catch { }
			finally
			{
				reader.Close();
				InsertParsed(rows);
			}
		}

        public void HandleIIFTPending(BankHandlerContext context)
		{
			List<tblChbPending> rows = new List<tblChbPending>();
			StreamReader reader = context.DecryptedStreamReader;
			try
			{
				/*
				In the file iitf transaction ID does not exist
				- Card number is on 7-23 position
				- Transaction date is on 60-64 position
				- Amount is on 64-76 + 3 fields with 978 currency code (this refers to the transactions in eur), Amounts in usd is on position 79-91 + 3 fields for the currency code 840 for usd currency
				- Authorisation code is on 154-160 position
				- Terminal number is on the line number 2 on the position 83-91
				*/
				int rowNum = 0;
				tblChbPending entity = null;
				for (string currentLine = reader.ReadLine(); currentLine != null; currentLine = reader.ReadLine())
				{
					if (currentLine.Length == 0) continue;
					try
					{
						if (rowNum++ % 2 == 0) { //even line
							entity = new tblChbPending();
							entity.StoredFileName = context.File.Name;
							entity.Installment = 1;
							entity.InsertDate = DateTime.Now;
							entity.TransCount = null;
							entity.TransID = null;
							entity.IsChargeback = true;
							entity.IsPhotocopy = !entity.IsChargeback;

							entity.CardNumber256 = Netpay.Infrastructure.Security.Encryption.Encrypt(currentLine.Substring(6, 16));
							entity.ApprovalNumber = currentLine.Substring(153, 6);
							entity.Amount = currentLine.Substring(78, 12).ToNullableDecimal() / 100;
							entity.Currency = Currency.GetByIsoNumber(currentLine.Substring(90, 3).ToInt32()).ID;
							//entity.DebitReferenceNum = currentLine.Substring();
							//entity.ReasonCode = currentLineSplit[1].Trim();
							//if (string.IsNullOrWhiteSpace(entity.ReasonCode)) entity.ReasonCode = "0";
							//entity.CaseID = currentLineSplit[0].Trim();

							entity.TransDate = new DateTime(DateTime.Now.Year, currentLine.Substring(59, 2).ToInt32(), currentLine.Substring(61, 2).ToInt32());
							if (entity.TransDate > DateTime.Now) entity.TransDate = entity.TransDate.Value.AddYears(-1);

							//entity.ChargebackDate = parsedDate;
							//entity.DeadlineDate = parsedDate;
						} else { //odd line
							entity.TerminalNumber = currentLine.Substring(82, 8);
							rows.Add(entity);
						}
					}
					catch (Exception ex)
					{
                        context.LogRowError(ex, currentLine);
						continue;
					}
				}
			}
			catch { }
			finally
			{
				reader.Close();
				InsertParsed(rows);
			}
		}

	}
}