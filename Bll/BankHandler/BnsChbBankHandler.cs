﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;

using Netpay.Infrastructure.Email.Pop3;
using Netpay.Infrastructure.Email.Pop3.SharpMimeTools;
using Netpay.Infrastructure.Security;
using Netpay.CommonTypes;

namespace Netpay.Bll.BankHandler
{
	public class BnsChbBankHandler : BankHandlerBase
	{
		private const int _recordBuffer = 100;
        public override DebitCompany DebitCompanyID { get { return Infrastructure.DebitCompany.BnS; } }

        public override void HandleNew(BankHandlerContext context)
		{
			if (context.File.Extension.ToUpper() == ".PGP")
			{
				// decrypt & delete original
				string output = Netpay.Crypt.Pgp.DecryptFile(context.File.FullName, true);
				File.Delete(context.File.FullName);

				// replace context file 
				string newFileFullName = Path.ChangeExtension(context.File.FullName, "txt");
				context.File = new FileInfo(newFileFullName);
			}
            base.HandleNew(context);
		}

		public override void FetchFile() //sftp
		{
            var bank = LoadEpaLogin(Accounts.ExternalServiceLogin.ServiceTypes.CHB, Accounts.ExternalServiceLogin.Protocols.SFTP);
            if (!bank.IsActive) return;

            string scriptFileName = Manager.MapTempFile("BnsChbFtpScript.ftp");
            string logFile = Manager.MapTempFile("BnsChbFtpLogFile.log");
            //create script file
            System.IO.StreamWriter sw = new System.IO.StreamWriter(scriptFileName);
            sw.WriteLine(bank.GetProtocolLoginCommand(GetPrivateKey(), GetHostKey()));
            sw.WriteLine("option batch continue");
            sw.WriteLine("option confirm off");
            sw.WriteLine("lcd " + GetPath(EpaFolder.New));
			sw.WriteLine("get -delete /from_bus/csv/TSPUK*.csv");
            sw.WriteLine("exit");
            sw.Close();

            var ret = bank.ExecuteFTPScript(scriptFileName, logFile);
            if (ret != 0) throw new ApplicationException("BnS SFTP failed with code = " + ret);
        }

        public override void HandlePending(BankHandlerContext context)
		{
			if (context.File.Name.StartsWith("TSPUK"))
                HandleTSPUKPending(context);
			else if (context.File.Name.StartsWith("TXN_"))
                HandleTXNPending(context);
            else throw new Exception("Unsupported file type:" + context.File.FullName);
        }

        public void HandleTSPUKPending(BankHandlerContext context)
		{
			List<tblChbPending> rows = new List<tblChbPending>();
			StreamReader reader = context.DecryptedStreamReader;
			//StreamReader reader = new StreamReader(Context.File.FullName);
			try
			{
				reader.ReadLine();
				for (string currentLine = reader.ReadLine(); currentLine != null; currentLine = reader.ReadLine())
				{
					try
					{
						/*
							=== Chargeback file:
							0 - CASE ID     ;
							1 - RSN.;
							2 - MER-NO;
							3 - CARDHOLDER-NUMBER  ;
							4 - TXN-DATE  ;
							5 - AMOUNT-TXN ;
							6 - CUR;
							7 - AMOUNT-EURO;
							8 - REF-NO;
							9 - TICKET-NO ;
							10 - MRF                           ;
							11 - COMMENT-NO;
							=== Photocopy request fFile:
							0 - CASE ID     ;
							1 - RSN.;
							2 - MER-NO;
							3 - CARDHOLDER-NUMBER  ;
							4 - TXN-DATE  ;
							5 - AMOUNT-TXN ;
							6 - CUR;
							7 - AMOUNT-EURO;
							8 - REF-NO;
							9 - TICKET-NO ;
							10 - DEADLINE;
							11 - COMMENTS;
							12 - MRF
						*/
						string[] currentLineSplit = currentLine.Split(';');
						tblChbPending entity = new tblChbPending();
						entity.ReasonCode = currentLineSplit[1].Trim();
						if (string.IsNullOrWhiteSpace(entity.ReasonCode)) entity.ReasonCode = "0";
						entity.CaseID = currentLineSplit[0].Trim();
						entity.Amount = currentLineSplit[5].ToNullableDecimal();
						entity.ApprovalNumber = currentLineSplit[8].Trim();
						entity.CardNumber256 = Netpay.Infrastructure.Security.Encryption.Encrypt(currentLineSplit[3]);
						entity.Currency = Currency.Get(currentLineSplit[6]).ID;
						entity.InsertDate = DateTime.Now;
						entity.Installment = 1;
						entity.StoredFileName = context.File.Name;
						entity.TerminalNumber = null;
						entity.TransCount = null;
						DateTime transDate;
						if (DateTime.TryParseExact(currentLineSplit[4], "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out transDate))
							entity.TransDate = transDate;
						entity.TransID = null;

						DateTime parsedDate;
						if (context.File.Name.StartsWith("TSPUK01"))
						{
							string[] fileNameSplit = Path.GetFileNameWithoutExtension(context.File.Name).Split('_');
							string type = fileNameSplit[7];
							entity.IsChargeback = (type == "CB");
							entity.IsPhotocopy = !entity.IsChargeback;
							if (DateTime.TryParseExact(fileNameSplit[10], "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out parsedDate))
								entity.ChargebackDate = parsedDate;
						}
						else if (context.File.Name.StartsWith("PHOTO") || context.File.Name.StartsWith("CHARB"))
						{
							entity.IsChargeback = context.File.Name.StartsWith("CHARB");
							entity.IsPhotocopy = !entity.IsChargeback;
							string fileName = Path.GetFileNameWithoutExtension(context.File.Name);
							if (DateTime.TryParseExact(fileName.Substring(fileName.Length - 8, 8), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out parsedDate))
								entity.ChargebackDate = parsedDate;
						}
						else
							throw new Exception("Cannot get B+S file type from its name: " + context.File.Name);

						entity.DebitRefCode = currentLineSplit[(entity.IsChargeback.GetValueOrDefault(false) ? 10 : 12)].Trim();
						if (entity.IsPhotocopy.GetValueOrDefault(false))
						{
							if (DateTime.TryParseExact(currentLineSplit[10], "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out parsedDate))
								entity.DeadlineDate = parsedDate;
						}
                        /*
                        //filter by reason code
                        if (entity.IsPhotocopy.GetValueOrDefault()) {
                            string[] realRRCodes = new string[] { 
                                "28", "29", "33", "34", //VISA
                                "6305", "6321", "6322", "6323", "6341", "6342" //MC
                            };
                            if (!realRRCodes.Contains(entity.ReasonCode)) {
	    						entity.IsChargeback = true;
    							entity.IsPhotocopy = false;
                            }
                        }
                        */
						rows.Add(entity);
					}
					catch (Exception ex)
					{
                        context.LogRowError(ex, currentLine);
						continue;
					}
				}
			}
			catch { }
			finally
			{
				reader.Close();
				InsertParsed(rows);
			}
		}

        public void HandleTXNPending(BankHandlerContext context)
		{
			StreamReader reader = context.DecryptedStreamReader;
			var dc = new Netpay.Dal.DataAccess.NetpayDataContext(Domain.Current.Sql1ConnectionString);
			try
			{
				int i = 0; //header
				var columns = new System.Collections.Generic.Dictionary<string, int>();
				var colHeader = reader.ReadLine().Split(','); 
				foreach (string item in colHeader) columns.Add(item.Trim(), i++);
				
				for (string currentLine = reader.ReadLine(); currentLine != null; currentLine = reader.ReadLine())
				{
					try {
						string[] currentLineSplit = currentLine.Split(',');
						if (dc.ExecuteCommand("Update tblEpaPending Set AcquirerReferenceNum='{0}' Where DebitRefCode='{1}_01'", currentLineSplit[columns["ARN"]], currentLineSplit[columns["MERCHANT RF"]]) == 0)
							dc.ExecuteCommand("Update tblLogImportEPA Set AcquirerReferenceNum='{0}' Where TransID IN(Select ID From tblCompanyTransPass Where DebitReferenceCode='{1}_01')", currentLineSplit[columns["ARN"]], currentLineSplit[columns["MERCHANT RF"]]);
					}
					catch (Exception ex)
					{
                        context.LogRowError(ex, currentLine);
						continue;
					}
				}
			}
			catch { }
			finally
			{
				reader.Close();
			}
		}

	}
}