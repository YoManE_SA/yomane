﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using Netpay.Dal.DataAccess;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System.Threading;
using Netpay.Bll.Accounts;

namespace Netpay.Bll.BankHandler
{
    public class Iso8583BankHandlerBase : BankHandlerBase
    {
        public Iso8583BankHandlerBase(DebitCompany debitCompanyType, string generatedFileMiddleName)
        {
            DebitCompanyType = debitCompanyType;
            GeneratedFileMiddleName = generatedFileMiddleName;
        }

        public const string SecuredObjectName = "Iso8583BankHandlerBase";

        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Bll.BankHandler.Module.Current, SecuredObjectName); } }

        public DebitCompany DebitCompanyType { get; private set; }

        public string GeneratedFileMiddleName { get; private set; }

        public override DebitCompany DebitCompanyID { get { return DebitCompanyType; } }

        protected class BatchStat
        {
            public int CountCredit;
            public decimal TotalCredit;
            public int CountDebit;
            public decimal TotalDebit;
            public decimal TotalAmount { get { return TotalDebit + TotalCredit; } }
            public int TotalCount { get { return CountCredit + CountDebit; } }
            public static BatchStat operator +(BatchStat v1, BatchStat v2)
            {
                return new BatchStat()
                {
                    CountCredit = v1.CountCredit + v2.CountCredit,
                    TotalCredit = v1.TotalCredit + v2.TotalCredit,
                    CountDebit = v1.CountDebit + v2.CountDebit,
                    TotalDebit = v1.TotalDebit + v2.TotalDebit,
                };
            }
        }

        public override void HandlePending(BankHandlerContext context)
        {
            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
            ////Logger.Log(LogTag.AuthBatch, "Inside HandlePending() method, debit company type:" + DebitCompanyType.ToString());

            try
            {
                var bank = LoadEpaLogin(Accounts.ExternalServiceLogin.ServiceTypes.Auth, Accounts.ExternalServiceLogin.Protocols.SFTP);
                if (!bank.IsActive)
                {
                    Logger.Log(LogTag.AuthBatch, "loaded epa login bank, the bank is not active, return, debit company is:" + DebitCompanyID.ToString());
                    return;
                }

                string scriptFileName = Manager.MapTempFile(DebitCompanyType.ToString() + "AuthFtpScript.ftp");
                string logFile = Manager.MapTempFile(DebitCompanyType.ToString() + "AuthFtpLogFile" + DateTime.Now.ToString("_yyyyMMddhhmmss") + ".log");

                var tempFileName = context.CreateDecryptedTempFile(Manager, true);
                if (DebitCompanyType == DebitCompany.eCentric)
                    SendFileToEcentric(bank, tempFileName);
                else if (DebitCompanyType == DebitCompany.ACS)
                    SendFileToCashbuild(bank, tempFileName);

                //using (System.IO.StreamWriter sw = new System.IO.StreamWriter(scriptFileName))
                //{
                //    sw.WriteLine(bank.GetProtocolLoginCommand(GetPrivateKey(), GetHostKey()));
                //    sw.WriteLine("option batch continue");
                //    sw.WriteLine("option confirm off");
                //    sw.WriteLine("put {0} In/{1} -delete", tempFileName, System.IO.Path.GetFileName(tempFileName));
                //    sw.WriteLine("exit");
                //    sw.Close();
                //}
                //var ret = bank.ExecuteFTPScript(scriptFileName, logFile);
                //if (System.IO.File.Exists(tempFileName)) System.IO.File.Delete(tempFileName);

                //var batch = Process.Batch.Load((int)DebitCompanyType, context.File.Name);
                //if (ret != 0)
                //{
                //    if (batch != null) batch.SetFailSent("SFTP fail code:" + ret);

                //    Logger.Log(LogSeverity.Error, LogTag.AuthBatch, DebitCompanyType.ToString() + "SFTP failed with code = " + ret);
                //    throw new ApplicationException(DebitCompanyType.ToString() + "SFTP failed with code = " + ret);
                //}
                //if (batch != null) batch.SetSent();
            }
            catch (Exception ex)
            {
                Logger.Log(LogTag.AuthBatch, ex, "Error on HandlePending for:" + DebitCompanyID.ToString());
                throw ex;
            }
        }

        private void SendFileToEcentric(ExternalServiceLogin bank, string sw2FilePath)
        {
            const int maxAttempts = 6; //3 by SFTP, 3 by email then finally 1 SMS attempt.

            for (var i = 0; i < maxAttempts; i++)
            {
                var result = i < 3 ? SendFileViaSftp(bank, sw2FilePath) : SendFileViaEmail(bank, sw2FilePath);
                Logger.Log(result.Key ? LogSeverity.Info : LogSeverity.Error, LogTag.Process, "SW2 File Upload: " + result.Value.ToString());
                if (result.Key)
                    break;
                else
                {
                    var retryTimer = Convert.ToInt32(Infrastructure.Application.GetParameter("resendTimeOut"));
                    Thread.Sleep(retryTimer); //wait 30 seconds bfore retrying
                }
                if (i + 1 == maxAttempts)
                {
                    var sendSupportSms = Convert.ToBoolean(Infrastructure.Application.GetParameter("sendSuppportSmsNotification"));
                    //notify team via SMS?

                    if (sendSupportSms)
                    {
                        var smsPhoneNumber = GetPhoneNumbers(Infrastructure.Application.GetParameter("smsSupportContact"));
                        var dt = DateTime.Now;
                        string s = dt.ToLocalTime().ToString("MMM dd, yyyy HH:mm:ss tt \"GMT\"zzz");
                        foreach (var number in smsPhoneNumber)
                            Netpay.Bll.SMS.SMSService.SendSms(number, string.Format("Last attempt to email or FTP '{0}' to eCentric was at {1}. Please investigate further.", Path.GetFileName(sw2FilePath), s));
                        Logger.Log(LogSeverity.Info, LogTag.Process, "SW2 File Upload: SMS notification sent because SFTP and email failed.");

                    }
                }
            }
        }

        private void SendFileToCashbuild(ExternalServiceLogin bank, string sw2FilePath)
        {
            var result = SendFileViaEmail(bank, sw2FilePath);
            Logger.Log(result.Key ? LogSeverity.Info : LogSeverity.Error, LogTag.Process, "SW2 File Upload: " + result.Value.ToString());
        }

        private IList<string> GetPhoneNumbers(string list)
        {
            IList<string> numbers = new List<string>();
            if (!string.IsNullOrEmpty(list))
                if (list.Contains(";"))
                {
                    numbers = list.Split(new char[] { ';' }).ToList();
                }
                else
                {
                    numbers.Add(list);
                }

            return numbers;
        }

        private KeyValuePair<bool, StringBuilder> SendFileViaEmail(ExternalServiceLogin bank, string sw2FilePath)
        {
            var dic = new Dictionary<bool, StringBuilder>();
            try
            {
                var username = Infrastructure.Application.GetParameter("smtpUserName");
                var password = Infrastructure.Application.GetParameter("smtpPassword");
                var xem = new ExchangeEmailManager(username, password);
                var body = "Good day,  Please confirm receipt of the attached SW2 file.";
                var attachments = new List<string> { sw2FilePath };
                List<string> toEmails = new List<string>();

                if (DebitCompanyType == DebitCompany.eCentric)
                    toEmails = new List<string> { Domain.Current.Sw2RecipientEmail };
                else if (DebitCompanyType == DebitCompany.ACS)
                    toEmails = new List<string> { Domain.Current.Sw2RecipientEmailCashbuild };

                xem.SendEmail(username, "SW2 File", body, attachments, toEmails);
                var response = "SW2 file successfully submitted" + Path.GetFileName(sw2FilePath);
                var sb = new StringBuilder();
                sb.AppendLine(response);
                dic.Add(true, sb);
            }
            catch (Exception ex)
            {
                var exceptionMessage = GetInnerException(ex);
                dic.Add(false, new StringBuilder("failed to send email -  " + exceptionMessage));
            }
            return dic.FirstOrDefault();
        }

        private static string GetInnerException(Exception ex)
        {
            var msg = ex.Message;
            var hasInnerException = ex.InnerException != null;
            if (hasInnerException)
            {
                var tmp = ex.InnerException;
                while (tmp != null)
                {
                    msg = tmp.Message;
                    tmp = tmp.InnerException;
                }
            }
            return msg;

        }
        private KeyValuePair<bool, StringBuilder> SendFileViaSftp(ExternalServiceLogin bank, string sw2FilePath)
        {
            var creds = bank.GetSftpCredentials();
            string username, password, hostIp = string.Empty;

            hostIp = creds.FirstOrDefault(x => x.Key.ToLower().Equals("url")).Value;
            username = creds.FirstOrDefault(x => x.Key.ToLower().Equals("username")).Value;
            password = creds.FirstOrDefault(x => x.Key.ToLower().Equals("password")).Value;

            var ftpCred = new FtpCredentials
            {
                HostIp = hostIp,
                Password = password,
                Username = username,
                PortNumber = 22,
                SshHostKeyFingerprint = GetHostKey(),
                SshPrivateKeyPath = GetPrivateKey()
            };
            return FtpManager.UploadFileToSftpServer(sw2FilePath, "/", ftpCred);
        }

        public override void GenerateFile()
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            var totalStat = new BatchStat();
            //var terminalNumbers = Process.Batch.GetTerminalNumbers(new List<int>() { (int)Bll.ThirdParty.eCentric.DEBITCOMPANY_ID });
            //if (terminalNumbers.Count == 0) return;
            var AuthRow = new Process.Batch((int)DebitCompanyID, null);
            AuthRow.SetInternalID(null);
            AuthRow.MarkTransationsForBatch();
            var trans = AuthRow.GetTransactions(true);
            //remove same day refunds
            var allRefunds = trans.Where(v => v.CreditType == CreditType.Refund).ToList();
            foreach (var v in allRefunds)
            {
                var orTrans = trans.Where(x => ((x.PaymentData != null && v.PaymentData != null) && x.PaymentData.EqualsTo(v.PaymentData)) && x.Amount == v.Amount && (x.CreditType == CreditType.Regular || x.CreditType == CreditType.Installments)).ToList();
                if (orTrans.Count > 0)
                {
                    trans.Remove(v);
                    foreach (var r in orTrans) trans.Remove(r);
                }
            }

            string fileName = GetFilePath(EpaFolder.New, GeneratedFileMiddleName + DateTime.Now.ToString("yyyyMMdd") + ".sw2"); // + AuthRow.InternalKey.ToString("0000000") +  "_" 
            fileName = Domain.GetUniqueFileName(fileName);
            using (var fileWriter = new System.IO.StreamWriter(fileName))
            {
                WriteFileHeader(fileWriter, AuthRow);
                foreach (var t in trans) WriteTransaction(fileWriter, totalStat, t);
                WriteFileTrailer(fileWriter, totalStat);
                fileWriter.Close();
            }
            AuthRow.SetPrepared(System.IO.Path.GetFileName(fileName), totalStat.TotalCount, totalStat.TotalDebit, totalStat.TotalCredit);
        }

        private void WriteFileHeader(System.IO.StreamWriter fileWriter, Process.Batch batch)
        {
            switch (DebitCompanyID)
            {
                case DebitCompany.eCentric:
                    fileWriter.Write("Header".PadRight(8)); //Record type
                    fileWriter.Write(GeneratedFileMiddleName.PadRight(10)); //Source Name
                    fileWriter.Write("MARKOFF FILE".PadRight(20)); //File Description
                    fileWriter.Write(batch.BatchDate.ToString("yyyyMMdd")); //File Date
                    fileWriter.Write(new string(' ', 143)); //Filler
                    fileWriter.Write("\r\n");
                    break;
                    // No headers for ACS
                    //case DebitCompany.ACS:
                    //    fileWriter.Write("Document Date");
                    //    fileWriter.Write("\t"); // Tab
                    //    fileWriter.Write("Posting Date");
                    //    fileWriter.Write("\t"); // Tab
                    //    fileWriter.Write("Document Type");
                    //    fileWriter.Write("\t"); // Tab
                    //    fileWriter.Write("Company Code");
                    //    fileWriter.Write("\t"); // Tab
                    //    fileWriter.Write("Currency");
                    //    fileWriter.Write("\t"); // Tab
                    //    fileWriter.Write("Reference");
                    //    fileWriter.Write("\t"); // Tab
                    //    fileWriter.Write("Document Header Text");
                    //    fileWriter.Write("\t"); // Tab
                    //    fileWriter.Write("Posting Key1");
                    //    fileWriter.Write("\t"); // Tab
                    //    fileWriter.Write("Account1");
                    //    fileWriter.Write("\t"); // Tab
                    //    fileWriter.Write("Profit Center1");
                    //    fileWriter.Write("\t"); // Tab
                    //    fileWriter.Write("Posting Key2");
                    //    fileWriter.Write("\t"); // Tab
                    //    fileWriter.Write("Account2");
                    //    fileWriter.Write("\t"); // Tab
                    //    fileWriter.Write("Profit Center2");
                    //    fileWriter.Write("\t"); // Tab
                    //    fileWriter.Write("Amount");
                    //    fileWriter.Write("\r\n"); // Carriage Return + New Line
                    //    break;
            }
        }

        private void WriteTransaction(System.IO.StreamWriter fileWriter, BatchStat batch, Transactions.Transaction trans)
        {
            switch (DebitCompanyID)
            {
                case DebitCompany.eCentric:
                    var authData = new ThirdParty.eCentric.Iso8583PostXml(trans.BatchData);
                    int cashBackAmount = authData.GetField("Field_054").ToNullableInt().GetValueOrDefault();
                    string field3 = authData.GetField("Field_003").EmptyIfNull().PadRight(20);
                    string terminalNumber = authData.GetField("Field_041");
                    var isCredit = (trans.CreditType == Infrastructure.CreditType.Credit) || (trans.CreditType == Infrastructure.CreditType.Refund);
                    if (isCredit) return; //Shoprite does not manage late reversals, see email from Adiel Hollenbach, Juan on 14 May 2015 12:29 PM
                    if (isCredit)
                    {
                        batch.CountCredit++;
                        batch.TotalCredit += trans.Amount;
                    }
                    else
                    {
                        batch.CountDebit++;
                        batch.TotalDebit += trans.Amount;
                    }
                    fileWriter.Write("Detail".PadRight(8)); //Record type
                    fileWriter.Write("00001"); //Chain Number
                    fileWriter.Write(authData.GetField("Field_042").Substring(0, 8).PadLeft(8, '0')); //Merchant Number ???
                    fileWriter.Write(authData.GetField("Field_042").PadLeft(15, '0')); //Branch Identifier
                    fileWriter.Write(trans.InsertDate.ToString("yyyyMMdd")); //Transaction date
                    fileWriter.Write(trans.MerchantPayDate.GetValueOrDefault().ToString("yyyyMMdd")); //Recon Date
                    fileWriter.Write(authData.GetField("Field_007").TruncStart(6)); //Transaction time
                    fileWriter.Write(authData.GetField("Field_037").PadRight(12)); //Retrieval reference number

                    fileWriter.Write("DENO01".PadRight(6)); //Card Type ??
                    fileWriter.Write(GeneratedFileMiddleName.PadRight(10)); //Card Category
                    fileWriter.Write(authData.GetField("Field_002").PadRight(23, ' ')); //Card Number
                    fileWriter.Write(((int)(trans.Amount * 100)).ToString().PadLeft(12, '0')); //Transaction Amount

                    fileWriter.Write(((int)(trans.Amount * 100) - cashBackAmount).ToString().PadLeft(12, '0')); //Purchase Amount 
                    fileWriter.Write(cashBackAmount.ToString().PadLeft(12, '0')); //Cashback Amount
                    fileWriter.Write(authData.MessageType); //Message Type

                    fileWriter.Write(field3.Substring(0, 2).PadLeft(5, '0')); //Transaction Type (ISO 8583 Field 3, position 1-2) 
                    fileWriter.Write(authData.GetField("Field_127_004").Substring(15, 7).TruncStart(4)); //Cashier Number (ISO 8583 Field 127.4, position 15-22)

                    fileWriter.Write(terminalNumber.Substring(terminalNumber.Length - 2).PadLeft(3, '0')); //Till Number (ISO 8583 Field 41, last 2 digits)
                    fileWriter.Write(terminalNumber.PadLeft(8, '0')); //Terminal Id (ISO 8583 Field 41)
                    fileWriter.Write(trans.ApprovalNumber.PadLeft(6, ' ')); //Authorization Code

                    fileWriter.Write(authData.GetField("Field_022").EmptyIfNull().PadLeft(3, '0')); //POS Code(ISO 8483 Field 22)
                    fileWriter.Write(authData.GetField("Field_011").EmptyIfNull().PadLeft(6, '0')); //Transaction Sequence (ISO 8583 Field 11)
                    fileWriter.Write(field3.Substring(2, 2)); //Account type from (ISO 8583 Field 3, position 3-4)
                    fileWriter.Write(field3.Substring(4, 2)); //Account type to(ISO 8583 Field 3, position 5-6)
                    fileWriter.Write("N");
                    fileWriter.Write("\r\n");
                    break;
                case DebitCompany.ACS:
                    var authDataACS = new ThirdParty.ACS.Iso8583PostXml(trans.BatchData);
                    int cashBackAmountACS = authDataACS.GetField("Field_054").ToNullableInt().GetValueOrDefault();
                    string field3ACS = authDataACS.GetField("Field_003").EmptyIfNull().PadRight(20);
                    string terminalNumberACS = authDataACS.GetField("Field_041");

                    fileWriter.Write(trans.InsertDate.ToString("dd.MM.yyyy")); // transaction date
                    fileWriter.Write("\t"); // Tab
                    fileWriter.Write(/*trans.MerchantPayDate.GetValueOrDefault()*/DateTime.Now.ToString("dd.MM.yyyy")); // date posted into SAP
                    fileWriter.Write("\t"); // Tab
                    fileWriter.Write("SA"); // Document Type
                    fileWriter.Write("\t"); // Tab
                    fileWriter.Write("CC01"); // Company Code");
                    fileWriter.Write("\t"); // Tab
                    fileWriter.Write(trans.CurrencyIsoCode.ToUpper());// "Currency");
                    fileWriter.Write("\t"); // Tab
                    fileWriter.Write(authDataACS.GetField("Field_037")); // any reference/text
                    fileWriter.Write("\t"); // Tab
                    fileWriter.Write("Yomane"); // any reference/text
                    fileWriter.Write("\t"); // Tab
                    fileWriter.Write("40"); // Posting Key1");
                    fileWriter.Write("\t"); // Tab
                    fileWriter.Write("118058"); // "Account1");
                    fileWriter.Write("\t"); // Tab
                    fileWriter.Write("SH05"); // Profit Center1");
                    fileWriter.Write("\t"); // Tab
                    fileWriter.Write("50"); // Posting Key2");
                    fileWriter.Write("\t"); // Tab
                    fileWriter.Write("118058"); // Account2");
                    fileWriter.Write("\t"); // Tab
                    fileWriter.Write(terminalNumberACS); // profit center linked to the merchant number e.g. SH01
                    fileWriter.Write("\t"); // Tab
                    fileWriter.Write(trans.Amount.ToString("0.00")); // "Amount");
                    fileWriter.Write("\r\n"); // Carriage Return + New Line
                    break;
            }
        }

        private void WriteFileTrailer(System.IO.StreamWriter fileWriter, BatchStat stat)
        {
            switch (DebitCompanyID)
            {
                case DebitCompany.eCentric:
                    fileWriter.Write("Trailer".PadRight(8)); //Record type
                    fileWriter.Write(DateTime.Now.ToString("yyyyMMdd")); //File Date
                    fileWriter.Write(((int)(stat.TotalAmount * 100)).ToString().PadLeft(16, '0')); //Sum Total
                    fileWriter.Write((stat.TotalCount + 2).ToString().PadLeft(7, '0')); //Trailer Record Count
                    fileWriter.Write("\r\n");
                    break;
            }
        }
    }
}
