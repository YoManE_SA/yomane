﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;

using Netpay.Infrastructure.Email.Pop3;
using Netpay.Infrastructure.Email.Pop3.SharpMimeTools;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.BankHandler
{
	public class AlliedEpaBankHandler : BankHandlerBase
	{
		private const int _recordBuffer = 100;
        public override DebitCompany DebitCompanyID { get { return Infrastructure.DebitCompany.Allied; } }

        public override void HandlePending(BankHandlerContext context)
		{
			List<tblEpaPending> rows = new List<tblEpaPending>();
            StreamReader reader = context.DecryptedStreamReader;
			try
			{
				var colNames = new System.Collections.Generic.List<string>(reader.ReadLine().Split(','));
				for (string currentLine = reader.ReadLine(); currentLine != null; currentLine = reader.ReadLine())
				{
					try
					{
						/*  
						* 0 Transaction ID,
						* 1 Original Transaction ID, 
						* 2 Time,
						* 3 Merchant,
						* 4 Customer,
						* 5 Email,
						* 6 State,
						* 7 Amount,
						* 8 Merchant Reference,
						* 9 Currency,
						*10 Card Type,
						*11 Phone,
						*12 Address,
						*13 Address2,
						*14 AddressCity,
						*15 AddressState,
						*16 AddressCountry,
						*17 AddressZipCode,
						*18 Status,
						*19 Site,
						*20 BIN,
						*21 Last4CC,
						*22 IPAddress,
						*23 Status Message
						*/
						DateTime transDate;
						string[] currentLineSplit = currentLine.Split(',');
						if (currentLineSplit[18].Trim() == "Successful")
						{
							tblEpaPending entity = new tblEpaPending();
							entity.DebitRefCode = currentLineSplit[8].Trim();
							entity.Amount = currentLineSplit[7].ToNullableDecimal();
							//entity.ApprovalNumber = currentLineSplit[14].Trim();
							//entity.CardNumber256 = Netpay.Infrastructure.Security.Encryption.Encrypt(Context.DomainHost, currentLineSplit[3]);
							entity.Currency = Currency.Get(currentLineSplit[9]).ID;
							entity.InsertDate = DateTime.Now;
                            entity.StoredFileName = context.File.Name;
							entity.Installment = 1;
							entity.TerminalNumber = null;
							entity.TransCount = null;
							entity.TransID = null;
							entity.PayoutDate = DateTime.Now;
							entity.IsRefund = currentLineSplit[6].Trim() == "Refund";
							if (DateTime.TryParseExact(currentLineSplit[2], "M/d/yyyy h:m:s tt", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out transDate))
								entity.TransDate = transDate;
							rows.Add(entity);
						}
					}
					catch (Exception ex)
					{
                        context.LogRowError(ex, currentLine);
						continue;
					}
				}
			}
			catch { }
			finally
			{
				reader.Close();
				InsertParsed(rows);
			}
		}

	}
}
