﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;

using Netpay.Infrastructure.Email.Pop3;
using Netpay.Infrastructure.Email.Pop3.SharpMimeTools;
using Netpay.Infrastructure.Security;
using Netpay.CommonTypes;

namespace Netpay.Bll.BankHandler
{
	public class CredoRaxChbBankHandler : BankHandlerBase
	{
        public override DebitCompany DebitCompanyID { get { return Infrastructure.DebitCompany.CredoRax; } }

        private const int _recordBuffer = 100;

		public override void FetchFile() //sftp
		{
            var bank = LoadEpaLogin(Accounts.ExternalServiceLogin.ServiceTypes.CHB, Accounts.ExternalServiceLogin.Protocols.SFTP);
            if (!bank.IsActive) return;

            string scriptFileName = Manager.MapTempFile("CredoRaxChbFtpScript.ftp");
            string logFile = Manager.MapTempFile("CredoRaxChbFtpLogFile.log");
            //create script file
            System.IO.StreamWriter sw = new System.IO.StreamWriter(scriptFileName);
            sw.WriteLine(bank.GetProtocolLoginCommand(GetPrivateKey(), GetHostKey()));
            sw.WriteLine("option batch continue");
            sw.WriteLine("option confirm off");
            sw.WriteLine("lcd " + GetPath(EpaFolder.New));
            sw.WriteLine("get -delete /netpay/outgoing/CS_*.*");
            sw.WriteLine("get -delete /netpay/outgoing/CA_*.*");
            sw.WriteLine("exit");
            sw.Close();        

            var ret = bank.ExecuteFTPScript(scriptFileName, logFile);
            if (ret != 0) throw new ApplicationException("Credorax SFTP failed with code = " + ret);
        }

        public override void HandleNew(BankHandlerContext context)
        {
            var archPath = GetFilePath(EpaFolder.Archive, context.File.Name);
            if (System.IO.File.Exists(archPath)) throw new DuplicateFileException();
            base.HandleNew(context);
        }

        public override void HandlePending(BankHandlerContext context)
		{
            //if (Context.File.Name.StartsWith("RR_")) return parseRRFiles(manager);
            if (context.File.Name.StartsWith("CS_")) parseCSFiles(context); //v4
            else if (context.File.Name.StartsWith("CA_")) parseCAFiles(context); //v5
            else throw new Exception("Unsupported file type:" + context.File.FullName);
        }

        private void parseRRFiles(BankHandlerContext context)
        {
            List<tblChbPending> rows = new List<tblChbPending>();
            StreamReader reader = context.DecryptedStreamReader;
            try
            {
                string[] headerLine = reader.ReadLine().Split(',');
                if (headerLine.Length != 26)
                    throw new Exception(string.Format("Failed to parse record in CredoRax CHB file \n\rFile: {0}\n\rLine 0\r\nInvalid file format", context.File.Name));
                for (string currentLine = reader.ReadLine(); currentLine != null; currentLine = reader.ReadLine())
                {
                    try
                    {
                        /*
                        0: Merchant No.
                        1: Client No.
                        2: Item Type
                        3: RR Date
                        4: Reason Code
                        5: Reason Description
                        6: ARN
                        7: Issuer No.
                        8: Item Slip Number
                        9: Card Scheme
                        10:Fulfilment Date
                        11:(Fulfilment) Description
                        12:Orig Post Date
                        13:Orig Tran Date
                        14:Orig Tran Curr
                        15:Orig Tran Amt
                        16:Orig Merch Sett Curr
                        17:Merch Sett Amt
                        18:Netwk Sett Curr
                        19:Netwk Sett Amt
                        20:Card No.
                        21:Orig Type
                        22:Orig Slip Number
                        23:Auth Code
                        24:Orig Batch Number
                        25:RAN
                        */
                        string[] currentLineSplit = extractCsvLine(currentLine);
                        if (currentLineSplit[0] == "====END OF FILE====") break;
                        tblChbPending entity = new tblChbPending();
                        entity.DebitCompany_id = (int)DebitCompanyID;
                        entity.ReasonCode = currentLineSplit[4].Trim();
                        if (string.IsNullOrWhiteSpace(entity.ReasonCode)) entity.ReasonCode = "0";
                        entity.CaseID = currentLineSplit[6].Trim();
                        entity.Amount = currentLineSplit[15].ToNullableDecimal();
                        entity.ApprovalNumber = currentLineSplit[23].Trim();
                        entity.CardNumber256 = Netpay.Infrastructure.Security.Encryption.Encrypt(currentLineSplit[20]);
                        entity.Currency = Currency.Get(currentLineSplit[14]).ID;
                        entity.InsertDate = DateTime.Now;
                        entity.Installment = 1;
                        entity.StoredFileName = context.File.Name;
                        entity.TerminalNumber = null;
                        entity.TransCount = null;
                        entity.TransID = null;
                        entity.DebitRefCode = currentLineSplit[25].Trim();
                        entity.IsChargeback = false;
                        entity.IsPhotocopy = !entity.IsChargeback;
                        DateTime parsedDate;
                        if (DateTime.TryParseExact(currentLineSplit[13], "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out parsedDate))
                            entity.TransDate = parsedDate;
                        if (DateTime.TryParseExact(currentLineSplit[12], "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out parsedDate))
                            entity.ChargebackDate = parsedDate;
                        if (DateTime.TryParseExact(currentLineSplit[10], "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out parsedDate))
                            entity.DeadlineDate = parsedDate;
                        rows.Add(entity);
                    }
                    catch (Exception ex)
                    {
                        context.LogRowError(ex, currentLine);
                        continue;
                    }
                }
            } finally {
                reader.Close();
                InsertParsed(rows);
            }
        }

        private static string[] extractCsvLine(string strLine)
		{
			var retArray = new List<string>(); 
			int curDel, startIndex = 0;
			do{
				curDel = strLine.IndexOf(',', startIndex);
				if(curDel == -1) curDel = strLine.Length;
				var fieldValue = strLine.Substring(startIndex, curDel - startIndex);
				if (fieldValue.StartsWith("\"")){
					curDel = strLine.IndexOf('\"', startIndex + 1);
					if (curDel == -1) curDel = strLine.Length;
					fieldValue = strLine.Substring(startIndex + 1, curDel - startIndex - 1);
					curDel++;
				}
				retArray.Add(fieldValue);
				startIndex = curDel + 1;
			} while(curDel < strLine.Length);
			return retArray.ToArray();
		}

        private void parseCSFiles(BankHandlerContext context)
		{
			List<tblChbPending> rows = new List<tblChbPending>();
			StreamReader reader = context.DecryptedStreamReader;
			try
			{
				string[] headerLine = extractCsvLine(reader.ReadLine());
                if (headerLine.Length < 32)
                {
                    reader.ReadToEnd();
                    throw new Exception(string.Format("Failed to parse record in CredoRax CHB file \n\rFile: {0}\n\rLine 0\r\nInvalid file format", context.File.Name));
                }
                for (string currentLine = reader.ReadLine(); currentLine != null; currentLine = reader.ReadLine())
				{
					try
					{
						/*CHB New Format
						0:   Merchant No.
						1:   Client No.
						2:   Posting Date
						3:   Item Type
						4:   Card No.
						5:   ARN
						6:   Reason Code
						7:   Reason Description
						8:   Cur
						9:   Amount
						10:  CCN
						11:  Orig Post Date
						12:  Orig Tran Date
						13:  Orig Type
						14:  Orig Tran Curr
						15:  Orig Tran Amt
						16:  Orig Merch Sett Curr
						17:  Merch Sett Amt
						18:  Netwk Sett Curr
						19:  Netwk Sett Amt
						20:  Original Slip
						21:  Item Slip
						22:  Auth Code
						23:  Batch No.
						24:  Merchant DBA Name
						25:  Merch Tran Ref.
						26:  Capture Method,
						27:	 Due Date
						28:	 Central Processing Date
						29:	 Work by Date
						30:	 Chargeback Amount
						31:	 Chargeback Currency
						 */
						string[] currentLineSplit = extractCsvLine(currentLine);
                        if (currentLineSplit[0] == "====END OF FILE====") 
                            break;
						tblChbPending entity = new tblChbPending();
                        entity.DebitCompany_id = (int)DebitCompanyID;
                        entity.ReasonCode = currentLineSplit[Array.IndexOf(headerLine, "Reason Code")].Trim();
						if (string.IsNullOrWhiteSpace(entity.ReasonCode)) entity.ReasonCode = "0";
						entity.CaseID = currentLineSplit[Array.IndexOf(headerLine, "ARN")].Trim();
                        //entity.AcquirerReferenceNum = currentLineSplit[Array.IndexOf(headerLine, "ARN")]).Trim();
                        entity.Amount = currentLineSplit[Array.IndexOf(headerLine, "Amount")].ToNullableDecimal();
						entity.ApprovalNumber = currentLineSplit[Array.IndexOf(headerLine, "Auth Code")].Trim();
						entity.CardNumber256 = Netpay.Infrastructure.Security.Encryption.Encrypt(currentLineSplit[Array.IndexOf(headerLine, "Card No.")]);
						entity.Currency = Currency.Get(currentLineSplit[Array.IndexOf(headerLine, "Cur")]).ID;
						entity.InsertDate = DateTime.Now;
						entity.Installment = 1;
						entity.StoredFileName = context.File.Name;
						entity.TerminalNumber = null;
						entity.TransCount = null;
                        entity.TransID = null;
                        entity.DebitRefCode = currentLineSplit[Array.IndexOf(headerLine, "Merch Tran Ref.")].Trim();
                        entity.IsChargeback = true;
                        entity.IsPhotocopy = !entity.IsChargeback;
                        DateTime parsedDate;
                        if (DateTime.TryParseExact(currentLineSplit[Array.IndexOf(headerLine, "Orig Tran Date")], "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out parsedDate))
                            entity.TransDate = parsedDate;
                        if (DateTime.TryParseExact(currentLineSplit[Array.IndexOf(headerLine, "Posting Date")], "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out parsedDate))
							entity.ChargebackDate = parsedDate;
                        rows.Add(entity);
					}
					catch (Exception ex)
					{
                        context.LogRowError(ex, currentLine);
						continue;
					}
				}
			} finally {
				reader.Close();
				InsertParsed(rows);
			}
		}
        private void parseCAFiles(BankHandlerContext context)
        {
            List<tblChbPending> rows = new List<tblChbPending>();
            StreamReader reader = context.DecryptedStreamReader;
            try
            {
                string[] headerLine = extractCsvLine(reader.ReadLine());
                if (headerLine.Length < 19)
                {
                    reader.ReadToEnd();
                    throw new Exception(string.Format("Failed to parse record in CredoRax CHB file \n\rFile: {0}\n\rLine 0\r\nInvalid file format", context.File.Name));
                }
                for (string currentLine = reader.ReadLine(); currentLine != null; currentLine = reader.ReadLine())
                {
                    try
                    {
                        /*
                         0: merchant_name,
                         1: mid,
                         2: transaction_type,
                         3: posting_date,
                         4: orig_transaction_date,
                         5: orig_transaction_currency,
                         6: orig_transaction_amount,
                         7: cbk_currency,
                         8: cbk_amount,
                         9: orig_request_id,
                         10: orig_searchable_code,
                         11: submerchant_id,
                         12: rrn,
                         13: arn,
                         14: card_number,
                         15: reason_code,
                         16: reason_desc,
                         17: work_by_date,
                         18: partial_ind
                         */

                        string[] currentLineSplit = extractCsvLine(currentLine);
                        if (currentLineSplit[0] == "====END OF FILE====")
                            break;
                        tblChbPending entity = new tblChbPending();
                        entity.DebitCompany_id = (int)DebitCompanyID;
                        entity.ReasonCode = currentLineSplit[Array.IndexOf(headerLine, "reason_code")].Trim();
                        if (string.IsNullOrWhiteSpace(entity.ReasonCode)) entity.ReasonCode = "0";
                        entity.CaseID = currentLineSplit[Array.IndexOf(headerLine, "arn")].Trim();
                        //entity.AcquirerReferenceNum = currentLineSplit[Array.IndexOf(headerLine, "ARN")]).Trim();
                        entity.Amount = currentLineSplit[Array.IndexOf(headerLine, "orig_transaction_amount")].ToNullableDecimal();
                        //entity.ApprovalNumber = currentLineSplit[Array.IndexOf(headerLine, "orig_searchable_code")].Trim();
                        entity.CardNumber256 = Netpay.Infrastructure.Security.Encryption.Encrypt(currentLineSplit[Array.IndexOf(headerLine, "card_number")]);
                        entity.Currency = Currency.Get(currentLineSplit[Array.IndexOf(headerLine, "orig_transaction_currency")]).ID;
                        entity.InsertDate = DateTime.Now;
                        entity.Installment = 1;
                        entity.StoredFileName = context.File.Name;
                        entity.TerminalNumber = null;
                        entity.TransCount = null;
                        entity.TransID = null;
                        entity.DebitReferenceNum = currentLineSplit[Array.IndexOf(headerLine, "orig_request_id")].Trim();
                        entity.IsChargeback = true;
                        entity.IsPhotocopy = !entity.IsChargeback;
                        DateTime parsedDate;
                        if (DateTime.TryParseExact(currentLineSplit[Array.IndexOf(headerLine, "orig_transaction_date")], "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out parsedDate))
                            entity.TransDate = parsedDate;
                        if (DateTime.TryParseExact(currentLineSplit[Array.IndexOf(headerLine, "posting_date")], "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out parsedDate))
                            entity.ChargebackDate = parsedDate;
                        //entity.DeadlineDate = currentLineSplit[Array.IndexOf(headerLine, "work_by_date")
                        rows.Add(entity);
                    }
                    catch (Exception ex)
                    {
                        context.LogRowError(ex, currentLine);
                        continue;
                    }
                }
            }
            finally
            {
                reader.Close();
                InsertParsed(rows);
            }
        }
    }
}