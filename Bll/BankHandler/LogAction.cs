﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.BankHandler
{
	public enum LogAction
	{
		Error = 0,
		NewFileProcessStart = 1,
		NewFileProcessCompleted = 2,
		PendingFileProcessStart = 3,
		PendingFileProcessComplete = 4,
		Uploaded = 5,
		Sent = 6
	}
}
