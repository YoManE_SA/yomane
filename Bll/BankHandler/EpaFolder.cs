﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.BankHandler
{
	public enum EpaFolder
	{
		New,
		Pending,
		Archive,
		Error, 
		Temp
	}
}
