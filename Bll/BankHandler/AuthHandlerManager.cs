﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Netpay.Dal.DataAccess;
using Netpay.Crypt;
using System.Threading;
using Netpay.Infrastructure;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.BankHandler
{
    public class AuthHandlerManager : BankHandlerManagerBase
    {
        public const string SecuredObjectName = "AuthManage";

        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(BankHandler.Module.Current, SecuredObjectName); } }

        private System.Threading.Timer _generateTimer = null;

        private static AuthHandlerManager _current = null;
        public static AuthHandlerManager Current
        {
            get
            {
                if (_current == null) _current = new AuthHandlerManager(); return _current;
            }
        }

        public static string[] AuthHandlers = new string[] { "ShvaAuthHandler", "FNBAuthHandler", "eCentricAuthHandler" };

        public static string[] ShvaAuthHandlerDebitCompanies = new string[] { "ShvaCal", "ShvaMobile", "ShvaIsracard" };

        private AuthHandlerManager() : base(LogTag.AuthBatch, @"Auth", new string[] { Bll.Accounts.ExternalServiceLogin.ServiceTypes.Auth, Bll.Accounts.ExternalServiceLogin.ServiceTypes.Deposit })
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            //typeof(new CatellaAuthHandler());
            AddHandler(new ShvaAuthHandler(DebitCompany.ShvaCal));
            AddHandler(new ShvaAuthHandler(DebitCompany.ShvaMobile));
            AddHandler(new ShvaAuthHandler(DebitCompany.ShvaIsracard));
            AddHandler(new FNBAuthHandler());
            AddHandler(new eCentricAuthHandler());
            AddHandler(new ACSAuthHandler());
        }


        public static void ExecGenerate(Type handlerType = null) { Current.ProcessGenerateAllDomains(handlerType); }


        //An overload that receives a string parameter , used from scheduled tasks.
        public static void ExecGenerate(string handlerType)
        {
            if (string.IsNullOrEmpty(handlerType)) return;
            if (!AuthHandlers.Contains(handlerType)) return;

            switch (handlerType)
            {
                case "eCentricAuthHandler":
                    Current.ProcessGenerateAllDomains(typeof(eCentricAuthHandler));
                    break;
                case "ACSAuthHandler":
                    Current.ProcessGenerateAllDomains(typeof(ACSAuthHandler));
                    break;
                case "FNBAuthHandler":
                    Current.ProcessGenerateAllDomains(typeof(FNBAuthHandler));
                    break;
                case "ShvaAuthHandler":
                    Current.ProcessGenerateAllDomains(typeof(ShvaAuthHandler));
                    break;
            }
        }

        public static void ExecGenerate(string handlerType, string debitCompany)
        {
            if (handlerType != "ShvaAuthHandler")
            {
                Logger.Log(LogTag.AuthBatch, new Exception("error handler name was declared on scheduled task:" + handlerType), null, false);
                return;
            }
            if (!ShvaAuthHandlerDebitCompanies.Contains(debitCompany))
            {
                Logger.Log(LogTag.AuthBatch, new Exception("error debit company name was declared on scheduled task for ShvaAuthHandler:" + debitCompany), null, false);
                return;
            }

            switch (debitCompany)
            {
                case "ShvaCal":
                    Current.ProcessGenerateAllDomains((new ShvaAuthHandler(DebitCompany.ShvaCal)).GetType());
                    break;
                case "ShvaMobile":
                    Current.ProcessGenerateAllDomains((new ShvaAuthHandler(DebitCompany.ShvaMobile)).GetType());
                    break;
                case "ShvaIsracard":
                    Current.ProcessGenerateAllDomains((new ShvaAuthHandler(DebitCompany.ShvaIsracard)).GetType());
                    break;
            }
        }

        public override void Log(LogAction action, string originalFileName, string storedFileName)
        {
            NetpayDataContext dc = new NetpayDataContext(Domain.Current.Sql1ConnectionString);
            tblChbFileLog entity = new tblChbFileLog();
            entity.ActionDate = DateTime.Now;
            entity.ActionType = (int)action;
            entity.OriginalFileName = Path.GetFileName(originalFileName);
            entity.StoredFileName = Path.GetFileName(storedFileName);
            dc.tblChbFileLogs.InsertOnSubmit(entity);
            dc.SubmitChanges();
        }


        private void Generate_TimerCallback(object state) { ProcessGenerateAllDomains(); }
        public bool EnableGenerateTimer
        {
            set
            {
                if (value)
                {
                    TimeSpan spanOf24 = new TimeSpan(24, 0, 0);
                    DateTime midnight = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 10, 0);
                    DateTime midnightTomorrow = midnight.AddDays(1);
                    var dueTime = (midnightTomorrow - DateTime.Now).TotalMilliseconds;
                    // 1min timespand for development to test
                    if (Infrastructure.Application.IsProduction)
                    {
                        if (_generateTimer == null)
                            _generateTimer = new System.Threading.Timer(Generate_TimerCallback, null, (int)dueTime, (int)spanOf24.TotalMilliseconds); //every day
                    }
                    else
                    {
                        if (_generateTimer == null)
                            _generateTimer = new Timer(Generate_TimerCallback, null, (60 * 1000), (120 * 1000));
                    }
                }
                else
                {
                    if (_generateTimer != null)
                    {
                        _generateTimer.Dispose();
                        _generateTimer = null;
                    }
                }
            }
        }

        public void ProcessGenerateAllDomains(Type handlerType = null)
        {
            foreach (Domain currentDomain in Domain.Domains.Values)
            {
                if (!currentDomain.EnableEpa) continue;
                Domain.Current = currentDomain;
                try
                {
                    ObjectContext.Current.CredentialsToken = Domain.Current.ServiceCredentials;
                    ProcessGenerate(handlerType);
                }
                catch (Exception ex)
                {
                    Logger.Log(LogTag, ex, "Error AuthGenerate bank files for doamin " + currentDomain.Host);
                }
            }
        }

        public void ProcessGenerate(Type handlerType = null)
        {
            if (!Infrastructure.Tasks.Lock.TryLock(LogTag.ToString() + ".Generate", DateTime.Now.AddMinutes(-15))) return;
            try
            {
                foreach (var handler in Handlers)
                {
                    if (handlerType != null && !handler.GetType().Equals(handlerType)) continue;
                    try
                    {
                        handler.GenerateFile();
                    }
                    catch (Exception ex)
                    {
                        Logger.Log(LogTag, ex, "Error AuthGenerate bank file for doamin " + Domain.Current.Host + ", " + handler.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(LogTag, ex, "Error in AuthGenerate bank file");

            }
            finally
            {
                Infrastructure.Tasks.Lock.Release(LogTag.ToString() + ".Generate");
            }
        }

        public override void Process(Type handlerType = null)
        {
            ProcessGenerate(handlerType);
            base.Process(handlerType);
        }
    }
}
