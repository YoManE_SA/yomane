﻿using System;
using System.IO;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using Netpay.Dal.Netpay;

namespace Netpay.Bll.BankHandler
{
    public class FraudHandlerManager : BankHandlerManagerBase
    {
        private static FraudHandlerManager _current = null;
        public static FraudHandlerManager Current { get { if (_current == null) _current = new FraudHandlerManager(); return _current; } }

        private FraudHandlerManager() : base(LogTag.Fraud, @"Fraud", new string[] { Bll.Accounts.ExternalServiceLogin.ServiceTypes.Fraud }) 
        { 
            AddHandler(new CredoRaxFraudBankHandler());
        }

        public static void ExecFetch() { Current.FetchFilesAllDomains(); }
        public static void ExecMatch() { Current.ProcessMatchAllDomains(); }

        public override void Log(LogAction action, string originalFileName, string storedFileName)
        {
            NetpayDataContext dc = new NetpayDataContext(Domain.Current.Sql1ConnectionString);
            ExternalServiceHistory entity = new ExternalServiceHistory();
            entity.Account_id = (int)Infrastructure.DebitCompany.CredoRax;
            entity.InsertDate = DateTime.Now;
            switch (action) {
                case LogAction.NewFileProcessCompleted: entity.ExternalServiceAction_id = "System.EncryptFile"; break;
                case LogAction.NewFileProcessStart: entity.ExternalServiceAction_id = "System.RecordPull"; break;
                case LogAction.PendingFileProcessComplete: entity.ExternalServiceAction_id = "System.FilePull"; break;
                case LogAction.PendingFileProcessStart: entity.ExternalServiceAction_id = "System.Parse"; break;
                case LogAction.Sent: entity.ExternalServiceAction_id = "System.FilePush"; break;
                case LogAction.Uploaded: entity.ExternalServiceAction_id = "System.FileUpload"; break;
            }
            entity.FileName = Path.GetFileName(originalFileName);
            entity.ExternalServiceType_id = "System.Fraud";
            entity.IsError = false;
            dc.ExternalServiceHistories.InsertOnSubmit(entity);
            dc.SubmitChanges();
        }

    }
}
