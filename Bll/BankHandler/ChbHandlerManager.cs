﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Netpay.Dal.DataAccess;
using Netpay.Crypt;
using System.Threading;
using Netpay.Infrastructure;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.BankHandler
{
    public class ChbHandlerManager : BankHandlerManagerBase
    {
        public const string SecuredObjectName = "ChbManage";

        public static Infrastructure.Security.SecuredObject SecuredObject { get { return SecuredObject.Get(BankHandler.Module.Current, SecuredObjectName); } }

        private static ChbHandlerManager _current = null;
        public static ChbHandlerManager Current { get { if (_current == null) _current = new ChbHandlerManager(); return _current; } }

        private ChbHandlerManager() : base(LogTag.Chb, @"CHB", new string[] { Bll.Accounts.ExternalServiceLogin.ServiceTypes.CHB }) 
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            AddHandler(new BnsChbBankHandler());
            AddHandler(new WireCardChbBankHandler());
            AddHandler(new CredoRaxChbBankHandler());
			AddHandler(new DeltaPayBankHandler());
			AddHandler(new CatellaChbBankHandler());
            AddHandler(new AtlasChbBankHandler());
        }

        public static void ExecFetch() { Current.FetchFilesAllDomains(); }
        public static void ExecMatch() { Current.ProcessMatchAllDomains(); }

        public override void Log(LogAction action, string originalFileName, string storedFileName)
        {
            //ExternalServiceHistory
            NetpayDataContext dc = new NetpayDataContext(Domain.Current.Sql1ConnectionString);
            tblChbFileLog entity = new tblChbFileLog();
            entity.ActionDate = DateTime.Now;
            entity.ActionType = (int)action;
            entity.OriginalFileName = Path.GetFileName(originalFileName);
            entity.StoredFileName = Path.GetFileName(storedFileName);
            dc.tblChbFileLogs.InsertOnSubmit(entity);
            dc.SubmitChanges();
        }

    }
}
