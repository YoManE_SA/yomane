﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.DataAccess;
using System.IO;
using System.Net;
using Netpay.Infrastructure;

using Netpay.Infrastructure.Security;
using Netpay.Dal.Netpay;

namespace Netpay.Bll.BankHandler
{
	public class InvikBankHandler : BankHandlerBase
	{
        public override DebitCompany DebitCompanyID { get { return Infrastructure.DebitCompany.Catella; } }
        public override string BankDirName { get { return "Invik"; } }

        private const int _recordBuffer = 100;

		private bool DownloadFTPFile(string srcFileName, string trgFileName, NetworkCredential netcred)
		{
			FtpWebRequest fwr = null;
			FtpWebResponse response = null;
			Stream responseStream = null;
			Stream sw = File.Create(trgFileName);
			try
			{
				fwr = FtpWebRequest.Create(srcFileName) as FtpWebRequest;
				fwr.UseBinary = true;
				fwr.Credentials = netcred;
				fwr.Method = WebRequestMethods.Ftp.DownloadFile;
				response = (FtpWebResponse)fwr.GetResponse();
				responseStream = response.GetResponseStream();
				int filePosition = 0;
				byte[] bufferBytes = new byte[4096];
				while (true)
				{
					filePosition = responseStream.Read(bufferBytes, 0, bufferBytes.Length);
					sw.Write(bufferBytes, 0, bufferBytes.Length);
					if (filePosition < bufferBytes.Length) break;
				}
				response.Close();
			}
			catch
			{
				return false;
			}
			finally
			{
				if (responseStream != null) responseStream.Close();
				if (response != null) response.Close();
				if (sw != null) sw.Close();
			}
			return true;

		}

        public override void FetchFile()
		{
			/*
			EpaDebitCompanyVO bank = Netpay.Infrastructure.Domain.Get(Context.DomainHost).Cache.GetEpaDebitCompany((int)EpaDebitCompany.BnS);
			string ftpFileName = bank.EpaFetchServer; // "ftp://84.94.231.232/"; //"ftp://netpay:NeTPay2008@172.17.3.67/E-STATEMENT";
			NetworkCredential netcred = new NetworkCredential(bank.EpaFetchUsername, bank.EpaFetchPassword); //"udi", "udi@netpay"
			FtpWebRequest fwr = null;
			FtpWebResponse response = null;
			Stream responseStream = null;
			try{
				fwr = FtpWebRequest.Create(ftpFileName) as FtpWebRequest;
				fwr.Credentials = netcred;
				fwr.UseBinary = true;
				fwr.Method = WebRequestMethods.Ftp.ListDirectory;
				response = (FtpWebResponse)fwr.GetResponse();
				StreamReader reader = new StreamReader(response.GetResponseStream());
				string line = reader.ReadLine();
				while (line != null)
				{
					string trgFileName = GetFilePath(EpaFolder.New, EpaDebitCompany.Invik, line);
					if (!System.IO.File.Exists(trgFileName)) DownloadFTPFile(ftpFileName + line, trgFileName, netcred);
					line = reader.ReadLine();
				}
				reader.Close();
			}
			catch
			{
				return false;
			}
			finally 
			{
				if (responseStream != null) responseStream.Close();
				if (response != null) response.Close();
			}
			*/
		}

        public override void HandlePending(BankHandlerContext context)
		{
			string ext = context.File.Extension.ToUpper();
            parseEStat(context);
		}

        private void parseEStat(BankHandlerContext context)
		{
			int nCurrency = 0;
			string terminalNum = null;
			List<tblEpaPending> rows = new List<tblEpaPending>(_recordBuffer);
			StreamReader reader = context.DecryptedStreamReader;
			string line = reader.ReadLine();
			while (line != null)
			{
				if (line.Length != 198)
				{
					Logger.Log(LogSeverity.Error, LogTag.Epa, "Invalid record size should be 198", string.Format("Line: {1}\n\rFile: {2}", line, context.File.Name));
					continue;
				}
				try
				{
					tblEpaPending row = new tblEpaPending();
					if (line.Substring(6, 2) == "BH") {
						nCurrency = Currency.Get(line.Substring(23, 3)).ID;
						terminalNum = line.Substring(27, 15).Trim();
					} else if (line.Substring(6, 2) == "TX") {
						string TransOK = line.Substring(112, 1);
						string ClrType = line.Substring(39, 15).Trim();
						row.StoredFileName = context.File.Name;
						row.InsertDate = DateTime.Now;
						row.DebitRefCode = line.Substring(54, 12); /*TransID*/
						row.CardNumber256 = Encryption.Encrypt(line.Substring(75, 19).Trim());
						row.Amount = line.Substring(161, 18).Trim().ToDecimal(0) / 100;
						row.TransDate = DateTime.ParseExact(line.Substring(67, 8), "yyyyMMdd", null);
						row.TerminalNumber = terminalNum;
						row.Installment = 1;
						row.Currency = nCurrency;
						if (row.Amount < 0) {
							row.IsRefund = true;
							row.Amount = -row.Amount;
						} else row.IsRefund = false;
						rows.Add(row);
					} else if (line.Substring(6, 2) == "TP") {
						DateTime dtVal = DateTime.ParseExact(line.Substring(9, 8), "yyyyMMdd", null);
						for (int i = 0; i < rows.Count; i++) rows[i].PayoutDate = dtVal;
						InsertParsed(rows);
						rows.Clear();
					}

				}
				catch (Exception ex)
				{
                    context.LogRowError(ex, line);
					continue;
				}
				line = reader.ReadLine();
			}
			reader.Close();
			InsertParsed(rows);
		}
	}
}
