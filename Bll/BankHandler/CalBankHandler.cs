﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using Netpay.Dal.Netpay;
using Netpay.CommonTypes;

namespace Netpay.Bll.BankHandler
{
	public class CalBankHandler : BankHandlerBase
	{
		private const int _recordBuffer = 100;
        public override DebitCompany DebitCompanyID { get { return Infrastructure.DebitCompany.ShvaCal; } }
        public override string BankDirName { get { return "VisaCal"; } }

        public override void HandlePending(BankHandlerContext context)
		{
			string ext = context.File.Extension.ToUpper();
			if (ext == ".NT2" || ext == ".NT4" || ext == ".PT6" || ext == ".NP8" || ext == ".117") //row length: 494
                parseNT24File(context);
			else if (ext == ".NT6" || ext == ".NT8" || ext == ".NF9" || ext == ".QBA" || ext == ".PQR" || ext == ".QAA" || ext == ".R2V") //row length: 504
                ParseNT68File(context);
			else if (ext == ".NF8" || ext == ".RCD" || ext == ".2VJ" || ext == ".LJG" || ext == ".SDX" || ext == ".7D4" || ext == ".RCC" || ext == ".3F8") //row length: 528
                parseNF8File(context);
			else throw new Exception("CalBankHandler File Not Supported '" + context.File.Name + "'");
        }

		private CommonTypes.Currency mapCurrencyCode(string curCode)
		{
			switch (curCode)
			{
				case "01": return CommonTypes.Currency.USD;
				case "07": return CommonTypes.Currency.EUR;
				case "02": return CommonTypes.Currency.GBP;
                case "35": return CommonTypes.Currency.AUD;
                case "40": return CommonTypes.Currency.JPY;
				case "03":
				case "04":
				case "10":
					return CommonTypes.Currency.ILS;
				//case "96": return Currency.;
				default: throw new Exception(string.Format("Currency not found in CalBankHandler currency map: {0}", curCode));
			}
		}

		private bool? IsTransTypeRefund(string transType)
		{
			if (transType == "01" || transType == "05" || transType == "08" || transType == "25" || transType == "41" || transType == "59" || transType == "60" || transType == "72") return false;
			else if (transType == "06" || transType == "13" || transType == "26" || transType == "42" || transType == "88" || transType == "86" || transType == "87") return true;
			//throw new Exception(string.Format("TransType not found in CalBankHandler IsRefund map: {0}", transType));
			return null;
		}

        private void ParseNT68File(BankHandlerContext context)
		{
			List<tblEpaPending> rows = new List<tblEpaPending>(_recordBuffer);
			StreamReader reader = context.DecryptedStreamReader;
			string line = reader.ReadLine();
			while (line != null)
			{
				if (line.Length < 504)
				{
                    context.LogRowErrorSize(504, line);
					continue;
				}

				try
				{
					tblEpaPending row = new tblEpaPending();
					row.StoredFileName = context.File.Name;
					row.InsertDate = DateTime.Now;
					row.CardNumber256 = Encryption.Encrypt(line.Substring(16, 16));
					row.IsRefund = IsTransTypeRefund(line.Substring(46, 2));
					row.Currency = (int)mapCurrencyCode(line.Substring(76, 2).Trim());
					//row.TransDate = DateTime.ParseExact(line.Substring(52, 8), "yyyyMMdd", null);
					row.TransDate = DateTime.ParseExact(line.Substring(272, 8), "yyyyMMdd", null);
					row.PayoutDate = DateTime.ParseExact(line.Substring(60, 8), "yyyyMMdd", null);
					row.Amount = line.Substring(81, 11).Trim().ToDecimal(0);
					if (row.Amount == 0) row.Amount = line.Substring(92, 14).Trim().ToDecimal(0);
					row.Installment = line.Substring(153, 3).ToInt32(1, 9000, 1);
					row.TerminalNumber = line.Substring(234, 8).Trim();
					row.ApprovalNumber = line.Substring(280, 6).Trim();
					if (row.ApprovalNumber == string.Empty) row.ApprovalNumber = "0000000";
					if (row.Amount < 0) row.Amount = -row.Amount;
					if (row.IsRefund != null) rows.Add(row);
				}
				catch (Exception ex)
				{
                    context.LogRowError(ex, line);
					continue;
				}

				if (rows.Count == rows.Capacity) 
				{
					InsertParsed(rows);
					rows.Clear();
				}
				line = reader.ReadLine();
			}
			reader.Close();
			InsertParsed(rows);
		}

        private void parseNF8File(BankHandlerContext context)
		{
			List<tblEpaPending> rows = new List<tblEpaPending>(_recordBuffer);
			StreamReader reader = context.DecryptedStreamReader;
			int lineNumber = 0;
			string line = reader.ReadLine();
			while (line != null)
			{
				lineNumber++;
				if (line.Length < 528)
				{
                    context.LogRowErrorSize(528, line);
					continue;
				}

				try
				{
					tblEpaPending row = new tblEpaPending();
					row.StoredFileName = context.File.Name;
					row.InsertDate = DateTime.Now;
					row.CardNumber256 = Encryption.Encrypt(line.Substring(16, 16));
					row.IsRefund = IsTransTypeRefund(line.Substring(46, 2));
					row.Currency = (int)mapCurrencyCode(line.Substring(76, 2).Trim());
					row.TransDate = DateTime.ParseExact(line.Substring(272, 8), "yyyyMMdd", null);
					row.PayoutDate = DateTime.ParseExact(line.Substring(60, 8), "yyyyMMdd", null);
					row.Amount = line.Substring(81, 11).Trim().ToDecimal(0);
					if (row.Amount == 0) row.Amount = line.Substring(92, 14).Trim().ToDecimal(0);
					row.Installment = line.Substring(175, 3).ToInt32(1, 9000, 1);
					row.TerminalNumber = line.Substring(234, 8).Trim();
					row.ApprovalNumber = line.Substring(280, 6).Trim();
					if (row.ApprovalNumber == string.Empty) row.ApprovalNumber = "0000000";
					if (row.Amount < 0) row.Amount = -row.Amount;
					if (row.IsRefund != null) rows.Add(row);
				}
				catch (Exception ex)
				{
                    context.LogRowError(ex, line);
					continue;
				}

				if (rows.Count == rows.Capacity)
				{
					InsertParsed(rows);
					rows.Clear();
				}
				line = reader.ReadLine();
			}
			reader.Close();
			InsertParsed(rows);
		}

        private void parseNT24File(BankHandlerContext context)
		{
			List<tblEpaPending> rows = new List<tblEpaPending>(_recordBuffer);
			StreamReader reader = context.DecryptedStreamReader;
			int lineNumber = 0;
			string line = reader.ReadLine();
			while (line != null)
			{
				lineNumber++;
				if (line.Length < 494)
				{
                    context.LogRowErrorSize(494, line);
					continue;
				}

				try
				{
					tblEpaPending row = new tblEpaPending();
					row.StoredFileName = context.File.Name;
					row.InsertDate = DateTime.Now;
					row.CardNumber256 = Encryption.Encrypt(line.Substring(24, 16));
					row.IsRefund = IsTransTypeRefund(line.Substring(40, 2));
					row.Currency = (int)mapCurrencyCode(line.Substring(105, 2).Trim());
					row.TransDate = DateTime.ParseExact(line.Substring(53, 8), "yyyyMMdd", null);
					row.PayoutDate = DateTime.ParseExact(line.Substring(69, 8), "yyyyMMdd", null);
					row.Amount = line.Substring(132, 11).Trim().ToDecimal(0);
					row.Installment = line.Substring(146, 3).ToInt32(1, 9000, 1);
					row.TerminalNumber = line.Substring(284, 8).Trim();
					row.ApprovalNumber = line.Substring(479, 6).Trim();
					if (row.ApprovalNumber == string.Empty) row.ApprovalNumber = "0000000";
					if (row.Amount < 0) row.Amount = -row.Amount;
					if (row.IsRefund != null) rows.Add(row);
				}
				catch (Exception ex)
				{
                    context.LogRowError(ex, line);
					continue;
				}

				if (rows.Count == rows.Capacity) 
				{
					InsertParsed(rows);
					rows.Clear();
				}
				line = reader.ReadLine();
			}
			reader.Close();
			InsertParsed(rows);
		}
		
	}
}
