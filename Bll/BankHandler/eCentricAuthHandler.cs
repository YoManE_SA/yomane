﻿using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace Netpay.Bll.BankHandler
{
    public class eCentricAuthHandler : Iso8583BankHandlerBase
    {
        public eCentricAuthHandler() : base(Infrastructure.DebitCompany.eCentric, "OneSP") { }
    }
}
