﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Bll.Accounts;
using System.IO;

namespace Netpay.Bll.BankHandler
{
    public class FNBAuthHandler : BankHandlerBase
    {
        public override DebitCompany DebitCompanyID { get { return Infrastructure.DebitCompany.eCentric; } }
        public override string BankDirName { get { return "FNB"; } }
        protected override string InternalBankDirName { get { return "FNB"; } }

        private const string STOREDPM_PROVIDERNAME = "System.eCentric";
        private System.Net.FtpWebRequest CreateFtpRequest(Accounts.ExternalServiceLogin login, string method, string fileName = null)
        {
            var request = System.Net.FtpWebRequest.Create("ftp://" + login.ServerURL + "/" + fileName) as System.Net.FtpWebRequest;
            //request.UsePassive = false;
            request.Credentials = new System.Net.NetworkCredential(login.Username, login.Password);
            request.Method = method;
            request.EnableSsl = true;
            request.KeepAlive = false;
            return request;
        }

        public override void FetchFile()
        {
            RetrieveFtpFiles();
            //if (!Infrastructure.Tasks.Lock.TryLock("FNB_DEPOSIT_FETCH", DateTime.Now.AddMinutes(-3))) return;           
            //try
            //{
            //    var bank = DebitCompanies.DebitCompany.GetServiceLogin((int)DebitCompany.eCentric, Accounts.ExternalServiceLogin.ServiceTypes.Deposit);
            //    if (!bank.IsActive) return;
            //    if (string.IsNullOrEmpty(bank.ServerURL) || string.IsNullOrEmpty(bank.Username))
            //        throw new ApplicationException("FNB server or username not defined.");
            //    string[] ftpFiles = null;
            //    try
            //    {
            //        System.Net.ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true; //ignore cert errors
            //        var ftp = CreateFtpRequest(bank, System.Net.WebRequestMethods.Ftp.ListDirectory);
            //        using (var response = (System.Net.FtpWebResponse)ftp.GetResponse())
            //        {
            //            using (var streamReader = new System.IO.StreamReader(response.GetResponseStream()))
            //            {
            //                ftpFiles = streamReader.ReadToEnd().Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            //                streamReader.Close();
            //            }
            //            response.Close();
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        throw new Exception("Get Directory Content", ex);
            //    }
            //    foreach (var f in ftpFiles)
            //    {
            //        if (System.IO.Path.GetExtension(f).ToUpper() != ".DAT") continue;
            //        try
            //        {
            //            var ftp = CreateFtpRequest(bank, System.Net.WebRequestMethods.Ftp.DownloadFile, f);
            //            using (var response = (System.Net.FtpWebResponse)ftp.GetResponse())
            //            {
            //                using (var streamReader = new System.IO.StreamReader(response.GetResponseStream()))
            //                {
            //                    System.IO.File.WriteAllText(System.IO.Path.Combine(GetPath(BankHandler.EpaFolder.New), f), streamReader.ReadToEnd());
            //                    streamReader.Close();
            //                }
            //                response.Close();
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            throw new Exception("Get File", ex);
            //        }
            //        try
            //        {
            //            var ftp = CreateFtpRequest(bank, System.Net.WebRequestMethods.Ftp.DeleteFile, f);
            //            ftp.UseBinary = true;
            //            using (var response = (System.Net.FtpWebResponse)ftp.GetResponse())
            //            {
            //                using (var streamReader = new System.IO.StreamReader(response.GetResponseStream()))
            //                {
            //                    var result = streamReader.ReadToEnd();
            //                    streamReader.Close();
            //                }
            //                response.Close();
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            throw new Exception("Delete File", ex);
            //        }
            //    }
            //}
            //catch
            //{
            //    throw;
            //}
            //finally
            //{
            //    Infrastructure.Tasks.Lock.Release("FNB_DEPOSIT_FETCH");
            //}
        }

        public override void HandlePending(BankHandlerContext context)
        {
            var rows = new List<Accounts.Balance>();
            var reader = context.DecryptedStreamReader;
            //using (var reader = context.DecryptedStreamReader)
            //{
            for (string currentLine = reader.ReadLine(); currentLine != null; currentLine = reader.ReadLine())
            {
                try
                {
                    var refnumber = currentLine.Substring(0, 15).Trim();
                    var amount = currentLine.Substring(181, 20).ToNullableDecimal().GetValueOrDefault() / 100;
                    var accountNumber = currentLine.Substring(201, 20).TrimEnd(); //transaction reference
                    var isDebit = currentLine.Substring(38, 1) == "D";

                    if (isDebit)
                    {
                        Logger.Log(LogSeverity.Info, LogTag.NetpayAdmin, "FNB Skipping " + currentLine);
                        continue;
                    }

                    int accountId = 0;
                    if (accountNumber.Length > 8)
                    {
                        int digitPos = 0;
                        accountNumber = accountNumber.Replace(" ", "");
                        for (; digitPos < accountNumber.Length; digitPos++)
                            if (char.IsNumber(accountNumber[digitPos])) break;
                        accountNumber = accountNumber.Substring(digitPos);
                        if (accountNumber.Length == 10) accountNumber = "971040" + accountNumber;
                        var spm = Accounts.StoredPaymentMethod.FindByPAN(accountNumber, null).SingleOrDefault(); //.Where(v => v.ProviderID == STOREDPM_PROVIDERNAME)
                        if (spm != null) accountId = spm.Account_id;
                    }
                    else
                    {
                        accountId = accountNumber.ToNullableInt().GetValueOrDefault();
                    }
                    if (accountId != 0)
                    {
                        if (isDebit) amount = -amount;
                        var exist = Accounts.Balance.Search(new Accounts.Balance.SearchFilters() { Amount = new Range<decimal?>(amount), AccountID = accountId, CurrencyIso = "ZAR", Text = "FNB transfer: " + refnumber }, null).Any();
                        if (!exist)
                            Accounts.Balance.Create(accountId, "ZAR", amount, "FNB transfer: " + refnumber, Accounts.Balance.SOURCE_DEPOSIT, null, false);
                    }
                    else Logger.Log(new Exception("FBN Error on parse, could not find card/account: " + accountNumber));
                }
                catch (Exception exx)
                {
                    Logger.Log(new Exception("FBN Error on parse: " + currentLine, exx));
                }
            }
            reader.Close();
            //}
        }

        private static System.Threading.Timer _timerFetch;
        public static bool EnableRealTimeFetch
        {
            get { return (_timerFetch == null); }
            set
            {
                if (value)
                {
                    if (_timerFetch == null) _timerFetch = new System.Threading.Timer(RealTimeFetchTimer, null, 0, 5 * 60 * 1000);
                }
                else if (_timerFetch != null) _timerFetch.Dispose();
            }
        }
        private static void RealTimeFetchTimer(object state)
        {
            var fnbHandler = AuthHandlerManager.Current.Handlers.Where(v => v is FNBAuthHandler).SingleOrDefault();
            if (fnbHandler == null) return;
            foreach (var d in Domain.Domains.Values)
            {
                Domain.Current = d;
                try
                {
                    ObjectContext.Current.Impersonate(d.ServiceCredentials);
                    fnbHandler.FetchFile();
                }
                catch (Exception ex)
                {
                    Logger.Log(ex, "FNB RealTimeFetchTimer");
                }
                finally
                {
                    ObjectContext.Current.StopImpersonate();
                }
            }
        }

        #region FTP Management


        private void RetrieveFtpFiles()
        {

            if (!Infrastructure.Tasks.Lock.TryLock("FNB_DEPOSIT_FETCH", DateTime.Now.AddMinutes(-3))) return;
            var bank = DebitCompanies.DebitCompany.GetServiceLogin((int)DebitCompany.eCentric, Accounts.ExternalServiceLogin.ServiceTypes.Deposit);


            try
            {
                //constructor accepts all certificates

                var ftp = new FtpManager(new FtpCredentials
                {
                    Username = bank.Username,
                    Password = bank.Password,
                    HostIp = bank.HostIp,
                    PortNumber = bank.Port
                });

                var item = ftp.ListAllContents(bank.FtpRootFolder, bank.SearchableFolderList, bank.SearchableExtensionList);
                //  var item = ftp.ListAllContents(bank.FtpRootFolder, messages, null);

                string destFolder = GetPath(BankHandler.EpaFolder.New);
                foreach (var directory in item)
                {

                     foreach (var file in directory.FileList)
                    {
                        string fileName = Path.GetFileName(file);
                        var destFile = SetDestinationFile(fileName, destFolder);
                        if (!string.IsNullOrEmpty(fileName))
                        {
                            var downloadedPath = ftp.DownloadFile(file, Path.Combine(destFolder, destFile), true);
                         
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(LogTag.Exception, new Exception("FNB FTP handler failed", ex));
            }
            finally
            {
                Infrastructure.Tasks.Lock.Release("FNB_DEPOSIT_FETCH");
            }
        }

        private static string SetDestinationFile(string file, string destFolder)
        {
            var newFileName = string.Empty;
            var extension = string.Empty;
            var dir = new DirectoryInfo(destFolder);
            var nameWithoutExt = Path.GetFileNameWithoutExtension(file);
            if (!string.IsNullOrEmpty(nameWithoutExt))
            {
                extension = file.Replace(nameWithoutExt, string.Empty);
                var similarFiles = dir.GetFiles().Where(x => x.Name.StartsWith(nameWithoutExt));
                int count = similarFiles.Count();
                if (count < 1)
                    newFileName = file;
                else
                {
                    newFileName = string.Format("{0}_{1}{2}", nameWithoutExt, count, extension);
                }
            }

            return newFileName;
        }


        private static IEnumerable<string> SetDownloadableExtensions(ExternalServiceLogin bank)
        {
            IList<string> list = new List<string>();
            var temp = bank.SearchableExtensionList;
            if (!string.IsNullOrEmpty(temp))
            {
                if (temp.Contains(";"))
                {
                    var tempList = temp.Split(';');
                    foreach (var s in tempList)
                    {
                        list.Add(s);
                    }
                }
                else
                {
                    list.Add(temp);
                }
            }
            return list;
        }

        private static IEnumerable<string> GetSearchableFolders(ExternalServiceLogin bank)
        {
            IList<string> list = new List<string>();
            var temp = bank.SearchableFolderList;
            if (!string.IsNullOrEmpty(temp))
            {
                if (temp.Contains(";"))
                {
                    var tempList = temp.Split(';');
                    foreach (var s in tempList)
                    {
                        list.Add(s);
                    }
                }
                else
                {
                    list.Add(temp);
                }
            }
            return list;

        }


        #endregion FTP Management

    }
}
