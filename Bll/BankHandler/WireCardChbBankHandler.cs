﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;

using Netpay.Infrastructure.Email.Pop3;
using Netpay.Infrastructure.Email.Pop3.SharpMimeTools;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.BankHandler
{
	public class WireCardChbBankHandler : BankHandlerBase
	{
        public override DebitCompany DebitCompanyID { get { return Infrastructure.DebitCompany.WireCard; } }
        private const int _recordBuffer = 100;

        public override void FetchFile() //sftp
		{
            var bank = LoadEpaLogin(Accounts.ExternalServiceLogin.ServiceTypes.CHB, Accounts.ExternalServiceLogin.Protocols.SFTP);
            if (!bank.IsActive) return;

            string scriptFileName = Manager.MapTempFile("WireCardChbFtpScript.ftp");
            string logFile = Manager.MapTempFile("WireCardEpaFtpLogFile.log");
            //create script file
            System.IO.StreamWriter sw = new System.IO.StreamWriter(scriptFileName);
            sw.WriteLine(bank.GetProtocolLoginCommand(GetPrivateKey(), GetHostKey()));
            sw.WriteLine("option batch continue");
            sw.WriteLine("option confirm off");
            sw.WriteLine("lcd " + GetPath(EpaFolder.New));
            sw.WriteLine("get -delete /toC12930/new/chargeback_*.csv");
            sw.WriteLine("exit");
            sw.Close();

            var ret = bank.ExecuteFTPScript(scriptFileName, logFile);
            if (ret != 0) throw new ApplicationException("WireCard SFTP failed with code = " + ret);
		}

        public override void HandlePending(BankHandlerContext context)
		{
			List<tblChbPending> rows = new List<tblChbPending>();
			StreamReader reader = context.DecryptedStreamReader;
            try
            {
                reader.ReadLine();
			    for (string currentLine = reader.ReadLine(); currentLine != null; currentLine = reader.ReadLine())
			    {
				    try
				    {
                        /*
                         * 0 "MERCHANTTRANSACTIONID";
                         * 1 "MERCHANTFUNCTIONID";
                         * 2 "CBID";
                         * 3 "CCNO";
                         * 4 "TRANSDATE";
                         * 5 "RRDATE";
                         * 6 "CBHISTORYDATE";
                         * 7 "CBDATE";
                         * 8 "CBAMOUNT";
                         * 9 "CBCURRENCY";
                         *10 "CBCONVERSIONRATE";
                         *11 "TRANSACTIONAMOUNT";
                         *12 "TRANSACTIONCURRENCY";
                         *13 "REASONCODE";
                         *14 "GUWID";
                         *15 "BUSINESSCASESIGNATURE";
                         *16 "BILLINGSTATUS"
                        */
                        DateTime transDate;
                        string[] currentLineSplit = currentLine.Split(';');
						tblChbPending entity = new tblChbPending();
                        entity.DebitRefCode = currentLineSplit[0].Trim();
						entity.Amount = currentLineSplit[8].ToNullableDecimal();
						entity.ApprovalNumber = currentLineSplit[14].Trim();
						entity.CardNumber256 = Netpay.Infrastructure.Security.Encryption.Encrypt(currentLineSplit[3]);
						entity.Currency = Currency.Get(currentLineSplit[9]).ID;
						entity.InsertDate = DateTime.Now;
						entity.Installment = 1;
						entity.StoredFileName = context.File.Name;
						entity.ReasonCode = currentLineSplit[13].Trim();
						entity.CaseID = currentLineSplit[15].Trim();
						entity.TerminalNumber = null;
						entity.TransCount = null;
                        if (string.IsNullOrWhiteSpace(entity.ReasonCode)) entity.ReasonCode = "0";
                        if (DateTime.TryParseExact(currentLineSplit[4], "dd-MMM-yy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out transDate))
                            entity.TransDate = transDate;
                        if (DateTime.TryParseExact(currentLineSplit[7], "dd-MMM-yy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out transDate))
                            entity.ChargebackDate = transDate;
						entity.TransID = null;
						entity.IsChargeback = true;
						entity.IsPhotocopy = false;
					    rows.Add(entity);	
				    }
				    catch (Exception ex)
				    {
                        context.LogRowError(ex, currentLine);
					    continue;
				    }		
			    }
            }
            catch { }
            finally
            {
                reader.Close();
                InsertParsed(rows);
            }
		}
	}
}