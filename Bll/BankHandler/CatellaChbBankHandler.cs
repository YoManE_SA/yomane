﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;

using Netpay.Infrastructure.Email.Pop3;
using Netpay.Infrastructure.Email.Pop3.SharpMimeTools;
using Netpay.Infrastructure.Security;
using Netpay.CommonTypes;

namespace Netpay.Bll.BankHandler
{
	public class CatellaChbBankHandler : BankHandlerBase
	{
		private const int CatellaDebitCompanyID = 21;
        public override DebitCompany DebitCompanyID { get { return Infrastructure.DebitCompany.Catella; } }

        public override void FetchFile() //sftp
		{
            var bank = LoadEpaLogin(Accounts.ExternalServiceLogin.ServiceTypes.CHB, Accounts.ExternalServiceLogin.Protocols.SFTP);
            if (!bank.IsActive) return;

            string scriptFileName = Manager.MapTempFile("CatellaChbFtpScript.ftp");
            string logFile = Manager.MapTempFile("CatellaChbFtpLogFile.log");
			//create script file
			System.IO.StreamWriter sw = new System.IO.StreamWriter(scriptFileName);
            sw.WriteLine(bank.GetProtocolLoginCommand(GetPrivateKey(), GetHostKey()));
			sw.WriteLine("option batch continue");
			sw.WriteLine("option confirm off");
            sw.WriteLine("lcd " + GetPath(EpaFolder.New));
			sw.WriteLine("get -delete /in/*.BIF");
			sw.WriteLine("exit");
			sw.Close();

            var ret = bank.ExecuteFTPScript(scriptFileName, logFile);
            if (ret != 0) throw new ApplicationException("Catella SFTP failed with code = " + ret);
		}

        public override void HandlePending(BankHandlerContext context)
		{
            HandleDatPending(context);
		}

        public void HandleDatPending(BankHandlerContext context)
		{
			List<tblChbPending> rows = new List<tblChbPending>();
            StreamReader reader = context.DecryptedStreamReader;
			try
			{
				var columns = reader.ReadLine();
				/*
					0. Entity id column name
					1. Card Type column name
					2. Arn column name
					3. Card Number column name
					4. Name column name
					5. Merchand Id column name
					6. Country column name
					7. Transaction Date column name
					8. Amount column name
					9. Currency column name
					10. AuthCode column name
					11. File Type column name
					12. Reason Code column name
					13. Received Date column name				 
				*/

				for (string currentLine = reader.ReadLine(); currentLine != null; currentLine = reader.ReadLine())
				{
					string[] currentLineSplit = currentLine.Split(';');
					tblChbPending entity = new tblChbPending();
					entity.ReasonCode = currentLineSplit[12].Trim();
                    if (string.IsNullOrWhiteSpace(entity.ReasonCode)) entity.ReasonCode = "0";
					entity.AcquirerReferenceNum = currentLineSplit[2].Trim();
					entity.CardNumber256 = Encryption.Encrypt(currentLineSplit[1]);
					entity.Amount = currentLineSplit[8].ToNullableInt() / 100;
					entity.Currency = Currency.Get(currentLineSplit[9]).ID;
					entity.TransID = currentLineSplit[0].ToNullableInt();
					entity.DebitCompany_id = CatellaDebitCompanyID;
					entity.TerminalNumber = currentLineSplit[5];
					entity.InsertDate = DateTime.Now;
					entity.Installment = 1;
					entity.StoredFileName = context.File.Name;
					entity.IsChargeback = context.File.Name.StartsWith("CB1");
					entity.IsPhotocopy = !entity.IsChargeback;
					DateTime transDate;
					if (DateTime.TryParseExact(currentLineSplit[7], "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out transDate))
						entity.TransDate = transDate;
					if (DateTime.TryParseExact(currentLineSplit[13], "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out transDate))
						entity.ChargebackDate = transDate;
					rows.Add(entity);
				}
			}
			catch { }
			finally
			{
				reader.Close();
				InsertParsed(rows);
			}
		}
	}
}
