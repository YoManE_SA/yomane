﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;

using Netpay.Infrastructure.Email.Pop3;
using Netpay.Infrastructure.Email.Pop3.SharpMimeTools;
using Netpay.Infrastructure.Security;
using Netpay.CommonTypes;

namespace Netpay.Bll.BankHandler
{
	public class CredoRaxFraudBankHandler : BankHandlerBase
	{
        public override DebitCompany DebitCompanyID { get { return Infrastructure.DebitCompany.CredoRax; } }

        private const int _recordBuffer = 100;

		public override void FetchFile() //sftp
		{
            var bank = LoadEpaLogin(Accounts.ExternalServiceLogin.ServiceTypes.Fraud, Accounts.ExternalServiceLogin.Protocols.SFTP);
            if (!bank.IsActive) return;

            string scriptFileName = Manager.MapTempFile("CredoRaxFraudFtpScript.ftp");
            string logFile = Manager.MapTempFile("CredoRaxFraudFtpLogFile.log");
            //create script file
            System.IO.StreamWriter sw = new System.IO.StreamWriter(scriptFileName);
            sw.WriteLine(bank.GetProtocolLoginCommand(GetPrivateKey(), GetHostKey()));
            sw.WriteLine("option batch continue");
            sw.WriteLine("option confirm off");
            sw.WriteLine("lcd " + GetPath(EpaFolder.New));
            sw.WriteLine("get -delete /netpay/outgoing/TC*.*");
            sw.WriteLine("get -delete /netpay/outgoing/TF_*.*");
            sw.WriteLine("get -delete /netpay/outgoing/SF_*.*");
            sw.WriteLine("exit");
            sw.Close();        

            var ret = bank.ExecuteFTPScript(scriptFileName, logFile);
            if (ret != 0) throw new ApplicationException("Credorax SFTP failed with code = " + ret);
        }

        public override void HandleNew(BankHandlerContext context)
        {
            var archPath = GetFilePath(EpaFolder.Archive, context.File.Name);
            if (System.IO.File.Exists(archPath)) throw new DuplicateFileException();
            base.HandleNew(context);
        }

        public override void HandlePending(BankHandlerContext context)
		{
            if (context.File.Name.StartsWith("TC")) parseTCFiles(context); //v4
            else if (context.File.Name.StartsWith("TF_")) parseTFFiles(context); //v5
            else if (context.File.Name.StartsWith("SF_")) parseSFFiles(context); //v5
            else throw new Exception("Unsupported file type:" + context.File.FullName);
        }

        public void parseTCFiles(BankHandlerContext context)
        {
            var rows = new List<TransMatchPending>();
            StreamReader reader = context.DecryptedStreamReader;
            try
            {
                string[] headerLine = reader.ReadLine().Split(',');
                if (headerLine.Length < 15)
                    throw new Exception(string.Format("Failed to parse record in CredoRax TC file \n\rFile: {0}\n\rLine 0\r\nInvalid file format", context.File.Name));
                for (string currentLine = reader.ReadLine(); currentLine != null; currentLine = reader.ReadLine())
                {
                    try
                    {
                        string[] currentLineSplit = extractCsvLine(currentLine);
                        if (currentLineSplit[0] == "====END OF FILE====") break;
                        /*
                        0: descriptor,
                        1: city,
                        2: country_code,
                        3: mcc_code,
                        4: fraud_type,
                        5: amount,
                        6: issuer_bin,
                        7: purchase_date,
                        8: card_number,
                        9: sequence,
                        10: arn_number,
                        11: pos_entry_mode,
                        12: "RequestID a1",
                        13: "Searchable Codes h9",
                        14: "Card Holder Country c9"
                        */

                        var entity = new TransMatchPending();
                        entity.DebitCompany_id = (int) DebitCompanyID;
                        entity.ExternalServiceType_id = Bll.Accounts.ExternalServiceLogin.ServiceTypes.Fraud;
                        entity.InsertDate = DateTime.Now;
                        entity.FileName = context.File.Name;
                        entity.ReasonCode = currentLineSplit[4].Trim();
                        entity.Amount = currentLineSplit[5].Trim().ToNullableDecimal();
                        entity.TransDate = currentLineSplit[7].ToNullableDate();
                        entity.ARN = currentLineSplit[10].Trim();
                        entity.DebitReferenceNum = currentLineSplit[12].Trim();
                        rows.Add(entity);
                    }
                    catch (Exception ex)
                    {
                        context.LogRowError(ex, currentLine);
                        continue;
                    }
                }
            } finally {
                reader.Close();
                InsertParsed(rows);
            }
        }
        public void parseTFFiles(BankHandlerContext context)
        {
            var rows = new List<TransMatchPending>();
            StreamReader reader = context.DecryptedStreamReader;
            try
            {
                string[] headerLine = reader.ReadLine().Split(',');
                if (headerLine.Length < 13)
                    throw new Exception(string.Format("Failed to parse record in CredoRax TF file \n\rFile: {0}\n\rLine 0\r\nInvalid file format", context.File.Name));
                for (string currentLine = reader.ReadLine(); currentLine != null; currentLine = reader.ReadLine())
                {
                    try
                    {
                        string[] currentLineSplit = extractCsvLine(currentLine);
                        if (currentLineSplit[0] == "====END OF FILE====") break;
                        /*
                        0: descriptor,
                        1: country_code,
                        2: trans_mcc,
                        3: fraud_type,
                        4: amount,
                        5: issuer_bin,
                        6: trans_date,
                        7: card_number,
                        8: arn,
                        9: payment_channel,
                        10: request_id,
                        11: searchable_code,
                        12: target_country_code
                        */

                        var entity = new TransMatchPending();
                        entity.DebitCompany_id = (int)DebitCompanyID;
                        entity.ExternalServiceType_id = Bll.Accounts.ExternalServiceLogin.ServiceTypes.Fraud;
                        entity.InsertDate = DateTime.Now;
                        entity.FileName = context.File.Name;
                        entity.ReasonCode = currentLineSplit[3].Trim();
                        entity.Amount = currentLineSplit[4].Trim().ToNullableDecimal();
                        entity.TransDate = currentLineSplit[6].ToNullableDate();
                        entity.ARN = currentLineSplit[8].Trim();
                        entity.DebitReferenceNum = currentLineSplit[10].Trim();
                        try { entity.CurrencyISOCode = Currency.Get(currentLineSplit[12]).IsoCode; } catch { } //for validation 
                        rows.Add(entity);
                    }
                    catch (Exception ex)
                    {
                        context.LogRowError(ex, currentLine);
                        continue;
                    }
                }
            }
            finally
            {
                reader.Close();
                InsertParsed(rows);
            }
        }

        public void parseSFFiles(BankHandlerContext context)
        {
            var rows = new List<TransMatchPending>();
            using (StreamReader reader = context.DecryptedStreamReader) { 
                try
                {
                    if (reader.EndOfStream) return;
                    string[] headerLine = reader.ReadLine().Split(',');
                    if (headerLine.Length < 63)
                        throw new Exception(string.Format("Failed to parse record in CredoRax TS file \n\rFile: {0}\n\rLine 0\r\nInvalid file format", context.File.Name));
                    for (string currentLine = reader.ReadLine(); currentLine != null; currentLine = reader.ReadLine())
                    {
                        try
                        {
                            string[] currentLineSplit = extractCsvLine(currentLine);
                            if (currentLineSplit[0] == "====END OF FILE====") break;
                            /*
                             0: Acquirer,
                             1: Issuer,
                             2: "Routing Number",
                             3: "Transaction Date",
                             4: "Fraud Type",
                             5: "Fraud Subtype",
                             6: "Account Number",
                             7: "Card Type Code",
                             8: "Post Date",
                             9: "Amount USD",
                             10: "Amount Local",
                             11: "Local Currency Code",
                             12: "Local Currency Exponent",
                             13: "Date Settled",
                             14: "Billing Amount",
                             15: "Billing Currency Code",
                             16: "Billing Currency Exponent",
                             17: "Merchant Name",
                             18: "Merchant City",
                             19: "Merchant State",
                             20: "Merchant Province",
                             21: "Merchant Country",
                             22: "Merchant Post Code",
                             23: "Merchant ID",
                             24: "Merchant MCC",
                             25: "Auth Response Code",
                             26: "POS Entry Mode",
                             27: "POS Approval Code",
                             28: "Terminal ID",
                             29: "Chargeback Y/N",
                             30: "Security Bulletin No",
                             31: ARN,
                             32: "Serial ID",
                             33: "Trace ID",
                             34: "Terminal Attendence Code",
                             35: "Cardholder Present Code",
                             36: "Cardholder Activated POS Level",
                             37: "Terminal Capabilities Code",
                             38: "Electronic Commerce Security Level",
                             39: "CVC Code",
                             40: "Report Date",
                             41: "Account Closed",
                             42: "Account Opened Date",
                             43: "Cardholder Postal Code",
                             44: "Cardholder Spending Limit",
                             45: "Acct Stat CD",
                             46: "Cardholder Spending Limit Currency",
                             47: "Chargeback Notified",
                             48: "Safe Chargeback Code",
                             49: "Safe Entered Date",
                             50: "Cashback Amount USD",
                             51: "Cashback Amount Local",
                             52: "Cashback Currency Code",
                             53: "Cashback Currency Exponent",
                             54: "Card Issue Number",
                             55: "Account Device Type",
                             56: "Secure Code",
                             57: "AVS Response Code",
                             58: "Card was Present",
                             59: "Terminal Operation Enviroment",
                             60: "RequestID a1",
                             61: "Searchable Codes h9",
                             62: "Card Holder Country c9"
                            */

                            var entity = new TransMatchPending();
                            entity.DebitCompany_id = (int)DebitCompanyID;
                            entity.ExternalServiceType_id = Bll.Accounts.ExternalServiceLogin.ServiceTypes.Fraud;
                            entity.InsertDate = DateTime.Now;
                            entity.FileName = context.File.Name;
                            entity.ReasonCode = currentLineSplit[4].Trim();
                            entity.Amount = currentLineSplit[10].Trim().ToNullableDecimal();
                            var currencyExponent = currentLineSplit[12].ToNullableInt().GetValueOrDefault();
                            if (currencyExponent > 0) entity.Amount /= (decimal)System.Math.Pow(10, currencyExponent);
                            entity.CurrencyISOCode = Currency.GetByIsoNumber(currentLineSplit[11].ToNullableInt().Value).IsoCode;
                            entity.TransDate = currentLineSplit[3].ToNullableDate();
                            entity.ARN = currentLineSplit[31].Trim();
                            entity.DebitReferenceNum = currentLineSplit[60].Trim();
                            rows.Add(entity);
                        }
                        catch (Exception ex)
                        {
                            context.LogRowError(ex, currentLine);
                            continue;
                        }
                    }
                }
                finally
                {
                    reader.Close();
                    InsertParsed(rows);
                }
            }
        }

        public static string[] extractCsvLine(string strLine)
		{
			var retArray = new List<string>(); 
			int curDel, startIndex = 0;
			do{
				curDel = strLine.IndexOf(',', startIndex);
				if(curDel == -1) curDel = strLine.Length;
				var fieldValue = strLine.Substring(startIndex, curDel - startIndex);
				if (fieldValue.StartsWith("\"")){
					curDel = strLine.IndexOf('\"', startIndex + 1);
					if (curDel == -1) curDel = strLine.Length;
					fieldValue = strLine.Substring(startIndex + 1, curDel - startIndex - 1);
					curDel++;
				}
				retArray.Add(fieldValue);
				startIndex = curDel + 1;
			} while(curDel < strLine.Length);
			return retArray.ToArray();
		}

	}
}