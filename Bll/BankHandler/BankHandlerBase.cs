﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Netpay.Dal.DataAccess;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;

namespace Netpay.Bll.BankHandler
{
    public abstract class BankHandlerBase
    {
        protected BankHandlerBase() { }
        public class DuplicateFileException : Exception {  }
        public BankHandlerManagerBase Manager { get; private set; }
        public abstract Infrastructure.DebitCompany DebitCompanyID { get; }
        //public int AccountID { get; private set; }
        public virtual string BankDirName { get { return DebitCompanyID.ToString(); } }
        
        protected virtual string InternalBankDirName { get { return DebitCompanyID.ToString(); } }

        public virtual void Init(BankHandlerManagerBase manager)
        {
            Manager = manager;
            //AccountID = DataContext.Reader.Accounts.Where(a => a.DebitCompany_id == (int)DebitCompanyID).Select(a => a.Account_id).SingleOrDefault();
            //if (AccountID == 0) throw new Exception("BankHandler could not find AccountID for DebitCompanyID=" + DebitCompanyID.ToString() + " ID=" + (int)DebitCompanyID);

        }

        public void EnsureDirectoriesForDoamin()
        {
            foreach (EpaFolder f in Enum.GetValues(typeof(EpaFolder)))
            {
                var directory = GetPath(f);
                if (!System.IO.Directory.Exists(directory))
                {
                    System.IO.Directory.CreateDirectory(directory);
                    Logger.Log(LogSeverity.Warning, Manager.LogTag, "Directory was not found " + directory + ", Created.", directory);
                }
            }
        }

        public Accounts.ExternalServiceLogin LoadEpaLogin(string serviceType, string protocolType)
        {
            var bank = DebitCompanies.DebitCompany.GetServiceLogin((int)DebitCompanyID, serviceType);
            if (!bank.IsActive) return bank;
            var isValid = bank.Validate();
            if (isValid != Infrastructure.ValidationResult.Success) throw new Infrastructure.ValidationException(isValid);
            return bank;
        }

        public string GetPath(EpaFolder folder)
        {
            return Domain.Current.MapPrivateDataPath(string.Format(@"Process\{0}\{1}\{2}\", InternalBankDirName, Manager.BasePath, folder.ToString()));
        }

        public string GetFilePath(EpaFolder folder, string fileName)
        {
            return System.IO.Path.Combine(GetPath(folder), fileName);
        }

        public string GetPrivateKey()
        {
            var ret = string.Format(Domain.Current.MapPrivateDataPath(@"Process\{0}\Keys\{1}.ppk"), BankDirName, Manager.BasePath);
            if (!System.IO.File.Exists(ret)) return null;
            return ret;
        }

        public string GetHostKey()
        {
            var hk = string.Format(Domain.Current.MapPrivateDataPath(@"Process\{0}\Keys\{1}_HostKey.txt"), BankDirName, Manager.BasePath);
            if (System.IO.File.Exists(hk)) return System.IO.File.ReadAllText(hk);
            return null;
        }

        public virtual void GenerateFile() { }
        public virtual void FetchFile() { }

        public void ProcessNew(BankHandlerContext context)
        {
            if (context.File.IsLocked()) return;
            string originalFileName = context.File.FullName;
            string destinationPath = Domain.GetUniqueFileName(GetFilePath(EpaFolder.Pending, System.IO.Path.GetFileName(context.File.FullName)));
            Manager.Log(BankHandler.LogAction.NewFileProcessStart, context.File.FullName, destinationPath);
            try
            {
                HandleNew(context);
                context.EncryptFile(destinationPath);
                File.Delete(context.File.FullName);
                context.File = new FileInfo(destinationPath);
                Manager.Log(BankHandler.LogAction.NewFileProcessCompleted, originalFileName, destinationPath);
            } catch (DuplicateFileException ex) {
                File.Delete(context.File.FullName);
                Manager.Log(BankHandler.LogAction.NewFileProcessCompleted, originalFileName, "");
            } catch (Exception ex) {
                context.HandleError(this, ex);
                Manager.Log(LogAction.Error, originalFileName, destinationPath);
            }
        }

        public virtual void HandleNew(BankHandlerContext context) { }

        public void ProcessPending(BankHandlerContext context)
        {
            if (context.File.IsLocked()) return;
            string originalFileName = context.File.FullName;
            Manager.Log(LogAction.PendingFileProcessStart, context.File.FullName, null);
            try
            {
                HandlePending(context);
                context.WriteRowErrors(this);
                context.MoveTo(this, EpaFolder.Archive);
                Manager.Log(LogAction.PendingFileProcessComplete, originalFileName, context.File.FullName);
            }
            catch (Exception ex)
            {
                context.HandleError(this, ex);
                Manager.Log(LogAction.Error, originalFileName, context.File.FullName);
            }
        }

        public virtual void HandlePending(BankHandlerContext context) { }

        public void ProcessMatch(DataContext dc, MatchValue value) {
            try {
                HandleMatch(dc, value);
                dc.TransMatchPendings.DeleteOnSubmit(value.Pending);
            } catch (Exception ex) {
                Logger.Log(Manager.LogTag, ex);
            }
        }

        public virtual void HandleMatch(DataContext dc, MatchValue value)
        {
            if (!value.MerchantID.HasValue)
                return;

            if (value.Pending.ExternalServiceType_id == Bll.Accounts.ExternalServiceLogin.ServiceTypes.Fraud)
            {
                var trx = Netpay.Bll.Transactions.Transaction.GetTransaction(value.MerchantID, value.TransID.Value, TransactionStatus.Captured);
                if (!trx.IsFraud) trx.SetFraud(true);
            }
            else
            {
                var history = new TransHistory();
                history.InsertDate = DateTime.Now;
                history.TransPass_id = value.TransID;
                history.Merchant_id = value.MerchantID.GetValueOrDefault();
                history.ReferenceNumber = value.Pending.ReasonCode.ToNullableInt();
                history.ReferenceUrl = value.Pending.FileName;
                history.IsSucceeded = true;
                if (FormatMatchEvent(dc, history, value))
                {
                    if (dc.TransHistories.Any(v => v.TransPass_id == value.TransID && v.TransHistoryType_id == history.TransHistoryType_id)) return;
                    //dc.ExecuteCommand("Update tblCompanyTransPass Set IsFraudByAcquirer=1 Where ID=" + value.TransID); // todo use trx setfraud method
                    dc.TransHistories.InsertOnSubmit(history);
                } 
            }
        }

        protected virtual bool FormatMatchEvent(DataContext dc, TransHistory history, MatchValue value)
        {
            switch (value.Pending.ExternalServiceType_id)
            {
                case Bll.Accounts.ExternalServiceLogin.ServiceTypes.Fraud:
                    history.TransHistoryType_id = (byte)CommonTypes.TransactionHistoryType.FraudSet;
                    break;
                case Bll.Accounts.ExternalServiceLogin.ServiceTypes.CHB:
                    history.TransHistoryType_id = (byte) CommonTypes.TransactionHistoryType.Chargeback;
                    break;
                case Bll.Accounts.ExternalServiceLogin.ServiceTypes.EPA:
                    //history.TransHistoryType_id = (byte)CommonTypes.TransactionHistoryType.epa;
                    break;
                //case Bll.Accounts.ExternalServiceLogin.ServiceTypes.Auth:
                //case Bll.Accounts.ExternalServiceLogin.ServiceTypes.Deposit:

            }
            //history.Description = "";
            return (history.TransHistoryType_id != 0);
        }

        protected void InsertParsed(List<tblEpaPending> parsed)
        {
            Infrastructure.DataContext.Writer.tblEpaPendings.InsertAllOnSubmit(parsed);
            Infrastructure.DataContext.Writer.SubmitChanges();
        }

        protected void InsertParsed(List<tblChbPending> parsed)
        {
            Infrastructure.DataContext.Writer.tblChbPendings.InsertAllOnSubmit(parsed);
            Infrastructure.DataContext.Writer.SubmitChanges();
        }

        protected void InsertParsed(List<TransMatchPending> parsed)
        {
            Infrastructure.DataContext.Writer.TransMatchPendings.InsertAllOnSubmit(parsed);
            Infrastructure.DataContext.Writer.SubmitChanges();
        }

    }
}
