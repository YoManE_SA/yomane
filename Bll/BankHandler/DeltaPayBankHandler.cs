﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;

using Netpay.Infrastructure.Email.Pop3;
using Netpay.Infrastructure.Email.Pop3.SharpMimeTools;
using Netpay.Infrastructure.Security;
using Netpay.CommonTypes;

namespace Netpay.Bll.BankHandler
{
	public class DeltaPayBankHandler : BankHandlerBase
	{
        public override DebitCompany DebitCompanyID { get { return Infrastructure.DebitCompany.DeltaPay; } }

        public override void HandlePending(BankHandlerContext context)
		{
            if (context.File.Name.EndsWith(".xls") || context.File.Name.EndsWith(".xlsx")) parseXlsFile(context);
            else throw new Exception("Unsupported file type:" + context.File.FullName);
        }

        public void parseXlsFile(BankHandlerContext context)
		{
			int currentLine = 0;
			List<tblChbPending> chbRows = new List<tblChbPending>();
			List<tblEpaPending> epaRows = new List<tblEpaPending>();
			var fileName = context.CreateDecryptedTempFile(Manager);
			string connectionString = context.File.Name.EndsWith(".xlsx") ? "Provider=Microsoft.ACE.OLEDB.12.0; Data Source={0};Extended Properties=\"Excel 12.0;HDR=Yes\";" : "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\"{0}\";Extended Properties=\"Excel 8.0;HDR=Yes\"";

			System.Data.OleDb.OleDbConnection cn = new System.Data.OleDb.OleDbConnection(string.Format(connectionString, fileName));
			System.Data.OleDb.OleDbDataReader reader = null;
			try
			{
				cn.Open();
				var dt = cn.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, null);
				if (dt.Rows.Count == 0) {
					cn.Close();
					return;
				}
				string tableName = dt.Rows[0]["TABLE_NAME"].ToString();
				dt.Dispose();

				System.Data.OleDb.OleDbCommand cmd = new System.Data.OleDb.OleDbCommand(string.Format("Select * from [{0}]", tableName), cn);
				reader = cmd.ExecuteReader();
				while(reader.Read())
				{
					currentLine++;
					try
					{
						DateTime parsedDate;
						string rowStatus = (string) reader["Status"];
						switch(rowStatus){
						case "APPROVED":
						case "REFUNDED":
						case "CLEARED":
							tblEpaPending epaEntity = new tblEpaPending();
							epaEntity.InsertDate = DateTime.Now;
							epaEntity.StoredFileName = context.File.Name;
							if (DateTime.TryParse(reader["Date"].ToString(), out parsedDate))
								epaEntity.TransDate = parsedDate;
							epaEntity.DebitReferenceNum = reader["Transaction No"].ToString().Trim();
							epaEntity.Amount = reader["Total Charged"].ToString().ToDecimal(0);
							epaEntity.Currency = Currency.Get(reader["Currency"].ToString().Trim()).ID;
							epaEntity.ApprovalNumber = reader["Approval ID"].ToString().Trim();
							epaEntity.IsRefund = (rowStatus == "REFUNDED");
							epaEntity.Installment = 1;
							epaEntity.PayoutDate = DateTime.Now;
							epaRows.Add(epaEntity);
							break;
						case "CHARGEDBACK":
							tblChbPending chbEntity = new tblChbPending();
							chbEntity.InsertDate = DateTime.Now;
							chbEntity.StoredFileName = context.File.Name;
							chbEntity.ChargebackDate = DateTime.Now;
							chbEntity.DeadlineDate = DateTime.Now.AddDays(7);
							if (DateTime.TryParse(reader["Date"].ToString(), out parsedDate))
								chbEntity.TransDate = parsedDate;
							chbEntity.DebitReferenceNum = reader["Transaction No"].ToString().Trim();
							chbEntity.Amount = reader["Total Charged"].ToString().ToDecimal(0);
							chbEntity.Currency = Currency.Get(reader["Currency"].ToString().Trim()).ID;
							chbEntity.ApprovalNumber = reader["Approval ID"].ToString().Trim();
							chbEntity.Installment = 1;
							chbEntity.IsChargeback = true;
							chbEntity.IsPhotocopy = false;
							chbRows.Add(chbEntity);
							break;
						}
					}
					catch (Exception ex)
					{
                        context.LogRowError(ex, "DBRow:" + currentLine);
						continue;
					}
				}
			}
			catch (Exception ex)
			{
				Logger.Log(ex, string.Format("Failed to parse DeltaPay Excel file \n\rFile: {0}\n\rError: {1}", context.File.Name, ex.Message));
			}
			finally
			{
				if (reader != null) reader.Close();
				if (cn.State !=System.Data.ConnectionState.Closed ) cn.Close();
				InsertParsed(epaRows);
				InsertParsed(chbRows);
				if (System.IO.File.Exists(fileName)) System.IO.File.Delete(fileName);
			}
		}
	}
}
