﻿using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace Netpay.Bll.BankHandler
{
    class ACSAuthHandler : Iso8583BankHandlerBase
    {
        public ACSAuthHandler() : base(Infrastructure.DebitCompany.ACS, "Markoff") { }
    }
}
