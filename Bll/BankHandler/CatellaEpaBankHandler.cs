﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;

using Netpay.Infrastructure.Email.Pop3;
using Netpay.Infrastructure.Email.Pop3.SharpMimeTools;
using Netpay.Infrastructure.Security;
using Netpay.CommonTypes;

namespace Netpay.Bll.BankHandler
{
	public class CatellaEpaBankHandler : BankHandlerBase
	{
		private const int CatellaDebitCompanyID = 21;
        public override DebitCompany DebitCompanyID { get { return Infrastructure.DebitCompany.Catella; } }

		public override void FetchFile() //sftp
		{
            var bank = LoadEpaLogin(Accounts.ExternalServiceLogin.ServiceTypes.EPA, Accounts.ExternalServiceLogin.Protocols.SFTP);
            if (!bank.IsActive) return;

            string scriptFileName = Manager.MapTempFile("CatellaEapFtpScript.ftp");
            string logFile = Manager.MapTempFile("CatellaEapFtpLogFile.log");
			//create script file
			System.IO.StreamWriter sw = new System.IO.StreamWriter(scriptFileName);
            sw.WriteLine(bank.GetProtocolLoginCommand(GetPrivateKey(), GetHostKey()));
			sw.WriteLine("option batch continue");
			sw.WriteLine("option confirm off");
            sw.WriteLine("lcd " + GetPath(EpaFolder.New));
			sw.WriteLine("get -delete /in/*.DAT");
			sw.WriteLine("exit");
			sw.Close();

            var ret = bank.ExecuteFTPScript(scriptFileName, logFile);
            if (ret != 0) throw new ApplicationException("Catella SFTP failed with code = " + ret);
		}

        public override void HandlePending(BankHandlerContext context)
		{
            HandleDatPending(context);
		}

        public void HandleDatPending(BankHandlerContext context)
		{
			List<tblEpaPending> rows = new List<tblEpaPending>();
			StreamReader reader = context.DecryptedStreamReader;
			try
			{
				for (string currentLine = reader.ReadLine(); currentLine != null; currentLine = reader.ReadLine())
				{
					if (currentLine.Substring(7, 2) != "TX") continue;
					tblEpaPending entity = new tblEpaPending();
					/*
					X_TransID = Mid(sLine, 55, 12)
					TransOK = Mid(sLine, 113, 1)
					ClrType = Trim(Mid(sLine, 40, 15))
					TransDate = Mid(sLine, 68, 8)
					'Response.Write("Trans ID #" & X_TransID & "=" & TransOK & "<br>")
					oleDbData.Execute "Update tblCompanyTransPass Set CTP_Status=" & IIF(TransOK = "N", TL_PASS, TL_FAIL) & " Where ID=" & X_TransID
					If ClrType = "CHARGEBACK" Then ChbList = ChbList & TransDate & " " & X_TransID & ",<br>" & vbCrLf 'ChargeBackTrans(1, "tblCompanyTransPass.ID=" & X_TransID)
					*/
					entity.TransID = currentLine.Substring(54, 12).ToInt32();
					//entity.Amount = currentLineSplit[8].ToNullableInt() / 100;
					//entity.DebitRefCode = currentLineSplit[0];
					//entity.TerminalNumber = currentLineSplit[5];
					//entity.InsertDate = DateTime.Now;
					entity.Installment = 1;
					entity.StoredFileName = context.File.Name;
					DateTime transDate;
					if (DateTime.TryParseExact(currentLine.Substring(67, 8), "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out transDate))
						entity.TransDate = transDate;

					rows.Add(entity);
				}
			
			}
			catch { }
			finally
			{
				reader.Close();
				InsertParsed(rows);
			}
		}
	}
}
