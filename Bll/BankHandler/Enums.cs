﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.BankHandler
{
    public enum EpaFolder { New, Pending, Archive, Error }
    public enum LogAction
    {
        Error = 0,
        NewFileProcessStart = 1,
        NewFileProcessCompleted = 2,
        PendingFileProcessStart = 3,
        PendingFileProcessComplete = 4,
        Uploaded = 5,
        Sent = 6
    }

    /*
    public enum EpaDebitCompany
    {
        BnS = 1,
        VisaCal = 2,
        Isracard = 3,
        Invik = 4,
        WireCard = 5,
        CredoRax = 6,
        DeltaPay = 7,
        Catella = 8,
        Allied = 9,
        Atlas = 10,
        Shva = 11,
        FNB = 12,
        eCentric = 13,
    }
    */
}
