﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;

using Netpay.Infrastructure.Email.Pop3;
using Netpay.Infrastructure.Email.Pop3.SharpMimeTools;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.BankHandler
{
	public class BnsEpaBankHandler : BankHandlerBase
	{
		private const int _recordBuffer = 100;
        public override DebitCompany DebitCompanyID { get { return Infrastructure.DebitCompany.BnS; } }

        public override void HandleNew(BankHandlerContext context)
		{
			if (context.File.Extension.ToUpper() == ".PGP")
			{
				// decrypt & delete original
				string output = Netpay.Crypt.Pgp.DecryptFile(context.File.FullName, true);
				File.Delete(context.File.FullName);

				// replace context file 
				string newFileFullName = Path.ChangeExtension(context.File.FullName, "txt");
				context.File = new FileInfo(newFileFullName);
			}

            base.HandleNew(context);
        }

        public override void FetchFile()
		{
            //return FetchFilePOP3(manager);
            FetchFileFTP();
		}

        public void FetchFileFTP()
		{
            var bank = LoadEpaLogin(Accounts.ExternalServiceLogin.ServiceTypes.EPA, Accounts.ExternalServiceLogin.Protocols.SFTP);
            if (!bank.IsActive) return;

            string scriptFileName = Manager.MapTempFile("BnsEpaFtpScript.ftp");
            string logFile = Manager.MapTempFile("BnsEpaFtpLogFile.log");
            //create script file
            System.IO.StreamWriter sw = new System.IO.StreamWriter(scriptFileName);
            sw.WriteLine(bank.GetProtocolLoginCommand(GetPrivateKey(), GetHostKey()));
            sw.WriteLine("option batch continue");
            sw.WriteLine("option confirm off");
            sw.WriteLine("lcd " + GetPath(EpaFolder.New));
            sw.WriteLine("get -delete /from_bus/epa/*");
            sw.WriteLine("exit");
            sw.Close();

            var ret = bank.ExecuteFTPScript(scriptFileName, logFile);
            if (ret != 0) throw new ApplicationException("BnS SFTP failed with code = " + ret);
		}

        public void FetchFilePOP3()
		{
            var bank = LoadEpaLogin(Accounts.ExternalServiceLogin.ServiceTypes.EPA, "POP3");
            if (!bank.IsActive) return;

			Uri uri = new Uri(bank.ServerURL);
			Netpay.Infrastructure.Email.Pop3.Pop3Client client = new Pop3Client(bank.ServerURL, uri.Port, true, bank.Username, bank.Password);

			// get messages & save attachments
			List<int> messagesIds;
			client.GetEmailIdList(out messagesIds);
			foreach (int currentMessageID in messagesIds)
			{
				string rawMessage;
				client.GetRawEmail(currentMessageID, out rawMessage);
				SharpMessage parsedMessage = new SharpMessage(rawMessage);
                string folderPath = GetPath(EpaFolder.New);
				foreach (SharpAttachment currentAttachment in parsedMessage.Attachments)
					currentAttachment.Save(folderPath, true);
			}
		}

        public override void HandlePending(BankHandlerContext context)
		{
			string extension = context.File.Extension.ToUpper();
			if ((extension.ToUpper() != ".TXT") && (!context.File.FullName.ToUpper().Contains("PAYBYNET-EPA")))
				throw new Exception("This file [3] is not valid BNS EPA file: '" + context.File.Name + "'");

			DateTime payout = DateTime.Now;
			int dateIndex = context.File.FullName.ToUpper().LastIndexOf("EPA");
			if (dateIndex > -1)
				DateTime.TryParseExact(context.File.FullName.Substring(dateIndex + 4, 8), "yyyyMMdd", null, System.Globalization.DateTimeStyles.None, out payout);

			List<tblEpaPending> rows = new List<tblEpaPending>(_recordBuffer);
			StreamReader reader = context.DecryptedStreamReader;
            try
            {
                string sDebitRefCode;
                int lineNumber = 0;
                for (string lineString = reader.ReadLine(); lineString != null; lineString = reader.ReadLine())
                {
                    lineNumber++;
                    if (lineString.Length == 256)
                    {
                        try
                        {
                            if (lineString.StartsWith("612") || lineString.StartsWith("613"))
                            {
                                sDebitRefCode = lineString.Substring(69, 40).Trim().ToUpper();
                                if (sDebitRefCode != string.Empty && !sDebitRefCode.StartsWith("REFUND") && !sDebitRefCode.StartsWith("BALANCE"))
                                {
									Currency cVO = Currency.GetByIsoNumber(lineString.Substring(43, 4).ToInt32());
                                    if (cVO == null) throw new ArgumentOutOfRangeException("Currency", lineString.Substring(43, 4).ToInt32(), string.Format("Invalid currency {2} in BNS EPA file \n\rLine: {0}\n\rFile: {1}\n\rLine data: " + lineString, lineNumber, context.File.Name, lineString.Substring(43, 4)));
                                    tblEpaPending row = new tblEpaPending();
                                    row.StoredFileName = context.File.Name;
                                    row.InsertDate = DateTime.Now;
                                    row.DebitRefCode = sDebitRefCode;
                                    row.PayoutDate = payout;
                                    row.IsRefund = lineString.StartsWith("613");
                                    row.Amount = lineString.Substring(33, 10).ToInt32() / (decimal)100;
                                    row.ApprovalNumber = null;
                                    row.CardNumber256 = Encryption.Encrypt(lineString.Substring(7, 16));
                                    row.Currency = cVO != null ? cVO.ID : (int)Netpay.CommonTypes.Currency.Unknown;
                                    row.Installment = 1;
                                    row.TerminalNumber = null;
                                    row.TransDate = null;
                                    rows.Add(row);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            context.LogRowError(ex, lineString);//lineNumber
                            continue;
                        }
                    }
                }
            } catch { }
            finally{
                reader.Close();
                InsertParsed(rows);
            }
        }
	}
}