﻿using System;
using System.Collections.Generic;
using System.Linq;
using Netpay.Crypt;
using Netpay.Infrastructure;
using Netpay.Dal.Netpay;

namespace Netpay.Bll.BankHandler
{
    public class MatchValue
    {
        public TransMatchPending Pending;
        public int? TransID;
        public int? MerchantID;
    }

    public abstract class BankHandlerManagerBase
	{
        public LogTag LogTag { get; private set; }
        public string BasePath { get; private set; }
        public string[] ExternalServiceTypes { get; private set; }
        public List<BankHandlerBase> Handlers;

        private System.Threading.Timer _fileMonitorTimer = null;

        public BankHandlerManagerBase(LogTag logTag, string basePath, string[] externalServiceTypes) 
        {
            LogTag = logTag; BasePath = basePath;
            ExternalServiceTypes = externalServiceTypes;
            Handlers = new List<BankHandlerBase>();
		}

        private void MonitorFiles_Timer(object state)
        {
            foreach (Domain currentDomain in Domain.Domains.Values)
            {
                if (!currentDomain.EnableEpa) continue;
                Domain.Current = currentDomain;
                try {
                    ObjectContext.Current.CredentialsToken = Domain.Current.ServiceCredentials;
                    try { ProcessNew(); }
                    catch (Exception ex) { Logger.Log(LogTag, ex, "Error processing HandleNew for domain '" + currentDomain.Host + "', see more details"); }
                    try { ProcessPending(); }
                    catch (Exception ex) { Logger.Log(LogTag, ex, "Error processing HandlePending for domain '" + currentDomain.Host + "', see more details"); }
                } catch (Exception ex) {
                    Logger.Log(LogTag, ex, "Error exec MonitorFiles for domain '" + currentDomain.Host + "', see more details");
                }
            }
        }

        public bool EnableFilesMonitor
        { 
            get{ return _fileMonitorTimer != null; }
            set{
                if (value) {
                    if (_fileMonitorTimer == null) _fileMonitorTimer = new System.Threading.Timer(MonitorFiles_Timer, null, 120 * 1000, 120 * 1000);
                } else {
                    if (_fileMonitorTimer != null) _fileMonitorTimer.Dispose();
                }
            }
        }
        public abstract void Log(LogAction action, string originalFileName, string storedFileName);
        
        protected void AddHandler(BankHandlerBase handler) 
        {
            handler.Init(this);
            foreach (Domain currentDomain in Domain.Domains.Values)
            {
                if (!currentDomain.EnableEpa) continue;
                Domain.Current = currentDomain;
                handler.EnsureDirectoriesForDoamin();
            }
            Handlers.Add(handler);
        }

        public string MapTempFile(string fileName)
        {
            return System.IO.Path.Combine(Domain.Current.MapPrivateDataPath(@"Temp"), fileName);
        }

        public void FetchFilesAllDomains()
        {
			foreach (Domain currentDomain in Domain.Domains.Values)
			{
				if (!currentDomain.EnableEpa) continue;
				Domain.Current = currentDomain;
                try {
                    FetchFiles();
                } catch (Exception ex) {
                    Logger.Log(LogTag, ex, "Error processing FetchFiles for domain" + currentDomain.Host + ", see more details");
                }
			}
        }

        public void FetchFiles(Type handlerType = null)
		{
            if (!Infrastructure.Tasks.Lock.TryLock(LogTag.ToString() + ".Fetch", DateTime.Now.AddMinutes(-15))) return;
            try {
                ObjectContext.Current.CredentialsToken = Domain.Current.ServiceCredentials;
                foreach (var handler in Handlers)
                {
                    if (handlerType != null && !handler.GetType().Equals(handlerType)) continue;
                    try { handler.FetchFile(); }
                    catch (Exception ex) { Logger.Log(LogTag, ex, "Error processing FetchFiles for domain" + Domain.Current.Host + ", " + handler.ToString()); }
                }
			} finally {
                Infrastructure.Tasks.Lock.Release(LogTag.ToString() + ".Fetch");
            }
		}

        public void ProcessNew(Type handlerType = null)
        {
            if (!Infrastructure.Tasks.Lock.TryLock(LogTag.ToString() + ".New", null)) return;
            try {
                foreach (var handler in Handlers)
				{
                    if (handlerType != null && !handler.GetType().Equals(handlerType)) continue;
                    var directory = handler.GetPath(EpaFolder.New);
                    if (!System.IO.Directory.Exists(directory)) continue;
                    string[] files = System.IO.Directory.GetFiles(directory);
					foreach (string currentFile in files)
					{
						var context = new BankHandlerContext(currentFile);
                        handler.ProcessNew(context); //function not throw
					}
				}
			} finally {
                Infrastructure.Tasks.Lock.Release(LogTag.ToString() + ".New");
            }
		}

        public void ProcessPending(Type handlerType = null)
		{
            if (!Infrastructure.Tasks.Lock.TryLock(LogTag.ToString() + ".Pending", null)) return;
			try
			{
                foreach (var handler in Handlers)
				{
                    /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
                    //Logger.Log(LogTag ,"Inside ProcessPending() , handler:" + handler.ToString());

                    if (handlerType != null && !handler.GetType().Equals(handlerType)) continue;
                    var directory = handler.GetPath(EpaFolder.Pending);
                    if (!System.IO.Directory.Exists(directory)) continue;
                    string[] files = System.IO.Directory.GetFiles(directory);

                    /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
                    //Logger.Log(LogTag, "Found " + files.Length + " inside folder:" + directory);

                    foreach (string currentFile in files)
                    {
                        var context = new BankHandlerContext(currentFile);
                        handler.ProcessPending(context); //function not throw
                    }
				}
			} finally {
                Infrastructure.Tasks.Lock.Release(LogTag.ToString() + ".Pending");
            }
		}

        public void ProcessMatchAllDomains()
        {
			foreach (Domain currentDomain in Domain.Domains.Values)
			{
				if (!currentDomain.EnableEpa) continue;
				Domain.Current = currentDomain;
                try {
                    ObjectContext.Current.Impersonate(currentDomain.ServiceCredentials);
                    PrcessMatch();
                } catch (Exception ex) {
                    Logger.Log(LogTag, ex, "Error processing Match for domain" + currentDomain.Host + ", see more details");
                } finally {
                    ObjectContext.Current.StopImpersonate();
                }
            }
        }

        public virtual void PrcessMatch(Type handlerType = null)
        {
            if (!Infrastructure.Tasks.Lock.TryLock(LogTag.ToString() + ".Match", null)) return;
            try
            {
                BankHandlerBase handler = null;
                if (handlerType != null) handler = Handlers.Where(v => v.GetType().Equals(handlerType)).Single();
                int? debitComapnyID = null;
                if (handler != null) debitComapnyID = (int)handler.DebitCompanyID;
                using (var dc = new DataContext(true))
                {
                    var matches = (from p in dc.TransMatchPendings
                     join t in dc.tblCompanyTransPasses on p.DebitCompany_id equals t.DebitCompanyID
                     where
                        p.MatchDate == null &&
                        t.InsertDate > DateTime.Now.AddYears(-2) && t.CreditType > 0 &&
                        ExternalServiceTypes.Contains(p.ExternalServiceType_id) &&
                        //(debitComapnyID == null || (int)debitComapnyID.Value == p.DebitCompany_id) &&
                        (p.ApprovalNumber == null || p.ApprovalNumber == t.ApprovalNumber) &&
                        (p.DebitReferenceCode == null || p.DebitReferenceCode == t.DebitReferenceCode) &&
                        (p.DebitReferenceNum == null || p.DebitReferenceNum == t.DebitReferenceNum) &&
                        (p.AcquirerReferenceNum == null || p.AcquirerReferenceNum == t.AcquirerReferenceNum)
                     //((p.Amount == null && p.CurrencyISOCode == null && p.CardNumber != null) || (p.Amount == t.Amount && p.CurrencyISOCode == t.Currency))
                     select new MatchValue() { Pending = p, TransID = t.ID, MerchantID = t.companyID }).ToList();

                    var matchCounts = matches.GroupBy(v => v.Pending.TransMatchPending_id).ToDictionary(v => v.Key, v => v.Count());
                    foreach (var v in matches)
                    {
                        if (v.Pending.MatchDate == null) v.Pending.MatchDate = DateTime.Now;
                        if (v.Pending.MatchCount == null) v.Pending.MatchCount = (byte?) matchCounts[v.Pending.TransMatchPending_id];
                        if (v.Pending.MatchCount.GetValueOrDefault() > 1) continue;
                        handler = Handlers.Where(h => (int)h.DebitCompanyID == v.Pending.DebitCompany_id.Value).SingleOrDefault();
                        if (handler == null) Logger.Log(LogTag, "PendingMatch was unable to find handler for debitCompany:" + v.Pending.DebitCompany_id);
                        else handler.ProcessMatch(dc, v);
                    }
                    dc.SubmitChanges();
                    dc.ExecuteCommand("Update Data.TransMatchPending Set MatchDate=getDate(), MatchCount = 0 Where MatchDate is Null");
                }
            } finally {
                Infrastructure.Tasks.Lock.Release(LogTag.ToString() + ".Match");
            }
        }

        public virtual void Process(Type handlerType = null)
        {
            

            FetchFiles(handlerType);
            ProcessNew(handlerType);
            ProcessPending(handlerType);
            PrcessMatch(handlerType);
        }
	}
}
