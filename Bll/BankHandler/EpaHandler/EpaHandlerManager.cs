﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Netpay.Dal.DataAccess;
using Netpay.Crypt;
using System.Threading;
using Netpay.Infrastructure;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.BankHandler
{
    public class EpaHandlerManager : BankHandlerManagerBase
    {
        public const string SecuredObjectName = "EpaManage";

        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(BankHandler.EpaHandler.Module.Current, SecuredObjectName); } }

        private static EpaHandlerManager _current = null;
        public static EpaHandlerManager Current { get { if (_current == null) _current = new EpaHandlerManager(); return _current; } }

        private EpaHandlerManager() : base(LogTag.Epa, @"EPA", new string[] { Bll.Accounts.ExternalServiceLogin.ServiceTypes.EPA }) 
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            AddHandler(new BnsEpaBankHandler());
            AddHandler(new InvikBankHandler());
            AddHandler(new CalBankHandler());
			AddHandler(new IsracardBankHandler());
            AddHandler(new DeltaPayBankHandler());
            AddHandler(new CatellaEpaBankHandler());
            AddHandler(new AlliedEpaBankHandler());
        }

        public static void ExecFetch() { Current.FetchFilesAllDomains(); }
        public static void ExecMatch() { Current.ProcessMatchAllDomains(); }

        public override void Log(LogAction action, string originalFileName, string storedFileName)
        {
            NetpayDataContext dc = new NetpayDataContext(Domain.Current.Sql1ConnectionString);
            tblEpaFileLog entity = new tblEpaFileLog();
            entity.ActionDate = DateTime.Now;
            entity.ActionType = (int)action;
            entity.OriginalFileName = Path.GetFileName(originalFileName);
            entity.StoredFileName = Path.GetFileName(storedFileName);
            dc.tblEpaFileLogs.InsertOnSubmit(entity);
            dc.SubmitChanges();
        }


    }
}
