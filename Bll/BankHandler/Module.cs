﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.Bll.BankHandler
{
    public class Module : Infrastructure.Module
    {
        public const string ModuleName = "BankHandler";
        public override string Name { get { return ModuleName; } }
        public override decimal Version { get { return 1.0m; } }
        public static Module Current { get { return Infrastructure.Module.Get(ModuleName) as Module; } }

        protected override void OnInstall(EventArgs e)
        {
            Infrastructure.Security.SecuredObject.Create(this, AuthHandlerManager.SecuredObjectName, Infrastructure.Security.PermissionGroup.ReadEditDelete);
            Infrastructure.Security.SecuredObject.Create(this, ChbHandlerManager.SecuredObjectName, Infrastructure.Security.PermissionGroup.ReadEditDelete);
            Infrastructure.Security.SecuredObject.Create(this, eCentricAuthHandler.SecuredObjectName, Infrastructure.Security.PermissionGroup.ReadEditDelete);
            base.OnInstall(e);
        }

        protected override void OnUninstall(EventArgs e)
        {
            Infrastructure.Security.SecuredObject.Remove(this);
            base.OnUninstall(e);
        }

    }
}
