﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;


namespace Netpay.Bll.BankHandler
{
	public class CatellaAuthHandler : BankHandlerBase
	{
		private const int CatellaDebitCompanyID = 21;
        public override DebitCompany DebitCompanyID { get { return Infrastructure.DebitCompany.Catella; } }
        private class BatchCounts
		{
			public int recordIndex;
			public int beatchHeader;

			public int totalTICount;

			public int totalTCCount;
			public int totalTDCount;

			public int totalTCSum;
			public int totalTDSum;
		}

		public override void GenerateFile()
		{
			var counts = new BatchCounts();
            var AuthRow = new Process.Batch(CatellaDebitCompanyID, null);
            AuthRow.SetInternalID(null);
            AuthRow.MarkTransationsForBatch();
            var transactions = AuthRow.GetTransactions().GroupBy(t => new { t.TerminalNumber, t.CurrencyID }).OrderBy(t => new { t.Key.TerminalNumber, t.Key.CurrencyID }).ToList();
			if (transactions.Count() == 0) return;

            string fileName = GetFilePath(EpaFolder.New, "Auth_" + DateTime.Now.ToString("yyyyMMdd") + ".csv");
            using (var sw = new System.IO.StreamWriter(fileName))
            {
                //header
                counts.recordIndex++;
                sw.Write(counts.recordIndex.ToString("000000"));
                sw.Write("FH ");
                sw.Write(DateTime.Now.ToUniversalTime().ToString("yyyyMMdd"));
                sw.Write(AuthRow.InternalKey.ToString("000000"));
                sw.Write(AuthRow.InternalKey.ToString("000000"));
                sw.Write(" ");
                sw.Write(Domain.Current.BrandName.PadRight(20, ' '));
                sw.Write("BANQUE INVIK".PadRight(20, ' '));
                sw.Write("".PadRight(15, ' ')); //accountSubId
                sw.Write("000000000000037");
                sw.Write(" ");
                sw.Write("NPA");
                sw.Write("BI ");
                sw.WriteLine(new string(' ', 40));

                foreach (var g in transactions)
                {
                    var terminal = DebitCompanies.Terminal.Load(CatellaDebitCompanyID, g.Key.TerminalNumber);
                    AddTransaction(terminal, Currency.Get(g.Key.CurrencyID), g.ToList(), counts, sw);
                }
                //footer
                counts.recordIndex++;
                sw.Write(counts.recordIndex.ToString("000000"));
                sw.Write("FT ");
                sw.Write(counts.beatchHeader.ToString("000000"));
                sw.Write(counts.recordIndex.ToString("000000"));
                sw.Write(counts.totalTICount.ToString("000000"));
                sw.Write(counts.totalTDCount.ToString("000000"));
                sw.Write(counts.totalTDSum.ToString("0000000000000000"));
                sw.WriteLine(" ");
                sw.Write(counts.totalTCCount.ToString("000000"));
                sw.Write(counts.totalTCSum.ToString("0000000000000000"));
                sw.WriteLine(new string(' ', 75));
                sw.Close();
            }
            AuthRow.SetPrepared(fileName, counts.totalTDCount + counts.totalTCCount, counts.totalTDSum, counts.totalTCSum);
		}

		private void AddTransaction(DebitCompanies.Terminal terminal, Currency currency, List<Transactions.Transaction> transactions, BatchCounts counts, System.IO.StreamWriter sw)
		{
			int transCIndex = 0, transDIndex = 0, transIndex = 0;
			int transCAmount = 0, transDAmount = 0;
			string password, password3d;
			terminal.GetDecryptedData(out password, out password3d);

			//header
			counts.beatchHeader++;
			counts.recordIndex++;
			sw.Write(counts.recordIndex.ToString("000000"));
			sw.Write("BH ");
			sw.Write(counts.beatchHeader.ToString("000000"));
			sw.Write(DateTime.Now.ToUniversalTime().ToString("yyyyMMdd"));
			sw.Write("XX");
			sw.Write(currency.IsoCode);
			sw.Write(" ");
			sw.Write(terminal.AccssesName1.EmptyIfNull().PadRight(15, ' '));
			sw.Write(terminal.Name.EmptyIfNull().PadRight(25, ' '));
			sw.Write(password.EmptyIfNull().PadRight(6, ' '));
			sw.WriteLine(new string(' ', 73));

			//transactions
			foreach (var t in transactions)
			{
				var merchant = t.Merchant;
				bool isCredit;
				string descriptor = terminal.Name + " " + merchant.Descriptor;
				int trnAmount = (int)(t.Amount * 100);

				counts.recordIndex++;
				sw.Write(counts.recordIndex.ToString("000000"));
				if (t.CreditType == Infrastructure.CreditType.Refund || t.CreditType == Infrastructure.CreditType.Credit  || t.CreditType == Infrastructure.CreditType.CreditCharge) {
					sw.Write("TC ");
					transCIndex++;
					isCredit = true;
				} else {
					sw.Write("TD ");
					transDIndex++;
					isCredit = false;
				}
				var pmData = t.PaymentData.MethodInstance;
				var address = t.PaymentData.BillingAddress;
				string accountValue1, accountValue2;
				pmData.GetDecryptedData(out accountValue1, out accountValue2);
				sw.Write(accountValue1.Replace(" ", "").PadRight(19, ' '));
				sw.Write("{0}{1}", pmData.ExpirationDate.Value.Year.ToString("00"), pmData.ExpirationDate.Value.Month.ToString("00"));
				sw.Write(t.ID.ToString().PadLeft(12, '0'));
				sw.Write(isCredit ? "-" : "+");
				sw.Write(trnAmount.ToString().PadLeft(12, '0'));
				sw.Write(t.ApprovalNumber.PadLeft(6, '0'));
				sw.Write(new string('0', 12));
				sw.Write(new string('0', 12));
				sw.Write("00");
				sw.Write("000000");
				sw.Write("      ");
				sw.Write("1");
				sw.WriteLine(new string(' ', 33));

				counts.recordIndex++;
				sw.Write(counts.recordIndex.ToString("000000"));
				sw.Write("TI ");
				sw.Write(accountValue1.Replace(" ", "").PadRight(19, ' '));
				sw.Write("0022");
				sw.Write(" ");
				sw.Write(address.City.EmptyIfNull().PadRight(20, ' '));
				sw.Write(terminal.Name.EmptyIfNull().PadRight(20, ' '));
				sw.Write(terminal.AccssesName1.EmptyIfNull().PadRight(12, ' '));
				sw.Write(t.DebitReferenceCode.EmptyIfNull().PadRight(12, ' '));
				sw.Write(descriptor.Truncate(15).PadRight(15, ' '));
				sw.Write("012");
				sw.Write("59");
				sw.Write("20351000060004420000000000");
				sw.Write("00");
				sw.WriteLine(new string(' ', 3));
			}
			//footer
			counts.recordIndex++;
			sw.Write(counts.recordIndex.ToString("000000"));
			sw.Write("BT ");

			sw.Write(counts.beatchHeader.ToString().PadLeft(6, '0'));
			sw.Write(transIndex.ToString().PadLeft(6, '0'));

			sw.Write(transDIndex.ToString().PadLeft(6, '0'));
			sw.Write(transCAmount.ToString().PadLeft(16, '0'));
			
			sw.Write(transCIndex.ToString().PadLeft(6, ' '));
			sw.Write(transDAmount.ToString().PadLeft(16, '0'));
			sw.WriteLine(new string(' ', 81));

			counts.totalTICount += transIndex;
			counts.totalTDCount += transDIndex;
			counts.totalTDSum += transDAmount;
			counts.totalTCCount += transCIndex;
			counts.totalTCSum += transCAmount;
		}

		public override void HandlePending(BankHandlerContext context)
		{
            var bank = base.LoadEpaLogin(Accounts.ExternalServiceLogin.ServiceTypes.Auth, Accounts.ExternalServiceLogin.Protocols.SFTP);
            if (!bank.IsActive) return;

            string scriptFileName = Manager.MapTempFile("CatellaBatchFtpScript.ftp");
			string logFile = Manager.MapTempFile("CatellaBatchFtpLogFile.log");

            var tempFileName = context.CreateDecryptedTempFile(Manager);
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(scriptFileName))
            {
                sw.WriteLine(bank.GetProtocolLoginCommand(GetPrivateKey(), GetHostKey()));
                sw.WriteLine("option batch continue");
			    sw.WriteLine("option confirm off");
                sw.WriteLine("put " + tempFileName + " -delete");
                sw.WriteLine("exit");
			    sw.Close();
            }
            var ret = bank.ExecuteFTPScript(scriptFileName, logFile);
            if (System.IO.File.Exists(tempFileName)) System.IO.File.Delete(tempFileName);

            var batch = Process.Batch.Load(CatellaDebitCompanyID, context.File.Name);
            if (ret != 0) {
                if (batch != null) batch.SetFailSent("SFTP fail code:" + ret);
                throw new ApplicationException("Catella SFTP failed with code = " + ret);
            }
            if (batch != null) batch.SetSent();
		}

	}
}
