﻿namespace Netpay.Bll.BankHandler
{
	public enum EpaDebitCompany
	{
		BnS = 1,
		VisaCal = 2,
		Isracard = 3,
		Invik = 4,
		WireCard = 5,
        CredoRax = 6,
		DeltaPay = 7,
		Catella = 8,
		Allied = 9,
		Atlas = 10,
		Shva = 11,
        FNB = 12,
        eCentric = 13,
    }
}
