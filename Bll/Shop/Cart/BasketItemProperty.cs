﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll.Shop.Cart
{
	public class BasketItemProperty : BaseDataObject
	{
		private Dal.Netpay.CartProductProperty _entity;
		public int ID { get { return _entity.CartProductProperty_id; } }
		public int BasketItemID { get { return _entity.CartProduct_id; } }
		public int? PropertyID { get { return _entity.ProductProperty_id; } }
		public string Name { get { return _entity.PropertyName; } }
		public string Value { get { return _entity.PropertyValue; } set { _entity.PropertyValue = value; } }
		public BasketItem BasketItem { get; internal set; }

		internal BasketItemProperty(Dal.Netpay.CartProductProperty entity, BasketItem basketItem)
			
		{ 
			_entity = entity;
			BasketItem = basketItem;
		}

		public BasketItemProperty(Products.Property propertyValue)
			
		{
			_entity = new Dal.Netpay.CartProductProperty();
			_entity.ProductProperty_id = propertyValue.ID;
			if (propertyValue.Parent != null) _entity.PropertyName = propertyValue.Parent.Name;
			_entity.PropertyValue = propertyValue.Name;
		}

		public static List<BasketItemProperty> Load(List<BasketItem> basketItems)
		{
			return (from bip in DataContext.Reader.CartProductProperties where basketItems.Select(bi => bi.ID).Contains(bip.CartProduct_id) select bip).AsEnumerable().Select((bip) => 
				new BasketItemProperty(bip, basketItems.Where(bi => bi.ID == bip.CartProduct_id).SingleOrDefault())).ToList();
		}

		internal void Save()
		{
			_entity.CartProduct_id = BasketItem.ID;
			DataContext.Writer.CartProductProperties.Update(_entity, (_entity.CartProductProperty_id != 0));
			DataContext.Writer.SubmitChanges();
		}


	}
}
