﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Shop.Cart
{
	public class Basket : BaseDataObject
	{
        public class BasketChanges
        {
            private bool _isChanged = false;
            private decimal _total = 0;

            internal BasketChanges(Dal.Netpay.Cart basket, List<BasketItem> basketItems) 
            {
                foreach (BasketItem item in basketItems)
                {
                    if (item.Changes.IsChanged) 
                        _isChanged = true;
                    
                    decimal convRate = Currency.ConvertRate(item.Changes.CurrencyIsoCode, basket.CurrencyISOCode);
                    _total += (item.Changes.Total * convRate);
                }
            }

            public bool IsChanged { get  {  return _isChanged;  } }
            public decimal Total { get { return _total; } }
        }
        
        private Dal.Netpay.Cart _entity;

		public int ID { get { return _entity.Cart_id; } }
        public int? ShopId { get { return _entity.MerchantSetShop_id; } set { _entity.MerchantSetShop_id = value; } }
        public int MerchantID { get { return _entity.Merchant_id; } }
		public int? CustomerID { get { return _entity.Customer_id; } }
		public string MerchantReference { get { return _entity.ReferenceNumber; } }
		public System.DateTime StartDate { get { return _entity.StartDate; } }

		public decimal TotalProducts { get { return _entity.TotalProducts; } }
		public decimal TotalShipping { get { return _entity.TotalShipping; } }
		public decimal Total { get { return _entity.Total.GetValueOrDefault(); } }
		public string CurrencyISOCode { get { return _entity.CurrencyISOCode; } set { if(_entity.CurrencyISOCode == value) return; _entity.CurrencyISOCode = value; UpdateTotals(); } }
        public byte Installments { get { return _entity.Installments; } set { _entity.Installments = value; } }
 
        private byte? maxInst;
        public byte MaxInstallments
        {
            get
            {
                if (maxInst == null)
                {
                    maxInst = 1;
                    if (ShopId.HasValue)
                    {
                        RegionShop shop = RegionShop.Load(ShopId.Value);
                        if (shop != null)
                        {
                            List<Netpay.Bll.Shop.RegionShop.InstallmentStep> steps = shop.InstallmentSteps;
                            if (steps != null && steps.Count > 0)
                            {
                                foreach (var step in steps)
                                {
                                    if (Total > step.CalculatedFromAmount)
                                        maxInst = step.MaxInstallments;
                                }
                            }   
                        }
                    }
                }

                return maxInst.Value;
            }
        }
        
        public string RecepientName { get { return _entity.RecepientName; } set { _entity.RecepientName = value; } }
		public string RecepientPhone { get { return _entity.RecepientPhone; } set { _entity.RecepientPhone = value; } }
		public string RecepientMail { get { return _entity.RecepientMail; } set { _entity.RecepientMail = value; } }
		public string Comment { get { return _entity.Comment; } set { _entity.Comment = value; } }
		public Guid Identifier { get { return _entity.Identifier; } }
		
		public int? TransPass_id { get { return _entity.TransPass_id; } }
		public int? TransPreAuth_id { get { return _entity.TransPreAuth_id; } }
		public int? TransPending_id { get { return _entity.TransPending_id; } }
		public System.DateTime? CheckoutDate { get { return _entity.CheckoutDate; } }

		private List<BasketItem> _basketItems;
		public List<BasketItem> BasketItems
		{
			get { 
				if (_basketItems == null){
					(_basketItems = BasketItem.Search(new BasketItem.SearchFilters() { BasketId = new List<int>() { ID } }, null)).ForEach(f => f.Basket = this);
					var products = Products.Product.Load(_basketItems.Where(bi => bi.ProductId != null).Select(bi => bi.ProductId.Value).Distinct().ToList(), null);
					_basketItems.Where(bi => bi.ProductId != null).ToList().ForEach(bi => bi.Product = products.Where(p => p.ID == bi.ProductId.Value).SingleOrDefault());
				}
				return _basketItems; 
			}
		}

        private BasketChanges _changes;
        public BasketChanges Changes
        {
            get
            {
                if (_changes != null)
                    return _changes;
                _changes = new BasketChanges(_entity, BasketItems);
                return _changes;
            }
        }

		private Merchants.MerchantPublicInfo _merchantInfo;
		public Merchants.MerchantPublicInfo MerchantInfo
		{
			get{
				if (_merchantInfo != null) return _merchantInfo;
				_merchantInfo = Merchants.MerchantPublicInfo.Load(MerchantID);
				return _merchantInfo;
			}
		}

		private Basket(Dal.Netpay.Cart entity)
		{ 
			_entity = entity;
		}

		public Basket(int merchantId, string merchantReference, int? customerId)  
		{
			_basketItems = new List<BasketItem>();
			_entity = new Dal.Netpay.Cart();
			_entity.Identifier = Guid.NewGuid();
			_entity.StartDate = DateTime.Now;
			_entity.Merchant_id = merchantId;
			_entity.ReferenceNumber = merchantReference;
			_entity.Customer_id = customerId;
		}

		public static Basket Load(int id)
		{
			//context.IsUserOfType(new UserRole[] { UserRole.NetpayUser, UserRole.NetpayAdmin });
			return (from p in DataContext.Reader.Carts where p.Cart_id == id select new Basket(p)).SingleOrDefault();
		}

		public static Basket Load(Guid identifier)
		{
			//context.IsUserOfType(new UserType[] { UserType.NetpayUser, UserType.NetpayAdmin });
			return (from p in DataContext.Reader.Carts where p.Identifier == identifier select new Basket(p)).SingleOrDefault();
		}

        public static Basket LoadActiveForCustomer(int customerId, int merchantId)
        {
			return (from p in DataContext.Reader.Carts
					where (p.Customer_id == customerId) && (p.Merchant_id == merchantId) && (p.CheckoutDate == null) &&
					(p.TransPass_id == null && p.TransPending_id == null && p.TransPreAuth_id == null)
				select new Basket(p)).FirstOrDefault();
        }

		public static List<Basket> LoadActiveForCustomer(int customerId)
        {
			return (from p in DataContext.Reader.Carts where p.Customer_id == customerId && p.CheckoutDate == null && 
				(p.TransPass_id == null && p.TransPending_id == null && p.TransPreAuth_id == null) 
				select new Basket(p)).ToList();
        }

		public static Basket LoadForTransaction(TransactionStatus status, int transId)
		{
			var exp = from p in DataContext.Reader.Carts select p;
			switch(status){
			case TransactionStatus.Captured: exp = exp.Where(p => p.TransPass_id == transId); break;
			case TransactionStatus.Authorized: exp = exp.Where(p => p.TransPreAuth_id == transId); break;
			case TransactionStatus.Pending: exp = exp.Where(p => p.TransPending_id == transId); break;
			default: return null; //throw new Exception("Not Supported");
			}
			return exp.Select(p => new Basket(p)).SingleOrDefault();
		}

		public void Save()
		{
			UpdateTotals();
			DataContext.Writer.Carts.Update(_entity, (_entity.Cart_id != 0));
			DataContext.Writer.SubmitChanges();
			foreach (var item in BasketItems)
				item.Save();
		}

		public void AddItem(BasketItem item)
		{
			item.Basket = this;
			BasketItems.Add(item);
			UpdateTotals();
		}

		public void RemoveItem(BasketItem item)
		{
			_basketItems.Remove(item);
			UpdateTotals();
		}

		public void RemoveAll()
		{
			_basketItems.Clear();
			UpdateTotals();
		}

		private void UpdateTotals()
		{
			_entity.TotalProducts = 0; 
			_entity.TotalShipping = 0; 
			_entity.Total = 0;
			foreach (BasketItem i in BasketItems) {
				decimal convRate = Currency.ConvertRate(i.CurrencyISOCode, _entity.CurrencyISOCode);
				i.CurrencyFXRate = convRate;
				_entity.TotalProducts += (convRate * i.TotalProduct);
				_entity.TotalShipping = (convRate * i.TotalShipping);
				_entity.Total += (convRate * i.Total);
				//_entity.TotalI++;	
			}
			//DataContext.Writer.SubmitChanges();
		}

		public void LockStock()
		{

		}

		public void CommitStock()
		{

		}

		public void CancelStock()
		{
		
		}
	}
}
