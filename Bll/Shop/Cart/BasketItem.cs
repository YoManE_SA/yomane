﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll.Shop.Cart
{
	public class BasketItem : BaseDataObject
	{
		public class SearchFilters
		{
			public Infrastructure.Range<int?> ID;
			public Infrastructure.Range<int?> Quantity;
			public Infrastructure.Range<decimal?> Price;
			public List<Products.ProductType> ProductType;
			public List<int> MerchantId;
			public List<int> ProductId;
			public List<int> ProductStockId;
			public List<int> BasketId;
			public List<int> CustomerId;
		}

        public class ItemChanges
        {
            internal ItemChanges(Dal.Netpay.CartProduct entity, Products.Product product) 
            {
                _entity = entity;
                _product = product;
            }

            private Dal.Netpay.CartProduct _entity;
            private Products.Product _product;

            public short Quantity { get { return IsAvailable ? _entity.Quantity : (short)0; } }
            public decimal Price { get { return _product.Price; } }
            public string CurrencyIsoCode { get { return _product.CurrencyISOCode; } }
            public Products.Product Product { get { return _product; }  }

            public decimal Total 
            { 
                get 
                {
                    decimal totalShipping = _entity.ShippingFee * Quantity;
                    decimal totalProduct = Price * Quantity;
                    return totalShipping + totalProduct;
                } 
            }
            public bool IsAvailable
            {
                get
                {
                    if (!_product.IsActive)
                        return false;

                    if (_product.Type == Products.ProductType.Virtual)
                        return true;

                    if (_product.QtyAvailable == null) // unlimited
                        return true;

                    return _product.QtyAvailable >= _entity.Quantity;
                }
            }
            public bool IsChanged
            {
                get
                {
                    return (!IsAvailable || (Price != _entity.Price && !_product.IsDynamicProduct) || CurrencyIsoCode != _entity.CurrencyISOCode);
                }
            }
        }

		private Dal.Netpay.CartProduct _entity;

		public int ID { get { return _entity.CartProduct_id; } }
		public int BasketId { get { return _entity.Cart_id; } }
		public int MerchantId { get { return _entity.Merchant_id; } }
		public int? ProductId { get { return _entity.Product_id; } }
		public int? ProductStockId { get { return _entity.ProductStock_id; } }

		public System.DateTime InsertDate { get { return _entity.InsertDate; } }
		public string Name { get { return _entity.Name; } }
		public Products.ProductType Type { get { return (Products.ProductType) _entity.ProductType_id; } }
		public short Quantity { get { return _entity.Quantity; } set { _entity.Quantity = value; } }
        public decimal Price { get { return _entity.Price; } set { if (IsDynamicPrice) _entity.Price = value; } }
		public string CurrencyISOCode { get { return _entity.CurrencyISOCode; } }
		public decimal CurrencyFXRate { get { return _entity.CurrencyFXRate; } internal set { _entity.CurrencyFXRate = value; } }
		public decimal ShippingFee { get { return _entity.ShippingFee; } }
		public decimal VATPercent { get { return _entity.VATPercent; } }
        
		public decimal TotalShipping { get { return ShippingFee * Quantity; } }
		public decimal TotalProduct { get { return Price * Quantity; } }
		public decimal Total { get { return TotalShipping  + TotalProduct; } }

		public System.Collections.Specialized.NameValueCollection UISetting { get; private set; }
        public int? MinQuantity { get { return UISetting["MinQuantity"].ToNullableInt(); } set { UISetting["MinQuantity"] = value != null ? value.ToString() : null; } }
        public int? MaxQuantity { get { return UISetting["MaxQuantity"].ToNullableInt(); } set { UISetting["MaxQuantity"] = value != null ? value.ToString() : null; } }
        public int? StepQuantity { get { return UISetting["StepQuantity"].ToNullableInt(); } set { UISetting["StepQuantity"] = value != null ? value.ToString() : null; } }
        public bool IsDynamicPrice { get { return UISetting["IsDynamicPrice"].ToNullableBool().GetValueOrDefault(); } set { UISetting["IsDynamicPrice"] = value.ToString(); } }

		private Basket _basket;
		public Basket Basket{
			internal set{ _basket = value; }
			get{
				if (_basket == null && _entity.Cart_id != 0) _basket = Cart.Basket.Load(_entity.Cart_id);
				return _basket;
			}
		}

		private Products.Product _product;
		public Products.Product Product
		{
			internal set { _product = value; }
			get
			{
				if (_product != null) return _product;
				if (_entity.Product_id != null) _product = Products.Product.Load(_entity.Product_id.Value);
				return _product;
			}
		}

        private ItemChanges _changes;
        public ItemChanges Changes
        {
            get
            {
                if (_changes != null)
                    return _changes;
                _changes = new ItemChanges(_entity, Product);
                return _changes;
            }
        }

		private List<BasketItemProperty> _itemProperties;
		public List<BasketItemProperty> ItemProperties
		{
			get
			{
				if (_itemProperties != null) return _itemProperties;
				_itemProperties = BasketItemProperty.Load(new List<BasketItem> { this });
				return _itemProperties;
			}
		}

		public string GuestDownloadKey { get{ return string.Format("{0}|{1}", ID, (ID + InsertDate.ToString()).ToSha256()); } }
		public string GuestDownloadLink 
		{ 
			get { 
				if (Type != Bll.Shop.Products.ProductType.Download) return null;
				if (Product == null || string.IsNullOrEmpty(Product.MediaFileName)) return null; 
				return string.Format("{0}/Shop.asmx/DownloadUnauthorized?fileKey={1}&asPlainData=true", Domain.Current.WebServicesUrl, GuestDownloadKey.ToEncodedUrl()); 
			} 
		}

		internal BasketItem(Dal.Netpay.CartProduct entity, Basket basket)
		{ 
			_entity = entity;
			Basket = basket;
			UISetting = System.Web.HttpUtility.ParseQueryString(_entity.UISetting.EmptyIfNull());
		}

		public BasketItem(int merchantId, int productId, int? productStockId, string languageCode)
		{
			UISetting = new System.Collections.Specialized.NameValueCollection();
			_entity = new Dal.Netpay.CartProduct();
			_entity.Merchant_id = merchantId;
			_entity.Product_id = productId;
			_entity.ProductStock_id = productStockId;
			_product = Products.Product.Load(productId);
			var pt = _product.GetTextForLanguage(languageCode);
			_entity.Name = (pt != null ? pt.Name : "Untitled");
			_entity.ProductType_id = (byte)_product.Type;
			_entity.Quantity = 1;
			_entity.Price = _product.Price;
			_entity.CurrencyISOCode = _product.CurrencyISOCode;
			MinQuantity = _product.QtyStart;
			MaxQuantity = _product.QtyEnd;
			StepQuantity = _product.QtyStep;
			IsDynamicPrice = _product.IsDynamicProduct;
			if (ProductStockId.HasValue) {
				var stock = Products.Stock.Load(ProductStockId.Value);
				if (stock == null) throw new Exception(string.Format("stock #{0} not exist", productStockId));
				if (stock.Properties != null) 
					_itemProperties = stock.Properties.Select(p => new BasketItemProperty(p) { BasketItem = this }).ToList();
			}
		}

		public BasketItem(int merchantId, string name, decimal price, string currencyIso, int? qntMin, int? qntMax, int? qntStep)
			
		{
			UISetting = new System.Collections.Specialized.NameValueCollection();
			_entity = new Dal.Netpay.CartProduct();
			_entity.Merchant_id = merchantId;
			_entity.Name = name;
			_entity.Quantity = 1;
			_entity.Price = price;
			_entity.CurrencyISOCode = currencyIso;
			if (qntMin != null) UISetting.Add("MinQuantity", qntMin.ToString());
			if (qntMax != null) UISetting.Add("MaxQuantity", qntMax.ToString());
			if (qntStep != null) UISetting.Add("StepQuantity", qntStep.ToString());
		}

		internal void Save()
		{
			_entity.Cart_id = Basket.ID;
			DataContext.Writer.CartProducts.Update(_entity, (_entity.CartProduct_id != 0));
			DataContext.Writer.SubmitChanges();
			if (_itemProperties != null) {
				DataContext.Writer.CartProductProperties.DeleteAllOnSubmit(DataContext.Writer.CartProductProperties.Where(c => c.CartProduct_id == _entity.CartProduct_id && !_itemProperties.Select(p=> p.ID).Contains(c.CartProductProperty_id)));
				DataContext.Writer.SubmitChanges();
				foreach (var ip in _itemProperties)
					ip.Save();
			}
		}

		public void Remove()
		{
			Basket.RemoveItem(this);
		}

		public void Delete()
		{
			if (_entity.CartProduct_id == 0) return;
			DataContext.Writer.CartProductProperties.DeleteAllOnSubmit(DataContext.Writer.CartProductProperties.Where(c=>c.CartProduct_id == _entity.CartProduct_id));
			DataContext.Writer.CartProducts.Delete(_entity);
			DataContext.Writer.SubmitChanges();
		}

		public static List<BasketItem> Search(SearchFilters filters, ISortAndPage sortAndPage)
		{
			if (filters == null) filters = new SearchFilters();
            if (Accounts.AccountFilter.Current != null) {
                filters.MerchantId = Accounts.AccountFilter.Current.FilterIDList(Accounts.AccountType.Merchant, filters.MerchantId);
                filters.CustomerId = Accounts.AccountFilter.Current.FilterIDList(Accounts.AccountType.Customer, filters.CustomerId);
            } else if (filters.BasketId == null) throw new MethodAccessException();
            var exp = (from bi in DataContext.Reader.CartProducts select new { bi });
			if (filters != null) {
				if (filters.ID.From.HasValue) exp = exp.Where(x => x.bi.CartProduct_id >= filters.ID.From);
				if (filters.ID.To.HasValue) exp = exp.Where(x => x.bi.CartProduct_id <= filters.ID.To);
				if (filters.Quantity.From.HasValue) exp = exp.Where(x => x.bi.Quantity >= filters.Quantity.From);
				if (filters.Quantity.To.HasValue) exp = exp.Where(x => x.bi.Quantity <= filters.Quantity.To);
				if (filters.Price.From.HasValue) exp = exp.Where(x => x.bi.Price >= filters.Price.From);
				if (filters.Price.To.HasValue) exp = exp.Where(x => x.bi.Price <= filters.Price.To);
				if (filters.ProductType != null) exp = exp.Where(x => filters.ProductType.Contains((Products.ProductType) x.bi.ProductType_id));
				if (filters.MerchantId != null) exp = exp.Where(x => filters.MerchantId.Contains(x.bi.Merchant_id));
				if (filters.ProductId != null) exp = exp.Where(x => filters.ProductStockId.Contains(x.bi.Product_id.GetValueOrDefault()));
				if (filters.ProductStockId != null) exp = exp.Where(x => filters.ProductStockId.Contains(x.bi.ProductStock_id.GetValueOrDefault()));
				if (filters.BasketId != null) exp = exp.Where(x => filters.BasketId.Contains(x.bi.Cart_id));
				if (filters.CustomerId != null) exp = exp.Where(x => filters.CustomerId.Contains(x.bi.data_Cart.Customer_id.GetValueOrDefault()));
			}
			var ret = exp.ApplySortAndPage(sortAndPage).Select(x => new BasketItem(x.bi, null)).ToList();
			var bip = BasketItemProperty.Load(ret);
			foreach (var bi in ret)
				bi._itemProperties = bip.Where(ip => ip.BasketItemID == bi.ID).ToList();
			return ret;
		}
	}
}
