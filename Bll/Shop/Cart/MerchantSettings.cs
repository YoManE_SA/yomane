﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using Netpay.Bll.Accounts;

namespace Netpay.Bll.Shop.Cart
{
	public class MerchantSettings : BaseDataObject
	{		
		public static Infrastructure.Security.SecuredObject SecuredObject { get { return Shop.MerchantSettings.SecuredObject; } }

		private bool _isNew; 
		private Dal.Netpay.SetMerchantCart _entity;
		public int Merchant_id { get { return _entity.Merchant_id; } set { _entity.Merchant_id = value; } }
		public bool IsEnabled { get { return _entity.IsEnabled; } set { _entity.IsEnabled = value; } }
		public bool IsKeepCart { get { return _entity.IsAllowDynamicProduct; } set { _entity.IsAllowDynamicProduct = value; } }
		public bool IsAllowPreAuth { get { return _entity.IsAllowPreAuth; } set { _entity.IsAllowPreAuth = value; } }
		public bool IsAllowDynamicProduct { get { return _entity.IsAllowDynamicProduct; } set { _entity.IsAllowDynamicProduct = value; } }

		private MerchantSettings(Netpay.Dal.Netpay.SetMerchantCart entity)
		{ 
			_entity = entity;
			_isNew = false;
		}

		public MerchantSettings(int merchantId)
		{
			_entity = new Dal.Netpay.SetMerchantCart();
			_entity.Merchant_id = merchantId;
			_isNew = true;
		}

		public void Save()
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);
			DataContext.Writer.SetMerchantCarts.Update(_entity, !_isNew);
			DataContext.Writer.SubmitChanges();
			_isNew = false;
		}

		public static MerchantSettings Load(int merchantId)
		{
			//ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);
			//AccountFilter.Current.Validate(Accounts.AccountType.Merchant, merchantId);
			var settings = (from lu in DataContext.Reader.SetMerchantCarts where lu.Merchant_id == merchantId select new MerchantSettings(lu)).SingleOrDefault();
            if (settings == null)
                settings = new MerchantSettings(merchantId);
            return settings;
		}

	}
}
