﻿using Netpay.Bll.Accounts;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.Shop
{
    public class RegionShop
    {
        public const string SecuredObjectName = "RegionShop";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Risk.Module.Current, SecuredObjectName); } }

        private MerchantSetShop _shop;
        private List<InstallmentStep> _installmentSteps;
        private ShopLocations _locations;
        private bool _isNew = false;
        private string _merchantNumber = null;

        public RegionShop(int merchantId)
        {
            _shop = new MerchantSetShop();
            _shop.Merchant_id = merchantId;
            _shop.IsEnabled = false;
            _isNew = true;
        }

        private RegionShop(MerchantSetShop shop)
        {
            _shop = shop;
            _isNew = false;
        }

        public int ShopId { get { return _shop.MerchantSetShop_id; } set { _shop.MerchantSetShop_id = value; } }
        public int MerchantId { get { return _shop.Merchant_id; } set { _shop.Merchant_id = value; } }
        public string SubDomainName { get { return _shop.SubDomainName; } set { _shop.SubDomainName = value; } }
        public string UIBaseColor { get { return _shop.UIBaseColor; } set { _shop.UIBaseColor = value; } }
        public string LogoFileName { get { return _shop.LogoFileName; } set { _shop.LogoFileName = value; } }
        public string BannerFileName { get { return _shop.BannerFileName; } set { _shop.BannerFileName = value; } }
        public string BannerLinkUrl { get { return _shop.BannerLinkUrl; } set { _shop.BannerLinkUrl = value; } }
        public string FacebookUrl { get { return _shop.URL_Facebook; } set { _shop.URL_Facebook = value; } }
        public string GooglePlusUrl { get { return _shop.URL_GooglePlus; } set { _shop.URL_GooglePlus = value; } }
        public string TwitterUrl { get { return _shop.URL_Twitter; } set { _shop.URL_Twitter = value; } }
        public string LinkedinUrl { get { return _shop.URL_Linkedin; } set { _shop.URL_Linkedin = value; } }
        public string PinterestUrl { get { return _shop.URL_Pinterest; } set { _shop.URL_Pinterest = value; } }
        public string YoutubeUrl { get { return _shop.URL_Youtube; } set { _shop.URL_Youtube = value; } }
        public string VimeoUrl { get { return _shop.URL_Vimeo; } set { _shop.URL_Vimeo = value; } }
        public bool IsShopEnabled { get { return _shop.IsEnabled; } set { _shop.IsEnabled = value; } }
        public string CurrencyIsoCode { get { return _shop.CurrencyISOCode; } set { _shop.CurrencyISOCode = value; } }

        public bool IsNew { get { return _isNew; } }
        public string MerchantNumber {
            get {
                if (_merchantNumber != null) return _merchantNumber;
                _merchantNumber = DataContext.Reader.tblCompanies.Where(v => v.ID == _shop.Merchant_id).Select(v => v.CustomerNumber).SingleOrDefault();
                return _merchantNumber;
            }
        }
        public string ShopUrl { get { return Module.GetUrl(MerchantNumber, ShopId); } }

        public ShopLocations Locations
        {
            get
            {
                if (_locations == null)
                {
                    _locations = new ShopLocations();
                    if (IsNew)
                    {
                        _locations.Regions = new List<Region>();
                        _locations.Countries = new List<Country>();
                        _locations.IsGlobal = false;
                    }
                    else
                    {
                        var dbLocations = (from l in DataContext.Reader.MerchantSetShopToCountryRegions where l.MerchantSetShop_id == ShopId select l);

                        _locations.Countries = new List<Country>();
                        var countryIsoCodes = dbLocations.Where(l => l.CountryISOCode != null).Select(l => l.CountryISOCode);
                        foreach (string iso in countryIsoCodes)
                        {
                            Country country = Country.CacheByIso[iso];
                            _locations.Countries.Add(country);
                        } 
                        
                        var regionsIsoCodes = dbLocations.Where(l => l.WorldRegionISOCode != null).Select(l => l.WorldRegionISOCode);
                        _locations.Regions = Region.Cache.Where(r => regionsIsoCodes.Contains(r.IsoCode)).ToList();

                        _locations.IsGlobal = _locations.Regions.Count == 0 && _locations.Countries.Count == 0;
                    }
                }

                return _locations;   
            }
            set { _locations = value; }
        }
        
        public class ShopLocations
        {
            private List<Region> _regions;
            private List<Country> _countries;
            private bool _isGlobal = false;

            public List<Region> Regions { get { return _regions; } set { _regions = value; } }
            public List<Country> Countries { get { return _countries; } set { _countries = value; } }
            public bool IsGlobal { get { return _isGlobal; } set { _isGlobal = value; } }

            public string RegionsToString()
            {
                if (IsGlobal)
                    return "Global";
                return Regions.Select(r => r.Name).ToDelimitedString();
            }

            public string CountriesToString()
            {
                return Countries.Select(c => c.Name).ToDelimitedString();
            }

            /// <summary>
            /// Concatenates the name from the shop locations
            /// </summary>
            public override string ToString()
            {
                if (IsGlobal)
                    return "Global";

                string name = string.Empty;
                if (Regions != null && Regions.Count > 0)
                    name += Regions.Select(r => r.Name).ToDelimitedString();
                if (Regions != null && Regions.Count > 0 && Countries != null && Countries.Count > 0)
                    name += ",";
                if (Countries != null && Countries.Count > 0)
                    name += Countries.Select(c => c.Name).ToDelimitedString();
                return name;
            }
        }
        
        public List<InstallmentStep> InstallmentSteps
        {
            get
            {
                if (_installmentSteps == null)
                {
                    _installmentSteps = (from s in DataContext.Reader.MerchantSetShopInstallments where s.MerchantSetShop_id == ShopId orderby s.Amount ascending select new InstallmentStep(s)).ToList();
                    if (_installmentSteps != null && _installmentSteps.Count > 0)
                    {
                        for (int idx = 0; idx < _installmentSteps.Count; idx++)
                        {
                            InstallmentStep step = _installmentSteps[idx];
                            if (idx == 0)
                                step.CalculatedFromAmount = 0;
                            else
                                step.CalculatedFromAmount = _installmentSteps[idx - 1].ToAmount + 1;
                        } 
                    }
                }

                return _installmentSteps;
            }
            set { _installmentSteps = value; }
        }

        /// <summary>
        /// Represent an installment step, ie an amount from which the number of installments is available.
        /// </summary>
        public class InstallmentStep
        {
            public InstallmentStep(MerchantSetShopInstallment entiy)
            {
                ID = entiy.MerchantSetShopInstallments_id;
                ToAmount = entiy.Amount;
                MaxInstallments = entiy.MaxInstallments;
            }

            public InstallmentStep(decimal calculatedFromAmount, decimal toAmount, byte maxInstallments)
            {
                ID = 0;
                CalculatedFromAmount = calculatedFromAmount;
                ToAmount = toAmount;
                MaxInstallments = maxInstallments;
            }

            public int ID { get; set; }
            public decimal CalculatedFromAmount { get; set; }
            public decimal ToAmount { get; set; }
            public byte MaxInstallments { get; set; }
        }

        public bool IsNameUsed(string name)
        {
            if (string.IsNullOrEmpty(name))
                return false;

            name = name.Trim().ToLower();
            if (IsNew)
            {
                return (from s in DataContext.Reader.MerchantSetShops where s.SubDomainName.Trim().ToLower() == name select s.SubDomainName).Count() > 0;
            }
            else
            {
                if (SubDomainName != null && name == SubDomainName.Trim().ToLower())
                    return false;
                return (from s in DataContext.Reader.MerchantSetShops where s.MerchantSetShop_id != _shop.MerchantSetShop_id && s.SubDomainName.Trim().ToLower() == name select s.SubDomainName).Count() > 0;
            }
        }

        /// <summary>
        /// Saves a shop, its installments and locations.
        /// </summary>
        /// <returns></returns>
        public int Save()
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Edit);
            AccountFilter.Current.Validate(Accounts.AccountType.Merchant, MerchantId);

            // validate name
            if (IsNameUsed(SubDomainName))
                throw new ArgumentException("Shop name already used.");

            // validate locations
            ShopLocations usedLocations = GetUsedLocations();

            var usedIso = usedLocations.Regions.Select(r => r.IsoCode);
            var currntIso = Locations.Regions.Select(r => r.IsoCode);
            if (usedIso.Intersect(currntIso).Count() > 0)
                throw new ArgumentException("Region can be used only once.");

            usedIso = usedLocations.Countries.Select(c => c.IsoCode2);
            currntIso = Locations.Countries.Select(c => c.IsoCode2);
            if (usedIso.Intersect(currntIso).Count() > 0)
                throw new ArgumentException("Country can be used only once.");

            Locations.IsGlobal = Locations.Regions.Count == 0 && Locations.Countries.Count == 0;
            if (usedLocations.IsGlobal && Locations.IsGlobal)
                throw new ArgumentException("There is already a global shop.");

            // validate installments steps
            if (InstallmentSteps != null && InstallmentSteps.Count > 0)
            {
                InstallmentSteps = InstallmentSteps.OrderBy(i => i.ToAmount).ToList();
                for (int idx = 0; idx < InstallmentSteps.Count; idx++)
                {
                    if (idx == 0)
                        continue;

                    if (InstallmentSteps[idx].ToAmount <= InstallmentSteps[idx - 1].ToAmount)
                        throw new ArgumentException("Invalid installment step");
                } 
            }

            // set default currency for new shop
            if (IsNew && CurrencyIsoCode == null)
            {
                Currency currency = Merchants.Merchant.GetSupportedCurrencies(MerchantId).FirstOrDefault();
                if (currency == null)
                    throw new Exception("Couldn't find default currency");
                CurrencyIsoCode = currency.IsoCode;
            }
            
            // save shop
            DataContext.Writer.MerchantSetShops.Update(_shop, !_isNew);
            DataContext.Writer.SubmitChanges();

            // save installment steps
            var installments = from i in DataContext.Writer.MerchantSetShopInstallments where i.MerchantSetShop_id == ShopId select i;
            DataContext.Writer.MerchantSetShopInstallments.DeleteAllOnSubmit(installments);
            foreach (InstallmentStep step in InstallmentSteps)
            {
                Netpay.Dal.Netpay.MerchantSetShopInstallment dbStep = new Netpay.Dal.Netpay.MerchantSetShopInstallment();
                dbStep.MerchantSetShop_id = _shop.MerchantSetShop_id;
                dbStep.Amount = step.ToAmount;
                dbStep.MaxInstallments = step.MaxInstallments;
                DataContext.Writer.MerchantSetShopInstallments.Insert(dbStep);
            }
            DataContext.Writer.SubmitChanges();

            // save locations
            var dbLocations = from l in DataContext.Writer.MerchantSetShopToCountryRegions where l.MerchantSetShop_id == ShopId select l;
            DataContext.Writer.MerchantSetShopToCountryRegions.DeleteAllOnSubmit(dbLocations);
            DataContext.Writer.SubmitChanges();

            foreach (var location in Locations.Countries)
            {
                var dbLocation = new Dal.Netpay.MerchantSetShopToCountryRegion();
                dbLocation.MerchantSetShop_id = ShopId;
                dbLocation.CountryISOCode = location.IsoCode2;
                DataContext.Writer.MerchantSetShopToCountryRegions.InsertOnSubmit(dbLocation);
            }
            DataContext.Writer.SubmitChanges();

            foreach (var location in Locations.Regions)
            {
                var dbLocation = new Dal.Netpay.MerchantSetShopToCountryRegion();
                dbLocation.MerchantSetShop_id = ShopId;
                dbLocation.WorldRegionISOCode = location.IsoCode;
                DataContext.Writer.MerchantSetShopToCountryRegions.InsertOnSubmit(dbLocation);
            }
            DataContext.Writer.SubmitChanges();

            // update products currency
            var prods = from p in DataContext.Writer.Products where p.MerchantSetShop_id == _shop.MerchantSetShop_id select p;
            foreach (Product prod in prods)
                prod.CurrencyISOCode = CurrencyIsoCode;
            DataContext.Writer.SubmitChanges();

            // update cart currency
            var carts = from c in DataContext.Writer.Carts where c.MerchantSetShop_id == _shop.MerchantSetShop_id select c;
            foreach (var cart in carts)
                cart.CurrencyISOCode = CurrencyIsoCode;
            DataContext.Writer.SubmitChanges();

            _isNew = false;
            return _shop.MerchantSetShop_id;
        }

        /// <summary>
        /// Deletes this shop, its products, locations and installments.
        /// Uses <see cref="Products.Product.Delete"/> to ensure complete product removal.
        /// </summary>
        public void Delete()
        {
            // delete products
            var filters = new Products.Product.SearchFilters();
            filters.ShopId = ShopId;
            var prods = Products.Product.Search(filters, null);
            foreach (var prod in prods)
                prod.Delete();

            // delete locations
            var dbLocations = from l in DataContext.Writer.MerchantSetShopToCountryRegions where l.MerchantSetShop_id == ShopId select l;
            DataContext.Writer.MerchantSetShopToCountryRegions.DeleteAllOnSubmit(dbLocations);
            DataContext.Writer.SubmitChanges();

            // delete instalments
            var installments = from i in DataContext.Writer.MerchantSetShopInstallments where i.MerchantSetShop_id == ShopId select i;
            DataContext.Writer.MerchantSetShopInstallments.DeleteAllOnSubmit(installments);
            DataContext.Writer.SubmitChanges();

            // delete shop
            var shop = (from s in DataContext.Writer.MerchantSetShops where s.MerchantSetShop_id == ShopId select s).Single();
            DataContext.Writer.MerchantSetShops.DeleteOnSubmit(shop);
            DataContext.Writer.SubmitChanges();
        }

        /// <summary>
        /// Loads a single shop by sub domain name
        /// </summary>
        /// <param name="subDomainName"></param>
        /// <returns></returns>
        public static RegionShop Load(string subDomainName)
        {
            var shops = from lu in DataContext.Reader.MerchantSetShops
                        where lu.SubDomainName == subDomainName
                        select lu;

            if (shops == null || shops.Count() != 1)
                return null;

            return new RegionShop(shops.Single());
        }

        /// <summary>
        /// Loads a single shop
        /// </summary>
        /// <param name="shopId"></param>
        /// <returns></returns>
        public static RegionShop Load(int shopId)
        {
            RegionShop shop = (from lu in DataContext.Reader.MerchantSetShops where lu.MerchantSetShop_id == shopId select new RegionShop(lu)).SingleOrDefault();
            return shop;
        }

        /// <summary>
        /// Loads all merchant shops
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public static List<RegionShop> LoadAll(int merchantId)
        {
            List<RegionShop> shops = (from lu in DataContext.Reader.MerchantSetShops where lu.Merchant_id == merchantId select new RegionShop(lu)).ToList();
            return shops;
        }

        public static List<RegionShop> Load(List<string> regions, List<string> countries, bool includeGlobal)
        {
            List<RegionShop> shops = (from cr in DataContext.Reader.MerchantSetShopToCountryRegions where regions.Contains(cr.WorldRegionISOCode) || countries.Contains(cr.CountryISOCode) select new RegionShop(cr.setting_MerchantSetShop)).ToList();
            if (includeGlobal)
            {
                List<RegionShop> globalShops = (from s in DataContext.Reader.MerchantSetShops where s.setting_MerchantSetShopToCountryRegions.Count == 0 select new RegionShop(s)).ToList();
                shops.AddRange(globalShops);
            }

            return shops;
        }

        /// <summary>
        /// Gets all used locations from all merchant other shops,
        /// To make sure a location is used only once.
        /// </summary>
        /// <param name="merchantId"></param>
        /// <returns></returns>
        public ShopLocations GetUsedLocations()
        {
            ShopLocations locations = new ShopLocations();
            List<RegionShop> shops = LoadAll(MerchantId);
            if (!IsNew)
                shops.Remove(shops.Where(s => s.ShopId == ShopId).Single());
            locations.Countries = shops.SelectMany(s => s.Locations.Countries).ToList();
            locations.Regions = shops.SelectMany(s => s.Locations.Regions).ToList();
            locations.IsGlobal = shops.Where(s => s.Locations.IsGlobal).SingleOrDefault() != null;

            return locations;
        }
    }
}
