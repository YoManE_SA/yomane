﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using Netpay.Bll.Accounts;

namespace Netpay.Bll.Shop
{
	public class MerchantSettings : BaseDataObject
	{
		public const string SecuredObjectName = "ShopSettings";
		public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Risk.Module.Current, SecuredObjectName); } }

		private bool _isNew; 
		private Dal.Netpay.MerchantSetCart _entity;
        private List<InstallmentStep> _installmentSteps;
       
		public int Merchant_id { get { return _entity.Merchant_id; } set { _entity.Merchant_id = value; } }

		public string SubDomainName { get { return _entity.ShopSubDomainName; } set { _entity.ShopSubDomainName = value; } }
		public string UIBaseColor { get { return _entity.UIBaseColor; } set { _entity.UIBaseColor = value; } }
		public string LogoFileName { get { return _entity.LogoFileName; } set { _entity.LogoFileName = value; } }
		public string BannerFileName { get { return _entity.BannerFileName; } set { _entity.BannerFileName = value; } }
		public string BannerLinkUrl { get { return _entity.BannerLinkUrl; } set { _entity.BannerLinkUrl = value; } }
		public string FacebookUrl { get { return _entity.URL_Facebook; } set { _entity.URL_Facebook = value; } }
		public string GooglePlusUrl { get { return _entity.URL_GooglePlus; } set { _entity.URL_GooglePlus = value; } }
		public string TwitterUrl { get { return _entity.URL_Twitter; } set { _entity.URL_Twitter = value; } }
		public string LinkedinUrl { get { return _entity.URL_Linkedin; } set { _entity.URL_Linkedin = value; } }
		public string PinterestUrl { get { return _entity.URL_Pinterest; } set { _entity.URL_Pinterest = value; } }
		public string YoutubeUrl { get { return _entity.URL_Youtube; } set { _entity.URL_Youtube = value; } }
		public string VimeoUrl { get { return _entity.URL_Vimeo; } set { _entity.URL_Vimeo = value; } }
        public bool IsShopEnabled { get { return _entity.IsEnabled; } set { _entity.IsEnabled = value; } }
        public List<InstallmentStep> InstallmentSteps 
        { 
            get 
            {
                if (_installmentSteps == null) 
                {
                    _installmentSteps = (from s in DataContext.Reader.MerchantSetCartInstallments where s.Merchant_id == Merchant_id orderby s.Amount ascending select new InstallmentStep(s)).ToList();
                }
                
                return _installmentSteps; 
            } 
            set { _installmentSteps = value; } 
        }

        public class InstallmentStep
        {
            public InstallmentStep(Netpay.Dal.Netpay.MerchantSetCartInstallment entiy)
            {
                ID = entiy.MerchantSetCartInstallments;
                Amount = entiy.Amount;
                MaxInstallments = entiy.MaxInstallments;
            }

            public InstallmentStep(decimal amount, byte maxInstallments) 
            {
                ID = 0;
                Amount = amount;
                MaxInstallments = maxInstallments;
            }

            public int ID { get; set; }
            public decimal Amount { get; set; }
            public byte MaxInstallments { get; set; }
        }

		internal MerchantSettings(Netpay.Dal.Netpay.MerchantSetCart entity)
		{ 
			_entity = entity;
			_isNew = false;
		}

		public MerchantSettings(int merchantId)
		{
			_entity = new Dal.Netpay.MerchantSetCart();
			_entity.Merchant_id = merchantId;
            _entity.IsEnabled = false;
			_isNew = true;
		}

		public void Save()
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Edit);
			AccountFilter.Current.Validate(AccountType.Merchant, _entity.Merchant_id);
			DataContext.Writer.MerchantSetCarts.Update(_entity, !_isNew);
			DataContext.Writer.SubmitChanges();

            var installments = from i in DataContext.Writer.MerchantSetCartInstallments where i.Merchant_id == _entity.Merchant_id select i;
            DataContext.Writer.MerchantSetCartInstallments.DeleteAllOnSubmit(installments);
            foreach (InstallmentStep step in InstallmentSteps)
            {
                Netpay.Dal.Netpay.MerchantSetCartInstallment dbStep = new Netpay.Dal.Netpay.MerchantSetCartInstallment();
                dbStep.Merchant_id = _entity.Merchant_id;
                dbStep.Amount = step.Amount;
                dbStep.MaxInstallments = step.MaxInstallments;
                DataContext.Writer.MerchantSetCartInstallments.Insert(dbStep);
            }
            DataContext.Writer.SubmitChanges();

            _isNew = false;
		}

		public static MerchantSettings Load(int merchantId)
		{
            var entity = (from lu in DataContext.Reader.MerchantSetCarts where lu.Merchant_id == merchantId select lu).SingleOrDefault();
            if (entity == null)
                return new MerchantSettings(merchantId);
            else
                return new MerchantSettings(entity);
		}
	}
}
