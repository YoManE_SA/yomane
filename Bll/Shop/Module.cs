﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Shop
{
	public class Module : Infrastructure.Module
	{
		public const string ModuleName = "Shop";
		public override string Name { get { return ModuleName; } }
		public override decimal Version { get { return 1.0m; } }
		public static Module Current { get { return Infrastructure.Module.Get(ModuleName) as Module; } }
		protected override void OnInstall(EventArgs e)
		{
			Infrastructure.Security.SecuredObject.Create(this, MerchantSettings.SecuredObjectName, PermissionGroup.ReadEditDelete);
            base.OnInstall(e);
		}

		protected override void OnUninstall(EventArgs e)
		{
			Infrastructure.Security.SecuredObject.Remove(this);
			base.OnUninstall(e);
		}

         public static string GetUrl(string accountNumber, int? shopId = null, int? productId = null)
        {
            string ret = Infrastructure.Domain.Current.ShopUrl.Replace("{0}", accountNumber).TrimEnd('/');
            if (Infrastructure.Domain.Current.UsePublicPage) {
                if (productId.HasValue) ret += "&Item=" + productId.Value; // &disp_lng=EN
            } else {
                if (shopId != null) ret += "/" + shopId.Value;
                if (productId != null) ret += "/Product/" + productId.Value;
            }
            return ret;
        }
    }
}