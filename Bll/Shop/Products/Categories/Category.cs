﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Shop.Products
{
    public class Category : BaseDataObject
    {
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Categories.Module.Current, SecuredObjectName); } }


        private Dal.Netpay.ProductCategory _entity;

        public short ID { get { return _entity.ProductCategory_id; } }
        public short? ParentID { get { return _entity.ParentID; } set { _entity.ParentID = value; } }
        public string Name { get { return _entity.Name; } set { _entity.Name = value; } }
        public byte? SortOrder { get { return _entity.SortOrder; } set { _entity.SortOrder = value; } }

        public List<Category> Values { get; private set; }

        private Category(Netpay.Dal.Netpay.ProductCategory entity)
        {
            _entity = entity;
        }

        public Category()
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            _entity = new Dal.Netpay.ProductCategory();
        }

        public Category(int merchantId)
        {
            _entity = new Dal.Netpay.ProductCategory();
            _entity.ParentID = null;
            Values = new List<Category>();
        }

        public void AddValue(Category prop)
        {
            prop._entity.ParentID = this.ID;
            Values.Add(prop);
        }

        public static List<Category> GetLevel(int? parent)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from p in DataContext.Reader.ProductCategories where (parent != null ? p.ParentID == parent : p.ParentID == null) select new Category(p)).ToList();
        }

        public static List<Category> Load(List<short> ids, bool flat)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            ids.AddRange((from p in DataContext.Reader.ProductCategories where ids.Contains(p.ProductCategory_id) select p.ParentID.GetValueOrDefault()).Distinct());
            var props = (from p in DataContext.Reader.ProductCategories where ids.Contains(p.ProductCategory_id) || ids.Contains(p.ParentID.GetValueOrDefault()) select new Category(p)).ToList();
            if (flat) return props;
            var propParent = props.Where(p => p.ParentID == null);
            foreach (var p in propParent) p.Values = props.Where(c => c.ParentID == p.ID).ToList();
            return propParent.ToList();
        }

        public static List<Category> LoadForMerchant(int merchantId)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin , SecuredObject, PermissionValue.Read);
            
            var props = (from p in DataContext.Reader.ProductCategories
                         where
                         (from pc in DataContext.Reader.ProductToProductCategories join pp in DataContext.Reader.Products on pc.Product_id equals pp.Product_id where pp.Merchant_id == merchantId select pc.ProductCategory_id)
                         .Contains(p.ProductCategory_id)
                         select new Category(p))
                         .ToList();

            var parents = props.Where(p => p.ParentID != null).Select(p => p.ParentID.Value).Distinct();
            var parentCat = (from p in DataContext.Reader.ProductCategories where parents.Contains(p.ProductCategory_id) select new Category(p)).ToList();
            foreach (var p in parentCat) p.Values = props.Where(c => c.ParentID == p.ID).ToList();
            return parentCat.Union(props.Where(p => p.ParentID == null && !parentCat.Select(c => c.ID).Contains(p.ID))).ToList();
        }

        public void Save()
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Edit);

            var isNew = true;
            DataContext.Writer.ProductCategories.Update(_entity, (_entity.ProductCategory_id != 0));
            DataContext.Writer.SubmitChanges();
            if (!isNew) return;
            foreach (var v in Values)
                v._entity.ParentID = _entity.ProductCategory_id;
        }

        public static List<Category> Cache
        {
            get
            {
                if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return Domain.Current.GetCachData("ProductCategories", () =>
                {
                    return new Domain.CachData((from pc in DataContext.Reader.ProductCategories select new Category(pc)).ToList(), DateTime.Now.AddHours(5));
                }) as List<Category>;
            }
        }

        public static List<Category> GetAllCategories
        {
            get
            {
                if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return (from pc in DataContext.Reader.ProductCategories select new Category(pc)).ToList();
            }
        }

        public static Category Get(int productcategoryid)
        {
            return Cache.Where(x => x.ID == productcategoryid).SingleOrDefault();
        }

        public void Save(bool withoutValues)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            DataContext.Writer.ProductCategories.Update(_entity, (_entity.ProductCategory_id != 0));
            DataContext.Writer.SubmitChanges();

            Domain.Current.RemoveCachData("ProductCategories");
        }

    }
}
