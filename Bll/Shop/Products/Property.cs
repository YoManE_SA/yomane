﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Shop.Products
{
    public class Property : BaseDataObject
    {
        public enum PropertyType { Text = 1, Color = 2, Image = 3 }
        //max 2 levels
        private Dal.Netpay.ProductProperty _entity;

        public int ID { get { return _entity.ProductProperty_id; } }
        public int? ParentID { get { return _entity.ParentID; } }
        public int? MerchantID { get { return _entity.Merchant_id; } }
        public string Name { get { return _entity.Name; } set { _entity.Name = value; } }
        public string Value { get { return _entity.Value; } set { _entity.Value = value; } }
        public PropertyType Type { get { return (PropertyType)_entity.ProductPropertyType_id; } }
        public List<Property> Values { get; private set; }
        private Property _parent = null;
        public Property Parent
        {
            get
            {
                if (!ParentID.HasValue) return null;
                if (_parent == null) _parent = Load(new List<int> { ParentID.Value }, true, true).SingleOrDefault();
                return _parent;
            }
        }

        private Property(Netpay.Dal.Netpay.ProductProperty entity)

        {
            _entity = entity;
        }

        public Property(int merchantId, PropertyType propertyType)

        {
            _entity = new Dal.Netpay.ProductProperty();
            _entity.Merchant_id = merchantId;
            _entity.ProductPropertyType_id = (byte)propertyType;
            _entity.ParentID = null;
            Values = new List<Property>();
        }

        public void AddValue(Property prop)
        {
            prop._entity.ParentID = this.ID;
            Values.Add(prop);
        }

        public string ValuesString
        {
            get
            {
                return string.Join(", ", Values.Select(v => v.Name));
            }
            set
            {
                string[] strValues = value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                var newList = new List<Property>();
                foreach (var s in strValues)
                {
                    var extistingItem = Values.Where(v => v.Name.ToLower() == s.ToLower().Trim()).SingleOrDefault();
                    if (extistingItem == null)
                    {
                        var newValue = new Property(_entity.Merchant_id.GetValueOrDefault(), PropertyType.Text) { Name = s.Trim() };
                        newValue._entity.ParentID = this.ID;
                        newList.Add(newValue);
                    }
                    else newList.Add(extistingItem);
                }
                Values = newList;
            }
        }

        public static List<Property> Load(List<int> ids, bool flat, bool explicitLoad)
        {
            //context.IsUserOfType(new UserType[] { UserType.MerchantPrimary });
            var parents = (from p in DataContext.Reader.ProductProperties where ids.Contains(p.ProductProperty_id) select p.ParentID.GetValueOrDefault()).Distinct().ToList();
            List<Property> props = null;
            if (explicitLoad)
            {
                props = (from p in DataContext.Reader.ProductProperties where ids.Contains(p.ProductProperty_id) || parents.Contains(p.ProductProperty_id) select new Property(p)).ToList();
            }
            else
            {
                ids.AddRange(parents);
                props = (from p in DataContext.Reader.ProductProperties where ids.Contains(p.ProductProperty_id) || ids.Contains(p.ParentID.GetValueOrDefault()) select new Property(p)).ToList();
            }

            if (flat)
                return props;

            var propParent = props.Where(p => p.ParentID == null);
            foreach (var p in propParent)
            {
                p.Values = props.Where(c => c.ParentID == p.ID).ToList();
                foreach (var pp in p.Values)
                    pp._parent = p;
            }
            return propParent.ToList();
        }

        public static Property Load(int merchantId, string name)
        {
            //context.IsUserOfType(new UserType[] { UserType.MerchantPrimary });
            var ret = (from p in DataContext.Reader.ProductProperties where (p.Merchant_id == null || p.Merchant_id == merchantId) && (p.ParentID == null) && p.Name.ToLower() == name.ToLower() select new Property(p)).SingleOrDefault();
            if (ret == null) return null;
            ret.Values = (from p in DataContext.Reader.ProductProperties where p.ParentID == ret.ID select new Property(p)).ToList();
            return ret;
        }

        public static List<Property> MapValues(List<Property> parentProps, List<string> values)
        {
            if (parentProps.Count() != values.Count()) throw new Exception("source and destenation properties lists are not with same length");
            var ret = new List<Property>();
            for (int i = 0; i < parentProps.Count(); i++)
            {
                var val = parentProps[i].Values.Where(t => t.Name.ToLower() == values[i].ToLower()).FirstOrDefault();
                ret.Add(val);
            }
            return ret;
        }

        public void Save()
        {
            var isNew = true;
            DataContext.Writer.ProductProperties.Update(_entity, (_entity.ProductProperty_id != 0));
            DataContext.Writer.SubmitChanges();
            if (!isNew) return;
            foreach (var v in Values)
                v._entity.ParentID = _entity.ProductProperty_id;
        }
    }
}
