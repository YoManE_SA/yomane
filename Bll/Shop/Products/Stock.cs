﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Shop.Products
{
	public class Stock : BaseDataObject
	{
		private  Dal.Netpay.ProductStock _entity;
		public int ID { get { return _entity.ProductStock_id; } }
		public int ProductID { get { return _entity.Product_id; } }
		public string SKU { get { return _entity.SKU; } set { _entity.SKU = value; } }
		public short? QtyAvailable { get { return _entity.QtyAvailable; } set { _entity.QtyAvailable = value; } }
		private List<Property> _properties;
		public List<Property> Properties { 
			get {
				if (_properties != null) return _properties;
				var strockRef = (from sr in DataContext.Reader.ProductStockReferences where sr.ProductStock_id == ID select sr).ToList();				
				_properties = Property.Load(strockRef.Select(sr => sr.ProductProperty_id).Distinct().ToList(), false, true).Select(p => p.Values.FirstOrDefault()).ToList();
				return _properties; 
			} 
		}

		private Stock(Netpay.Dal.Netpay.ProductStock entity)
			
		{ 
			_entity = entity;
		}

		public Stock(int productId, List<Property> properties)
			
		{
			_entity = new Dal.Netpay.ProductStock();
			_entity.Product_id = productId;
			_properties = properties;
		}

		public void Save()
		{
			DataContext.Writer.ProductStocks.Update(_entity, (_entity.ProductStock_id != 0));
			DataContext.Writer.SubmitChanges();
			var currentReferences = DataContext.Writer.ProductStockReferences.Where(sr => sr.ProductStock_id == _entity.ProductStock_id).ToList();
			foreach(var v in Properties) {
				if (!currentReferences.Exists(sr => sr.ProductProperty_id == v.ID))
					DataContext.Writer.ProductStockReferences.InsertOnSubmit(new Dal.Netpay.ProductStockReference() { Product_id = _entity.Product_id, ProductProperty_id = v.ID, ProductStock_id = _entity.ProductStock_id });
			}
			DataContext.Writer.SubmitChanges();
		}

		public void Delete()
		{
			//User user = SecurityManager.GetInternalUsezr(new UserType[] { UserType.MerchantPrimary });
			if (_entity.ProductStock_id == 0) return;
			DataContext.Writer.ExecuteCommand("Update Data.CartProduct Set ProductStock_id = Null Where ProductStock_id={0}", _entity.ProductStock_id);
			DataContext.Writer.ProductStockReferences.DeleteAllOnSubmit(DataContext.Writer.ProductStockReferences.Where(sr => sr.ProductStock_id == _entity.ProductStock_id));
			DataContext.Writer.ProductStocks.Delete(_entity);
			DataContext.Writer.SubmitChanges();
		}

		public static Stock Load(int stockId)
		{
			//context.IsUserOfType(new UserType[] { UserType.MerchantPrimary });
			var entity = (from lu in DataContext.Reader.ProductStocks where lu.ProductStock_id == stockId select lu).SingleOrDefault();
			if (entity == null) return null;
			return new Stock(entity);
		}

		public static Stock Load(int productId, List<int> propValues)
		{
			//context.IsUserOfType(new UserType[] { UserType.MerchantPrimary });
			var stockid = (from sr in DataContext.Reader.ProductStockReferences
						   where sr.Product_id == productId && propValues.Contains(sr.ProductProperty_id)
						   group sr by sr.ProductStock_id into gsr
						   where gsr.Count() == propValues.Count()
						   select gsr.Key).FirstOrDefault();
			var entity = (from s in DataContext.Reader.ProductStocks where s.ProductStock_id == stockid select s).SingleOrDefault();
			if (entity == null) return null;
			return new Stock(entity);
		}

		private static void addPropertyLevel(int productId, Property[] topProps, int propIndex, List<Stock> stocks, List<Property> newPropList)
		{
			if (propIndex >= topProps.Length) {
				if ((from s in stocks where Enumerable.SequenceEqual(s.Properties.Select(t => t.Name.ToLower()).OrderBy(t => t), newPropList.Select(t => t.Name.ToLower()).OrderBy(t => t)) select s).FirstOrDefault() != null) return;
				stocks.Add(new Stock(productId, newPropList.ToList()));
				return;
			}
			if (propIndex == 0) newPropList = new List<Property>();
			foreach (var v in topProps[propIndex].Values)
			{
				newPropList.Add(v);
				addPropertyLevel(productId, topProps, propIndex + 1, stocks, newPropList);
				newPropList.Remove(v);
			}
		}

		public static List<Stock> AddStock(int productId, List<Property> propList)
		{
			var currentStock = Search(productId);
			var topProps =  propList.Where(prop => prop.ParentID == null).ToArray();
			addPropertyLevel(productId, topProps.ToArray(), 0, currentStock, null);
			return currentStock;
		}

		public static List<Stock> Search(int productId)
		{
			//context.IsUserOfType(new UserType[] { UserType.MerchantPrimary });
			var strockRef = (from sr in DataContext.Reader.ProductStockReferences where sr.Product_id == productId select sr).ToList();
			var stockList = (from s in DataContext.Reader.ProductStocks where strockRef.Select(sr => sr.ProductStock_id).Distinct().Contains(s.ProductStock_id) select new Stock(s)).ToList();
			var allProps = Property.Load(strockRef.Select(sr => sr.ProductProperty_id).Distinct().ToList(), true, true);
			foreach (var s in stockList)
				s._properties = allProps.Where(prop => strockRef.Where(sr => sr.ProductStock_id == s.ID).Select(sr => sr.ProductProperty_id).Contains(prop.ID)).ToList();
			return stockList;
		}


	}
}
