﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using Netpay.Dal.Netpay;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace Netpay.Bll.Shop.Products
{
    public enum ProductType { Unmanaged = 0, Physical = 1, Virtual = 2, Download = 3 }
    public class Product : BaseDataObject
    {
        public class SearchFilters
        {
            public Range<int?> ID;
            public Range<decimal?> Price;
            public int? MerchantId;
            public int? ShopId;
            public bool? PromoOnly;
            public string Text;
            public string Name;
            public bool? IsActive;
            public List<string> Tags;
            public List<int> Categoryies;
            public List<int> MerchantGroupID;
            public string LanguageIso;
            public List<string> regions;
            public List<string> countries;
            public bool? includeGlobalRegion;
        }

        private Dal.Netpay.Product _entity;

        public int ID { get { return _entity.Product_id; } }
        public int MerchantID { get { return _entity.Merchant_id; } }
        public int ShopID { get { return _entity.MerchantSetShop_id; } set { _entity.MerchantSetShop_id = value; } }
        public string CurrencyISOCode { get { return _entity.CurrencyISOCode; } set { _entity.CurrencyISOCode = value; } }
        public decimal Price { get { return _entity.Price; } set { _entity.Price = value; } }
        public string SKU { get { return _entity.SKU; } set { _entity.SKU = value; } }
        public bool IsActive { get { return _entity.isActive; } set { _entity.isActive = value; } }
        public bool IsAuthorize { get { return _entity.TransType == (byte)Infrastructure.TransactionType.Authorization; } set { _entity.TransType = (byte)(value ? Infrastructure.TransactionType.Authorization : Infrastructure.TransactionType.Capture); } }
        public byte Installments { get { return _entity.Installments; } set { _entity.Installments = value; } }
        public short QtyStart { get { if (Type == ProductType.Physical) return _entity.QtyStart; return 1; } set { _entity.QtyStart = value; } }
        public short QtyEnd { get { if (Type == ProductType.Physical) return _entity.QtyEnd; return 10; } set { _entity.QtyEnd = value; } }
        public short QtyStep { get { if (Type == ProductType.Physical) return _entity.QtyStep; return 1; } set { _entity.QtyStep = value; } }
        public short? QtyAvailable { get { return _entity.QtyAvailable; } set { _entity.QtyAvailable = value; } }
        public byte? SortOrder { get { return _entity.SortOrder; } set { _entity.SortOrder = value; } }
        public bool IsDynamicProduct { get { return _entity.isDynamicPrice; } set { _entity.isDynamicPrice = value; } }
        public string ImageFileName { get { return _entity.ImageFileName; } set { _entity.ImageFileName = value; } }
        public string ImageFileNameSmall
        {
            get
            {
                if (ImageFileName == null)
                    return null;
                return $"{Path.GetDirectoryName(ImageFileName)}/{Path.GetFileNameWithoutExtension(ImageFileName)}_small{Path.GetExtension(ImageFileName)}";
            }
        }

        public bool IsSmallImage
        {
            get
            {
                if (ImageFileNameSmall == null)
                    return false;
                return File.Exists(ImageSmallPhysicalPath);
            }
        }
        public string RecurringString { get { return _entity.RecurringString; } set { _entity.RecurringString = value; } }
        public ProductType Type { get { return (ProductType)_entity.ProductType_id; } set { _entity.ProductType_id = (byte)value; } }
        public string MediaFileName { get { return _entity.DownloadFileName; } set { _entity.DownloadFileName = value; } }

        public int AccountID { get; private set; }
        public string AccountNumber { get; private set; }

        public short? ActualQuantity
        {
            get
            {
                if (!IsActive)
                    return 0;
                return QtyAvailable;
            }
        }

        private Product(Netpay.Dal.Netpay.Product entity)
        {
            _entity = entity;
        }

        public Product(int merchantId)
        {
            _entity = new Dal.Netpay.Product();
            _entity.Merchant_id = merchantId;
            _entity.ProductType_id = (byte)ProductType.Physical;
            var acc = DataContext.Reader.Accounts.Where(a => a.Merchant_id == merchantId).SingleOrDefault();
            if (acc == null) return;
            AccountID = acc.Account_id;
            AccountNumber = acc.AccountNumber;
        }

        public string MediaPysicalPath
        {
            get
            {
                if (Type != Products.ProductType.Download) return null;
                if (string.IsNullOrEmpty(MediaFileName)) return null;
                return System.IO.Path.Combine(Accounts.Account.MapPrivatePath(AccountID, "Products"), MediaFileName);
            }
        }


        public string ProductURL { get { return Module.GetUrl(AccountNumber, ShopID, ID); } }

        public string ImageURL
        {
            get
            {
                if (string.IsNullOrEmpty(ImageFileName))
                    return null;
                return Accounts.Account.MapPublicVirtualPath(AccountNumber, ImageFileName);
            }
        }

        public string PublicImageURL
        {
            get
            {
                if (string.IsNullOrEmpty(ImageFileName))
                    return null;
                return (new System.Uri(new System.Uri(Domain.Current.WebServicesUrl), ImageURL)).ToString();
            }
        }

        public string ImagePhysicalPath
        {
            get
            {
                if (string.IsNullOrEmpty(ImageFileName))
                    return null;
                return Accounts.Account.MapPublicPath(AccountNumber, ImageFileName);
            }
        }

        public string ImageSmallPhysicalPath
        {
            get
            {
                if (string.IsNullOrEmpty(ImageFileNameSmall))
                    return null;
                return Accounts.Account.MapPublicPath(AccountNumber, ImageFileNameSmall);
            }
        }

        public void Save()
        {
            DataContext.Writer.Products.Update(_entity, (_entity.Product_id != 0));
            DataContext.Writer.SubmitChanges();

            if (_tags != null)
            {
                DataContext.Writer.ProductTags.DeleteAllOnSubmit(from t in DataContext.Writer.ProductTags where t.Product_id == ID && !_tags.Contains(t.Tag) select t);
                var newTags = _tags.ToList();
                newTags.RemoveAll(s => (from pt in DataContext.Writer.ProductTags where pt.Product_id == ID && _tags.Contains(pt.Tag) select pt.Tag).Contains(s));
                DataContext.Writer.ProductTags.InsertAllOnSubmit(newTags.Select(s => new Dal.Netpay.ProductTag() { Product_id = ID, Tag = s }));
                DataContext.Writer.SubmitChanges();
            }

            if (_categories != null)
            {
                DataContext.Writer.ProductToProductCategories.DeleteAllOnSubmit(from t in DataContext.Writer.ProductToProductCategories where t.Product_id == ID && !_categories.Contains(t.ProductCategory_id) select t);
                var newCategories = _categories.ToList();
                newCategories.RemoveAll(s => (from pt in DataContext.Writer.ProductToProductCategories where pt.Product_id == ID && _categories.Contains(pt.ProductCategory_id) select pt.ProductCategory_id).Contains(s));
                DataContext.Writer.ProductToProductCategories.InsertAllOnSubmit(newCategories.Select(s => new Dal.Netpay.ProductToProductCategory() { Product_id = ID, ProductCategory_id = s }));
                DataContext.Writer.SubmitChanges();
            }
        }

        public static Product Load(int productId)
        {
            return Load(new List<int> { productId }, null).SingleOrDefault();
        }

        public static List<Product> Load(List<int> productIds, string languageIso)
        {
            //context.IsUserOfType(new UserType[] { UserType.MerchantPrimary });
            var ret = (from p in DataContext.Reader.Products
                       join m in DataContext.Reader.tblCompanies on p.Merchant_id equals m.ID
                       join a in DataContext.Reader.Accounts on m.ID equals a.Merchant_id
                       where productIds.Contains(p.Product_id)
                       select new { p, a.Account_id, a.AccountNumber }).ToList().Select(x => new Product(x.p) { AccountNumber = x.AccountNumber, AccountID = x.Account_id }).ToList();
            var texts = ProductText.Load(ret.Select(p => p.ID).ToList(), languageIso).GroupBy(t => t.ProductID);
            foreach (var p in ret)
                p._texts = texts.Where(k => k.Key == p.ID).Select(k => k.ToList()).SingleOrDefault();
            return ret;
        }

        public static void ChangeCurrency(int merchantId, string currencyIso)
        {
            var prods = from p in DataContext.Writer.Products where p.Merchant_id == merchantId select p;
            foreach (var prod in prods)
                prod.CurrencyISOCode = currencyIso;
            DataContext.Writer.SubmitChanges();
        }

        public static string GetDefaultCurrencyIso(int merchantId)
        {
            var prod = (from p in DataContext.Writer.Products where p.Merchant_id == merchantId select p).FirstOrDefault();
            if (prod == null)
                return null;

            return prod.CurrencyISOCode;
        }

        public void Delete()
        {
            if (_entity.Product_id == 0) return;
            //User user = SecurityManager.GetInternalUsezr(new UserType[] { UserType.MerchantPrimary });
            DataContext.Writer.ExecuteCommand("Update Data.CartProduct Set ProductStock_id = Null, Product_id = Null Where Product_id={0}", ID);
            DataContext.Writer.ProductTags.DeleteAllOnSubmit(from t in DataContext.Writer.ProductTags where t.Product_id == ID select t);
            DataContext.Writer.ProductStockReferences.DeleteAllOnSubmit(from t in DataContext.Writer.ProductStockReferences where t.Product_id == ID select t);
            DataContext.Writer.ProductStocks.DeleteAllOnSubmit(from t in DataContext.Writer.ProductStocks where t.Product_id == ID select t);
            DataContext.Writer.Products.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }

        private List<Property> _properties;
        public List<Property> Properties
        {
            get
            {
                if (_properties == null)
                {
                    var propIdList = (from sr in DataContext.Reader.ProductStockReferences where sr.Product_id == ID select sr.ProductProperty_id).Distinct().ToList();
                    _properties = Property.Load(propIdList, false, true);
                }
                return _properties;
            }
        }

        private List<ProductText> _texts;
        public List<ProductText> Texts
        {
            get
            {
                if (_texts == null)
                    _texts = ProductText.Load(new int[] { ID }.ToList(), null);
                return _texts;
            }
        }

        private List<string> _tags;
        public List<string> Tags
        {
            get
            {
                if (_tags == null)
                    _tags = (from sr in DataContext.Reader.ProductTags where sr.Product_id == ID select sr.Tag).Distinct().ToList();
                return _tags;
            }
            set
            {
                _tags = value.Distinct().ToList();
            }
        }

        private List<Stock> _stocks;
        public List<Stock> Stocks
        {
            get
            {
                if (_stocks == null) _stocks = Stock.Search(ID);
                return _stocks;
            }
        }

        private List<short> _categories;
        public List<short> Categories
        {
            get
            {
                if (_categories == null)
                    _categories = (from sr in DataContext.Reader.ProductToProductCategories where sr.Product_id == ID select sr.ProductCategory_id).ToList();
                return _categories;
            }
            set
            {
                _categories = value;
            }
        }

        public ProductText GetTextForLanguage(string languageCode)
        {
            ProductText text = null;
            if (!string.IsNullOrEmpty(languageCode))
            {
                if (languageCode.Length > 2) languageCode = languageCode.Substring(0, 2);
                text = Texts.Where(t => languageCode.Equals(t.LanguageISOCode, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                if (text != null) return text;
            }
            text = Texts.Where(t => t.IsDefault).FirstOrDefault();
            if (text != null) return text;
            text = Texts.FirstOrDefault();
            if (text != null) return text;
            return new ProductText(ID, languageCode) { Name = "Text Undefined" };
        }

        public ProductText Text { get { return GetTextForLanguage(null); } }

        public class NearestProducts
        {
            public int? PrevProductId;
            public int? NextPrductId;
        }

        public NearestProducts GetNearestProducts()
        {
            NearestProducts np = new Product.NearestProducts();
            np.PrevProductId = (from p in DataContext.Reader.Products
                                where p.MerchantSetShop_id == ShopID && p.isActive && p.Product_id < ID
                                orderby p.Product_id descending
                                select p.Product_id).FirstOrDefault();
            if (np.PrevProductId == 0)
                np.PrevProductId = null;
            np.NextPrductId = (from p in DataContext.Reader.Products
                               where p.MerchantSetShop_id == ShopID && p.isActive && p.Product_id > ID
                               orderby p.Product_id ascending
                               select p.Product_id).FirstOrDefault();
            if (np.NextPrductId == 0)
                np.NextPrductId = null;
            return np;
        }

        /*
        public static int? GetNextProductId(int merchantId, int productId)
        {
            int? id = (from p in DataContext.Reader.Products
                       where p.Merchant_id == merchantId && p.isActive && p.Product_id > productId
                       select p.Product_id).FirstOrDefault();

            if (id == null)
            {
                id = (from p in DataContext.Reader.Products
                      where p.Merchant_id == merchantId && p.isActive
                      orderby p.Product_id ascending
                      select p.Product_id).FirstOrDefault();
            }

            return id;
        }

        public static int? GetPrevProductId(int merchantId, int productId)
        {
            int? id = (from p in DataContext.Reader.Products
                       where p.Merchant_id == merchantId && p.isActive && p.Product_id < productId
                       select p.Product_id).FirstOrDefault();

            if (id == null)
            {
                id = (from p in DataContext.Reader.Products
                      where p.Merchant_id == merchantId && p.isActive
                      orderby p.Product_id descending
                      select p.Product_id).FirstOrDefault();
            }

            return id;
        }
        */

        public class CategorisedProduct
        {
            public short CategoryId { get; set; }
            public string CategoryName { get; set; }
            public string ProductName { get; set; }
            public string ProductDescription { get; set; }
            public int ProductId { get; set; }
            public string CurencyIso { get; set; }
            public decimal Price { get; set; }
            public string ImageFileName { get; set; }
            public string ImageSmallFileName
            {
                get
                {
                    if (string.IsNullOrEmpty(ImageFileName))
                        return null;

                    var smallImageFileName = $"UIServices/{Path.GetFileNameWithoutExtension(ImageFileName)}_small{Path.GetExtension(ImageFileName)}";
                    var smallImageFileFullName = Accounts.Account.MapPublicPath(AccountNumber, smallImageFileName);
                    if (!File.Exists(smallImageFileFullName))
                        return null;

                    return smallImageFileName;
                }
                set { }
            }
            public string AccountNumber { get; private set; }
            public string ImageURL
            {
                get
                {
                    if (string.IsNullOrEmpty(ImageFileName))
                        return null;
                    return Accounts.Account.MapPublicVirtualPath(AccountNumber, ImageFileName);
                }
                set { }
            }
            public string ImageSmallURL
            {
                get
                {
                    if (string.IsNullOrEmpty(ImageSmallFileName))
                        return null;
                    return Accounts.Account.MapPublicVirtualPath(AccountNumber, ImageSmallFileName);
                }
                set { }
            }
            public bool IsDynamic { get; set; }
            public bool IsRecurring { get; set; }
        }

        public static List<CategorisedProduct> GetCategorised(int shopId, int itemsPerCategory)
        {
            string sql = $@"WITH TopN AS (
                            SELECT [Data].ProductToProductCategory.Product_id, [Data].ProductToProductCategory.ProductCategory_id, ROW_NUMBER()
                            OVER(
                                PARTITION BY ProductCategory_id
                                order by ProductCategory_id
                            ) AS RowNo
                            FROM [Data].ProductToProductCategory
                            JOIN [Data].Product product on[Data].ProductToProductCategory.Product_id = product.Product_id
                            WHERE product.MerchantSetShop_id = {shopId} AND product.isActive = 1
                        )

                        SELECT product.Product_id as ProductId, tblCompany.CustomerNumber as AccountNumber, product.CurrencyISOCode as CurencyIso, product.Price, product.ImageFileName, category.ProductCategory_id as CategoryId, category.Name as CategoryName, productText.Name as ProductName, productText.[Description] as ProductDescription, isDynamicPrice as IsDynamic, CAST(CASE WHEN LEN([RecurringString]) > 0 THEN 1 ELSE 0 END AS bit) AS IsRecurring FROM TopN 
                        JOIN [Data].Product product on TopN.Product_id = product.Product_id
                        JOIN [List].ProductCategory category on TopN.ProductCategory_id = category.ProductCategory_id
                        JOIN [Data].ProductText productText on TopN.Product_id = productText.Product_id
						JOIN tblCompany on [Merchant_id] = tblCompany.ID
                        WHERE RowNo <= {itemsPerCategory} AND category.ParentID is NULL";
            var prods = DataContext.Reader.ExecuteQuery<CategorisedProduct>(sql).ToList();

            return prods;
        }

        public static List<Product> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            var exp = (from p in DataContext.Reader.Products
                       join m in DataContext.Reader.tblCompanies on p.Merchant_id equals m.ID
                       join a in DataContext.Reader.Accounts on m.ID equals a.Merchant_id
                       orderby p.SortOrder ascending
                       select new { p, a.Account_id, a.AccountNumber }).AsQueryable();

            if (filters != null)
            {
                if (filters.ID.From != null) exp = exp.Where(p => p.p.Product_id >= filters.ID.From);
                if (filters.ID.To != null) exp = exp.Where(p => p.p.Product_id <= filters.ID.To);

                if (filters.Price.From != null) exp = exp.Where(p => p.p.Price >= filters.Price.From);
                if (filters.Price.To != null) exp = exp.Where(p => p.p.Price <= filters.Price.To);

                if (filters.MerchantId != null) exp = exp.Where(p => p.p.Merchant_id == filters.MerchantId.GetValueOrDefault());
                if (filters.ShopId != null)
                    exp = exp.Where(p => p.p.MerchantSetShop_id == filters.ShopId);

                if (!string.IsNullOrEmpty(filters.Name)) exp = exp.Where(p => (from t in DataContext.Reader.ProductTexts where t.Name.Contains(filters.Name) && t.Product_id == p.p.Product_id select t.Product_id).Contains(p.p.Product_id));
                if (!string.IsNullOrEmpty(filters.Text))
                {
                    if (filters.Tags == null) filters.Tags = new List<string>();
                    filters.Tags.AddRange(filters.Text.Split(' ').Select(t => t.Trim().ToLower()));
                }
                if (filters.Tags != null && filters.Tags.Count > 0) exp = exp.Where(p => (from t in DataContext.Reader.ProductTags where filters.Tags.Contains(t.Tag) && t.Product_id == p.p.Product_id select t.Product_id).Contains(p.p.Product_id));
                if (filters.IsActive.HasValue) exp = exp.Where(p => p.p.isActive == filters.IsActive);
                if (filters.Categoryies != null && filters.Categoryies.Count > 0) exp = exp.Where(p => (from t in DataContext.Reader.ProductToProductCategories where filters.Categoryies.Contains(t.ProductCategory_id) && t.Product_id == p.p.Product_id select t.Product_id).Contains(p.p.Product_id));
                if (filters.MerchantGroupID != null && filters.MerchantGroupID.Count > 0) exp = exp.Where(p => (from c in DataContext.Reader.tblCompanies where filters.MerchantGroupID.Contains(c.GroupID.Value) select c.ID).Contains(p.p.Merchant_id));

                if (filters.regions != null && filters.regions.Count > 0)
                {
                    if (filters.includeGlobalRegion != null && filters.includeGlobalRegion.Value)
                        exp = exp.Where(p => p.p.setting_MerchantSetShop.setting_MerchantSetShopToCountryRegions.Any(r => filters.regions.Contains(r.WorldRegionISOCode)) || p.p.setting_MerchantSetShop.setting_MerchantSetShopToCountryRegions.Count() == 0);
                    else
                        exp = exp.Where(p => p.p.setting_MerchantSetShop.setting_MerchantSetShopToCountryRegions.Any(r => filters.regions.Contains(r.WorldRegionISOCode)));
                }

                if (filters.countries != null && filters.countries.Count > 0)
                {
                    if (filters.includeGlobalRegion != null && filters.includeGlobalRegion.Value)
                        exp = exp.Where(p => p.p.setting_MerchantSetShop.setting_MerchantSetShopToCountryRegions.Any(r => filters.countries.Contains(r.CountryISOCode)) || p.p.setting_MerchantSetShop.setting_MerchantSetShopToCountryRegions.Count() == 0);
                    else
                        exp = exp.Where(p => p.p.setting_MerchantSetShop.setting_MerchantSetShopToCountryRegions.Any(r => filters.countries.Contains(r.CountryISOCode)));
                }
            }
            var ret = exp.ApplySortAndPage(sortAndPage).Select(p => new Product(p.p) { AccountID = p.Account_id, AccountNumber = p.AccountNumber }).ToList();
            var texts = ProductText.Load(ret.Select(p => p.ID).ToList(), filters.LanguageIso).GroupBy(t => t.ProductID);
            foreach (var p in ret)
                p._texts = texts.Where(k => k.Key == p.ID).Select(k => k.ToList()).SingleOrDefault();
            return ret;
        }

    }
}
