﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Shop.Products
{
	public class ProductText : BaseDataObject
	{
		private bool _isNew = true;
		private Dal.Netpay.ProductText _entity;
		public int ProductID { get { return _entity.Product_id;} set { _entity.Product_id = value; } }
		public string LanguageISOCode { get { return _entity.LanguageISOCode; } }
		public string Name { get { return _entity.Name; } set { _entity.Name = value.EmptyIfNull().Truncate(200); } }
		public string Description { get { return _entity.Description; } set { _entity.Description = value.EmptyIfNull().Truncate(1000); } }
		public string ReceiptText { get { return _entity.ReceiptText; } set { _entity.ReceiptText = value.EmptyIfNull().Truncate(600); } }
		public string ReceiptLink { get { return _entity.ReceiptLink; } set { _entity.ReceiptLink = value.EmptyIfNull().Truncate(600); } }
		public bool IsDefault { get { return _entity.IsDefaultLanguage; } set { _entity.IsDefaultLanguage = value; } }
		public string MetaTitle { get { return _entity.MetaTitle; } set { _entity.MetaTitle = value.EmptyIfNull().Truncate(100); } }
		public string MetaDescription { get { return _entity.MetaDescription; } set { _entity.MetaDescription = value.EmptyIfNull().Truncate(250); } }
		public string MetaKeyword { get { return _entity.MetaKeyword; } set { _entity.MetaKeyword = value.EmptyIfNull().Truncate(250); } }
		
		private ProductText(Netpay.Dal.Netpay.ProductText entity)
		{ 
			_entity = entity;
			_isNew = false;
		}

		public ProductText(int productId, string isoCode)
		{
			_entity = new Dal.Netpay.ProductText();
			_entity.Product_id = productId;
			_entity.LanguageISOCode = isoCode;
		}

		public void Save()
		{
			DataContext.Writer.ProductTexts.Update(_entity, !_isNew);
			DataContext.Writer.SubmitChanges();
			_isNew = false;
		}

		public static List<ProductText> Load(List<int> productIds, string language)
		{
			//context.IsUserOfType(new UserType[] { UserType.MerchantPrimary });
			var exp = from lu in DataContext.Reader.ProductTexts select lu;
			if (productIds != null) exp = exp.Where(lu => productIds.Contains(lu.Product_id));
			if (!string.IsNullOrEmpty(language)) exp = exp.Where(lu => (language == lu.LanguageISOCode || lu.IsDefaultLanguage));
			return exp.Select(lu => new ProductText(lu)).ToList();
		}

		public void Delete()
		{
			//User user = SecurityManager.GetInternalUsezr(new UserType[] { UserType.MerchantPrimary });
			if (_entity.Product_id == 0) return;
			DataContext.Writer.ProductTexts.Delete(_entity);
			DataContext.Writer.SubmitChanges();
		}		
	}
}
