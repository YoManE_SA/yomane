﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure;

namespace Netpay.Bll.Transactions
{
	public class Payment : BaseDataObject, PaymentMethods.IStoredMethod
	{
        public static SecuredObject SecuredObject { get { return Transaction.SecuredObject; } }

        public class SearchFilters {
            public string IssuerCountryIsoCode;
            public string AccountValue1;
			public string First6Digits;
			public string Last4Digits;
			public DateTime? ExpirationDate;
			public CommonTypes.PaymentMethodEnum? PaymentMethod;
			public BillingAddress.SearchFilters BillingAddressFilters;
		}

		protected Netpay.Dal.Netpay.TransPaymentMethod _entity;

		public int ID { get { return _entity.TransPaymentMethod_id; } }
		//public int Customer_id { get { return _entity.Customer_id; } set { _entity.Customer_id = value; } }
		//public int? BillingAddress_id { get { return _entity.CustomerAddress_id; } set { _entity.CustomerAddress_id = value; } }

		int PaymentMethods.IStoredMethod.PaymentMethodId { get { return _entity.PaymentMethod_id; } set { _entity.PaymentMethod_id = (short)value; } }
		System.Data.Linq.Binary PaymentMethods.IStoredMethod.Value1Encrypted { get { return _entity.Value1Encrypted; } set { _entity.Value1Encrypted = value; } }
		System.Data.Linq.Binary PaymentMethods.IStoredMethod.Value2Encrypted { get { return _entity.Value2Encrypted; } set { _entity.Value2Encrypted = value; } }
		string PaymentMethods.IStoredMethod.Value1First6Data { get { return _entity.Value1First6Text; } set { _entity.Value1First6Text = value; } }
		string PaymentMethods.IStoredMethod.Value1Last4Data { get { return _entity.Value1Last4Text; } set { _entity.Value1Last4Text = value; } }
		byte PaymentMethods.IStoredMethod.EncryptionKey { get { return _entity.EncryptionKey; } set { _entity.EncryptionKey = value; } }
		System.DateTime? PaymentMethods.IStoredMethod.ExpirationDate { get { return _entity.ExpirationDate; } set { _entity.ExpirationDate = value; } }
		private PaymentMethods.MethodData _methodInstance;
		public string IssuerCountryIsoCode { get { return _entity.IssuerCountryIsoCode; } set { _entity.IssuerCountryIsoCode = value; } }
		public PaymentMethods.MethodData MethodInstance { get { if (_methodInstance == null) _methodInstance = PaymentMethods.MethodData.Create(this); return _methodInstance; } set { _methodInstance = value; } }
        public int MerchantID { get { return _entity.Merchant_id; } set { _entity.Merchant_id = value; } }
        public int? OldCreditCardID { get { return _entity.CreditCard_id; } }

		public BillingAddress BillingAddress { get; set; }

		protected Payment(Dal.Netpay.TransPaymentMethod entity, Dal.Netpay.TransPaymentBillingAddress addressEntity)
		{ 
			_entity = entity;
			if (addressEntity != null) BillingAddress = new BillingAddress(addressEntity);
		}

		public Payment()
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            _entity = new Dal.Netpay.TransPaymentMethod();
		}


        public bool EqualsTo(PaymentMethods.IStoredMethod p)
        {
            return p.PaymentMethodId == _entity.PaymentMethod_id && p.Value1Encrypted == _entity.Value1Encrypted && p.Value2Encrypted == _entity.Value2Encrypted;
        }

		public static List<Payment> Load(List<int> ids)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var retExp = (from c in DataContext.Reader.TransPaymentMethods
				join a in DataContext.Reader.TransPaymentBillingAddresses on c.TransPaymentBillingAddress_id equals a.TransPaymentBillingAddress_id into na
				from ca in na.DefaultIfEmpty()
				where ids.Contains(c.TransPaymentMethod_id)
				select new Payment(c, ca));
			//if (user.Type == Infrastructure.UserType.Customer) retExp = retExp.Where(o => o.c.Customer_id == user.ID);
			return retExp.ToList();
		}

		//public byte[] GetEncyptedValue1 { get { return _entity.Value1Encrypted.ToArray(); } }

		public override ValidationResult Validate()
		{
			var ret = MethodInstance.Validate();
			if (ret != ValidationResult.Success) return ret;
			return ValidationResult.Success;
		}

		public void Save(Payer payerInfo = null)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            ValidationException.RaiseIfNeeded(Validate());
			if (BillingAddress != null) {
				_entity.TransPaymentBillingAddress_id = BillingAddress.Save(); 
			} else {
				if (_entity.TransPaymentBillingAddress_id != null)
				{
					var address = Bll.Transactions.BillingAddress.Load(_entity.TransPaymentBillingAddress_id.Value);
					if (address != null) address.Delete();
				}
				_entity.TransPaymentBillingAddress_id = null;
			}
            if (_methodInstance != null) _entity.PaymentMethodText = _methodInstance.Display;
            //backword compatibility
            if (ID == 0 && PaymentMethods.PaymentMethod.Get(_entity.PaymentMethod_id).Type == CommonTypes.PaymentMethodType.CreditCard)
            {
                var cc = new Netpay.Dal.Netpay.tblCreditCard() {
                    cc_InsertDate = DateTime.Now,
                    CCard_First6 = _entity.Value1First6Text.ToNullableInt32().GetValueOrDefault(),
                    CCard_Last4 = (short)_entity.Value1Last4Text.ToNullableInt32().GetValueOrDefault(),
                    CompanyID = MerchantID,
                    ExpMM = _entity.ExpirationDate.GetValueOrDefault().Month.ToString("00"),
                    ExpYY = _entity.ExpirationDate.GetValueOrDefault().Year.ToString("00"),
                    CCard_number256 = _entity.Value1Encrypted,
                    cc_cui = Infrastructure.Security.Encryption.Decrypt(_entity.Value2Encrypted.ToArray()),
                    Member = payerInfo != null ? payerInfo.FullName.EmptyIfNull() : "",
                    phoneNumber = payerInfo != null ? payerInfo.PhoneNumber.EmptyIfNull() : "",
                    PersonalNumber = payerInfo != null ? payerInfo.PersonalNumber.EmptyIfNull() : "",
                    email = payerInfo != null ? payerInfo.EmailAddress.EmptyIfNull() : "",
                    Comment = ""
                };
                DataContext.Writer.tblCreditCards.Update(cc, false);
                DataContext.Writer.SubmitChanges();
                _entity.CreditCard_id = cc.ID;
            }
			DataContext.Writer.TransPaymentMethods.Update(_entity, (_entity.TransPaymentMethod_id != 0));
			DataContext.Writer.SubmitChanges();
		}

		public void Delete()
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            if (_entity.TransPaymentMethod_id == 0) return;
			//if (BillingAddress != null) BillingAddress.DeleteAddress();
			DataContext.Writer.TransPaymentMethods.Delete(_entity);
			DataContext.Writer.SubmitChanges();
		}

		internal static IQueryable<Dal.Netpay.TransPaymentMethod> Search(Netpay.Dal.DataAccess.NetpayDataContext context, SearchFilters filters, out bool isEmptyFilters)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
            
            isEmptyFilters = true;
			var paymentExp = (from pm in context.TransPaymentMethods select pm);

            if (filters.PaymentMethod != null)
            {
                isEmptyFilters = false;
                paymentExp = paymentExp.Where(pm => pm.PaymentMethod_id == (int)filters.PaymentMethod);
            }


			if (filters.Last4Digits != null) {
				isEmptyFilters = false;
				paymentExp = paymentExp.Where(pm => pm.Value1Last4Text == filters.Last4Digits);
			}
			if (filters.First6Digits != null) {
				isEmptyFilters = false;
				paymentExp = paymentExp.Where(pm => pm.Value1First6Text == filters.First6Digits);
			}
			if (filters.ExpirationDate != null) {
				isEmptyFilters = false;
				paymentExp = paymentExp.Where(pm => pm.ExpirationDate == filters.ExpirationDate);
			}
			if (filters.AccountValue1 != null)
			{
				isEmptyFilters = false;
				paymentExp = paymentExp.Where(pm => pm.Value1Encrypted.Equals(new System.Data.Linq.Binary(Encryption.Encrypt(filters.AccountValue1))));
			}

            if (filters.IssuerCountryIsoCode != null)
            {
                isEmptyFilters = false;
                paymentExp = paymentExp.Where(pm => pm.IssuerCountryIsoCode == filters.IssuerCountryIsoCode);
            }

			if (filters.BillingAddressFilters != null) {
				bool isEmpty;
				var sq = BillingAddress.Search(context, filters.BillingAddressFilters, out isEmpty);
				if (!isEmpty) paymentExp = paymentExp.Where(pm => sq.Contains(pm.trans_TransPaymentBillingAddress));
			}
			return paymentExp;
		}
	}
}
