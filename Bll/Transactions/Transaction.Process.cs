using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Transactions
{
    public partial class Transaction
    {
		public System.Collections.Specialized.NameValueCollection GetProcessParamters(bool isRefund = false)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Add);

            var requestParams = new System.Collections.Specialized.NameValueCollection();
			var pmData = PaymentData;
			var payerData = PayerData;

			if (Merchant != null) requestParams.Add("CompanyNum", Merchant.Number);
			requestParams.Add("Currency", CurrencyIsoCode);
			requestParams.Add("Amount", Amount.ToString("0.00"));
			if (Installments != 0) requestParams.Add("Payments", Installments.ToString());
			if (CustomerID.HasValue) requestParams.Add("CustomerID", CustomerID.ToString());
			if (TransactionSource.HasValue) requestParams.Add("requestSource", ((int)TransactionSource).ToString());
			requestParams.Add("TransType", ((int)TransType).ToString());
			if (ProductId != null) requestParams.Add("MerchantProductId", ProductId.ToString());
			if (!string.IsNullOrEmpty(IP)) requestParams.Add("ClientIP", IP);
            if (!string.IsNullOrEmpty(OrderNumber)) requestParams.Add("Order", OrderNumber);
            if (pmData != null)
			{
				if (pmData.MethodInstance is PaymentMethods.CreditCard)
				{
					var cardData = pmData.MethodInstance as PaymentMethods.CreditCard;
					requestParams.Add("TypeCredit", isRefund? "0" : ((int)CreditType).ToString());
					requestParams.Add("CardNum", cardData.CardNumber);
					if (!string.IsNullOrEmpty(cardData.Cvv)) requestParams.Add("CVV2", cardData.Cvv);
					requestParams.Add("ExpMonth", cardData.ExpirationMonth.ToString("00"));
					requestParams.Add("ExpYear", cardData.ExpirationYear.ToString("00"));
					if (payerData != null) requestParams.Add("Member", payerData.FullName);
				}
				else if (pmData.MethodInstance is PaymentMethods.BankAccount)
				{
					var bankAccount = pmData.MethodInstance as PaymentMethods.BankAccount;
					requestParams.Add("CreditType", ((int)CreditType).ToString());
					requestParams.Add("PaymentMethod", ((int)bankAccount.PaymentMethodId).ToString());
					requestParams.Add("RoutingNumber", bankAccount.AccountNumber);
					requestParams.Add("AccountNumber", bankAccount.RoutingNumber);
					if (payerData != null) requestParams.Add("AccountName", payerData.FullName);
					//requestParams.Add("BankAccountType" & Server.URLEncode(trim(rsData4("BankAccountTypeId"))) & "&"
					//requestParams.Add("BankName=" + pmData.is
					//requestParams.Add("BankCity=" & Server.URLEncode(trim(rsData4("BankCity"))) & "&"
					//requestParams.Add("BankPhone=" & Server.URLEncode(trim(rsData4("BankPhone"))) & "&"
					//requestParams.Add("BankState=" & Server.URLEncode(trim(rsData4("BankState"))) & "&"
					if (!string.IsNullOrEmpty(pmData.IssuerCountryIsoCode)) requestParams.Add("BankCountry", pmData.IssuerCountryIsoCode);
				}
				else throw new Exception("methodType not supported");
				var address = pmData.BillingAddress;
				if (address != null)
				{
					if (!string.IsNullOrEmpty(address.AddressLine1)) requestParams.Add("BillingAddress1", address.AddressLine1);
					if (!string.IsNullOrEmpty(address.AddressLine2)) requestParams.Add("BillingAddress2", address.AddressLine2);
					if (!string.IsNullOrEmpty(address.City)) requestParams.Add("BillingCity", address.City);
					if (!string.IsNullOrEmpty(address.PostalCode)) requestParams.Add("BillingZipCode", address.PostalCode);
					if (!string.IsNullOrEmpty(address.StateISOCode)) requestParams.Add("BillingState", address.StateISOCode);
					if (!string.IsNullOrEmpty(address.CountryISOCode)) requestParams.Add("BillingCountry", address.CountryISOCode);
				}
			}
			if (payerData != null) {
				if (!string.IsNullOrEmpty(payerData.EmailAddress)) requestParams.Add("Email", payerData.EmailAddress);
				if (!string.IsNullOrEmpty(payerData.PersonalNumber)) requestParams.Add("PersonalNum", payerData.PersonalNumber);
				if (!string.IsNullOrEmpty(payerData.PhoneNumber)) requestParams.Add("PhoneNumber", payerData.PhoneNumber);
				if (payerData.DateOfBirth.HasValue) requestParams.Add("BirthDate", payerData.DateOfBirth.Value.ToString("yyy-MM-dd"));
				var address = payerData.ShippingDetails;
				if (address != null) {
					if (!string.IsNullOrEmpty(address.AddressLine1)) requestParams.Add("Shipping_Address1", address.AddressLine1);
					if (!string.IsNullOrEmpty(address.AddressLine2)) requestParams.Add("Shipping_Address2", address.AddressLine2);
					if (!string.IsNullOrEmpty(address.City)) requestParams.Add("Shipping_City", address.City);
					if (!string.IsNullOrEmpty(address.PostalCode)) requestParams.Add("Shipping_PostalCode", address.PostalCode);
					if (!string.IsNullOrEmpty(address.StateISOCode)) requestParams.Add("Shipping_StateIso", address.StateISOCode);
					if (!string.IsNullOrEmpty(address.CountryISOCode)) requestParams.Add("Shipping_CountryIso", address.CountryISOCode);
				}
			}
			if (!string.IsNullOrEmpty(PayForText)) requestParams.Add("PayFor", PayForText.Truncate(100));
			if (!string.IsNullOrEmpty(Comment)) requestParams.Add("Comment", Comment);
			//requestParams.Add("referringUrl=" & Server.URLEncode(Left(DBText(Request.ServerVariables("HTTP_REFERER")), 200)) & "&"
			//requestParams.Add("RefTransID", ID.ToString());
			return requestParams;
		}

		public static string GetProcessUrl(CommonTypes.PaymentMethodGroupEnum group)
		{
			var d = Infrastructure.Domain.Current;
			if (group == CommonTypes.PaymentMethodGroupEnum.CreditCard || group == CommonTypes.PaymentMethodGroupEnum.Prepaid)
				return string.Format("{0}remote_charge2.aspx?", d.ProcessUrl);
			else if (group == CommonTypes.PaymentMethodGroupEnum.DirectDebit || group == CommonTypes.PaymentMethodGroupEnum.EuropeanDirectDebit ||
					group == CommonTypes.PaymentMethodGroupEnum.InstantOnlinkBankTransfer || group == CommonTypes.PaymentMethodGroupEnum.OnlineBankTransfer)
				return string.Format("{0}remoteCharge_echeck.aspx?", d.ProcessUrl);
			return null;
		}

		public static System.Collections.Specialized.NameValueCollection ProcessTransaction(CommonTypes.PaymentMethodGroupEnum group, System.Collections.Specialized.NameValueCollection values)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Add);

            System.Text.Encoding serviceEncoding = System.Text.Encoding.GetEncoding("windows-1255");
			var sb = new StringBuilder();
			for (int i = 0; i < values.Count; i++)
				if (values[i] != null) sb.AppendFormat("&{0}={1}", values.GetKey(i), System.Web.HttpUtility.UrlEncode(values[i], serviceEncoding));
			if (sb.Length > 0) sb.Remove(0, 1);
			sb.Insert(0, GetProcessUrl(group));
			var response = ProcessTransaction(sb.ToString());
			return System.Web.HttpUtility.ParseQueryString(response, serviceEncoding);
		}

		public static string ProcessTransaction(string requestData)
		{
			try {
				var wc = new System.Net.WebClient();
				//response = wc.UploadString(requestData, "get");
				return wc.DownloadString(requestData);
			} catch (System.Net.WebException exception)
			{
				using (var reader = new System.IO.StreamReader(exception.Response.GetResponseStream()))
				{
					var response = reader.ReadToEnd();
					Netpay.Infrastructure.Logger.Log(LogSeverity.Error, LogTag.Process, "error while sending to remote charge", response);
					throw new Exception("Unexpected communication failure");
				}
			}
		}

    }
}
