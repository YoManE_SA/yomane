﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Transactions
{
    public partial class Transaction
    {
		public Transaction(Netpay.Dal.Netpay.tblCompanyTransApproval entity)
			: this(TransactionStatus.Authorized)
		{
			if (entity != null) LoadEntity(entity);
		}

		private void LoadEntity(Netpay.Dal.Netpay.tblCompanyTransApproval entity)
		{
            FraudDetectionLogId = entity.FraudDetectionLog_id;

			ID = entity.ID;
			Amount = entity.Amount;
			CurrencyID = entity.Currency.Value;
			Installments = entity.Payments;
			CreditType = (Infrastructure.CreditType) entity.CreditType;
            SystemText = entity.SystemText;

			DebitCompanyID = entity.DebitCompanyID;
			MerchantID = entity.CompanyID;
            CustomerID = entity.CustomerID;
            PayerDataID = entity.TransPayerInfo_id;
			PaymentDataID = entity.TransPaymentMethod_id;

			PaymentMethodID = (CommonTypes.PaymentMethodEnum)entity.PaymentMethod.GetValueOrDefault();
			PaymentMethodReferenceID = entity.PaymentMethodID;
			PaymentMethodDisplay = entity.PaymentMethodDisplay;
			TransactionSource = (Infrastructure.TransactionSource?) entity.TransSource_id;
            TransType = (TransactionType) entity.TransactionTypeID;
			InsertDate = entity.InsertDate;
			ApprovalNumber = entity.approvalNumber;
			PaymentsIDs = entity.PayID.ToString();
			PrimaryPaymentID = entity.PayID;
			PassedTransactionID = entity.TransAnswerID;
			CurrencyID = CurrencyID;
			Comment = entity.Comment;
			RecurringSeriesID = entity.RecurringSeries;
			IP = entity.IPAddress;
			Installments = entity.Payments;
			TerminalNumber = entity.TerminalNumber;
			ReferringUrl = entity.referringUrl;
			DebitReferenceCode = entity.DebitReferenceCode;
            DebitReferenceNum = entity.DebitReferenceNum;
			ProductId = entity.MerchantProduct_id;
			// fees
			TransactionFee = entity.netpayFee_transactionCharge;

            IsCardPresent = entity.IsCardPresent;
        }

        private static IQueryable<Dal.Netpay.tblCompanyTransApproval> SearchExpressionTransPreAuthorized(LoadOptions loadoptions, SearchFilters filters)
        {
            DataContext.Reader.CommandTimeout = 120;

            var expression = (IQueryable<Netpay.Dal.Netpay.tblCompanyTransApproval>)from t in DataContext.Reader.tblCompanyTransApprovals orderby t.InsertDate descending select t;
            if (Login.Current.Role == UserRole.Partner)
            {
                if (filters.merchantIDs == null) filters.merchantIDs = new List<int>();
                if (filters.customersIDs == null) filters.customersIDs = new List<int>();
                expression = expression.Where(t => filters.merchantIDs.Contains(t.CompanyID.Value) || filters.customersIDs.Contains(t.CustomerID));
            }
            else
            {
                if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
                    expression = expression.Where(t => filters.merchantIDs.Contains(t.CompanyID.Value));
                if (filters.customersIDs != null && filters.customersIDs.Count > 0)
                    expression = expression.Where(t => filters.customersIDs.Contains(t.CustomerID));
            }

            // IsChargeBack - Not exist in tblCompanyTransApproval

            //Date 
            if (filters.date.From != null)
                expression = expression.Where(t => t.InsertDate >= filters.date.From.Value.MinTime());
            if (filters.date.To != null)
                expression = expression.Where(t => t.InsertDate <= filters.date.To.Value.MaxTime());

            //Specific Transaction ID
            if (filters.specificTransactionID.HasValue)
            {
                expression = expression.Where(t => t.ID == filters.specificTransactionID.Value);
            }

            //Transaction ID range
            if (filters.ID.From != null)
                expression = expression.Where(t => t.ID >= filters.ID.From.Value);
            if (filters.ID.To != null)
                expression = expression.Where(t => t.ID <= filters.ID.To.Value);

            //Terminal Number
            if (!string.IsNullOrWhiteSpace(filters.debitTerminalNumber))
                expression = expression.Where(t => t.TerminalNumber == filters.debitTerminalNumber);

            //Debit Company
            if (filters.debitCompanyID != null)
                expression = expression.Where(t => t.DebitCompanyID == filters.debitCompanyID.Value);

            //Payment Method
            if (filters.paymentMethodID != null)
                expression = expression.Where(t => t.PaymentMethod == filters.paymentMethodID.Value);

            //Credit Types 
            //if (filters.creditTypes != null && filters.creditTypes.Count() > 0)
            //	expression = expression.Where(t => filters.creditTypes.Contains(t.CreditType));
            if (filters.creditTypes != null && filters.creditTypes.Count() > 0)
            {
                //If the search include "Regular" type , return "Regular" and 
                //also values that are different than 0-"Refund" and 8-"Installments"
                if (filters.creditTypes.Contains(1))
                {
                    expression = expression.Where(t => (filters.creditTypes.Contains(t.CreditType)) || (t.CreditType != 0 && t.CreditType != 8));
                }
                else
                {
                    expression = expression.Where(t => (filters.creditTypes.Contains(t.CreditType)));
                }
            }

            //Order ID
            if (filters.orderID != null)
                expression = expression.Where(t => t.OrderNumber.StartsWith(filters.orderID));

            //Currency ID
            if (filters.currencyID != null)
                expression = expression.Where(t => t.Currency == filters.currencyID);

            //Amount
            if (filters.amount.From != null)
                expression = expression.Where(t => t.Amount >= filters.amount.From);
            if (filters.amount.To != null)
                expression = expression.Where(t => t.Amount <= filters.amount.To);

            //Reply Code
            if (filters.replyCode != null)
                expression = expression.Where(t => t.replyCode == filters.replyCode);

            //Payment Method Id's
            if (filters.paymentMethodsIds != null)
                expression = expression.Where(t => filters.paymentMethodsIds.Contains(t.PaymentMethod.Value));

            if (filters.PayID != null)
                expression = expression.Where(t => t.PayID == filters.PayID.Value);

            if (filters.deviceId != null)
                expression = expression.Where(t => t.MobileDevice_id == filters.deviceId);

            if (filters.HistoryTypes != null)
                expression = expression.Where(t => DataContext.Reader.TransHistories.Where(v => filters.HistoryTypes.Contains((CommonTypes.TransactionHistoryType)v.TransHistoryType_id)).Select(v => v.TransPreAuth_id).Contains(t.ID));

            //Device ID
            if (filters.deviceId != null)
                expression = expression.Where(t => t.MobileDevice_id == filters.deviceId);

            //Payment Status - Check what is the meaning of PayId column in DataBase

            //Payment Status bank dropdown - Check if it is relevant

            //Debit ref' code
            if (!string.IsNullOrEmpty(filters.debitReferenceCode))
            {
                expression = expression.Where(t => t.DebitReferenceCode == filters.debitReferenceCode.Trim());
            }

            //Debit ref' num.
            if (!string.IsNullOrEmpty(filters.debitReferenceNumber))
            {
                expression = expression.Where(t => t.DebitReferenceNum == filters.debitReferenceNumber.Trim());
            }

            //Approval Number
            if (!string.IsNullOrEmpty(filters.debitApprovalNumber))
            {
                expression = expression.Where(t => t.approvalNumber == filters.debitApprovalNumber);
            }

            //UnsettledOnly - 'UnsettledInstallments' column - Not exist in tblCompanyTransApproval

            //Denied Status - Not exist in tblCompanyTransApproval

            //Chb Reason - Check if relevant.

            //Denied date - not exist

            //Is Test
            if (filters.isTest != null)
                expression = expression.Where(t => t.isTestOnly);

            //Original Transaction Id - not exist

            //IsPendingChargeBack - not exist 

            //Batch ID - not exist

            //Payer Filters
            if (filters.PayerFilters != null)
            {
                bool isEmpty;
                var sq = Payer.Search(DataContext.Reader, filters.PayerFilters, out isEmpty);
                if (!isEmpty) expression = expression.Where(t => sq.Contains(t.trans_TransPayerInfo));
            }

            //Payment Filters
            if (filters.PaymentFilters != null)
            {
                bool isEmpty;
                var sq = Payment.Search(DataContext.Reader, filters.PaymentFilters, out isEmpty);
                if (!isEmpty) expression = expression.Where(t => sq.Contains(t.trans_TransPaymentMethod));
            }

            return expression;
        }


		private static List<Transaction> SearchTransPreAuthorized(LoadOptions loadOptions, SearchFilters filters)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Read);

            var expression = SearchExpressionTransPreAuthorized(loadOptions, filters);

            List<Transaction> transactions = expression.ApplySortAndPage(loadOptions.sortAndPage).Select(t => new Transaction(t)).ToList<Transaction>();
			return transactions;
		}

		public CaptureResult CaptureTransaction(decimal amount, string comment, out int? captureTransID)
		{
			captureTransID = null;
			if (amount <= 0) return CaptureResult.InvalidAmount;
			//User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

			//NetpayDataContext dc = DataContext;
			//var transaction = Transactions.GetTransaction(transactionID, TransactionStatus.Authorized);
			//if (transaction == null) return CaptureResult.TransactionNotFound;
			if (PassedTransactionID != null) return CaptureResult.AlreadyCaptured;
			if (Amount * (decimal)1.1 < amount) return CaptureResult.AmountTooLarge;

			// build request
			StringBuilder requestBuilder = new StringBuilder("TransType=2&TypeCredit=1");
			requestBuilder.AppendFormat("&CompanyNum={0}", Merchant.Number);
			requestBuilder.AppendFormat("&TransApprovalID={0}", ID);
			requestBuilder.AppendFormat("&Currency={0}", CurrencyID);
			requestBuilder.AppendFormat("&Order={0}", OrderNumber);
			requestBuilder.AppendFormat("&Amount={0}", amount);
			requestBuilder.AppendFormat("&Comment={0}", comment);

			// http request
			string httpReply;
			var httpStatus = HttpClient.SendHttpRequest(Domain.ProcessUrl + "remote_charge2.aspx", out httpReply, requestBuilder.ToString());
			if (httpStatus != System.Net.HttpStatusCode.OK)
				return CaptureResult.ConnectionError;

			// params
			var replyParams = System.Web.HttpUtility.ParseQueryString(httpReply);
			string replyCode = replyParams["Reply"];
			string replyTransactionID = replyParams["TransID"];
			if (replyCode == null || replyTransactionID == null)
				return CaptureResult.BadReplyFormat;
			int parsedTransactionID;
			if (int.TryParse(replyTransactionID, out parsedTransactionID))
				captureTransID = parsedTransactionID;
			else
			{
				captureTransID = null;
				return CaptureResult.BadReplyFormat;
			}

			// reply
			switch (replyCode)
			{
				case "000": return CaptureResult.OK;
				case "001": return CaptureResult.AttemptPending;
				default: return CaptureResult.AttemptDeclined;
			}
		}
/*
		public static bool? IsAutoCaptureOK(ref NetpayDataContext dc, int AuthTransactionID)
		{
			var expression = (IQueryable<tblAutoCapture>)from t in dc.tblAutoCaptures where t.AuthorizedTransactionID == AuthTransactionID select t;
			AutoCaptureVO ac = new AutoCaptureVO(expression.FirstOrDefault());
			if (ac.CaptureTransactionID != null) return true;
			if (ac.DeclineTransactionID != null) return false;
			return null;
		}
*/
        protected void SaveApproval()
        {
            var entity = new Netpay.Dal.Netpay.tblCompanyTransApproval();
            if (PaymentData != null && PaymentData.OldCreditCardID != null)
            {
                entity.PaymentMethod_id = 1;
                entity.CreditCardID = PaymentMethodReferenceID;
            }
            //
            entity.FraudDetectionLog_id = FraudDetectionLogId;
            //

            entity.replyCode = ReplyCode;
            entity.Amount = Amount;
            entity.Currency = CurrencyID;
            entity.Payments = Installments;
            entity.CreditType = (byte)CreditType;
            entity.DebitCompanyID = DebitCompanyID;
            entity.CompanyID = MerchantID.GetValueOrDefault();
            entity.CustomerID = CustomerID.GetValueOrDefault();
            entity.TransPayerInfo_id = PayerDataID;
            entity.TransPaymentMethod_id = PaymentDataID;
            entity.PaymentMethod = (short?)PaymentMethodID;
            entity.PaymentMethodID = PaymentMethodReferenceID;
            entity.PaymentMethodDisplay = PaymentMethodDisplay.EmptyIfNull();
            entity.isTestOnly = IsTest;
            entity.TransSource_id = (byte?)TransactionSource;
            entity.TransactionTypeID = (int)TransType;
            entity.PayID = PrimaryPaymentID;
            entity.InsertDate = InsertDate;
            //entity.DeniedStatus = DeniedStatusID;
            entity.OrderNumber = OrderNumber;
            entity.Comment = Comment;
            entity.IPAddress = IP;
            entity.TerminalNumber = TerminalNumber;
            entity.referringUrl = ReferringUrl.EmptyIfNull();
            //entity.PayerInfo_id = PayerIDUsed.EmptyIfNull();
            entity.DebitReferenceCode = DebitReferenceCode;
            entity.DebitReferenceNum = DebitReferenceNum;
            //entity.IPCountry = IPCountry.EmptyIfNull();
            entity.PayforText = PayForText;
            entity.MerchantProduct_id = ProductId;
            // fees
            entity.netpayFee_transactionCharge = TransactionFee;
            entity.DebitFee = DebitFee;

            DataContext.Writer.tblCompanyTransApprovals.InsertOnSubmit(entity);
            DataContext.Writer.SubmitChanges();
            ID = entity.ID;
        }
    }
}
