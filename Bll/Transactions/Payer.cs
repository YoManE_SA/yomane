﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure;

namespace Netpay.Bll.Transactions
{
	public class Payer : BaseDataObject, IPersonalInfo
	{
        public static SecuredObject SecuredObject { get { return Transaction.SecuredObject; } }

        public class SearchFilters
		{
			public int? MerchantID;
			public string FirstName;
			public string LastName;
			public string InvoiceName;
			public string PersonalNumber;
			public string PhoneNumber;
			public string EmailAddress;
			public string Comment;
			public DateTime? DateOfBirth;
			public ShippingDetails.SearchFilters ShippingDetailsFilters;
		}

		protected Netpay.Dal.Netpay.TransPayerInfo _entity;
		public int ID { get { return _entity.TransPayerInfo_id; } }

		public string FullName { get { return PersonalInfo.MakeFullName(FirstName, LastName); } set { string a, b; PersonalInfo.SplitFullName(value, out a, out b); FirstName = a; LastName = b; } }
		public string FirstName { get { return _entity.FirstName; } set { _entity.FirstName = value; } }
		public string LastName { get { return _entity.LastName; } set { _entity.LastName = value; } }
		public string PersonalNumber { get { return _entity.PersonalNumber; } set { _entity.PersonalNumber = value; } }
		public string PhoneNumber { get { return _entity.PhoneNumber; } set { _entity.PhoneNumber = value; } }
		public string EmailAddress { get { return _entity.EmailAddress; } set { _entity.EmailAddress = value; } }
		public DateTime? DateOfBirth { get { return _entity.DateOfBirth; } set { _entity.DateOfBirth = value; } }

		public string InvoiceName { get { return _entity.InvoiceName; } set { _entity.InvoiceName = value; } }
		public string Comment { get { return _entity.Comment; } set { _entity.Comment = value; } }
		public ShippingDetails ShippingDetails { get; set; }

		protected Payer(Dal.Netpay.TransPayerInfo entity, Dal.Netpay.TransPayerShippingDetail addressEntity)
		{ 
			_entity = entity;
			if (addressEntity != null) ShippingDetails = new ShippingDetails(addressEntity);
		}

		public Payer()
		{
			_entity = new Dal.Netpay.TransPayerInfo();
		}

		public static List<Payer> Load(List<int> ids)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            //User user = Transaction.validateUser(credentialsToken);
            var retExp = (from c in DataContext.Reader.TransPayerInfos
						  join a in DataContext.Reader.TransPayerShippingDetails on c.TransPayerShippingDetail_id equals a.TransPayerShippingDetail_id into na
				from ca in na.DefaultIfEmpty()
				where ids.Contains(c.TransPayerInfo_id)
						  select new Payer(c, ca));
			//if (user.Type == Infrastructure.UserType.Customer) retExp = retExp.Where(o => o.c.Customer_id == user.ID);
			return retExp.ToList();
		}

        public void Save()
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            DataContext.Writer.TransPayerInfos.Update(_entity, (_entity.TransPayerInfo_id != 0));
            DataContext.Writer.SubmitChanges();
        }

		public class PayerUsage {
			public string FirstName { get; set; }
			public string LastName { get; set; }
			public string EmailAddress { get; set; }
			public string PhoneNumber { get; set; }
			public int Count { get; set; }
		}
		public static List<PayerUsage> Search(SearchFilters filters)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            bool isEmpty = false;
			//var user = Transaction.validateUser(credentialsToken);
			if (filters == null) filters = new SearchFilters();
			if (ObjectContext.Current.User.IsInRole(UserRole.Merchant))
				filters.MerchantID = Merchants.Merchant.Current.ID;
			var exp = Search(DataContext.Reader, filters, out isEmpty);
			return (from x in exp 
				group x by new { x.FirstName, x.LastName, x.EmailAddress, x.PhoneNumber } into xg orderby xg.Count() descending
				select new PayerUsage() { 
					Count = xg.Count(), 
					FirstName = xg.Key.FirstName, 
					LastName = xg.Key.LastName, 
					PhoneNumber = xg.Key.PhoneNumber, 
					EmailAddress = xg.Key.EmailAddress }).ToList();
		}

		internal static IQueryable<Dal.Netpay.TransPayerInfo> Search(Netpay.Dal.DataAccess.NetpayDataContext context, SearchFilters filters, out bool isEmptyFilters)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
            
            isEmptyFilters = true;
			var expression = (from p in context.TransPayerInfos select p);
			if (filters.MerchantID != null)
			{
				isEmptyFilters = false;
				expression = expression.Where(p => p.Merchant_id == filters.MerchantID);
			}

			if (filters.PhoneNumber != null) {
				isEmptyFilters = false;
				expression = expression.Where(p => p.PhoneNumber == filters.PhoneNumber);
			}
			if (filters.PersonalNumber != null) {
				isEmptyFilters = false;
				expression = expression.Where(p => p.PersonalNumber == filters.PersonalNumber.Trim());
			}
			if (filters.FirstName != null) {
				isEmptyFilters = false;
				expression = expression.Where(p => p.FirstName == filters.FirstName.Trim());
			}
			if (filters.LastName != null)
			{
				isEmptyFilters = false;
				expression = expression.Where(p => p.LastName == filters.LastName.Trim());
			}
			if (filters.EmailAddress != null)
			{
				isEmptyFilters = false;
				expression = expression.Where(p => p.EmailAddress == filters.EmailAddress.Trim());
			}
			if (filters.InvoiceName != null) {
				isEmptyFilters = false;
				expression = expression.Where(p => p.InvoiceName.Contains(filters.InvoiceName.Trim()));
			}
			if (filters.Comment != null) {
				isEmptyFilters = false;
				expression = expression.Where(p => p.Comment.Contains(filters.Comment.Trim()));
			}
			if (filters.ShippingDetailsFilters != null) {
				bool isEmpty;
				var sq = ShippingDetails.Search(context, filters.ShippingDetailsFilters, out isEmpty);
				if (!isEmpty) expression = expression.Where(pm => sq.Contains(pm.trans_TransPayerShippingDetail));
			}
			return expression;
		}
	}
}
