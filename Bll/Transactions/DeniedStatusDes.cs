﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Bll.Transactions
{
    public class DeniedStatusDes : BaseDataObject
    {
        private Dal.Netpay.TransDeniedStatus _entity;

        public int DeniedStatusNumber { get { return _entity.DeniedStatus; } }
        public string DeniedStatusDescription { get { return _entity.Name; } }

        private DeniedStatusDes (Dal.Netpay.TransDeniedStatus entity)
        {
            _entity = entity;
        }

        public static List<DeniedStatusDes> Cache
        {
            get
            {
                return Domain.Current.GetCachData("DeniedStatusDes", () =>
               {
                   return new Domain.CachData((from tds in DataContext.Reader.TransDeniedStatus select new DeniedStatusDes(tds)).ToList(), DateTime.Now.AddHours(6));
               }) as List<DeniedStatusDes>;
            }
        }

        public static string GetDecsctiptionByStatusNumber(int deniedStatusNum)
        {
            return Cache.Where(x => x.DeniedStatusNumber == deniedStatusNum).SingleOrDefault().DeniedStatusDescription;
        }
    }
}
