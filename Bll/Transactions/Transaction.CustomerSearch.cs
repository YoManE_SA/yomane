﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Bll.Accounts;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Transactions
{
	public partial class TransactionBasicInfo
    {
		private TransactionBasicInfo(Netpay.Dal.Netpay.Trans_SpGetPayerTransactionsResult entity)
		{
			LoadEntity(entity);
		}

		private void LoadEntity(Netpay.Dal.Netpay.Trans_SpGetPayerTransactionsResult entity)
		{
			ID = entity.TransID.GetValueOrDefault();
			Status = (Infrastructure.TransactionStatus) entity.TransactionStatus.GetValueOrDefault();
			Amount = entity.TransAmount.GetValueOrDefault();
			CurrencyID = entity.TransCurrency.GetValueOrDefault();
			Installments = (byte) entity.TransInstallments.GetValueOrDefault(1);
			ReplyCode = entity.ReplyCode;

			PaymentMethodID = (CommonTypes.PaymentMethodEnum)entity.PaymentMethod_id.GetValueOrDefault();
			PaymentMethodDisplay = entity.PaymentMethodText;
			IsTest = entity.TransIsTestOnly.GetValueOrDefault();
			TransactionSource = (Infrastructure.TransactionSource)entity.TransSource_id.GetValueOrDefault();
			InsertDate = entity.TransDate.GetValueOrDefault();
			TransType = (TransactionType) entity.TransType.GetValueOrDefault();
			CreditType = (Infrastructure.CreditType)entity.TransCreditType.GetValueOrDefault();

			PrimaryPaymentID = entity.PrimaryPayedID.GetValueOrDefault();
			PaymentsIDs = entity.PayID;
			ApprovalNumber = entity.ApprovalNumber;
			PassedTransactionID = entity.TransAnswerID;
			//chb
			ChargebackDate = entity.ChbDate.GetValueOrDefault();
			DeniedStatusID = (byte) entity.ChbStatus.GetValueOrDefault();
			IsPendingChargeback = entity.isPendingChargeback.GetValueOrDefault() == 1;
			MerchantID = entity.Merchant_id;
			// fees
			TransactionFee = entity.TransactionChargeFee.GetValueOrDefault();
		}

		public static List<TransactionBasicInfo> SearchPayerTransactions(int? merchantId, string emailAddress, string firstName, string lastName, string phoneNumber, ISortAndPage sortAndPage)
		{
			AccountFilter.Current.Validate(Accounts.AccountType.Merchant, ref merchantId);
			int? rowCount = 0;
			var rawResult = DataContext.Reader.Trans_SpGetPayerTransactions(merchantId, firstName, lastName, phoneNumber, emailAddress, (short)(sortAndPage.PageCurrent * sortAndPage.PageSize), (short)sortAndPage.PageSize, ref rowCount);
			if (sortAndPage != null)
				sortAndPage.DataFunction = (pi) => SearchPayerTransactions(merchantId, emailAddress, firstName, lastName, phoneNumber, pi);
			return rawResult.Select(t => new TransactionBasicInfo(t)).ToList();
		}
    }
}
