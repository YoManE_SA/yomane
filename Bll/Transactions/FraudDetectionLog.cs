﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Transactions
{
    public class FraudDetectionLog : BaseDataObject
    {
        public class SearchFilters
        {
            public int? FraudDetectionId;
            public int? TransPassId;
            public int? TransPreAuthId;
            public int? TransPendingId;
            public int? TransFailId;

        }

        protected Netpay.Dal.Netpay.FraudDetection _entity;

        public int FraudDetectionId { get { return _entity.FraudDetection_id; } }

        public int? MerchantId { get { return _entity.Merchant_id; } set { _entity.Merchant_id = value; } }

        public int? TransPassId { get { return _entity.TransPass_id; } set { _entity.TransPass_id = value; } }

        public int? TransPreAuthId { get { return _entity.TransPreAuth_id; } set { _entity.TransPreAuth_id = value; } }

        public int? TransPendingId { get { return _entity.TransPending_id; } set { _entity.TransPending_id = value; } }

        public int? TransFailId { get { return _entity.TransFail_id; } set { _entity.TransFail_id = value; } }

        public DateTime InsertDate { get { return _entity.InsertDate; } set { _entity.InsertDate = value; } }

        public string SendingString { get { return _entity.SendingString; } set { _entity.SendingString = value; } }

        public string ReturnAnswer { get { return _entity.ReturnAnswer; } set { _entity.ReturnAnswer = value; } }

        public string ReturnExplanation { get { return _entity.ReturnExplanation; } set { _entity.ReturnExplanation = value; } }

        public decimal? ReturnScore { get { return _entity.ReturnScore; } set { _entity.ReturnScore = value; } }

        public decimal? ReturnRiskScore { get { return _entity.ReturnRiskScore; } set { _entity.ReturnRiskScore = value; } }

        public string ReturnBinCountry { get { return _entity.ReturnBinCountry; } set { _entity.ReturnBinCountry = value; } }

        public string ReturnQueriesRemaining { get { return _entity.ReturnQueriesRemaining; } set { _entity.ReturnQueriesRemaining = value; } }

        public string ReferenceCode { get { return _entity.ReferenceCode; } set { _entity.ReferenceCode = value; } }

        public decimal? AllowedScore { get { return _entity.allowedScore; } set { _entity.allowedScore = value; } }

        public string ReplyCode { get { return _entity.replyCode; } set { _entity.replyCode = value; } }

        public bool? IsTemperScore { get { return _entity.IsTemperScore; } set { _entity.IsTemperScore = value; } }

        public bool? IsProceed { get { return _entity.IsProceed; } set { _entity.IsProceed = value; } }

        public decimal? TransAmount { get { return _entity.transAmount; } set { _entity.transAmount = value; } }

        public byte? TransCurrency { get { return _entity.transCurrency; } set { _entity.transCurrency = value; } }

        public string PaymentMethodDisplay { get { return _entity.PaymentMethodDisplay; } set { _entity.PaymentMethodDisplay = value; } }

        public string BinCountry { get { return _entity.BinCountry; } set { _entity.BinCountry = value; } }

        public bool? IsDuplicateAnswer { get { return _entity.IsDuplicateAnswer; } set { _entity.IsDuplicateAnswer = value; } }

        public string SendingIp { get { return _entity.SendingIp; } set { _entity.SendingIp = value; } }

        public string SendingCity { get { return _entity.SendingCity; } set { _entity.SendingCity = value; } }

        public string SendingRegion { get { return _entity.SendingRegion; } set { _entity.SendingRegion = value; } }

        public string SendingPostal { get { return _entity.SendingPostal; } set { _entity.SendingPostal = value; } }

        public string SendingCountry { get { return _entity.SendingCountry; } set { _entity.SendingCountry = value; } }

        public string SendingDomain { get { return _entity.SendingDomain; } set { _entity.SendingDomain = value; } }

        public string SendingBin { get { return _entity.SendingBin; } set { _entity.SendingBin = value; } }

        public decimal? AllowedRiskScore { get { return _entity.AllowedRiskScore; } set { _entity.AllowedRiskScore = value; } }

        public bool? IsCountryWhitelisted { get { return _entity.IsCountryWhitelisted; } set { _entity.IsCountryWhitelisted = value; } }

        internal FraudDetectionLog(Dal.Netpay.FraudDetection entity)
        {
            _entity = entity;
        }

        public FraudDetectionLog()
        {
            _entity = new Dal.Netpay.FraudDetection();
        }

        public static FraudDetectionLog LoadByTransPassId(int transPassId)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, Transaction.SecuredObject, PermissionValue.Read);

            return (from fd in DataContext.Reader.FraudDetections
                      where fd.TransPass_id == transPassId
                      select new FraudDetectionLog(fd)).SingleOrDefault();
        }

        public static FraudDetectionLog LoadByTransFailId(int transFailId)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, Transaction.SecuredObject, PermissionValue.Read);


            return (from fd in DataContext.Reader.FraudDetections
                    where fd.TransFail_id == transFailId
                    select new FraudDetectionLog(fd)).SingleOrDefault();
        }

        public static FraudDetectionLog LoadByTransPreAuthId(int transPreAuthId)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, Transaction.SecuredObject, PermissionValue.Read);


            return (from fd in DataContext.Reader.FraudDetections
                     where fd.TransPreAuth_id == transPreAuthId
                     select new FraudDetectionLog(fd)).SingleOrDefault();
        }

        public static FraudDetectionLog LoadByTransPendingId(int transPendingId)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, Transaction.SecuredObject, PermissionValue.Read);


            return (from fd in DataContext.Reader.FraudDetections
                     where fd.TransPending_id == transPendingId
                     select new FraudDetectionLog(fd)).SingleOrDefault();
        }

    }
}

