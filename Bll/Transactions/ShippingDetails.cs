﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Transactions
{
	public class ShippingDetails : BaseDataObject, IAddress
	{
		public class SearchFilters
		{
			public string Address;
			public string City;
			public string PostalCode;
			public string StateISOCode;
			public string CountryISOCode;
		}

		protected Netpay.Dal.Netpay.TransPayerShippingDetail _entity;

		public int ID { get { return _entity.TransPayerShippingDetail_id; } }
		public string Title { get { return _entity.Title; } set { _entity.Title = value; } }
		public string AddressLine1 { get { return _entity.Street1; } set { _entity.Street1 = value.NullIfEmpty(); } }
		public string AddressLine2 { get { return _entity.Street2; } set { _entity.Street2 = value.NullIfEmpty(); } }
		public string City { get { return _entity.City; } set { _entity.City = value.NullIfEmpty(); } }
		public string PostalCode { get { return _entity.PostalCode; } set { _entity.PostalCode = value.NullIfEmpty(); } }
		public string StateISOCode { get { return _entity.StateISOCode; } set { _entity.StateISOCode = value.NullIfEmpty(); } }
		public string CountryISOCode { get { return _entity.CountryISOCode; } set { _entity.CountryISOCode = value.NullIfEmpty(); } }

		internal ShippingDetails(Dal.Netpay.TransPayerShippingDetail entity)  { _entity = entity; }
		public ShippingDetails()
			 
		{
			_entity = new Dal.Netpay.TransPayerShippingDetail();
		}

		public static ShippingDetails Load(int id)
		{
			//User user = Transaction.validateUser(credentialsToken);
			var exp = (from c in DataContext.Reader.TransPayerShippingDetails
					   where c.TransPayerShippingDetail_id == id
					   select c);
			//if (user.Type == Infrastructure.UserType.Customer) exp = exp.Where(r => r.c.Customer_id == user.ID);
			var ret = exp.SingleOrDefault();
			if (ret == null) return null;
			return new ShippingDetails(ret);
		}

		public void Save()
		{
			//User user = Customer.validateCustomer(_entity.Customer_id);
			ValidationException.RaiseIfNeeded(Validate());
			DataContext.Writer.TransPayerShippingDetails.Update(_entity, (_entity.TransPayerShippingDetail_id != 0));
			DataContext.Writer.SubmitChanges();
		}

		internal static IQueryable<Dal.Netpay.TransPayerShippingDetail> Search(Netpay.Dal.DataAccess.NetpayDataContext context, SearchFilters filters, out bool isEmptyFilters)
		{
			isEmptyFilters = true;
			var billingExp = (from ba in context.TransPayerShippingDetails select ba);
			if (!string.IsNullOrEmpty(filters.Address)) {
				isEmptyFilters = false;
				billingExp = billingExp.Where(ba => ba.Street1.Contains(filters.Address) || ba.Street2.Contains(filters.Address));
			}
			if (!string.IsNullOrEmpty(filters.City)) {
				isEmptyFilters = false;
				billingExp = billingExp.Where(ba => ba.City.Contains(filters.City));
			}
			if (!string.IsNullOrEmpty(filters.PostalCode)) {
				isEmptyFilters = false;
				billingExp = billingExp.Where(ba => ba.City.Contains(filters.PostalCode));
			}
			if (!string.IsNullOrEmpty(filters.CountryISOCode)) {
				isEmptyFilters = false;
				billingExp = billingExp.Where(ba => ba.CountryISOCode.Contains(filters.CountryISOCode));
			}
			if (!string.IsNullOrEmpty(filters.StateISOCode)) {
				isEmptyFilters = false;
				billingExp = billingExp.Where(ba => ba.StateISOCode.Contains(filters.StateISOCode));
			}
			return billingExp;
		}

	}
}
