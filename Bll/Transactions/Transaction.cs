﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Bll.Accounts;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Transactions
{
    public class TransactionsStatus
    {
        public int? Key { get; set; }
        public int PassCount { get; set; }
        public int FailCount { get; set; }
        public int PendingCount { get; set; }
        public int ApprovalCount { get; set; }
    }

    public partial class Transaction : TransactionBasicInfo
    {
        /// <summary>
        /// Helper class that is used in transactions search to get 
        /// initial minimal data from all transactions tables.
        /// </summary>
        private class TransactionSearchHelper
        {
            public int TransactionId { get; set; }

            public DateTime TransactionInsertDate { get; set; }

            public Type TransactionType { get; set; }

            /// <summary>
            /// Determine whether data was taken from Archive DB (e.g. for DeclinedArchivedTransactions)
            /// </summary>
            public bool IsFromArchiveDB { get; set; }
        }

        public const string DeleteTestSecuredObjectName = "DeleteTestTransactions";
        public static Infrastructure.Security.SecuredObject DeleteTestSecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Transactions.Module.Current, DeleteTestSecuredObjectName); } }

        //Dictionary that maps between 'Transaction Status' or 'FailedTransactionType' to 'Color'        
        public static Dictionary<Tuple<TransactionStatus?, FailedTransactionType?>, Tuple<System.Drawing.Color, System.Drawing.Color>> TransactionColorDictionary = new Dictionary<Tuple<TransactionStatus?, FailedTransactionType?>, Tuple<System.Drawing.Color, System.Drawing.Color>>{
            { Tuple.Create<TransactionStatus?,FailedTransactionType?>(TransactionStatus.Captured,null),Tuple.Create(System.Drawing.Color.FromName("#66cc66"),System.Drawing.Color.FromName("#66cc66"))},
            { Tuple.Create<TransactionStatus?,FailedTransactionType?>(TransactionStatus.Declined,null),Tuple.Create(System.Drawing.Color.FromName("#ff6666"),System.Drawing.Color.FromName("#ff6666"))},
            { Tuple.Create<TransactionStatus?,FailedTransactionType?>(TransactionStatus.DeclinedArchived,null),Tuple.Create(System.Drawing.Color.FromName("#ff6666"),System.Drawing.Color.FromName("#ff6666"))},
            { Tuple.Create<TransactionStatus?,FailedTransactionType?>(TransactionStatus.Pending,null),Tuple.Create(System.Drawing.Color.FromName("#6699cc"),System.Drawing.Color.FromName("#6699cc"))},
            { Tuple.Create<TransactionStatus?,FailedTransactionType?>(TransactionStatus.Authorized,null),Tuple.Create(System.Drawing.Color.FromName("#6699cc"),System.Drawing.Color.FromName("#6699cc"))},
            { Tuple.Create<TransactionStatus?,FailedTransactionType?>(null,FailedTransactionType.PreAuthorized),Tuple.Create(System.Drawing.Color.FromName("#ff6666"),System.Drawing.Color.FromName("#ffffff"))},
            { Tuple.Create<TransactionStatus?,FailedTransactionType?>(null,FailedTransactionType.PreAuthorizedCapture),Tuple.Create(System.Drawing.Color.FromName("#ff6666"),System.Drawing.Color.FromName("#c5c5c5"))},
            { Tuple.Create<TransactionStatus?,FailedTransactionType?>(null,null),Tuple.Create(System.Drawing.Color.FromName("#f8f8f8"),System.Drawing.Color.FromName("#f8f8f8"))}
            };

        //Dictionary that maps between 'Transactions Status' or 'FailedTransactionType' to 'String'
        public static Dictionary<Tuple<TransactionStatus?, FailedTransactionType?>, string> TransactionColorDictionaryText = new Dictionary<Tuple<TransactionStatus?, FailedTransactionType?>, string>{
            { Tuple.Create<TransactionStatus?,FailedTransactionType?>(TransactionStatus.Captured,null),"CAPTURED"},
            { Tuple.Create<TransactionStatus?,FailedTransactionType?>(TransactionStatus.Declined,null),"DECLINED"},
            { Tuple.Create<TransactionStatus?,FailedTransactionType?>(TransactionStatus.DeclinedArchived,null),"DECLINED-ARCHIVED"},
            { Tuple.Create<TransactionStatus?,FailedTransactionType?>(TransactionStatus.Pending,null),"PENDING"},
            { Tuple.Create<TransactionStatus?,FailedTransactionType?>(TransactionStatus.Authorized,null),"AUTHORIZED"},
            { Tuple.Create<TransactionStatus?,FailedTransactionType?>(null,FailedTransactionType.PreAuthorized),"DECLINED-PREAUTHORIZED"},
            { Tuple.Create<TransactionStatus?,FailedTransactionType?>(null,FailedTransactionType.PreAuthorizedCapture),"DECLINED-PREAUTHORIZED-CAPTURE"},
            { Tuple.Create<TransactionStatus?,FailedTransactionType?>(null,null),"TEST-TRANSACTION"}
            };

        //Dictionary that indicates whether should mark X for 'Transactions Status' or 'FailedTransactionType' values
        public static Dictionary<Tuple<TransactionStatus?, FailedTransactionType?>, bool> TransactionColorDictionaryIsX = new Dictionary<Tuple<TransactionStatus?, FailedTransactionType?>, bool>{
            { Tuple.Create<TransactionStatus?,FailedTransactionType?>(TransactionStatus.Captured,null), false},
            { Tuple.Create<TransactionStatus?,FailedTransactionType?>(TransactionStatus.Declined,null), false},
            { Tuple.Create<TransactionStatus?,FailedTransactionType?>(TransactionStatus.DeclinedArchived,null),false},
            { Tuple.Create<TransactionStatus?,FailedTransactionType?>(TransactionStatus.Pending,null),false},
            { Tuple.Create<TransactionStatus?,FailedTransactionType?>(TransactionStatus.Authorized,null),false},
            { Tuple.Create<TransactionStatus?,FailedTransactionType?>(null,FailedTransactionType.PreAuthorized),false},
            { Tuple.Create<TransactionStatus?,FailedTransactionType?>(null,FailedTransactionType.PreAuthorizedCapture),false},
            { Tuple.Create<TransactionStatus?,FailedTransactionType?>(null,null),true}
            };


        public class SearchFilters
        {
            public Range<int?> ID;
            public int? specificTransactionID;
            public int? transactionID { set { ID.From = ID.To = value; } }
            public List<PaymentsStatus> paymentsStatusList;
            public int? PaymentStatusBankDropDown;
            public Netpay.Infrastructure.PaymentsStatus? paymentsStatus;
            public bool HasUnsettledInstallments; // This filters by Unsettledinstallments column - in this case it's bigger than 1. that returns transactions that are "Settled" or "Partialy Settled"
            public Range<DateTime?> date;
            public Range<DateTime?> releaseDate; // Used in Rolling Reserve filter.
            public bool? showOnlyRollingReserve; // Used for Rolling Reserve filter.
            public bool? iCreditType0;
            public bool? iCreditType1;
            public bool? iNotRelease;
            public Range<decimal?> amount;
            public int? currencyID;
            public bool? isTest;
            public bool? hasEpa;
            public bool? hasCart;

            public List<int> merchantIDs;
            public List<int> customersIDs;

            public bool? isChargeback;
            public int? ChbReason;
            public bool? isPendingChargeback;
            public int? deniedStatusIntVal;
            public List<Netpay.Infrastructure.DeniedStatus> deniedStatus;
            public Range<DateTime?> deniedDate;
            public int? originalTransactionID;

            public int? declinedType;
            public List<int> paymentMethodsIds;
            public int? deviceId;
            public int? debitCompanyID;
            public int? paymentMethodID;
            public List<int> creditTypes;
            public string orderID;
            public string replyCode;

            public string debitTerminalNumber;
            public string debitReferenceCode;
            public string debitReferenceNumber;
            public string debitApprovalNumber;

            public List<int> BatchID;

            public Payer.SearchFilters PayerFilters;
            public Payment.SearchFilters PaymentFilters;

            public LoadOptions LoadOptionsFilter;

            //For declined transaction
            public string DeclinedAutoRefundFilter;

            public List<CommonTypes.TransactionHistoryType> HistoryTypes;
            public bool? isFraud;
            public int? PayID;

            /*
			public string ToFriendlyString()
			{
				StringBuilder sb = new StringBuilder();
				System.Reflection.FieldInfo[] fields = this.GetType().GetFields();
				foreach (var currentField in fields)
				{
					object currentFieldValue = currentField.GetValue(this);
					if (currentFieldValue != null && currentField.Name != "merchantIDs" && currentField.Name != "transactionHistoryTypes")
						sb.AppendLine(Bll.SearchFilters.FriendlyNames[currentField.Name] + ": " + currentFieldValue);
				}

				return sb.ToString();
			}
			*/
        }

        public class LoadOptions
        {
            public List<Infrastructure.TransactionStatus> dataStores;
            public bool loadPaymentData;
            public bool loadPayerData;
            public bool loadMerchant;
            public bool loadDebitCompany;
            public bool loadBatchData;

            public ISortAndPage sortAndPage;
            public LoadOptions() { dataStores = new List<TransactionStatus>(); }
            public LoadOptions(Netpay.Infrastructure.TransactionStatus status, ISortAndPage sortAndPage, bool loadPaymentData, bool loadMerchant)
            {
                dataStores = new List<TransactionStatus>() { status };
                this.sortAndPage = sortAndPage;
                this.loadPayerData = this.loadPaymentData = loadPaymentData;
                this.loadMerchant = loadMerchant;
            }
        }

        protected Transaction(TransactionStatus status) { Status = status; }

        public Transaction()
        {
            _payerData = new Payer();
            _paymentData = new Payment();
            CreditType = CreditType.Regular;
            TransType = TransactionType.Capture;
            IsPaymentManagerTransaction = false;
            IsPaymentFeesTransaction = false;
            OrderNumber = ""; //Set default value - null is not acceptable by DB
            TerminalNumber = ""; //Set default value - null is not acceptable by DB
            DebitReferenceCode = ""; //Set default value - null is not acceptable by DB
        }

        //public Transaction(int merchantId, decimal amount, CommonTypes.Currency currency, int installments, )  { }

        public string DebitApprovalNumber { get; set; } //Belongs only to tblTransPending
        public string TransOrder { get; set; } // Belongs only to tblTransPending

        public string RefTrans { get; set; } // Property added in order to implement functionality that is located at Trans_detail_chb.asp - line 81

        public int FraudDetectionLogId { get; set; }

        public DateTime? PD { get; set; } // Used in Rolling Reserve filter "Create Date"

        public DateTime? DeniedSendDate { get; set; }

        public DateTime? DeniedPrintDate { get; set; }

        public DateTime? DeniedValDate { get; set; }

        public Tuple<Netpay.Infrastructure.TransactionStatus?, FailedTransactionType?> GetTransactionLegendKey
        {
            get
            {
                if (Status != TransactionStatus.Declined)
                {
                    return Tuple.Create<Netpay.Infrastructure.TransactionStatus?, FailedTransactionType?>(Status, null);
                }
                else if (TransactionTypeText == FailedTransactionType.Debit.ToString() || TransactionTypeText == FailedTransactionType.StoredCardDebit.ToString())
                {
                    return Tuple.Create<Netpay.Infrastructure.TransactionStatus?, FailedTransactionType?>(Status, null);
                }
                else
                {
                    FailedTransactionType enumVal = (FailedTransactionType)Enum.Parse(typeof(FailedTransactionType), TransactionTypeText);
                    return Tuple.Create<Netpay.Infrastructure.TransactionStatus?, FailedTransactionType?>(null, enumVal);
                }
            }
        }

        public int? RetrievalRequestReasoneCode
        {
            get
            {
                if (IsRetrievalRequest.HasValue && IsRetrievalRequest.Value)
                {
                    return (from code in DataContext.Reader.tblRetrivalRequests
                            where code.TransPass_ID == ID
                            select code.RR_ReasonCode)
                            .SingleOrDefault();
                }
                else
                {
                    return null;
                }
            }
        }

        public string OrderNumber { get; set; }
        public string Comment { get; set; }
        public string PayForText { get; set; }
        public int? ProductId { get; set; }
        //public int? CartId { get; set; }

        public string IP { get; set; }
        public string ReferringUrl { get; set; }
        public string IPCountry { get; set; }
        public string PayerIDUsed { get; set; }

        public string TransactionTypeText { get; set; } /*fail only*/
        public int? RecurringSeriesID { get; set; }

        //response
        public string ReplyDescriptionText { get; set; }

        public int PaymentMethodReferenceID { get; set; }

        //references
        public int? PaymentDataID { get; set; }
        public int? PayerDataID { get; set; }
        public int? DebitCompanyID { get; set; }

        //debitor
        public string TerminalNumber { get; set; }
        public string DebitReferenceCode { get; set; }
        public string DebitReferenceNum { get; set; }
        public string BatchData { get; set; }
        public string AcquirerReferenceNum { get; set; }

        //chb

        public string DeniedStatusText { get; set; }
        public bool? IsRetrievalRequest { get; set; }

        public bool IsPendingChargebackForRefundPage
        {
            get
            {
                if (IsRetrievalRequest.HasValue && IsRetrievalRequest.Value && RetrievalRequestReasoneCode.HasValue)
                {
                    var chargeBack = ChargeBackReason.Cache.Where(x => x.ReasonCode == RetrievalRequestReasoneCode.Value).SingleOrDefault();
                    if (chargeBack != null)
                    {
                        return (chargeBack.IsPendingChargeBack);
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public bool? IsChargeback { get; set; }
        public int OriginalTransactionID { get; set; }
        public string DeniedAdminComment { get; set; }
        public bool IsFraud { get; set; }


        //fees
        public decimal RatioFee { get; set; }
        public decimal ClarificationFee { get; set; }
        public decimal ChargebackFee { get; set; }
        public decimal HandlingFee { get; set; }
        public decimal DebitFee { get; set; }
        public decimal DebitFeeChb { get; set; }
        public decimal TotalFees
        {
            get
            {
                return TransactionFee + RatioFee + ClarificationFee + ChargebackFee + HandlingFee + DebitFee + DebitFeeChb;
            }
        }

        public decimal Cashback
        {
            get
            {
                try
                {
                    if (IsCashback)
                        return 0;

                    return (from p in Search(new Transactions.Transaction.LoadOptions(Status, null, true, false),
                        new SearchFilters()
                    {
                        debitReferenceCode = DebitReferenceCode
                    })
                                          where p.IsCashback
                                          select p.Amount
                    ).SingleOrDefault();
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        public override string PaymentMethodDisplay { get { if (_paymentData == null) return base.PaymentMethodDisplay; return _paymentData.MethodInstance.Display; } set { base.PaymentMethodDisplay = value; } }
        public override string IssuerCountryIsoCode { get { if (_paymentData == null) return base.IssuerCountryIsoCode; return _paymentData.IssuerCountryIsoCode; } set { base.IssuerCountryIsoCode = value; } }

        private Payment _paymentData;
        private Payer _payerData;
        private DebitCompanies.DebitCompany _debitCompany;
        private DebitCompanies.ResponseCode _replyDescription;

        public Payment PaymentData
        {
            get
            {
                if (_paymentData != null) return _paymentData;
                if (PaymentDataID != null) _paymentData = Payment.Load(new List<int>() { PaymentDataID.Value }).SingleOrDefault();
                return _paymentData;
            }
            set
            {
                _paymentData = value;
                PaymentMethodDisplay = value != null ? _paymentData.ToString() : "";
                PaymentDataID = value != null && value.ID != 0 ? value.ID : (int?)null;
            }
        }

        // For NewAdmin, Merchants-Management-> Summary Tab -> Add Admin Transactions - select Transaction Type 'Admin'
        public bool IsPaymentManagerTransaction { get; set; }
        // For NewAdmin, Merchants-Management-> Summary Tab -> Add Admin Transactions - select Transaction Type 'Fees'
        public bool IsPaymentFeesTransaction { get; set; }

        public Payer PayerData
        {
            get
            {
                if (_payerData != null) return _payerData;
                if (PayerDataID != null) _payerData = Payer.Load(new List<int>() { PayerDataID.Value }).SingleOrDefault();
                return _payerData;
            }
            set
            {
                _payerData = value;
                PayerDataID = value != null && value.ID != 0 ? value.ID : (int?)null;
            }
        }

        public DebitCompanies.DebitCompany DebitCompany
        {
            get
            {
                if (DebitCompanyID == null) return null;
                if (_debitCompany == null) _debitCompany = Bll.DebitCompanies.DebitCompany.GetCachedItem(DebitCompanyID.Value);
                return _debitCompany;
            }
        }

        public DebitCompanies.ResponseCode ReplyDescription
        {
            get
            {
                if (_replyDescription != null) return _replyDescription;
                if (DebitCompanyID != null && string.IsNullOrEmpty(ReplyDescriptionText))
                    _replyDescription = Bll.DebitCompanies.ResponseCode.GetItem(DebitCompanyID.GetValueOrDefault(), ReplyCode);
                return _replyDescription;
            }
        }

        public static void DeleteTestTransactions(int merchantID)
        {
            ObjectContext.Current.IsUserOfType(UserRole.Admin, DeleteTestSecuredObject, PermissionValue.Execute);
            AccountFilter.Current.Validate(Accounts.AccountType.Merchant, merchantID);
            var testTransactions = from t in DataContext.Reader.tblCompanyTransPasses where t.companyID == merchantID && t.isTestOnly && t.UnsettledInstallments > 0 select t;
            var testTransactionsIDs = testTransactions.Select(t => t.ID).ToArray();
            var refundRequests = from rr in DataContext.Reader.tblRefundAsks where testTransactionsIDs.Contains(rr.transID) select rr;

            DataContext.Writer.tblCompanyTransPasses.DeleteAllOnSubmit(testTransactions);
            DataContext.Writer.tblRefundAsks.DeleteAllOnSubmit(refundRequests);
            DataContext.Writer.SubmitChanges();
        }

        public static int TestTransactionsCount(int merchantID)
        {

            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Read);


            AccountFilter.Current.Validate(Accounts.AccountType.Merchant, merchantID);
            return (from t in DataContext.Reader.tblCompanyTransPasses where t.companyID == merchantID && t.isTestOnly && t.UnsettledInstallments > 0 select t).Count();
        }

        public void SendClientEmail(string toEmail = null)
        {
            string paramter = string.Empty;
            if (!string.IsNullOrEmpty(toEmail)) paramter = "Email=" + System.Uri.EscapeDataString(toEmail);
            PendingEvents.Create(CommonTypes.PendingEventType.InfoEmailSendClient, ID, Status, paramter);
        }

        public void SendMerchantEmail(string toEmail = null)
        {
            string paramter = string.Empty;
            if (!string.IsNullOrEmpty(toEmail)) paramter = "Email=" + System.Uri.EscapeDataString(toEmail);
            PendingEvents.Create(CommonTypes.PendingEventType.InfoEmailSendMerchant, ID, Status, paramter);
        }

        public void SendClientSMS(string toPhone = null)
        {
            string paramter = string.Empty;
            //if (!string.IsNullOrEmpty(toPhone)) paramter = string.Format("Phone={0}&MerchantText={1}", System.Uri.EscapeDataString(toPhone), System.Uri.EscapeDataString(Merchant.merchantText));
            PendingEvents.Create(CommonTypes.PendingEventType.InfoSmsSendClient, ID, Status, paramter);
        }

        /******************************************SEARCH ******************************************/

        private static void LoadTransactionReferences(List<Transaction> list, LoadOptions loadOptions)
        {
            //var ids = list.Select(t => t.Pay);
            Dictionary<int, Payment> paymentData = null;
            Dictionary<int, Payer> payerData = null;
            Dictionary<int, Bll.Merchants.Merchant> merchants = null;
            //Dictionary<int, Bll.DebitCompany.DebitCompany> debitCompanys = null;
            if (loadOptions.loadPaymentData)
                paymentData = Payment.Load(list.Where(t => t.PaymentDataID != null).Select(t => t.PaymentDataID.GetValueOrDefault()).Distinct().ToList()).ToDictionary(pm => pm.ID);
            if (loadOptions.loadPayerData)
                payerData = Payer.Load(list.Where(t => t.PayerDataID != null).Select(t => t.PayerDataID.GetValueOrDefault()).Distinct().ToList()).ToDictionary(pa => pa.ID);
            if (loadOptions.loadMerchant)
                merchants = Bll.Merchants.Merchant.Load(list.Where(t => t.MerchantID != null).Select(t => t.MerchantID.GetValueOrDefault()).Distinct().ToList()).ToDictionary(m => m.ID);
            //if (loadOptions.loadDebitCompany)
            //debitCompanys = Bll.DebitCompany.DebitCompany.Load(list.Where(t => t.DebitCompanyID != null).Select(t => t.DebitCompanyID.GetValueOrDefault()).ToList()).ToDictionary(d => d.ID);
            foreach (var t in list)
            {
                Payment payment = null;
                Payer payer = null;
                Bll.Merchants.Merchant merchant = null;
                //Bll.DebitCompany.DebitCompany debitCompany = null;
                if ((t.PayerDataID != null) && (paymentData != null) && paymentData.TryGetValue(t.PaymentDataID.Value, out payment)) t._paymentData = payment;
                if ((t.PayerDataID != null) && (payerData != null) && payerData.TryGetValue(t.PayerDataID.Value, out payer)) t._payerData = payer;
                if ((t.MerchantID != null) && (merchants != null) && merchants.TryGetValue(t.MerchantID.Value, out merchant)) t._merchant = merchant;
                //if ((t.DebitCompanyID != null) && (debitCompanys != null) && debitCompanys.TryGetValue(t.DebitCompanyID.Value, out debitCompany)) t._debitCompany = debitCompany;
            }
        }

        public static List<Transaction> Search(Netpay.Infrastructure.TransactionStatus status, SearchFilters filters, ISortAndPage sortAndPage, bool loadPaymentMethod, bool loadMerchant)
        {
            return Search(new LoadOptions(status, sortAndPage, loadPaymentMethod, loadMerchant), filters);
        }

        public static List<Transaction> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            filters.LoadOptionsFilter.sortAndPage = sortAndPage;
            return Search(filters.LoadOptionsFilter, filters);
        }

        /// <summary>
        /// This 'Search' function is different from other 'Search' functions , 
        /// This function gets data from 5 different tables , because there are
        /// 5 types of transactions - 'Captured' , 'Declined' , 'Pending' , 'Authorized' , 'DeclinedArchived'(comes from different DB)
        /// This function creates a list of transactions that were loaded from the DB based on the filters it recieves.
        /// At the first time when loading the whole list of transactions based on the fliters conditions
        /// Only the ID and InsertDate columns are loaded and a BIG list called 'searchHelperData' is created
        /// The 'searchHelperData' is a list of objects from the helping class "TransactionSearchHelper" , that objects inclueds
        /// Only the ID , InsertDate , and TransactionType of the transactions that were loaded according to the filters conditions.
        ///  
        /// Afterwards - SortAndPage functionality is invoked , and it Sorts the data according to the InsertDate property
        /// Of the transactions that were loaded partially.
        /// 
        /// This part of functionality(SortAndPage) should support to sort by different properties  
        /// For now it sorts the data by the InsertDate propery. 
        /// 
        /// After sorting the BIG list of transactions(SearchHelperData) that were loaded partially(only ID and InsertDate)
        /// The transactions that are needed to be loaded completely are loaded from the database and shown on the screen
        /// According to the "Paging" of the user.
        /// 
        /// TBD - Sorting by all kinds of transaction fields
        /// </summary>
        /// <param name="loadOptions"></param>
        /// <param name="filters"></param>
        /// <returns></returns>
        public static List<Transaction> Search(LoadOptions loadOptions, SearchFilters filters)
        {

            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Read);

            if (filters == null) filters = new SearchFilters();
            List<Transaction> transactions = new List<Transaction>();
            filters.merchantIDs = AccountFilter.Current.FilterIDList(AccountType.Merchant, filters.merchantIDs);
            filters.customersIDs = AccountFilter.Current.FilterIDList(AccountType.Customer, filters.customersIDs);

            // SearchHelperData will hold minimal fields taken from all 5 transactions tables
            // that will be used to apply sorting and paging in order to get the list of IDs
            // to fetch from the 5 tables. It also provides the final order.
            List<TransactionSearchHelper> searchHelperData = new List<TransactionSearchHelper>();

            // First get TransactionSearchHelper for tblCompanyTransPass entities
            if (loadOptions.dataStores.Contains(TransactionStatus.Captured))
            {
                var expression = SearchExpressionTransCaptured(loadOptions, filters);
                var searchHelperItems = expression
                                            .Select(i => new TransactionSearchHelper()
                                            {
                                                TransactionType = typeof(Dal.Netpay.tblCompanyTransPass),
                                                TransactionId = i.ID,
                                                TransactionInsertDate = i.InsertDate,
                                                IsFromArchiveDB = false
                                            })
                                            .ToList();
                searchHelperData.AddRange(searchHelperItems);
            }

            //Second get TransactionSearchHelper for tblCompanyTransFail entities
            if (loadOptions.dataStores.Contains(TransactionStatus.Declined))
            {
                var expression = SearchExpressionTransFail(loadOptions, filters);
                var searchHelperItems = expression
                                            .Select(i => new TransactionSearchHelper()
                                            {
                                                TransactionType = typeof(Dal.Netpay.tblCompanyTransFail),
                                                TransactionId = i.ID,
                                                TransactionInsertDate = i.InsertDate,
                                                IsFromArchiveDB = false
                                            })
                                            .ToList();
                searchHelperData.AddRange(searchHelperItems);
            }

            //Third get TransactionSearchHelper for tblCompanyTransApproval
            if (loadOptions.dataStores.Contains(TransactionStatus.Authorized))
            {
                var expression = SearchExpressionTransPreAuthorized(loadOptions, filters);
                var searchHelperItems = expression
                                            .Select(i => new TransactionSearchHelper()
                                            {
                                                TransactionType = typeof(Dal.Netpay.tblCompanyTransApproval),
                                                TransactionId = i.ID,
                                                TransactionInsertDate = i.InsertDate,
                                                IsFromArchiveDB = false
                                            })
                                            .ToList();
                searchHelperData.AddRange(searchHelperItems);
            }

            //Fourth get TransactionSearchHelper for tblCompanyTransPending
            if (loadOptions.dataStores.Contains(TransactionStatus.Pending))
            {
                var expression = SearchExpressionTransPending(loadOptions, filters);
                var searchHelperItems = expression
                                            .Select(i => new TransactionSearchHelper()
                                            {
                                                TransactionType = typeof(Dal.Netpay.tblCompanyTransPending),
                                                TransactionId = i.ID,
                                                TransactionInsertDate = i.insertDate,
                                                IsFromArchiveDB = false
                                            })
                                            .ToList();
                searchHelperData.AddRange(searchHelperItems);
            }

            //Fifth get TransactionSearchHelper for tblCompanyTransFail in other DataContext - 
            if (loadOptions.dataStores.Contains(TransactionStatus.DeclinedArchived))
            {
                var expression = SearchExpressionTransDeclinedArchived(loadOptions, filters);
                var searchHelperItems = expression
                                            .Select(i => new TransactionSearchHelper()
                                            {
                                                TransactionType = typeof(Dal.Netpay.tblCompanyTransFail),
                                                TransactionId = i.ID,
                                                TransactionInsertDate = i.InsertDate,
                                                IsFromArchiveDB = true
                                            })
                                            .ToList();
                searchHelperData.AddRange(searchHelperItems);
            }

            List<TransactionSearchHelper> transactionsToLoad;
            SortAndPage helperSortAndPage = null;
            if (loadOptions != null && loadOptions.sortAndPage != null)
            {
                // Prepare SortAndPage for TransactionSearchHelper to sort by InsertDate and Descending
                helperSortAndPage = new SortAndPage(loadOptions.sortAndPage.PageCurrent,
                    loadOptions.sortAndPage.PageSize,
                    "TransactionInsertDate",
                    true);

                // Get ordered list of transactions to load
                transactionsToLoad = searchHelperData.ApplySortAndPage<TransactionSearchHelper>(helperSortAndPage).ToList();
            }
            else
            {
                transactionsToLoad = searchHelperData.ToList();
            }

            // First load the selected transactions from DB
            // loadedTransactions will hold the transactions loaded from DB
            List<Transaction> loadedTransactions =
                transactionsToLoad
                    .GroupBy(x => new { x.TransactionType, x.IsFromArchiveDB })
                    .SelectMany(i => LoadFromDB(i.Key.TransactionType, i.Key.IsFromArchiveDB, i.ToList()))
                    .ToList();

            // Post processing of transactions after they were loaded from DB
            PostProcess(loadedTransactions, filters);

            // Then apply the order
            var orderedTransactions = transactionsToLoad
                .Select(i => loadedTransactions
                                    .Where(j => (
                                                  (i.TransactionType == typeof(Dal.Netpay.tblCompanyTransPass) &&
                                                   j.Status == TransactionStatus.Captured) ||
                                                  (i.TransactionType == typeof(Dal.Netpay.tblCompanyTransFail) &&
                                                   i.IsFromArchiveDB == false &&
                                                   j.Status == TransactionStatus.Declined) ||
                                                  (i.TransactionType == typeof(Dal.Netpay.tblCompanyTransPending) &&
                                                   j.Status == TransactionStatus.Pending) ||
                                                  (i.TransactionType == typeof(Dal.Netpay.tblCompanyTransApproval) &&
                                                   j.Status == TransactionStatus.Authorized) ||
                                                  (i.TransactionType == typeof(Dal.Netpay.tblCompanyTransFail) &&
                                                   i.IsFromArchiveDB == true &&
                                                   j.Status == TransactionStatus.DeclinedArchived)
                                                ) && i.TransactionId == j.ID)
                                    .FirstOrDefault())
                .ToList();

            if (loadOptions.sortAndPage != null)
            {
                loadOptions.sortAndPage.DataFunction = (pi) => { loadOptions.sortAndPage = pi; return Search(loadOptions, filters); };

                // Update row count in input SortAndPage
                loadOptions.sortAndPage.RowCount = helperSortAndPage.RowCount;
            }

            // Load entities from DB for transaction references (depending on loadOptions values)
            LoadTransactionReferences(orderedTransactions, loadOptions);

            return orderedTransactions;
        }


        private static List<Transaction> LoadFromDB(Type transactionType, bool isFromArchivedDB, IList<TransactionSearchHelper> itemsToLoad)
        {
            if (itemsToLoad == null)
                return new List<Transaction>();

            // Get list of IDs to load
            List<int> idsToLoad = itemsToLoad.Select(i => i.TransactionId).ToList();

            // Load from tblCompanyTransPass
            if (transactionType == typeof(Dal.Netpay.tblCompanyTransPass))
            {
                var items = from t in DataContext.Reader.tblCompanyTransPasses
                            where idsToLoad.Contains(t.ID)
                            select new Transaction(t);

                return items.ToList();
            }

            // Load from tblCompanyTransApproval
            else if (transactionType == typeof(Dal.Netpay.tblCompanyTransApproval))
            {
                var items = from t in DataContext.Reader.tblCompanyTransApprovals
                            where idsToLoad.Contains(t.ID)
                            select new Transaction(t);
                return items.ToList();
            }

            // Load from tblCompanyTransPending
            else if (transactionType == typeof(Dal.Netpay.tblCompanyTransPending))
            {
                var items = from t in DataContext.Reader.tblCompanyTransPendings
                            where idsToLoad.Contains(t.ID)
                            select new Transaction(t);
                return items.ToList();
            }

            // Load from tblCompanyTransFail
            else if (transactionType == typeof(Dal.Netpay.tblCompanyTransFail) && isFromArchivedDB == false)
            {
                var items = from t in DataContext.Reader.tblCompanyTransFails
                            where idsToLoad.Contains(t.ID)
                            select new Transaction(t);
                return items.ToList();
            }

            //Load from tblCompanyTransFail in Archived DataBase
            else if (transactionType == typeof(Dal.Netpay.tblCompanyTransFail) && isFromArchivedDB == true)
            {
                var dc = new Dal.DataAccess.NetpayDataContext(Domain.Current.SqlArchiveConnectionString);
                dc.ObjectTrackingEnabled = false;

                var items = from t in dc.tblCompanyTransFails
                            where idsToLoad.Contains(t.ID)
                            select new Transaction(t);
                return items.ToList();
            }

            return new List<Transaction>();
        }

        /// <summary>
        /// Post processing on Transactions (after they were loaded from DB)
        /// </summary>
        /// <param name="transactions"></param>
        /// <param name="filters"></param>
        private static void PostProcess(List<Transaction> transactions, SearchFilters filters)
        {
            // Post processing on captured transactions
            var capturedTransactions = transactions.Where(i => i.Status == TransactionStatus.Captured).ToList();
            PostSearchTransCaptured(capturedTransactions, filters);
        }

        public void SetTransactionFees(Merchants.ProcessTerminals terminal)
        {
            if (terminal == null) terminal = Merchants.ProcessTerminals.GetTerminalForTransaction(MerchantID.GetValueOrDefault(), (CommonTypes.Currency)CurrencyID, PaymentMethodID, "");
            if (terminal == null) throw new Exception(string.Format("Unable to fine terminal for fees TransID:{0}, Merchant:{1}, Currency:{2}", ID, MerchantID, CurrencyIsoCode));
            if (Status == TransactionStatus.Captured)
            {
                //if(DeniedStatusID == 0) 
                if (TransType == TransactionType.Authorization)
                {
                    TransactionFee = terminal.ApproveFixedFee;
                }
                else
                {
                    if (CreditType == Infrastructure.CreditType.Refund)
                    {
                        TransactionFee = terminal.RefundFixedFee;
                    }
                    else //if (CreditType == Infrastructure.CreditType.Debit) 
                    {
                        TransactionFee = terminal.FixedFee;
                        RatioFee = (Amount * terminal.PercentFee) / 100;
                    }
                }
            }
            else if (Status == TransactionStatus.Authorized)
            {
                TransactionFee = terminal.ApproveFixedFee;
            }
            else if (Status == TransactionStatus.Declined)
            {
                TransactionFee = terminal.FailFixedFee;
            }
        }

        public void Save()
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Edit);

            if (ID == 0)
                InsertDate = DateTime.Now;

            if (_payerData != null && _payerData.ID != PayerDataID.GetValueOrDefault(-1))
            {
                if (_payerData.ID == 0) _payerData.Save();
                PayerDataID = _payerData.ID;
            }
            if (_paymentData != null && _paymentData.ID != PaymentDataID.GetValueOrDefault(-1))
            {
                if (_paymentData.ID == 0) _paymentData.Save(_payerData);
                PaymentDataID = _paymentData.ID;
                PaymentMethodReferenceID = PaymentData.OldCreditCardID.GetValueOrDefault();
                PaymentMethodID = _paymentData.MethodInstance.PaymentMethodId;
            }
            if (Status == TransactionStatus.Captured) SavePass();
            else if (Status == TransactionStatus.Declined) SaveFail();
            else if (Status == TransactionStatus.Authorized) SaveApproval();
            else if (Status == TransactionStatus.Pending) SavePending();
        }

        public static Transaction GetTransaction(int? merchantID, int transactionID, Infrastructure.TransactionStatus status)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Read);

            var sf = new SearchFilters();

            //Initialize the sortAndPage with values that fits a search of only one transaction.
            var sortAndPage = new SortAndPage(0, 1);
            sortAndPage.RowCount = 1;

            var lo = new LoadOptions(status, sortAndPage, false, false);
            if (merchantID != null) sf.merchantIDs = new List<int>(new[] { merchantID.Value });
            sf.ID.From = sf.ID.To = transactionID;
            return Search(lo, sf).SingleOrDefault();
        }

        public static Transaction GetTranasctionByApprovalNumber(string approvalnumber, Infrastructure.TransactionStatus status)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Read);

            var sf = new SearchFilters();

            //Initialize the sortAndPage with values that fits a search of only one transaction.
            var sortAndPage = new SortAndPage(0, 1);
            sortAndPage.RowCount = 1;

            var lo = new LoadOptions(status, sortAndPage, false, false);
            sf.debitApprovalNumber = approvalnumber;
            return Search(lo, sf).SingleOrDefault();
        }

        public enum GetTransactionCountGroup { Merchant, Customer, DebitCompany };
        public static TransactionsStatus GetTransactionCount(DateTime sinceDate)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Read);

            return new TransactionsStatus()
            {
                PassCount = (from t in DataContext.Reader.tblCompanyTransPasses where t.InsertDate >= sinceDate select t).Count(),
                FailCount = (from t in DataContext.Reader.tblCompanyTransFails where t.InsertDate >= sinceDate select t).Count(),
                ApprovalCount = (from t in DataContext.Reader.tblCompanyTransApprovals where t.InsertDate >= sinceDate select t).Count(),
                PendingCount = (from t in DataContext.Reader.tblCompanyTransPendings where t.insertDate >= sinceDate select t).Count(),
            };
        }


        public static int GetCustomerPassedTransactionCount(int customerid)
        {
            if (customerid == 0) return 0;
            return (from tp in DataContext.Reader.tblCompanyTransPasses where tp.CustomerID == customerid select tp.ID).Count();
        }

        public static int GetCustomerFailedTransactionCount(int customerid)
        {
            if (customerid == 0) return 0;
            return (from tf in DataContext.Reader.tblCompanyTransFails where tf.CustomerID == customerid select tf.ID).Count();
        }

        public static List<TransactionsStatus> GetTransactionCount(GetTransactionCountGroup grouping, List<int> idList)
        {

            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Read);


            Dictionary<int, int> lstPass = null, lstFail = null, lstPending = null, lstApproval = null;
            switch (grouping)
            {
                case GetTransactionCountGroup.Customer:
                    idList = AccountFilter.Current.FilterIDList(Accounts.AccountType.Customer, idList);
                    lstPass = (from t in DataContext.Reader.tblCompanyTransPasses where idList.Contains(t.CustomerID) group t by t.CustomerID into g select new { key = g.Key, count = g.Count() }).ToDictionary(tp => tp.key, tp => tp.count);
                    lstFail = (from t in DataContext.Reader.tblCompanyTransFails where idList.Contains(t.CustomerID) group t by t.CustomerID into g select new { key = g.Key, count = g.Count() }).ToDictionary(tp => tp.key, tp => tp.count);
                    lstApproval = (from t in DataContext.Reader.tblCompanyTransApprovals where idList.Contains(t.CustomerID) group t by t.CustomerID into g select new { key = g.Key, count = g.Count() }).ToDictionary(tp => tp.key, tp => tp.count);
                    lstPending = (from t in DataContext.Reader.tblCompanyTransPendings where idList.Contains(t.CustomerID) group t by t.CustomerID into g select new { key = g.Key, count = g.Count() }).ToDictionary(tp => tp.key, tp => tp.count);
                    break;
                case GetTransactionCountGroup.Merchant:
                    idList = AccountFilter.Current.FilterIDList(Accounts.AccountType.Merchant, idList);
                    lstPass = (from t in DataContext.Reader.tblCompanyTransPasses where idList.Contains(t.companyID) group t by t.companyID into g select new { key = g.Key, count = g.Count() }).ToDictionary(tp => tp.key, tp => tp.count);
                    lstFail = (from t in DataContext.Reader.tblCompanyTransFails where idList.Contains(t.CompanyID.Value) group t by t.CompanyID into g select new { key = g.Key, count = g.Count() }).ToDictionary(tp => tp.key.Value, tp => tp.count);
                    lstApproval = (from t in DataContext.Reader.tblCompanyTransApprovals where idList.Contains(t.CompanyID.Value) group t by t.CompanyID into g select new { key = g.Key, count = g.Count() }).ToDictionary(tp => tp.key.Value, tp => tp.count);
                    lstPending = (from t in DataContext.Reader.tblCompanyTransPendings where idList.Contains(t.CompanyID.Value) group t by t.CompanyID into g select new { key = g.Key, count = g.Count() }).ToDictionary(tp => tp.key.Value, tp => tp.count);
                    break;
                case GetTransactionCountGroup.DebitCompany:
                    idList = AccountFilter.Current.FilterIDList(Accounts.AccountType.DebitCompany, idList);
                    lstPass = (from t in DataContext.Reader.tblCompanyTransPasses where idList.Contains(t.DebitCompanyID.Value) group t by t.DebitCompanyID into g select new { key = g.Key, count = g.Count() }).ToDictionary(tp => tp.key.Value, tp => tp.count);
                    lstFail = (from t in DataContext.Reader.tblCompanyTransFails where idList.Contains(t.DebitCompanyID.Value) group t by t.DebitCompanyID into g select new { key = g.Key, count = g.Count() }).ToDictionary(tp => tp.key.Value, tp => tp.count);
                    lstApproval = (from t in DataContext.Reader.tblCompanyTransApprovals where idList.Contains(t.DebitCompanyID.Value) group t by t.DebitCompanyID into g select new { key = g.Key, count = g.Count() }).ToDictionary(tp => tp.key.Value, tp => tp.count);
                    lstPending = (from t in DataContext.Reader.tblCompanyTransPendings where idList.Contains(t.DebitCompanyID.Value) group t by t.DebitCompanyID into g select new { key = g.Key, count = g.Count() }).ToDictionary(tp => tp.key.Value, tp => tp.count);
                    break;
            }
            var ret = new List<TransactionsStatus>();
            foreach (var c in idList)
            {
                int tmp;
                ret.Add(new TransactionsStatus()
                {
                    Key = c,
                    PassCount = lstPass.TryGetValue(c, out tmp) ? tmp : 0,
                    FailCount = lstFail.TryGetValue(c, out tmp) ? tmp : 0,
                    ApprovalCount = lstApproval.TryGetValue(c, out tmp) ? tmp : 0,
                    PendingCount = lstPending.TryGetValue(c, out tmp) ? tmp : 0,
                });
            }
            return ret;
        }

        public static List<Transaction> GetSettlementTransactions(int payID, ISortAndPage sortAndPage)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Read);
            //User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

            var expression = (IQueryable<Netpay.Dal.Netpay.tblCompanyTransPass>)from t in DataContext.Reader.tblCompanyTransPasses where (t.PrimaryPayedID == payID || t.PayID.Contains(";" + payID.ToString() + ";")) orderby t.InsertDate descending select t;
            //if (context.User.Type == UserRole.MerchantLimited || context.User.Type == UserRole.MerchantPrimary) expression = expression.Where(t => t.companyID == context.User.ID);

            if (sortAndPage != null)
                sortAndPage.DataFunction = (pi) => GetSettlementTransactions(payID, pi);

            List<Transaction> results = expression.ApplySortAndPage(sortAndPage).Select(t => new Transaction(t)).ToList<Transaction>();

            // remove installments not in this settlement
            foreach (var currentTransaction in results)
                if (currentTransaction.IsInstallments)
                    currentTransaction.InstallmentList.RemoveAll(i => i.SettlementID != payID);

            // duplicate denied status 4 (UnsettledBeenSettledAndDeducted)
            var tempDuplicates = new List<Transaction>();
            foreach (var currentTransaction in results)
            {
                if (currentTransaction.DeniedStatusID == (int)Netpay.Infrastructure.DeniedStatus.UnsettledBeenSettledAndDeducted)
                {
                    Transaction duplicate = (Transaction)currentTransaction.MemberwiseClone(); //.DeepCopy()
                    duplicate.Amount = duplicate.Amount * -1;
                    tempDuplicates.Add(duplicate);
                }
            }
            results.AddRange(tempDuplicates);

            return results;
        }

        public static List<int> GetUsedCurrencies(SearchFilters filters)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Read);

            var expression = from t in DataContext.Reader.tblCompanyTransPasses select t;
            if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
                expression = expression.Where(t => filters.merchantIDs.Contains(t.companyID));
            if (filters.paymentMethodID != null)
                expression = expression.Where(t => t.PaymentMethod == filters.paymentMethodID);

            return expression.GroupBy(t => t.Currency).Select(g => g.Key.Value).ToList();
        }

        public static Dictionary<int, CountAmount> GetUnsettledBalance(SearchFilters filters)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Read);

            var expression = from t in DataContext.Reader.tblCompanyTransPasses where t.UnsettledInstallments > 0 select t;
            if (filters != null)
            {
                // filters
                if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
                    expression = expression.Where(t => filters.merchantIDs.Contains(t.companyID));
                if (filters.date.From != null)
                    expression = expression.Where(t => t.InsertDate >= filters.date.From.Value.MinTime());
                if (filters.date.To != null)
                    expression = expression.Where(t => t.InsertDate <= filters.date.To.Value.AlignToEnd());
                if (filters.hasEpa != null)
                    expression = expression.Where(t => t.tblLogImportEPAs != null && t.tblLogImportEPAs.Count > 0 && t.tblLogImportEPAs.First().IsPaid == filters.hasEpa.Value);
            }

            var groupExpression = expression.GroupBy(t => t.Currency).Select(g => new { CurrencyID = g.Key, ca = new CountAmount(g.Sum(x => x.UnsettledAmount).GetValueOrDefault(), g.Count()) });
            var result = groupExpression.ToDictionary(k => k.CurrencyID.Value, v => v.ca);

            return result;
        }

        public static Dictionary<string, decimal> GetBalanceByCurrency(SearchFilters filters)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Read);

            var expression = from t in DataContext.Reader.tblCompanyTransPasses where t.CreditType == (byte)CreditType.Regular || t.CreditType == (byte)CreditType.Refund select t;
            if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
                expression = expression.Where(t => filters.merchantIDs.Contains(t.companyID));
            if (filters.paymentMethodID != null)
                expression = expression.Where(t => t.PaymentMethod == filters.paymentMethodID);
            if (filters.currencyID != null)
                expression = expression.Where(t => t.Currency == filters.currencyID);
            if (filters.HasUnsettledInstallments)
                //Check this filter logic !!
                expression = expression.Where(t => t.UnsettledInstallments > 0);

            var result = (from e in expression group e by e.Currency into x select new { x.Key, balance = x.Sum(t => t.CreditType == (byte)CreditType.Regular ? t.Amount : t.Amount * -1) }).ToDictionary(x => Currency.Get(x.Key.GetValueOrDefault()).IsoCode, x => x.balance);
            return result;
            //decimal result = expression.ToDictionary(t => .Sum(t => t.CreditType == (byte)CreditType.Regular ? t.Amount : t.Amount * -1);
            //return result;
        }

        public static decimal GetBalance(SearchFilters filters)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Read);

            var expression = from t in DataContext.Reader.tblCompanyTransPasses where t.CreditType == (byte)CreditType.Regular || t.CreditType == (byte)CreditType.Refund select t;
            if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
                expression = expression.Where(t => filters.merchantIDs.Contains(t.companyID));
            if (filters.paymentMethodID != null)
                expression = expression.Where(t => t.PaymentMethod == filters.paymentMethodID);
            if (filters.currencyID != null)
                expression = expression.Where(t => t.Currency == filters.currencyID);

            if (expression.Count() == 0)
                return 0;

            decimal result = expression.Sum(t => t.CreditType == (byte)CreditType.Regular ? t.Amount : t.Amount * -1);
            return result;
        }

        public static void RecalcMerchantPo(int nCompanyID, int nPayID)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Edit);

            var merchant = Bll.Merchants.Merchant.Load(nCompanyID);
            if (merchant != null)
            {
                int PayingDaysMargin = merchant.PayingDaysMargin;
                string PayingDates1 = merchant.PayingDates1;
                string PayingDates2 = merchant.PayingDates2;
                string PayingDates3 = merchant.PayingDates3;
                List<Merchants.Merchant.MerchantPayDate> MerchantPayDateList = Merchants.Merchant.MerchantPayDate.Parse(new string[] { PayingDates1, PayingDates2, PayingDates3 });

                if (PayingDaysMargin > 0)
                {
                    DataContext.Writer.tblCompanyTransPasses
                                       .Where(x => (x.companyID == nCompanyID && x.PrimaryPayedID == nPayID)).ToList()
                                       .ForEach(y => y.MerchantPD = y.InsertDate.AddDays(PayingDaysMargin));

                    DataContext.Writer.SubmitChanges();
                }
                else
                {
                    //Update merchantPD date
                    var transPassList = DataContext.Writer.tblCompanyTransPasses
                         .Where(x => x.companyID == nCompanyID && x.PrimaryPayedID == nPayID)
                         .ToList();

                    if (transPassList.Count() > 0)
                    {
                        foreach (var transPass in transPassList)
                        {
                            int nDay = transPass.InsertDate.Day;
                            foreach (var itemDate in MerchantPayDateList)
                            {
                                int xRet = IsDayInRange(itemDate, nDay);
                                if (xRet > -1)
                                {
                                    DateTime GetTransPayDate;
                                    if (xRet < nDay)
                                    {
                                        GetTransPayDate = new DateTime(transPass.InsertDate.Year, transPass.InsertDate.Month, xRet).AddMonths(1);
                                    }
                                    else
                                    {
                                        GetTransPayDate = new DateTime(transPass.InsertDate.Year, transPass.InsertDate.Month, xRet);
                                    }
                                    transPass.MerchantPD = GetTransPayDate;
                                    break;
                                }
                            }
                        }
                        DataContext.Writer.SubmitChanges();
                        //Till here good.

                        //Update installments dates.
                        var installmentsList = (from ti in DataContext.Writer.tblCompanyTransInstallments
                                                join tp in DataContext.Writer.tblCompanyTransPasses on ti.transAnsID equals tp.ID into tiAndtp
                                                from tiLeftJointp in tiAndtp.DefaultIfEmpty()
                                                where tiLeftJointp.companyID == nCompanyID && tiLeftJointp.PrimaryPayedID == nPayID
                                                select new { tblCompanyTransInstallments = ti, tblCompanyTransPass = tiLeftJointp })
                                     .ToList();

                        if (installmentsList.Count() > 0)
                        {
                            installmentsList.ForEach(z =>
                            {
                                z.tblCompanyTransInstallments.MerchantPD = z.tblCompanyTransPass.MerchantPD.AddMonths(z.tblCompanyTransInstallments.InsID);
                            }
                                       );
                        }
                        DataContext.Writer.SubmitChanges();
                    }

                }
            }
        }

        public static int IsDayInRange(Merchants.Merchant.MerchantPayDate merchantPayDate, int xDay)
        {
            if (merchantPayDate.From <= xDay && merchantPayDate.To >= xDay)
            {
                return merchantPayDate.When;
            }
            else return -1;
        }

        public static bool CheckIFMerchantAlreadyHadReserveOrRelease(int ncompanyid, int securityperiod)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Read);

            var result = (from tp in DataContext.Reader.tblCompanyTransPasses
                          where (tp.companyID == ncompanyid &&
                                 tp.PaymentMethod == (short)CommonTypes.PaymentMethodEnum.RollingReserve &&
                                 tp.CreditType == 0 &&
                                 tp.InsertDate < DateTime.Now.AddMonths(-securityperiod))
                          select tp)
                                 .FirstOrDefault();

            if (result == null) return false;
            else return true;
        }

    }
}
