﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Transactions
{
	public class BillingAddress : BaseDataObject, IAddress
	{
		public class SearchFilters
		{
			public string Address;
			public string City;
			public string PostalCode;
			public string StateISOCode;
			public string CountryISOCode;
		}

		protected Netpay.Dal.Netpay.TransPaymentBillingAddress _entity;

		public int ID { get { return _entity.TransPaymentBillingAddress_id; } }
		public string AddressLine1 { get { return _entity.Street1; } set { _entity.Street1 = value.NullIfEmpty(); } }
		public string AddressLine2 { get { return _entity.Street2; } set { _entity.Street2 = value.NullIfEmpty(); } }
		public string City { get { return _entity.City; } set { _entity.City = value.NullIfEmpty(); } }
		public string PostalCode { get { return _entity.PostalCode; } set { _entity.PostalCode = value.NullIfEmpty(); } }
		public string StateISOCode { get { return _entity.StateISOCode; } set { _entity.StateISOCode = value.NullIfEmpty(); } }
		public string CountryISOCode { get { return _entity.CountryISOCode; } set { _entity.CountryISOCode = value.NullIfEmpty(); } }

		internal BillingAddress(Dal.Netpay.TransPaymentBillingAddress entity)  { _entity = entity; }
		public BillingAddress()
		{
			_entity = new Dal.Netpay.TransPaymentBillingAddress();
		}

		public static BillingAddress Load(int id)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, Transaction.SecuredObject, PermissionValue.Read);

            //User user = Transaction.validateUser(credentialsToken);
            var exp = (from c in DataContext.Reader.TransPaymentBillingAddresses
					   where c.TransPaymentBillingAddress_id == id
					   select c);
			//if (user.Type == Infrastructure.UserType.Customer) exp = exp.Where(r => r.c.Customer_id == user.ID);
			var ret = exp.SingleOrDefault();
			if (ret == null) return null;
			return new BillingAddress(ret);
		}

		public int Save()
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, Transaction.SecuredObject, PermissionValue.Edit);

            //User user = Customer.validateCustomer(_entity.Customer_id);
            ValidationException.RaiseIfNeeded(Validate());
			DataContext.Writer.TransPaymentBillingAddresses.Update(_entity, (_entity.TransPaymentBillingAddress_id != 0));
			DataContext.Writer.SubmitChanges();
			return _entity.TransPaymentBillingAddress_id;
		}

		public void Delete()
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, Transaction.SecuredObject, PermissionValue.Delete);

            if (_entity.TransPaymentBillingAddress_id == 0) return;
			DataContext.Writer.TransPaymentBillingAddresses.Delete(_entity);
			DataContext.Writer.SubmitChanges();
		}

		internal static IQueryable<Dal.Netpay.TransPaymentBillingAddress> Search(Netpay.Dal.DataAccess.NetpayDataContext context, SearchFilters filters, out bool isEmptyFilters)
		{
			isEmptyFilters = true;
			var exp = (from ba in context.TransPaymentBillingAddresses select ba);
			if (!string.IsNullOrEmpty(filters.Address)) {
				isEmptyFilters = false;
				exp = exp.Where(ba => ba.Street1.Contains(filters.Address) || ba.Street2.Contains(filters.Address));
			}
			if (!string.IsNullOrEmpty(filters.City)) {
				isEmptyFilters = false;
				exp = exp.Where(ba => ba.City.Contains(filters.City));
			}
			if (!string.IsNullOrEmpty(filters.PostalCode)) {
				isEmptyFilters = false;
				exp = exp.Where(ba => ba.City.Contains(filters.PostalCode));
			}
			if (!string.IsNullOrEmpty(filters.CountryISOCode)) {
				isEmptyFilters = false;
				exp = exp.Where(ba => ba.CountryISOCode.Contains(filters.CountryISOCode));
			}
			if (!string.IsNullOrEmpty(filters.StateISOCode)) {
				isEmptyFilters = false;
				exp = exp.Where(ba => ba.StateISOCode.Contains(filters.StateISOCode));
			}
			return exp;
		}
	}
}
