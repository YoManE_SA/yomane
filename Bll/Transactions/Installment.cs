﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Transactions
{
	public class Installment : BaseDataObject
	{
		public class SearchFilters
		{
			public Infrastructure.Range<int?> ID;
			public int? MerchantId;
			public int? TransactionId;

		}		
		private Dal.Netpay.tblCompanyTransInstallment _entity;

		public int ID { get { return _entity.id; } }
		public byte Index { get { return _entity.InsID; } }
		public int SettlementID { get { return _entity.payID; } }
		public decimal Amount { get { return _entity.amount; } }
		public string Comment { get { return _entity.comment; } }
		public System.DateTime MerchantPD { get { return _entity.MerchantPD; } }
		public System.DateTime? MerchantRealPD { get { return _entity.MerchantRealPD; } }
		public int? MerchantId { get { return _entity.CompanyID; } }
		public int? TransactionId { get { return _entity.transAnsID; } }
        public bool? IsPaid { get; set; }
        
		private Installment(Dal.Netpay.tblCompanyTransInstallment entity)
		{
			_entity = entity;
		}

		public static List<Installment> LoadForTransaction(int transactionId)
		{
			return Search(new SearchFilters() { TransactionId = transactionId }, null);
		}

		public static List<Installment> Search(SearchFilters filters, ISortAndPage sortAndPage)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, TransactionBasicInfo.SecuredObject, PermissionValue.Read);

            var exp = from q in DataContext.Reader.tblCompanyTransInstallments
                      join logImportEpa in DataContext.Reader.tblLogImportEPAs
                      on new { p1 = q.transAnsID, p2 = q.InsID } equals new { p1 = (int?)logImportEpa.TransID, p2 = logImportEpa.Installment } into qAndEpa
                      from qAndEpaIncludeEmpty in qAndEpa.DefaultIfEmpty()
                      select new
                      {
                          q,
                          qAndEpaIncludeEmpty.IsPaid
                      };

            if (filters != null)
			{
				if (filters.ID.From.HasValue) exp = exp.Where((x) => x.q.id >= filters.ID.From.Value);
				if (filters.ID.To.HasValue) exp = exp.Where((x) => x.q.id <= filters.ID.To.Value);
				if (filters.MerchantId.HasValue) exp = exp.Where((x) => x.q.CompanyID == filters.MerchantId.Value);
				if (filters.TransactionId.HasValue) exp = exp.Where((x) => x.q.transAnsID == filters.TransactionId.Value);

			}
			return exp.ApplySortAndPage(sortAndPage).Select(x => new Installment(x.q) { IsPaid = x.IsPaid }).ToList();
		}
	}
}
