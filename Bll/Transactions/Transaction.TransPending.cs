﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using Microsoft.VisualBasic;

namespace Netpay.Bll.Transactions
{
    public partial class Transaction
    {
        public Transaction(Netpay.Dal.Netpay.tblCompanyTransPending entity)
            : this(TransactionStatus.Pending)
        {
            if (entity != null) LoadEntity(entity);
        }


        //This class is used to create parameter that is passed to the 
        //InsertPassedTransaction function.
        private class TransPendingExtended
        {
            public Netpay.Dal.Netpay.tblCompanyTransPending TransPendingEntity { get; set; }

            public bool? IsPendingReply { get; set; }

            public string PendingReplyUrl { get; set; }
        }

        private void LoadEntity(Netpay.Dal.Netpay.tblCompanyTransPending entity)
        {
            //
            FraudDetectionLogId = entity.FraudDetectionLog_id;
            TransOrder = entity.trans_order;
            DebitApprovalNumber = entity.DebitApprovalNumber; 
            IsTest = entity.isTestOnly;
            OriginalTransactionID = entity.trans_originalID;
            ReplyCode = entity.replyCode;
            //

            ID = entity.companyTransPending_id;
            Amount = entity.trans_amount;
            CurrencyID = entity.trans_currency.GetValueOrDefault();
            Installments = entity.trans_payments;
            CreditType = (Infrastructure.CreditType)entity.trans_creditType;
            SystemText = entity.SystemText;

            DebitCompanyID = entity.DebitCompanyID;
            MerchantID = entity.CompanyID;
            CustomerID = entity.CustomerID;
            PayerDataID = entity.TransPayerInfo_id;
            PaymentDataID = entity.TransPaymentMethod_id;

            PaymentMethodID = (CommonTypes.PaymentMethodEnum)entity.PaymentMethod.GetValueOrDefault();
            PaymentMethodReferenceID = entity.PaymentMethodID;
            PaymentMethodDisplay = entity.PaymentMethodDisplay;
            TransactionSource = (Infrastructure.TransactionSource?)entity.TransSource_id;
            TransType = (TransactionType)entity.trans_type;
            InsertDate = entity.insertDate;
            Comment = entity.Comment;
            IP = entity.IPAddress;
            Installments = 0;
            TerminalNumber = entity.TerminalNumber;
            PayerIDUsed = entity.payerIdUsed;
            DebitReferenceCode = entity.DebitReferenceCode;
            DebitReferenceNum = entity.DebitReferenceNum;
            ProductId = entity.MerchantProduct_id;

            IsCardPresent = entity.IsCardPresent;
        }

        private static IQueryable<Dal.Netpay.tblCompanyTransPending> SearchExpressionTransPending(LoadOptions loadOptions, SearchFilters filters)
        {
            DataContext.Reader.CommandTimeout = 120;
            var expression = (IQueryable<Netpay.Dal.Netpay.tblCompanyTransPending>)from t in DataContext.Reader.tblCompanyTransPendings orderby t.insertDate descending select t;
            if (Login.Current.Role == UserRole.Partner)
            {
                if (filters.merchantIDs == null) filters.merchantIDs = new List<int>();
                if (filters.customersIDs == null) filters.customersIDs = new List<int>();
                expression = expression.Where(t => filters.merchantIDs.Contains(t.CompanyID.Value) || filters.customersIDs.Contains(t.CustomerID));
            }
            else
            {
                if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
                    expression = expression.Where(t => filters.merchantIDs.Contains(t.CompanyID.Value));
                if (filters.customersIDs != null && filters.customersIDs.Count > 0)
                    expression = expression.Where(t => filters.customersIDs.Contains(t.CustomerID));
            }

            //Date
            if (filters.date.From != null)
                expression = expression.Where(t => t.insertDate >= filters.date.From.Value.MinTime());
            if (filters.date.To != null)
                expression = expression.Where(t => t.insertDate <= filters.date.To.Value.MaxTime());

            //Specific Transaction ID
            if (filters.specificTransactionID.HasValue)
            {
                expression = expression.Where(t => t.ID == filters.specificTransactionID.Value);
            }

            //Transaction ID Range
            if (filters.ID.From != null)
                expression = expression.Where(t => t.companyTransPending_id >= filters.ID.From.Value);
            if (filters.ID.To != null)
                expression = expression.Where(t => t.companyTransPending_id <= filters.ID.To.Value);

            //Terminal Number
            if (!string.IsNullOrWhiteSpace(filters.debitTerminalNumber))
                expression = expression.Where(t => t.TerminalNumber == filters.debitTerminalNumber);

            //Debit Company
            if (filters.debitCompanyID != null)
                expression = expression.Where(t => t.DebitCompanyID == filters.debitCompanyID.Value);

            //Payment Method
            if (filters.paymentMethodID != null)
                expression = expression.Where(t => t.PaymentMethod == filters.paymentMethodID.Value);

            //Credit Types
            if (filters.creditTypes != null && filters.creditTypes.Count() > 0)
            //expression = expression.Where(t => filters.creditTypes.Contains(t.trans_creditType));
            {
                //If the search include "Regular" type , return "Regular" and 
                //also values that are different than 0-"Refund" and 8-"Installments"
                if (filters.creditTypes.Contains(1))
                {
                    expression = expression.Where(t => (filters.creditTypes.Contains(t.trans_creditType)) || (t.trans_creditType != 0 && t.trans_creditType != 8));
                }
                else
                {
                    expression = expression.Where(t => (filters.creditTypes.Contains(t.trans_creditType)));
                }
            }

            //Order ID
            if (filters.orderID != null)
                expression = expression.Where(t => t.OrderNumber.StartsWith(filters.orderID));

            //Currency ID
            if (filters.currencyID != null)
                expression = expression.Where(t => t.trans_currency == filters.currencyID);

            //Amount
            if (filters.amount.From != null)
                expression = expression.Where(t => t.trans_amount >= filters.amount.From);
            if (filters.amount.To != null)
                expression = expression.Where(t => t.trans_amount <= filters.amount.To);

            //Reply Code
            if (filters.replyCode != null)
                expression = expression.Where(t => t.replyCode == filters.replyCode);

            ////Payment Methods Id's
            if (filters.paymentMethodsIds != null)
                expression = expression.Where(t => filters.paymentMethodsIds.Contains(t.PaymentMethod.Value));

            //Device ID
            if (filters.deviceId != null)
                expression = expression.Where(t => t.MobileDevice_id == filters.deviceId);

            //Payment status - not exist.

            //Payment Status bank DropDown - not exist.

            //Debit ref' code
            if (!string.IsNullOrEmpty(filters.debitReferenceCode))
            {
                expression = expression.Where(t => t.DebitReferenceCode == filters.debitReferenceCode.Trim());
            }

            //Debit ref' num.
            if (!string.IsNullOrEmpty(filters.debitReferenceNumber))
            {
                expression = expression.Where(t => t.DebitReferenceNum == filters.debitReferenceNumber.Trim());
            }

            //Approval Number
            if (!string.IsNullOrEmpty(filters.debitApprovalNumber))
            {
                expression = expression.Where(t => t.DebitApprovalNumber == filters.debitApprovalNumber);
            }

            //UnsettledOnly - not exist

            //Denied Status - not exist

            //Chb Reason - check if relevant.

            //Is Test 
            if (filters.isTest != null)
                expression = expression.Where(t => t.isTestOnly);

            //Original Transaction ID
            if (filters.originalTransactionID.GetValueOrDefault(0) > 0)
                expression = expression.Where(t => t.OriginalTransID == filters.originalTransactionID.Value);

            //Is Pending ChargeBack - not exist.

            //Batch Id - not exist

            //HistoryTypes
            if (filters.HistoryTypes != null)
                expression = expression.Where(t => DataContext.Reader.TransHistories.Where(v => filters.HistoryTypes.Contains((CommonTypes.TransactionHistoryType)v.TransHistoryType_id)).Select(v => v.TransPending_id).Contains(t.ID));

            //Payer Filters
            if (filters.PayerFilters != null)
            {
                bool isEmpty;
                var sq = Payer.Search(DataContext.Reader, filters.PayerFilters, out isEmpty);
                if (!isEmpty) expression = expression.Where(t => sq.Contains(t.trans_TransPayerInfo));
            }

            //Payment Filters
            if (filters.PaymentFilters != null)
            {
                bool isEmpty;
                var sq = Payment.Search(DataContext.Reader, filters.PaymentFilters, out isEmpty);
                if (!isEmpty) expression = expression.Where(t => sq.Contains(t.trans_TransPaymentMethod));
            }

            return expression;
        }

        private static List<Transaction> SearchTransPending(LoadOptions loadOptions, SearchFilters filters)
        {
            var expression = SearchExpressionTransPending(loadOptions, filters);

            List<Transaction> transactions = expression.ApplySortAndPage(loadOptions.sortAndPage).Select(t => new Transaction(t)).ToList<Transaction>();
            return transactions;
        }

        public static bool PendingResult(int pendingID, out TransactionStatus status, out int transID)
        {

            var logPendingFinalize = ((IQueryable<Netpay.Dal.Netpay.tblLogPendingFinalize>)from t in DataContext.Reader.tblLogPendingFinalizes where t.PendingID == pendingID select t).SingleOrDefault();
            transID = pendingID; status = TransactionStatus.Pending;
            if (logPendingFinalize == null)
                return false;
            if (logPendingFinalize.TransFailID.GetValueOrDefault() != 0)
            {
                status = TransactionStatus.Declined;
                transID = logPendingFinalize.TransFailID.GetValueOrDefault();
                return true;
            }
            else if (logPendingFinalize.TransPassID.GetValueOrDefault() != 0)
            {
                status = TransactionStatus.Captured;
                transID = logPendingFinalize.TransPassID.GetValueOrDefault();
                return true;
            }

            return false;
        }


        protected void SavePending()
        {
            var entity = new Netpay.Dal.Netpay.tblCompanyTransPending();
            //
            entity.FraudDetectionLog_id = FraudDetectionLogId;
            //
            if (PaymentData != null && PaymentData.OldCreditCardID != null)
            {
                //entity.PaymentMethod_id = 1;
                entity.CreditCardID = PaymentMethodReferenceID;
            }
            entity.replyCode = ReplyCode;
            entity.trans_amount = Amount;
            entity.Currency = CurrencyID;
            entity.trans_payments = Installments;
            entity.trans_creditType = (byte)CreditType;
            entity.DebitCompanyID = DebitCompanyID;
            entity.CompanyID = MerchantID.GetValueOrDefault();
            entity.CustomerID = CustomerID.GetValueOrDefault();
            entity.TransPayerInfo_id = PayerDataID;
            entity.TransPaymentMethod_id = PaymentDataID;
            entity.PaymentMethod = (short?)PaymentMethodID;
            entity.PaymentMethodID = PaymentMethodReferenceID;
            entity.PaymentMethodDisplay = PaymentMethodDisplay.EmptyIfNull();
            entity.isTestOnly = IsTest;
            entity.TransSource_id = (byte?)TransactionSource;
            entity.trans_type = (int)TransType;
            //entity.PayID = PrimaryPaymentID;
            entity.insertDate = InsertDate;
            //entity.DeniedStatus = DeniedStatusID;
            entity.OrderNumber = OrderNumber;
            entity.Comment = Comment;
            entity.IPAddress = IP;
            entity.TerminalNumber = TerminalNumber;
            //entity.referringUrl = ReferringUrl.EmptyIfNull();
            //entity.PayerInfo_id = PayerIDUsed.EmptyIfNull();
            entity.DebitReferenceCode = DebitReferenceCode;
            entity.DebitReferenceNum = DebitReferenceNum;
            //entity.IPCountry = IPCountry.EmptyIfNull();
            entity.PayforText = PayForText;
            entity.MerchantProduct_id = ProductId;
            // fees
            //entity.netpayFee_transactionCharge = TransactionFee;
            //entity.DebitFee = DebitFee;

            DataContext.Writer.tblCompanyTransPendings.InsertOnSubmit(entity);
            DataContext.Writer.SubmitChanges();
            ID = entity.ID;
        }

        //Phase 2
        public static int MovePendingTransaction(int pendingtransactionId, string returnCode, decimal? newamount = null, string systemtext = null)
        {            
            int newTransactionId = 0;
            string X_Email;
            byte? XOldMethod;
            string X_DebitReferenceCode;
            string X_DebitReferenceNum;
            string X_AcquirerReferenceNum;
            int? X_OCurrency;
            decimal X_OAmount;
            bool bIsNetpayTerminal;

            TransPendingExtended TransPendingExtendedEntity = (from tp in DataContext.Reader.tblCompanyTransPendings
                                                               join ca in DataContext.Reader.tblCompanyChargeAdmins on tp.CompanyID equals ca.company_id into transactionCa
                                                               join pm in DataContext.Reader.TransPaymentMethods on tp.TransPaymentMethod_id equals pm.TransPaymentMethod_id into transactionPm
                                                               where tp.companyTransPending_id == pendingtransactionId
                                                               from ca in transactionCa.DefaultIfEmpty()
                                                               from pm in transactionPm.DefaultIfEmpty()
                                                               orderby tp.companyTransPending_id descending
                                                               select new TransPendingExtended
                                                               {
                                                                   TransPendingEntity = tp,
                                                                   IsPendingReply = (bool?)ca.isPendingReply,
                                                                   PendingReplyUrl = ca.PendingReplyUrl
                                                               })
                                                               .SingleOrDefault();    

            if (TransPendingExtendedEntity != null)
            {
                //Set variable(for shorter code.)
                var dbPendingEntity = TransPendingExtendedEntity.TransPendingEntity;

                //Save old amount 
                //decimal oldamount = dbPendingEntity.trans_amount;

                //set new amount if needed
                //if (newamount.HasValue && newamount.Value < dbPendingEntity.trans_amount) dbPendingEntity.trans_amount = newamount.Value;

                //Set new comment if needed
                //if (newamount.HasValue && newamount.Value < dbPendingEntity.trans_amount) dbPendingEntity.Comment = string.Format("Original Amount:{0},AdminUser:{1},Note:{2}", oldamount, AdminUser.Current.FullName, note);
               
                XOldMethod = (dbPendingEntity.PaymentMethod >= (int)CommonTypes.PaymentMethodEnum.EC_MIN && dbPendingEntity.PaymentMethodID <= (int)CommonTypes.PaymentMethodEnum.EC_MAX) ? (byte)2 : (byte)1;
                X_DebitReferenceCode = dbPendingEntity.DebitReferenceCode;
                X_DebitReferenceNum = dbPendingEntity.DebitReferenceNum;
                X_AcquirerReferenceNum = dbPendingEntity.AcquirerReferenceNum;
                X_Email = Bll.Transactions.Transaction.GetTransaction(null, pendingtransactionId, TransactionStatus.Pending).PayerData.EmailAddress;

                if (returnCode == "000")
                {
                    X_OCurrency = TransPendingExtendedEntity.TransPendingEntity.trans_currency;
                    X_OAmount = TransPendingExtendedEntity.TransPendingEntity.trans_amount;
                    bIsNetpayTerminal = Bll.DebitCompanies.Terminal.Load(TransPendingExtendedEntity.TransPendingEntity.DebitCompanyID.Value, TransPendingExtendedEntity.TransPendingEntity.TerminalNumber).IsNetpayTerminal;

                    //Invoke the function "insertPassedTransaction"
                    newTransactionId = InsertPassedTransaction(null, dbPendingEntity.CompanyID, dbPendingEntity.FraudDetectionLog_id, dbPendingEntity.transactionSource_id, XOldMethod, dbPendingEntity.trans_creditType, dbPendingEntity.CustomerID, dbPendingEntity.IPAddress, dbPendingEntity.trans_amount, dbPendingEntity.trans_currency, dbPendingEntity.PaymentMethodDisplay, dbPendingEntity.trans_payments, dbPendingEntity.trans_order, dbPendingEntity.PayforText, dbPendingEntity.MerchantProduct_id, dbPendingEntity.Comment, dbPendingEntity.TerminalNumber, dbPendingEntity.DebitApprovalNumber, dbPendingEntity.isTestOnly, dbPendingEntity.PaymentMethod, null, dbPendingEntity.PaymentMethodID, null, dbPendingEntity.DebitCompanyID, dbPendingEntity.TransPayerInfo_id, dbPendingEntity.TransPaymentMethod_id, "0", dbPendingEntity.Is3DSecure, dbPendingEntity.MobileDevice_id, dbPendingEntity.OriginalTransID, X_DebitReferenceCode, X_DebitReferenceNum, X_AcquirerReferenceNum, X_OCurrency, X_OAmount, dbPendingEntity.IsCardPresent, systemtext);

                    //Send notifications
                    if (newTransactionId != 0)
                    {
                        var newPassedTransaction = Transaction.GetTransaction(null, newTransactionId, TransactionStatus.Captured);
                        if (newPassedTransaction == null) throw new Exception("new passed transaction wasn't found.");

                        var processSettings = Bll.Merchants.Merchant.Load(dbPendingEntity.CompanyID).ProcessSettings;
                        if (processSettings != null)
                        {
                            if (processSettings.IsPassNotificationSentToMerchant)
                            {
                                newPassedTransaction.SendMerchantEmail();
                            }
                            if (processSettings.IsPassNotificationSentToPayer)
                            {
                                newPassedTransaction.SendClientEmail();
                            }
                        }
                    }
                }
                else // Inset to Fail flow
                {                    
                    newTransactionId = InsertFailedTransaction(null, dbPendingEntity.CompanyID, dbPendingEntity.FraudDetectionLog_id, dbPendingEntity.transactionSource_id, dbPendingEntity.trans_type, dbPendingEntity.trans_creditType, dbPendingEntity.CustomerID, dbPendingEntity.IPAddress, dbPendingEntity.trans_amount, dbPendingEntity.trans_currency, dbPendingEntity.PaymentMethodDisplay, dbPendingEntity.trans_payments, dbPendingEntity.trans_order, returnCode, "", dbPendingEntity.PayforText, dbPendingEntity.MerchantProduct_id, dbPendingEntity.Comment, dbPendingEntity.TerminalNumber, dbPendingEntity.PaymentMethod, XOldMethod, dbPendingEntity.PaymentMethodID, dbPendingEntity.isTestOnly, "", dbPendingEntity.DebitCompanyID, dbPendingEntity.TransPayerInfo_id, dbPendingEntity.TransPaymentMethod_id, "0", dbPendingEntity.Is3DSecure, dbPendingEntity.MobileDevice_id, X_DebitReferenceCode, X_DebitReferenceNum, dbPendingEntity.IsCardPresent, systemtext);

                    //Send notification if needed
                    if (newTransactionId != 0)
                    {
                        var newFailedTransaction = Transaction.GetTransaction(null, newTransactionId, TransactionStatus.Declined);

                        var processSettings = Bll.Merchants.Merchant.Load(dbPendingEntity.CompanyID).ProcessSettings;
                        if (processSettings != null)
                        {
                            if (processSettings.IsFailNotificationSentToMerchant)
                            {
                                newFailedTransaction.SendMerchantEmail();
                            }
                        }
                    }
                }

                //Updates on db if a new passed or fail transaction created.
                if (newTransactionId != 0)
                {
                    //Update Data.Cart table:
                    string updateCartQuery = "Update Data.Cart Set TransPending_id=Null" + (returnCode == "000" ? ", TransPass_id=" + newTransactionId : "") + " Where TransPending_id=" + pendingtransactionId;
                    DataContext.Writer.ExecuteCommand(updateCartQuery);

                    //Update eventPending table
                    DataContext.Writer.EventPendings.DeleteAllOnSubmit(DataContext.Writer.EventPendings.Where(x => x.TransPending_id == pendingtransactionId));

                    //Delete pending transaction entity
                    DataContext.Writer.tblCompanyTransPendings.DeleteOnSubmit(DataContext.Writer.tblCompanyTransPendings.Where(x => x.companyTransPending_id == pendingtransactionId).SingleOrDefault());

                    //Insert value to tblLogPendingFinalize
                    var newLogPendingFinalizeEntity = new Netpay.Dal.Netpay.tblLogPendingFinalize();
                    newLogPendingFinalizeEntity.FinalizeDate = DateTime.Now;
                    newLogPendingFinalizeEntity.PendingID = pendingtransactionId;
                    newLogPendingFinalizeEntity.TransPassID = returnCode == "000" ? newTransactionId.ToNullableInt() : null;
                    newLogPendingFinalizeEntity.TransFailID = returnCode != "000" ? newTransactionId.ToNullableInt() : null;
                    DataContext.Writer.tblLogPendingFinalizes.InsertOnSubmit(newLogPendingFinalizeEntity);

                    //Create pending event
                    if (TransPendingExtendedEntity.IsPendingReply.HasValue && TransPendingExtendedEntity.IsPendingReply.Value && !string.IsNullOrEmpty(TransPendingExtendedEntity.PendingReplyUrl))
                    {
                        if (returnCode == "000")
                        {
                            PendingEvents.Create(CommonTypes.PendingEventType.PendingSendNotify, newTransactionId, TransactionStatus.Captured, null);
                        }
                        else
                        {
                            PendingEvents.Create(CommonTypes.PendingEventType.PendingSendNotify, newTransactionId, TransactionStatus.Declined, null);
                        }
                    }

                    DataContext.Writer.SubmitChanges();
                }
            }

            return newTransactionId;
        }


        public static int InsertPassedTransaction(DateTime? insertDateOutParam, int? companyId, int? fraudDetectionLog_id, int? transactionTypeId,
                                                    byte? iMethodType, byte creditTypeId, int? customerId, string customerIp, decimal amount, int? transCurrency, string ccTypeEngString,
                                                    byte payments, string orderNumber, string payFor, int? productId, string comment, string terminalNumber, string approvalNumber, bool isTestOnly, short? PaymentMethod,
                                                    DateTime? pDate, int? cardId, string referringUrl, int? debitCompanyId, int? transPayerInfo_id, int? transPaymentMethod_id, string PayerID, bool? is3DSecure, int? deviceId, int? OriginalTransID,
                                                    string X_DebitReferenceCode, string X_DebitReferenceNum, string X_AcquirerReferenceNum, int? X_OCurrency, decimal? X_OAmount, bool? iscardpresent, string systemtext = null)
        {            
            decimal netpayFeeTransactionCharge = 0;
            decimal netpayFeeRatioCharge = 0;
            decimal debitFeeTransactionCharge;
            decimal RatioChargeParam;
            var sTempChr = "";
            var xBin = "";
            string paymentId = "";
            var xBinFraud = "";
            byte CTP_Status = 0;
            DateTime? MerchantPD;

            xBin = PaymentMethod >= (int)CommonTypes.PaymentMethodEnum.CCUnknown && transPaymentMethod_id.HasValue ? GetTransPaymentMethodIssuerCounrtyIsoCode(transPaymentMethod_id.Value) : "--";

            //Get fee transaction charge
            netpayFeeTransactionCharge = GetTransactionFees(ref netpayFeeRatioCharge, companyId, PaymentMethod, transCurrency, amount, xBin, creditTypeId);

            decimal outChbFee;
            debitFeeTransactionCharge = GetTransactionDebitFees(debitCompanyId, terminalNumber, PaymentMethod, transCurrency, amount, creditTypeId, ref pDate, 0, netpayFeeRatioCharge, out outChbFee, out RatioChargeParam);

            DateTime sInsertDate = DateTime.Now;

            sTempChr = "0";
            bool bIsNetpayTerminal = Bll.DebitCompanies.Terminal.Load(debitCompanyId.Value, terminalNumber).IsNetpayTerminal;
            if (!bIsNetpayTerminal) sTempChr = "X";
            if ((creditTypeId == 8) && payments > 1 && sTempChr.Trim() == "0")
            {
                for (int j = 1; j <= payments; j++)
                {
                    paymentId += ";" + sTempChr;
                }
                paymentId += ";";
            }
            else
            {
                paymentId = ";" + sTempChr + ";";
            }

            if (fraudDetectionLog_id.HasValue && fraudDetectionLog_id.Value != 0)
            {
                xBinFraud = DataContext.Reader.FraudDetections.Where(x => x.FraudDetection_id == fraudDetectionLog_id.Value).Select(x => x.ReturnBinCountry).SingleOrDefault();
            }

            MerchantPD = GetTransPayDate(companyId.Value, DateTime.Now, creditTypeId);
            if (!MerchantPD.HasValue) throw new Exception("Unable to get MerchantPD");
            
            decimal nUnsettledAmount;
            int nUnsettledInstallments;

            if (paymentId.ToUpper() == ";X;")
            {
                nUnsettledAmount = 0;
            }
            else if (creditTypeId == 0)
            {
                nUnsettledAmount = -amount;
            }
            else
            {
                nUnsettledAmount = amount;
            }

            if (paymentId.ToUpper() == ";X;")
            {
                nUnsettledInstallments = 0;
            }
            else if (creditTypeId == 6)
            {
                nUnsettledInstallments = 1;
            }
            else
            {
                nUnsettledInstallments = payments;
            }

            string sRefTrans = null;

            var newPassedEntity = new Dal.Netpay.tblCompanyTransPass();
            newPassedEntity.companyID = companyId.Value;
            newPassedEntity.InsertDate = sInsertDate; 
            newPassedEntity.FraudDetectionLog_id = fraudDetectionLog_id.HasValue ? fraudDetectionLog_id.Value : 0;
            newPassedEntity.TransSource_id = (byte?)transactionTypeId;
            newPassedEntity.PayID = paymentId.TakeLeftChars(305);
            newPassedEntity.CreditType = creditTypeId;
            newPassedEntity.CustomerID = customerId.HasValue ? customerId.Value : 0;
            newPassedEntity.IPAddress = customerIp.TakeLeftChars(50);
            newPassedEntity.Amount = amount;
            newPassedEntity.Currency = transCurrency;
            newPassedEntity.Payments = payments;
            newPassedEntity.OrderNumber = orderNumber.TakeLeftChars(100);
            newPassedEntity.PayforText = payFor.TakeLeftChars(100);
            newPassedEntity.MerchantProduct_id = productId;
            newPassedEntity.Comment = comment.TakeLeftChars(500);
            newPassedEntity.TerminalNumber = terminalNumber.TakeLeftChars(10);
            newPassedEntity.ApprovalNumber = approvalNumber.TakeLeftChars(50);
            newPassedEntity.isTestOnly = isTestOnly;
            newPassedEntity.PD = pDate.HasValue ? pDate.Value : DataContext.SQLMinDateValue;
            newPassedEntity.MerchantPD = MerchantPD.Value;
            newPassedEntity.PaymentMethod_id = iMethodType.Value;
            newPassedEntity.PaymentMethodID = cardId.HasValue ? cardId.Value : 0;
            newPassedEntity.PaymentMethod = PaymentMethod;
            newPassedEntity.PaymentMethodDisplay = ccTypeEngString.TakeLeftChars(50);
            newPassedEntity.referringUrl = string.IsNullOrEmpty(referringUrl) ? string.Empty : referringUrl.TakeLeftChars(500);
            newPassedEntity.DebitCompanyID = debitCompanyId;
            newPassedEntity.payerIdUsed = PayerID.TakeLeftChars(10);
            newPassedEntity.OriginalTransID = OriginalTransID.HasValue ? OriginalTransID.Value : 0;
            newPassedEntity.netpayFee_ratioCharge = netpayFeeRatioCharge;
            newPassedEntity.netpayFee_transactionCharge = netpayFeeTransactionCharge;
            newPassedEntity.DebitReferenceCode = X_DebitReferenceCode.TakeLeftChars(40);
            newPassedEntity.DebitReferenceNum = X_DebitReferenceNum.TakeLeftChars(40);
            newPassedEntity.AcquirerReferenceNum = string.IsNullOrEmpty(X_AcquirerReferenceNum) ? null : X_AcquirerReferenceNum.TakeLeftChars(40);
            newPassedEntity.OCurrency = X_OCurrency.HasValue ? ((byte)(X_OCurrency.Value)).ToNullableByte() : null;
            newPassedEntity.OAmount = X_OAmount;
            newPassedEntity.IPCountry = xBinFraud.TakeLeftChars(2);
            newPassedEntity.CTP_Status = CTP_Status;
            newPassedEntity.DebitFee = debitFeeTransactionCharge;
            newPassedEntity.UnsettledAmount = nUnsettledAmount;
            newPassedEntity.UnsettledInstallments = nUnsettledInstallments;
            newPassedEntity.RefTrans = sRefTrans;
            newPassedEntity.MobileDevice_id = deviceId.HasValue ? deviceId : null;
            newPassedEntity.TransPayerInfo_id = transPayerInfo_id;
            newPassedEntity.TransPaymentMethod_id = transPaymentMethod_id;
            newPassedEntity.Is3DSecure = is3DSecure;
            newPassedEntity.DeniedAdminComment = ""; 
            newPassedEntity.IsCardPresent = iscardpresent;
            newPassedEntity.SystemText = systemtext;

            //Set default values
            newPassedEntity.netpayFee_chbCharge = 0;
            newPassedEntity.netpayFee_ClrfCharge = 0;
            newPassedEntity.DeniedDate = DataContext.SQLMinDateValue;
            newPassedEntity.DeniedPrintDate = DataContext.SQLMinDateValue;
            newPassedEntity.DeniedSendDate = DataContext.SQLMinDateValue;
            newPassedEntity.replyCode = string.Empty;

            DataContext.Writer.tblCompanyTransPasses.InsertOnSubmit(newPassedEntity);
            DataContext.Writer.SubmitChanges();

            return newPassedEntity.ID;
        }

        public static int InsertFailedTransaction(DateTime? insertDateOutParam, int? companyId, int? fraudDetectionLog_id, int? transactionTypeId, int? transTypeId, byte creditTypeId, int? customerId, string customerIp, decimal amount, int? transCurrcency, string ccTypeEngString, byte payments, string orderNumber, string replyCode, string debitDeclineReason, string payFor, int? productId, string comment, string terminalNumber, short? PaymentMethod, byte? iMethodType, int? cardId, bool? isTestOnly, string referringUrl, int? debitCompanyId, int? transPayerInfo_id, int? transPaymentMethod_id, string PayerID, bool? is3DSecure, int? deviceId, string X_DebitReferenceCode, string X_DebitReferenceNum, bool? iscardpresent, string systemtext=null)
        {
            int newTransactionId = 0;
            
            //Get the xBin value 
            string xBin = "";
            xBin = PaymentMethod >= (int)CommonTypes.PaymentMethodEnum.CCUnknown && transPayerInfo_id.HasValue ? GetTransPaymentMethodIssuerCounrtyIsoCode(transPayerInfo_id.Value) : "--";

            string xBinFraud = "";
            if (fraudDetectionLog_id.HasValue && fraudDetectionLog_id.Value != 0)
            {
                xBinFraud = DataContext.Reader.FraudDetections.Where(x => x.FraudDetection_id == fraudDetectionLog_id.Value).Select(x => x.ReturnBinCountry).SingleOrDefault();
            }

            decimal netpayFeeTransactionCharge = 0;
            decimal debitFee = 0;

            //Check if chargeFail is true at the current Response code flow, if true get the failed fee
            var ResponseCodeItem = Bll.DebitCompanies.ResponseCode.GetItem(debitCompanyId.GetValueOrDefault(-1), replyCode);
            if (ResponseCodeItem != null && ResponseCodeItem.ChargeFail)
            {
                netpayFeeTransactionCharge = GetUsageFees(companyId, PaymentMethod, transCurrcency, xBin, 1);
            }

            //Get the debit usage fee
            debitFee = GetTransactionDebitUsageFees(debitCompanyId, terminalNumber, PaymentMethod, transCurrcency, amount, creditTypeId);

            var newFailedEntity = new Dal.Netpay.tblCompanyTransFail();
            newFailedEntity.CompanyID = companyId;
            newFailedEntity.InsertDate = DateTime.Now;
            newFailedEntity.FraudDetectionLog_id = fraudDetectionLog_id.HasValue ? fraudDetectionLog_id.Value : 0;
            newFailedEntity.TransSource_id = (byte?)transactionTypeId;
            newFailedEntity.TransType = transTypeId.HasValue ? transTypeId.Value : -1;
            newFailedEntity.CreditType = creditTypeId;
            newFailedEntity.CustomerID = customerId.HasValue ? customerId.Value : 0;
            newFailedEntity.IPAddress = customerIp.TakeLeftChars(50);
            newFailedEntity.Amount = amount;
            newFailedEntity.Currency = transCurrcency.HasValue ? transCurrcency.Value : 1;
            newFailedEntity.Payments = payments;
            newFailedEntity.OrderNumber = orderNumber.TakeLeftChars(100);
            newFailedEntity.replyCode = replyCode.TakeLeftChars(50);
            newFailedEntity.DebitDeclineReason = string.IsNullOrEmpty(debitDeclineReason) ? null : (debitDeclineReason.TakeLeftChars(500));
            newFailedEntity.PayforText = payFor.TakeLeftChars(100);
            newFailedEntity.MerchantProduct_id = productId;
            newFailedEntity.Comment = comment.TakeLeftChars(500);
            newFailedEntity.TerminalNumber = terminalNumber.TakeLeftChars(10);
            newFailedEntity.PaymentMethod = PaymentMethod;
            newFailedEntity.PaymentMethod_id = iMethodType.Value;
            newFailedEntity.PaymentMethodID = cardId.HasValue ? cardId.Value : 0;
            newFailedEntity.PaymentMethodDisplay = ccTypeEngString.TakeLeftChars(50);
            newFailedEntity.isTestOnly = isTestOnly.Value;
            newFailedEntity.referringUrl = referringUrl.TakeLeftChars(500);
            newFailedEntity.DebitCompanyID = debitCompanyId;
            newFailedEntity.payerIdUsed = PayerID.TakeLeftChars(10);
            newFailedEntity.DebitReferenceCode = X_DebitReferenceCode.TakeLeftChars(40);
            newFailedEntity.DebitReferenceNum = X_DebitReferenceNum.TakeLeftChars(40);
            newFailedEntity.netpayFee_transactionCharge = netpayFeeTransactionCharge;
            newFailedEntity.DebitFee = debitFee;
            newFailedEntity.IPCountry = xBinFraud.TakeLeftChars(2);
            newFailedEntity.ctf_JumpIndex = 0;
            newFailedEntity.IsGateway = true; //Always true
            newFailedEntity.MobileDevice_id = deviceId;
            newFailedEntity.TransPayerInfo_id = transPayerInfo_id;
            newFailedEntity.TransPaymentMethod_id = transPaymentMethod_id;
            newFailedEntity.Is3DSecure = is3DSecure;
            newFailedEntity.IsCardPresent = iscardpresent;
            newFailedEntity.SystemText = systemtext;

            DataContext.Writer.tblCompanyTransFails.InsertOnSubmit(newFailedEntity);
            DataContext.Writer.SubmitChanges();

            newTransactionId = newFailedEntity.ID;

            return newTransactionId;
        }

        public static decimal GetTransactionFees(ref decimal RatioChargeParam, int? ixCompanyID, short? ixPaymentMethod, int? ixOCurrency, decimal ixAmount, string binsCountry, int ixTypeCredit)
        {
            binsCountry = string.Format(",{0},", binsCountry);

            var rsFees = (from ccf in DataContext.Reader.tblCompanyCreditFees
                          where ccf.CCF_CompanyID == ixCompanyID &&
                                ccf.CCF_CurrencyID == ixOCurrency &&
                                ccf.CCF_PaymentMethod == ixPaymentMethod &&
                                (ccf.CCF_ListBINs.Contains(binsCountry) || ccf.CCF_ListBINs == "")
                          orderby ccf.CCF_IsDisabled ascending, ccf.CCF_ListBINs descending
                          select ccf)
                          .FirstOrDefault();

            if (rsFees == null) throw new Exception(string.Format("Terminal for merchant:{0},currency:{1},paymentmethod:{2} wasn't found.", ixCompanyID, ixOCurrency, ixPaymentMethod));

            if (rsFees != null)
            {
                if (ixTypeCredit == 0)
                {
                    RatioChargeParam = 0;
                    return rsFees.CCF_RefundFixedFee;
                }
                else
                {
                    RatioChargeParam = (ixAmount * (rsFees.CCF_PercentFee / 100));
                    return rsFees.CCF_FixedFee;
                }
            }
            else
            {
                RatioChargeParam = 0;
                return 0;
            }
        }

        public static decimal GetTransactionDebitFees(int? ixDebitCompanyID, string sTerminal, short? ixPaymentMethod, int? ixCurrency, decimal ixAmount, byte ixTypeCredit, ref DateTime? payDate, int deniedSt, decimal transRatioFeeCharge, out decimal outChbFee, out decimal outRatioChargeParam)
        {
            decimal GetTransactionDebitFeesVar;
            decimal nMaxPrec;
            outRatioChargeParam = 0; //Check this line !
            outChbFee = 0;

            var rsFees = (from dcf in DataContext.Reader.tblDebitCompanyFees
                          where dcf.DCF_DebitCompanyID == ixDebitCompanyID &&
                                (dcf.DCF_CurrencyID == 255 || dcf.DCF_CurrencyID == ixCurrency) &&
                                (dcf.DCF_PaymentMethod == 0 || dcf.DCF_PaymentMethod == ixPaymentMethod) &&
                                (dcf.DCF_TerminalNumber == "" || dcf.DCF_TerminalNumber == sTerminal)
                          orderby dcf.DCF_TerminalNumber descending, dcf.DCF_CurrencyID ascending, dcf.DCF_PaymentMethod descending
                          select dcf)
                                .FirstOrDefault();

            if (rsFees != null)
            {
                if (ixTypeCredit == 0)
                {
                    outRatioChargeParam = 0; // in AdminCash this variable is called "RatioChargeParam" , Check this issue! look at AdminCash 'Func_TransCharge.asp' line 359
                    outChbFee = 0;
                    return rsFees.DCF_RefundFixedFee;
                }
                else
                {
                    GetTransactionDebitFeesVar = rsFees.DCF_FixedFee;
                    if (rsFees.DCF_FixedCurrency != 255)
                    {
                        GetTransactionDebitFeesVar = Bll.Currency.Convert(GetTransactionDebitFeesVar, (CommonTypes.Currency)rsFees.DCF_FixedCurrency, (CommonTypes.Currency)ixCurrency);
                    }

                    //till here checked
                    if (rsFees.DCF_MaxPrecFee != null)
                    {
                        nMaxPrec = rsFees.DCF_MaxPrecFee.Value;
                        if (rsFees.DCF_MaxPrecFee == 0 && transRatioFeeCharge != 0) nMaxPrec = (transRatioFeeCharge / ixAmount) * 100;
                        GetTransactionDebitFeesVar = decimal.Round(GetTransactionDebitFeesVar + ((ixAmount * ((rsFees.DCF_MinPrecFee.Value) / 100) + ((nMaxPrec - rsFees.DCF_MinPrecFee.Value) / 100) * ((rsFees.DCF_PercentFee) / 100))), 2);
                    }
                    //till here checked

                    else if (rsFees.DCF_MinPrecFee != 0)
                    {
                        GetTransactionDebitFeesVar = decimal.Round(GetTransactionDebitFeesVar + (ixAmount * (rsFees.DCF_MinPrecFee.Value / 100) * (rsFees.DCF_MinPrecFee.Value / 100)), 2);
                    }
                    //till here checked

                    else
                    {
                        GetTransactionDebitFeesVar = decimal.Round(GetTransactionDebitFeesVar + (ixAmount * (rsFees.DCF_PercentFee / 100)), 2);
                    }
                    //till here checked

                    if (deniedSt > 0)
                    {
                        outChbFee = rsFees.DCF_CBFixedFee;
                        if (rsFees.DCF_CHBCurrency != 255)
                        {
                            outChbFee = Bll.Currency.Convert(outChbFee, (CommonTypes.Currency)rsFees.DCF_CHBCurrency, (CommonTypes.Currency)ixCurrency);
                        }
                    }
                }
                payDate = GetTransactionDebitPayDate(DateTime.Now, rsFees.DCF_PayTransDays, rsFees.DCF_PayINDays);
            }
            else
            {
                outRatioChargeParam = 0; // Check what is this variable.
                GetTransactionDebitFeesVar = 0;
                outChbFee = 0;
            }

            return GetTransactionDebitFeesVar;
        }

        public static decimal GetUsageFees(int? ixCompanyID, int? PaymentMethod, int? ixOCurrency, string binsCountry, int bGetSource) //1=fail, 2=approval
        {
            if (!ixCompanyID.HasValue) throw new Exception("Company id doesn't have value.");
            if (!PaymentMethod.HasValue) throw new Exception("PaymentMethod doesn't have value.");
            if (!ixOCurrency.HasValue) throw new Exception("Currency doesn't have value.");

            binsCountry = string.Format(",{0},", binsCountry);

            decimal result = 0;
            var rsFees = (from terminals in DataContext.Reader.tblCompanyCreditFees
                          where terminals.CCF_CompanyID == ixCompanyID.Value &&
                                terminals.CCF_CurrencyID == ixOCurrency.Value &&
                                terminals.CCF_PaymentMethod == PaymentMethod.Value &&
                                (terminals.CCF_ListBINs == "" || terminals.CCF_ListBINs.Contains(binsCountry))
                          orderby terminals.CCF_IsDisabled ascending, terminals.CCF_ListBINs descending
                          select new { FailFixedFee = terminals.CCF_FailFixedFee, ApprovedFixedFee = terminals.CCF_ApproveFixedFee })
                          .FirstOrDefault();

            if (rsFees != null)
            {
                if (bGetSource == 1) result = rsFees.FailFixedFee;
                else if (bGetSource == 2) result = rsFees.ApprovedFixedFee;
            }

            return result;
        }

        public static decimal GetTransactionDebitUsageFees(int? ixDebitCompanyID, string sTerminal, int? ixPaymentMethod, int? ixCurrency, decimal ixAmount, byte ixTypeCredit)
        {
            if (!ixDebitCompanyID.HasValue) throw new Exception("ixDebitCompanyID variable is null");
            if (!ixPaymentMethod.HasValue) throw new Exception("ixPaymentMethod variable is null");
            if (!ixCurrency.HasValue) throw new Exception("ixCurrency variable is null");

            decimal result = 0;

            var rsFees = (from dcf in DataContext.Reader.tblDebitCompanyFees
                          where dcf.DCF_DebitCompanyID == ixDebitCompanyID.Value &&
                                (dcf.DCF_CurrencyID == 255 || dcf.DCF_CurrencyID == ixCurrency.Value) &&
                                (dcf.DCF_PaymentMethod == 0 || dcf.DCF_PaymentMethod == ixPaymentMethod.Value) &&
                                (dcf.DCF_TerminalNumber == "" || dcf.DCF_TerminalNumber == null || dcf.DCF_TerminalNumber == sTerminal)
                          orderby dcf.DCF_TerminalNumber descending, dcf.DCF_CurrencyID ascending, dcf.DCF_PaymentMethod descending
                          select new { FailFixedFee = dcf.DCF_FailFixedFee })
                          .FirstOrDefault();

            if (rsFees != null) result = rsFees.FailFixedFee;

            return result;
        }


        public static DateTime GetTransactionDebitPayDate(DateTime nDate, string DCF_PayTransDays, string DCF_PayINDays)
        {
            DateTime GetTransactionDebitPayDateVar = nDate;

            int i;
            if (DCF_PayTransDays == "" || DCF_PayINDays == "")
            {
                return GetTransactionDebitPayDateVar;
            }
            string[] DCF_PayTransDaysArray = DCF_PayTransDays.Split(',');
            string[] DCF_PayINDaysArray = DCF_PayINDays.Split(',');

            for (i = DCF_PayTransDaysArray.Length - 1; i >= 0; i--)
            {
                if ((nDate.Day) >= int.Parse(DCF_PayTransDaysArray[i]))
                {
                    break;
                }
            }

            if (DCF_PayINDays.Length - 1 >= i)
            {
                GetTransactionDebitPayDateVar = new DateTime(nDate.Year, nDate.Month, int.Parse(DCF_PayINDaysArray[i]));
                if (int.Parse(DCF_PayINDaysArray[i]) <= nDate.Day)
                {
                    GetTransactionDebitPayDateVar = GetTransactionDebitPayDateVar.AddMonths(1);
                }
            }
            else
            {
                GetTransactionDebitPayDateVar = nDate;
            }

            return GetTransactionDebitPayDateVar;
        }

        public static string GetTransPaymentMethodIssuerCounrtyIsoCode(int transpaymentmethodid)
        {
            return (from transPaymentMethodtable in DataContext.Reader.TransPaymentMethods
                    where transPaymentMethodtable.TransPaymentMethod_id == transpaymentmethodid
                    select transPaymentMethodtable.IssuerCountryIsoCode)
                    .SingleOrDefault();
        }

        public static DateTime? GetTransPayDate(int cmpID, DateTime xTransDate, int xCreditType)
        {
            DateTime ret = new DateTime(1901, 1, 1);

            if (xCreditType == 0)
            {
                return xTransDate;
            }

            var merchant = Bll.Merchants.Merchant.Search(new Merchants.Merchant.SearchFilters() { MerchantId = cmpID }, null).SingleOrDefault();

            if (merchant != null)
            {
                var date = merchant.DateFirstTransPass != null ? merchant.DateFirstTransPass : DateTime.Now;
                if (merchant.PayingDaysMarginInitial > 0 && (((TimeSpan)(date - DateTime.Now)).Days) < merchant.PayingDaysMarginInitial)
                {
                    ret = xTransDate.AddDays(merchant.PayingDaysMarginInitial);
                }
                else if (merchant.PayingDaysMargin > 0)
                {
                    ret = xTransDate.AddDays(merchant.PayingDaysMargin);
                }
                else
                {
                    int daysToAdd;
                    var payDates = Merchants.Merchant.MerchantPayDate.Parse(merchant.PayingDates1, merchant.PayingDates2, merchant.PayingDates3);
                    foreach (Merchants.Merchant.MerchantPayDate payDate in payDates)
                    {
                        daysToAdd = IsDayInRange(payDate, xTransDate.Day);
                        if (daysToAdd > -1)
                        {
                            ret = new DateTime(xTransDate.Year, xTransDate.Month, daysToAdd).AddMonths(1);
                            break;
                        }
                    }
                }
                return ret;
            }
            else
            {
                return null;
            }
        }
    }
}





