﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Transactions
{
    public partial class Transaction
    {
        public class ChargebackHistory : BaseDataObject
        {
            public ChargebackHistory() { }

            private ChargebackHistory(Dal.Netpay.GetChargebackHistoryResult data)
            {
                ID = data.ID.GetValueOrDefault();
                TypeID = (CommonTypes.TransactionHistoryType)data.TypeID.GetValueOrDefault();
                RecordDate = data.InsertDate.GetValueOrDefault();
                ReasonCode = data.ReasonCode;
                Title = data.Title;
                Description = data.Description;
                RequiredMedia = data.RequiredMedia;
                IsPendingChargeback = data.IsPendingChargeback.GetValueOrDefault();
                RefundInfo = data.RefundInfo;
            }

            public int ID { get; set; }
            public CommonTypes.TransactionHistoryType TypeID { get; set; }
            public DateTime RecordDate { get; set; }
            public int? ReasonCode { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
            public string RequiredMedia { get; set; }
            public string RefundInfo { get; set; }
            public bool IsPendingChargeback { get; set; }

            public static List<ChargebackHistory> LoadForTransaction(int transID)
            {
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant, UserRole.Partner }, SecuredObject, PermissionValue.Read);

                return (from ch in DataContext.Reader.GetChargebackHistory(transID) select new ChargebackHistory(ch)).ToList();
            }

        }

        public static Dictionary<int, string> GetChargebackReasons()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Read);

            var results = from cr in DataContext.Reader.tblChargebackReasons select cr;
            return results.ToDictionary(kvp => kvp.ReasonCode, kvp => kvp.Title);
        }

        public static void CreateChargeback(int transactionId, int reasonCode, string comment)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Add);
                        
            DataContext.Writer.CreateCHB(transactionId, DateTime.Now, comment, reasonCode);
        }


        //Add the update of the transaction item.
        public static void RemoveChargeback(int transactionId)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
            {
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Edit);
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Delete);
            }
                        
            var trans = (from t in DataContext.Reader.tblCompanyTransPasses where t.ID == transactionId select t).SingleOrDefault();
            if (trans == null)
                return;
            
            bool isClearChargeback = false;
            if (trans.DeniedStatus == (int)DeniedStatus.UnsettledInVerification && trans.PrimaryPayedID <= 0)
            {
                trans.DeniedStatus = (int)DeniedStatus.ValidTransaction;
                trans.netpayFee_chbCharge = 0;
                trans.netpayFee_ClrfCharge = 0;
                isClearChargeback = true;
                DataContext.Writer.tblCompanyTransPasses.Update(trans, trans.ID > 0);
            }
            //Till here [V]

            else if ((trans.DeniedStatus == (int)DeniedStatus.UnsettledBeenSettledAndValid || trans.DeniedStatus == (int)DeniedStatus.SetFoundValidRefunded) && trans.PrimaryPayedID <= 0)
            {
                trans.DeniedStatus = (int)DeniedStatus.ValidTransaction;
                trans.netpayFee_chbCharge = 0;
                trans.netpayFee_ClrfCharge = 0;
                isClearChargeback = true;
                DataContext.Writer.tblCompanyTransPasses.Update(trans, trans.ID > 0);
            }
            //Till here [V]
            else if (trans.DeniedStatus == (int)DeniedStatus.DuplicateValid && trans.PrimaryPayedID <= 0)
            {
                var chbCount = (from t in DataContext.Reader.tblCompanyTransPasses where t.OriginalTransID == trans.OriginalTransID && t.DeniedStatus > 0 select t).Count();
                if (chbCount == 1)
                {
                    var originalTrans = (from t in DataContext.Reader.tblCompanyTransPasses where t.ID == trans.OriginalTransID select t).SingleOrDefault();
                    if (originalTrans != null)
                    {
                        originalTrans.DeniedStatus = (int)DeniedStatus.ValidTransaction;
                        DataContext.Writer.tblCompanyTransPasses.Update(originalTrans, originalTrans.ID > 0);
                    }
                }
                DataContext.Writer.tblCompanyTransPasses.EnsureAttached(trans);
                DataContext.Writer.tblCompanyTransPasses.DeleteOnSubmit(trans);
                isClearChargeback = true;
            }
            //Till here [V]
            else if (trans.DeniedStatus == (int)DeniedStatus.DuplicateDeducted && trans.PrimaryPayedID <= 0)
            {
                var chbCount = (from t in DataContext.Reader.tblCompanyTransPasses where t.OriginalTransID == trans.OriginalTransID && t.DeniedStatus > 0 select t).Count();
                if (chbCount == 1)
                {
                    var originalTrans = (from t in DataContext.Reader.tblCompanyTransPasses where t.ID == trans.OriginalTransID select t).SingleOrDefault();
                    if (originalTrans != null)
                    {
                        originalTrans.DeniedStatus = (int)DeniedStatus.ValidTransaction;
                        DataContext.Writer.tblCompanyTransPasses.Update(originalTrans, originalTrans.ID > 0);
                    }
                }
                DataContext.Writer.tblCompanyTransPasses.EnsureAttached(trans);
                DataContext.Writer.tblCompanyTransPasses.DeleteOnSubmit(trans);
                isClearChargeback = true;
            }
            //Till here [V]

            if (isClearChargeback)
            {
                var log = new Netpay.Dal.Netpay.tblLogTransPass();
                log.ltp_ActionType = 1;
                log.ltp_Transaction = trans.ID;
                log.ltp_User = Infrastructure.Security.AdminUser.Current.ID;
                log.ltp_Username = Login.Current.UserName;
                log.ltp_InsertDate = DateTime.Now;
                if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Request != null)
                {
                    string ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                    log.ltp_IP = ip;
                }
                DataContext.Writer.tblLogTransPasses.InsertOnSubmit(log); // This line works

                //Delete !!ALL!! types of histories include retrieval request.
                var histories = (from h in DataContext.Writer.TransHistories
                                 where ( (h.TransHistoryType_id == (int)Netpay.CommonTypes.TransactionHistoryType.Chargeback || h.TransHistoryType_id == (int)Netpay.CommonTypes.TransactionHistoryType.RetrievalRequest)
                                 && h.TransPass_id == transactionId)
                                 select h);
                                
                DataContext.Writer.TransHistories.DeleteAllOnSubmit(histories);
                DataContext.Writer.SubmitChanges();
            }
        }

        public static void CreateRetrievalRequest(int transactionId, int reasonCode, string comment)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Add);
            var result = DataContext.Writer.RequestPhotocopyCHB(transactionId, DateTime.Now, comment, reasonCode);
            if ((int)result.ReturnValue >= 0)
            {
                Transaction trans = GetTransaction(null, transactionId, TransactionStatus.Captured);
                //History.Create(trans.ID, trans.Status, Netpay.CommonTypes.TransactionHistoryType.RetrievalRequest, trans.MerchantID.Value, "added by admin");
            }
        }
    }
}
