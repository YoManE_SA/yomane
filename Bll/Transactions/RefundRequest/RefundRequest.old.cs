﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Bll.Accounts;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Transactions
{
    public class RefundRequest : BaseDataObject
    {
        public class SearchItem
        {
            public Dal.Netpay.tblRefundAsk Refund { get; set; }

            public Dal.Netpay.tblCompanyTransPass Transaction { get; set; }

            public Dal.Netpay.tblDebitTerminal Terminal { get; set; }
        }

        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(RefundRequests.Module.Current, SecuredObjectName); } }

        public enum RefundType { Normal, Partial, Fraud }
        public enum RequestStatus
        {
            Pending = 1,
            InProgress = 2,
            Created = 50,
            Processed = 51,
            Batched = 52,
            AdminCancel = 101,
            SourceCancel = 102,
        }
        /*oldCode*/
        private enum RefundRequestStatus { Pending = 0, Processing = 1, CreatedAccepted = 2, Canceled = 3, ProcessedAccepted = 4, BatchFile = 5, CanceledByMerchant = 6 }

        private static RequestStatus StatusOldToNew(RefundRequestStatus value)
        {
            switch (value)
            {
                case RefundRequestStatus.Pending: return RequestStatus.Pending;
                case RefundRequestStatus.Processing: return RequestStatus.InProgress;
                case RefundRequestStatus.CreatedAccepted: return RequestStatus.Created;
                case RefundRequestStatus.Canceled: return RequestStatus.AdminCancel;
                case RefundRequestStatus.ProcessedAccepted: return RequestStatus.Processed;
                case RefundRequestStatus.BatchFile: return RequestStatus.Batched;
                case RefundRequestStatus.CanceledByMerchant: return RequestStatus.SourceCancel;
                default: return RequestStatus.Pending;
            }
        }

        private static RefundRequestStatus StatusNewToOld(RequestStatus value)
        {
            switch (value)
            {
                case RequestStatus.Pending: return RefundRequestStatus.Pending;
                case RequestStatus.InProgress: return RefundRequestStatus.Processing;
                case RequestStatus.Created: return RefundRequestStatus.CreatedAccepted;
                case RequestStatus.AdminCancel: return RefundRequestStatus.Canceled;
                case RequestStatus.Processed: return RefundRequestStatus.ProcessedAccepted;
                case RequestStatus.Batched: return RefundRequestStatus.BatchFile;
                case RequestStatus.SourceCancel: return RefundRequestStatus.CanceledByMerchant;
                default: return RefundRequestStatus.Pending;
            }
        }

        public static Dictionary<RequestStatus, System.Drawing.Color> StatusColor = new Dictionary<RequestStatus, System.Drawing.Color> {
            { RequestStatus.Pending, System.Drawing.ColorTranslator.FromHtml("#6699cc") },
            { RequestStatus.InProgress, System.Drawing.ColorTranslator.FromHtml("#9e6cff") },
            { RequestStatus.Created, System.Drawing.ColorTranslator.FromHtml("#ff8040") },
            { RequestStatus.Processed, System.Drawing.ColorTranslator.FromHtml("#ff8040") },
            { RequestStatus.Batched, System.Drawing.ColorTranslator.FromHtml("#ffeb65") },
            { RequestStatus.AdminCancel, System.Drawing.ColorTranslator.FromHtml("#66cc66") },
            { RequestStatus.SourceCancel, System.Drawing.ColorTranslator.FromHtml("#ff6666") }
        };

        public class SearchFilters
        {
            public Range<int?> ID;
            public Range<DateTime?> Date;
            public Range<int?> TransactionID;
            public Range<decimal?> RequestAmount;
            public Range<decimal?> Amount;
            public string CurrencyISOCode;
            public List<int> MerchantIDs;
            public byte? DebitCompanyID;
            public List<RequestStatus> Status;
            public bool AllCompleted;
            public string terminalNumber;

        }
        private Dal.Netpay.tblRefundAsk _entity;
        public int ID { get { return _entity.id; } }
        public int MerchantID { get { return _entity.companyID; } }

        public int AccountID
        {
            get
            {
                return Bll.Accounts.Account.GetAccountIdByMerchantId(MerchantID).Value;
            }
        }

        public int TransactionID { get { return _entity.transID; } }
        public System.DateTime Date { get { return _entity.RefundAskDate; } }
        public decimal RequestAmount { get { return _entity.RefundAskAmount; } }
        public decimal? RefundAmount { get { return _entity.RefundAskAmount; } set { _entity.RefundAskAmount = value.GetValueOrDefault(); } }

        public string ConfirmationNum { get { return _entity.RefundAskConfirmationNum; } }

        public string CurrencyIso { get { return Currency.Get(_entity.RefundAskCurrency).IsoCode; } }
        public string Comment { get { return _entity.RefundAskComment; } set { _entity.RefundAskComment = value; } }
        public RequestStatus Status { get { return StatusOldToNew((RefundRequestStatus)_entity.RefundAskStatus); } }
        public bool IsPendingOrInProgress
        {
            get
            {
                return (Status == RequestStatus.InProgress || Status == RequestStatus.Pending);
            }
        }
        public byte Flag { get { return (byte)_entity.RefundFlag; } set { _entity.RefundFlag = (byte)value; } }

        public string FlagImageUrl
        {
            get
            {
                switch (Flag)
                {
                    case 0:
                        return "~/Images/flag_clear.gif";
                    case 1:
                        return "~/Images/flag_green.gif";
                    case 2:
                        return "~/Images/flag_red.gif";
                    default:
                        return "~/Images/flag_clear.gif";
                }
            }
        }

        public RefundType Type { get { return (RefundType)_entity.RefundType; } set { _entity.RefundType = (byte)value; } }

        //public string ConfirmationNum { get { return _entity.con; } set { _entity.RefundAskConfirmationNum = value; } }
        //public string StatusLog { get { return _entity.RefundAskStatusHistory; } }

        public bool IsEnableProcess
        {
            get
            {
                if ((Status == RequestStatus.InProgress || Status == RequestStatus.Pending) && (!Transaction.IsPendingChargebackForRefundPage))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool IsEnableCreate
        {
            get
            {
                if (IsEnableProcess || Status == RequestStatus.Batched)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool IsEnableCancel
        {
            get
            {
                if (Status == RequestStatus.Pending || Status == RequestStatus.InProgress || Status == RequestStatus.Batched)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool? IsAllowChangeAmount
        {
            get
            {
                if (Transaction != null && Transaction.DebitCompany != null)
                {
                    return ((Status == RequestStatus.Pending || Status == RequestStatus.InProgress) && (Transaction.DebitCompany.IsAllowPartialRefund));
                }
                else
                {
                    return null;
                }
            }
        }

        private Transaction _transaction;
        public Transaction Transaction
        {
            get
            {
                if (_transaction == null) return _transaction = Transaction.GetTransaction(MerchantID, TransactionID, TransactionStatus.Captured);
                return _transaction;
            }
        }


        private RefundRequest(Dal.Netpay.tblRefundAsk entity)
        {
            _entity = entity;
        }

        public static RefundRequest Load(int id)
        {
            return Search(new SearchFilters() { ID = new Range<int?>(id) }, null).FirstOrDefault();
        }

        public static List<RefundRequest> RequestsForTrasaction(int transactionID)
        {
            return Search(new SearchFilters() { TransactionID = new Range<int?>(transactionID) }, null);
        }

        public static decimal TotalRefundAmountForTransaction(int transactionID)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);


            decimal totalRefundAmount = (from ra in DataContext.Reader.tblRefundAsks
                                         where ra.transID == transactionID
                                         && ra.RefundAskStatus != 3
                                         && ra.RefundAskStatus != 6
                                         select ra.RefundAskAmount).ToList().Sum();

            return totalRefundAmount;
        }

        public static RequestRefundResult Create(int transactionID, TransactionStatus transactionStatus, decimal amount, string comment)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Add);

            int? merchantId = null;
            AccountFilter.Current.Validate(Accounts.AccountType.Merchant, ref merchantId, false);
            //if (context.IsUserOfType(new UserRole[] { UserRole.MerchantPrimary, UserRole.MerchantLimited }, false)) merchantId = context.User.ID;
            var transaction = Transaction.GetTransaction(merchantId, transactionID, transactionStatus);
            if (transaction == null) return RequestRefundResult.TransactionNotFound;
            if (!transaction.Merchant.ProcessSettings.IsAskRefund) return RequestRefundResult.NoPermission;
            if (transaction.InsertDate < DateTime.Now.AddMonths(-12)) return RequestRefundResult.TransactionTooOld;

            var refunds = Search(new SearchFilters() { TransactionID = new Range<int?>(transactionID) }, null);
            decimal totalRefunded = refunds.Select(rr => (rr.Status == RequestStatus.SourceCancel || rr.Status == RequestStatus.AdminCancel ? 0 : rr.RefundAmount.GetValueOrDefault(rr.RequestAmount))).Sum();
            decimal availableForRefund = transaction.Amount - totalRefunded;

            if (availableForRefund <= 0) return RequestRefundResult.TransactionFullyRefunded;
            if (amount == 0) return RequestRefundResult.NoPermission;
            if (availableForRefund < amount) return RequestRefundResult.AmountTooLarge;

            var entity = new Dal.Netpay.tblRefundAsk();
            entity.companyID = transaction.MerchantID.GetValueOrDefault();
            entity.transID = transaction.ID;

            //Check this line
            entity.RefundType = 0;
            //


            entity.RefundAskStatus = (int)StatusNewToOld(RequestStatus.Pending);
            entity.RefundAskAmount = amount;
            entity.RefundAskComment = comment;
            entity.RefundAskCurrency = transaction.CurrencyID;
            entity.RefundAskDate = DateTime.Now;
            new RefundRequest(entity).Save();
            return RequestRefundResult.Success;
        }

        public static RequestRefundResult CreateForAdmin(int transactionID, TransactionStatus transactionStatus, decimal amount, string comment, byte refundType)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            int? merchantId = null;

            if (!(ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add)))
            {
                AccountFilter.Current.Validate(Accounts.AccountType.Merchant, ref merchantId, false);
            }
            //if (context.IsUserOfType(new UserRole[] { UserRole.MerchantPrimary, UserRole.MerchantLimited }, false)) merchantId = context.User.ID;
            var transaction = Transaction.GetTransaction(merchantId, transactionID, transactionStatus);
            if (transaction == null) return RequestRefundResult.TransactionNotFound;
            if (!transaction.Merchant.ProcessSettings.IsAskRefund) return RequestRefundResult.NoPermission;
            if (transaction.InsertDate < DateTime.Now.AddMonths(-12)) return RequestRefundResult.TransactionTooOld;

            var refunds = Search(new SearchFilters() { TransactionID = new Range<int?>(transactionID) }, null);
            decimal totalRefunded = refunds.Select(rr => (rr.Status == RequestStatus.SourceCancel || rr.Status == RequestStatus.AdminCancel ? 0 : rr.RefundAmount.GetValueOrDefault(rr.RequestAmount))).Sum();
            decimal availableForRefund = transaction.Amount - totalRefunded;

            if (availableForRefund <= 0) return RequestRefundResult.TransactionFullyRefunded;
            if (amount == 0) return RequestRefundResult.NoPermission;
            if (availableForRefund < amount) return RequestRefundResult.AmountTooLarge;

            var entity = new Dal.Netpay.tblRefundAsk();
            entity.companyID = transaction.MerchantID.GetValueOrDefault();
            entity.transID = transaction.ID;

            //Check this line 
            entity.RefundType = refundType;
            //

            entity.RefundAskStatus = (int)StatusNewToOld(RequestStatus.Pending);
            entity.RefundAskAmount = amount;
            entity.RefundAskComment = comment;
            entity.RefundAskCurrency = transaction.CurrencyID;
            entity.RefundAskDate = DateTime.Now;
            new RefundRequest(entity).Save();
            return RequestRefundResult.Success;
        }

        public bool CancelRequest()
        {
            //Context.IsUserOfType(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
            if (Status != RequestStatus.Pending) return false;
            _entity.RefundAskStatus = (int)StatusNewToOld(RequestStatus.SourceCancel);
            Save();
            return true;
        }

        public void UpdateCommentAndConfirmationNumber(string comment, string conf)
        {
            _entity.RefundAskComment = comment;
            _entity.RefundAskConfirmationNum = conf;
            Save();
        }

        public void UpdateFlag()
        {
            _entity.RefundFlag = Convert.ToByte((Flag + 1) % 3);
            Save();
        }

        public bool CancelRequestByAdmin()
        {
            if (Status != RequestStatus.Pending) return false;
            _entity.RefundAskStatus = (int)StatusNewToOld(RequestStatus.AdminCancel);
            AddLog("Refund request cancelled by admin.");
            Save();
            return true;
        }


        private void Save()
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Edit);

            if (_entity.RefundAskConfirmationNum == null) _entity.RefundAskConfirmationNum = "";
            if (_entity.RefundAskStatusHistory == null) _entity.RefundAskStatusHistory = "";
            DataContext.Writer.tblRefundAsks.Update(_entity, (_entity.id != 0));
            DataContext.Writer.SubmitChanges();
        }

        public static TransactionRefundAbility GetRefundAbility(Transaction trans)
        {
            if (trans == null) return TransactionRefundAbility.None; // no transaction
            if (trans.Status != TransactionStatus.Captured) return TransactionRefundAbility.None; // not pass
            if (trans.CreditType == 0) return TransactionRefundAbility.None; // not debit
            if (trans.DebitCompanyID == null) return TransactionRefundAbility.None; // not bank transaction
            if (trans.IsChargeback != null && trans.IsChargeback.Value) return TransactionRefundAbility.None; // charged back

            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);
            var retrievals = Transaction.ChargebackHistory.LoadForTransaction(trans.ID);
            foreach (var retrieval in retrievals)
            {
                if ((retrieval.IsPendingChargeback) || (retrieval.TypeID == CommonTypes.TransactionHistoryType.Chargeback)) return TransactionRefundAbility.None;
            }
            var bank = trans.DebitCompany; //user.Domain.Cache.GetDebitCompany(debitCompanyID);
            if (bank == null) return TransactionRefundAbility.None; // bank not found
            if (!bank.IsAllowRefund) return TransactionRefundAbility.None; // refund not allowed
            return bank.IsAllowPartialRefund ? TransactionRefundAbility.FullOrPartial : TransactionRefundAbility.FullOnly;
        }

        private static IQueryable<SearchItem> SearchExpression(SearchFilters filters)
        {
            var expression = from r in DataContext.Reader.tblRefundAsks.Where(AccountFilter.Current.QueryValidate(Accounts.AccountType.Merchant, (Dal.Netpay.tblRefundAsk r) => r.companyID))
                             join t in DataContext.Reader.tblCompanyTransPasses on r.transID equals t.ID
                             join terminal in DataContext.Reader.tblDebitTerminals on t.TerminalNumber equals terminal.terminalNumber into transterminal
                             from transAndTerminals in transterminal.DefaultIfEmpty()
                             orderby r.RefundAskDate descending
                             select new SearchItem() { Refund = r, Transaction = t, Terminal = transAndTerminals };
            if (filters != null)
            {
                if (filters.ID.From.HasValue) expression = expression.Where(t => t.Refund.id >= filters.ID.From);
                if (filters.ID.To.HasValue) expression = expression.Where(t => t.Refund.id <= filters.ID.To);
                if (filters.MerchantIDs != null && filters.MerchantIDs.Count > 0) expression = expression.Where(t => filters.MerchantIDs.Contains(t.Refund.companyID));
                //if (filters.isChargeback == null)
                if (filters.Date.From.HasValue) expression = expression.Where(t => t.Refund.RefundAskDate >= filters.Date.From.Value);
                if (filters.Date.To.HasValue) expression = expression.Where(t => t.Refund.RefundAskDate <= filters.Date.To.Value.AlignToEnd());

                if (filters.TransactionID.From.HasValue) expression = expression.Where(t => t.Refund.transID >= filters.TransactionID.From);
                if (filters.TransactionID.To.HasValue) expression = expression.Where(t => t.Refund.transID <= filters.TransactionID.To);
                if (filters.CurrencyISOCode != null) expression = expression.Where(t => t.Refund.RefundAskCurrency == Currency.Get(filters.CurrencyISOCode).ID);
                if (filters.RequestAmount.From != null) expression = expression.Where(t => t.Refund.RefundAskAmount >= filters.RequestAmount.From);
                if (filters.RequestAmount.To != null) expression = expression.Where(t => t.Refund.RefundAskAmount <= filters.RequestAmount.To);
                if (filters.Amount.From != null) expression = expression.Where(t => t.Refund.RefundAskAmount >= filters.Amount.From);
                if (filters.Amount.To != null) expression = expression.Where(t => t.Refund.RefundAskAmount <= filters.Amount.To);
                if (filters.DebitCompanyID.HasValue) expression = expression.Where(t => t.Terminal.DebitCompany == filters.DebitCompanyID.Value);
                if (!string.IsNullOrEmpty(filters.terminalNumber)) expression = expression.Where(t => t.Terminal.terminalNumber == filters.terminalNumber);

                //if (filters.paymentMethodID != null) expression = expression.Where(t => t.Transaction.PaymentMethod == filters.paymentMethodID.Value);
                //if (filters.creditType != null) expression = expression.Where(t => t.Transaction.CreditType == filters.creditType.Value);
                //if (filters.orderID != null) expression = expression.Where(t => t.Transaction.OrderNumber == filters.orderID);
                //if (filters.replyCode != null) expression = expression.Where(t => t.Transaction.replyCode == filters.replyCode);

                if (filters.Status != null && filters.Status.Count > 0)
                {
                    var nstatus = filters.Status.Select(b => StatusNewToOld(b)).ToList();
                    expression = expression.Where(t => nstatus.Contains((RefundRequestStatus)t.Refund.RefundAskStatus));
                }
                if (filters.AllCompleted)
                    expression = expression.Where(t => (t.Refund.RefundAskStatus == (byte)RefundRequestStatus.Processing) || (t.Refund.RefundAskStatus == (byte)RefundRequestStatus.ProcessedAccepted) || (t.Refund.RefundAskStatus == (byte)RefundRequestStatus.CreatedAccepted));
            }

            return expression;
        }

        public static List<RefundRequest> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);

            var expression = SearchExpression(filters);

            if (sortAndPage != null)
                sortAndPage.DataFunction = (pi) => Search(filters, sortAndPage);
            return expression.ApplySortAndPage(sortAndPage).Select(t => new RefundRequest(t.Refund) { _transaction = new Transaction(t.Transaction) }).ToList();
        }

        public static int Count(SearchFilters filters)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);

            var expression = SearchExpression(filters);

            return expression.Count(i => i.Refund.id > 0);
        }


        public static SearchItem Max()
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);

            var expression = SearchExpression(null);

            // Assuming SearchExpression sorts by RefundAskDate DESC (as it does)
            return expression.FirstOrDefault();
        }

        public void AddLog(string text)
        {
            StatusChange.Create(StatusChange.StatusItemType.RefundReq, ID, (StatusChange.ActionStatus)Status, text);
        }

        public int Process(decimal amount)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Add);

            var requestParams = Transaction.GetProcessParamters(true);
            requestParams["RefTransID"] = TransactionID.ToString();
            requestParams["requestSource"] = "6";
            requestParams["isLocalTransaction"] = "true";
            requestParams["Comment"] = Comment;

            //Follow the logic at Refund_ask_transCharge.asp file
            requestParams["Amount"] = amount.ToString("0.00");
            requestParams["Payments"] = "1";


            _entity.RefundAskAmount = amount;
            _entity.RefundAskStatus = (byte)StatusNewToOld(RequestStatus.InProgress);
            var responseParams = Transaction.ProcessTransaction(Transaction.RefPaymentMethod.Group, requestParams);
            var retId = responseParams["TransID"].ToNullableInt();
            if (responseParams["Reply"] == "000")
            {
                _entity.RefundAskStatus = (byte)StatusNewToOld(RequestStatus.Processed);
                AddLog(string.Format("Approved TransactionID: {0}", retId));
            }
            else if (responseParams["Reply"] == "001")
            {
                _entity.RefundAskStatus = (byte)StatusNewToOld(RequestStatus.Pending);
                AddLog(string.Format("Pending TransactionID: {0}", retId));
            }
            else
            {
                AddLog(string.Format("Declined TransactionID: {0}", retId));
            }
            Save();
            return retId.GetValueOrDefault();
        }


        public int Create(decimal amount, string ip = "")
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Add);

            _entity.RefundAskAmount = amount;
            var retId = DataContext.Writer.CreateRefund(TransactionID, ip, amount, Comment, (byte)Type).SingleOrDefault().Column1;
            _entity.RefundAskStatus = (int)StatusNewToOld(RequestStatus.Created);
            AddLog(string.Format("Created TransactionID: {0}", retId));
            Save();
            return retId.GetValueOrDefault();
        }
    }
}
