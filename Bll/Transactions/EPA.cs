﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Transactions
{
	public class EPA : BaseDataObject
	{
		private Dal.Netpay.tblLogImportEPA _entity;
		private EPA(Dal.Netpay.tblLogImportEPA entity) { _entity = entity; }

		public int ID { get { return _entity.ID; } }
		public int TransactionID { get { return _entity.TransID; } }
		public byte Installment { get { return _entity.Installment; } }
		public bool IsPaid { get { return _entity.IsPaid; } }
		public bool IsRefunded { get { return _entity.IsRefunded; } }
		public DateTime PaidInsertDate { get { return _entity.PaidInsertDate; } }
		public DateTime RefundedInsertDate { get { return _entity.RefundedInsertDate; } }

		public static List<EPA> LoadForTransaction(int id)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, TransactionBasicInfo.SecuredObject, PermissionValue.Read);

            return (from e in DataContext.Reader.tblLogImportEPAs where e.TransID == id orderby e.Installment select new EPA(e)).ToList();
		}

	}
}
