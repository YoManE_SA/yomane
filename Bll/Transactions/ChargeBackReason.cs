﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Bll.Transactions
{
    public class ChargeBackReason : BaseDataObject
    {
        private Dal.Netpay.tblChargebackReason _entity;

        public string Brand { get { return _entity.Brand; } }

        public int ReasonCode { get { return _entity.ReasonCode; } }

        public bool IsPendingChargeBack { get { return _entity.IsPendingChargeback; } }

        public string Text { get { return Brand + " " + ReasonCode; } }

        private ChargeBackReason(Dal.Netpay.tblChargebackReason entity)
        {
            _entity = entity;
        }

        public static List<ChargeBackReason> Cache
        {
            get
            {
                return Domain.Current.GetCachData("ChargeBackReason", () =>
               {
                   return new Domain.CachData((from cbr in DataContext.Reader.tblChargebackReasons select new ChargeBackReason(cbr)).ToList(), DateTime.Now.AddHours(6));
               }) as List<ChargeBackReason>;
            }
        }
    }
}
