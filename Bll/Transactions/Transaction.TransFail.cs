﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;


namespace Netpay.Bll.Transactions
{
    public partial class Transaction
    {
        public Transaction(Netpay.Dal.Netpay.tblCompanyTransFail entity)
            : this(TransactionStatus.Declined)
        {
            if (entity != null) LoadEntity(entity);
        }

        private void LoadEntity(Netpay.Dal.Netpay.tblCompanyTransFail entity)
        {
            //
            FraudDetectionLogId = entity.FraudDetectionLog_id;
            //

            ID = entity.ID;
            Amount = entity.Amount;
            CurrencyID = entity.Currency.Value;
            Installments = entity.Payments;
            CreditType = (Infrastructure.CreditType)entity.CreditType;
            SystemText = entity.SystemText;

            DebitCompanyID = entity.DebitCompanyID;
            MerchantID = entity.CompanyID;
            CustomerID = entity.CustomerID;
            PayerDataID = entity.TransPayerInfo_id;
            PaymentDataID = entity.TransPaymentMethod_id;

            PaymentMethodID = (CommonTypes.PaymentMethodEnum)entity.PaymentMethod.GetValueOrDefault();
            PaymentMethodReferenceID = entity.PaymentMethodID;
            PaymentMethodDisplay = entity.PaymentMethodDisplay;
            IsTest = entity.isTestOnly;
            TransactionSource = (Infrastructure.TransactionSource?)entity.TransSource_id;
            InsertDate = entity.InsertDate;
            TransType = (TransactionType)entity.TransactionTypeID;
            //TransType = (TransactionType)entity.TransType;
            TransactionTypeText = ((Netpay.Infrastructure.FailedTransactionType)entity.TransType).ToString();
            DeniedStatusText = ((Netpay.Infrastructure.DeniedStatus)DeniedStatusID).ToString();
            OrderNumber = entity.OrderNumber;
            Comment = entity.Comment;
            IP = entity.IPAddress;
            Installments = entity.Payments;
            TerminalNumber = entity.TerminalNumber;
            ReferringUrl = entity.referringUrl;
            PayerIDUsed = entity.payerIdUsed;
            DebitReferenceCode = entity.DebitReferenceCode;
            DebitReferenceNum = entity.DebitReferenceNum;
            IPCountry = entity.IPCountry;
            PrimaryPaymentID = entity.PayID.GetValueOrDefault();
            ProductId = entity.MerchantProduct_id;
            // fees
            TransactionFee = entity.netpayFee_transactionCharge;

			ReplyDescriptionText = entity.DebitDeclineReason;
			ReplyCode = entity.replyCode;

            IsCardPresent = entity.IsCardPresent;
        }

        private static IQueryable<Dal.Netpay.tblCompanyTransFail> SearchExpressionTransFail(LoadOptions loadOptions, SearchFilters filters)
        {
            var expression = (IQueryable<Netpay.Dal.Netpay.tblCompanyTransFail>)from t in DataContext.Reader.tblCompanyTransFails orderby t.InsertDate descending select t;

            
            // add filters
            if (Login.Current.Role == UserRole.Partner)
            {
                if (filters.merchantIDs == null) filters.merchantIDs = new List<int>();
                if (filters.customersIDs == null) filters.customersIDs = new List<int>();
                expression = expression.Where(t => filters.merchantIDs.Contains(t.CompanyID.Value) || filters.customersIDs.Contains(t.CustomerID));
            }
            else
            {
                if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
                    expression = expression.Where(t => filters.merchantIDs.Contains(t.CompanyID.Value));
                if (filters.customersIDs != null && filters.customersIDs.Count > 0)
                    expression = expression.Where(t => filters.customersIDs.Contains(t.CustomerID));
            }

            //Date
            if (filters.date.From != null)
                expression = expression.Where(t => t.InsertDate >= filters.date.From.Value.MinTime());
            if (filters.date.To != null)
                expression = expression.Where(t => t.InsertDate <= filters.date.To.Value.MaxTime());

            //Specific Transaction ID
            if (filters.specificTransactionID.HasValue)
            {
                expression = expression.Where(t => t.ID == filters.specificTransactionID.Value);
            }

            //Transaction ID Range
            if (filters.ID.From != null)
                expression = expression.Where(t => t.ID >= filters.ID.From.Value);
            if (filters.ID.To != null)
                expression = expression.Where(t => t.ID <= filters.ID.To.Value);
            //Terminal Number
            if (!string.IsNullOrWhiteSpace(filters.debitTerminalNumber))
                expression = expression.Where(t => t.TerminalNumber == filters.debitTerminalNumber);
            //Debit Company
            if (filters.debitCompanyID != null)
                expression = expression.Where(t => t.DebitCompanyID == filters.debitCompanyID.Value);
            //Payment Method
            if (filters.paymentMethodID != null)
                expression = expression.Where(t => t.PaymentMethod == filters.paymentMethodID.Value);
            //Credit Types
            if (filters.creditTypes != null && filters.creditTypes.Count() > 0)
            {
                //If the search include "Regular" type , return "Regular" and 
                //also values that are different than 0-"Refund" and 8-"Installments"
                if (filters.creditTypes.Contains(1))
                {
                    expression = expression.Where(t => (filters.creditTypes.Contains(t.CreditType)) || (t.CreditType != 0 && t.CreditType != 8));
                }
                else
                {
                    expression = expression.Where(t => (filters.creditTypes.Contains(t.CreditType)));
                }
            }
            //Order ID
            if (filters.orderID != null)
                expression = expression.Where(t => t.OrderNumber.StartsWith(filters.orderID));
            //Currency ID
            if (filters.currencyID != null)
                expression = expression.Where(t => t.Currency == filters.currencyID);
            //Amount
            if (filters.amount.From != null)
                expression = expression.Where(t => t.Amount >= filters.amount.From);
            if (filters.amount.To != null)
                expression = expression.Where(t => t.Amount <= filters.amount.To);
            //Reply Code
            if (filters.replyCode != null && filters.replyCode != "")
                expression = expression.Where(t => t.replyCode == filters.replyCode);
            //Payment Methods id's
            if (filters.paymentMethodsIds != null)
                expression = expression.Where(t => filters.paymentMethodsIds.Contains(t.PaymentMethod.Value));
            //Device ID
            if (filters.deviceId != null)
                expression = expression.Where(t => t.MobileDevice_id == filters.deviceId);

            //Check if  "filters.PaymentStatusList" belongs here.

            //Check if filters.PaymenStatusBankDropDown belongs here.

            //Debit ref' code
            if (!string.IsNullOrEmpty(filters.debitReferenceCode))
            {
                expression = expression.Where(t => t.DebitReferenceCode == filters.debitReferenceCode.Trim());
            }

            //Debit ref' num.
            if (!string.IsNullOrEmpty(filters.debitReferenceNumber))
            {
                expression = expression.Where(t => t.DebitReferenceNum == filters.debitReferenceNumber.Trim());
            }
            //Approval Number - not exist in tblcompanytranspass

            //UnsettledInstallments - not exist in tblcompanytranspass

            //Check if the chbReason filter is relevant here.

            //Denied Date -  not exist in tblcompanytranspass
            if (filters.isTest != null)
                expression = expression.Where(t => t.isTestOnly);

            //Denied Status - not exist in tblcompanytranspass

            //Original Transaction Id - not exist in tblcompanytranspass

            //Is Pending Chargeback - not exist in tblcompanytranspass

            //Batch ID - "AuthorizationBatchID" column -  not exist in tblcompanytranspass

            //Payer Filters
            if (filters.PayerFilters != null)
            {
                bool isEmpty;
                var sq = Payer.Search(DataContext.Reader, filters.PayerFilters, out isEmpty);
                if (!isEmpty) expression = expression.Where(t => sq.Contains(t.trans_TransPayerInfo));
            }

            //Payment filters 
            if (filters.PaymentFilters != null)
            {
                bool isEmpty;
                var sq = Payment.Search(DataContext.Reader, filters.PaymentFilters, out isEmpty);
                if (!isEmpty) expression = expression.Where(t => sq.Contains(t.trans_TransPaymentMethod));
            }

            //Declined Type			
            if (filters.declinedType != null)
            {
                if (filters.declinedType == 1)
                    expression = expression.Where(t => t.TransType == (int)FailedTransactionType.PreAuthorized);
                else
                    expression = expression.Where(t => t.TransType != (int)FailedTransactionType.PreAuthorized);
            }

            //Pay ID
            if (filters.PayID != null)
                expression = expression.Where(t => t.PayID == filters.PayID.Value);

            //History Types
            if (filters.HistoryTypes != null)
                expression = expression.Where(t => DataContext.Reader.TransHistories.Where(v => filters.HistoryTypes.Contains((CommonTypes.TransactionHistoryType)v.TransHistoryType_id)).Select(v => v.TransFail_id).Contains(t.ID));


            //--Filters Only For TransPassFail Table --//

            if (!string.IsNullOrEmpty(filters.DeclinedAutoRefundFilter))
            {
                if (filters.DeclinedAutoRefundFilter == "ARF_WAIT")
                {
                    expression = expression.Where(t =>
                     (from lca in DataContext.Reader.tblLogChargeAttempts
                      where (t.replyCode == "521"
                            && t.ID == lca.Lca_TransNum
                            && (lca.Lca_DateStart.HasValue)
                            && ((TimeSpan)(t.InsertDate - lca.Lca_DateStart.Value)).TotalHours <= 1
                            //&& lca.Lca_ResponseString == null || lca.Lca_ResponseString == "NP-INT"
                            )
                      select t.ID).ToList().Count() > 0);
                }

                else if (filters.DeclinedAutoRefundFilter == "ARF_DONE")
                {
                    expression = expression.Where(t =>
                     (from lca in DataContext.Reader.tblLogChargeAttempts
                      where (t.replyCode == "521"
                            && t.ID == lca.Lca_TransNum
                            && (lca.Lca_DateStart.HasValue)
                            && ((TimeSpan)(t.InsertDate - lca.Lca_DateStart.Value)).TotalHours <= 1
                            //&& lca.Lca_ResponseString.StartsWith("Refunded")
                            )
                      select t.ID).ToList().Count() > 0);
                }
                else if (filters.DeclinedAutoRefundFilter == "ARF_FAIL")
                {
                    expression = expression.Where(t =>
                     (from lca in DataContext.Reader.tblLogChargeAttempts
                      where (t.replyCode == "521"
                            && t.ID == lca.Lca_TransNum
                            && (lca.Lca_DateStart.HasValue)
                            && ((TimeSpan)(t.InsertDate - lca.Lca_DateStart.Value)).TotalHours <= 1
                            //&& lca.Lca_ResponseString.StartsWith("Try Refund")
                            )
                      select t.ID).ToList().Count() > 0);
                }

                //expressionWithCharge = expressionWithCharge.Where(x=>x.anyA)
            }

            if (filters.declinedType != null)
            {
                if (filters.declinedType == 1)
                    expression = expression.Where(t => t.TransType == (int)FailedTransactionType.PreAuthorized);

                else if (filters.declinedType == 0)
                    expression = expression.Where(t => t.TransType == (int)FailedTransactionType.Debit);
            }

            return expression;
        }

        private static List<Transaction> SearchTransDeclined(LoadOptions loadOptions, SearchFilters filters)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Read);

            var expression = SearchExpressionTransFail(loadOptions, filters);
                        
            List<Transaction> transactions = expression.ApplySortAndPage(loadOptions.sortAndPage).Select(t => new Transaction(t)).ToList<Transaction>();
            return transactions;
        }

        protected void SaveFail()
        {
            var entity = new Netpay.Dal.Netpay.tblCompanyTransFail();
            //
            entity.FraudDetectionLog_id = FraudDetectionLogId;
            //

            if (PaymentData != null && PaymentData.OldCreditCardID != null)
            {
                entity.PaymentMethod_id = 1;
                entity.CreditCardID = PaymentMethodReferenceID;
            }
            entity.replyCode = ReplyCode;
            entity.Amount = Amount;
            entity.Currency = CurrencyID;
            entity.Payments = Installments;
            entity.CreditType = (byte)CreditType;
            entity.DebitCompanyID = DebitCompanyID;
            entity.CompanyID = MerchantID.GetValueOrDefault();
            entity.CustomerID = CustomerID.GetValueOrDefault();
            entity.TransPayerInfo_id = PayerDataID;
            entity.TransPaymentMethod_id = PaymentDataID;
            entity.PaymentMethod = (short?)PaymentMethodID;
            entity.PaymentMethodID = PaymentMethodReferenceID;
            entity.PaymentMethodDisplay = PaymentMethodDisplay.EmptyIfNull();
            entity.isTestOnly = IsTest;
            entity.TransSource_id = (byte?)TransactionSource;
            entity.TransactionTypeID = (int)TransType;
            entity.PayID = PrimaryPaymentID;
            entity.InsertDate = InsertDate;
            //entity.DeniedStatus = DeniedStatusID;
            entity.OrderNumber = OrderNumber;
            entity.Comment = Comment;
            entity.IPAddress = IP;
            entity.TerminalNumber = TerminalNumber;
            entity.referringUrl = ReferringUrl.EmptyIfNull();
            entity.payerIdUsed = PayerIDUsed.EmptyIfNull();
            entity.DebitReferenceCode = DebitReferenceCode;
            entity.DebitReferenceNum = DebitReferenceNum;
            entity.IPCountry = IPCountry.EmptyIfNull();
            entity.PayforText = PayForText;
            entity.MerchantProduct_id = ProductId;
            // fees
            entity.netpayFee_transactionCharge = TransactionFee;
            entity.DebitFee = DebitFee;

            DataContext.Writer.tblCompanyTransFails.InsertOnSubmit(entity);
            DataContext.Writer.SubmitChanges();
            ID = entity.ID;
        }
    }
}
