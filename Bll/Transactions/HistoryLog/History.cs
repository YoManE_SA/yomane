﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Transactions
{
	public class History : BaseDataObject
	{
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(HistoryLog.Module.Current, SecuredObjectName); } }

        public class SearchFilters
		{
			public List<Netpay.CommonTypes.TransactionHistoryType> HistoryTypes;
			public Infrastructure.Range<DateTime?> Date;
			public Infrastructure.Range<decimal?> Amount;
            public Infrastructure.Range<int?> MerchantId;
            public List<int> MerchantIDs;

            //ID filter that represents the "TransHistory_id" coloum.
            public Infrastructure.Range<int?> ID;

            //TransID filter that represents TransPass_id/TransPreAuth_id/TransPending_id/TransFail_id (from TransHistory table).
            public TransactionStatus? TransactionStatus;
            public int? TransID;

            //Event Type Enum filter
            public Netpay.CommonTypes.TransactionHistoryType? EventType;
        }

        protected Netpay.Dal.Netpay.TransHistory _entity;

		public int ID { get { return _entity.TransHistory_id; }}
		public CommonTypes.TransactionHistoryType HistoryType { get { return (CommonTypes.TransactionHistoryType) _entity.TransHistoryType_id; } }
        //I enabled the five next properties.(ask eran if thats ok)
		public int Merchant_id { get { return _entity.Merchant_id; } }
		public int? TransPass_id { get { return _entity.TransPass_id; } }
		public int? TransPreAuth_id { get { return _entity.TransPreAuth_id; } }
		public int? TransPending_id { get { return _entity.TransPending_id; } }
		public int? TransFail_id { get { return _entity.TransFail_id; } }
        //Untill here the properties were commented.
		public DateTime InsertDate { get { return _entity.InsertDate; } }
		public bool? IsSucceeded { get { return _entity.IsSucceeded; } }
		public string Description { get { return _entity.Description; } }
		public int? ReferenceNumber { get { return _entity.ReferenceNumber; } }
		public string ReferenceUrl { get { return _entity.ReferenceUrl; } }

		private History(Dal.Netpay.TransHistory entity)  { _entity = entity; }

		public static void Create(Transaction transaction, CommonTypes.TransactionHistoryType historyType, string description)
		{
            Create(transaction.ID, transaction.Status, historyType, transaction.MerchantID.GetValueOrDefault(), description);
		}
		public static void Create(int transactionId, TransactionStatus transactionStatus, CommonTypes.TransactionHistoryType historyType, int merchantId, string description)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            var entity = new Dal.Netpay.TransHistory();
			switch (transactionStatus)
			{
				case Netpay.Infrastructure.TransactionStatus.Captured: entity.TransPass_id = transactionId; break;
				case Netpay.Infrastructure.TransactionStatus.Declined: entity.TransFail_id = transactionId; break;
				case Netpay.Infrastructure.TransactionStatus.Authorized: entity.TransPreAuth_id = transactionId; break;
				case Netpay.Infrastructure.TransactionStatus.Pending: entity.TransPending_id = transactionId; break;
				default: throw new Exception("Invalid transaction status");
			}
			entity.InsertDate = DateTime.Now;
			entity.TransHistoryType_id = (byte)historyType;
			entity.Merchant_id = merchantId;
			entity.Description = description.RemoveUnicodeChars(); // col is varchar, unicode removed
			DataContext.Writer.TransHistories.InsertOnSubmit(entity);
			DataContext.Writer.SubmitChanges();
		}

		public void Save()
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            ValidationException.RaiseIfNeeded(Validate());
			DataContext.Writer.TransHistories.Update(_entity, (_entity.TransHistory_id != 0));
			DataContext.Writer.SubmitChanges();
		}

		public void Delete()
		{
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Delete);

            if (_entity.TransHistory_id == 0) return;
			DataContext.Writer.TransHistories.Delete(_entity);
			DataContext.Writer.SubmitChanges();
		}

		public static List<History> GetTransactionHistory(int transID, Netpay.Infrastructure.TransactionStatus status)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var exp = from th in DataContext.Reader.TransHistories select th;
			switch (status)
			{
				case Netpay.Infrastructure.TransactionStatus.Captured:
					exp = exp.Where(th => th.TransPass_id == transID);
					break;
				case Netpay.Infrastructure.TransactionStatus.Declined:
					exp = exp.Where(th => th.TransFail_id == transID);
					break;
				case Netpay.Infrastructure.TransactionStatus.Authorized:
					exp = exp.Where(th => th.TransPreAuth_id == transID);
					break;
				case Netpay.Infrastructure.TransactionStatus.Pending:
					exp = exp.Where(th => th.TransPending_id == transID);
					break;
				default:
					return null;
			}

			var results = exp.Select(th => new History(th)).ToList();
			return results;
		}	
        
        public static KeyValuePair<string, int?> GetTransID(int TransHistoryId)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            int? result=null;
            var exp = from th in DataContext.Reader.TransHistories select th;
            exp = exp.Where(th => th.TransHistory_id == TransHistoryId);
                     
            if (exp.Where(th => th.TransPass_id != null).Count() > 0)
            {
                result = exp.Where(th => th.TransPass_id != null).FirstOrDefault().TransPass_id;
                return new KeyValuePair<string, int?>("Pass", result); 
            }
            if (exp.Where(th => th.TransFail_id != null).Count() > 0)
            {
                result = exp.Where(th => th.TransFail_id != null).FirstOrDefault().TransFail_id;
                return new KeyValuePair<string, int?>("Fail", result);
            }
            if (exp.Where(th => th.TransPreAuth_id != null).Count() > 0)
            {
                result = exp.Where(th => th.TransPreAuth_id != null).FirstOrDefault().TransPreAuth_id;
                return new KeyValuePair<string, int?>("Approval", result);
            }
            if (exp.Where(th => th.TransPending_id != null).Count() > 0)
            {
                result = exp.Where(th => th.TransPending_id != null).FirstOrDefault().TransPending_id;
                return new KeyValuePair<string, int?>("Pending", result);
            }
            return new KeyValuePair<string, int?>("", result); 
                        
        }	

       
		public static List<History> Search(SearchFilters filters , ISortAndPage sortAndPage)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var exp = (from th in DataContext.Reader.TransHistories select th);
            if (filters != null)
            {
                //The Merchant ID value.
                if (filters.MerchantId.From.HasValue) exp = exp.Where(th => th.Merchant_id >= filters.MerchantId.From);
                if (filters.MerchantId.To.HasValue) exp = exp.Where(th => th.Merchant_id <= filters.MerchantId.To);

                //Check the dates range
                if (filters.Date.To.HasValue)
                {
                    // Set To date to EOD
                    filters.Date.To = filters.Date.To.Value.Date.AddDays(1).AddMilliseconds(-1);
                }

                //The Date value:
                if (filters.Date.From.HasValue) exp = exp.Where(th => th.InsertDate >= filters.Date.From);
                if (filters.Date.To.HasValue) exp = exp.Where(th => th.InsertDate <= filters.Date.To);

                //The TransHistoryId value , at the Filter section it is called ID:
                if (filters.ID.From.HasValue) exp = exp.Where(th => th.TransHistory_id >= filters.ID.From);
                if (filters.ID.To.HasValue) exp = exp.Where(th => th.TransHistory_id <= filters.ID.To);

                //The TransID value: represents TransPass_id/TransPreAuth_id/TransPending_id/TransFail_id (from TransHistory table)
                if (filters.TransID != null) {
                    if (filters.TransactionStatus == null)
                        exp = exp.Where(th => th.TransPass_id == filters.TransID || th.TransPreAuth_id == filters.TransID || th.TransPending_id == filters.TransID || th.TransFail_id == filters.TransID);
                    else {
                        switch (filters.TransactionStatus) {
                            case Netpay.Infrastructure.TransactionStatus.Captured:
                                exp = exp.Where(th => th.TransPass_id == filters.TransID.Value);
                                break;
                            case Netpay.Infrastructure.TransactionStatus.Declined:
                                exp = exp.Where(th => th.TransFail_id == filters.TransID.Value);
                                break;
                            case Netpay.Infrastructure.TransactionStatus.Authorized:
                                exp = exp.Where(th => th.TransPreAuth_id == filters.TransID.Value);
                                break;
                            case Netpay.Infrastructure.TransactionStatus.Pending:
                                exp = exp.Where(th => th.TransPending_id == filters.TransID.Value);
                                break;
                        }
                    }
                }
                //The Event Type value
                if (filters.EventType != null) exp = exp.Where(th => th.TransHistoryType_id == (int)filters.EventType);
           }
            return exp.OrderByDescending(x => x.TransHistory_id).ApplySortAndPage(sortAndPage).Select(th => new History(th)).ToList();
        }
	}
}
