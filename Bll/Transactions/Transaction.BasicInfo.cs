﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Transactions
{
	public partial class TransactionBasicInfo : BaseDataObject
    {

        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Transactions.Module.Current, SecuredObjectName); } }

		public int ID { get; set; }

		public Netpay.Infrastructure.TransactionStatus Status { get; set; }
		[Infrastructure.EntityPropertyMapping("InsertDate")]
		public DateTime InsertDate { get; set; }
		public decimal Amount { get; set; }
		public bool IsTest { get; set; }
        public bool Is3dSecure { get; set; }
        public bool? IsCardPresent { get; set; }
        public string SystemText { get; set; }

        public int? PassedTransactionID { get; set; }
		public string ApprovalNumber { get; set; }
		public int? MerchantID { get; set; }
		public int? CustomerID { get; set; }

		//currency
		[Infrastructure.EntityPropertyMapping("Currency")]
		public int CurrencyID { get; set; }
		public Currency Currency { get { return Currency.Get(CurrencyID); } set { if (value == null) return; CurrencyID = value.ID; } }
		public string CurrencyIsoCode { get { var cur = Currency; if (cur == null) return null; return cur.IsoCode; } set { Currency = Currency.Get(value); } }
		public string CurrencySymbol { get { var cur = Currency; if (cur == null) return null; return cur.Symbol; } }

		//Info
		public Infrastructure.CreditType CreditType { get; set; }
		public TransactionType TransType { get; set; }
		public Infrastructure.TransactionSource? TransactionSource { get; set; }
        public bool IsCashback { get; set; }

        //payment
        public CommonTypes.PaymentMethodEnum PaymentMethodID { get; set; }
		public PaymentMethods.PaymentMethod RefPaymentMethod { get { return PaymentMethods.PaymentMethod.Load(PaymentMethodID); } }
		public string PaymentMethodName { get { var pm = RefPaymentMethod; if (pm == null) return string.Empty; return pm.Name; } }
		public virtual string PaymentMethodDisplay { get; set; }
		public virtual string IssuerCountryIsoCode { get; set; }

		//installments
		public byte Installments { get; set; }
		public bool IsInstallments { get { return Installments > 1; } }
		public decimal AnyOtherInstallment { get { if (!IsInstallments) return 0; return (int)(Amount / Installments); } }
		public decimal FirstInstallment { get { if (!IsInstallments) return Amount; return AnyOtherInstallment + (Amount - (AnyOtherInstallment * Installments)); } }

		//settlement
		public int PrimaryPaymentID { get; set; }
		public string PaymentsIDs { get; set; }
        public DateTime? MerchantPayDate { get; set; }

		//chb
		public byte DeniedStatusID { get; set; }
		public bool IsPendingChargeback { get; set; }
		public DateTime ChargebackDate { get; set; }

		//response
		public string ReplyCode { get; set; }

		//fees
		public decimal TransactionFee { get; set; } // used to be NetpayFee

		protected TransactionBasicInfo()
        {
            PaymentsIDs = "0"; //Set default value - null is not acceptable by DB
        }

		public decimal SignedAmount
		{		
			get
			{
				if (TransactionSource == Infrastructure.TransactionSource.CcStorageFee || CreditType == Infrastructure.CreditType.Refund) return (Amount * (-1));
				else return Amount;
			}
		}

		public string StatusText { get { return Status.ToString(); } set { } }
		public string CreditTypeText { get { return CreditType.ToString(); } set { } }
		public string FormatedAmount { get { return new Money(SignedAmount, Currency).ToString(); } }


		private List<Installment> _installmentList;
		public List<Installment> InstallmentList
		{
			get
			{
				if (Status != TransactionStatus.Captured) return null;
				if (_installmentList != null) return _installmentList;
				_installmentList = Installment.LoadForTransaction(ID);
				return _installmentList;
			}
		}

		private List<EPA> _epaList;
		public List<EPA> EpaList
		{
			get
			{
				if (Status != TransactionStatus.Captured) return null;
				if (_epaList != null) return _epaList;
				_epaList = EPA.LoadForTransaction(ID);
				return _epaList;
			}
		}

		protected Merchants.Merchant _merchant;
		public Merchants.Merchant Merchant
		{
			get {
				if (_merchant != null) return _merchant;
				if (MerchantID != null) _merchant = Bll.Merchants.Merchant.Load(MerchantID.Value);
				return _merchant;
			} set {
				if (ID == 0) _merchant = value;
			}
		}

		
		public string MoneyTransferText
		{
			get
			{
				const string NO_RECORD = "No record";
				if (EpaList == null) return NO_RECORD;
				if (EpaList.Count == 0) return NO_RECORD;
				if (Installments == 1)
				{
					if (EpaList[0].IsPaid && EpaList[0].IsRefunded)
						return "Paid and refunded (" + EpaList[0].PaidInsertDate.ToShortDateString() + ", " + EpaList[0].RefundedInsertDate.ToShortDateString() + ")";
					if (EpaList[0].IsPaid)
						return "Paid (" + EpaList[0].PaidInsertDate.ToShortDateString() + ")";
					if (EpaList[0].IsRefunded)
						return "Refunded (" + EpaList[0].RefundedInsertDate.ToShortDateString() + ")";
					return NO_RECORD;
				}
				string sTemp, sOut = "<table>";
				foreach (var record in EpaList)
				{
					if (EpaList[0].IsPaid && EpaList[0].IsRefunded)
						sTemp = "Paid and refunded (" + EpaList[0].PaidInsertDate.ToShortDateString() + ", " + EpaList[0].RefundedInsertDate.ToShortDateString() + ")";
					else if (EpaList[0].IsPaid)
						sTemp = "Paid (" + EpaList[0].PaidInsertDate.ToShortDateString() + ")";
					else if (EpaList[0].IsRefunded)
						sTemp = "Refunded (" + EpaList[0].RefundedInsertDate.ToShortDateString() + ")";
					else
						sTemp = string.Empty;
					sOut += "<tr><td>" + sTemp + "</td></tr>";
				}
				return (sOut == "<table>" ? NO_RECORD : sOut + "</table>");
			}
		}


	}
}
