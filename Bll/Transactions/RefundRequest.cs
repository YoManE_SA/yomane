﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Bll.Accounts;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Transactions
{
	public class RefundRequest : BaseDataObject
	{
		public const string SecuredObjectName = "RefundRequest";
		public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Transactions.Module.Current, SecuredObjectName); } }

		public enum RequestStatus
		{
			Pending = 1,
			InProgress = 2,
			Created = 50,
			Processed = 51,
			Batched = 52,
			AdminCancel = 101,
			SourceCancel = 102,
		}

		public static Dictionary<RequestStatus, System.Drawing.Color> StatusColor = new Dictionary<RequestStatus, System.Drawing.Color> { 
			{ RequestStatus.Pending, System.Drawing.ColorTranslator.FromHtml("#6699cc") },
			{ RequestStatus.InProgress, System.Drawing.ColorTranslator.FromHtml("#9e6cff") },
			{ RequestStatus.Created, System.Drawing.ColorTranslator.FromHtml("#ff8040") }, 
			{ RequestStatus.Processed, System.Drawing.ColorTranslator.FromHtml("#ff8040") }, 
			{ RequestStatus.Batched, System.Drawing.ColorTranslator.FromHtml("#ffeb65") },
			{ RequestStatus.AdminCancel, System.Drawing.ColorTranslator.FromHtml("#66cc66") },
			{ RequestStatus.SourceCancel, System.Drawing.ColorTranslator.FromHtml("#ff6666") }
		};

		public class SearchFilters{
			public Range<int?> ID;
			public Range<DateTime?> Date;
			public Range<int?> TransactionID;
			public Range<decimal?> RequestAmount;
			public Range<decimal?> Amount;
			public string CurrencyISOCode;
			public List<int> MerchantIDs;
			public List<RequestStatus> Status;
			public bool AllCompleted;

		}
		private Dal.Netpay.RefundRequest _entity;

		public int ID { get { return _entity.RefundRequest_id; } }
		public int MerchantID { get { return _entity.Merchant_id; } }
		public int TransactionID { get { return _entity.SourceTransPass_id; } }
		public System.DateTime Date { get { return _entity.InsertDate; } }
		public decimal RequestAmount { get { return _entity.AmountRequest; } }
		public decimal? RefundAmount { get { return _entity.AmountRefund; } set { _entity.AmountRefund = value; } }
		public string CurrencyIso { get { return _entity.CurrencyISOCode; } }
		public string Comment { get { return _entity.Comment; } set { _entity.Comment = value; } }
		public RequestStatus Status { get { return (RequestStatus)_entity.ActionStatus_id; } }
		public byte Flag { get { return _entity.Flag; } set { _entity.Flag = value; } }
		//public string ConfirmationNum { get { return _entity.con; } set { _entity.RefundAskConfirmationNum = value; } }
		//public string StatusLog { get { return _entity.RefundAskStatusHistory; } }

		private Transaction _transaction;
		public Transaction Transaction
		{
			get{
				if(_transaction == null) return _transaction = Transaction.GetTransaction(MerchantID, TransactionID, TransactionStatus.Captured);
				return _transaction;
			}
		}

		private RefundRequest(Dal.Netpay.RefundRequest entity)
			
		{
			_entity = entity;
		}

		public static RefundRequest Load(int id)
		{
			return Search(new SearchFilters() { ID = new Range<int?>(id) }, null).FirstOrDefault();
		}

		public static List<RefundRequest> RequestsForTrasaction(int transactionID)
		{
			return Search(new SearchFilters() { TransactionID = new Range<int?>(transactionID) }, null);
		}

		public static RequestRefundResult Create(int transactionID, TransactionStatus transactionStatus, decimal amount, string comment)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Add);
			int? merchantId = null;
			AccountFilter.Current.Validate(Accounts.AccountType.Merchant, ref merchantId, false);
			//if (context.IsUserOfType(new UserRole[] { UserRole.MerchantPrimary, UserRole.MerchantLimited }, false)) merchantId = context.User.ID;
			var transaction = Transaction.GetTransaction(merchantId, transactionID, transactionStatus);
			if (transaction == null) return RequestRefundResult.TransactionNotFound;
			if (!transaction.Merchant.ProcessSettings.IsAskRefund) return RequestRefundResult.NoPermission;
			if (transaction.InsertDate < DateTime.Now.AddMonths(-12)) return RequestRefundResult.TransactionTooOld;

			var refunds = Search(new SearchFilters() { TransactionID = new Range<int?>(transactionID) }, null);
			decimal totalRefunded = refunds.Select(rr => (rr.Status == RequestStatus.SourceCancel || rr.Status == RequestStatus.AdminCancel ? 0 : rr.RefundAmount.GetValueOrDefault(rr.RequestAmount))).Sum();
			decimal availableForRefund = transaction.Amount - totalRefunded;

			if (availableForRefund <= 0) return RequestRefundResult.TransactionFullyRefunded;
			if (amount == 0) return RequestRefundResult.NoPermission;
			if (availableForRefund < amount) return RequestRefundResult.AmountTooLarge;

			var entity = new Dal.Netpay.RefundRequest();
			entity.Merchant_id = transaction.MerchantID.GetValueOrDefault();
			entity.SourceTransPass_id = transaction.ID;
			entity.ActionStatus_id = (int)RefundRequestStatus.Pending;
			entity.AmountRequest = amount;
			entity.Comment = comment;
			entity.CurrencyISOCode = transaction.CurrencyIsoCode;
			entity.InsertDate = DateTime.Now;
			new RefundRequest(entity).Save();
			return RequestRefundResult.Success;
		}


		public bool CancelRequest()
		{
			//Context.IsUserOfType(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
			if (Status != RequestStatus.Pending) return false;
			_entity.ActionStatus_id = (int)RefundRequestStatus.CanceledByMerchant;
			Save();
			return true;
		}

		private void Save()
		{
			DataContext.Writer.RefundRequests.Update(_entity, (_entity.RefundRequest_id != 0));
			DataContext.Writer.SubmitChanges();
		}

		public static TransactionRefundAbility GetRefundAbility(Transaction trans)
		{
			if (trans == null) return TransactionRefundAbility.None; // no transaction
			if (trans.Status != TransactionStatus.Captured) return TransactionRefundAbility.None; // not pass
			if (trans.CreditType == 0) return TransactionRefundAbility.None; // not debit
			if (trans.DebitCompanyID == null) return TransactionRefundAbility.None; // not bank transaction
			if ((trans.IsChargeback != null) && (bool)trans.IsChargeback) return TransactionRefundAbility.None; // charged back

			int debitCompanyID = (int)trans.DebitCompanyID;
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);
			var retrievals = Transaction.ChargebackHistory.LoadForTransaction(trans.ID);
			foreach (var retrieval in retrievals)
			{
				if ((retrieval.IsPendingChargeback) || (retrieval.TypeID == CommonTypes.TransactionHistoryType.Chargeback)) return TransactionRefundAbility.None;
			}
			var bank = trans.DebitCompany; //user.Domain.Cache.GetDebitCompany(debitCompanyID);
			if (bank == null) return TransactionRefundAbility.None; // bank not found
			if (!bank.IsAllowRefund) return TransactionRefundAbility.None; // refund not allowed
			return bank.IsAllowPartialRefund ? TransactionRefundAbility.FullOrPartial : TransactionRefundAbility.FullOnly;
		}

		public static List<RefundRequest> Search(SearchFilters filters, ISortAndPage sortAndPage)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);
			var expression = from r in DataContext.Reader.RefundRequests.Where(AccountFilter.Current.QueryValidate(Accounts.AccountType.Merchant, (Dal.Netpay.RefundRequest r) => r.Merchant_id)) 
				join t in DataContext.Reader.tblCompanyTransPasses on r.SourceTransPass_id equals t.ID orderby r.InsertDate descending select new { Request = r, Transaction = t };
			if (filters != null) {
				if (filters.ID.From.HasValue) expression = expression.Where(t => t.Request.RefundRequest_id >= filters.ID.From);
				if (filters.ID.To.HasValue) expression = expression.Where(t => t.Request.RefundRequest_id <= filters.ID.To);
				if (filters.MerchantIDs != null && filters.MerchantIDs.Count > 0) expression = expression.Where(t => filters.MerchantIDs.Contains(t.Request.Merchant_id));
				//if (filters.isChargeback == null)
				if (filters.Date.From.HasValue) expression = expression.Where(t => t.Request.InsertDate >= filters.Date.From);
				if (filters.Date.To.HasValue) expression = expression.Where(t => t.Request.InsertDate <= filters.Date.To);

				if (filters.TransactionID.From.HasValue) expression = expression.Where(t => t.Request.SourceTransPass_id >= filters.TransactionID.From);
				if (filters.TransactionID.To.HasValue) expression = expression.Where(t => t.Request.SourceTransPass_id <= filters.TransactionID.To);
				if (filters.CurrencyISOCode != null) expression = expression.Where(t => t.Request.CurrencyISOCode == filters.CurrencyISOCode);
				if (filters.RequestAmount.From != null) expression = expression.Where(t => t.Request.AmountRequest >= filters.RequestAmount.From);
				if (filters.RequestAmount.To != null) expression = expression.Where(t => t.Request.AmountRequest <= filters.RequestAmount.To);
				if (filters.Amount.From != null) expression = expression.Where(t => t.Request.AmountRefund >= filters.Amount.From);
				if (filters.Amount.To != null) expression = expression.Where(t => t.Request.AmountRefund <= filters.Amount.To);

				//if (filters.paymentMethodID != null) expression = expression.Where(t => t.Transaction.PaymentMethod == filters.paymentMethodID.Value);
				//if (filters.creditType != null) expression = expression.Where(t => t.Transaction.CreditType == filters.creditType.Value);
				//if (filters.orderID != null) expression = expression.Where(t => t.Transaction.OrderNumber == filters.orderID);
				//if (filters.replyCode != null) expression = expression.Where(t => t.Transaction.replyCode == filters.replyCode);

				if (filters.Status != null && filters.Status.Count > 0)
					expression = expression.Where(t => filters.Status.Contains((RequestStatus)t.Request.ActionStatus_id));
				if (filters.AllCompleted)
					expression = expression.Where(t => (t.Request.ActionStatus_id == (byte)RefundRequestStatus.Processing) || (t.Request.ActionStatus_id == (byte)RefundRequestStatus.ProcessedAccepted) || (t.Request.ActionStatus_id == (byte)RefundRequestStatus.CreatedAccepted));
			}
			if (sortAndPage != null)
				sortAndPage.DataFunction = (pi) => Search(filters, sortAndPage);
			return expression.ApplySortAndPage(sortAndPage).Select(t => new RefundRequest(t.Request) { _transaction = new Transaction(t.Transaction) }).ToList();
		}

		public void AddLog(string text)
		{
			StatusChange.Create(StatusChange.StatusItemType.RefundRequest, ID, (StatusChange.ActionStatus) Status, text);	
		}

		public int Process(decimal amount)
		{
			var requestParams = Transaction.GetProcessParamters();
			requestParams["RefTransID"] = TransactionID.ToString();
			requestParams["requestSource"] = "6";
			requestParams["isLocalTransaction"] = "true";
			requestParams["Comment"] = Comment;
			_entity.AmountRefund = amount;
			_entity.ActionStatus_id = (byte)RequestStatus.InProgress;
			var responseParams = Transaction.ProcessTransaction(Transaction.RefPaymentMethod.Group, requestParams);
			var retId = responseParams["TransID"].ToNullableInt();
			if (responseParams["Reply"] == "000") {
				_entity.ActionStatus_id = (byte)RequestStatus.Processed;
				AddLog(string.Format("Approved TransactionID: {0}", retId));
			} else if (responseParams["Reply"] == "001") {
				_entity.ActionStatus_id = (byte) RequestStatus.Pending;
				AddLog(string.Format("Pending TransactionID: {0}", retId));
			} else {
				AddLog(string.Format("Declined TransactionID: {0}", retId));
			}
			Save();
			return retId.GetValueOrDefault();
		}

		public int Create(decimal amount)
		{
			_entity.AmountRefund = amount;
			var retId = DataContext.Writer.CreateRefund(TransactionID, null, amount, Comment).SingleOrDefault().Column1;
			_entity.AmountRefund = (int)RequestStatus.Created;
			AddLog(string.Format("Created TransactionID: {0}", retId));
			Save();
			return retId.GetValueOrDefault();
		}
	}
}
