﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Security;
using Netpay.Bll.Accounts;
using Netpay.CommonTypes;

namespace Netpay.Bll.Transactions
{
    public partial class Transaction
    {
        public Transaction(Netpay.Dal.Netpay.tblCompanyTransPass entity)
            : this(TransactionStatus.Captured)
        {
            if (entity != null) LoadEntity(entity);
        }

        public static int[] DeniedArrayNumbers = new int[] { 1, 2, 4, 6, 8 };

        private void LoadEntity(Netpay.Dal.Netpay.tblCompanyTransPass entity)
        {
            //////////////////////////////////////////////////
            FraudDetectionLogId = entity.FraudDetectionLog_id;
            DeniedSendDate = entity.DeniedSendDate;
            DeniedPrintDate = entity.DeniedPrintDate; // Used in the 'ChargeBack' tab , in 'Print' function
            PD = entity.PD; // Used in "Rolling reserve filter"
                            //DeniedValDate = entity.DeniedValDate;
                            //ReplyCode = entity.replyCode;

            // 'IsRetrievalRequest' is needed for the functionality of
            // The 'ChargeBack/Confirmation' tab ,       
            IsRetrievalRequest = entity.IsRetrievalRequest;
            /////////////////////////////////////////////////

            ID = entity.ID;
            Amount = entity.Amount;
            CurrencyID = entity.Currency.Value;
            Installments = entity.Payments;
            CreditType = (Infrastructure.CreditType)entity.CreditType;
            SystemText = entity.SystemText;
            IsCashback = entity.IsCashback;

            DebitCompanyID = entity.DebitCompanyID;
            MerchantID = entity.companyID;
            PayerDataID = entity.TransPayerInfo_id;
            PaymentDataID = entity.TransPaymentMethod_id;
            CustomerID = entity.CustomerID;

            PaymentMethodID = (CommonTypes.PaymentMethodEnum)entity.PaymentMethod.GetValueOrDefault();
            PaymentMethodReferenceID = entity.PaymentMethodID;
            PaymentMethodDisplay = entity.PaymentMethodDisplay;
            IsTest = entity.isTestOnly;
            Is3dSecure = entity.Is3DSecure.GetValueOrDefault();
            IsCardPresent = entity.IsCardPresent;


			DeniedStatusID = entity.DeniedStatus;
			TransactionSource = (Infrastructure.TransactionSource?) entity.TransSource_id;
            TransType = (TransactionType) entity.TransactionTypeID;
			PaymentsIDs = entity.PayID;
			PrimaryPaymentID = entity.PrimaryPayedID;
			InsertDate = entity.InsertDate;
			ChargebackDate = entity.DeniedDate;
			ApprovalNumber = entity.ApprovalNumber;
			DeniedStatusText = ((Netpay.Infrastructure.DeniedStatus)DeniedStatusID).ToString();
			OrderNumber = entity.OrderNumber;
			Comment = entity.Comment;
			RecurringSeriesID = entity.RecurringSeries;
			IP = entity.IPAddress;
			IsChargeback = entity.IsChargeback;
            IsFraud = entity.IsFraudByAcquirer.HasValue ? entity.IsFraudByAcquirer.Value : false;

            TerminalNumber = entity.TerminalNumber;
            DeniedAdminComment = entity.DeniedAdminComment;
            ReferringUrl = entity.referringUrl;
            PayerIDUsed = entity.payerIdUsed;
            DebitReferenceCode = entity.DebitReferenceCode;
            DebitReferenceNum = entity.DebitReferenceNum;
            IPCountry = entity.IPCountry;
            OriginalTransactionID = entity.OriginalTransID;
            IsPendingChargeback = entity.IsPendingChargeback.GetValueOrDefault(false);
            PayForText = entity.PayforText;
            ProductId = entity.MerchantProduct_id;
            // fees
            MerchantPayDate = entity.MerchantPD;
            TransactionFee = entity.netpayFee_transactionCharge;
            RatioFee = entity.netpayFee_ratioCharge;
            ClarificationFee = entity.netpayFee_ClrfCharge.GetValueOrDefault();
            ChargebackFee = entity.netpayFee_chbCharge.GetValueOrDefault();
            HandlingFee = entity.HandlingFee;
            AcquirerReferenceNum = entity.AcquirerReferenceNum;

            DebitFee = entity.DebitFee;
            DebitFeeChb = entity.DebitFeeChb.GetValueOrDefault();

            // payment method
            /*
			if (loadPaymentMethod)
			{
				if (entity.list_PaymentMethod != null)
                {
                    // load payment method
                    PaymentMethod = new PaymentMethodVO(domainHost, entity.list_PaymentMethod);

                    // load transaction payment method
					if (entity.list_PaymentMethod.pm_Type.Value == (int)PaymentMethodType.CreditCard)
						PaymentMethodData = new PaymentMethodDataVO(entity.tblCreditCard);
					if (entity.list_PaymentMethod.pm_Type.Value == (int)PaymentMethodType.ECheck || entity.list_PaymentMethod.pm_Type.Value == (int)PaymentMethodType.BankTransfer)
						PaymentMethodData = new PaymentMethodDataVO(domainHost, entity.tblCheckDetail);
					if (entity.list_PaymentMethod.pm_Type.Value == (int)PaymentMethodType.PhoneDebit)
                        PaymentMethodData = new PaymentMethodDataVO(entity.trans_PhoneDetail);
				}
			}
			*/
            // merchnt

            // installments
            //if (entity.Payments > 1) InstallmentList = entity.tblCompanyTransInstallments.Select(i => new InstallmentVO(i)).ToList();

            //EPA import log (is the transaction paid)
            //if (loadEpaImportLog) EpaImportLogList = entity.tblLogImportEPAs.Select(i => new EpaImportLogVO(i)).OrderBy(l => l.Installment).ToList();
        }


        public void UpdateChargeBackForTransPass(int transactionId, string deniedAdminComment, DateTime deniedDate)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Edit);

            if (transactionId <= 0) return;

            Netpay.Dal.Netpay.tblCompanyTransPass entity = new Netpay.Dal.Netpay.tblCompanyTransPass();

            entity = (from tbltp in DataContext.Reader.tblCompanyTransPasses
                      where tbltp.ID == transactionId
                      select tbltp).SingleOrDefault();

            if (entity == null) return;

            if (!(string.IsNullOrEmpty(deniedAdminComment)))
                entity.DeniedAdminComment = deniedAdminComment;

            if (entity.PaymentMethodID == 25) entity.DeniedValDate = deniedDate.AddMonths(1);
            else entity.DeniedValDate = deniedDate;

            entity.DeniedDate = deniedDate;

            DataContext.Writer.tblCompanyTransPasses.Update(entity, entity.ID > 0);
            DataContext.Writer.SubmitChanges();
        }

        public static void UpdateTransactionComment(int transactionId, string comment)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Edit);

            if (transactionId <= 0) return;
            var entity = (from tbltp in DataContext.Reader.tblCompanyTransPasses where tbltp.ID == transactionId select tbltp).SingleOrDefault();
            if (entity == null) return;
            entity.Comment = comment;
            DataContext.Writer.tblCompanyTransPasses.Update(entity, entity.ID > 0);
            DataContext.Writer.SubmitChanges();
        }

        public static void UpdateTransactionDeniedSendDate(int transactionId, DateTime deniedSendDate)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Edit);

            if (transactionId <= 0) return;
            Dal.Netpay.tblCompanyTransPass entity = (from tbltp in DataContext.Reader.tblCompanyTransPasses
                                                     where tbltp.ID == transactionId
                                                     select tbltp).SingleOrDefault();

            if (entity != null) entity.DeniedSendDate = deniedSendDate;

            DataContext.Writer.tblCompanyTransPasses.Update(entity, entity.ID > 0);
            DataContext.Writer.SubmitChanges();
        }
        
        private void SavePass()
        {
            var entity = new Netpay.Dal.Netpay.tblCompanyTransPass();
            //
            entity.FraudDetectionLog_id = FraudDetectionLogId;
            //

            entity.PD = DataContext.SQLMinDateValue;
            entity.MerchantPD = (MerchantPayDate.GetValueOrDefault() < DataContext.SQLMinDateValue ? DataContext.SQLMinDateValue : MerchantPayDate.GetValueOrDefault());

            //Check the Dates logic of DeniedPrintDate and DeniedSendDate
            entity.DeniedPrintDate = (ChargebackDate < DataContext.SQLMinDateValue ? DataContext.SQLMinDateValue : ChargebackDate);
            entity.DeniedSendDate = (ChargebackDate < DataContext.SQLMinDateValue ? DataContext.SQLMinDateValue : ChargebackDate);
            entity.DeniedDate = (ChargebackDate < DataContext.SQLMinDateValue ? DataContext.SQLMinDateValue : ChargebackDate);
            entity.replyCode = "000";

            if (PaymentData != null && PaymentData.OldCreditCardID != null)
            {
                entity.PaymentMethod_id = 1;
                entity.CreditCardID = PaymentMethodReferenceID;
            }
            else if (IsPaymentManagerTransaction)
                entity.PaymentMethod_id = 4;
            else if (IsPaymentFeesTransaction)
                entity.PaymentMethod_id = 5;

            entity.Amount = Amount;
            entity.Currency = CurrencyID;
            entity.Payments = Installments;
            entity.CreditType = (byte)CreditType;
            entity.IsCashback = IsCashback;
            entity.DebitCompanyID = DebitCompanyID;
            entity.companyID = MerchantID.GetValueOrDefault();
            entity.CustomerID = CustomerID.GetValueOrDefault();
            entity.TransPayerInfo_id = PayerDataID;
            entity.TransPaymentMethod_id = PaymentDataID;
            entity.PaymentMethod = (short?)PaymentMethodID;
            entity.PaymentMethodID = PaymentMethodReferenceID;
            entity.PaymentMethodDisplay = PaymentMethodDisplay.EmptyIfNull();
            entity.isTestOnly = IsTest;
            entity.Is3DSecure = Is3dSecure;
            entity.DeniedStatus = DeniedStatusID;
            entity.TransSource_id = (byte?)TransactionSource;
            entity.TransactionTypeID = (int)TransType;
            entity.PayID = PaymentsIDs;
            entity.PrimaryPayedID = PrimaryPaymentID;
            entity.InsertDate = InsertDate;
            entity.ApprovalNumber = ApprovalNumber.EmptyIfNull();
            //entity.DeniedStatus = DeniedStatusID;
            entity.OrderNumber = OrderNumber;
            entity.Comment = Comment;
            entity.RecurringSeries = RecurringSeriesID;
            entity.IPAddress = IP;
            entity.IsChargeback = IsChargeback;
            entity.IsFraudByAcquirer = IsFraud;
            entity.TerminalNumber = TerminalNumber;
            entity.DeniedAdminComment = DeniedAdminComment.EmptyIfNull();
            entity.referringUrl = ReferringUrl.EmptyIfNull();
            entity.payerIdUsed = PayerIDUsed.EmptyIfNull();
            entity.DebitReferenceCode = DebitReferenceCode;
            entity.DebitReferenceNum = DebitReferenceNum;
            entity.IPCountry = IPCountry.EmptyIfNull();
            entity.OriginalTransID = OriginalTransactionID;
            entity.IsPendingChargeback = IsPendingChargeback;
            entity.PayforText = PayForText;
            entity.MerchantProduct_id = ProductId;
            // fees
            entity.netpayFee_transactionCharge = TransactionFee;
            entity.netpayFee_ratioCharge = RatioFee;
            entity.netpayFee_ClrfCharge = ClarificationFee;
            entity.netpayFee_chbCharge = ChargebackFee;
            entity.HandlingFee = HandlingFee;
            entity.DebitFee = DebitFee;
            entity.DebitFeeChb = DebitFeeChb;

            //old settlement data
            if (PaymentsIDs.EmptyIfNull().ToUpper() == ";X;")
            {
                entity.UnsettledAmount = 0;
                entity.UnsettledInstallments = 0;
            }
            else
            {
                bool isMinusType = new Infrastructure.CreditType[] { Infrastructure.CreditType.Debit, Infrastructure.CreditType.Refund }.Contains(CreditType);
                entity.UnsettledAmount = (isMinusType ? -1 : 1) * Amount;
                //if (CreditType == Infrastructure.CreditType.Installments) entity.UnsettledInstallments = Installments;
                if (CreditType == Infrastructure.CreditType.CreditCharge) entity.UnsettledInstallments = 1;
                else entity.UnsettledInstallments = Installments;
            }

            DataContext.Writer.tblCompanyTransPasses.InsertOnSubmit(entity);
            DataContext.Writer.SubmitChanges();
            if (!string.IsNullOrEmpty(BatchData))
            {
                DataContext.Writer.AuthorizationTransDatas.InsertOnSubmit(new Dal.Netpay.AuthorizationTransData() { TransPass_id = entity.ID, VariableChar = BatchData });
                DataContext.Writer.SubmitChanges();
            }
            ID = entity.ID;
        }


        private static IQueryable<Dal.Netpay.tblCompanyTransPass> SearchExpressionTransCaptured(LoadOptions loadOptions, SearchFilters filters)
        {
            var expression = (IQueryable<Netpay.Dal.Netpay.tblCompanyTransPass>)from t in DataContext.Reader.tblCompanyTransPasses orderby t.InsertDate descending select t;

            // add filters
            if (Login.Current.Role == UserRole.Partner)
            {
                if (filters.merchantIDs == null) filters.merchantIDs = new List<int>();
                if (filters.customersIDs == null) filters.customersIDs = new List<int>();
                expression = expression.Where(t => filters.merchantIDs.Contains(t.companyID) || filters.customersIDs.Contains(t.CustomerID));
            }
            else
            {
                if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
                    expression = expression.Where(t => filters.merchantIDs.Contains(t.companyID));
                if (filters.customersIDs != null && filters.customersIDs.Count > 0)
                    expression = expression.Where(t => filters.customersIDs.Contains(t.CustomerID));
            }
            if (filters.isChargeback == null)
            {
                if (filters.deniedDate.From != null) expression = expression.Where(t => t.DeniedDate >= filters.deniedDate.From.Value.MinTime());
                if (filters.deniedDate.To != null) expression = expression.Where(t => t.DeniedDate <= filters.deniedDate.To.Value.AlignToEnd());
                if (filters.date.From != null) expression = expression.Where(t => t.InsertDate >= filters.date.From.Value.MinTime());
                if (filters.date.To != null) expression = expression.Where(t => t.InsertDate <= filters.date.To.Value.MaxTime());
            }
            else
            {
                if (filters.isChargeback.Value)
                {
                    expression = expression.Where(t => t.IsChargeback.Value);
                    if (filters.deniedDate.From != null) expression = expression.Where(t => t.DeniedDate >= filters.deniedDate.From.Value.MinTime());
                    if (filters.deniedDate.To != null) expression = expression.Where(t => t.DeniedDate <= filters.deniedDate.To.Value.MaxTime());
                    if (filters.date.From != null) expression = expression.Where(t => t.DeniedDate >= filters.date.From.Value.MinTime());
                    if (filters.date.To != null) expression = expression.Where(t => t.DeniedDate <= filters.date.To.Value.MaxTime());
                }
                else
                {
                    expression = expression.Where(t => !t.IsChargeback.Value);
                    if (filters.date.From != null) expression = expression.Where(t => t.InsertDate >= filters.date.From.Value.MinTime());
                    if (filters.date.To != null) expression = expression.Where(t => t.InsertDate <= filters.date.To.Value.MaxTime());
                }
            }

            //IsFraud
            if (filters.isFraud.HasValue)
                expression = expression.Where(t => t.IsFraudByAcquirer == filters.isFraud.Value);

            //History Types
            if (filters.HistoryTypes != null)
                expression = expression.Where(t => DataContext.Reader.TransHistories.Where(v => filters.HistoryTypes.Contains((CommonTypes.TransactionHistoryType)v.TransHistoryType_id)).Select(v => v.TransPass_id).Contains(t.ID));

            //Specific Transaction ID
            if (filters.specificTransactionID.HasValue)
            {
                expression = expression.Where(t => t.ID == filters.specificTransactionID.Value);
            }

            //Transaction ID Range
            if (filters.ID.From != null)
                expression = expression.Where(t => t.ID >= filters.ID.From.Value);
            if (filters.ID.To != null)
                expression = expression.Where(t => t.ID <= filters.ID.To.Value);
            //Terminal Number
            if (!string.IsNullOrWhiteSpace(filters.debitTerminalNumber))
                expression = expression.Where(t => t.TerminalNumber == filters.debitTerminalNumber);
            //Debit Company
            if (filters.debitCompanyID != null)
                expression = expression.Where(t => t.DebitCompanyID == filters.debitCompanyID.Value);
            //Payment Method
            if (filters.paymentMethodID != null)
                expression = expression.Where(t => t.PaymentMethod == filters.paymentMethodID.Value);
            //Credit Types
            if (filters.creditTypes != null && filters.creditTypes.Count() > 0)
            {
                //If the search include "Regular" type , return "Regular" and 
                //also values that are different than 0-"Refund" and 8-"Installments"
                if (filters.creditTypes.Contains(1))
                {
                    expression = expression.Where(t => (filters.creditTypes.Contains(t.CreditType)) || (t.CreditType != 0 && t.CreditType != 8));
                }
                else
                {
                    expression = expression.Where(t => (filters.creditTypes.Contains(t.CreditType)));
                }
            }
            //Order ID
            if (filters.orderID != null)
                expression = expression.Where(t => t.OrderNumber.StartsWith(filters.orderID));
            //Currency ID
            if (filters.currencyID != null)
                expression = expression.Where(t => t.Currency == filters.currencyID);
            //Amount
            if (filters.amount.From != null)
                expression = expression.Where(t => t.Amount >= filters.amount.From);
            if (filters.amount.To != null)
                expression = expression.Where(t => t.Amount <= filters.amount.To);
            //Reply Code
            if (!string.IsNullOrEmpty(filters.replyCode))
            {
                expression = expression.Where(t => t.replyCode == filters.replyCode);
            }
            //Payment Methods Id's
            if (filters.paymentMethodsIds != null)
                expression = expression.Where(t => filters.paymentMethodsIds.Contains(t.PaymentMethod.Value));

            if (filters.PayID != null)
                expression = expression.Where(t => t.PrimaryPayedID == filters.PayID.Value);

            //Device ID
            if (filters.deviceId != null)
                expression = expression.Where(t => t.MobileDevice_id == filters.deviceId);

            //Payment Status
            if (filters.paymentsStatusList != null && filters.paymentsStatusList.Count() > 0)
            {
                if (filters.paymentsStatusList.Contains(PaymentsStatus.Archived))
                    expression = expression.Where(t => t.PayID.Contains(";X;")); // [V]

                if (filters.paymentsStatusList.Contains(PaymentsStatus.PartiallySettled))
                    expression = expression.Where(t => t.PayID.Length > 3 && t.PayID.Contains(";0;")); // [V]

                if (filters.paymentsStatusList.Contains(PaymentsStatus.Settled))
                    expression = expression.Where(t => t.PrimaryPayedID != 0); // [V]

                if (filters.paymentsStatusList.Contains(PaymentsStatus.Unsettled))
                    expression = expression.Where(t => t.PayID.Contains(";0;")); // [V]

            }

            //Payment status bank dropdown
            if (filters.PaymentStatusBankDropDown != null)
            {
                if (filters.PaymentStatusBankDropDown == 1) //Overdue not paid transactions 
                {
                    expression = expression.Where(t =>
                      !t.isTestOnly
                      && t.MerchantPD <= DateTime.Now
                      && (from epatbl in DataContext.Reader.tblLogImportEPAs
                          where epatbl.TransID == t.ID
                          select epatbl.ID).ToList().Count() == 0);
                }
                else if (filters.PaymentStatusBankDropDown == 2) //Only paid transactions
                {
                    expression = expression.Where(t =>
                        !t.isTestOnly
                        && (from epatbl in DataContext.Reader.tblLogImportEPAs
                            where epatbl.TransID == t.ID
                            select epatbl.ID).ToList().Count() > 0);
                }
                else if (filters.PaymentStatusBankDropDown == 3) //Only not paid transactions
                {
                    expression = expression.Where(t =>
                        !t.isTestOnly
                        && (from epatbl in DataContext.Reader.tblLogImportEPAs
                            where epatbl.TransID == t.ID
                            select epatbl.ID).ToList().Count() == 0);
                }
            }

            //Debit ref' code
            if (!string.IsNullOrEmpty(filters.debitReferenceCode))
            {
                expression = expression.Where(t => t.DebitReferenceCode == filters.debitReferenceCode.Trim());
            }
            //Debit ref' num.
            if (!string.IsNullOrEmpty(filters.debitReferenceNumber))
            {
                expression = expression.Where(t => t.DebitReferenceNum == filters.debitReferenceNumber.Trim());
            }
            //Approval Number
            if (!string.IsNullOrEmpty(filters.debitApprovalNumber))
            {
                expression = expression.Where(t => t.ApprovalNumber == filters.debitApprovalNumber);
            }

            //HasUnsettledInstallments
            if (filters.HasUnsettledInstallments)
                expression = expression.Where(t => t.UnsettledInstallments > 0);

            //Denied Status - by int val
            if (filters.deniedStatusIntVal != null)
            {
                if (filters.deniedStatusIntVal == 1)
                {
                    expression = expression.Where(t => !t.isTestOnly && DeniedArrayNumbers.Contains(t.DeniedStatus));
                }
                else
                {
                    expression = expression.Where(t => t.isTestOnly || !DeniedArrayNumbers.Contains(t.DeniedStatus));
                }
            }

            //Chb Reason
            if (filters.ChbReason != null)
            {
                expression = expression.Where(t =>
                    (from th in DataContext.Reader.TransHistories
                     where ((th.TransHistoryType_id == 4 || th.TransHistoryType_id == 6) && (th.ReferenceNumber == filters.ChbReason))
                     select th.TransPass_id).ToList().Contains(t.ID)
                    ||
                    (
                        t.OriginalTransID > 0 &&
                        (from th in DataContext.Reader.TransHistories
                         where ((th.TransHistoryType_id == 4 || th.TransHistoryType_id == 6) && (th.ReferenceNumber == filters.ChbReason))
                         select th.TransPass_id).ToList().Contains(t.OriginalTransID)
                    )
                );
            }

            //Denied Date
            if (filters.deniedDate.From != null)
                expression = expression.Where(t => t.DeniedDate >= filters.deniedDate.From.Value.MinTime());
            if (filters.deniedDate.To != null)
                expression = expression.Where(t => t.DeniedDate <= filters.deniedDate.To.Value.MaxTime());

            //Denied Status
            if (filters.deniedStatus != null)
                expression = expression.Where(t => filters.deniedStatus.Contains((DeniedStatus)t.DeniedStatus));

            //Is Test
            if (filters.isTest != null)
                expression = expression.Where(t => t.isTestOnly);

            //Original Transaction ID
            if (filters.originalTransactionID.GetValueOrDefault(0) > 0)
                expression = expression.Where(t => t.OriginalTransID == filters.originalTransactionID.Value);

            //Is Pending Chargeback
            if (filters.isPendingChargeback != null)
            {
                if (!filters.isPendingChargeback.Value)
                    expression = expression.Where(t => t.IsPendingChargeback == filters.isPendingChargeback.Value || t.IsPendingChargeback == null);
                else
                    expression = expression.Where(t => t.IsPendingChargeback == filters.isPendingChargeback.Value);
            }

            //Batch ID
            if (filters.BatchID != null)
                expression = expression.Where(t => filters.BatchID.Contains(t.AuthorizationBatchID.GetValueOrDefault()));

            //Payer Filters
            if (filters.PayerFilters != null)
            {
                bool isEmpty;
                var sq = Payer.Search(DataContext.Reader, filters.PayerFilters, out isEmpty);
                if (!isEmpty) expression = expression.Where(t => sq.Contains(t.trans_TransPayerInfo));
            }

            //Payment Filters
            if (filters.PaymentFilters != null)
            {
                bool isEmpty;
                var sq = Payment.Search(DataContext.Reader, filters.PaymentFilters, out isEmpty);
                if (!isEmpty) expression = expression.Where(t => sq.Contains(t.trans_TransPaymentMethod));
            }

            //Rolling Reserve filters
            /*
            SELECT t.id, t.InsertDate, CompanyID, PrimaryPayedID, t.Currency, Amount, CreditType, PaymentMethodDisplay, CompanyName, CUR_ISOName, OriginalTransID, 
            CASE WHEN t.ID > OriginalTransID THEN t.ID ELSE OriginalTransID END BaseTransID, 
            CASE WHEN t.CreditType = 0 AND OriginalTransID = 0 THEN 1 ELSE 0 END IsUnreleased
            FROM tblCompanyTransPass t
            INNER JOIN tblCompany m ON CompanyID = m.ID
            INNER JOIN tblSystemCurrencies ON t.Currency = CUR_ID
            WHERE t.PaymentMethod = 4 AND(t.PD >= '01/01/2014 00:00:00') AND(t.PD <= '01/01/2016 23:59:00')
            ORDER BY IsUnreleased DESC, BaseTransID DESC, t.ID DESC, InsertDate desc
            */

            //Invoke Rolling Reserve filters only if the 'Show Only Rolling Reserve checkbox is checked.'
            if (filters.showOnlyRollingReserve.HasValue && filters.showOnlyRollingReserve.Value == true)
            {
                expression = expression.Where(t => t.PaymentMethod == 4);
                if (filters.releaseDate.From != null) expression = expression.Where(t => t.PD >= filters.releaseDate.From.Value.MinTime());
                if (filters.releaseDate.To != null) expression = expression.Where(t => t.PD <= filters.releaseDate.To.Value.MaxTime());
                if (filters.iCreditType0 == false) expression = expression.Where(t => t.CreditType != 0);
                if (filters.iCreditType1 == false) expression = expression.Where(t => t.CreditType != 1);
                if (filters.iNotRelease == true) expression = expression.Where(t => t.OriginalTransID == 0);
            }

            return expression;
        }

        private static List<Transaction> PostSearchTransCaptured(List<Transaction> transactions, SearchFilters filters)
        {
            if (filters.HasUnsettledInstallments)
            {
                // TBD: Meir to add the following to the search expression
                foreach (var currentTransaction in transactions)
                    if (currentTransaction.IsInstallments)
                        currentTransaction.InstallmentList.RemoveAll(i => i.SettlementID > 0);
            }

            return transactions;
        }

        public static List<Transaction> SearchTransCaptured(LoadOptions loadOptions, SearchFilters filters)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Read);
            
            // Get the search expression
            var expression = SearchExpressionTransCaptured(loadOptions, filters);

            List<Transaction> transactions = null;
            try
            {
                //DataContext.Reader.SetupLogger();
                if (loadOptions != null && loadOptions.loadBatchData)
                    transactions = (from t in expression
                                    join b in DataContext.Reader.AuthorizationTransDatas
                   on t.ID equals b.TransPass_id
                                    select new { t, b.VariableChar })
                                    .ApplySortAndPage(loadOptions.sortAndPage)
                                    .Select(c => new Transaction(c.t) { BatchData = c.VariableChar })
                                    .ToList();
                else transactions = expression.ApplySortAndPage(loadOptions.sortAndPage).Select(t => new Transaction(t)).ToList();

                if (filters.HasUnsettledInstallments)
                {
                    foreach (var currentTransaction in transactions)
                        if (currentTransaction.IsInstallments)
                            currentTransaction.InstallmentList.RemoveAll(i => i.SettlementID > 0);
                }

                PostSearchTransCaptured(transactions, filters);
            }
            catch (Exception ex)
            {
                Logger.Log(LogSeverity.Error, LogTag.DataAccess, ex.Message, DataContext.Reader.Logged);
                throw ex;
            }

            return transactions;
        }

        public Transaction CreateRefundTransaction(decimal amount)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Add);

            var ret = new Transaction()
            {
                Status = Status,
                ReplyCode = ReplyCode,
                TerminalNumber = TerminalNumber,
                PaymentsIDs = ";0;",
                IP = IP,
                OrderNumber = OrderNumber,
                Amount = amount,
                CurrencyID = CurrencyID,
                MerchantID = MerchantID,
                DebitReferenceCode = DebitReferenceCode,
                DebitCompanyID = DebitCompanyID,
                InsertDate = DateTime.Now,
                ApprovalNumber = ApprovalNumber,
                Installments = 1,
                TransactionSource = TransactionSource,
                TransType = TransactionType.Capture,
                CreditType = CreditType.Refund,
                CustomerID = CustomerID,
                //UnsettledInstallments = 1,
                MerchantPayDate = DateTime.Now.Date.AddDays(1).Date,
                PayForText = PayForText,
                OriginalTransactionID = ID,
                PaymentData = new Payment() { MerchantID = MerchantID.GetValueOrDefault() },
                PayerData = new Payer() { FullName = PayerData.FullName, EmailAddress = PayerData.EmailAddress, PersonalNumber = PayerData.PersonalNumber, PhoneNumber = PayerData.PhoneNumber }
            };
            if (PaymentData != null) PaymentData.MethodInstance.CopyTo(ret.PaymentData.MethodInstance);
            return ret;
        }

        public static List<Transaction> GetRefunds(int originalTransactionID)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Read);

            SearchFilters refundFilters = new SearchFilters();
            refundFilters.originalTransactionID = originalTransactionID;
            var lo = new LoadOptions() { sortAndPage = new SortAndPage(0, 10, "InsertDate", true) };
            List<Transaction> refunds = SearchTransCaptured(lo, refundFilters);
            return refunds;
        }



        public static List<int> GetRefundsAdmin(int transactionId)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Read);

            return (from t in DataContext.Reader.tblCompanyTransPasses
                    where (t.CreditType == 0 && (!t.IsChargeback.HasValue || t.IsChargeback.Value == false) && t.DeniedStatus != 5 && t.OriginalTransID == transactionId)
                    select t.ID).ToList();
        }

        public static List<int> GetDuplicate(int transactionID)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Read);

            return (from t in DataContext.Reader.tblCompanyTransPasses
                    where (t.DeniedStatus != 0 && t.OriginalTransID == transactionID)
                    select t.ID).ToList();
        }

        public static Transaction GetTransWithQrId(string transQrId)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Read);

            int? merchantId = null;
            AccountFilter.Current.Validate(Accounts.AccountType.Merchant, ref merchantId);
            string orderId = "SQR-ID:" + transQrId.ToString();
            var trans = (from t in DataContext.Reader.tblCompanyTransPasses where t.companyID == merchantId && t.InsertDate > DateTime.Now.AddMinutes(-90) && t.OrderNumber == orderId select new Transaction(t)).SingleOrDefault();
            return trans;
        }

        public class TransactionLookupResult
        {
            public int TransID { get; set; }
            public DateTime TransDate { get; set; }
            public string MethodString { get; set; }
            public string MerchantName { get; set; }
            public string MerchantWebSite { get; set; }
            public string MerchantSupportPhone { get; set; }
            public string MerchantSupportEmail { get; set; }
        }

        public static List<TransactionLookupResult> TransLookup(DateTime date, decimal amount, string last4cc)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Read);

            var list = (from t in DataContext.Reader.tblCompanyTransPasses
                        join pm in DataContext.Reader.TransPaymentMethods on t.TransPaymentMethod_id equals pm.TransPaymentMethod_id
                        join c in DataContext.Reader.tblCompanies on t.companyID equals c.ID
                        where !t.isTestOnly &&
                            (t.InsertDate >= date.Date.AddDays(-1) && t.InsertDate <= date.Date.AddDays(1)) &&
                            t.Amount == amount && pm.Value1Last4Text == last4cc
                        orderby t.InsertDate
                        select new { t, c }).ToList();
            var ret = new List<TransactionLookupResult>();
            foreach (var r in list)
            {
                var item = new TransactionLookupResult();
                item.TransID = r.t.ID;
                item.TransDate = r.t.InsertDate;
                item.MethodString = r.t.PaymentMethodDisplay;
                item.MerchantName = r.c.CompanyName;
                item.MerchantWebSite = r.c.URL;
                item.MerchantSupportPhone = r.c.merchantSupportPhoneNum;
                item.MerchantSupportEmail = r.c.merchantSupportEmail;
                ret.Add(item);
            }
            return ret;
        }

        /// <summary>
        /// Fee is taken from trx terminal when isFraud is true
        /// To set a different fee use <see cref="SetFraud(bool, decimal)"/>
        /// </summary>
        /// <param name="isFraud"></param>
        public void SetFraud(bool isFraud)
        {
            decimal fee = 0;
            if (IsFraud)
            {
                var terminal = Merchants.ProcessTerminals.GetTerminalForTransaction(MerchantID.Value, Currency.EnumValue, PaymentMethodID, IPCountry);
                if (terminal != null)
                    fee = terminal.FraudRefundFixedFee; 
            }

            SetFraud(isFraud, fee);
        }

        public void SetFraud(bool isFraud, decimal fee)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(UserRole.Admin, SecuredObject, PermissionValue.Edit);

            // already fraud
            if (isFraud && IsFraud)
                throw new Exception("Already fraud");

            // already not fraud
            if (!isFraud && !IsFraud)
                throw new Exception("Already not fraud");

            // check fee 
            var feeTrx = from t in DataContext.Writer.tblCompanyTransPasses where t.OriginalTransID == ID && t.TransSource_id == (short)Infrastructure.TransactionSource.FraudFee select t;
            if (feeTrx.Count() > 1)
                throw new Exception("Transaction has multiple fraud fees");
            if (feeTrx.Count() == 1 && feeTrx.Single().PayID != ";0;")
                throw new Exception("Transaction fraud fee is settled");

            IsFraud = isFraud;
            string userName = "Unknown";
            if (ObjectContext.Current.User != null)
                userName = ObjectContext.Current.User.UserName;
            var isFraudBit = isFraud ? 1 : 0;
            DataContext.Writer.ExecuteCommand($"UPDATE [dbo].[tblCompanyTransPass] SET [IsFraudByAcquirer] = {isFraudBit} WHERE ID = {ID}");
            //Save(); will save new trx and override ID
            // todo enable update in trx save function

            if (isFraud)
            {
                History.Create(this, CommonTypes.TransactionHistoryType.FraudSet, "User: " + userName);
                Risk.RiskItem.BlockCardFromTransaction(ID, TransactionStatus.Captured);

                if (fee > 0)
                {
                    var newFeeTrx = new Transaction(TransactionStatus.Captured);
                    newFeeTrx.MerchantID = MerchantID;
                    newFeeTrx.TransType = TransactionType.Capture;
                    newFeeTrx.CreditType = CreditType.Refund;
                    newFeeTrx.InsertDate = DateTime.Now;
                    newFeeTrx.TransactionSource = Infrastructure.TransactionSource.FraudFee;
                    newFeeTrx.PaymentMethodID = PaymentMethodEnum.SystemFees;
                    newFeeTrx.PaymentMethodDisplay = $"Fraud fee for transaction {ID}";
                    newFeeTrx.DebitCompanyID = null;
                    newFeeTrx.OriginalTransactionID = ID;
                    newFeeTrx.PaymentsIDs = ";0;";
                    newFeeTrx.Amount = fee;
                    newFeeTrx.Currency = Currency;
                    newFeeTrx.Installments = 1;
                    newFeeTrx.IsTest = IsTest;
                    newFeeTrx.IP = "";
                    newFeeTrx.OrderNumber = "";
                    newFeeTrx.TerminalNumber = "";
                    newFeeTrx.DebitReferenceCode = "";
                    newFeeTrx.Save();
                }

                PendingEvents.Create(PendingEventType.EmailMerchantFraud, ID, TransactionStatus.Captured, null);
            }
            else
            {
                History.Create(this, CommonTypes.TransactionHistoryType.FraudUnset, "User: " + userName);
                Risk.RiskItem.UnblockCardFromTransaction(ID, TransactionStatus.Captured);
                DataContext.Writer.tblCompanyTransPasses.DeleteAllOnSubmit(feeTrx);
            } 
        }

        /////for test/////
        public static List<KeyValuePair<int, int>> FindDuplicate()
        {            
            var maybeduplicate = (from t in DataContext.Reader.tblCompanyTransPasses
                                  where (t.CreditType != 0 && t.DeniedStatus >= 10)
                                  select t.ID);

            var duplicate = (from t in DataContext.Reader.tblCompanyTransPasses
                             where ((maybeduplicate.Contains(t.OriginalTransID)) && (t.DeniedStatus != 0))
                             select new KeyValuePair<int, int>(t.ID, t.OriginalTransID)).ToList();

            var duplicatedDuplicate = duplicate.GroupBy(x => x.Value).Where(x => x.Count() > 1).ToList();

            return duplicate; // inside duplicate there are the id's that are duplicated transactions.
            //var duplicateValues = plants.GroupBy(x => x.Value).Where(x => x.Count() > 1);
        }
        /////for test /////
    }
}
