﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Transactions.Recurring
{
	public class ChargeHistory : BaseDataObject
	{
		public class SearchFilters
		{
			public Range<int?> ID;
			public Range<DateTime?> Date;
			public CommonTypes.ProcessTransactionSource? TransSource;
			public int? TransID;
			public int? ChargeID;
			public string ReplyCode;
		}

		protected Netpay.Dal.Netpay.tblRecurringAttempt _entity;
		public int ID { get { return _entity.ID; } }
		public DateTime Date { get { return _entity.ra_Date; } }

        public string Comment { get { return _entity.ra_Comments; } }

        //private int? ChargeID { get { return _entity.ra_Charge; } }
        //private int? CreditCardID { get { return _entity.ra_CreditCard; } }
        //private int? ECheckID { get { return _entity.ra_ECheck; } }
        private int? TransFailID { get { return _entity.ra_TransFail; } }
        private int? TransPassID { get { return _entity.ra_TransPass; } }
        private int? TransPendingID { get { return _entity.ra_TransPending; } }
        private int? TransApprovalID { get { return _entity.ra_TransApproval; } }

        public int? ChargeId { get { return _entity.ra_Charge; } }
		//public int? SeriesId { get { return _entity.seria; } }
		private Recurring.Charge _charge;
		public Recurring.Charge Charge 
		{ 
			get{
				if (_charge != null) return _charge;
				if (ChargeId == null) return null;
				_charge = Charge.Load(ChargeId.Value);
				return _charge;
			}
		}
		public Transaction Transaction { get; set; }
		private string ReplyCode { get { return _entity.ra_ReplyCode; } }
		private string Comments { get { return _entity.ra_Comments; } }


        public Infrastructure.TransactionStatus? TransactionStatus
        {
            get
            {
                // Assume that only one of TransFailID/TransPassID/TransPendingID/TransApprovalID is not null
                if (TransFailID.HasValue)
                    return Infrastructure.TransactionStatus.Declined;
                else if (TransPassID.HasValue)
                    return Infrastructure.TransactionStatus.Captured;
                else if (TransPendingID.HasValue)
                    return Infrastructure.TransactionStatus.Pending;
                else if (TransApprovalID.HasValue)
                    return Infrastructure.TransactionStatus.Authorized;

                return null;
            }
        }

        public int? TransactionId
        {
            get
            {
                // Assume that only one of TransFailID/TransPassID/TransPendingID/TransApprovalID is not null
                if (TransFailID.HasValue)
                    return TransFailID;
                else if (TransPassID.HasValue)
                    return TransPassID;
                else if (TransPendingID.HasValue)
                    return TransPendingID;
                else if (TransApprovalID.HasValue)
                    return TransApprovalID;

                return null;
            }
        }

        public ChargeHistory(Dal.Netpay.tblRecurringAttempt entity)
		{ 
			_entity = entity;
			//Charge = new Recurring.Charge(entity.tblRecurringCharge);
			if (entity.ra_TransPass != null)
				Transaction = Transaction.GetTransaction(null, entity.ra_TransPass.Value, Infrastructure.TransactionStatus.Captured);
			else if (entity.ra_TransFail != null)
				Transaction = Transaction.GetTransaction(null, entity.ra_TransFail.Value, Infrastructure.TransactionStatus.Declined);
			else if (entity.ra_TransApproval != null)
				Transaction = Transaction.GetTransaction(null, entity.ra_TransApproval.Value, Infrastructure.TransactionStatus.Authorized);
			else if (entity.ra_TransPending != null)
				Transaction = Transaction.GetTransaction(null, entity.ra_TransApproval.Value, Infrastructure.TransactionStatus.Pending);
		}
		
		public ChargeHistory Load(int id)
		{
			return Search(new SearchFilters() { ID = new Range<int?>(id) }, null).SingleOrDefault();
		}

		public static List<ChargeHistory> Search(SearchFilters filters, ISortAndPage sortAndPage)
		{
			var exp = (from c in DataContext.Reader.tblRecurringAttempts select c);
			if (filters.ID.From.HasValue) exp = exp.Where(c => c.ID >= filters.ID.From.Value);
			if (filters.ID.To.HasValue) exp = exp.Where(c => c.ID <= filters.ID.To.Value);
			if (filters.Date.From.HasValue) exp = exp.Where(c => c.ra_Date >= filters.Date.From.Value);
			if (filters.Date.To.HasValue) exp = exp.Where(c => c.ra_Date <= filters.Date.To.Value.AlignToEnd());
			if (filters.ChargeID.HasValue) exp = exp.Where(c => c.ra_Charge == filters.ChargeID.Value);

			return exp.ApplySortAndPage(sortAndPage).Select(c => new ChargeHistory(c)).ToList();
		}

	}
}
