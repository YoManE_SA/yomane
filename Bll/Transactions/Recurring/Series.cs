﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Bll.Accounts;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Transactions.Recurring
{
    public enum SeriesStatus
    {
        Paid = 1,
        Blocked = 2, // blocked by netpay
        Suspended = 3, // suspended by the merchant
        Active = 4,
        Deleted = 5
    }
    
    public class Series : Infrastructure.BaseDataObject
    {
        public class SearchFilters
        {
            public Infrastructure.Range<int?> ID;
            public Infrastructure.Range<DateTime?> Date;
            public Infrastructure.Range<decimal?> Amount;
            public Infrastructure.Range<int?> MerchantId;
            public CommonTypes.PaymentMethodEnum? PaymentMethodId;
            public CommonTypes.Currency? CurrencyId;
            public SeriesStatus? Status;
        }

        public const string SecuredObjectName = "Manage";

        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Transactions.Recurring.Module.Current, SecuredObjectName); } }


        private Dal.Netpay.tblRecurringSery _entity;

        public int ID { get { return _entity.ID; } }
        public int? MerchantId { get { return _entity.rs_Company; } }
        public int SeriesNumber { get { return _entity.rs_SeriesNumber; } }
        public int Charges { get { return _entity.rs_ChargeCount; } set { _entity.rs_ChargeCount = value; } }
        public Infrastructure.ScheduleInterval IntervalUnit { get { return (Infrastructure.ScheduleInterval)_entity.rs_IntervalUnit; } set { _entity.rs_IntervalUnit = (int)value; } }
        public int IntervalCount { get { return _entity.rs_IntervalLength; } set { _entity.rs_IntervalLength = value; } }
        public decimal Amount { get { return _entity.rs_ChargeAmount; } set { _entity.rs_ChargeAmount = value; } }
        public int? Currency { get { return _entity.rs_Currency; } set { _entity.rs_Currency = value; } }

        public System.DateTime StartDate { get { return _entity.rs_StartDate; } }
        public string Comments { get { return _entity.rs_Comments; } }
        public bool Suspended { get { return _entity.rs_Suspended; } }
        public bool Blocked { get { return _entity.rs_Blocked; } }
        public bool Paid { get { return _entity.rs_Paid; } }
        public bool Deleted { get { return _entity.rs_Deleted; } }
        public decimal Total { get { return _entity.rs_SeriesTotal; } }
        public int? ReqTrmCode { get { return _entity.rs_ReqTrmCode; } }
        public string IP { get { return _entity.rs_IP; } }
        public bool IsFlexible { get { return _entity.rs_Flexible; } }
        public bool IsPreAuthorized { get { return _entity.rs_Approval; } }
        public bool? IsFirstApproval { get { return _entity.rs_FirstApproval; } }
        public bool IsPrecreated { get { return _entity.rs_IsPrecreated.GetValueOrDefault(); } }
        public string Identifier { get { return _entity.rs_Identifier; } }

        private Transactions.Payment _payment;
        public Transactions.Payment Payment
        {
            get
            {
                if (_payment != null) return _payment;
                if (_entity.TransPaymentMethod_id == null) return null;
                _payment = Transactions.Payment.Load(new List<int>() { _entity.TransPaymentMethod_id.Value }).SingleOrDefault();
                return _payment;
            }
        }

        protected Merchants.Merchant _merchant;
        public Merchants.Merchant Merchant
        {
            get
            {
                if (_merchant != null) return _merchant;
                if (MerchantId != null) _merchant = Bll.Merchants.Merchant.Load(MerchantId.Value);
                return _merchant;
            }
            set
            {
                if (ID == 0) _merchant = value;
            }
        }

        //public int? CreditCard { get { return _entity.rs_CreditCard; } }
        //public int? ECheck { get { return _entity.rs_ECheck; } }

        private Series(Dal.Netpay.tblRecurringSery entity) { _entity = entity; }
        public Series() { _entity = new Dal.Netpay.tblRecurringSery(); }


        public string IntervalDisplayText
        {
            get
            {
                string txt = IntervalCount + " " + IntervalUnit;
                if (IntervalCount > 1)
                    txt += "s";

                return txt;
            }
        }

        public string RecurringString()
        {
            string intervalChar = "";
            switch (IntervalUnit)
            {
                case ScheduleInterval.Minute: intervalChar = "N"; break;
                case ScheduleInterval.Hour: intervalChar = "H"; break;
                case ScheduleInterval.Day: intervalChar = "D"; break;
                case ScheduleInterval.Week: intervalChar = "W"; break;
                case ScheduleInterval.Month: intervalChar = "M"; break;
                case ScheduleInterval.Year: intervalChar = "Y"; break;
                case ScheduleInterval.Quarter: intervalChar = "Q"; break;
            }

            string amountString = "";
            if (Amount > 0)
                amountString = "A" + Amount.ToString("0.00");

            return String.Format("{0}{1}{2}{3}", Charges, intervalChar, IntervalCount, amountString);
        }

        public static Series Parse(string seriesList)
        {
            int charges, intervalCount;
            Infrastructure.ScheduleInterval intervalUnit;
            decimal amount = 0;

            int intervalPos = seriesList.IndexOfAny(new char[] { 'N', 'H', 'D', 'W', 'M', 'Y', 'Q' });
            if (intervalPos == -1)
                throw new Exception("intervalUnit value not found in Series");

            if ((!int.TryParse(seriesList.Substring(0, intervalPos), out charges)) || (charges < 1))
                throw new Exception("invalid cherges value in Series");

            switch (seriesList.Substring(intervalPos, 1).ToUpper())
            {
                case "N": intervalUnit = ScheduleInterval.Minute; break;
                case "H": intervalUnit = ScheduleInterval.Hour; break;
                case "D": intervalUnit = ScheduleInterval.Day; break;
                case "W": intervalUnit = ScheduleInterval.Week; break;
                case "M": intervalUnit = ScheduleInterval.Month; break;
                case "Y": intervalUnit = ScheduleInterval.Year; break;
                case "Q": intervalUnit = ScheduleInterval.Quarter; break;
                default: throw new Exception("invalid intervalUnit in Series");
            }

            intervalPos++;
            int amountPos = seriesList.IndexOf('A', intervalPos);
            if (amountPos == -1)
            {
                if ((!int.TryParse(seriesList.Substring(intervalPos), out intervalCount)) || (intervalCount < 1))
                    throw new Exception("invalid intervalCount in Series");
            }
            else
            {
                if ((!int.TryParse(seriesList.Substring(intervalPos, amountPos - intervalPos), out intervalCount)) || (intervalCount < 1))
                    throw new Exception("invalid intervalCount in Series");
                if ((!decimal.TryParse(seriesList.Substring(amountPos + 1), out amount)) || (amount < 0m))
                    throw new Exception("invalid amount in Series");
            }
            var entity = new Dal.Netpay.tblRecurringSery() { rs_IntervalLength = intervalCount, rs_ChargeCount = charges, rs_ChargeAmount = amount, rs_IntervalUnit = (int)intervalUnit };
            return new Series(entity);
        }

        public static List<Series> ParseList(string seriesList)
        {
            var ret = new System.Collections.Generic.List<Series>();
            foreach (var r in seriesList.EmptyIfNull().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
            {
                ret.Add(Parse(r));
            }
            return ret;
        }

        private static void LoadSeriesReferences(List<Series> list, bool loadMerchant)
        {
            Dictionary<int, Bll.Merchants.Merchant> merchants = null;

            if (loadMerchant)
                merchants = Bll.Merchants.Merchant.Load(list.Where(t => t.MerchantId != null).Select(t => t.MerchantId.GetValueOrDefault()).Distinct().ToList()).ToDictionary(m => m.ID);

            foreach (var s in list)
            {
                Bll.Merchants.Merchant merchant = null;
                if ((s.MerchantId != null) && (merchants != null) && merchants.TryGetValue(s.MerchantId.Value, out merchant)) s._merchant = merchant;
            }
        }

        public static List<Series> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            return Search(filters, sortAndPage, false);
        }


        public static List<Series> Search(SearchFilters filters, ISortAndPage sortAndPage, bool loadMerchant = false)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);

            var expression = (IQueryable<Dal.Netpay.tblRecurringSery>)from s in DataContext.Reader.tblRecurringSeries.Where(AccountFilter.Current.QueryValidate(Accounts.AccountType.Merchant, (Dal.Netpay.tblRecurringSery z) => z.rs_Company))
                                                                      orderby s.rs_StartDate descending
                                                                      select s;

            if (filters != null)
            {
                if (filters.ID.From != null) expression = expression.Where(s => s.ID >= filters.ID.From.Value);
                if (filters.ID.To != null) expression = expression.Where(s => s.ID <= filters.ID.To.Value);
                if (filters.MerchantId.From.HasValue) expression = expression.Where(s => s.rs_Company >= filters.MerchantId.From.Value);
                if (filters.MerchantId.To.HasValue) expression = expression.Where(s => s.rs_Company <= filters.MerchantId.To.Value);
                if (filters.Date.From != null) expression = expression.Where(s => s.rs_StartDate >= filters.Date.From.Value.AlignToEnd());
                if (filters.Date.To != null) expression = expression.Where(s => s.rs_StartDate <= filters.Date.To.Value.AlignToEnd());
                if (filters.Amount.From != null) expression = expression.Where(s => s.rs_SeriesTotal >= filters.Amount.From);
                if (filters.Amount.To != null) expression = expression.Where(s => s.rs_SeriesTotal <= filters.Amount.To);
                if (filters.CurrencyId != null) expression = expression.Where(s => s.rs_Currency == (int)filters.CurrencyId.Value);
                if (filters.PaymentMethodId != null)
                {
                    if (filters.PaymentMethodId.Value == CommonTypes.PaymentMethodEnum.ECCheck)
                        expression = expression.Where(s => s.rs_ECheck != 0);
                    else
                    {
                        expression = expression.Where(s => s.tblCreditCard.ccTypeID + 20 == (int)filters.PaymentMethodId.Value);
                    }
                }
                if (filters.Status != null)
                {
                    switch (filters.Status.Value)
                    {
                        case SeriesStatus.Deleted:
                            expression = expression.Where(s => s.rs_Deleted);
                            break;
                        case SeriesStatus.Paid:
                            expression = expression.Where(s => s.rs_Paid && !s.rs_Deleted);
                            break;
                        case SeriesStatus.Blocked:
                            expression = expression.Where(s => s.rs_Blocked && !s.rs_Deleted);
                            break;
                        case SeriesStatus.Suspended:
                            expression = expression.Where(s => s.rs_Suspended && !s.rs_Deleted);
                            break;
                        case SeriesStatus.Active:
                            expression = expression.Where(s => !s.rs_Paid && !s.rs_Blocked && !s.rs_Suspended && !s.rs_Deleted);
                            break;
                        default:
                            expression = expression.Where(s => !s.rs_Deleted);
                            break;
                    }
                }
            }


            var items = expression.ApplySortAndPage(sortAndPage).Select(s => new Series(s)).ToList();
            LoadSeriesReferences(items, loadMerchant);
            return items;
        }

        public static Series Load(int id)
        {
            return Search(new SearchFilters() { ID = new Range<int?>(id) }, null, true).SingleOrDefault();
        }

        public void Delete()
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Delete);
            
            _entity.rs_Deleted = true;
            Save();
        }

        public void Suspend()
        {
            Context.IsUserOfType(new UserRole[] { UserRole.Merchant, UserRole.Admin });
            _entity.rs_Suspended = true;
            Save();
        }

        public void Resume()
        {
            Context.IsUserOfType(new UserRole[] { UserRole.Merchant, UserRole.Admin });
            _entity.rs_Suspended = false;
            Save();
        }

        public SeriesStatus Status
        {
            get
            {
                if (_entity.rs_Deleted) return SeriesStatus.Deleted;
                else if (_entity.rs_Paid) return SeriesStatus.Paid;
                else if (_entity.rs_Blocked) return SeriesStatus.Blocked;
                else if (_entity.rs_Suspended) return SeriesStatus.Suspended;
                return SeriesStatus.Active;
            }
        }

        private void Save()
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            DataContext.Writer.tblRecurringSeries.Update(_entity, (_entity.ID != 0));
            DataContext.Writer.SubmitChanges();
        }

        public static void UpdateSeriesTotal(int seriesID)
        {           
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Edit);

            //User user = SecurityManager.GetInternalUser(new UserRole[] { UserRole.MerchantPrimary, UserRole.MerchantLimited });
            var series = (from s in DataContext.Writer.tblRecurringSeries where s.rs_Company == Merchants.Merchant.Current.MerchantID && s.ID == seriesID select s).SingleOrDefault();
            if (series == null) throw new ApplicationException("Series not found.");

            decimal total = series.tblRecurringCharges.Sum(c => c.rc_Amount);
            series.rs_SeriesTotal = total;
            DataContext.Writer.SubmitChanges();
        }

        public static CreateSeriesResult CreateSeries(int transactionID, TransactionStatus status, int charges, int intervalCount, ScheduleInterval intervalUnit, decimal? amount, string ip, string comment, out int seriesID)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);
            seriesID = 0;

            // check status
            if (status != TransactionStatus.Captured && status != TransactionStatus.Authorized)
                return CreateSeriesResult.WrongTransactionStatus;
            bool isCaptured = status == TransactionStatus.Captured;

            // get transaction 
            var transaction = Transaction.GetTransaction(null, transactionID, status);
            if (transaction == null)
                return CreateSeriesResult.TransactionNotFound;

            // check transaction series
            if (transaction.RecurringSeriesID != null)
            {
                seriesID = transaction.RecurringSeriesID.Value;
                return CreateSeriesResult.TransactionSeriesAlreadyExists;
            }

            // get interval unit letter
            string intervalUnitLetter = "D";
            switch (intervalUnit)
            {
                case ScheduleInterval.Day:
                    intervalUnitLetter = "D";
                    break;
                case ScheduleInterval.Week:
                    intervalUnitLetter = "W";
                    break;
                case ScheduleInterval.Month:
                    intervalUnitLetter = "M";
                    break;
                case ScheduleInterval.Year:
                    intervalUnitLetter = "Y";
                    break;
                case ScheduleInterval.Quarter:
                    intervalUnitLetter = "Q";
                    break;
                default:
                    break;
            }

            // comment
            if (comment == null)
                comment = "";

            // build recurring string
            string recurringString = "";
            if (amount == null)
            {
                recurringString = string.Format("recurring1={0}{1}{2}", charges, intervalUnitLetter, intervalCount);
            }
            else
            {
                recurringString = string.Format("recurring1=1{0}{1}", intervalUnitLetter, intervalCount);
                recurringString += string.Format("&recurring2={0}{1}{2}A{3}", charges, intervalUnitLetter, intervalCount, amount.Value);
            }

            seriesID = DataContext.Writer.RecurringBatchCreateFlexibleSeriesEx(isCaptured, transactionID, recurringString, comment, null, ip, null, null, null).SingleOrDefault().Column1.GetValueOrDefault();

            /*
			 * Proc return values
			 * -1 invalid number of charges
			 * -2 invalid interval unit
			 * -3 invalid interval length
			 * -4 invalid transaction ID - no passed transaction found
			 * -5 merchant is not allowed to make recurring transactions
			 */

            if (seriesID <= 0)
                return (CreateSeriesResult)seriesID;
            else
                return CreateSeriesResult.Success;
        }


        public static int GetUnprocessedRecurringCharges()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var exp = (from c in DataContext.Reader.tblCompanies
                       join rs in DataContext.Reader.tblRecurringSeries on c.ID equals rs.rs_Company
                       join rc in DataContext.Reader.tblRecurringCharges on rs.ID equals rc.rc_Series
                       select new { c, rs, rc });

           exp = exp.Where(x => x.c.ActiveStatus >= 20 &&
                                 x.rc.rc_Date > DateTime.Now.AddMonths(-6) &&
                                 x.rc.rc_Suspended == false &&
                                 x.rc.rc_Blocked == false &&
                                 x.rc.rc_Paid == false &&
                                 x.rc.rc_Pending == false &&
                                 x.rs.rs_Suspended == false &&
                                 x.rs.rs_Blocked == false &&
                                 x.rs.rs_Paid == false &&
                                 x.rs.rs_Deleted == false &&
                                 ((TimeSpan)(DateTime.Now - x.rc.rc_Date)).Days > 0);

            return exp.Select(x => x.rc.ID).Count();
        }
    }
}
