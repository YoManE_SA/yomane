﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Transactions.Recurring
{
	public class MerchantSettings : BaseDataObject
	{       
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Bll.Merchants.ProcessSettings.SecuredObject; } }


        private bool isNew;
		private Dal.Netpay.tblMerchantRecurringSetting _entity;
		public int MerchantID { get { return _entity.MerchantID; } }
		public bool IsEnabled { get { return _entity.IsEnabled; } set { _entity.IsEnabled = value; } }
		public bool IsEnabledFromTransPass { get { return _entity.IsEnabledFromTransPass; } set { _entity.IsEnabledFromTransPass = value; } }
		public bool IsEnabledModify { get { return _entity.IsEnabledModify; } set { _entity.IsEnabledModify = value; } }
		public bool ForceMD5OnModify { get { return _entity.ForceMD5OnModify; } set { _entity.ForceMD5OnModify = value; } }
		public int MaxYears { get { return _entity.MaxYears; } set { _entity.MaxYears = value; } }
		public int MaxCharges { get { return _entity.MaxCharges; } set { _entity.MaxCharges = value; } }
		public int MaxStages { get { return _entity.MaxStages; } set { _entity.MaxStages = value; } }

		private MerchantSettings(Netpay.Dal.Netpay.tblMerchantRecurringSetting entity)
		{ 
			_entity = entity;
			isNew = false;
		}

		public MerchantSettings(int merchantId) 
		{
			_entity = new Dal.Netpay.tblMerchantRecurringSetting();
			_entity.MerchantID = merchantId;
			isNew = true;
		}

		public void Save()
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            DataContext.Writer.tblMerchantRecurringSettings.Update(_entity, !isNew);
			DataContext.Writer.SubmitChanges();
			isNew = false;
		}

		public static MerchantSettings Load(int merchantId)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var entity = (from lu in DataContext.Reader.tblMerchantRecurringSettings where lu.MerchantID == merchantId select lu).SingleOrDefault();
			if (entity == null) return null;
			return new MerchantSettings(entity);
		}

		public bool CanCreateFromExistingTransaction
		{
			get{
				return _entity.IsEnabled && _entity.IsEnabledFromTransPass;
			}
		}

		public static MerchantSettings Current
		{
			get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                if (Merchants.Merchant.Current == null) return null;
				var ret = Login.Current.Items.GetValue<MerchantSettings>("Recurring.MerchantSettings");
				if (ret == null) {
					ret = MerchantSettings.Load(Merchants.Merchant.Current.ID);
					if(ret == null) ret = new MerchantSettings(Merchants.Merchant.Current.ID);
					Login.Current.Items["Recurring.MerchantSettings"] = ret;
				}
				return ret;
			}
		}


	}
}
