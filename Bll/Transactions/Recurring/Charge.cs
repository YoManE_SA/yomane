﻿using System;
using System.Collections.Generic;
using System.Linq;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Security;
using Netpay.CommonTypes;
using Netpay.Bll.Accounts;

namespace Netpay.Bll.Transactions.Recurring
{
	public class Charge : BaseDataObject
	{
		public class SearchFilters
		{
			public Infrastructure.Range<int?> ID;
			public Infrastructure.Range<DateTime?> Date;
			public Infrastructure.Range<decimal?> Amount;
			public Infrastructure.Range<int?> MerchantId;
			public int? SeriesId;
			//public bool? IsDeleted;
			public CommonTypes.PaymentMethodEnum? PaymentMethodId;
			public CommonTypes.Currency? CurrencyId;
		}

		private Dal.Netpay.tblRecurringCharge _entity;

		public int ID { get { return _entity.ID; } }
		public int SeriesId { get { return _entity.rc_Series; } }
		public int ChargeNumber { get { return _entity.rc_ChargeNumber; } }
		public System.DateTime Date { get { return _entity.rc_Date; } }
		public int? CreditCard { get { return _entity.rc_CreditCard; } }
		public int? ECheck { get { return _entity.rc_ECheck; } }
		public string Comments { get { return _entity.rc_Comments; } }
		public bool Suspended { get { return _entity.rc_Suspended; } }
		public bool Blocked { get { return _entity.rc_Blocked; } }
		public bool Paid { get { return _entity.rc_Paid; } }
		public decimal Amount { get { return _entity.rc_Amount; } }
		public int? Currency { get { return _entity.rc_Currency; } }
		public int Attempts { get { return _entity.rc_Attempts; } }
		public bool Pending { get { return _entity.rc_Pending; } }
		
		private Series _series;
		public Series Series 
		{ 
			get { 
				if (_series != null) return _series;
				_series = Series.Load(_entity.rc_Series); 
				return _series;
			} 
		}

		private Transactions.Payment _payment;
		public Transactions.Payment Payment
		{
			get
			{
				if (_payment != null) return _payment;
				if (_entity.TransPaymentMethod_id == null) return null;
				_payment = Transactions.Payment.Load(new List<int>() { _entity.TransPaymentMethod_id.Value }).SingleOrDefault();
				return _payment;
			}
		}

        internal Charge(Dal.Netpay.tblRecurringCharge entity) { _entity = entity; }

		public void Suspend()
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant });
			_entity.rc_Suspended = true;
			DataContext.Writer.tblRecurringCharges.Update(_entity, true);
            DataContext.Writer.SubmitChanges();
        }

		public void Resume()
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant });
			_entity.rc_Suspended = false;
            DataContext.Writer.tblRecurringCharges.Update(_entity, true);
            DataContext.Writer.SubmitChanges();
        }

        public static Charge Load(int id)
		{
			return Search(new SearchFilters() { ID = new Infrastructure.Range<int?>(id) }, null).SingleOrDefault();
		}

		private static Predicate<Dal.Netpay.tblRecurringCharge> SecurityWhere(int value) 
		{
			return new Predicate<Dal.Netpay.tblRecurringCharge>(c => AccountFilter.Current.ObjectIDs[Accounts.AccountType.Merchant].Contains(value));
		}

		public static List<Charge> Search(SearchFilters filters, ISortAndPage sortAndPage)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant });
			var expression = (IQueryable<tblRecurringCharge>)from c in DataContext.Reader.tblRecurringCharges 
				join s in DataContext.Reader.tblRecurringSeries.Where(AccountFilter.Current.QueryValidate(Accounts.AccountType.Merchant, (Dal.Netpay.tblRecurringSery z) => z.rs_Company)) on c.rc_Series equals s.ID
				where !s.rs_Deleted
				orderby c.rc_ChargeNumber
				select c;
			if (filters != null)
			{
				//if (filters.IsDeleted.HasValue) expression = expression.Where(c => c.is == filters.ID.From);
				if (filters.ID.From != null) expression = expression.Where(c => c.ID >= filters.ID.From);
				if (filters.ID.To != null) expression = expression.Where(c => c.ID <= filters.ID.To);
				if (filters.SeriesId != null) expression = expression.Where(c => c.rc_Series == filters.SeriesId);
				if (filters.Date.From != null) expression = expression.Where(c => c.rc_Date >= filters.Date.From.Value);
				if (filters.Date.To != null) expression = expression.Where(c => c.rc_Date <= filters.Date.To.Value.AlignToEnd());
				if (filters.PaymentMethodId != null) expression = expression.Where(c => (c.tblCreditCard.ccTypeID + 20) == (int)filters.PaymentMethodId);					
			}
			return expression.ApplySortAndPage(sortAndPage).Select(c => new Charge(c)).ToList();
		}

		/*
		public static void UpdateCharge(int chargeID, RecurringChargeVO data, bool updateSubsequentCharges)
		{
			User user = SecurityManager.GetInternalUserForInvocation(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
			
			NetpayDataContext dc = new NetpayDataContext();
			tblRecurringCharge charge = (from c in dc.tblRecurringCharges where c.tblRecurringSery.rs_Company == user.ID && c.ID == chargeID select c).SingleOrDefault();
			if (charge == null)
				throw new ApplicationException("Charge not found.");
		}		 
		*/
	}
}
