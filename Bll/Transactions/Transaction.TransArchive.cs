﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Transactions
{
    public partial class Transaction
    {

        private static IQueryable<Dal.Netpay.tblCompanyTransFail> SearchExpressionTransDeclinedArchived(LoadOptions loadOptions , SearchFilters filters)
        {
            var dc = new Dal.DataAccess.NetpayDataContext(Domain.Current.SqlArchiveConnectionString);
            dc.ObjectTrackingEnabled = false;

            var expression = (IQueryable<Netpay.Dal.Netpay.tblCompanyTransFail>)from t in dc.tblCompanyTransFails orderby t.InsertDate descending select t;
            if (Login.Current.Role == UserRole.Partner)
            {
                if (filters.merchantIDs == null) filters.merchantIDs = new List<int>();
                if (filters.customersIDs == null) filters.customersIDs = new List<int>();
                expression = expression.Where(t => filters.merchantIDs.Contains(t.CompanyID.Value) || filters.customersIDs.Contains(t.CustomerID));
            }
            else
            {
                if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
                    expression = expression.Where(t => filters.merchantIDs.Contains(t.CompanyID.Value));
                if (filters.customersIDs != null && filters.customersIDs.Count > 0)
                    expression = expression.Where(t => filters.customersIDs.Contains(t.CustomerID));
            }

            if (filters.ID.From != null)
                expression = expression.Where(t => t.ID >= filters.ID.From.Value);
            if (filters.ID.To != null)
                expression = expression.Where(t => t.ID <= filters.ID.To.Value);

            //Specific Transaction ID
            if (filters.specificTransactionID.HasValue)
            {
                expression = expression.Where(t => t.ID == filters.specificTransactionID.Value);
            }

            if (!string.IsNullOrWhiteSpace(filters.debitTerminalNumber))
                expression = expression.Where(t => t.TerminalNumber == filters.debitTerminalNumber);
            if (filters.debitCompanyID != null)
                expression = expression.Where(t => t.DebitCompanyID == filters.debitCompanyID.Value);
            if (filters.date.From != null)
                expression = expression.Where(t => t.InsertDate >= filters.date.From.Value.MinTime());
            if (filters.date.To != null)
                expression = expression.Where(t => t.InsertDate <= filters.date.To.Value.MaxTime());
            if (filters.paymentMethodID != null)
                expression = expression.Where(t => t.PaymentMethod == filters.paymentMethodID.Value);
            if (filters.creditTypes != null && filters.creditTypes.Count() > 0)
                expression = expression.Where(t => filters.creditTypes.Contains(t.CreditType));
            if (filters.orderID != null)
                expression = expression.Where(t => t.OrderNumber.StartsWith(filters.orderID));
            if (filters.currencyID != null)
                expression = expression.Where(t => t.Currency == filters.currencyID);
            if (filters.amount.From != null)
                expression = expression.Where(t => t.Amount >= filters.amount.From);
            if (filters.amount.To != null)
                expression = expression.Where(t => t.Amount <= filters.amount.To);
            if (filters.replyCode != null && filters.replyCode != "")
                expression = expression.Where(t => t.replyCode == filters.replyCode);
            if (filters.paymentMethodsIds != null)
                expression = expression.Where(t => filters.paymentMethodsIds.Contains(t.PaymentMethod.Value));
            if (filters.declinedType != null)
            {
                if (filters.declinedType == 1)
                    expression = expression.Where(t => t.TransType == (int)FailedTransactionType.PreAuthorized);
                else
                    expression = expression.Where(t => t.TransType != (int)FailedTransactionType.PreAuthorized);
            }
            if (filters.PayID != null)
                expression = expression.Where(t => t.PayID == filters.PayID.Value);
            if (filters.isTest != null)
                expression = expression.Where(t => t.isTestOnly);

            if (filters.PayerFilters != null)
            {
                bool isEmpty;
                var sq = Payer.Search(dc, filters.PayerFilters, out isEmpty);
                if (!isEmpty) expression = expression.Where(t => sq.Contains(t.trans_TransPayerInfo));
            }
            if (filters.PaymentFilters != null)
            {
                bool isEmpty;
                var sq = Payment.Search(dc, filters.PaymentFilters, out isEmpty);
                if (!isEmpty) expression = expression.Where(t => sq.Contains(t.trans_TransPaymentMethod));
            }

            return expression;
        }

		private static List<Transaction> SearchDeclinedArchived(LoadOptions loadOptions, SearchFilters filters)
		{
            var expression = SearchExpressionTransDeclinedArchived(loadOptions, filters);

			List<Transaction> transactions = expression.ApplySortAndPage(loadOptions.sortAndPage).Select(t => new Transaction(t)).ToList<Transaction>();
			return transactions;
		}
    }
}
