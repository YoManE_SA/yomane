﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Linq;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Domains;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure.VO;
using Netpay.CommonTypes;

namespace Netpay.Bll
{
	public class TransactionShortInfo
	{
		public int TransID { get; set; }
		public DateTime TransDate { get; set; }
		public string MethodString { get; set; }
		public string MerchantName { get; set; }
		public string MerchantWebSite { get; set; }
		public string MerchantSupportPhone { get; set; }
		public string MerchantSupportEmail { get; set; }
	}
	/// <summary>
	/// Transactions related logic.
	/// </summary>
	public static class Transactions
	{
		/// <summary>
		/// Returns the amount balance
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="filters"></param>
		/// <returns></returns>
		public static decimal GetBalance(Guid credentialsToken, SearchFilters filters)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary, UserType.NetpayAdmin, UserType.NetpayUser, UserType.Partner });
			if (user.Type != UserType.NetpayAdmin)
				filters.merchantIDs = SecurityManager.FilterRequestedIDs(filters.merchantIDs, user.AllowedMerchantsIDs);

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var expression = from t in dc.tblCompanyTransPasses where t.CreditType == (byte)CreditType.Regular || t.CreditType == (byte)CreditType.Refund select t;
			if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
				expression = expression.Where(t => filters.merchantIDs.Contains(t.companyID));
			if (filters.paymentMethodID != null)
				expression = expression.Where(t => t.PaymentMethod == filters.paymentMethodID);
			if (filters.currencyID != null)
				expression = expression.Where(t => t.Currency == filters.currencyID);

			if (expression.Count() == 0)
				return 0;

			decimal result = expression.Sum(t => t.CreditType == (byte)CreditType.Regular ? t.Amount : t.Amount * -1);
			return result;
		}

		/// <summary>
		/// Returns a list of the currency ids used in the selected captured transaction.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="filters"></param>
		/// <returns></returns>
		public static List<int> GetUsedCurrencies(Guid credentialsToken, SearchFilters filters)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary, UserType.NetpayAdmin, UserType.NetpayUser, UserType.Partner });
			if (user.Type != UserType.NetpayAdmin)
				filters.merchantIDs = SecurityManager.FilterRequestedIDs(filters.merchantIDs, user.AllowedMerchantsIDs);

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var expression = from t in dc.tblCompanyTransPasses select t;
			if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
				expression = expression.Where(t => filters.merchantIDs.Contains(t.companyID));
			if (filters.paymentMethodID != null)
				expression = expression.Where(t => t.PaymentMethod == filters.paymentMethodID);

			return expression.GroupBy(t => t.Currency).Select(g => g.Key.Value).ToList();
		}

		public static List<TransactionVO> GetCustomerTransactions(Guid credentialsToken, string customerEmail, string customerName, int creditCardLast4Digits, PagingInfo pagingInfo)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary });

			customerEmail = customerEmail.ToSql();
			customerName = customerName.ToSql();

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			List<TransactionVO> results = new List<TransactionVO>();

            // captured 
			string query = "SELECT [transactions].[ID], [transactions].[companyID], [transactions].[TransSource_id], [transactions].[DebitCompanyID], [transactions].[CustomerID], [transactions].[FraudDetectionLog_id], [transactions].[OriginalTransID], [transactions].[PrimaryPayedID], [transactions].[PayID], [transactions].[InsertDate], [transactions].[Amount], [transactions].[Currency], [transactions].[Payments], [transactions].[CreditType], [transactions].[IPAddress], [transactions].[replyCode], [transactions].[OrderNumber], [transactions].[Interest], [transactions].[Comment], [transactions].[TerminalNumber], [transactions].[ApprovalNumber], [transactions].[DeniedDate], [transactions].[DeniedStatus], [transactions].[DeniedPrintDate], [transactions].[DeniedSendDate], [transactions].[DeniedAdminComment], [transactions].[PD], [transactions].[MerchantPD], [transactions].[PaymentMethod_id], [transactions].[PaymentMethodID], [transactions].[PaymentMethodDisplay], [transactions].[isTestOnly], [transactions].[referringUrl], [transactions].[payerIdUsed], [transactions].[netpayFee_transactionCharge], [transactions].[netpayFee_ratioCharge], [transactions].[DebitReferenceCode], [transactions].[OCurrency], [transactions].[OAmount], [transactions].[netpayFee_chbCharge], [transactions].[netpayFee_ClrfCharge], [transactions].[PaymentMethod], [transactions].[UnsettledAmount], [transactions].[UnsettledInstallments], [transactions].[IPCountry], [transactions].[HandlingFee], [transactions].[CTP_Status], [transactions].[DebitFee], [transactions].[BTFileName], [transactions].[RecurringSeries], [transactions].[RecurringChargeNumber], [transactions].[DeniedValDate], [transactions].[MerchantRealPD] ";
			query += "FROM [dbo].[tblCompanyTransPass] AS [transactions] ";
			query += "Left JOIN [dbo].[tblCheckDetails] AS [checkDetails] ON ";
			query += "(";
            query += "[checkDetails].[id] = [transactions].[CheckDetailsID] AND";
			query += string.Format("[checkDetails].[email] = N'{0}' AND [checkDetails].[accountName] = N'{1}'", customerEmail, customerName);
			query += ") ";
			query += "Left JOIN [dbo].[tblCreditCard] AS [creditcard] ON ";
			query += "(";
			query += "[creditcard].[ID] = [transactions].[PaymentMethodID] AND ";
			query += string.Format("[creditcard].[email] = N'{0}' AND [creditcard].[Member] = N'{1}' ", customerEmail, customerName);
			query += ") ";
			query += "WHERE ";
			query += string.Format("[transactions].[companyID] = {0} ", user.ID);
			query += "AND ";
			query += "((";
			query += string.Format("[checkDetails].[email] = N'{0}' AND [checkDetails].[accountName] = N'{1}' ", customerEmail, customerName);
			query += ") ";
			query += "OR ";
			query += "(";
			query += string.Format("[creditcard].[email] = N'{0}' AND [creditcard].[Member] = N'{1}' ", customerEmail, customerName);
			query += "AND ";
			query += string.Format("[creditcard].[CCard_Last4] = {0} ", creditCardLast4Digits);
			query += "))";
			query += "ORDER BY [transactions].[InsertDate] DESC";
			IEnumerable<TransactionVO> result = (IEnumerable<TransactionVO>)dc.ExecuteQuery<tblCompanyTransPass>(query, new object[0]).Select(t => new TransactionVO(user.Domain.Host, t));
			results.AddRange(result);

			// declined
			query = "SELECT [transactions].[ID],[transactions].[companyID],[transactions].[TransSource_id],[transactions].[DebitCompanyID],[transactions].[CustomerID],[transactions].[FraudDetectionLog_id],[transactions].[InsertDate],[transactions].[IPAddress],[transactions].[Amount],[transactions].[Currency],[transactions].[Payments],[transactions].[CreditType],[transactions].[replyCode],[transactions].[OrderNumber],[transactions].[Comment],[transactions].[TerminalNumber],[transactions].[TransType],[transactions].[PaymentMethod_id],[transactions].[PaymentMethodID],[transactions].[PaymentMethodDisplay],[transactions].[isTestOnly],[transactions].[referringUrl],[transactions].[payerIdUsed],[transactions].[DebitReferenceCode],[transactions].[netpayFee_transactionCharge],[transactions].[PayID],[transactions].[PaymentMethod],[transactions].[IPCountry],[transactions].[ctf_JumpIndex],[transactions].[AutoRefundStatus],[transactions].[AutoRefundDate],[transactions].[AutoRefundReply],[transactions].[DeclineCount],[transactions].[DeclineSourceCount],[transactions].[DeclineReplyCount] ";
			query += "FROM [dbo].[tblCompanyTransFail] AS [transactions] ";
			query += "Left JOIN [dbo].[tblCheckDetails] AS [checkDetails] ON ";
			query += "(";
            query += "[checkDetails].[id] = [transactions].[CheckDetailsID] AND";
			query += string.Format("[checkDetails].[email] = N'{0}' AND [checkDetails].[accountName] = N'{1}'", customerEmail, customerName);
			query += ") ";
			query += "Left JOIN [dbo].[tblCreditCard] AS [creditcard] ON ";
			query += "(";
			query += "[creditcard].[ID] = [transactions].[PaymentMethodID] AND ";
			query += string.Format("[creditcard].[email] = N'{0}' AND [creditcard].[Member] = N'{1}' ", customerEmail, customerName);
			query += ") ";
			query += "WHERE ";
			query += string.Format("[transactions].[companyID] = {0} ", user.ID);
			query += "AND ";
			query += "((";
			query += string.Format("[checkDetails].[email] = N'{0}' AND [checkDetails].[accountName] = N'{1}' ", customerEmail, customerName);
			query += ") ";
			query += "OR ";
			query += "(";
			query += string.Format("[creditcard].[email] = N'{0}' AND [creditcard].[Member] = N'{1}' ", customerEmail, customerName);
			query += "AND ";
			query += string.Format("[creditcard].[CCard_Last4] = {0} ", creditCardLast4Digits);
			query += "))";
			query += "ORDER BY [transactions].[InsertDate] DESC";

			result = (IEnumerable<TransactionVO>)dc.ExecuteQuery<tblCompanyTransFail>(query, new object[0]).Select(t => new TransactionVO(user.Domain.Host, t));
			results.AddRange(result);

			// authorized
			query = "SELECT [transactions].[ID],[transactions].[companyID],[transactions].[TransSource_id],[transactions].[DebitCompanyID],[transactions].[CustomerID],[transactions].[FraudDetectionLog_id],[transactions].[InsertDate],[transactions].[PayID],[transactions].[IPAddress],[transactions].[Amount],[transactions].[Currency],[transactions].[Payments],[transactions].[CreditType],[transactions].[replyCode],[transactions].[OrderNumber],[transactions].[Interest],[transactions].[Comment],[transactions].[TerminalNumber],[transactions].[approvalNumber],[transactions].[PaymentMethod_id],[transactions].[PaymentMethodID],[transactions].[PaymentMethodDisplay],[transactions].[TransAnswerID],[transactions].[referringUrl],[transactions].[DebitReferenceCode],[transactions].[netpayFee_transactionCharge],[transactions].[PaymentMethod],[transactions].[RecurringSeries],[transactions].[RecurringChargeNumber] ";
			query += "FROM [dbo].[tblCompanyTransApproval] AS [transactions] ";
			query += "Left JOIN [dbo].[tblCheckDetails] AS [checkDetails] ON ";
			query += "(";
            query += "[checkDetails].[id] = [transactions].[CheckDetailsID] AND";
			query += string.Format("[checkDetails].[email] = N'{0}' AND [checkDetails].[accountName] = N'{1}'", customerEmail, customerName);
			query += ") ";
			query += "Left JOIN [dbo].[tblCreditCard] AS [creditcard] ON ";
			query += "(";
			query += "[creditcard].[ID] = [transactions].[PaymentMethodID] AND ";
			query += string.Format("[creditcard].[email] = N'{0}' AND [creditcard].[Member] = N'{1}' ", customerEmail, customerName);
			query += ") ";
			query += "WHERE ";
			query += string.Format("[transactions].[companyID] = {0} ", user.ID);
			query += "AND ";
			query += "((";
			query += string.Format("[checkDetails].[email] = N'{0}' AND [checkDetails].[accountName] = N'{1}' ", customerEmail, customerName);
			query += ") ";
			query += "OR ";
			query += "(";
			query += string.Format("[creditcard].[email] = N'{0}' AND [creditcard].[Member] = N'{1}' ", customerEmail, customerName);
			query += "AND ";
			query += string.Format("[creditcard].[CCard_Last4] = {0} ", creditCardLast4Digits);
			query += "))";
			query += "ORDER BY [transactions].[InsertDate] DESC";

			result = (IEnumerable<TransactionVO>)dc.ExecuteQuery<tblCompanyTransApproval>(query, new object[0]).Select(t => new TransactionVO(user.Domain.Host, t));
			results.AddRange(result);

			// pending
			query = "SELECT [transactions].[companyTransPending_id],[transactions].[companyBatchFiles_id],[transactions].[transactionSource_id],[transactions].[company_id],[transactions].[CustomerID],[transactions].[DebitCompanyID],[transactions].[FraudDetectionLog_id],[transactions].[insertDate],[transactions].[TerminalNumber],[transactions].[PaymentMethodID],[transactions].[PaymentMethod],[transactions].[PaymentMethodDisplay],[transactions].[IPAddress],[transactions].[replyCode],[transactions].[Comment],[transactions].[trans_amount],[transactions].[trans_currency],[transactions].[trans_creditType],[transactions].[trans_payments],[transactions].[trans_originalID],[transactions].[trans_order],[transactions].[DebitReferenceCode],[transactions].[DebitApprovalNumber],[transactions].[Locked],[transactions].[trans_type],[transactions].[ID],[transactions].[payerIdUsed],[transactions].[OrderNumber],[transactions].[CompanyID] ";
			query += "FROM [dbo].[tblCompanyTransPending] AS [transactions] ";
			query += "Left JOIN [dbo].[tblCheckDetails] AS [checkDetails] ON ";
			query += "(";
            query += "[checkDetails].[id] = [transactions].[CheckDetailsID] AND";
			query += string.Format("[checkDetails].[email] = N'{0}' AND [checkDetails].[accountName] = N'{1}'", customerEmail, customerName);
			query += ") ";
			query += "Left JOIN [dbo].[tblCreditCard] AS [creditcard] ON ";
			query += "(";
			query += "[creditcard].[ID] = [transactions].[PaymentMethodID] AND ";
			query += string.Format("[creditcard].[email] = N'{0}' AND [creditcard].[Member] = N'{1}' ", customerEmail, customerName);
			query += ") ";
			query += "WHERE ";
			query += string.Format("[transactions].[companyID] = {0} ", user.ID);
			query += "AND ";
			query += "((";
			query += string.Format("[checkDetails].[email] = N'{0}' AND [checkDetails].[accountName] = N'{1}' ", customerEmail, customerName);
			query += ") ";
			query += "OR ";
			query += "(";
			query += string.Format("[creditcard].[email] = N'{0}' AND [creditcard].[Member] = N'{1}' ", customerEmail, customerName);
			query += "AND ";
			query += string.Format("[creditcard].[CCard_Last4] = {0} ", creditCardLast4Digits);
			query += "))";
			query += "ORDER BY [transactions].[InsertDate] DESC";

			result = (IEnumerable<TransactionVO>)dc.ExecuteQuery<tblCompanyTransPending>(query, new object[0]).Select(t => new TransactionVO(user.Domain.Host, t));
			results.AddRange(result);

			pagingInfo.TotalItems = results.Count();
			pagingInfo.DataFunction = (credToken, pi) => GetCustomerTransactions(credToken, customerEmail, customerName, creditCardLast4Digits, pi);
			if (pagingInfo.CountOnly) return null;

			return results.Skip(pagingInfo.Skip).Take(pagingInfo.Take).ToList();
		}

		public static List<TransactionVO> Search(Guid credentialsToken, SearchFilters filters, PagingInfo pagingInfo)
		{
			return Search(credentialsToken, filters, pagingInfo, false, false);
		}

		/// <summary>
		/// Provides unified transaction search.
		/// Filters provided must be supported by all transactions status searches.
		/// This method is not paged at the database level and will perform poorly, if not provided with the proper filters.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="filters"></param>
		/// <param name="pagingInfo"></param>
		/// <param name="loadPaymentMethod"></param>
		/// <param name="loadMerchant"></param>
		/// <returns></returns>
		public static List<TransactionVO> Search(Guid credentialsToken, SearchFilters filters, PagingInfo pagingInfo, bool loadPaymentMethod, bool loadMerchant)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited, UserType.NetpayUser, UserType.NetpayAdmin });
			if (user.Type != UserType.NetpayAdmin)
			{
				if (user.Type == UserType.Customer) 
				{
					filters.customersIDs = new List<int>();
					filters.customersIDs.Add(user.ID);
				}
				else
					filters.merchantIDs = SecurityManager.FilterRequestedIDs(filters.merchantIDs, user.AllowedMerchantsIDs);
			}

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);

			if (filters != null && filters.AssignedFilters.Count == 0)
				throw new ArgumentException("Must specify some filters.");

			List<TransactionVO> failed = Search(credentialsToken, TransactionStatus.Declined, filters, loadPaymentMethod, loadMerchant);
			List<TransactionVO> passed = Search(credentialsToken, TransactionStatus.Captured, filters, loadPaymentMethod, loadMerchant);
			List<TransactionVO> pending = Search(credentialsToken, TransactionStatus.Pending, filters, loadPaymentMethod, loadMerchant);
			List<TransactionVO> preAuthorized = Search(credentialsToken, TransactionStatus.Authorized, filters, loadPaymentMethod, loadMerchant);

			List<TransactionVO> unified = new List<TransactionVO>();
			unified.AddRange(failed);
			unified.AddRange(passed);
			unified.AddRange(pending);
			unified.AddRange(preAuthorized);

			unified = unified.OrderByDescending(t => t.InsertDate).ToList<TransactionVO>();
			IEnumerable<TransactionVO> result = null;
			if (pagingInfo == null)
			{
				result = unified;
			}
			else
			{
				pagingInfo.TotalItems = unified.Count;
				pagingInfo.DataFunction = (credToken, pi) => Search(credToken, filters, pi, loadPaymentMethod, loadMerchant);
				if (pagingInfo.CountOnly) return null;
				result = unified.Skip(pagingInfo.Skip).Take(pagingInfo.Take);
			}

			return result.ToList<TransactionVO>();

			/*
			string unionQuery = "SELECT [transactions].[ID], [transactions].[companyID], [transactions].[TransSource_id], [transactions].[DebitCompanyID], [transactions].[CustomerID], [transactions].[FraudDetectionLog_id], [transactions].[InsertDate], [transactions].[Amount], [transactions].[Currency], [transactions].[Payments], [transactions].[CreditType], [transactions].[replyCode], [transactions].[OrderNumber], [transactions].[Comment], [transactions].[TerminalNumber] FROM [dbo].[tblCompanyTransPass] AS [transactions] Left JOIN [dbo].[tblCheckDetails] AS [checkDetails] ON ([checkDetails].[id] = [transactions].[PaymentMethodID] AND[checkDetails].[email] = N'tamir@netpay-intl.com' AND [checkDetails].[accountName] LIKE N'tamir netpay%') Left JOIN [dbo].[tblCreditCard] AS [creditcard] ON ([creditcard].[ID] = [transactions].[PaymentMethodID] AND [creditcard].[email] = N'tamir@netpay-intl.com' AND [creditcard].[Member] LIKE N'tamir%' ) WHERE [transactions].[companyID] = 35 AND ([checkDetails].[email] = N'tamir@netpay-intl.com' AND [checkDetails].[accountName] LIKE N'tamir%' OR [creditcard].[email] = N'tamir@netpay-intl.com' AND [creditcard].[Member] LIKE N'tamir%' ) ";
					unionQuery += "Union All ";
					unionQuery += "SELECT [transactions].[ID], [transactions].[companyID], [transactions].[TransSource_id], [transactions].[DebitCompanyID], [transactions].[CustomerID], [transactions].[FraudDetectionLog_id], [transactions].[InsertDate], [transactions].[Amount], [transactions].[Currency], [transactions].[Payments], [transactions].[CreditType], [transactions].[replyCode], [transactions].[OrderNumber], [transactions].[Comment], [transactions].[TerminalNumber] FROM [dbo].[tblCompanyTransFail] AS [transactions] Left JOIN [dbo].[tblCheckDetails] AS [checkDetails] ON ([checkDetails].[id] = [transactions].[PaymentMethodID] AND[checkDetails].[email] = N'tamir@netpay-intl.com' AND [checkDetails].[accountName] LIKE N'tamir netpay%') Left JOIN [dbo].[tblCreditCard] AS [creditcard] ON ([creditcard].[ID] = [transactions].[PaymentMethodID] AND [creditcard].[email] = N'tamir@netpay-intl.com' AND [creditcard].[Member] LIKE N'tamir%' ) WHERE [transactions].[companyID] = 35 AND ([checkDetails].[email] = N'tamir@netpay-intl.com' AND [checkDetails].[accountName] LIKE N'tamir%' OR [creditcard].[email] = N'tamir@netpay-intl.com' AND [creditcard].[Member] LIKE N'tamir%' ) "; ;
			
			
			
			string countQuery = "";

			string pagedQuery = "SELECT * ";
			pagedQuery += "FROM ";
			pagedQuery += "( ";
				pagedQuery += "SELECT ROW_NUMBER() OVER (ORDER BY InsertDate) RowNumber, * ";
				pagedQuery += "FROM ";
				pagedQuery += "( ";
				pagedQuery = pagedQuery + unionQuery;
				pagedQuery += ") t1 ";
			pagedQuery += ") t2 ";
			pagedQuery += "WHERE ";
			pagedQuery += "RowNumber BETWEEN 9000 AND 9200 ";

					IEnumerable<TransactionVO> results = dc.ExecuteQuery<TransactionVO>(pagedQuery, new object[0]);

					pagingInfo.TotalItems = results.Count();
					return results.Skip(pagingInfo.Skip).Take(pagingInfo.Take).ToList();
			*/
		}

		/// <summary>
		/// Search transactions using the filters class.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="status">Chooses the transaction table.</param>
		/// <param name="filters"></param>
		/// <returns></returns>
		public static List<TransactionVO> Search(Guid credentialsToken, TransactionStatus status, SearchFilters filters)
		{
			PagingInfo pi = null;
			return Search(credentialsToken, status, filters, pi, null);
		}

		/// <summary>
		/// Search transactions using the filters class.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="status">Chooses the transaction table.</param>
		/// <param name="filters"></param>
		/// <param name="loadPaymentMethod"></param>
		/// <param name="loadMerchant"></param>
		/// <returns></returns>
		public static List<TransactionVO> Search(Guid credentialsToken, TransactionStatus status, SearchFilters filters, bool loadPaymentMethod, bool loadMerchant)
		{
			PagingInfo pi = null;
			return Search(credentialsToken, status, filters, pi, null, loadPaymentMethod, loadMerchant);
		}

		/// <summary>
		/// Search transactions using the filters class.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="status">Chooses the transaction table.</param>
		/// <param name="filters"></param>
		/// <param name="pagingInfo"></param>
		/// <returns></returns>
		public static List<TransactionVO> Search(Guid credentialsToken, TransactionStatus status, SearchFilters filters, PagingInfo pagingInfo)
		{
			return Search(credentialsToken, status, filters, pagingInfo, null, false, false);
		}

		/// <summary>
		/// Search transactions using the filters class.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="status">Chooses the transaction table.</param>
		/// <param name="filters"></param>
		/// <param name="pagingInfo"></param>
		/// <param name="sortingInfo"></param>
		/// <returns></returns>
		public static List<TransactionVO> Search(Guid credentialsToken, TransactionStatus status, SearchFilters filters, PagingInfo pagingInfo, SortingInfo sortingInfo)
		{
			return Search(credentialsToken, status, filters, pagingInfo, sortingInfo, false, false);
		}

		/// <summary>
		/// Search transactions using the filters class.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="status">Chooses the transaction table.</param>
		/// <param name="filters"></param>
		/// <param name="pagingInfo"></param>
		/// <param name="sortingInfo"></param>
		/// <param name="loadPaymentMethod"></param>
		/// <param name="loadMerchant"></param>
		/// <returns></returns>
		public static List<TransactionVO> Search(Guid credentialsToken, TransactionStatus status, SearchFilters filters, PagingInfo pagingInfo, SortingInfo sortingInfo, bool loadPaymentMethod, bool loadMerchant)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited, UserType.NetpayUser, UserType.NetpayAdmin, UserType.Partner, UserType.Customer });
			if (user.Type != UserType.NetpayAdmin)
			{
				if (user.Type == UserType.Customer)
				{
					filters.customersIDs = new List<int>();
					filters.customersIDs.Add(user.ID);
				}
				else
					filters.merchantIDs = SecurityManager.FilterRequestedIDs(filters.merchantIDs, user.AllowedMerchantsIDs);
			}

			List<TransactionVO> transactions = null;
			switch (status)
			{
				case TransactionStatus.Captured:
					transactions = SearchTransCaptured(user, filters, pagingInfo, sortingInfo, loadPaymentMethod, loadMerchant);
					break;
				case TransactionStatus.Declined:
					transactions = SearchTransDeclined(user.Domain, filters, pagingInfo, sortingInfo, loadPaymentMethod, loadMerchant);
					break;
				case TransactionStatus.Authorized:
					transactions = SearchTransPreAuthorized(user.Domain, filters, pagingInfo, sortingInfo, loadPaymentMethod, loadMerchant);
					break;
				case TransactionStatus.Pending:
					transactions = SearchTransPending(user.Domain, filters, pagingInfo, sortingInfo, loadPaymentMethod, loadMerchant);
					break;
				case TransactionStatus.DeclinedArchived:
					transactions = SearchDeclinedArchived(user.Domain, filters, pagingInfo, sortingInfo, loadPaymentMethod, loadMerchant);
					break;
				default:
					throw new ApplicationException("Invalid transaction status.");
			}

			return transactions;
		}

		public static List<TransactionVO> GetRefunds(Guid credentialsToken, int originalTransactionID)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited, UserType.NetpayUser, UserType.NetpayAdmin, UserType.Partner, UserType.Customer });
			SearchFilters refundFilters=new SearchFilters();
			refundFilters.originalTransactionID=originalTransactionID;
			PagingInfo refundPaging=new PagingInfo();
			refundPaging.CurrentPage = 1;
			refundPaging.PageSize = 10;
			SortingInfo refundSorting=new SortingInfo();
			List<TransactionVO> refunds=SearchTransCaptured(user, refundFilters, refundPaging, null, false, false);
			return refunds;
		}

		private static List<TransactionVO> SearchTransCaptured(User user, SearchFilters filters, PagingInfo pagingInfo, SortingInfo sortingInfo, bool loadPaymentMethod, bool loadMerchant)
		{
			Domain domain = user.Domain;
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			var expression = (IQueryable<tblCompanyTransPass>)from t in dc.tblCompanyTransPasses orderby t.InsertDate descending select t;

			// add filters
			if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
				expression = expression.Where(t => filters.merchantIDs.Contains(t.companyID));
			if (filters.customersIDs != null && filters.customersIDs.Count > 0)
				expression = expression.Where(t => filters.customersIDs.Contains(t.CustomerID));
			if (filters.isChargeback == null)
			{
				if (filters.deniedDateFrom != null) expression = expression.Where(t => t.DeniedDate >= filters.deniedDateFrom.Value.MinTime());
				if (filters.deniedDateTo != null) expression = expression.Where(t => t.DeniedDate <= filters.deniedDateTo.Value.MaxTime());
				if (filters.dateFrom != null) expression = expression.Where(t => t.InsertDate >= filters.dateFrom.Value.MinTime());
				if (filters.dateTo != null) expression = expression.Where(t => t.InsertDate <= filters.dateTo.Value.MaxTime());
			}
			else
			{
				if (filters.isChargeback.Value)
				{
					expression = expression.Where(t => t.IsChargeback.Value);
					if (filters.deniedDateFrom != null) expression = expression.Where(t => t.DeniedDate >= filters.deniedDateFrom.Value.MinTime());
					if (filters.deniedDateTo != null) expression = expression.Where(t => t.DeniedDate <= filters.deniedDateTo.Value.MaxTime());
					if (filters.dateFrom != null) expression = expression.Where(t => t.DeniedDate >= filters.dateFrom.Value.MinTime());
					if (filters.dateTo != null) expression = expression.Where(t => t.DeniedDate <= filters.dateTo.Value.MaxTime());
				}
				else
				{
					expression = expression.Where(t => !t.IsChargeback.Value);
					if (filters.dateFrom != null) expression = expression.Where(t => t.InsertDate >= filters.dateFrom.Value.MinTime());
					if (filters.dateTo != null) expression = expression.Where(t => t.InsertDate <= filters.dateTo.Value.MaxTime());
				}
			}
			if (filters.transactionID != null)
				expression = expression.Where(t => t.ID == filters.transactionID.Value);
            if (filters.debitCompanyID != null)
                expression = expression.Where(t => t.DebitCompanyID == filters.debitCompanyID.Value);
			if (filters.paymentMethodID != null)
				expression = expression.Where(t => t.PaymentMethod == filters.paymentMethodID.Value);
			if (filters.creditType != null)
				expression = expression.Where(t => t.CreditType == filters.creditType.Value);
			if (filters.orderID != null)
				expression = expression.Where(t => t.OrderNumber.StartsWith(filters.orderID));
			if (filters.currencyID != null)
				expression = expression.Where(t => t.Currency == filters.currencyID);
			if (filters.amountFrom != null)
				expression = expression.Where(t => t.Amount >= filters.amountFrom);
			if (filters.amountTo != null)
				expression = expression.Where(t => t.Amount <= filters.amountTo);
			if (filters.replyCode != null)
				expression = expression.Where(t => t.replyCode == filters.replyCode);
			if (filters.last4Digits != null)
				expression = expression.Where(t => t.tblCreditCard.CCard_Last4 == filters.last4Digits);
			if (filters.first6Digits != null)
				expression = expression.Where(t => t.tblCreditCard.CCard_First6 == filters.first6Digits);
			if (filters.phoneNumber != null)
				expression = expression.Where(t => t.tblCreditCard.phoneNumber == filters.phoneNumber || t.tblCheckDetail.PhoneNumber == filters.phoneNumber);
			if (filters.personalNumber != null)
				expression = expression.Where(t => t.tblCreditCard.PersonalNumber == filters.personalNumber.Trim() || t.tblCheckDetail.PersonalNumber == filters.personalNumber.Trim());
			if (filters.cardHolderName != null)
				expression = expression.Where(t => t.tblCreditCard.Member.StartsWith(filters.cardHolderName.Trim()) || t.tblCheckDetail.AccountName.StartsWith(filters.cardHolderName.Trim()));
			if (filters.cardHolderEmail != null)
				expression = expression.Where(t => t.tblCreditCard.email == filters.cardHolderEmail.Trim() || t.tblCheckDetail.Email == filters.cardHolderEmail.Trim());
			if (filters.paymentMethodsIds != null)
				expression = expression.Where(t => filters.paymentMethodsIds.Contains(t.PaymentMethod.Value));
			if (filters.deviceId != null)
				expression = expression.Where(t => t.MobileDevice_id == filters.deviceId);

			if (filters.paymentsStatus != null)
			{
				if (filters.paymentsStatus == PaymentsStatus.Archived.ToString())
				{
					expression = expression.Where(t => t.PayID.Contains(";X;"));
				}
				if (filters.paymentsStatus == PaymentsStatus.PartiallySettled.ToString())
				{
					expression = expression.Where(t => t.PayID.Length > 3 && t.PayID.Contains(";0;"));
				}
				if (filters.paymentsStatus == PaymentsStatus.Settled.ToString())
				{
					expression = expression.Where(t => !t.PayID.Contains(";0;"));
				}
				if (filters.paymentsStatus == PaymentsStatus.Unsettled.ToString())
				{
					expression = expression.Where(t => t.PayID.Replace(";", "") == "0");
				}
			}
			if (filters.cardHolderCity != null)
				expression = expression.Where(t => t.tblCreditCard.tblBillingAddress.city == filters.cardHolderCity.Trim() || t.tblCheckDetail.tblBillingAddress.city == filters.cardHolderCity.Trim());
			if (filters.cardHolderCountryID != null)
				expression = expression.Where(t => t.tblCreditCard.tblBillingAddress.countryId == filters.cardHolderCountryID || t.tblCheckDetail.tblBillingAddress.countryId == filters.cardHolderCountryID);
			if (filters.cardHolderStateID != null)
				expression = expression.Where(t => t.tblCreditCard.tblBillingAddress.stateId == filters.cardHolderStateID || t.tblCheckDetail.tblBillingAddress.stateId == filters.cardHolderStateID);
			if (filters.cardHolderZipcode != null)
				expression = expression.Where(t => t.tblCreditCard.tblBillingAddress.zipCode == filters.cardHolderZipcode.Trim() || t.tblCheckDetail.tblBillingAddress.zipCode == filters.cardHolderZipcode.Trim());
			if (filters.creditCard != null)
			{
				byte[] encryptedArray = null;
				Encryption.Encrypt(domain.Host, filters.creditCard, out encryptedArray);
				Binary encryptedBinary = new Binary(encryptedArray);
				expression = expression.Where(t => t.tblCreditCard.CCard_number256.Equals(encryptedBinary));
			}
			if (filters.expirationMonth != null)
				expression = expression.Where(t => t.tblCreditCard.ExpMM == filters.expirationMonth.Value.ToString("0#"));
			if (filters.expirationYear != null)
				expression = expression.Where(t => t.tblCreditCard.ExpYY == filters.expirationYear.Value.ToString("0#"));
			if (filters.deniedStatus != null)
				expression = expression.Where(t => filters.deniedStatus.Contains((DeniedStatus)t.DeniedStatus));
			if (filters.isTest != null)
				expression = expression.Where(t => t.isTestOnly);
			if (filters.deniedDateFrom != null)
				expression = expression.Where(t => t.DeniedDate >= filters.deniedDateFrom.Value.MinTime());
			if (filters.deniedDateTo != null)
				expression = expression.Where(t => t.DeniedDate <= filters.deniedDateTo.Value.MaxTime());
			if (filters.originalTransactionID.GetValueOrDefault(0) > 0)
				expression = expression.Where(t => t.OriginalTransID == filters.originalTransactionID.Value);
            if (filters.isPendingChargeback != null) 
            {
                if (!filters.isPendingChargeback.Value)
                    expression = expression.Where(t => t.IsPendingChargeback == filters.isPendingChargeback.Value || t.IsPendingChargeback == null);
                else
                    expression = expression.Where(t => t.IsPendingChargeback == filters.isPendingChargeback.Value);
            }
            
			// handle sorting if not null
			if (sortingInfo != null)
			{
				if (sortingInfo.Direction == SortDirection.Ascending)
					expression = expression.OrderBy<tblCompanyTransPass>(sortingInfo.MappedEntityProperty);
				else
					expression = expression.OrderByDescending<tblCompanyTransPass>(sortingInfo.MappedEntityProperty);
			}

			// handle paging if not null
			if (pagingInfo != null)
			{
				pagingInfo.TotalItems = expression.Count();
				pagingInfo.DataFunction = (credToken, pi) => SearchTransCaptured(user, filters, pi, sortingInfo, loadPaymentMethod, loadMerchant);
				if (pagingInfo.CountOnly)
					return null;
				expression = expression.Skip(pagingInfo.Skip).Take(pagingInfo.Take);
			}

			bool loadEpaImportLog = ((user.PayPercent < 100 && user.Type == UserType.MerchantPrimary) || user.Type == UserType.Partner);
			List<TransactionVO> transactions = null;
			try
			{
				// run query and return collection
				dc.SetupLogger();
				transactions = expression.Select(t => new TransactionVO(domain.Host, t, loadPaymentMethod, loadMerchant, loadEpaImportLog)).ToList<TransactionVO>();
			}
			catch (Exception ex)
			{
				Logger.Log(LogSeverity.Error, LogTag.DataAccess, ex.Message, dc.Logged);
				throw ex;
			}

			return transactions;
		}

		private static List<TransactionVO> SearchTransDeclined(Domain domain, SearchFilters filters, PagingInfo pagingInfo, SortingInfo sortingInfo, bool loadPaymentMethod, bool loadMerchant)
		{
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			var expression = (IQueryable<tblCompanyTransFail>)from t in dc.tblCompanyTransFails orderby t.InsertDate descending select t;

			// add filters
			if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
				expression = expression.Where(t => filters.merchantIDs.Contains(t.CompanyID.Value));
			if (filters.customersIDs != null && filters.customersIDs.Count > 0)
				expression = expression.Where(t => filters.customersIDs.Contains(t.CustomerID));
			if (filters.transactionID != null)
				expression = expression.Where(t => t.ID == filters.transactionID.Value);
            if (filters.debitCompanyID != null)
                expression = expression.Where(t => t.DebitCompanyID == filters.debitCompanyID.Value);
            if (filters.dateFrom != null)
				expression = expression.Where(t => t.InsertDate >= filters.dateFrom.Value.MinTime());
			if (filters.dateTo != null)
				expression = expression.Where(t => t.InsertDate <= filters.dateTo.Value.MaxTime());
			if (filters.paymentMethodID != null)
				expression = expression.Where(t => t.PaymentMethod == filters.paymentMethodID.Value);
			if (filters.creditType != null)
				expression = expression.Where(t => t.CreditType == filters.creditType.Value);
			if (filters.orderID != null)
				expression = expression.Where(t => t.OrderNumber.StartsWith(filters.orderID));
			if (filters.currencyID != null)
				expression = expression.Where(t => t.Currency == filters.currencyID);
			if (filters.amountFrom != null)
				expression = expression.Where(t => t.Amount >= filters.amountFrom);
			if (filters.amountTo != null)
				expression = expression.Where(t => t.Amount <= filters.amountTo);
			if (filters.replyCode != null && filters.replyCode != "")
				expression = expression.Where(t => t.replyCode == filters.replyCode);
			if (filters.last4Digits != null)
				expression = expression.Where(t => t.tblCreditCard.CCard_Last4 == filters.last4Digits);
			if (filters.first6Digits != null)
				expression = expression.Where(t => t.tblCreditCard.CCard_First6 == filters.first6Digits);
			if (filters.phoneNumber != null)
				expression = expression.Where(t => t.tblCreditCard.phoneNumber == filters.phoneNumber || t.tblCheckDetail.PhoneNumber == filters.phoneNumber);
			if (filters.personalNumber != null)
				expression = expression.Where(t => t.tblCreditCard.PersonalNumber == filters.personalNumber || t.tblCheckDetail.PersonalNumber == filters.personalNumber);
			if (filters.cardHolderName != null)
				expression = expression.Where(t => t.tblCreditCard.Member.StartsWith(filters.cardHolderName) || t.tblCheckDetail.AccountName.StartsWith(filters.cardHolderName));
			if (filters.cardHolderEmail != null)
				expression = expression.Where(t => t.tblCreditCard.email == filters.cardHolderEmail.Trim() || t.tblCheckDetail.Email == filters.cardHolderEmail.Trim());
			if (filters.paymentMethodsIds != null)
				expression = expression.Where(t => filters.paymentMethodsIds.Contains(t.PaymentMethod.Value));
			if (filters.declinedType != null)
			{
				if (filters.declinedType == 1)
					expression = expression.Where(t => t.TransType == (int)FailedTransactionType.PreAuthorized);
				else
					expression = expression.Where(t => t.TransType != (int)FailedTransactionType.PreAuthorized);
			}
			if (filters.cardHolderCity != null)
				expression = expression.Where(t => t.tblCreditCard.tblBillingAddress.city == filters.cardHolderCity.Trim() || t.tblCheckDetail.tblBillingAddress.city == filters.cardHolderCity.Trim());
			if (filters.cardHolderCountryID != null)
				expression = expression.Where(t => t.tblCreditCard.tblBillingAddress.countryId == filters.cardHolderCountryID || t.tblCheckDetail.tblBillingAddress.countryId == filters.cardHolderCountryID);
			if (filters.cardHolderStateID != null)
				expression = expression.Where(t => t.tblCreditCard.tblBillingAddress.stateId == filters.cardHolderStateID || t.tblCheckDetail.tblBillingAddress.stateId == filters.cardHolderStateID);
			if (filters.cardHolderZipcode != null)
				expression = expression.Where(t => t.tblCreditCard.tblBillingAddress.zipCode == filters.cardHolderZipcode.Trim() || t.tblCheckDetail.tblBillingAddress.zipCode == filters.cardHolderZipcode.Trim());
			if (filters.creditCard != null)
			{
				byte[] encryptedArray = null;
				Encryption.Encrypt(domain.Host, filters.creditCard, out encryptedArray);
				Binary encryptedBinary = new Binary(encryptedArray);
				expression = expression.Where(t => t.tblCreditCard.CCard_number256.Equals(encryptedBinary));
			}
			if (filters.expirationMonth != null)
				expression = expression.Where(t => t.tblCreditCard.ExpMM == filters.expirationMonth.Value.ToString("0#"));
			if (filters.expirationYear != null)
				expression = expression.Where(t => t.tblCreditCard.ExpYY == filters.expirationYear.Value.ToString("0#"));
			if (filters.deviceId != null)
				expression = expression.Where(t => t.MobileDevice_id == filters.deviceId);
			if (filters.isTest != null)
				expression = expression.Where(t => t.isTestOnly);

			// handle paging if not null
			if (pagingInfo != null)
			{
				pagingInfo.TotalItems = expression.Count();
				pagingInfo.DataFunction = (credToken, pi) => SearchTransDeclined(domain, filters, pi, sortingInfo, loadPaymentMethod, loadMerchant);
				if (pagingInfo.CountOnly)
					return null;
				expression = expression.Skip(pagingInfo.Skip).Take(pagingInfo.Take);
			}

			// get data 
			List<TransactionVO> transactions = expression.Select(t => new TransactionVO(domain.Host, t, loadPaymentMethod, loadMerchant)).ToList<TransactionVO>();
			return transactions;
		}

		private static List<TransactionVO> SearchTransPreAuthorized(Domain domain, SearchFilters filters, PagingInfo pagingInfo, SortingInfo sortingInfo, bool loadPaymentMethod, bool loadMerchant)
		{
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			dc.CommandTimeout = 120;

			var expression = (IQueryable<tblCompanyTransApproval>)from t in dc.tblCompanyTransApprovals orderby t.InsertDate descending select t;

			// add filters
			if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
				expression = expression.Where(t => filters.merchantIDs.Contains(t.CompanyID.Value));
			if (filters.customersIDs != null && filters.customersIDs.Count > 0)
				expression = expression.Where(t => filters.customersIDs.Contains(t.CustomerID));
			if (filters.transactionID != null)
				expression = expression.Where(t => t.ID == filters.transactionID.Value);
            if (filters.debitCompanyID != null)
                expression = expression.Where(t => t.DebitCompanyID == filters.debitCompanyID.Value);
            if (filters.dateFrom != null)
				expression = expression.Where(t => t.InsertDate >= filters.dateFrom.Value.MinTime());
			if (filters.dateTo != null)
				expression = expression.Where(t => t.InsertDate <= filters.dateTo.Value.MaxTime());
			if (filters.paymentMethodID != null)
				expression = expression.Where(t => t.PaymentMethod == filters.paymentMethodID.Value);
			if (filters.creditType != null)
				expression = expression.Where(t => t.CreditType == filters.creditType.Value);
			if (filters.orderID != null)
				expression = expression.Where(t => t.OrderNumber.StartsWith(filters.orderID));
			if (filters.currencyID != null)
				expression = expression.Where(t => t.Currency == filters.currencyID);
			if (filters.amountFrom != null)
				expression = expression.Where(t => t.Amount >= filters.amountFrom);
			if (filters.amountTo != null)
				expression = expression.Where(t => t.Amount <= filters.amountTo);
			if (filters.replyCode != null)
				expression = expression.Where(t => t.replyCode == filters.replyCode);
			if (filters.last4Digits != null)
				expression = expression.Where(t => t.tblCreditCard.CCard_Last4 == filters.last4Digits);
			if (filters.first6Digits != null)
				expression = expression.Where(t => t.tblCreditCard.CCard_First6 == filters.first6Digits);
			if (filters.phoneNumber != null)
				expression = expression.Where(t => t.tblCreditCard.phoneNumber == filters.phoneNumber || t.tblCheckDetail.PhoneNumber == filters.phoneNumber);
			if (filters.personalNumber != null)
				expression = expression.Where(t => t.tblCreditCard.PersonalNumber == filters.personalNumber || t.tblCheckDetail.PersonalNumber == filters.personalNumber);
			if (filters.cardHolderName != null)
				expression = expression.Where(t => t.tblCreditCard.Member.StartsWith(filters.cardHolderName) || t.tblCheckDetail.AccountName.StartsWith(filters.cardHolderName));
			if (filters.cardHolderEmail != null)
				expression = expression.Where(t => t.tblCreditCard.email == filters.cardHolderEmail.Trim() || t.tblCheckDetail.Email == filters.cardHolderEmail.Trim());
			if (filters.paymentMethodsIds != null)
				expression = expression.Where(t => filters.paymentMethodsIds.Contains(t.PaymentMethod.Value));
			if (filters.cardHolderCity != null)
				expression = expression.Where(t => t.tblCreditCard.tblBillingAddress.city == filters.cardHolderCity.Trim() || t.tblCheckDetail.tblBillingAddress.city == filters.cardHolderCity.Trim());
			if (filters.cardHolderCountryID != null)
				expression = expression.Where(t => t.tblCreditCard.tblBillingAddress.countryId == filters.cardHolderCountryID || t.tblCheckDetail.tblBillingAddress.countryId == filters.cardHolderCountryID);
			if (filters.cardHolderStateID != null)
				expression = expression.Where(t => t.tblCreditCard.tblBillingAddress.stateId == filters.cardHolderStateID || t.tblCheckDetail.tblBillingAddress.stateId == filters.cardHolderStateID);
			if (filters.cardHolderZipcode != null)
				expression = expression.Where(t => t.tblCreditCard.tblBillingAddress.zipCode == filters.cardHolderZipcode.Trim() || t.tblCheckDetail.tblBillingAddress.zipCode == filters.cardHolderZipcode.Trim());
			if (filters.deviceId != null)
				expression = expression.Where(t => t.MobileDevice_id == filters.deviceId);
			if (filters.creditCard != null)
			{
				byte[] encryptedArray = null;
				Encryption.Encrypt(domain.Host, filters.creditCard, out encryptedArray);
				Binary encryptedBinary = new Binary(encryptedArray);
				expression = expression.Where(t => t.tblCreditCard.CCard_number256.Equals(encryptedBinary));
			}
			if (filters.expirationMonth != null)
				expression = expression.Where(t => t.tblCreditCard.ExpMM == filters.expirationMonth.Value.ToString("0#"));
			if (filters.expirationYear != null)
				expression = expression.Where(t => t.tblCreditCard.ExpYY == filters.expirationYear.Value.ToString("0#"));

			// handle paging if not null
			if (pagingInfo != null)
			{
				pagingInfo.TotalItems = expression.Count();
				pagingInfo.DataFunction = (credToken, pi) => SearchTransPreAuthorized(domain, filters, pi, sortingInfo, loadPaymentMethod, loadMerchant);
				if (pagingInfo.CountOnly)
					return null;
				expression = expression.Skip(pagingInfo.Skip).Take(pagingInfo.Take);
			}

			// run query and return collection
			List<TransactionVO> transactions = expression.Select(t => new TransactionVO(domain.Host, t, loadPaymentMethod, loadMerchant)).ToList<TransactionVO>();
			return transactions;
		}

		private static List<TransactionVO> SearchTransPending(Domain domain, SearchFilters filters, PagingInfo pagingInfo, SortingInfo sortingInfo, bool loadPaymentMethod, bool loadMerchant)
		{
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			dc.CommandTimeout = 120;

			var expression = (IQueryable<tblCompanyTransPending>)from t in dc.tblCompanyTransPendings orderby t.insertDate descending select t;

			// add filters
			if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
				expression = expression.Where(t => filters.merchantIDs.Contains(t.CompanyID.GetValueOrDefault()));
			if (filters.customersIDs != null && filters.customersIDs.Count > 0)
				expression = expression.Where(t => filters.customersIDs.Contains(t.CustomerID));
			if (filters.transactionID != null)
				expression = expression.Where(t => t.companyTransPending_id == filters.transactionID.Value);
            if (filters.debitCompanyID != null)
                expression = expression.Where(t => t.DebitCompanyID == filters.debitCompanyID.Value);
            if (filters.dateFrom != null)
				expression = expression.Where(t => t.insertDate >= filters.dateFrom.Value.MinTime());
			if (filters.dateTo != null)
				expression = expression.Where(t => t.insertDate <= filters.dateTo.Value.MaxTime());
			if (filters.paymentMethodID != null)
				expression = expression.Where(t => t.PaymentMethod == filters.paymentMethodID.Value);
			if (filters.creditType != null)
				expression = expression.Where(t => t.trans_creditType == filters.creditType.Value);
			if (filters.orderID != null)
				expression = expression.Where(t => t.OrderNumber.StartsWith(filters.orderID));
			if (filters.currencyID != null)
				expression = expression.Where(t => t.trans_currency == filters.currencyID);
			if (filters.amountFrom != null)
				expression = expression.Where(t => t.trans_amount >= filters.amountFrom);
			if (filters.amountTo != null)
				expression = expression.Where(t => t.trans_amount <= filters.amountTo);
			if (filters.replyCode != null)
				expression = expression.Where(t => t.replyCode == filters.replyCode);
			if (filters.last4Digits != null)
				expression = expression.Where(t => t.tblCreditCard.CCard_Last4 == filters.last4Digits);
			if (filters.first6Digits != null)
				expression = expression.Where(t => t.tblCreditCard.CCard_First6 == filters.first6Digits);
			if (filters.phoneNumber != null)
				expression = expression.Where(t => t.tblCreditCard.phoneNumber == filters.phoneNumber || t.tblCheckDetail.PhoneNumber == filters.phoneNumber);
			if (filters.personalNumber != null)
				expression = expression.Where(t => t.tblCreditCard.PersonalNumber == filters.personalNumber || t.tblCheckDetail.PersonalNumber == filters.personalNumber);
			if (filters.cardHolderName != null)
				expression = expression.Where(t => t.tblCreditCard.Member.StartsWith(filters.cardHolderName) || t.tblCheckDetail.AccountName.StartsWith(filters.cardHolderName));
			if (filters.cardHolderEmail != null)
				expression = expression.Where(t => t.tblCreditCard.email == filters.cardHolderEmail.Trim() || t.tblCheckDetail.Email == filters.cardHolderEmail.Trim());
			if (filters.paymentMethodsIds != null)
				expression = expression.Where(t => filters.paymentMethodsIds.Contains(t.PaymentMethod.Value));
			if (filters.cardHolderCity != null)
				expression = expression.Where(t => t.tblCreditCard.tblBillingAddress.city == filters.cardHolderCity.Trim() || t.tblCheckDetail.tblBillingAddress.city == filters.cardHolderCity.Trim());
			if (filters.cardHolderCountryID != null)
				expression = expression.Where(t => t.tblCreditCard.tblBillingAddress.countryId == filters.cardHolderCountryID || t.tblCheckDetail.tblBillingAddress.countryId == filters.cardHolderCountryID);
			if (filters.cardHolderStateID != null)
				expression = expression.Where(t => t.tblCreditCard.tblBillingAddress.stateId == filters.cardHolderStateID || t.tblCheckDetail.tblBillingAddress.stateId == filters.cardHolderStateID);
			if (filters.cardHolderZipcode != null)
				expression = expression.Where(t => t.tblCreditCard.tblBillingAddress.zipCode == filters.cardHolderZipcode.Trim() || t.tblCheckDetail.tblBillingAddress.zipCode == filters.cardHolderZipcode.Trim());
			if (filters.deviceId != null)
				expression = expression.Where(t => t.MobileDevice_id == filters.deviceId);
			if (filters.creditCard != null)
			{
				byte[] encryptedArray = null;
				Encryption.Encrypt(domain.Host, filters.creditCard, out encryptedArray);
				Binary encryptedBinary = new Binary(encryptedArray);
				expression = expression.Where(t => t.tblCreditCard.CCard_number256.Equals(encryptedBinary));
			}
			if (filters.expirationMonth != null)
				expression = expression.Where(t => t.tblCreditCard.ExpMM == filters.expirationMonth.Value.ToString("0#"));
			if (filters.expirationYear != null)
				expression = expression.Where(t => t.tblCreditCard.ExpYY == filters.expirationYear.Value.ToString("0#"));

			// handle paging if not null
			if (pagingInfo != null)
			{
				pagingInfo.TotalItems = expression.Count();
				pagingInfo.DataFunction = (credToken, pi) => SearchTransPending(domain, filters, pi, sortingInfo, loadPaymentMethod, loadMerchant);
				if (pagingInfo.CountOnly)
					return null;
				expression = expression.Skip(pagingInfo.Skip).Take(pagingInfo.Take);
			}

			// run query and return collection
			List<TransactionVO> transactions = expression.Select(t => new TransactionVO(domain.Host, t, loadPaymentMethod, loadMerchant)).ToList<TransactionVO>();
			return transactions;
		}

		private static List<TransactionVO> SearchDeclinedArchived(Domain domain, SearchFilters filters, PagingInfo pagingInfo, SortingInfo sortingInfo, bool loadPaymentMethod, bool loadMerchant)
		{
			NetpayDataContext dc = new NetpayDataContext(domain.SqlArchiveConnectionString);
			var expression = (IQueryable<tblCompanyTransFail>)from t in dc.tblCompanyTransFails orderby t.InsertDate descending select t;

			// add filters
			if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
				expression = expression.Where(t => filters.merchantIDs.Contains(t.CompanyID.Value));
			if (filters.customersIDs != null && filters.customersIDs.Count > 0)
				expression = expression.Where(t => filters.customersIDs.Contains(t.CustomerID));
			if (filters.transactionID != null)
				expression = expression.Where(t => t.ID == filters.transactionID.Value);
            if (filters.debitCompanyID != null)
                expression = expression.Where(t => t.DebitCompanyID == filters.debitCompanyID.Value);
            if (filters.dateFrom != null)
				expression = expression.Where(t => t.InsertDate >= filters.dateFrom.Value.MinTime());
			if (filters.dateTo != null)
				expression = expression.Where(t => t.InsertDate <= filters.dateTo.Value.MaxTime());
			if (filters.paymentMethodID != null)
				expression = expression.Where(t => t.PaymentMethod == filters.paymentMethodID.Value);
			if (filters.creditType != null)
				expression = expression.Where(t => t.CreditType == filters.creditType.Value);
			if (filters.orderID != null)
				expression = expression.Where(t => t.OrderNumber.StartsWith(filters.orderID));
			if (filters.currencyID != null)
				expression = expression.Where(t => t.Currency == filters.currencyID);
			if (filters.amountFrom != null)
				expression = expression.Where(t => t.Amount >= filters.amountFrom);
			if (filters.amountTo != null)
				expression = expression.Where(t => t.Amount <= filters.amountTo);
			if (filters.replyCode != null && filters.replyCode != "")
				expression = expression.Where(t => t.replyCode == filters.replyCode);
			if (filters.paymentMethodsIds != null)
				expression = expression.Where(t => filters.paymentMethodsIds.Contains(t.PaymentMethod.Value));
			if (filters.declinedType != null)
			{
				if (filters.declinedType == 1)
					expression = expression.Where(t => t.TransType == (int)FailedTransactionType.PreAuthorized);
				else
					expression = expression.Where(t => t.TransType != (int)FailedTransactionType.PreAuthorized);
			}
			if (filters.isTest != null)
				expression = expression.Where(t => t.isTestOnly);

			// handle paging if not null
			if (pagingInfo != null)
			{
				pagingInfo.TotalItems = expression.Count();
				pagingInfo.DataFunction = (credToken, pi) => SearchDeclinedArchived(domain, filters, pi, sortingInfo, loadPaymentMethod, loadMerchant);
				if (pagingInfo.CountOnly)
					return null;
				expression = expression.Skip(pagingInfo.Skip).Take(pagingInfo.Take);
			}

			// get data 
			List<TransactionVO> transactions = expression.Select(t => new TransactionVO(domain.Host, t, loadPaymentMethod, loadMerchant)).ToList<TransactionVO>();
			return transactions;
		}
        
        public static TransactionVO GetTransaction(string domainHost, int merchantID, int transactionID, TransactionStatus status)
        {
            Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            
            TransactionVO transaction = null;
            switch (status)
            {
                case TransactionStatus.Captured:
                    var entityPass = (from t in dc.tblCompanyTransPasses where t.companyID == merchantID && t.ID == transactionID select t).SingleOrDefault();
                    if (entityPass != null)
                        transaction = new TransactionVO(domainHost, entityPass, true, true, false);
                    break;
                case TransactionStatus.Declined:
                    var entityFail = (from t in dc.tblCompanyTransFails where t.CompanyID == merchantID && t.ID == transactionID select t).SingleOrDefault();
                    if (entityFail != null)
                        transaction = new TransactionVO(domainHost, entityFail, true, true);
                    break;
                case TransactionStatus.Authorized:
                    var entityAuth = (from t in dc.tblCompanyTransApprovals where t.CompanyID == merchantID && t.ID == transactionID select t).SingleOrDefault();
                    if (entityAuth != null)
                        transaction = new TransactionVO(domainHost, entityAuth, true, true);
                    break;
                case TransactionStatus.Pending:
                    var entityPending = (from t in dc.tblCompanyTransPendings where t.CompanyID == merchantID && t.ID == transactionID select t).SingleOrDefault();
                    if (entityPending != null)
                        transaction = new TransactionVO(domainHost, entityPending, true, true);
                    break;
                case TransactionStatus.DeclinedArchived:
                    throw new NotImplementedException();
                default:
                    throw new ApplicationException("Invalid transaction status");
            }

            return transaction;
        }

        public static TransactionVO GetTransaction(string domainHost, int transactionID, TransactionStatus status)
        {
            Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);

            TransactionVO transaction = null;
            switch (status)
            {
                case TransactionStatus.Captured:
                    var entityPass = (from t in dc.tblCompanyTransPasses where t.ID == transactionID select t).SingleOrDefault();
                    if (entityPass != null)
                        transaction = new TransactionVO(domainHost, entityPass, true, true, false);
                    break;
                case TransactionStatus.Declined:
                    var entityFail = (from t in dc.tblCompanyTransFails where t.ID == transactionID select t).SingleOrDefault();
                    if (entityFail != null)
                        transaction = new TransactionVO(domainHost, entityFail, true, true);
                    break;
                case TransactionStatus.Authorized:
                    var entityAuth = (from t in dc.tblCompanyTransApprovals where t.ID == transactionID select t).SingleOrDefault();
                    if (entityAuth != null)
                        transaction = new TransactionVO(domainHost, entityAuth, true, true);
                    break;
                case TransactionStatus.Pending:
                    var entityPending = (from t in dc.tblCompanyTransPendings where t.ID == transactionID select t).SingleOrDefault();
                    if (entityPending != null)
                        transaction = new TransactionVO(domainHost, entityPending, true, true);
                    break;
                case TransactionStatus.DeclinedArchived:
                    throw new NotImplementedException();
                default:
                    throw new ApplicationException("Invalid transaction status");
            }

            return transaction;
        }

		public static TransactionVO GetTransaction(Guid credentialsToken, int transactionID, TransactionStatus status)
		{
			return GetTransaction(credentialsToken, transactionID, status, true, true);
		}

		public static TransactionVO GetTransaction(Guid credentialsToken, int transactionID, TransactionStatus status, bool loadPaymentMethod, bool loadMerchant)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser, UserType.MerchantPrimary, UserType.MerchantLimited, UserType.Customer });

			SearchFilters filters = new SearchFilters();
			filters.transactionID = transactionID;
			filters.transactionStatus = status;
			if (user.Type != UserType.NetpayAdmin)
				filters.merchantIDs = SecurityManager.FilterRequestedIDs(filters.merchantIDs, user.AllowedMerchantsIDs);

			TransactionVO transaction = Search(credentialsToken, status, filters, loadPaymentMethod, loadMerchant).SingleOrDefault();
			return transaction;
		}

		public static List<TransactionVO> GetSettlementTransactions(Guid credentialsToken, int payID, PagingInfo pagingInfo)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var expression = (IQueryable<tblCompanyTransPass>)from t in dc.tblCompanyTransPasses where t.companyID == user.ID && (t.PrimaryPayedID == payID || t.PayID.Contains(";" + payID.ToString() + ";")) orderby t.InsertDate descending select t;

			// handle paging if not null
			if (pagingInfo != null)
			{
				pagingInfo.TotalItems = expression.Count();
				pagingInfo.DataFunction = (credToken, pi) => GetSettlementTransactions(credToken, payID, pi);
				if (pagingInfo.CountOnly)
					return null;
				expression = expression.Skip(pagingInfo.Skip).Take(pagingInfo.Take);
			}

			List<TransactionVO> results = expression.Select(t => new TransactionVO(user.Domain.Host, t)).ToList<TransactionVO>();

            // remove installments not in this settlement
			foreach (TransactionVO currentTransaction in results)
				if (currentTransaction.IsInstallments)
					currentTransaction.InstallmentList.RemoveAll(i => i.SettlementID != payID);

            // duplicate denied status 4 (UnsettledBeenSettledAndDeducted)
            List<TransactionVO> tempDuplicates = new List<TransactionVO>();
            foreach (TransactionVO currentTransaction in results) 
            {
                if (currentTransaction.DeniedStatusID == (int)DeniedStatus.UnsettledBeenSettledAndDeducted) 
                {
                    TransactionVO duplicate = currentTransaction.DeepCopy();
                    duplicate.Amount = duplicate.Amount * -1;
                    tempDuplicates.Add(duplicate);
                }           
            }
            results.AddRange(tempDuplicates);

			return results;
		}

		public static bool PendingResult(string domain, int pendingID, out TransactionStatus status, out int transID)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domain).Sql1ConnectionString);
			tblLogPendingFinalize logPendingFinalize = ((IQueryable<tblLogPendingFinalize>)from t in dc.tblLogPendingFinalizes where t.PendingID == pendingID select t).SingleOrDefault();
			transID = pendingID; status = TransactionStatus.Pending;
			if (logPendingFinalize == null)
				return false;
			if (logPendingFinalize.TransFailID.GetValueOrDefault() != 0)
			{
				status = TransactionStatus.Declined;
				transID = logPendingFinalize.TransFailID.GetValueOrDefault();
				return true;
			}
			else if (logPendingFinalize.TransPassID.GetValueOrDefault() != 0)
			{
				status = TransactionStatus.Captured;
				transID = logPendingFinalize.TransPassID.GetValueOrDefault();
				return true;
			}

			return false;
		}

		public static CaptureResult CaptureTransaction(Guid credentialsToken, int transactionID, decimal amount, string comment, out int? captureTransID)
		{
			captureTransID = null;
			if (amount <= 0) return
				CaptureResult.InvalidAmount;
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			TransactionVO transaction = Transactions.GetTransaction(credentialsToken, transactionID, TransactionStatus.Authorized);
			if (transaction == null) return CaptureResult.TransactionNotFound;
			if (transaction.PassedTransactionID != null) return CaptureResult.AlreadyCaptured;
			if (transaction.Amount * (decimal)1.1 < amount) return CaptureResult.AmountTooLarge;

			// build request
			StringBuilder requestBuilder = new StringBuilder("TransType=2&TypeCredit=1");
			requestBuilder.AppendFormat("&CompanyNum={0}", transaction.Merchant.Number);
			requestBuilder.AppendFormat("&TransApprovalID={0}", transaction.ID);
			requestBuilder.AppendFormat("&Currency={0}", transaction.CurrencyID);
			requestBuilder.AppendFormat("&Order={0}", transaction.OrderNumber);
			requestBuilder.AppendFormat("&Amount={0}", amount);
			requestBuilder.AppendFormat("&Comment={0}", comment);

			// http request
			string httpReply;
			HttpStatusCode httpStatus = HttpClient.SendHttpRequest(user.Domain.ProcessUrl + "remote_charge2.aspx", out httpReply, requestBuilder.ToString());
			if (httpStatus != HttpStatusCode.OK)
				return CaptureResult.ConnectionError;

			// params
			NameValueCollection replyParams = HttpUtility.ParseQueryString(httpReply);
			string replyCode = replyParams["Reply"];
			string replyTransactionID = replyParams["TransID"];
			if (replyCode == null || replyTransactionID == null)
				return CaptureResult.BadReplyFormat;
			int parsedTransactionID;
			if (int.TryParse(replyTransactionID, out parsedTransactionID))
				captureTransID = parsedTransactionID;
			else
			{
				captureTransID = null;
				return CaptureResult.BadReplyFormat;
			}

			// reply
			switch (replyCode)
			{
				case "000": return CaptureResult.OK;
				case "001": return CaptureResult.AttemptPending;
				default: return CaptureResult.AttemptDeclined;
			}
		}

		/// <summary>
		///	Returns AutoCapture record status
		/// </summary>
		/// <returns>TRUE if captured, FALSE if declined, NULL if not attemtped yet</returns>
		public static bool? IsAutoCaptureOK(ref NetpayDataContext dc, int AuthTransactionID)
		{
			var expression = (IQueryable<tblAutoCapture>)from t in dc.tblAutoCaptures where t.AuthorizedTransactionID == AuthTransactionID select t;
			AutoCaptureVO ac = new AutoCaptureVO(expression.FirstOrDefault());
			if (ac.CaptureTransactionID != null) return true;
			if (ac.DeclineTransactionID != null) return false;
			return null;
		}

		/// <summary>
		///	Returns list of "chargeback" and "retrieval request" history items for the transaction
		/// </summary>
		/// <param name="credentialsToken">user information</param>
		/// <param name="transID">ID of captured transaction</param>
		/// <returns>List of TransactionHistoryChargebackVO</returns>
		public static List<TransactionHistoryChargebackVO> GetHistoryChargebacks(Guid credentialsToken, int transID)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary, UserType.NetpayAdmin, UserType.NetpayUser, UserType.Partner });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			return (from ch in dc.GetChargebackHistory(transID) select new TransactionHistoryChargebackVO(ch)).ToList<TransactionHistoryChargebackVO>();
		}

		public static Dictionary<int, string> GetChargebackReasons(Guid credentialsToken)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var results = from cr in dc.tblChargebackReasons select cr;
			return results.ToDictionary(kvp => kvp.ReasonCode, kvp => kvp.Title);
		}

		public static void CreateChargeback(Guid credentialsToken, int transactionId, int reasonCode, string comment) 
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			dc.CreateCHB(transactionId, DateTime.Now, comment, reasonCode);
		}

		public static void RenmoveChargeback(Guid credentialsToken, int transactionId)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);

			var trans = (from t in dc.tblCompanyTransPasses where t.ID == transactionId select t).SingleOrDefault();
			if (trans == null)
				return;

			bool isClearChargeback = false;
			if (trans.DeniedStatus == (int)DeniedStatus.UnsettledInVerification && trans.PrimaryPayedID <= 0) 
			{
				trans.DeniedStatus = (int)DeniedStatus.ValidTransaction;
				trans.netpayFee_chbCharge = 0;
				trans.netpayFee_ClrfCharge = 0;
				isClearChargeback = true;
			}
			else if ((trans.DeniedStatus == (int)DeniedStatus.UnsettledBeenSettledAndValid || trans.DeniedStatus == (int)DeniedStatus.SetFoundValidRefunded) && trans.PrimaryPayedID <= 0)
			{
				trans.DeniedStatus = (int)DeniedStatus.ValidTransaction;
				trans.netpayFee_chbCharge = 0;
				trans.netpayFee_ClrfCharge = 0;
				isClearChargeback = true;	
			}
			else if (trans.DeniedStatus == (int)DeniedStatus.DuplicateValid && trans.PrimaryPayedID <= 0) 
			{
 				var chbCount = (from t in dc.tblCompanyTransPasses where t.OriginalTransID == trans.OriginalTransID && t.DeniedStatus > 0 select t).Count();
				if (chbCount == 1) 
				{
					var originalTrans = (from t in dc.tblCompanyTransPasses where t.ID == trans.OriginalTransID select t).SingleOrDefault();
					if (originalTrans != null)
						originalTrans.DeniedStatus = (int)DeniedStatus.ValidTransaction;
				}
				dc.tblCompanyTransPasses.DeleteOnSubmit(trans);
				isClearChargeback = true;
			}
			else if (trans.DeniedStatus == (int)DeniedStatus.DuplicateDeducted && trans.PrimaryPayedID <= 0)
			{
			 	var chbCount = (from t in dc.tblCompanyTransPasses where t.OriginalTransID == trans.OriginalTransID && t.DeniedStatus > 0 select t).Count();
				if (chbCount == 1) 
				{
					var originalTrans = (from t in dc.tblCompanyTransPasses where t.ID == trans.OriginalTransID select t).SingleOrDefault();
					if (originalTrans != null)
						originalTrans.DeniedStatus = (int)DeniedStatus.ValidTransaction;
				}
				dc.tblCompanyTransPasses.DeleteOnSubmit(trans);
				isClearChargeback = true;
			}

			if (isClearChargeback) 
			{
				tblLogTransPass log = new tblLogTransPass();
				log.ltp_ActionType = 1;
				log.ltp_Transaction = trans.ID;
				log.ltp_User = user.ID;
				log.ltp_Username = user.Name;
				log.ltp_InsertDate = DateTime.Now;
				if (HttpContext.Current != null && HttpContext.Current.Request != null)
				{
					string ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
					log.ltp_IP = ip;
				}
				dc.tblLogTransPasses.InsertOnSubmit(log);

				var histories = trans.trans_TransHistories.Where(h => h.list_TransHistoryType.TransHistoryType_id == (int)TransactionHistoryType.Chargeback);
				dc.TransHistories.DeleteAllOnSubmit(histories);

				dc.SubmitChanges();
			}
		}

		public static void CreateRetrievalRequest(Guid credentialsToken, int transactionId, int reasonCode, string comment)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var result = dc.RequestPhotocopyCHB(transactionId, DateTime.Now, comment, reasonCode);
			if ((int)result.ReturnValue >= 0) 
			{
				TransactionVO trans = GetTransaction(credentialsToken, transactionId, TransactionStatus.Captured);
				TransactionHistoryVO history = new TransactionHistoryVO();
				history.TransactionID = trans.ID;
				history.TransactionStatus = trans.Status;
				history.Description = "added by admin";
				history.InsertDate = DateTime.Now;
				history.MerchantID = trans.MerchantID.Value;
				history.Type = TransactionHistoryType.RetrievalRequest;
				AddHistory(credentialsToken, transactionId, TransactionStatus.Captured, history);
			}
		}

		public static void RemoveRetrievalRequest(Guid credentialsToken, int transactionId)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);

			var trans = (from t in dc.tblCompanyTransPasses where t.ID == transactionId select t).SingleOrDefault();
			if (trans == null)
				return;

			var histories = trans.trans_TransHistories.Where(h => h.list_TransHistoryType.TransHistoryType_id == (int)TransactionHistoryType.RetrievalRequest);
			dc.TransHistories.DeleteAllOnSubmit(histories);

			dc.SubmitChanges();
		}

		public static void DeleteHistory(Guid credentialsToken, int historyID)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var history = (from th in dc.TransHistories where th.TransHistory_id == historyID select th).SingleOrDefault();
			if (history == null)
				return;

			dc.TransHistories.DeleteOnSubmit(history);
			dc.SubmitChanges();
		}

		public static void AddHistory(Guid credentialsToken, int transID, TransactionStatus status, TransactionHistoryVO vo)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);

			TransHistory entity = new TransHistory();
			switch (status)
			{
				case TransactionStatus.Captured:
					entity.TransPass_id = transID;
					break;
				case TransactionStatus.Declined:
					entity.TransFail_id = transID;
					break;
				case TransactionStatus.Authorized:
					entity.TransPreAuth_id = transID;
					break;
				case TransactionStatus.Pending:
					entity.TransPending_id = transID;
					break;
				default:
					return;
			}
			entity.TransHistoryType_id = (byte)vo.Type;
			entity.Description = vo.Description;
			entity.InsertDate = DateTime.Now;
			entity.Merchant_id = vo.MerchantID;
			entity.ReferenceNumber = vo.ReferenceNumber;

			dc.TransHistories.InsertOnSubmit(entity);
			dc.SubmitChanges();
		}

		public static List<TransactionHistoryVO> GetHistories(Guid credentialsToken, int transID, TransactionStatus status)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary, UserType.NetpayAdmin, UserType.NetpayUser, UserType.Partner });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var histories = from th in dc.TransHistories select th;
			switch (status)
			{
				case TransactionStatus.Captured:
					histories = histories.Where(th => th.TransPass_id == transID);
					break;
				case TransactionStatus.Declined:
					histories = histories.Where(th => th.TransFail_id == transID);
					break;
				case TransactionStatus.Authorized:
					histories = histories.Where(th => th.TransPreAuth_id == transID);
					break;
				case TransactionStatus.Pending:
					histories = histories.Where(th => th.TransPending_id == transID);
					break;
				default:
					return null;
			}

			var results = histories.Select(th => new TransactionHistoryVO(th)).ToList();
			return results;
		}

        public static void DeleteTestTransactions(Guid credentialsToken)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary });
            DeleteTestTransactions(credentialsToken, user.ID);
        }

        public static void DeleteTestTransactions(Guid credentialsToken, int merchantID)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser, UserType.MerchantPrimary });
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            var testTransactions = from t in dc.tblCompanyTransPasses where t.companyID == merchantID && t.isTestOnly && t.UnsettledInstallments > 0 select t;
            var testTransactionsIDs = testTransactions.Select(t => t.ID).ToArray();
            var refundRequests = from rr in dc.tblRefundAsks where testTransactionsIDs.Contains(rr.transID) select rr;

            dc.tblCompanyTransPasses.DeleteAllOnSubmit(testTransactions);
            dc.tblRefundAsks.DeleteAllOnSubmit(refundRequests);
            dc.SubmitChanges();
        }

		public static List<TransactionShortInfo> TransLookup(string domainHost, DateTime date, decimal amount, string last4cc)
		{
			var domain = DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			var list = (from t in dc.tblCompanyTransPasses 
				join cc in dc.tblCreditCards on t.CreditCardID equals cc.ID
				join c in dc.tblCompanies on t.companyID equals c.ID
				where !t.isTestOnly && 
					(t.InsertDate >= date.Date.AddDays(-1) && t.InsertDate <= date.Date.AddDays(1)) && 
					t.Amount == amount && cc.CCard_Last4 == last4cc.ToNullableShort().GetValueOrDefault()
				orderby t.InsertDate
				select new {t, c}).ToList();
			var ret = new List<TransactionShortInfo>();
			foreach (var r in list)
			{
				var item = new TransactionShortInfo();
				item.TransID = r.t.ID;
				item.TransDate = r.t.InsertDate;
				item.MethodString = r.t.PaymentMethodDisplay;
				item.MerchantName = r.c.CompanyName;
				item.MerchantWebSite = r.c.URL;
				item.MerchantSupportPhone = r.c.merchantSupportPhoneNum;
				item.MerchantSupportEmail = r.c.merchantSupportEmail;
				ret.Add(item);
			}
			return ret;
		}
	}
}