﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure;
using Netpay.Dal.Netpay;
using Netpay.Dal.DataAccess;
using System.Net;
using System.Net.Mail;
using Netpay.Infrastructure.Domains;
using Netpay.Infrastructure.Tasks;
using System.Xml.Linq;
using System.IO;
using Netpay.CommonTypes;
using System.Data.Linq;

namespace Netpay.Bll
{
	/// <summary>
	/// Payoneer Partner = Merchant
	/// Payoneer Payee = Customer
	/// </summary>
	public static class ExternalCards
	{
		public static ExternalCardCustomerVO GetCustomer(int customerID) 
		{
			User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			tblExternalCardCustomer entity = (from pc in dc.tblExternalCardCustomers where pc.Merchant_id == user.ID && pc.ExternalCardCustomer_id == customerID select pc).SingleOrDefault();
			if (entity == null)
				return null;

			return new ExternalCardCustomerVO(entity);		
		}
		
		public static ExternalCardCustomerVO GetCustomer(Guid uniqueID) 
		{
			User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			tblExternalCardCustomer entity = (from pc in dc.tblExternalCardCustomers where pc.Merchant_id == user.ID && pc.UniqueID == uniqueID select pc).SingleOrDefault();
			if (entity == null)
				return null;

			return new ExternalCardCustomerVO(entity);
		}

		public static ExternalCardCustomerPaymentVO GetPayment(int paymentID)
		{
			User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			ExternalCardCustomerPaymentVO payment = (from p in dc.tblExternalCardCustomerPayments where p.tblExternalCardCustomer.Merchant_id == user.ID && p.ExternalCardCustomerPayment_id == paymentID select new ExternalCardCustomerPaymentVO(p)).SingleOrDefault();

			return payment;
		}

		public static List<ExternalCardCustomerPaymentVO> GetPayments(int customerID, ISortAndPage sortAndPage)
		{
			User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
			ExternalCardCustomerVO customer = GetCustomer(customerID);
			if (customer == null)
				throw new ApplicationException("Customer not found");

			DataLoadOptions dlo = new DataLoadOptions();
			dlo.LoadWith<tblExternalCardCustomerPayment>(p => p.tblExternalCardCustomer);
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			dc.LoadOptions = dlo;
			var expression = (IQueryable<tblExternalCardCustomerPayment>)from pc in dc.tblExternalCardCustomerPayments where pc.ExternalCardCustomer_id == customer.ID orderby pc.ExternalCardCustomerPayment_id descending select pc;

			// handle paging if not null
			if (sortAndPage != null)
				sortAndPage.DataFunction = (credToken, pi) => GetPayments(customerID, sortAndPage);

			return expression.ApplySortAndPage(sortAndPage).Select(pc => new ExternalCardCustomerPaymentVO(pc)).ToList();
		}

		public static List<ExternalCardCustomerPaymentVO> GetPayments(SearchFilters filters, ISortAndPage sortAndPage)
		{
			User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
			DataLoadOptions dlo = new DataLoadOptions();
			dlo.LoadWith<tblExternalCardCustomerPayment>(p => p.tblExternalCardCustomer);
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			dc.LoadOptions = dlo;
			var expression = (IQueryable<tblExternalCardCustomerPayment>)from pc in dc.tblExternalCardCustomerPayments where pc.tblExternalCardCustomer.Merchant_id == user.ID orderby pc.ExternalCardCustomerPayment_id descending select pc;

			if (filters.dateFrom != null)
				expression = expression.Where(pc => pc.InsertDate >= filters.dateFrom.Value.MinTime());
			if (filters.dateTo != null)
				expression = expression.Where(pc => pc.InsertDate <= filters.dateTo.Value.MaxTime());
			if (filters.replyCode != null)
				expression = expression.Where(pc => pc.Result == filters.replyCode);
			if (filters.amountFrom != null)
				expression = expression.Where(pc => pc.Amount >= filters.amountFrom);
			if (filters.amountTo != null)
				expression = expression.Where(pc => pc.Amount <= filters.amountTo);
            if (filters.externalCardProvider != null)
                expression = expression.Where(pc => pc.tblExternalCardCustomer.tblExternalCardTerminal.ExternalCardProvider_id == (int)filters.externalCardProvider.Value);

			// handle paging if not null
			if (sortAndPage != null)
				sortAndPage.DataFunction = (credToken, pi) => GetPayments(filters, sortAndPage);

			List<ExternalCardCustomerPaymentVO> results = expression.ApplySortAndPage(sortAndPage).Select(pc => new ExternalCardCustomerPaymentVO(pc)).ToList();
			return results;
		}

		public static void UpdateCustomer(ExternalCardCustomerVO customer) 
		{
			User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			tblExternalCardCustomer entity = (from pc in dc.tblExternalCardCustomers where pc.Merchant_id == user.ID && pc.UniqueID == customer.UniqueID select pc).SingleOrDefault();
			if (entity == null)
				return;

			entity.Email = customer.Email;
			entity.Name = customer.Name;
			entity.Status = (short)customer.Status;
			dc.SubmitChanges();
		}

		public static List<ExternalCardCustomerVO> GetCustomers(SearchFilters filters, ISortAndPage sortAndPage)
		{
			User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var expression = from pc in dc.tblExternalCardCustomers where pc.Merchant_id == user.ID select pc;
			if (filters != null)
			{
				if (filters.CustomerStatus != null)
					expression = expression.Where(pc => pc.Status == (int)filters.CustomerStatus); 
				if (filters.comment != null)
					expression = expression.Where(pc => pc.Comment.StartsWith(filters.comment));
				if (filters.email != null)
					expression = expression.Where(pc => pc.Email == filters.email); 
                if (filters.externalCardProvider != null)
                    expression = expression.Where(pc => pc.tblExternalCardTerminal.ExternalCardProvider_id == (byte)filters.externalCardProvider.Value); 
			}

			if (sortAndPage != null)
				sortAndPage.DataFunction = (credToken, pi) => GetCustomers(filters, sortAndPage);
			List<ExternalCardCustomerVO> result = expression.ApplySortAndPage(sortAndPage).Select(pc => new ExternalCardCustomerVO(pc)).ToList();
			return result;
		}

        public static bool IsProviderAvailable(ExternalCardProviderEnum provider) 
        {
            return GetTerminal(provider) != null;
        }

        private static tblExternalCardTerminal GetTerminal(ExternalCardProviderEnum provider) 
        {
            User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            var terminal = (from terminalToMerchant in dc.tblExternalCardTerminalToMerchants where terminalToMerchant.Merchant_id == user.ID && terminalToMerchant.IsActive && terminalToMerchant.tblExternalCardTerminal.ExternalCardProvider_id == (int)provider select terminalToMerchant.tblExternalCardTerminal).SingleOrDefault();

            return terminal;
        }

        public static string AddLivekashCustomer(string email, string mobileNumber, string firstName, string LastName, DateTime birthDate, string address1, string address2, string city, string state, string country, string zipCode, Gender gender, string comment)
        {
            User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);

            // check customer
            var found = from c in dc.tblExternalCardCustomers
                        where c.Email.Trim().ToLower() == email.Trim().ToLower() && c.Merchant_id == user.ID && c.tblExternalCardTerminal.ExternalCardProvider_id == (int)ExternalCardProviderEnum.LiveKash
                                                select c;
            if (found.Count() > 0)
                return "Duplicate Customer";

            // get terminal
            tblExternalCardTerminal terminal = GetTerminal(ExternalCardProviderEnum.LiveKash);
            if (terminal == null)
                return "Terminal not found";

            // api request
            Guid refID = Guid.NewGuid();
            XElement requestXml = new XElement("VcardRequest");
            requestXml.Add(new XElement("Merchant_Reference", refID));
            requestXml.Add(new XElement("C_IP_address", "80.179.180.59"));
            requestXml.Add(new XElement("Vendor_Request", "CREATE"));
            requestXml.Add(new XElement("Vendor_Id", terminal.AuthVar1));
            requestXml.Add(new XElement("Card_Issue", "LIVEKASH"));
            requestXml.Add(new XElement("Terminal_Pass", Encryption.Decrypt(terminal.AuthPassword256.ToArray())));
            requestXml.Add(new XElement("Terminal_Id", terminal.AuthUsername));
            requestXml.Add(new XElement("Last_Name", LastName.ToEncodedHtml()));
            requestXml.Add(new XElement("First_Name", firstName.ToEncodedHtml()));
            requestXml.Add(new XElement("Date_birth", birthDate.ToString("yyyy-MM-dd")));
            requestXml.Add(new XElement("Mobile_Number", mobileNumber.ToEncodedHtml()));
            requestXml.Add(new XElement("Email", email.ToEncodedHtml()));
            requestXml.Add(new XElement("Currency_Request", "USD"));
            requestXml.Add(new XElement("date_from", DateTime.Now.ToString("yyyy-MM-dd")));
            requestXml.Add(new XElement("date_to", DateTime.Now.AddYears(2).ToString("yyyy-MM-dd")));
            requestXml.Add(new XElement("Address_1", address1.ToEncodedHtml()));
            requestXml.Add(new XElement("Address_2", address2.ToEncodedHtml()));
            requestXml.Add(new XElement("City", city.ToEncodedHtml()));
            requestXml.Add(new XElement("State", state.ToEncodedHtml()));
            requestXml.Add(new XElement("Country", country.ToEncodedHtml()));
            requestXml.Add(new XElement("Merchant_date", DateTime.Now.ToString("yyyy-MM-dd")));
            requestXml.Add(new XElement("Merchant_Time", DateTime.Now.ToString("HH:mm:ss"))); 
            requestXml.Add(new XElement("Zip_Code", zipCode.ToEncodedHtml()));
            requestXml.Add(new XElement("Gender", gender == Gender.Female ? "0" : "1"));

            // api call
            string url = null; ;
            if (Configuration.IsProduction)
                url = "https://www.lpg24.net/api/LPG_Card_Creation.asp";
            else
                url = "https://www.lpg24.net/test-api/LPG_Card_Creation.asp";
            WebClient webClient = new WebClient();
            string response = webClient.UploadString(url, "post", requestXml.ToString());
            if (!response.Contains("<?xml")) 
            {
                Logger.Log(LogSeverity.Error, LogTag.ExternalCards, "LiveKash create card failed, see more info", response);
                return "LiveKash server has returned an error, see logs for more info.";
            }

            XElement responseXml = XElement.Parse(response);
            string result = responseXml.Elements("Status").First().Value;
            
            if (result == "ERROR")
            {
                string reason = responseXml.Elements("Fail_Reason").First().Value;
                return reason;
            }
            else 
            {
                XElement extraData = new XElement("ExtraData");
                extraData.Add(requestXml);
                extraData.Add(responseXml);

                // encrypt sensitive data
                EncryptNode(user.Domain.Host, extraData.Element("VcardRequest").Element("Terminal_Pass"));
                EncryptNode(user.Domain.Host, extraData.Element("VcardRespond").Element("Vcard"));
                EncryptNode(user.Domain.Host, extraData.Element("VcardRespond").Element("cvv2"));
                EncryptNode(user.Domain.Host, extraData.Element("VcardRespond").Element("pin_code"));

                // persist
                tblExternalCardCustomer entity = new tblExternalCardCustomer();
                entity.ExternalCardTerminal_id = terminal.ExternalCardTerminal_id;
                entity.Merchant_id = user.ID;
                entity.UniqueID = refID;
                entity.Name = firstName.ToEncodedHtml() + " " + LastName.ToEncodedHtml();
                entity.Email = email.ToEncodedHtml();
                entity.Comment = comment.ToEncodedHtml();
                entity.Status = (int)ExternalCardCustomerStatus.Activated;
                entity.ExtraData = extraData;
                dc.tblExternalCardCustomers.InsertOnSubmit(entity);
                dc.SubmitChanges();

                return "ok";
            }
        }

        private static XElement EncryptNode(string domainHost, XElement node) 
        {
            node.Value = Encryption.Encrypt(domainHost, node.Value).ToBase64();
            node.SetAttributeValue("isEncrypted", true);
            return node;
        }

		public static string AddPayoneerCustomer(string customerName, string customerEmail, string comment) 
		{
            User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);

            // check customer
            var found = from c in dc.tblExternalCardCustomers
                        where c.Email.Trim().ToLower() == customerEmail.Trim().ToLower() && c.Merchant_id == user.ID && c.tblExternalCardTerminal.ExternalCardProvider_id == (int)ExternalCardProviderEnum.Payoneer
                        select c;
            if (found.Count() > 0)
                return "Duplicate Customer";

            // get terminal
            tblExternalCardTerminal terminal = GetTerminal(ExternalCardProviderEnum.Payoneer);
            if (terminal == null)
                return "Terminal Not Found";

			// add customer
			tblExternalCardCustomer entity = new tblExternalCardCustomer();
            entity.ExternalCardTerminal_id = terminal.ExternalCardTerminal_id;
			entity.Merchant_id = user.ID;
			entity.UniqueID = Guid.NewGuid();
			entity.Name = customerName.ToEncodedHtml();
			entity.Email = customerEmail.ToEncodedHtml();
			entity.Comment = comment.ToEncodedHtml();
			entity.Status = (int)ExternalCardCustomerStatus.Added;
			dc.tblExternalCardCustomers.InsertOnSubmit(entity);
			dc.SubmitChanges();

			Task task = new Task("externalCards", "external cards reg email", LogTag.Exception, "Netpay.Bll", "Netpay.Bll.ExternalCards", "SendRegistrationEmail", new List<object>() { entity.UniqueID });
			Netpay.Infrastructure.Application.Queue.Enqueue(task);

            return "ok";
		}

        /// <summary>
        /// Required for payoneer reg
        /// </summary>
        /// <param name="credentialsToken"></param>
        /// <param name="uniqueID"></param>
		public static void SendRegistrationEmail(Guid uniqueID)
		{
			User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
			var merchant = Merchant.Merchant.Load(user.ID);

            // get terminal
            tblExternalCardTerminal terminal = GetTerminal(ExternalCardProviderEnum.Payoneer);
            if (terminal == null)
                return;

			ExternalCardCustomerVO customer = GetCustomer(uniqueID);
			if (customer == null)
				return;

			try
			{
				// get customer registration link
				// p4 is the customer unique id
				// add p6 for redirection to work properly
                string payoneerUsername = terminal.AuthUsername;
                string payoneerPassword = Encryption.Decrypt(terminal.AuthPassword256.ToArray());
                string payoneerPartnerID = terminal.AuthVar1;
				string regRequest = string.Format(user.Domain.PayoneerServiceBase + "?mname=GetToken&p1={0}&p2={1}&p3={2}&p4={3}&p6={4}", payoneerUsername, payoneerPassword, payoneerPartnerID, uniqueID, "http://www.netpay-intl.com/");
				string regResult = new WebClient().DownloadString(regRequest);

				// send registration link to customer
				MailMessage message = new MailMessage();
				message.From = new MailAddress(user.Domain.MailAddressFrom, user.Domain.MailNameFrom);
				message.To.Add(new MailAddress(customer.Email, customer.Name));
				message.Subject = "Payoneer registration request";
				message.Body = string.Format("{0} has sent you a Payoneer registration request. <br/>Click the link below to begin your registration at Payoneer. <br/><br/>{1}", merchant.Name, regResult);
				message.IsBodyHtml = true;
				Netpay.Infrastructure.Email.SmtpClient.Send(message);

				customer.Status = ExternalCardCustomerStatus.RegistrationEmailSent;
			}
			catch (Exception e)
			{
				Logger.Log(e);
				customer.Status = ExternalCardCustomerStatus.RegistrationEmailFailed;
			}

			// update status 
			UpdateCustomer(customer);
		}

		public static bool ChangeCustomerStatus(string domainHost, Guid customerUniqueID, ExternalCardCustomerStatus status) 
		{
			Domain domain = Netpay.Infrastructure.Domains.DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			tblExternalCardCustomer entity = (from pc in dc.tblExternalCardCustomers where pc.UniqueID == customerUniqueID select pc).SingleOrDefault();
			if (entity == null)
				return false;

			entity.Status = (short)status;
			dc.SubmitChanges();

			return true;
		}

		/*
		public static bool RequestMoneyTransfer(Currency currency, decimal amount)
		{
			User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
			
			// lock balance money
			MerchantBalanceChangeResult result = Merchants.DebitMerchant(amount, currency, BalanceStatus.Processing, "Payment Request", BalanceSource.Merchant, user.ID, "");
			if (result != MerchantBalanceChangeResult.Success)
				return false;				
			
			// create wire


			return true;
		}
		*/

		public static bool GetPayoneerBalance(out decimal dueFees, out decimal accountBalance) 
		{
			User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
			dueFees = accountBalance = 0;

            // get terminal
            tblExternalCardTerminal terminal = GetTerminal(ExternalCardProviderEnum.Payoneer);
            if (terminal == null)
                return false;

			try
			{
                string payoneerUsername = terminal.AuthUsername;
                string payoneerPassword = Encryption.Decrypt(terminal.AuthPassword256.ToArray());
                string payoneerPartnerID = terminal.AuthVar1;
				string request = string.Format(user.Domain.PayoneerServiceBase + "?mname=GetAccountDetails&p1={0}&p2={1}&p3={2}", payoneerUsername, payoneerPassword, payoneerPartnerID);
				string result = new WebClient().DownloadString(request);

				XElement parsedResult = XElement.Parse(result);
				dueFees = decimal.Parse(parsedResult.Element("FeesDue").Value);
				accountBalance = decimal.Parse(parsedResult.Element("AccountBalance").Value);

				return true;

				/*
					<GetAccountDetails>
						<FeesDue><!-- Fees due by partner to Payoneer Inc -->
						</FeesDue>
						<AccountBalance>
						<!-- Balance remaining in the Partner’s Account for payments -->
						</AccountBalance>
					</GetAccountDetails>
				 */
			}
			catch (Exception e)
			{
				Logger.Log(e);
				return false;
			}		
		}

		public class ExternalCardPaymentResult
		{
			public ExternalCardPaymentResult(){}
            public ExternalCardPaymentResult(ExternalCardPaymentResultEnum result) { Result = result; }
			
			public ExternalCardPaymentResultEnum Result { get; set; }
			public int CustomerID { get; set; }
			public string ResultCode { get; set; }
			public string ResultDescription { get; set; }
			public decimal Amount { get; set; }
			public Guid CustomerUniqueID { get; set; }
		}

		public static List<ExternalCardPaymentResult> PayToCustomers(string fileContent)
		{
			User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

			Dictionary<int, decimal> payments = new Dictionary<int, decimal>();
			StringReader reader = new StringReader(fileContent);
			while (true)
			{
				string currentLine = reader.ReadLine();
				if (currentLine == null)
					break;

				string[] currentLineSplit = currentLine.Split(',');
				int customerID;
				if (!int.TryParse(currentLineSplit[0], out customerID))
					continue;
				decimal amount;
				if (!decimal.TryParse(currentLineSplit[1], out amount))
					continue;

				payments.Add(customerID, amount);
			}

			return PayToCustomers(payments);
		}

		public static List<ExternalCardPaymentResult> PayToCustomers(Dictionary<int, decimal> payments) 
		{
			User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

			List<ExternalCardPaymentResult> results = new List<ExternalCardPaymentResult>();
			foreach (KeyValuePair<int, decimal> currentPayment in payments) 
			{
				ExternalCardPaymentResult result = PayToCustomer(currentPayment.Key, currentPayment.Value);
				results.Add(result);
			}

			return results;
		}

		public static List<ExternalCardPaymentResult> PayToCustomers(Dictionary<Guid, decimal> payments)
		{
			User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

			List<ExternalCardPaymentResult> results = new List<ExternalCardPaymentResult>();
			foreach (KeyValuePair<Guid, decimal> currentPayment in payments) 
			{
				ExternalCardPaymentResult result = PayToCustomer(currentPayment.Key, currentPayment.Value);
				results.Add(result);
			}

			return results;
		}

		public static ExternalCardPaymentResult PayToCustomer(Guid customerID, decimal amount) 
		{
			User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

			ExternalCardCustomerVO customer = GetCustomer(customerID);
			if (customer == null)
				return new ExternalCardPaymentResult() { Result = ExternalCardPaymentResultEnum.CustomerNotFound, CustomerUniqueID = customerID, Amount = amount};

			return PayToCustomer(customer, amount);			
		}

		public static ExternalCardPaymentResult PayToCustomer(int customerID, decimal amount) 
		{
			User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

			ExternalCardCustomerVO customer = GetCustomer(customerID);
			if (customer == null)
				return new ExternalCardPaymentResult() { Result = ExternalCardPaymentResultEnum.CustomerNotFound, CustomerID = customerID, Amount = amount };

			return PayToCustomer(customer, amount);
		}

		private static object _payToCustomerSync = new object();
		private static ExternalCardPaymentResult PayToCustomer(ExternalCardCustomerVO customer, decimal amount) 
		{
            switch (customer.Provider)
	        {
                case ExternalCardProviderEnum.Payoneer:
                    return PayToPayoneerCustomer(customer, amount);
                case ExternalCardProviderEnum.LiveKash:
                    return PayToLiveKashCustomer(customer, amount);
                default:
                    throw new ApplicationException("Unknown provider");
	        }
		}

        private static ExternalCardPaymentResult PayToLiveKashCustomer(ExternalCardCustomerVO customer, decimal amount)
        {
            User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

            // get terminal
            tblExternalCardTerminal terminal = GetTerminal(ExternalCardProviderEnum.LiveKash);
            if (terminal == null)
                return new ExternalCardPaymentResult(ExternalCardPaymentResultEnum.TerminalNotFound);

            // validate
            if (amount <= 0)
                return new ExternalCardPaymentResult() { Result = ExternalCardPaymentResultEnum.InvalidAmount };

            // get customer card
            if (customer.ExtraData == null)
                return new ExternalCardPaymentResult() { Result = ExternalCardPaymentResultEnum.CustomerDataNotFound };
            string cardNumber = customer.ExtraData.Element("VcardRespond").Element("Vcard").Value.FromEncryptedBase64(credentialsToken);

            // api call
            lock (_payToCustomerSync)
            {
                try
                {
                    string terminalID = terminal.AuthUsername;
                    string terminalPassword = Encryption.Decrypt(terminal.AuthPassword256.ToArray());
                    string merchantID = terminal.AuthVar1;
                    Guid paymentUniqueID = Guid.NewGuid();

                    // api request
                    Guid refID = Guid.NewGuid();
                    XElement requestXml = new XElement("VcardRequest");
                    requestXml.Add(new XElement("Merchant_Reference", paymentUniqueID));
                    requestXml.Add(new XElement("C_IP_address", "80.179.180.59"));
                    requestXml.Add(new XElement("Vendor_Request", "LOAD"));
                    requestXml.Add(new XElement("Vendor_Id", merchantID));
                    requestXml.Add(new XElement("Card_Issue", "LIVEKASH"));
                    requestXml.Add(new XElement("Terminal_Pass", terminalPassword));
                    requestXml.Add(new XElement("Terminal_Id", terminalID));
                    requestXml.Add(new XElement("Currency_Request", "USD"));
                    requestXml.Add(new XElement("Card_Id", cardNumber));
                    requestXml.Add(new XElement("Amount", amount));

                    // api call
                    string url = null; ;
                    if (Configuration.IsProduction)
                        url = "https://www.lpg24.net/api/LPG_Card_Creation.asp";
                    else
                        url = "https://www.lpg24.net/test-api/LPG_Card_Creation.asp";
                    WebClient webClient = new WebClient();
                    string response = webClient.UploadString(url, "post", requestXml.ToString());
                    if (!response.Contains("<?xml"))
                    {
                        Logger.Log(LogSeverity.Error, LogTag.ExternalCards, "LiveKash payment failed, see more info", response);
                        return new ExternalCardPaymentResult(ExternalCardPaymentResultEnum.UnexpectedError);
                    }

                    XElement responseXml = XElement.Parse(response);
                    string result = responseXml.Elements("Status").First().Value;
                    tblExternalCardCustomerPayment entity = new tblExternalCardCustomerPayment();
                    entity.Result = result;
                    entity.ExternalCardCustomer_id = customer.ID;
                    entity.UniqueID = paymentUniqueID;
                    entity.Amount = amount;
                    entity.InsertDate = DateTime.Now;
                    ExternalCardPaymentResult paymentResult = new ExternalCardPaymentResult();
                    if (result == "ERROR")
                    {
                        string reason = responseXml.Elements("Fail_Reason").First().Value;
                        Logger.Log(LogSeverity.Error, LogTag.ExternalCards, "LiveKash payment failed, see more info", response);

                        if (reason.Contains("Insufficient fund"))
                        {
                            entity.ResultDescription = "Insufficient funds";
                            paymentResult.Result = ExternalCardPaymentResultEnum.InsufficientFunds;
                        }
                        else 
                        {
                            entity.ResultDescription = "Transfer failed";
                            paymentResult.Result = ExternalCardPaymentResultEnum.TransferFailed;
                        }
                    }
                    else
                    {
                        paymentResult.Result = ExternalCardPaymentResultEnum.Success;
                    }

                    NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
                    dc.tblExternalCardCustomerPayments.InsertOnSubmit(entity);
                    dc.SubmitChanges();

                    return paymentResult;
                }
                catch (Exception e)
                {
                    Logger.Log(e);
                    return new ExternalCardPaymentResult() { Result = ExternalCardPaymentResultEnum.UnexpectedError };
                }
            }
        }

        private static ExternalCardPaymentResult PayToPayoneerCustomer(ExternalCardCustomerVO customer, decimal amount) 
        {
            User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

            // get terminal
            tblExternalCardTerminal terminal = GetTerminal(ExternalCardProviderEnum.Payoneer);
            if (terminal == null)
                return new ExternalCardPaymentResult(ExternalCardPaymentResultEnum.TerminalNotFound);
            
            if (amount <= 0)
                return new ExternalCardPaymentResult() { Result = ExternalCardPaymentResultEnum.InvalidAmount };

            string requestResult = null;
            string request = null;
            string resultCode = null;
            string resultDescription = null;
            lock (_payToCustomerSync)
            {
                try
                {
                    string payoneerUsername = terminal.AuthUsername;
                    string payoneerPassword = Encryption.Decrypt(terminal.AuthPassword256.ToArray());
                    string payoneerPartnerID = terminal.AuthVar1;
                    string payoneerProgramID = terminal.AuthVar2;
                    Guid paymentUniqueID = Guid.NewGuid();
                    request = string.Format(user.Domain.PayoneerServiceBase + "?mname=PerformPayoutPayment&p1={0}&p2={1}&p3={2}&p4={3}&p5={4}&p6={5}&p7={6}&p8={7}&p9={8}", payoneerUsername, payoneerPassword, payoneerPartnerID, payoneerProgramID, paymentUniqueID, customer.UniqueID, amount, "Transfer to payee", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
                    requestResult = new WebClient().DownloadString(request);

                    /*
                    <PerformPayoutPayment>
                        <Description><!-- If Status is not 000, This will describes the reason</Description>
                        <PaymentID><!-- A unique reference code for the payment performed --></PaymentID>
                        <Status><!-- See status code table below --></Status>
                        <PayoneerID><!-- The unique Payee ID in the Payoneer Inc system --></PayoneerID>
                    </PerformPayoutPayment>
                    */

                    XElement parsedResult = XElement.Parse(requestResult);
                    tblExternalCardCustomerPayment entity = new tblExternalCardCustomerPayment();
                    entity.Result = resultCode = parsedResult.Element("Status").Value;
                    entity.ResultDescription = resultDescription = parsedResult.Element("Description").Value;
                    entity.ExternalCardCustomer_id = customer.ID;
                    entity.UniqueID = paymentUniqueID;
                    entity.Amount = amount;

                    NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
                    dc.tblExternalCardCustomerPayments.InsertOnSubmit(entity);
                    dc.SubmitChanges();
                }
                catch (Exception e)
                {
                    Logger.Log(e, requestResult + "\n\n" + request);
                    return new ExternalCardPaymentResult() { Result = ExternalCardPaymentResultEnum.UnexpectedError };
                }
            }

            if (resultCode == "000")
                return new ExternalCardPaymentResult() { Result = ExternalCardPaymentResultEnum.Success, CustomerID = customer.ID, ResultCode = resultCode, ResultDescription = resultDescription, Amount = amount };
            else
                return new ExternalCardPaymentResult() { Result = ExternalCardPaymentResultEnum.TransferFailed, CustomerID = customer.ID, ResultCode = resultCode, ResultDescription = resultDescription, Amount = amount };
        }
	}
}
