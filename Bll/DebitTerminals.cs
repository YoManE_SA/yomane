﻿using System;
using System.Collections.Generic;
using System.Linq;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Domains;
using Netpay.Infrastructure.VO;

namespace Netpay.Bll
{
	public static class DebitTerminals
	{
		public static List<TerminalVO> GetTerminals(string domainHost, SearchFilters sf)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			var res = (from t in dc.tblDebitTerminals select t);
			if (sf != null) {
				if (sf.debitCompanyID != null)
					res = res.Where(t => t.DebitCompany == sf.debitCompanyID);
				if (string.IsNullOrEmpty(sf.terminalNumber))
					res = res.Where(t => t.terminalNumber == sf.terminalNumber);
			}
			return res.Select(t => new TerminalVO(domainHost, t)).ToList();
		}
	}
}
