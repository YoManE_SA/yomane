﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.PeriodicFees
{
    public class AccountFees : BaseDataObject
    {
        public const string SecuredObjectName = "AccountFees";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(PeriodicFees.Module.Current, SecuredObjectName); } }

        public class SearchFilters
        {
            public int? id;
            public DateTime NextCharge;
            public int? accountId;
        }

        protected Netpay.Dal.Netpay.SetPeriodicFee _entity;
        public int ID { get { return _entity.SetPeriodicFee_id; } }
        public int FeeTypeID { get { return _entity.PeriodicFeeType_id; } set { _entity.PeriodicFeeType_id = value; } }
        public int AccountID { get { return _entity.Account_id; } }
        public string CurrencyISOCode { get { return _entity.CurrencyISOCode; } set { _entity.CurrencyISOCode = value; } }
        public decimal Amount { get { return _entity.Amount; } set { _entity.Amount = value; } }
        public bool IsActive { get { return _entity.IsActive; } set { _entity.IsActive = value; } }
        public DateTime DateNextCharge { get { return _entity.DateNextCharge; } set { _entity.DateNextCharge = value; } }
        public int? AccountStoredPaymentMethodID { get { return _entity.AccountPaymentMethod_id; } set { _entity.AccountPaymentMethod_id = value; } }
        public byte? ChargeType { get { return _entity.ChargeType; } set { _entity.ChargeType = value; } }

        public string StoredPaymentMethodName
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                if (AccountStoredPaymentMethodID.HasValue)
                {
                    return (from apm in DataContext.Reader.AccountPaymentMethods
                            where apm.AccountPaymentMethod_id == AccountStoredPaymentMethodID.Value
                            select apm.PaymentMethodText).SingleOrDefault();
                }
                else
                {
                    return "---";
                }
            }
        }



        public List<KeyValuePair<string, int>> AccountStoredPaymentMethods
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                var exp = from apm in DataContext.Reader.AccountPaymentMethods
                          where apm.Account_id == AccountID
                          select new KeyValuePair<string, int>(apm.PaymentMethodText, apm.AccountPaymentMethod_id);
                var list = exp.ToList();
                if (list.Count() == 0)
                {
                    var EmptyList = new List<KeyValuePair<string, int>>();
                    EmptyList.Add(new KeyValuePair<string, int>("No payment method stored yet", -1));
                    return EmptyList;
                }
                return list;
            }
        }

        public PeriodicFeeType RefFeeType
        {
            get
            {
                return PeriodicFeeType.Load(_entity.PeriodicFeeType_id);
            }
        }

        protected AccountFees(Dal.Netpay.SetPeriodicFee entity)
        {
            _entity = entity;
        }

        public AccountFees(int accountId)
        {
            _entity = new Dal.Netpay.SetPeriodicFee();
            _entity.Account_id = accountId;
        }

        public override ValidationResult Validate()
        {
            return base.Validate();
        }

        public void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            ValidationException.RaiseIfNeeded(Validate());
            DataContext.Writer.SetPeriodicFees.Update(_entity, (_entity.SetPeriodicFee_id != 0));
            DataContext.Writer.SubmitChanges();
        }

        public void Delete()
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);
            if (_entity.SetPeriodicFee_id == 0) return;
            DataContext.Writer.SetPeriodicFees.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }

        public static AccountFees Load(int id)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);
            var ret = (from a in DataContext.Reader.SetPeriodicFees where a.SetPeriodicFee_id == id select a).SingleOrDefault();
            if (ret == null) return null;
            return new AccountFees(ret);
        }

        public void UpdateDateNextCharge()
        {
            if (RefFeeType == null) throw new Exception("Ref fee type is null for periodic fee id:" + ID);
            PeriodicInterval timeInterval = RefFeeType.TimeInterval;
            switch (timeInterval)
            {
                case PeriodicInterval.Day:
                    DateNextCharge = DateNextCharge.AddDays(1);
                    break;
                case PeriodicInterval.Week:
                    DateNextCharge = DateNextCharge.AddWeeks(1);
                    break;
                case PeriodicInterval.SemiMonth:
                    DateNextCharge = DateNextCharge.AddWeeks(2);
                    break;
                case PeriodicInterval.Month:
                    DateNextCharge = DateNextCharge.AddMonths(1);
                    break;
                case PeriodicInterval.Quarter:
                    DateNextCharge = DateNextCharge.AddMonths(3);
                    break;
                case PeriodicInterval.SemiYear:
                    DateNextCharge = DateNextCharge.AddMonths(6);
                    break;
                case PeriodicInterval.Year:
                    DateNextCharge = DateNextCharge.AddYears(1);
                    break;
            }
            Save();
        }

        public static List<AccountFees> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);
            var exp = (from a in DataContext.Reader.SetPeriodicFees select a);
            if (filters != null)
            {
                if (filters.id.HasValue) exp = exp.Where((x) => x.SetPeriodicFee_id == filters.id.Value);
                if (filters.accountId.HasValue) exp = exp.Where((x) => x.Account_id == filters.accountId.Value);
            }
            var result = exp.ApplySortAndPage(sortAndPage).Select(x => new AccountFees(x)).ToList();
            return result;
        }

        public static void Execute()
        {
            // expression represents account ids of merchants with status processing
            var IdsOfMerchants = from merchants in DataContext.Reader.tblCompanies.Where(mer => mer.ActiveStatus == (byte)MerchantStatus.Processing)
                                 join accounts in DataContext.Reader.Accounts on merchants.ID equals accounts.Merchant_id
                                 select accounts.Account_id;

            // expression represents account ids of customers with status fully active
            var IdsOfCustomers = from customers in DataContext.Reader.Customers.Where(cust => cust.ActiveStatus_id == (byte)Customers.ActiveStatus.FullyActive)
                                 join accounts in DataContext.Reader.Accounts on customers.Customer_id equals accounts.Customer_id
                                 select accounts.Account_id;

            // expression represents account ids of affiliates with IsActive = true            
            var IdsOfAffiliates = from affiliates in DataContext.Reader.tblAffiliates.Where(aff => aff.IsActive == true)
                                  join accounts in DataContext.Reader.Accounts on affiliates.affiliates_id equals accounts.Affiliate_id
                                  select accounts.Account_id;
            
            // Consider that account can be customer/affiliate/merchant
            var feesToProcess = (from periodicFeeTypes in DataContext.Reader.PeriodicFeeTypes.Where(feeType => feeType.IsActive == true && feeType.ProcessMerchant_id.HasValue && feeType.Amount.HasValue)
                                 join currencies in DataContext.Reader.CurrencyLists
                                     on periodicFeeTypes.CurrencyISOCode equals currencies.CurrencyISOCode
                                 join periodicFees in DataContext.Reader.SetPeriodicFees.Where(fee => fee.IsActive == true && fee.ChargeType.HasValue && fee.DateNextCharge <= DateTime.Now)
                                     on periodicFeeTypes.PeriodicFeeType_id equals periodicFees.PeriodicFeeType_id
                                 join accounts in DataContext.Reader.Accounts.Where(ac => IdsOfMerchants.Contains(ac.Account_id) || IdsOfCustomers.Contains(ac.Account_id) || IdsOfAffiliates.Contains(ac.Account_id))
                                     on periodicFees.Account_id equals accounts.Account_id
                                 select new PeriodicFeeData
                                 {
                                     TypeID = periodicFeeTypes.PeriodicFeeType_id,
                                     TimeInterval = (PeriodicInterval)periodicFeeTypes.TimeInterval_id,
                                     ProcessingMerchantID = periodicFeeTypes.ProcessMerchant_id.Value,
                                     Name = periodicFeeTypes.Name,
                                     Amount = periodicFeeTypes.Amount.Value,
                                     CurrencyIso = periodicFeeTypes.CurrencyISOCode,
                                     AccountID = accounts.Account_id,
                                     AccountType = (Accounts.AccountType)accounts.AccountType_id,
                                     ChargeType = (PeriodicFeeChargeType)periodicFees.ChargeType.Value,
                                     PeriodicFeeID = periodicFees.SetPeriodicFee_id,
                                     StoredPmId = periodicFees.AccountPaymentMethod_id
                                 })
                                     .ToList();

            if (feesToProcess.Count > 0)
            {
                feesToProcess.ForEach(feeToProcess => { ExecuteChargeFee(feeToProcess); });
            }
        }


        public static void ExecuteChargeFee(PeriodicFeeData feeData)
        {
            var periodicFeeEntity = Load(feeData.PeriodicFeeID);
            if (periodicFeeEntity == null) throw new Exception("periodic fee id doesn't exist.");

            try
            {
                // relevant only to merchant
                if (feeData.ChargeType == PeriodicFeeChargeType.OnlyProcessing)
                {
                    CreateProcessTransactionForPeriodicFee(feeData);
                    // TODO - check tblTransactionAmount issue.
                }
                // relevant to merchant, customer or affiliate
                else if (feeData.ChargeType == PeriodicFeeChargeType.OnlyCC)
                {
                    CreateCCTransactionForPeriodicFee(feeData);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }
            finally
            {
                periodicFeeEntity.UpdateDateNextCharge();
            }
        }

        public static void ExecuteForDate(DateTime date)
        {

        }

        // Create transaction for periodic fee with charge type - Processing
        public static void CreateProcessTransactionForPeriodicFee(PeriodicFeeData feeData)
        {
            var trans = new Transactions.Transaction() { PaymentData = null, Status = TransactionStatus.Captured, PaymentsIDs = ";0;" };
            trans.InsertDate = DateTime.Now;
            trans.MerchantID = Accounts.Account.LoadAccount(feeData.AccountID).MerchantID.Value;
            trans.TransactionSource = feeData.TimeInterval.ToTransactionSource();
            trans.CreditType = CreditType.Refund;
            trans.Amount = feeData.Amount; // unsettled amount updated inside SavePass
            trans.Installments = 1; // unsettled installments updated inside SavePass, this property is mapped to Payments, which should be 1.
            trans.CurrencyID = Currency.Get(feeData.CurrencyIso).ID;
            trans.CurrencyIsoCode = feeData.CurrencyIso;
            trans.PaymentMethodID = CommonTypes.PaymentMethodEnum.BankFees;  
            trans.PaymentMethodDisplay = Domain.Current.BrandName + " Fee";
            trans.IP = "";
            trans.Save();
        }

        // Create transaction for periodic fee with charge type - CC
        public static void CreateCCTransactionForPeriodicFee(PeriodicFeeData feeData)
        {
            var pm = Accounts.StoredPaymentMethod.Load(feeData.StoredPmId.Value);
            if (!feeData.StoredPmId.HasValue)
            {
                throw new Exception("Stored pm id is null for periodic fee id:" + feeData.PeriodicFeeID);
            }

            // create new transaction
            var trans = new Transactions.Transaction();

            //general data
            trans.MerchantID = feeData.ProcessingMerchantID;
            trans.TransactionSource = feeData.TimeInterval.ToTransactionSource(); // check with Eliad
            trans.CreditType = CreditType.Regular;
            trans.Amount = feeData.Amount;
            trans.Installments = 1;
            trans.CurrencyID = Currency.Get(feeData.CurrencyIso).ID;
            trans.CurrencyIsoCode = feeData.CurrencyIso;

            //Payer data
            trans.PayerData.FullName = pm.OwnerName;
            trans.PayerData.PersonalNumber = pm.OwnerSSN;

            switch(feeData.AccountType)
            {
                case Accounts.AccountType.Merchant:
                    Merchants.Merchant merchant = Merchants.Merchant.LoadByAccountId(feeData.AccountID);
                    if (merchant == null) throw new Exception($"merchant with account id:{feeData.AccountID} not found");
                    trans.PayerData.EmailAddress = merchant.ContactEmail;
                    trans.PayerData.PhoneNumber = merchant.ContactPhone;
                    break;

                case Accounts.AccountType.Customer:
                    Customers.Customer customer = Customers.Customer.LoadByAccountID(feeData.AccountID);
                    if (customer == null) throw new Exception($"customer with account id:{feeData.AccountID} not found");
                    trans.PayerData.EmailAddress = customer.EmailAddress;
                    trans.PayerData.PhoneNumber = customer.PhoneNumber;
                    break;

                case Accounts.AccountType.Affiliate:
                    Affiliates.Affiliate affiliate = Affiliates.Affiliate.LoadByAccountID(feeData.AccountID);
                    if (affiliate == null) throw new Exception($"affiliate with account id:{feeData.AccountID} not found");
                    trans.PayerData.EmailAddress = affiliate.LoginEmailAddress;
                    break;
            }

            //payment data
            trans.PaymentData.MethodInstance = pm.MethodInstance;
            var values = trans.GetProcessParamters();
            values = Bll.Transactions.Transaction.ProcessTransaction(CommonTypes.PaymentMethodGroupEnum.CreditCard, values);
            if (values["Reply"] != "000")
            {
                // Add notification message 
                var notification = new Bll.Content.Bulletin();
                notification.Text = $"Credit card transaction for periodic fee:{feeData.PeriodicFeeID} failed, transaction id:{values["TransID"].ToString()}";
                notification.Type = "SYS_INFO";
                notification.SolutionID = Bll.Content.Bulletin.GetSolutions().Where(x => x.Value == "Admin Fees").SingleOrDefault().Key;
                notification.InsertDate = DateTime.Now;
                notification.ExpirationDate = DateTime.Now.AddYears(1);
                notification.Save(); 
            }
        }
    }

    public class PeriodicFeeData
    {
        public int TypeID { get; set; }
        public PeriodicInterval TimeInterval { get; set; }
        public int AccountID { get; set; }
        public Accounts.AccountType AccountType { get; set; }
        public int ProcessingMerchantID { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public string CurrencyIso { get; set; }
        public PeriodicFeeChargeType ChargeType { get; set; }
        public int PeriodicFeeID { get; set; }
        public int? StoredPmId { get; set; }
    }
}
