﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.PeriodicFees
{
	
	public class PeriodicFeeType : BaseDataObject
	{
		public const string SecuredObjectName = "PeriodicFeeType";
		public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(PeriodicFees.Module.Current, SecuredObjectName); } }

		public class SearchFilters
		{
			public Range<int?> ID;
			public Range<decimal?> Amount;
			public string CurrencyISOCode;
			public bool? isActive;
            public Accounts.AccountType? AccountTypeToSearch;
		}

		protected Netpay.Dal.Netpay.PeriodicFeeType _entity;

		public int ID { get { return _entity.PeriodicFeeType_id; } }
		public string CurrencyISOCode { get { return _entity.CurrencyISOCode; } set { _entity.CurrencyISOCode = value; } }
		public decimal? Amount { get { return _entity.Amount; } set { _entity.Amount = value; } }
		public string Name { get { return _entity.Name; } set { _entity.Name = value; } }
		//public string Description { get { return _entity.Description; } set { _entity.Description = value; } }
		public PeriodicInterval TimeInterval { get { return (PeriodicInterval)_entity.TimeInterval_id; } set { _entity.TimeInterval_id = (byte)value; } }
		public bool IsActive { get { return _entity.IsActive; } set { _entity.IsActive = value; } }
		public Accounts.AccountType AccountType { get { return (Accounts.AccountType)_entity.AccountType_id; } set { _entity.AccountType_id = (byte)value; } }
		public int? ProcessMerchantID { get { return _entity.ProcessMerchant_id; } set { _entity.ProcessMerchant_id = value; } }
        public string ProcessMerchantName { get { string ret; Bll.Merchants.Merchant.CachedNamesForDomain().TryGetValue(ProcessMerchantID.GetValueOrDefault(), out ret); return ret; } }

		protected PeriodicFeeType(Dal.Netpay.PeriodicFeeType entity)
		{
			_entity = entity;
		}

		public PeriodicFeeType()
		{
			_entity = new Dal.Netpay.PeriodicFeeType();
			//_entity.TimeInterval_id = 1;
		}

		public void Save()
		{
			ValidationException.RaiseIfNeeded(Validate());
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

			DataContext.Writer.PeriodicFeeTypes.Update(_entity, (_entity.PeriodicFeeType_id != 0));
			DataContext.Writer.SubmitChanges();
		}

		public void Delete()
		{
			if (_entity.PeriodicFeeType_id == 0) return;
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

			DataContext.Writer.PeriodicFeeTypes.Delete(_entity);
			DataContext.Writer.SubmitChanges();
		}

		public static PeriodicFeeType Load(int id)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

			var ret = (from a in DataContext.Reader.PeriodicFeeTypes where a.PeriodicFeeType_id == id select a).SingleOrDefault();
			if (ret == null) return null;
			return new PeriodicFeeType(ret);
		}

		public static List<PeriodicFeeType> Search(SearchFilters filters, ISortAndPage sortAndPage)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

			var exp = (from a in DataContext.Reader.PeriodicFeeTypes select a);
			if (filters != null)
			{
				if (filters.ID.From.HasValue) exp = exp.Where((x) => x.PeriodicFeeType_id >= filters.ID.From.Value);
				if (filters.ID.To.HasValue) exp = exp.Where((x) => x.PeriodicFeeType_id <= filters.ID.To.Value);
				if (filters.Amount.From.HasValue) exp = exp.Where((x) => x.Amount >= filters.Amount.From.Value);
				if (filters.Amount.To.HasValue) exp = exp.Where((x) => x.Amount <= filters.Amount.To.Value);
				if (filters.CurrencyISOCode != null) exp = exp.Where((x) => x.CurrencyISOCode == filters.CurrencyISOCode);
                if (filters.AccountTypeToSearch != null) exp = exp.Where((x) => (Accounts.AccountType)x.AccountType_id == filters.AccountTypeToSearch);
                if (filters.isActive.HasValue) exp = exp.Where((x) => x.IsActive == filters.isActive.Value);
			}
			return exp.ApplySortAndPage(sortAndPage).Select(x => new PeriodicFeeType(x)).ToList();
		}
	}
}
