﻿using System;
using Netpay.Infrastructure.VO;
using Netpay.Infrastructure;

namespace Netpay.Bll
{
	public class SortingInfo
	{
		private string _property = null;
		private SortDirection _direction = SortDirection.Ascending;
		private Type _voType = null;

		public Type VOType
		{
			get { return _voType; }
			set { _voType = value; }
		}

		public string Property
		{
			get { return _property; }
			set { _property = value; }
		}

		public SortDirection Direction
		{
			get { return _direction; }
			set { _direction = value; }
		}

		public string MappedEntityProperty
		{
			get 
			{ 
				return VOUtils.GetMappedProperty(VOType, _property);
			}
		}
	}
}
