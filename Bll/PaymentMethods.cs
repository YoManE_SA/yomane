﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure.VO;

namespace Netpay.Bll
{
	public class PaymentMethods
	{
		private static IQueryable<tblCreditCard> SearchCreditCards(User user, SearchFilters filters)
		{
			if (user.Type != UserType.NetpayAdmin)
				filters.merchantIDs = SecurityManager.FilterRequestedIDs(filters.merchantIDs, user.AllowedMerchantsIDs);

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			dc.CommandTimeout = 120;

			var expression = from cc in dc.tblCreditCards select cc;

			// add filters
			if (filters != null)
			{
				if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
					expression = expression.Where(cc => (cc.CompanyID != null && filters.merchantIDs.Contains((int)cc.CompanyID)));
				if (filters.last4Digits != null)
					expression = expression.Where(cc => cc.CCard_Last4 == filters.last4Digits);
				if (filters.first6Digits != null)
					expression = expression.Where(cc => cc.CCard_First6 == filters.first6Digits);
				if (filters.phoneNumber != null)
					expression = expression.Where(cc => cc.phoneNumber == filters.phoneNumber);
				if (filters.personalNumber != null)
					expression = expression.Where(cc => cc.PersonalNumber == filters.personalNumber.Trim());
				if (filters.cardHolderName != null)
					expression = expression.Where(cc => cc.Member.StartsWith(filters.cardHolderName.Trim()));
				if (filters.cardHolderEmail != null)
					expression = expression.Where(cc => cc.email == filters.cardHolderEmail.Trim());
				if (filters.cardHolderCity != null)
					expression = expression.Where(cc => cc.tblBillingAddress.city == filters.cardHolderCity.Trim());
				if (filters.cardHolderCountryID != null)
					expression = expression.Where(cc => cc.tblBillingAddress.countryId == filters.cardHolderCountryID);
				if (filters.cardHolderStateID != null)
					expression = expression.Where(cc => cc.tblBillingAddress.stateId == filters.cardHolderStateID);
				if (filters.cardHolderZipcode != null)
					expression = expression.Where(cc => cc.tblBillingAddress.zipCode == filters.cardHolderZipcode.Trim());
				if (filters.creditCard != null)
				{
					byte[] encryptedArray = null;
					Encryption.Encrypt(user.Domain.Host, filters.creditCard, out encryptedArray);
					Binary encryptedBinary = new Binary(encryptedArray);
					expression = expression.Where(cc => cc.CCard_number256.Equals(encryptedBinary));
				}
				if (filters.expirationMonth != null)
					expression = expression.Where(cc => cc.ExpMM == filters.expirationMonth.Value.ToString("0#"));
				if (filters.expirationYear != null)
					expression = expression.Where(cc => cc.ExpYY == filters.expirationYear.Value.ToString("0#")); 
			}		

			return expression;
		}
		
		public static List<CreditCardVO> SearchCreditCards(Guid credentialsToken, SearchFilters filters, PagingInfo pagingInfo, SortingInfo sortingInfo)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary, UserType.NetpayAdmin, UserType.NetpayUser });
			if (user.Type != UserType.NetpayAdmin)
				filters.merchantIDs = SecurityManager.FilterRequestedIDs(filters.merchantIDs, user.AllowedMerchantsIDs);

			var expression = SearchCreditCards(user, filters);

			// handle sorting if not null
			if (sortingInfo != null)
			{
				if (sortingInfo.Direction == SortDirection.Ascending)
					expression = expression.OrderBy<tblCreditCard>(sortingInfo.MappedEntityProperty);
				else
					expression = expression.OrderByDescending<tblCreditCard>(sortingInfo.MappedEntityProperty);
			}

			// handle paging if not null
			if (pagingInfo != null)
			{
				pagingInfo.TotalItems = expression.Count();
				expression = expression.Skip(pagingInfo.Skip).Take(pagingInfo.Take);
			}

			return expression.Select(cc => new CreditCardVO(cc)).ToList<CreditCardVO>();
		}

		private static IQueryable<tblCheckDetail> SearchEchecks(User user, SearchFilters filters)
		{
			if (user.Type != UserType.NetpayAdmin)
				filters.merchantIDs = SecurityManager.FilterRequestedIDs(filters.merchantIDs, user.AllowedMerchantsIDs);

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			dc.CommandTimeout = 120;

			var expression = from cd in dc.tblCheckDetails select cd;

			// add filters
			if (filters != null)
			{
				if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
					expression = expression.Where(cd => (cd.CompanyId != null && filters.merchantIDs.Contains((int)cd.CompanyId)));
				if (filters.phoneNumber != null)
					expression = expression.Where(cd => cd.PhoneNumber == filters.phoneNumber);
				if (filters.cardHolderName != null)
					expression = expression.Where(cd => cd.AccountName.StartsWith(filters.cardHolderName.Trim()));
				if (filters.cardHolderEmail != null)
					expression = expression.Where(cd => cd.Email == filters.cardHolderEmail.Trim());
				if (filters.cardHolderCity != null)
					expression = expression.Where(cd => cd.tblBillingAddress.city == filters.cardHolderCity.Trim());
				if (filters.cardHolderCountryID != null)
					expression = expression.Where(cc => cc.tblBillingAddress.countryId == filters.cardHolderCountryID);
				if (filters.cardHolderStateID != null)
					expression = expression.Where(cd => cd.tblBillingAddress.stateId == filters.cardHolderStateID);
				if (filters.cardHolderZipcode != null)
					expression = expression.Where(cd => cd.tblBillingAddress.zipCode == filters.cardHolderZipcode.Trim());
			}

			return expression;
		}

		public static List<CheckDetailsVO> SearchEchecks(Guid credentialsToken, SearchFilters filters, PagingInfo pagingInfo, SortingInfo sortingInfo)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary, UserType.NetpayAdmin, UserType.NetpayUser });
			if (user.Type != UserType.NetpayAdmin)
				filters.merchantIDs = SecurityManager.FilterRequestedIDs(filters.merchantIDs, user.AllowedMerchantsIDs);

			var expression = SearchEchecks(user, filters);

			// handle sorting if not null
			if (sortingInfo != null)
			{
				if (sortingInfo.Direction == SortDirection.Ascending)
					expression = expression.OrderBy<tblCheckDetail>(sortingInfo.MappedEntityProperty);
				else
					expression = expression.OrderByDescending<tblCheckDetail>(sortingInfo.MappedEntityProperty);
			}

			// handle paging if not null
			if (pagingInfo != null)
			{
				pagingInfo.TotalItems = expression.Count();
				expression = expression.Skip(pagingInfo.Skip).Take(pagingInfo.Take);
			}

			return expression.Select(cd => new CheckDetailsVO(cd)).ToList();
		}

		public class PaymentMethodUsage {
			public string Name, Email, Last4Digits;
			public int Count;
		}
		public static List<PaymentMethodUsage> SearchPaymentMethodDistinct(Guid credentialsToken, SearchFilters filters)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary, UserType.NetpayAdmin, UserType.NetpayUser });
			if (user.Type != UserType.NetpayAdmin)
				filters.merchantIDs = SecurityManager.FilterRequestedIDs(filters.merchantIDs, user.AllowedMerchantsIDs);

			List<PaymentMethodUsage> ccPaymentMethods = SearchCreditCards(user, filters).GroupBy(cc => new { Name = cc.Member, Email = cc.email, Card = cc.CCard_Last4 }).Select(g => new PaymentMethodUsage { Email = g.Key.Email, Name = g.Key.Name, Last4Digits = g.Key.Card.ToString(), Count = g.Count() }).ToList();
			List<PaymentMethodUsage> ecPaymentMethods = SearchEchecks(user, filters).GroupBy(cc => new { Name = cc.AccountName, Email = cc.Email }).Select(g => new PaymentMethodUsage { Email = g.Key.Email, Name = g.Key.Name, Count = g.Count() }).ToList();
			List<PaymentMethodUsage> paymentMethodsJoined = new List<PaymentMethodUsage>();
			paymentMethodsJoined.AddRange(ccPaymentMethods);
			paymentMethodsJoined.AddRange(ecPaymentMethods);

            return paymentMethodsJoined;
            //return SearchCreditCards(user, filters).GroupBy(cc => new { Name = cc.Member, Email = cc.email, Card = cc.CCard_Last4 }).Select(g => new PaymentMethodDataVO() { Email = g.Key.Email, Name = g.Key.Name, Last4Digits = g.Key.Card, Count = g.Count() }).ToList<PaymentMethodDataVO>();
		}

		public static List<CreditCardVO> SearchCreditCardsDistinct(Guid credentialsToken, SearchFilters filters)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary, UserType.NetpayAdmin, UserType.NetpayUser });
			return SearchCreditCards(user, filters).GroupBy(cc => new { Name = cc.Member, Email = cc.email, Card = cc.CCard_Last4 }).Select(g => new CreditCardVO() { CardholderEmail = g.Key.Email, CardholderName = g.Key.Name, Last4Digits = g.Key.Card, Count = g.Count() }).ToList<CreditCardVO>();
		}
	}
}
