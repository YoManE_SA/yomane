﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.Bll
{
	/// <summary>
	/// Search filters.
	/// </summary>
	[Serializable]
	public class SearchFilters
	{
        public string freeText = null;
        public string email = null;
		public int? transactionID = null;
		public DateTime? dateFrom = null;
		public DateTime? dateTo = null;
		public int? paymentMethodID = null;
		public int? creditType = null;
		public string orderID = null;
		public string cardHolderName = null;
		public string cardHolderEmail = null;
		public byte? currencyID = null;
		public int? amountFrom = null;
		public int? originalTransactionID = null;
		public int? amountTo = null;
		public string replyCode = null;
		public int? first6Digits = null;
		public short? last4Digits = null;
		public string phoneNumber = null;
		public string personalNumber = null;
		public List<int> merchantIDs = null;
		public decimal? reportAmount = null;
		public string reportCondition = null;
		public bool? reportReserved = null;
		public bool? reportRelased = null;
		public bool? reportTotal = null;
		public int? debitCompanyID = null;
		public string terminalNumber = null;
		public int? makePaymentsRequestsID = null;
		public string basicInfoCustomerNumber = null;
		public string basicInfoCustomerName = null;
		public int? paymentType = null;
		public int? logSeverityID = null;
		public bool? failStatExcludeMultiples = null;
		public bool? transTypeDebit = null;
		public bool? transTypePreAuth = null;
		public bool? transTypeCapture = null;
		public string logTag = null;
		public TransactionStatus? transactionStatus = null;
		public int? merchantGroupID = null;
		public int? pspID = null;
		public int? managingCompanyID = null;
		public int? merchantStatus = null;
		public bool? isBankTransferReceived = null;
		public string bankTransferID = null;
		public bool? futureCharges = null;
		public string paymentsStatus = null;
		public int? declinedType = null;
		public List<short> paymentMethodsIds = null;
		public string cardHolderCity = null;
		public string cardHolderZipcode = null;
		public int? cardHolderStateID = null;
		public int? cardHolderCountryID = null;
		public int? refundAskStatus = null;
		public List<RefundRequestStatus> refundRequestStatuses = null;
		public int? refundAskID = null;
		public string creditCard = null;
		public int? expirationMonth = null;
		public int? expirationYear = null;
		public int? seriesID = null;
		public bool? isCreditCardStorageCancelled = null;
		public RecurringSeriesStatus? seriesStatus = null;
		public RejectingSource? rejectingSource = null;
		public bool? isTest = null;
		public bool? isGateway = null;
		public bool? isPaidOut = null;
		public bool? isChargeback = null;
		public bool? isBankPaid = null;
		public int? industryID = null;
		public string accountManager = null;
		public Guid? cartID = null;
		public string cartItemName = null;
		public CartStatus? cartStatus = null;
		public BlockedItemType? blockedItemType = null;
		public BlockedItemSource? blockedItemSource = null;
        public BlockedCardLevel? blockedCardLevel = null;
		public string blockedItemValue = null;
		public string comment = null;
		public List<DeniedStatus> deniedStatus = null;
		public DateTime? deniedDateFrom = null;
		public DateTime? deniedDateTo = null;
		public string ipCountryIso2 = null;
		public string binCountryIso2 = null;
		public TransactionAmountGroup? transactionAmountGroup = null;
		public List<int> transactionAmountTypeIDs = null;
		public List<TransactionHistoryType> transactionHistoryTypes = null;
		public ExternalCardCustomerStatus? CustomerStatus = null;
        public WireStatus? wireStatus = null;
        public WireFlag? wireFlag = null;
        public bool? wireApprovalLevel1 = null;
        public bool? wireApprovalLevel2 = null;
        public int? minimumId = null;
        public int? maximumId = null;
        public int? wireId = null;
        public bool? isPendingChargeback = null;
        public ExternalCardProviderEnum? externalCardProvider = null;
        public int? affiliateId = null;
        public byte? transactionSourceId = null;
        public byte? merchantDepartmentId = null;
		public int? deviceId = null;
		public List<int> customersIDs = null;

		private static Dictionary<string, string> _friendlyNames = null;
		public static Dictionary<string, string> FriendlyNames
		{
			get
			{
				if (_friendlyNames == null)
				{
					_friendlyNames = new Dictionary<string, string>();
					_friendlyNames.Add("cardHolderName", "Cardhoder Name");
					_friendlyNames.Add("transactionID", "Transaction Num.");
					_friendlyNames.Add("dateFrom", "From Date");
					_friendlyNames.Add("dateTo", "To Date");
					_friendlyNames.Add("cardHolderEmail", "Cardholder Email");
					_friendlyNames.Add("phoneNumber", "Cardholder Phone");
					_friendlyNames.Add("last4Digits", "Last 4 digits of CC number");
					_friendlyNames.Add("paymentsStatus", "Payout Status");
					_friendlyNames.Add("seriesID", "Series");
					_friendlyNames.Add("personalNumber", "Personal Number");
					_friendlyNames.Add("orderID", "Order Number");
					_friendlyNames.Add("replyCode", "Reply Code");
					_friendlyNames.Add("isChargeback", "Chargeback");
					_friendlyNames.Add("transactionHistoryTypes", "History Type");
				}

				return _friendlyNames;
			}
		}

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();
			FieldInfo[] fields = this.GetType().GetFields();
			foreach (FieldInfo currentField in fields)
			{
				object currentFieldValue = currentField.GetValue(this);
				if (currentFieldValue != null)
					sb.AppendLine(currentField.Name + "=" + currentFieldValue);
			}

			return sb.ToString();
		}

		/// <summary>
		/// Filter fileds friendly names
		/// </summary>
		/// <returns></returns>
		public string ToFriendlyString()
		{
			StringBuilder sb = new StringBuilder();
			FieldInfo[] fields = this.GetType().GetFields();
			foreach (FieldInfo currentField in fields)
			{
				object currentFieldValue = currentField.GetValue(this);
				if (currentFieldValue != null && currentField.Name != "merchantIDs" && currentField.Name != "transactionHistoryTypes")
					sb.AppendLine(FriendlyNames[currentField.Name] + ": " + currentFieldValue);
			}

			return sb.ToString();
		}

		public List<string> AssignedFilters
		{
			get
			{
				List<string> assignedFields = new List<string>();
				FieldInfo[] fields = this.GetType().GetFields();
				foreach (FieldInfo currentField in fields)
				{
					object currentFieldValue = currentField.GetValue(this);
					if (currentFieldValue != null)
						assignedFields.Add(currentField.Name);
				}

				return assignedFields;
			}
		}
	}
}
