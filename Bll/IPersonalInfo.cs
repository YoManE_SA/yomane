﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll
{
	public interface IPersonalInfo
	{
		string FullName { get; set; }
		string FirstName { get; set; }
		string LastName { get; set; }
		string EmailAddress { get; set; }
		string PhoneNumber { get; set; }
		string PersonalNumber { get; set; }
		DateTime? DateOfBirth { get; set; }

	}

	public class PersonalInfo : IPersonalInfo
	{
		public string FullName { get { return MakeFullName(FirstName, LastName); } set { string a, b; SplitFullName(value, out a, out b); FirstName = a; LastName = b ; } }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string EmailAddress { get; set; }
		public string PhoneNumber { get; set; }
		public string PersonalNumber { get; set; }
		public DateTime? DateOfBirth { get; set; }

		public static void SplitFullName(string fullName, out string firstName, out string lastName)
		{
			firstName = lastName = null;
			if (string.IsNullOrEmpty(fullName)) return;
			var names = fullName.Split(new char[] { '\t', ' ' }, 2, StringSplitOptions.RemoveEmptyEntries);
			if (names.Length > 0) firstName = names[0];
			if (names.Length > 1) lastName = names[1];
		}

		public static string MakeFullName(string firstName, string lastName)
		{
			return string.Format("{0} {1}", firstName, lastName).Trim();
		}

		public static void Copy(IPersonalInfo src, IPersonalInfo dest)
		{
			dest.FullName = src.FullName;
			dest.FirstName = src.FirstName;
			dest.LastName = src.LastName;
			dest.EmailAddress = src.EmailAddress;
			dest.PhoneNumber = src.PhoneNumber;
			dest.PersonalNumber = src.PersonalNumber;
			dest.DateOfBirth = src.DateOfBirth;
		}
	}
}
