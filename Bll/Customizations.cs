﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Domains;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure.VO;
using Netpay.CommonTypes;
using Netpay.Dal.Reports;

namespace Netpay.Bll
{
	/// <summary>
	/// Working with customization skins
	/// </summary>
	public static class Customizations
	{
		public static string GetCss(string cssClassName, MerchantCustomizationText text)
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine(cssClassName + "{");
			if (!string.IsNullOrEmpty(text.BackColor)) sb.AppendLine("background-color:" + text.BackColor + " !important;");
			if (!string.IsNullOrEmpty(text.ForeColor)) sb.AppendLine("color:" + text.ForeColor + " !important;");
			if (!string.IsNullOrEmpty(text.FontSize)) sb.AppendLine("font-size:" + text.FontSize + "px !important;");
			if (!string.IsNullOrEmpty(text.FontFamily)) sb.AppendLine("font-family:" + text.FontFamily + " !important;");
			sb.AppendLine("font-weight:" + (text.FontIsBold ? "bold" : "normal") + " !important;");
			sb.AppendLine("font-style:" + (text.FontIsItalic ? "italic" : "normal") + " !important;");
			sb.AppendLine("}");
			return sb.ToString();
		}

		public static string GetCss(string cssClassName, MerchantCustomizationLogo logo)
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine(cssClassName + "{");
			if (!string.IsNullOrEmpty(logo.Align)) sb.AppendLine("align:" + logo.Align + " !important;");
			if (logo.Width > 0) sb.AppendLine("width:" + logo.Width + "px !important;");
			if (logo.Height > 0) sb.AppendLine("width:" + logo.Height + "px !important;");
			sb.AppendLine("}");
			return sb.ToString();
		}

		public static string GetImageFileName(string merchantNum, MerchantCustomizationVO skin, string itemName)
		{
			return merchantNum + "_" + skin.SkinID + "_" + itemName + ".jpg";
		}

	}
}