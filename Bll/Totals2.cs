﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Domains;
using System.Collections.Specialized;
using Netpay.CommonTypes;
using Netpay.Bll;

namespace Netpay.Bll.Totals2
{

	public class CAmount
	{
		private System.Collections.Generic.Dictionary<TransactionAmountType, CountAmount> _events = new Dictionary<TransactionAmountType, CountAmount>();
		public System.Collections.Generic.Dictionary<TransactionAmountType, CountAmount> Events { get { return _events; } }
		
		public void Add(CAmount amount)
		{
			foreach (var et in amount._events) this[et.Key].Add(et.Value);
		}

		public CountAmount this[TransactionAmountType type]
		{
			get
			{
				CountAmount result = null;
				if (!_events.TryGetValue(type, out result))
				{
					result = new CountAmount();
					_events.Add(type, result);
				}
				return result;
			}
		}

		public CountAmount CalcTotal(TransactionAmountType lMin, TransactionAmountType lMax)
		{
			CountAmount result = new CountAmount();
			foreach (var et in _events)
				if (et.Key >= lMin && et.Key <= lMax) result.Add(et.Value);
			return result;
		}

		public CountAmount Total { get { return CalcTotal(TransactionAmountType.Authorize, TransactionAmountType.AmountMax); } }
		public CountAmount Fees { get { return CalcTotal(TransactionAmountType.FeeAuthorization, TransactionAmountType.FeeMax); } }
		public CountAmount DebitFees { get { return CalcTotal(TransactionAmountType.BankFeeAuthorization, TransactionAmountType.BankFeeMax); } }
		public decimal TotalPay { get { return Total.Amount - Fees.Amount; } }
		public decimal TotalProfit { get { return Fees.Amount - DebitFees.Amount; } }

		public decimal TotalFeePercent
		{
			get
			{
				decimal amtCapture = this[TransactionAmountType.Capture].Amount;
				decimal amtCapFees = this[TransactionAmountType.FeeTransaction].Amount;
				if ((amtCapture == 0) || (amtCapFees == 0)) return 0;
				return System.Math.Round((amtCapFees / amtCapture) * 100, 2);
			}
		}
	}

	public class CPayment
	{
		public enum CreateOptions
		{
			RollingReserve = 1,
			RollingRelease = 2,
			AddStorageFee = 4,
			AllOptions = 255
		}

		public int CompanyID { get; private set; }
		public int PayID { get; private set; }
		public int Currency { get; private set; }
		public string CompanyName { get; private set; }
		public decimal VAT { get; private set; }
		public bool IsGradedFees { get; private set; }
		internal Domain domain;
		private IDataReader reader;
		private CreateOptions createOptions;
		private int secKeepCurrency, secFlags, rrState, secPeriod;
		private decimal prime, exchangeRate, rrRetAmount, rrAmount, secDeposit, secKeepAmount, payPercent, curRes;
		private DateTime payDate;
		private bool apply, hasRows;
		private CAmount[] PaymentMethodAmount;

		public CPayment(Domain pDomain)
		{
			domain = pDomain;
			apply = false; hasRows = false;
			secKeepCurrency = 255;
			PaymentMethodAmount = new CAmount[(int)PaymentMethodEnum.PaymentMethodMax + 1];
		}

		~CPayment()
		{
			for (int i = 0; i < PaymentMethodAmount.Length; i++)
				if (PaymentMethodAmount[i] == null) PaymentMethodAmount[i] = null;
		}

		public void InitByPayID(int nPayID, int nCompanyID, int nCurrency, DateTime xPayDate)
		{
			if (nPayID > 0)
			{
				try
				{
					reader = Helpers.ExecReader(domain, "SELECT tblTransactionPay.*, tblCompany.CompanyName, tblCompany.SecurityPeriod, tblCompany.RREnable, tblCompany.RRAutoRet, tblCompany.RRKeepAmount, tblCompany.RRKeepCurrency, tblCompany.RRState " +
					" FROM tblTransactionPay Left Join tblCompany ON(tblCompany.ID = tblTransactionPay.CompanyID)" +
					" WHERE tblTransactionPay.id=" + nPayID);
					if (reader.Read()) InitFromPaymentRS(reader);
				}
				catch (Exception ex) 
				{
					Logger.Log(ex);
				}
				finally
				{
					reader.Close();
				}
			}
			else
			{
				try
				{
					reader = Helpers.ExecReader(domain, "SELECT tblCompany.*, tblBillingCompanys.VATamount " +
					" FROM tblCompany Left Join tblBillingCompanys ON(tblCompany.BillingCompanys_id = tblBillingCompanys.BillingCompanys_id) WHERE ID=" + nCompanyID);
					if (reader.Read()) InitFromCompanyRS(reader, nCurrency, xPayDate);
				}
				catch (Exception ex)
				{
					Logger.Log(ex);
				}
				finally
				{
					reader.Close();
				}
			}
			curRes = 0;
			if (secKeepAmount > 0)
			{
				try
				{
					reader = Helpers.ExecReader(domain, "Select SettledAmount, SettledCurrency From tblTransactionAmount Where " +
					"MerchantID=" + CompanyID + (secKeepCurrency != 255 ? "" : " And SettledCurrency=" + Currency) + " And TypeID=" + (int)PaymentMethodEnum.RollingReserve + " And Installment=0");
					while (reader.Read())
					{
						if (secKeepCurrency != 255) curRes += (new Money(Helpers.TestVar(reader["Amount"], 0m, -1m, 0m), domain.Cache.Currencies[Helpers.TestVar(reader["Currency"], 0, -1, 0)])).ConvertTo(domain.Cache.Currencies[secKeepCurrency]).Amount;
						else curRes += Helpers.TestVar(reader["Amount"], 0m, -1m, 0m);
					}
				}
				catch (Exception ex)
				{
					Logger.Log(ex);
				}
				finally
				{
					reader.Close();
				}
			}
		}

		public void InitFromCompanyRS(IDataReader xRs, int nCurrency, DateTime xPayDate)
		{
			PayID = 0;
			Currency = nCurrency;
			payDate = xPayDate;
			CompanyID = Helpers.TestVar(xRs["ID"], 0, -1, 0);
			CompanyName = xRs["CompanyName"].ToString();
			VAT = (Helpers.TestVar(xRs["IsChargeVAT"], false) ? Helpers.TestVar(xRs["VATAmount"], 0m, -1m, 0m) : 0);
			IsGradedFees = Helpers.TestVar(xRs["CFF_Currency"], 0, -1, -1) >= 0;
			secDeposit = Helpers.TestVar(xRs["SecurityDeposit"], 0m, -1m, 0m);
			secPeriod = Helpers.TestVar(xRs["SecurityPeriod"], 0, -1, 0);
			secKeepAmount = Helpers.TestVar(xRs["RRKeepAmount"], 0m, -1m, 0m);
			secKeepCurrency = Helpers.TestVar(xRs["RRKeepCurrency"], 0, -1, 0);
			rrState = Helpers.TestVar(xRs["RRState"], 0, -1, 0);
			secFlags = (Helpers.TestVar(xRs["RREnable"], false) ? 1 : 0) | (Helpers.TestVar(xRs["RRAutoRet"], false) ? 2 : 0);
			payPercent = Helpers.TestVar(xRs["PayPercent"], 0m, 100m, 100m);
			//if (Currency != 0) mExchangeRate = ConvertCurrencyRate(Currency, 0);
			//else mExchangeRate = ConvertCurrencyRate(1, Currency);
			prime = Helpers.TestVar(Helpers.ExecScalar(domain, "SELECT Prime FROM tblGlobalValues"), 0m, -1m, 0m);
		}

		public void InitFromPaymentRS(IDataReader xRs)
		{
			PayID = Helpers.TestVar(xRs["id"], 0, -1, 0);
			Currency = Helpers.TestVar(xRs["Currency"], 0, -1, 0);
			CompanyID = Helpers.TestVar(xRs["CompanyID"], 0, -1, 0);
			CompanyName = xRs["CompanyName"].ToString();
			VAT = (Helpers.TestVar(xRs["IsChargeVAT"], false) ? Helpers.TestVar(xRs["VATAmount"], 0m, -1m, 0m) : 0);
			IsGradedFees = ((bool?)xRs["IsGradedFees"] ?? false);
			payDate = Helpers.TestVar(xRs["Paydate"], DateTime.MinValue, DateTime.MaxValue, DateTime.MinValue);
			secDeposit = Helpers.TestVar(xRs["SecurityDeposit"], 0m, -1m, 0m);
			secPeriod = Helpers.TestVar(xRs["SecurityPeriod"], 0, -1, 0);
			secKeepAmount = Helpers.TestVar(xRs["RRKeepAmount"], 0m, -1m, 0m);
			secKeepCurrency = Helpers.TestVar(xRs["RRKeepCurrency"], 0, -1, 0);
			rrState = Helpers.TestVar(xRs["RRState"], 0, -1, 0);
			payPercent = Helpers.TestVar(xRs["PayPercent"], 0m, 100m, 100m);
			secFlags = (Helpers.TestVar(xRs["RREnable"], false) ? 1 : 0) | (Helpers.TestVar(xRs["RRAutoRet"], false) ? 2 : 0);
			exchangeRate = Helpers.TestVar(xRs["ExchangeRate"], 0m, -1m, 0m);
			prime = Helpers.TestVar(xRs["PrimePercent"], 0m, -1m, 0m);
			//nCompanyName = xRs("BillingCompanyName");
		}

		public int CreatePayment(DateTime xPayDate, CreateOptions createOptions)
		{
			this.createOptions = createOptions;
			if (PayID > 0)
			{
				AddTransactions("");
			}
			else
			{
				try
				{
					reader = Helpers.ExecReader(domain, "SELECT tblBillingCompanys.* FROM tblBillingCompanys LEFT JOIN tblCompany ON(tblCompany.BillingCompanys_id = tblBillingCompanys.BillingCompanys_id) " +
					" WHERE tblCompany.ID=" + CompanyID);
					if (reader.Read())
					{
						payDate = xPayDate;
						string sSQL = "Insert Into tblTransactionPay " +
							" (PayDate, CompanyID, TerminalType, PrimePercent, ExchangeRate, Currency, BillingCompanys_id, BillingLanguageShow, BillingCurrencyShow, BillingCompanyName, BillingCompanyAddress, BillingCompanyNumber, BillingCompanyEmail, IsGradedFees, IsChargeVAT, VATamount, SecurityDeposit, PayPercent)VALUES(" +
							"'" + payDate + "'," + CompanyID + "," + 1 + "," + prime + "," + exchangeRate + "," + Currency + "," + reader["BillingCompanys_id"] + ",'" +
							reader["LanguageShow"] + "'," + reader["CurrencyShow"] + ",'" + reader["name"] + "','" + reader["address"] + "','" +
							reader["number"] + "','" + reader["email"] + "'," + (IsGradedFees ? 1 : 0) + "," + (VAT > 0 ? 1 : 0) + "," + VAT + "," + secDeposit + "," + payPercent + ")";
						//Response.Write(sSQL + "<br>")
						Helpers.ExecSql(domain, sSQL);
						PayID = Helpers.TestVar(Helpers.ExecScalar(domain, "Select Max(id) From tblTransactionPay Where companyID=" + CompanyID), 0, -1, 0);
					}
				}
				catch (Exception ex)
				{
					Logger.Log(ex);
				}
				finally
				{
					reader.Close();
				}

				//Copy Fees From tblCompanyCreditFees To tblTransactionPayFees
				Helpers.ExecSql(domain, "Insert Into tblTransactionPayFees (CCF_TransactionPayID, CCF_CompanyID, CCF_CurrencyID, CCF_PaymentMethod, CCF_ExchangeTo, CCF_MaxAmount, CCF_PercentFee, CCF_FixedFee, CCF_FailFixedFee, CCF_ApproveFixedFee, CCF_RefundFixedFee, CCF_ClarificationFee, CCF_CBFixedFee, CCF_ListBINs)" +
					"Select " + PayID + ",CCF_CompanyID, CCF_CurrencyID, CCF_PaymentMethod, CCF_ExchangeTo, CCF_MaxAmount, CCF_PercentFee, CCF_FixedFee, CCF_FailFixedFee, CCF_ApproveFixedFee, CCF_RefundFixedFee, CCF_ClarificationFee, CCF_CBFixedFee, CCF_ListBINs From tblCompanyCreditFees Where CCF_CompanyID=" + CompanyID + " And CCF_ExchangeTo=" + Currency);

				Helpers.ExecSql(domain, "Insert Into tblPaymentFeesFloor (CFF_PayID, CFF_CompanyID, CFF_TotalTo, CFF_Precent)" +
					"Select " + PayID + ",CFF_CompanyID, CFF_TotalTo, CFF_Precent From tblCompanyFeesFloor Where CFF_CompanyID=" + CompanyID);
			}
			apply = true;
			return PayID;
		}

		public void UpdateCach()
		{
			if (PaymentMethodAmount != null)
			{
			/*
				CAmount ttls = Totals;
				NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
				foreach (var e in ttls.Events)
				{
					if (e.Value.Count == 0 && e.Value.Amount == 0m) continue;
					Dal.Netpay.tblSettlementAmount sa = new Dal.Netpay.tblSettlementAmount();
					sa.InsertDate = DateTime.Now;
					sa.MerchantID = CompanyID;
					sa.SettledCount = e.Value.Count;
					sa.SettledAmount = e.Value.Amount;
					sa.TypeID = (int)e.Key;
					sa.SettlementID = PayID;
					sa.SettledCurrency = Currency;
					dc.tblSettlementAmounts.InsertOnSubmit(sa);
					dc.SubmitChanges();
				}
			 **/
			}
		}

		public bool UpdatePayment()
		{
			if (PayID != 0) Helpers.ExecSql(domain, "Delete From tblSettlementAmount Where MerchantID=" + CompanyID + " And SettlementID=" + PayID + " And TypeID=" + (int)TransactionAmountType.RollingReserve);
			if (RollingReserve != 0) AddPaymentAmount(RollingReserve, "Rolling Reserve (" + (rrState == 4 ? "fixed external" : secDeposit.ToString("0.00") + "%") + ")", TransactionAmountType.RollingReserve, payDate.AddMonths(secPeriod));
			if (PayID != 0) Helpers.ExecSql(domain, "Delete From tblSettlementAmount Where MerchantID=" + CompanyID + " And SettlementID=" + PayID + " And TypeID=" + (int)TransactionAmountType.DebitorDirectPayment);
			if (payPercent != 100) AddPaymentAmount(TotalNotPayedAmount, "Deposit Of " + payPercent.ToString("0.00") + "%", TransactionAmountType.DebitorDirectPayment, null);

			//if (payPercent != 0) AddPaymentAmount(TotalNotPayedAmount, "Direct Payment From", TransactionAmountType.DebitorDirectPayment);
			if (hasRows)
			{
				Helpers.ExecSql(domain, "UPDATE tblTransactionPay Set transTotal=" + TotalAmount + ", TransChargeTotal=" + TotalFees + ", TransPayTotal=" + Total + ", transRollingReserve=" + RollingReserve + " WHERE ID=" + PayID);
				//UpdateCach();
				//oledbData.execute "UPDATE tblWireMoney SET WireAmount=" + Total + "-wireFee WHERE WireType=1 AND WireSourceTbl_id=" + mPayID
				return true;
			}
			else
			{
				DeletePayment(domain.Host, PayID, CompanyID);
				return false;
			}
		}

		public int AddPaymentAmount(decimal amount, string comment, TransactionAmountType tat, DateTime? dateRelease)
		{
			if (amount == 0m) return 0;
			if (PaymentMethodAmount[(int)PaymentMethodEnum.ManualFees] == null) PaymentMethodAmount[(int)PaymentMethodEnum.ManualFees] = new CAmount();
			PaymentMethodAmount[(int)PaymentMethodEnum.ManualFees][tat].Add(amount, 1);
			/*
			if (apply)
			{
				NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
				Dal.Netpay.tblSettlementAmount ta = new Dal.Netpay.tblSettlementAmount();
				ta.SettledCount = 1;
				ta.SettledAmount = -amount;
				ta.SettledCurrency = Currency;
				ta.InsertDate = DateTime.Now;
				ta.MerchantID = CompanyID;
				ta.InsertDate = payDate;
				ta.SettlementID = PayID;
				ta.Description = comment;
				ta.ReleaseDate = dateRelease;
				ta.TypeID = (int)tat;
				dc.tblSettlementAmounts.InsertOnSubmit(ta);
				dc.SubmitChanges();
				return ta.ID;
			}
			*/
			return 0;
		}

		public int AddPaymentAmountSql(string sql, string comment, TransactionAmountType tat)
		{
			//oledbData.CommandTimeout = 600
			try
			{
				reader = Helpers.ExecReader(domain, sql);
				if (reader.Read())
				{
					comment = comment.Replace("%1", Helpers.TestVar(reader[1], -1, ""));
					if (reader.FieldCount > 2) comment = comment.Replace("%2", Helpers.TestVar(reader[2], -1, ""));
					return AddPaymentAmount(Helpers.TestVar(reader[0], 0m, -1m, 0m), comment, tat, null);
				}
			}
			catch (Exception ex)
			{
				Logger.Log(ex);
			}
			finally
			{
				reader.Close();
			}

			return 0;
		}

		public void AddOtherPaymentElements()
		{
			if (((createOptions & CreateOptions.AddStorageFee) != 0) || !apply)
			{
                AddPaymentAmountSql("Select -(Count(*) * StorageFee), Count(*), StorageFee From tblCCStorage Left Join Setting.SetMerchantSettlement mcs ON(tblCCStorage.companyID = mcs.Merchant_ID And mcs.Currency_ID=" + Currency + ") WHERE PayID=0 AND CompanyID=" + CompanyID + " Group By StorageFee",
					"CC storage fee (" + domain.Cache.Countries[Currency].Name + "%2 X %1)", TransactionAmountType.FeeCCStorage);
				if (apply) Helpers.ExecSql(domain, "Update tblCCStorage Set PayID=" + PayID + " Where PayID=0 AND CompanyID=" + CompanyID);
			}
			if (((createOptions & CreateOptions.RollingRelease) != 0) || !apply) CalcRetReserve();
		}

		public CAmount GetAmountByPaymentMethod(PaymentMethodEnum pm, int oldPM)
		{
			if (pm == PaymentMethodEnum.Unknown)
			{ //old style detection
				if (oldPM == 1) return GetAmountByPaymentMethod(PaymentMethodEnum.CC_MIN, 0);
				else if (oldPM == 2) return GetAmountByPaymentMethod(PaymentMethodEnum.EC_MIN, 0);
				else return GetAmountByPaymentMethod(PaymentMethodEnum.ManualFees, 0);
			}
			else
			{
				if (PaymentMethodAmount[(int)pm] == null) PaymentMethodAmount[(int)pm] = new CAmount();
				return PaymentMethodAmount[(int)pm];
			}
		}

		private string GetTransIDUpTo(string xWhere, decimal nAmount)
		{
			string pRet = "";
			if (xWhere != "") xWhere = " And " + xWhere;
			xWhere = "MerchantID=" + CompanyID + " And SettledCurrency=" + Currency + " And IsNull(SettlementID, 0) = 0 " + xWhere;
			xWhere = "Select ID, SettledAmount From tblTransactionAmount Where " + xWhere + " And TypeID IN(" + (int)TransactionAmountType.FeeTransaction + ", " + (int)TransactionAmountType.FeeLine + ") Order By ID Desc";
			try
			{
				reader = Helpers.ExecReader(domain, xWhere);
				while (reader.Read())
				{
					nAmount = nAmount - Helpers.TestVar(reader["SettledAmount"], 0m, -1m, 0m);
					if (nAmount < 0) break;
					pRet += "," + reader["ID"];
				}
			}
			catch (Exception ex)
			{
				Logger.Log(ex);
			}
			finally
			{
				reader.Close();
			}

			if (pRet.Length > 1) pRet = pRet.Substring(1);
			return pRet;
		}

		public void AddTransactions(string xWhere)
		{
			int xPayID;
			string sWhere;
			//oleDbData.CommandTimeout = 180
			xPayID = (apply ? 0 : PayID);
			string sql = "SELECT TypeID, Count(ID) As evCount, Sum(SettledAmount) As evAmount From tblSettlementAmount Where SettlementID=" + xPayID + " Group By TypeID";
			//Logger.Log(sql);
			try
			{
				reader = Helpers.ExecReader(domain, sql);
				while (reader.Read())
				{
					CAmount pSumItem = GetAmountByPaymentMethod((PaymentMethodEnum)PaymentMethodEnum.RollingReserve, 1);
					pSumItem[(TransactionAmountType)Helpers.TestVar(reader["TypeID"], 0, -1, 0)].Add(Helpers.TestVar(reader["evAmount"], 0m, -1m, 0m), Helpers.TestVar(reader["evCount"], 0, -1, 0));
				}
			}
			catch (Exception ex)
			{
				Logger.Log(ex);
			}
			finally
			{
				reader.Close();
			}

			if (!string.IsNullOrEmpty(xWhere)) xWhere = " And " + xWhere;
			sWhere = "tblTransactionAmount.MerchantID=" + CompanyID + " And tblTransactionAmount.Currency=" + Currency + " And IsNull(SettlementID, 0)=" + xPayID;
			xWhere = "SELECT PaymentMethod, TypeID, Count(tblTransactionAmount.ID) As evCount, Sum(tblTransactionAmount.Amount) As evAmount" +
				" From tblTransactionAmount Left Join tblCompanyTransPass ON(tblTransactionAmount.TransPassID = tblCompanyTransPass.ID)" +
				" Where " + sWhere + xWhere + " Group By PaymentMethod, TypeID";
			Logger.Log(xWhere);
			try
			{
				reader = Helpers.ExecReader(domain, xWhere);
				while (reader.Read())
				{
					hasRows = true;
					CAmount pSumItem = GetAmountByPaymentMethod((PaymentMethodEnum)Helpers.TestVar(reader["PaymentMethod"], 0, -1, 0), 1);
					pSumItem[(TransactionAmountType)Helpers.TestVar(reader["TypeID"], 0, -1, 0)].Add(Helpers.TestVar(reader["evAmount"], 0m, -1m, 0m), Helpers.TestVar(reader["evCount"], 0, -1, 0));
				}
			}
			catch (Exception ex)
			{
				Logger.Log(ex);
			}
			finally
			{
				reader.Close();
			}

			if (apply) Helpers.ExecSql(domain, "Update tblTransactionAmount Set SettlementID=" + PayID + ", SettledCurrency=" + Currency + ", SettlementDate='" + payDate + "' Where " + sWhere + xWhere);
			//oleDbData.CommandTimeout = 30
		}

		public bool HasRows { get { return hasRows; } }
		public bool HasPaymentMethodTotal(PaymentMethodEnum lPM) { return PaymentMethodAmount[(int)lPM] != null; }

		public CAmount PaymentMethodTotal(PaymentMethodEnum lMin, PaymentMethodEnum lMax)
		{
			CAmount pRet = new CAmount();
			for (int i = (int)lMin; i <= (int)lMax; i++)
				if (PaymentMethodAmount[i] != null) pRet.Add(PaymentMethodAmount[i]);
			return pRet;
		}

		public decimal RollingReserve
		{
			set { if (rrState == 4) rrAmount = -value; }
			get
			{
				decimal pRet = 0;
				if (PayID > 0 && !apply) pRet = rrAmount;
				else if (rrState == 4) pRet = rrAmount;
				else
				{
					if (rrState == 2)
					{
						string sSQL = "Select Top 1 ID From tblSettlementAmount Where MerchantID=" + CompanyID +
							 " And TypeID=" + (int)TransactionAmountType.RollingReserve + " And InsertDate < convert(datetime, '" + payDate.AddMonths(-secPeriod).ToString("yyyy-MM-dd") + "', 120)";
						if (Helpers.TestVar(Helpers.ExecScalar(domain, sSQL), 0, -1, 0) > 0) secFlags &= ~1;
						else secFlags |= 1;
					}
					var RRAmt = -PaymentMethodTotal(PaymentMethodEnum.CC_MIN, PaymentMethodEnum.EC_MAX)[TransactionAmountType.Capture].Amount; //* (mPayPercent / 100)
					if ((((rrState == 1 || rrState == 2) && ((secFlags & 1) == 1)) || rrState == 3) && (((createOptions & CreateOptions.RollingReserve) != 0) || !apply) && RRAmt > 0)
					{
						if (rrState == 1)
						{
							pRet = -(RRAmt * (secDeposit / 100));
						}
						else
						{
							decimal nMax = (secKeepAmount - curRes);
							if (nMax < 0)
							{
								pRet = 0;
							}
							else
							{
								pRet = (RRAmt * (secDeposit / 100));
								if (nMax < pRet) pRet = nMax;
								pRet = -pRet;
							}
						}
					}
				}
				return pRet;
			}
		}

		public void AddRetReserve(int nID)
		{
			try
			{
				reader = Helpers.ExecReader(domain, "Select ID, SettledAmount, SettledCurrency, InsertDate From tblSettlementAmount " +
				"Where MerchantID=" + CompanyID + " And SettledCurrency=" + Currency + " And ReferenceNumber=0 And ID=" + nID);
				if (reader.Read()) IAddRetReserve(reader);
			}
			catch (Exception ex)
			{
				Logger.Log(ex);
			}
			finally
			{
				reader.Close();
			}
		}

		private int IAddRetReserve(IDataReader iRs)
		{
			int pRet = AddPaymentAmount(Helpers.TestVar(iRs["SettledAmount"], 0m, -1m, 0m), "Release Reserve (" + Helpers.TestVar(iRs["SettlementDate"], DateTime.MinValue, DateTime.MaxValue, DateTime.MinValue).ToString("yyyy/MM/dd") + ")", TransactionAmountType.RollingReserve, null);
			rrRetAmount += Helpers.TestVar(iRs["SettledAmount"], 0m, -1m, 0m);
			if (apply) Helpers.ExecSql(domain, "Update tblSettlementAmount Set ReferenceNumber=" + pRet + " Where ID=" + iRs["ID"]);
			return pRet;
		}

		private void CalcRetReserve()
		{
			if ((rrState == 1 && (secFlags & 2) > 0) || rrState == 2 || rrState == 3)
			{
				try
				{
					reader = Helpers.ExecReader(domain, "Select ID, SettledAmount, SettledCurrency, SettlementDate From tblSettlementAmount Where " +
					"MerchantID=" + CompanyID + " And SettledCurrency=" + Currency + " And TypeID=" + (int)TransactionAmountType.RollingReserve + " And IsNull(ReferenceNumber, 0) = 0 And Cast(ReleaseDate as date) <= convert(datetime, '" + payDate.AddMonths(-secPeriod).ToString("yyyy-MM-dd") + "', 120)");
					while (reader.Read())
					{
						if (rrState == 3)
						{
							decimal nItemValue;
							if (secKeepCurrency != 255) nItemValue = new Money(Helpers.TestVar(reader["SettledAmount"], 0m, -1m, 0m), domain.Cache.Currencies[Helpers.TestVar(reader["SettledCurrency"], 0, -1, 0)]).ConvertTo(domain.Cache.Currencies[secKeepCurrency]).Amount;
							else nItemValue = Helpers.TestVar(reader["SettledAmount"], 0m, -1m, 0m);
							if (curRes < secKeepAmount) break;
							curRes = curRes - nItemValue;
						}
						IAddRetReserve(reader);
					}
				}
				catch (Exception ex)
				{
					Logger.Log(ex);
				}
				finally
				{
					reader.Close();
				}
			}
		}

		public decimal ReserveReturn { get { return rrRetAmount; } }
		public decimal ReserveTotal { get { return RollingReserve + rrRetAmount; } }

		public CAmount Totals { get { return PaymentMethodTotal(0, PaymentMethodEnum.PaymentMethodMax); } }
		public decimal TotalFees { get { return Totals.Fees.Amount; } }
		public decimal TotalProcessAmount { get { return PaymentMethodTotal(PaymentMethodEnum.CC_MIN, PaymentMethodEnum.EC_MAX).Total.Amount + BankFees.Total.Amount; } }
		public decimal TotalSysAdminAmount { get { return PaymentMethodTotal(PaymentMethodEnum.ManualFees, PaymentMethodEnum.SystemFees).Total.Amount; } }

		public CAmount BankFees { get { return PaymentMethodTotal(PaymentMethodEnum.BankFees, PaymentMethodEnum.BankFees); } }
		public decimal TotalAmount { get { return (TotalProcessAmount * (payPercent / 100)) + ReserveTotal + TotalSysAdminAmount; } }

		public decimal TotalNotPayedAmount
		{
			get
			{
				if (payPercent == 100m) return 0m;
				return (TotalProcessAmount * (NotPayPercent / 100));
			}
		}
		public decimal NotPayPercent { get { return 100 - payPercent; } }
		public decimal VATAmount { get { return TotalFees * (VAT + 1); } }
		public decimal Total { get { return TotalAmount - VATAmount; } }


		public static CPayment GetPayment(string domainHost, int payID, int companyID, int currency, DateTime payDate, string xWhere)
		{
			CPayment pRet = new CPayment(DomainsManager.GetDomain(domainHost));
			pRet.InitByPayID(payID, companyID, currency, payDate);
			pRet.AddTransactions(xWhere);
			if (payID == 0) pRet.AddOtherPaymentElements();
			return pRet;
		}

		public static void DeletePayment(string domainHost, int payID, int companyID)
		{
			Domain d = DomainsManager.GetDomain(domainHost);
			Helpers.ExecSql(d, "Delete From tblSettlementAmount Where MerchantID=" + companyID + " And SettlementID=" + payID);
			Helpers.ExecSql(d, "Delete From tblTransactionAmount Where MerchantID=" + companyID + " And SettlementID=" + payID + " And (IsNull(TransPassID, 0) = 0 And IsNull(TransApprovalID, 0) = 0 And IsNull(TransPendingID, 0) = 0 And IsNull(TransFailID, 0) = 0)");
			Helpers.ExecSql(d, "Update tblTransactionAmount Set SettlementID=null Where MerchantID=" + companyID + " And SettlementID=" + payID);
			Helpers.ExecSql(d, "Update tblCCStorage Set PayID=0 Where CompanyID=" + companyID + " And PayID=" + payID);
			Helpers.ExecSql(d, "Delete From tblPaymentFeesFloor Where CFF_PayID=" + payID);
			Helpers.ExecSql(d, "Delete From tblTransactionPayFees Where CCF_TransactionPayID=" + payID);
			Helpers.ExecSql(d, "Delete From tblTransactionPay Where ID=" + payID);
			Helpers.ExecSql(d, "Delete From tblWireMoney Where WireType=1 And WireSourceTbl_id=" + payID);
		}
	}

	public class AffiliateAmount
	{
		public decimal Amount, FeePercent;
		public string CreditInfo;
		public decimal Total
		{
			get
			{
				if (FeePercent == 0m) return 0m;
				return Amount * (FeePercent / 100);
			}
		}
	}

	public class AffiliateFeeItem
	{
		public PaymentMethodEnum PaymentMethod;
		public System.Collections.Generic.List<AffiliateAmount> Items = new List<AffiliateAmount>();
		public decimal CreditAmount;

		public AffiliateAmount AddAmount(decimal nPercent, decimal nAmount, string creditInfo)
		{
			AffiliateAmount pItem = new AffiliateAmount();
			Items.Add(pItem);
			pItem.Amount = nAmount;
			pItem.FeePercent = nPercent;
			pItem.CreditInfo = creditInfo;
			return pItem;
		}

		public decimal Total
		{
			get
			{
				decimal pRet = 0;
				for (int i = 0; i < Items.Count; i++) pRet = pRet + Items[i].Total;
				return pRet;
			}
		}
	}

	public class AffiliateTotalLine
	{
		public string Text;
		public int Quantity;
		public decimal Amount;
		public decimal Total { get { return Quantity * Amount; } }
	}

	public class AffiliateFees
	{
		public int AffiliateID, AFP_ID;
		public CPayment Payment;
		public AffiliateFeeItem[] PaymentMethodAmount;
		public AffiliateTotalLine[] PaymentLines;

		private AffiliateFees(CPayment xPayment) { Payment = xPayment; PaymentMethodAmount = new AffiliateFeeItem[(int)PaymentMethodEnum.PaymentMethodMax + 1]; }
		public bool HasPaymentMethodAmount(PaymentMethodEnum lPM) { return PaymentMethodAmount[(int)lPM] != null; }

		public AffiliateFeeItem GetPaymentMethodAmount(PaymentMethodEnum lPM)
		{
			if (PaymentMethodAmount[(int)lPM] == null)
			{
				PaymentMethodAmount[(int)lPM] = new AffiliateFeeItem();
				PaymentMethodAmount[(int)lPM].PaymentMethod = lPM;
			}
			return PaymentMethodAmount[(int)lPM];
		}

		public decimal SubTotal
		{
			get
			{
				decimal pRet = 0;
				for (int i = 0; i < PaymentMethodAmount.Length; i++)
					if (PaymentMethodAmount[i] != null) pRet += PaymentMethodAmount[i].Total;
				return pRet;
			}
		}

		public decimal Total
		{
			get
			{
				decimal pRet = 0;
				for (int i = 0; i < PaymentLines.Length; i++)
					if (PaymentLines[i] != null) pRet += PaymentLines[i].Total;
				return pRet + SubTotal;
			}
		}

		public void UpdatePayment()
		{
			int AFCompanyID;
			decimal feeRatio = Total / Payment.Total;
			if (AFP_ID != 0)
			{
				Helpers.ExecSql(Payment.domain, "Update tblAffiliatePayments AFP_FeeRatio=" + feeRatio.ToString("0.00") + ", AFP_PaymentAmount=" + Total.ToString("0.00") + " Where AFP_ID=" + AFP_ID);
			}
			else
			{
				Helpers.ExecSql(Payment.domain, "Insert Into tblAffiliatePayments(AFP_Affiliate_ID, AFP_TransPaymentID, AFP_CompanyID, AFP_FeeRatio, AFP_PaymentAmount, AFP_PaymentCurrency)Values(" +
					AffiliateID + "," + Payment.PayID + "," + Payment.CompanyID + "," + feeRatio.ToString("0.00") + "," + Total.ToString("0.00") + "," + Payment.Currency + ")");
				AFP_ID = Helpers.TestVar(Helpers.ExecScalar(Payment.domain, "Select Max(AFP_ID) From tblAffiliatePayments Where AFP_Affiliate_ID=" + AffiliateID), 0, -1, 0);
				Helpers.ExecSql(Payment.domain, "INSERT INTO tblAffiliateFeeSteps(AFS_AffiliateID, AFS_UpToAmount, AFS_Currency, AFS_PercentFee, AFS_PaymentMethod, AFS_CompanyID, AFS_CalcMethod, AFS_SlicePercent, AFS_AFPID)" +
					"Select AFS_AffiliateID, AFS_UpToAmount, AFS_Currency, AFS_PercentFee, AFS_PaymentMethod, AFS_CompanyID, AFS_CalcMethod, AFS_SlicePercent, " + AFP_ID + " From tblAffiliateFeeSteps Where AFS_AffiliateID=" + AffiliateID + " And AFS_AFPID is null And AFS_CompanyID=" + Payment.CompanyID + " And AFS_Currency=" + Payment.Currency);
			}
			AFCompanyID = Helpers.TestVar(Helpers.ExecScalar(Payment.domain, "Select AFCompanyID From tblAffiliates Where affiliates_id=" + AffiliateID), 0, -1, 0);
			if (AFCompanyID > 0)
			{
				//sSQL = "INSERT INTO tblCompanyTransPass(companyID, TransactionTypeID, CreditType, IPAddress, Amount, Currency, OCurrency, Comment, paymentMethod_Id, paymentMethodDisplay, MerchantPD, paymentMethod, UnsettledAmount, UnsettledInstallments)" + _
				//    " VALUES(" + AFCompanyID + ", 10, 1, '" + dbpages.DBText(Request.ServerVariables("REMOTE_ADDR")) + "', " + Round(xValue, 2) + "," + iReader("Currency") + "," + iReader("Currency") + ", '" + "Pay For Settlment #" + iReader("id") + " - " + dbpages.FormatCurr(iReader("Currency"), iReader("TransPayTotal")) + "', " + 4 + ", '" + "Affiliate Payment" + "', GETDATE(), " + ePaymentMethod.PMD_Admin + "," + Round(xValue, 2) + ", 1)"
				//dbPages.ExecSql(sSQL)
			}
		}

		public void LoadLines()
		{
			System.Collections.Generic.List<AffiliateTotalLine> parr = new System.Collections.Generic.List<AffiliateTotalLine>();
			IDataReader mRs = null;
			try
			{
				mRs = Helpers.ExecReader(Payment.domain, "Select * From tblAffiliatePaymentsLines Where AFPL_AFP_ID=" + AFP_ID);
				while (mRs.Read())
				{
					AffiliateTotalLine pItem = new AffiliateTotalLine();
					pItem.Text = Helpers.TestVar(mRs["AFPL_Text"], -1, "");
					pItem.Quantity = Helpers.TestVar(mRs["AFPL_Quantity"], 0, -1, 0);
					pItem.Amount = Helpers.TestVar(mRs["AFPL_Amount"], 0m, -1m, 0m);
					parr.Add(pItem);
				}
			}
			catch (Exception ex)
			{
				Logger.Log(ex);
			}
			finally
			{
				mRs.Close();
			}

			PaymentLines = parr.ToArray();
		}

		public static bool AddSteps(AffiliateFees pRet, IDataReader mRs, PaymentMethodEnum PmdID, bool bRead)
		{
			int oldPmdID = -1;
			decimal lStepTotalAmount, lineSubTotal = 0, nPercentFee, nSlicePercent;
			AffiliateFeeItem pItem = pRet.GetPaymentMethodAmount(PmdID);
			CAmount lineTotal = pRet.Payment.PaymentMethodTotal(PmdID, PmdID);
			lStepTotalAmount = 0;
			if (bRead)
			{
				oldPmdID = Helpers.TestVar(mRs["AFS_PaymentMethod"], 0, -1, 0);
				switch ((PartnerFeeCalc)Helpers.TestVar(mRs["AFS_CalcMethod"], 0, -1, 0))
				{
					case PartnerFeeCalc.Volume:
						lineSubTotal = lineTotal.Total.Amount;
						break;
					case PartnerFeeCalc.Fees:
						lineSubTotal = lineTotal.Fees.Amount;
						break;
					case PartnerFeeCalc.MercSale_TermBuy:
						lineSubTotal = lineTotal.TotalProfit;
						break;
					case PartnerFeeCalc.MercSale_AffiBuy:
						lineSubTotal = lineTotal.Total.Amount;
						break;
					case PartnerFeeCalc.MercSale_AffiBuySPercent:
						lineSubTotal = lineTotal.Total.Amount;
						break;
				}
			}
			pItem.CreditAmount = lineSubTotal;
			while (bRead)
			{
				string sCreditInfo = null;
				if (oldPmdID != Helpers.TestVar(mRs["AFS_PaymentMethod"], 0, -1, 0)) break;
				nPercentFee = Helpers.TestVar(mRs["AFS_PercentFee"], 0m, -1m, 0m);
				nSlicePercent = Helpers.TestVar(mRs["AFS_SlicePercent"], 0m, -1m, 0m) / 100;
				decimal nStepAmount = (lineSubTotal > Helpers.TestVar(mRs["AFS_UpToAmount"], 0m, -1m, 0m) ? Helpers.TestVar(mRs["AFS_UpToAmount"], 0m, -1m, 0m) : lineSubTotal) - lStepTotalAmount;
				if ((PartnerFeeCalc)Helpers.TestVar(mRs["AFS_CalcMethod"], 0, -1, 0) == PartnerFeeCalc.MercSale_AffiBuy)
				{
					sCreditInfo = "(" + lineTotal.TotalFeePercent.ToString("0.00") + "% - " + nPercentFee.ToString("0.00") + "%) X " + (nSlicePercent * 100m).ToString("0.00") + "% = " + ((lineTotal.TotalFeePercent - nPercentFee) * nSlicePercent).ToString("0.00") + "%";
					nPercentFee = lineTotal.TotalFeePercent - nPercentFee;
				}
				else if (Helpers.TestVar(mRs["AFS_CalcMethod"], 0, -1, 0) == (int)PartnerFeeCalc.MercSale_AffiBuySPercent)
				{
					decimal nStepPercent = Helpers.TestVar(mRs["AFS_UpToAmount"], 0m, -1m, 0m) < lineTotal.TotalFeePercent ? Helpers.TestVar(mRs["AFS_UpToAmount"], 0m, -1m, 0m) : lineTotal.TotalFeePercent;
					sCreditInfo = "(" + nStepPercent + "% - " + nPercentFee.ToString("0.00") + "%) X " + (nSlicePercent * 100).ToString("0.0") + "% = " + ((nStepPercent - nPercentFee) * nSlicePercent).ToString("0.00") + "%";
					nPercentFee = nStepPercent - nPercentFee;
					nStepAmount = lineSubTotal;
				}
				pItem.AddAmount(nPercentFee * nSlicePercent, nStepAmount, sCreditInfo);
				if (lineSubTotal < Helpers.TestVar(mRs["AFS_UpToAmount"], 0m, -1m, 0m)) break;
				lStepTotalAmount = Helpers.TestVar(mRs["AFS_UpToAmount"], 0m, -1m, 0m);
				bRead = mRs.Read();
			}
			return bRead;
		}

		public static void AddAffiliateFees(AffiliateFees pRet, int transType)
		{
			IDataReader mRs = null;
			bool bRead = false;
			try
			{
				mRs = Helpers.ExecReader(pRet.Payment.domain, "Select * From tblAffiliateFeeSteps Where AFS_AffiliateID=" + pRet.AffiliateID + " And AFS_CompanyID=" + pRet.Payment.CompanyID + " And AFS_Currency=" + pRet.Payment.Currency + " And AFS_AFPID" + (pRet.AFP_ID != 0 ? "=" + pRet.AFP_ID : " is null") + " And AFS_PaymentMethod <> 0 And AFS_TransType=" + transType + " Order By AFS_PaymentMethod Asc, AFS_UpToAmount Asc");
				bRead = mRs.Read();
				for (int i = (int)PaymentMethodEnum.CC_MIN; i < (int)PaymentMethodEnum.PaymentMethodMax; i++)
				{
					if (pRet.Payment.HasPaymentMethodTotal((PaymentMethodEnum)i))
					{
						if (bRead)
						{
							while (Helpers.TestVar(mRs["AFS_PaymentMethod"], 0, -1, 0) < i)
							{
								bRead = mRs.Read();
								if (!bRead) break;
							}
						}
						if (bRead) bRead = AddSteps(pRet, mRs, (PaymentMethodEnum)i, bRead);
					}
				}
			}
			catch (Exception ex)
			{
				Logger.Log(ex);
			}
			finally
			{
				mRs.Close();
			}

			for (int i = (int)PaymentMethodEnum.CC_MIN; i < (int)PaymentMethodEnum.PaymentMethodMax; i++)
			{
				if (pRet.Payment.HasPaymentMethodTotal((PaymentMethodEnum)i))
				{
					if (!pRet.HasPaymentMethodAmount((PaymentMethodEnum)i))
					{
						try
						{
							mRs = Helpers.ExecReader(pRet.Payment.domain, "Select * From tblAffiliateFeeSteps Where AFS_AffiliateID=" + pRet.AffiliateID + " And AFS_CompanyID=" + pRet.Payment.CompanyID + " And AFS_Currency=" + pRet.Payment.Currency + " And AFS_AFPID" + (pRet.AFP_ID != 0 ? "=" + pRet.AFP_ID : " is null") + " And AFS_PaymentMethod = 0 And AFS_TransType=" + transType + " Order By AFS_PaymentMethod Asc, AFS_UpToAmount Asc");
							bRead = mRs.Read();
							if (bRead) AddSteps(pRet, mRs, (PaymentMethodEnum)i, bRead);
						}
						catch (Exception ex)
						{
							Logger.Log(ex);
						}
						finally
						{
							mRs.Close();
						}
					}
				}
			}
		}

		public static AffiliateFees CalcAffiliateFees(int xAFP_ID, CPayment xPayment, int xAffID)
		{
			AffiliateFees pRet = new AffiliateFees(xPayment);
			pRet.AffiliateID = xAffID; pRet.AFP_ID = xAFP_ID;
			if (xAFP_ID != 0) pRet.LoadLines();
			AddAffiliateFees(pRet, 0);
			//AddAffiliateFees(pRet, 1);
			//AddAffiliateFees(pRet, 2);
			return pRet;
		}
	}

	internal static class Helpers
	{
		public static long ExecSql(Domain d, string sqlStr)
		{
			var dbCon = new SqlConnection(d.Sql1ConnectionString);
			var dbCom = new SqlCommand(sqlStr, dbCon);
			try
			{
				dbCon.Open();
				return dbCom.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				Logger.Log(ex, sqlStr);
				throw;
			}
			finally
			{
				dbCon.Close();
				dbCom.Dispose();
				dbCon.Dispose();
			}
		}

		public static object ExecScalar(Domain d, string sqlStr)
		{
			var dbCon = new SqlConnection(d.Sql1ConnectionString);
			var dbCom = new SqlCommand(sqlStr, dbCon);
			dbCon.Open();
			try
			{
				return dbCom.ExecuteScalar();
			}
			catch (Exception ex)
			{
				Logger.Log(ex);
				throw ex;
			}
			finally
			{
				dbCon.Close();
				dbCom.Dispose();
				dbCon.Dispose();
			}
		}

		public static IDataReader ExecReader(Domain d, string sqlStr)
		{
			SqlConnection dbCon = new SqlConnection(d.Sql1ConnectionString);
			SqlCommand dbCom = new SqlCommand(sqlStr, dbCon);
			try
			{
				dbCon.Open();
				return dbCom.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
			}
			catch (Exception ex)
			{
				Logger.Log(ex);
				throw ex;
			}
			finally
			{
				dbCom.Dispose();
			}
		}

		public static int TestVar(object val, int nMin, int nMax, int nDefault)
		{
			int pRet;
			if (val == null || val == DBNull.Value) return nDefault;
			else if (val is int) return pRet = (int)val;
			if (!int.TryParse(val.ToString(), out pRet)) pRet = nDefault;
			if (nMin < nMax && (pRet < nMin || pRet > nMax)) pRet = nDefault;
			return pRet;
		}

		public static decimal TestVar(object val, decimal nMin, decimal nMax, decimal nDefault)
		{
			decimal pRet;
			if (val == null || val == DBNull.Value) return nDefault;
			else if (val is int) return pRet = (decimal)val;
			if (!decimal.TryParse(val.ToString(), out pRet)) pRet = nDefault;
			if (nMin < nMax && (pRet < nMin || pRet > nMax)) pRet = nDefault;
			return pRet;
		}

		public static DateTime TestVar(object val, DateTime nMin, DateTime nMax, DateTime nDefault)
		{
			DateTime pRet;
			if (val == null || val == DBNull.Value) return nDefault;
			else if (val is DateTime) return pRet = (DateTime)val;
			if (!DateTime.TryParse(val.ToString(), out pRet)) pRet = nDefault;
			if (nMin < nMax && (pRet < nMin || pRet > nMax)) pRet = nDefault;
			return pRet;
		}

		public static string TestVar(object val, int nMax, string nDefault)
		{
			string pRet;
			if (val == null) return nDefault;
			else if (val is string) return pRet = (string)val;
			pRet = val.ToString();
			if (nMax > 0 && (pRet.Length > nMax)) pRet = pRet.Substring(0, nMax);
			return pRet;
		}

		public static bool TestVar(object val, bool nDefault)
		{
			bool pRet;
			if (val == null) return nDefault;
			else if (val is bool) return pRet = (bool)val;
			if (!bool.TryParse(val.ToString(), out pRet)) pRet = nDefault;
			return pRet;
		}
	}

}
