﻿using System;
using System.Collections.Generic;
using System.Linq;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure.VO;
using Netpay.CommonTypes;

namespace Netpay.Bll
{

	public static class Recurring
	{
		/// <summary>
		/// Represents a parsed recurring series
		/// </summary>
		public struct Series
		{
			private int _charges;
			private int _intervalCount;
			private ScheduleInterval _intervalUnit;
			private decimal _amount;

			public int Charges { get { return _charges; } set { _charges = value; } }
			public int IntervalCount { get { return _intervalCount; } set { _intervalCount = value; } }
			public ScheduleInterval IntervalUnit { get { return _intervalUnit; } set { _intervalUnit = value; } }
			public decimal Amount { get { return _amount; } set { _amount = value; } }

			public string RecurringString()
			{
				string intervalChar = "";
				switch (IntervalUnit)
				{
					case ScheduleInterval.Minute: intervalChar = "N"; break;
					case ScheduleInterval.Hour: intervalChar = "H"; break;
					case ScheduleInterval.Day: intervalChar = "D"; break;
					case ScheduleInterval.Week: intervalChar = "W"; break;
					case ScheduleInterval.Month: intervalChar = "M"; break;
					case ScheduleInterval.Year: intervalChar = "Y"; break;
					case ScheduleInterval.Quarter: intervalChar = "Q"; break;
				}
				return String.Format("{0}{1}{2}", _charges, intervalChar, _intervalCount);
			}

			public static Series Parse(string seriesList)
			{
				Series series = new Series();
				int intervalPos = seriesList.IndexOfAny(new char[] { 'N', 'H', 'D', 'W', 'M', 'Y', 'Q' });
				if (intervalPos == -1)
					throw new Exception("intervalUnit value not found in Series");

				if ((!int.TryParse(seriesList.Substring(0, intervalPos), out series._charges)) || (series._charges < 1))
					throw new Exception("invalid cherges value in Series");

				switch (seriesList.Substring(intervalPos, 1).ToUpper())
				{
					case "N": series._intervalUnit = ScheduleInterval.Minute; break;
					case "H": series._intervalUnit = ScheduleInterval.Hour; break;
					case "D": series._intervalUnit = ScheduleInterval.Day; break;
					case "W": series._intervalUnit = ScheduleInterval.Week; break;
					case "M": series._intervalUnit = ScheduleInterval.Month; break;
					case "Y": series._intervalUnit = ScheduleInterval.Year; break;
					case "Q": series._intervalUnit = ScheduleInterval.Quarter; break;
					default: throw new Exception("invalid intervalUnit in Series");
				}

				intervalPos++;
				int amountPos = seriesList.IndexOf('A', intervalPos);
				if (amountPos == -1)
				{
					if ((!int.TryParse(seriesList.Substring(intervalPos), out series._intervalCount)) || (series._intervalCount < 1))
						throw new Exception("invalid intervalCount in Series");
				}
				else
				{
					if ((!int.TryParse(seriesList.Substring(intervalPos, amountPos - intervalPos), out series._intervalCount)) || (series._intervalCount < 1))
						throw new Exception("invalid intervalCount in Series");
					if ((!decimal.TryParse(seriesList.Substring(amountPos + 1), out series._amount)) || (series._amount < 0m))
						throw new Exception("invalid amount in Series");
				}

				return series;
			}

			public static List<Series> ParseList(string seriesList)
			{
				var ret = new System.Collections.Generic.List<Bll.Recurring.Series>();
				foreach(var r in seriesList.EmptyIfNull().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries)) {
					ret.Add(Bll.Recurring.Series.Parse(r));
				}
				return ret;
			}

		}

		/// <summary>
		/// Parses recurring series from its string representation
		/// </summary>
		/*
        public static MerchantRecurringSettingVO GetMerchantRecurringSettings(string domainHost, int merchantID) 
        {
            NetpayDataContext dc = new NetpayDataContext(Infrastructure.Domains.DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
            MerchantRecurringSettingVO settings = (from m in dc.tblMerchantRecurringSettings where m.MerchantID == merchantID select new MerchantRecurringSettingVO(m)).SingleOrDefault();
            return settings;
        }

        public static MerchantRecurringSettingVO GetMerchantRecurringSettings(Guid credentialsToken)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            MerchantRecurringSettingVO settings = (from m in dc.tblMerchantRecurringSettings where m.MerchantID == user.ID select new MerchantRecurringSettingVO(m)).SingleOrDefault();
            return settings;
        }
		*/
		public static bool CanCreateFromExistingTransaction(Guid credentialsToken)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
            var settings = Transaction.Recurring.MerchantSettings.Load(credentialsToken, user.ID);
            if (settings == null)
                return false;
            
            return settings.IsEnabled && settings.IsEnabledFromTransPass;
		}

		public static CreateSeriesResult CreateSeries(Guid credentialsToken, int transactionID, TransactionStatus status, int charges, int intervalCount, ScheduleInterval intervalUnit, decimal? amount, string ip, string comment, out int seriesID)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited, UserType.NetpayAdmin, UserType.NetpayUser });
			seriesID = 0;

			// check status
			if (status != TransactionStatus.Captured && status != TransactionStatus.Authorized)
				return CreateSeriesResult.WrongTransactionStatus;
			bool isCaptured = status == TransactionStatus.Captured;

			// get transaction 
			var transaction = Transaction.Transaction.GetTransaction(credentialsToken, null, transactionID, status);
			if (transaction == null)
				return CreateSeriesResult.TransactionNotFound;

			// check transaction series
			if (transaction.RecurringSeriesID != null)
			{
				seriesID = transaction.RecurringSeriesID.Value;
				return CreateSeriesResult.TransactionSeriesAlreadyExists;
			}

			// get interval unit letter
			string intervalUnitLetter = "D";
			switch (intervalUnit)
			{
				case ScheduleInterval.Day:
					intervalUnitLetter = "D";
					break;
				case ScheduleInterval.Week:
					intervalUnitLetter = "W";
					break;
				case ScheduleInterval.Month:
					intervalUnitLetter = "M";
					break;
				case ScheduleInterval.Year:
					intervalUnitLetter = "Y";
					break;
				case ScheduleInterval.Quarter:
					intervalUnitLetter = "Q";
					break;
				default:
					break;
			}

			// comment
			if (comment == null)
				comment = "";

			// build recurring string
			string recurringString = "";
			if (amount == null)
			{
				recurringString = string.Format("recurring1={0}{1}{2}", charges, intervalUnitLetter, intervalCount);
			}
			else
			{
				recurringString = string.Format("recurring1=1{0}{1}", intervalUnitLetter, intervalCount);
				recurringString += string.Format("&recurring2={0}{1}{2}A{3}", charges, intervalUnitLetter, intervalCount, amount.Value);
			}

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			seriesID = dc.RecurringBatchCreateFlexibleSeriesEx(isCaptured, transactionID, recurringString, comment, null, ip, null, null, null).SingleOrDefault().Column1.GetValueOrDefault();
			
            /*
             * Proc return values
             * -1 invalid number of charges
             * -2 invalid interval unit
             * -3 invalid interval length
             * -4 invalid transaction ID - no passed transaction found
             * -5 merchant is not allowed to make recurring transactions
             */

            if (seriesID <= 0)
                return (CreateSeriesResult)seriesID;
            else
                return CreateSeriesResult.Success;
		}
		
		public static List<Transaction.Recurring.RecurringChargeHistory> GetChargeHistory(Guid credentialsToken, int chargeID)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var results = (from h in dc.tblRecurringAttempts where h.tblRecurringCharge.tblRecurringSery.rs_Company == user.ID && h.ra_Charge == chargeID select new Transaction.Recurring.RecurringChargeHistory(credentialsToken, h)).ToList();
	
			return results;
		}
		
		/// <summary>
		/// Returns a merchant series, paged.
		/// </summary>
		public static List<RecurringSeriesVO> GetSeries(Guid credentialsToken, SearchFilters filters, ISortAndPage sortAndPage)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var expression = (IQueryable<tblRecurringSery>)from s in dc.tblRecurringSeries where s.rs_Company == user.ID orderby s.rs_StartDate descending select s;
			
			if (filters != null)
			{
				if (filters.dateFrom != null)
					expression = expression.Where(s => s.rs_StartDate >= filters.dateFrom.Value);
				if (filters.dateTo != null)
					expression = expression.Where(s => s.rs_StartDate <= filters.dateTo.Value.AddDays(1).AddSeconds(-1));
				if (filters.reportAmount != null)
					expression = expression.Where(s => s.rs_SeriesTotal == filters.reportAmount.Value);
				if (filters.currencyID != null)
					expression = expression.Where(s => s.rs_Currency == filters.currencyID.Value);
				if (filters.paymentMethodID != null)
				{
					if (filters.paymentMethodID.Value == (int)PaymentMethodEnum.ECCheck)
						expression = expression.Where(s => s.rs_ECheck != 0);
					else
					{
						expression = expression.Where(s => s.tblCreditCard.ccTypeID + 20 == filters.paymentMethodID.Value);
					}
				}	
				if (filters.seriesID != null)
					expression = expression.Where(s => s.ID == filters.seriesID.Value);
				if (filters.seriesStatus != null)
				{
					switch (filters.seriesStatus.Value)
					{
						case RecurringSeriesStatus.Deleted:
							expression = expression.Where(s => s.rs_Deleted);
							break;
						case RecurringSeriesStatus.Paid:
							expression = expression.Where(s => s.rs_Paid && !s.rs_Deleted);
							break;
						case RecurringSeriesStatus.Blocked:
							expression = expression.Where(s => s.rs_Blocked && !s.rs_Deleted);
							break;
						case RecurringSeriesStatus.Suspended:
							expression = expression.Where(s => s.rs_Suspended && !s.rs_Deleted);
							break;
						case RecurringSeriesStatus.Active:
							expression = expression.Where(s => !s.rs_Paid && !s.rs_Blocked && !s.rs_Suspended && !s.rs_Deleted);
							break;
						default:
							expression = expression.Where(s => !s.rs_Deleted);
							break;
					}
				} else expression = expression.Where(s => !s.rs_Deleted);
			}
			
			return expression.ApplySortAndPage(sortAndPage).Select(s => new RecurringSeriesVO(user.Domain.Host, s, false)).ToList<RecurringSeriesVO>();
		}

		/// <summary>
		/// Returns a specific merchant series.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="seriesID"></param>
		/// <returns></returns>
		public static RecurringSeriesVO GetSeries(Guid credentialsToken, int seriesID)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			RecurringSeriesVO series = (from s in dc.tblRecurringSeries where s.rs_Company == user.ID && s.ID == seriesID && !s.rs_Deleted select new RecurringSeriesVO(user.Domain.Host, s, true)).SingleOrDefault();

			return series;
		}

		/// <summary>
		/// Deletes a series.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="seriesID"></param>
		public static void DeleteSeries(Guid credentialsToken, int seriesID)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			tblRecurringSery series = (from s in dc.tblRecurringSeries where s.rs_Company == user.ID && s.ID == seriesID select s).SingleOrDefault();
			if (series == null)
				throw new ApplicationException("Series not found.");
				
			series.rs_Deleted = true;
			dc.SubmitChanges();
		}

		/// <summary>
		/// Suspends a series.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="seriesID"></param>
		public static void SuspendSeries(Guid credentialsToken, int seriesID)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			tblRecurringSery series = (from s in dc.tblRecurringSeries where s.rs_Company == user.ID && s.ID == seriesID select s).SingleOrDefault();
			if (series == null)
				throw new ApplicationException("Series not found.");

			series.rs_Suspended = true;
			dc.SubmitChanges();
		}

		/// <summary>
		/// Resumes a series.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="seriesID"></param>
		public static void ResumeSeries(Guid credentialsToken, int seriesID)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			tblRecurringSery series = (from s in dc.tblRecurringSeries where s.rs_Company == user.ID && s.ID == seriesID select s).SingleOrDefault();
			if (series == null)
				throw new ApplicationException("Series not found.");

			series.rs_Suspended = false;
			dc.SubmitChanges();
		}

		/// <summary>
		/// Suspends a charge.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="chargeID"></param>
		public static void SuspendCharge(Guid credentialsToken, int chargeID)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			tblRecurringCharge charge = (from c in dc.tblRecurringCharges where c.tblRecurringSery.rs_Company == user.ID && c.ID == chargeID select c).SingleOrDefault();
			if (charge == null)
				throw new ApplicationException("Charge not found.");

			charge.rc_Suspended = true;
			dc.SubmitChanges();
		}

		/// <summary>
		/// Resumes a suspended charge.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="chargeID"></param>
		public static void ResumeCharge(Guid credentialsToken, int chargeID)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			tblRecurringCharge charge = (from c in dc.tblRecurringCharges where c.tblRecurringSery.rs_Company == user.ID && c.ID == chargeID select c).SingleOrDefault();
			if (charge == null)
				throw new ApplicationException("Charge not found.");

			charge.rc_Suspended = false;
			dc.SubmitChanges();
		}

		/// <summary>
		/// Updates a series total.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="seriesID"></param>
		public static void UpdateSeriesTotal(Guid credentialsToken, int seriesID)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			tblRecurringSery series = (from s in dc.tblRecurringSeries where s.rs_Company == user.ID && s.ID == seriesID select s).SingleOrDefault();
			if (series == null)
				throw new ApplicationException("Series not found.");
			
			decimal total = series.tblRecurringCharges.Sum(c => c.rc_Amount);
			series.rs_SeriesTotal = total;
			dc.SubmitChanges();
		}

		/// <summary>
		/// Returns merchant recurring charges, paged.
		/// </summary>
		public static List<RecurringChargeVO> GetCharges(Guid credentialsToken, SearchFilters filters, ISortAndPage sortAndPage)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var expression = (IQueryable<tblRecurringCharge>)from c in dc.tblRecurringCharges where c.tblRecurringSery.rs_Company == user.ID && !c.tblRecurringSery.rs_Deleted orderby c.rc_ChargeNumber select c;

			if (filters != null)
			{
				if (filters.seriesID != null)
					expression = expression.Where(c => c.rc_Series == filters.seriesID);
				if (filters.dateFrom != null)
					expression = expression.Where(c => c.rc_Date >= filters.dateFrom);
				if (filters.dateTo != null)
					expression = expression.Where(c => c.rc_Date <= filters.dateTo);
				if (filters.paymentMethodID != null)
					expression = expression.Where(c => (c.tblCreditCard.ccTypeID + 20) == filters.paymentMethodID);					
			}
			return expression.ApplySortAndPage(sortAndPage).Select(c => new RecurringChargeVO(user.Domain.Host, c)).ToList<RecurringChargeVO>();
		}

		/*
		public static void UpdateCharge(Guid credentialsToken, int chargeID, RecurringChargeVO data, bool updateSubsequentCharges)
		{
			User user = SecurityManager.GetInternalUserForInvocation(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
			
			NetpayDataContext dc = new NetpayDataContext();
			tblRecurringCharge charge = (from c in dc.tblRecurringCharges where c.tblRecurringSery.rs_Company == user.ID && c.ID == chargeID select c).SingleOrDefault();
			if (charge == null)
				throw new ApplicationException("Charge not found.");


		}		 
		*/
	}
}
