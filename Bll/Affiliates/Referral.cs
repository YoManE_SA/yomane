﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Affiliates
{
	public class Referral : BaseDataObject
	{
		public const string SecuredObjectName = "Refferals";
		public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Affiliates.Module.Current, SecuredObjectName); } }

		public class SearchFilters
		{
			public Infrastructure.Range<int?> ID;
			public Infrastructure.Range<DateTime?> Date;
			public int? AffiliateId;	
		}

		private Dal.Netpay.tblAffiliatesCount _entity;

		public int ID { get { return _entity.AFCID;  } }
		public int AffiliateID { get { return _entity.AFCAFFID.GetValueOrDefault(); } }
		public System.DateTime Date { get { return _entity.AFCDate.GetValueOrDefault(); } }
		public byte Type { get { return _entity.AFCType.GetValueOrDefault(); } set { _entity.AFCType = value; } }
		public string IpAddress { get { return _entity.AFCIPAddress; } set { _entity.AFCIPAddress = value; } }
		public string ReferralVal { get { return _entity.AFCReferal; } set { _entity.AFCReferal = value; } }

		private Referral(Dal.Netpay.tblAffiliatesCount entity) { _entity = entity; }

		public static List<Referral> Search(SearchFilters filters, ISortAndPage sortAndPage)
		{
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            //if ((context.User.Type != UserType.NetpayUser) && (context.User.Type != UserType.NetpayAdmin))
            //	filters.AccountIDs = SecurityManager.FilterRequestedIDs(filters.AccountIDs, context.User.AllowedMerchantsIDs);

            var expression = from m in DataContext.Reader.tblAffiliatesCounts select m;
			if (filters.ID.From != null) expression = expression.Where(p => p.AFCID >= filters.ID.From);
			if (filters.ID.To != null) expression = expression.Where(p => p.AFCID <= filters.ID.To);
			if (filters.Date.From != null) expression = expression.Where(p => p.AFCDate >= filters.Date.From);
			if (filters.Date.To != null) expression = expression.Where(p => p.AFCDate <= filters.Date.To);
			if (filters.AffiliateId != null) expression = expression.Where(p => p.AFCAFFID == filters.AffiliateId);

			return expression.ApplySortAndPage(sortAndPage).Select(m => new Referral(m)).ToList();
		}
	}
}
