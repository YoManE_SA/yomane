﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Affiliates
{
    public class Fees : BaseDataObject
    {
        public const string SecuredObjectName = "Fees";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Affiliates.Module.Current, SecuredObjectName); } }

        public class FeeStep
        {
            public decimal UpToAmount { get; set; }
            public decimal? SlicePercent { get; set; }
            public decimal PercentFee { get; set; }
            public bool IsEmpty { get { return UpToAmount == 0 && SlicePercent.GetValueOrDefault(0) == 0 && PercentFee == 0; } }
        }

        private Dal.Netpay.tblAffiliateFeeStep _entity;

        public int ID { get { return _entity.AFS_ID; } }
        public int? AffiliateID { get { return _entity.AFS_AffiliateID; } }
        public int? MerchantID { get { return _entity.AFS_CompanyID; } }

        public CommonTypes.Currency Currency { get { return (CommonTypes.Currency)_entity.AFS_Currency; } set { _entity.AFS_Currency = (int) value; } }
        public CommonTypes.PaymentMethodEnum? PaymentMethod { get { return (CommonTypes.PaymentMethodEnum?)_entity.AFS_PaymentMethod; } set { _entity.AFS_PaymentMethod = (short?)value; } }
        public PartnerTransType TransType { get { return (PartnerTransType)_entity.AFS_TransType; } set { _entity.AFS_TransType = (byte)value; } }
        public PartnerFeeCalc CalcMethod { get { return (Infrastructure.PartnerFeeCalc)_entity.AFS_CalcMethod; } set { _entity.AFS_CalcMethod = (byte)value; } }

        public int? PaymentID { get { return _entity.AFS_AFPID; } set { _entity.AFS_AFPID = value; } }

        public List<FeeStep> Steps { get; set; }

        private Fees(Dal.Netpay.tblAffiliateFeeStep entity) { _entity = entity; }
        public Fees(int partnerId, int? merchantId)
        {
            _entity = new Dal.Netpay.tblAffiliateFeeStep();
            _entity.AFS_AffiliateID = partnerId;
            _entity.AFS_CompanyID = merchantId;
            Steps = new List<FeeStep>();
        }

        private static IQueryable<Dal.Netpay.tblAffiliateFeeStep> GetCompatibleQuery(DataContext context, Dal.Netpay.tblAffiliateFeeStep entity)
        {
            var exp = (from v in context.tblAffiliateFeeSteps where v.AFS_Currency == entity.AFS_Currency && v.AFS_TransType == entity.AFS_TransType select v);
            if(entity.AFS_AFPID.HasValue) exp = exp.Where(v => v.AFS_AFPID == entity.AFS_AFPID); else exp = exp.Where(v => v.AFS_AFPID == null);
            if (entity.AFS_PaymentMethod.HasValue) exp = exp.Where(v => v.AFS_PaymentMethod == entity.AFS_PaymentMethod); else exp = exp.Where(v => v.AFS_PaymentMethod == null);
            if (entity.AFS_AffiliateID.HasValue) exp = exp.Where(v => v.AFS_AffiliateID == entity.AFS_AffiliateID); else exp = exp.Where(v => v.AFS_AffiliateID == null);
            if (entity.AFS_CompanyID.HasValue) exp = exp.Where(v => v.AFS_CompanyID == entity.AFS_CompanyID); else exp = exp.Where(v => v.AFS_CompanyID == null);
            return exp;
        }

        public void Save()
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Edit);

            DataContext.Writer.tblAffiliateFeeSteps.DeleteAllOnSubmit(GetCompatibleQuery(DataContext.Writer, _entity));
            DataContext.Writer.SubmitChanges();
            if (Steps == null) return;
            _entity.AFS_ID = 0;
            var insertItems = new List<Dal.Netpay.tblAffiliateFeeStep>();
            Steps = Steps.OrderBy(v => v.UpToAmount).ToList();
            foreach (var s in Steps)
            {
                var newEntity = _entity.DeepCopy(true);
                newEntity.AFS_UpToAmount = s.UpToAmount;
                newEntity.AFS_PercentFee = s.PercentFee;
                newEntity.AFS_SlicePercent = s.SlicePercent;
                insertItems.Add(newEntity);
            }
            DataContext.Writer.tblAffiliateFeeSteps.InsertAllOnSubmit(insertItems);
            DataContext.Writer.SubmitChanges();
            if (insertItems.Count > 0) _entity.AFS_ID = insertItems.FirstOrDefault().AFS_ID;
        }

        public void Delete()
        {            
            if (_entity.AFS_ID == 0) return;

            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            DataContext.Writer.tblAffiliateFeeSteps.DeleteAllOnSubmit(GetCompatibleQuery(DataContext.Writer, _entity));
            DataContext.Writer.SubmitChanges();
        }

        public static List<Fees> GroupResults(IEnumerable<Dal.Netpay.tblAffiliateFeeStep> items)
        {
            var groups = items.GroupBy(v => new { v.AFS_AffiliateID, v.AFS_CompanyID, v.AFS_Currency, v.AFS_PaymentMethod, v.AFS_TransType, v.AFS_AFPID });
            var ret = new List<Fees>();
            foreach (var g in groups)
            {
                ret.Add(new Fees(g.First()) {
                    Steps = g.OrderBy(v => v.AFS_UpToAmount).Select(v => new FeeStep() { PercentFee = v.AFS_PercentFee, SlicePercent = v.AFS_SlicePercent, UpToAmount = v.AFS_UpToAmount }).ToList()
                });
            }
            return ret;
        }

        public static List<Fees> Load(int? affiliateId, int? merchantId, int? paymentId = null)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var exp = (from m in DataContext.Reader.tblAffiliateFeeSteps select m);
            if (affiliateId.HasValue) exp = exp.Where(p => p.AFS_AffiliateID == affiliateId.Value); else exp = exp.Where(p => p.AFS_AffiliateID == null);
            if (merchantId.HasValue) exp = exp.Where(p => p.AFS_CompanyID == merchantId.Value); else exp = exp.Where(p => p.AFS_CompanyID == null);
            if (paymentId.HasValue) exp = exp.Where(p => p.AFS_AFPID == paymentId.Value); else exp = exp.Where(p => p.AFS_AFPID == null);
            return GroupResults(exp.AsEnumerable());
        }

        public static Fees Load(int id)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var item = (from m in DataContext.Reader.tblAffiliateFeeSteps where m.AFS_ID == id select m).SingleOrDefault();
            if (item == null) return null;
            var exp = GetCompatibleQuery(DataContext.Reader, item);
            return GroupResults(exp.AsEnumerable()).FirstOrDefault();
        }
    }
}
