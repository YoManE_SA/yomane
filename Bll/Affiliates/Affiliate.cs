﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Affiliates
{
    public class Affiliate : Accounts.Account
    {
        public class SearchFilters
        {
            public bool? MustHaveLoginDetails;
            public Range<int?> ID;
            public bool? IsActive;
            public string Text;
        }

        public class AffiliateListItem
        {
            public int ID { get; set; }
            public string Name { get; set; }
        }

        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Affiliates.Module.Current, SecuredObjectName); } }

        private Dal.Netpay.tblAffiliate _entity;

        public int ID { get { return _entity.affiliates_id; } }
        public bool IsActive { get { return _entity.IsActive; } set { _entity.IsActive = value; } }
        public string Name { get { return _entity.name; } set { _entity.name = value; base.AccountName = value; } }
        public string Comments { get { return _entity.comments; } set { _entity.comments = value; } }
        public string LinkRefID { get { return _entity.AFLinkRefID; } set { _entity.AFLinkRefID = value; } }
        public string HeaderImageFileName { get { return _entity.HeaderImageFileName; } private set { _entity.HeaderImageFileName = value; } }
        //public string HeaderSmallImageFileName { get { return _entity.HeaderSmallImageFileName; } private set { _entity.HeaderSmallImageFileName = value; } }
        //public string UserName { get { return _entity.ControlPanelUsername; } set { _entity.ControlPanelUsername = value; } }
        //public string EmailAddress { get { return _entity.ControlPanelEmail; } set { _entity.ControlPanelEmail = value; } }
        //public System.Data.Linq.Binary ControlPanelPassword256 { get { return _entity.ControlPanelPassword256; } private set { _entity.ControlPanelPassword256 = value; } }

        //public decimal FeeRatio { get { return _entity.FeeRatio; } private set { _entity.FeeRatio = value; } }
        //public int? AFCompanyID { get { return _entity.AFCompanyID; } private set { _entity.AFCompanyID = value; } }
        public bool TransactionApplication { get { return _entity.AFTransactionApplication; } set { _entity.AFTransactionApplication = value; } }
        public bool Settlements { get { return _entity.AFSettlements; } set { _entity.AFSettlements = value; } }

        public bool IsAllowControlPanel { get { return _entity.isAllowControlPanel; } set { _entity.isAllowControlPanel = value; } }
        public bool ShowMerchants { get { return _entity.AFShowMerchants; } set { _entity.AFShowMerchants = value; } }
        public bool ShowStats { get { return _entity.AFShowStats; } set { _entity.AFShowStats = value; } }
        public bool ShowSystemPay { get { return _entity.ShowSystemPay; } set { _entity.ShowSystemPay = value; } }
        public bool ShowRemoteCharge { get { return _entity.ShowRemoteCharge; } set { _entity.ShowRemoteCharge = value; } }
        public bool ShowCustomerPurchase { get { return _entity.ShowCustomerPurchase; } set { _entity.ShowCustomerPurchase = value; } }
        public bool ShowPublicPay { get { return _entity.ShowPublicPay; } set { _entity.ShowPublicPay = value; } }
        public bool ShowDataUpdate { get { return _entity.ShowDataUpdate; } set { _entity.ShowDataUpdate = value; } }
        public bool ShowFees { get { return _entity.ShowFees; } set { _entity.ShowFees = value; } }
        public bool ShowDownloads { get { return _entity.ShowDownloads; } set { _entity.ShowDownloads = value; } }
        public bool ShowPayments { get { return _entity.ShowPayments; } set { _entity.ShowPayments = value; } }

        //public string IDnumber { get { return _entity.IDnumber; } set { _entity.IDnumber = value; } }
        public string LegalName { get { return _entity.LegalName; } set { _entity.LegalName = value; } }
        public string LegalNumber { get { return _entity.LegalNumber; } set { _entity.LegalNumber = value; } }
        public string PaymentPayeeName { get { return _entity.PaymentPayeeName; } set { _entity.PaymentPayeeName = value; } }
        public int? PaymentReceiveCurrency { get { return _entity.PaymentReceiveCurrency; } set { _entity.PaymentReceiveCurrency = value; } }
        //public byte? PaymentReceiveType { get { return _entity.PaymentReceiveType; } set { _entity.PaymentReceiveType = value; } }
        //public string PaymentMethod { get { return _entity.PaymentMethod; } set { _entity.PaymentMethod = value; } }

        //public string PaymentAccount { get { return _entity.PaymentAccount; } set { _entity.PaymentAccount = value; } }
        public string OutboundUsername { get { return _entity.OutboundUsername; } set { _entity.OutboundUsername = value; } }
        public string OutboundPassword { get { return _entity.OutboundPassword; } set { _entity.OutboundPassword = value; } }

        public short? GenerateRandomCardPin { get { return _entity.GenerateRandomCardPin; } set { _entity.GenerateRandomCardPin = value; } }

        public int MerchantsNum { get; set; }
        public int LeadsNum { get; set; }
        public int SettlementsNum { get; set; }

        private Affiliate(Dal.Netpay.tblAffiliate entity, AccountLoadCombined accountData) : base(accountData) { _entity = entity; }
        public Affiliate() : base(Accounts.AccountType.Affiliate)
        {
            _entity = new Dal.Netpay.tblAffiliate();
        }

        public static new Affiliate Current
        {
            get
            {
                if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return Accounts.Account.CurrentObject as Affiliate;
            }
        }

        public static Affiliate Load(int? id)
        {            
            if (id == null) id = Affiliate.Current.ID;
            return Load(new List<int>() { id.Value }).SingleOrDefault();
        }

        public static Affiliate LoadByAccountID(int accountId)
        {
            var sf = new SearchFilters() { ID = new Range<int?>() { From = accountId, To = accountId } };
            return Search(sf, null).SingleOrDefault();
        }

        public static List<Affiliate> Load(List<int> Ids)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from a in Accounts.Account.LoadQuery(DataContext.Reader, false)
                    join c in DataContext.Reader.tblAffiliates on a.account.Affiliate_id equals c.affiliates_id
                    where Ids.Contains(c.affiliates_id)
                    select new Affiliate(c, a)).ToList();
        }

        public void Save()
        {            
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Partner }, SecuredObject, PermissionValue.Edit);
            Accounts.AccountFilter.Current.Validate(Accounts.AccountType.Affiliate, _entity.affiliates_id);

            if (_entity.name == null) _entity.name = "";
            if (_entity.comments == null) _entity.comments = "";
            if (_entity.HeaderImageFileName == null) _entity.HeaderImageFileName = "";
            if (_entity.HeaderSmallImageFileName == null) _entity.HeaderSmallImageFileName = "";
            if (_entity.ControlPanelUsername == null) _entity.ControlPanelUsername = "";

            DataContext.Writer.tblAffiliates.Update(_entity, (_entity.affiliates_id != 0));
            DataContext.Writer.SubmitChanges();
            if (_accountEntity.Affiliate_id == null) _accountEntity.Affiliate_id = _entity.affiliates_id;
            base.SaveAccount();
        }

        public void Delete()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            if (_entity.affiliates_id == 0) return;
            DataContext.Writer.tblAffiliates.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }

        public static List<Accounts.AccountInfo> AutoComplete(string text)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            int numValue = text.ToNullableInt().GetValueOrDefault(-1);
            return (from a in DataContext.Reader.Accounts
                    join c in DataContext.Reader.tblAffiliates on a.Affiliate_id equals c.affiliates_id
                    where c.name.Contains(text) || a.Account_id == numValue
                    select new Accounts.AccountInfo()
                    {
                        AccountID = a.Account_id,
                        TargetID = c.affiliates_id,
                        AccountName = string.Format("{0}", c.name),
                        AccountType = Accounts.AccountType.Affiliate,
                        AccountStatus = (c.IsActive).ToString(),
                        StatusColor = c.IsActive ? System.Drawing.Color.Green : System.Drawing.Color.Red
                    }).Take(10).ToList();
        }

        public static List<AffiliateListItem> GetAffiliatesList()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var expression = from tblaccount in Accounts.Account.LoadQuery(DataContext.Reader)
                             join a in DataContext.Reader.tblAffiliates on tblaccount.account.Affiliate_id equals a.affiliates_id
                             select new AffiliateListItem { ID = a.affiliates_id, Name = a.name };


            return Domain.Current.GetCachData("AffiliatesList", () =>
                {
                    return new Domain.CachData(expression.ToList(), DateTime.Now.AddHours(6));
                }) as List<AffiliateListItem>;
        }

        public static List<Affiliate> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            //if ((context.User.Type != UserType.NetpayUser) && (context.User.Type != UserType.NetpayAdmin))
            //	filters.AccountIDs = SecurityManager.FilterRequestedIDs(filters.AccountIDs, context.User.AllowedMerchantsIDs);

            var expression = from tblaccount in Accounts.Account.LoadQuery(DataContext.Reader)
                             join a in DataContext.Reader.tblAffiliates on tblaccount.account.Affiliate_id equals a.affiliates_id
                             join sa in DataContext.Reader.SetMerchantAffiliates on a.affiliates_id equals sa.Affiliate_id into FirstLeftJoin

                             from FirstLeftJoinIncludeEmpty in FirstLeftJoin.DefaultIfEmpty()
                             join tp in DataContext.Reader.tblTransactionPays on FirstLeftJoinIncludeEmpty.Merchant_id equals tp.CompanyID into SecondLeftJoin

                             from SecondLeftJoinIncludeEmpty in SecondLeftJoin.DefaultIfEmpty()
                             join p in DataContext.Reader.tblAffiliatePayments on new { X = a.affiliates_id, Y = SecondLeftJoinIncludeEmpty.id } equals new { X = p.AFP_Affiliate_ID, Y = p.AFP_TransPaymentID } into ThirdLeftJoin

                             from ThirdLeftJoinIncludeEmpty in ThirdLeftJoin.DefaultIfEmpty()
                             select new { a, tblaccount, FirstLeftJoinIncludeEmpty, SecondLeftJoinIncludeEmpty, ThirdLeftJoinIncludeEmpty };

            if (filters != null)
            {
                if (filters.MustHaveLoginDetails.HasValue && filters.MustHaveLoginDetails.Value) expression = expression.Where(x => x.tblaccount.account.LoginAccount_id.HasValue);
                if (filters.ID.From != null) expression = expression.Where(p => p.tblaccount.account.Account_id >= filters.ID.From);
                if (filters.ID.To != null) expression = expression.Where(p => p.tblaccount.account.Account_id <= filters.ID.To);
                if (filters.IsActive.HasValue) expression = expression.Where(p => p.a.IsActive == filters.IsActive);
                if (filters.Text != null) expression = expression.Where(p => p.a.name.Contains(filters.Text) || p.a.comments.Contains(filters.Text));
            }

            var expressionGroup = expression.GroupBy(i => new
                                    {
                                        i.a.affiliates_id,
                                        i.a.name,
                                        i.a.IsActive
                                    })
                                    .Select(i => new { i.Key.affiliates_id, i.Key.name, i.Key.IsActive, Values = i.ToList() });

            expressionGroup = expressionGroup.ApplySortAndPage(sortAndPage);

            return expressionGroup
                            .Select(p => new Affiliate(p.Values.Select(k => k.a).FirstOrDefault(),
                                                        p.Values.Select(k => k.tblaccount).FirstOrDefault())
                            {
                                MerchantsNum = p.Values.Where(i => i.FirstLeftJoinIncludeEmpty != null).Select(i => i.FirstLeftJoinIncludeEmpty.Merchant_id).Distinct().Count(),
                                SettlementsNum = p.Values.Where(i => i.SecondLeftJoinIncludeEmpty != null).Select(i => i.SecondLeftJoinIncludeEmpty.id).Distinct().Count(),
                                LeadsNum = (from x in DataContext.Reader.tblAffiliatesCounts
                                            where x.AFCAFFID == p.affiliates_id
                                            select x.AFCID).ToList().Distinct().Count()
                            })
                            .ToList();
        }

        private List<int> _appIdentities;
        public List<int> AppIdentities
        {
            //Come from pcp no need for security check  in admin.
            get
            {
                if (_appIdentities != null) return _appIdentities;
                _appIdentities = (from ai in DataContext.Reader.tblAffiliatesToApplicationIdentities where ai.Affiliates_id == _entity.affiliates_id select ai.ApplicationIdentity_id).ToList();
                return _appIdentities;
            }
        }

        /// <summary>
        /// This function gets an affiliate id , and a LinkRef , 
        /// And checks if that LinkRef exist in the database with different affiliate id
        /// If yes - return true , if not return false.
        /// That function is used in the saving operation of an affiliate , if the LinkRef
        /// That was given by the user in the UI exist in the DB with different affiliate_id
        /// The LinkRef will not be saved , in order to prevant duplicate LinkRef's in DB.
        /// </summary>
        /// <param name="afLinkRef"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool CheckIfAFLinkRefExist(string afLinkRef , int id)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            if (id <= 0) return true; //Behave like the row exist , that means Save won't be invoked

            //The values will be compared as LowerCase strings.
            string afLinkRefLowerCase = afLinkRef.Trim().ToLower();
                        
            int num = (from a in DataContext.Reader.tblAffiliates
                       where (a.AFLinkRefID.ToLower() == afLinkRefLowerCase && a.affiliates_id != id)
                       select a.affiliates_id).Count();

            if (num > 0)
            {
                return true;
            }
            else return false;
        }

        public static void SetHeaderImageFileName(int affiliateId , string headerImageFileName)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            if (string.IsNullOrEmpty(headerImageFileName) || affiliateId <= 0) return;
            DataContext.Reader.ExecuteCommand("Update tblAffiliates Set HeaderImageFileName = {0} Where Affiliates_id={1}", headerImageFileName, affiliateId);
        }

        public static void DeleteHeaderFileName(int affiliateId)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            if (affiliateId <= 0) return;
            DataContext.Reader.ExecuteCommand("Update tblAffiliates Set HeaderImageFileName = '' Where Affiliates_id = {0}", affiliateId);
        }

        public static List<string> GetSecuredObjectsForAffiliates()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from sso in DataContext.Reader.SecurityObjects
                    where sso.AppModule_id == 21
                    select sso.Name).ToList();
        }

        public enum PinGenerationMode
        {
            LastFourIdDigits = 1,
            RandomNumbers = 2
        }
        /*
                private List<MerchantSettings> _merchantSettings;
                public List<MerchantSettings> AccountSettings
                {
                    get{
                        if (_merchantSettings == null) _merchantSettings = MerchantSettings.
                        return 
                    }
                }
        */
    }
}
