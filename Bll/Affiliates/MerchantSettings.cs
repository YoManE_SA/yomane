﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Affiliates
{
	public class MerchantSettings : BaseDataObject
	{
		public const string SecuredObjectName = "MerchantSettings";
		public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Affiliates.Module.Current, SecuredObjectName); } }

		public class SearchFilters
		{
			public Range<int?> ID;
			public int? AffiliateId;
			public int? MerchantId;
		}

		private static object _partnerConfigLock = new object();
		private Netpay.Dal.Netpay.SetMerchantAffiliate _entity;
		public int ID { get { return _entity.SetMerchantAffiliate_id; } }
		public string UserID { get { return _entity.UserID; } set { _entity.UserID = value; }  }
		public int MerchantId { get { return _entity.Merchant_id; } }

        public string CompanyName { get; private set; }
        public string AffiliateName { get; private set; }

		public int PartnerId { get { return _entity.Affiliate_id; } }
		public decimal? FeesReducedPercentage { get { return _entity.FeesReducedPercentage; } set { _entity.FeesReducedPercentage = value; } }
		
		public DateTime? SyncDate { get { return _entity.SyncDateTime; } set { _entity.SyncDateTime = value; } }
		public System.Collections.Specialized.NameValueCollection ConfigurationValues { get { return System.Web.HttpUtility.ParseQueryString(_entity.ConfigurationValues.EmptyIfNull()); } set { _entity.ConfigurationValues = value.ToString(); } }

		private MerchantSettings(Dal.Netpay.SetMerchantAffiliate entity) { _entity = entity; }

		public MerchantSettings(int merchantId, int partnerId)
		{
			_entity = new Dal.Netpay.SetMerchantAffiliate();
			_entity.Merchant_id = merchantId;
			_entity.Affiliate_id = partnerId;
		}

		public static MerchantSettings Load(int partnerId, int merchantId)
		{
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from s in DataContext.Reader.SetMerchantAffiliates where s.Merchant_id == merchantId && s.Affiliate_id == partnerId select new MerchantSettings(s)).SingleOrDefault();
		}

		public static MerchantSettings Load(int partnerId, string partnerUserId)
		{
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from s in DataContext.Reader.SetMerchantAffiliates where s.UserID == partnerUserId && s.Affiliate_id == partnerId select new MerchantSettings(s)).SingleOrDefault();
		}

		public void Save()
		{
			if (!string.IsNullOrEmpty(UserID))
			{
				var umid = (from s in DataContext.Reader.SetMerchantAffiliates where s.Affiliate_id == PartnerId && s.UserID == UserID select s.Merchant_id).FirstOrDefault();
				if (umid != 0 && umid != MerchantId) throw new Exception("User already registered on another merchant");
			}
                        
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Edit);

			if (_entity.SetMerchantAffiliate_id == 0) {
				if ((from s in DataContext.Reader.SetMerchantAffiliates where s.Affiliate_id == PartnerId && s.Merchant_id == _entity.Merchant_id select (int?)s.SetMerchantAffiliate_id).FirstOrDefault() != null)
					throw new Exception("Row already exist");
			}
			DataContext.Writer.SetMerchantAffiliates.Update(_entity, (_entity.SetMerchantAffiliate_id != 0));
			DataContext.Writer.SubmitChanges();
		}


		public void Delete()
		{
			if (_entity.SetMerchantAffiliate_id == 0) return;

            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

			DataContext.Writer.SetMerchantAffiliates.Delete(_entity);
			DataContext.Writer.SubmitChanges();
		}

		public static void SetPartnerSyncDate(int partnerId, int merchantId, DateTime? syncDate)
		{
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            lock (_partnerConfigLock)
			{
				var refData = (from sma in DataContext.Reader.SetMerchantAffiliates where sma.Affiliate_id == partnerId && sma.Merchant_id == merchantId select sma).SingleOrDefault();
				refData.SyncDateTime = syncDate;
				DataContext.Writer.SubmitChanges();
			}
		}

		public static bool SavePartnerServiceConfig(int partnerId, int merchantId, string configName, string configValue)
		{
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);


            lock (_partnerConfigLock)
			{
				using(var context = new DataContext(true)){
					var refData = (from sma in context.SetMerchantAffiliates where sma.Affiliate_id == partnerId && sma.Merchant_id == merchantId select sma).SingleOrDefault();
					if (refData == null) return false;
					var serviceData = System.Web.HttpUtility.ParseQueryString(refData.ConfigurationValues ?? "");
					serviceData[configName] = configValue;
					serviceData["LASTUPDATE"] = DateTime.Now.ToString("s");
					refData.ConfigurationValues = serviceData.ToString();
					context.SubmitChanges();
				}
			}
			return true;
		}

		public static List<MerchantSettings> Search(SearchFilters filters, ISortAndPage sortAndPage)
		{
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
            
            var expression = (from m in DataContext.Reader.SetMerchantAffiliates
                              join a in DataContext.Reader.tblAffiliates on m.Affiliate_id equals a.affiliates_id into ma
                              join c in DataContext.Reader.tblCompanies on m.Merchant_id equals c.ID into mc
                              from mcIncludeEmpty in mc.DefaultIfEmpty()
                              from maIncludeEmpty in ma.DefaultIfEmpty()
                              select new { m, mcIncludeEmpty.CompanyName, maIncludeEmpty.name });
            
			if (filters.ID.From != null) expression = expression.Where(p => p.m.SetMerchantAffiliate_id >= filters.ID.From);
			if (filters.ID.To != null) expression = expression.Where(p => p.m.SetMerchantAffiliate_id <= filters.ID.To);
			if (filters.MerchantId != null) expression = expression.Where(p => p.m.Merchant_id == filters.MerchantId);
			if (filters.AffiliateId != null) expression = expression.Where(p => p.m.Affiliate_id == filters.AffiliateId);
			return expression.ApplySortAndPage(sortAndPage).Select(p => new MerchantSettings(p.m) {CompanyName=p.CompanyName, AffiliateName = p.name}).ToList();
		}

		public static List<MerchantSettings> ForMerchant(Merchants.Merchant merchant)
		{
			return Search(new SearchFilters() { MerchantId = merchant.ID }, null);
		}

        public static List<MerchantSettings> ForAffiliate(Affiliates.Affiliate affiliate)
        {
            return Search(new SearchFilters() { AffiliateId = affiliate.ID }, null);
        }

    }
}
