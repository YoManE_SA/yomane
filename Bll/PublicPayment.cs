﻿using System;
using System.Collections.Generic;
using System.Linq;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Domains;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure.VO;
using Netpay.CommonTypes;

namespace Netpay.Bll
{
	/// <summary>
	/// Business logics related to the Public Payment Page.
	/// </summary>
	public static class PublicPayment
	{
		/// <summary>
		/// If the transaction is from Public payment page, return Public Payment Customer Data (aka Payer Info) VO
		/// </summary>
		/// 
		public static PublicPayCustomerVO GetPublicPayCustomer(Guid credentialsToken, int transactionID, TransactionStatus status)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary, UserType.NetpayAdmin, UserType.NetpayUser, UserType.Customer });
			
			string transLocation;
			switch (status)
			{
				case TransactionStatus.Authorized: transLocation = "approval"; break;
				case TransactionStatus.Captured: transLocation = "pass"; break;
				case TransactionStatus.Declined: transLocation = "fail"; break;
				case TransactionStatus.DeclinedArchived: transLocation = "fail"; break;
				case TransactionStatus.Pending: transLocation = "pending"; break;
				default: throw new ApplicationException("Unrecognized TransactionStatus: \"" + status.ToString() + "\"");
			}

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var foundTransaction = Transaction.Transaction.GetTransaction(credentialsToken, null, transactionID, status);
			if (foundTransaction == null)
				throw new ApplicationException("Transaction not found: " + transLocation + " " + transactionID.ToString());
			tblPublicPayCostumerData result = (from h in dc.tblPublicPayCostumerDatas where h.transID == transactionID && h.transLocation == transLocation select h).FirstOrDefault();
			
			return new PublicPayCustomerVO(result);
		}

		public static List<PublicPayChargeOptionVO> GetChargeOptions(string domainHost, string merchantNumber, Language? language)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			var options = from p in dc.tblPublicPayChargeOptions join merchants in dc.tblCompanies on p.companyID equals merchants.ID where merchants.CustomerNumber == merchantNumber && p.isActive select p;
			if (language == null) options = options.Where(p => p.Lang == null);
			else options = options.Where(p => p.Lang == (int)language || p.Lang == null);
			var results = options.Select(co => new PublicPayChargeOptionVO(domainHost, co)).ToList();
			return results;
		}

		public static List<PublicPayChargeOptionVO> GetChargeOptions(Guid credentialsToken, bool? isActive, Language? language)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var options = from p in dc.tblPublicPayChargeOptions where p.companyID == user.ID select p;
			if (language == null) options = options.Where(p => p.Lang == null);
			else options = options.Where(p => p.Lang == (int?)language);
			if (isActive != null) options = options.Where(p => p.isActive == isActive);
			var results = options.Select(co => new PublicPayChargeOptionVO(user.Domain.Host, co)).ToList();
			return results;
		}

		public static PublicPayChargeOptionVO GetChargeOption(string domainHost, string merchantNumber, int chargeOptionID)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			tblPublicPayChargeOption option = (from publicPayChargeOptions in dc.tblPublicPayChargeOptions join merchants in dc.tblCompanies on publicPayChargeOptions.companyID equals merchants.ID where merchants.CustomerNumber == merchantNumber && publicPayChargeOptions.id == chargeOptionID select publicPayChargeOptions).SingleOrDefault();
            if (option == null)
                return null;

			return new PublicPayChargeOptionVO(domainHost, option);
		}

		public static PublicPayChargeOptionVO GetChargeOption(Guid credentialsToken, int chargeOptionID)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			tblPublicPayChargeOption option = (from publicPayChargeOptions in dc.tblPublicPayChargeOptions where publicPayChargeOptions.companyID == user.ID && publicPayChargeOptions.id == chargeOptionID select publicPayChargeOptions).SingleOrDefault();
			if (option == null) return null;
			return new PublicPayChargeOptionVO(user.Domain.Host, option);
		}

		public static List<PublicPayChargeOptionVO> GetChargeOptions(string domainHost, int merchantID, Language language, bool? isActive)
		{
			var domain = DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			var result = (from p in dc.tblPublicPayChargeOptions where p.companyID == merchantID select p);
			if (isActive != null) result = result.Where(p => p.isActive == isActive.Value);
			if (language == Language.Unknown) result = result.Where(p => p.Lang == null);
			else result = result.Where(p => p.Lang == (int)language || p.Lang == null);

			return (from p in result select new PublicPayChargeOptionVO(domainHost, p)).ToList();
		}

		public static List<PublicPayChargeOptionVO> GetChargeOptions(string domainHost, string merchantNumber, Language language)
		{
			var merchant = Merchant.Merchant.Load(domainHost, merchantNumber);
			return GetChargeOptions(domainHost, merchant.ID, language, true);
		}

		public static List<PublicPayChargeOptionVO> GetChargeOptions(string domainHost, int merchantID, Language language, int page, int pageSize)
		{
			var domain = DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			var result = (from p in dc.tblPublicPayChargeOptions where p.isActive && p.companyID == merchantID select p).Skip(page * pageSize).Take(pageSize);

			if (language == Language.Unknown)
				result = result.Where(p => p.Lang == null);
			else
				result = result.Where(p => p.Lang == (int)language || p.Lang == null);

			return (from p in result select new PublicPayChargeOptionVO(domainHost, p)).ToList();
		}

		public static List<PublicPayChargeOptionVO> GetChargeOptions(string domainHost, string merchantNumber, Language language, int page, int pageSize)
		{
			var merchant = Merchant.Merchant.Load(domainHost, merchantNumber);
			return GetChargeOptions(domainHost, merchant.ID, language, page, pageSize);
		}

		public static List<PublicPayChargeOptionVO> SearchChargeOptions(string domainHost, string merchantNumber, List<string> tags, Language language, int page, int pageSize)
		{
			var domain = DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			var productsIds = (from t in dc.tblPublicPayChargeOptionTags where tags.Contains(t.Tag) select t.Product_id).Distinct().ToList();
			var productsExp = (from p in dc.tblPublicPayChargeOptions where productsIds.Contains(p.id) && p.isActive select p);
			if (!string.IsNullOrEmpty(merchantNumber)) {
				var merchant = Merchant.Merchant.Load(domainHost, merchantNumber);
				if (merchant != null) productsExp = productsExp.Where(p => p.companyID == merchant.ID);
			}
			var products = productsExp.Skip(page * pageSize).Take(pageSize);
			if (language == Language.Unknown) 
				products = products.Where(p => p.Lang == null);
			else 
				products = products.Where(p => p.Lang == (int)language || p.Lang == null);

			return products.Select(p => new PublicPayChargeOptionVO(domainHost, p)).ToList();
		}

		/*
		public static List<PublicPayChargeOptionVO> SearchChargeOptionsByTags(string domainHost, string merchantNumber, List<string> tags, string keyword)
		{
			var domain = DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			MerchantVO merchant = Merchants.GetMerchant(domainHost, merchantNumber);
			var productsIds = (from t in dc.tblPublicPayChargeOptionTags where tags.Contains(t.Tag) select t.Product_id).Distinct().ToList();
			var products = (from p in dc.tblPublicPayChargeOptions where p.companyID == merchant.ID && (productsIds.Contains(p.id) || p.Text.StartsWith(keyword)) select p).Distinct().Take(10);
			return products.Select(p => new PublicPayChargeOptionVO(domainHost, p)).ToList();
		}
		*/

        public static List<string> SearchChargeOptionsTags(string domainHost, string merchantNumber, string tagPrefix)
        {
            var domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            var merchant = Merchant.Merchant.Load(domainHost, merchantNumber);

            var tags = (from t in dc.tblPublicPayChargeOptionTags where t.tblPublicPayChargeOption.companyID == merchant.ID && t.Tag.StartsWith(tagPrefix) select t.Tag).Distinct().Take(10);
            return tags.ToList();
        }

		/*
		public static List<PublicPayChargeOptionVO> SearchChargeOption(string domainHost, Language? language, string keyword, int page = 0, int pageSize = 10)
		{
			var domain = DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			var result = (from p in dc.tblPublicPayChargeOptions where p.isActive && p.Text.Contains(keyword) select p).Take(10);
			if (language == null) result = result.Where(p => p.Lang == null);
			else result = result.Where(p => p.Lang == (int)language || p.Lang == null);
			return (from p in result select new PublicPayChargeOptionVO(domainHost, p)).Skip(page * pageSize).Take(pageSize).ToList();
		}
		*/

		public static List<ProductPurchasedInRangeVO> SearchChargeOptionsByLocation(string domainHost, Language? language, double latitude, double longitude, short? radius)
		{
			var domain = DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			if  (language == null || language == Language.Unknown) language = null;
			return (from res in dc.SpGetProductPurchasedInRange(latitude.ToString(), longitude.ToString(), radius, (byte?)language, true) select new ProductPurchasedInRangeVO(domain, res)).Take(10).ToList();
		}

		public static List<PublicPayChargeOptionVO> GetPromoChargeOptions(string domainHost, Language language, int page, int pageSize)
		{
			var domain = DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			var products = (from p in dc.tblPublicPayChargeOptions where p.isActive select p).Skip(page * pageSize).Take(pageSize);
			if (language == Language.Unknown) 
				products = products.Where(p => p.Lang == null);
			else 
				products = products.Where(p => p.Lang == (int)language || p.Lang == null);

			return (from p in products select new PublicPayChargeOptionVO(domainHost, p)).ToList();
		}

		public static bool DeleteChargeOption(Guid credentialsToken, int chargeOptionID)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			tblPublicPayChargeOption option = (from publicPayChargeOptions in dc.tblPublicPayChargeOptions where publicPayChargeOptions.id == chargeOptionID && publicPayChargeOptions.companyID == user.ID select publicPayChargeOptions).SingleOrDefault();
			if (option == null) throw new ApplicationException("chargeOption does not exist");
			dc.MerchantProductPurchaseds.DeleteAllOnSubmit(dc.MerchantProductPurchaseds.Where(mpp => mpp.MerchantProduct_id == chargeOptionID).ToList());
			dc.tblPublicPayChargeOptionTags.DeleteAllOnSubmit(dc.tblPublicPayChargeOptionTags.Where(ppr => ppr.Product_id == chargeOptionID).ToList());
			dc.tblPublicPayChargeOptions.DeleteOnSubmit(option);
			dc.SubmitChanges();
			return true;
		}

		public static void SaveChargeOption(Guid credentialsToken, PublicPayChargeOptionVO value)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			tblPublicPayChargeOption option = (from publicPayChargeOptions in dc.tblPublicPayChargeOptions where publicPayChargeOptions.id == value.ID && publicPayChargeOptions.companyID == user.ID select publicPayChargeOptions).SingleOrDefault();
			if (option == null ) {
				option = new tblPublicPayChargeOption();
				option.companyID = user.ID;
				dc.tblPublicPayChargeOptions.InsertOnSubmit(option);
			}
			option.Text = value.Text; 
			option.SKU = value.SKU;
			option.Description = value.Description;
            option.ReceiptText = value.ReceiptText;
			option.ReceiptLink = value.ReceiptLink;
			option.Amount = value.Amount;
			option.Currency = value.CurrencyID;
			option.QtyAvailable = value.QuantityAvailable;
			option.QtyStart = value.QuantityMin;
			option.QtyEnd = value.QuantityMax;
			option.QtyStep = value.QuantityInterval;
			option.Payments = value.PaymentsMax;
			option.ImageFileName = value.ImageFileName;
			option.CreditType = value.CreditType;
			option.isActive = value.IsActive;
			option.Priority = value.Priority;
			option.Lang = (int?)value.Language;
			option.isDynamicProduct = value.IsDynamicAmount;
			dc.SubmitChanges();
			value.ID = option.id;

            // tags
            if (value.Tags != null && value.Tags.Count > 0)
            {
                var oldTags = (from t in dc.tblPublicPayChargeOptionTags where t.Product_id == option.id select t);
                dc.tblPublicPayChargeOptionTags.DeleteAllOnSubmit(oldTags);
                dc.SubmitChanges();

                List<string> newTags = value.Tags.Select(t => t.Trim()).Distinct().Where(t => t.Length <= 20 && !t.Contains(" ")).Take(10).ToList();
                foreach (string newTag in newTags)
                {
                    tblPublicPayChargeOptionTag tag = new tblPublicPayChargeOptionTag();
                    tag.Product_id = option.id;
                    tag.Tag = newTag;
                    dc.tblPublicPayChargeOptionTags.InsertOnSubmit(tag);
                }
                dc.SubmitChanges(); 
            }
		}

		public static void SaveMerchantProductPurchased(Guid credentialsToken, int productId, double latitude, double longitude)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary, UserType.Customer });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			dc.SpSaveMerchantProductPurchased(productId, latitude.ToString(), longitude.ToString());
		}

		public static Transaction.Transaction GetTransWithQrId(Guid credentialsToken, string transQrId)
		{
			User user = Netpay.Infrastructure.Security.SecurityManager.GetInternalUser(credentialsToken);
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			string orderId = "SQR-ID:" + transQrId.ToString();
            var trans = (from t in dc.tblCompanyTransPasses where t.companyID == user.ID && t.InsertDate > DateTime.Now.AddMinutes(-90) && t.OrderNumber == orderId select new Transaction.Transaction(user, t)).SingleOrDefault();
            return trans;
		}

		public static bool IsManagedStock(string domainHost, int productId)
		{
			Domain d = DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(d.Sql1ConnectionString);
			var product = (from m in dc.tblPublicPayChargeOptions where m.id == productId select m).SingleOrDefault();
			if (product == null) return false;
			if (product.QtyAvailable == null) return false;
			return true;
		}

		public static bool PuchaseItem(string domainHost, string merchantNumber, int productId, int quantity)
		{
			Domain d = DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(d.Sql1ConnectionString);
			var merchantId = (from m in dc.tblCompanies where m.CustomerNumber == merchantNumber select m.ID).SingleOrDefault();
			int retValue = dc.ExecuteCommand("Update tblPublicPayChargeOptions Set QtyAvailable = QtyAvailable-{0} Where id = {1} And companyID = {2} And QtyAvailable >= {0}", quantity, productId, merchantId);
			return retValue > 0;
		}

		public static void CancelPuchaseItem(string domainHost, string merchantNumber, int productId, int quantity)
		{
			Domain d = DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(d.Sql1ConnectionString);
			var merchantId = (from m in dc.tblCompanies where m.CustomerNumber == merchantNumber select m.ID).SingleOrDefault();
			dc.ExecuteCommand("Update tblPublicPayChargeOptions Set QtyAvailable = QtyAvailable + {0} Where id={1} And companyID = {2}", quantity, productId, merchantId);
		}
	}
}