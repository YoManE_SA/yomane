﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll
{
	public class ErrorText : BaseDataObject
	{
		private Netpay.Dal.Netpay.SysErrorCode _entity;

		public string GroupName { get { return _entity.ErrorMessage; } }
		public int ErrorCode { get { return _entity.ErrorCode; } }

		public string ErrorMessage { get { return _entity.ErrorMessage; } set { _entity.ErrorMessage = value; } }
		public string LanguageISOCode { get { return _entity.LanguageISOCode; } set { _entity.LanguageISOCode = value; } }

		private ErrorText(Guid credentialsToken, Netpay.Dal.Netpay.SysErrorCode entity) : base(credentialsToken) { _entity = entity; }

		private void EnsureAttached()
		{
			if (DataContext.SysErrorCodes.GetOriginalEntityState(_entity) == null)
				DataContext.SysErrorCodes.Attach(_entity);
		}
		public void Save()
		{
			EnsureAttached();
			DataContext.Refresh(System.Data.Linq.RefreshMode.KeepCurrentValues, _entity);
			DataContext.SubmitChanges();
		}


		public static Dictionary<string, Dictionary<int, ErrorText>> _cach; //by domain
		public static void RefreshCach(string domainHost)
		{
			var domain = Infrastructure.Domains.DomainsManager.GetDomain(domainHost);
			lock (typeof(ErrorText)) {
				if (_cach == null) _cach = new Dictionary<string,Dictionary<int,ErrorText>>();
				var context = CreateContext(domain.GuestCredentials);
				_cach[domainHost] = (from e in context.DataContext.SysErrorCodes select new ErrorText(domain.GuestCredentials, e)).ToDictionary(k => k.ErrorCode);

			}
		}
		private static void EnsureCach(string domainHost)
		{
			if (_cach != null && _cach.ContainsKey(domainHost)) return;
			RefreshCach(domainHost);
		}

		public static string GetErrorMessage(string domainHost, string language, int errorCode)
		{
			if (string.IsNullOrEmpty(language)) language = "en";
			EnsureCach(domainHost);
			if (!_cach[domainHost].ContainsKey(errorCode)) return null;
			var err = _cach[domainHost][errorCode];
			if (err == null) return null;
			return err.ErrorMessage;
		}

		public static Dictionary<int, string> GetErrorGroup(string domainHost, string language, HashSet<string> groups)
		{
			EnsureCach(domainHost);
			var errs = _cach[domainHost];
			if (string.IsNullOrEmpty(language)) language = "en";
			return (from e in errs.Values where e.LanguageISOCode == language && groups.Contains(e.GroupName) select e).ToDictionary(k => k.ErrorCode, v => v.ErrorMessage);
		}
	}
}
