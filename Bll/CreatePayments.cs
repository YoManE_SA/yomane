﻿using System;
using System.Collections.Generic;
using Netpay.Infrastructure;
using Netpay.Infrastructure.VO;
using Netpay.CommonTypes;
using Netpay.Dal.DataAccess;
using Netpay.Dal.Netpay;
using System.Linq;
using System.Text;

namespace Netpay.Bll
{
	public static class CreatePayments
	{
		public class CreatePaymentInfo
		{
			public int payID;
			public int merchantID;
			public Currency currencyID;
			public DateTime payDate;
			public bool rollingReserve;
			public bool rollingRelease;
			public bool includeTest;
			public bool includeStorage;
			public bool? epaPaid;
			public bool apply;
			//filter
			public DateTime? insertDateFrom;
			public DateTime? insertDateTo;
			public DateTime? merchantPayDateFrom;
			public DateTime? merchantPayDateTo;
			public List<CreditType> creditType;
			public List<PaymentMethodEnum> includePaymentMethods;
			public List<PaymentMethodEnum> excludePaymentMethods;
			public List<int> includeTransactions;
			public List<int> includeInstallmets;
			public List<int> excludeTransactions;
			public List<int> excludeInstallmets;
			public List<int> excludeSettleAmount;
		}

		public static void DeletePayment(Guid credentialsToken, int payID, int companyID)
		{
			Infrastructure.Security.User user = Infrastructure.Security.SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayUser, UserType.NetpayAdmin });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			dc.ExecuteCommand("Delete From tblSettlementAmount Where MerchantID=" + companyID + " And SettlementID=" + payID);
			dc.ExecuteCommand("Delete From tblTransactionAmount Where MerchantID=" + companyID + " And SettlementID=" + payID + " And (IsNull(TransPassID, 0) = 0 And IsNull(TransApprovalID, 0) = 0 And IsNull(TransPendingID, 0) = 0 And IsNull(TransFailID, 0) = 0)");
			dc.ExecuteCommand("Update tblTransactionAmount Set SettlementID=null Where MerchantID=" + companyID + " And SettlementID=" + payID);
			dc.ExecuteCommand("Update tblCCStorage Set PayID=0 Where CompanyID=" + companyID + " And PayID=" + payID);
			dc.ExecuteCommand("Delete From tblPaymentFeesFloor Where CFF_PayID=" + payID);
			dc.ExecuteCommand("Delete From tblTransactionPayFees Where CCF_TransactionPayID=" + payID);
			dc.ExecuteCommand("Delete From tblTransactionPay Where ID=" + payID);
			dc.ExecuteCommand("Delete From tblWireMoney Where WireType=1 And WireSourceTbl_id=" + payID);
		}

		public static Settlement2VO GetPayment(Guid credentialsToken, int payID)
		{
			Infrastructure.Security.User user = Infrastructure.Security.SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayUser, UserType.NetpayAdmin });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Host);
			Settlement2VO settlment = null;
			tblTransactionPay payment = (from t in dc.tblTransactionPays where t.id == payID select t).Single();
			if (payment == null) throw new ApplicationException(string.Format("payment {0} not found", payID));
			settlment = new Settlement2VO(payment);
			if (payment.IsTotalsCached)
			{
				var list = (from sa in dc.tblSettlementAmountCaches where sa.SettlementID == settlment.payId select sa).ToList();
				foreach (var item in list)
					settlment.Totals[(TransactionAmountType)item.TypeID].Add(item.SettledAmount.GetValueOrDefault(0), item.SettledCount.GetValueOrDefault(0));
			}
			else
			{
				CreatePaymentInfo cpi = new CreatePaymentInfo();
				cpi.merchantID = payment.CompanyID;
				cpi.currencyID = (Currency)payment.Currency;
				cpi.payID = payment.id;
				settlment = CreatePayment(credentialsToken, cpi);
			}
			return settlment;
		}

		private static int AddPaymentAmount(NetpayDataContext dc, Settlement2VO settlment, bool apply, TransactionAmountType tat, decimal amount, string comment, DateTime? dateRelease)
		{
			if (amount == 0m) return 0;
			CountAmount ca;
			if (!settlment.Totals.TryGetValue(tat, out ca)) settlment.Totals.Add(tat, ca = new CountAmount());
			ca.Add(amount, 1);

			tblSettlementAmount sa = new tblSettlementAmount();
			tblTransactionAmount ta = new tblTransactionAmount();
			sa.ItemCount = 1;
			sa.Description = comment;
			sa.ReleaseDate = dateRelease;
			ta.SettledAmount = amount;
			ta.SettledCurrency = (int)settlment.currencyID;
			ta.MerchantID = settlment.merchantId;
			ta.InsertDate = settlment.payDate;
			ta.SettlementID = settlment.payId;
			ta.TypeID = (int)tat;
			settlment.SettlmentAmounts.Add(new SettlementAmountVO(sa, ta));

			if (!apply) return 0;
			dc.tblSettlementAmounts.InsertOnSubmit(sa);
			dc.SubmitChanges();
			ta.SettlementAmountID = sa.ID;
			dc.tblTransactionAmounts.InsertOnSubmit(ta);
			dc.SubmitChanges();
			return ta.ID;
		}

		private static int IAddRetReserve(NetpayDataContext dc, Settlement2VO settlment, bool apply, tblTransactionAmount ta)
		{
			int pRet = AddPaymentAmount(dc, settlment, apply, TransactionAmountType.RollingRelease, ta.SettledAmount.GetValueOrDefault(), string.Format("Release Reserve ({0})", ta.InsertDate.ToString("yyyy/MM/dd")), null);
			if (apply) dc.ExecuteCommand("Update tblSettlementAmount Set ReferenceNumber=" + pRet + " Where ID=" + ta.SettlementAmountID);
			return pRet;
		}

		private class paymentTotalRawData { public short? PaymentMethod = null; public int TypeID = 0; public int evCount = 0; public decimal evAmount = 0m; }
		private static CountAmount GetAmountTotal(Dictionary<TransactionAmountType, CountAmount> amounts, TransactionAmountType[] events)
		{
			CountAmount ca = new CountAmount();
			if (events != null)
			{
				foreach (var e in events)
					if (amounts.ContainsKey(e)) ca.Add(amounts[e]);
			}
			else
			{
				foreach (var am in amounts) ca.Add(am.Value);
			}
			return ca;
		}

		public static Settlement2VO CreatePayment(Guid credentialsToken, CreatePaymentInfo createInfo)
		{
			decimal curReservedAmount = 0, rrAmount = 0;
			Settlement2VO settlment = null;
			tblTransactionPay payment = null;
			Dictionary<PaymentMethodEnum, Dictionary<TransactionAmountType, CountAmount>> pmAmounts = new Dictionary<PaymentMethodEnum, Dictionary<TransactionAmountType, CountAmount>>();
			Infrastructure.Security.User user = Infrastructure.Security.SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayUser, UserType.NetpayAdmin });
			var dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var merchant = Merchant.Merchant.Load(credentialsToken, createInfo.merchantID);
			if (merchant == null) throw new ApplicationException("unable to load merchant info");

			if (createInfo.payID > 0)
			{
				if (createInfo.apply)
				{
					if (createInfo.excludeInstallmets != null) dc.ExecuteCommand(string.Format("Update tblTransactionAmount Set SettlementID=NULL Where ID IN({0})", string.Join<int>(", ", createInfo.excludeInstallmets)));
					if (createInfo.excludeTransactions != null) dc.ExecuteCommand(string.Format("Update tblTransactionAmount Set SettlementID=NULL Where TransPassID IN({0})", string.Join<int>(", ", createInfo.excludeTransactions)));
					if (createInfo.excludeSettleAmount != null) dc.ExecuteCommand(string.Format("Update tblSettlementAmount Set SettlementID=NULL Where ID IN({0})", string.Join<int>(", ", createInfo.excludeSettleAmount)));
					dc.ExecuteCommand("Delete From tblSettlementAmount Where ID IN(Select SettlementAmountID From tblTransactionAmount Where MerchantID=" + createInfo.merchantID + " And SettlementID=" + createInfo.payID + " And TypeID IN(" + (int)TransactionAmountType.RollingReserve + "," + (int)TransactionAmountType.DebitorDirectPayment + "," + (int)TransactionAmountType.Tax + ")");
				}
				payment = (from t in dc.tblTransactionPays where t.id == createInfo.payID select t).SingleOrDefault();
				if (payment == null) throw new ApplicationException("unable to load payment record");
				createInfo.rollingReserve = (payment.SecurityDeposit != 0);

			}
			else
			{
				tblBillingCompany billingCompany = (from b in dc.tblBillingCompanies where b.BillingCompanys_id == merchant.BillingCompanysId select b).Single();
				payment = new tblTransactionPay();
				payment.BillingCompanys_id = billingCompany.BillingCompanys_id;
				payment.BillingLanguageShow = billingCompany.LanguageShow;
				payment.BillingCurrencyShow = (byte)billingCompany.currencyShow;
				payment.BillingCompanyName = billingCompany.name;
				payment.BillingCompanyAddress = billingCompany.address;
				payment.BillingCompanyNumber = billingCompany.number;
				payment.BillingCompanyEmail = billingCompany.email;
				payment.BillingCreateDate = DateTime.Now;
				payment.BillingToInfo = String.Format("{0} {1} {2}, {3}", merchant.LegalName, merchant.Address, merchant.City, merchant.Zipcode).Truncate(2000);
				payment.PayDate = createInfo.payDate;
				payment.CompanyID = createInfo.merchantID;
				payment.Currency = (int)createInfo.currencyID;
				payment.isChargeVAT = merchant.IsChargeVAT;
				payment.VatAmount = merchant.IsChargeVAT ? (decimal)billingCompany.VATamount : 0;
				payment.SecurityDeposit = merchant.DepositInfo.SecurityDeposit;
				payment.PayPercent = merchant.PayPercent;
				payment.IsGradedFees = (merchant.GradedFeeCurrency != Currency.Unknown);
				payment.PrimePercent = (double)dc.ExecuteQuery<float?>("SELECT Prime FROM tblGlobalValues").Single().GetValueOrDefault(0);
				if (createInfo.currencyID != Currency.ILS) payment.ExchangeRate = (double)Money.ConvertRate(user.Domain, createInfo.currencyID, Currency.ILS);
				else payment.ExchangeRate = (double)Money.ConvertRate(user.Domain, Currency.USD, createInfo.currencyID);
				payment.Comment = "";
				if (createInfo.apply)
				{
					dc.tblTransactionPays.InsertOnSubmit(payment);
					dc.SubmitChanges();
					//copy fees to settlment
					dc.ExecuteCommand("Insert Into tblTransactionPayFees (CCF_TransactionPayID, CCF_CompanyID, CCF_CurrencyID, CCF_PaymentMethod, CCF_ExchangeTo, CCF_MaxAmount, CCF_PercentFee, CCF_FixedFee, CCF_FailFixedFee, CCF_ApproveFixedFee, CCF_RefundFixedFee, CCF_ClarificationFee, CCF_CBFixedFee, CCF_ListBINs)" +
						"Select " + payment.id + ",CCF_CompanyID, CCF_CurrencyID, CCF_PaymentMethod, CCF_ExchangeTo, CCF_MaxAmount, CCF_PercentFee, CCF_FixedFee, CCF_FailFixedFee, CCF_ApproveFixedFee, CCF_RefundFixedFee, CCF_ClarificationFee, CCF_CBFixedFee, CCF_ListBINs From tblCompanyCreditFees Where CCF_CompanyID=" + createInfo.merchantID + " And CCF_ExchangeTo=" + (int)createInfo.currencyID);
					dc.ExecuteCommand("Insert Into tblPaymentFeesFloor (CFF_PayID, CFF_CompanyID, CFF_TotalTo, CFF_Precent)" +
						"Select " + payment.id + ",CFF_CompanyID, CFF_TotalTo, CFF_Precent From tblCompanyFeesFloor Where CFF_CompanyID=" + createInfo.merchantID);
				}
			}
			settlment = new Settlement2VO(payment);
			//load settlment amount data
			/*
			if (createInfo.payID > 0)
			{
				if (!pmAmounts.ContainsKey(PaymentMethod.Unknown)) pmAmounts.Add(PaymentMethod.Unknown, new Dictionary<TransactionAmountType, CountAmount>());
				settlment.SettlmentAmounts = (from sa in dc.tblSettlementAmounts where sa.SettlementID == createInfo.payID && sa.MerchantID == payment.CompanyID select new SettlementAmountVO(sa)).ToList();
				foreach (var item in settlment.SettlmentAmounts)
				{
					CountAmount ca;
					if (!pmAmounts[PaymentMethod.Unknown].TryGetValue((TransactionAmountType)item.TypeID, out ca)) pmAmounts[PaymentMethod.Unknown].Add((TransactionAmountType)item.TypeID, ca = new CountAmount());
					ca.Add(item.SettledAmount.GetValueOrDefault(), item.SettledCount.GetValueOrDefault());
				}
			}
			//settlment.SettlementAmounts = (from sa in dc.tblSettlementAmounts where sa.SettlementID.GetValueOrDefault(0) == createInfo.payID select new SettlementAmountVO(sa)).ToList();
			*/
			//add transactions
			StringBuilder query = new StringBuilder();
			query.Append(" Where MerchantID =" + createInfo.merchantID);
			query.Append(" And SettledCurrency =" + (int)createInfo.currencyID);
			query.Append(" And IsNull(SettlementID, 0) =" + createInfo.payID);
			if (createInfo.apply || createInfo.payID <= 0)
			{
				if (createInfo.insertDateFrom != null) query.Append(" And InsertDate >= " + createInfo.insertDateFrom.Value.ToSqlValue());
				if (createInfo.insertDateTo != null) query.Append(" And InsertDate <= " + createInfo.insertDateTo.Value.ToSqlValue());
				if (createInfo.merchantPayDateFrom != null) query.Append(" And SettlementDate >= " + createInfo.merchantPayDateFrom.Value.ToSqlValue());
				if (createInfo.merchantPayDateTo != null) query.Append(" And SettlementDate <= " + createInfo.merchantPayDateTo.Value.ToSqlValue());
				if (!createInfo.includeTest) query.Append(" And isTestOnly=0");
				if (createInfo.creditType != null) query.Append(" And CreditType IN (" + string.Join<CreditType>(", ", createInfo.creditType) + ")");
				if (createInfo.includePaymentMethods != null) query.Append(" And PaymentMethod IN (" + string.Join<PaymentMethodEnum>(", ", createInfo.includePaymentMethods) + ")");
				if (createInfo.excludePaymentMethods != null) query.Append(" And PaymentMethod Not IN (" + string.Join<PaymentMethodEnum>(", ", createInfo.excludePaymentMethods) + ")");
				if (createInfo.includeTransactions != null) query.Append(" And TransPassID IN (" + string.Join<int>(", ", createInfo.includeTransactions) + ")");
				if (createInfo.includeInstallmets != null) query.Append(" And ID IN (" + string.Join<int>(", ", createInfo.includeInstallmets) + ")");

				query = new StringBuilder(
					"SELECT PaymentMethod, TypeID, Count(tblTransactionAmount.ID) As evCount, Sum(tblTransactionAmount.SettledAmount) As evAmount" +
					" From tblTransactionAmount Inner Join tblCompanyTransPass ON(tblTransactionAmount.TransPassID = tblCompanyTransPass.ID)" + query.ToString() +
					" Group By PaymentMethod, TypeID" +
					" Union " +
					"SELECT PaymentMethod, TypeID, Count(tblTransactionAmount.ID) As evCount, Sum(tblTransactionAmount.SettledAmount) As evAmount" +
					" From tblTransactionAmount Inner Join tblCompanyTransFail ON(tblTransactionAmount.TransFailID = tblCompanyTransFail.ID)" + query.ToString() +
					" Group By PaymentMethod, TypeID" +
					" Union " +
					"SELECT PaymentMethod, TypeID, Count(tblTransactionAmount.ID) As evCount, Sum(tblTransactionAmount.SettledAmount) As evAmount" +
					" From tblTransactionAmount Inner Join tblCompanyTransApproval ON(tblTransactionAmount.TransApprovalID = tblCompanyTransApproval.ID)" + query.ToString() +
					" Group By PaymentMethod, TypeID"
				);
			}
			else
			{
				query = new StringBuilder(
					"SELECT Cast(0 as smallint), TypeID, Count(tblTransactionAmount.ID) As evCount, Sum(tblTransactionAmount.SettledAmount) As evAmount" +
					" From tblTransactionAmount " + query.ToString() +
					" Group By TypeID");
				settlment.SettlmentAmounts = (from sa in dc.tblSettlementAmounts join ta in dc.tblTransactionAmounts on sa.ID equals ta.SettlementAmountID where ta.SettlementID.GetValueOrDefault(0) == createInfo.payID select new SettlementAmountVO(sa, ta)).ToList();
			}
			/*
			var exp = from ta in dc.tblTransactionAmounts 
				join tp in dc.tblCompanyTransPasses on ta.TransPassID equals tp.ID
				where ta.MerchantID == createInfo.merchantID && ta.Currency == (int)createInfo.currencyID && ta.SettlementID == createInfo.payID
				select new { amountData = ta, passData = tp };

			if (createInfo.insertDateFrom != null) exp = exp.Where(td => td.amountData.InsertDate >= createInfo.insertDateFrom);
			if (createInfo.insertDateTo != null) exp = exp.Where(td => td.amountData.InsertDate <= createInfo.insertDateTo);
			if (createInfo.merchantPayDateFrom != null) exp = exp.Where(td => td.amountData.SettlementDate >= createInfo.merchantPayDateFrom);
			if (createInfo.merchantPayDateTo != null) exp = exp.Where(td => td.amountData.SettlementDate <= createInfo.merchantPayDateTo);

			if (createInfo.creditType != null) exp = exp.Where(td => createInfo.creditType.Contains((CreditType)td.passData.CreditType));
			if (createInfo.includePaymentMethods != null) exp = exp.Where(td => createInfo.includePaymentMethods.Contains((PaymentMethod)td.passData.PaymentMethod.GetValueOrDefault()));
			if (createInfo.excludePaymentMethods != null) exp = exp.Where(td => createInfo.excludePaymentMethods.Contains((PaymentMethod)td.passData.PaymentMethod.GetValueOrDefault()));

			if (createInfo.includeTransactions != null) exp = exp.Where(td => createInfo.includeTransactions.Contains(td.passData.ID));
			if (createInfo.includeInstallmets != null) exp = exp.Where(td => createInfo.includeInstallmets.Contains(td.amountData.ID));

			var result = exp.GroupBy(td => new { td.passData.PaymentMethod, td.amountData.TypeID } ).Select( g => new  {  PaymentMethod = g.Key.PaymentMethod, TypeID = g.Key.TypeID , evAmount = g.Sum(ta => ta.amountData.Amount), evCount = g.Count() } );
			var list = result.ToList();
			*/
			System.Web.HttpContext.Current.Response.Write(query);
			var list = dc.ExecuteQuery<paymentTotalRawData>(query.ToString()).ToList();
			foreach (var item in list)
			{
				CountAmount ca;
				PaymentMethodEnum pm = (PaymentMethodEnum)item.PaymentMethod.GetValueOrDefault(0);
				TransactionAmountType typeid = (TransactionAmountType)item.TypeID;
				if (!pmAmounts.ContainsKey(pm)) pmAmounts.Add(pm, new Dictionary<TransactionAmountType, CountAmount>());
				if (!pmAmounts[pm].TryGetValue(typeid, out ca)) pmAmounts[pm].Add(typeid, ca = new CountAmount());
				ca.Add(item.evAmount, item.evCount);
			}
			if (createInfo.apply)
			{
				dc.ExecuteCommand("Update tblTransactionAmount Set SettlementID=" + payment.id + ", SettledCurrency=" + (int)payment.Currency + ", SettlementDate=" + payment.PayDate.ToSqlValue() + " From tblTransactionAmount Inner Join tblCompanyTransPass ON( tblTransactionAmount.TransPassID = tblCompanyTransPass.ID )" + query);
				dc.ExecuteCommand("Update tblTransactionAmount Set SettlementID=" + payment.id + ", SettledCurrency=" + (int)payment.Currency + ", SettlementDate=" + payment.PayDate.ToSqlValue() + " From tblTransactionAmount Inner Join tblCompanyTransFail ON( tblTransactionAmount.TransFailID = tblCompanyTransFail.ID )" + query);
				dc.ExecuteCommand("Update tblTransactionAmount Set SettlementID=" + payment.id + ", SettledCurrency=" + (int)payment.Currency + ", SettlementDate=" + payment.PayDate.ToSqlValue() + " From tblTransactionAmount Inner Join tblCompanyTransApproval ON( tblTransactionAmount.TransApprovalID = tblCompanyTransApproval.ID )" + query);
			}

			//merge amounts
			foreach (var va in pmAmounts)
				foreach (var vc in va.Value)
				{
					if (!settlment.Totals.ContainsKey(vc.Key)) settlment.Totals.Add(vc.Key, new CountAmount());
					settlment.Totals[vc.Key].Add(vc.Value);
				}
			//if only read old then quit
			if (createInfo.payID > 0 && !createInfo.apply)
			{
				settlment.Total = GetAmountTotal(settlment.Totals, null).Amount;
				return settlment;
			}
			//add storage
			if (createInfo.includeStorage)
			{
                var storageList = dc.ExecuteQuery<CountAmount>("Select StorageFee As Amount, Count(*) As Count From tblCCStorage Left Join Setting.SetMerchantSettlement mcs ON(tblCCStorage.companyID = mcs.Merchant_ID And mcs.Currency_ID=" + (int)createInfo.currencyID + ") Where PayID=" + createInfo.payID + " And CompanyID=" + createInfo.merchantID + " Group By StorageFee").ToList();
				foreach (var v in storageList)
					AddPaymentAmount(dc, settlment, createInfo.apply, TransactionAmountType.FeeCCStorage, (v.Amount * v.Count), string.Format("CC storage fee ({0} X {1})", v.Count, v.Amount.ToAmountFormat(user.Domain.Host, (int)settlment.currencyID)), null);
				if (createInfo.apply) dc.ExecuteCommand("Update tblCCStorage Set PayID=" + settlment.payId + " Where PayID=0 And CompanyID=" + createInfo.merchantID);
			}

			//add rolling release
			if (createInfo.rollingRelease)
			{
				if ((merchant.DepositInfo.State == MerchantDepositState.HoldDynamically && merchant.DepositInfo.AutoRet) || merchant.DepositInfo.State == MerchantDepositState.HoldSingle || merchant.DepositInfo.State == MerchantDepositState.HoldFixedAmount)
				{
					var retList = (from sa in dc.tblSettlementAmounts
								   join ta in dc.tblTransactionAmounts on sa.ID equals ta.SettlementAmountID
								   where ta.MerchantID == createInfo.merchantID && ta.SettledCurrency == (int)createInfo.currencyID &&
									   ((ta.TypeID == (int)TransactionAmountType.RollingReserve && ta.SettlementID == createInfo.payID) ||
									   (ta.TypeID == (int)TransactionAmountType.RollingReserve && sa.ReferenceNumber == null && sa.ReleaseDate <= createInfo.payDate.AddMonths(-merchant.DepositInfo.SecurityPeriod)))
								   select new { ta, sa }).ToList();
					foreach (var r in retList)
					{
						if (merchant.DepositInfo.State == MerchantDepositState.HoldFixedAmount)
						{
							decimal nItemValue;
							if ((int)merchant.DepositInfo.KeepCurrency != 255) nItemValue = new Money(user.Domain, r.ta.SettledAmount.GetValueOrDefault(), (Currency)r.ta.SettledCurrency).ConvertTo(user.Domain.Cache.Currencies[(int)merchant.DepositInfo.KeepCurrency]).Amount;
							else nItemValue = r.ta.SettledAmount.GetValueOrDefault();
							if (curReservedAmount < merchant.DepositInfo.KeepAmount) break;
							curReservedAmount = curReservedAmount - nItemValue;
						}
						IAddRetReserve(dc, settlment, createInfo.apply, r.ta);
					}
				}
			}

			//add rolling reserve
			CountAmount RollingReserve = null;
			if (createInfo.rollingReserve)
			{
				decimal rollingReserveAmount = 0;
				if (createInfo.payID > 0 && !createInfo.apply) rollingReserveAmount = rrAmount;
				else if (merchant.DepositInfo.State == MerchantDepositState.HoldExternal) rollingReserveAmount = rrAmount;
				else
				{
					if (merchant.DepositInfo.State == MerchantDepositState.HoldSingle)
					{
						decimal? rollingTook = dc.ExecuteQuery<decimal?>("Select Top 1 Amount From tblTransactionAmount Where MerchantID=" + createInfo.merchantID +
							 " And TypeID=" + (int)TransactionAmountType.RollingReserve + " And InsertDate < " + createInfo.payDate.AddMonths(-merchant.DepositInfo.SecurityPeriod).ToSqlValue()).Single();
						merchant.DepositInfo.Enable = rollingTook.Value > 0m ? false : true;
					}
					//detect amount need to be taken
					var needRollingAmount = -GetAmountTotal(settlment.Totals, new TransactionAmountType[] { TransactionAmountType.Capture, TransactionAmountType.InstallmentCapture }).Amount;
					if (
						(merchant.DepositInfo.State == MerchantDepositState.HoldDynamically || merchant.DepositInfo.State == MerchantDepositState.HoldSingle || merchant.DepositInfo.State == MerchantDepositState.HoldFixedAmount)
						&& merchant.DepositInfo.Enable && (needRollingAmount > 0))
					{
						if (merchant.DepositInfo.State == MerchantDepositState.HoldDynamically)
						{
							rollingReserveAmount = -(needRollingAmount * (merchant.DepositInfo.SecurityDeposit / 100));
						}
						else
						{
							decimal nMax = (merchant.DepositInfo.KeepAmount - curReservedAmount);
							if (nMax < 0)
							{
								rollingReserveAmount = 0;
							}
							else
							{
								rollingReserveAmount = (needRollingAmount * (merchant.DepositInfo.SecurityDeposit / 100));
								if (nMax < rollingReserveAmount) rollingReserveAmount = nMax;
								rollingReserveAmount = -rollingReserveAmount;
							}
						}
					}
					RollingReserve = new CountAmount(-rollingReserveAmount, 1);
				}
			}

			//finnal updates
			if (RollingReserve != null && RollingReserve.Amount != 0) AddPaymentAmount(dc, settlment, createInfo.apply, TransactionAmountType.RollingReserve, RollingReserve.Amount, string.Format("Rolling Reserve ({0}%)", merchant.DepositInfo.State == MerchantDepositState.HoldExternal ? "fixed external" : settlment.securityDeposit.ToString("0.00")), createInfo.payDate.AddMonths(merchant.DepositInfo.SecurityPeriod));
			if (settlment.payPercent != 100)
			{
				decimal notPayPercent = -(1 - (settlment.payPercent / 100)) * GetAmountTotal(settlment.Totals, new TransactionAmountType[] { TransactionAmountType.Capture, TransactionAmountType.InstallmentItem, TransactionAmountType.Refund, TransactionAmountType.Chargeback, TransactionAmountType.DebitorDirectFees }).Amount;
				AddPaymentAmount(dc, settlment, createInfo.apply, TransactionAmountType.DebitorDirectPayment, notPayPercent, string.Format("Direct Deposit Of {0}%", settlment.payPercent.ToString("0.00")), null);
			}
			if (settlment.vatRate > 0)
			{
				decimal vatAmount = settlment.vatRate * GetAmountTotal(settlment.Totals, new TransactionAmountType[] { TransactionAmountType.FeeTransaction, TransactionAmountType.FeeLine, TransactionAmountType.FeeAuthorization, TransactionAmountType.FeeChb, TransactionAmountType.FeeClarification, TransactionAmountType.FeeCreateInvoice, TransactionAmountType.FeeCCStorage, TransactionAmountType.FeeDeclined }).Amount;
				AddPaymentAmount(dc, settlment, createInfo.apply, TransactionAmountType.Tax, vatAmount, string.Format("Tax Of {0}%", settlment.vatRate.ToString("0.00")), null);
			}

			settlment.Total = GetAmountTotal(settlment.Totals, null).Amount;


			if (createInfo.apply)
			{
				if (createInfo.payID != 0) dc.ExecuteCommand("Delete From tblSettlementAmountCach Where SettlementID=" + createInfo.payID + " And MerchantID=" + createInfo.merchantID);
				foreach (var e in settlment.Totals)
				{
					if (e.Value.IsZero) continue;
					Dal.Netpay.tblSettlementAmountCach sa = new Dal.Netpay.tblSettlementAmountCach();
					sa.InsertDate = DateTime.Now;
					sa.MerchantID = settlment.merchantId;
					sa.SettledCount = e.Value.Count;
					sa.SettledAmount = e.Value.Amount;
					sa.TypeID = (int)e.Key;
					sa.SettlementID = settlment.payId;
					sa.SettledCurrency = (int)settlment.currencyID;
					dc.tblSettlementAmountCaches.InsertOnSubmit(sa);
				}
				dc.SubmitChanges();
			}
			return settlment;
		}

		public static void CreatePaymentWire(Guid credentialsToken, Settlement2VO settlment)
		{
			Infrastructure.Security.User user = Infrastructure.Security.SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayUser, UserType.NetpayAdmin });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			int execCount = dc.ExecuteCommand(string.Format("Update tblWireMoney SET WireAmount={0}-wireFee Where WireType=1 And WireSourceTbl_id={1}", settlment.Total, settlment.payId));
			if (execCount == 0)
			{
				decimal exchangeRate = 0;
				Currency receiveCurrencyId = (Currency)dc.ExecuteQuery<int>(string.Format("Select dbo.GetMerchantPayoutCurrency({0}, {1})", settlment.merchantId, (int)settlment.currencyID)).Single();
				exchangeRate = System.Math.Round(Money.ConvertRateWithFee(user.Domain, settlment.currencyID, receiveCurrencyId), 5);
				dc.ExecuteCommand(String.Format("INSERT INTO tblWireMoney (Company_id, WireSourceTbl_id, WireInsertDate, WireType, WireCurrency, WireExchangeRate, wireCompanyName," +
					" wireCompanyLegalName, wireIDnumber, wireCompanyLegalNumber, wirePaymentMethod, wirePaymentPayeeName, wirePaymentBank, wirePaymentBranch," +
					" wirePaymentAccount, wirePaymentAbroadAccountName, wirePaymentAbroadAccountNumber, wirePaymentAbroadBankName," +
					" wirePaymentAbroadBankAddress, wirePaymentAbroadBankAddressSecond, wirePaymentAbroadBankAddressCity, wirePaymentAbroadBankAddressState," +
					" wirePaymentAbroadBankAddressZip, wirePaymentAbroadBankAddressCountry, wirePaymentAbroadSwiftNumber, wirePaymentAbroadIBAN," +
					" wirePaymentAbroadABA, wirePaymentAbroadSortCode, wirePaymentAbroadAccountName2, wirePaymentAbroadAccountNumber2," +
					" wirePaymentAbroadBankName2, wirePaymentAbroadBankAddress2, wirePaymentAbroadBankAddressSecond2, wirePaymentAbroadBankAddressCity2," +
					" wirePaymentAbroadBankAddressState2, wirePaymentAbroadBankAddressZip2, wirePaymentAbroadBankAddressCountry2," +
					" wirePaymentAbroadSwiftNumber2, wirePaymentAbroadIBAN2, wirePaymentAbroadABA2, wirePaymentAbroadSortCode2)" +
					" SELECT {0}, {1}, '{2}', 1, {3}, {4}, CompanyName," +
					" CompanyLegalName, IDnumber, CompanyLegalNumber, PaymentMethod, PaymentPayeeName, PaymentBank, PaymentBranch, PaymentAccount," +
					" mba_AccountName, mba_AccountNumber, mba_BankName, mba_BankAddress, mba_BankAddressSecond, mba_BankAddressCity, mba_BankAddressState," +
					" mba_BankAddressZip, mba_BankAddressCountry, mba_SwiftNumber, mba_IBAN, mba_ABA, mba_SortCode, mba_AccountName2, mba_AccountNumber2," +
					" mba_BankName2, mba_BankAddress2, mba_BankAddressSecond2, mba_BankAddressCity2, mba_BankAddressState2, mba_BankAddressZip2," +
					" mba_BankAddressCountry2, mba_SwiftNumber2, mba_IBAN2, mba_ABA2, mba_SortCode2" +
					" FROM tblCompany INNER JOIN GetMerchantBankAccount({0}, {5}) ON ID=mba_Merchant",
					settlment.merchantId, settlment.payId, settlment.payDate.ToSql(), (int)settlment.currencyID, exchangeRate, (int)receiveCurrencyId));
			}
		}
	}
}
