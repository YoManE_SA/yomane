﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.ModuleManager
{
    public class Module : Infrastructure.Module
    {        
        public const string ModuleName = "ModuleManager";

        //The SecuredObjectName is added here beacause there is no class in bll that has
        //Functionality issued to ModuleManager module except this Module class.
        public const string SecuredObjectName = "Manage";

        //The SecuredObject is added here beacause there is no class in bll that has
        //Functionality issued to ModuleManager module except this Module class.
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Current, SecuredObjectName); } }

        public override string Name { get { return ModuleName; } }
        public override decimal Version { get { return 1.0m; } }
        public static Module Current { get { return Infrastructure.Module.Get(ModuleName) as Module; } }

        protected override void OnInstall(EventArgs e)
        {
            Infrastructure.Security.SecuredObject.Create(this, SecuredObjectName, PermissionGroup.ReadEditDelete);
            base.OnInstall(e);
        }

        protected override void OnUninstall(EventArgs e)
        {
            Infrastructure.Security.SecuredObject.Remove(this);
            base.OnUninstall(e);
        }

    }
}
