﻿using System;
using System.Collections.Generic;
using System.Linq;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Domains;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure.VO;
using Netpay.CommonTypes;
using Netpay.Dal.Reports;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.Xml.Linq;
using System.ServiceModel.Channels;
using System.Net;
using System.Text;
using System.IO;

namespace Netpay.Bll
{
    /// <summary>
    /// Merchant related logic.
    /// </summary>
    public static class Merchants
    {
        private static object _partnerConfigLock = new object();
		public static MerchantPartnerSettingsVO GetPartnerSettings(Guid credentialsToken, int partnerId)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			Netpay.Dal.Netpay.SetMerchantAffiliate settings = (from s in dc.SetMerchantAffiliates where s.Merchant_id == user.ID && s.Affiliate_id == partnerId select s).SingleOrDefault();
            if (settings == null)
                return null;

            return new MerchantPartnerSettingsVO(settings);
        }

        public static MerchantPartnerSettingsVO GetPartnerSettings(string domainHost, int merchantId, int partnerId)
        {
            var doamin = Infrastructure.Domains.DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(doamin.Sql1ConnectionString);
			Netpay.Dal.Netpay.SetMerchantAffiliate settings = (from s in dc.SetMerchantAffiliates where s.Merchant_id == merchantId && s.Affiliate_id == partnerId select s).SingleOrDefault();
            if (settings == null)
                return null;
            return new MerchantPartnerSettingsVO(settings);
        }

        public static void SavePartnerSettings(string domainHost, int merchantId, int partnerId, MerchantPartnerSettingsVO settings)
        {
            var doamin = Infrastructure.Domains.DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(doamin.Sql1ConnectionString);
			if (!string.IsNullOrEmpty(settings.UserID)){
				var umid = (from s in dc.SetMerchantAffiliates where s.Affiliate_id == partnerId && s.UserID == settings.UserID select s.Merchant_id).FirstOrDefault();
				if (umid != 0 && umid != merchantId) throw new Exception("User already registered on another merchant");
			}
			Netpay.Dal.Netpay.SetMerchantAffiliate entity = (from s in dc.SetMerchantAffiliates where s.Merchant_id == merchantId && s.Affiliate_id == partnerId select s).SingleOrDefault();
            if (entity == null) {
				entity = new Dal.Netpay.SetMerchantAffiliate();
				entity.Merchant_id = merchantId;
				entity.Affiliate_id = partnerId;
				dc.SetMerchantAffiliates.InsertOnSubmit(entity);
			}
			entity.UserID = settings.UserID;
            dc.SubmitChanges();
        }

        public static void SetPartnerSyncDate(string domainHost, int partnerId, int merchantId, DateTime? syncDate)
        {
            lock (_partnerConfigLock)
            {
                var domain = Infrastructure.Domains.DomainsManager.GetDomain(domainHost);
				var dc = new Netpay.Dal.Netpay.NetpayDataContextBase(domain.Sql1ConnectionString);
                var refData = (from sma in dc.SetMerchantAffiliates where sma.Affiliate_id == partnerId && sma.Merchant_id == merchantId select sma).SingleOrDefault();
                refData.SyncDateTime = syncDate;
                dc.SubmitChanges();
            }
        }

        public static bool SavePartnerServiceConfig(string domainHost, int partnerId, int merchantId, string configName, string configValue)
        {
            lock (_partnerConfigLock)
            {
                var domain = Infrastructure.Domains.DomainsManager.GetDomain(domainHost);
				var dc = new Netpay.Dal.Netpay.NetpayDataContextBase(domain.Sql1ConnectionString);
                var refData = (from sma in dc.SetMerchantAffiliates where sma.Affiliate_id == partnerId && sma.Merchant_id == merchantId select sma).SingleOrDefault();
                if (refData == null) return false;
                var serviceData = System.Web.HttpUtility.ParseQueryString(refData.ConfigurationValues ?? "");
                serviceData[configName] = configValue;
                serviceData["LASTUPDATE"] = DateTime.Now.ToString("s");
                refData.ConfigurationValues = serviceData.ToString();
                dc.SubmitChanges();
            }
            return true;
        }

        public static List<MerchantRegistrationVO> GetMerchantRegistrations(Guid credentialsToken)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser });
			string path = FilesManager.GetPrivateAccessPath(user.Domain.Host, UserType.NetpayAdmin, FilesManager.SubFolders.MerchantRegistrations);
			string[] files = Directory.GetFiles(path);

			List<MerchantRegistrationVO> registrations = new List<MerchantRegistrationVO>();
			foreach (string file in files) 
			{
				string content = File.ReadAllText(file);
				MerchantRegistrationVO registration = content.FromXml<MerchantRegistrationVO>(UnicodeEncoding.UTF8);
				registration.DecryptBankInfo(user.Domain.Host);
				registrations.Add(registration);
			}

			return registrations;
        }

		public static MerchantRegistrationVO GetMerchantRegistration(Guid credentialsToken, Guid registrationID)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser });
			string path = FilesManager.GetPrivateAccessPath(user.Domain.Host, UserType.NetpayAdmin, FilesManager.SubFolders.MerchantRegistrations) + registrationID + ".xml";

			string content = File.ReadAllText(path);
			MerchantRegistrationVO registration = content.FromXml<MerchantRegistrationVO>(UnicodeEncoding.UTF8);
			registration.DecryptBankInfo(user.Domain.Host);
	
			return registration;
		}

		public static void UpdateRegistration(Guid credentialsToken, MerchantRegistrationVO registration)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser });
			string path = FilesManager.GetPrivateAccessPath(user.Domain.Host, UserType.NetpayAdmin, FilesManager.SubFolders.MerchantRegistrations) + registration.ID + ".xml";
			registration.EncryptBankInfo(user.Domain.Host);
			string updatedContent = registration.ToXml();
			File.WriteAllText(path, updatedContent);
		}

        public static void DeleteRegistration(Guid credentialsToken, Guid registrationID)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser });
			string path = FilesManager.GetPrivateAccessPath(user.Domain.Host, UserType.NetpayAdmin, FilesManager.SubFolders.MerchantRegistrations) + registrationID + ".xml";
			File.Delete(path);
        }

        public static bool CreateMerchantLocally(Guid credentialsToken, Guid registrationID) 
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser });
			string path = FilesManager.GetPrivateAccessPath(user.Domain.Host, UserType.NetpayAdmin, FilesManager.SubFolders.MerchantRegistrations) + registrationID + ".xml";
			string content = File.ReadAllText(path);
			MerchantRegistrationVO reg = content.FromXml<MerchantRegistrationVO>();
			reg.DecryptBankInfo(user.Domain.Host);

            // insert merchant to netpay  
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            string sql = "SET NOCOUNT ON;INSERT INTO tblCompany (CompanyName, ActiveStatus) VALUES ('New Company', " + (int)MerchantStatus.New + ");SELECT SCOPE_IDENTITY() AS NewCompanyID;SET NOCOUNT OFF";
            IEnumerable<decimal> result = dc.ExecuteQuery<decimal>(sql);
            int merchantId = (int)result.First();

			Netpay.Dal.Netpay.tblCompany merchantEntity = (from m in dc.tblCompanies where m.ID == merchantId select m).SingleOrDefault();
            merchantEntity.ActiveStatus = (int)MerchantStatus.New;
            merchantEntity.InsertDate = DateTime.Now;
            merchantEntity.CompanyName = reg.DbaName;
			merchantEntity.CompanyLegalName = reg.LegalBusinessName;
			merchantEntity.CompanyLegalNumber = reg.LegalBusinessNumber;
			merchantEntity.HashKey = Netpay.Infrastructure.Security.Encryption.GetHashKey(10);
            //merchantEntity.merchantOpenningDate = DateTime.Now;
            //merchantEntity.merchantClosingDate = DateTime.Now;
            //merchantEntity.CFF_ResetDate = DateTime.Now;
            //merchantEntity.lastInsertDate = DateTime.Now;
            //merchantEntity.PasswordUpdate = DateTime.Now;
            merchantEntity.Mail = reg.Email;
            //merchantEntity.UserName = merchantData.UserName;
            //merchantEntity.CompanyCountry = merchantData.Country;
            //merchantEntity.Country = merchantData.ContactCountry;
			merchantEntity.IDnumber = reg.OwnerSsn;
            merchantEntity.CompanyCity = reg.PhisicalCity;
            merchantEntity.City = reg.City;
            merchantEntity.CompanyStreet = reg.PhisicalAddress;
            merchantEntity.Street = reg.Address;
            merchantEntity.CompanyZIP = reg.PhisicalZip;
            merchantEntity.ZIP = reg.Zipcode;
            merchantEntity.URL = reg.Url;
            merchantEntity.CompanyPhone = reg.Phone;
            merchantEntity.Phone = reg.Phone;
			merchantEntity.CompanyFax = reg.Fax;
            merchantEntity.cellular = reg.Phone;
            merchantEntity.IsSystemPayCVV2 = false;
            merchantEntity.IsSystemPayPhoneNumber = true;
            merchantEntity.IsSystemPayPersonalNumber = false;
            merchantEntity.IsSystemPayEmail = true;
            merchantEntity.IsRefund = false;
            merchantEntity.IsConfirmation = false;
            merchantEntity.IsBillingAddressMust = true;
            merchantEntity.merchantSupportEmail = reg.Email;
            merchantEntity.merchantSupportPhoneNum = reg.Phone;
            merchantEntity.FirstName = reg.FirstName;
            merchantEntity.LastName = reg.LastName;
            merchantEntity.AlertEmail = reg.Email;
            merchantEntity.Blocked = true;
            merchantEntity.Closed = false;
            merchantEntity.comment = "Created from registration";
            merchantEntity.IsAllow3DTrans = false;
            merchantEntity.IsAllowBasket = false;
            merchantEntity.IsAllowBatchFiles = false;
            merchantEntity.IsAllowMakePayments = false;
            merchantEntity.IsAllowRecurring = false;
            merchantEntity.IsAllowRecurringFromTransPass = false;
            merchantEntity.IsAllowRemotePull = false;
            merchantEntity.IsAllowSilentPostCcDetails = false;
            merchantEntity.IsAskRefund = false;
            merchantEntity.IsAskRefundRemote = false;
            merchantEntity.IsCcStorage = false;
            merchantEntity.IsCcStorageCharge = false;
            merchantEntity.isNetpayTerminal = false;
            merchantEntity.IsPublicPay = true;
            dc.SubmitChanges();

			// update reg file
			reg.MerchantID = merchantEntity.ID;
			UpdateRegistration(credentialsToken, reg);
			
			// save canceled check doc
			FileVO fileVo = new FileVO();
			fileVo.Company_id = merchantEntity.ID;
			fileVo.FileName = reg.CanceledCheckImageFileName;
			fileVo.FileExt = Path.GetExtension(reg.CanceledCheckImageFileName).Replace(".", "");
			fileVo.FileTitle = "Registration canceled check";

			byte[] cancelledCheckImageData = reg.CanceledCheckImageContent.FromBase64();
			MemoryStream stream = new MemoryStream(cancelledCheckImageData);
			Files.SaveFile(credentialsToken, fileVo, stream);

			//string cancelledCheckImagePath = FilesManager.GetPrivateAccessPath(user.Domain.Host, UserType.MerchantPrimary, merchantEntity.ID, FilesManager.SubFolders.RegistrationDocs) + reg.CanceledCheckImageFileName;
			//byte[] cancelledCheckImageData = reg.CanceledCheckImageContent.FromBase64();
			//File.WriteAllBytes(cancelledCheckImagePath, cancelledCheckImageData);

            return true;
        }

        public static bool RegisterMerchant(string domainHost, MerchantRegistrationVO vo)
        {
			vo.ID = Guid.NewGuid();
			string path = FilesManager.GetPrivateAccessPath(domainHost, UserType.NetpayAdmin, FilesManager.SubFolders.MerchantRegistrations) + vo.ID + ".xml";
			vo.EncryptBankInfo(domainHost);
			string content = vo.ToXml();
			File.WriteAllText(path, content);
            return true;
        }

		public static void SendRegistrationEmail(string domainHost, MerchantRegistrationVO vo)
		{
			var d = Netpay.Infrastructure.Domains.DomainsManager.GetDomain(domainHost);
			System.Globalization.CultureInfo cultureInfo = System.Globalization.CultureInfo.GetCultureInfo("en-us");
			string subject, body = Infrastructure.Email.FormatMesage.GetMessageText(System.IO.Path.Combine(d.EmailTemplatePath, "Merchant_Register.htm"), out subject);
			System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();

			var dataObj = new { Domain = d, Registration = vo };
			message.IsBodyHtml = true;
			message.Subject = Infrastructure.Email.FormatMesage.FormatMessage(subject, dataObj, null, cultureInfo);
			message.Body = Infrastructure.Email.FormatMesage.FormatMessage(body, dataObj, null, cultureInfo);
			message.From = new System.Net.Mail.MailAddress(d.MailAddressFrom, d.MailNameFrom);
			message.To.Add(new System.Net.Mail.MailAddress(vo.Email, string.Format("{0} {1}", vo.FirstName, vo.LastName)));
			Infrastructure.Email.SmtpClient.Send(message);
		}

        public static decimal GetMerchantTaxRate(string domainHost, int merchantID)
        {
            Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            return (decimal)(from b in dc.tblBillingCompanies
                             join m in dc.tblCompanies on b.BillingCompanys_id equals m.BillingCompanys_id
                             where m.ID == merchantID
                             select b.VATamount).SingleOrDefault();
        }

        /*
        public static void ChangeSyncToken(Guid credentialsToken) 
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary });
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            SetMerchantMobileApp settings = (from mmas in dc.SetMerchantMobileApps where mmas.Merchant_id == user.ID select mmas).SingleOrDefault();
            if (settings == null)
                return;

            settings.SyncToken = Guid.NewGuid();
            dc.SubmitChanges();
        }
        */

        public static MerchantMobileAppSettingsVO GetMobileAppSettings(string domainHost, int merchantID)
        {
            Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			Netpay.Dal.Netpay.tblCompany merchant = (from m in dc.tblCompanies where m.ID == merchantID select m).SingleOrDefault();
            if (merchant == null)
                return null;
			Netpay.Dal.Netpay.SetMerchantMobileApp settings = (from mmas in dc.SetMerchantMobileApps where mmas.Merchant_id == merchantID select mmas).SingleOrDefault();
            if (settings == null)
                return null;

            string[] currencies = GetSupportedCurrencies(domainHost, merchantID).Select(vo => vo.IsoCode).ToArray();

            return new MerchantMobileAppSettingsVO(settings, merchant, currencies.ToDelimitedString());
        }

        public static bool IsMobileEnabled(string domainHost, int merchantID)
        {
            Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            bool isEnabled = (from mmas in dc.SetMerchantMobileApps where mmas.Merchant_id == merchantID select mmas.IsEnableMobileApp).SingleOrDefault();
            return isEnabled;
        }

        public static List<MobileDeviceVO> GetMobileDevices(string domainHost, int merchantID)
        {
            Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            return (from m in dc.MobileDevices where m.Merchant_id == merchantID select new MobileDeviceVO(m)).ToList();
        }

        public static MobileDeviceRegistrationResult RegisterMobileDevice(Guid credentialsToken, string deviceUniqueID, string devicePhoneNumber, string deviceUserAgent, out int? deviceId, out string passCode)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            passCode = null; deviceId = null;
			Netpay.Dal.Netpay.SetMerchantMobileApp settings = (from mmas in dc.SetMerchantMobileApps where mmas.Merchant_id == user.ID select mmas).SingleOrDefault();
            if (settings == null)
                return MobileDeviceRegistrationResult.GeneralFailure;

			Netpay.Dal.Netpay.MobileDevice device = null;
            if (user.SubID == null)
                device = (from d in dc.MobileDevices where d.Merchant_id == user.ID && d.MerchantSubUser_id == null && d.DeviceIdentity == deviceUniqueID select d).SingleOrDefault();
            else
                device = (from d in dc.MobileDevices where d.Merchant_id == user.ID && d.MerchantSubUser_id == user.SubID && d.DeviceIdentity == deviceUniqueID select d).SingleOrDefault();
            if (device == null)
            {
                int deviceCount = (from d in dc.MobileDevices where d.Merchant_id == user.ID && d.IsActive select d).Count();
                if (deviceCount >= settings.MaxDeviceCount) return MobileDeviceRegistrationResult.MaxDevicesExceeded;
				device = new Netpay.Dal.Netpay.MobileDevice();
                device.InsertDate = DateTime.Now;
                device.Merchant_id = user.ID;
                device.MerchantSubUser_id = user.SubID;
                device.IsActive = true;
                device.IsActivated = false;
                device.DeviceIdentity = deviceUniqueID;
                device.PassCode = ((new Random()).Next(800000000) + 100000000).ToString();
                dc.MobileDevices.InsertOnSubmit(device);
            }
            device.DevicePhoneNumber = devicePhoneNumber;
            device.DeviceUserAgent = deviceUserAgent;
            dc.SubmitChanges();
            deviceId = device.MobileDevice_id;
            passCode = device.PassCode;
            return MobileDeviceRegistrationResult.Success;
        }

        public static bool SendMobilePassCode(Guid credentialsToken, string deviceUniqueID)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);

			Netpay.Dal.Netpay.MobileDevice device = null;
            if (user.SubID == null)
                device = (from d in dc.MobileDevices where d.Merchant_id == user.ID && d.MerchantSubUser_id == null && d.DeviceIdentity == deviceUniqueID select d).SingleOrDefault();
            else
                device = (from d in dc.MobileDevices where d.Merchant_id == user.ID && d.MerchantSubUser_id == user.SubID && d.DeviceIdentity == deviceUniqueID select d).SingleOrDefault();

            if (device == null) return false;
            bool smsResult = SMS.SMSService.SendSms(user.Domain.Host, device.DevicePhoneNumber, string.Format("You PassCode Is: {0}", device.PassCode));
            return smsResult;
        }

        public static void SetMobileDevicesFriendlyName(Guid credentialsToken, int deviceId, string friendlyName)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			Netpay.Dal.Netpay.MobileDevice mdv = (from md in dc.MobileDevices where md.Merchant_id == user.ID && md.MobileDevice_id == deviceId select md).SingleOrDefault();
            mdv.FriendlyName = friendlyName;
            dc.SubmitChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="credentialsToken"></param>
        /// <param name="deviceID"></param>
        /// <param name="activationCode">Hashed activation code</param>
        /// <returns></returns>
        public static bool ActivateMobileDevice(Guid credentialsToken, string deviceID, string activationCode)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			Netpay.Dal.Netpay.SetMerchantMobileApp settings = (from mmas in dc.SetMerchantMobileApps where mmas.Merchant_id == user.ID select mmas).SingleOrDefault();
            if (settings == null)
                return false;

			Netpay.Dal.Netpay.MobileDevice device = null;
            if (user.SubID == null)
                device = (from d in dc.MobileDevices where d.Merchant_id == user.ID && d.MerchantSubUser_id == null && d.DeviceIdentity == deviceID select d).SingleOrDefault();
            else
                device = (from d in dc.MobileDevices where d.Merchant_id == user.ID && d.MerchantSubUser_id == user.SubID && d.DeviceIdentity == deviceID select d).SingleOrDefault();
            if (device == null)
                return false;

            if (device.PassCode != activationCode)
                return false;

            device.IsActivated = true;
            device.LastLogin = DateTime.Now;
            dc.SubmitChanges();

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="credentialsToken"></param>
        /// <param name="deviceID"></param>
        /// <returns>True if the device is blocked</returns>
        public static bool RegisterDeviceSignatureFailure(Guid credentialsToken, int deviceID)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary });
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);

			Netpay.Dal.Netpay.MobileDevice device = null;
            if (user.SubID == null)
                device = (from d in dc.MobileDevices where d.Merchant_id == user.ID && d.MerchantSubUser_id == null && d.MobileDevice_id == deviceID select d).SingleOrDefault();
            else
                device = (from d in dc.MobileDevices where d.Merchant_id == user.ID && d.MerchantSubUser_id == user.SubID && d.MobileDevice_id == deviceID select d).SingleOrDefault();
            if (device == null)
                return false;

            device.SignatureFailCount++;
            dc.SubmitChanges();

            if (device.SignatureFailCount >= 3)
                return true;

            return false;
        }

        public static void BlockMobileDevice(string domainHost, int deviceId, bool isActive)
        {
            Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            var device = (from d in dc.MobileDevices where d.MobileDevice_id == deviceId select d).SingleOrDefault();
            device.IsActive = isActive;
            dc.SubmitChanges();
        }

        public static bool IsSyncToken(string domainHost, int merchantID, Guid syncToken)
        {
            Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            var token = (from mmas in dc.SetMerchantMobileApps where mmas.Merchant_id == merchantID && mmas.SyncToken == syncToken select mmas.SyncToken).SingleOrDefault();

            return token != null;
        }

        public static bool SendClientTransactionEmail(string domainHost, int merchantID, int transactionID, string email)
        {
            Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);

            // check sync token
            MerchantMobileAppSettingsVO settings = Netpay.Bll.Merchants.GetMobileAppSettings(domainHost, merchantID);
            if (settings == null) return false;

			var paymentMethod = (from pm in dc.TransPayerInfos join t in dc.tblCompanyTransPasses on pm.TransPayerInfo_id equals t.TransPayerInfo_id  where t.ID == transactionID select pm).SingleOrDefault();
            if (paymentMethod == null) return false;
            if (string.IsNullOrEmpty(paymentMethod.EmailAddress))
            {
				paymentMethod.EmailAddress = email;
                dc.SubmitChanges();
            }
            // create pending event
            PendingEvents.Create(domainHost, PendingEventType.InfoEmailSendClient, transactionID, TransactionStatus.Captured, string.Format("Email={0}", email));

            return true;
        }

        public static bool SendClientTransactionSms(string domainHost, int merchantID, int transactionID, string phone, string merchantText)
        {
            Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            // check sync token
            MerchantMobileAppSettingsVO settings = Netpay.Bll.Merchants.GetMobileAppSettings(domainHost, merchantID);
            if (settings == null) return false;

			var paymentMethod = (from pm in dc.TransPayerInfos join t in dc.tblCompanyTransPasses on pm.TransPayerInfo_id equals t.TransPayerInfo_id where t.ID == transactionID select pm).SingleOrDefault();
			if (paymentMethod == null) return false;

            if (string.IsNullOrEmpty(paymentMethod.PhoneNumber))
            {
                paymentMethod.PhoneNumber = phone;
                dc.SubmitChanges();
            }
            // create pending event
            PendingEvents.Create(domainHost, PendingEventType.InfoSmsSendClient, transactionID, TransactionStatus.Captured, string.Format("Phone={0}&MerchantText={1}", phone, merchantText.ToEncodedUrl()));

            return true;
        }

        public static DailyRiskReportByMerchant GetRiskStats(Guid credentialsToken)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary });
            ReportsDataContext dc = new ReportsDataContext(user.Domain.ReportsConnectionString);
            DailyRiskReportByMerchant data = (from drrc in dc.DailyRiskReportByMerchants where drrc.CompanyID == user.ID select drrc).SingleOrDefault();

            return data;
        }

        public static string GetCustomizations(Guid credentialsToken, int? merchantID = null)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary, UserType.NetpayAdmin, UserType.NetpayUser });
            if (user.Type == UserType.MerchantLimited || user.Type == UserType.MerchantPrimary) merchantID = user.ID;
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            return dc.GetMerchantCustomizationSkins(merchantID);
        }

        public static MerchantCustomizationVO LoadCustomization(Guid credentialsToken, int? skinID = null, int? merchantID = null)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary, UserType.NetpayAdmin, UserType.NetpayUser });
            if (user.Type == UserType.MerchantLimited || user.Type == UserType.MerchantPrimary) merchantID = user.ID;
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            var expression = from t in dc.tblMerchantCustomizations where t.MerchantID == merchantID select t;
            if (skinID.HasValue) expression = expression.Where(t => t.SkinID == skinID.Value);
            var result = expression.SingleOrDefault();
            if (result == null) return null;
            return new MerchantCustomizationVO(result);
        }

        public static MerchantCustomizationVO LoadCustomization(Domain currentDomain, int? skinID = null, int? merchantID = null)
        {
            NetpayDataContext dc = new NetpayDataContext(currentDomain.Sql1ConnectionString);
            var expression = from t in dc.tblMerchantCustomizations where t.MerchantID == merchantID select t;
            if (skinID.HasValue) expression = expression.Where(t => t.SkinID == skinID.Value);
            var result = expression.SingleOrDefault();
            if (result == null) return null;
            return new MerchantCustomizationVO(result);
        }

        public static int SaveCustomization(Guid credentialsToken, MerchantCustomizationVO custom)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary, UserType.NetpayAdmin, UserType.NetpayUser });
            if ((user.Type == UserType.MerchantLimited || user.Type == UserType.MerchantPrimary) && user.ID != custom.MerchantID)
                throw new ApplicationException(string.Format("Merchant {0} is not allowed to save customization for merchant {1}", user.ID, custom.MerchantID));
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            var expression = from t in dc.tblMerchantCustomizations where t.MerchantID == custom.MerchantID select t;
            expression = (custom.SkinID.HasValue ? expression.Where(t => t.SkinID == custom.SkinID.Value) : expression.OrderByDescending(t => t.SkinID));
			Netpay.Dal.Netpay.tblMerchantCustomization entity;
            if (custom.SkinID.HasValue && expression.Count() > 0)
                entity = expression.First();
            else
            {
                if (!custom.SkinID.HasValue) custom.SkinID = (expression.Count() == 0 ? 1 : expression.First().SkinID + 1);
				entity = new Netpay.Dal.Netpay.tblMerchantCustomization();
                entity.MerchantID = custom.MerchantID;
                entity.SkinID = custom.SkinID.Value;
                dc.tblMerchantCustomizations.InsertOnSubmit(entity);
            }
            entity.BackImageRepeat = custom.BackImageRepeat;
            entity.PageBackColor = custom.BackColor;

            entity.LogoMainAlign = custom.LogoMain.Align;
            entity.LogoMainWidth = custom.LogoMain.Width;
            entity.LogoMainHeight = custom.LogoMain.Height;

            entity.LogoTopRightAlign = custom.LogoTopRight.Align;
            entity.LogoTopRightWidth = custom.LogoTopRight.Width;
            entity.LogoTopRightHeight = custom.LogoTopRight.Height;

            entity.LogoTopLeftAlign = custom.LogoTopLeft.Align;
            entity.LogoTopLeftWidth = custom.LogoTopLeft.Width;
            entity.LogoTopLeftHeight = custom.LogoTopLeft.Height;

            entity.LogoBottomRightAlign = custom.LogoBottomRight.Align;
            entity.LogoBottomRightWidth = custom.LogoBottomRight.Width;
            entity.LogoBottomRightHeight = custom.LogoBottomRight.Height;

            entity.LogoBottomLeftAlign = custom.LogoBottomLeft.Align;
            entity.LogoBottomLeftWidth = custom.LogoBottomLeft.Width;
            entity.LogoBottomLeftHeight = custom.LogoBottomLeft.Height;

            entity.BackColor = custom.Text.BackColor;
            entity.ForeColor = custom.Text.ForeColor;
            entity.FontFamily = custom.Text.FontFamily;
            entity.FontSize = custom.Text.FontSize;
            entity.FontIsBold = custom.Text.FontIsBold;
            entity.FontIsItalic = custom.Text.FontIsItalic;

            entity.TitleBackColor = custom.TextTitle.BackColor;
            entity.TitleForeColor = custom.TextTitle.ForeColor;
            entity.TitleFontFamily = custom.TextTitle.FontFamily;
            entity.TitleFontSize = custom.TextTitle.FontSize;
            entity.TitleFontIsBold = custom.TextTitle.FontIsBold;
            entity.TitleFontIsItalic = custom.TextTitle.FontIsItalic;

            entity.FormTitleBackColor = custom.TextFormTitle.BackColor;
            entity.FormTitleForeColor = custom.TextFormTitle.ForeColor;
            entity.FormTitleFontFamily = custom.TextFormTitle.FontFamily;
            entity.FormTitleFontSize = custom.TextFormTitle.FontSize;
            entity.FormTitleFontIsBold = custom.TextFormTitle.FontIsBold;
            entity.FormTitleFontIsItalic = custom.TextFormTitle.FontIsItalic;

            entity.GroupTitleBackColor = custom.TextGroupTitle.BackColor;
            entity.GroupTitleForeColor = custom.TextGroupTitle.ForeColor;
            entity.GroupTitleFontFamily = custom.TextGroupTitle.FontFamily;
            entity.GroupTitleFontSize = custom.TextGroupTitle.FontSize;
            entity.GroupTitleFontIsBold = custom.TextGroupTitle.FontIsBold;
            entity.GroupTitleFontIsItalic = custom.TextGroupTitle.FontIsItalic;

            entity.FieldBackColor = custom.TextField.BackColor;
            entity.FieldForeColor = custom.TextField.ForeColor;
            entity.FieldFontFamily = custom.TextField.FontFamily;
            entity.FieldFontSize = custom.TextField.FontSize;
            entity.FieldFontIsBold = custom.TextField.FontIsBold;
            entity.FieldFontIsItalic = custom.TextField.FontIsItalic;
            entity.ButtonBackColor = custom.TextButton.BackColor;
            entity.ButtonForeColor = custom.TextButton.ForeColor;
            entity.ButtonFontFamily = custom.TextButton.FontFamily;
            entity.ButtonFontSize = custom.TextButton.FontSize;
            entity.ButtonFontIsBold = custom.TextButton.FontIsBold;
            entity.ButtonFontIsItalic = custom.TextButton.FontIsItalic;
            dc.SubmitChanges();
            return custom.SkinID.Value;
        }

        /// <summary>
        /// Gets all transactions that are yet to be paid to the merchant.
        /// </summary>
        /// <param name="credentialsToken"></param>
        /// <param name="filters"></param>
        /// <param name="pagingInfo"></param>
        /// <returns></returns>
        public static List<Transaction.Transaction> GetUnsettledTransactions(Guid credentialsToken, SearchFilters filters, PagingInfo pagingInfo)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary });
            filters.merchantIDs = SecurityManager.FilterRequestedIDs(filters.merchantIDs, user.AllowedMerchantsIDs);

            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            var expression = from t in dc.tblCompanyTransPasses select t;

            // filters
            if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
                expression = expression.Where(t => filters.merchantIDs.Contains(t.companyID));
            if (filters.currencyID != null)
                expression = expression.Where(t => t.Currency == filters.currencyID.Value);
            if (filters.dateFrom != null)
                expression = expression.Where(t => t.InsertDate >= filters.dateFrom.Value.MinTime());
            if (filters.dateTo != null)
                expression = expression.Where(t => t.InsertDate <= filters.dateTo.Value.MaxTime());
            if (filters.isBankPaid != null)
                expression = expression.Where(t => t.tblLogImportEPAs != null && t.tblLogImportEPAs.Count > 0 && t.tblLogImportEPAs.First().IsPaid == filters.isBankPaid.Value);
            expression = expression.Where(t => t.UnsettledInstallments > 0);
            expression = expression.OrderByDescending(t => t.InsertDate);

            // handle paging if not null
            if (pagingInfo != null)
            {
                pagingInfo.TotalItems = expression.Count();
                pagingInfo.DataFunction = (credToken, pi) => GetUnsettledTransactions(credToken, filters, pi);
                if (pagingInfo.CountOnly) return null;
                expression = expression.Skip(pagingInfo.Skip).Take(pagingInfo.Take);
            }

            // get data & filter installments
            var voData = expression.Select(t => new Transaction.Transaction(user, t)).ToList();
            foreach (var currentTransaction in voData)
                if (currentTransaction.IsInstallments)
                    currentTransaction.InstallmentList.RemoveAll(i => i.SettlementID > 0);

            return voData;
        }

        /// <summary>
        /// Gets the merchant unsettled transactions grouped by currency.
        /// Returns CurrencyID, TransactionsCount
        /// </summary>
        /// <param name="credentialsToken"></param>
        /// <param name="filters"></param>
        public static Dictionary<int, CountAmount> GetUnsettledTransactionsBalance(Guid credentialsToken, SearchFilters filters)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary, UserType.NetpayUser, UserType.NetpayAdmin });
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            if (user.Type == UserType.MerchantLimited || user.Type == UserType.MerchantPrimary)
            {
                if (filters == null)
                    filters = new SearchFilters();
                filters.merchantIDs = new List<int>(new int[] { user.ID });
            }

            var expression = from t in dc.tblCompanyTransPasses where t.UnsettledInstallments > 0 select t;
            if (filters != null)
            {
                // filters
                if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
                    expression = expression.Where(t => filters.merchantIDs.Contains(t.companyID));
                if (filters.dateFrom != null)
                    expression = expression.Where(t => t.InsertDate >= filters.dateFrom.Value.MinTime());
                if (filters.dateTo != null)
                    expression = expression.Where(t => t.InsertDate <= filters.dateTo.Value.MaxTime());
                if (filters.isBankPaid != null)
                    expression = expression.Where(t => t.tblLogImportEPAs != null && t.tblLogImportEPAs.Count > 0 && t.tblLogImportEPAs.First().IsPaid == filters.isBankPaid.Value);
            }

            var groupExpression = expression.GroupBy(t => t.Currency).Select(g => new { CurrencyID = g.Key, ca = new CountAmount(g.Sum(x => x.UnsettledAmount).GetValueOrDefault(), g.Count()) });
            var result = groupExpression.ToDictionary(k => k.CurrencyID.Value, v => v.ca);

            return result;
        }

        public static Dictionary<int, decimal> MerchantRollingReserve(Guid credentialsToken, int merchantID)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited, UserType.NetpayUser, UserType.NetpayAdmin });
            if (user.Type == UserType.MerchantLimited || user.Type == UserType.MerchantPrimary) merchantID = user.ID;
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            var expresion = (from s in dc.tblCompanyTransPasses where s.PaymentMethod == (int)PaymentMethodEnum.RollingReserve && s.companyID == merchantID select s);
            var groupExpression = expresion.GroupBy(x => x.Currency).Select(g => new { CurrencyID = g.Key, total = g.Sum(a => a.Amount * System.Math.Sign(a.CreditType - 0.5)) });
            return groupExpression.ToDictionary(k => k.CurrencyID.Value, v => v.total);
        }

        public static void DeleteTranslation(Guid credentialsToken, Language languageID, TranslationGroup translationGroup)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary });
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            var languageData = (from ld in dc.MerchantSetTexts where ld.Merchant_id == user.ID && ld.Language_id == (int)languageID && ld.SolutionList_id == (int)translationGroup select ld).ToList();
            foreach (var l in languageData)
                dc.MerchantSetTexts.DeleteOnSubmit(l);
            dc.SubmitChanges();
        }

        public static void SetTranslation(Guid credentialsToken, Language translationLanguage, TranslationGroup translationGroup, string key, string value)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			Netpay.Dal.Netpay.MerchantSetText transalation = (from t in dc.MerchantSetTexts where t.Merchant_id == user.ID && t.Language_id == (int)translationLanguage && t.SolutionList_id == (int)translationGroup && t.TextKey == key select t).SingleOrDefault();
            if (transalation == null)
            {
				transalation = new Netpay.Dal.Netpay.MerchantSetText();
                transalation.SolutionList_id = (byte)translationGroup;
                transalation.Language_id = (byte)translationLanguage;
                transalation.TextKey = key;
                transalation.Merchant_id = user.ID;
                transalation.TextValue = value.Truncate(2500);
                dc.MerchantSetTexts.InsertOnSubmit(transalation);
            }
            else
                transalation.TextValue = value.Truncate(2500);

            dc.SubmitChanges();
        }

        public static string GetTranslation(string domainHost, string merchantNumber, Language translationLanguage, TranslationGroup translationGroup, string key)
        {
            MerchantVO merchant = GetMerchant(domainHost, merchantNumber);
            if (merchant == null)
                throw new ApplicationException("Merchant not found");

            NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			Netpay.Dal.Netpay.MerchantSetText transalation = (from t in dc.MerchantSetTexts where t.Merchant_id == merchant.ID && t.Language_id == (int)translationLanguage && t.SolutionList_id == (int)translationGroup && t.TextKey == key select t).SingleOrDefault();
            if (transalation == null)
                return null;

            return transalation.TextValue;
        }

        public static string GetTranslation(Guid credentialsToken, Language translationLanguage, TranslationGroup translationGroup, string key)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			Netpay.Dal.Netpay.MerchantSetText transalation = (from t in dc.MerchantSetTexts where t.Merchant_id == user.ID && t.Language_id == (int)translationLanguage && t.SolutionList_id == (int)translationGroup && t.TextKey == key select t).SingleOrDefault();
            if (transalation == null)
                return null;

            return transalation.TextValue;
        }

        public static Dictionary<string, string> GetTranslations(string domainHost, int merchantId, Language translationLanguage, TranslationGroup translationGroup)
        {
            NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
            var transalations = from t in dc.MerchantSetTexts where t.Merchant_id == merchantId && t.Language_id == (int)translationLanguage && t.SolutionList_id == (int)translationGroup select t;

            var ret = transalations.ToDictionary(t => t.TextKey, t => t.TextValue);
            if (ret.Count == 0) return null;
            return ret;
        }

        public static Dictionary<string, string> GetTranslations(Guid credentialsToken, Language translationLanguage, TranslationGroup translationGroup)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            var transalations = from t in dc.MerchantSetTexts where t.Merchant_id == user.ID && t.Language_id == (int)translationLanguage && t.SolutionList_id == (int)translationGroup select t;

            var ret = transalations.ToDictionary(t => t.TextKey, t => t.TextValue);
            if (ret.Count == 0) return null;
            return ret;
        }

        public static Dictionary<int, SetMerchantSettlementsVO> GetCurrencySettings(Guid credentialsToken, int merchantId)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser, UserType.MerchantPrimary, UserType.MerchantLimited });
            if (user.Type == UserType.MerchantPrimary || user.Type == UserType.MerchantLimited) merchantId = user.ID;
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            return (from s in dc.SetMerchantSettlements where s.Merchant_id == merchantId select new SetMerchantSettlementsVO(s)).ToDictionary(s => s.CurrencyID);
        }

        public static DateTime GetMerchantNextSettlementDate(Guid credentialsToken)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
            return GetMerchantNextSettlementDate(user.Domain, user.ID);
        }

        public static DateTime GetMerchantNextSettlementDate(Domain domain, int merchantID)
        {
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			Netpay.Dal.Netpay.tblCompany merchant = (from t in dc.tblCompanies where t.ID == merchantID select t).SingleOrDefault();

            DayOfWeek defaultPayDate = DayOfWeek.Wednesday;
            if (merchant.PayingDaysMarginInitial > 0 || merchant.PayingDaysMargin > 0)
            {
                return DateTime.Now.AddDays(DateTime.Now.DayOfWeek < defaultPayDate ? (int)(defaultPayDate - DateTime.Now.DayOfWeek) : (int)((DayOfWeek.Saturday - DateTime.Now.DayOfWeek) + defaultPayDate));
            }
            else
            {
                DateTime returnDate;
                DateTime today = DateTime.Now;
                DateTime payDate = DateTime.Now;
                int payDate1 = merchant.payingDates1.Length < 6 ? 0 : merchant.payingDates1.Substring(4, 2).ToInt32(0);
                int payDate2 = merchant.payingDates2.Length < 6 ? 0 : merchant.payingDates2.Substring(4, 2).ToInt32(0);
                int payDate3 = merchant.payingDates3.Length < 6 ? 0 : merchant.payingDates3.Substring(4, 2).ToInt32(0);

                if (payDate1 == 0 && payDate2 == 0 && payDate3 == 0) return DateTime.Now;
                while (true)
                {
                    if (payDate1 > 0 && (returnDate = new DateTime(payDate.Year, payDate.Month, payDate1)) > today) break;
                    if (payDate2 > 0 && (returnDate = new DateTime(payDate.Year, payDate.Month, payDate2)) > today) break;
                    if (payDate3 > 0 && (returnDate = new DateTime(payDate.Year, payDate.Month, payDate3)) > today) break;
                    payDate = payDate.AddMonths(1);
                }

                return returnDate;
            }
        }

        private static int GetTransactionSettlementDateInRangeValue(string dateRange, int day)
        {
            int fromDay, toDay;
            if (dateRange.Length < 6) return -1;
            if (!(int.TryParse(dateRange.Substring(0, 2), out fromDay) && int.TryParse(dateRange.Substring(2, 2), out toDay))) return -1;
            if ((fromDay <= day) && (toDay >= day)) return dateRange.Substring(4, 2).ToInt32(-1);
            return -1;
        }

        public static DateTime GetTransactionSettlementDate(Domain domain, int merchantID, DateTime transDate)
        {
            DateTime dateFirstTrans = DateTime.Now;
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			Netpay.Dal.Netpay.tblCompany merchant = (from t in dc.tblCompanies where t.ID == merchantID select t).SingleOrDefault();
            if (merchant.PayingDaysMarginInitial > 0) dateFirstTrans = (from a in dc.MerchantActivities where a.Merchant_id == merchantID select a.DateFirstTransPass).SingleOrDefault().GetValueOrDefault(DateTime.Now);
            if ((merchant.PayingDaysMarginInitial > 0) && ((transDate - dateFirstTrans).TotalDays < merchant.PayingDaysMarginInitial))
                return transDate.AddDays(merchant.PayingDaysMarginInitial);
            else if (merchant.PayingDaysMargin > 0)
                return transDate.AddDays(merchant.PayingDaysMargin);
            else
            {
                string[] dateValue = new string[] { merchant.payingDates1, merchant.payingDates2, merchant.payingDates3 };
                for (int i = 0; i < dateValue.Length; i++)
                {
                    int payDay = GetTransactionSettlementDateInRangeValue(dateValue[i], transDate.Day);
                    if (payDay > -1) return (new DateTime(transDate.Year, transDate.Month, payDay)).AddMonths(1);
                }
                return new DateTime(1901, 1, 1);
            }
        }

        /// <summary>
        /// Deletes a merchant limited user, its access list and password history
        /// </summary>
        /// <param name="credentialsToken"></param>
        /// <param name="limitedUserID"></param>
        public static void DeleteLimitedUser(Guid credentialsToken, int limitedUserID)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary });

            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			Netpay.Dal.Netpay.MerchantSubUser limitedUser = (from lu in dc.MerchantSubUsers where lu.Merchant_id == user.ID && lu.MerchantSubUser_id == limitedUserID select lu).SingleOrDefault();
            if (limitedUser == null)
                throw new ApplicationException("User not found.");

            var accessList = from lls in dc.tblLimitedLoginSecurities where lls.lls_MerchantID == limitedUser.Merchant_id && lls.lls_UserID == limitedUser.MerchantSubUser_id select lls;
            var passwordHistory = from ph in dc.tblPasswordHistories where ph.LPH_RefID == limitedUser.MerchantSubUser_id && ph.LPH_RefType == (int)UserType.MerchantLimited select ph;

            dc.tblLimitedLoginSecurities.DeleteAllOnSubmit(accessList);
            dc.tblPasswordHistories.DeleteAllOnSubmit(passwordHistory);
            dc.MerchantSubUsers.DeleteOnSubmit(limitedUser);
            dc.SubmitChanges();
        }

        public static List<PermissionObjectVO> GetLimitedUserAccessList(Guid credentialsToken, int limitedUserID)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary });

            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            List<PermissionObjectVO> list = (from ll in dc.tblLimitedLoginSecurities where ll.lls_MerchantID == user.ID && ll.lls_UserID == limitedUserID select new PermissionObjectVO(ll)).ToList<PermissionObjectVO>();

            return list;
        }

        public static List<PermissionObjectVO> GetLimitedUserAccessList(Guid credentialsToken)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited });

            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            List<PermissionObjectVO> list = (from ll in dc.tblLimitedLoginSecurities where ll.lls_MerchantID == user.ID && ll.lls_UserID == user.SubID select new PermissionObjectVO(ll)).ToList<PermissionObjectVO>();

            return list;
        }

        public static CreateLimitedUserResult CreateLimitedUser(Guid credentialsToken, string userName, string password, string clientIPAddress)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary });

            // check input
            if (userName == null || userName.Trim() == "")
                return CreateLimitedUserResult.InvalidUserName;

            // check password
            if (!SecurityManager.CheckPasswordStrength(password))
                return CreateLimitedUserResult.WeakPassword;

            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);

            // check user name
			Netpay.Dal.Netpay.MerchantSubUser foundUser = (from lu in dc.MerchantSubUsers where lu.UserName == userName.Trim() select lu).SingleOrDefault();
            if (foundUser != null)
                return CreateLimitedUserResult.UserAlreadyExists;

            //  create limited user
			Netpay.Dal.Netpay.MerchantSubUser limitedUserEntity = new Netpay.Dal.Netpay.MerchantSubUser();
            limitedUserEntity.Merchant_id = user.ID;
            limitedUserEntity.UserName = userName;
            dc.MerchantSubUsers.InsertOnSubmit(limitedUserEntity);
            dc.SubmitChanges();

            // create limited user password history
            byte[] encryptedPassword;
            Encryption.Encrypt(user.Domain.Host, password, out encryptedPassword);
			Netpay.Dal.Netpay.tblPasswordHistory passwordHistory = new Netpay.Dal.Netpay.tblPasswordHistory();
            passwordHistory.LPH_FailCount = 0;
            passwordHistory.LPH_ID = 1;
            passwordHistory.LPH_Insert = DateTime.Now;
            passwordHistory.LPH_Password256 = encryptedPassword;
            passwordHistory.LPH_RefID = limitedUserEntity.MerchantSubUser_id;
            passwordHistory.LPH_RefType = (int)UserType.MerchantLimited;
            passwordHistory.LPH_LastFail = DateTime.Now;
            passwordHistory.LPH_LastSuccess = DateTime.Now;
            passwordHistory.LPH_IP = clientIPAddress == null ? "" : clientIPAddress;
            dc.tblPasswordHistories.InsertOnSubmit(passwordHistory);

            dc.SubmitChanges();

            return CreateLimitedUserResult.Success;
        }

        public static MerchantLimitedUserVO GetLimitedUser(Guid credentialsToken, int limitedUserID)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary });

            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			Netpay.Dal.Netpay.MerchantSubUser limitedUserEntity = (from lu in dc.MerchantSubUsers where lu.Merchant_id == user.ID && lu.MerchantSubUser_id == limitedUserID select lu).SingleOrDefault();
            if (limitedUserEntity == null)
                throw new ApplicationException("User not found.");

            return new MerchantLimitedUserVO(limitedUserEntity);
        }

        public static List<MerchantLimitedUserVO> GetLimitedUsers(Guid credentialsToken)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            List<MerchantLimitedUserVO> limitedUsers = (from lu in dc.MerchantSubUsers where lu.Merchant_id == user.ID select new MerchantLimitedUserVO(lu)).ToList();

            return limitedUsers;
        }

        public static ChangePasswordResult ChangeLimitedUserPassword(Guid credentialsToken, int limitedUserID, string password, string passwordConfirm)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary });

            // check new password confirmation
            if (password != passwordConfirm)
                return ChangePasswordResult.PasswordConfirmationMissmatch;

            // check new password strength
            if (!SecurityManager.CheckPasswordStrength(passwordConfirm))
                return ChangePasswordResult.WeakPassword;

            // check limited user ownership
            MerchantLimitedUserVO limitedUser = GetLimitedUser(credentialsToken, limitedUserID);
            if (limitedUser == null)
                throw new ApplicationException("User not found.");

            // call UpdatePasswordChange proc.
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            var updateResult = dc.UpdatePasswordChange((int)UserType.MerchantLimited, limitedUser.ID, "", password).FirstOrDefault();
            if (updateResult.IsInserted.Value)
                return ChangePasswordResult.Success;
            else
                return ChangePasswordResult.PasswordUsedInThePast;
        }

        public static void SetLimitedUserPermission(Guid credentialsToken, int limitedUserID, string objectName, PermissionObjectType objectType, bool isAllowed)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary });

            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            var foundEntries = from lls in dc.tblLimitedLoginSecurities where lls.lls_Item == objectName && lls.lls_MerchantID == user.ID && lls.lls_UserID == limitedUserID select lls;
            if (foundEntries.Count() > 0)
            {
                dc.tblLimitedLoginSecurities.DeleteAllOnSubmit(foundEntries);
                dc.SubmitChanges();
            }

            if (isAllowed)
            {
				Netpay.Dal.Netpay.tblLimitedLoginSecurity newEntry = new Netpay.Dal.Netpay.tblLimitedLoginSecurity();
                newEntry.lls_MerchantID = user.ID;
                newEntry.lls_Item = objectName;
                newEntry.lls_UserID = limitedUserID;
                newEntry.lls_ItemType = (int)objectType;

                dc.tblLimitedLoginSecurities.InsertOnSubmit(newEntry);
                dc.SubmitChanges();
            }
        }

        public static MerchantExtendedVO GetMerchantExtended(string domainHost, int merchantID)
        {
            NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
            var entity = (from m in dc.GetMerchantExtended(merchantID) select m).SingleOrDefault();

            MerchantExtendedVO vo = new MerchantExtendedVO();
            vo.AccountManager = entity.AccountManager;
            vo.IndustryID = entity.IndustryID;
            vo.IndustryText = entity.IndustryText;
            vo.MerchantCity = entity.MerchantCity;
            vo.MerchantClosingDate = entity.MerchantClosingDate.GetValueOrDefault();
            vo.MerchantContactName = entity.MerchantContactName;
            vo.MerchantCountry = entity.MerchantCountry;
            vo.MerchantEmail = entity.MerchantEmail;
            vo.MerchantGroupID = entity.MerchantGroupID;
            vo.MerchantGroupName = entity.MerchantGroupName;
            vo.MerchantID = entity.MerchantID.GetValueOrDefault();
            vo.MerchantName = entity.MerchantName;
            vo.MerchantNumber = entity.MerchantNumber;
            vo.MerchantOpeningDate = entity.MerchantOpeningDate.GetValueOrDefault();
            vo.MerchantStatusID = entity.MerchantStatusID;
            vo.MerchantStatusText = entity.MerchantStatusText;
            vo.MerchantUrl = entity.MerchantUrl;
            vo.UserName = entity.UserName;
            vo.ParentCompanyID = entity.ParentCompanyID;
            vo.ParentCompanyName = entity.ParentCompanyName;
            vo.Password = entity.UserPassword;

            return vo;
        }

        public static List<CurrencyVO> GetSupportedCurrencies(Guid credentialsToken)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
            return GetSupportedCurrencies(user.Domain.Host, user.ID);
        }

        public static List<CurrencyVO> GetSupportedCurrencies(string domainHost, int merchantID)
        {
            Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            int[] ids = (from ccf in dc.tblCompanyCreditFees where ccf.CCF_CompanyID == merchantID select ccf.CCF_CurrencyID).Distinct().ToArray();
            List<CurrencyVO> currencies = domain.Cache.GetCurrencies(ids);

            return currencies;
        }

        public static MerchantVO GetMerchant(string domainHost, int merchantID)
        {
            NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
            MerchantVO merchant = (from c in dc.tblCompanies where c.ID == merchantID select new MerchantVO(c)).SingleOrDefault();
            return merchant;
        }

        public static MerchantVO GetMerchant(Guid credentialsToken)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
            return GetMerchant(user.Domain.Host, user.ID);
        }

        public static MerchantVO GetMerchant(Guid credentialsToken, int merchantID)
        {
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser, UserType.MerchantPrimary, UserType.MerchantLimited });
			if ((user.Type == UserType.MerchantPrimary || user.Type == UserType.MerchantLimited) && merchantID != user.ID)
				throw new MethodAccessDeniedException();
            return GetMerchant(user.Domain.Host, merchantID);
        }

        public static MerchantVO GetMerchant(string domainHost, string merchantNumber)
        {
            NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
            MerchantVO merchant = (from c in dc.tblCompanies where c.CustomerNumber == merchantNumber select new MerchantVO(c)).SingleOrDefault();
            return merchant;
        }

        /// <summary>
        /// Updates merchant data.
        /// </summary>
        /// <param name="credentialsToken"></param>
        /// <param name="merchantData"></param>
        public static void UpdateMerchant(Guid credentialsToken, MerchantVO merchantData)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			Netpay.Dal.Netpay.tblCompany merchantEntity = (from c in dc.tblCompanies where c.ID == user.ID select c).SingleOrDefault();
            if (merchantEntity == null)
                throw new ApplicationException("Merchant not found");

            // clean all input strings
            SecurityManager.HtmlEncodeStringProperties(merchantData);

            // set updated values
            //merchantEntity.HandlingFee = merchantData.HandlingFee;
            merchantEntity.Mail = merchantData.EMail;
            merchantEntity.UserName = merchantData.UserName;
            merchantEntity.CompanyCountry = merchantData.Country;
            merchantEntity.Country = merchantData.ContactCountry;
            merchantEntity.CompanyCity = merchantData.City;
            merchantEntity.City = merchantData.ContactCity;
            merchantEntity.CompanyStreet = merchantData.Address;
            merchantEntity.Street = merchantData.ContactAddress;
            merchantEntity.CompanyZIP = merchantData.Zipcode;
            merchantEntity.ZIP = merchantData.ContactZipcode;
            merchantEntity.URL = merchantData.Url;
            merchantEntity.CompanyFax = merchantData.Fax;
            merchantEntity.CompanyPhone = merchantData.Phone;
            merchantEntity.Phone = merchantData.ContactPhone;
            merchantEntity.cellular = merchantData.ContactMobilePhone;
            merchantEntity.IsSystemPayCVV2 = merchantData.IsSystemPayCVV2;
            merchantEntity.IsSystemPayPhoneNumber = merchantData.IsSystemPayPhoneNumber;
            merchantEntity.IsSystemPayPersonalNumber = merchantData.IsSystemPayPersonalNumber;
            merchantEntity.IsSystemPayEmail = merchantData.IsSystemPayEmail;
            merchantEntity.IsRefund = merchantData.IsRefund;
            merchantEntity.IsConfirmation = merchantData.IsConfirmation;
            merchantEntity.IsBillingAddressMust = merchantData.IsBillingAddressMust;
            merchantEntity.merchantSupportEmail = merchantData.SupportEmail;
            merchantEntity.merchantSupportPhoneNum = merchantData.SupportPhone;
            merchantEntity.FirstName = merchantData.ContactFirstName;
            merchantEntity.LastName = merchantData.ContactLastName;

            dc.SubmitChanges();
        }

        public static Dictionary<string, string> GetLinkedMerchant(Guid credentialsToken)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary });
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            return (from c in dc.tblCompanies where c.MerchantLinkName == user.LinkName select new { id = c.CustomerNumber, name = c.CustomerNumber + " - " + c.CompanyLegalName }).ToDictionary(c => c.id, c => c.name);
        }

        public static LoginResult LoginLinkedMerchant(Guid credentialsToken, string merchantNumber, out System.Guid retCredentialsToken)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary });
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            var merchantLogin = (from c in dc.tblCompanies where c.MerchantLinkName == user.LinkName && c.CustomerNumber == merchantNumber select c).SingleOrDefault();
            if (merchantLogin == null) throw new ApplicationException("Specified merchant not found:" + merchantNumber);
            var merchantPassword = (from p in dc.tblPasswordHistories where p.LPH_RefID == merchantLogin.ID && p.LPH_RefType == (int)UserType.MerchantPrimary && p.LPH_ID == 1 select p.LPH_Password256).SingleOrDefault();
            if (merchantPassword == null) throw new ApplicationException("Merchant's login information could not be found:" + merchantNumber);
            LoginResult lr = SecurityManager.Login(user.Domain.Host, UserType.MerchantPrimary, merchantLogin.UserName, merchantLogin.Mail, Encryption.Decrypt(user.Domain.Host, merchantPassword.ToArray()), out retCredentialsToken);
            return lr;
        }

        public static List<MerchantBeneficiaryVO> GetBeneficiaries(Guid credentialsToken)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            List<MerchantBeneficiaryVO> beneficiaries = (from b in dc.tblCompanyMakePaymentsProfiles where b.company_id == user.ID orderby b.isSystem descending, b.basicInfo_costumerName select new MerchantBeneficiaryVO(b)).ToList<MerchantBeneficiaryVO>();

            return beneficiaries;
        }

        public static void DeleteBeneficiary(Guid credentialsToken, int benficiaryID)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			Netpay.Dal.Netpay.tblCompanyMakePaymentsProfile beneficiary = (from b in dc.tblCompanyMakePaymentsProfiles where b.company_id == user.ID && b.CompanyMakePaymentsProfiles_id == benficiaryID select b).SingleOrDefault();
            if (beneficiary == null)
                return;

            dc.tblCompanyMakePaymentsProfiles.DeleteOnSubmit(beneficiary);
            dc.SubmitChanges();
        }

        public static MerchantBeneficiaryVO GetBeneficiary(Guid credentialsToken, int benficiaryID)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			Netpay.Dal.Netpay.tblCompanyMakePaymentsProfile beneficiary = (from b in dc.tblCompanyMakePaymentsProfiles where b.company_id == user.ID && b.CompanyMakePaymentsProfiles_id == benficiaryID select b).SingleOrDefault();
            if (beneficiary == null)
                return null;

            MerchantBeneficiaryVO vo = new MerchantBeneficiaryVO(beneficiary);
            return vo;
        }

        public static void CreateBeneficiary(Guid credentialsToken, MerchantBeneficiaryVO beneficiaryData)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

            // clean input
            SecurityManager.HtmlEncodeStringProperties(beneficiaryData);

            // set data
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			Netpay.Dal.Netpay.tblCompanyMakePaymentsProfile beneficiaryEntity = new Netpay.Dal.Netpay.tblCompanyMakePaymentsProfile();
            beneficiaryEntity.company_id = user.ID;
            beneficiaryEntity.ProfileType = beneficiaryData.ProfileType;
            beneficiaryEntity.basicInfo_costumerNumber = beneficiaryData.BasicInfoCostumerNumber;
            beneficiaryEntity.basicInfo_costumerName = beneficiaryData.BasicInfoCostumerName;
            beneficiaryEntity.basicInfo_contactPersonName = beneficiaryData.BasicInfoContactPersonName;
            beneficiaryEntity.basicInfo_phoneNumber = beneficiaryData.BasicInfoPhoneNumber;
            beneficiaryEntity.basicInfo_faxNumber = beneficiaryData.BasicInfoFaxNumber;
            beneficiaryEntity.basicInfo_email = beneficiaryData.BasicInfoEmail;
            beneficiaryEntity.basicInfo_address = beneficiaryData.BasicInfoAddress;
            beneficiaryEntity.basicInfo_comment = beneficiaryData.BasicInfoComment;
            beneficiaryEntity.bankIsraelInfo_PayeeName = beneficiaryData.BankIsraelInfoPayeeName;
            beneficiaryEntity.bankIsraelInfo_CompanyLegalNumber = beneficiaryData.BankIsraelInfoCompanyLegalNumber;
            beneficiaryEntity.bankIsraelInfo_personalIdNumber = beneficiaryData.BankIsraelInfoPersonalIDNumber;
            beneficiaryEntity.bankIsraelInfo_bankBranch = beneficiaryData.BankIsraelInfoBankBranch;
            beneficiaryEntity.bankIsraelInfo_AccountNumber = beneficiaryData.BankIsraelInfoAccountNumber;
            beneficiaryEntity.bankIsraelInfo_PaymentMethod = beneficiaryData.BankIsraelInfoPaymentMethod;
            beneficiaryEntity.bankIsraelInfo_BankCode = beneficiaryData.BankIsraelInfoBankCode;
            beneficiaryEntity.bankAbroadAccountName = beneficiaryData.BankAbroadAccountName;
            beneficiaryEntity.bankAbroadAccountNumber = beneficiaryData.BankAbroadAccountNumber;
            beneficiaryEntity.bankAbroadBankName = beneficiaryData.BankAbroadBankName;
            beneficiaryEntity.bankAbroadBankAddress = beneficiaryData.BankAbroadBankAddress;
            beneficiaryEntity.bankAbroadSwiftNumber = beneficiaryData.BankAbroadSwiftNumber;
            beneficiaryEntity.bankAbroadIBAN = beneficiaryData.BankAbroadIBAN;
            beneficiaryEntity.bankAbroadABA = beneficiaryData.BankAbroadABA;
            beneficiaryEntity.bankAbroadSortCode = beneficiaryData.BankAbroadSortCode;
            beneficiaryEntity.bankAbroadAccountName2 = beneficiaryData.BankAbroadAccountName2;
            beneficiaryEntity.bankAbroadAccountNumber2 = beneficiaryData.BankAbroadAccountNumber2;
            beneficiaryEntity.bankAbroadBankName2 = beneficiaryData.BankAbroadBankName2;
            beneficiaryEntity.bankAbroadBankAddress2 = beneficiaryData.BankAbroadBankAddress2;
            beneficiaryEntity.bankAbroadSwiftNumber2 = beneficiaryData.BankAbroadSwiftNumber2;
            beneficiaryEntity.bankAbroadIBAN2 = beneficiaryData.BankAbroadIBAN2;
            beneficiaryEntity.bankAbroadABA2 = beneficiaryData.BankAbroadABA2;
            beneficiaryEntity.bankAbroadSortCode2 = beneficiaryData.BankAbroadSortCode2;
            beneficiaryEntity.bankAbroadBankAddressSecond = beneficiaryData.BankAbroadBankAddressSecond;
            beneficiaryEntity.bankAbroadBankAddressCity = beneficiaryData.BankAbroadBankAddressCity;
            beneficiaryEntity.bankAbroadBankAddressState = beneficiaryData.BankAbroadBankAddressState;
            beneficiaryEntity.bankAbroadBankAddressZip = beneficiaryData.BankAbroadBankAddressZip;
            beneficiaryEntity.bankAbroadBankAddressCountry = beneficiaryData.BankAbroadBankAddressCountry;
            beneficiaryEntity.bankAbroadBankAddressSecond2 = beneficiaryData.BankAbroadBankAddressSecond2;
            beneficiaryEntity.bankAbroadBankAddressCity2 = beneficiaryData.BankAbroadBankAddressCity2;
            beneficiaryEntity.bankAbroadBankAddressState2 = beneficiaryData.BankAbroadBankAddressState2;
            beneficiaryEntity.bankAbroadBankAddressZip2 = beneficiaryData.BankAbroadBankAddressZip2;
            beneficiaryEntity.bankAbroadBankAddressCountry2 = beneficiaryData.BankAbroadBankAddressCountry2;
            beneficiaryEntity.bankAbroadSepaBic = beneficiaryData.BankAbroadSepaBic;
            beneficiaryEntity.bankAbroadSepaBic2 = beneficiaryData.BankAbroadSepaBic2;

            dc.tblCompanyMakePaymentsProfiles.InsertOnSubmit(beneficiaryEntity);
            dc.SubmitChanges();
        }

        public static void UpdateBeneficiary(Guid credentialsToken, MerchantBeneficiaryVO beneficiaryData)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

            // get beneficiary
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			Netpay.Dal.Netpay.tblCompanyMakePaymentsProfile beneficiaryEntity = (from b in dc.tblCompanyMakePaymentsProfiles where b.company_id == user.ID && b.CompanyMakePaymentsProfiles_id == beneficiaryData.ID select b).SingleOrDefault();
            if (beneficiaryEntity == null)
                throw new ApplicationException("Beneficiary not found.");

            // clean input
            SecurityManager.HtmlEncodeStringProperties(beneficiaryData);

            // set data
            beneficiaryEntity.ProfileType = beneficiaryData.ProfileType;
            beneficiaryEntity.basicInfo_costumerNumber = beneficiaryData.BasicInfoCostumerNumber;
            beneficiaryEntity.basicInfo_costumerName = beneficiaryData.BasicInfoCostumerName;
            beneficiaryEntity.basicInfo_contactPersonName = beneficiaryData.BasicInfoContactPersonName;
            beneficiaryEntity.basicInfo_phoneNumber = beneficiaryData.BasicInfoPhoneNumber;
            beneficiaryEntity.basicInfo_faxNumber = beneficiaryData.BasicInfoFaxNumber;
            beneficiaryEntity.basicInfo_email = beneficiaryData.BasicInfoEmail;
            beneficiaryEntity.basicInfo_address = beneficiaryData.BasicInfoAddress;
            beneficiaryEntity.basicInfo_comment = beneficiaryData.BasicInfoComment;
            beneficiaryEntity.bankIsraelInfo_PayeeName = beneficiaryData.BankIsraelInfoPayeeName;
            beneficiaryEntity.bankIsraelInfo_CompanyLegalNumber = beneficiaryData.BankIsraelInfoCompanyLegalNumber;
            beneficiaryEntity.bankIsraelInfo_personalIdNumber = beneficiaryData.BankIsraelInfoPersonalIDNumber;
            beneficiaryEntity.bankIsraelInfo_bankBranch = beneficiaryData.BankIsraelInfoBankBranch;
            beneficiaryEntity.bankIsraelInfo_AccountNumber = beneficiaryData.BankIsraelInfoAccountNumber;
            beneficiaryEntity.bankIsraelInfo_PaymentMethod = beneficiaryData.BankIsraelInfoPaymentMethod;
            beneficiaryEntity.bankIsraelInfo_BankCode = beneficiaryData.BankIsraelInfoBankCode;
            beneficiaryEntity.bankAbroadAccountName = beneficiaryData.BankAbroadAccountName;
            beneficiaryEntity.bankAbroadAccountNumber = beneficiaryData.BankAbroadAccountNumber;
            beneficiaryEntity.bankAbroadBankName = beneficiaryData.BankAbroadBankName;
            beneficiaryEntity.bankAbroadBankAddress = beneficiaryData.BankAbroadBankAddress;
            beneficiaryEntity.bankAbroadSwiftNumber = beneficiaryData.BankAbroadSwiftNumber;
            beneficiaryEntity.bankAbroadIBAN = beneficiaryData.BankAbroadIBAN;
            beneficiaryEntity.bankAbroadABA = beneficiaryData.BankAbroadABA;
            beneficiaryEntity.bankAbroadSortCode = beneficiaryData.BankAbroadSortCode;
            beneficiaryEntity.bankAbroadAccountName2 = beneficiaryData.BankAbroadAccountName2;
            beneficiaryEntity.bankAbroadAccountNumber2 = beneficiaryData.BankAbroadAccountNumber2;
            beneficiaryEntity.bankAbroadBankName2 = beneficiaryData.BankAbroadBankName2;
            beneficiaryEntity.bankAbroadBankAddress2 = beneficiaryData.BankAbroadBankAddress2;
            beneficiaryEntity.bankAbroadSwiftNumber2 = beneficiaryData.BankAbroadSwiftNumber2;
            beneficiaryEntity.bankAbroadIBAN2 = beneficiaryData.BankAbroadIBAN2;
            beneficiaryEntity.bankAbroadABA2 = beneficiaryData.BankAbroadABA2;
            beneficiaryEntity.bankAbroadSortCode2 = beneficiaryData.BankAbroadSortCode2;
            beneficiaryEntity.bankAbroadBankAddressSecond = beneficiaryData.BankAbroadBankAddressSecond;
            beneficiaryEntity.bankAbroadBankAddressCity = beneficiaryData.BankAbroadBankAddressCity;
            beneficiaryEntity.bankAbroadBankAddressState = beneficiaryData.BankAbroadBankAddressState;
            beneficiaryEntity.bankAbroadBankAddressZip = beneficiaryData.BankAbroadBankAddressZip;
            beneficiaryEntity.bankAbroadBankAddressCountry = beneficiaryData.BankAbroadBankAddressCountry;
            beneficiaryEntity.bankAbroadBankAddressSecond2 = beneficiaryData.BankAbroadBankAddressSecond2;
            beneficiaryEntity.bankAbroadBankAddressCity2 = beneficiaryData.BankAbroadBankAddressCity2;
            beneficiaryEntity.bankAbroadBankAddressState2 = beneficiaryData.BankAbroadBankAddressState2;
            beneficiaryEntity.bankAbroadBankAddressZip2 = beneficiaryData.BankAbroadBankAddressZip2;
            beneficiaryEntity.bankAbroadBankAddressCountry2 = beneficiaryData.BankAbroadBankAddressCountry2;
            beneficiaryEntity.bankAbroadSepaBic = beneficiaryData.BankAbroadSepaBic;
            beneficiaryEntity.bankAbroadSepaBic2 = beneficiaryData.BankAbroadSepaBic2;

            dc.SubmitChanges();
        }

        public static List<Netpay.Dal.Netpay.GetCompanyCurrencyBalanceListPagedResult> GetMerchantCurrencyBalanceList(Guid credentialsToken, byte currencyID, PagingInfo pagingInfo)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);

            var expression = from b in dc.GetCompanyCurrencyBalanceList(user.ID, currencyID) select b;

            // handle paging if not null
            if (pagingInfo != null)
            {
                //var sql = @"SELECT Count(*) FROM tblCompanyBalance left join tblCompanyMakePaymentsRequests on  tblCompanyBalance.sourceTbl_id = tblCompanyMakePaymentsRequests.CompanyMakePaymentsRequests_id WHERE tblCompanyBalance.company_id =" + user.ID + " AND currency =" + currencyID;
                //var total = dc.ExecuteQuery<int>(sql).First();
                var total = (from b in dc.tblCompanyBalances where b.company_id == user.ID && b.currency == currencyID select b).Count();
                pagingInfo.TotalItems = (int)total;
                return (from b in dc.GetCompanyCurrencyBalanceListPaged(user.ID, currencyID, pagingInfo.Skip, pagingInfo.Take) select b).ToList<Netpay.Dal.Netpay.GetCompanyCurrencyBalanceListPagedResult>();
            }

            return (from b in dc.GetCompanyCurrencyBalanceListPaged(user.ID, currencyID, 0, pagingInfo.TotalItems) select b).ToList<Netpay.Dal.Netpay.GetCompanyCurrencyBalanceListPagedResult>();
        }

        public static List<Netpay.Dal.Netpay.GetCompanyBalanceResult> GetMerchantBalance(Guid credentialsToken, int merchantID)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited, UserType.NetpayAdmin, UserType.NetpayUser });
            if (user.Type == UserType.MerchantPrimary || user.Type == UserType.MerchantLimited) merchantID = user.ID;

            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            return (from b in dc.GetCompanyBalance(merchantID) select b).ToList<Netpay.Dal.Netpay.GetCompanyBalanceResult>();
        }

        public static Netpay.Dal.Netpay.GetCompanyBalanceResult GetMerchantBalance(Guid credentialsToken, int merchantID, Currency currency)
        {
            return GetMerchantBalance(credentialsToken, merchantID).Where(b => b.currency == (int)currency).SingleOrDefault();
        }

        private static object _balanceChangeSync = new object();
        public static MerchantBalanceChangeResult DebitMerchant(Guid credentialsToken, decimal amount, Currency currency, BalanceStatus status, string comment, BalanceSource sourceType, int sourceID, string sourceInfo)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
            lock (_balanceChangeSync)
            {
				List<Netpay.Dal.Netpay.GetCompanyBalanceResult> balanceList = GetMerchantBalance(credentialsToken, user.ID);
				Netpay.Dal.Netpay.GetCompanyBalanceResult currencyBalance = balanceList.Where(b => b.currency == (int)currency).SingleOrDefault();
                decimal balance = currencyBalance.balance.GetValueOrDefault();
                if (amount <= 0) return MerchantBalanceChangeResult.InvalidAmount;
                if ((balance - amount) < 0) return MerchantBalanceChangeResult.InsufficientFunds;
                InsertBalace(credentialsToken, sourceType, sourceID, sourceInfo, -amount, currency, comment, BalanceStatus.Cleared, false);
            }
            return MerchantBalanceChangeResult.Success;
        }

        public static MerchantBalanceChangeResult CreditMerchant(Guid credentialsToken, decimal amount, Currency currency, string comment, BalanceSource sourceType, int sourceID, string sourceInfo)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
            lock (_balanceChangeSync)
            {
				List<Netpay.Dal.Netpay.GetCompanyBalanceResult> balanceList = GetMerchantBalance(credentialsToken, user.ID);
				Netpay.Dal.Netpay.GetCompanyBalanceResult currencyBalance = balanceList.Where(b => b.currency == (int)currency).SingleOrDefault();
                decimal balance = currencyBalance.balance.GetValueOrDefault();
                if (amount <= 0) return MerchantBalanceChangeResult.InvalidAmount;
                InsertBalace(credentialsToken, sourceType, sourceID, sourceInfo, amount, currency, comment, BalanceStatus.Cleared, false);
            }
            return MerchantBalanceChangeResult.Success;
        }

        public static void InsertBalace(Guid credentialsToken, BalanceSource sourceType, int sourceID, string sourceInfo, decimal amount, Currency currency, string comment, BalanceStatus status, bool addFee)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			Netpay.Dal.Netpay.tblCompanyBalance entity = new Netpay.Dal.Netpay.tblCompanyBalance();
            entity.amount = amount;
            entity.comment = comment.Truncate(2000);
            entity.company_id = user.ID;
            entity.currency = (int)currency;
            entity.insertDate = DateTime.Now;
            entity.status = (byte)status;
            entity.sourceType = (byte)sourceType;
            entity.sourceTbl_id = sourceID;
            entity.sourceInfo = sourceInfo.Truncate(200);
            dc.tblCompanyBalances.InsertOnSubmit(entity);
            dc.SubmitChanges();

            if (addFee)
            {
                var wireFees = (from n in dc.SetMerchantSettlements where n.Merchant_id == user.ID && n.Currency_id == (int)currency select n).SingleOrDefault();
                decimal feeAmount = wireFees.WireFee - (amount * (wireFees.WireFeePercent / 100));
                InsertBalace(credentialsToken, BalanceSource.Fee, 0, "Bank transfer", -feeAmount, currency, "", BalanceStatus.Cleared, false);
            }
        }

        public static System.Collections.Generic.Dictionary<CommonTypes.Currency, decimal> GetMonthlyTotalDebit(Guid credentialsToken)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
            ReportsDataContext dc = new ReportsDataContext(user.Domain.ReportsConnectionString);
            return dc.spGetMerchantMonthlyTotal(user.ID).ToDictionary(f => (CommonTypes.Currency)f.CurrencyID.GetValueOrDefault(), f => f.MonthlyDebitAmount.GetValueOrDefault());
        }

		public static void SaveRulesAndRegulations(string domainHost, int merchantId, string contents)
		{
			string path = System.IO.Path.Combine(Infrastructure.FilesManager.GetPrivateAccessPath(domainHost, UserType.MerchantPrimary, merchantId), "RulesAndRegulations.html");
			System.IO.File.WriteAllText(path, contents);
		}

		public static string GetRulesAndRegulations(string domainHost, int merchantId)
		{
			string path = System.IO.Path.Combine(Infrastructure.FilesManager.GetPrivateAccessPath(domainHost, UserType.MerchantPrimary, merchantId), "RulesAndRegulations.html");
			if (!System.IO.File.Exists(path)) return string.Empty;
			return System.IO.File.ReadAllText(path);
		}

    }
}
