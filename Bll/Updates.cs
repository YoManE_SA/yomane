﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Xml.Linq;
using Netpay.Dal.Netpay;
using Netpay.Dal.DataAccess;

using Netpay.Infrastructure;

namespace Netpay.Bll
{
    /// <summary>
    /// Scheduled system updates
    /// </summary>
    public static class Updates
    {
        public static void ExecDebitRefund(string domainHost)
        {
            string retValue, aspFile = Domain.Get(domainHost).ProcessUrl + "DebitRefund.asp";
            try
            {
                if (Infrastructure.HttpClient.SendHttpRequest(aspFile, out retValue, null, null, 1000 * 1000) != HttpStatusCode.OK)
                    Logger.Log(LogSeverity.Error, LogTag.Updates, string.Format("Exec {0} has failed.", aspFile), retValue);
            }
            catch (Exception e)
            {
                Logger.Log(LogSeverity.Error, LogTag.Updates, string.Format("Exec {0} has failed.", aspFile), e.ToString());
            }
        }

		public static void UpdateCurrencies()
        {
			foreach (var d in Infrastructure.Domain.Domains.Values)
				UpdateDomainCurrencies(d.Host);
		}

        public static void UpdateDomainCurrencies(string domainHost)
        {
			Domain.Current = Domain.Get(domainHost);
			ObjectContext.Current.CredentialsToken = Domain.Current.ServiceCredentials;

			Dictionary<string, System.Collections.Generic.KeyValuePair<DateTime, decimal> > currencies = new Dictionary<string, System.Collections.Generic.KeyValuePair<DateTime, decimal>>();

            // israel updates
            string israelXml = null;
            XElement israelXmlParsed = null;
            for (int tries = 1; tries <= 10; tries++)
            {
                try
                {
					israelXml = new WebClient().DownloadString("https://forex.boi.org.il/currency.xml");
                    israelXmlParsed = XElement.Parse(israelXml);

                    break;
                }
                catch (Exception e)
                {
                    if (tries == 10) 
                    {
						Logger.Log(LogSeverity.Error, LogTag.Updates, "Try (" + tries + ") to fetch https://forex.boi.org.il/currency.xml has failed. currencies will not be updated.", e.ToString());
                        return;
                    } 
                }
            }

			DateTime fileDate = DateTime.Parse(israelXmlParsed.Element("LAST_UPDATE").Value);
            foreach (XElement currentCurrency in israelXmlParsed.Descendants("CURRENCY"))
            {
                string code = currentCurrency.Element("CURRENCYCODE").Value;
                decimal rate = decimal.Parse(currentCurrency.Element("RATE").Value) / decimal.Parse(currentCurrency.Element("UNIT").Value);
				currencies.Add(code, new System.Collections.Generic.KeyValuePair<DateTime, decimal>(fileDate, rate) );
            }

            // europe update
            // updates currencies not present in the israeli update
            string euroXml = null;
            XElement euroXmlParsed = null;
            for (int tries = 1; tries <= 10; tries++)
            {
                try
                {
                    euroXml = new WebClient().DownloadString("http://www.ecb.int/stats/eurofxref/eurofxref-daily.xml");
                    euroXmlParsed = XElement.Parse(euroXml);

                    break;
                }
                catch (Exception e)
                {
                    if (tries == 10) 
                    {
                        Logger.Log(LogSeverity.Error, LogTag.Updates, "Try (" + tries + ") to fetch http://www.ecb.int/stats/eurofxref/eurofxref-daily.xml has failed. currencies will not be updated.", e.ToString());
                        return;
                    }
                }
            }

			fileDate = DateTime.Now;
			XNamespace ns = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref";
            XElement ilsCurrency = (from el in euroXmlParsed.Descendants(ns + "Cube") where el.Attribute("currency") != null && el.Attribute("currency").Value == "ILS" select el).SingleOrDefault();
            if (ilsCurrency == null)
                Logger.Log(LogSeverity.Error, LogTag.Updates, "could not find ils currency in european xml");
            else
            {
                decimal ilsRate = decimal.Parse(ilsCurrency.Attribute("rate").Value);
                IEnumerable<XElement> relevantCurrencies = from el in euroXmlParsed.Descendants(ns + "Cube") where el.Attribute("currency") != null && el.Attribute("currency").Value != "ILS" select el;
                foreach (XElement currentCurrency in relevantCurrencies)
                {
                    string code = currentCurrency.Attribute("currency").Value;
                    if (!currencies.ContainsKey(code))
                    {
                        decimal euroRate = decimal.Parse(currentCurrency.Attribute("rate").Value);
                        decimal rate = ilsRate / euroRate;
						currencies.Add(code, new System.Collections.Generic.KeyValuePair<DateTime, decimal>(fileDate, rate));
                    }
                }
            }

			currencies.Remove("ILS"); //remove base currency which should always be 1

			{ //update new
				foreach (var v in currencies)
				{
					var cur = Bll.Currency.Get(v.Key);
                    if (cur == null) continue;
					cur.RateRequestDate = DateTime.Now;
					cur.RateValueDate = v.Value.Key;
					cur.BaseRate = v.Value.Value;
					cur.Save();
				}
			}
			
			{ //update old
				NetpayDataContext dc = new NetpayDataContext(Domain.Current.Sql1ConnectionString);
				var entities = from c in dc.tblSystemCurrencies select c;
				foreach (var currentEntity in entities)
				{
					System.Collections.Generic.KeyValuePair<DateTime, decimal> updatedValue;
					if (currencies.TryGetValue(currentEntity.CUR_ISOName, out updatedValue))
					{
						currentEntity.CUR_RateRequestDate = DateTime.Now;
						currentEntity.CUR_RateValueDate = updatedValue.Key;
						currentEntity.CUR_BaseRate = updatedValue.Value;
					}
				}
				dc.SubmitChanges();
			}

        }

        private static string StripQuot(string value)
        {
            if (value.StartsWith("\"")) value = value.Substring(1);
            if (value.EndsWith("\"")) value = value.Substring(0, value.Length - 1);
            return value;
        }

        public static void UpdateGeoIP()
        {
            foreach (Domain domain in Domain.Domains.Values)
            {
                string line = "";
                int lineCount = 0, dataCount = 0;
                try
                {
                    if (!Infrastructure.Tasks.Lock.TryLock("GeoIPUpdate", DateTime.Now.AddMinutes(-15)))
                        return;

                    NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
                    string geoDataUrl = @"http://software77.net/geo-ip/?DL=1";
                    var httpRequest = System.Net.WebRequest.Create(geoDataUrl) as System.Net.HttpWebRequest;
                    var httpResponse = httpRequest.GetResponse() as System.Net.HttpWebResponse;
                    var gzStream = new System.IO.Compression.GZipStream(httpResponse.GetResponseStream(), System.IO.Compression.CompressionMode.Decompress);
                    var gzReader = new System.IO.StreamReader(gzStream);
                    dc.ExecuteCommand("Delete From tblGeoIP");
                    while (!gzReader.EndOfStream)
                    {
                        lineCount++;
                        line = gzReader.ReadLine();
                        if (!line.StartsWith("#"))
                        {
                            string[] values = line.Split(',');
                            var geoIPRow = new tblGeoIP();
                            dc.tblGeoIPs.InsertOnSubmit(geoIPRow);
                            geoIPRow.GI_Start = StripQuot(values[0]).ToInt64(0);
                            geoIPRow.GI_End = StripQuot(values[1]).ToInt64(0);
                            geoIPRow.GI_Diff = geoIPRow.GI_End - geoIPRow.GI_Start;
                            geoIPRow.GI_IsoCode = StripQuot(values[4]);
                            //if (geoIPRow.GI_Start == 0 && geoIPRow.GI_End == 0) System.Diagnostics.Debugger.Break();
                            dataCount++;
                            if (dataCount % 100 == 0) dc.SubmitChanges();
                        }
                    }
                    dc.SubmitChanges();
                    gzReader.Close();
                    gzStream.Close();
                    httpResponse.Close();
                }
                catch (Exception e)
                {
                    Logger.Log(LogSeverity.Error, LogTag.Updates, string.Format("Update GeoIP has failed -Line {0}:{1}.", lineCount, line), e.ToString());
                }
                finally
                {
                    Infrastructure.Tasks.Lock.Release("GeoIPUpdate");
                }
            }
        }
    }
}
