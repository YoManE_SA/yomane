﻿using System;
using System.Collections.Generic;
using System.Linq;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure.VO;
using System.Data.Linq;

namespace Netpay.Bll
{
	/// <summary>
	/// Represents an entry in the Credit Card Storage service
	/// </summary>
	public static class CreditCardStorage
	{
		public static List<CreditCardStorageVO> GetCards(Guid credentialsToken, SearchFilters filters, ISortAndPage sortAndPage)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var expression = (IQueryable<tblCCStorage>)from ccs in dc.tblCCStorages where ccs.companyID == user.ID orderby ccs.InsertDate descending select ccs;

			// add filters
			if (filters.dateFrom != null)
				expression = expression.Where(ccs => ccs.InsertDate >= filters.dateFrom.Value.MinTime());
			if (filters.dateTo != null)
				expression = expression.Where(ccs => ccs.InsertDate <= filters.dateTo.Value.MaxTime());
			if (filters.paymentMethodID != null)
				expression = expression.Where(ccs => ccs.PaymentMethod == filters.paymentMethodID.Value);
			if (filters.cardHolderName != null)
				expression = expression.Where(ccs => ccs.CHFullName.StartsWith(filters.cardHolderName.Trim()));
			if (filters.last4Digits != null)
				expression = expression.Where(ccs => ccs.CCardLast4 == filters.last4Digits.ToString());
			if (filters.isCreditCardStorageCancelled != null)
				expression = expression.Where(ccs => ccs.isDeleted == filters.isCreditCardStorageCancelled.Value);
			if (filters.expirationMonth != null)
				expression = expression.Where(ccs => ccs.ExpMM == filters.expirationMonth.Value.ToString("00"));
			if (filters.expirationYear != null)
				expression = expression.Where(ccs => ccs.ExpYY == filters.expirationYear.Value.ToString("0000"));

			// handle paging if not null
			if (sortAndPage != null)
				sortAndPage.DataFunction = (credToken, pi) => GetCards(credToken, filters, sortAndPage);
			// run query and return collection
			var ccstorages = expression.ApplySortAndPage(sortAndPage).Select(ccs => new CreditCardStorageVO(ccs)).ToList<CreditCardStorageVO>();
			return ccstorages;
		}

		public static CreditCardStorageVO GetCard(Guid credentialsToken, int cardID)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			tblCCStorage entity = (from cc in dc.tblCCStorages where cc.ID == cardID && cc.companyID == user.ID select cc).SingleOrDefault();
			if (entity == null) return null;
			return new CreditCardStorageVO(entity);
		}

		/// <summary>
		/// Updates an existing card.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="data"></param>
		public static void Update(Guid credentialsToken, CreditCardStorageVO data)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			tblCCStorage entity = (from cc in dc.tblCCStorages where cc.ID == data.ID && cc.companyID == user.ID select cc).SingleOrDefault();
			if (entity == null)
				throw new ApplicationException("Card not found.");

			SecurityManager.HtmlEncodeStringProperties(data);

			entity.CHEmail = data.CardholderEmail;
			entity.CHFullName = data.CardholderFullName;
			entity.CHPersonalNum = data.CardholderID;
			entity.CHPhoneNumber = data.CardholderPhone;
			entity.CHSCity = data.CardholderCity;
			entity.CHStreet = data.CardholderStreet1;
			entity.CHStreet1 = data.CardholderStreet2;
			entity.CHSZipCode = data.CardholderZipCode;
			entity.Comment = data.Comment;
			entity.countryId = data.CountryID;
			entity.stateId = data.StateID;
			entity.ExpMM = data.ExpirationMonth;
			entity.ExpYY = data.ExpirationYear;
			entity.IPAddress = data.IPAddress;

			dc.SubmitChanges();
		}

        public enum CardStorageResult 
        {
            Unknown,
            Success,
            InvalidCard,
            CardExpired,
            CardNotFound
        }

		/// <summary>
		/// Creates a new stored card.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="data"></param>
        public static CardStorageResult Create(Guid credentialsToken, CreditCardStorageVO data)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

			CreditCard card = new CreditCard(data.CardNumber);
			card.ExpirationDate = new DateTime(data.ExpirationYear.ToInt32(), data.ExpirationMonth.ToInt32(), 1);
            if (!card.IsValid)
                return CardStorageResult.InvalidCard;
            if (card.ExpirationDate < DateTime.Now)
                return CardStorageResult.CardExpired;
            if (data.Cui == null)
                data.Cui = "";

			SecurityManager.HtmlEncodeStringProperties(data);

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			int storedCardId = dc.StoreCreditCard(user.ID, (int)card.GetPaymentMethod(user.CredentialsToken), data.CardholderID.EmptyIfNull(), card.ToString(), card.ExpirationDate.Month, card.ExpirationDate.Year, data.Cui, data.CardholderPhone.EmptyIfNull(), data.CardholderEmail.EmptyIfNull(), data.CardholderFullName, data.Comment.EmptyIfNull(), card.GetPaymentMethodDisplay(user.Domain.Host), data.IPAddress.EmptyIfNull(), data.CountryID, data.StateID, data.CardholderStreet1, data.CardholderStreet2, data.CardholderCity, data.CardholderZipCode);
            
            return CardStorageResult.Success;
        }

		/// <summary>
		/// Cancels multiple stored cards.
		/// The cards are not deleted but flaged as canceled (isDeleted).
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="cardIDs"></param>
		public static void Cancel(Guid credentialsToken, List<int> cardIDs)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited, UserType.NetpayUser, UserType.NetpayAdmin });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);

			var expression = (IQueryable<tblCCStorage>)(from ccs in dc.tblCCStorages where ccs.companyID == user.ID && cardIDs.Contains(ccs.ID) && !ccs.isDeleted select ccs);
			foreach (tblCCStorage element in expression)
				element.isDeleted = true;

			dc.SubmitChanges();
		}

        public static CardStorageResult Cancel(Guid credentialsToken, string cardNumber)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);

            Binary cardEncrypted = Encryption.Encrypt(user.Domain.Host, cardNumber);
            var card = (from ccs in dc.tblCCStorages where ccs.companyID == user.ID && ccs.CCard_number256 == cardEncrypted && !ccs.isDeleted select ccs).SingleOrDefault();
            if (card == null)
                return CardStorageResult.CardNotFound;

            card.isDeleted = true;
            dc.SubmitChanges();

            return CardStorageResult.Success;
        }

        public static CardStorageResult Verify(Guid credentialsToken, string cardNumber)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);

            Binary cardEncrypted = Encryption.Encrypt(user.Domain.Host, cardNumber);
            var card = (from ccs in dc.tblCCStorages where ccs.companyID == user.ID && ccs.CCard_number256 == cardEncrypted && !ccs.isDeleted select ccs).SingleOrDefault();
            if (card == null)
                return CardStorageResult.CardNotFound;
            
            return CardStorageResult.Success;
        }
	}
}
