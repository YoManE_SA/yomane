﻿using System;
using System.Collections.Generic;
using System.Linq;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Domains;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure.VO;

namespace Netpay.Bll
{
	public static class Cart
	{
		public static void SaveOptions(Guid credentialsToken, MerchantSettingsCartVO settings)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary });

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			tblCompanySettingsCart entity = (from pco in dc.tblCompanySettingsCarts where pco.CompanyID == user.ID select pco).SingleOrDefault();
			if (entity == null)
			{
				entity = new tblCompanySettingsCart();
				entity.CompanyID = user.ID;
				dc.tblCompanySettingsCarts.InsertOnSubmit(entity);
				dc.SubmitChanges();
			}

			entity.CompanyID = settings.MerchantID;
			entity.IsEnabled = settings.IsEnabled;
			entity.IsCartStored = settings.IsCartStored;
			entity.IsAuthOnly = settings.IsAuthOnly;
			entity.IsShippingEnabled = settings.IsShippingEnabled;
			entity.IsShippingRequired = settings.IsShippingRequired;

			dc.SubmitChanges();
		}

		public static MerchantSettingsCartVO GetOptions(Guid credentialsToken)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary });

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			tblCompanySettingsCart settings = (from o in dc.tblCompanySettingsCarts where o.CompanyID == user.ID select o).FirstOrDefault();
			if (settings == null) return null;
			return new MerchantSettingsCartVO(settings);
		}

		public static MerchantSettingsCartVO GetOptions(string domainHost, string merchantNumber)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			tblCompanySettingsCart settings = (from s in dc.tblCompanySettingsCarts join merchants in dc.tblCompanies on s.CompanyID equals merchants.ID where merchants.CustomerNumber == merchantNumber select s).FirstOrDefault();
			if (settings == null) return null;
			return new MerchantSettingsCartVO(settings);
		}

		public static void AddItem(string domain, CartItemVO cartData, bool merge)
		{
			tblCartItem ci;
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domain).Sql1ConnectionString);
			if (merge){
				ci = (from c in dc.tblCartItems where c.CartID == cartData.CartID && c.ItemID == cartData.ItemID select c).SingleOrDefault();
				if (ci != null) ci.Quantity += cartData.Quantity;
				else {
					merge = false;
					ci = new tblCartItem();
					ci.Quantity = cartData.Quantity;
				}
			} else{
				ci = new tblCartItem();
				ci.Quantity = cartData.Quantity;
			}
			ci.MerchantID = cartData.MerchantID;
			ci.CartID = cartData.CartID;
			ci.ItemID = cartData.ItemID;
			ci.Name = cartData.Name;
			ci.InsertDate = DateTime.Now;
			ci.Details = cartData.Details;
			ci.Price = cartData.Price;
			ci.CurrencyID = cartData.CurrencyID;
			ci.ShippingFee = cartData.ShippingFee;
			ci.TaxRatio = cartData.TaxRatio;
			ci.ItemURL = cartData.ItemUrl;
			ci.ItemImage = cartData.ImageUrl;
			ci.QuantityMin = cartData.QuantityMin;
			ci.QuantityMax = cartData.QuantityMax;
			ci.QuantityStep = cartData.QuantityStep;
			if (!merge) dc.tblCartItems.InsertOnSubmit(ci);
			dc.SubmitChanges();
		}

		public static List<CartItemVO> GetCartItems(string domain, Guid cartID)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domain).Sql1ConnectionString);
			return (from ci in dc.tblCartItems where ci.CartID == cartID select new CartItemVO(ci)).ToList();
		}

		public static CartVO CreateCart(string domain, int merchantID, int? customerID, int currencyID, Guid cartID)
		{
			tblCart c = new tblCart();
			c.ID = cartID;
			c.MerchantID = merchantID;
			c.CustomerID = customerID;
			c.StartDate = DateTime.Now;
			c.CurrencyID = currencyID;

			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domain).Sql1ConnectionString);
			dc.tblCarts.InsertOnSubmit(c);
			dc.SubmitChanges();

			return GetCart(domain, cartID);
		}

		public static CartVO GetCart(string domain, Guid cartID)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domain).Sql1ConnectionString);
			return (from c in dc.tblCarts where c.ID == cartID select new CartVO(c)).FirstOrDefault();
		}

		public static void UpdateItemQuantity(string domain, Guid cartID, int itemID, int quantity)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domain).Sql1ConnectionString);
			tblCartItem ci = (from c in dc.tblCartItems where c.CartID == cartID && c.ID == itemID select c).FirstOrDefault();
			if (ci == null) return;
			ci.Quantity = quantity;
			if (quantity == 0) dc.tblCartItems.DeleteOnSubmit(ci);
			dc.SubmitChanges();
		}

		public static List<CartVO> SearchCarts(Guid credentialsToken, SearchFilters filters, PagingInfo pagingInfo)
		{
			User user = Netpay.Infrastructure.Security.SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
			if (user.Type != UserType.NetpayAdmin)
				filters.merchantIDs = SecurityManager.FilterRequestedIDs(filters.merchantIDs, user.AllowedMerchantsIDs);
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var expression = (IQueryable<tblCart>)from c in dc.tblCarts orderby c.StartDate descending select c;

			if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
				expression = expression.Where(c => filters.merchantIDs.Contains(c.MerchantID));
			if (filters.dateFrom != null) expression = expression.Where(c => c.StartDate >= filters.dateFrom);
			if (filters.dateTo != null) expression = expression.Where(c => c.StartDate <= filters.dateTo);
			if (filters.amountFrom != null) expression = expression.Where(c => c.Total >= filters.amountFrom);
			if (filters.amountTo != null) expression = expression.Where(c => c.Total <= filters.amountTo);
			if (filters.cartID != null) expression = expression.Where(c => c.ID == filters.cartID);
			if (filters.cartStatus != null) expression = expression.Where(c => c.OrderStatus == (int)filters.cartStatus);

			if (pagingInfo != null)
			{
				pagingInfo.TotalItems = expression.Count();
				pagingInfo.DataFunction = (credToken, pi) => SearchCarts(credToken, filters, pi);
				if (pagingInfo.CountOnly) return null;
				expression = expression.Skip(pagingInfo.Skip).Take(pagingInfo.Take);
			}
			return expression.Select(c => new CartVO(c)).ToList<CartVO>();
		}

		public static List<CartItemVO> SearchCartsItems(Guid credentialsToken, SearchFilters filters, PagingInfo pagingInfo)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
			if (user.Type != UserType.NetpayAdmin)
				filters.merchantIDs = SecurityManager.FilterRequestedIDs(filters.merchantIDs, user.AllowedMerchantsIDs);
			
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var expression = from ci in dc.tblCartItems select ci;

			if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
				expression = expression.Where(ci => filters.merchantIDs.Contains(ci.MerchantID));
			if (filters.amountFrom != null)
				expression = expression.Where(ci => ci.Total >= filters.amountFrom);
			if (filters.amountTo != null)
				expression = expression.Where(ci => ci.Total <= filters.amountTo);
			if (filters.dateFrom != null)
				expression = expression.Where(ci => ci.InsertDate >= filters.dateFrom.Value.MinTime());
			if (filters.dateTo != null)
				expression = expression.Where(ci => ci.InsertDate <= filters.dateTo.Value.MaxTime());
			if (filters.cartItemName != null)
				expression = expression.Where(ci => ci.Name == filters.cartItemName);

			if (pagingInfo != null)
			{
				pagingInfo.TotalItems = expression.Count();
				pagingInfo.DataFunction = (credToken, pi) => SearchCarts(credToken, filters, pi);
				if (pagingInfo.CountOnly) 
					return null;
				expression = expression.Skip(pagingInfo.Skip).Take(pagingInfo.Take);
			}

			return expression.Select(ci => new CartItemVO(ci)).ToList();
		}

		public static bool SetCartStatus(string domain, CartVO cart, bool approval, string replyCode, int transID)
		{
			bool bClearCart = false;
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domain).Sql1ConnectionString);
			tblCart ci = (from c in dc.tblCarts where c.ID == cart.ID select c).FirstOrDefault();
			//ci.Quantity = cart.Quantity;
			//ci.TotalItems = cart.TotalItems;
			ci.TotalProducts = cart.TotalProducts;
			//ci.TaxRatio = cart.TaxRatio;
			ci.TotalShipping = cart.TotalShipping;
			switch(replyCode){
			case "000":
				if (approval) {
					ci.TransApprovalID = transID;
					ci.OrderStatus = (int) CartStatus.Authorized;
				} else {
					ci.TransPassID = transID;
					ci.OrderStatus = (int) CartStatus.Approved;
				}
				bClearCart = true;
				break;
			case "001":
			case "552":
			case "553":
			case "554":
			case "556":
				ci.TransPendingID = transID; 
				ci.OrderStatus = (int) CartStatus.Pending;
				bClearCart = true;
				break;
			default:
				ci.OrderStatus = (int) CartStatus.Declined;
				tblCartFailLog cfl = new tblCartFailLog();
				cfl.MerchantID = ci.MerchantID;
				cfl.CartID = ci.ID;
				cfl.TransID = transID;
				cfl.FailDate = DateTime.Now;
				dc.tblCartFailLogs.InsertOnSubmit(cfl);
				break;
			}
			dc.SubmitChanges();
			return bClearCart;
		}

		public static void UpdateCart(string domain, CartVO cart)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domain).Sql1ConnectionString);
			tblCart ci = (from c in dc.tblCarts where c.ID == cart.ID select c).FirstOrDefault();
			ci.ID = cart.ID;
			ci.MerchantID = cart.MerchantID;
			ci.CustomerID = cart.CustomerID;
			ci.StartDate = cart.StartDate;
			ci.CheckoutDate = cart.CheckoutDate;
			ci.CurrencyID = cart.Currency;
			ci.TotalProducts = cart.TotalProducts;
			ci.TotalShipping = cart.TotalShipping;
			ci.Installments = cart.Installments;
			ci.RecepientName = cart.RecepientName;
			ci.RecepientPhone = cart.RecepientPhone;
			ci.RecepientMail = cart.RecepientMail;
			ci.Comment = cart.Comment;
			ci.ShippingAddress = cart.ShippingAddress;
			ci.ShippingAddress2 = cart.ShippingAddress2;
			ci.ShippingCity = cart.ShippingCity;
			ci.ShippingZip = cart.ShippingZip;
			ci.ShippingState = cart.ShippingState;
			ci.ShippingCountry = cart.ShippingCountry;
			ci.ReferenceNumber = cart.ReferenceNumber;
			ci.CustomStatus = cart.CustomStatus;
			if (cart.TransPassID != null || cart.TransPendingID != null || cart.TransPendingID != null) 
				ci.OrderStatus = ci.OrderStatus;
			dc.SubmitChanges();
		}
	}
}
