﻿using System;
using System.Linq;
using System.Collections.Generic;
using Netpay.CommonTypes;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using Netpay.Infrastructure.VO;
using Netpay.Infrastructure.Domains;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll
{
	public static class Files
    {
        public static string GetFilePath(string domainHost, FileVO file)
        {
            Domain domain = DomainsManager.GetDomain(domainHost);
            if (file.Company_id != null)
                return String.Format("{0}/{1}", domain.merchantFilesFolder, file.FileName);
            else if (file.Customer_id != null)
                return String.Format("{0}/{1}", domain.CustomerFilesFolder, file.FileName);
            return null;
        }

        public static void SaveFile(Guid credentialsToken, FileVO file, System.IO.Stream dataStream)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken);
            if (user == null) throw new ApplicationException("Unknown user with sent credentialsToken");

            Domain domain = DomainsManager.GetDomain(user.Domain.Host);
			/*For Test*/
			//Domain domain = DomainsManager.GetDomain("localhost");
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            Netpay.Dal.Netpay.tblFile entity = null;
            var exp = (from f in dc.tblFiles where f.File_id == file.File_id select f);
            if (user.Type == UserType.Customer) {
                file.Customer_id = user.ID;
                file.Company_id = null;
                exp = exp.Where(f => f.Customer_id == user.ID);
            } else if (user.Type == UserType.MerchantLimited || user.Type == UserType.MerchantPrimary) {
                file.Customer_id = null;
                file.Company_id = user.ID;
                exp = exp.Where(f => f.Company_id == user.ID);
            } else if (user.Type == UserType.NetpayUser || user.Type == UserType.NetpayAdmin) {
                //do nothing
            } else throw new ApplicationException("Method access denied, see log.");
            if (file.File_id != 0) entity = exp.SingleOrDefault();
            if (entity == null) {
                entity = new Dal.Netpay.tblFile();
                entity.InsertDate = DateTime.Now;
                dc.tblFiles.InsertOnSubmit(entity);
                dc.SubmitChanges();
                file.File_id = entity.File_id;
                file.FileName = string.Format("{0}_{1}", entity.File_id, file.FileName);
            }
            entity.Customer_id = file.Customer_id;
            entity.Company_id = file.Company_id;
            entity.FileTitle = file.FileTitle;
            entity.FileName = file.FileName;
            entity.FileExt = file.FileExt;
            if(string.IsNullOrEmpty(entity.FileExt)) {
                entity.FileExt = System.IO.Path.GetExtension(entity.FileName);
                if (entity.FileExt.StartsWith(".")) entity.FileExt = entity.FileExt.Substring(1);
            }
            entity.AdminComment = file.AdminComment;
			entity.AdminApprovalDate = file.AdminApprovalDate;
			entity.AdminApprovalUser = file.AdminApprovalUser;
			dc.SubmitChanges();

            if (dataStream != null){
                var fileStream = new System.IO.FileStream(GetFilePath(user.Domain.Host, file), System.IO.FileMode.Create);
                try {
                    byte[] buffer = new byte[4096];
                    int read = buffer.Length;
                    while (read == buffer.Length) {
                        read = dataStream.Read(buffer, 0, buffer.Length);
                        fileStream.Write(buffer, 0, read);
                    }
                } catch(Exception ex) {
                    Logger.Log(ex, string.Format("Unable to save file '{0}' ", GetFilePath(user.Domain.Host, file)));
                } finally {
		            fileStream.Close();
                    dataStream.Close();
                }
            }
        }

        public static List<FileVO> GetFiles(Guid credentialsToken, int? merchantID, int? customerID)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken);
            Domain domain = DomainsManager.GetDomain(user.Domain.Host);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            var exp = (from f in dc.tblFiles select f);
            if (user.Type == UserType.Customer) {
                exp = exp.Where(f => f.Customer_id == user.ID);
            } else if (user.Type == UserType.MerchantLimited || user.Type == UserType.MerchantPrimary) {
				exp = exp.Where(f => f.Company_id == user.ID);
            } else if (user.Type == UserType.NetpayUser || user.Type == UserType.NetpayAdmin) {
				if (merchantID != null) exp = exp.Where(f => f.Company_id == merchantID);
				if (customerID != null) exp = exp.Where(f => f.Customer_id == customerID);
            } else throw new ApplicationException("Method access denied, see log.");

            return (from fi in exp select new FileVO(fi)).ToList();
        }

        public static FileVO GetFile(Guid credentialsToken, int fileId)
        {
			User user = SecurityManager.GetInternalUser(credentialsToken);
			Domain domain = DomainsManager.GetDomain(user.Domain.Host);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			var exp = (from f in dc.tblFiles where f.File_id == fileId select f);
			if (user.Type == UserType.Customer) exp = exp.Where(f => f.Customer_id == user.ID);
			else if (user.Type == UserType.MerchantLimited || user.Type == UserType.MerchantPrimary) exp = exp.Where(f => f.Company_id == user.ID);
			return (from fi in exp select new FileVO(fi)).SingleOrDefault();
		}

		public static void DeleteFile(Guid credentialsToken, int fileId)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken);
			Domain domain = DomainsManager.GetDomain(user.Domain.Host);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			var exp = (from f in dc.tblFiles where f.File_id == fileId select f);
			if (user.Type == UserType.Customer) exp = exp.Where(f => f.Customer_id == user.ID);
			else if (user.Type == UserType.MerchantLimited || user.Type == UserType.MerchantPrimary) exp = exp.Where(f => f.Company_id == user.ID);
			dc.tblFiles.DeleteOnSubmit(exp.SingleOrDefault());
			dc.SubmitChanges();
		}

		public static List<NoteVO> GetNotes(Guid credentialsToken, int? merchantID, int? customerID)
        {
			User user = SecurityManager.GetInternalUser(credentialsToken);
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);

			var exp = (from n in dc.tblNotes select n);
            if (user.Type == UserType.Customer) {
				exp = exp.Where(n => n.Customer_id == user.ID);
            } else if (user.Type == UserType.MerchantLimited || user.Type == UserType.MerchantPrimary) {
				exp = exp.Where(n => n.Merchant_id == user.ID);
            } else if (user.Type == UserType.NetpayUser || user.Type == UserType.NetpayAdmin) {
				if (merchantID != null) exp = exp.Where(n => n.Merchant_id == merchantID);
				if (customerID != null) exp = exp.Where(n => n.Customer_id == customerID);
            } else throw new ApplicationException("Method access denied, see log.");

            return (from fi in exp select new NoteVO(fi)).ToList();
		}

		public static void AddNote(Guid credentialsToken, int? merchantID, int? customerID, string text)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayUser, UserType.NetpayAdmin });
			if (user == null) throw new Exception("wrong user type");
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var newNote = new Netpay.Dal.Netpay.tblNote();
			newNote.InsertDate = DateTime.Now;
			newNote.NoteText = text;
			newNote.Merchant_id = merchantID;
			newNote.Customer_id = customerID;
			newNote.InsertUser = user.Name;
			dc.tblNotes.InsertOnSubmit(newNote);
			dc.SubmitChanges();
		}
    

    }
}
