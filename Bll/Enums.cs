﻿namespace Netpay.Bll
{
    public enum Solution
    {
        HostedPaymentPage = 1,
        PublicPaymentPage = 2,
        AdminFees = 3,
        Admin = 4,
        MerchantControlPanel = 10
    }

    public enum MobileDeviceRegistrationResult
    {
        Unknown = 0,
        Success = 1,
        GeneralFailure = 2,
        MaxDevicesExceeded = 3
    }

    public enum ExternalCardPaymentResultEnum
    {
        Unknown = 0,
        Success = 1,
        InsufficientFunds = 2,
        InvalidAmount = 3,
        UnexpectedError = 4,
        TransferFailed = 5,
        TerminalNotFound = 6,
        CustomerDataNotFound = 7,
        CustomerNotFound = 10,
        MerchantNotAuthorized = 20,
        MerchantNotFound = 20
    }

    public enum HistoryLogType
    {
        RemoteCardStorage = 1,
        RemoteRefundRequest = 2,
        ThirdPartyInvoices = 3,
        RemoteCardStorageV2 = 4,
        BlockedItemsManagementV2 = 5,
        CardConnectRegister = 7,
        HostedPageChargeSystemProcess = 31,
        HostedPageChargeMerchantNotification = 32,
        Other = 99
    }

    public enum MerchantBalanceChangeResult
    {
        Unknown = 0,
        Success = 1,
        InsufficientFunds = 2,
        InvalidAmount = 3
    }

    public enum CreateSeriesResult
    {
        InvalidChargesNumber = -1,
        InvalidInterhvalUnit = -2,
        InvalidIntervalLength = -3,
        InvalidTransactionNotFound = -4,
        RecurringDisabled = -5,

        Success = 1,
        Error = 2,
        TransactionNotFound = 3,
        TransactionSeriesAlreadyExists = 4,
        WrongTransactionStatus = 5
    }

    public enum AddBlockedItemResult
    {
        Success = 1,
        ItemAlreadyBlocked = 2,
        NoItemValue = 3
    }

    public enum AddBlockedCardResult
    {
        Success = 1,
        CardAlreadyBlocked = 2,
        InvalidCard = 3,
        TransactionNotFound = 4,
        PaymentMethodIsNotCreditcard = 5,
        CardNotFound = 6,
        CardNumberNotFound = 7
    }

    public enum RequestRefundResult
    {
        TransactionNotFound = 1,
        TransactionFullyRefunded = 2,
        AmountTooLarge = 3,
        Success = 4,
        NoPermission = 5,
        TransactionTooOld = 6,
    }

    public enum CaptureResult
    {
        InvalidAmount,
        TransactionNotFound,
        AlreadyCaptured,
        AmountTooLarge,
        ConnectionError,
        BadReplyFormat,
        AttemptDeclined,
        AttemptPending,
        GeneralError,
        OK
    }


    public enum LocalizationSite
    {
        HostedPageV2,
        MerchantControlPanel,
        AffilliateConrolPanel,
        PublicPageV2,
        MobileAdminCash,
    }

    public enum InvoicesProvider
    {
        Unknwon = 0,
        Invoice4u = 1,
        Tamal = 2,
        iCount = 3,
        Local = 10,
    }

    public enum DebitReturnCodes
    {
        Code520 = 520,
        Code521 = 521
    }

    public enum RefundStatus
    {
        New = 0,
        Refunded = 1,
        Never_Charged = 2,
        Failed = 3,
        Void = 4
    }
}