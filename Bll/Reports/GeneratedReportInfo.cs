﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.DataAccess;
using System.IO;
using Netpay.Bll.Reports;

namespace Netpay.Bll.Reports
{
	public class GeneratedReportInfo
	{
		public GeneratedReportInfo() { }

		internal GeneratedReportInfo(FileInfo fileInfo, ReportType reportType)
		{
			ReportFile = fileInfo;
			ReportFileType = reportType;
		}

		public FileInfo ReportFile { get; set; }
		public ReportType ReportFileType { get; set; }
	}
}
