﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Reports;
using Netpay.Infrastructure;

namespace Netpay.Bll.Reports.VO
{
	public class RollingReserveReportDetailVO
	{
		public RollingReserveReportDetailVO() { }

		public RollingReserveReportDetailVO(RollingReserveData entity)
		{
			CompanyID = entity.CompanyID;
			CompanyName = entity.CompanyName;
			CompanyNumber = entity.CompanyNumber;
			TransactionID = entity.TransactionID;
			SettlementID = entity.SettlementID;
			InsertDate = entity.InsertDate;
			CurrencyID = entity.CurrencyID;
			CurrencySymbol = entity.CurrencySymbol;
			ReserveCreditType = entity.CreditType;
			ID = entity.ID;
			Amount = entity.Amount;
			ReserveComment = entity.Comment;
			ReserveCreditTypeTitle = entity.CreditType == 0 ? "Reserved" : "Released";
			RollingReserveState = entity.RollingReserveState;
		}

		public int CompanyID { set; get; }
		public string CompanyName { set; get; }
		public int CompanyNumber { set; get; }
		[EntityPropertyMapping("TransactionID")]
		public int TransactionID { set; get; }
		[EntityPropertyMapping("SettlementID")]
		public int SettlementID { set; get; }
		[EntityPropertyMapping("InsertDate")]
		public DateTime InsertDate { set; get; }
		public int CurrencyID { set; get; }
		public string CurrencySymbol { set; get; }
		public int ReserveCreditType { get; set; }
		public int ID { set; get; }
		[EntityPropertyMapping("Amount")]
		public decimal Amount { set; get; }
		public string ReserveComment { set; get; }
		public string ReserveCreditTypeTitle { set; get; }
		public int? RollingReserveState { set; get; }
	}
}
