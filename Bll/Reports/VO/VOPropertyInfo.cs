﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll.Reports.VO
{
	public class VOPropertyInfo
	{
		private Type _voType = null;
		private string _property = null;
		private string _parameter = null;

		public Type VOType
		{
			get { return _voType; }
			set { _voType = value; }
		}

		public string Property
		{
			get { return _property; }
			set { _property = value; }
		}

		public string Parameter
		{
			get { return _parameter; }
			set { _parameter = value; }
		}

		public string MappedEntityProperty
		{
			get
			{
				return GetMappedProperty(VOType, _property);
			}
		}


		public static string GetMappedProperty(Type voType, string voPropertyName)
		{
			PropertyInfo property = voType.GetProperty(voPropertyName);
			if (property == null)
				throw new ApplicationException(string.Format("Property '{0}' was not found in type '{1}'.", voPropertyName, voType.Name));
			object[] customAttributes = property.GetCustomAttributes(typeof(EntityPropertyMapping), false);
			if (customAttributes.Length == 0)
				throw new ApplicationException(string.Format("Could not find a mapping attribute for '{0}.{1}'", voType.Name, voPropertyName));
			EntityPropertyMapping mapAttribute = (EntityPropertyMapping)customAttributes[0];

			return mapAttribute.EntityPropertyName;
		}

	}
}
