﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Dal.Reports;

namespace Netpay.Bll.Reports.VO
{
	public class RollingReserveReportVO
	{
		public RollingReserveReportVO() { }

		public RollingReserveReportVO(RollingReserveSummary entity)
		{
			CompanyID = entity.CompanyID;
			CompanyName = entity.CompanyName;
			CurrencyID = entity.CurrencyID;
			CurrencySymbol = entity.CurrencySymbol;
			ID = entity.ID;
			Released = entity.Released;
			Reserved = entity.Reserved;			
			Total = entity.Total;
			ReservedCount = entity.ReservedCount;
			ReleasedCount = entity.ReleasedCount;
			TotalCount = entity.TotalCount;
			RollingReserveState = entity.RollingReserveState;
			RollingReservePercent = entity.RollingReservePercent;
			UnsettledAmount = entity.UnsettledAmount;
		}

		public int CompanyID { set; get; }
		[EntityPropertyMapping("CompanyName")]
		public string CompanyName { set; get; }
		public int CurrencyID { set; get; }
		public string CurrencySymbol { set; get; }
		public int ID { set; get; }
		[EntityPropertyMapping("Released")]
		public decimal Released { set; get; }
		[EntityPropertyMapping("Reserved")]
		public decimal Reserved { set; get; }
		[EntityPropertyMapping("Total")]
		public decimal? Total { set; get; }
		public int ReservedCount { set; get; }
		public int ReleasedCount { set; get; }
		public int? TotalCount { set; get; }
		public int? RollingReserveState { set; get; }
		public decimal? RollingReservePercent { set; get; }
		public decimal? UnsettledAmount { set; get; }
	}
}
