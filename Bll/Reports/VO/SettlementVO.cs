﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;

namespace Netpay.Infrastructure.VO
{
	public class SettlementVO
   {
      public SettlementVO() { }

		public SettlementVO(tblTransactionPay entity)
		{
			ID = entity.id;
			PayDate = entity.PayDate;
			TransPayTotal = entity.transPayTotal;
			TransChargeTotal = entity.transChargeTotal;
			CurrencyID = (int)entity.Currency;
			Tp_Note = entity.tp_Note;
			CompanyID = entity.CompanyID;
			VatAmount = entity.VatAmount;
			InvoiceNumber = entity.InvoiceNumber;
			TotalCaptureCount = entity.TotalCaptureCount;
			TotalCaptureAmount = entity.TotalCaptureAmount;
			TotalAdminCount = entity.TotalAdminCount;
			TotalAdminAmount = entity.TotalAdminAmount;
			TotalSystemCount = entity.TotalSystemCount;
			TotalSystemAmount = entity.TotalSystemAmount;
			TotalRefundCount = entity.TotalRefundCount;
			TotalRefundAmount = entity.TotalRefundAmount;
			TotalChbCount = entity.TotalChbCount;
			TotalChbAmount = entity.TotalChbAmount;
			TotalFeeProcessCapture = entity.TotalFeeProcessCapture;
			TotalFeeClarification = entity.TotalFeeClarification;
			TotalFeeFinancing = entity.TotalFeeFinancing;
			TotalFeeHandling = entity.TotalFeeHandling;
			TotalFeeBank = entity.TotalFeeBank;
			TotalRollingReserve = entity.TotalRollingReserve;
			TotalRollingRelease = entity.TotalRollingRelease;
			TotalDirectDeposit = entity.TotalDirectDeposit;
			TotalFeeChb = entity.TotalFeeChb;
			TotalAmountFee = entity.TotalAmountFee;
			TotalAmountTrans = entity.TotalAmountTrans;
			TotalPayout = entity.TotalPayout;
		}

		public SettlementVO(viewSettlementsReport entity) 
        {		
			ID = entity.id;
			PayDate = entity.PayDate;
			TransPayTotal = entity.transPayTotal;
			TransChargeTotal = entity.transChargeTotal;
			CurrencyID = (int)entity.currency;
			WireFee = entity.wireFee;
			WireStatus = entity.WireStatus;
			WireStatusDate = entity.WireStatusDate;
			Tp_Note = entity.tp_Note;
			WireFileName = entity.WireFileName;
			CompanyID = entity.CompanyID;
			InvoiceNumber = entity.InvoiceNumber;
			ID_Type = entity.id_Type;
			ID_BillingCompanyID = entity.id_BillingCompanyID;
        }
        
		public int ID { get; set; }
		public DateTime PayDate { get; set; }
		public decimal VatAmount { get; set; }
		public double TransPayTotal { get; set; }
		public double TransChargeTotal { get; set; }
		public int CurrencyID { get; set; }
		public decimal? WireFee { get; set; }
		public byte? WireStatus { get; set; }
		public DateTime? WireStatusDate { get; set; }
		public string Tp_Note { get; set; }
		public string WireFileName { get; set; }
		public int CompanyID { get; set; }
		public int InvoiceNumber { get; set; }
		public int? ID_Type { get; set; }
		public int? ID_BillingCompanyID { get; set; }

		public int? TotalCaptureCount { get; set; }
		public decimal? TotalCaptureAmount { get; set; }
		public int? TotalAdminCount { get; set; }
		public decimal? TotalAdminAmount { get; set; }
		public int? TotalSystemCount { get; set; }
		public decimal? TotalSystemAmount { get; set; }
		public int? TotalRefundCount { get; set; }
		public decimal? TotalRefundAmount { get; set; }
		public int? TotalChbCount { get; set; }
		public decimal? TotalChbAmount { get; set; }
		public decimal? TotalFeeProcessCapture { get; set; }
		public decimal? TotalFeeProcessDecline { get; set; }
		public decimal? TotalFeeProcessAuth { get; set; }
		public decimal? TotalFeeClarification { get; set; }
		public decimal? TotalFeeFinancing { get; set; }
		public decimal? TotalFeeHandling { get; set; }
		public decimal? TotalFeeBank { get; set; }
		public decimal? TotalRollingReserve { get; set; }
		public decimal? TotalRollingRelease { get; set; }
		public decimal? TotalDirectDeposit { get; set; }
		public decimal? TotalVatRatio { get; set; }
		public decimal? TotalFeeChb { get; set; }
		public decimal? TotalAmountFee { get; set; }
		public decimal? TotalAmountTrans { get; set; }
		public decimal? TotalPayout { get; set; }
    }
}
