﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;


namespace Netpay.Bll.Reports.VO
{
	public class TransactionReportVO
	{
		public TransactionReportVO(){}
		
		[EntityPropertyMapping("TerminalID")]
		public int? TerminalID { get; set; }

		[EntityPropertyMapping("TerminalName")]
		public string TerminalName { get; set; }

        [EntityPropertyMapping("TerminalSearchTag")]
        public string TerminalSearchTag { get; set; }

        [EntityPropertyMapping("TerminalNumber")]
		public string TerminalNumber { get; set; }
		[EntityPropertyMapping("AcquiringBankID")]
		public int? AcquiringBankID { get; set; }
		[EntityPropertyMapping("AcquiringBankName")]
		public string AcquiringBankName { get; set; }
		[EntityPropertyMapping("MerchantID")]
		public int? MerchantID { get; set; }
		[EntityPropertyMapping("MerchantNumber")]
		public int? MerchantNumber { get; set; }
		[EntityPropertyMapping("MerchantName")]
		public string MerchantName { get; set; }
        [EntityPropertyMapping("MerchantEmail")]
        public string MerchantEmail { get; set; }
		public int TransactionID { get; set; }
		[EntityPropertyMapping("TransactionDate")]
		public DateTime TransactionDate { get; set; }
		[EntityPropertyMapping("TotalCount")]
		public int TotalCount { get; set; }
		[EntityPropertyMapping("TotalAmount")]
		public decimal TotalAmount { get; set; }
		[EntityPropertyMapping("PassedCount")]
		public int PassedCount { get; set; }
		[EntityPropertyMapping("PassedAmount")]
		public decimal PassedAmount { get; set; }
		[EntityPropertyMapping("RefundsCount")]
		public int RefundsCount { get; set; }
		[EntityPropertyMapping("RefundsAmount")]
		public decimal RefundsAmount { get; set; }
		[EntityPropertyMapping("ChargebacksCount")]
		public int ChargebacksCount { get; set; }
		[EntityPropertyMapping("ChargebacksAmount")]
		public decimal ChargebacksAmount { get; set; }
        [EntityPropertyMapping("PendingChargebacksCount")]
        public int PendingChargebacksCount { get; set; }
        [EntityPropertyMapping("PendingChargebacksAmount")]
        public decimal PendingChargebacksAmount { get; set; }
        [EntityPropertyMapping("FailedCount")]
		public int FailedCount { get; set; }
		[EntityPropertyMapping("FailedAmount")]
		public decimal FailedAmount { get; set; }
        [EntityPropertyMapping("FraudCount")]
        public int FraudCount { get; set; }
        [EntityPropertyMapping("FraudAmount")]
        public decimal FraudAmount { get; set; }
        [EntityPropertyMapping("TransactionCurrencyID")]
		public int? TransactionCurrencyID { get; set; }
		[EntityPropertyMapping("TransactionCurrencyIsoCode")]
		public string TransactionCurrencyIsoCode { get; set; }
		[EntityPropertyMapping("TransactionAmountUSD")]
		public decimal TransactionAmountUSD { get; set; }
		[EntityPropertyMapping("TransactionStatus")]
		public string TransactionStatus { get; set; }
		[EntityPropertyMapping("PaymentMethodID")]
		public byte? PaymentMethodID { get; set; }
		public string RejectionCode { get; set; }
		public string RejectionText { get; set; }
		public int? TransTypeID { get; set; }
		public string TerminalContractNumber { get; set; }
		[EntityPropertyMapping("MerchantGroupID")]
		public int? MerchantGroupID { get; set; }
		[EntityPropertyMapping("MerchantGroupName")]
		public string MerchantGroupName { get; set; }
		[EntityPropertyMapping("IndustryID")]
		public int? IndustryID { get; set; }
		[EntityPropertyMapping("IndustryText")]
		public string IndustryText { get; set; }
		[EntityPropertyMapping("PassedTransactionFee")]
		public decimal PassedTransactionFee { get; set; }
		[EntityPropertyMapping("RefundFee")]
		public decimal RefundFee { get; set; }
		[EntityPropertyMapping("FailedTransactionFee")]
		public decimal FailedTransactionFee { get; set; }
		[EntityPropertyMapping("RatioFee")]
		public decimal RatioFee { get; set; }
		[EntityPropertyMapping("ChargebackFee")]
		public decimal ChargebackFee { get; set; }
		[EntityPropertyMapping("ClarificationFee")]
		public decimal ClarificationFee { get; set; }
		[EntityPropertyMapping("CapturedDebitFee")]
		public decimal? CapturedDebitFee { get; set; }
		[EntityPropertyMapping("CapturedDebitFeeCHB")]
		public decimal? CapturedDebitFeeCHB { get; set; }
		[EntityPropertyMapping("RefundDebitFee")]
		public decimal? RefundDebitFee { get; set; }
		[EntityPropertyMapping("DeclinedDebitFee")]
		public decimal? DeclinedDebitFee { get; set; }
		[EntityPropertyMapping("AuthorizedDebitFee")]
		public decimal? AuthorizedDebitFee { get; set; }



		[EntityPropertyMapping("HandlingFee")]
        public decimal HandlingFee { get; set; }


        [EntityPropertyMapping("ManualFee")]
        public decimal ManualFee { get; set; }





        [EntityPropertyMapping("FraudFee")]
        public decimal FraudFee { get; set; }

        [EntityPropertyMapping("MonthID")]
        public bool MonthID { get; set; }
		[EntityPropertyMapping("MonthText")]
		public bool MonthText { get; set; }
		[EntityPropertyMapping("WeekID")]
		public int WeekID { get; set; }
		[EntityPropertyMapping("WeekText")]
		public string WeekText { get; set; }
		[EntityPropertyMapping("TransactionMonth")]
		public string TransactionMonth { get; set; }
		[EntityPropertyMapping("IPCountry")]
		public string IPCountry { get; set; }
		[EntityPropertyMapping("BinCountry")]
        public string BinCountry { get; set; }
        [EntityPropertyMapping("MerchantDepartmentName")]
		public string MerchantDepartmentName { get; set; }
		[EntityPropertyMapping("AdminTransDebit")]
		public decimal? AdminTransDebit { get; set; }
		[EntityPropertyMapping("AdminTransCredit")]
		public decimal? AdminTransCredit { get; set; }
		[EntityPropertyMapping("SICCodeNumber")]
		public short? SICCodeNumber { get; set; }

		public decimal CaptureFee { get { return RatioFee + PassedTransactionFee; } set { } }

		public decimal CaptureProfit { get { return CaptureFee - CapturedDebitFee.GetValueOrDefault(); } set { } }
		public decimal RefundProfit { get { return RefundFee - RefundDebitFee.GetValueOrDefault(); } set { } }
		public decimal CHBProfit { get { return ChargebackFee - CapturedDebitFeeCHB.GetValueOrDefault(); } set { } }
		public decimal DeclinedProfit { get { return FailedTransactionFee - DeclinedDebitFee.GetValueOrDefault(); } set { } }
		public decimal TotalProfit { get { return CaptureProfit + RefundProfit + CHBProfit + DeclinedProfit; } set { } }

		public static TransactionReportVO operator +(TransactionReportVO a, TransactionReportVO b)
		{
			TransactionReportVO result = new TransactionReportVO();
			result.TotalAmount = a.TotalAmount + b.TotalAmount;
			result.TotalCount = a.TotalCount + b.TotalCount;
			result.PassedAmount = a.PassedAmount + b.PassedAmount;
			result.PassedCount = a.PassedCount + b.PassedCount;
			result.RefundsAmount = a.RefundsAmount + b.RefundsAmount;
			result.RefundsCount = a.RefundsCount + b.RefundsCount;
			result.ChargebacksAmount = a.ChargebacksAmount + b.ChargebacksAmount;
			result.ChargebacksCount = a.ChargebacksCount + b.ChargebacksCount;
            result.PendingChargebacksAmount = a.PendingChargebacksAmount + b.PendingChargebacksAmount;
            result.PendingChargebacksCount = a.PendingChargebacksCount + b.PendingChargebacksCount;
			result.FailedAmount = a.FailedAmount + b.FailedAmount;
			result.FailedCount = a.FailedCount + b.FailedCount;
			result.PassedTransactionFee = a.PassedTransactionFee + b.PassedTransactionFee;
			result.RefundFee = a.RefundFee + b.RefundFee;
			result.FailedTransactionFee = a.FailedTransactionFee + b.FailedTransactionFee;
			result.RatioFee = a.RatioFee + b.RatioFee;
			result.ChargebackFee = a.ChargebackFee + b.ChargebackFee;
			result.HandlingFee = a.HandlingFee + b.HandlingFee;
			result.CapturedDebitFee = a.CapturedDebitFee.GetValueOrDefault(0) + b.CapturedDebitFee.GetValueOrDefault(0);
			result.RefundDebitFee = a.RefundDebitFee.GetValueOrDefault(0) + b.RefundDebitFee.GetValueOrDefault(0);
			result.DeclinedDebitFee = a.DeclinedDebitFee.GetValueOrDefault(0) + b.DeclinedDebitFee.GetValueOrDefault(0);
			result.AuthorizedDebitFee = a.AuthorizedDebitFee.GetValueOrDefault(0) + b.AuthorizedDebitFee.GetValueOrDefault(0);
			result.CapturedDebitFeeCHB = a.CapturedDebitFeeCHB.GetValueOrDefault(0) + b.CapturedDebitFeeCHB.GetValueOrDefault(0);
			result.AdminTransCredit = a.AdminTransCredit.GetValueOrDefault(0) + b.AdminTransCredit.GetValueOrDefault(0);
			result.AdminTransDebit = a.AdminTransDebit.GetValueOrDefault(0) + b.AdminTransDebit.GetValueOrDefault(0);

			return result;
		}
	}
}
