﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.Reports.VO
{
	public class FailStatReplyGroupVO
	{
		private string _terminalNumber = null;
		private int? _groupCount = null;
		private string _contractNumber = null;

		public FailStatReplyGroupVO() { }

		public FailStatReplyGroupVO(string terminalNumber, int? groupCount, string contractNumber)
		{
			_terminalNumber = terminalNumber;
			_groupCount = groupCount;
			_contractNumber = contractNumber;			
		}

		public string TerminalNumber
		{
			get { return _terminalNumber; }
			set { _terminalNumber = value; }
		}
		public int? GroupCount
		{
			get { return _groupCount; }
			set { _groupCount = value; }
		}
		public string ContractNumber
		{
			get { return _contractNumber; }
			set { _contractNumber = value; }
		}
	}
}
