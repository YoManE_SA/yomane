﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Netpay.CommonTypes;
using Netpay.Infrastructure;

namespace Netpay.Bll.Reports.VO
{
	public class FailStatGroupVO
	{
		private string _rejectCode = null;
		private int? _groupCount = null;
		private string _rejectText = null;
		private string _rejectingSourceID = null;
		private decimal? _amount = null;

		public FailStatGroupVO() { }

		public FailStatGroupVO(string rejectCode, int? groupCount, string rejectText, string rejectingSourceID, decimal? amount)
		{
			_rejectCode = rejectCode;
			_groupCount = groupCount;
			_rejectText = rejectText;
			_rejectingSourceID = rejectingSourceID;		
			_amount = amount;	
		}

		public decimal? Amount
		{
			get { return _amount; }
			set { _amount = value; }
		}

		public string RejectCode 
		{
			get { return _rejectCode; }
			set { _rejectCode = value; } 
		}

		public int? GroupCount 
		{
			get { return _groupCount; }
			set { _groupCount = value; } 
		}

		public string RejectText 
		{
			get { return _rejectText; }
			set { _rejectText = value; }
		}

		public string RejectingSourceID 
		{
			get { return _rejectingSourceID; }
			set { _rejectingSourceID = value; }
		}

		public string GetRejectedSourceText()
		{
			if (_rejectingSourceID == null)
				return "";

			return  Infrastructure.GlobalData.GetValue(GlobalDataGroup.RejectingSource, Language.English, int.Parse(_rejectingSourceID)).Value; 
		}
	}
}
