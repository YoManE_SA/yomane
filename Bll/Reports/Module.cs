﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Reports
{
	public class Module : Infrastructure.Module
	{
		public const string ModuleName = "Reports";
		public override string Name { get { return ModuleName; } }
		public override decimal Version { get { return 1.0m; } }
		public static Module Current { get { return Infrastructure.Module.Get(ModuleName) as Module; } }

		public const string SecuredObjectName = "View";
		public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Transactions.Module.Current, SecuredObjectName); } }

		protected override void OnInstall(EventArgs e)
		{
			Infrastructure.Security.SecuredObject.Create(this, SecuredObjectName, PermissionGroup.Read);
			base.OnInstall(e);
		}

		protected override void OnUninstall(EventArgs e)
		{
			Infrastructure.Security.SecuredObject.Remove(this);
			base.OnUninstall(e);
		}
	}
}
