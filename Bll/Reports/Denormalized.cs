﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure;
using Netpay.Dal.Reports;
using Netpay.CommonTypes;
using Netpay.Bll.Reports.VO;

namespace Netpay.Bll.Reports
{
	public static class Denormalized
	{
		/*
		/// <summary>
		/// Search transaction denormalized history
		/// This overload also returns sibling histories of the found histories.
		/// Sibling histories are filtered by siblingFilters.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="filters"></param>
		/// <param name="siblingFilters"></param>
		/// <returns></returns>
		public static List<DenormalizedTransactionHistoryVO> SearchHistory(SearchFilters filters, SearchFilters siblingFilters)
		{
			User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited, UserType.NetpayUser, UserType.NetpayAdmin });
			if (user.Type != UserType.NetpayAdmin)
				filters.merchantIDs = SecurityManager.FilterRequestedIDs(filters.merchantIDs, user.AllowedMerchantsIDs);

			string outerFilters = string.Format(" AND MerchantID IN ({0}) ", filters.merchantIDs.ToDelimitedString());
			if (siblingFilters.transactionHistoryTypes != null && siblingFilters.transactionHistoryTypes.Count > 0)
				outerFilters += string.Format(" AND HistoryTypeID IN ({0}) ", siblingFilters.transactionHistoryTypes.Select(tht => (int)tht).ToDelimitedString());

			string innerFilters = "";
			if (filters.dateFrom != null)
				innerFilters += string.Format("\n AND CAST(HistoryInsertDate AS DATE) >= CAST('{0}' AS DATE)", filters.dateFrom.Value.ToSql());
			if (filters.dateTo != null)
				innerFilters += string.Format("\n AND CAST(HistoryInsertDate AS DATE) <= CAST('{0}' AS DATE)", filters.dateTo.Value.ToSql());
			if (filters.transactionHistoryTypes != null && filters.transactionHistoryTypes.Count > 0)
				innerFilters += string.Format(" AND HistoryTypeID IN ({0}) ", filters.transactionHistoryTypes.Select(tht => (int)tht).ToDelimitedString());

			ReportsDataContext dc = new ReportsDataContext(user.Domain.ReportsConnectionString);
			string query = "SELECT * FROM DenormalizedTransactionHistory WHERE 1=1 " + outerFilters + " AND TransactionID IN ( SELECT TransactionID FROM DenormalizedTransactionHistory WHERE 1=1 " + innerFilters + ")";
			List<DenormalizedTransactionHistoryVO> results = dc.ExecuteQuery<DenormalizedTransactionHistoryVO>(query, new object[0]{}).ToList();

			return results;		
		}
		*/

		public static List<DenormalizedTransactionHistoryVO> SearchHistory(Transactions.History.SearchFilters filters)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant });
			filters.MerchantIDs = Accounts.AccountFilter.Current.FilterIDList(Accounts.AccountType.Merchant, filters.MerchantIDs);

			string whereClause = " WHERE 1=1 ";
			if (filters.MerchantIDs != null && filters.MerchantIDs.Count > 0)
				whereClause += string.Format(" AND MerchantID IN ({0}) ", filters.MerchantIDs.ToDelimitedString());
			if (filters.HistoryTypes != null && filters.HistoryTypes.Count > 0)
				whereClause += string.Format(" AND HistoryTypeID IN ({0}) ", filters.HistoryTypes.Select(tht => (int)tht).ToDelimitedString());
			if (filters.Date.From != null)
				whereClause += string.Format("\n AND CAST(HistoryInsertDate AS DATE) >= CAST('{0}' AS DATE)", filters.Date.From.Value.ToSql());
			if (filters.Date.To != null)
				whereClause += string.Format("\n AND CAST(HistoryInsertDate AS DATE) <= CAST('{0}' AS DATE)", filters.Date.To.Value.ToSql());

			using(ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString))
			{
				string query = "SELECT * FROM DenormalizedTransactionHistory " + whereClause;
				return  dc.ExecuteQuery<DenormalizedTransactionHistoryVO>(query, new object[0] { }).ToList();
			}
		}

		/// <summary>
		/// Search transaction denormalized history
		/// </summary>
		public static List<DenormalizedTransactionHistoryVO> SearchHistory(Transactions.History.SearchFilters filters, ISortAndPage sortAndPage) 
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant });
			filters.MerchantIDs = Accounts.AccountFilter.Current.FilterIDList(Accounts.AccountType.Merchant, filters.MerchantIDs);

			using(ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString))
			{
				var expression = (IQueryable<DenormalizedTransactionHistory>)from t in dc.DenormalizedTransactionHistories orderby t.HistoryInsertDate descending select t;
				if (filters.MerchantIDs != null) expression = expression.Where(th => filters.MerchantIDs.Contains(th.MerchantID.GetValueOrDefault()));

				if (filters.Date.From != null) expression = expression.Where(t => t.HistoryInsertDate >= filters.Date.From.Value.MinTime());
				if (filters.Date.To != null) expression = expression.Where(t => t.HistoryInsertDate <= filters.Date.To.Value.MaxTime());
				if (filters.HistoryTypes != null && filters.HistoryTypes.Count > 0) 
				{
					int[] historyTypeIDs = filters.HistoryTypes.Select(tht => (int)tht).ToArray();
					expression = expression.Where(th => historyTypeIDs.Contains(th.HistoryTypeID));
				}

				if (sortAndPage != null) sortAndPage.DataFunction = (pi) => SearchHistory(filters, sortAndPage);
				return expression.ApplySortAndPage(sortAndPage).Select(th => new DenormalizedTransactionHistoryVO(th)).ToList();
			}
		}
	}
}
