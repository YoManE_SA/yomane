﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.Reports
{
	public enum ReportType
	{
		MerchantCapturedTransactions = 1,
		AdminDailyRiskByTerminal = 2,
		AdminDailyRiskByMerchant = 3,
		AdminFailStats = 4,
		AdminRollingReserves = 5,
		AdminDailyStatusByMerchant = 6,
		AdminDailyStatusByTerminal = 7,
		AdminTransactionsReport = 8,
		MerchantSummary = 9,
		MerchantDeclinedTransactions = 10,
		MerchantChargebackTransactions = 21,
		MerchantDeclinedArchive = 11,
		MerchantRecurringCharges = 12,
		MerchantRecurringSeries = 13,
		MerchantCapturedArchive = 14,
		MerchantPendingTransactions = 15,
		PartnerCapturedTransactions = 16,
		PartnerDeclinedTransactions = 17,
		MerchantCarts = 18,
		MerchantRollingReserve = 19,
		MerchantSettlmentReport = 20,
        MerchantCustomerTransactionsReport = 22,
        MerchantRefundRequests = 23,
        ExportFromAdminList = 24
	}
}
