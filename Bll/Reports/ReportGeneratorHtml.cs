﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Reflection;
using Netpay.Infrastructure.Security;

using Netpay.Infrastructure;
using Netpay.Dal.Reports;
using Netpay.CommonTypes;
using System.Data;
using System.Dynamic;
using Netpay.Bll.Reports.VO;
using Netpay.Bll.OldVO;

namespace Netpay.Bll.Reports
{
    /*
	public class ReportGeneratorHtml : ReportGeneratorBase
    {
        public const string currencyFormat = ",0.00";

		public static string CreateExcel(DataTable data)
		{
			return CreateExcelHeader(data) + CreateExcelBody(data) + CreateExcelFooter();
		}
		
		public static string CreateExcel(IEnumerable report, List<ReportFieldInfo> fields)
		{
			return CreateExcel(report, fields, null);
		}

		public static string CreateExcel(IEnumerable report, List<ReportFieldInfo> fields, string header)
		{
			return CreateExcelHeader(fields, header) + CreateExcelBody(report, fields) + CreateExcelFooter();
		}

		public static string CreateExcelHeader(List<ReportFieldInfo> fields, string headerText)
		{
			return CreateExcelHeader(fields, null, headerText);
		}

		public static string CreateExcelHeader(List<ReportFieldInfo> fields, string mainTitles, string headerText)
		{
			StringBuilder excelBuilder = new StringBuilder("<html xmlns=\"http://www.w3.org/TR/REC-html40\">");
			excelBuilder.Append("<head>");
			excelBuilder.Append("<meta http-equiv=Content-Type content=\"text/html; charset=utf-8\">");
            excelBuilder.Append("<style> .decimal{mso-number-format:\"\\#\\#0\\.00\";} </style>");
			excelBuilder.Append("</head>");
			excelBuilder.Append("<body>");
			excelBuilder.Append("<table border=\"1\">");
			
			if (mainTitles != null)
				excelBuilder.AppendFormat("<tr><td colspan=\"{0}\"><font size=\"4\" face=\"Arial\">{1}</font></td></tr>", fields.Count, mainTitles);
			if (headerText != null)
				excelBuilder.AppendFormat("<tr><td colspan=\"{0}\">{1}</td></tr>", fields.Count, headerText);

			excelBuilder.Append(CreateFieldsHeaders(fields, false));

			return excelBuilder.ToString();
		}

		public static string CreateFieldsHeaders(List<ReportFieldInfo> fields, bool spaceTop)
		{
			StringBuilder excelBuilder = new StringBuilder();
			if (spaceTop)
				excelBuilder.AppendFormat("<tr><td colspan=\"{0}\">&nbsp;</td></tr>", fields.Count);
			excelBuilder.Append("<tr>");
			foreach (ReportFieldInfo currentField in fields)
			{
				if (currentField.Title.Trim() == "&nbsp;")
					excelBuilder.Append("<th bgcolor=\"#ffffff\">" + currentField.Title + "</th>");
				else
					excelBuilder.Append("<th bgcolor=\"#edf662\">" + currentField.Title + "</th>");
			}
			excelBuilder.Append("</tr>");
			return excelBuilder.ToString();
		}

		public static string CreateExcelHeader(DataTable data)
		{
			StringBuilder excelBuilder = new StringBuilder("<html xmlns=\"http://www.w3.org/TR/REC-html40\">");
			excelBuilder.Append("<head>");
			excelBuilder.Append("<meta http-equiv=Content-Type content=\"text/html; charset=utf-8\">");
            excelBuilder.Append("<style> .decimal{mso-number-format:\"\\#\\#0\\.00\";} </style>");
			excelBuilder.Append("</head>");
			excelBuilder.Append("<body>");
			excelBuilder.Append("<table border=\"1\">");
			excelBuilder.Append("<tr>");
			foreach (DataColumn currentColumn in data.Columns)
			{
				excelBuilder.Append("<th bgcolor=\"#edf662\">" + currentColumn.ColumnName + "</th>");
			}
			excelBuilder.Append("</tr>");

			return excelBuilder.ToString();
		}

		public static string CreateExcelBody(DataTable data)
		{
			StringBuilder excelBuilder = new StringBuilder();
			foreach (DataRow currentRow in data.Rows)
			{
				excelBuilder.Append("</tr>");

				foreach (DataColumn currentColumn in data.Columns) 
				{
					excelBuilder.Append("<td>" + currentRow[currentColumn] + "</td>");
				}

				excelBuilder.Append("</tr>");
			}

			return excelBuilder.ToString();
		}

		public static string CreateExcelBody(IEnumerable data, List<ReportFieldInfo> fields)
		{
			StringBuilder excelBuilder = new StringBuilder();
			foreach (object currentItem in data)
			{
				excelBuilder.Append("<tr>");
				PropertyInfo[] properties = currentItem.GetType().GetProperties();
				foreach (ReportFieldInfo currentField in fields)
				{
					if (currentField.Method != null)
                    {
                        string result = null;
                        try 
	                    {	        
		                    result = currentField.Method.Invoke(currentItem);
                            decimal parsed;
                            bool isDecimal = decimal.TryParse(result, out parsed);

                            if (isDecimal)
                                excelBuilder.Append("<td class='decimal'>" + result + "</td>");
                            else
                                excelBuilder.Append("<td>" + result + "</td>");
	                    }
	                    catch (Exception ex)
	                    {
                            Logger.Log(LogSeverity.Error, LogTag.Reports, "Error  invoking report field.", string.Format("Data item:\n {0}\n\nField:\n {1}\n\nError:\n {2}", currentItem.ToExtendedString(), currentField.ToExtendedString(), ex.ToString()));
	                    }                    
                    }
					else if (currentField.Property != null)
					{
						object propertyValue = currentItem;
						foreach (string propertyName in currentField.Property.Split('.'))
						{
							PropertyInfo propInfo = propertyValue.GetType().GetProperty(propertyName);
							if (propInfo == null)
								break;
							propertyValue = propInfo.GetValue(propertyValue, null);
							if (propertyValue == null)
								break;
						}

						if (propertyValue == null)
							excelBuilder.Append("<td>&nbsp;</td>");
						else
							excelBuilder.Append("<td>" + propertyValue + "</td>");
					}
					else
						excelBuilder.Append("<td>&nbsp;</td>");
				}
				excelBuilder.Append("</tr>");
			}

			return excelBuilder.ToString();
		}

		public static string CreateExcelFooter()
		{
			return "</table></body></html>";
		}

		public static List<ReportFieldInfo> GetFields(ReportType reportType)
		{
			return GetFields(Login.Current, reportType);
		}
		
		internal static List<ReportFieldInfo> GetFields(Login user, ReportType reportType)
		{
			List<ReportFieldInfo> fields = new List<ReportFieldInfo>();
			switch(reportType)
			{
				case ReportType.MerchantSettlmentReport:
					fields.Add(new ReportFieldInfo("Number", "ID")) ;
					fields.Add(new ReportFieldInfo("Date", (object item) => ((SettlementVO)item).PayDate.ToDateTimeFormat()));
					fields.Add(new ReportFieldInfo("Currency", (object item) => Currency.Get((int)((SettlementVO)item).CurrencyID).IsoCode));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					fields.Add(new ReportFieldInfo("Total Fee Amount", (object item) => ((SettlementVO)item).TotalAmountFee.GetValueOrDefault().ToAmountFormat()));
					fields.Add(new ReportFieldInfo("Total Trans. Amount", (object item) => ((SettlementVO)item).TotalAmountTrans.GetValueOrDefault().ToAmountFormat()));
					fields.Add(new ReportFieldInfo("Total Payout", (object item) => ((SettlementVO)item).TotalPayout.GetValueOrDefault().ToAmountFormat()));
					fields.Add(new ReportFieldInfo("Vat Ratio", (object item) => ((SettlementVO)item).TotalVatRatio.GetValueOrDefault().ToAmountFormat()));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					fields.Add(new ReportFieldInfo("Capture Count", "TotalCaptureCount"));
					fields.Add(new ReportFieldInfo("Capture Amount", (object item) => ((SettlementVO)item).TotalCaptureAmount.GetValueOrDefault().ToAmountFormat()));
					fields.Add(new ReportFieldInfo("Admin Count", (object item) => ((SettlementVO)item).TotalAdminCount.ToString()));
					fields.Add(new ReportFieldInfo("Admin Amount", (object item) => ((SettlementVO)item).TotalAdminAmount.ToString()));
					fields.Add(new ReportFieldInfo("System Count", "TotalSystemCount"));
					fields.Add(new ReportFieldInfo("System Amount", (object item) => ((SettlementVO)item).TotalSystemAmount.GetValueOrDefault().ToAmountFormat()));
					fields.Add(new ReportFieldInfo("Refund Count", "TotalRefundCount"));
					fields.Add(new ReportFieldInfo("Refund Amount", (object item) => ((SettlementVO)item).TotalRefundAmount.GetValueOrDefault().ToAmountFormat()));
					fields.Add(new ReportFieldInfo("CHB Count", "TotalChbCount"));
					fields.Add(new ReportFieldInfo("CHB Amount", (object item) => ((SettlementVO)item).TotalChbAmount.GetValueOrDefault().ToAmountFormat()));
					fields.Add(new ReportFieldInfo("Captured Process Fee", (object item) => ((SettlementVO)item).TotalFeeProcessCapture.GetValueOrDefault().ToAmountFormat()));
					//fields.Add(new ReportFieldInfo("FeeProcess Decline", (object item) => ((SettlementVO)item).TotalFeeProcessDecline.ToString()));
					//fields.Add(new ReportFieldInfo("FeeProcess Auth", (object item) => ((SettlementVO)item).TotalFeeProcessAuth.ToString()));
					fields.Add(new ReportFieldInfo("Clarification Fee", (object item) => ((SettlementVO)item).TotalFeeClarification.GetValueOrDefault().ToAmountFormat()));
					fields.Add(new ReportFieldInfo("Financing Fee", (object item) => ((SettlementVO)item).TotalFeeFinancing.GetValueOrDefault().ToAmountFormat()));
					fields.Add(new ReportFieldInfo("Handling Fee", (object item) => ((SettlementVO)item).TotalFeeHandling.GetValueOrDefault().ToAmountFormat()));
					fields.Add(new ReportFieldInfo("Bank Fee", (object item) => ((SettlementVO)item).TotalFeeBank.GetValueOrDefault().ToAmountFormat()));
					fields.Add(new ReportFieldInfo("CHB Fee ", (object item) => ((SettlementVO)item).TotalFeeChb.GetValueOrDefault().ToAmountFormat()));
					fields.Add(new ReportFieldInfo("Rolling Reserve", (object item) => ((SettlementVO)item).TotalRollingReserve.GetValueOrDefault().ToAmountFormat()));
					fields.Add(new ReportFieldInfo("Rolling Release", (object item) => ((SettlementVO)item).TotalRollingRelease.GetValueOrDefault().ToAmountFormat()));
					fields.Add(new ReportFieldInfo("Direct Deposit", (object item) => ((SettlementVO)item).TotalDirectDeposit.GetValueOrDefault().ToAmountFormat()));
					break;
				case ReportType.MerchantRollingReserve:
					fields.Add(new ReportFieldInfo("Date", (object item) => ((Transactions.Transaction)item).InsertDate.ToDateTimeFormat()));
					fields.Add(new ReportFieldInfo("Transaction", (object item) => ((Transactions.Transaction)item).ID.ToString()));
					fields.Add(new ReportFieldInfo("Settlement", (object item) => ((Transactions.Transaction)item).PrimaryPaymentID.ToString()));
					fields.Add(new ReportFieldInfo("Reserve Hold", (object item) => 
					{
						if (((Transactions.Transaction)item).CreditType == (byte)CreditType.Refund)
							return ((Transactions.Transaction)item).Amount.ToAmountFormat(((Transactions.Transaction)item).CurrencyID, true);
						
						return "";
					}));
					fields.Add(new ReportFieldInfo("Reserve Release", (object item) => 
					{
						if (((Transactions.Transaction)item).CreditType == CreditType.Regular)
							return ((Transactions.Transaction)item).Amount.ToAmountFormat(((Transactions.Transaction)item).CurrencyID);
						
						return "";
					}));
					fields.Add(new ReportFieldInfo("Comment", (object item) => ((Transactions.Transaction)item).Comment));
					break;
				case ReportType.AdminDailyRiskByMerchant:
					// details
					fields.Add(new ReportFieldInfo("Merchant Name", "CompanyName"));
					fields.Add(new ReportFieldInfo("Merchant #", "CompanyNumber"));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// captured
					fields.Add(new ReportFieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByMerchant)item).PassedVolumeDailyAvarge.GetValueOrDefault().ToString("#,0")));
					fields.Add(new ReportFieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByMerchant)item).PassedVolumeYesterday.GetValueOrDefault().ToString("#,0")));
					fields.Add(new ReportFieldInfo("Difference from Average (%)", (object item) => ReportHelper.GetFormattedDifference(((DailyRiskReportByMerchant)item).PassedVolumeYesterday.GetValueOrDefault(), ((DailyRiskReportByMerchant)item).PassedVolumeDailyAvarge.GetValueOrDefault(), 20)));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					fields.Add(new ReportFieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByMerchant)item).PassedTransactionSizeDailyAverage.GetValueOrDefault().ToString("#,0")));
					fields.Add(new ReportFieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByMerchant)item).PassedTransactionSizeYesterdayAverage.GetValueOrDefault().ToString("#,0")));
					fields.Add(new ReportFieldInfo("Difference from Average (%)", (object item) => ReportHelper.GetFormattedDifference(((DailyRiskReportByMerchant)item).PassedTransactionSizeYesterdayAverage.GetValueOrDefault(), ((DailyRiskReportByMerchant)item).PassedTransactionSizeDailyAverage.GetValueOrDefault(), 20)));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					fields.Add(new ReportFieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByMerchant)item).PassedTransactionCountDailyAverage.GetValueOrDefault().ToString("#,0.##")));
					fields.Add(new ReportFieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByMerchant)item).PassedTransactionCountYesterday.GetValueOrDefault().ToString()));
					fields.Add(new ReportFieldInfo("Difference from Average (%)", (object item) => ReportHelper.GetFormattedDifference(((DailyRiskReportByMerchant)item).PassedTransactionCountYesterday.GetValueOrDefault(), ((DailyRiskReportByMerchant)item).PassedTransactionCountDailyAverage.GetValueOrDefault(), 20)));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// declined
					fields.Add(new ReportFieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByMerchant)item).RejectedSourceIssuerPerDay.GetValueOrDefault().ToString("#,0.##")));
					fields.Add(new ReportFieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByMerchant)item).RejectedSourceIssuerYesterday.GetValueOrDefault().ToString()));
					fields.Add(new ReportFieldInfo("Difference from Average (%)", (object item) => ReportHelper.GetFormattedDifference(((DailyRiskReportByMerchant)item).RejectedSourceIssuerYesterday.GetValueOrDefault(), ((DailyRiskReportByMerchant)item).RejectedSourceIssuerPerDay.GetValueOrDefault(), 20)));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					fields.Add(new ReportFieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByMerchant)item).RejectedSourceRiskPerDay.GetValueOrDefault().ToString("#,0.##")));
					fields.Add(new ReportFieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByMerchant)item).RejectedSourceRiskYesterday.GetValueOrDefault().ToString()));
					fields.Add(new ReportFieldInfo("Difference from Average (%)", (object item) => ReportHelper.GetFormattedDifference(((DailyRiskReportByMerchant)item).RejectedSourceRiskYesterday.GetValueOrDefault(), ((DailyRiskReportByMerchant)item).RejectedSourceRiskPerDay.GetValueOrDefault(), 5)));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					fields.Add(new ReportFieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByMerchant)item).RejectedSourceGatewayPerDay.GetValueOrDefault().ToString("#,0.##")));
					fields.Add(new ReportFieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByMerchant)item).RejectedSourceGatewayYesterday.GetValueOrDefault().ToString()));
					fields.Add(new ReportFieldInfo("Difference from Average (%)", (object item) => ReportHelper.GetFormattedDifference(((DailyRiskReportByMerchant)item).RejectedSourceGatewayYesterday.GetValueOrDefault(), ((DailyRiskReportByMerchant)item).RejectedSourceGatewayPerDay.GetValueOrDefault(), 5)));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// refunds
					fields.Add(new ReportFieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByMerchant)item).RefundCountPerDayAverage.GetValueOrDefault().ToString("#,0.##")));
					fields.Add(new ReportFieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByMerchant)item).RefundCountYesterday.GetValueOrDefault().ToString()));
					fields.Add(new ReportFieldInfo("Difference from Average (%)", (object item) => ReportHelper.GetFormattedDifference(((DailyRiskReportByMerchant)item).RefundCountYesterday.GetValueOrDefault(), ((DailyRiskReportByMerchant)item).RefundCountPerDayAverage.GetValueOrDefault())));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// authorized
					fields.Add(new ReportFieldInfo("3 days old, not captured", (object item) => ((DailyRiskReportByMerchant)item).PreAuthorizedNotCapturedCount.GetValueOrDefault().ToString()));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// visa chargebacks
					fields.Add(new ReportFieldInfo("# of CHB month to date.", (object item) => ReportHelper.GetFormatted(((DailyRiskReportByMerchant)item).VisaThisMonthChargebacksCount.GetValueOrDefault(), 150, "#,0")));
					fields.Add(new ReportFieldInfo("% of CHB month to date.", (object item) => ReportHelper.GetFormattedDifference(((DailyRiskReportByMerchant)item).VisaThisMonthChargebacksCount.GetValueOrDefault(), ((DailyRiskReportByMerchant)item).VisaThisMonthNotChargebacksCount.GetValueOrDefault())));
					fields.Add(new ReportFieldInfo("Projected # of CHB at the end of this month.", (object item) => ReportHelper.GetFormatted(ThisMonthProjectedTotal(((DailyRiskReportByMerchant)item).VisaThisMonthChargebacksCount), 190D, "#,0")));
					fields.Add(new ReportFieldInfo("Projected % of CHB at the end of this month.", (object item) => ReportHelper.GetFormattedPercentage(ThisMonthProjectedTotal(((DailyRiskReportByMerchant)item).VisaThisMonthChargebacksCount), ThisMonthProjectedTotal(((DailyRiskReportByMerchant)item).VisaThisMonthNotChargebacksCount), 1.85D)));
					fields.Add(new ReportFieldInfo("% CHB Volume of Monthly Volume to Date", (object item) => ReportHelper.GetFormattedPercentage(((DailyRiskReportByMerchant)item).VisaThisMonthChargebacksAmount.GetValueOrDefault(), ((DailyRiskReportByMerchant)item).VisaThisMonthNotChargebacksAmount.GetValueOrDefault(), 10)));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// mastercard chargebacks
					fields.Add(new ReportFieldInfo("# of CHB month to date.", (object item) => ReportHelper.GetFormatted(((DailyRiskReportByMerchant)item).MastercardThisMonthChargebacksCount.GetValueOrDefault(), 35, "")));
					fields.Add(new ReportFieldInfo("% of CHB compared to Previous Month's # of Transactions.", (object item) => ReportHelper.GetFormattedDifference(((DailyRiskReportByMerchant)item).MastercardThisMonthChargebacksCount.GetValueOrDefault(), ((DailyRiskReportByMerchant)item).MastercardLastMonthNotChargebacksCount.GetValueOrDefault(), .5m)));
					fields.Add(new ReportFieldInfo("Projected # of CHB at the end of this month.", (object item) => ReportHelper.GetFormatted(ThisMonthProjectedTotal(((DailyRiskReportByMerchant)item).MastercardThisMonthChargebacksCount), 45, "#,0")));
					fields.Add(new ReportFieldInfo("Projected % of CHB at the end of this month.", (object item) => ReportHelper.GetFormattedPercentage(ThisMonthProjectedTotal(((DailyRiskReportByMerchant)item).MastercardThisMonthChargebacksCount), ThisMonthProjectedTotal(((DailyRiskReportByMerchant)item).MastercardThisMonthNotChargebacksCount), .8D)));
					fields.Add(new ReportFieldInfo("% CHB Volume of Monthly Volume to Date", (object item) => ReportHelper.GetFormattedPercentage(((DailyRiskReportByMerchant)item).MastercardThisMonthChargebacksAmount.GetValueOrDefault(), ((DailyRiskReportByMerchant)item).MastercardThisMonthNotChargebacksAmount.GetValueOrDefault(), 10m)));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));					
					break;
				case ReportType.AdminDailyRiskByTerminal:
					// details
					fields.Add(new ReportFieldInfo("Aquiring Bank", "AquiringBankName"));
					fields.Add(new ReportFieldInfo("Terminal Name", "TerminalName"));
					fields.Add(new ReportFieldInfo("Terminal #", "TerminalNumber"));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// captured
					fields.Add(new ReportFieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByTerminal)item).PassedVolumeDailyAvarge.GetValueOrDefault().ToString("#,0")));
					fields.Add(new ReportFieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByTerminal)item).PassedVolumeYesterday.GetValueOrDefault().ToString("#,0")));
					fields.Add(new ReportFieldInfo("Difference from Average (%)", (object item) => ReportHelper.GetFormattedDifference(((DailyRiskReportByTerminal)item).PassedVolumeYesterday.GetValueOrDefault(), ((DailyRiskReportByTerminal)item).PassedVolumeDailyAvarge.GetValueOrDefault(), 20)));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));	
					fields.Add(new ReportFieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByTerminal)item).PassedTransactionSizeDailyAverage.GetValueOrDefault().ToString("#,0")));
					fields.Add(new ReportFieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByTerminal)item).PassedTransactionSizeYesterdayAverage.GetValueOrDefault().ToString("#,0")));
					fields.Add(new ReportFieldInfo("Difference from Average (%)", (object item) => ReportHelper.GetFormattedDifference(((DailyRiskReportByTerminal)item).PassedTransactionSizeYesterdayAverage.GetValueOrDefault(), ((DailyRiskReportByTerminal)item).PassedTransactionSizeDailyAverage.GetValueOrDefault(), 20)));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					fields.Add(new ReportFieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByTerminal)item).PassedTransactionCountDailyAverage.GetValueOrDefault().ToString("#,0.##")));
					fields.Add(new ReportFieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByTerminal)item).PassedTransactionCountYesterday.GetValueOrDefault().ToString()));
					fields.Add(new ReportFieldInfo("Difference from Average (%)", (object item) => ReportHelper.GetFormattedDifference(((DailyRiskReportByTerminal)item).PassedTransactionCountYesterday.GetValueOrDefault(), ((DailyRiskReportByTerminal)item).PassedTransactionCountDailyAverage.GetValueOrDefault(), 20)));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// declined
					fields.Add(new ReportFieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByTerminal)item).RejectedSourceIssuerPerDay.GetValueOrDefault().ToString("#,0.##")));
					fields.Add(new ReportFieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByTerminal)item).RejectedSourceIssuerYesterday.GetValueOrDefault().ToString()));
					fields.Add(new ReportFieldInfo("Difference from Average (%)", (object item) => ReportHelper.GetFormattedDifference(((DailyRiskReportByTerminal)item).RejectedSourceIssuerYesterday.GetValueOrDefault(), ((DailyRiskReportByTerminal)item).RejectedSourceIssuerPerDay.GetValueOrDefault(), 20)));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					fields.Add(new ReportFieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByTerminal)item).RejectedSourceRiskPerDay.GetValueOrDefault().ToString("#,0.##")));
					fields.Add(new ReportFieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByTerminal)item).RejectedSourceRiskYesterday.GetValueOrDefault().ToString()));
					fields.Add(new ReportFieldInfo("Difference from Average (%)", (object item) => ReportHelper.GetFormattedDifference(((DailyRiskReportByTerminal)item).RejectedSourceRiskYesterday.GetValueOrDefault(), ((DailyRiskReportByTerminal)item).RejectedSourceRiskPerDay.GetValueOrDefault(), 5)));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					fields.Add(new ReportFieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByTerminal)item).RejectedSourceGatewayPerDay.GetValueOrDefault().ToString("#,0.##")));
					fields.Add(new ReportFieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByTerminal)item).RejectedSourceGatewayYesterday.GetValueOrDefault().ToString()));
					fields.Add(new ReportFieldInfo("Difference from Average (%)", (object item) => ReportHelper.GetFormattedDifference(((DailyRiskReportByTerminal)item).RejectedSourceGatewayYesterday.GetValueOrDefault(), ((DailyRiskReportByTerminal)item).RejectedSourceGatewayPerDay.GetValueOrDefault(), 5)));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// refunds
					fields.Add(new ReportFieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByTerminal)item).RefundCountPerDayAverage.GetValueOrDefault().ToString("#,0.##")));
					fields.Add(new ReportFieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByTerminal)item).RefundCountYesterday.GetValueOrDefault().ToString()));
					fields.Add(new ReportFieldInfo("Difference from Average (%)", (object item) => ReportHelper.GetFormattedDifference(((DailyRiskReportByTerminal)item).RefundCountYesterday.GetValueOrDefault(), ((DailyRiskReportByTerminal)item).RefundCountPerDayAverage.GetValueOrDefault())));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// authorized
					fields.Add(new ReportFieldInfo("3 days old, not captured", (object item) => ((DailyRiskReportByTerminal)item).PreAuthorizedNotCapturedCount.GetValueOrDefault().ToString()));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// visa chargebacks
					fields.Add(new ReportFieldInfo("# of CHB month to date.", (object item) => ReportHelper.GetFormatted(((DailyRiskReportByTerminal)item).VisaThisMonthChargebacksCount.GetValueOrDefault(), 150, "#,0")));
					fields.Add(new ReportFieldInfo("% of CHB month to date.", (object item) => ReportHelper.GetFormattedDifference(((DailyRiskReportByTerminal)item).VisaThisMonthChargebacksCount.GetValueOrDefault(), ((DailyRiskReportByTerminal)item).VisaThisMonthNotChargebacksCount.GetValueOrDefault())));
					fields.Add(new ReportFieldInfo("Projected # of CHB at the end of this month.", (object item) => ReportHelper.GetFormatted(ThisMonthProjectedTotal(((DailyRiskReportByTerminal)item).VisaThisMonthChargebacksCount), 190D, "#,0")));
					fields.Add(new ReportFieldInfo("Projected % of CHB at the end of this month.", (object item) => ReportHelper.GetFormattedPercentage(ThisMonthProjectedTotal(((DailyRiskReportByTerminal)item).VisaThisMonthChargebacksCount), ThisMonthProjectedTotal(((DailyRiskReportByTerminal)item).VisaThisMonthNotChargebacksCount), 1.85D)));
					fields.Add(new ReportFieldInfo("% CHB Volume of Monthly Volume to Date", (object item) => ReportHelper.GetFormattedPercentage(((DailyRiskReportByTerminal)item).VisaThisMonthChargebacksAmount.GetValueOrDefault(), ((DailyRiskReportByTerminal)item).VisaThisMonthNotChargebacksAmount.GetValueOrDefault(), 10)));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// mastercard chargebacks
					fields.Add(new ReportFieldInfo("# of CHB month to date.", (object item) => ReportHelper.GetFormatted(((DailyRiskReportByTerminal)item).MastercardThisMonthChargebacksCount.GetValueOrDefault(), 35, "")));
					fields.Add(new ReportFieldInfo("% of CHB compared to Previous Month's # of Transactions.", (object item) => ReportHelper.GetFormattedDifference(((DailyRiskReportByTerminal)item).MastercardThisMonthChargebacksCount.GetValueOrDefault(), ((DailyRiskReportByTerminal)item).MastercardLastMonthNotChargebacksCount.GetValueOrDefault(), .5m)));
					fields.Add(new ReportFieldInfo("Projected # of CHB at the end of this month.", (object item) => ReportHelper.GetFormatted(ThisMonthProjectedTotal(((DailyRiskReportByTerminal)item).MastercardThisMonthChargebacksCount), 45, "#,0")));
					fields.Add(new ReportFieldInfo("Projected % of CHB at the end of this month.", (object item) => ReportHelper.GetFormattedPercentage(ThisMonthProjectedTotal(((DailyRiskReportByTerminal)item).MastercardThisMonthChargebacksCount), ThisMonthProjectedTotal(((DailyRiskReportByTerminal)item).MastercardThisMonthNotChargebacksCount), .8D)));
					fields.Add(new ReportFieldInfo("% CHB Volume of Monthly Volume to Date", (object item) => ReportHelper.GetFormattedPercentage(((DailyRiskReportByTerminal)item).MastercardThisMonthChargebacksAmount.GetValueOrDefault(), ((DailyRiskReportByTerminal)item).MastercardThisMonthNotChargebacksAmount.GetValueOrDefault(), 10m)));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					break;
				case ReportType.AdminDailyStatusByMerchant:
					// info 
					fields.Add(new ReportFieldInfo("Merchant Name", "MerchantName"));
					fields.Add(new ReportFieldInfo("Merchant ID", "ID"));
					fields.Add(new ReportFieldInfo("Status", "MerchantStatusText"));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// Charge Attempts
					fields.Add(new ReportFieldInfo("Amount Authorization", (object item) => ((DailyStatusReportByMerchant)item).ChargeAttemptsAmountAuthorization.GetValueOrDefault().ToString(currencyFormat)));
					fields.Add(new ReportFieldInfo("Count Authorization", (object item) => ((DailyStatusReportByMerchant)item).ChargeAttemptsCountAuthorization.GetValueOrDefault().ToString("#,#")));
					fields.Add(new ReportFieldInfo("Amount Sales", (object item) => ((DailyStatusReportByMerchant)item).ChargeAttemptsAmountSales.GetValueOrDefault().ToString(currencyFormat)));
					fields.Add(new ReportFieldInfo("Count Sales", (object item) => ((DailyStatusReportByMerchant)item).ChargeAttemptsCountSales.GetValueOrDefault().ToString("#,#")));
					fields.Add(new ReportFieldInfo("Sent To Bank", (object item) => ((DailyStatusReportByMerchant)item).ChargeAttemptsSentToBank.GetValueOrDefault().ToString("#,#")));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// Issuer Decline Ratio
					fields.Add(new ReportFieldInfo("Percent", (object item) => ((DailyStatusReportByMerchant)item).IssuerDeclineRatio.GetValueOrDefault().ToString("P2")));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// Transactions
					fields.Add(new ReportFieldInfo("Amount Authorization", (object item) => ((DailyStatusReportByMerchant)item).TransactionsAmountAuthorization.GetValueOrDefault().ToString(currencyFormat)));
					fields.Add(new ReportFieldInfo("Count Authorization", (object item) => ((DailyStatusReportByMerchant)item).TransactionsCountAuthorization.GetValueOrDefault().ToString("#,#")));
					fields.Add(new ReportFieldInfo("Amount Sales", (object item) => ((DailyStatusReportByMerchant)item).TransactionAmountSales.GetValueOrDefault().ToString(currencyFormat)));
					fields.Add(new ReportFieldInfo("Count Sales", (object item) => ((DailyStatusReportByMerchant)item).TransactionCountSales.GetValueOrDefault().ToString("#,#")));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// Approval Ratio
					fields.Add(new ReportFieldInfo("Netpay", (object item) => ((DailyStatusReportByMerchant)item).NetpayApprovalRatio.GetValueOrDefault().ToString("P2")));
					fields.Add(new ReportFieldInfo("Bank", (object item) => ((DailyStatusReportByMerchant)item).BankApprovalRatio.GetValueOrDefault().ToString("P2")));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// Issuer Decline
					fields.Add(new ReportFieldInfo("Amount Authorization", (object item) => ((DailyStatusReportByMerchant)item).IssuerDeclineAmountAuthorization.GetValueOrDefault().ToString(currencyFormat)));
					fields.Add(new ReportFieldInfo("Count Authorization", (object item) => ((DailyStatusReportByMerchant)item).IssuerDeclineCountAuthorization.GetValueOrDefault().ToString("#,#")));
					fields.Add(new ReportFieldInfo("Amount Sales", (object item) => ((DailyStatusReportByMerchant)item).IssuerDeclineAmountSales.GetValueOrDefault().ToString(currencyFormat)));
					fields.Add(new ReportFieldInfo("Count Sales", (object item) => ((DailyStatusReportByMerchant)item).IssuerDeclineCountSales.GetValueOrDefault().ToString("#,#")));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// Risk Decline 
					fields.Add(new ReportFieldInfo("Amount Authorization", (object item) => ((DailyStatusReportByMerchant)item).RiskDeclineAmountAuthorization.GetValueOrDefault().ToString(currencyFormat)));
					fields.Add(new ReportFieldInfo("Count Authorization", (object item) => ((DailyStatusReportByMerchant)item).RiskDeclineCountAuthorization.GetValueOrDefault().ToString("#,#")));
					fields.Add(new ReportFieldInfo("Amount Sales", (object item) => ((DailyStatusReportByMerchant)item).RiskDeclineAmountSales.GetValueOrDefault().ToString(currencyFormat)));
					fields.Add(new ReportFieldInfo("Count Sales", (object item) => ((DailyStatusReportByMerchant)item).RiskDeclineCountSales.GetValueOrDefault().ToString("#,#")));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// GTW Decline
					fields.Add(new ReportFieldInfo("Amount Authorization", (object item) => ((DailyStatusReportByMerchant)item).GTWDeclineAmountAuthorization.GetValueOrDefault().ToString(currencyFormat)));
					fields.Add(new ReportFieldInfo("Count Authorization", (object item) => ((DailyStatusReportByMerchant)item).GTWDeclineCountAuthorization.GetValueOrDefault().ToString("#,#")));
					fields.Add(new ReportFieldInfo("Amount Sales", (object item) => ((DailyStatusReportByMerchant)item).GTWDeclineAmountSales.GetValueOrDefault().ToString(currencyFormat)));
					fields.Add(new ReportFieldInfo("Count Sales", (object item) => ((DailyStatusReportByMerchant)item).GTWDeclineCountSales.GetValueOrDefault().ToString("#,#")));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// Visa Chargebacks
					fields.Add(new ReportFieldInfo("# Of CHB Month To Date", (object item) => ((DailyStatusReportByMerchant)item).NumberOfVisaCHBMonthToDate.GetValueOrDefault().ToString("#,#")));
					fields.Add(new ReportFieldInfo("# Of Sales Month To Date", (object item) => ((DailyStatusReportByMerchant)item).NumberOfVisaSalesMonthToDate.GetValueOrDefault().ToString("#,#")));
					fields.Add(new ReportFieldInfo("% of CHB Month To Date", (object item) => ((DailyStatusReportByMerchant)item).PercentOfVisaCHBMonthToDate.GetValueOrDefault().ToString("P2")));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// MC Chargebacks
					fields.Add(new ReportFieldInfo("# Of CHB Month To Date", (object item) => ((DailyStatusReportByMerchant)item).NumberOfMCCHBMonthToDate.GetValueOrDefault().ToString("#,#")));
					fields.Add(new ReportFieldInfo("# Of Sales Previuos Month", (object item) => ((DailyStatusReportByMerchant)item).NumberOfMCSalesPrevMonthToDate.GetValueOrDefault().ToString("#,#")));
					fields.Add(new ReportFieldInfo("% of CHB Month To Date", (object item) => ((DailyStatusReportByMerchant)item).PercentOfMCCHBMonthToDate.GetValueOrDefault().ToString("P2")));
					fields.Add(new ReportFieldInfo("# Of Sales Month To Date", (object item) => ((DailyStatusReportByMerchant)item).NumberOfMCSalesMonthToDate.GetValueOrDefault().ToString("#,#")));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// refunds
					fields.Add(new ReportFieldInfo("Count", (object item) => ((DailyStatusReportByMerchant)item).RefundsCount.GetValueOrDefault().ToString("#,#")));
					fields.Add(new ReportFieldInfo("Amount", (object item) => ((DailyStatusReportByMerchant)item).RefundsAmount.GetValueOrDefault().ToString(currencyFormat)));
					fields.Add(new ReportFieldInfo("Ratio", (object item) => ((DailyStatusReportByMerchant)item).RefundsRatio.GetValueOrDefault().ToString("P2")));
					break;
				case ReportType.AdminDailyStatusByTerminal:
					// info
					fields.Add(new ReportFieldInfo("Acquiring Bank Name", "AcquiringBankName"));
					fields.Add(new ReportFieldInfo("Terminal Number", "TerminalNumber"));
					fields.Add(new ReportFieldInfo("Terminal Name", "TerminalName"));
					fields.Add(new ReportFieldInfo("Active", (object item) => ((DailyStatusReportByTerminal)item).TerminalIsActive.GetValueOrDefault() ? "Yes" : "No"));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// transactions
					fields.Add(new ReportFieldInfo("Amount", (object item) => ((DailyStatusReportByTerminal)item).TransactionAmountSales.GetValueOrDefault().ToString(currencyFormat)));
					fields.Add(new ReportFieldInfo("Count", (object item) => ((DailyStatusReportByTerminal)item).TransactionCountSales.GetValueOrDefault().ToString("#,#")));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// visa
					fields.Add(new ReportFieldInfo("Count", (object item) => ((DailyStatusReportByTerminal)item).NumberOfVisaCHBMonthToDate.GetValueOrDefault().ToString("#,#"))); // # Of CHB Month To Date
					fields.Add(new ReportFieldInfo("Percent", (object item) => ((DailyStatusReportByTerminal)item).PercentOfVisaCHBMonthToDate.GetValueOrDefault().ToString("P2"))); // % of CHB Month To Date
					fields.Add(new ReportFieldInfo("Amount", (object item) => ((DailyStatusReportByTerminal)item).AmountOfVisaChbMonthToDate.GetValueOrDefault().ToString(currencyFormat))); // Amount of CHB Month To Date
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// mc
					fields.Add(new ReportFieldInfo("Count", (object item) => ((DailyStatusReportByTerminal)item).NumberOfMCCHBMonthToDate.GetValueOrDefault().ToString("#,#"))); // # Of CHB Month To Date
					fields.Add(new ReportFieldInfo("Percent", (object item) => ((DailyStatusReportByTerminal)item).PercentOfMCCHBMonthToDate.GetValueOrDefault().ToString("P2"))); // % of CHB Month To Date
					fields.Add(new ReportFieldInfo("Amount", (object item) => ((DailyStatusReportByTerminal)item).AmountOfMCChbMonthToDate.GetValueOrDefault().ToString(currencyFormat))); // Amount of CHB Month To Date
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// refunds
					fields.Add(new ReportFieldInfo("Count", (object item) => ((DailyStatusReportByTerminal)item).RefundsCount.GetValueOrDefault().ToString("#,#")));
					fields.Add(new ReportFieldInfo("Amount", (object item) => ((DailyStatusReportByTerminal)item).RefundsAmount.GetValueOrDefault().ToString(currencyFormat)));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					// sub total
					fields.Add(new ReportFieldInfo("Amount", (object item) => ((DailyStatusReportByTerminal)item).AmountBalance.GetValueOrDefault().ToString(currencyFormat)));
					break;
				case ReportType.PartnerCapturedTransactions:
					fields.Add(new ReportFieldInfo("Merchant", "Merchant.Name"));
					fields.Add(new ReportFieldInfo("Trans. Num.", "ID"));
					fields.Add(new ReportFieldInfo("Trans. Date", "InsertDate"));
					fields.Add(new ReportFieldInfo("Trans. Amount", (object item) => ((Transactions.Transaction)item).SignedAmount.ToString(currencyFormat)));
					fields.Add(new ReportFieldInfo("Trans. Currency", "CurrencyIsoCode"));
					fields.Add(new ReportFieldInfo("Trans. Credit Type", "CreditTypeText"));
					fields.Add(new ReportFieldInfo("Trans. Denied Status", "DeniedStatusText"));
					fields.Add(new ReportFieldInfo("Trans. Approval Num.", "ApprovalNumber"));
					fields.Add(new ReportFieldInfo("Payment Method", (object item) => ((Transactions.Transaction)item).PaymentMethodDisplay == null ? "" : ((Transactions.Transaction)item).PaymentMethodDisplay)); 
					fields.Add(new ReportFieldInfo("Payout", (object item) => ((Transactions.Transaction)item).IsInstallments ? "" : Enums.GetPaymentsStatus(((Transactions.Transaction)item).PaymentsIDs.Trim()).ToString()));
					fields.Add(new ReportFieldInfo("Installments", ReportHelper.GetInstallments));
					fields.Add(new ReportFieldInfo("Bank Transfer", "MoneyTransferText"));
					break;
				case ReportType.PartnerDeclinedTransactions:
					fields.Add(new ReportFieldInfo("Merchant", "Merchant.Name"));
					fields.Add(new ReportFieldInfo("Trans. Num.", "ID"));
					fields.Add(new ReportFieldInfo("Trans. Date", "InsertDate"));
					fields.Add(new ReportFieldInfo("Trans. Amount", (object item) => ((Transactions.Transaction)item).SignedAmount.ToString(currencyFormat)));
					fields.Add(new ReportFieldInfo("Trans. Currency", "CurrencyIsoCode"));
					fields.Add(new ReportFieldInfo("Trans. Credit Type", "CreditTypeText"));
					fields.Add(new ReportFieldInfo("Trans. Denied Status", "DeniedStatusText"));
					fields.Add(new ReportFieldInfo("Payment Method", (object item) => ((Transactions.Transaction)item).PaymentMethodDisplay == null ? "" : ((Transactions.Transaction)item).PaymentMethodDisplay)); 
					fields.Add(new ReportFieldInfo("Reply Code", "ReplyCode"));
					fields.Add(new ReportFieldInfo("Reply Description", "ReplyDescription.DescriptionCustomerEng"));
					break;
				case ReportType.MerchantChargebackTransactions:
					fields.Add(new ReportFieldInfo("Trans. Num.", "TransactionID"));
					fields.Add(new ReportFieldInfo("Trans. Date", "TransactionInsertDate"));
					fields.Add(new ReportFieldInfo("Trans. Amount", (object item) => ((DenormalizedTransactionHistoryVO)item).TransactionAmount.GetValueOrDefault().ToString(currencyFormat)));
					fields.Add(new ReportFieldInfo("Trans. Currency", "TransactionCurrencyIsoCode"));
					fields.Add(new ReportFieldInfo("Payment Method", (object item) => ((DenormalizedTransactionHistoryVO)item).TransactionPaymentMethodName == null ? "" : ((DenormalizedTransactionHistoryVO)item).TransactionPaymentMethodName));
					fields.Add(new ReportFieldInfo("Action Type", "HistoryTypeName"));
					fields.Add(new ReportFieldInfo("CHB Date", "HistoryInsertDate"));
					fields.Add(new ReportFieldInfo("CHB Code", "HistoryReferenceNumber"));
					fields.Add(new ReportFieldInfo("CHB Description", "HistoryDescription"));
					break;
				case ReportType.MerchantSummary:
					fields.Add(new ReportFieldInfo("Currency", "CurrencyName"));
					fields.Add(new ReportFieldInfo("Payment Method", "PaymentMethod"));
					fields.Add(new ReportFieldInfo("Total Fees", (object item) => ((MerchantSummaryVO)item).TotalFees.ToAmountFormat()));
					fields.Add(new ReportFieldInfo("Total Trans Amount", (object item) => ((MerchantSummaryVO)item).TotalTransAmount.ToAmountFormat()));
					fields.Add(new ReportFieldInfo("Estimated Payout", (object item) => ((MerchantSummaryVO)item).NetSalesAmount.ToAmountFormat()));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					fields.Add(new ReportFieldInfo("Captured", "SalesCount"));
					fields.Add(new ReportFieldInfo("Capture Amount", (object item) => ((MerchantSummaryVO)item).SalesAmount.ToAmountFormat()));
					fields.Add(new ReportFieldInfo("Refunds", "RefundsCount"));
					fields.Add(new ReportFieldInfo("Refunds Amount", (object item) => ((MerchantSummaryVO)item).RefundsAmount.ToAmountFormat()));
					fields.Add(new ReportFieldInfo("ChargeBacks", "ChargeBacksCount"));
					fields.Add(new ReportFieldInfo("ChargeBacks Amount", (object item) => ((MerchantSummaryVO)item).ChargeBacksAmount.ToAmountFormat()));
					fields.Add(new ReportFieldInfo("Declined Transactions", "DeclinedCount"));
					fields.Add(new ReportFieldInfo("&nbsp;", (object item) => "&nbsp;"));
					fields.Add(new ReportFieldInfo("Processing Fee", (object item) => ((MerchantSummaryVO)item).TotalApprovedFees.ToAmountFormat()));
					fields.Add(new ReportFieldInfo("ChargeBacks Fee", (object item) => ((MerchantSummaryVO)item).ChargeBacksFee.ToAmountFormat()));
					//fields.Add(new ReportFieldInfo("Refunds Fee", (object item) => ((MerchantSummaryVO)item).RefundsProcessingFee.ToAmountFormat()));
                    fields.Add(new ReportFieldInfo("Refunds Fee", (object item) => ((MerchantSummaryVO)item).RefundsTransactionsFee.ToAmountFormat()));
                    fields.Add(new ReportFieldInfo("Declined Transactions Fee", (object item) => ((MerchantSummaryVO)item).DeclinedTransactionsFee.ToAmountFormat()));
					break;
				case ReportType.MerchantDeclinedArchive:
					fields.Add(new ReportFieldInfo("Trans. Num.", "ID"));
					fields.Add(new ReportFieldInfo("Trans. Date", "InsertDate"));
					fields.Add(new ReportFieldInfo("Trans. Amount", (object item) => ((Transactions.Transaction)item).Amount.ToString(currencyFormat)));
					fields.Add(new ReportFieldInfo("Trans. Currency", "CurrencyIsoCode"));
					fields.Add(new ReportFieldInfo("Trans. Credit Type", "CreditTypeText"));
					fields.Add(new ReportFieldInfo("Trans. Order", "OrderNumber"));
					fields.Add(new ReportFieldInfo("Trans. Denied Status", "DeniedStatusText"));
					fields.Add(new ReportFieldInfo("Trans. Approva Num.", "ApprovalNumber"));
					fields.Add(new ReportFieldInfo("Payment Method", "PaymentMethodDisplay"));
					fields.Add(new ReportFieldInfo("Card Type", (object item) => ((Transactions.Transaction)item).RefPaymentMethod != null ? ((Transactions.Transaction)item).RefPaymentMethod.Name : null));
					fields.Add(new ReportFieldInfo("Reply Code", "ReplyCode"));
					fields.Add(new ReportFieldInfo("Reply Description", "ReplyCodeDescription"));
					fields.Add(new ReportFieldInfo("Comment", "Comment"));
					break;
				case ReportType.MerchantRecurringCharges:
					fields.Add(new ReportFieldInfo("Charge No.", "ChargeNumber"));
					fields.Add(new ReportFieldInfo("Series No.", "SeriesId"));
					fields.Add(new ReportFieldInfo("Type", (object item) => ((Transactions.Recurring.Charge)item).Series.IsPreAuthorized ? "Auth" : "Debit"));
					fields.Add(new ReportFieldInfo("Payment Method", (object item) => ((Transactions.Recurring.Charge)item).Payment?.MethodInstance.PaymentMethod.Name));
					fields.Add(new ReportFieldInfo("Charge Date", "Date"));
					fields.Add(new ReportFieldInfo("Currency", (object item) => Bll.Currency.GetIsoCode((CommonTypes.Currency)((Transactions.Recurring.Charge)item).Currency.GetValueOrDefault())));
					fields.Add(new ReportFieldInfo("Amount", "Amount"));
					break;
				case ReportType.MerchantRecurringSeries:
					fields.Add(new ReportFieldInfo("ID", "ID"));
					fields.Add(new ReportFieldInfo("Start Date", "StartDate"));
					fields.Add(new ReportFieldInfo("Type", (object item) => ((Transactions.Recurring.Series)item).IsPreAuthorized ? "Auth" : "Debit"));
					fields.Add(new ReportFieldInfo("Payment Method", (object item) => ((Transactions.Recurring.Series)item).Payment?.MethodInstance.PaymentMethod.Name));
					fields.Add(new ReportFieldInfo("Amount", "Amount"));
					fields.Add(new ReportFieldInfo("Currency", (object item) => Bll.Currency.GetIsoCode((CommonTypes.Currency)((Transactions.Recurring.Series)item).Currency.GetValueOrDefault())));
                    fields.Add(new ReportFieldInfo("Charges", "Charges"));
					fields.Add(new ReportFieldInfo("Flexible", "IsFlexible"));
					fields.Add(new ReportFieldInfo("Charge Interval", "IntervalUnit"));
					fields.Add(new ReportFieldInfo("Interval Length", "IntervalCount"));
					fields.Add(new ReportFieldInfo("Status", "Status"));
					break;
				case ReportType.MerchantCapturedTransactions:
				case ReportType.MerchantDeclinedTransactions:
				case ReportType.MerchantCapturedArchive:
				case ReportType.MerchantPendingTransactions:
                    fields.Add(new ReportFieldInfo("Trans. Num.", "ID"));
					fields.Add(new ReportFieldInfo("Trans. Date", "InsertDate"));
					fields.Add(new ReportFieldInfo("Trans. Currency", "CurrencyIsoCode"));
					fields.Add(new ReportFieldInfo("Trans. Amount", (object item) => ((Transactions.Transaction)item).SignedAmount.ToString(currencyFormat)));
					fields.Add(new ReportFieldInfo("Trans. Fee", (object item) => ((Transactions.Transaction)item).TransactionFee.ToString(currencyFormat)));
					fields.Add(new ReportFieldInfo("Processing Fee", (object item) => ((Transactions.Transaction)item).RatioFee.ToString(currencyFormat)));
					fields.Add(new ReportFieldInfo("Chargeback Fee", (object item) => ((Transactions.Transaction)item).ChargebackFee.ToString(currencyFormat)));
					fields.Add(new ReportFieldInfo("Retrieval Fee", (object item) => ((Transactions.Transaction)item).ClarificationFee.ToString(currencyFormat)));
					fields.Add(new ReportFieldInfo("Handling Fee", (object item) => ((Transactions.Transaction)item).HandlingFee.ToString(currencyFormat)));
					fields.Add(new ReportFieldInfo("Trans. Credit Type", "CreditTypeText"));
					fields.Add(new ReportFieldInfo("Trans. Order", "OrderNumber"));
					if (reportType == ReportType.MerchantCapturedTransactions) 
						fields.Add(new ReportFieldInfo("Trans. Denied Status", "DeniedStatusText"));
					fields.Add(new ReportFieldInfo("Trans. Approval Num.", "ApprovalNumber"));
					fields.Add(new ReportFieldInfo("Payment Method", (object item) => ((Transactions.Transaction)item).PaymentData == null ? "" : ((Transactions.Transaction)item).PaymentData.MethodInstance.PaymentMethod.Name));
					//fields.Add(new ReportFieldInfo("Payment Method", (object item) => ((Transaction.Transaction)item).PaymentMethodDisplay == null ? "" : ((TransactionVO)item).PaymentMethodDisplay)); 
					fields.Add(new ReportFieldInfo("Card Last 4 Digits", (object item) => ((Transactions.Transaction)item).PaymentData == null ? "" : ((Transactions.Transaction)item).PaymentData.MethodInstance.Value1Last4));
					fields.Add(new ReportFieldInfo("Card Country", "PaymentData.IssuerCountryIsoCode"));
					fields.Add(new ReportFieldInfo("Card Month Exp.", (object item) => ((Transactions.Transaction)item).PaymentData == null ? "" : ((Transactions.Transaction)item).PaymentData.MethodInstance.ExpirationMonth.ToString()));
					fields.Add(new ReportFieldInfo("Card Year Exp.",  (object item) => ((Transactions.Transaction)item).PaymentData == null ? "" : ((Transactions.Transaction)item).PaymentData.MethodInstance.ExpirationYear.ToString()));
					fields.Add(new ReportFieldInfo("Card Bin", (object item) => ((Transactions.Transaction)item).PaymentData == null ? "" : ((Transactions.Transaction)item).PaymentData.MethodInstance.Value1First6.ToString()));
					fields.Add(new ReportFieldInfo("Cardholder Name", "PayerData.FullName"));
					fields.Add(new ReportFieldInfo("Cardholder Phone", "PayerData.PhoneNumber"));
					fields.Add(new ReportFieldInfo("Cardholder Email", "PayerData.EmailAddress"));
					fields.Add(new ReportFieldInfo("Cardholder ID Num.", "PayerData.PersonalNumber"));
					fields.Add(new ReportFieldInfo("Billing Address 1", "PaymentData.BillingAddress.AddressLine1"));
					fields.Add(new ReportFieldInfo("Billing Address 2", "PaymentData.BillingAddress.AddressLine2"));
					fields.Add(new ReportFieldInfo("Billing City", "PaymentData.BillingAddress.City"));
					fields.Add(new ReportFieldInfo("Billing Zip Code", "PaymentData.BillingAddress.PostalCode"));
					fields.Add(new ReportFieldInfo("Billing State", ReportHelper.GetStateName));
					fields.Add(new ReportFieldInfo("Billing Country", ReportHelper.GetCountryName));
                    fields.Add(new ReportFieldInfo("Settlment", "PrimaryPaymentID"));
					if (reportType == ReportType.MerchantCapturedTransactions)
					{
						fields.Add(new ReportFieldInfo("Payout", (object item) => ((Transactions.Transaction)item).IsInstallments ? "" : Enums.GetPaymentsStatus(((Transactions.Transaction)item).PaymentsIDs.Trim()).ToString()));
						fields.Add(new ReportFieldInfo("Installments", ReportHelper.GetInstallments));
					}
					else if (reportType == ReportType.MerchantDeclinedTransactions || reportType == ReportType.MerchantDeclinedArchive)
					{
						fields.Add(new ReportFieldInfo("Reply Code", "ReplyCode"));
						fields.Add(new ReportFieldInfo("Reply Description", "ReplyDescription.DescriptionCustomerEng"));
					}
					fields.Add(new ReportFieldInfo("Comment", "Comment"));
					if (user != null && reportType == ReportType.MerchantCapturedTransactions) 
					{
						if (Login.Current.Role == UserRole.Merchant && Merchants.Merchant.Current.PayPercent < 100)
							fields.Add(new ReportFieldInfo("Bank Transfer", "MoneyTransferText"));
					}
					break;
                case ReportType.MerchantCustomerTransactionsReport:
                    fields.Add(new ReportFieldInfo("Trans. Num.", "ID"));
                    fields.Add(new ReportFieldInfo("Trans. Date", "InsertDate"));
                    fields.Add(new ReportFieldInfo("Trans. Currency", "CurrencyIsoCode"));
					fields.Add(new ReportFieldInfo("Trans. Amount", (object item) => ((Transactions.Transaction)item).SignedAmount.ToString(currencyFormat)));
                    fields.Add(new ReportFieldInfo("Trans. Credit Type", "CreditTypeText"));
					fields.Add(new ReportFieldInfo("Payment Method", (object item) => ((Transactions.Transaction)item).PaymentMethodName));
                    fields.Add(new ReportFieldInfo("Cardholder Name", (object item) => ((Transactions.Transaction)item).PayerData.FullName));
                    fields.Add(new ReportFieldInfo("Cardholder Phone", (object item) => ((Transactions.Transaction)item).PayerData.PhoneNumber));
                    fields.Add(new ReportFieldInfo("Cardholder Email", (object item) => ((Transactions.Transaction)item).PayerData.EmailAddress));
                    fields.Add(new ReportFieldInfo("Pay For", "PayForText"));
                    fields.Add(new ReportFieldInfo("Comment", "Comment"));
                    break;
				case ReportType.MerchantRefundRequests:
					fields.Add(new ReportFieldInfo("Req. ID", "ID"));
                    fields.Add(new ReportFieldInfo("Trans. ID", "TransactionID"));
					fields.Add(new ReportFieldInfo("Req. Amount", "RequestAmount"));
					fields.Add(new ReportFieldInfo("Req. Currency", "CurrencyIso"));
					fields.Add(new ReportFieldInfo("Req. Comment", "Comment"));
                    fields.Add(new ReportFieldInfo("Req. Status", "Status"));
					fields.Add(new ReportFieldInfo("Req. Date", "Date"));
					fields.Add(new ReportFieldInfo("Ref. Amount", "RefundAmount"));
					break;

			}

			return fields;
		}

		public static string GetStateName(object item)
		{
			var transaction = (Transactions.Transaction)item;
			if (transaction.PaymentData == null || transaction.PaymentData.BillingAddress == null)
				return "";

			State state = State.Get(transaction.PaymentData.BillingAddress.StateISOCode);
			if (state == null)
				return "";

			return state.Name;
		}

		public static string GetCountryName(object item)
		{
			var transaction = (Transactions.Transaction)item;
			if (transaction.PaymentData == null || transaction.PaymentData.BillingAddress == null)
				return "";

			Country country = Country.Get(transaction.PaymentData.BillingAddress.CountryISOCode);
			if (country == null)
				return "";

			return country.Name;
		}

		public static string GetInstallments(object item)
		{
			var transaction = (Transactions.Transaction)item;
			if (!transaction.IsInstallments)
				return "";

			StringBuilder installmentsBuilder = new StringBuilder();
			foreach (var currentInstallment in transaction.InstallmentList)
			{
				installmentsBuilder.Append("<table>");
				installmentsBuilder.Append("<tr>");
				installmentsBuilder.Append("<td></td>");
				installmentsBuilder.Append("<td>");
				installmentsBuilder.Append("Installment");
				installmentsBuilder.Append(": ");
				installmentsBuilder.Append(currentInstallment.Comment);
				installmentsBuilder.Append("</td>");
				installmentsBuilder.Append("<td>");
				installmentsBuilder.Append(currentInstallment.Amount.ToAmountFormat(transaction.CurrencyID));
				installmentsBuilder.Append("</td>");

				if (currentInstallment.SettlementID > 0)
					installmentsBuilder.Append(string.Format("<td>Settled ({0})</td>", currentInstallment.SettlementID, transaction.CurrencyID));
				else
					installmentsBuilder.Append("<td>Unsettled</td>");

				installmentsBuilder.Append("</tr>");
				installmentsBuilder.Append("</table>");
			}

			return installmentsBuilder.ToString();
		}

		public static string GetBankPayoutText(Transactions.Transaction transaction)
		{
			string output = "";
			if (transaction.EpaList == null) return "No Records";
			if (transaction.EpaList.Count == 0) return "No Records";
			if (transaction.Installments == 1)
			{
				if (transaction.EpaList[0].IsPaid && transaction.EpaList[0].IsRefunded)
				{
					output = "Paid (" + transaction.EpaList[0].PaidInsertDate + ") And Refunded (" + transaction.EpaList[0].RefundedInsertDate + ")";
				}
				else if (transaction.EpaList[0].IsPaid)
				{
					output = "Paid (" + transaction.EpaList[0].PaidInsertDate + ")";
				}
				else if (transaction.EpaList[0].IsRefunded)
				{
					output = "Refunded (" + transaction.EpaList[0].RefundedInsertDate + ")";
				}
			}
			else
			{
				string temp = null;
				foreach (var record in transaction.EpaList)
				{
					if (record.IsPaid && record.IsRefunded)
					{
						temp = "Paid And Refunded";
					}
					else if (record.IsPaid)
					{
						temp = "Paid (" + record.PaidInsertDate + ")";
					}
					else if (record.IsRefunded)
					{
						temp = "Refunded (" + record.RefundedInsertDate + ")";
					}
					if (!string.IsNullOrEmpty(temp)) output += ";" + temp;
				}
			}

			return output;
		}

		public static string GetFormattedPercentage(double a, double b)
		{
			return (Netpay.Infrastructure.Math.GetPercentage(a, b) / 100).ToString("P");
		}

		public static string GetFormattedPercentage(decimal a, decimal b)
		{
			return (Netpay.Infrastructure.Math.GetPercentage(a, b) / 100).ToString("P");
		}

		public static string GetFormattedPercentage(int a, int b, decimal threshold)
		{
			return GetFormattedPercentage(Convert.ToDecimal(a), Convert.ToDecimal(b), threshold);
		}

		public static string GetFormattedPercentage(double a, double b, double threshold)
		{
			double percentage = Netpay.Infrastructure.Math.GetPercentage(a, b);
			if (percentage >= threshold)
				return "<span class=\"highRisk\">" + (percentage / 100).ToString("P") + "</span>";

			return (percentage / 100).ToString("P");
		}

		public static string GetFormattedPercentage(decimal a, decimal b, decimal threshold)
		{
			decimal percentage = Netpay.Infrastructure.Math.GetPercentage(a, b);
			if (percentage >= threshold)
				return "<span class=\"highRisk\">" + (percentage / 100).ToString("P") + "</span>";

			return (percentage / 100).ToString("P");
		}

		public static string GetFormatted(int value, decimal threshold, string format)
		{
			return GetFormatted(Convert.ToDecimal(value), threshold, format);
		}

		public static string GetFormatted(double value, double threshold, string format)
		{
			if (value >= threshold)
				return "<span class=\"highRisk\">" + value.ToString(format) + "</span>";

			return value.ToString(format);
		}

		public static string GetFormatted(decimal value, decimal threshold, string format)
		{
			if (value >= threshold)
				return "<span class=\"highRisk\">" + value.ToString(format) + "</span>";

			return value.ToString(format);
		}

		public static string GetFormattedDifference(int yesterday, double dailyAverage)
		{
			return (Netpay.Infrastructure.Math.GetPercentageDifference(yesterday, dailyAverage) / 100).ToString("P");
		}

		public static string GetFormattedDifference(decimal yesterday, decimal dailyAverage)
		{
			return (Netpay.Infrastructure.Math.GetPercentageDifference(yesterday, dailyAverage) / 100).ToString("P");
		}

		public static string GetFormattedDifference(int yesterday, double dailyAverage, double threshold)
		{
			double difference = Netpay.Infrastructure.Math.GetPercentageDifference((double)yesterday, dailyAverage);
			if (difference >= threshold)
				return "<span class=\"highRisk\">" + (difference / 100).ToString("P") + "</span>";

			return (difference / 100).ToString("P");
		}

		public static string GetFormattedDifference(decimal yesterday, decimal dailyAverage, decimal threshold)
		{
			decimal difference = Netpay.Infrastructure.Math.GetPercentageDifference(yesterday, dailyAverage);
			if (difference >= threshold)
				return "<span class=\"highRisk\">" + (difference / 100).ToString("P") + "</span>";

			return (difference / 100).ToString("P");
		}

		public static int ThisMonthTotalDays
		{
			get
			{
				int daysInThisMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1).Day;
				return daysInThisMonth;
			}
		}

		public static int ThisMontPassedhDays
		{
			get
			{
				int daysPassed = DateTime.Now.Day - 1;
				return daysPassed;
			}
		}

		public static double ThisMonthAveragePerDay(double? thisMonthCount)
		{
			if (thisMonthCount == null || thisMonthCount == 0)
				return 0;
				
			double averagePerDay = thisMonthCount.Value / (double)ThisMontPassedhDays;
			return averagePerDay;
		}

		public static double ThisMonthProjectedTotal(double? thisMonthCount)
		{
			if (thisMonthCount == null || thisMonthCount == 0)
				return 0;

			double projectedTotal = ThisMonthAveragePerDay(thisMonthCount) * ThisMonthTotalDays;
			return projectedTotal;
		}
	}
    */
}
