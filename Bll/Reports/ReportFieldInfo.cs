﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.Reports
{
	public class ReportFieldInfo
	{
		public ReportFieldInfo(){}
		public ReportFieldInfo(string title, string property)
		{
			Title = title;
			Property = property;
		}

		public ReportFieldInfo(string title, Func<object, string> method)
		{
			Title = title;
			Method = method;
		}

        public string Title { get; set; }
        public string Property { get; set; }
        public Func<object, string> Method { get; set; }
	}
}
