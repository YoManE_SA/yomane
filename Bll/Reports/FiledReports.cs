﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Netpay.Infrastructure.Tasks;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Security;
using System.Collections;
using Netpay.Infrastructure;
using Netpay.Dal.Reports;
using Netpay.CommonTypes;
using Netpay.Bll.OldVO;
using Netpay.Bll.Reports.VO;
using Netpay.Bll.Accounts;

namespace Netpay.Bll.Reports
{
    /// <summary>
    /// Manages reports that are cached as files.
    /// </summary>
    public static class FiledReports
    {

        private static string GetReportPath(ReportType reportType)
        {
            string path = null;
            if (Login.Current == null || Login.Current.UserName == Domain.Current.ServiceUser)
                path = Domain.Current.MapPrivateDataPath("Reports");
            else
                path = (Login.Current.IsInRole(UserRole.Admin) ? AdminUser.Current.MapPrivatePath("Reports") : Account.Current.MapPrivatePath("Reports"));
            path = System.IO.Path.Combine(path, reportType.ToString()) + "\\";
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            return path;
        }

        private static string GetAdminListExportPath()
        {
            string path = null;
            if (Login.Current == null || Login.Current.UserName == Domain.Current.ServiceUser)
                path = Domain.Current.MapPrivateDataPath("AdminListExports");
            else
                path = (Login.Current.IsInRole(UserRole.Admin) ? AdminUser.Current.MapPrivatePath("AdminListExports") : Account.Current.MapPrivatePath("AdminListExports"));
            path = path + "\\";
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            return path;
        }

        public static FileInfo GetReportFile(ReportType reportType, string fileName)
        {
            return new FileInfo(GetReportFullName(reportType, fileName));
        }

        public static string GetReportFullName(ReportType reportType, string fileName)
        {
            return GetReportPath(reportType) + fileName;
        }

        public static string GetAdminListExportFullName(string fileName)
        {
            return GetAdminListExportPath() + fileName;
        }



        public static void DeleteReport(ReportType reportType, string reportFileName)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Account }, Module.SecuredObject, PermissionValue.Delete);
            string reportFileFullName = GetReportFullName(reportType, reportFileName);
            if (System.IO.File.Exists(reportFileFullName))
                System.IO.File.Delete(reportFileFullName);
        }

        public static void DeleteExportedCsv(string fileName)
        {
            string exportedFileFullName = GetAdminListExportFullName(fileName);
            if (System.IO.File.Exists(exportedFileFullName))
                System.IO.File.Delete(exportedFileFullName);
        }

        public static void CancelReport(Guid taskID)
        {
            Netpay.Infrastructure.Application.Queue.CancelTask(Task.GetUserGroupID(Login.Current), taskID);
        }

        public static List<FileInfo> GetGeneratedReports(ReportType reportType)
        {
            List<FileInfo> fileInfos = new List<FileInfo>();
            string[] filePaths = Directory.GetFiles(GetReportPath(reportType));
            foreach (string currentFilePath in filePaths)
            {
                FileInfo currentFile = new FileInfo(currentFilePath);
                if (!currentFile.IsLocked())
                    fileInfos.Add(new FileInfo(currentFilePath));
            }

            return fileInfos.OrderByDescending(f => f.CreationTime).ToList<FileInfo>();
        }

        public static List<GeneratedReportInfo> GetGeneratedAdminListExports()
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Module.SecuredObject, PermissionValue.Read);
            List<GeneratedReportInfo> fileInfos = new List<GeneratedReportInfo>();

            var files = Directory.GetFiles(GetAdminListExportPath());
            foreach (string currentFilePath in files)
            {
                FileInfo currentFile = new FileInfo(currentFilePath);
                if (!currentFile.IsLocked())
                    fileInfos.Add(new GeneratedReportInfo(currentFile, ReportType.ExportFromAdminList));
            }

            return fileInfos.OrderByDescending(f => f.ReportFile.CreationTime).ToList<GeneratedReportInfo>();
        }

        public static List<GeneratedReportInfo> GetGeneratedReports()
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant, UserRole.Partner }, Module.SecuredObject, PermissionValue.Read);

            ReportType[] reportTypes;
            switch (Login.Current.Role)
            {
                //case UserRole.Unknown:
                //	throw new ApplicationException("Invalid user type");
                case UserRole.Customer:
                    throw new ApplicationException("Invalid user type");
                case UserRole.Merchant:
                case UserRole.MerchantSubUser:
                    reportTypes = new ReportType[] { ReportType.MerchantCapturedArchive, ReportType.MerchantCapturedTransactions, ReportType.MerchantDeclinedArchive, ReportType.MerchantDeclinedTransactions, ReportType.MerchantChargebackTransactions, ReportType.MerchantSummary, ReportType.MerchantPendingTransactions, ReportType.MerchantRecurringCharges, ReportType.MerchantRecurringSeries, ReportType.MerchantSettlmentReport, ReportType.MerchantCustomerTransactionsReport };
                    break;
                case UserRole.Admin:
                    reportTypes = new ReportType[] { ReportType.AdminDailyRiskByMerchant, ReportType.AdminDailyRiskByTerminal, ReportType.AdminDailyStatusByMerchant, ReportType.AdminDailyStatusByTerminal, ReportType.AdminFailStats, ReportType.AdminRollingReserves, ReportType.AdminTransactionsReport };
                    break;
                case UserRole.Partner:
                    reportTypes = new ReportType[] { ReportType.PartnerCapturedTransactions, ReportType.PartnerDeclinedTransactions };
                    break;
                default:
                    throw new ApplicationException("Invalid user type");
            }

            List<GeneratedReportInfo> fileInfos = new List<GeneratedReportInfo>();
            foreach (ReportType currentReportType in reportTypes)
            {
                string[] filePaths = Directory.GetFiles(GetReportPath(currentReportType));
                foreach (string currentFilePath in filePaths)
                {
                    FileInfo currentFile = new FileInfo(currentFilePath);
                    if (!currentFile.IsLocked())
                        fileInfos.Add(new GeneratedReportInfo(currentFile, currentReportType));
                }
            }

            return fileInfos.OrderByDescending(f => f.ReportFile.CreationTime).ToList<GeneratedReportInfo>();
        }

        public static TaskInfo GetActiveReport()
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant, UserRole.Partner }, Module.SecuredObject, PermissionValue.Read);
            if (Netpay.Infrastructure.Application.Queue.ActiveTask.GroupID == Task.GetUserGroupID(Login.Current))
            {
                Task activeTask = Netpay.Infrastructure.Application.Queue.ActiveTask;
                TaskInfo vo = new TaskInfo();
                vo.TaskID = activeTask.TaskID;
                vo.OwnerID = activeTask.GroupID;
                vo.Description = activeTask.Description;
                vo.InsertDate = activeTask.CreationDate;

                return vo;
            }

            return null;
        }

        public static List<TaskInfo> GetQueuedReports()
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant, UserRole.Partner }, Module.SecuredObject, PermissionValue.Read);

            if (Login.Current == null)
            {
                throw new Exception("Inside GetQueuedReports(), Login.Current is null");
            }

            var pGetUserGroupID = Task.GetUserGroupID(Login.Current);

            if (Netpay.Infrastructure.Application.Queue == null)
                return new List<TaskInfo>();

            var pGetTasks = Netpay.Infrastructure.Application.Queue.GetTasks(pGetUserGroupID);

            var pWhere = pGetTasks.Where(t => t.TypeName == typeof(FiledReports).ToString());

            var pSelect = pWhere.Select(t => new TaskInfo(t));

            List<TaskInfo> queuedTasks = pSelect.ToList<TaskInfo>();

            return queuedTasks;
        }

        public static List<TaskInfo> GetQueuedAdminListExports()
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant, UserRole.Partner }, Module.SecuredObject, PermissionValue.Read);

            if (Netpay.Infrastructure.Application.Queue == null)
                return new List<TaskInfo>();

            List<TaskInfo> queuedTasks = Netpay.Infrastructure.Application.Queue.GetTasks(Task.GetUserGroupID(Login.Current)).Where(t => t.MethodName == "ExportFromAdminList").Select(t => new TaskInfo(t)).ToList<TaskInfo>();

            return queuedTasks;
        }

        public static void ExportTask(string comment, ReportType reportType, ICollection data)
        {
            ExportTask(null, comment, null, reportType, new ICollection[] { data });
        }

        public static void ExportTask(string title, string comment, ReportType reportType, ICollection data)
        {
            ExportTask(title, comment, null, reportType, new ICollection[] { data });
        }

        public static void ExportTask(string title, string comment, string fileName, ReportType reportType, ICollection data)
        {
            ExportTask(title, comment, fileName, reportType, new ICollection[] { data });
        }

        public static void ExportTask(string title, string comment, string fileName, ReportType reportType, ICollection[] dataGroups)
        {
            if (dataGroups.Sum(d => d.Count) > Infrastructure.Application.MaxFiledReportSize)
                throw new ApplicationException("Report size over limit.");

            if (fileName == null)
                fileName = reportType.ToString();
            string reportFileFullName = Domain.GetUniqueFileName(string.Format("{0}{1}.xlsx", GetReportPath(reportType), fileName));

            ReportGeneratorBase generator = new ReportGeneratorExcel();
            List<ReportGeneratorBase.FieldInfo> fields = generator.GetFields(Login.Current, reportType);
            byte[] generated = generator.Generate(fields, dataGroups, title, comment);

            System.IO.File.WriteAllBytes(reportFileFullName, generated);
        }

        public static void ExportFromAdminList(object currentSearchFilters, System.Reflection.MethodInfo searchMethod, Type currentItemType, string listName)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Module.SecuredObject, PermissionValue.Read);

            if (currentSearchFilters == null) throw new Exception("ExportFromAdminList error - currenctSearchFilters parameter is null.");
            if (searchMethod == null) throw new Exception("ExportFromAdminList error - searchMethod is null.");

            IList items = (searchMethod.Invoke(null, new Object[] { currentSearchFilters, /* Limit number of results to 1000 */ new SortAndPage(0, 1000) }) as IList);

            if (items == null || items.Count == 0)
            {
                throw new Exception("No items found to be exported.");
            }
            else //Create csv file the will be presented at Admin - Logs - Exported Reports
            {
                //get the full file name path
                string exportedFileFullName = Domain.GetUniqueFileName(string.Format("{0}{1}.csv", GetAdminListExportPath(), listName));

                //Get the properties list
                System.Reflection.PropertyInfo[] pi = currentItemType.GetProperties();
                string csv = items.ToCsvString(pi);

                System.IO.File.WriteAllText(exportedFileFullName, csv);
            }
        }

        public static void ExportTask(string comment, ReportType reportType, Func<ISortAndPage, IEnumerable> dataFunction)
        {
            var sortAndPage = new SortAndPage(0, 1, null, false) { };
            dataFunction.Invoke(sortAndPage);
            sortAndPage.PageSize = sortAndPage.RowCount;
            ICollection data = dataFunction.Invoke(sortAndPage) as ICollection;
            ExportTask(null, comment, null, reportType, data);
        }



        /*
		public static void ExportTask(string comment, ReportType reportType, Func<ISortAndPage, IEnumerable> dataFunction)
		{
			ExportTask(null, comment, null, reportType, dataFunction);
		}

		public static void ExportTask(string comment, string fileName, ReportType reportType, Func<ISortAndPage, IEnumerable> dataFunction)
		{
			ExportTask(null, comment, fileName, reportType, dataFunction);
		}

		public static void ExportTask(string title, string comment, string fileName, ReportType reportType, Func<ISortAndPage, IEnumerable> dataFunction)
		{
			if (fileName == null)
				fileName = reportType.ToString();
            string reportFileFullName = Domain.GetUniqueFileName(string.Format("{0}{1}.xlsx", GetReportPath(reportType), fileName));

			var sortAndPage = new SortAndPage(0, 500, null, false); // max is 2100
			List<ReportFieldInfo> fields = ReportHelper.GetFields(Login.Current, reportType);
			StreamWriter writer = System.IO.File.AppendText(reportFileFullName);
			try
			{
				writer.Write(ReportHelper.CreateExcelHeader(fields, title, comment));
				//writer.Write(ReportHelper.CreateFieldsHeaders(fields, false));
				ICollection data = (ICollection)dataFunction.Invoke(sortAndPage);
                if (data.Count > Infrastructure.Application.MaxFiledReportSize)
					throw new ApplicationException("Report size over limit.");
				while (data.Count > 0)
				{
					writer.Write(ReportHelper.CreateExcelBody(data, fields));
					sortAndPage.PageCurrent++;
					data = (ICollection)dataFunction.Invoke(sortAndPage);
				}
				writer.Write(ReportHelper.CreateExcelFooter());
			}
			catch (Exception ex)
			{
				Logger.Log(ex);
				throw ex;
			}
			finally
			{
				writer.Flush();
				writer.Close();
			}
		}
    */

        public static void CreatePartnerCapturedTransactionsReport(Transactions.Transaction.SearchFilters filters)
        {
            var data = Transactions.Transaction.Search(new Transactions.Transaction.LoadOptions(TransactionStatus.Captured, null, true, true), filters);
            ExportTask(null, ReportType.PartnerCapturedTransactions, data);
        }

        public static void CreatePartnerDeclinedTransactionsReport(Transactions.Transaction.SearchFilters filters)
        {
            var data = Transactions.Transaction.Search(new Transactions.Transaction.LoadOptions(TransactionStatus.Declined, null, true, true), filters);
            ExportTask(null, ReportType.PartnerDeclinedTransactions, data);
        }

        public static bool IsOverLimit(Func<ISortAndPage, IEnumerable> dataFunction)
        {
            var sortAndPage = new SortAndPage(0, 1, null, false) { };
            dataFunction.Invoke(sortAndPage);
            return sortAndPage.RowCount > Infrastructure.Application.MaxFiledReportSize;
        }

        public static void CreateMerchantCustomerTransactionsReport(Transactions.Transaction.SearchFilters filters)
        {
            string fileName = "Customers_Transactions_" + filters.date.From.Value.ToString("dd-MM-yyyy") + "_" + filters.date.To.Value.ToString("dd-MM-yyyy");
            var data = Transactions.Transaction.Search(new Transactions.Transaction.LoadOptions(TransactionStatus.Captured, null, true, false), filters);

            string strReportTitle = "Customers Transactions";
            strReportTitle += Environment.NewLine + Merchants.Merchant.GetMerchantNameByMerchantId(filters.merchantIDs[0]);
            strReportTitle += Environment.NewLine + filters.date.From.Value.ToShortDateString() + " - " + filters.date.To.Value.ToShortDateString();

            ExportTask(strReportTitle, null, fileName, ReportType.MerchantCustomerTransactionsReport, data);
        }

        public static void CreateMerchantCapturedTransactionsReport(Transactions.Transaction.SearchFilters filters)
        {
            string fileName = "Approved_Transactions_" + filters.date.From.Value.ToString("dd-MM-yyyy") + "_" + filters.date.To.Value.ToString("dd-MM-yyyy");
            var data = Transactions.Transaction.Search(new Transactions.Transaction.LoadOptions(TransactionStatus.Captured, null, true, false), filters);

            // SN/LG: 2018-03-01 - Hijack to show only values bigger than 0
            data = (from p in data where p.SignedAmount > 0 select p).ToList();

            string strReportTitle = "Approved Transactions";
            strReportTitle += Environment.NewLine + Merchants.Merchant.GetMerchantNameByMerchantId(filters.merchantIDs[0]);
            strReportTitle += Environment.NewLine + filters.date.From.Value.ToShortDateString() + " - " + filters.date.To.Value.ToShortDateString();

            ExportTask(strReportTitle, null, fileName, ReportType.MerchantCapturedTransactions, data);
        }

        public static void CreateMerchantDeclinedTransactionsReport(Transactions.Transaction.SearchFilters filters)
        {
            string fileNameAppend = "Declined_Transactions_" + filters.date.From.Value.ToString("dd-MM-yyyy") + "_" + filters.date.To.Value.ToString("dd-MM-yyyy");
            var data = Transactions.Transaction.Search(new Transactions.Transaction.LoadOptions(TransactionStatus.Declined, null, true, false), filters);

            string strReportTitle = "Declined Transactions";
            strReportTitle += Environment.NewLine + Merchants.Merchant.GetMerchantNameByMerchantId(filters.merchantIDs[0]);
            strReportTitle += Environment.NewLine + filters.date.From.Value.ToShortDateString() + " - " + filters.date.To.Value.ToShortDateString();

            ExportTask(strReportTitle, null, fileNameAppend, ReportType.MerchantDeclinedTransactions, data);
        }

        /*
        public static void CreateMerchantChargebackTransactionsReport(SearchFilters filters)
        {
            //filters.transactionHistoryTypes = new List<TransactionHistoryType>() { TransactionHistoryType.Chargeback };
            //SearchFilters siblingFilters = new SearchFilters() { transactionHistoryTypes = new List<TransactionHistoryType>() { TransactionHistoryType.Chargeback, TransactionHistoryType.RetrievalRequest } };
            //List<DenormalizedTransactionHistoryVO> results = Denormalized.SearchHistory(filters, siblingFilters);

            filters.isChargeback = true;
            string fileNameAppend = "Chargeback_Transactions_" + filters.dateFrom.Value.ToString("dd-MM-yyyy") + "_" + filters.dateTo.Value.ToString("dd-MM-yyyy");
            ExportTask("Chargeback Transactions", filters.ToFriendlyString(), fileNameAppend, ReportType.MerchantChargebackTransactions, (creds, pi) => Transactions.Search(TransactionStatus.Captured, filters, pi, null, true, false));
            //ExportTask("Chargeback Transactions", filters.ToFriendlyString(), fileNameAppend, ReportType.MerchantChargebackTransactions, results);
        }
        */

        public static void CreateMerchantChargebackTransactionsReport(Transactions.History.SearchFilters filters)
        {
            filters.HistoryTypes = new List<TransactionHistoryType>() { TransactionHistoryType.Chargeback };
            List<DenormalizedTransactionHistoryVO> results = Denormalized.SearchHistory(filters);
            string fileNameAppend = "Chargeback_Transactions_" + filters.Date.From.Value.ToString("dd-MM-yyyy") + "_" + filters.Date.To.Value.ToString("dd-MM-yyyy");

            string strReportTitle = "Chargeback Transactions";
            strReportTitle += Environment.NewLine + Merchants.Merchant.GetMerchantNameByMerchantId(filters.MerchantIDs[0]);
            strReportTitle += Environment.NewLine + filters.Date.From.Value.ToShortDateString() + " - " + filters.Date.To.Value.ToShortDateString();

            ExportTask(strReportTitle, null, fileNameAppend, ReportType.MerchantChargebackTransactions, results);
        }

        public static void CreateMerchantSummaryReport(Transactions.Transaction.SearchFilters filters)
        {
            int? merchantId = null;
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant, UserRole.Partner }, Module.SecuredObject, PermissionValue.Read);
            AccountFilter.Current.Validate(Accounts.AccountType.Merchant, ref merchantId);
            List<MerchantSummaryVO> results = (from rsm in DataContext.Reader.ReportGetSummaryMerchant(filters.date.From, filters.date.To, (int?)merchantId) select new MerchantSummaryVO(rsm)).ToList<MerchantSummaryVO>();

            string fileName = "Processing_Summary_" + filters.date.From.Value.ToString("dd-MM-yyyy") + "_" + filters.date.To.Value.ToString("dd-MM-yyyy");

            string strReportTitle = "Processing Summary";
            strReportTitle += Environment.NewLine + Merchants.Merchant.GetMerchantNameByMerchantId(merchantId.Value);
            strReportTitle += Environment.NewLine + filters.date.From.Value.ToShortDateString() + " - " + filters.date.To.Value.ToShortDateString();

            ExportTask(strReportTitle, null, fileName, ReportType.MerchantSummary, results);
        }

        public static void CreateMerchantSettlementReport(Transactions.Transaction.SearchFilters filters)
        {
            int? merchantId = null;
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant, UserRole.Partner }, Module.SecuredObject, PermissionValue.Read);
            AccountFilter.Current.Validate(Accounts.AccountType.Merchant, ref merchantId);
            List<Settlements.Settlement> results = (from s in DataContext.Reader.tblTransactionPays where s.CompanyID == merchantId.Value && s.PayDate >= filters.date.From.Value.MinTime() && s.PayDate <= filters.date.To.Value.MaxTime() orderby s.Currency, s.PayDate select new Settlements.Settlement(s)).ToList();
            List<Settlements.Settlement>[] grouped = (from r in results group r by r.CurrencyID into g select g.ToList()).ToArray();

            string fileName = "Settlement_Report_" + filters.date.From.Value.ToString("dd-MM-yyyy") + "_" + filters.date.To.Value.ToString("dd-MM-yyyy");

            string strReportTitle = "Settlement Report";
            strReportTitle += Environment.NewLine + Merchants.Merchant.GetMerchantNameByMerchantId(merchantId.Value);
            strReportTitle += Environment.NewLine + filters.date.From.Value.ToShortDateString() + " - " + filters.date.To.Value.ToShortDateString();

            ExportTask(strReportTitle, null, fileName, ReportType.MerchantSettlmentReport, grouped);
        }

        public static void CreateAdminDailyStatusByMerchantReport()
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant, UserRole.Partner }, Module.SecuredObject, PermissionValue.Read);
            List<DailyStatusReportByMerchant> data;
            using (ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString))
                data = (from dsr in dc.DailyStatusReportByMerchants orderby dsr.MerchantName, dsr.ID select dsr).ToList();
            ExportTask(null, null, ReportType.AdminDailyStatusByMerchant, data);
        }

        public static void CreateAdminDailyStatusByTerminalReport()
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant, UserRole.Partner }, Module.SecuredObject, PermissionValue.Read);
            List<DailyStatusReportByTerminal> data;
            using (ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString))
                data = (from dsr in dc.DailyStatusReportByTerminals orderby dsr.AcquiringBankName, dsr.TerminalNumber, dsr.TerminalName select dsr).ToList();
            ExportTask(null, null, ReportType.AdminDailyStatusByTerminal, (ICollection)data);
        }

        public static void CreateAdminDailyRiskByTerminalReport()
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant, UserRole.Partner }, Module.SecuredObject, PermissionValue.Read);
            List<DailyRiskReportByTerminal> data;
            using (ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString))
                data = (from trs in dc.DailyRiskReportByTerminals where trs.PassedVolumeDailyAvarge > 0 || trs.MastercardThisMonthChargebacksCount > 0 || trs.VisaThisMonthChargebacksCount > 0 || trs.RefundCountPerDayAverage > 0 orderby trs.AquiringBankName, trs.TerminalName select trs).ToList();
            ExportTask(null, "Daily stats are calculated from the last 3 months", ReportType.AdminDailyRiskByTerminal, (ICollection)data);
        }

        public static void CreateAdminDailyRiskByMerchantReport()
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant, UserRole.Partner }, Module.SecuredObject, PermissionValue.Read);
            List<DailyRiskReportByMerchant> data;
            using (ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString))
                data = (from drrc in dc.DailyRiskReportByMerchants orderby drrc.CompanyName select drrc).ToList();
            ExportTask(null, "Daily stats are calculated from the last 3 months", ReportType.AdminDailyRiskByMerchant, (ICollection)data);
        }

        public static void CreateDailyReports()
        {
            foreach (var d in Domain.Domains.Values)
            {
                Domain.Current = d;
                try
                {
                    ObjectContext.Current.CredentialsToken = Domain.Current.ServiceCredentials;
                    CreateAdminDailyRiskByTerminalReport();
                    CreateAdminDailyRiskByMerchantReport();
                    CreateAdminDailyStatusByMerchantReport();
                    CreateAdminDailyStatusByTerminalReport();
                }
                catch (Exception ex)
                {
                    Logger.Log(LogSeverity.Error, LogTag.Reports, "Error generating Daily Reports for doamin " + d.Host + ", see more details", ex.ToString());
                }
            }

        }

    }
}
