﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.Reports
{
	public enum ReportFormat
	{
		Html = 1,
		Csv = 2,
		Excel = 3,
		Pdf = 4
	}
}
