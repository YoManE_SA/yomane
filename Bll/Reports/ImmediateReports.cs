﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.DataAccess;
using System.Reflection;
using System.Collections;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure;
using Netpay.Dal.Reports;
using System.Data;
using Netpay.CommonTypes;
using Netpay.Bll.Reports.VO;
using Netpay.Bll.Accounts;

namespace Netpay.Bll.Reports
{
	public static class ImmediateReports
	{
		public static List<RollingReserveReportVO> GetRollingReserveSummaryReport(ISortAndPage sortingInfo)
		{
			ObjectContext.Current.IsUserOfType(UserRole.Admin);
			ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);
			var expression = (IQueryable<RollingReserveSummary>)from r in dc.RollingReserveSummaries orderby r.CompanyName select r;
			return expression.ApplySortAndPage(sortingInfo).Select(r => new RollingReserveReportVO(r)).ToList<RollingReserveReportVO>();
		}

		public static List<RollingReserveReportVO> GetRollingReserveSummaryReport(SearchFilters filters, ISortAndPage sortingInfo)
		{
			ObjectContext.Current.IsUserOfType(UserRole.Admin);
			ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);
			var expression = (IQueryable<RollingReserveSummary>)from r in dc.RollingReserveSummaries orderby r.CompanyName select r;

			// add filters
			if(filters.merchantIDs != null && filters.merchantIDs.Count > 0)
				expression = expression.Where(r => filters.merchantIDs.Contains(r.CompanyID));
			if (filters.merchantGroupID != null)
				expression = expression.Where(r => r.CompanyGroupID == filters.merchantGroupID);
			if (filters.currencyID != null)
			{
				expression = expression.Where(r => r.CurrencyID == filters.currencyID.Value);
			}
			if (filters.reportAmount != null)
			{
				if (filters.reportRelased != null)
				{
					if (filters.reportCondition != null)
					{
						if (filters.reportCondition.Equals("lt"))
						{
							expression = expression.Where(r => r.Released < filters.reportAmount.Value);
						}
						else
						{
							expression = expression.Where(r => r.Released > filters.reportAmount.Value);
						}
					}
				}
				if (filters.reportReserved != null)
				{
					if (filters.reportCondition != null)
					{
						if (filters.reportCondition.Equals("lt"))
						{
							expression = expression.Where(r => r.Reserved < filters.reportAmount.Value);
						}
						else
						{
							expression = expression.Where(r => r.Reserved > filters.reportAmount.Value);
						}
					}
				}
				if (filters.reportTotal != null)
				{
					if (filters.reportCondition != null)
					{
						if (filters.reportCondition.Equals("lt"))
						{
							expression = expression.Where(r => (r.Reserved - r.Released) < filters.reportAmount.Value);
						}
						else
						{
							expression = expression.Where(r => (r.Reserved - r.Released) > filters.reportAmount.Value);
						}
					}
				}
			}

			return expression.ApplySortAndPage(sortingInfo).Select(r => new RollingReserveReportVO(r)).ToList<RollingReserveReportVO>();
		}

        public static List<RollingReserveReportDetailVO> GetRollingReserveDetailReport(int merchantID, int currencyID, ISortAndPage sortingInfo)
		{
			ObjectContext.Current.IsUserOfType(UserRole.Admin);
			ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);
			var expression = (IQueryable<RollingReserveData>)from r in dc.RollingReserveDatas where r.CompanyID == merchantID && r.CurrencyID == currencyID orderby r.InsertDate descending select r;
			return expression.ApplySortAndPage(sortingInfo).Select(r => new RollingReserveReportDetailVO(r)).ToList<RollingReserveReportDetailVO>();
		}

		public static string GetRollingReserveDetailTotal(int merchantID, int currencyID)
		{
			ObjectContext.Current.IsUserOfType(UserRole.Admin);
			ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);
			var total = (from r in dc.RollingReserveSummaries where r.CompanyID == merchantID && r.CurrencyID == currencyID select (r.CurrencySymbol + (r.Reserved - r.Released).ToString())).SingleOrDefault();
			return total;	
		}

		public static List<FailStatReportVO> GetFailStatReportDetail()
		{
			ObjectContext.Current.IsUserOfType(UserRole.Admin);
			ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);
			var expression = from r in dc.FailStatReportDatas select r;
			return expression.Select(r => new FailStatReportVO(r)).ToList<FailStatReportVO>();
		}

		public static List<FailStatReportVO> GetFailStatReportDetail(SearchFilters filters)
		{
			ObjectContext.Current.IsUserOfType(UserRole.Admin);
			ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);
			var expression = from r in dc.FailStatReportDatas select r;

			// add filters
			if (filters.currencyID != null)
			{
				expression = expression.Where(r => r.CurrencyCode == filters.currencyID.Value);
			}
			if (filters.paymentMethodID != null)
			{
				expression = expression.Where(r => r.PaymentMethod == filters.paymentMethodID.Value);
			}
			if (filters.debitCompanyID != null)
			{
				expression = expression.Where(r => r.DebitCompanyID == filters.debitCompanyID.Value);
			}
			if (filters.terminalNumber != null)
			{
				expression = expression.Where(r => r.TerminalNumber == filters.terminalNumber);
			}
			if (filters.replyCode != null)
			{
				expression = expression.Where(r => r.ReplyCode == filters.replyCode);
			}
			if (filters.dateFrom != null)
			{
				expression = expression.Where(r => r.InsertDate >= filters.dateFrom.Value.MinTime());
			}
			if (filters.dateTo != null)
			{
				expression = expression.Where(r => r.InsertDate <= filters.dateTo.Value.MaxTime());
			}
			return expression.Select(r => new FailStatReportVO(r)).ToList<FailStatReportVO>();
		}

		public class ReplyComparer : IEqualityComparer<FailStatReportData>
		{
			public bool Equals(FailStatReportData x, FailStatReportData y)
			{
				if (x == null || y == null)
					return false;
				else
					return (x.CardNumber.Equals(y.CardNumber)&&(x.ReplyCode==y.ReplyCode));
			}

			public int GetHashCode(FailStatReportData obj)
			{
				return obj.CardNumber.GetHashCode();
			}
		}

		public static DenormalizedTransactionVO GetDenormalizedTransaction(TransactionStatus transactionStatus, int transactionID, bool? isChargeback)
		{
			ObjectContext.Current.IsUserOfType(UserRole.Admin);
			ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);
			DenormalizedTransaction transaction = (from t in dc.DenormalizedTransactions where t.TransactionID == transactionID && t.TransactionStatus == transactionStatus.ToString() && t.IsChargeback == isChargeback select t).SingleOrDefault();
			if (transaction == null)
				return null;
					
			return new DenormalizedTransactionVO(transaction);
		}

		public static List<DenormalizedTransactionVO> GetDenormalizedTransactions(SearchFilters filters, ISortAndPage sortAndPage)
		{
			ObjectContext.Current.IsUserOfType(UserRole.Admin);
			ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);
			var expression = from e in dc.DenormalizedTransactions select e;
			
			// filters
			if (filters.transactionStatus != null)
				expression = expression.Where(e => e.TransactionStatus == filters.transactionStatus.Value.ToString());
			if (filters.dateFrom != null)
				expression = expression.Where(e => e.TransactionDate >= filters.dateFrom);
			if (filters.dateTo != null)
				expression = expression.Where(e => e.TransactionDate <= filters.dateTo);
			if (filters.isBankTransferReceived != null)
			{
				if (filters.isBankTransferReceived.Value)
					expression = expression.Where(e => e.BankTransferID != null);
				else
					expression = expression.Where(e => e.BankTransferID == null);
			}
			if (filters.bankTransferID != null)
				expression = expression.Where(e => e.BankTransferID == filters.bankTransferID);
			if (filters.debitCompanyID != null)
				expression = expression.Where(e => e.AcquiringBankID == filters.debitCompanyID);
			if (filters.terminalNumber != null)
				expression = expression.Where(e => e.TerminalNumber == filters.terminalNumber);

			return expression.ApplySortAndPage(sortAndPage).Select(e => new DenormalizedTransactionVO(e)).ToList();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="amountCols"></param>
		/// <param name="filters"></param>
		/// <param name="aggregateInfos"></param>
		/// <param name="groupingInfos"></param>
		/// <returns></returns>
		public static DataTable GetTransactionAmountsReport(List<TransactionAmountType> amountCols, SearchFilters filters, List<VOPropertyInfo> aggregateInfos, List<VOPropertyInfo> groupingInfos)
		{
			ObjectContext.Current.IsUserOfType(UserRole.Admin);
			ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);
			dc.CommandTimeout = 120;
			dc.ObjectTrackingEnabled = false;

			if (groupingInfos == null || groupingInfos.Count == 0)
				throw new ApplicationException("Must provide atleast one group.");

			// build clauses
			StringBuilder selectClauseBuilder = new StringBuilder();
			StringBuilder whereClauseBuilder = new StringBuilder();
			StringBuilder groupByClauseBuilder = new StringBuilder();

			// select clause
			foreach (VOPropertyInfo currentInfo in aggregateInfos)
			{
				string mappedProperty = currentInfo.MappedEntityProperty;
				switch (mappedProperty)
				{
					case "TotalCount":
						selectClauseBuilder.Append("\nCOUNT(1) AS [TotalCount],");
						break;
					case "TotalAmount":
						selectClauseBuilder.Append("\nSUM([Amount]) AS [TotalAmount],");
						break;
					default:
						selectClauseBuilder.AppendFormat("\n[{0}],", mappedProperty);
						break;
				}
			}

			// amounts
			foreach (TransactionAmountType currentAmount in amountCols)
			{
				selectClauseBuilder.AppendFormat("\nSUM(CASE WHEN AmountTypeID = {0} THEN Amount ELSE 0 END) AS [{1}_amount],", (int)currentAmount, currentAmount);
			}

			// group by clause
			foreach (VOPropertyInfo currentInfo in groupingInfos)
			{
				string mappedProperty = currentInfo.MappedEntityProperty;
				selectClauseBuilder.AppendFormat("[{0}],", mappedProperty);
				groupByClauseBuilder.AppendFormat("[{0}],", mappedProperty);
			}

			// where clause
			whereClauseBuilder.Append(" 0 = 0 "); 
			if (filters.transactionStatus != null)
				whereClauseBuilder.AppendFormat("\n AND TransactionStatus = '{0}'", filters.transactionStatus.ToString());
			if (filters.dateFrom != null)
				whereClauseBuilder.AppendFormat("\n AND CAST([AmountDate] AS DATE) >= CAST('{0}' AS DATE)", filters.dateFrom.Value);
			if (filters.dateTo != null)
				whereClauseBuilder.AppendFormat("\n AND CAST([AmountDate] AS DATE) <= CAST('{0}' AS DATE)", filters.dateTo.Value);
			if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
				whereClauseBuilder.AppendFormat("\n AND MerchantID IN ({0})", filters.merchantIDs.ToDelimitedString());
			if (filters.currencyID != null)
				whereClauseBuilder.AppendFormat("\n AND TransactionCurrencyID = {0}", filters.currencyID);
			if (filters.terminalNumber != null)
				whereClauseBuilder.AppendFormat("\n AND TerminalNumber = '{0}'", filters.terminalNumber.ToSql());
			if (filters.debitCompanyID != null)
				whereClauseBuilder.AppendFormat("\n AND AcquiringBankID = {0}", filters.debitCompanyID);
			if (filters.transactionAmountTypeIDs != null)
				whereClauseBuilder.AppendFormat("\n AND AmountTypeID IN({0})", filters.transactionAmountTypeIDs.ToDelimitedString());

			// add user bank & merchant restrictions
			if (AccountFilter.Current.ObjectIDs.ContainsKey(Accounts.AccountType.DebitCompany) && AccountFilter.Current.ObjectIDs[Accounts.AccountType.DebitCompany].Count > 0)
				whereClauseBuilder.AppendFormat("\n AND AcquiringBankID IN({0})", AccountFilter.Current.ObjectIDs[Accounts.AccountType.DebitCompany].ToDelimitedString());
			if (AccountFilter.Current.ObjectIDs.ContainsKey(Accounts.AccountType.Merchant) && AccountFilter.Current.ObjectIDs[Accounts.AccountType.Merchant].Count > 0)
				whereClauseBuilder.AppendFormat("\n AND MerchantID IN({0})", AccountFilter.Current.ObjectIDs[Accounts.AccountType.Merchant].ToDelimitedString());

			// build query
			string selectClause = selectClauseBuilder.ToString().Remove(selectClauseBuilder.Length - 1);
			string whereClause = whereClauseBuilder.ToString();
			string groupByClause = groupByClauseBuilder.ToString().Remove(groupByClauseBuilder.Length - 1);
			string query = string.Format("SELECT {0} \nFROM [DenormalizedTransactionAmounts] WITH(NOLOCK) \nWHERE {1} \nGROUP BY {2}", selectClause, whereClause, groupByClause);

			// run query
			DataTable result = Crud.ExecQuery(Domain.Current.ReportsConnectionString, query);
			return result;
		}

		public static List<TransactionReportVO> GetTransactionsReport(SearchFilters filters, List<VOPropertyInfo> aggregateInfos, List<VOPropertyInfo> groupingInfos, ISortAndPage sortingInfo)
		{
			ObjectContext.Current.IsUserOfType(UserRole.Admin);

			ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);
			dc.CommandTimeout = 120;
			dc.ObjectTrackingEnabled = false;
			
			if (groupingInfos == null || groupingInfos.Count == 0)
				throw new ApplicationException("Must provide atleast one group.");

			// $ conversion mode
			string columnPostfix = "";
			if (filters.currencyID == null)
			{
				var amountGroup = groupingInfos.Where(gi => gi.MappedEntityProperty == "TransactionCurrencyID").SingleOrDefault();
				if (amountGroup == null)
					columnPostfix = "USD";
			}

			// build clauses
			StringBuilder selectClauseBuilder = new StringBuilder();
			StringBuilder whereClauseBuilder = new StringBuilder();
			StringBuilder groupByClauseBuilder = new StringBuilder();
			
			// select clause
			foreach (VOPropertyInfo currentInfo in aggregateInfos)
			{
				string mappedProperty = currentInfo.MappedEntityProperty;
				switch (mappedProperty)
				{
					case "TotalCount":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Captured' AND DenormalizedTransactions.IsChargeback = 0 AND IsRefund = 0) OR (DenormalizedTransactions.TransactionStatus = 'Declined' AND DenormalizedTransactions.IsAuthorization = 0) THEN 1 ELSE 0 END) AS [TotalCount],");
						break;
					case "TotalAmount":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Captured' AND DenormalizedTransactions.IsChargeback = 0 AND IsRefund = 0) OR (DenormalizedTransactions.TransactionStatus = 'Declined' AND DenormalizedTransactions.IsAuthorization = 0) THEN DenormalizedTransactions.TransactionAmount" + columnPostfix + " ELSE 0 END) AS [TotalAmount],");
						break;
					case "PassedCount":
						selectClauseBuilder.Append("\nSUM(CASE WHEN DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Captured' AND DenormalizedTransactions.IsChargeback = 0 AND DenormalizedTransactions.IsRefund = 0 THEN 1 ELSE 0 END) AS [PassedCount],");
						break;
					case "PassedAmount":
						selectClauseBuilder.Append("\nSUM(CASE WHEN DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Captured' AND DenormalizedTransactions.IsChargeback = 0 AND DenormalizedTransactions.IsRefund = 0 THEN DenormalizedTransactions.TransactionAmount" + columnPostfix + " ELSE 0 END) AS [PassedAmount],");
						break;
					case "RefundsCount":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Captured' AND DenormalizedTransactions.IsRefund = 1) THEN 1 ELSE 0 END) AS [RefundsCount],");
						break;
					case "RefundsAmount":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Captured' AND DenormalizedTransactions.IsRefund = 1) THEN DenormalizedTransactions.TransactionAmount" + columnPostfix + " * -1 ELSE 0 END) AS [RefundsAmount],");
						break;
					case "ChargebacksCount":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Captured' AND DenormalizedTransactions.IsChargeback = 1) THEN 1 ELSE 0 END) AS [ChargebacksCount],");
						break;
					case "ChargebacksAmount":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Captured' AND DenormalizedTransactions.IsChargeback = 1) THEN DenormalizedTransactions.TransactionAmount" + columnPostfix + " * -1 ELSE 0 END) AS [ChargebacksAmount],");
						break;
                    case "PendingChargebacksCount":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Captured' AND DenormalizedTransactions.IsPendingChargeback = 1) THEN 1 ELSE 0 END) AS [PendingChargebacksCount],");
                        break;
                    case "PendingChargebacksAmount":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Captured' AND DenormalizedTransactions.IsPendingChargeback = 1) THEN DenormalizedTransactions.TransactionAmount" + columnPostfix + " * -1 ELSE 0 END) AS [PendingChargebacksAmount],");
                        break;
                    case "FailedCount":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Declined' AND DenormalizedTransactions.IsAuthorization = 0) THEN 1 ELSE 0 END) AS [FailedCount],");
						break;
					case "FailedAmount":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Declined' AND DenormalizedTransactions.IsAuthorization = 0) THEN DenormalizedTransactions.TransactionAmount" + columnPostfix + " ELSE 0 END) AS [FailedAmount],");
						break;
                    case "FraudCount":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Captured' AND DenormalizedTransactions.IsFraudByAcquirer = 1) THEN 1 ELSE 0 END) AS [FraudCount],");
						break;
					case "FraudAmount":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Captured' AND DenormalizedTransactions.IsFraudByAcquirer = 1) THEN DenormalizedTransactions.TransactionAmount" + columnPostfix + " ELSE 0 END) AS [FraudAmount],");
						break;
					case "PassedTransactionFee":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Captured' AND DenormalizedTransactions.IsRefund = 0) THEN DenormalizedTransactions.TransactionFee" + columnPostfix + " * -1 ELSE 0 END) AS [PassedTransactionFee],");
						break;
					case "RefundFee":
						selectClauseBuilder.Append("\nSUM(CASE WHEN DenormalizedTransactions.PaymentMethodID > 14 AND (DenormalizedTransactions.TransactionStatus = 'Captured' AND DenormalizedTransactions.IsRefund = 1) THEN DenormalizedTransactions.TransactionFee" + columnPostfix + " * -1 ELSE 0 END) AS [RefundFee],");
						break;
					case "FailedTransactionFee":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Declined') THEN DenormalizedTransactions.TransactionFee" + columnPostfix + " * -1 ELSE 0 END) AS [FailedTransactionFee],");
						break;
					case "RatioFee":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Captured') THEN DenormalizedTransactions.RatioFee" + columnPostfix + " * -1 ELSE 0 END) AS [RatioFee],");
						break;
					case "ChargebackFee":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Captured' AND DenormalizedTransactions.IsChargeback = 1) THEN DenormalizedTransactions.ChargebackFee" + columnPostfix + " * -1 ELSE 0 END) AS [ChargebackFee],");
						break;
					case "ClarificationFee":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Captured' AND DenormalizedTransactions.IsChargeback = 1) THEN DenormalizedTransactions.ClarificationFee" + columnPostfix + " * -1 ELSE 0 END) AS [ClarificationFee],");
						break;
					case "CapturedDebitFee":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Captured') THEN DenormalizedTransactions.DebitFee" + columnPostfix + " * -1 ELSE 0 END) AS [CapturedDebitFee],");
						break;
					case "RefundDebitFee":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Captured' AND DenormalizedTransactions.IsRefund = 1) THEN DenormalizedTransactions.DebitFee" + columnPostfix + " * -1 ELSE 0 END) AS [RefundDebitFee],");
						break;
					case "DeclinedDebitFee":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Declined') THEN DenormalizedTransactions.DebitFee" + columnPostfix + " * -1 ELSE 0 END) AS [DeclinedDebitFee],");
						break;
					case "AuthorizedDebitFee":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Authorized') THEN DenormalizedTransactions.DebitFee" + columnPostfix + " * -1 ELSE 0 END) AS [AuthorizedDebitFee],");
						break;
					case "CapturedDebitFeeCHB":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Captured' AND DenormalizedTransactions.IsChargeback = 1) THEN DenormalizedTransactions.DebitFeeCHB" + columnPostfix + " * -1 ELSE 0 END) AS [CapturedDebitFeeCHB],");
						break;
                    case "HandlingFee":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID > 14 AND DenormalizedTransactions.TransactionStatus = 'Captured') THEN DenormalizedTransactions.HandlingFee" + columnPostfix + " * -1 ELSE 0 END) AS [HandlingFee],");
						break;
                    case "ManualFee":
                        selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID = 1 AND DenormalizedTransactions.TransactionStatus = 'Captured') THEN DenormalizedTransactions.TransactionAmount" + columnPostfix + " * -1 ELSE 0 END) AS [ManualFee],");
                        break;
                    case "FraudFee":
                        selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID = 3 AND DenormalizedTransactions.TransactionStatus = 'Captured' AND DenormalizedTransactions.TransactionSourceId = 50) THEN DenormalizedTransactions.TransactionAmount" + columnPostfix + " * -1 ELSE 0 END) AS [FraudFee],");
                        break;
					case "AdminTransDebit":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID = 2 AND DenormalizedTransactions.IsRefund = 1) THEN DenormalizedTransactions.TransactionAmount" + columnPostfix + " * -1 ELSE 0 END) AS [AdminTransDebit],");
						break;
					case "AdminTransCredit":
						selectClauseBuilder.Append("\nSUM(CASE WHEN (DenormalizedTransactions.PaymentMethodID = 2 AND DenormalizedTransactions.IsRefund = 0) THEN DenormalizedTransactions.TransactionAmount" + columnPostfix + " ELSE 0 END) AS [AdminTransCredit],");
						break;
					default:
						selectClauseBuilder.AppendFormat("\n[{0}],", mappedProperty);
						break;
				}
			}

            // from clause
            string fromClause = null;
            if (filters.affiliateId != null)
                fromClause = "DenormalizedTransactions WITH(NOLOCK) LEFT OUTER JOIN MerchantAffiliates WITH(NOLOCK) ON DenormalizedTransactions.CompanyID = MerchantAffiliates.MerchantID";
            else
                fromClause = "DenormalizedTransactions WITH(NOLOCK)";

			// group by clause
			foreach (VOPropertyInfo currentInfo in groupingInfos)
			{
				string mappedProperty = currentInfo.MappedEntityProperty;
				switch (mappedProperty)
				{
					case "MerchantID":
						selectClauseBuilder.Append("\nDenormalizedTransactions.CompanyID AS [MerchantID],");
						groupByClauseBuilder.Append("\nDenormalizedTransactions.CompanyID,");
						break;
					case "MerchantNumber":
						selectClauseBuilder.Append("\nDenormalizedTransactions.CompanyNumber AS [MerchantNumber],");
						groupByClauseBuilder.Append("\nDenormalizedTransactions.CompanyNumber,");
						break;
					case "MerchantName":
						selectClauseBuilder.Append("\nDenormalizedTransactions.CompanyName AS [MerchantName],");
						groupByClauseBuilder.Append("\nDenormalizedTransactions.CompanyName,");
						break;
					case "TransactionMonth":
						selectClauseBuilder.Append("\nRight('0'+LTrim(RTrim(Str(Month(DenormalizedTransactions.TransactionDate)))), 2)+'/'+LTrim(RTrim(Str(Year(DenormalizedTransactions.TransactionDate)))) AS [TransactionMonth],");
						groupByClauseBuilder.Append("\nRight('0'+LTrim(RTrim(Str(Month(DenormalizedTransactions.TransactionDate)))), 2)+'/'+LTrim(RTrim(Str(Year(DenormalizedTransactions.TransactionDate)))),");
						break;
					case "WeekID":
						selectClauseBuilder.Append("\nDenormalizedTransactions.WeekID, DenormalizedTransactions.WeekText");
						groupByClauseBuilder.Append("\nDenormalizedTransactions.WeekID, DenormalizedTransactions.WeekText,");
						break;
					default:
						selectClauseBuilder.AppendFormat("[{0}],", mappedProperty);
						groupByClauseBuilder.AppendFormat("[{0}],", mappedProperty);
						break;
				}
			}
			
			// where clause
			whereClauseBuilder.Append(" 0 = 0 "); // exlude internal transactions
			whereClauseBuilder.Append("\n AND (DenormalizedTransactions.PaymentMethodID > " + (int)PaymentMethodEnum.MaxInternal + " OR DenormalizedTransactions.PaymentMethodID = " + (int)PaymentMethodEnum.Admin + " OR DenormalizedTransactions.PaymentMethodID = " + (int)PaymentMethodEnum.ManualFees + ")");
			if (filters.transactionStatus != null)
				whereClauseBuilder.AppendFormat("\n AND DenormalizedTransactions.TransactionStatus = '{0}'", filters.transactionStatus.ToString());
			if (filters.dateFrom != null)
				whereClauseBuilder.AppendFormat("\n AND CAST(DenormalizedTransactions.TransactionDate AS DATE) >= CAST('{0}' AS DATE)", filters.dateFrom.Value.ToSql());
			if (filters.dateTo != null)
				whereClauseBuilder.AppendFormat("\n AND CAST(DenormalizedTransactions.TransactionDate AS DATE) <= CAST('{0}' AS DATE)", filters.dateTo.Value.ToSql());
			if (filters.paymentMethodID != null)
				whereClauseBuilder.AppendFormat("\n AND DenormalizedTransactions.PaymentMethodID = {0}", filters.paymentMethodID);
			if (filters.terminalNumber != null)
				whereClauseBuilder.AppendFormat("\n AND DenormalizedTransactions.TerminalNumber = '{0}'", filters.terminalNumber.ToSql());
            if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
				whereClauseBuilder.AppendFormat("\n AND DenormalizedTransactions.CompanyID IN ({0})", filters.merchantIDs.ToDelimitedString());
			if (filters.debitCompanyID != null)
				whereClauseBuilder.AppendFormat("\n AND DenormalizedTransactions.AcquiringBankID = {0}", filters.debitCompanyID);
			if (filters.currencyID != null)
				whereClauseBuilder.AppendFormat("\n AND DenormalizedTransactions.TransactionCurrencyID = {0}", filters.currencyID);
			if (filters.merchantGroupID != null)
				whereClauseBuilder.AppendFormat("\n AND DenormalizedTransactions.MerchantGroupID = {0}", filters.merchantGroupID);
			if (filters.terminalSearchTag != null)
				whereClauseBuilder.AppendFormat("\n AND DenormalizedTransactions.TerminalSearchTag = '{0}'", filters.terminalSearchTag);
			if (filters.merchantStatus != null)
				whereClauseBuilder.AppendFormat("\n AND DenormalizedTransactions.MerchantStatusID = {0}", filters.merchantStatus);
			if (filters.isGateway != null)
				whereClauseBuilder.AppendFormat("\n AND DenormalizedTransactions.IsGateway = {0}", ((bool)(filters.isGateway) ? 1 : 0));
			if (filters.isPaidOut != null)
				whereClauseBuilder.AppendFormat("\n AND DenormalizedTransactions.IsPaidOut = {0}", ((bool)(filters.isPaidOut) ? 1 : 0));
			if (filters.industryID != null)
				whereClauseBuilder.AppendFormat("\n AND DenormalizedTransactions.IndustryID = {0}", filters.industryID);
			if (filters.accountManager != null)
				whereClauseBuilder.AppendFormat("\n AND DenormalizedTransactions.AccountManager = '{0}'", filters.accountManager.ToSql());
			if (filters.ipCountryIso2 != null)
				whereClauseBuilder.AppendFormat("\n AND DenormalizedTransactions.IPCountry = '{0}'", filters.ipCountryIso2.ToSql());
			if (filters.binCountryIso2 != null)
				whereClauseBuilder.AppendFormat("\n AND DenormalizedTransactions.BinCountry = '{0}'", filters.binCountryIso2.ToSql());
            if (filters.affiliateId != null)
                whereClauseBuilder.AppendFormat("\n AND MerchantAffiliates.AffiliateID = {0}", filters.affiliateId);
            if (filters.transactionSourceId != null)
                whereClauseBuilder.AppendFormat("\n AND DenormalizedTransactions.TransactionSourceId = {0}", filters.transactionSourceId);
            if (filters.merchantDepartmentId != null)
                whereClauseBuilder.AppendFormat("\n AND DenormalizedTransactions.MerchantDepartmentId = {0}", filters.merchantDepartmentId);

			// add user bank & merchant restrictions
			if (AccountFilter.Current.ObjectIDs.ContainsKey(Accounts.AccountType.DebitCompany) && AccountFilter.Current.ObjectIDs[Accounts.AccountType.DebitCompany].Count > 0)
				whereClauseBuilder.AppendFormat("\n AND DenormalizedTransactions.AcquiringBankID IN({0})", AccountFilter.Current.ObjectIDs[Accounts.AccountType.DebitCompany].ToDelimitedString());
			if (AccountFilter.Current.ObjectIDs.ContainsKey(Accounts.AccountType.Merchant) && AccountFilter.Current.ObjectIDs[Accounts.AccountType.Merchant].Count > 0)
				whereClauseBuilder.AppendFormat("\n AND DenormalizedTransactions.CompanyID IN({0})", AccountFilter.Current.ObjectIDs[Accounts.AccountType.Merchant].ToDelimitedString());
				
			// order by clause
			string orderByClause = "";
			if (sortingInfo == null)
				orderByClause = "[TotalAmount] DESC";
			else
			{
				string orderByColumn = sortingInfo.SortKey;
				VOPropertyInfo foundGroup = (from voProp in groupingInfos where voProp.Property == orderByColumn select voProp).SingleOrDefault();
				VOPropertyInfo foundAggregate = (from voProp in aggregateInfos where voProp.Property == orderByColumn select voProp).SingleOrDefault();
				if (foundGroup == null && foundAggregate == null)
					orderByClause = "[TotalAmount] DESC";					
				else
				{
					orderByClause = orderByColumn;
					if (sortingInfo.SortDesc) orderByClause += " DESC";				
				}
			}
		
			// build query
			string selectClause = selectClauseBuilder.ToString().Remove(selectClauseBuilder.Length - 1);
			string whereClause = whereClauseBuilder.ToString();
			string groupByClause = groupByClauseBuilder.ToString().Remove(groupByClauseBuilder.Length - 1);
			string query = string.Format("SELECT {0} \nFROM {1} \nWHERE {2} \nGROUP BY {3} \nORDER BY {4}", selectClause, fromClause, whereClause, groupByClause, orderByClause);
            System.Web.HttpContext.Current.Response.Write(string.Format("\r\n<div style=\"display:none;\">{0}</div>\r\n", query));

            // run query
			IEnumerable<TransactionReportVO> results = (IEnumerable<TransactionReportVO>)dc.ExecuteQuery<TransactionReportVO>(query, new object[0]);
			return results.ToList<TransactionReportVO>();
		}

		public static List<FailStatGroupVO> GetFailedTransactionsReport(SearchFilters filters, ISortAndPage sortingInfo)
		{
			ObjectContext.Current.IsUserOfType(UserRole.Admin);
			ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);
			dc.CommandTimeout = 120;
			
			var expression = from dt in dc.DenormalizedTransactions where dt.TransactionStatus == TransactionStatus.Declined.ToString() select dt;

			// apply filters
			if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
			{
				expression = expression.Where(dt => dt.CompanyID != null && filters.merchantIDs.Contains(dt.CompanyID.Value));
			}
			if (filters.rejectingSource != null)
			{
				expression = expression.Where(dt => dt.RejectingSourceID == (byte)filters.rejectingSource);
			}
			if (filters.currencyID != null)
			{
				expression = expression.Where(dt => dt.TransactionCurrencyID == filters.currencyID.Value);
			}
			if (filters.paymentMethodID != null)
			{
				expression = expression.Where(dt => dt.PaymentMethodID == filters.paymentMethodID.Value);
			}
			if (filters.debitCompanyID != null)
			{
				expression = expression.Where(dt => dt.AcquiringBankID == filters.debitCompanyID.Value);
			}
			if (filters.terminalNumber != null)
			{
				expression = expression.Where(dt => dt.TerminalNumber == filters.terminalNumber);
			}
			if (filters.replyCode != null)
			{
				expression = expression.Where(dt => dt.RejectionCode == filters.replyCode);
			}
			if (filters.dateFrom != null)
			{
				expression = expression.Where(dt => dt.TransactionDate >= filters.dateFrom.Value.MinTime());
			}
			if (filters.dateTo != null)
			{
				expression = expression.Where(dt => dt.TransactionDate <= filters.dateTo.Value.MaxTime());
			}
			if (filters.transTypeDebit != null && !filters.transTypeDebit.Value)
			{
				expression = expression.Where(dt => dt.TransTypeID != 0);
			}
			if (filters.transTypePreAuth != null && !filters.transTypePreAuth.Value)
			{
				expression = expression.Where(dt => dt.TransTypeID != 1);
			}
			if (filters.transTypeCapture != null && !filters.transTypeCapture.Value)
			{
				expression = expression.Where(dt => dt.TransTypeID != 2);
			}
			
			// group
			var group = expression.GroupBy(dt => new { rejectionCode = dt.RejectionCode, rejectionText = dt.RejectionText, rejectingSourceID = dt.RejectingSourceID }).OrderByDescending(g => g.Count());
			
			// sort
			if (sortingInfo != null)
			{
				switch (sortingInfo.SortKey)
				{
					case "code":
						if (!sortingInfo.SortDesc)
							group = group.OrderBy(g => g.Key.rejectionCode);
						else
							group = group.OrderByDescending(g => g.Key.rejectionCode);
						break;
					case "count":
						if (!sortingInfo.SortDesc)
							group = group.OrderBy(g => g.Count());
						else
							group = group.OrderByDescending(g => g.Count());
						break;
					case "source":
						if (!sortingInfo.SortDesc)
							group = group.OrderBy(g => g.Key.rejectingSourceID);
						else
							group = group.OrderByDescending(g => g.Key.rejectingSourceID);
						break;
					case "amount":
						if (!sortingInfo.SortDesc)
							group = group.OrderBy(g => g.Sum(k => k.TransactionAmountUSD));
						else
							group = group.OrderByDescending(g => g.Sum(k => k.TransactionAmountUSD));
						break;
					default:
						break;
				} 
			}

			// fetch to vo
			List<FailStatGroupVO> data = null;
			if (filters.currencyID != null)
				data = group.Select(g => new FailStatGroupVO(g.Key.rejectionCode, g.Count(), g.Key.rejectionText, g.Key.rejectingSourceID.ToString(), g.Sum(sg => sg.TransactionAmount))).ToList<FailStatGroupVO>();
			else
				data = group.Select(g => new FailStatGroupVO(g.Key.rejectionCode, g.Count(), g.Key.rejectionText, g.Key.rejectingSourceID.ToString(), g.Sum(sg => sg.TransactionAmountUSD))).ToList<FailStatGroupVO>();

			return data;
		}

		public static List<FailStatReplyGroupVO> GetFailStatReplyReportGrouped(SearchFilters filters)
		{
			ObjectContext.Current.IsUserOfType(UserRole.Admin);

			ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);
			var expression = from dt in dc.DenormalizedTransactions select dt;
			IOrderedQueryable<FailStatReplyGroupVO> groupExpression;

			// add filters
			if (filters.currencyID != null)
			{
				expression = expression.Where(dt => dt.TransactionCurrencyID == filters.currencyID.Value);
			}
			if (filters.paymentMethodID != null)
			{
				expression = expression.Where(dt => dt.PaymentMethodID == filters.paymentMethodID.Value);
			}
			if (filters.debitCompanyID != null)
			{
				expression = expression.Where(dt => dt.AcquiringBankID == filters.debitCompanyID.Value);
			}
			if (filters.terminalNumber != null)
			{
				expression = expression.Where(dt => dt.TerminalNumber == filters.terminalNumber);
			}
			if (filters.replyCode != null)
			{
				expression = expression.Where(dt => dt.RejectionCode == filters.replyCode);
			}
			if (filters.dateFrom != null)
			{
				expression = expression.Where(dt => dt.TransactionDate >= filters.dateFrom.Value);
			}
			if (filters.dateTo != null)
			{
				DateTime dateTo = filters.dateTo.Value + new TimeSpan(23, 59, 59);
				expression = expression.Where(dt => dt.TransactionDate <= dateTo);
			}
			if (!(bool)filters.transTypeDebit)
			{
				expression = expression.Where(dt => dt.TransTypeID != 0);
			}
			if (!(bool)filters.transTypePreAuth)
			{
				expression = expression.Where(dt => dt.TransTypeID != 1);
			}
			if (!(bool)filters.transTypeCapture)
			{
				expression = expression.Where(dt => dt.TransTypeID != 2);
			}
			
			groupExpression = (from rg in expression group rg by new { rg.TerminalNumber, rg.TerminalContractNumber } into g select new FailStatReplyGroupVO { TerminalNumber = g.Key.TerminalNumber, GroupCount = g.Count(), ContractNumber = g.Key.TerminalContractNumber }).OrderByDescending(a => a.GroupCount);
			return groupExpression.Select(dt => new FailStatReplyGroupVO(dt.TerminalNumber, dt.GroupCount, dt.ContractNumber)).ToList<FailStatReplyGroupVO>();
		}

		public static object GetPropertyValue(object obj, string property)
		{
			System.Reflection.PropertyInfo propertyInfo = obj.GetType().GetProperty(property);
			return propertyInfo.GetValue(obj, null);
		}
	}
}
