﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AnalysisServices;
using Microsoft.AnalysisServices.AdomdClient;
using System.Data;
using Netpay.Infrastructure;


namespace Netpay.Bll.Reports
{
	public static class AnalysisServicesManager
	{
		/*
		private static string analysisServicesConnectionString = null;

		public static void Init(Domain domain)
		{
			analysisServicesConnectionString = domain.AnalysisServicesConnectionString;

			// replace connection string
			string sqlNetpayConnectionString = domain.AnalysisServicesNetpayConnectionString;
			Server olapServer = new Server();
			try
			{
				olapServer.Connect(analysisServicesConnectionString);
				Database olapDatabase = olapServer.Databases["AnalysisServices"];
				Microsoft.AnalysisServices.DataSource olapDataSource = olapDatabase.DataSources["Netpay"];
				olapDataSource.ConnectionString = sqlNetpayConnectionString;
				olapDataSource.ConnectionStringSecurity = ConnectionStringSecurity.PasswordRemoved;
				olapDataSource.Update();
			}
			catch (Exception ex)
			{
				Logger.Log(ex);
				throw ex;
			}
			finally
			{
				olapServer.Disconnect();
				olapServer.Dispose();
			}
		}
		*/

		public static void ProcessCube(Domain domain)
		{
			AdomdConnection connection = new AdomdConnection(domain.AnalysisServicesConnectionString);
			AdomdCommand command = connection.CreateCommand();
			command.CommandType = CommandType.Text;
			command.CommandText = "<Batch xmlns=\"http://schemas.microsoft.com/analysisservices/2003/engine\"><Parallel><Process><Object><DatabaseID>DenormalizedTransactions</DatabaseID><CubeID>DenormalizedTransactionAmounts</CubeID></Object><Type>ProcessFull</Type><WriteBackTableCreation>UseExisting</WriteBackTableCreation></Process></Parallel></Batch>";
			try
			{
				DateTime start = DateTime.Now;
				
				connection.Open();
				command.ExecuteNonQuery();

				Logger.Log(LogSeverity.Info, LogTag.Ssas, "Cube process completed: " + (DateTime.Now - start).ToString());
			}
			catch (Exception ex)
			{
				Logger.Log(LogSeverity.Error, LogTag.Ssas, "Cube process failed.", ex.ToString());
			}
			finally
			{
				connection.Close();
			}
		}
	}
}
