﻿using System;
using System.Collections.Generic;
using System.Linq;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Reports
{
	public class IntegrationService : BaseDataObject
	{
		private Dal.Reports.tblIntegrationServicesLog _entity;
		private IntegrationService(Dal.Reports.tblIntegrationServicesLog entity) { _entity = entity; }
		public int ID { get { return _entity.ID; } }
		public string PackageName { get { return _entity.PackageName; } }
		public DateTime StartDate { get { return _entity.StartDate; } }
		public DateTime EndDate { get { return _entity.EndDate; } }
		public bool IsSuccessful { get { return _entity.IsSuccessful; } }

		public static IntegrationService GetPackageLastUpdateLog(string packageName)
		{
			ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);
			packageName = packageName.ToLower().Replace(" ", string.Empty).Replace(".dtsx", string.Empty);
			return (from l in dc.tblIntegrationServicesLogs where (l.PackageName.ToLower().Replace(" ", string.Empty).Replace(".dtsx", string.Empty) == packageName && l.IsSuccessful) orderby l.StartDate descending select new IntegrationService(l)).FirstOrDefault();
		}

		/// <summary>
		/// Returns the latest denormalized transaction date.
		/// </summary>
		public static DateTime DenormalizedMaxDate
		{
			get
			{
                return Domain.Current.GetCachData("denormalizedMaxDate", () =>
                {
                    ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);
                    var value = (from md in dc.DenormalizedTransactions select md).Max(md => md.TransactionDate);
                    if (value == null) value = DateTime.Now;
                    return new Domain.CachData(value, DateTime.Now.AddMinutes(30));
                }).ToNullableDate().GetValueOrDefault();
			}
		}

		/// <summary>
		/// Returns the earliest denormalized transaction date.
		/// </summary>
		public static DateTime DenormalizedMinDate
		{
			get
			{
                return Domain.Current.GetCachData("denormalizedMinDate", () =>
                {
                    ReportsDataContext dc = new ReportsDataContext(Domain.Current.ReportsConnectionString);
                    var value = (from md in dc.DenormalizedTransactions select md).Min(md => md.TransactionDate);
                    if (value == null) value = DateTime.Now;
                    return new Domain.CachData(value, DateTime.Now.AddMinutes(30));
                }).ToNullableDate().GetValueOrDefault();
			}
		}
	}
}
