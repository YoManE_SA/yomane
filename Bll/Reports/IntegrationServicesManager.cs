﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Dts.Runtime.Wrapper;
using System.IO;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Domains;
using Netpay.Dal.Reports;
using Netpay.Infrastructure;

namespace Netpay.Bll.Reports
{
	public static class IntegrationServicesManager
	{
		/// <summary>
		/// Loads a package if it is not yet loaded.
		/// </summary>
		/// <param name="domainHost"></param>
		/// <param name="packageFileName"></param>
		public static Package GetPackage(string domainHost, string packageFileName)
		{
			string integrationServicesNetpayConnectionString = DomainsManager.GetDomain(domainHost).IntegrationServicesNetpayConnectionString;
			string integrationServicesReportsConnectionString = DomainsManager.GetDomain(domainHost).IntegrationServicesReportsConnectionString;
			string packsBasePath = Configuration.GetParameter("integrationServicesPacksBasePath");
			Microsoft.SqlServer.Dts.Runtime.Wrapper.Application dtsRuntimeApp = new Microsoft.SqlServer.Dts.Runtime.Wrapper.Application();
			if (!Directory.Exists(packsBasePath))
				Directory.CreateDirectory(packsBasePath);
			string currentPackageFullPath = packsBasePath + packageFileName;

			if (!File.Exists(currentPackageFullPath))
			{
				Logger.Log(string.Format("Package file not found: '{0}'", currentPackageFullPath));
				return null;
			}		

			// load package
			Package package = (Package)dtsRuntimeApp.LoadPackage(currentPackageFullPath, true, null);

			// replace connection strings with the decrypted ones
			package.Connections["Netpay"].ConnectionString = integrationServicesNetpayConnectionString;
			package.Connections["Reports"].ConnectionString = integrationServicesReportsConnectionString;

			return package;
		}
		
		/// <summary>
		/// Executes a package.
		/// The package must be loaded.
		/// </summary>
		/// <param name="domainHost"></param>
		/// <param name="packageFileName"></param>
		public static void ExecutePackage(string domainHost, string packageFileName)
		{
			// get package
			Package package = GetPackage(domainHost, packageFileName);
			if (package == null)
				throw new ApplicationException(string.Format("'{0}' package not loaded.", packageFileName));
	
			// execute package
			PackageEvents events = new PackageEvents(packageFileName);
			DateTime start = DateTime.Now;
			DTSExecResult result = package.Execute(null, null, events, null, null); 
			
			// log
			ReportsDataContext dc = new ReportsDataContext(DomainsManager.GetDomain(domainHost).ReportsConnectionString);
			tblIntegrationServicesLog log = new tblIntegrationServicesLog();
			log.PackageName = packageFileName;
			log.StartDate = start;
			log.EndDate = DateTime.Now;	
			log.IsSuccessful = (result == DTSExecResult.DTSER_SUCCESS);
			dc.tblIntegrationServicesLogs.InsertOnSubmit(log);
			dc.SubmitChanges();		

			if (result != DTSExecResult.DTSER_SUCCESS)
				throw new ApplicationException("Package failed.");

			Logger.Log(LogSeverity.Info, LogTag.Ssis, string.Format("{0} completed with result: {1}", packageFileName, result));
		}
		
		public static List<string> GetPackageFiles()
		{
			string packsBasePath = Configuration.GetParameter("integrationServicesPacksBasePath");
			return Directory.GetFiles(packsBasePath).Select(path => Path.GetFileName(path)).Where(fileName => fileName.EndsWith(".dtsx")).ToList<string>();
		}

		private class PackageEvents : IDTSEvents100
		{
			public PackageEvents(string packageFileName)
			{
				_packageID = packageFileName;
			}
			
			#region IDTSEvents100 Members

			private string _packageID = null;
			
			public void OnBreakpointHit(IDTSBreakpointSite100 pBreakpointSite, IDTSBreakpointTarget100 pBreakpointTarget)
			{
				//Logger.Log("OnBreakpointHit");
			}

			public void OnCustomEvent(IDTSTaskHost100 pTaskHost, string EventName, string EventText, ref object[] ppsaArguments, string SubComponent, ref bool pbFireAgain)
			{
				//Logger.Log("OnCustomEvent");
			}

			public void OnError(IDTSRuntimeObject100 pSource, int ErrorCode, string SubComponent, string Description, string HelpFile, int HelpContext, string IDOfInterfaceWithError, out bool pbCancel)
			{
				pbCancel = false; 
				Logger.Log(LogSeverity.Error, LogTag.Ssis, string.Format("Package: {0}\n Source: {1}\n SubComponent: {2}\n Description: {3}\n HelpFile: {4}\n HelpContext: {5}", _packageID, pSource, SubComponent, Description, HelpFile, HelpContext));
			}

			public void OnExecutionStatusChanged(IDTSExecutable100 pExec, DTSExecStatus newStatus, ref bool pbFireAgain)
			{
				//Logger.Log("OnExecutionStatusChanged");
			}

			public void OnInformation(IDTSRuntimeObject100 pSource, int InformationCode, string SubComponent, string Description, string HelpFile, int HelpContext, string IDOfInterfaceWithError, ref bool pbFireAgain)
			{
				//Logger.Log(LogSeverity.Info, LogTag.Ssis, string.Format("Package: {0}\n Source: {1}\n Description: {2}", _packageID, pSource, Description));
			}

			public void OnPostExecute(IDTSExecutable100 pExec, ref bool pbFireAgain)
			{
				//Logger.Log("OnPostExecute");
			}

			public void OnPostValidate(IDTSExecutable100 pExec, ref bool pbFireAgain)
			{
				//Logger.Log("OnPostValidate");
			}

			public void OnPreExecute(IDTSExecutable100 pExec, ref bool pbFireAgain)
			{
				//Logger.Log("OnPreExecute");
			}

			public void OnPreValidate(IDTSExecutable100 pExec, ref bool pbFireAgain)
			{
				//Logger.Log("OnPreValidate");
			}

			public void OnProgress(IDTSTaskHost100 pTaskHost, string ProgressDescription, int PercentComplete, int ProgressCountLow, int ProgressCountHigh, string SubComponent, ref bool pbFireAgain)
			{
				//Logger.Log(string.Format("DTS Progress ({0}%)\n Package: {1}\n Description: {2}", PercentComplete, _packageID, ProgressDescription));
			}

			public void OnQueryCancel(out bool pbCancel)
			{
				pbCancel = false;
				//Logger.Log("OnQueryCancel");
			}

			public void OnTaskFailed(IDTSTaskHost100 pTaskHost)
			{
				Logger.Log(LogSeverity.Error, LogTag.Ssis, string.Format("OnTaskFailed: Package: {0}\n Value: {1}\n VariableName: ", _packageID, pTaskHost.ExecutionValue, pTaskHost.ExecValueVariable.QualifiedName));
			}

			public void OnVariableValueChanged(IDTSContainer100 pContainer, IDTSVariable100 pVariable, ref bool pbFireAgain)
			{
				//Logger.Log("OnVariableValueChanged");
			}

			public void OnWarning(IDTSRuntimeObject100 pSource, int WarningCode, string SubComponent, string Description, string HelpFile, int HelpContext, string IDOfInterfaceWithError)
			{
				Logger.Log(LogSeverity.Warning, LogTag.Ssis, string.Format("Package: {0}\n Source: {1}\n Description: {2}", _packageID, pSource, Description));
			}

			#endregion
		}
	}
}
