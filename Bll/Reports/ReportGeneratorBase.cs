﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Reflection;
using Netpay.Infrastructure.Security;

using Netpay.Infrastructure.VO;
using Netpay.Infrastructure;
using Netpay.Dal.Reports;
using Netpay.CommonTypes;
using System.Data;
using System.Dynamic;
using Netpay.Bll.Reports.VO;
using Netpay.Bll.OldVO;

namespace Netpay.Bll.Reports
{
    public abstract class ReportGeneratorBase
    {
        public class FieldInfo
        {
            public FieldInfo() { }
            public FieldInfo(string title, string property)
            {
                Title = title;
                Property = property;
                IsHeaderGroup = false;
            }

            public FieldInfo(string title, Func<object, object> method)
            {
                Title = title;
                Method = method;
                IsHeaderGroup = false;
            }

            public FieldInfo(string title, int colSpan)
            {
                Title = title;
                IsHeaderGroup = true;
                ColSpan = colSpan;
            }

            public string Title { get; set; }
            public string Property { get; set; }
            public Func<object, object> Method { get; set; }
            public bool IsHeaderGroup { get; set; }
            public int ColSpan { get; set; }
            public bool IsSpacer { get { return Title.Trim() == "%%spacer%%"; } }
        }

        public enum DataStyle
        {
            None,
            HighRisk
        }

        public class DataInfo
        {
            public DataInfo(DataStyle style, string data)
            {
                Style = style;
                Data = data;
            }

            public DataInfo(string data) : this(DataStyle.None, data) { }

            public DataStyle Style { get; set; }
            public string Data { get; set; }
        }

        public const string currencyFormat = ",0.00";

        public abstract byte[] Generate(List<FieldInfo> fields, ICollection[] dataGroups, string title, string comments);

        public byte[] Generate(List<FieldInfo> fields, ICollection data, string title, string comments)
        {
            return Generate(fields, new ICollection[] { data }, title, comments);
        }

        public byte[] Generate(ReportType reportType, ICollection data, string title, string comments)
        {
            return Generate(reportType, new ICollection[] { data }, title, comments);
        }

        public byte[] Generate(ReportType reportType, ICollection[] data, string title, string comments)
        {
            var fields = GetFields(reportType);
            return Generate(fields, data, title, comments);
        }

        public List<FieldInfo> GetFields(ReportType reportType)
        {
            return GetFields(Login.Current, reportType);
        }

        internal List<FieldInfo> GetFields(Login user, ReportType reportType)
        {
            List<FieldInfo> fields = new List<FieldInfo>();
            switch (reportType)
            {
                case ReportType.MerchantSettlmentReport:
                    fields.Add(new FieldInfo("Date", (object item) => ((Settlements.Settlement)item).PayDate.ToDateTimeFormat()));
                    fields.Add(new FieldInfo("Number", "ID"));
                    fields.Add(new FieldInfo("Total Trans. Amount", (object item) => ((Settlements.Settlement)item).TotalAmountTrans.GetValueOrDefault().ToAmountFormat()));
                    fields.Add(new FieldInfo("Total Payout", (object item) => ((Settlements.Settlement)item).TransPayTotal.ToString()));
                    fields.Add(new FieldInfo("Currency", (object item) => Currency.Get((int)((Settlements.Settlement)item).CurrencyID).IsoCode));
                    fields.Add(new FieldInfo("Cashback Amount", (object item) => ((Settlements.Settlement)item).TotalCashbackAmount.GetValueOrDefault().ToAmountFormat()));

                    fields.Add(new FieldInfo("Total Fee Amount", (object item) => ((Settlements.Settlement)item).TotalAmountFee.GetValueOrDefault().ToAmountFormat()));
                    fields.Add(new FieldInfo("Vat Amount", (object item) => ((Settlements.Settlement)item).VatAmount.ToAmountFormat()));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    fields.Add(new FieldInfo("Capture Count", "TotalCaptureCount"));
                    //fields.Add(new FieldInfo("Capture Amount", (object item) => ((Settlements.Settlement)item).TotalCaptureAmount.GetValueOrDefault().ToAmountFormat()));
                    //fields.Add(new FieldInfo("Admin Count", (object item) => ((Settlements.Settlement)item).TotalAdminCount.ToString()));
                    //fields.Add(new FieldInfo("Admin Amount", (object item) => ((Settlements.Settlement)item).TotalAdminAmount.ToString()));
                    //fields.Add(new FieldInfo("System Count", "TotalSystemCount"));
                    //fields.Add(new FieldInfo("System Amount", (object item) => ((Settlements.Settlement)item).TotalSystemAmount.GetValueOrDefault().ToAmountFormat()));
                    fields.Add(new FieldInfo("Refund Count", "TotalRefundCount"));
                    fields.Add(new FieldInfo("Refund Amount", (object item) => ((Settlements.Settlement)item).TotalRefundAmount.GetValueOrDefault().ToAmountFormat()));
                    fields.Add(new FieldInfo("CHB Count", "TotalChbCount"));
                    fields.Add(new FieldInfo("CHB Amount", (object item) => ((Settlements.Settlement)item).TotalChbAmount.GetValueOrDefault().ToAmountFormat()));
                    fields.Add(new FieldInfo("Captured Process Fee", (object item) => ((Settlements.Settlement)item).TotalFeeProcessCapture.GetValueOrDefault().ToAmountFormat()));
                    fields.Add(new FieldInfo("Clarification Fee", (object item) => ((Settlements.Settlement)item).TotalFeeClarification.GetValueOrDefault().ToAmountFormat()));
                    fields.Add(new FieldInfo("Financing Fee", (object item) => ((Settlements.Settlement)item).TotalFeeFinancing.GetValueOrDefault().ToAmountFormat()));
                    fields.Add(new FieldInfo("Handling Fee", (object item) => ((Settlements.Settlement)item).TotalFeeHandling.GetValueOrDefault().ToAmountFormat()));
                    fields.Add(new FieldInfo("Bank Fee", (object item) => ((Settlements.Settlement)item).TotalFeeBank.GetValueOrDefault().ToAmountFormat()));
                    fields.Add(new FieldInfo("CHB Fee ", (object item) => ((Settlements.Settlement)item).TotalFeeChb.GetValueOrDefault().ToAmountFormat()));
                    fields.Add(new FieldInfo("Rolling Reserve", (object item) => ((Settlements.Settlement)item).TotalRollingReserve.GetValueOrDefault().ToAmountFormat()));
                    fields.Add(new FieldInfo("Rolling Release", (object item) => ((Settlements.Settlement)item).TotalRollingRelease.GetValueOrDefault().ToAmountFormat()));
                    fields.Add(new FieldInfo("Direct Deposit", (object item) => ((Settlements.Settlement)item).TotalDirectDeposit.GetValueOrDefault().ToAmountFormat()));
                    break;
                case ReportType.MerchantRollingReserve:
                    fields.Add(new FieldInfo("Date", (object item) => ((Transactions.Transaction)item).InsertDate.ToDateTimeFormat()));
                    fields.Add(new FieldInfo("Transaction", (object item) => ((Transactions.Transaction)item).ID.ToString()));
                    fields.Add(new FieldInfo("Settlement", (object item) => ((Transactions.Transaction)item).PrimaryPaymentID.ToString()));
                    fields.Add(new FieldInfo("Reserve Hold", (object item) =>
                    {
                        if (((Transactions.Transaction)item).CreditType == (byte)CreditType.Refund)
                            return ((Transactions.Transaction)item).Amount.ToAmountFormat(((Transactions.Transaction)item).CurrencyID, true);

                        return "";
                    }));
                    fields.Add(new FieldInfo("Reserve Release", (object item) =>
                    {
                        if (((Transactions.Transaction)item).CreditType == CreditType.Regular)
                            return ((Transactions.Transaction)item).Amount.ToAmountFormat(((Transactions.Transaction)item).CurrencyID);

                        return "";
                    }));
                    fields.Add(new FieldInfo("Comment", (object item) => ((Transactions.Transaction)item).Comment));
                    break;
                case ReportType.AdminDailyRiskByMerchant:
                    // group headers
                    fields.Add(new FieldInfo("Details", 2));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Passed Transactions Volume", 3));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Average Passed Transaction Amount", 3));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Overall transaction count", 3));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Issuer declined transaction count", 3));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Risk declined transaction count", 3));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Gateway declined transaction count", 3));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Refunds", 3));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Authorized", 1));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Visa chargebacks", 5));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Mastercard chargebacks", 5));
                    fields.Add(new FieldInfo("%%spacer%%", 1));

                    // details
                    fields.Add(new FieldInfo("Merchant Name", "CompanyName"));
                    fields.Add(new FieldInfo("Merchant #", "CompanyNumber"));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // captured
                    fields.Add(new FieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByMerchant)item).PassedVolumeDailyAvarge.GetValueOrDefault().ToString("#,0")));
                    fields.Add(new FieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByMerchant)item).PassedVolumeYesterday.GetValueOrDefault().ToString("#,0")));
                    fields.Add(new FieldInfo("Difference from Average (%)", (object item) => GetFormattedDifference(((DailyRiskReportByMerchant)item).PassedVolumeYesterday.GetValueOrDefault(), ((DailyRiskReportByMerchant)item).PassedVolumeDailyAvarge.GetValueOrDefault(), 20)));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    fields.Add(new FieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByMerchant)item).PassedTransactionSizeDailyAverage.GetValueOrDefault().ToString("#,0")));
                    fields.Add(new FieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByMerchant)item).PassedTransactionSizeYesterdayAverage.GetValueOrDefault().ToString("#,0")));
                    fields.Add(new FieldInfo("Difference from Average (%)", (object item) => GetFormattedDifference(((DailyRiskReportByMerchant)item).PassedTransactionSizeYesterdayAverage.GetValueOrDefault(), ((DailyRiskReportByMerchant)item).PassedTransactionSizeDailyAverage.GetValueOrDefault(), 20)));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    fields.Add(new FieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByMerchant)item).PassedTransactionCountDailyAverage.GetValueOrDefault().ToString("#,0.##")));
                    fields.Add(new FieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByMerchant)item).PassedTransactionCountYesterday.GetValueOrDefault().ToString()));
                    fields.Add(new FieldInfo("Difference from Average (%)", (object item) => GetFormattedDifference(((DailyRiskReportByMerchant)item).PassedTransactionCountYesterday.GetValueOrDefault(), ((DailyRiskReportByMerchant)item).PassedTransactionCountDailyAverage.GetValueOrDefault(), 20)));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // declined
                    fields.Add(new FieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByMerchant)item).RejectedSourceIssuerPerDay.GetValueOrDefault().ToString("#,0.##")));
                    fields.Add(new FieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByMerchant)item).RejectedSourceIssuerYesterday.GetValueOrDefault().ToString()));
                    fields.Add(new FieldInfo("Difference from Average (%)", (object item) => GetFormattedDifference(((DailyRiskReportByMerchant)item).RejectedSourceIssuerYesterday.GetValueOrDefault(), ((DailyRiskReportByMerchant)item).RejectedSourceIssuerPerDay.GetValueOrDefault(), 20)));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    fields.Add(new FieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByMerchant)item).RejectedSourceRiskPerDay.GetValueOrDefault().ToString("#,0.##")));
                    fields.Add(new FieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByMerchant)item).RejectedSourceRiskYesterday.GetValueOrDefault().ToString()));
                    fields.Add(new FieldInfo("Difference from Average (%)", (object item) => GetFormattedDifference(((DailyRiskReportByMerchant)item).RejectedSourceRiskYesterday.GetValueOrDefault(), ((DailyRiskReportByMerchant)item).RejectedSourceRiskPerDay.GetValueOrDefault(), 5)));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    fields.Add(new FieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByMerchant)item).RejectedSourceGatewayPerDay.GetValueOrDefault().ToString("#,0.##")));
                    fields.Add(new FieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByMerchant)item).RejectedSourceGatewayYesterday.GetValueOrDefault().ToString()));
                    fields.Add(new FieldInfo("Difference from Average (%)", (object item) => GetFormattedDifference(((DailyRiskReportByMerchant)item).RejectedSourceGatewayYesterday.GetValueOrDefault(), ((DailyRiskReportByMerchant)item).RejectedSourceGatewayPerDay.GetValueOrDefault(), 5)));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // refunds
                    fields.Add(new FieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByMerchant)item).RefundCountPerDayAverage.GetValueOrDefault().ToString("#,0.##")));
                    fields.Add(new FieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByMerchant)item).RefundCountYesterday.GetValueOrDefault().ToString()));
                    fields.Add(new FieldInfo("Difference from Average (%)", (object item) => GetFormattedDifference(((DailyRiskReportByMerchant)item).RefundCountYesterday.GetValueOrDefault(), ((DailyRiskReportByMerchant)item).RefundCountPerDayAverage.GetValueOrDefault())));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // authorized
                    fields.Add(new FieldInfo("3 days old, not captured", (object item) => ((DailyRiskReportByMerchant)item).PreAuthorizedNotCapturedCount.GetValueOrDefault().ToString()));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // visa chargebacks
                    fields.Add(new FieldInfo("# of CHB month to date.", (object item) => GetFormatted(((DailyRiskReportByMerchant)item).VisaThisMonthChargebacksCount.GetValueOrDefault(), 150, "#,0")));
                    fields.Add(new FieldInfo("% of CHB month to date.", (object item) => GetFormattedDifference(((DailyRiskReportByMerchant)item).VisaThisMonthChargebacksCount.GetValueOrDefault(), ((DailyRiskReportByMerchant)item).VisaThisMonthNotChargebacksCount.GetValueOrDefault())));
                    fields.Add(new FieldInfo("Projected # of CHB at the end of this month.", (object item) => GetFormatted(ThisMonthProjectedTotal(((DailyRiskReportByMerchant)item).VisaThisMonthChargebacksCount), 190D, "#,0")));
                    fields.Add(new FieldInfo("Projected % of CHB at the end of this month.", (object item) => GetFormattedPercentage(ThisMonthProjectedTotal(((DailyRiskReportByMerchant)item).VisaThisMonthChargebacksCount), ThisMonthProjectedTotal(((DailyRiskReportByMerchant)item).VisaThisMonthNotChargebacksCount), 1.85D)));
                    fields.Add(new FieldInfo("% CHB Volume of Monthly Volume to Date", (object item) => GetFormattedPercentage(((DailyRiskReportByMerchant)item).VisaThisMonthChargebacksAmount.GetValueOrDefault(), ((DailyRiskReportByMerchant)item).VisaThisMonthNotChargebacksAmount.GetValueOrDefault(), 10)));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // mastercard chargebacks
                    fields.Add(new FieldInfo("# of CHB month to date.", (object item) => GetFormatted(((DailyRiskReportByMerchant)item).MastercardThisMonthChargebacksCount.GetValueOrDefault(), 35, "")));
                    fields.Add(new FieldInfo("% of CHB compared to Previous Month's # of Transactions.", (object item) => GetFormattedDifference(((DailyRiskReportByMerchant)item).MastercardThisMonthChargebacksCount.GetValueOrDefault(), ((DailyRiskReportByMerchant)item).MastercardLastMonthNotChargebacksCount.GetValueOrDefault(), .5m)));
                    fields.Add(new FieldInfo("Projected # of CHB at the end of this month.", (object item) => GetFormatted(ThisMonthProjectedTotal(((DailyRiskReportByMerchant)item).MastercardThisMonthChargebacksCount), 45, "#,0")));
                    fields.Add(new FieldInfo("Projected % of CHB at the end of this month.", (object item) => GetFormattedPercentage(ThisMonthProjectedTotal(((DailyRiskReportByMerchant)item).MastercardThisMonthChargebacksCount), ThisMonthProjectedTotal(((DailyRiskReportByMerchant)item).MastercardThisMonthNotChargebacksCount), .8D)));
                    fields.Add(new FieldInfo("% CHB Volume of Monthly Volume to Date", (object item) => GetFormattedPercentage(((DailyRiskReportByMerchant)item).MastercardThisMonthChargebacksAmount.GetValueOrDefault(), ((DailyRiskReportByMerchant)item).MastercardThisMonthNotChargebacksAmount.GetValueOrDefault(), 10m)));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    break;
                case ReportType.AdminDailyRiskByTerminal:
                    // group headers
                    fields.Add(new FieldInfo("Details", 3));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Passed Transactions Volume", 3));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Average Passed Transaction Amount", 3));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Overall transaction count", 3));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Issuer declined transaction count", 3));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Risk declined transaction count", 3));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Gateway declined transaction count", 3));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Refunds", 3));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Authorized", 1));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Visa chargebacks", 5));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Mastercard chargebacks", 5));
                    fields.Add(new FieldInfo("%%spacer%%", 1));

                    // details
                    fields.Add(new FieldInfo("Aquiring Bank", "AquiringBankName"));
                    fields.Add(new FieldInfo("Terminal Name", "TerminalName"));
                    fields.Add(new FieldInfo("Terminal #", "TerminalNumber"));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // captured
                    fields.Add(new FieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByTerminal)item).PassedVolumeDailyAvarge.GetValueOrDefault().ToString("#,0")));
                    fields.Add(new FieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByTerminal)item).PassedVolumeYesterday.GetValueOrDefault().ToString("#,0")));
                    fields.Add(new FieldInfo("Difference from Average (%)", (object item) => GetFormattedDifference(((DailyRiskReportByTerminal)item).PassedVolumeYesterday.GetValueOrDefault(), ((DailyRiskReportByTerminal)item).PassedVolumeDailyAvarge.GetValueOrDefault(), 20)));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    fields.Add(new FieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByTerminal)item).PassedTransactionSizeDailyAverage.GetValueOrDefault().ToString("#,0")));
                    fields.Add(new FieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByTerminal)item).PassedTransactionSizeYesterdayAverage.GetValueOrDefault().ToString("#,0")));
                    fields.Add(new FieldInfo("Difference from Average (%)", (object item) => GetFormattedDifference(((DailyRiskReportByTerminal)item).PassedTransactionSizeYesterdayAverage.GetValueOrDefault(), ((DailyRiskReportByTerminal)item).PassedTransactionSizeDailyAverage.GetValueOrDefault(), 20)));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    fields.Add(new FieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByTerminal)item).PassedTransactionCountDailyAverage.GetValueOrDefault().ToString("#,0.##")));
                    fields.Add(new FieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByTerminal)item).PassedTransactionCountYesterday.GetValueOrDefault().ToString()));
                    fields.Add(new FieldInfo("Difference from Average (%)", (object item) => GetFormattedDifference(((DailyRiskReportByTerminal)item).PassedTransactionCountYesterday.GetValueOrDefault(), ((DailyRiskReportByTerminal)item).PassedTransactionCountDailyAverage.GetValueOrDefault(), 20)));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // declined
                    fields.Add(new FieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByTerminal)item).RejectedSourceIssuerPerDay.GetValueOrDefault().ToString("#,0.##")));
                    fields.Add(new FieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByTerminal)item).RejectedSourceIssuerYesterday.GetValueOrDefault().ToString()));
                    fields.Add(new FieldInfo("Difference from Average (%)", (object item) => GetFormattedDifference(((DailyRiskReportByTerminal)item).RejectedSourceIssuerYesterday.GetValueOrDefault(), ((DailyRiskReportByTerminal)item).RejectedSourceIssuerPerDay.GetValueOrDefault(), 20)));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    fields.Add(new FieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByTerminal)item).RejectedSourceRiskPerDay.GetValueOrDefault().ToString("#,0.##")));
                    fields.Add(new FieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByTerminal)item).RejectedSourceRiskYesterday.GetValueOrDefault().ToString()));
                    fields.Add(new FieldInfo("Difference from Average (%)", (object item) => GetFormattedDifference(((DailyRiskReportByTerminal)item).RejectedSourceRiskYesterday.GetValueOrDefault(), ((DailyRiskReportByTerminal)item).RejectedSourceRiskPerDay.GetValueOrDefault(), 5)));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    fields.Add(new FieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByTerminal)item).RejectedSourceGatewayPerDay.GetValueOrDefault().ToString("#,0.##")));
                    fields.Add(new FieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByTerminal)item).RejectedSourceGatewayYesterday.GetValueOrDefault().ToString()));
                    fields.Add(new FieldInfo("Difference from Average (%)", (object item) => GetFormattedDifference(((DailyRiskReportByTerminal)item).RejectedSourceGatewayYesterday.GetValueOrDefault(), ((DailyRiskReportByTerminal)item).RejectedSourceGatewayPerDay.GetValueOrDefault(), 5)));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // refunds
                    fields.Add(new FieldInfo("Daily Average ($)", (object item) => ((DailyRiskReportByTerminal)item).RefundCountPerDayAverage.GetValueOrDefault().ToString("#,0.##")));
                    fields.Add(new FieldInfo("Yesterday ($)", (object item) => ((DailyRiskReportByTerminal)item).RefundCountYesterday.GetValueOrDefault().ToString()));
                    fields.Add(new FieldInfo("Difference from Average (%)", (object item) => GetFormattedDifference(((DailyRiskReportByTerminal)item).RefundCountYesterday.GetValueOrDefault(), ((DailyRiskReportByTerminal)item).RefundCountPerDayAverage.GetValueOrDefault())));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // authorized
                    fields.Add(new FieldInfo("3 days old, not captured", (object item) => ((DailyRiskReportByTerminal)item).PreAuthorizedNotCapturedCount.GetValueOrDefault().ToString()));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // visa chargebacks
                    fields.Add(new FieldInfo("# of CHB month to date.", (object item) => GetFormatted(((DailyRiskReportByTerminal)item).VisaThisMonthChargebacksCount.GetValueOrDefault(), 150, "#,0")));
                    fields.Add(new FieldInfo("% of CHB month to date.", (object item) => GetFormattedDifference(((DailyRiskReportByTerminal)item).VisaThisMonthChargebacksCount.GetValueOrDefault(), ((DailyRiskReportByTerminal)item).VisaThisMonthNotChargebacksCount.GetValueOrDefault())));
                    fields.Add(new FieldInfo("Projected # of CHB at the end of this month.", (object item) => GetFormatted(ThisMonthProjectedTotal(((DailyRiskReportByTerminal)item).VisaThisMonthChargebacksCount), 190D, "#,0")));
                    fields.Add(new FieldInfo("Projected % of CHB at the end of this month.", (object item) => GetFormattedPercentage(ThisMonthProjectedTotal(((DailyRiskReportByTerminal)item).VisaThisMonthChargebacksCount), ThisMonthProjectedTotal(((DailyRiskReportByTerminal)item).VisaThisMonthNotChargebacksCount), 1.85D)));
                    fields.Add(new FieldInfo("% CHB Volume of Monthly Volume to Date", (object item) => GetFormattedPercentage(((DailyRiskReportByTerminal)item).VisaThisMonthChargebacksAmount.GetValueOrDefault(), ((DailyRiskReportByTerminal)item).VisaThisMonthNotChargebacksAmount.GetValueOrDefault(), 10)));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // mastercard chargebacks
                    fields.Add(new FieldInfo("# of CHB month to date.", (object item) => GetFormatted(((DailyRiskReportByTerminal)item).MastercardThisMonthChargebacksCount.GetValueOrDefault(), 35, "")));
                    fields.Add(new FieldInfo("% of CHB compared to Previous Month's # of Transactions.", (object item) => GetFormattedDifference(((DailyRiskReportByTerminal)item).MastercardThisMonthChargebacksCount.GetValueOrDefault(), ((DailyRiskReportByTerminal)item).MastercardLastMonthNotChargebacksCount.GetValueOrDefault(), .5m)));
                    fields.Add(new FieldInfo("Projected # of CHB at the end of this month.", (object item) => GetFormatted(ThisMonthProjectedTotal(((DailyRiskReportByTerminal)item).MastercardThisMonthChargebacksCount), 45, "#,0")));
                    fields.Add(new FieldInfo("Projected % of CHB at the end of this month.", (object item) => GetFormattedPercentage(ThisMonthProjectedTotal(((DailyRiskReportByTerminal)item).MastercardThisMonthChargebacksCount), ThisMonthProjectedTotal(((DailyRiskReportByTerminal)item).MastercardThisMonthNotChargebacksCount), .8D)));
                    fields.Add(new FieldInfo("% CHB Volume of Monthly Volume to Date", (object item) => GetFormattedPercentage(((DailyRiskReportByTerminal)item).MastercardThisMonthChargebacksAmount.GetValueOrDefault(), ((DailyRiskReportByTerminal)item).MastercardThisMonthNotChargebacksAmount.GetValueOrDefault(), 10m)));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    break;
                case ReportType.AdminDailyStatusByMerchant:
                    // group headers
                    fields.Add(new FieldInfo("Details", 3));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Charge Attempts", 5));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Issuer Decline Ratio", 1));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Transactions", 4));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Approval Ratio", 2));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Issuer Declined Transactions", 4));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Risk Declined Transactions", 4));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Gateway Declined Transactions", 4));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Visa Chargebacks", 3));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("MC Chargebacks", 4));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Refunds", 3));

                    // info 
                    fields.Add(new FieldInfo("Merchant Name", "MerchantName"));
                    fields.Add(new FieldInfo("Merchant ID", "ID"));
                    fields.Add(new FieldInfo("Status", "MerchantStatusText"));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // Charge Attempts
                    fields.Add(new FieldInfo("Amount Authorization", (object item) => ((DailyStatusReportByMerchant)item).ChargeAttemptsAmountAuthorization.GetValueOrDefault().ToString(currencyFormat)));
                    fields.Add(new FieldInfo("Count Authorization", (object item) => ((DailyStatusReportByMerchant)item).ChargeAttemptsCountAuthorization.GetValueOrDefault().ToString("#,#")));
                    fields.Add(new FieldInfo("Amount Sales", (object item) => ((DailyStatusReportByMerchant)item).ChargeAttemptsAmountSales.GetValueOrDefault().ToString(currencyFormat)));
                    fields.Add(new FieldInfo("Count Sales", (object item) => ((DailyStatusReportByMerchant)item).ChargeAttemptsCountSales.GetValueOrDefault().ToString("#,#")));
                    fields.Add(new FieldInfo("Sent To Bank", (object item) => ((DailyStatusReportByMerchant)item).ChargeAttemptsSentToBank.GetValueOrDefault().ToString("#,#")));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // Issuer Decline Ratio
                    fields.Add(new FieldInfo("Percent", (object item) => ((DailyStatusReportByMerchant)item).IssuerDeclineRatio.GetValueOrDefault().ToString("P2")));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // Transactions
                    fields.Add(new FieldInfo("Amount Authorization", (object item) => ((DailyStatusReportByMerchant)item).TransactionsAmountAuthorization.GetValueOrDefault().ToString(currencyFormat)));
                    fields.Add(new FieldInfo("Count Authorization", (object item) => ((DailyStatusReportByMerchant)item).TransactionsCountAuthorization.GetValueOrDefault().ToString("#,#")));
                    fields.Add(new FieldInfo("Amount Sales", (object item) => ((DailyStatusReportByMerchant)item).TransactionAmountSales.GetValueOrDefault().ToString(currencyFormat)));
                    fields.Add(new FieldInfo("Count Sales", (object item) => ((DailyStatusReportByMerchant)item).TransactionCountSales.GetValueOrDefault().ToString("#,#")));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // Approval Ratio
                    fields.Add(new FieldInfo("Netpay", (object item) => ((DailyStatusReportByMerchant)item).NetpayApprovalRatio.GetValueOrDefault().ToString("P2")));
                    fields.Add(new FieldInfo("Bank", (object item) => ((DailyStatusReportByMerchant)item).BankApprovalRatio.GetValueOrDefault().ToString("P2")));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // Issuer Decline
                    fields.Add(new FieldInfo("Amount Authorization", (object item) => ((DailyStatusReportByMerchant)item).IssuerDeclineAmountAuthorization.GetValueOrDefault().ToString(currencyFormat)));
                    fields.Add(new FieldInfo("Count Authorization", (object item) => ((DailyStatusReportByMerchant)item).IssuerDeclineCountAuthorization.GetValueOrDefault().ToString("#,#")));
                    fields.Add(new FieldInfo("Amount Sales", (object item) => ((DailyStatusReportByMerchant)item).IssuerDeclineAmountSales.GetValueOrDefault().ToString(currencyFormat)));
                    fields.Add(new FieldInfo("Count Sales", (object item) => ((DailyStatusReportByMerchant)item).IssuerDeclineCountSales.GetValueOrDefault().ToString("#,#")));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // Risk Decline 
                    fields.Add(new FieldInfo("Amount Authorization", (object item) => ((DailyStatusReportByMerchant)item).RiskDeclineAmountAuthorization.GetValueOrDefault().ToString(currencyFormat)));
                    fields.Add(new FieldInfo("Count Authorization", (object item) => ((DailyStatusReportByMerchant)item).RiskDeclineCountAuthorization.GetValueOrDefault().ToString("#,#")));
                    fields.Add(new FieldInfo("Amount Sales", (object item) => ((DailyStatusReportByMerchant)item).RiskDeclineAmountSales.GetValueOrDefault().ToString(currencyFormat)));
                    fields.Add(new FieldInfo("Count Sales", (object item) => ((DailyStatusReportByMerchant)item).RiskDeclineCountSales.GetValueOrDefault().ToString("#,#")));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // GTW Decline
                    fields.Add(new FieldInfo("Amount Authorization", (object item) => ((DailyStatusReportByMerchant)item).GTWDeclineAmountAuthorization.GetValueOrDefault().ToString(currencyFormat)));
                    fields.Add(new FieldInfo("Count Authorization", (object item) => ((DailyStatusReportByMerchant)item).GTWDeclineCountAuthorization.GetValueOrDefault().ToString("#,#")));
                    fields.Add(new FieldInfo("Amount Sales", (object item) => ((DailyStatusReportByMerchant)item).GTWDeclineAmountSales.GetValueOrDefault().ToString(currencyFormat)));
                    fields.Add(new FieldInfo("Count Sales", (object item) => ((DailyStatusReportByMerchant)item).GTWDeclineCountSales.GetValueOrDefault().ToString("#,#")));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // Visa Chargebacks
                    fields.Add(new FieldInfo("# Of CHB Month To Date", (object item) => ((DailyStatusReportByMerchant)item).NumberOfVisaCHBMonthToDate.GetValueOrDefault().ToString("#,#")));
                    fields.Add(new FieldInfo("# Of Sales Month To Date", (object item) => ((DailyStatusReportByMerchant)item).NumberOfVisaSalesMonthToDate.GetValueOrDefault().ToString("#,#")));
                    fields.Add(new FieldInfo("% of CHB Month To Date", (object item) => ((DailyStatusReportByMerchant)item).PercentOfVisaCHBMonthToDate.GetValueOrDefault().ToString("P2")));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // MC Chargebacks
                    fields.Add(new FieldInfo("# Of CHB Month To Date", (object item) => ((DailyStatusReportByMerchant)item).NumberOfMCCHBMonthToDate.GetValueOrDefault().ToString("#,#")));
                    fields.Add(new FieldInfo("# Of Sales Previuos Month", (object item) => ((DailyStatusReportByMerchant)item).NumberOfMCSalesPrevMonthToDate.GetValueOrDefault().ToString("#,#")));
                    fields.Add(new FieldInfo("% of CHB Month To Date", (object item) => ((DailyStatusReportByMerchant)item).PercentOfMCCHBMonthToDate.GetValueOrDefault().ToString("P2")));
                    fields.Add(new FieldInfo("# Of Sales Month To Date", (object item) => ((DailyStatusReportByMerchant)item).NumberOfMCSalesMonthToDate.GetValueOrDefault().ToString("#,#")));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // refunds
                    fields.Add(new FieldInfo("Count", (object item) => ((DailyStatusReportByMerchant)item).RefundsCount.GetValueOrDefault().ToString("#,#")));
                    fields.Add(new FieldInfo("Amount", (object item) => ((DailyStatusReportByMerchant)item).RefundsAmount.GetValueOrDefault().ToString(currencyFormat)));
                    fields.Add(new FieldInfo("Ratio", (object item) => ((DailyStatusReportByMerchant)item).RefundsRatio.GetValueOrDefault().ToString("P2")));
                    break;
                case ReportType.AdminDailyStatusByTerminal:
                    // group headers
                    fields.Add(new FieldInfo("Details", 4));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Debit Trans", 2));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Visa CHB Month To Date", 3));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("MC CHB Month To Date", 3));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Refunds", 2));
                    fields.Add(new FieldInfo("%%spacer%%", 1));
                    fields.Add(new FieldInfo("Sub Total", 1));
                    fields.Add(new FieldInfo("%%spacer%%", 1));

                    // info
                    fields.Add(new FieldInfo("Acquiring Bank Name", "AcquiringBankName"));
                    fields.Add(new FieldInfo("Terminal Number", "TerminalNumber"));
                    fields.Add(new FieldInfo("Terminal Name", "TerminalName"));
                    fields.Add(new FieldInfo("Active", (object item) => ((DailyStatusReportByTerminal)item).TerminalIsActive.GetValueOrDefault() ? "Yes" : "No"));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // transactions
                    fields.Add(new FieldInfo("Amount", (object item) => ((DailyStatusReportByTerminal)item).TransactionAmountSales.GetValueOrDefault().ToString(currencyFormat)));
                    fields.Add(new FieldInfo("Count", (object item) => ((DailyStatusReportByTerminal)item).TransactionCountSales.GetValueOrDefault().ToString("#,#")));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // visa
                    fields.Add(new FieldInfo("Count", (object item) => ((DailyStatusReportByTerminal)item).NumberOfVisaCHBMonthToDate.GetValueOrDefault().ToString("#,#"))); // # Of CHB Month To Date
                    fields.Add(new FieldInfo("Percent", (object item) => ((DailyStatusReportByTerminal)item).PercentOfVisaCHBMonthToDate.GetValueOrDefault().ToString("P2"))); // % of CHB Month To Date
                    fields.Add(new FieldInfo("Amount", (object item) => ((DailyStatusReportByTerminal)item).AmountOfVisaChbMonthToDate.GetValueOrDefault().ToString(currencyFormat))); // Amount of CHB Month To Date
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // mc
                    fields.Add(new FieldInfo("Count", (object item) => ((DailyStatusReportByTerminal)item).NumberOfMCCHBMonthToDate.GetValueOrDefault().ToString("#,#"))); // # Of CHB Month To Date
                    fields.Add(new FieldInfo("Percent", (object item) => ((DailyStatusReportByTerminal)item).PercentOfMCCHBMonthToDate.GetValueOrDefault().ToString("P2"))); // % of CHB Month To Date
                    fields.Add(new FieldInfo("Amount", (object item) => ((DailyStatusReportByTerminal)item).AmountOfMCChbMonthToDate.GetValueOrDefault().ToString(currencyFormat))); // Amount of CHB Month To Date
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // refunds
                    fields.Add(new FieldInfo("Count", (object item) => ((DailyStatusReportByTerminal)item).RefundsCount.GetValueOrDefault().ToString("#,#")));
                    fields.Add(new FieldInfo("Amount", (object item) => ((DailyStatusReportByTerminal)item).RefundsAmount.GetValueOrDefault().ToString(currencyFormat)));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    // sub total
                    fields.Add(new FieldInfo("Amount", (object item) => ((DailyStatusReportByTerminal)item).AmountBalance.GetValueOrDefault().ToString(currencyFormat)));
                    break;
                case ReportType.PartnerCapturedTransactions:
                    fields.Add(new FieldInfo("Merchant", "Merchant.Name"));
                    fields.Add(new FieldInfo("Trans. Num.", "ID"));
                    fields.Add(new FieldInfo("Trans. Date", "InsertDate"));
                    fields.Add(new FieldInfo("Trans. Amount", (object item) => ((Transactions.Transaction)item).SignedAmount.ToString(currencyFormat)));
                    fields.Add(new FieldInfo("Trans. Currency", "CurrencyIsoCode"));
                    fields.Add(new FieldInfo("Trans. Credit Type", "CreditTypeText"));
                    fields.Add(new FieldInfo("Trans. Denied Status", "DeniedStatusText"));
                    fields.Add(new FieldInfo("Trans. Approval Num.", "ApprovalNumber"));
                    fields.Add(new FieldInfo("Payment Method", (object item) => ((Transactions.Transaction)item).PaymentMethodDisplay == null ? "" : ((Transactions.Transaction)item).PaymentMethodDisplay)); /*. Enums.GetCreditCardType(((TransactionVO)item).PaymentMethod.CardTypeID).ToString()*/
                    fields.Add(new FieldInfo("Payout", (object item) => ((Transactions.Transaction)item).IsInstallments ? "" : Enums.GetPaymentsStatus(((Transactions.Transaction)item).PaymentsIDs.Trim()).ToString()));
                    fields.Add(new FieldInfo("Installments", GetInstallments));
                    fields.Add(new FieldInfo("Bank Transfer", "MoneyTransferText"));
                    break;
                case ReportType.PartnerDeclinedTransactions:
                    fields.Add(new FieldInfo("Merchant", "Merchant.Name"));
                    fields.Add(new FieldInfo("Trans. Num.", "ID"));
                    fields.Add(new FieldInfo("Trans. Date", "InsertDate"));
                    fields.Add(new FieldInfo("Trans. Amount", (object item) => ((Transactions.Transaction)item).SignedAmount.ToString(currencyFormat)));
                    fields.Add(new FieldInfo("Trans. Currency", "CurrencyIsoCode"));
                    fields.Add(new FieldInfo("Trans. Credit Type", "CreditTypeText"));
                    fields.Add(new FieldInfo("Trans. Denied Status", "DeniedStatusText"));
                    fields.Add(new FieldInfo("Payment Method", (object item) => ((Transactions.Transaction)item).PaymentMethodDisplay == null ? "" : ((Transactions.Transaction)item).PaymentMethodDisplay));
                    fields.Add(new FieldInfo("Reply Code", "ReplyCode"));
                    fields.Add(new FieldInfo("Reply Description", "ReplyDescription.DescriptionCustomerEng"));
                    break;
                case ReportType.MerchantChargebackTransactions:
                    fields.Add(new FieldInfo("Trans. Num.", "TransactionID"));
                    fields.Add(new FieldInfo("Trans. Date", "TransactionInsertDate"));
                    fields.Add(new FieldInfo("Trans. Amount", (object item) => ((DenormalizedTransactionHistoryVO)item).TransactionAmount.GetValueOrDefault().ToString(currencyFormat)));
                    fields.Add(new FieldInfo("Trans. Currency", "TransactionCurrencyIsoCode"));
                    fields.Add(new FieldInfo("Payment Method", (object item) => ((DenormalizedTransactionHistoryVO)item).TransactionPaymentMethodName == null ? "" : ((DenormalizedTransactionHistoryVO)item).TransactionPaymentMethodName));
                    fields.Add(new FieldInfo("Action Type", "HistoryTypeName"));
                    fields.Add(new FieldInfo("CHB Date", "HistoryInsertDate"));
                    fields.Add(new FieldInfo("CHB Code", "HistoryReferenceNumber"));
                    fields.Add(new FieldInfo("CHB Description", "HistoryDescription"));
                    break;
                case ReportType.MerchantSummary:
                    fields.Add(new FieldInfo("Currency", "CurrencyName"));
                    fields.Add(new FieldInfo("Payment Method", "PaymentMethod"));
                    fields.Add(new FieldInfo("Total Fees", (object item) => ((MerchantSummaryVO)item).TotalFees.ToAmountFormat()));
                    fields.Add(new FieldInfo("Total Trans Amount", (object item) => ((MerchantSummaryVO)item).TotalTransAmount.ToAmountFormat()));
                    fields.Add(new FieldInfo("Estimated Payout", (object item) => ((MerchantSummaryVO)item).NetSalesAmount.ToAmountFormat()));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    fields.Add(new FieldInfo("Captured", "SalesCount"));
                    fields.Add(new FieldInfo("Capture Amount", (object item) => ((MerchantSummaryVO)item).SalesAmount.ToAmountFormat()));
                    fields.Add(new FieldInfo("Refunds", "RefundsCount"));
                    fields.Add(new FieldInfo("Refunds Amount", (object item) => ((MerchantSummaryVO)item).RefundsAmount.ToAmountFormat()));
                    fields.Add(new FieldInfo("ChargeBacks", "ChargeBacksCount"));
                    fields.Add(new FieldInfo("ChargeBacks Amount", (object item) => ((MerchantSummaryVO)item).ChargeBacksAmount.ToAmountFormat()));
                    fields.Add(new FieldInfo("Declined Transactions", "DeclinedCount"));
                    fields.Add(new FieldInfo("%%spacer%%", (object item) => "%%spacer%%"));
                    fields.Add(new FieldInfo("Processing Fee", (object item) => ((MerchantSummaryVO)item).TotalApprovedProcessFee.ToAmountFormat()));
                    fields.Add(new FieldInfo("ChargeBacks Fee", (object item) => ((MerchantSummaryVO)item).ChargeBacksFee.ToAmountFormat()));
                    fields.Add(new FieldInfo("Refunds Fee", (object item) => ((MerchantSummaryVO)item).RefundsTransactionsFee.ToAmountFormat()));
                    fields.Add(new FieldInfo("Declined Transactions Fee", (object item) => ((MerchantSummaryVO)item).DeclinedTransactionsFee.ToAmountFormat()));
                    break;
                case ReportType.MerchantDeclinedArchive:
                    fields.Add(new FieldInfo("Trans. Num.", "ID"));
                    fields.Add(new FieldInfo("Trans. Date", "InsertDate"));
                    fields.Add(new FieldInfo("Trans. Amount", (object item) => ((Transactions.Transaction)item).Amount.ToString(currencyFormat)));
                    fields.Add(new FieldInfo("Trans. Currency", "CurrencyIsoCode"));
                    fields.Add(new FieldInfo("Trans. Credit Type", "CreditTypeText"));
                    fields.Add(new FieldInfo("Trans. Order", "OrderNumber"));
                    fields.Add(new FieldInfo("Trans. Denied Status", "DeniedStatusText"));
                    fields.Add(new FieldInfo("Trans. Approva Num.", "ApprovalNumber"));
                    fields.Add(new FieldInfo("Payment Method", "PaymentMethodDisplay"));
                    fields.Add(new FieldInfo("Card Type", (object item) => ((Transactions.Transaction)item).RefPaymentMethod != null ? ((Transactions.Transaction)item).RefPaymentMethod.Name : null));
                    fields.Add(new FieldInfo("Reply Code", "ReplyCode"));
                    fields.Add(new FieldInfo("Reply Description", "ReplyCodeDescription"));
                    fields.Add(new FieldInfo("Comment", "Comment"));
                    break;
                case ReportType.MerchantRecurringCharges:
                    fields.Add(new FieldInfo("Charge No.", "ChargeNumber"));
                    fields.Add(new FieldInfo("Series No.", "SeriesId"));
                    fields.Add(new FieldInfo("Type", (object item) => ((Transactions.Recurring.Charge)item).Series.IsPreAuthorized ? "Auth" : "Debit"));
                    fields.Add(new FieldInfo("Payment Method", (object item) => ((Transactions.Recurring.Charge)item).Payment?.MethodInstance.PaymentMethod.Name));
                    fields.Add(new FieldInfo("Charge Date", "Date"));
                    fields.Add(new FieldInfo("Currency", (object item) => Bll.Currency.GetIsoCode((CommonTypes.Currency)((Transactions.Recurring.Charge)item).Currency.GetValueOrDefault())));
                    fields.Add(new FieldInfo("Amount", "Amount"));
                    break;
                case ReportType.MerchantRecurringSeries:
                    fields.Add(new FieldInfo("ID", "ID"));
                    fields.Add(new FieldInfo("Start Date", "StartDate"));
                    fields.Add(new FieldInfo("Type", (object item) => ((Transactions.Recurring.Series)item).IsPreAuthorized ? "Auth" : "Debit"));
                    fields.Add(new FieldInfo("Payment Method", (object item) => ((Transactions.Recurring.Series)item).Payment?.MethodInstance.PaymentMethod.Name));
                    fields.Add(new FieldInfo("Amount", "Amount"));
                    fields.Add(new FieldInfo("Currency", (object item) => Bll.Currency.GetIsoCode((CommonTypes.Currency)((Transactions.Recurring.Series)item).Currency.GetValueOrDefault())));
                    fields.Add(new FieldInfo("Charges", "Charges"));
                    fields.Add(new FieldInfo("Flexible", "IsFlexible"));
                    fields.Add(new FieldInfo("Charge Interval", "IntervalUnit"));
                    fields.Add(new FieldInfo("Interval Length", "IntervalCount"));
                    fields.Add(new FieldInfo("Status", "Status"));
                    break;
                case ReportType.MerchantCapturedTransactions:
                case ReportType.MerchantDeclinedTransactions:
                case ReportType.MerchantCapturedArchive:
                case ReportType.MerchantPendingTransactions:

                    #region New Fields

                    fields.Add(new FieldInfo("Trans. Date", "InsertDate"));
                    fields.Add(new FieldInfo("Trans. Num.", "ID"));
                    fields.Add(new FieldInfo("Trans. Amount", "SignedAmount"));//.ToString(currencyFormat)));
                    fields.Add(new FieldInfo("Trans. Currency", "CurrencyIsoCode"));
                    fields.Add(new FieldInfo("Total Fees", "TotalFees"));
                    fields.Add(new FieldInfo("Trans. Fee", "TransactionFee"));
                    fields.Add(new FieldInfo("Processing Fee", "RatioFee"));
                    fields.Add(new FieldInfo("Chargeback Fee", "ChargebackFee"));
                    if (reportType == ReportType.MerchantDeclinedTransactions)
                        fields.Add(new FieldInfo("Decline Fee", "TransactionFee"));
                    //fields.Add(new FieldInfo("Retrieval Fee", "ClarificationFee"));
                    //fields.Add(new FieldInfo("Handling Fee", "HandlingFee"));
                    //fields.Add(new FieldInfo("Cashback", "Cashback"));
                    fields.Add(new FieldInfo("Trans. Type", "CreditTypeText"));
                    //fields.Add(new FieldInfo("Trans. Order", "OrderNumber"));
                    if (reportType == ReportType.MerchantCapturedTransactions)
                        fields.Add(new FieldInfo("Trans. Denied Status", "DeniedStatusText"));
                    //fields.Add(new FieldInfo("Trans. Approval Num.", "ApprovalNumber"));
                    fields.Add(new FieldInfo("Payment Method", (object item) => ((Transactions.Transaction)item).PaymentData == null ? "" : ((Transactions.Transaction)item).PaymentData.MethodInstance.PaymentMethod.Name));
                    fields.Add(new FieldInfo("Card Last 4 Digits", (object item) => ((Transactions.Transaction)item).PaymentData == null ? "" : ((Transactions.Transaction)item).PaymentData.MethodInstance.Value1Last4));
                    fields.Add(new FieldInfo("Card Month Exp.", (object item) => ((Transactions.Transaction)item).PaymentData == null ? "" : ((Transactions.Transaction)item).PaymentData.MethodInstance.ExpirationMonth.ToString()));
                    fields.Add(new FieldInfo("Card Year Exp.", (object item) => ((Transactions.Transaction)item).PaymentData == null ? "" : ((Transactions.Transaction)item).PaymentData.MethodInstance.ExpirationYear.ToString()));
                    fields.Add(new FieldInfo("Card Bin", (object item) => ((Transactions.Transaction)item).PaymentData == null ? "" : ((Transactions.Transaction)item).PaymentData.MethodInstance.Value1First6.ToString()));
                    if (reportType != ReportType.MerchantDeclinedTransactions)
                        fields.Add(new FieldInfo("Settlement", "PrimaryPaymentID"));
                    if (reportType == ReportType.MerchantCapturedTransactions)
                    {
                        fields.Add(new FieldInfo("Payout Status", (object item) => ((Transactions.Transaction)item).IsInstallments ? "" : Enums.GetPaymentsStatus(((Transactions.Transaction)item).PaymentsIDs.Trim()).ToString()));
                        fields.Add(new FieldInfo("Installments", GetInstallments));
                    }
                    else if (reportType == ReportType.MerchantDeclinedTransactions || reportType == ReportType.MerchantDeclinedArchive)
                    {
                        fields.Add(new FieldInfo("Reply Code", "ReplyCode"));
                        fields.Add(new FieldInfo("Reply Description", "ReplyDescription.DescriptionCustomerEng"));
                    }
                    fields.Add(new FieldInfo("Comment", "Comment"));
                    if (user != null && reportType == ReportType.MerchantCapturedTransactions)
                    {
                        if (Login.Current.Role == UserRole.Merchant && Merchants.Merchant.Current.PayPercent < 100)
                            fields.Add(new FieldInfo("Bank Transfer", "MoneyTransferText"));
                    }

                    #endregion

                    #region Old

                    //fields.Add(new FieldInfo("Trans. Date", "InsertDate"));
                    //fields.Add(new FieldInfo("Trans. Num.", "ID"));
                    //fields.Add(new FieldInfo("Trans. Amount", (object item) => ((Transactions.Transaction)item).SignedAmount.ToString(currencyFormat)));
                    //fields.Add(new FieldInfo("Trans. Currency", "CurrencyIsoCode"));

                    //fields.Add(new FieldInfo("Trans. Fee", (object item) => ((Transactions.Transaction)item).TransactionFee.ToString(currencyFormat)));
                    //fields.Add(new FieldInfo("Processing Fee", (object item) => ((Transactions.Transaction)item).RatioFee.ToString(currencyFormat)));
                    //fields.Add(new FieldInfo("Chargeback Fee", (object item) => ((Transactions.Transaction)item).ChargebackFee.ToString(currencyFormat)));
                    //fields.Add(new FieldInfo("Retrieval Fee", (object item) => ((Transactions.Transaction)item).ClarificationFee.ToString(currencyFormat)));
                    //fields.Add(new FieldInfo("Handling Fee", (object item) => ((Transactions.Transaction)item).HandlingFee.ToString(currencyFormat)));
                    //fields.Add(new FieldInfo("Trans. Credit Type", "CreditTypeText"));
                    //fields.Add(new FieldInfo("Trans. Order", "OrderNumber"));
                    //if (reportType == ReportType.MerchantCapturedTransactions) 
                    //	fields.Add(new FieldInfo("Trans. Denied Status", "DeniedStatusText"));
                    //fields.Add(new FieldInfo("Trans. Approval Num.", "ApprovalNumber"));
                    //fields.Add(new FieldInfo("Payment Method", (object item) => ((Transactions.Transaction)item).PaymentData == null ? "" : ((Transactions.Transaction)item).PaymentData.MethodInstance.PaymentMethod.Name));
                    //fields.Add(new FieldInfo("Card Last 4 Digits", (object item) => ((Transactions.Transaction)item).PaymentData == null ? "" : ((Transactions.Transaction)item).PaymentData.MethodInstance.Value1Last4));
                    //fields.Add(new FieldInfo("Card Month Exp.", (object item) => ((Transactions.Transaction)item).PaymentData == null ? "" : ((Transactions.Transaction)item).PaymentData.MethodInstance.ExpirationMonth.ToString()));
                    //fields.Add(new FieldInfo("Card Year Exp.",  (object item) => ((Transactions.Transaction)item).PaymentData == null ? "" : ((Transactions.Transaction)item).PaymentData.MethodInstance.ExpirationYear.ToString()));
                    //fields.Add(new FieldInfo("Card Bin", (object item) => ((Transactions.Transaction)item).PaymentData == null ? "" : ((Transactions.Transaction)item).PaymentData.MethodInstance.Value1First6.ToString()));
                    //fields.Add(new FieldInfo("Settlment", "PrimaryPaymentID"));
                    //if (reportType == ReportType.MerchantCapturedTransactions)
                    //{
                    //	fields.Add(new FieldInfo("Payout", (object item) => ((Transactions.Transaction)item).IsInstallments ? "" : Enums.GetPaymentsStatus(((Transactions.Transaction)item).PaymentsIDs.Trim()).ToString()));
                    //	fields.Add(new FieldInfo("Installments", GetInstallments));
                    //}
                    //else if (reportType == ReportType.MerchantDeclinedTransactions || reportType == ReportType.MerchantDeclinedArchive)
                    //{
                    //	fields.Add(new FieldInfo("Reply Code", "ReplyCode"));
                    //	fields.Add(new FieldInfo("Reply Description", "ReplyDescription.DescriptionCustomerEng"));
                    //}
                    //fields.Add(new FieldInfo("Comment", "Comment"));
                    //if (user != null && reportType == ReportType.MerchantCapturedTransactions) 
                    //{
                    //	if (Login.Current.Role == UserRole.Merchant && Merchants.Merchant.Current.PayPercent < 100)
                    //		fields.Add(new FieldInfo("Bank Transfer", "MoneyTransferText"));
                    //}

                    #endregion

                    //fields.Add(new FieldInfo("Card Country", "PaymentData.IssuerCountryIsoCode"));
                    //fields.Add(new FieldInfo("Cardholder Name", "PayerData.FullName"));
                    //fields.Add(new FieldInfo("Cardholder Phone", "PayerData.PhoneNumber"));
                    //fields.Add(new FieldInfo("Cardholder Email", "PayerData.EmailAddress"));
                    //fields.Add(new FieldInfo("Cardholder ID Num.", "PayerData.PersonalNumber"));
                    //fields.Add(new FieldInfo("Billing Address 1", "PaymentData.BillingAddress.AddressLine1"));
                    //fields.Add(new FieldInfo("Billing Address 2", "PaymentData.BillingAddress.AddressLine2"));
                    //fields.Add(new FieldInfo("Billing City", "PaymentData.BillingAddress.City"));
                    //fields.Add(new FieldInfo("Billing Zip Code", "PaymentData.BillingAddress.PostalCode"));
                    //fields.Add(new FieldInfo("Billing State", GetStateName));
                    //fields.Add(new FieldInfo("Billing Country", GetCountryName));
                    break;
                case ReportType.MerchantCustomerTransactionsReport:
                    fields.Add(new FieldInfo("Trans. Date", "InsertDate"));
                    fields.Add(new FieldInfo("Trans. Num.", "ID"));
                    fields.Add(new FieldInfo("Trans. Amount", (object item) => ((Transactions.Transaction)item).SignedAmount.ToString(currencyFormat)));
                    fields.Add(new FieldInfo("Trans. Currency", "CurrencyIsoCode"));
                    fields.Add(new FieldInfo("Trans. Type", "CreditTypeText"));
                    fields.Add(new FieldInfo("Payment Method", (object item) => ((Transactions.Transaction)item).PaymentMethodName));
                    fields.Add(new FieldInfo("Last 4 Digits", (object item) => ((Transactions.Transaction)item).PaymentData.MethodInstance.Value1Last4));
                    //fields.Add(new FieldInfo("Cardholder Name", (object item) => ((Transactions.Transaction)item).PayerData.FullName));
                    //fields.Add(new FieldInfo("Cardholder Phone", (object item) => ((Transactions.Transaction)item).PayerData.PhoneNumber));
                    //fields.Add(new FieldInfo("Cardholder Email", (object item) => ((Transactions.Transaction)item).PayerData.EmailAddress));
                    //fields.Add(new FieldInfo("Pay For", "PayForText"));
                    //fields.Add(new FieldInfo("Comment", "Comment"));
                    break;
                case ReportType.MerchantRefundRequests:
                    fields.Add(new FieldInfo("Req. ID", "ID"));
                    fields.Add(new FieldInfo("Trans. ID", "TransactionID"));
                    fields.Add(new FieldInfo("Req. Amount", "RequestAmount"));
                    fields.Add(new FieldInfo("Req. Currency", "CurrencyIso"));
                    fields.Add(new FieldInfo("Req. Comment", "Comment"));
                    fields.Add(new FieldInfo("Req. Status", "Status"));
                    fields.Add(new FieldInfo("Req. Date", "Date"));
                    fields.Add(new FieldInfo("Ref. Amount", "RefundAmount"));
                    break;

            }

            return fields;
        }

        public string GetStateName(object item)
        {
            var transaction = (Transactions.Transaction)item;
            if (transaction.PaymentData == null || transaction.PaymentData.BillingAddress == null)
                return "";

            State state = State.Get(transaction.PaymentData.BillingAddress.StateISOCode);
            if (state == null)
                return "";

            return state.Name;
        }

        public string GetCountryName(object item)
        {
            var transaction = (Transactions.Transaction)item;
            if (transaction.PaymentData == null || transaction.PaymentData.BillingAddress == null)
                return "";

            Country country = Country.Get(transaction.PaymentData.BillingAddress.CountryISOCode);
            if (country == null)
                return "";

            return country.Name;
        }

        public string GetInstallments(object item)
        {
            var transaction = (Transactions.Transaction)item;
            if (!transaction.IsInstallments)
                return "";

            StringBuilder installmentsBuilder = new StringBuilder();
            foreach (var currentInstallment in transaction.InstallmentList)
            {
                installmentsBuilder.Append("Installment");
                installmentsBuilder.Append(": ");
                installmentsBuilder.Append(currentInstallment.Comment);
                installmentsBuilder.Append(", ");
                installmentsBuilder.Append(currentInstallment.Amount.ToAmountFormat(transaction.CurrencyID));

                if (currentInstallment.SettlementID > 0)
                    installmentsBuilder.Append(string.Format(", Settled ({0})", currentInstallment.SettlementID, transaction.CurrencyID));
                else
                    installmentsBuilder.Append(", Unsettled");

                installmentsBuilder.AppendLine();
            }

            return installmentsBuilder.ToString();
        }

        public string GetBankPayoutText(Transactions.Transaction transaction)
        {
            string output = "";
            if (transaction.EpaList == null) return "No Records";
            if (transaction.EpaList.Count == 0) return "No Records";
            if (transaction.Installments == 1)
            {
                if (transaction.EpaList[0].IsPaid && transaction.EpaList[0].IsRefunded)
                {
                    output = "Paid (" + transaction.EpaList[0].PaidInsertDate + ") And Refunded (" + transaction.EpaList[0].RefundedInsertDate + ")";
                }
                else if (transaction.EpaList[0].IsPaid)
                {
                    output = "Paid (" + transaction.EpaList[0].PaidInsertDate + ")";
                }
                else if (transaction.EpaList[0].IsRefunded)
                {
                    output = "Refunded (" + transaction.EpaList[0].RefundedInsertDate + ")";
                }
            }
            else
            {
                string temp = null;
                foreach (var record in transaction.EpaList)
                {
                    if (record.IsPaid && record.IsRefunded)
                    {
                        temp = "Paid And Refunded";
                    }
                    else if (record.IsPaid)
                    {
                        temp = "Paid (" + record.PaidInsertDate + ")";
                    }
                    else if (record.IsRefunded)
                    {
                        temp = "Refunded (" + record.RefundedInsertDate + ")";
                    }
                    if (!string.IsNullOrEmpty(temp)) output += ";" + temp;
                }
            }

            return output;
        }

        public string GetFormattedPercentage(double a, double b)
        {
            return (Netpay.Infrastructure.Math.GetPercentage(a, b) / 100).ToString("P");
        }

        public string GetFormattedPercentage(decimal a, decimal b)
        {
            return (Netpay.Infrastructure.Math.GetPercentage(a, b) / 100).ToString("P");
        }

        public DataInfo GetFormattedPercentage(int a, int b, decimal threshold)
        {
            return GetFormattedPercentage(Convert.ToDecimal(a), Convert.ToDecimal(b), threshold);
        }

        public DataInfo GetFormattedPercentage(double a, double b, double threshold)
        {
            double percentage = Netpay.Infrastructure.Math.GetPercentage(a, b);
            DataInfo info = new DataInfo((percentage / 100).ToString("P"));
            if (percentage >= threshold)
                info.Style = DataStyle.HighRisk;

            return info;
        }

        public DataInfo GetFormattedPercentage(decimal a, decimal b, decimal threshold)
        {
            decimal percentage = Netpay.Infrastructure.Math.GetPercentage(a, b);
            DataInfo info = new DataInfo((percentage / 100).ToString("P"));
            if (percentage >= threshold)
                info.Style = DataStyle.HighRisk;

            return info;
        }

        public DataInfo GetFormatted(int value, decimal threshold, string format)
        {
            return GetFormatted(Convert.ToDecimal(value), threshold, format);
        }

        public DataInfo GetFormatted(double value, double threshold, string format)
        {
            DataInfo info = new DataInfo(value.ToString(format));
            if (value >= threshold)
                info.Style = DataStyle.HighRisk;

            return info;
        }

        public DataInfo GetFormatted(decimal value, decimal threshold, string format)
        {
            DataInfo info = new DataInfo(value.ToString(format));
            if (value >= threshold)
                info.Style = DataStyle.HighRisk;

            return info;
        }

        public string GetFormattedDifference(int yesterday, double dailyAverage)
        {
            return (Netpay.Infrastructure.Math.GetPercentageDifference(yesterday, dailyAverage) / 100).ToString("P");
        }

        public string GetFormattedDifference(decimal yesterday, decimal dailyAverage)
        {
            return (Netpay.Infrastructure.Math.GetPercentageDifference(yesterday, dailyAverage) / 100).ToString("P");
        }

        public DataInfo GetFormattedDifference(int yesterday, double dailyAverage, double threshold)
        {
            double difference = Netpay.Infrastructure.Math.GetPercentageDifference((double)yesterday, dailyAverage);
            DataInfo info = new DataInfo((difference / 100).ToString("P"));
            if (difference >= threshold)
                info.Style = DataStyle.HighRisk;

            return info;
        }

        public DataInfo GetFormattedDifference(decimal yesterday, decimal dailyAverage, decimal threshold)
        {
            decimal difference = Netpay.Infrastructure.Math.GetPercentageDifference(yesterday, dailyAverage);
            DataInfo info = new DataInfo((difference / 100).ToString("P"));
            if (difference >= threshold)
                info.Style = DataStyle.HighRisk;

            return info;
        }

        public int ThisMonthTotalDays
        {
            get
            {
                int daysInThisMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1).Day;
                return daysInThisMonth;
            }
        }

        public int ThisMontPassedhDays
        {
            get
            {
                int daysPassed = DateTime.Now.Day - 1;
                return daysPassed;
            }
        }

        public double ThisMonthAveragePerDay(double? thisMonthCount)
        {
            if (thisMonthCount == null || thisMonthCount == 0)
                return 0;

            double averagePerDay = thisMonthCount.Value / (double)ThisMontPassedhDays;
            return averagePerDay;
        }

        public double ThisMonthProjectedTotal(double? thisMonthCount)
        {
            if (thisMonthCount == null || thisMonthCount == 0)
                return 0;

            double projectedTotal = ThisMonthAveragePerDay(thisMonthCount) * ThisMonthTotalDays;
            return projectedTotal;
        }
    }
}
