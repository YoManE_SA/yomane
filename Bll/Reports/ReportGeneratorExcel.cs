﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Reflection;

using Netpay.Infrastructure;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Linq;

namespace Netpay.Bll.Reports
{
    public class ReportGeneratorExcel : ReportGeneratorBase
    {
        public override byte[] Generate(List<FieldInfo> fields, ICollection[] dataGroups, string title, string comments)
        {
            using (ExcelPackage package = new ExcelPackage())
            {
                // workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Report");
                package.Workbook.Properties.Title = "Report";
                package.Workbook.Properties.Author = "Auto Generated";
                int rowIndex = 1;

                // title
                if (title != null)
                {
                    if (title.Contains(Environment.NewLine))
                    {
                        foreach (string strTitleLine in title.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            worksheet.Cells[rowIndex, 1, rowIndex, fields.Count].Merge = true; // [int FromRow, int FromCol, int ToRow, int ToCol]
                            worksheet.Cells[rowIndex, 1, rowIndex, fields.Count].Value = strTitleLine;
                            worksheet.Row(rowIndex).Height = 20;
                            worksheet.Row(rowIndex).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Row(rowIndex).Style.WrapText = true;
                            rowIndex++;
                        }
                    }
                    else
                    {
                        worksheet.Cells[rowIndex, 1, 1, fields.Count].Merge = true; // [int FromRow, int FromCol, int ToRow, int ToCol]
                        worksheet.Cells[rowIndex, 1, 1, fields.Count].Value = title;
                        worksheet.Row(rowIndex).Height = 20;
                        worksheet.Row(rowIndex).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Row(rowIndex).Style.WrapText = true;
                        rowIndex++;
                    }
                }

                // comments
                if (comments != null)
                {
                    worksheet.Cells[rowIndex, 1, rowIndex, fields.Count].Merge = true;
                    worksheet.Cells[rowIndex, 1, rowIndex, fields.Count].Value = comments;
                    worksheet.Row(rowIndex).Height = 20;
                    worksheet.Row(rowIndex).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    rowIndex++;
                }

                // headers groups
                int colIndex = 1;
                var headersGroups = fields.Where(f => f.IsHeaderGroup);
                if (headersGroups.Any())
                {
                    foreach (var field in headersGroups)
                    {
                        var range = worksheet.Cells[rowIndex, colIndex, rowIndex, colIndex + field.ColSpan - 1];
                        range.Merge = true;
                        range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        range.Style.Font.Bold = true;
                        if (field.IsSpacer)
                        {
                            range.Value = "-----";
                            range.Style.Fill.BackgroundColor.SetColor(Color.White);
                            range.Style.Font.Color.SetColor(Color.White);
                        }
                        else
                        {
                            range.Value = field.Title.Trim();
                            range.Style.Fill.BackgroundColor.SetColor(Color.White);
                            range.Style.Font.Color.SetColor(Color.Black);
                            range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }

                        colIndex += field.ColSpan;
                    }
                    rowIndex++;
                }

                // headers
                colIndex = 1;
                var headers = fields.Where(f => !f.IsHeaderGroup);
                foreach (var field in headers)
                {
                    var cell = worksheet.Cells[rowIndex, colIndex];
                    cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    cell.Style.Font.Bold = true;
                    if (field.IsSpacer)
                    {
                        cell.Value = "-----";
                        cell.Style.Fill.BackgroundColor.SetColor(Color.White);
                        cell.Style.Font.Color.SetColor(Color.White);
                    }
                    else
                    {
                        cell.Value = field.Title.Trim();
                        cell.Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                        cell.Style.Font.Color.SetColor(Color.Black);
                        cell.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }

                    colIndex++;
                }
                rowIndex++;

                // groups
                foreach (ICollection group in dataGroups)
                {
                    // rows
                    foreach (object rowData in group)
                    {
                        // cols
                        for (colIndex = 1; colIndex <= fields.Count; colIndex++)
                        {
                            var field = fields[colIndex - 1];
                            var cell = worksheet.Cells[rowIndex, colIndex];

                            if (field.Method != null)
                            {
                                try
                                {
                                    DataInfo dataInfo;
                                    object result = field.Method.Invoke(rowData);
                                    if (result.GetType() == typeof(string))
                                        dataInfo = new DataInfo(result as string);
                                    else
                                        dataInfo = result as DataInfo;

                                    cell.Value = dataInfo.Data.Trim() == "%%spacer%%" ? "" : dataInfo.Data.Trim();
                                    if (dataInfo.Style == DataStyle.HighRisk)
                                        cell.Style.Font.Color.SetColor(Color.Red);
                                }
                                catch (Exception ex)
                                {
                                    Logger.Log(LogSeverity.Error, LogTag.Reports, "Error invoking report field.", string.Format("Data item:\n {0}\n\nField:\n {1}\n\nError:\n {2}", rowData.ToExtendedString(), field.ToExtendedString(), ex.ToString()));
                                }
                            }
                            else if (field.Property != null)
                            {
                                try
                                {
                                    object propertyValue = rowData;
                                    foreach (string propertyName in field.Property.Split('.'))
                                    {
                                        PropertyInfo propInfo = propertyValue.GetType().GetProperty(propertyName);
                                        if (propInfo == null)
                                            break;
                                        propertyValue = propInfo.GetValue(propertyValue, null);
                                        if (propertyValue == null)
                                            break;
                                    }

                                    if (propertyValue != null)
                                        cell.Value = propertyValue.ToString();
                                }
                                catch (Exception ex)
                                {
                                    Logger.Log(LogSeverity.Error, LogTag.Reports, "Error getting report field property.", string.Format("Data item:\n {0}\n\nField:\n {1}\n\nError:\n {2}", rowData.ToExtendedString(), field.ToExtendedString(), ex.ToString()));
                                }
                            }

                            if (cell.Value != null && cell.Value.ToString().IsNumeric())
                            {
                                cell.Style.Numberformat.Format = @"#,##0.00;[Red]\-#,##0.00"; //"0.00";
                                cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            }
                        }
                        rowIndex++;
                    }
                }

                worksheet.Cells.AutoFitColumns(0);
                worksheet.View.PageLayoutView = false;
                return package.GetAsByteArray();
            }
        }
    }
}
