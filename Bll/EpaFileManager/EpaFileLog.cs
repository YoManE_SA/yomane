﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Netpay.Infrastructure;

namespace Netpay.Bll.EpaFileManager
{
    public class EpaFileLog : BaseDataObject
    {
        private Dal.Netpay.tblEpaFileLog _entity;

        public int ID { get { return _entity.ID; } set { _entity.ID = value; } }

        public DateTime ActionDate { get { return _entity.ActionDate; } set { _entity.ActionDate = value; } }

        public int ActionType { get { return _entity.ActionType; } set { _entity.ActionType = value; } }

        public string StoredFileName { get { return _entity.StoredFileName; } set { _entity.StoredFileName = value; } }

        public string OriginalFileName { get { return _entity.OriginalFileName; } set { _entity.OriginalFileName = value; } }

        public string Details { get { return _entity.Details; } set { _entity.Details = value; } }

        public string Username { get { return _entity.Username; } set { _entity.Username = value; } }

        private EpaFileLog(Dal.Netpay.tblEpaFileLog entity)
        {
            _entity = entity;
        }

        public EpaFileLog()
        {
            _entity = new Dal.Netpay.tblEpaFileLog();
        }
        public static EpaFileLog Load(int EpaFileLogid)
        {
            if (EpaFileLogid > 0)
            {
                return (from dr in DataContext.Reader.tblEpaFileLogs
                        where dr.ID == EpaFileLogid
                        select new EpaFileLog(dr)).SingleOrDefault();
            }
            return null;
        }

        public void Save()
        {
            DataContext.Writer.tblEpaFileLogs.Update(_entity, (_entity.ID > 0));
            DataContext.Writer.SubmitChanges();
        }
        public void Delete()
        {
            if (_entity.ID == 0) return;
            DataContext.Writer.tblEpaFileLogs.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }

    }


}

