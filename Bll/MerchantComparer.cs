﻿using System.Collections.Generic;

namespace Netpay.Bll
{
	public class MerchantComparer : IEqualityComparer<Merchants.Merchant>
	{
		public bool Equals(Merchants.Merchant x, Merchants.Merchant y)
		{
			return ((x == null || y == null) ? false : (x.ID == y.ID));
		}

		public int GetHashCode(Merchants.Merchant obj)
		{
			return obj.Name.GetHashCode();
		}
	}
}
