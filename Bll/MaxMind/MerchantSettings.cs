﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Bll.Accounts;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.MaxMind
{
	public class MerchantSettings : BaseDataObject
	{
		public const string SecuredObjectName = "MerchantSettings";
		public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(MaxMind.Module.Current, SecuredObjectName); } }

		private Dal.Netpay.SetMerchantMaxmind _entity;
		private int _Merchant_id { get { return _entity.Merchant_id; } }

		public bool IsEnabled { get { return _entity.IsEnabled.GetValueOrDefault(); } set { _entity.IsEnabled = value; } }
		public decimal ScoreAllowed { get { return _entity.ScoreAllowed.GetValueOrDefault(); } set { _entity.ScoreAllowed = value; } }
		public decimal RiskScoreAllowed { get { return _entity.RiskScoreAllowed.GetValueOrDefault(); } set { _entity.RiskScoreAllowed = value; } }
		public bool IsAllowPublicEmail { get { return _entity.IsAllowPublicEmail.GetValueOrDefault(); } set { _entity.IsAllowPublicEmail = value; } }
		public string SkipIPList { get { return _entity.SkipIPList; } set { _entity.SkipIPList = value; } }
		public bool IsSkipVirtualTerminal { get { return _entity.IsSkipVirtualTerminal.GetValueOrDefault(); } set { _entity.IsSkipVirtualTerminal = value; } }

		private MerchantSettings(Netpay.Dal.Netpay.SetMerchantMaxmind entity)
		{ 
			_entity = entity;
		}

		public MerchantSettings(int merchantID)
			
		{
			_entity = new Dal.Netpay.SetMerchantMaxmind();
			_entity.Merchant_id = merchantID;
		}

		public static MerchantSettings Load(int merchantID)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

			AccountFilter.Current.Validate(Accounts.AccountType.Merchant, merchantID);
			var entity = (from mmas in DataContext.Reader.SetMerchantMaxminds where mmas.Merchant_id == merchantID select mmas).SingleOrDefault();
			if (entity == null) return null;
			return new MerchantSettings(entity);
		}

		public void Save()
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

			DataContext.Writer.SetMerchantMaxminds.Update(_entity, (_entity.Merchant_id != 0));
			DataContext.Writer.SubmitChanges();
		}
	}
}
