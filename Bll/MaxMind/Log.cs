﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.MaxMind
{
	public class Log : BaseDataObject
	{
        public const string SecuredObjectName = "Log";

        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(MaxMind.Module.Current, SecuredObjectName); } }

        public class SearchFilters
		{
			public Infrastructure.Range<int?> ID;
			public Infrastructure.Range<DateTime?> InsertDate;
			public int? MerchantId;

			public int? Currency;
			public Infrastructure.Range<decimal?> Amount;
		}
		public enum LogStatus { Processed, Rejected, RiskDecline }
		public static Dictionary<LogStatus, System.Drawing.Color> StatusColor = new Dictionary<LogStatus, System.Drawing.Color> { 
			{ LogStatus.Processed, System.Drawing.ColorTranslator.FromHtml("#66cc66") } , 
			{ LogStatus.Rejected, System.Drawing.ColorTranslator.FromHtml("#ff6666") }, 
			{ LogStatus.RiskDecline, System.Drawing.ColorTranslator.FromHtml("#606060") },
		};

		private Dal.Netpay.FraudDetection _entity;

		public int ID { get { return _entity.FraudDetection_id; } }
		public System.DateTime InsertDate { get { return _entity.InsertDate; } }
		public int? MerchantId { get { return _entity.Merchant_id; } }

		public int? TransPassId { get { return _entity.TransPass_id; } }
		public int? TransPreAuthId { get { return _entity.TransPreAuth_id; } }
		public int? TransPendingId { get { return _entity.TransPending_id; } }
		public int? TransFailId { get { return _entity.TransFail_id; } }

		public string SendingString { get { return _entity.SendingString; } }
		public string ReturnAnswer { get { return _entity.ReturnAnswer; } }
		public string ReturnExplanation { get { return _entity.ReturnExplanation; } }
		public decimal? ReturnScore { get { return _entity.ReturnScore; } }
		public decimal? ReturnRiskScore { get { return _entity.ReturnRiskScore; } }
		public string ReturnBinCountry { get { return _entity.ReturnBinCountry; } }
		public string ReturnQueriesRemaining { get { return _entity.ReturnQueriesRemaining; } }
		public string ReferenceCode { get { return _entity.ReferenceCode; } }
		public decimal? AllowedScore { get { return _entity.allowedScore; } }
		public string ReplyCode { get { return _entity.replyCode; } }
		public bool? IsTemperScore { get { return _entity.IsTemperScore; } }
		public bool? IsProceed { get { return _entity.IsProceed; } }
		public decimal Amount { get { return _entity.transAmount.GetValueOrDefault(); } }
		public CommonTypes.Currency Currency { get { return (CommonTypes.Currency)_entity.transCurrency.GetValueOrDefault(); } }
		public string PaymentMethodDisplay { get { return _entity.PaymentMethodDisplay; } }
		public string BinCountry { get { return _entity.BinCountry; } }
		public bool? IsDuplicateAnswer { get { return _entity.IsDuplicateAnswer; } }
		public string SendingIp { get { return _entity.SendingIp; } }
		public string SendingCity { get { return _entity.SendingCity; } }
		public string SendingRegion { get { return _entity.SendingRegion; } }
		public string SendingPostal { get { return _entity.SendingPostal; } }
		public string SendingCountry { get { return _entity.SendingCountry; } }
		public string SendingDomain { get { return _entity.SendingDomain; } }
		public string SendingBin { get { return _entity.SendingBin; } }
		public decimal? AllowedRiskScore { get { return _entity.AllowedRiskScore; } }
		public bool? IsCountryWhitelisted { get { return _entity.IsCountryWhitelisted; } }

		private Log(Dal.Netpay.FraudDetection entity)  { _entity = entity; }

		public LogStatus Status 
		{ 
			get {
				if (!_entity.IsProceed.GetValueOrDefault()) return LogStatus.RiskDecline; 
				else if(_entity.TransFail_id != null) return LogStatus.Rejected;
				return LogStatus.Processed; 
			} 
		}

		public static List<Log> Search(SearchFilters filters, ISortAndPage sortAndPage)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var exp = (from f in DataContext.Reader.FraudDetections select f);
			if (filters != null) {
				if (filters.ID.From.HasValue) exp = exp.Where(l => l.FraudDetection_id >= filters.ID.From);
				if (filters.ID.To.HasValue) exp = exp.Where(l => l.FraudDetection_id <= filters.ID.To);
				if (filters.InsertDate.From.HasValue) exp = exp.Where(l => l.InsertDate >= filters.InsertDate.From);
				if (filters.InsertDate.To.HasValue) exp = exp.Where(l => l.InsertDate <= filters.InsertDate.To);
				if (filters.MerchantId.HasValue) exp = exp.Where(l => l.Merchant_id == filters.MerchantId);

				if (filters.Amount.From.HasValue) exp = exp.Where(l => l.transAmount >= filters.Amount.From);
				if (filters.Amount.To.HasValue) exp = exp.Where(l => l.transAmount <= filters.Amount.To);
				if (filters.Currency.HasValue) exp = exp.Where(l => l.transCurrency == filters.Currency);
			}
			return exp.ApplySortAndPage(sortAndPage).Select(f => new Log(f)).ToList();
		}
	}
}
