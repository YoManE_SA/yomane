﻿using System;
using System.Collections.Generic;
using System.Linq;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Domains;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure.VO;
using Netpay.CommonTypes;

namespace Netpay.Bll
{
	public static class HostedPage
	{
		public static void SaveSettings(Guid credentialsToken, MerchantSettingsHostedVO settings)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary });

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			Netpay.Dal.Netpay.tblCompanySettingsHosted entity = (from ppo in dc.tblCompanySettingsHosteds where ppo.CompanyID == user.ID select ppo).SingleOrDefault();
			if (entity == null)
			{
				entity = new Netpay.Dal.Netpay.tblCompanySettingsHosted();
				entity.CompanyID = user.ID;
				dc.tblCompanySettingsHosteds.InsertOnSubmit(entity);
				dc.SubmitChanges();
			}

			entity.IsEnabled = settings.IsEnabled;
			entity.IsCreditCard = settings.IsCreditCard;
			entity.IsCreditCardRequiredEmail = settings.IsCreditCardRequiredEmail;
			entity.IsCreditCardRequiredPhone = settings.IsCreditCardRequiredPhone;
			entity.IsCreditCardRequiredID = settings.IsCreditCardRequiredID;
			entity.IsCreditCardRequiredCVV = settings.IsCreditCardRequiredCVV;
			entity.IsEcheck = settings.IsEcheck;
			entity.IsEcheckRequiredEmail = settings.IsEcheckRequiredEmail;
			entity.IsEcheckRequiredPhone = settings.IsEcheckRequiredPhone;
			entity.IsEcheckRequiredID = settings.IsEcheckRequiredID;
			entity.IsDirectDebit = settings.IsDirectDebit;
			entity.IsDirectDebitRequiredEmail = settings.IsDirectDebitRequiredEmail;
			entity.IsDirectDebitRequiredPhone = settings.IsDirectDebitRequiredPhone;
			entity.IsDirectDebitRequiredID = settings.IsDirectDebitRequiredID;
			entity.IsCustomer = settings.IsCustomer;
			entity.IsSignatureOptional = settings.IsSignatureOptional;
            entity.IsShowAddress1 = settings.IsShowAddress1;
            entity.IsShowAddress2 = settings.IsShowAddress2;
            entity.IsShowCity = settings.IsShowCity;
            entity.IsShowZipCode = settings.IsShowZipCode;
            entity.IsShowState = settings.IsShowState;
            entity.IsShowCountry = settings.IsShowCountry;
            entity.IsShowEmail = settings.IsShowEmail;
            entity.IsShowPhone = settings.IsShowPhone;
            entity.IsShowPersonalID = settings.IsShowPersonalID;
			entity.IsShippingAddressRequired = settings.IsShippingAddressRequire;
			entity.NotificationUrl = settings.NotificationUrl;
			entity.RedirectionUrl = settings.RedirectionUrl;
			entity.LogoPath = settings.LogoImage;
			entity.UIVersion = settings.UIVersion;
			entity.ThemeCssFileName = settings.ThemeCssFileName;
			entity.MerchantTextDefaultLanguage = (byte)settings.MerchantTextDefaultLanguage;
			dc.SubmitChanges();
		}

		public static CommonTypes.Language GetDefaultLanguage(Guid credentialsToken)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var value = (from ppo in dc.tblCompanySettingsHosteds where ppo.CompanyID == user.ID select ppo.MerchantTextDefaultLanguage).SingleOrDefault();
			if(value == null) return Language.English;
			return (Language)value;
		}

		/// <summary>
		/// Merchants hosted payment options
		/// </summary>
		public static MerchantSettingsHostedVO GetSettings(Guid credentialsToken)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary });

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			Netpay.Dal.Netpay.tblCompanySettingsHosted settings = (from o in dc.tblCompanySettingsHosteds where o.CompanyID == user.ID select o).FirstOrDefault();
			if (settings == null)
				return null;

			return new MerchantSettingsHostedVO(settings);
		}

		/// <summary>
		/// Merchants hosted payment options
		/// </summary>
		public static MerchantSettingsHostedVO GetSettings(string domainHost, string merchantNumber)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			Netpay.Dal.Netpay.tblCompanySettingsHosted settings = (from s in dc.tblCompanySettingsHosteds join merchants in dc.tblCompanies on s.CompanyID equals merchants.ID where merchants.CustomerNumber == merchantNumber select s).FirstOrDefault();
			if (settings == null)
				return null;
			return new MerchantSettingsHostedVO(settings);
		}

		/// <summary>
		/// Gets the merchant payment methods that are assigned fees and possibly terminals.
		/// </summary>
		/// <param name="domain"></param>
		/// <param name="merchantID"></param>
		/// <param name="currencyID"></param>
		/// <param name="popularOnly"></param>
		/// <returns></returns>
		public static PaymentMethodVO[] GetAvailablePaymentMethods(string domain, int merchantID, int currencyID, bool popularOnly)
		{
			// get available terminal methods
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domain).Sql1ConnectionString);
			List<short> terminalPaymentMethods = (from o in dc.tblCompanyCreditFees where o.CCF_CompanyID == merchantID && o.CCF_CurrencyID == currencyID && o.CCF_ExchangeTo != (short)Currency.UnsignedUnknown && o.CCF_IsDisabled == false && o.CCF_PaymentMethod.HasValue select o.CCF_PaymentMethod.Value).Distinct().ToList();

			// filter popular
			List<PaymentMethodVO> paymentMethods = DomainsManager.GetDomain(domain).Cache.PaymentMethods;
			if (popularOnly)
				paymentMethods = paymentMethods.Where(pm => pm.IsPopular).ToList();

			// filter terminal required
			List<PaymentMethodVO> results = new List<PaymentMethodVO>();
			foreach (PaymentMethodVO currentPaymentMethod in paymentMethods)
			{
				if (currentPaymentMethod.IsTerminalRequired)
				{
					if (terminalPaymentMethods.Contains((short)currentPaymentMethod.ID))
						results.Add(currentPaymentMethod);
				}
				else
					results.Add(currentPaymentMethod);
			}
			return results.ToArray();
		}

		public static List<PaymentMethodGroupVO> GetPaymentMethodGroups(string domain, bool isPopular)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domain).Sql1ConnectionString);
			List<PaymentMethodGroupVO> groups = (from o in dc.PaymentMethodGroups where o.IsPopular == isPopular orderby o.SortOrder select new PaymentMethodGroupVO(o)).ToList();

			return groups;
		}

		public static List<PaymentMethodGroupVO> GetPaymentMethodGroups(string domain, bool isPopular, string merchantNumber)
		{
			var merchant = Merchant.Merchant.Load(domain, merchantNumber);
			if (merchant == null)
				throw new ApplicationException("Merchant not found.");

			return GetPaymentMethodGroups(domain, isPopular, merchant);
		}

		/// <summary>
		/// Gets the payment groups for a merchant.
		/// Groups that are not enabled in the hosted page settings are excluded.
		/// </summary>
		/// <param name="domain"></param>
		/// <param name="isPopular"></param>
		/// <param name="merchant"></param>
		/// <returns></returns>
		public static List<PaymentMethodGroupVO> GetPaymentMethodGroups(string domain, bool isPopular, Merchant.Merchant merchant)
		{
			List<PaymentMethodGroupVO> groups = GetPaymentMethodGroups(domain, isPopular);
			MerchantSettingsHostedVO settings = HostedPage.GetSettings(domain, merchant.Number);
			if (settings == null)
				return null;
			PaymentMethodGroupVO group = null;
			for (int i = groups.Count - 1; i >= 0; i--)
			{
				group = groups[i];
				switch (group.Type)
				{
					case PaymentMethodType.CreditCard: if (!settings.IsCreditCard) groups.Remove(group); break;
					case PaymentMethodType.ECheck: if (!settings.IsEcheck) groups.Remove(group); break;
					case PaymentMethodType.BankTransfer: if (!settings.IsDirectDebit) groups.Remove(group); break;
					case PaymentMethodType.PhoneDebit: if (!settings.IsPhoneDebit) groups.Remove(group); break;
					case PaymentMethodType.Wallet: if (!settings.IsCustomer) groups.Remove(group); break;
					case PaymentMethodType.ExternalWallet: if (!settings.IsWebMoney && !settings.IsPayPal && !settings.IsGoogleCheckout && !settings.IsMoneyBookers) groups.Remove(group); break;
				}
			}

			return groups;
		}
	}
}
