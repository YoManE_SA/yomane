﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.DataAccess;
using Netpay.Dal.Netpay;

using Netpay.Infrastructure;

namespace Netpay.Bll.ThirdParty
{
	public class Facebook
	{
		public static string GetMerchantNumber(long pageID) 
		{
			return (from mn in DataContext.Reader.FacebookPageToMerchants where mn.FBPageID == pageID select mn.Merchant).SingleOrDefault();
		}

		public static bool AddMerchantPage(string merchantNumber, long pageID)
		{
			var merchant = Merchants.Merchant.Load(merchantNumber);
			if (merchant == null)
				return false;

			FacebookPageToMerchant entity = (from fbptm in DataContext.Reader.FacebookPageToMerchants where fbptm.FBPageID == pageID select fbptm).SingleOrDefault();
			if (entity == null)
			{
				entity = new FacebookPageToMerchant();
				entity.Merchant = merchantNumber;
				entity.FBPageID = pageID;
				DataContext.Writer.FacebookPageToMerchants.InsertOnSubmit(entity);
			}
			else
			{
				entity.Merchant = merchantNumber;
			}

			DataContext.Writer.SubmitChanges();
			return true;
		}
	}
}
