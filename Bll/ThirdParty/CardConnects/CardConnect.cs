﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Linq;
using Netpay.Dal.DataAccess;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;

using Netpay.Infrastructure.Security;

namespace Netpay.Bll.ThirdParty
{
    public static class CardConnect
    {
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(CardConnects.Module.Current, SecuredObjectName); } }

        private static object syncRoot = new object();
		private static Dictionary<string, string> credentials = null;
		private static Dictionary<string, string> businessTypes = null;
		private static Dictionary<string, string> industries = null;

		private static string ServiceBaseUrl
		{
			get
			{
				if (Infrastructure.Application.IsProduction)
					return "https://cardconnect.merchantinfoonline.com/agentcenter/";
				else
					return "https://testapi.merchantinfoonline.com/agentcenter/";
			}
		}

		private static Dictionary<string, string> GetCredentials()
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);


            if (credentials == null)
			{
				lock (syncRoot)
				{
					if (credentials == null)
					{
						tblDebitCompany debitCompany = (from debCom in DataContext.Reader.tblDebitCompanies where debCom.DebitCompany_ID == 60 select debCom).SingleOrDefault();
						string userName = debitCompany.RegistrationUsrname;

                        if (debitCompany.RegistrationPassword256 != null)
                        {
                            string password = Encryption.Decrypt(debitCompany.RegistrationPassword256.ToArray());

                            credentials = new Dictionary<string, string>();
                            credentials.Add("username", userName);
                            credentials.Add("password", password);
                        }
                        else
                        {
                            string password = null;

                            credentials = new Dictionary<string, string>();
                            credentials.Add("username", userName);
                            credentials.Add("password", password);
                        }
					}
				}
			}

			return credentials;
		}

		public static Dictionary<string, string> GetBusinessTypes()
		{           

            if (businessTypes == null)
			{
				lock (syncRoot)
				{
					if (businessTypes == null)
					{
						Dictionary<string, string> creds = GetCredentials();
						XElement root = new XElement("businessTypeListRequest");
						HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ServiceBaseUrl + "businesstype/list");
						request.Method = "POST";
						request.Accept = "application/xml";
						request.ContentType = "application/xml";
						request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(creds["username"] + ":" + creds["password"])));

						WebResponse response = null;
						try
						{
							byte[] dataBytes = root.ToString().ToBytes();
							request.GetRequestStream().Write(dataBytes, 0, dataBytes.Length);
							response = request.GetResponse();
						}
						catch (Exception ex)
						{
							Logger.Log(ex);
							return new Dictionary<string, string>();
						}
						string responseData = new StreamReader(response.GetResponseStream()).ReadToEnd();
						XElement responseXml = XElement.Parse(responseData);

						Dictionary<string, string> resuls = new Dictionary<string, string>();
						var types = responseXml.Descendants("businessTypes");
						foreach (XElement type in types)
						{
							string id = type.Descendants("id").Single().Value;
							string desc = type.Descendants("description").Single().Value;
							resuls.Add(id, desc);
						}

						businessTypes = resuls.OrderBy(i => i.Value).ToDictionary(k => k.Key, v => v.Value);
					}
				}
			}

			return businessTypes;
		}

		public static Dictionary<string, string> GetIndustries()
		{
			if (industries == null)
			{
				lock (syncRoot)
				{
					if (industries == null)
					{
						Dictionary<string, string> creds = GetCredentials();
						XElement root = new XElement("industryListRequest");
						HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ServiceBaseUrl + "industry/list");
						request.Method = "POST";
						request.Accept = "application/xml";
						request.ContentType = "application/xml";
						request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(creds["username"] + ":" + creds["password"])));

						WebResponse response = null;
						try
						{
							byte[] dataBytes = root.ToString().ToBytes();
							request.GetRequestStream().Write(dataBytes, 0, dataBytes.Length);
							response = request.GetResponse();
						}
						catch (Exception ex)
						{
							Logger.Log(ex);
							return new Dictionary<string, string>();
						}
						string responseData = new StreamReader(response.GetResponseStream()).ReadToEnd();
						XElement responseXml = XElement.Parse(responseData);

						Dictionary<string, string> resuls = new Dictionary<string, string>();
						var industriesNodes = responseXml.Descendants("industries");
						foreach (XElement industry in industriesNodes)
						{
							string id = industry.Descendants("id").Single().Value;
							string desc = industry.Descendants("description").Single().Value;
							resuls.Add(id, desc);
						}

						industries = resuls.OrderBy(i => i.Value).ToDictionary(k => k.Key, v => v.Value);
					}
				}
			}

			return industries;	
		}

		public static bool SendAttachment(Guid registrationID, out string resultMessage)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
            {
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Merchants.Merchant.SecuredObject, PermissionValue.Add);
            }

            Dictionary<string, string> creds = GetCredentials();

			var reg = Merchants.Registration.Load(registrationID);
			if (reg == null)
			{
				resultMessage = "Registration not found";
				return false;
			}

			XElement attachmentCreateRequest = new XElement("attachmentCreateRequest");
			XElement attachment = new XElement("attachment");
			attachmentCreateRequest.Add(attachment);
			attachment.Add(new XElement("fileName", reg.CanceledCheckImageFileName));
			attachment.Add(new XElement("mimeType", reg.CanceledCheckImageMimeType));
			attachment.Add(new XElement("description", "Canceled check"));
			attachment.Add(new XElement("attachmentType", "VC"));
			attachment.Add(new XElement("document", reg.CanceledCheckImageContent));
			attachment.Add(new XElement("merchantId", reg.IntegrationData.ContainsKey("CardConnectID") ? reg.IntegrationData["CardConnectID"] : ""));

			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ServiceBaseUrl + "attachment/create");
			request.Method = "PUT";
			request.Accept = "application/xml";
			request.ContentType = "application/xml";
			request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(creds["username"] + ":" + creds["password"])));
			byte[] dataBytes = attachmentCreateRequest.ToString().ToBytes();
			request.GetRequestStream().Write(dataBytes, 0, dataBytes.Length);

			HttpWebResponse response = (HttpWebResponse)request.GetResponse();
			string responseData = new StreamReader(response.GetResponseStream()).ReadToEnd();
			XElement result = XElement.Parse(responseData);
			XElement errorNode = result.Descendants("error").SingleOrDefault();

			if (errorNode != null)
			{
				resultMessage = errorNode.Descendants("code").First().Value + ": " + errorNode.Descendants("message").First().Value;
				return false;
			}
			else
			{
				string attachmentId = result.Descendants("id").Single().Value;
				reg.IntegrationData["CardConnectCheckImageID"] = attachmentId;
				reg.Save();
				resultMessage = "Attachment CardConnect ID: " + attachmentId;
				return true;
			}
		}

		public static bool CreateMerchant(Guid registrationID, out string resultMessage)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
            {
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Bll.Merchants.Merchant.SecuredObject , PermissionValue.Add);
            }
            
                //User user = SecurityManager.GetInternalUser(new UserRole[] { UserRole.NetpayAdmin, UserRole.NetpayUser });
                Dictionary<string, string> creds = GetCredentials();

			var reg = Merchants.Registration.Load(registrationID);
			if (reg == null)
			{
				resultMessage = "Registration not found";
				return false;
			}

			XElement merchantCreateRequest = new XElement("merchantCreateRequest");
			XElement merchant = new XElement("merchant");
			merchantCreateRequest.Add(merchant);
			merchant.Add(new XElement("id", 0));
			merchant.Add(new XElement("dbaName", reg.DbaName));
			merchant.Add(new XElement("legalBusinessName", reg.LegalBusinessName));
			merchant.Add(new XElement("taxId", reg.LegalBusinessNumber));
			merchant.Add(new XElement("firstName", reg.FirstName));
			merchant.Add(new XElement("lastName", reg.LastName));
			merchant.Add(new XElement("email", reg.Email));
			merchant.Add(new XElement("address", reg.Address));
			merchant.Add(new XElement("city", reg.City));
			merchant.Add(new XElement("state", reg.State));
			merchant.Add(new XElement("zip", reg.Zipcode));
			merchant.Add(new XElement("ownerDob", reg.OwnerDob == null ? "18000101" : reg.OwnerDob.Value.ToString("yyyyMMdd")));
			merchant.Add(new XElement("ownerSsn", reg.OwnerSsn));
			merchant.Add(new XElement("phone", reg.Phone));
			merchant.Add(new XElement("url", reg.Url));
			merchant.Add(new XElement("physicalAddress", reg.PhisicalAddress));
			merchant.Add(new XElement("physicalCity", reg.PhisicalCity));
			merchant.Add(new XElement("physicalState", reg.PhisicalState));
			merchant.Add(new XElement("physicalZip", reg.PhisicalZip));
			merchant.Add(new XElement("stateOfInc", reg.StateOfIncorporation));
			merchant.Add(new XElement("typeOfBusiness", reg.TypeOfBusiness == null ? 0 : reg.TypeOfBusiness.Value));
			merchant.Add(new XElement("businessStartDate", reg.BusinessStartDate == null ? "18000101" : reg.BusinessStartDate.Value.ToString("yyyyMMdd")));
			merchant.Add(new XElement("industry", reg.Industry == null ? 0 : reg.Industry.Value));
			merchant.Add(new XElement("businessDescription", reg.BusinessDescription));
			merchant.Add(new XElement("delivery0to7", reg.PercentDelivery0to7));
			merchant.Add(new XElement("delivery8to14", reg.PercentDelivery8to14));
			merchant.Add(new XElement("delivery15to30", reg.PercentDelivery15to30));
			merchant.Add(new XElement("deliveryOver30", reg.PercentDeliveryOver30));
			merchant.Add(new XElement("monthlyVolume", reg.AnticipatedMonthlyVolume.Value.ToAmountFormat()));
			merchant.Add(new XElement("salesCode", "LT18"));
			merchant.Add(new XElement("averageTicket", reg.AnticipatedAverageTransactionAmount.Value.ToAmountFormat()));
			merchant.Add(new XElement("highTicket", reg.AnticipatedLargestTransactionAmount.Value.ToAmountFormat()));
			merchant.Add(new XElement("taxId", ""));
			XElement application = new XElement("application");
			merchant.Add(application);
			application.Add(new XElement("bankRouting", reg.BankRoutingNumber));
			application.Add(new XElement("bankAccount", reg.BankAccountNumber));
			application.Add(new XElement("apexMid", 0));

			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ServiceBaseUrl + "merchant/create");
			request.Method = "PUT";
			request.Accept = "application/xml";
			request.ContentType = "application/xml";
			request.Headers.Add(HttpRequestHeader.Authorization, "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(creds["username"] + ":" + creds["password"])));
			byte[] dataBytes = merchantCreateRequest.ToString().ToBytes();
			request.GetRequestStream().Write(dataBytes, 0, dataBytes.Length);

			WebResponse response = request.GetResponse();
			string responseData = new StreamReader(response.GetResponseStream()).ReadToEnd();
			Process.GenericLog.Log(HistoryLogType.CardConnectRegister, reg.MerchantID, merchantCreateRequest.ToString(), responseData);

			XElement result = XElement.Parse(responseData);
			XElement errorNode = result.Descendants("error").SingleOrDefault();
			if (errorNode != null)
			{
				resultMessage = errorNode.Descendants("code").First().Value + ": " + errorNode.Descendants("message").First().Value;
				return false;
			}
			else
			{
				string merchantId = result.Descendants("id").Single().Value;
				resultMessage = "Merchant CardConnect ID: " + merchantId;
				//reg.IntegrationData["CardConnect_IsRegistered"] = "Yes";
				reg.IntegrationData["CardConnectID"] = merchantId;
				reg.Save();

				return true;
			}
		}
	}
}
