﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.DataAccess;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;

using Netpay.Infrastructure.Security;

namespace Netpay.Bll.ThirdParty
{
	public class ZohoCRM
	{
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Zoho.Module.Current, SecuredObjectName); } }

        private static string ServiceBaseUrl
		{
			get
			{
				return "https://crm.zoho.com/crm/private/xml/Leads/insertRecords";
			}
		}

		public static bool CreateLead(Guid registrationID, out string resultMessage)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
            {
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);
            }

			var reg = Merchants.Registration.Load(registrationID);
			if (reg == null)
			{
				resultMessage = "Registration not found";
				return false;
			}

			var sb = new System.Text.StringBuilder();
			sb.AppendFormat("<Leads>\r\n");
			sb.AppendFormat(" <row no=\"1\">\r\n");
			//if (reg.IntegrationData.ContainsKey("ZohoCRMID")) sb.AppendFormat("  <FL val=\"Id\">{0}</FL>\r\n", reg.IntegrationData["ZohoCRMID"].ToEncodedHtml());
			sb.AppendFormat("  <FL val=\"First Name\">{0}</FL>\r\n", reg.FirstName.ToEncodedHtml());
			sb.AppendFormat("  <FL val=\"Last Name\">{0}</FL>\r\n", reg.LastName.ToEncodedHtml());
			sb.AppendFormat("  <FL val=\"Title\">{0}</FL>\r\n", "");
			sb.AppendFormat("  <FL val=\"Email\">{0}</FL>\r\n", reg.Email.ToEncodedHtml());
			sb.AppendFormat("  <FL val=\"Fax\">{0}</FL>\r\n", reg.Fax.ToEncodedHtml());
			sb.AppendFormat("  <FL val=\"Street\">{0}</FL>\r\n", reg.Address.ToEncodedHtml());
			sb.AppendFormat("  <FL val=\"City\">{0}</FL>\r\n", reg.City.ToEncodedHtml());
			sb.AppendFormat("  <FL val=\"State\">{0}</FL>\r\n", reg.State.ToEncodedHtml());
			sb.AppendFormat("  <FL val=\"Zip Code\">{0}</FL>\r\n", reg.Zipcode.ToEncodedHtml());
			sb.AppendFormat("  <FL val=\"Business Phone\">{0}</FL>\r\n", reg.Phone.ToEncodedHtml());
			sb.AppendFormat("  <FL val=\"Website Address\">{0}</FL>\r\n", reg.Url.ToEncodedHtml());
			sb.AppendFormat("  <FL val=\"Owner dob\">{0}</FL>\r\n", (reg.OwnerDob == null ? "01/01/1901" : reg.OwnerDob.Value.ToString("MM/dd/yyyy")).ToEncodedHtml());
			sb.AppendFormat("  <FL val=\"Owner ssn\">{0}</FL>\r\n", reg.OwnerSsn.ToEncodedHtml());
			sb.AppendFormat("  <FL val=\"Business Phisical address\">{0}</FL>\r\n", reg.PhisicalAddress.ToEncodedHtml());
			sb.AppendFormat("  <FL val=\"Business Phisical city\">{0}</FL>\r\n", reg.PhisicalCity.ToEncodedHtml());
			sb.AppendFormat("  <FL val=\"Business Phisical state\">{0}</FL>\r\n", reg.PhisicalState.ToEncodedHtml());
			sb.AppendFormat("  <FL val=\"Business Phisical zip\">{0}</FL>\r\n", reg.PhisicalZip.ToEncodedHtml());
			sb.AppendFormat("  <FL val=\"State of incorporation\">{0}</FL>\r\n", reg.StateOfIncorporation.ToEncodedHtml());
			sb.AppendFormat("  <FL val=\"Type of business\">{0}</FL>\r\n", reg.TypeOfBusiness == null ? 0 : reg.TypeOfBusiness.Value);
			sb.AppendFormat("  <FL val=\"Business start date\">{0}</FL>\r\n", (reg.BusinessStartDate == null ? "01/01/1901" : reg.BusinessStartDate.Value.ToString("MM/dd/yyyy")).ToEncodedHtml());
			sb.AppendFormat("  <FL val=\"Business description\">{0}</FL>\r\n", reg.BusinessDescription.ToEncodedHtml());
			sb.AppendFormat("  <FL val=\"Anticipated monthly volume\">{0}</FL>", reg.AnticipatedMonthlyVolume.Value.ToAmountFormat());
			sb.AppendFormat("  <FL val=\"Anticipated average transaction amount\">{0}</FL>\r\n", reg.AnticipatedAverageTransactionAmount.Value.ToAmountFormat());
			sb.AppendFormat("  <FL val=\"Anticipated largest transaction amount\">{0}</FL>\r\n", reg.AnticipatedLargestTransactionAmount.Value.ToAmountFormat());
			//sb.AppendFormat("  <FL val=\"Bank routing number\">{0}</FL>\r\n", reg.BankRoutingNumber.ToEncodedHtml());
			//sb.AppendFormat("  <FL val=\"Bank account number\">{0}</FL>\r\n", reg.BankAccountNumber.ToEncodedHtml());
			sb.AppendFormat("  <FL val=\"% of Goods Delivered in 0 to 7 days\">{0}</FL>\r\n", reg.PercentDelivery0to7);
			sb.AppendFormat("  <FL val=\"% of Goods Delivered in 15 to 30 days\">{0}</FL>\r\n", reg.PercentDelivery15to30);
			sb.AppendFormat("  <FL val=\"% of Goods Delivered in 8 to 14 days\">{0}</FL>\r\n", reg.PercentDelivery8to14);
			sb.AppendFormat("  <FL val=\"% of Goods Delivered over 30 days\">{0}</FL>\r\n", reg.PercentDeliveryOver30);
			sb.AppendFormat("  <FL val=\"Company\">{0}</FL>\r\n", reg.LegalBusinessName);
			sb.AppendFormat("  <FL val=\"FEIN\">{0}</FL>\r\n", reg.LegalBusinessNumber);
			sb.AppendFormat("  <FL val=\"DBA Name\">{0}</FL>\r\n", reg.DbaName.ToEncodedHtml());

			if (reg.IntegrationData.ContainsKey("CardConnectID")) sb.AppendFormat("  <FL val=\"Card Connect ID\">{0}</FL>\r\n", reg.IntegrationData["CardConnectID"].ToEncodedHtml());
			if (reg.IntegrationData.ContainsKey("CardConnectCheckImageID")) sb.AppendFormat("  <FL val=\"Card Connect Image ID\">{0}</FL>\r\n", reg.IntegrationData["CardConnectCheckImageID"].ToEncodedHtml());

			sb.AppendFormat(" </row>\r\n");
			sb.AppendFormat("</Leads>");

			string serviceUrl = ServiceBaseUrl + "?newFormat=1&scope=crmapi&duplicateCheck=2&authtoken=" + Domain.Current.GetConfig("ZohoCRMAuth");
			string retData;
			var ret = Infrastructure.HttpClient.SendHttpRequest(serviceUrl, out retData, "xmlData=" + sb.ToString().ToEncodedUrl(), null, 0, null);
			if (ret != System.Net.HttpStatusCode.OK){
				resultMessage = string.Format("HTTP Error: {0} - {1}", (int)ret, ret);
				return false;
			}
			resultMessage = "Unknown Reply Format";
			var result = System.Xml.Linq.XElement.Parse(retData);
			var retItem = result.Descendants("message").SingleOrDefault();
			if (retItem != null) resultMessage = retItem.Value;
			retItem = result.Descendants("FL").Where(r => r.Attribute("val").Value == "Id").SingleOrDefault();
			if (retItem != null) {
				reg.IntegrationData["ZohoCRMID"] = retItem.Value;
				resultMessage += " - New ID: " + retItem.Value;
				reg.Save();
			}
			return true;
		}

	}
}
