﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.Bll.ThirdParty
{
    public class MulticatC
    {
        public class GetCardDetailResponse
        {
            public string ResponseCode { get; set; }
            public string ResponseMessage { get; set; }
            public CardDetail[] CardDetailList { get; set; }

            public class CardDetail
            {
                public string CardStatus { get; set; }
                public string CardNumber { get; set; }
                public string ReferenceID { get; set; }
                public string CustomerCode { get; set; }
                public string CustomerName { get; set; }
                public string CardHolderName { get; set; }
                public string MobileNumber { get; set; }
                public decimal CardBalance { get; set; }
            }
        }

        public class RegisterCardResponse
        {
            public string ResponseCode { get; set; }
            public string ResponseMessage { get; set; }
            public string CardNumber { get; set; }
        }

        public class CardTransactionResponse
        {
            public string ResponseCode { get; set; }
            public string ResponseMessage { get; set; }
            public string TransactionID { get; set; }
        }
    }
}
