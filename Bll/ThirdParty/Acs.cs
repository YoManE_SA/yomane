﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Netpay.Infrastructure;

namespace Netpay.Bll.ThirdParty
{
    public class ACS : Iso8583Base
    {
        /*
        Comp1:                
        1B2C	3D4E	5F67	890A	BDFA	1350	FDEC	9876
        Comp2:
        1245	BCEF	2580	FECB	1478	DBFE	6547	CFEB
        Comp3:
        5231	FCDB	4321	BEFD	BCDF	5798	BCDE	B123
        */
        public static readonly string[] chMASTER_KEY_DEV = { "1B2C3D4E5F67890ABDFA1350FDEC9876", "1245BCEF2580FECB1478DBFE6547CFEB", "5231FCDB4321BEFDBCDF5798BCDEB123" };
        //public static readonly string[] chMASTER_KEY_DEV = { "1133557799199776", "B210A3BFD5EE9843" };
        //private static readonly string[]  chMASTER_KEY_PRD = { "84ED0F94D10C0EA4", "9A671A634B2535C2" };
        private const int chKEY_SIZE = 128; // 128bit for Triple DES
        private const int chTCP_SERVER_PORT = 52015;
        public const string chSTOREDPM_PROVIDERNAME = "System.ACS";
        public const int chDEBITCOMPANY_ID = 80;
        public const string chCONFIG_FILE_KEY_NAME = "ACS_KEY";

        public ACS(string domainHost, int listenPort = chTCP_SERVER_PORT) : base(Iso8583Factory.Implementation.ACS, chMASTER_KEY_DEV, chKEY_SIZE, chTCP_SERVER_PORT, chSTOREDPM_PROVIDERNAME, chDEBITCOMPANY_ID, "ACS", chCONFIG_FILE_KEY_NAME)
        {
            DomainHost = domainHost;
            ServerPort = listenPort;
            KeepAliveSeconds = 30;
        }

        public static void EnableServers(bool bEnable)
        {
            Iso8583Base.EnableServers(Iso8583Factory.Implementation.ACS, bEnable, "ACS_Enable", "ACS_Log");
        }
    }
}