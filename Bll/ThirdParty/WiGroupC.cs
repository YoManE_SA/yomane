﻿using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.Bll.ThirdParty
{
    public partial class WiGroupC
    {
        public class WiGroupExecuteTransactionResponse
        {
            public int ReplyCode { get; set; }
            public string ReplyMessage { get; set; }
        }

        public static WiGroupExecuteTransactionResponse ExecuteTransaction(WiJSON.Transaction pTransaction, string strIPAddress)
        {
            WiGroupExecuteTransactionResponse pResponse = new WiGroupExecuteTransactionResponse();
            Infrastructure.ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);

            Netpay.Dal.Netpay.NetpayDataContextBase dc = new Dal.Netpay.NetpayDataContextBase(Domain.Current.Sql1ConnectionString);

            Netpay.Dal.Netpay.CustomerWiCode pWiCode = (from p in dc.CustomerWiCodes
                                                        where p.WiCode == pTransaction.token.id
                                                        select p).SingleOrDefault();

            pWiCode.wiTrxId = pTransaction.wiTrxId;
            dc.SubmitChanges();

            int DEBITCOMPANY_ID = 67; // eCentric
            string CLASS_NAME = "WiGroupC";

            if (pWiCode == null)
            {
                pResponse.ReplyCode = 6;
                pResponse.ReplyMessage = "WiCode not found";
                return pResponse; //Error
            }
            else if (pWiCode.DateExpire <= DateTime.Now)
            {
                pResponse.ReplyCode = 6;
                pResponse.ReplyMessage = "WiCode expired";
                return pResponse; //Error
            }

            else if (pWiCode.DateClaimed.HasValue)
            {
                pResponse.ReplyCode = 6;
                pResponse.ReplyMessage = "WiCode already claimed";
                return pResponse; //Error
            }

            try
            {
                // ZAR
                var curObj = Bll.Currency.GetByIsoNumber(710);

                #region GENERIC

                // Find Customer
                var pCustomer = (from p in dc.Customers
                                 where p.Customer_id == pWiCode.Customer_id
                                 select p).SingleOrDefault();

                var pAccount = (from p in dc.Accounts
                                where p.Customer_id == pWiCode.Customer_id
                                select p).SingleOrDefault();

                var spms = Bll.Accounts.StoredPaymentMethod.LoadForAccount(pAccount.Account_id);
                var spm = spms.FirstOrDefault(); //.Where(s => s.ProviderID == STOREDPM_PROVIDERNAME)

                if (spm == null)
                {
                    pResponse.ReplyCode = 14;
                    pResponse.ReplyMessage = "Invalid card number";
                    return pResponse; //Error
                    //ret = -14; //Invalid card number
                }
                else if (pTransaction.totalAmount <= 0)
                {
                    pResponse.ReplyCode = 13;
                    pResponse.ReplyMessage = "invalid amount";
                    return pResponse; //Error
                    //ret = -13; //invalid amount
                }
                else if (curObj == null)
                {
                    pResponse.ReplyCode = 39;
                    pResponse.ReplyMessage = "No credit account";
                    return pResponse; //Error
                    //ret = -39; //No credit account
                }

                /// TODO: WiGroup - Add back for expiration date checks.
                //if (spm.MethodInstance.ExpirationDate.GetValueOrDefault(DateTime.MaxValue) < DateTime.Now)
                //{
                //    pResponse.ReplyCode = -54;
                //    pResponse.ReplyMessage = "Expired card";
                //    return pResponse; //Error
                //    //ret = -54; //Expired card
                //}

                #endregion

                #region PAYMENT / WITHDRAWAL

                if (pTransaction.type == "PAYMENT"|| pTransaction.type == "WITHDRAWAL")
                {

                    var terminal = Bll.DebitCompanies.Terminal.LoadWithExtAccountID(DEBITCOMPANY_ID, pTransaction.storeTrxDetails.storeId.ToString());

                    if (terminal == null)
                    {
                        pResponse.ReplyCode = 6;
                        pResponse.ReplyMessage = "Merchant Terminal not found";
                        return pResponse; //Error
                    }

                    var refTermianl = (from n in DataContext.Reader.tblCompanyCreditFeesTerminals where n.CCFT_Terminal == terminal.TerminalNumber select n).FirstOrDefault();
                    if (refTermianl.CCFT_CompanyID == null)
                    {
                        pResponse.ReplyCode = 6;
                        pResponse.ReplyMessage = "Terminal fees not found";
                        return pResponse; //Error
                    }


                    var currenctBalace = Bll.Accounts.Balance.GetStatus(spm.Account_id, curObj.IsoCode).Expected;
                    if (Bll.Accounts.Balance.GetStatus(spm.Account_id, curObj.IsoCode).Expected < (pTransaction.totalAmount / 100.0m))
                    {
                        pResponse.ReplyCode = 51;
                        pResponse.ReplyMessage = "Not sufficient funds";
                        return pResponse; //Error
                                          //ret = -51; //Not sufficient funds
                    }

                    Bll.Customers.Customer customer = Customers.Customer.LoadObject(spm.Account_id) as Customers.Customer;

                    //validate merchant group by app identity
                    var merchantGroup = Merchants.Merchant.GetMerchantsGroup(new List<int>() { refTermianl.CCFT_CompanyID.GetValueOrDefault() }).FirstOrDefault().Value;
                    if (!customer.CanAcceptMerchantGroup(null))
                    {
                        pResponse.ReplyCode = 56;
                        pResponse.ReplyMessage = "No card record";
                        return pResponse; //Error
                                          //ret = -56; //No card record
                    }

                    int OriginalTransId = 0;
                    string authorizationCode = DateTime.Now.Ticks.ToString("000000").TruncStart(6);

                    int ret = pResponse.ReplyCode;

                    Iso8583Base.Iso8583PostXml pXMLRequest = new Iso8583Base.Iso8583PostXml(new Iso8583Base(Iso8583Factory.Implementation.WiGroup
                        , new string[] { "1133557799199776", "B210A3BFD5EE9843" }
                        , 64
                        , 10
                        , "WiGroup"
                        , DEBITCOMPANY_ID
                        , CLASS_NAME
                        , "WiGroup"));

                    pXMLRequest.Amount = Convert.ToDecimal(pTransaction.totalAmount / 100.0m);
                    pXMLRequest.ReferenceNumber = pTransaction.wiTrxId;
                    pXMLRequest.ReferenceCode = pTransaction.wiTrxId.ToString();
                    pXMLRequest.MessageType = "0200";

                    string strValue1 = "";
                    string strValue2 = "";
                    spm.MethodInstance.GetDecryptedData(out strValue1, out strValue2);
                    pXMLRequest.PAN = strValue1;
                    string pin = Infrastructure.Security.Encryption.DecodeCvv(pAccount.PincodeSHA256);

                    var trans = new Transactions.Transaction()
                    {
                        Status = (ret < 0 ? TransactionStatus.Declined : TransactionStatus.Captured),
                        ReplyCode = System.Math.Abs(ret).ToString("000"),
                        TerminalNumber = terminal.TerminalNumber,
                        PaymentsIDs = ";0;",
                        IP = strIPAddress.ToString(),
                        OrderNumber = pXMLRequest.ReferenceNumber.GetValueOrDefault().ToString(),
                        Amount = pXMLRequest.Amount,
                        CurrencyID = curObj.ID,
                        MerchantID = refTermianl.CCFT_CompanyID.GetValueOrDefault(),
                        DebitReferenceCode = pXMLRequest.ReferenceCode,
                        DebitCompanyID = DEBITCOMPANY_ID,
                        InsertDate = DateTime.Now,
                        ApprovalNumber = authorizationCode,
                        Installments = 1,
                        TransactionSource = TransactionSource.DebitCards,
                        TransType = TransactionType.Capture,
                        CreditType = (pXMLRequest.BalanceAmount < 0 ? CreditType.Regular : CreditType.Refund),
                        CustomerID = customer.CustomerID,
                        //UnsettledInstallments = 1,
                        MerchantPayDate = DateTime.Now.Date.AddDays(1).Date,
                        PayForText = CLASS_NAME + " POS",
                        OriginalTransactionID = OriginalTransId,
                        PaymentData = new Transactions.Payment() { MerchantID = refTermianl.CCFT_CompanyID.GetValueOrDefault() },
                        PayerData = new Transactions.Payer() { FullName = customer.AccountName, EmailAddress = customer.EmailAddress, PersonalNumber = customer.PersonalNumber, PhoneNumber = customer.PhoneNumber }
                    };
                    if (trans.Status == TransactionStatus.Captured) trans.BatchData = pXMLRequest.OuterXml;
                    trans.PaymentData.MethodInstance = new PaymentMethods.CreditCard(trans.PaymentData) { CardNumber = pXMLRequest.PAN, Cvv = pin, ExpirationDate = pXMLRequest.ExpDate, PaymentMethodId = spm.MethodInstance.PaymentMethodId };

                    var refFees = Merchants.ProcessTerminals.Load(refTermianl.CCFT_CCF_ID.GetValueOrDefault());
                    if (refFees != null) trans.SetTransactionFees(refFees);
                    trans.Save();
                    if (ret > 0)
                    {
                        return pResponse; //Error
                    }

                    var balance = Bll.Accounts.Balance.Create(spm.Account_id, curObj.IsoCode, pXMLRequest.BalanceAmount, CLASS_NAME + ": " + pXMLRequest.ReferenceNumber, Bll.Accounts.Balance.SOURCE_PURCHASE, trans.ID, true);

                    if (refFees != null && refFees.CashbackPercent != 0 && trans.CreditType == CreditType.Regular)
                    {
                        var cashbackAmount = (refFees.CashbackPercent * pXMLRequest.Amount) / 100;
                        var cashbackTrans = trans.CreateRefundTransaction(cashbackAmount);
                        cashbackTrans.IsCashback = true;
                        cashbackTrans.Comment = "Cashback from trans #" + trans.ID;
                        cashbackTrans.Save();
                        Bll.Accounts.Balance.Create(spm.Account_id, curObj.IsoCode, cashbackAmount, "Cashback transaction #" + pXMLRequest.ReferenceNumber, Bll.Accounts.Balance.SOURCE_PURCHASE, trans.ID, false);
                    }

                    pResponse.ReplyCode = -1;
                    pResponse.ReplyMessage = "Success";

                }

                #endregion

                #region DEPOSIT

                else if (pTransaction.type == "DEPOSIT")
                {
                    string strText = pTransaction.type + " - " + pTransaction.token.id;

                    var exist = Accounts.Balance.Search(new Accounts.Balance.SearchFilters() 
                    { 
                        Amount = new Range<decimal?>(pTransaction.totalAmount), 
                        AccountID = pAccount.Account_id, 
                        CurrencyIso = "ZAR", 
                        Text = strText 
                    }, null).Any();

                    if (!exist)
                    { 
                        Accounts.Balance.Create(pAccount.Account_id, "ZAR", pTransaction.totalAmount, strText, Accounts.Balance.SOURCE_DEPOSIT, null, true);
                        pResponse.ReplyCode = -1;
                        pResponse.ReplyMessage = "Success";
                    }
                    else
                    {
                        pResponse.ReplyCode = 1;
                        pResponse.ReplyMessage = "WiCode already claimed.";
                    }

                }

                #endregion

                else
                {
                    pResponse.ReplyCode = 1;
                    pResponse.ReplyMessage = "Type not specified.";
                }

                if (pResponse.ReplyCode == 0)
                {
                    var pWiCodeToUpdate = (from p in dc.CustomerWiCodes
                                           where p.CustomerWiCode_id == pWiCode.CustomerWiCode_id
                                           select p).SingleOrDefault();

                    pWiCodeToUpdate.DateClaimed = DateTime.Now;
                    dc.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                pResponse.ReplyCode = 6;
                pResponse.ReplyMessage = "Error - " + ex.Message.TruncEnd(20);
                Logger.Log(ex);
            }
            finally
            {
                Infrastructure.ObjectContext.Current.StopImpersonate();
            }

            return pResponse;
        }

        #region JSON Classes

        public class WiJSON
        {
            #region Token

            public class Token
            {
                public string maxTrxAmount { get; set; }
                public string tokenLifeInMin { get; set; }
                public string transactionType { get; set; }
                public string vspReference { get; set; }
            }
            public class TokenResult
            {
                public Token token { get; set; }
                public string responseCode { get; set; }
                public string responseDesc { get; set; }

                public class Token
                {
                    public int wiCode { get; set; }
                    public string wiQR { get; set; }
                }
            }

            #endregion

            #region Transaction

            public class Transaction
            {
                public int wiTrxId { get; set; }
                public Token token { get; set; }
                public string type { get; set; }
                public Storetrxdetails storeTrxDetails { get; set; }
                public int totalAmount { get; set; }
                public int basketAmount { get; set; }
                public Product[] products { get; set; }
                public Vspcredentials vspCredentials { get; set; }

                public class Token
                {
                    public string id { get; set; }
                    public string type { get; set; }
                    public string vspReference { get; set; }
                }

                public class Storetrxdetails
                {
                    public long storeId { get; set; }
                    public string remoteStoreId { get; set; }
                    public int retailerId { get; set; }
                    public string basketId { get; set; }
                    public string trxId { get; set; }
                    public string posId { get; set; }
                    public string cashierId { get; set; }
                }

                public class Vspcredentials
                {
                    public string id { get; set; }
                    public string password { get; set; }
                }

                public class Product
                {
                    public string id { get; set; }
                    public int units { get; set; }
                    public int pricePerUnit { get; set; }
                }
            }

            public class TransactionResultSuccess
            {
                public Token token { get; set; }
                public string type { get; set; }
                public Storetrxdetails storeTrxDetails { get; set; }
                public int wiTrxId { get; set; }
                public int totalAmountProcessed { get; set; }
                public int basketAmountProcessed { get; set; }
                public int tipAmountProcessed { get; set; }
                public int amountToSettle { get; set; }
                public Vsp vsp { get; set; }
                public Discount[] discount { get; set; }
                public object[] loyalty { get; set; }
                public string responseCode { get; set; }
                public string responseDesc { get; set; }

                public class Token
                {
                    public string id { get; set; }
                    public string type { get; set; }
                }

                public class Storetrxdetails
                {
                    public long storeId { get; set; }
                    public string basketId { get; set; }
                    public string trxId { get; set; }
                    public string posId { get; set; }
                    public string cashierId { get; set; }
                }

                public class Vsp
                {
                    public int id { get; set; }
                    public string name { get; set; }
                    public string trxId { get; set; }
                    public string responseCode { get; set; }
                    public string responseDesc { get; set; }
                }

                public class Discount
                {
                    public string name { get; set; }
                    public int amount { get; set; }
                    public object[] product { get; set; }
                }

            }

            public class TransactionResultFail
            {
                public Token token { get; set; }
                public string type { get; set; }
                public Storetrxdetails storeTrxDetails { get; set; }
                public int totalAmountProcessed { get; set; }
                public int basketAmountProcessed { get; set; }
                public string responseCode { get; set; }
                public string responseDesc { get; set; }

                public class Token
                {
                    public string id { get; set; }
                    public string type { get; set; }
                }

                public class Storetrxdetails
                {
                    public long storeId { get; set; }
                    public string basketId { get; set; }
                    public string trxId { get; set; }
                    public string posId { get; set; }
                    public string cashierId { get; set; }
                }

            }

            #endregion

            #region Advise

            public class Advise
            {
                public string action { get; set; }
                public Originaltrx originalTrx { get; set; }
                public Vspcredentials vspCredentials { get; set; }

                public class Originaltrx
                {
                    public Storetrxdetails storeTrxDetails { get; set; }
                    public int wiTrxId { get; set; }
                    public string type { get; set; }
                    public int vspTrxId { get; set; }
                }

                public class Storetrxdetails
                {
                    public int storeId { get; set; }
                    public string basketId { get; set; }
                    public string trxId { get; set; }
                    public string posId { get; set; }
                    public string cashierId { get; set; }
                }

                public class Vspcredentials
                {
                    public string id { get; set; }
                    public string password { get; set; }
                }

            }

            public class AdviseResponse
            {
                public string responseCode { get; set; }
                public string responseDesc { get; set; }
            }

            #endregion

            #region CustomerID

            public class GetCustomerIDResult
            {
                public string responseCode { get; set; }
                public string responseDesc { get; set; }
                public int? CustomerID { get; set; }
            }

            #endregion
        }

        #endregion



    }
}
