﻿using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.Bll.ThirdParty
{
    public static class Iso8583Factory
    {
        public enum Implementation { eCentric, ACS, WiGroup, Multicat };

        public static Iso8583Base Create(Implementation typeToCreate, string domainHost)
        {
            Iso8583Base theInstance = null;

            switch (typeToCreate)
            {
                case Implementation.eCentric:
                    theInstance = Domain.Get(domainHost).GetCachData("eCentric", () => new Domain.CachData((new eCentric(domainHost)), DateTime.MaxValue)) as eCentric;
                    break;
                case Implementation.ACS:
                    theInstance = Domain.Get(domainHost).GetCachData("ACS", () => new Domain.CachData((new ACS(domainHost)), DateTime.MaxValue)) as ACS;
                    break;
            }
            return theInstance;
        }

    }
}
