﻿using System;
using System.Text;
using System.Linq;
using Netpay.Infrastructure;
using System.Security.Cryptography;

namespace Netpay.Bll.ThirdParty
{
	public static class Orange
	{
		public const string ServiceURL = "http://devcom.orange.co.il/DevComAPI.asmx/HandleRequest";
		public const string ServiceCodeMaster = "NETPAY_MAIN";
		public const string ServiceCodeChild = "NETPAY_USERS";
		public const string ServiceCodeInvoices = "NETPAY_BILL";
		public const string ServiceConfirmPostfix = "_CONFIRM";
		public const int PartnerId = 53;

		public const int ERROR_ALREADYEXIST = 26;
		public const int ERROR_NOTEXIST = 27;

		private const int KeyLen = 32;
		private const int KeySize = 256;
		//private const string SecurityPassword = "MrvmqTFTsYJS!HEA2RgG2NfvmT0ZdEon";
		//private const string ServiceUser = "NetPay";
		
		private static RijndaelManaged createRijndaelSession(string password)
		{
			RijndaelManaged rijndaelCipher = new RijndaelManaged();
			rijndaelCipher.Mode = CipherMode.CBC;
			rijndaelCipher.Padding = PaddingMode.PKCS7;
			rijndaelCipher.KeySize = KeySize;
			rijndaelCipher.BlockSize = KeySize;

			byte[] pwdBytes = System.Text.Encoding.UTF8.GetBytes(password);
			byte[] keyBytes = new byte[KeyLen];
			int len = pwdBytes.Length;
			if (len > keyBytes.Length) len = keyBytes.Length;
			System.Array.Copy(pwdBytes, keyBytes, len);
			rijndaelCipher.Key = keyBytes;
			rijndaelCipher.IV = keyBytes;
			return rijndaelCipher;
		}

		private static string serviceDataEncrypt(RijndaelManaged rijndaelCipher, string text)
		{
			ICryptoTransform transform = rijndaelCipher.CreateEncryptor();
			byte[] plainText = System.Text.Encoding.UTF8.GetBytes(text);
			byte[] cipherBytes = transform.TransformFinalBlock(plainText, 0, plainText.Length);
			return Convert.ToBase64String(cipherBytes);
		}

		private static string serviceDataDecrypt(RijndaelManaged rijndaelCipher, string text)
		{
			ICryptoTransform transform = rijndaelCipher.CreateDecryptor();
			byte[] encryptedData = Convert.FromBase64String(text);
			byte[] plainText = transform.TransformFinalBlock(encryptedData, 0, encryptedData.Length);
			return System.Text.Encoding.UTF8.GetString(plainText);
		}

		private static string serviceDataDecrypt(RijndaelManaged rijndaelCipher, byte[] encryptedData)
		{
			ICryptoTransform transform = rijndaelCipher.CreateDecryptor();
			byte[] plainText = transform.TransformFinalBlock(encryptedData, 0, encryptedData.Length);
			return System.Text.Encoding.UTF8.GetString(plainText);
		}

		private static System.Xml.XmlDocument SendOrangeMessage(string serviceData)
		{
			string retData;
			var con = Netpay.Bll.Affiliates.Affiliate.Load(PartnerId);
			var rijndaelCipher = createRijndaelSession(con.OutboundPassword);
			serviceData = composeMessage(serviceData);
			string sendData = string.Format("ProviderName={0}&EncryptType=2&RequestData={1}", con.OutboundUsername, serviceDataEncrypt(rijndaelCipher, serviceData).ToEncodedUrl(System.Text.Encoding.GetEncoding("windows-1255")));
			if (Netpay.Infrastructure.HttpClient.SendHttpRequest(ServiceURL, out retData, sendData, System.Text.Encoding.UTF8, 0, null) != System.Net.HttpStatusCode.OK)
				return null;
			System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
			xmlDoc.LoadXml(retData);
			var nl = xmlDoc.GetElementsByTagName("RESPONSE");
			if (nl.Count < 1) throw new Exception("response format error, could not decrypt");
			retData = serviceDataDecrypt(rijndaelCipher, nl[0].InnerText);
			xmlDoc.LoadXml(retData);
			return xmlDoc;
		}

		private static string composeMessage(string msgBody)
		{
			StringBuilder reqData = new StringBuilder();
			//reqData.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
			reqData.AppendLine("<MSG>");
			reqData.AppendLine(" <HEADER>");
			reqData.AppendLine("  <ACTION>Request</ACTION>");
			reqData.AppendLine("  <ACTION_TYPE>Send</ACTION_TYPE>");
			reqData.AppendFormat("  <MSG_ID>{0}</MSG_ID>\r\n", Guid.NewGuid());
			reqData.AppendFormat("  <TRANSACTION_TS>{0}</TRANSACTION_TS>\r\n", DateTime.Now.ToString("ddMMyyyHHmmss"));
			reqData.AppendLine(" </HEADER>");
			reqData.AppendLine(" <BODY>");
			reqData.Append(msgBody);
			reqData.AppendLine(" </BODY>");
			reqData.AppendLine("</MSG>");
			return reqData.ToString();
		}

		public static bool checkProvisionStatus(System.Guid credentialsToken)
		{
			var settings = Bll.Affiliates.MerchantSettings.Load(PartnerId, Merchants.Merchant.Current.ID);
			//Bll.Affiliates.MerchantSettings.ForMerchant(Merchants.Merchant.Current).Where(s => s.PartnerId == PartnerId).SingleOrDefault();
			if (settings == null) return true;
			return checkProvisionStatus(settings);
		}

		private static bool checkProvisionStatus(Bll.Affiliates.MerchantSettings settings)
		{
			StringBuilder reqData = new StringBuilder();
			reqData.AppendLine("  <REQUEST_TYPE>Information</REQUEST_TYPE>");
			reqData.AppendLine("  <REQUEST_METHOD>CheckProvisioning</REQUEST_METHOD>");
			reqData.AppendLine("  <SUBS_ID_TYPE>MSISDN</SUBS_ID_TYPE>");
			reqData.AppendLine(string.Format("  <SUBSCRIBER_ID>{0}</SUBSCRIBER_ID>", settings.UserID));
			reqData.AppendLine(string.Format("  <SERVICE_CODE>{0}</SERVICE_CODE>", ServiceCodeMaster));
			reqData.AppendLine("  <REQUEST_PARAMS></REQUEST_PARAMS>");
			var retValue = SendOrangeMessage(reqData.ToString());
			var nl = retValue.GetElementsByTagName("RESULT_CODE"); //PROV_CODE
			if (nl.Count != 1) throw new Exception("CheckProvisioning response format error, could not fint PROV_CODE element");
			return (nl[0].InnerText.Trim() == "0");
		}

		private static void setServiceStatus(Bll.Affiliates.MerchantSettings settings, string serviceCode, string codeValue)
		{
			Bll.Affiliates.MerchantSettings.SavePartnerServiceConfig(PartnerId, settings.MerchantId, serviceCode + ServiceConfirmPostfix, codeValue);
			/*
			if ((serviceCode == ServiceCodeMaster) && (codeValue == "1")) {
				//wait 15 mins before adding child services
				Netpay.Bll.Merchants.SetPartnerSyncDate(domainHost, PartnerId, settings.MerchantId, DateTime.Now.AddMinutes(15));
			} else if ((serviceCode != ServiceCodeMaster) && (codeValue == "0")) {
				//wait 15 mins before removing master service
				Netpay.Bll.Merchants.SetPartnerSyncDate(domainHost, PartnerId, settings.MerchantId, DateTime.Now.AddMinutes(15));
			}
			*/
		}

		private static int sendChargeRequest(Bll.Affiliates.MerchantSettings settings, string serviceCode, string verb, int? count)
		{
			StringBuilder reqData = new StringBuilder();
			reqData.AppendLine("  <REQUEST_TYPE>Activation</REQUEST_TYPE>");
			reqData.AppendLine(string.Format("  <REQUEST_METHOD>{0}</REQUEST_METHOD>", verb));
			reqData.AppendLine("  <SUBS_ID_TYPE>MSISDN</SUBS_ID_TYPE>");
			reqData.AppendLine(string.Format("  <SUBSCRIBER_ID>{0}</SUBSCRIBER_ID>", settings.UserID));
			reqData.AppendLine(string.Format("  <SERVICE_CODE>{0}</SERVICE_CODE>", serviceCode + "_ACT"));
			reqData.AppendLine("  <REQUEST_PARAMS>");
			if (count != null)
				reqData.AppendLine(string.Format("  <NUMBER_OF_USERS>{0}</NUMBER_OF_USERS>", count.GetValueOrDefault()));
			reqData.AppendLine("  </REQUEST_PARAMS>");
			var retValue = SendOrangeMessage(reqData.ToString());
			var nl = retValue.GetElementsByTagName("RESULT_CODE");
			Bll.Process.GenericLog.Log(CommonTypes.LogHistoryType.OrangeOutboundCommunication, settings.MerchantId, serviceCode, reqData.ToString(), retValue.InnerXml, "");
			if (nl.Count != 1) throw new Exception(string.Format("{0}-{1} response format error, could not find RESULT_CODE element", serviceCode, verb));
			int res = nl[0].InnerText.Trim().ToInt32(-1);

			if (verb != "ModifyService") {
				if (res == ERROR_ALREADYEXIST) setServiceStatus(settings, serviceCode, count.GetValueOrDefault(1).ToString());
				else if (res == ERROR_NOTEXIST) setServiceStatus(settings, serviceCode, "0");
			}
			return res;
		}

		public static bool activateInvoiceService(Bll.Affiliates.MerchantSettings settings, bool activate)
		{
			if (settings.ConfigurationValues[ServiceCodeInvoices + ServiceConfirmPostfix].ToInt32() == 0 && activate) {
				sendChargeRequest(settings, ServiceCodeInvoices, "AddService", null);
				return true;
			}
			if (settings.ConfigurationValues[ServiceCodeInvoices + ServiceConfirmPostfix].ToInt32() == 1 && !activate) {
				sendChargeRequest(settings, ServiceCodeInvoices, "RemoveService", null);
				return true;
			}
			return false;
		}

		public static bool changeNumOfDevicesService(Bll.Affiliates.MerchantSettings settings, int newOfDevices)
		{
			int paramSubAccounts = settings.ConfigurationValues[ServiceCodeChild + ServiceConfirmPostfix].ToInt32();
			if (paramSubAccounts == 0 && newOfDevices > 0) {
				sendChargeRequest(settings, ServiceCodeChild, "AddService", newOfDevices);
				return true;
			} else if (paramSubAccounts > 0 && newOfDevices == 0) {
				sendChargeRequest(settings, ServiceCodeChild, "RemoveService", null);
				return true;
			} else if ((paramSubAccounts > 0 && newOfDevices > 0) && (paramSubAccounts != newOfDevices)) {
				sendChargeRequest(settings, ServiceCodeChild, "ModifyService", newOfDevices);
				return true;
			}
			return false;
		}

		public static bool sendAccountCharge(int merchantId)
		{
			var settings = Bll.Affiliates.MerchantSettings.Load(PartnerId, merchantId);
			if (settings == null) return false;
			bool isMobileEnabled = settings.ConfigurationValues[ServiceCodeMaster].ToInt32(0) == 1;
			bool isInvoiceEnabled = settings.ConfigurationValues[ServiceCodeInvoices].ToInt32(0) == 1;
			int signedDevCount = settings.ConfigurationValues[ServiceCodeChild].ToInt32(0); 
			if (isMobileEnabled)
			{
				if (settings.ConfigurationValues[ServiceCodeMaster + ServiceConfirmPostfix].ToInt32() != 1)
				{
					sendChargeRequest(settings, ServiceCodeMaster, "AddService", null);
					return true;
				}
				if (activateInvoiceService(settings, isInvoiceEnabled)) return true;
				if (changeNumOfDevicesService(settings, signedDevCount)) return true;
			} else {
				if (changeNumOfDevicesService(settings, 0)) return true;
				if (activateInvoiceService(settings, false)) return true;
				//if ((settings.ConfigurationValues[ServiceCodeInvoices + ServiceConfirmPostfix].ToInt32() == 1) || (settings.ConfigurationValues[ServiceCodeChild + ServiceConfirmPostfix].ToInt32() > 0)) return true;
				if (settings.ConfigurationValues[ServiceCodeMaster + ServiceConfirmPostfix].ToInt32() == 1) {
					sendChargeRequest(settings, ServiceCodeMaster, "RemoveService", null);
					return true;
				}
			}
			Bll.Affiliates.MerchantSettings.SetPartnerSyncDate(PartnerId, merchantId, null);
			return false;
		}

		private static Bll.Affiliates.MerchantSettings getMerchantServices(string phoneNumber)
		{
			return Bll.Affiliates.MerchantSettings.Load(PartnerId, phoneNumber);
		}

		private static string createNotificationResponse(string msgId, int resCode, string resDescription)
		{
			System.Text.StringBuilder sb = new StringBuilder();
			sb.Append("<MSG>");
			sb.Append(" <HEADER>");
			sb.Append("  <ACTION>RESPONSE</ACTION>");
			sb.AppendFormat("  <REF_MSG_ID>{0}</REF_MSG_ID>", msgId);
			sb.AppendFormat("  <RESULT_CODE>{0}</RESULT_CODE>", resCode);
			sb.AppendFormat("  <RESULT_DESC>{0}</RESULT_DESC>", resDescription);
			sb.Append(" </HEADER>");
			sb.Append(" <BODY />");
			sb.Append("</MSG>");
			return sb.ToString();
		}

		public static void setMerchantServices(int merchantId, bool mainService, bool invService, int childCount)
		{
			Bll.Affiliates.MerchantSettings.SavePartnerServiceConfig(PartnerId, merchantId, ServiceCodeMaster, mainService ? "1" : "0");
			Bll.Affiliates.MerchantSettings.SavePartnerServiceConfig(PartnerId, merchantId, ServiceCodeInvoices, invService ? "1" : "0");
			Bll.Affiliates.MerchantSettings.SavePartnerServiceConfig(PartnerId, merchantId, ServiceCodeChild, childCount.ToString());
			if (Netpay.Bll.ThirdParty.Orange.sendAccountCharge(merchantId))
				Bll.Affiliates.MerchantSettings.SetPartnerSyncDate(PartnerId, merchantId, DateTime.Now.AddMinutes(15));
		}

		public static string parseNotification(string notifyData)
		{
			var con = Netpay.Bll.Affiliates.Affiliate.Load(PartnerId);
			var rijndaelCipher = createRijndaelSession(con.OutboundPassword);
			string requestData = serviceDataDecrypt(rijndaelCipher, notifyData);
			System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
			doc.LoadXml(requestData);
			string strMsgId = doc.GetElementsByTagName("MSG_ID")[0].InnerText;
			string serviceCode = doc.GetElementsByTagName("SERVICE_CODE")[0].InnerText;
			var settings = getMerchantServices(doc.GetElementsByTagName("SUBSCRIBER_ID")[0].InnerText);
			if (settings == null) {
				Bll.Process.GenericLog.Log(CommonTypes.LogHistoryType.OrangeInboundNotification, null, serviceCode, doc.InnerXml, "Phone number not found", "Fail");
				return serviceDataEncrypt(rijndaelCipher, createNotificationResponse(strMsgId, 1, "Phone number not found"));
			}
			string codeValue = "";
			switch (doc.GetElementsByTagName("SERVICE_ACTION")[0].InnerText)
			{
				case "Add":
					if (serviceCode == ServiceCodeChild) 
						codeValue = doc.GetElementsByTagName("number_of_users")[0].InnerText;
					else codeValue = "1"; 
					break;
				case "Remove": codeValue = "0"; break;
				case "Modify":
					codeValue = doc.GetElementsByTagName("number_of_users")[0].InnerText;
					break;
			}
			setServiceStatus(settings, serviceCode, codeValue);
			Bll.Process.GenericLog.Log(CommonTypes.LogHistoryType.OrangeInboundNotification, settings.MerchantId, serviceCode, doc.InnerXml, "OK", "OK");
			string retData = serviceDataEncrypt(rijndaelCipher, createNotificationResponse(strMsgId, 0, "OK"));
			return retData;
		}

		public static void executePending(string domainHost)
		{
			Domain.Current = Domain.Get(domainHost);
			ObjectContext.Current.CredentialsToken = Domain.Current.ServiceCredentials;
			var pendingList = (from sma in DataContext.Reader.SetMerchantAffiliates where sma.Affiliate_id == PartnerId && sma.SyncDateTime <= DateTime.Now select sma).ToList();
			foreach(var p in pendingList) {
				if (sendAccountCharge(p.Merchant_id))
					Bll.Affiliates.MerchantSettings.SetPartnerSyncDate(PartnerId, p.Merchant_id, DateTime.Now.AddMinutes(15));
			}
		}
	}
}
