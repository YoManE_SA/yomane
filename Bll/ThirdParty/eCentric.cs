﻿using System;
using System.Web;
using System.Linq;
using Netpay.Infrastructure;
using System.Collections.Generic;

namespace Netpay.Bll.ThirdParty
{
    public class eCentric : Iso8583Base
    {
        //public static readonly string[] chMASTER_KEY_DEV = { "F2862531984FF873", "43490BC8E504E33D", "26E0795BE01CA897" };
        public static readonly string[] chMASTER_KEY_DEV = { "1133557799199775", "C210A3BFD5EE9843" };
        //private static readonly string[]  chMASTER_KEY_PRD = { "84ED0F94D10C0EA4", "9A671A634B2535C2" };
        private const int       chKEY_SIZE = 64;
        private const int       chTCP_SERVER_PORT = 9393;
        public  const string    chSTOREDPM_PROVIDERNAME = "System.eCentric";
        public  const int       chDEBITCOMPANY_ID = 67;
        public const string chCONFIG_FILE_KEY_NAME = "eCentric_KEY";

        public eCentric(string domainHost, int listenPort = chTCP_SERVER_PORT) : base(Iso8583Factory.Implementation.eCentric, chMASTER_KEY_DEV, chKEY_SIZE, chTCP_SERVER_PORT, chSTOREDPM_PROVIDERNAME, chDEBITCOMPANY_ID, "eCentric", chCONFIG_FILE_KEY_NAME)
        {
            DomainHost = domainHost;
            ServerPort = listenPort;
            KeepAliveSeconds = 30;
        }

        public static void EnableServers(bool bEnable)
        {
            Iso8583Base.EnableServers(Iso8583Factory.Implementation.eCentric, bEnable, "eCentric_Enable", "eCentric_Log");
        }
    }
}
