﻿using System;
using System.Linq;
using System.Collections.Generic;
using Netpay.Infrastructure;
using Netpay.Bll.Accounts;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.ThirdParty
{
    public class Lamda : Infrastructure.Module, Accounts.IStoredPaymentMethodProvider
    {
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Current, SecuredObjectName); } }

        public const string PM_ProviderName = "System.Lamda";
        public const string PM_FixedBIN = "666666";

        public override string Name { get { return PM_ProviderName; } }
        public override decimal Version { get { return 1.1m; } }
        public static Lamda Current { get { return Infrastructure.Module.Get(PM_ProviderName) as Lamda; } }

        bool Accounts.IStoredPaymentMethodProvider.SupportCreate { get { return true; } }
        bool Accounts.IStoredPaymentMethodProvider.SupportBalance { get { return true; } }
        bool Accounts.IStoredPaymentMethodProvider.SupportStatus { get { return false; } }
        bool Accounts.IStoredPaymentMethodProvider.SupportLoad { get { return true; } }

        public Lamda()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, error) => { return true; };
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Ssl3 | System.Net.SecurityProtocolType.Tls | System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;
            if (!Accounts.StoredPaymentMethod.Providers.ContainsKey(PM_ProviderName))
                Accounts.StoredPaymentMethod.Providers.Add(PM_ProviderName, this);
        }

        protected override void OnInstall(EventArgs e)
        {
            Accounts.ExternalServiceLogin.RegisterServiceType(PM_ProviderName, "Lamda Login");
            base.OnInstall(e);

            //Infrastructure.Security.SecuredObject.Create(this, SecuredObjectName, Infrastructure.Security.PermissionGroup.ReadEditDelete);
        }

        protected override void OnUninstall(EventArgs e)
        {            
            base.OnUninstall(e);
            //Infrastructure.Security.SecuredObject.Remove(this);
        }

        public class LamdaRequest : System.Xml.XmlDocument
        {
            private string _terminalPassword;
            private LamdaRequest() { }

            public LamdaRequest(int accountId, string methodName)
            {
                AppendChild(CreateXmlDeclaration("1.0", "UTF-8", null));
                var body = CreateElement(methodName);
                AppendChild(body);
                var exl = Accounts.ExternalServiceLogin.LoadForAccount(accountId, PM_ProviderName);
                if (exl == null) throw new Exception("Lamda merchant external service login not found");
                _terminalPassword = exl.Password;
                var credentials = CreateElement("Credentials");
                credentials.AppendChild(CreateElement("MerchantId")).InnerText = exl.Reference1;
                credentials.AppendChild(CreateElement("TerminalId")).InnerText = exl.Username;
                using (var hash = System.Security.Cryptography.SHA256.Create()) {
                    credentials.AppendChild(CreateElement("TerminalPassword")).InnerText =
                        System.BitConverter.ToString(hash.ComputeHash(System.Text.Encoding.UTF8.GetBytes(_terminalPassword))).Replace("-", "").ToLower();
                }
                body.AppendChild(credentials);
            }

            public void AddParam(string paramName, string value)
            {
                DocumentElement.AppendChild(CreateElement(paramName)).InnerText = value;
            }

            public string GetParam(string paramName)
            {
                var tags = DocumentElement.GetElementsByTagName(paramName);
                if (tags.Count < 1) return null;
                return tags[0].InnerText;
            }

            public int ValidateResponseCode(bool Throw = true, int okCode = 1001)
            {
                var resCode = SelectSingleNode("//ResponseCode").InnerText.ToNullableInt();
                var resText = SelectSingleNode("//ResponseTxt").InnerText;
                if (resCode.GetValueOrDefault() != okCode && Throw)
                    throw new Exception(string.Format("Lamda Error:{0} - {1}", resCode, resText));
                return resCode.GetValueOrDefault();
            }

            public string GetSignatureKey()
            {
                var tid = SelectSingleNode("//TransactionId").InnerText;
                return string.Format("{0}{1}", _terminalPassword, tid);
            }

            public LamdaRequest Send(string addressSegment = null)
            {
                string signature = null, retXml = null;
                var messageBytes = System.Text.Encoding.UTF8.GetBytes(OuterXml);
                var serviceAddress = (Infrastructure.Application.IsProduction) ? "https://lamdacardservices.com/secureCard/post/{0}/index.php" : "https://test.lamdacardservices.com/secureCard/post/{0}/index.php";
                serviceAddress = string.Format(serviceAddress, addressSegment == null ? DocumentElement.LocalName : addressSegment);
                using (var hmac = new System.Security.Cryptography.HMACSHA256()) {
                    hmac.Key = System.Text.Encoding.UTF8.GetBytes(GetSignatureKey());
                    var binSign = hmac.ComputeHash(messageBytes);
                    signature = System.Convert.ToBase64String(binSign); //System.BitConverter.ToString(binSign).Replace("-", "").ToLower();
                }
                var sendValues = new System.Collections.Specialized.NameValueCollection();
                sendValues.Add("encodedMessage", Convert.ToBase64String(messageBytes));
                sendValues.Add("signature", signature);
                using (var wc = new System.Net.WebClient()) {
                    //wc.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                    wc.Headers["Accept"] = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"; //must include this header
                    wc.Headers["User-Agent"] = "Mozilla/5.0"; //must include this header
                    retXml = System.Text.Encoding.UTF8.GetString(wc.UploadValues(serviceAddress, "POST", sendValues));
                }
                var retValues = System.Web.HttpUtility.ParseQueryString(retXml);
                var retDoc = new LamdaRequest();
                retDoc.Load(new System.IO.MemoryStream(Convert.FromBase64String(retValues["encodedMessage"])));
                if (System.Diagnostics.Debugger.IsAttached) {
                    System.Diagnostics.Debug.WriteLine("Request: {0}\r\n{1}", addressSegment, OuterXml);
                    System.Diagnostics.Debug.WriteLine("Response: {0}\r\n{1}", addressSegment, retDoc.OuterXml);
                }
                return retDoc;
            }
        }

        public class LamdaCard : PaymentMethods.CreditCard
        {
            public LamdaCard() : base(CommonTypes.PaymentMethodEnum.CC_LamdaCard) { }
            public string CardId { get { return base.Cvv; } set { base.Cvv = value; } }
            public Accounts.StoredPaymentMethod.MethodStatus Status { get; internal set; }
            public string ExtStatus { get; internal set; }
            public string CurrencyIso { get; set; }
        }

        public int IncNextTransID()
        {
            lock (this) {
                var lastNum = GetSetting("TransID").ToNullableInt().GetValueOrDefault(0) + 1;
                SetSetting("TransID", lastNum.ToString());
                return lastNum;
            }
        }

        public int IncNextOrderID()
        {
            lock (this)
            {
                var lastNum = GetSetting("OrderID").ToNullableInt().GetValueOrDefault(0) + 1;
                SetSetting("OrderID", lastNum.ToString());
                return lastNum;
            }
        }

        public Accounts.Balance.BalanceStatus GetMerchantBalance(int merchantId)
        {
            if (Netpay.Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            var accId = Accounts.Account.LoadAccount(Accounts.AccountType.Merchant, merchantId).AccountID;
            var req = new LamdaRequest(accId, "MerchantBalanceRequest");
            req.AddParam("TransactionId", IncNextTransID().ToString());
            req.AddParam("TransactionType", "LCMBL");
            var res = req.Send("MerchantBalance");
            res.ValidateResponseCode();
            return new Accounts.Balance.BalanceStatus()
            {
                CurrencyIso = Currency.GetByIsoNumber(res.GetParam("Currency").ToNullableInt().GetValueOrDefault()).IsoCode,
                Current = res.GetParam("Balance").ToNullableDecimal().GetValueOrDefault(),
            };
        }

        private int GetCustomerProcessAccountID(Customers.Customer customer)
        {
            var appIdentiry = customer.RefIdentity;
            if (appIdentiry == null) throw new Exception("Customer not attached to AppIdentity");

            if (string.IsNullOrEmpty(appIdentiry.ProcessMerchantNumber)) throw new Exception("Process merchant number not assigned to app identity.");
            return Accounts.Account.LoadAccount(Accounts.AccountType.Merchant, appIdentiry.ProcessMerchantNumber).AccountID;
        }

        private int GetPMProcessAccountID(Accounts.StoredPaymentMethod pm)
        {
            var customer = Customers.Customer.LoadObject(pm.Account_id) as Customers.Customer;
            return GetCustomerProcessAccountID(customer);
        }

        public Accounts.StoredPaymentMethod Create(Customers.Customer customer, IAddress address, System.Collections.Specialized.NameValueCollection extendedParams)
        {
            return PlasticRequest(customer, extendedParams["embosed"] == "1");
        }

        public Accounts.StoredPaymentMethod PlasticRequest(Customers.Customer customer, bool isPreemboss)
        {
            var address = customer.PersonalAddress;
            if (address == null) address = new Accounts.AccountAddress();
            var req = new LamdaRequest(GetCustomerProcessAccountID(customer), isPreemboss ? "PlasticCardRequest" : "PreembossPlasticCardRequest");
            req.AddParam("TransactionId", IncNextTransID().ToString());
            req.AddParam("TransactionType", "LC101");
            req.AddParam("ProductType", "LP006");
            req.AddParam("Title", "Mr");
            req.AddParam("Email", customer.EmailAddress);
            req.AddParam("IdentificationType", "1"); //use SSN
            req.AddParam("IdentificationValue", customer.PersonalNumber);
            req.AddParam("FirstName", customer.FirstName);
            req.AddParam("LastName", customer.LastName);
            req.AddParam("DOB", customer.DateOfBirth.GetValueOrDefault().ToString("yyyyMMdd"));
            req.AddParam("Address1", address.AddressLine1);
            req.AddParam("Address2", address.AddressLine2);
            req.AddParam("City", address.City);
            req.AddParam("State", address.StateISOCode);
            req.AddParam("Country", address.CountryISOCode);
            req.AddParam("PostalCode", address.PostalCode);

            req.AddParam("Nationality", address.CountryISOCode);
            req.AddParam("PhoneCountryCode", customer.PhoneNumber.NullIfEmpty().ValueIfNull("1").Substring(0, 3));
            req.AddParam("Phone", customer.PhoneNumber);
            req.AddParam("OrderId", IncNextOrderID().ToString("000000000000"));
            var res = req.Send();
            res.ValidateResponseCode();
            var newStoredMethod = new Accounts.StoredPaymentMethod() {
                Account_id = customer.AccountID,
                OwnerSSN = customer.PersonalNumber,
                OwnerName = customer.FullName,
                ProviderID = PM_ProviderName,
                Status = Accounts.StoredPaymentMethod.MethodStatus.WaitingActivate,
            };
            newStoredMethod.ProviderReference1 = res.GetParam("ClientId");
            newStoredMethod.MethodInstance.PaymentMethodId = CommonTypes.PaymentMethodEnum.CC_LamdaCard;
            var ccInfo = (newStoredMethod.MethodInstance as PaymentMethods.CreditCard);
            ccInfo.CardNumber = string.Format("{0}000000{1}", PM_FixedBIN, res.GetParam("CardId"));
            //ccInfo.ExpirationDate = res.GetParam("ClientId");
            if (!string.IsNullOrEmpty(newStoredMethod.ProviderReference1))
                SetSetting(customer.AccountID, "ClientId", res.GetParam("ClientId"));
            newStoredMethod.Save();
            return newStoredMethod;
        }

        public List<LamdaCard> AccountCardList(Customers.Customer customer)
        {
            var req = new LamdaRequest(GetCustomerProcessAccountID(customer), "AccountCardListRequest");
            req.AddParam("TransactionId", IncNextTransID().ToString());
            req.AddParam("TransactionType", "LC009");
            //var exs = Accounts.ExternalServiceLogin.LoadForAccount(customer.AccountID).Where(v=> v.)
            req.AddParam("ClientId", GetSetting(customer.AccountID, "ClientId"));
            var res = req.Send("AccountCardList");
            res.ValidateResponseCode();
            var ret = new List<LamdaCard>();
            var cards = res.GetElementsByTagName("Card");
            foreach (System.Xml.XmlElement v in cards) {
                var crd = new LamdaCard();
                crd.CardId = v.GetElementsByTagName("CardId")[0].InnerText;
                crd.CardNumber = string.Format("{0}000000{1}", PM_FixedBIN, v.GetElementsByTagName("CardNumber")[0].InnerText);
                crd.CurrencyIso = Currency.GetByIsoNumber(v.GetElementsByTagName("Currency")[0].InnerText.ToNullableInt().GetValueOrDefault()).IsoCode;
                crd.ExtStatus = v.GetElementsByTagName("Status")[0].InnerText;
                switch (crd.ExtStatus)
                {
                    case "Active": crd.Status = Accounts.StoredPaymentMethod.MethodStatus.Active; break;
                    case "Inactive": crd.Status = Accounts.StoredPaymentMethod.MethodStatus.Disabled; break;
                    case "Blocked": crd.Status = Accounts.StoredPaymentMethod.MethodStatus.Disabled; break;
                    case "Deleted": crd.Status = Accounts.StoredPaymentMethod.MethodStatus.Disabled; break;
                }
                //crd.ExpirationDate = v.GetElementsByTagName("ExpirationDate")[0].InnerText;
                ret.Add(crd);
            }
            return ret;
        }

        public Accounts.Balance Load(Accounts.StoredPaymentMethod pm, decimal amount, CommonTypes.Currency currency, string refCode)
        {
            if (pm.ProviderID.EmptyIfNull().ToUpper() != PM_ProviderName.ToUpper())
                throw new Exception("Not Lamda card");
            var curBalance = Accounts.Balance.GetStatus(pm.Account_id, Currency.Get(currency).IsoCode);
            if (curBalance == null || curBalance.Expected < amount) throw new Exception("amount is more than current balance");
            var clientId = GetSetting(pm.Account_id, "ClientId");
            var cardId = pm.ProviderReference1;

            var orderId = IncNextTransID().ToString();
            var req = new LamdaRequest(GetPMProcessAccountID(pm), "CardLoadRequest");
            req.AddParam("TransactionId", orderId.ToString());
            req.AddParam("TransactionType", "LC010");

            req.AddParam("ClientId", clientId);
            req.AddParam("CardId", cardId);
            req.AddParam("OrderId", "11" + orderId);
            req.AddParam("Amount", ((int)(amount * 100)).ToString());
            req.AddParam("Currency", Currency.Get(currency).IsoNumber);
            var res = req.Send("CardLoad");
            res.ValidateResponseCode(true, 2101);
            return Accounts.Balance.Create(pm.Account_id, Currency.Get(currency).IsoCode, -amount, "Lamda Card Load", PM_ProviderName, res.GetParam("TransactionId").ToNullableInt(), false);
        }

        public StoredPaymentMethod.MethodStatus GetStatus(StoredPaymentMethod pm) { throw new NotImplementedException(); }

        public List<Balance> GetBalace(StoredPaymentMethod pm, Range<DateTime> range)
        {
            var customer = Accounts.Account.LoadObject(pm.Account_id) as Customers.Customer;
            return GetBalace(customer, pm, range);
        }

        public List<Accounts.Balance> GetBalace(Customers.Customer customer, Accounts.StoredPaymentMethod pm, Range<DateTime> range)
        {
            var clientId = GetSetting(customer.AccountID, "ClientId");
            var req = new LamdaRequest(GetPMProcessAccountID(pm), "CardInfoRequest");
            req.AddParam("TransactionId", IncNextTransID().ToString());
            req.AddParam("TransactionType", "LC002");
            req.AddParam("ClientId", clientId);
            req.AddParam("CardId", pm.ProviderReference1);
            req.AddParam("ExpDate", pm.MethodInstance.ExpirationDate.GetValueOrDefault().ToString("yyMM"));
            req.AddParam("MaxRows", 100.ToString());
            req.AddParam("StartDate", "1" + range.From.ToString("yyMMdd"));
            req.AddParam("EndDate", "1" + range.To.ToString("yyMMdd"));
            req.AddParam("DOB", customer.DateOfBirth.GetValueOrDefault().ToString("yyyyMMdd"));
            var res = req.Send("CardInfo");
            res.ValidateResponseCode(true, 2001);
            var cards = res.GetElementsByTagName("Transaction");
            var ret = new List<Accounts.Balance>();
            foreach (System.Xml.XmlElement v in cards)
            {
                DateTime dateTime;
                DateTime.TryParseExact(v.SelectSingleNode("//TransactionDate").InnerText.Trim() + "T" + v.SelectSingleNode("//TransactionTime").InnerText.Trim(), "yyyyMMddTHHmmss", null, System.Globalization.DateTimeStyles.None, out dateTime);
                ret.Add(new Accounts.Balance(new Dal.Netpay.AccountBalance() {
                    Account_id = customer.AccountID,
                    InsertDate = dateTime,
                    Amount = v.SelectSingleNode("//TranAmount").InnerText.ToNullableDecimal().GetValueOrDefault() / 100,
                    CurrencyISOCode = Currency.Get(v.SelectSingleNode("//TranCurr").InnerText).IsoCode,
                    SystemText = v.SelectSingleNode("//TranTypeDesc").InnerText,
                    TotalBalance = v.SelectSingleNode("//AcctCurrAmount").InnerText.ToNullableDecimal().GetValueOrDefault() / 100,
                    IsPending = false,
                }));
            }
            return ret;
        }

        public void PullCardStatus()
        {
            var cards = Accounts.StoredPaymentMethod.Search(new Accounts.StoredPaymentMethod.SearchFilters() {
                Provider = PM_ProviderName,
                Status = Accounts.StoredPaymentMethod.MethodStatus.WaitingActivate }, null);
            foreach (var savedcCard in cards) {
                var customer = Customers.Customer.LoadObject(savedcCard.Account_id) as Customers.Customer;
                var card = AccountCardList(customer).Where(v => v.CardId == savedcCard.ProviderReference1).SingleOrDefault();
                if (card == null) continue;
                if (savedcCard.Status != card.Status) {
                    savedcCard.Status = card.Status;
                    savedcCard.Save();
                }
            }
        }

        public static void PullCardStatusAllDomains()
        {
            foreach (var d in Domain.Domains.Values) {
                Domain.Current = d;
                try {
                    ObjectContext.Current.Impersonate(d.ServiceCredentials);
                    if (Current != null) Current.PullCardStatus();
                    ObjectContext.Current.StopImpersonate();
                } catch(Exception ex) {
                    Logger.Log(ex);
                }
            }
        }

    }
}
