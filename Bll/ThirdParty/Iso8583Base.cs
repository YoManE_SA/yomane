﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Netpay.Infrastructure;

namespace Netpay.Bll.ThirdParty
{
    public class Iso8583Base
    {
        public Iso8583Base(Iso8583Factory.Implementation implementationType, string[] MasterKeyDev, int KeySize, int TcpServerPort, string StoredPmProviderName, int DebitCompanyId, string ClassName, string ConfigFileKeyName)
        {
            IMPLEMENTATION_TYPE = implementationType;
            MASTER_KEY_DEV = MasterKeyDev;
            KEY_SIZE = KeySize;
            TCP_SERVER_PORT = TcpServerPort;
            StoredPmProviderName = STOREDPM_PROVIDERNAME;
            DEBITCOMPANY_ID = DebitCompanyId;
            CLASS_NAME = ClassName;
            CONFIG_FILE_KEY_NAME = ConfigFileKeyName;
        }

        protected string[] MASTER_KEY_DEV { get; private set; }

        protected int KEY_SIZE { get; private set; }

        protected int TCP_SERVER_PORT { get; private set; }

        protected string STOREDPM_PROVIDERNAME { get; private set; }

        protected int DEBITCOMPANY_ID { get; private set; }

        protected string CLASS_NAME { get; private set; }

        protected string CONFIG_FILE_KEY_NAME { get; private set; }

        public Iso8583Factory.Implementation IMPLEMENTATION_TYPE { get; private set; }

        public bool LogCommunication { get; set; }

        #region Encryption And Formatting
        protected static byte[] StringToByteArray(string data)
        {
            var ret = new byte[data.Length / 2];
            for (int i = 0; i < data.Length; i += 2)
                ret[i / 2] = Convert.ToByte(data.Substring(i, 2), 16);
            return ret;
        }

        protected static string ByteArrayToString(byte[] data)
        {
            var ret = new System.Text.StringBuilder(data.Length * 2);
            for (int i = 0; i < data.Length; i++)
                ret.Append(data[i].ToString("X2"));
            return ret.ToString();
        }

        protected System.Security.Cryptography.SymmetricAlgorithm CreateCrypto(string key)
        {
            if (IMPLEMENTATION_TYPE == Iso8583Factory.Implementation.ACS)
            {
                var tripleDes = System.Security.Cryptography.TripleDES.Create();
                tripleDes.Mode = System.Security.Cryptography.CipherMode.ECB;
                tripleDes.Padding = System.Security.Cryptography.PaddingMode.None;
                tripleDes.KeySize = KEY_SIZE;
                tripleDes.BlockSize = 64;
                tripleDes.IV = new byte[tripleDes.BlockSize / 8];
                if (key != null) tripleDes.Key = StringToByteArray(key);
                return tripleDes;
            }
            else
            {
                var des = System.Security.Cryptography.DES.Create();
                des.Mode = System.Security.Cryptography.CipherMode.ECB;
                des.Padding = System.Security.Cryptography.PaddingMode.None;
                des.KeySize = KEY_SIZE;
                des.BlockSize = KEY_SIZE;
                des.IV = new byte[KEY_SIZE / 8];
                if (key != null) des.Key = StringToByteArray(key);
                return des;
            }
        }

        protected string Encrypt(byte[] bytes, string key)
        {
            using (var cry = CreateCrypto(key))
            {
                using (var enc = cry.CreateEncryptor())
                {
                    return ByteArrayToString(enc.TransformFinalBlock(bytes, 0, bytes.Length));
                }
            }
        }

        protected byte[] Decrypt(string data, string key)
        {
            using (var cry = CreateCrypto(key))
            {
                using (var dec = cry.CreateDecryptor())
                {
                    var bytes = StringToByteArray(data);
                    return dec.TransformFinalBlock(bytes, 0, bytes.Length);
                }
            }
        }

        protected string GenerateKWP()
        {
            using (var cry = CreateCrypto(null))
            {
                cry.GenerateKey();
                return ByteArrayToString(cry.Key);
            }
        }

        protected static void XorCombine(byte[] dest, byte[] src)
        {
            for (int i = 0; i < dest.Length; i++)
                dest[i] ^= src[i % src.Length];
        }

        public static byte[] XorCombine3(byte[] cc1, byte[] cc2, byte[] cc3)
        {
            byte[] result = new byte[cc1.Length];
            for (int i = 0; i < cc1.Length; i++)
            {
                byte b1 = cc1[i];
                byte b2 = cc2[i];
                byte b3 = cc3[i];
                result[i] = (byte)(b1 ^ b2 ^ b3);
            }
            return result;
        }

        public static string CombineMasterKey(string[] keyParts)
        {
            var br = StringToByteArray(keyParts[0]);
            if (keyParts.Length == 3)
            {
                return ByteArrayToString(XorCombine3(StringToByteArray(keyParts[0]), StringToByteArray(keyParts[1]), StringToByteArray(keyParts[2])));
            }
            else
            {
                for (int i = 1; i < keyParts.Length; i++)
                {
                    var b2 = StringToByteArray(keyParts[i]);
                    XorCombine(br, b2);
                }
            }
            return ByteArrayToString(br);
        }

        public string GetDomainMasterKey(string domain)
        {
            string ret;
            if (Infrastructure.Application.IsProduction)
            {
                var keyParts = new List<string>();
                for (int i = 0; i < 5; i++)
                {
                    var kp = Domain.Get(domain).GetConfig(CONFIG_FILE_KEY_NAME + (i + 1), (i == 0));
                    if (kp == null) break;
                    keyParts.Add(kp);
                }
                ret = CombineMasterKey(keyParts.ToArray());
            }
            else ret = CombineMasterKey(MASTER_KEY_DEV);
            Logger.Log("Loaded " + CLASS_NAME + " domain: " + domain + " key, CheckValue:" + GenerateCheckValue(ret) + (Infrastructure.Application.IsProduction ? "" : " (Test)"));
            return ret;
        }

        public string[] GenerateMasterKey(int parts)
        {
            var ret = new string[parts];
            using (var cry = CreateCrypto(null))
            {
                for (int i = 0; i < parts; i++)
                {
                    cry.GenerateKey();
                    ret[i] = ByteArrayToString(cry.Key);
                }
            }
            return ret;
        }

        protected string GenerateCheckValue(string key)
        {
            return Encrypt(StringToByteArray("0000000000000000"), key).Substring(0, 6);
        }

        public string GenerateCheckValue(string[] parts)
        {
            return GenerateCheckValue(CombineMasterKey(parts));
        }

        public static byte[] XmlDocToByteArray(System.Xml.XmlDocument doc)
        {
            var strOut = doc.OuterXml;
            var responseBytes = new byte[2048];
            var dataLength = System.Text.Encoding.UTF8.GetBytes(strOut, 0, strOut.Length, responseBytes, 2);
            responseBytes[0] = (byte)(dataLength / 256); responseBytes[1] = (byte)(dataLength % 256);
            Array.Resize(ref responseBytes, (int)dataLength + 2);
            return responseBytes;
        }
        #endregion

        #region Iso8583PostXml

        public class Iso8583PostXml : System.Xml.XmlDocument
        {
            public DateTime RequestDate { get; set; }
            public Iso8583PostXml(System.IO.Stream stream) { RequestDate = DateTime.Now; Load(stream); }
            public Iso8583PostXml(string xmlData) { RequestDate = DateTime.Now; LoadXml(xmlData); }

            private Iso8583Base Container { get; set; }

            public Iso8583PostXml(Iso8583Base container)
            {
                Container = container;
                RequestDate = DateTime.Now;
                AppendChild(CreateXmlDeclaration("1.0", System.Text.Encoding.UTF8.WebName, null));
                AppendChild(CreateElement("Iso8583PostXml"));
                DocumentElement.AppendChild(CreateElement("MsgType"));
                DocumentElement.AppendChild(CreateElement("Fields"));
            }
            public System.Xml.XmlElement Fields { get { return DocumentElement["Fields"]; } }

            public string MessageType
            {
                get
                {
                    var ret = DocumentElement.SelectSingleNode("MsgType");
                    if (ret == null) return null;
                    return ret.InnerText;
                }
                set
                {
                    var ret = DocumentElement.SelectSingleNode("MsgType");
                    if (ret == null) throw new Exception("MsgType element not found");
                    ret.InnerText = value;
                }
            }

            public string GetField(string fieldName)
            {
                var ret = Fields[fieldName];
                if (ret == null) return null;
                return ret.InnerText;
            }

            public void SetField(string fieldName, string value)
            {
                var ret = Fields[fieldName];
                if (value == null)
                {
                    if (ret != null) ret.ParentNode.RemoveChild(ret);
                    return;
                }
                if (ret == null)
                {
                    ret = CreateElement(fieldName);
                    Fields.AppendChild(ret);
                }
                ret.InnerText = value;
            }

            public void CopyFrom(Iso8583PostXml src, string[] fieldNames)
            {
                foreach (var f in fieldNames) SetField(f, src.GetField(f));
            }

            public string PAN { get { return GetField("Field_002"); } set { SetField("Field_002", value); } }
            public decimal Amount { get { return GetField("Field_004").ToNullableDecimal().GetValueOrDefault() / 100; } set { SetField("Field_004", ((int)(value * 100)).ToString()); } }
            public decimal BalanceAmount { get { return MessageType.StartsWith("04") ? Amount : -Amount; } }
            public string CurrencyIsoNumber { get { return GetField("Field_049"); } set { SetField("Field_049", value); } }

            public string TerminalNumber { get { return GetField("Field_041"); } set { SetField("Field_041", value); } }
            public string MerchantNumber { get { return GetField("Field_042"); } set { SetField("Field_042", value); } }

            public string ReferenceCode { get { return GetField("Field_037"); } set { SetField("Field_037", value); } }
            public int? ReferenceNumber { get { return GetField("Field_011").ToNullableInt(); } set { SetField("Field_011", value != null ? value.ToString() : (string)null); } }

            public DateTime? ExpDate
            {
                get
                {
                    var dateExpString = GetField("Field_014").EmptyIfNull();
                    if (dateExpString.Length == 4 && dateExpString.Substring(2, 2).ToNullableInt().GetValueOrDefault() <= 12)
                        return new DateTime(2000 + dateExpString.Substring(0, 2).ToNullableInt().GetValueOrDefault(), dateExpString.Substring(2, 2).ToNullableInt().GetValueOrDefault(1), 1).AddMonths(1).AddDays(-1);
                    return null;
                }
                set { SetField("Field_014", value != null ? value.Value.ToString("MMYY") : (string)null); }
            }

            public string ResponseCode { get { return GetField("Field_039"); } set { SetField("Field_039", value); } }

            public string GetPinCode(Client client)
            {
                var pan = PAN;
                var encryptedPin = GetField("Field_052");
                if (string.IsNullOrEmpty(encryptedPin)) return null;
                var pinBytes = client.Container.Decrypt(encryptedPin, client.PinWorkingKey);
                XorCombine(pinBytes, StringToByteArray("0000" + pan.Substring(pan.Length - 13, 12)));
                encryptedPin = ByteArrayToString(pinBytes);
                return encryptedPin.Substring(2, 4);
            }

            public void SetPinCode(Client client, string pinCode)
            {
                var pan = PAN;
                var encryptedPin = StringToByteArray("0" + pinCode.Length.ToString() + pinCode + (new string('F', 14 - pinCode.Length)));
                XorCombine(encryptedPin, StringToByteArray("0000" + pan.Substring(pan.Length - 13, 12)));
                SetField("Field_052", Container.Encrypt(encryptedPin, client.PinWorkingKey));
            }
        }
        #endregion

        #region TcpClient
        public class Client : IDisposable
        {
            public class MessageReceivedArgs : System.EventArgs { public Iso8583PostXml Request; }
            public event EventHandler<MessageReceivedArgs> MessageReceived;
            public string MasterKey { get; set; }
            public string LogFileName { get; set; }
            public bool LogCommunication { get; set; }
            public Iso8583Base Container { get; private set; }

            public bool IsLoggedIn { get; internal set; }
            public string PinWorkingKey { get; internal set; }
            public System.Net.Sockets.Socket TcpClient { get; private set; }

            public bool IsConnected { get { lock (TcpClient) return (TcpClient != null) && (TcpClient.Connected); } }

            private DateTime _lastActive = DateTime.Now;
            private System.Threading.ManualResetEvent sendComplete, receiveComplete;
            private Iso8583PostXml _lastRecievedMessage;
            private int dataPosition = 0;
            private byte[] data;

            public Client(string masterKey)
            {
                data = new byte[2048];
                sendComplete = new System.Threading.ManualResetEvent(true);
                receiveComplete = new System.Threading.ManualResetEvent(false);
                MessageReceived += Client_MessageReceived;
                MasterKey = masterKey;
            }

            public Client(System.Net.Sockets.Socket client, Iso8583Base iso8583Base)
            {
                Container = iso8583Base;
                LogFileName = Domain.Get(iso8583Base.DomainHost).MapPrivateDataPath(@"Logs\" + iso8583Base.CLASS_NAME + DateTime.Now.ToString("_yyyyMMdd") + ".txt");
                MasterKey = iso8583Base.GetDomainMasterKey(iso8583Base.DomainHost);
                LogCommunication = iso8583Base.LogCommunication;
                sendComplete = new System.Threading.ManualResetEvent(true);
                data = new byte[2048];
                TcpClient = client;
                MessageReceived += iso8583Base.Server_MessageReceived;
                TcpClient.BeginReceive(data, 0, data.Length, System.Net.Sockets.SocketFlags.None, ReceiveCallback, null);
                WriteLog(Infrastructure.LogSeverity.Info, client, "Server Connect, Active Clients " + (Container.Connections.Count + 1));
            }

            public void Dispose()
            {
                if (TcpClient != null) TcpClient.Dispose();
                if (sendComplete != null) { sendComplete.Dispose(); sendComplete = null; }
                if (receiveComplete != null) { receiveComplete.Dispose(); receiveComplete = null; }
            }

            public void Connect(string address)
            {
                if (TcpClient == null) TcpClient = new System.Net.Sockets.Socket(System.Net.Sockets.AddressFamily.InterNetwork, System.Net.Sockets.SocketType.Stream, System.Net.Sockets.ProtocolType.Tcp);
                TcpClient.Connect(address, Container.TCP_SERVER_PORT);
                dataPosition = 0;
                TcpClient.BeginReceive(data, 0, data.Length, System.Net.Sockets.SocketFlags.None, ReceiveCallback, null);
            }

            public void Close()
            {
                lock (this)
                {
                    if (TcpClient == null) return;
                    try { TcpClient.Shutdown(System.Net.Sockets.SocketShutdown.Both); }
                    catch { }
                    if (Container != null)
                    {
                        lock (Container.Connections) Container.Connections.Remove(this);
                        WriteLog(Infrastructure.LogSeverity.Info, TcpClient, "Server Disconnect, Active Clients " + Container.Connections.Count);
                    }
                    else WriteLog(Infrastructure.LogSeverity.Info, TcpClient, "Client Disconnect");
                    lock (this)
                    {
                        TcpClient.Close();
                        //TcpClient.Dispose();
                        TcpClient = null;
                    }
                }
            }

            private static object _logLock = new object();
            private void WriteLog(LogSeverity? severity, System.Net.Sockets.Socket socket, string text)
            {
                System.Net.EndPoint epSrc = null, epDst = null;
                try
                {
                    epSrc = socket.LocalEndPoint;
                    epDst = socket.RemoteEndPoint;
                }
                catch { }
                var ret = string.Format("\r\n{0} {1} {2} - {3}", DateTime.Now.ToString("u"), epSrc, epDst, text.EmptyIfNull().TruncEnd(50));
                lock (_logLock)
                {
                    System.Console.Write(ret);
                    System.IO.File.AppendAllText(LogFileName, ret);
                }
                if (severity.HasValue) Logger.Log(severity.Value, LogTag.Process, text);
            }

            private void ReceiveCallback(IAsyncResult callbackResult)
            {
                int bytesRead;
                lock (this)
                {
                    _lastActive = DateTime.Now;
                    if (TcpClient == null || !TcpClient.Connected)
                    {
                        bytesRead = -1;
                    }
                    else
                    {
                        try
                        {
                            System.Net.Sockets.SocketError errorCode;
                            bytesRead = TcpClient.EndReceive(callbackResult, out errorCode);
                            if (errorCode != System.Net.Sockets.SocketError.Success)
                                bytesRead = 0;
                        }
                        catch (Exception ex)
                        {
                            WriteLog(Infrastructure.LogSeverity.Error, TcpClient, "RECV ERR:" + ex.Message);
                            bytesRead = -1;
                        }
                    }
                }
                if (bytesRead <= 0) { Close(); return; }
                dataPosition += bytesRead;
                if (dataPosition > 2)  //wait for message length
                {
                    int messageLength = ((data[0] * 256) + data[1]) + 2;
                    if (bytesRead >= messageLength)
                    {
                        var request = new Iso8583PostXml(new System.IO.MemoryStream(data, 2, messageLength - 2));
                        if (LogCommunication) WriteLog(null, TcpClient, "R:" + request.InnerText);
                        System.Threading.ThreadPool.QueueUserWorkItem(OnProcessRequest, request);
                        if (dataPosition > messageLength)
                            Array.Copy(data, messageLength, data, 0, dataPosition - messageLength);
                        dataPosition -= messageLength;
                    }
                }
                lock (this)
                    if (TcpClient != null || TcpClient.Connected)
                        try
                        {
                            TcpClient.BeginReceive(data, dataPosition, data.Length - dataPosition, System.Net.Sockets.SocketFlags.None, ReceiveCallback, null);
                        }
                        catch (Exception ex)
                        {
                            WriteLog(Infrastructure.LogSeverity.Error, TcpClient, "RECV ERR:" + ex.Message);
                        }
            }

            protected void OnProcessRequest(object state)
            {
                try
                {
                    Iso8583PostXml request = state as Iso8583PostXml;
                    if (MessageReceived != null) MessageReceived(this, new MessageReceivedArgs() { Request = request });
                    if (receiveComplete != null)
                    {
                        _lastRecievedMessage = request;
                        receiveComplete.Set();
                        receiveComplete.Reset();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(ex, Container.CLASS_NAME + " OnProcessRequest");
                }
            }

            public Iso8583PostXml Send(Iso8583PostXml data, int waitMiliseconds = 0)  /*-1 infinitie*/
            {
                var sendBytes = XmlDocToByteArray(data);
                lock (this)
                    if (TcpClient == null || !TcpClient.Connected) return null;
                if (!sendComplete.WaitOne(60 * 1000))
                { //wait 1 min
                    WriteLog(Infrastructure.LogSeverity.Error, TcpClient, "SEND ERR: Unable to send, another send is in progress");
                    if (Container == null) throw new Exception("Unable to send, another send is in progress");
                    else return null;
                }
                sendComplete.Reset();

                lock (this)
                {
                    if (LogCommunication) WriteLog(null, TcpClient, "S:" + data.InnerText);
                    TcpClient.BeginSend(sendBytes, 0, sendBytes.Length, System.Net.Sockets.SocketFlags.None, SendCallback, null);
                }
                if (waitMiliseconds == 0 || receiveComplete == null) return null;
                if (!receiveComplete.WaitOne(waitMiliseconds)) return null;
                return _lastRecievedMessage;
            }

            private void SendCallback(IAsyncResult callbackResult)
            {
                int bytesSent;
                lock (this) _lastActive = DateTime.Now;
                try { bytesSent = TcpClient.EndSend(callbackResult); }
                catch (Exception ex)
                {
                    bytesSent = -1;
                    WriteLog(Infrastructure.LogSeverity.Error, TcpClient, "SEND ERR: " + ex.Message);
                }
                if (bytesSent <= 0 || !TcpClient.Connected) { Close(); return; }
                sendComplete.Set();
            }

            public void SendKeepAlive()
            {
                DateTime lastActive;
                lock (this)
                {
                    if (TcpClient == null || !TcpClient.Connected) return;
                    lastActive = _lastActive;
                }
                if (DateTime.Now.Subtract(lastActive).Seconds > Container.KeepAliveSeconds)
                {
                    var sendDate = DateTime.Now;
                    var data = new Iso8583PostXml(Container);
                    data.MessageType = "0800";
                    data.SetField("Field_007", sendDate.ToUniversalTime().ToString("MMddHHmmss"));
                    data.SetField("Field_011", sendDate.ToString("HHmmss"));
                    data.SetField("Field_012", sendDate.ToString("HHmmss"));
                    data.SetField("Field_013", sendDate.ToString("MMdd"));
                    data.SetField("Field_070", "301");
                    Send(data);
                    //lock(this) _lastActive = DateTime.Now;
                }
            }

            private void Client_MessageReceived(object sender, Client.MessageReceivedArgs e)
            {
                var request = e.Request;
                if (request.MessageType == "0810")
                {
                    var messageSubType = request.GetField("Field_070");
                    switch (messageSubType)
                    {
                        case "001": //Sign on
                        case "002": //Sign off
                            if (request.GetField("Field_039") != "00") return; //response code
                            IsLoggedIn = (messageSubType == "001");
                            break;
                        case "101":
                            if (request.GetField("Field_039") != "00") return; //response code
                            PinWorkingKey = Iso8583Base.ByteArrayToString(Container.Decrypt(request.GetField("Field_053").Substring(0, 16), MasterKey));
                            if (Container.GenerateCheckValue(PinWorkingKey) != request.GetField("Field_053").Substring(16, 6)) PinWorkingKey = null;
                            break;
                    }
                }
            }
        }
        #endregion

        #region TcpServer
        public bool IsRunning { get; private set; }
        public string DomainHost { get; internal set; }
        public int ServerPort { get; internal set; }
        protected System.Net.Sockets.Socket tcpListener;
        protected List<Client> connections;
        protected System.Threading.Timer keepAliveTimer;
        public int KeepAliveSeconds { get; set; }
        public static System.Threading.ManualResetEvent allDone = new System.Threading.ManualResetEvent(false);

        public List<Client> Connections
        {
            get
            {
                if (connections == null) connections = new List<Client>();
                return connections;
            }
        }

        private void ListenerCallback(IAsyncResult callbackResult)
        {
            System.Net.Sockets.Socket client = null;

            try
            {
                lock (this)
                {
                    if (!IsRunning) return;
                    try
                    {
                        client = tcpListener.EndAccept(callbackResult);
                    }
                    catch (Exception ex1)
                    {
                        Infrastructure.Logger.Log(ex1, CLASS_NAME + " ListenerCallback");
                    }
                }
                if (client == null) return;
                var newClient = new Client(client, this);
                lock (Connections) Connections.Add(newClient);
            }
            catch (Exception ex)
            {
                Infrastructure.Logger.Log(ex, CLASS_NAME + " ListenerCallback");
            }


            tcpListener.BeginAccept(new AsyncCallback(ListenerCallback), null);
        }

        private void Server_MessageReceived(object sender, Client.MessageReceivedArgs e)
        {
            var client = sender as Iso8583Base.Client;

            #region Detailed logging for ACS until testing is done.
            /// TODO: Take out this entire code block on go live
            if (!Infrastructure.Application.IsProduction && !string.IsNullOrEmpty(e.Request.MessageType) && e.Request.MessageType != "0810" && e.Request.MessageType != "0800")
                Infrastructure.Logger.Log(LogSeverity.Info, LogTag.Process, CLASS_NAME + " XML Request - " + e.Request.MessageType, e.Request.OuterXml);

            #endregion

            var response = ProcessRequest(client, e.Request);

            #region Detailed logging for ACS until testing is done.
            /// TODO: Take out this entire code block on go live
            if (!Infrastructure.Application.IsProduction && !string.IsNullOrEmpty(e.Request.MessageType) && e.Request.MessageType != "0810" && e.Request.MessageType != "0800")
                Infrastructure.Logger.Log(LogSeverity.Info, LogTag.Process, CLASS_NAME + " XML Response - " + e.Request.MessageType, (response != null ? response.OuterXml : "[NULL]"));

            #endregion

            if (response != null) client.Send(response);
        }

        private void SendKeepAlive(object state)
        {
            Client[] conCopy = null;
            lock (Connections) conCopy = Connections.ToArray();
            foreach (var c in conCopy) c.SendKeepAlive();
        }

        public void StartTcpServer()
        {
            try
            {
                var localEndPoint = new System.Net.IPEndPoint(System.Net.IPAddress.Any, TCP_SERVER_PORT);
                lock (this)
                {
                    if (tcpListener == null) tcpListener = new System.Net.Sockets.Socket(System.Net.Sockets.AddressFamily.InterNetwork, System.Net.Sockets.SocketType.Stream, System.Net.Sockets.ProtocolType.Tcp);
                    tcpListener.Bind(localEndPoint);
                    tcpListener.Listen(Int32.MaxValue);
                    IsRunning = true;
                    tcpListener.BeginAccept(new AsyncCallback(ListenerCallback), null);
                    if (KeepAliveSeconds != 0 && keepAliveTimer == null) keepAliveTimer = new System.Threading.Timer(SendKeepAlive, null, KeepAliveSeconds * 1000, KeepAliveSeconds * 1000);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                StartTcpServer();
            }
        }

        public void StopTcpServer()
        {
            lock (this)
            {
                if (keepAliveTimer != null) { keepAliveTimer.Dispose(); keepAliveTimer = null; }
                tcpListener.Close();

                Client[] conCopy = null;
                lock (Connections) conCopy = Connections.ToArray();
                foreach (var con in conCopy) con.Close();
                Connections.Clear();

                tcpListener.Dispose(); tcpListener = null;
                IsRunning = false;
            }
        }
        #endregion

        private int ExecuteTransaction(Client client, Iso8583PostXml request, out Transactions.Transaction transaction)
        {
            int ret = 0;
            transaction = null;
            System.Net.IPAddress ipAddress = new System.Net.IPAddress(0);

            if (client == null || client.TcpClient == null || client.TcpClient.RemoteEndPoint == null)
                return -95;

            ipAddress = (client.TcpClient.RemoteEndPoint as System.Net.IPEndPoint).Address;
            Infrastructure.ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);
            try
            {
                var curObj = Bll.Currency.GetByIsoNumber(request.CurrencyIsoNumber.EmptyIfNull().ToNullableInt().GetValueOrDefault(-1));

                var terminal = Bll.DebitCompanies.Terminal.LoadWithExtAccountID(DEBITCOMPANY_ID, request.MerchantNumber);
                if (terminal == null) return -6; //Error
                var refTermianl = (from n in DataContext.Reader.tblCompanyCreditFeesTerminals where n.CCFT_Terminal == terminal.TerminalNumber select n).FirstOrDefault();
                if (refTermianl.CCFT_CompanyID == null) return -6; //Error

                var spms = Bll.Accounts.StoredPaymentMethod.FindByPAN(request.PAN, null);
                var spm = spms.FirstOrDefault(); //.Where(s => s.ProviderID == STOREDPM_PROVIDERNAME)

                if (spm == null) ret = -14; //Invalid card number
                else if (request.Amount == 0) ret = -13; //invalid amount
                else if (curObj == null) ret = -39; //No credit account
                string pin = null;
                Bll.Customers.Customer customer = null;

                string strBalanceText = CLASS_NAME + ": " + request.ReferenceNumber;

                if (this.IMPLEMENTATION_TYPE == Iso8583Factory.Implementation.ACS)
                    strBalanceText = CLASS_NAME + ": " + request.ReferenceCode;

                if (spm != null && ret == 0)
                {
                    //if (Infrastructure.Application.IsProduction)
                    pin = request.GetPinCode(client);
                    //else
                    //    pin = request.GetField("Field_052");

                    if (!Infrastructure.Application.IsProduction)
                        Logger.Log(CLASS_NAME + " ,pincode decrypted is:" + pin);

                    //if (Infrastructure.Application.IsProduction)
                    //{
                    if ((!string.IsNullOrEmpty(pin)) && !Bll.Accounts.Account.ValidatePinCode(spm.Account_id, pin)) ret = -55; //Incorrect PIN
                                                                                                                               //else if (expDate.Month != spm.MethodInstance.ExpirationMonth || expDate.Year != spm.MethodInstance.ExpirationYear) return 
                    else
                    {
                        //if (Infrastructure.Application.IsProduction)
                        if (spm.MethodInstance.ExpirationDate.GetValueOrDefault(DateTime.MaxValue) < DateTime.Now) ret = -54; //Expired card
                    }
                    //}

                    customer = Customers.Customer.LoadObject(spm.Account_id) as Customers.Customer;

                    //var currenctBalace = Bll.Accounts.Balance.GetStatus(spm.Account_id, curObj.IsoCode).Expected;

                    Dal.Netpay.NetpayDataContextBase dc = new Dal.Netpay.NetpayDataContextBase(Domain.Current.Sql1ConnectionString);

                    if (!request.MessageType.StartsWith("04"))
                    {
                        try
                        {
                            var currenctBalace = (from p in dc.AccountBalances
                                                  where p.Account_id == spm.Account_id
                                                  && p.CurrencyISOCode == curObj.IsoCode
                                                  select p.TotalBalance).First();

                            if (currenctBalace < -request.BalanceAmount) ret = -51; //Not sufficient funds
                        }
                        catch { ret = -51; }
                    }
                    else
                    {
                        // Check if previous reversal exists
                        var pExistingReversal = Bll.Accounts.Balance.Search(new Accounts.Balance.SearchFilters()
                        {
                            Text = strBalanceText,
                            Amount = new Range<decimal?>(request.BalanceAmount),
                            AccountIDs = new List<int>() { spm.Account_id },
                            SourceType = Bll.Accounts.Balance.SOURCE_PURCHASE
                        }, null);

                        if (pExistingReversal.Count > 0)
                            return -26;

                    }
                }
                if (customer == null)
                {
                    customer = new Bll.Customers.Customer();
                    if (ret == 0) ret = -14; //Error
                }
                if (ret == 0)
                { //validate merchant group by app identity
                    var merchantGroup = Merchants.Merchant.GetMerchantsGroup(new List<int>() { refTermianl.CCFT_CompanyID.GetValueOrDefault() }).FirstOrDefault().Value;
                    if (!customer.CanAcceptMerchantGroup(merchantGroup)) ret = -56; //No card record
                }
                int OriginalTransId = 0;
                string authorizationCode = DateTime.Now.Ticks.ToString("000000").TruncStart(6);
                var trans = new Transactions.Transaction()
                {
                    Status = (ret < 0 ? TransactionStatus.Declined : TransactionStatus.Captured),
                    ReplyCode = System.Math.Abs(ret).ToString("000"),
                    TerminalNumber = terminal.TerminalNumber,
                    PaymentsIDs = ";0;",
                    IP = ipAddress.ToString(),
                    OrderNumber = request.ReferenceNumber.GetValueOrDefault().ToString(),
                    Amount = request.Amount,
                    CurrencyID = curObj.ID,
                    MerchantID = refTermianl.CCFT_CompanyID.GetValueOrDefault(),
                    DebitReferenceCode = request.ReferenceCode,
                    DebitCompanyID = DEBITCOMPANY_ID,
                    InsertDate = DateTime.Now,
                    ApprovalNumber = authorizationCode,
                    Installments = 1,
                    TransactionSource = TransactionSource.DebitCards,
                    TransType = TransactionType.Capture,
                    CreditType = (request.BalanceAmount < 0 ? CreditType.Regular : CreditType.Refund),
                    CustomerID = customer.CustomerID,
                    //UnsettledInstallments = 1,
                    MerchantPayDate = DateTime.Now.Date.AddDays(1).Date,
                    PayForText = CLASS_NAME + " POS",
                    OriginalTransactionID = OriginalTransId,
                    PaymentData = new Transactions.Payment() { MerchantID = refTermianl.CCFT_CompanyID.GetValueOrDefault() },
                    PayerData = new Transactions.Payer() { FullName = customer.AccountName, EmailAddress = customer.EmailAddress, PersonalNumber = customer.PersonalNumber, PhoneNumber = customer.PhoneNumber }
                };
                if (trans.Status == TransactionStatus.Captured) trans.BatchData = request.OuterXml;
                trans.PaymentData.MethodInstance = new PaymentMethods.CreditCard(trans.PaymentData) { CardNumber = request.PAN, Cvv = pin, ExpirationDate = request.ExpDate, PaymentMethodId = spm.MethodInstance.PaymentMethodId };

                var refFees = Merchants.ProcessTerminals.Load(refTermianl.CCFT_CCF_ID.GetValueOrDefault());
                if (refFees != null) trans.SetTransactionFees(refFees);
                trans.Save();
                transaction = trans;
                if (ret < 0) return ret;

                var balance = Bll.Accounts.Balance.Create(spm.Account_id, curObj.IsoCode, request.BalanceAmount, strBalanceText, Bll.Accounts.Balance.SOURCE_PURCHASE, trans.ID, false);
                if (refFees != null && refFees.CashbackPercent != 0 && trans.CreditType == CreditType.Regular)
                {
                    var cashbackAmount = (refFees.CashbackPercent * request.Amount) / 100;
                    var cashbackTrans = trans.CreateRefundTransaction(cashbackAmount);
                    cashbackTrans.IsCashback = true;
                    cashbackTrans.Comment = "Cashback from trans #" + trans.ID;
                    cashbackTrans.Save();
                    Bll.Accounts.Balance.Create(spm.Account_id, curObj.IsoCode, cashbackAmount, "Cashback transaction #" + request.ReferenceNumber, Bll.Accounts.Balance.SOURCE_PURCHASE, trans.ID, false);
                }

                #region Cashback Reversal

                if (refFees != null && refFees.CashbackPercent != 0 && trans.CreditType == CreditType.Refund)
                {
                    var cashbackAmount = ((refFees.CashbackPercent * request.Amount) / 100) * -1;

                    var cashbackTrans = trans.CreateRefundTransaction(cashbackAmount);
                    cashbackTrans.IsCashback = true;
                    cashbackTrans.Comment = "Cashback reverse from trans #" + trans.ID;
                    cashbackTrans.Save();
                    Bll.Accounts.Balance.Create(spm.Account_id, curObj.IsoCode, cashbackAmount, "Cashback reverse transaction #" + request.ReferenceNumber, Bll.Accounts.Balance.SOURCE_PURCHASE, trans.ID, false);
                }

                #endregion

                return authorizationCode.ToNullableInt().GetValueOrDefault();
            }
            finally
            {
                Infrastructure.ObjectContext.Current.StopImpersonate();
            }

            #region Stress Testing
            //int ret = 0;
            //transaction = null;
            //System.Net.IPAddress ipAddress = new System.Net.IPAddress(0);

            //if (client == null || client.TcpClient == null || client.TcpClient.RemoteEndPoint == null)
            //    return -95;

            //ipAddress = (client.TcpClient.RemoteEndPoint as System.Net.IPEndPoint).Address;
            //Infrastructure.ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);
            //Dal.Netpay.NetpayDataContextBase dc = new Dal.Netpay.NetpayDataContextBase(Domain.Current.Sql1ConnectionString);

            //try
            //{

            //    //revert
            //    var curObj = Bll.Currency.GetByIsoNumber(request.CurrencyIsoNumber.EmptyIfNull().ToNullableInt().GetValueOrDefault(-1));

            //    var pTerminalNumber = dc.SpGetTerminalNumber(DEBITCOMPANY_ID, request.MerchantNumber).FirstOrDefault();

            //    if (pTerminalNumber == null || string.IsNullOrEmpty(pTerminalNumber.terminalNumber))
            //        return -6; //Error

            //    #region Old

            //    //var terminal = Bll.DebitCompanies.Terminal.LoadWithExtAccountID(DEBITCOMPANY_ID, request.MerchantNumber);
            //    //if (terminal == null) return -6; //Error
            //    //var refTermianl = (from n in DataContext.Reader.tblCompanyCreditFeesTerminals where n.CCFT_Terminal == terminal.TerminalNumber select n).FirstOrDefault();
            //    //if (refTermianl.CCFT_CompanyID == null) return -6; //Error

            //    //var spms = Bll.Accounts.StoredPaymentMethod.FindByPAN(request.PAN, null);
            //    //var spm = spms.FirstOrDefault(); //.Where(s => s.ProviderID == STOREDPM_PROVIDERNAME)

            //    #endregion

            //    var pCustomerDetailsForISO = dc.SpGetCustomerDetailsForISOUsingPAN(request.PAN ).FirstOrDefault();

            //    if (pCustomerDetailsForISO == null) ret = -14; //Invalid card number
            //    else if (request.Amount == 0) ret = -13; //invalid amount
            //    else if (curObj == null) ret = -39; //No credit account
            //    string pin = null;
            //    Bll.Customers.Customer customer = null;
            //    if (pCustomerDetailsForISO != null && ret == 0)
            //    {
            //        if (Infrastructure.Application.IsProduction)
            //            pin = request.GetPinCode(client);
            //        else
            //            pin = request.GetField("Field_052");

            //        //Logger.Log(CLASS_NAME + " ,pincode decrypted is:" + pin);

            //        if (Infrastructure.Application.IsProduction)
            //            if ((!string.IsNullOrEmpty(pin)) && !Bll.Accounts.Account.ValidatePinCodeWithoutLoadingAccount(pCustomerDetailsForISO.PincodeSHA256, pin)) ret = -55; //Incorrect PIN
            //                                                                                                                       //else if (expDate.Month != spm.MethodInstance.ExpirationMonth || expDate.Year != spm.MethodInstance.ExpirationYear) return 
            //            else if (pCustomerDetailsForISO.ExpirationDate.GetValueOrDefault(DateTime.MaxValue) < DateTime.Now) ret = -54; //Expired card
            //        //customer = Customers.Customer.LoadObject(spm.Account_id) as Customers.Customer;

            //        //var currenctBalace = Bll.Accounts.Balance.GetStatus(spm.Account_id, curObj.IsoCode).Expected;

            //        try
            //        {
            //            var currenctBalace = (from p in dc.AccountBalances
            //                                  where p.data_Account.Customer_id == pCustomerDetailsForISO.Customer_id
            //                                  && p.CurrencyISOCode == curObj.IsoCode
            //                                  select p.TotalBalance).First();

            //            if (request.BalanceAmount < 0 && currenctBalace < -request.BalanceAmount) ret = -51; //Not sufficient funds
            //        }
            //        catch { ret = -51; }
            //    }
            //    //if (customer == null)
            //    //{
            //    //    customer = new Bll.Customers.Customer();
            //    //    if (ret == 0) ret = -14; //Error
            //    //}

            //    /*
            //    if (ret == 0)
            //    { //validate merchant group by app identity
            //        var merchantGroup = Merchants.Merchant.GetMerchantsGroup(new List<int>() { pTerminalNumber.CCFT_CompanyID.GetValueOrDefault() }).FirstOrDefault().Value;
            //        if (!customer.CanAcceptMerchantGroup(merchantGroup)) ret = -56; //No card record
            //    }
            //    */

            //    int OriginalTransId = 0;
            //    string authorizationCode = DateTime.Now.Ticks.ToString("000000").TruncStart(6);
            //    /*
            //    var trans = new Transactions.Transaction()
            //    {
            //        Status = (ret < 0 ? TransactionStatus.Declined : TransactionStatus.Captured),
            //        ReplyCode = System.Math.Abs(ret).ToString("000"),
            //        TerminalNumber = pTerminalNumber.terminalNumber,
            //        PaymentsIDs = ";0;",
            //        IP = ipAddress.ToString(),
            //        OrderNumber = request.ReferenceNumber.GetValueOrDefault().ToString(),
            //        Amount = request.Amount,
            //        CurrencyID = curObj.ID,
            //        MerchantID = pTerminalNumber.CCFT_CompanyID.GetValueOrDefault(),
            //        DebitReferenceCode = request.ReferenceCode,
            //        DebitCompanyID = DEBITCOMPANY_ID,
            //        InsertDate = DateTime.Now,
            //        ApprovalNumber = authorizationCode,
            //        Installments = 1,
            //        TransactionSource = TransactionSource.DebitCards,
            //        TransType = TransactionType.Capture,
            //        CreditType = (request.BalanceAmount < 0 ? CreditType.Regular : CreditType.Refund),
            //        CustomerID = customer.CustomerID,
            //        //UnsettledInstallments = 1,
            //        MerchantPayDate = DateTime.Now.Date.AddDays(1).Date,
            //        PayForText = CLASS_NAME + " POS",
            //        OriginalTransactionID = OriginalTransId,
            //        PaymentData = new Transactions.Payment() { MerchantID = pTerminalNumber.CCFT_CompanyID.GetValueOrDefault() },
            //        PayerData = new Transactions.Payer() { FullName = customer.AccountName, EmailAddress = customer.EmailAddress, PersonalNumber = customer.PersonalNumber, PhoneNumber = customer.PhoneNumber }
            //    };
            //    if (trans.Status == TransactionStatus.Captured) trans.BatchData = request.OuterXml;
            //    trans.PaymentData.MethodInstance = new PaymentMethods.CreditCard(trans.PaymentData) { CardNumber = request.PAN, Cvv = pin, ExpirationDate = request.ExpDate, PaymentMethodId = spm.MethodInstance.PaymentMethodId };
            //    */
            //    //var refFees = Merchants.ProcessTerminals.Load(pTerminalNumber.CCFT_CCF_ID.GetValueOrDefault());
            //    //if (refFees != null) trans.SetTransactionFees(refFees);
            //    //trans.Save();
            //    //transaction = trans;
            //    //if (ret < 0) return ret;
            //    //var balance = Bll.Accounts.Balance.Create(spm.Account_id, curObj.IsoCode, request.BalanceAmount, CLASS_NAME + ": " + request.ReferenceNumber, Bll.Accounts.Balance.SOURCE_PURCHASE, trans.ID, false);
            //    //if (refFees != null && refFees.CashbackPercent != 0 && trans.CreditType == CreditType.Regular)
            //    //{
            //    //    var cashbackAmount = (refFees.CashbackPercent * request.Amount) / 100;
            //    //    var cashbackTrans = trans.CreateRefundTransaction(cashbackAmount);
            //    //    cashbackTrans.IsCashback = true;
            //    //    cashbackTrans.Comment = "Cashback from trans #" + trans.ID;
            //    //    cashbackTrans.Save();
            //    //    Bll.Accounts.Balance.Create(spm.Account_id, curObj.IsoCode, cashbackAmount, "Cashback transaction #" + request.ReferenceNumber, Bll.Accounts.Balance.SOURCE_PURCHASE, trans.ID, false);
            //    //}
            //    return authorizationCode.ToNullableInt().GetValueOrDefault();
            //}
            //finally
            //{
            //    Infrastructure.ObjectContext.Current.StopImpersonate();
            //}
            #endregion Stress Testing
        }

        private int CheckTransactionExist(string pan, string currencyIsoNumber, decimal? amount, int? sourceId, string transReference)
        {
            Infrastructure.ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);
            try
            {
                var curObj = Bll.Currency.GetByIsoNumber(currencyIsoNumber.EmptyIfNull().ToNullableInt().GetValueOrDefault(-1));
                var spms = Bll.Accounts.StoredPaymentMethod.FindByPAN(pan, null);
                var spm = spms.Where(s => s.ProviderID == STOREDPM_PROVIDERNAME).SingleOrDefault();
                if (spm == null) return -14; //Invalid card number
                if (curObj == null) return -39; //No credit account

                Accounts.Balance.SearchFilters searchFilters = new Accounts.Balance.SearchFilters()
                {
                    AccountID = spm.Account_id,
                    CurrencyIso = curObj.IsoCode,
                    InsertDate = new Range<DateTime?>(DateTime.Now.AddDays(-2), DateTime.Now.AddHours(1)),
                    IsPending = false,
                    SourceType = Bll.Accounts.Balance.SOURCE_PURCHASE,
                    SourceID = sourceId,
                    Amount = new Range<decimal?>(amount),
                    Text = CLASS_NAME + ": " + transReference,
                };

                if (this.IMPLEMENTATION_TYPE == Iso8583Factory.Implementation.ACS)
                    searchFilters = new Accounts.Balance.SearchFilters()
                    {
                        AccountID = spm.Account_id,
                        CurrencyIso = curObj.IsoCode,
                        InsertDate = new Range<DateTime?>(DateTime.Now.AddDays(-2), DateTime.Now.AddHours(1)),
                        IsPending = false,
                        SourceType = Bll.Accounts.Balance.SOURCE_PURCHASE,
                        Amount = new Range<decimal?>(amount * -1),
                        Text = CLASS_NAME + ": " + transReference,
                    };

                var balance = Bll.Accounts.Balance.Search(searchFilters, null).LastOrDefault();
                if (balance == null) return -25; //unable to locate transaction record
                return balance.ID;
            }
            finally
            {
                Infrastructure.ObjectContext.Current.StopImpersonate();
            }
        }

        private void AddRequestLog(Client client, Iso8583PostXml request, Iso8583PostXml response, Transactions.Transaction transaction)
        {
            //return;
            string merchantNum = null;
            if (transaction != null) merchantNum = (from c in DataContext.Reader.tblCompanies where c.ID == transaction.MerchantID select c.CustomerNumber).SingleOrDefault();
            var cal = new Process.ChargeAttemptLog()
            {
                DateStart = request.RequestDate,
                DateEnd = response.RequestDate,
                RequestMethod = "CUSTOM",
                MerchantNumber = merchantNum.EmptyIfNull(),
                RemoteIPAddress = (client.TcpClient.RemoteEndPoint as System.Net.IPEndPoint).Address.ToString(),
                HttpHost = (client.TcpClient.LocalEndPoint as System.Net.IPEndPoint).Address.ToString(),
                LocalIPAddress = (client.TcpClient.LocalEndPoint as System.Net.IPEndPoint).Address.ToString(),
                TransactionTypeId = TransactionSource.Unknown,
                ReplyCode = response.GetField("Field_039"),
                SessionContents = "PinKey:" + client.PinWorkingKey,
                QueryString = "",
                RequestForm = "",
                PathTranslate = CLASS_NAME + " TCP",
                //RequestString = request.OuterXml.TruncEnd(3500).Replace("<", "&lt;").Replace(">", "&gt;"),
                //ResponseString = response.OuterXml.TruncEnd(3500).Replace("<", "&lt;").Replace(">", "&gt;"),
                TransactionID = (transaction != null ? transaction.ID : (int?)null),
            };
            if (cal.ReplyCode == "00") cal.ReplyCode = "000";
            cal.Save();
        }

        private void ExecuteTransaction(Client client, Iso8583PostXml request, Iso8583PostXml response)
        {
            Transactions.Transaction transaction = null;
            int ret = -96; //System malfunction
            try
            {
                if (request.MessageType.Substring(1, 1) == "4")
                { //reversal
                    ret = CheckTransactionExist(request.PAN, request.CurrencyIsoNumber, request.Amount, request.ReferenceNumber, request.ReferenceCode);
                    if (ret > 0) ret = ExecuteTransaction(client, request, out transaction);
                }
                else
                {
                    if (request.MessageType.Substring(3, 1) == "1")
                        ret = CheckTransactionExist(request.PAN, request.CurrencyIsoNumber, request.BalanceAmount, request.ReferenceNumber, request.ReferenceCode);
                    if (ret < 0)
                        ret = ExecuteTransaction(client, request, out transaction);
                }
            }
            catch (Exception ex) { Logger.Log(LogTag.Process, ex, request.OuterXml); }
            if (ret > 0)
            {
                response.SetField("Field_038", ret.ToString("000000")); //Authorization ID response
                response.SetField("Field_039", "00"); //response. code
            }
            else response.SetField("Field_039", (-ret).ToString("00")); //response. code
            response.CopyFrom(request, new string[] { "Field_059" }); //echo
            response.RequestDate = DateTime.Now;
            //AddRequestLog(client, request, response, transaction);
        }

        private Iso8583PostXml ProcessRequest(Client client, Iso8583PostXml request)
        {
            Domain.Current = Domain.Get(DomainHost);
            Iso8583PostXml response = null;

            //StringBuilder sbToLog = new StringBuilder();
            //sbToLog.AppendLine("request.MessageType=" + request.MessageType);
            switch (request.MessageType)
            {
                //case "0810":
                case "0800": //network managment
                    response = new Iso8583PostXml(this);
                    response.MessageType = "0810"; //request.MessageType
                    var messageSubType = request.GetField("Field_070");
                    response.CopyFrom(request, new string[] { "Field_007", "Field_011", "Field_012", "Field_013" });

                    //sbToLog.AppendLine("messageSubType=" + messageSubType);

                    switch (messageSubType)
                    {
                        case "001": //Sign on
                        case "002": //Sign off
                            response.SetField("Field_039", "00"); //response code
                            response.CopyFrom(request, new string[] { "Field_070" });
                            client.IsLoggedIn = (messageSubType == "001");

                            //sbToLog.AppendLine("client.IsLoggedIn=" + client.IsLoggedIn.ToString());
                            break;
                        case "101": //key exchange 
                            response.SetField("Field_039", "00"); //response code
                            response.CopyFrom(request, new string[] { "Field_070" });
                            var keySend = GenerateKWP();
                            client.PinWorkingKey = keySend;
                            keySend = Encrypt(StringToByteArray(client.PinWorkingKey), GetDomainMasterKey(DomainHost)) + GenerateCheckValue(keySend); //add check digits
                            keySend = keySend.PadRight(96, '0');
                            response.SetField("Field_053", keySend); //send new key
                            Logger.Log(LogSeverity.Info, LogTag.Process, CLASS_NAME + " Key exchange new KWP check:" + GenerateCheckValue(client.PinWorkingKey));
                            break;
                        case "301": //echo message
                            response.SetField("Field_039", "00"); //response code
                            response.CopyFrom(request, new string[] { "Field_070" });
                            break;
                    }
                    break;
                case "0200": //transaction
                case "0201": //transaction repeat
                    //sbToLog.AppendLine("client.IsLoggedIn=" + client.IsLoggedIn.ToString());
                    if (Infrastructure.Application.IsProduction)
                        if (!client.IsLoggedIn) return null;
                    response = new Iso8583PostXml(this);
                    response.MessageType = "0210";
                    ExecuteTransaction(client, request, response);
                    break;
                case "0420": //reversal
                case "0421": //reversal repeat
                    //sbToLog.AppendLine("client.IsLoggedIn=" + client.IsLoggedIn.ToString());
                    if (Infrastructure.Application.IsProduction)
                        if (!client.IsLoggedIn) return null;
                    response = new Iso8583PostXml(this);
                    response.MessageType = "0430";
                    ExecuteTransaction(client, request, response);
                    break;
            }

            //if (this.IMPLEMENTATION_TYPE == Iso8583Factory.Implementation.ACS)
            //    System.IO.File.AppendAllText(client.LogFileName, sbToLog.ToString());

            return response;
        }

        public static Iso8583Base GetInstanceOfDomain(string domainHost)
        {
            return Domain.Get(domainHost).GetCachData("ACS", () => new Domain.CachData((new ACS(domainHost)), DateTime.MaxValue)) as ACS;
        }

        protected static void EnableServers(Iso8583Factory.Implementation implementationType, bool bEnable, string configKeyIsEnable, string configKeyLog)
        {
            int ret = 0;
            foreach (var v in Domain.Domains.Values)
            {
                if (!v.GetConfig(configKeyIsEnable, null).ToNullableBool().GetValueOrDefault()) continue;
                var i = Iso8583Factory.Create(implementationType, v.Host);
                i.LogCommunication = v.GetConfig(configKeyLog, null).ToNullableBool().GetValueOrDefault();
                if (bEnable) { if (!i.IsRunning) i.StartTcpServer(); }
                else { if (i.IsRunning) i.StopTcpServer(); }
                ret++;
            }
            Logger.Log(LogTag.AppInit, implementationType.ToString() + " Servers " + (bEnable ? "Started" : "Stopped") + ", server count:" + ret);
        }
    }
}
