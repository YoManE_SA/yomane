﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.Bll.ThirdParty
{
    public class PayU
    {
        #region Class Declaration

        #region Staging

        private static string strPayU_APIURL = "https://staging.payu.co.za/service/PayUAPI";
        private static string strPayU_Username = "Staging Integration Store 3";
        private static string strPayU_Password = "WSAUFbw6";
        private static string strPayU_SafeKey = "{07F70723-1B96-4B97-B891-7BF708594EEA}";

        #endregion

        #region Live

        /// TODO: Create entries in config file.

        #endregion

        #endregion


        public static TopUpWalletFromCreditCardResponse TopUpWalletFromCreditCard(int nCustomer_id, string strMerchantUserID, string strCustomerEmail, string strCustomerFirstName, string strCustomerLastName, string strCustomerCellNo, string strCustomerRegionalID, string strCustomerIPAddress, decimal fAmount, string strDescription, DateTime dtCCExpiryDate, string strPAN, string strCCV, string strNameOnCard, string strCountryCode = "27", string strCountryOfResidence = "ZAR", string strCurrencyCode = "ZAR")
        {
            TopUpWalletFromCreditCardResponse pResult = new PayU.TopUpWalletFromCreditCardResponse();

            string strMerchantRef = "PayU Topup - " + nCustomer_id;

            #region Customer

            Dal.DataAccess.NetpayDataContext dc = new Dal.DataAccess.NetpayDataContext(Infrastructure.Domain.Current.Sql1ConnectionString);

            #region Check if customer exist

            var pCustomerAccount = (from p in dc.Accounts
                                    where p.Customer_id == nCustomer_id
                                    select p).SingleOrDefault();

            if (pCustomerAccount == null)
            {
                pResult.GatewayResponse = new TopUpWalletFromCreditCardResponse.WalletResponse()
                {
                    resultCode = 1,
                    resultMessage = "Customer not found",
                    successful = false
                };
                return pResult;
            }

            #endregion


            #endregion

            #region PayU Post

            StringBuilder sbSOAPXML = new StringBuilder();

            sbSOAPXML.AppendLine("            <soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soap=\"http://soap.api.controller.web.payjar.com/\" xmlns:SOAPENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENV=\"SOAP-ENV\">");
            sbSOAPXML.AppendLine("   <soapenv:Header>");
            sbSOAPXML.AppendLine("      <wsse:Security SOAP-ENV:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">");
            sbSOAPXML.AppendLine("         <wsse:UsernameToken wsu:Id=\"UsernameToken-9\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">");
            sbSOAPXML.AppendLine("            <wsse:Username>" + strPayU_Username + "</wsse:Username>");
            sbSOAPXML.AppendLine("            <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">" + strPayU_Password + "</wsse:Password>");
            sbSOAPXML.AppendLine("         </wsse:UsernameToken>");
            sbSOAPXML.AppendLine("      </wsse:Security>");
            sbSOAPXML.AppendLine("   </soapenv:Header>");
            sbSOAPXML.AppendLine("   <soapenv:Body>");
            sbSOAPXML.AppendLine("      <soap:doTransaction xmlns:ns2=\"http://soap.api.controller.web.payjar.com/\">");
            sbSOAPXML.AppendLine("         <Api>ONE_ZERO</Api>");
            sbSOAPXML.AppendLine("         <Safekey>" + strPayU_SafeKey + "</Safekey>");
            sbSOAPXML.AppendLine("         <TransactionType>PAYMENT</TransactionType>");
            sbSOAPXML.AppendLine("         <AdditionalInformation>");
            sbSOAPXML.AppendLine("            <merchantReference>"+ strMerchantRef + "</merchantReference>");
            sbSOAPXML.AppendLine("            <notificationUrl>http://example.com/notification-page/</notificationUrl>");
            sbSOAPXML.AppendLine("         </AdditionalInformation>");
            sbSOAPXML.AppendLine("         <Customer>");
            sbSOAPXML.AppendLine("            <countryCode>" + strCountryCode + "</countryCode>");
            sbSOAPXML.AppendLine("            <countryOfResidence>" + strCountryOfResidence + "</countryOfResidence>");
            sbSOAPXML.AppendLine("            <merchantUserId>" + strMerchantUserID + "</merchantUserId>");
            sbSOAPXML.AppendLine("            <email>" + strCustomerEmail + "</email>");
            sbSOAPXML.AppendLine("            <firstName>" + strCustomerFirstName + "</firstName>");
            sbSOAPXML.AppendLine("            <lastName>" + strCustomerLastName + "</lastName>");
            sbSOAPXML.AppendLine("            <mobile>" + strCustomerCellNo + "</mobile>");
            sbSOAPXML.AppendLine("            <regionalId>" + strCustomerRegionalID + "</regionalId>");
            sbSOAPXML.AppendLine("            <ip>" + strCustomerIPAddress + "</ip>");
            sbSOAPXML.AppendLine("         </Customer>");
            sbSOAPXML.AppendLine("         <Basket>");
            sbSOAPXML.AppendLine("            <amountInCents>" + (fAmount * 100).ToString() + "</amountInCents>");
            sbSOAPXML.AppendLine("            <currencyCode>" + strCurrencyCode + "</currencyCode>");
            sbSOAPXML.AppendLine("            <description>" + strDescription + "</description>");
            sbSOAPXML.AppendLine("         </Basket>");
            sbSOAPXML.AppendLine("         <Creditcard>");
            sbSOAPXML.AppendLine("            <amountInCents>" + (fAmount * 100).ToString() + "</amountInCents>");
            sbSOAPXML.AppendLine("            <cardExpiry>" + dtCCExpiryDate.ToString("MMyyyy") + "</cardExpiry>");
            sbSOAPXML.AppendLine("            <cardNumber>" + strPAN + "</cardNumber>");
            sbSOAPXML.AppendLine("            <cvv>" + strCCV + "</cvv>");
            sbSOAPXML.AppendLine("            <nameOnCard>" + strNameOnCard + "</nameOnCard>");
            sbSOAPXML.AppendLine("         </Creditcard>");
            sbSOAPXML.AppendLine("      </soap:doTransaction>");
            sbSOAPXML.AppendLine("   </soapenv:Body>");
            sbSOAPXML.AppendLine("</soapenv:Envelope>");


            var httpWebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(strPayU_APIURL);
            httpWebRequest.ContentType = "text/xml";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new System.IO.StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(sbSOAPXML.ToString());
                streamWriter.Flush();
            }

            var httpResponse = (System.Net.HttpWebResponse)httpWebRequest.GetResponse();
            string strResult = "";

            using (var streamReader = new System.IO.StreamReader(httpResponse.GetResponseStream()))
            {
                strResult = streamReader.ReadToEnd();
            }

            pResult.TransResponse = new TopUpWalletFromCreditCardResponse.TransactionResponse()
            {
                displayMessage = ExtractFieldFromXML(strResult, "displayMessage"),
                merchantReference = ExtractFieldFromXML(strResult, "merchantReference"),
                resultCode = ExtractFieldFromXML(strResult, "resultCode"),
                resultMessage = ExtractFieldFromXML(strResult, "resultMessage"),
                successful = Convert.ToBoolean(ExtractFieldFromXML(strResult, "successful"))
            };

            #endregion

            #region Add the balance to customer if PayU was success

            if (pResult.TransResponse.successful)
            {
                string strPayURef = strMerchantRef + " - " + ExtractFieldFromXML(strResult, "payUReference");

                #region Check if balance already exists

                bool bDoesExist = Bll.Accounts.Balance.Search(new Bll.Accounts.Balance.SearchFilters() { Amount = new Infrastructure.Range<decimal?>(fAmount), AccountID = pCustomerAccount.Account_id, CurrencyIso = "ZAR", Text = strPayURef }, null).Any();

                if (bDoesExist)
                {
                    pResult.GatewayResponse = new TopUpWalletFromCreditCardResponse.WalletResponse()
                    {
                        resultCode = 2,
                        resultMessage = "Duplicate detected",
                        successful = false
                    };
                    return pResult;
                }


                #endregion

                Bll.Accounts.Balance.Create(pCustomerAccount.Account_id, "ZAR", fAmount, strPayURef, Bll.Accounts.Balance.SOURCE_DEPOSIT, null, false);
                pResult.GatewayResponse = new TopUpWalletFromCreditCardResponse.WalletResponse()
                {
                    resultCode = 0,
                    resultMessage = "Success",
                    successful = true
                };
            }

            #endregion

            return pResult;
        }


        #region XML Classes

        public class TopUpWalletFromCreditCardResponse
        {
            public TransactionResponse TransResponse { get; set; }
            public WalletResponse GatewayResponse { get; set; }
            public class TransactionResponse
            {
                public string displayMessage { get; set; }
                public string merchantReference { get; set; }
                public string resultCode { get; set; }
                public string resultMessage { get; set; }
                public bool successful { get; set; }
            }
            public class WalletResponse
            {
                public int resultCode { get; set; }
                public string resultMessage { get; set; }
                public bool successful { get; set; }
            }
        }

        #endregion

        #region Generic Classes

        static string ExtractFieldFromXML(string strXML, string strFieldNameExact)
        {
            string strFieldNameStartInXMLWillBe = "<" + strFieldNameExact + ">";
            string strFieldNameEndInXMLWillBe = "</" + strFieldNameExact + ">";

            strXML = strXML.Remove(0, strXML.IndexOf(strFieldNameStartInXMLWillBe));
            strXML = strXML.Substring(0, strXML.IndexOf(strFieldNameEndInXMLWillBe));
            strXML = strXML.Replace(strFieldNameStartInXMLWillBe, string.Empty);

            return strXML;
        }

        #endregion

    }
}
