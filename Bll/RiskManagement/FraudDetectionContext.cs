﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure.VO;
using System.Net.Mail;
using System.Net;
using Netpay.Infrastructure.Domains;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.CommonTypes;

namespace Netpay.Bll.RiskManagement
{
	public class FraudDetectionContext
	{
		private BinNumberVO _bin = null;
		private CountryVO _countryByBin = null;
		private CountryVO _countryByBillingAddress = null;
		private CountryVO _countryByIP = null;
		private CurrencyVO _currency = null;
		private Merchant.Merchant _merchant = null;
		private Merchant.RiskSettings _merchantRisk = null;
		private List<RiskItem> _blockedItems = null;
		private List<MerchantRiskRule> _creditcardRiskRules = null;
		private CreditcardWhitelistVO _cardWhitelistEntry = null;

		public Domain Domain { get; set; }
		public IPAddress IP { get; set; }
		public string Email { get; set; }
		public int BinNumber { get; set; }
		public int CurrencyID { get; set; }
		public int BillingAddressCountryID { get; set; }
		public string CardHolderFullName { get; set; }
		public string MerchantNumber { get; set; }
		public decimal Amount { get; set; }
		public int Installments { get; set; }
		public byte[] EncryptedCCNumber { get; set; }
		public byte[] EncryptedCheckAccountNumber { get; set; }
		public CreditType CreditType { get; set; }

		public CreditcardWhitelistVO CardWhitelistEntry
		{
			get 
			{
				if(_cardWhitelistEntry == null) 
				{
					NetpayDataContext dc = new NetpayDataContext(Domain.Sql1ConnectionString);
					_cardWhitelistEntry = (from ccwl in dc.tblCreditCardWhitelists where ccwl.ccwl_Merchant == Merchant.ID && ccwl.ccwl_CardNumber256 == EncryptedCCNumber && !ccwl.ccwl_IsBurnt == false && ((ccwl.ccwl_ExpYear.Value * 13) + ccwl.ccwl_ExpMonth) >= ((DateTime.Now.Year * 13) + DateTime.Now.Month)  select new CreditcardWhitelistVO(ccwl)).SingleOrDefault();
				}
				
				return _cardWhitelistEntry; 
			}
		}

		public List<MerchantRiskRule> CreditcardRiskRules
		{
			get
			{
				if(_creditcardRiskRules == null)
				{
					const short paymentMethodAll = 0;
					const short currencyAll = -1;
					const byte creditTypeAll = 255;
					const int whiteListLevelAll = -1;
					
					NetpayDataContext dc = new NetpayDataContext(Domain.Sql1ConnectionString);
					var expression = from ccr in dc.tblCreditCardRiskManagements where ccr.CCRM_CompanyID == Merchant.ID && ccr.CCRM_IsActive select ccr;
					expression = expression.Where(ccr => ccr.CCRM_PaymentMethod == paymentMethodAll || ccr.CCRM_PaymentMethod == (short)PaymentMethod);
					expression = expression.Where(ccr => ccr.CCRM_Currency == currencyAll || ccr.CCRM_Currency == CurrencyID);
					expression = expression.Where(ccr => ccr.CCRM_CreditType == creditTypeAll || ccr.CCRM_CreditType == (byte)CreditType);
					if(CardWhitelistEntry != null)
						expression = expression.Where(ccr => ccr.CCRM_WhitelistLevel == whiteListLevelAll || ccr.CCRM_WhitelistLevel >= CardWhitelistEntry.LevelID);

					_creditcardRiskRules = expression.Select(ccr => new MerchantRiskRule(Domain.ServiceCredentials, ccr)).ToList();
				}
				
				return _creditcardRiskRules; 
			}
		}

		public List<RiskItem> BlockedItems
		{
			get
			{
				if(_blockedItems == null)
				{
					_blockedItems = Netpay.Bll.RiskManagement.RiskItem.Search(Domain.ServiceCredentials, new RiskItem.SearchFilters() { MerchantID = Merchant.ID }, null);
				}

				return _blockedItems;
			}
		}

		public BinNumberVO Bin
		{
			get
			{
				if(_bin == null)
					_bin = Domain.Cache.GetBin(BinNumber);

				return _bin;
			}
		}

		public CountryVO CountryByBin
		{
			get
			{
				if(_countryByBin == null && Bin != null)
					_countryByBin = Domain.Cache.GetCountry(Bin.BinCountryIsoCode2);

				return _countryByBin;
			}
		}

		public CountryVO CountryByBillingAddress
		{
			get
			{
				if(_countryByBillingAddress == null)
					_countryByBillingAddress = Domain.Cache.GetCountry(BillingAddressCountryID);

				return _countryByBillingAddress;
			}
		}

		public CountryVO CountryByIP
		{
			get
			{
				if(_countryByIP == null)
					_countryByIP = Domain.Cache.GetCountry(IP);

				return _countryByIP;
			}
		}

		public CurrencyVO Currency
		{
			get
			{
				if(_currency == null)
					_currency = Domain.Cache.GetCurrency(CurrencyID);

				return _currency;
			}
		}

		public Merchant.Merchant Merchant
		{
			get
			{
				if(_merchant == null){
					_merchant = Bll.Merchant.Merchant.Load(Domain.ServiceCredentials, MerchantNumber);
					_merchantRisk = Bll.Merchant.RiskSettings.Load(Domain.ServiceCredentials, _merchant.ID);
				}
				return _merchant;
			}
		}
		public Merchant.RiskSettings MerchantRisk
		{
			get { return _merchantRisk; }
		}

		public PaymentMethodEnum PaymentMethod
		{
			get
			{
				return (PaymentMethodEnum)Bin.PaymentMethodID;
			}
		}
	}
}
