﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.RiskManagement
{
	public enum FraudDetectionResultSeverity
	{
		None = 0,
		Low = 1,
		Medium = 2,
		High = 3
	}
}
