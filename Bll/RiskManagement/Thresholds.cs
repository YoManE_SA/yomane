﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.RiskManagement
{
	public static class Thresholds
	{
		public static readonly decimal VisaMaxChbCountRatio = 1.0m;
		public static readonly decimal MastercardMaxChbCountRatio = 1.0m;
		public static readonly decimal VisaMaxChbAmountRatio = 1.0m;
		public static readonly decimal MastercardMaxChbAmountRatio = 1.0m;
	}
}
