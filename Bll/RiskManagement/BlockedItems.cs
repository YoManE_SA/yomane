﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure.VO;
using Netpay.Infrastructure.Domains;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.CommonTypes;

namespace Netpay.Bll.RiskManagement
{
	public static class BlockedItems
	{
		/// <summary>
		/// Gets the merchant / global blocked items such as emails and bins
		/// </summary>
		/// <param name="domainHost"></param>
		/// <param name="merchantID"></param>
		/// <returns></returns>
		public static List<BlockedItemVO> GetBlockedItems(string domainHost, int merchantID)
		{
			return GetBlockedItems(domainHost, merchantID, null);
		}
		
		/// <summary>
		/// Gets the merchant / global blocked items such as emails and bins
		/// </summary>
		/// <param name="domainHost"></param>
		/// <param name="merchantID"></param>
		/// <param name="pagingInfo"></param>
		/// <returns></returns>
		public static List<BlockedItemVO> GetBlockedItems(string domainHost, int merchantID, PagingInfo pagingInfo)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			var expression = from gbi in dc.tblBLCommons where gbi.BL_CompanyID == merchantID || gbi.BL_CompanyID == null orderby gbi.BL_InsertDate descending select new BlockedItemVO(gbi);
			
			// handle paging if not null
			if (pagingInfo != null)
			{
				pagingInfo.TotalItems = expression.Count();
				expression = expression.Skip(pagingInfo.Skip).Take(pagingInfo.Take);
			}

			List<BlockedItemVO> items = expression.ToList();
			return items;
		}

		/// <summary>
		/// Gets the merchant / global blocked items such as emails and bins
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="pagingInfo"></param>
		/// <returns></returns>
		public static List<BlockedItemVO> GetBlockedItems(Guid credentialsToken, PagingInfo pagingInfo)
		{
			return GetBlockedItems(credentialsToken, null, pagingInfo);
		}

		/// <summary>
		/// Gets the merchant / global blocked items such as emails and bins
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="filters"></param>
		/// <param name="pagingInfo"></param>
		/// <returns></returns>
		public static List<BlockedItemVO> GetBlockedItems(Guid credentialsToken, SearchFilters filters, PagingInfo pagingInfo)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited, UserType.NetpayUser, UserType.NetpayAdmin });
			if (user.Type == UserType.MerchantPrimary || user.Type == UserType.MerchantLimited) filters.merchantIDs = new List<int>() { user.ID };
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var expression = from gbi in dc.tblBLCommons select gbi;

			if (filters != null)
			{
				if (filters.merchantIDs != null)
					expression = expression.Where(bi => filters.merchantIDs.Contains(bi.BL_CompanyID.GetValueOrDefault()) || bi.BL_CompanyID == null);
				if (filters.blockedItemSource != null)
					expression = expression.Where(bi => bi.BL_BlockSourceID == (byte)filters.blockedItemSource.Value);
				if (filters.blockedItemType != null)
					expression = expression.Where(bi => bi.BL_Type == (byte)filters.blockedItemType.Value);
				if (filters.blockedItemValue != null)
					expression = expression.Where(bi => bi.BL_Value.Contains(filters.blockedItemValue));
				if (filters.comment != null)
					expression = expression.Where(bi => bi.BL_Comment.Contains(filters.comment));
			}

			// order
			expression = expression.OrderByDescending(gbi => gbi.BL_InsertDate);

			// handle paging if not null
			if (pagingInfo != null)
			{
				pagingInfo.TotalItems = expression.Count();
				expression = expression.Skip(pagingInfo.Skip).Take(pagingInfo.Take);
			}

			return expression.Select(gbi => new BlockedItemVO(gbi)).ToList();
		}

		public static AddBlockedItemResult AddBlockedItem(Guid credentialsToken, BlockedItemType type, string value, string comment, out DateTime? blockDate)
		{
            return AddBlockedItem(credentialsToken, null, type, value, comment, out blockDate);
		}

        public static AddBlockedItemResult AddBlockedItem(Guid credentialsToken, int? merchantID, BlockedItemType type, string value, string comment, out DateTime? blockDate)
		{
            if (value == null || value.Trim() == "")
            {
                blockDate = null;
                return AddBlockedItemResult.NoItemValue;
            }
			
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser, UserType.MerchantPrimary, UserType.MerchantLimited });
			BlockedItemSource blockSource = BlockedItemSource.Unknown;
			if (user.Type == UserType.MerchantPrimary || user.Type == UserType.MerchantLimited)
			{
				blockSource = BlockedItemSource.Merchant;
				merchantID = user.ID;
			}
			else
				blockSource = BlockedItemSource.Netpay;
				
			// search identical item
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var foundEntity = (from bi in dc.tblBLCommons where (bi.BL_CompanyID == merchantID || bi.BL_CompanyID == null) && bi.BL_Type == (byte)type && bi.BL_Value == value select bi).FirstOrDefault();
            if (foundEntity != null)
            {
                blockDate = foundEntity.BL_InsertDate;
                return AddBlockedItemResult.ItemAlreadyBlocked;
            }
			
			// add new
			Netpay.Dal.Netpay.tblBLCommon entity = new Netpay.Dal.Netpay.tblBLCommon();
			entity.BL_BlockLevel = 0;
			entity.BL_Comment = comment;
			entity.BL_CompanyID = merchantID;
			entity.BL_InsertDate = DateTime.Now;
			entity.BL_Type = (byte)type;
			entity.BL_BlockSourceID = (byte)blockSource;
			entity.BL_User = "";
			entity.BL_Value = value;
			dc.tblBLCommons.InsertOnSubmit(entity);
			dc.SubmitChanges();

            blockDate = entity.BL_InsertDate;
            return AddBlockedItemResult.Success; 
		}

        public static void DeleteBlockedItem(Guid credentialsToken, int? merchantID, BlockedItemType type, string value, out DateTime? blockDate)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser, UserType.MerchantPrimary, UserType.MerchantLimited });

			IEnumerable<Netpay.Dal.Netpay.tblBLCommon> entities = null;
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			if (user.Type == UserType.MerchantPrimary || user.Type == UserType.MerchantLimited)
				entities = (from bi in dc.tblBLCommons where bi.BL_CompanyID == user.ID && bi.BL_Type == (byte)type && bi.BL_Value == value && bi.BL_BlockSourceID == (byte)BlockedItemSource.Merchant select bi).ToList();
			else
				entities = (from bi in dc.tblBLCommons where bi.BL_CompanyID == merchantID && bi.BL_Type == (byte)type && bi.BL_Value == value select bi).ToList();

            if (entities == null || entities.Count() == 0)
            {
                blockDate = null;
                return;
            }

            blockDate = entities.First().BL_InsertDate;
            dc.tblBLCommons.DeleteAllOnSubmit(entities);
			dc.SubmitChanges();		
		}

		public static void DeleteBlockedItems(Guid credentialsToken, List<int> blockedItemsIDs)
		{
			DeleteBlockedItems(credentialsToken, null, blockedItemsIDs);
		}

		public static void DeleteBlockedItems(Guid credentialsToken, int? merchantID, List<int> blockedItemsIDs)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser, UserType.MerchantPrimary, UserType.MerchantLimited });

			IEnumerable<Netpay.Dal.Netpay.tblBLCommon> entities = null;
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			if (user.Type == UserType.MerchantPrimary || user.Type == UserType.MerchantLimited)
				entities = (from bi in dc.tblBLCommons where bi.BL_CompanyID == user.ID && blockedItemsIDs.Contains(bi.BL_ID) && bi.BL_BlockSourceID == (byte)BlockedItemSource.Merchant select bi).ToList();
			else
				entities = (from bi in dc.tblBLCommons where bi.BL_CompanyID == merchantID && blockedItemsIDs.Contains(bi.BL_ID) select bi).ToList();

			if (entities == null || entities.Count() == 0)
				return;

			dc.tblBLCommons.DeleteAllOnSubmit(entities);
			dc.SubmitChanges();
		}

		/// <summary>
		/// Returns true if the creditcard has been blocked by the merchant.
		/// </summary>
		public static bool IsBlockedCard(string doaminHost, int merchantID, byte[] encryptedCard)
		{
			NetpayDataContext dc = new NetpayDataContext(DomainsManager.GetDomain(doaminHost).Sql1ConnectionString);
			Netpay.Dal.Netpay.tblFraudCcBlackList entity = (from fbl in dc.tblFraudCcBlackLists
                                          where fbl.fcbl_ccNumber256 == encryptedCard && ((fbl.company_id == merchantID && fbl.fcbl_BlockLevel == (int)BlockedCardLevel.Merchant) || fbl.fcbl_BlockLevel == (int)BlockedCardLevel.SystemPermanent) 
                                          select fbl).SingleOrDefault();

			return (entity != null);
		}

		/// <summary>
		/// Returns merchant blocked credit cards.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="filters"></param>
		/// <param name="pagingInfo"></param>
        /// <param name="sortingInfo"></param>
		/// <returns></returns>
        public static List<BlockedCreditCardVO> GetBlockedCards(Guid credentialsToken, SearchFilters filters, PagingInfo pagingInfo, SortingInfo sortingInfo)
		{
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser, UserType.MerchantLimited, UserType.MerchantPrimary });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            if (user.Type == UserType.MerchantLimited || user.Type == UserType.MerchantPrimary) filters.merchantIDs = user.AllowedMerchantsIDs;
            var expression = from bl in dc.tblFraudCcBlackLists select bl;

            // filters
            if (filters.merchantIDs != null)
                expression = expression.Where(bl => filters.merchantIDs.Contains(bl.company_id) || bl.fcbl_BlockLevel == (byte)BlockedCardLevel.SystemPermanent || (bl.fcbl_BlockLevel == (byte)BlockedCardLevel.SystemTemporary && bl.fcbl_UnblockDate >= DateTime.Now));
            else expression = expression.Where(bl => bl.company_id != 0 || bl.fcbl_BlockLevel == (byte)BlockedCardLevel.SystemPermanent || (bl.fcbl_BlockLevel == (byte)BlockedCardLevel.SystemTemporary && bl.fcbl_UnblockDate >= DateTime.Now));

			if (filters.blockedItemSource != null)
			{
				if (filters.blockedItemSource.Value == BlockedItemSource.Merchant)
					expression = expression.Where(bc => bc.fcbl_BlockLevel == (byte)BlockedCardLevel.Merchant);
				else if (filters.blockedItemSource.Value == BlockedItemSource.Netpay)
					expression = expression.Where(bc => bc.fcbl_BlockLevel == (byte)BlockedCardLevel.SystemPermanent || bc.fcbl_BlockLevel == (byte)BlockedCardLevel.SystemTemporary);
			}
            if (filters.blockedCardLevel != null) 
                expression = expression.Where(bc => bc.fcbl_BlockLevel == (byte)filters.blockedCardLevel.Value);
			if (filters.last4Digits != null)
				expression = expression.Where(fccbl => fccbl.fcbl_ccDisplay.EndsWith(filters.last4Digits.Value.ToString("0000")));
            if (filters.dateFrom != null)
                expression = expression.Where(fccbl => fccbl.fcbl_InsertDate >= filters.dateFrom);
            if (filters.dateTo != null)
                expression = expression.Where(fccbl => fccbl.fcbl_InsertDate <= filters.dateTo);

            // handle sorting if not null
            if (sortingInfo != null)
            {
                if (sortingInfo.Direction == SortDirection.Ascending)
					expression = expression.OrderBy<Netpay.Dal.Netpay.tblFraudCcBlackList>(sortingInfo.MappedEntityProperty);
                else
					expression = expression.OrderByDescending<Netpay.Dal.Netpay.tblFraudCcBlackList>(sortingInfo.MappedEntityProperty);
            }

            // handle paging if not null
			if (pagingInfo != null)
			{
				pagingInfo.TotalItems = expression.Count();
				expression = expression.Skip(pagingInfo.Skip).Take(pagingInfo.Take);
			}

			return expression.Select(bl => new BlockedCreditCardVO(bl)).ToList<BlockedCreditCardVO>();
		}

        /// <summary>
        /// Gets cards blockerd by the merchant
        /// </summary>
        /// <param name="credentialsToken"></param>
        /// <param name="transactionID"></param>
        /// <param name="transactionStatus"></param>
        /// <returns></returns>
		public static BlockedCreditCardVO GetBlockedCard(Guid credentialsToken, int transactionID, TransactionStatus transactionStatus)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary, UserType.NetpayAdmin, UserType.NetpayUser });

            // get transaction
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            var transaction = Transaction.Transaction.GetTransaction(credentialsToken, null, transactionID, transactionStatus);
			if (transaction == null)
				return null;

            // get transaction card
			if (transaction.PaymentData == null || transaction.PaymentData.PaymentMethodType != PaymentMethodType.CreditCard)
				return null;

			Netpay.Dal.Netpay.TransPaymentMethod pmEntity = (from cc in dc.TransPaymentMethods where cc.TransPaymentMethod_id == transaction.PaymentMethodReferenceID select cc).SingleOrDefault();
			if (pmEntity == null)
				return null;

			if (pmEntity.Value1Encrypted == null)
				return null;

			string cardNumber = Encryption.Decrypt(user.Domain.Host, pmEntity.Value1Encrypted.ToArray());
			CreditCard card = new CreditCard(cardNumber);
			byte[] binaryCard = card.ToEncryptedArray(user.Domain.Host);

            // get blocked card
			Netpay.Dal.Netpay.tblFraudCcBlackList blockedCard = (from bc in dc.tblFraudCcBlackLists 
                                               where bc.fcbl_ccNumber256 == binaryCard && ((bc.company_id == user.ID && bc.fcbl_BlockLevel == (int)BlockedCardLevel.Merchant) || bc.fcbl_BlockLevel == (int)BlockedCardLevel.SystemPermanent) 
                                               select bc).FirstOrDefault();
			if (blockedCard == null)
				return null;

			return new BlockedCreditCardVO(blockedCard);
		}

		public static AddBlockedCardResult AddBlockedCard(string domainHost, int merchantID, CreditCard card, int creditCardRuleID, DateTime releaseDate, string replyCode, string comment, out DateTime? blockDate) 
		{
            if (!card.IsValid)
            {
                blockDate = null;
                return AddBlockedCardResult.InvalidCard;
            }

			Domain domain = DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			Netpay.Dal.Netpay.tblFraudCcBlackList foundCard = (from bc in dc.tblFraudCcBlackLists where bc.fcbl_ccNumber256 == card.ToEncryptedArray(domainHost) && bc.fcbl_BlockLevel == (int)BlockedCardLevel.Merchant && bc.company_id == merchantID select bc).SingleOrDefault();
            if (foundCard != null)
            {
                blockDate = foundCard.fcbl_InsertDate;
                return AddBlockedCardResult.CardAlreadyBlocked;
            }
			Netpay.Dal.Netpay.tblFraudCcBlackList entity = new Netpay.Dal.Netpay.tblFraudCcBlackList();
			entity.company_id = merchantID;
			//entity.creditCardType_id = (short)card.GetCardType(domainHost);
            entity.PaymentMethod_id = (short)card.GetPaymentMethod(domainHost);
			entity.fcbl_BlockCount = 0;
			entity.fcbl_BlockLevel = (byte)BlockedCardLevel.Merchant;
			entity.fcbl_ccDisplay = card.ToDisplayableString();
			//entity.fcbl_ccExpMonth = card.ExpirationDate.Month.ToString();
			//entity.fcbl_ccExpYear = card.ExpirationDate.Year.ToString();
			entity.fcbl_ccNumber256 = card.ToEncryptedArray(domainHost);
			entity.fcbl_CCRMID = creditCardRuleID;
			entity.fcbl_comment = comment.ToEncodedHtml();
			entity.fcbl_InsertDate = DateTime.Now;
			entity.fcbl_ReplyCode = replyCode;
			entity.fcbl_UnblockDate = releaseDate;

			dc.tblFraudCcBlackLists.InsertOnSubmit(entity);
			dc.SubmitChanges();
            blockDate = entity.fcbl_InsertDate;
			return AddBlockedCardResult.Success;			
		}

        public static AddBlockedCardResult AddBlockedCard(string domainHost, int merchantID, CreditCard card, string comment, out DateTime? blockDate) 
		{
            return AddBlockedCard(domainHost, merchantID, card, 0, DateTime.Now.AddYears(100), "", comment, out blockDate);
		}

		/// <summary>
		/// Adds a merchant blocked card.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="card"></param>
		/// <param name="comment"></param>
        /// <param name="blockDate"></param>
        public static AddBlockedCardResult AddBlockedCard(Guid credentialsToken, CreditCard card, string comment, out DateTime? blockDate)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary, UserType.NetpayAdmin, UserType.NetpayUser });
            return AddBlockedCard(user.Domain.Host, user.ID, card, comment, out blockDate);
		}

        public static List<AddBlockedCardResult> AddBlockedCards(Guid credentialsToken, List<string> cardNumbers, string comment)
        {
            DateTime? blockDate;
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary });
            List<AddBlockedCardResult> results = new List<AddBlockedCardResult>();
            foreach (string currentCardumber in cardNumbers) 
            {
                CreditCard card = new CreditCard(currentCardumber);
                AddBlockedCardResult result = AddBlockedCard(user.Domain.Host, user.ID, card, comment, out blockDate);
            }

            return results;
        }

		/// <summary>
		/// Adds a merchant blocked card.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="transactionID"></param>
		/// <param name="transactionStatus"></param>
		/// <param name="comment"></param>
        /// <param name="blockDate"></param>
		/// <returns></returns>
        public static AddBlockedCardResult AddBlockedCard(Guid credentialsToken, int transactionID, TransactionStatus transactionStatus, string comment, out DateTime? blockDate)
		{
            blockDate = null;
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary, UserType.NetpayAdmin, UserType.NetpayUser });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var transaction = Transaction.Transaction.GetTransaction(credentialsToken, null, transactionID, transactionStatus);
			if (transaction == null)
				return AddBlockedCardResult.TransactionNotFound;

			if (transaction.PaymentData == null)
				return AddBlockedCardResult.CardNotFound;

			if (transaction.PaymentData.PaymentMethodType != PaymentMethodType.CreditCard)
				return AddBlockedCardResult.PaymentMethodIsNotCreditcard;

			Netpay.Dal.Netpay.tblCreditCard cardEntity = (from cc in dc.tblCreditCards where cc.ID == transaction.PaymentMethodReferenceID select cc).SingleOrDefault();
			if (cardEntity == null)
				return AddBlockedCardResult.CardNotFound;
			if (cardEntity.CCard_number256 == null)
				return AddBlockedCardResult.CardNumberNotFound;

			string cardNumber = Encryption.Decrypt(user.Domain.Host, cardEntity.CCard_number256.ToArray());
			CreditCard card = new CreditCard(cardNumber);
			card.SetExpirationDate(cardEntity.ExpMM, cardEntity.ExpYY);
			if (!card.IsValid)
				return AddBlockedCardResult.InvalidCard;

            return AddBlockedCard(credentialsToken, card, comment, out blockDate);
		}

        /// <summary>
        /// Deletes a merchant blocked card.
        /// </summary>
        /// <param name="credentialsToken"></param>
        /// <param name="card"></param>
        /// <param name="blockDate"></param>
        public static void DeleteBlockedCard(Guid credentialsToken, CreditCard card, out DateTime? blockDate)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser, UserType.MerchantLimited, UserType.MerchantPrimary });
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);

            var expression = (from bl in dc.tblFraudCcBlackLists where bl.fcbl_ccNumber256 == card.ToEncryptedArray(user.Domain.Host) select bl);
            if (user.Type == UserType.MerchantPrimary || user.Type == UserType.MerchantLimited)
                expression = expression.Where(bl => bl.company_id == user.ID && bl.fcbl_BlockLevel == (byte)BlockedCardLevel.Merchant);

			List<Netpay.Dal.Netpay.tblFraudCcBlackList> cards = expression.ToList<Netpay.Dal.Netpay.tblFraudCcBlackList>();
            if (cards.Count > 0) {
                blockDate = cards.First().fcbl_InsertDate;
                dc.tblFraudCcBlackLists.DeleteAllOnSubmit(cards);
                dc.SubmitChanges();
            }
            else blockDate = null;
        }

        /// <summary>
		/// Deletes a merchant blocked card.
		/// </summary>
		/// <param name="credentialsToken"></param>
		/// <param name="blockedCardsIDs"></param>
		public static void DeleteBlockedCards(Guid credentialsToken, List<int> blockedCardsIDs)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser, UserType.MerchantLimited, UserType.MerchantPrimary });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);

            var expression = (from bl in dc.tblFraudCcBlackLists where blockedCardsIDs.Contains(bl.fraudCcBlackList_id) select bl);
            if (user.Type == UserType.MerchantPrimary || user.Type == UserType.MerchantLimited)
                expression = expression.Where(bl =>  bl.company_id == user.ID && bl.fcbl_BlockLevel == (byte)BlockedCardLevel.Merchant);

			List<Netpay.Dal.Netpay.tblFraudCcBlackList> cards = expression.ToList<Netpay.Dal.Netpay.tblFraudCcBlackList>();
			dc.tblFraudCcBlackLists.DeleteAllOnSubmit(cards);
			dc.SubmitChanges();
		}
	}
}
