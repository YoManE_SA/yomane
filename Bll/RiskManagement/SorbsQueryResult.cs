﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.RiskManagement
{
	public enum SorbsQueryResult
	{
		NotFound = 0,
		Http = 2,
		Socks = 3,
		Misc = 4,
		Smtp = 5,
		Spam = 6,
		Web = 7,
		Block = 8,
		Zombie = 9,
		Dul = 10,
		BadConfig = 11,
		NoMail = 12

		/*
		http.dnsbl.sorbs.net    127.0.0.2
		socks.dnsbl.sorbs.net    127.0.0.3
		misc.dnsbl.sorbs.net    127.0.0.4
		smtp.dnsbl.sorbs.net    127.0.0.5
		spam.dnsbl.sorbs.net    127.0.0.6
		web.dnsbl.sorbs.net    127.0.0.7
		block.dnsbl.sorbs.net    127.0.0.8
		zombie.dnsbl.sorbs.net    127.0.0.9
		dul.dnsbl.sorbs.net    127.0.0.10
		badconf.rhsbl.sorbs.net    127.0.0.11
		nomail.rhsbl.sorbs.net    127.0.0.12
		*/
	}
}
