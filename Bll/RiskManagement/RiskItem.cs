﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll.RiskManagement
{
	public class RiskItem : BaseDataObject
	{
		public enum RiskListType { Black = -1, Bronze = 0, Silver = 1, Gold = 2, Platinum = 3 }
		public enum RiskSource { System, Merchant }
		public enum RiskValueType
		{
			//AccountValue2 = -2,
			AccountValue1 = -1,
			//Unknown = 0,
			Email = 1,
			FullName = 2,
			Phone = 3,
			PersonalNumber = 4,
			Bin = 5,
			BinCountry = 6
		}
		public class RiskItemValue { public string Value; public RiskValueType ValueType; }

		public class SearchFilters
		{
			public Range<int?> ID;
			public int? MerchantID;
			public Range<DateTime?> InsertDate;
			public RiskSource? Source;
			public RiskListType? ListType;
			public RiskValueType? ValueType;
			public string Value;
			public string Account1First6;
			public string Account1Last4;
			public string Text;
		}

		public int ID { get; private set; }
		public int? MerchantID { get; private set; } 
		public DateTime InsertDate { get; private set; }
		public RiskSource Source { get; private set; }
		public RiskListType ListType { get; private set; }
		public RiskValueType ValueType { get; private set; }
		public string Value { get; private set; }
		public string Display { get; private set; }
		public string Comment { get; private set; }
		public DateTime? Duration { get; private set; }

		private RiskItem(Guid credentialsToken, Dal.Netpay.tblBLCommon entity) 
			: base(credentialsToken) 
		{
			ID = entity.BL_BlockSourceID;
			MerchantID = entity.BL_CompanyID;
			InsertDate = entity.BL_InsertDate;
			Source = (entity.BL_CompanyID == null ? RiskSource.System : RiskSource.Merchant);
			ListType = RiskListType.Black;
			ValueType = (RiskValueType)entity.BL_Type;
			Value = entity.BL_Value;
			Display = entity.BL_Value;
			Comment = entity.BL_Comment;
		}

		private RiskItem(Guid credentialsToken, Dal.Netpay.tblFraudCcBlackList entity) 
			: base(credentialsToken) 
		{
			ID = entity.fraudCcBlackList_id;
			MerchantID = entity.company_id;
			InsertDate = entity.fcbl_InsertDate;
			Source = (RiskSource)entity.fcbl_BlockLevel;
			ListType = RiskListType.Black;
			ValueType = RiskValueType.AccountValue1;
			Display = entity.fcbl_ccDisplay;
			if (entity.fcbl_ccNumber256 != null) Value = System.Convert.ToBase64String(entity.fcbl_ccNumber256.ToArray());
			Comment = entity.fcbl_comment;
			Duration = entity.fcbl_UnblockDate;
		}

		private RiskItem(Guid credentialsToken, Dal.Netpay.tblCreditCardWhitelist entity)
			: base(credentialsToken) 
		{
			ID = entity.ID;
			MerchantID = entity.ccwl_Merchant;
			InsertDate = entity.ccwl_InsertDate;
			Source = (RiskSource)entity.ccwl_Merchant;
			ListType = (RiskListType) entity.ccwl_Level;
			ValueType = RiskValueType.AccountValue1;
			Display = entity.ccwl_Bin + "..." + entity.ccwl_Last4;
			if (entity.ccwl_CardNumber256 != null) Value = System.Convert.ToBase64String(entity.ccwl_CardNumber256.ToArray());
			Comment = entity.ccwl_Comment;
			Duration = entity.ccwl_BurnDate;
		}

		public static List<RiskItem> Search(Guid credentialsToken, SearchFilters filters, ISortAndPage sortAndPage)
		{
			if (filters == null) return null;
			var context = CreateContext(credentialsToken);
			var result = new List<RiskItem>();
			if (sortAndPage != null) sortAndPage.RowCount = 0;
			if ((filters.ValueType == null || filters.ValueType != RiskValueType.AccountValue1) && (filters.ListType == null || filters.ListType == RiskListType.Black) && (filters.Value != null))
			{
				var exp = (from s in context.DataContext.tblBLCommons select s);
				if (filters.ID.From != null) exp = exp.Where(s => s.BL_ID >= filters.ID.From);
				if (filters.ID.To != null) exp = exp.Where(s => s.BL_ID <= filters.ID.To);
				if (filters.InsertDate.From != null) exp = exp.Where(s => s.BL_InsertDate >= filters.InsertDate.From);
				if (filters.InsertDate.To != null) exp = exp.Where(s => s.BL_InsertDate <= filters.InsertDate.To);
				if (filters.ValueType != null) exp = exp.Where(s => s.BL_Type == (int)filters.ValueType);
				if (filters.Value != null) exp = exp.Where(s => s.BL_Value == filters.Value);
				if (filters.MerchantID != null) exp = exp.Where(s => s.BL_CompanyID == filters.MerchantID);
				result.AddRange(exp.ApplySortAndPage(sortAndPage).Select(s => new RiskItem(credentialsToken, s)));
			}

			bool bHasValue = (filters.Account1First6 != null) || (filters.Account1Last4 != null);
			if ((filters.ValueType == null || filters.ValueType == RiskValueType.AccountValue1) && (filters.ListType == null || filters.ListType == RiskListType.Black) && bHasValue)
			{
				var exp = (from s in context.DataContext.tblFraudCcBlackLists select s);
				if (filters.ID.From != null) exp = exp.Where(s => s.fraudCcBlackList_id >= filters.ID.From);
				if (filters.ID.To != null) exp = exp.Where(s => s.fraudCcBlackList_id <= filters.ID.To);
				if (filters.InsertDate.From != null) exp = exp.Where(s => s.fcbl_InsertDate >= filters.InsertDate.From);
				if (filters.InsertDate.To != null) exp = exp.Where(s => s.fcbl_InsertDate <= filters.InsertDate.To);
				if (filters.MerchantID != null) exp = exp.Where(s => s.company_id == filters.MerchantID);
				//if (filters.Value != null) exp = exp.Where(s => s.fcbl_ccNumber256 == System.Convert.FromBase64String(filters.Value));
				result.AddRange(exp.ApplySortAndPage(sortAndPage).Select(s => new RiskItem(credentialsToken, s)));
			}

			if ((filters.ValueType == null || filters.ValueType == RiskValueType.AccountValue1) && (filters.ListType == null || filters.ListType != RiskListType.Black) && bHasValue)
			{
				var exp = (from s in context.DataContext.tblCreditCardWhitelists select s);
				if (filters.ID.From != null) exp = exp.Where(s => s.ID >= filters.ID.From);
				if (filters.ID.To != null) exp = exp.Where(s => s.ID <= filters.ID.To);
				if (filters.InsertDate.From != null) exp = exp.Where(s => s.ccwl_InsertDate >= filters.InsertDate.From);
				if (filters.InsertDate.To != null) exp = exp.Where(s => s.ccwl_InsertDate <= filters.InsertDate.To);
				//if (filters.Value != null) exp = exp.Where(s => s.ccwl_CardNumber256 == System.Convert.FromBase64String(filters.Value));
				if (filters.Account1First6 != null) exp = exp.Where(s => s.ccwl_Bin == filters.Account1First6.ToNullableInt());
				if (filters.Account1Last4 != null) exp = exp.Where(s => s.ccwl_Last4 == filters.Account1Last4.ToNullableInt());
				if (filters.MerchantID != null) exp = exp.Where(s => s.ccwl_Merchant == filters.MerchantID);
				result.AddRange(exp.ApplySortAndPage(sortAndPage).Select(s => new RiskItem(credentialsToken, s)));
			}
			return result; 
		}

		public static List<RiskItem> Search(Guid credentialsToken, bool? isActive, RiskSource? source, List<RiskItemValue> items)
		{
			var context = CreateContext(credentialsToken);
			var result = new List<RiskItem>();
			var ccItems = items.Where(i => i.ValueType == RiskValueType.AccountValue1).ToList();
			items.RemoveAll(i => ccItems.Contains(i));
			if (items.Count > 0) {
				var exp = (from s in context.DataContext.tblBLCommons select s);
				if (source != null) exp = exp.Where(s => s.BL_CompanyID != null);
				var predicate = PredicateBuilder.False<Dal.Netpay.tblBLCommon>();
				foreach(var item in items)
					predicate = predicate.Or(s => s.BL_Type == (int)item.ValueType && s.BL_Value == item.Value);
				exp = exp.Where(predicate);
				result.AddRange(exp.Select(s => new RiskItem(credentialsToken, s)));
			}
			if (ccItems.Count > 0) 
			{
				var exp = (from s in context.DataContext.tblFraudCcBlackLists select s);
				if (source != null) exp = exp.Where(s => s.company_id != null);
				var predicate = PredicateBuilder.False<Dal.Netpay.tblFraudCcBlackList>();
				foreach (var item in ccItems)
					predicate = predicate.Or(s => s.fcbl_ccNumber256 == System.Convert.FromBase64String(item.Value));
				exp = exp.Where(predicate);
				result.AddRange(exp.Select(s => new RiskItem(credentialsToken, s)));
			}
			if (ccItems.Count > 0)
			{
				var exp = (from s in context.DataContext.tblCreditCardWhitelists select s);
				if (source != null) exp = exp.Where(s => s.ccwl_Merchant != null);
				var predicate = PredicateBuilder.False<Dal.Netpay.tblCreditCardWhitelist>();
				foreach (var item in ccItems)
					predicate = predicate.Or(s => s.ccwl_CardNumber256 == System.Convert.FromBase64String(item.Value));
				exp = exp.Where(predicate);
				result.AddRange(exp.Select(s => new RiskItem(credentialsToken, s)));
			}
			return result;
		}

		public static void Create(Guid credentialsToken, RiskSource source, RiskListType listType, List<RiskItemValue> items, string comment)
		{
			var context = CreateContext(credentialsToken, true);
			var result = Search(credentialsToken, null, source, items);
			items.RemoveAll(i => result.Exists(n=> n.Value == i.Value && n.ValueType == i.ValueType));
			var ccItems = items.Where(i => i.ValueType == RiskValueType.AccountValue1).ToList();
			items.RemoveAll(i => ccItems.Contains(i));
			if (items.Count > 0) {
				if (listType != RiskListType.Black) throw new Exception("white list of items other then AccountValue1 are not supported");
				foreach(var item in items) {
					var newRow = new Dal.Netpay.tblBLCommon();
					newRow.BL_InsertDate = DateTime.Now;
					newRow.BL_BlockLevel = (byte)source;
					newRow.BL_Value = item.Value;
					newRow.BL_Type = (byte) item.ValueType;
					newRow.BL_User = context.User.Name;
					newRow.BL_Comment = comment;
					context.DataContext.tblBLCommons.InsertOnSubmit(newRow);
				}
			}
			if (ccItems.Count > 0) {
				foreach(var item in items) {
					if (listType == RiskListType.Black) {
						var newRow = new Dal.Netpay.tblFraudCcBlackList();
						newRow.fcbl_InsertDate = DateTime.Now;
						newRow.fcbl_ccNumber256 = System.Convert.FromBase64String(item.Value);
						newRow.fcbl_BlockLevel = (byte)source;
						newRow.fcbl_ccDisplay = item.Value;
						newRow.fcbl_comment = comment;
						context.DataContext.tblFraudCcBlackLists.InsertOnSubmit(newRow);
					}else{
						var newRow = new Dal.Netpay.tblCreditCardWhitelist();
						newRow.ccwl_InsertDate = DateTime.Now;
						newRow.ccwl_CardNumber256 = System.Convert.FromBase64String(item.Value);
						newRow.ccwl_Level = (byte)source;
						newRow.ccwl_Bin = item.Value.Substring(0, 6).ToNullableInt();
						newRow.ccwl_Last4 = (short?)item.Value.Substring(item.Value.Length - 4).ToNullableInt();
						newRow.ccwl_Comment = comment;
						context.DataContext.tblCreditCardWhitelists.InsertOnSubmit(newRow);
					}
				}
			}
			context.DataContext.SubmitChanges();
		}

		public static void Delete(Guid credentialsToken, RiskValueType valueType, RiskListType listType, List<int> ids) 
		{
			if (ids == null || ids.Count == 0) return;
			var context = CreateContext(credentialsToken);
			if (valueType == RiskValueType.AccountValue1) {
				if (listType == RiskListType.Black) context.DataContext.tblFraudCcBlackLists.DeleteAllOnSubmit((from n in context.DataContext.tblFraudCcBlackLists where ids.Contains(n.fraudCcBlackList_id) select n));
				else context.DataContext.tblCreditCardWhitelists.DeleteAllOnSubmit((from n in context.DataContext.tblCreditCardWhitelists where ids.Contains(n.ID) select n));
			} else context.DataContext.tblBLCommons.DeleteAllOnSubmit((from n in context.DataContext.tblBLCommons where ids.Contains(n.BL_ID) select n));
			context.DataContext.SubmitChanges();
		}

		public static List<RiskItem> Search(Guid credentialsToken, int transactionId, TransactionStatus status, List<RiskValueType> valueTypes, bool blocksOnly)
		{
			var trans = Transaction.Transaction.GetTransaction(credentialsToken, null, transactionId, status);
			if (trans == null) throw new Exception("Transaction not found");
			string encData1 = null, encData12 = null;
			if (trans.PaymentData != null) trans.PaymentData.GetDecryptedData(out encData1, out encData12);
			var searchItems = new List<RiskItemValue>();
			foreach (var vt in valueTypes)
			{
				switch (vt) {
				case RiskValueType.AccountValue1:
					if (string.IsNullOrEmpty(encData1)) searchItems.Add(new RiskItemValue() { ValueType= RiskValueType.AccountValue1, Value = encData1 }); break;
				case RiskValueType.Bin:
					if (trans.PaymentData != null) searchItems.Add(new RiskItemValue() { ValueType = RiskValueType.Bin, Value = trans.PaymentData.First6Digits }); break;
				case RiskValueType.BinCountry:
					if (trans.PaymentData != null) searchItems.Add(new RiskItemValue() { ValueType = RiskValueType.BinCountry, Value = trans.PaymentData.IssuerCountryIsoCode }); break;
				case RiskValueType.Email:
					if (trans.PayerData != null) searchItems.Add(new RiskItemValue() { ValueType = RiskValueType.Email, Value = trans.PayerData.EmailAddress }); break;
				case RiskValueType.Phone:
					if (trans.PayerData != null) searchItems.Add(new RiskItemValue() { ValueType = RiskValueType.Phone, Value = trans.PayerData.PhoneNumber }); break;
				case RiskValueType.PersonalNumber:
					if (trans.PayerData != null) searchItems.Add(new RiskItemValue() { ValueType = RiskValueType.PersonalNumber, Value = trans.PayerData.PersonalNumber }); break;
				}
			}
			return Search(credentialsToken, true, RiskSource.Merchant, searchItems);
		}

	}
}
