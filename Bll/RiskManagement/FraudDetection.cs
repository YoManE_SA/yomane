﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Domains;
using System.Net;
using Netpay.Infrastructure.VO;
using System.Text.RegularExpressions;
using System.Data.Linq;
using System.Timers;
using System.Net.Sockets;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.Bll.RiskManagement
{
	public static class FraudDetection
	{
		private static object _syncRoot = new object();
		private static List<FraudDetectionRule> _rules = null;
		private static Dictionary<string, HashSet<TransactionCache>> _recentTransactions = new Dictionary<string, HashSet<TransactionCache>>();
		private static Timer _recentTransactionsVulture = new Timer();
		private static TimeSpan _maxTransactionCacheTime = new TimeSpan(0, 5, 0); 

		internal static void Init()
		{
			// setup vulture timer
			_recentTransactionsVulture.Elapsed += new ElapsedEventHandler(RecentTransactionsVultureTimerElapsed);
			_recentTransactionsVulture.AutoReset = true;
			_recentTransactionsVulture.Interval = 10000;
			_recentTransactionsVulture.Start();
		}

		private static void RecentTransactionsVultureTimerElapsed(object sender, ElapsedEventArgs e)
		{
			if (_recentTransactions.Count == 0)
				return;

			lock (_syncRoot)
			{
				// remove old transactions
				for (int groupEntryIndex = _recentTransactions.Count - 1; groupEntryIndex >= 0; groupEntryIndex--)
				{
					KeyValuePair<string, HashSet<TransactionCache>> currentGroupEntry = _recentTransactions.ElementAt(groupEntryIndex);
					HashSet<TransactionCache> currentGroup = currentGroupEntry.Value;
					
					for (int groupIndex = currentGroup.Count - 1; groupIndex >= 0; groupIndex--)
					{
						TransactionCache currentTransactionCache = currentGroup.ElementAt(groupIndex);
						TimeSpan currentCachedTime = DateTime.Now - currentTransactionCache.InsertDate;
						if (currentCachedTime >= _maxTransactionCacheTime)
							currentGroup.Remove(currentTransactionCache);
					}

					if (currentGroup.Count == 0)
						_recentTransactions.Remove(currentGroupEntry.Key);
				}	
			}
		}

		public static List<FraudDetectionRule> Rules
		{
			get 
			{
				if (_rules == null)
				{
					lock (_syncRoot)
					{
						if (_rules == null)
						{
							_rules = new List<FraudDetectionRule>();

							_rules.Add(new FraudDetectionRule(FraudDetectionRuleGroup.PreProcess, info =>
							{
								// merchant
								if (info.Merchant == null)
									return new FraudDetectionResult(FraudDetectionResultSeverity.Medium, "merchant not found");

								return null;
							}));
							
							_rules.Add(new FraudDetectionRule(FraudDetectionRuleGroup.PreProcess, info =>
							{
								// card holder name non letters
								if (RegularExpressions.notLetter.IsMatch(info.CardHolderFullName))
									return new FraudDetectionResult(FraudDetectionResultSeverity.Medium, "card holder name contains non letter characters");

								return null;
							}));

							_rules.Add(new FraudDetectionRule(FraudDetectionRuleGroup.PreProcess, info =>
							{
								// card holder name repetitions
								if (RegularExpressions.repetition.IsMatch(info.CardHolderFullName))
									return new FraudDetectionResult(FraudDetectionResultSeverity.Medium, "card holder name contains repetitions");

								return null;
							}));

							_rules.Add(new FraudDetectionRule(FraudDetectionRuleGroup.PreProcess, info =>
							{
								// ip country
								if (info.CountryByIP == null)
									return new FraudDetectionResult(FraudDetectionResultSeverity.Medium, "could not resolve country by ip");

								return null;
							}));

							_rules.Add(new FraudDetectionRule(FraudDetectionRuleGroup.PreProcess, info =>
							{
								// ip country
								if (info.CountryByBin == null)
									return new FraudDetectionResult(FraudDetectionResultSeverity.Medium, "could not resolve country by bin");

								return null;
							}));

							_rules.Add(new FraudDetectionRule(FraudDetectionRuleGroup.PreProcess, info =>
							{
								// billing address country
								if (info.CountryByBillingAddress == null)
									return new FraudDetectionResult(FraudDetectionResultSeverity.Medium, "could not resolve country by billing address");

								return null;
							}));

							_rules.Add(new FraudDetectionRule(FraudDetectionRuleGroup.PreProcess, info =>
							{
								// validate credit card
								string decryptedCard = Encryption.Decrypt(info.Domain.Host, info.EncryptedCCNumber);

								if (!CreditCard.Validate(decryptedCard))
									return new FraudDetectionResult(FraudDetectionResultSeverity.Medium, "card is invalid");

								return null;
							}));
							
							_rules.Add(new FraudDetectionRule(FraudDetectionRuleGroup.PreProcess, info => 
							{
								// bin country to ip country
								if (info.CountryByIP == null)
									return new FraudDetectionResult(FraudDetectionResultSeverity.Medium, "could not compare bin country to ip country cause no country was resolved by ip");

								if (info.CountryByBin == null)
									return new FraudDetectionResult(FraudDetectionResultSeverity.Medium, "could not compare bin country to ip country cause the bin country was not found");

								if (info.CountryByBin.ID != info.CountryByIP.ID)
									return new FraudDetectionResult(FraudDetectionResultSeverity.Medium, string.Format("bin number country ({0}) is not the same as ip country ({1})", info.CountryByBin.Name, info.CountryByIP.Name));
								
								return null;
							}));

							_rules.Add(new FraudDetectionRule(FraudDetectionRuleGroup.PreProcess, info =>
							{
								// input country to ip country
								if (info.CountryByBillingAddress == null)
									return new FraudDetectionResult(FraudDetectionResultSeverity.Medium, "could compare input country to ip country cause the input country was not found");

								if (info.CountryByIP == null)
									return new FraudDetectionResult(FraudDetectionResultSeverity.Medium, "could not compare input country to ip country cause no country was resolved by ip");

								if (info.CountryByBillingAddress.ID != info.CountryByIP.ID)
									return new FraudDetectionResult(FraudDetectionResultSeverity.Medium, string.Format("input country ({0}) is not the same as ip country ({1})", info.CountryByBillingAddress.Name, info.CountryByIP.Name));

								return null;
							}));

							_rules.Add(new FraudDetectionRule(FraudDetectionRuleGroup.PreProcess, info =>
							{
								// country black list
								if (info.Merchant == null || info.CountryByIP == null)
									return new FraudDetectionResult(FraudDetectionResultSeverity.Medium, "could not check country black list, merchant not found or country by ip not found");

								if (info.MerchantRisk.CountryBlacklist != null && info.MerchantRisk.CountryBlacklist.Count > 0)
									if (info.MerchantRisk.CountryBlacklist.Contains(info.CountryByIP.IsoCode2))
										return new FraudDetectionResult(FraudDetectionResultSeverity.Medium, string.Format("ip country ({0}) is in the merchant black list", info.CountryByIP.Name));

								return null;
							}));

							_rules.Add(new FraudDetectionRule(FraudDetectionRuleGroup.PreProcess, info =>
							{
								// same transaction in the last 5 minutes
								HashSet<TransactionCache> foundGroup = null;
								lock (_syncRoot)
								{
									if (_recentTransactions.ContainsKey(info.Email))
									{
										foundGroup = _recentTransactions[info.Email];
										TransactionCache foundCachedTransaction = (from t in foundGroup where t.MerchantID.Value == info.Merchant.ID && t.InsertDate >= DateTime.Now.AddMinutes(-5) && t.IP == info.IP.ToString() && t.Amount == info.Amount && t.CurrencyID == info.CurrencyID && t.Installments == info.Installments && t.EncryptedCCNumber == info.EncryptedCCNumber select t).SingleOrDefault();
										if (foundCachedTransaction != null)
										{	
											foundCachedTransaction.InsertDate = DateTime.Now;	
											return new FraudDetectionResult(FraudDetectionResultSeverity.Medium, "Same transaction in the last 5 minutes.");								
										}									
									}
								}

								TransactionCache cache = new TransactionCache();
								cache.Amount = info.Amount;
								cache.CurrencyID = info.CurrencyID;
								cache.EncryptedCCNumber = info.EncryptedCCNumber;
								cache.InsertDate = DateTime.Now;
								cache.Installments = info.Installments;
								cache.IP = info.IP.ToString();
								cache.MerchantID = info.Merchant.ID;
								
								if (foundGroup == null)
								{
									foundGroup = new HashSet<TransactionCache>();
									_recentTransactions.Add(info.Email, foundGroup);
								}
									
								foundGroup.Add(cache);
							
								return null;
							}));

							_rules.Add(new FraudDetectionRule(FraudDetectionRuleGroup.PreProcess, info =>
							{
								// merchant allowed amounts
								if (info.MerchantRisk.AllowedAmounts != null && info.MerchantRisk.AllowedAmounts.Count > 0 && !info.MerchantRisk.AllowedAmounts.Contains(info.Amount))
									return new FraudDetectionResult(FraudDetectionResultSeverity.Medium, "Amount is not in the merchant allowed amounts");

								return null;
							}));

							_rules.Add(new FraudDetectionRule(FraudDetectionRuleGroup.PreProcess, info =>
							{
								// blocked items
								
								// email
								string foundItem = (from bi in info.BlockedItems where bi.ValueType == RiskItem.RiskValueType.Email && bi.Value.Trim().ToLower() == info.Email.Trim().ToLower() select bi.Value).SingleOrDefault();
								if (foundItem != null)
									return new FraudDetectionResult(FraudDetectionResultSeverity.Medium, "Email is blocked");

								// cardholder name
								foundItem = (from bi in info.BlockedItems where bi.ValueType == RiskItem.RiskValueType.FullName && bi.Value.Trim().ToLower() == info.CardHolderFullName.Trim().ToLower() select bi.Value).SingleOrDefault();
								if (foundItem != null)
									return new FraudDetectionResult(FraudDetectionResultSeverity.Medium, "Cardholder name is blocked");								
								
								// bin
								foundItem = (from bi in info.BlockedItems where bi.ValueType == RiskItem.RiskValueType.Bin && bi.Value.Trim() == info.BinNumber.ToString() select bi.Value).SingleOrDefault();
								if (foundItem != null)
									return new FraudDetectionResult(FraudDetectionResultSeverity.Medium, "Bin is blocked");

								// bin country 
								foundItem = (from bi in info.BlockedItems where bi.ValueType == RiskItem.RiskValueType.BinCountry && bi.Value.Trim().ToLower() == info.CountryByBin.IsoCode2.ToLower() select bi.Value).SingleOrDefault();
								if (foundItem != null)
									return new FraudDetectionResult(FraudDetectionResultSeverity.Medium, "Bin country is blocked");									
								
								//TODO: Phone, PersonalNumber

								foundItem = (from bi in info.BlockedItems where bi.ValueType == RiskItem.RiskValueType.AccountValue1 && bi.Value == System.Convert.ToBase64String(info.EncryptedCCNumber) select bi.Value).SingleOrDefault();
								if (foundItem != null)
									return new FraudDetectionResult(FraudDetectionResultSeverity.Medium, "The card has been blocked by the merchant");

								return null;
							}));

							_rules.Add(new FraudDetectionRule(FraudDetectionRuleGroup.PreProcess, info =>
							{
								// merchant daily limits
								// query is costly: sum merchants todays captured amount, converted to a single currency
								// cannot cache this one, need to check current daily sum + current transaction amount >= daily amount 
								// possible solution: real time amount counter

								return null;
							}));

							_rules.Add(new FraudDetectionRule(FraudDetectionRuleGroup.PreProcess, info =>
							{
								// Sorbs detection
								// http://www.sorbs.net/
								SorbsQueryResult result = QuerySorbs(info.IP);
								if (result != SorbsQueryResult.NotFound)
									return new FraudDetectionResult(FraudDetectionResultSeverity.Medium, "The request ip is listed in SORBS as " + result.ToString());								

								return null;
							}));

							_rules.Add(new FraudDetectionRule(FraudDetectionRuleGroup.PreProcess, info =>
							{
								// pre process cc risk rules
								const int passedReplySource = -1;					
								const short paymentMethodAll = 0;
								const byte creditTypeAll = 255;
								var rules = info.CreditcardRiskRules.Where(ccr => ccr.ReplySource == passedReplySource);

								foreach(MerchantRiskRule currentRule in rules) 
								{
									// validate rule
									if (currentRule.MaxAmount == 0 && currentRule.MaxCount == 0)
										continue;
									
									if (currentRule.TimeframeHours == 0)
									{
										// transaction amount limit
										if (info.Amount > currentRule.MaxAmount) 
										{
											FraudDetectionResult result = new FraudDetectionResult();
											result.Description = "Transaction exceeded maximum amount.";
											result.ReplyCode = currentRule.MaxAmountReplyCode;
											result.Severity = FraudDetectionResultSeverity.High;
											if (currentRule.BlockHours > 0) 
											{
												result.CreateBlock = true;
												result.BlockDuration = new TimeSpan(0, currentRule.BlockHours, 0, 0);
											}

											return result;
										}
									}
									else 
									{
										// timeframe mode
										NetpayDataContext dc = new NetpayDataContext(info.Domain.Sql1ConnectionString);
										DateTime timeframeStart = DateTime.Now.AddHours(currentRule.TimeframeHours * -1);
										var expression = from ctt in dc.ProcessApproveds where ctt.Merchant_id == info.Merchant.ID && ctt.TransCurrency == info.CurrencyID && ctt.TransDate >= timeframeStart select ctt;
										if (currentRule.PaymentMethod != null && currentRule.PaymentMethod != paymentMethodAll)
											expression = expression.Where(ctt => ctt.PaymentMethod_id == currentRule.PaymentMethod);
										if (currentRule.PaymentMethod >= (byte)PaymentMethodEnum.EC_MIN && currentRule.PaymentMethod <= (byte)PaymentMethodEnum.EC_MAX)
											;//expression = expression.Where(ctt => ctt.CheckingAccountNumber == info.EncryptedCheckAccountNumber);
										else
											;//expression = expression.Where(ctt => ctt.CreditCardNumber == info.EncryptedCCNumber);
										if (currentRule.CreditType != creditTypeAll)
											expression = expression.Where(ctt => ctt.TransCreditType_id == currentRule.CreditType);
										var stats = expression.GroupBy(ctt => ctt.Merchant_id).Select(g => new { count = g.Count(), sum = g.Sum(ctt => ctt.TransAmount) }).Single();

										// amount limit in timeframe
										if (currentRule.MaxAmount > 0) 
										{
											if (info.Amount + stats.sum > currentRule.MaxAmount) 
											{
												FraudDetectionResult result = new FraudDetectionResult();
												result.Description = "Transaction exceeded maximum amount.";
												result.ReplyCode = currentRule.MaxAmountReplyCode;
												result.Severity = FraudDetectionResultSeverity.High;
												if (currentRule.BlockHours > 0)
												{
													result.CreateBlock = true;
													result.BlockDuration = new TimeSpan(0, currentRule.BlockHours, 0, 0);
												}

												return result;
											}
										}

										// count limit in timeframe
										if (currentRule.MaxCount > 0) 
										{
											if (stats.count >= currentRule.MaxCount) 
											{
												FraudDetectionResult result = new FraudDetectionResult();
												result.Description = "Transactions exceeded maximum count.";
												result.ReplyCode = currentRule.MaxCountReplyCode;
												result.Severity = FraudDetectionResultSeverity.High;
												if (currentRule.BlockHours > 0)
												{
													result.CreateBlock = true;
													result.BlockDuration = new TimeSpan(0, currentRule.BlockHours, 0, 0);
												}

												return result;
											}
										}
									}
								}

								return null;
							}));

							_rules.Add(new FraudDetectionRule(FraudDetectionRuleGroup.PostProcess, info =>
							{
								// post process fail cc risk rules
								const int passedReplySource = -1;
								const byte creditTypeAll = 255;
								const short paymentMethodAll = 0;
								var rules = info.CreditcardRiskRules.Where(ccr => ccr.ReplySource != passedReplySource);

								foreach(MerchantRiskRule currentRule in rules) 
								{
									// validate rule
									if (currentRule.BlockHours == 0)
										continue;
									if (currentRule.MaxAmount == 0 && currentRule.MaxCount == 0)
										continue;
									
									if (currentRule.TimeframeHours == 0)
									{
										// transaction amount limit
										if (info.Amount > currentRule.MaxAmount) 
										{
											FraudDetectionResult result = new FraudDetectionResult();
											result.Description = "Transaction exceeded maximum amount.";
											result.ReplyCode = currentRule.MaxAmountReplyCode;
											result.Severity = FraudDetectionResultSeverity.High;
											if (currentRule.BlockHours > 0) 
											{
												result.CreateBlock = true;
												result.BlockDuration = new TimeSpan(0, currentRule.BlockHours, 0, 0);
											}

											return result;
										}
									}
									else 
									{
										// timeframe mode
										NetpayDataContext dc = new NetpayDataContext(info.Domain.Sql1ConnectionString);
										DateTime timeframeStart = DateTime.Now.AddHours(currentRule.TimeframeHours * -1);
										var expression = from ctt in dc.tblCompanyTransFails where ctt.CompanyID == info.Merchant.ID && ctt.Currency == info.CurrencyID && ctt.InsertDate >= timeframeStart select ctt;
										if (currentRule.PaymentMethod != null && currentRule.PaymentMethod != paymentMethodAll)
											expression = expression.Where(ctt => ctt.PaymentMethod == currentRule.PaymentMethod);
										expression = expression.Where(ctt => ctt.tblCreditCard.CCard_number256 == info.EncryptedCCNumber);
										if (currentRule.CreditType != creditTypeAll)
											expression = expression.Where(ctt => ctt.CreditType == currentRule.CreditType);
										var stats = expression.GroupBy(ctt => ctt.CompanyID).Select(g => new { count = g.Count(), sum = g.Sum(ctt => ctt.Amount) }).SingleOrDefault();

										// amount limit in timeframe
                                        if (stats != null && currentRule.MaxAmount > 0) 
										{
											if (info.Amount + stats.sum > currentRule.MaxAmount) 
											{
												FraudDetectionResult result = new FraudDetectionResult();
												result.Description = "Transaction exceeded maximum amount.";
												result.ReplyCode = currentRule.MaxAmountReplyCode;
												result.Severity = FraudDetectionResultSeverity.High;
												if (currentRule.BlockHours > 0)
												{
													result.CreateBlock = true;
													result.BlockDuration = new TimeSpan(0, currentRule.BlockHours, 0, 0);
												}

												return result;
											}
										}

										// count limit in timeframe
                                        if (stats != null && currentRule.MaxCount > 0) 
										{
											if (stats.count >= currentRule.MaxCount) 
											{
												FraudDetectionResult result = new FraudDetectionResult();
												result.Description = "Transactions exceeded maximum count.";
												result.ReplyCode = currentRule.MaxCountReplyCode;
												result.Severity = FraudDetectionResultSeverity.High;
												if (currentRule.BlockHours > 0)
												{
													result.CreateBlock = true;
													result.BlockDuration = new TimeSpan(0, currentRule.BlockHours, 0, 0);
												}

												return result;
											}
										}
									}
								}

								return null;
							}));
						}
					}
				}

				return _rules;
			}
		}

		/// <summary>
		/// Returns true if the server is listed as a zombie (open proxy)
		/// </summary>
		/// <param name="ip"></param>
		/// <returns></returns>
		public static SorbsQueryResult QuerySorbs(IPAddress ip)
		{
			IPAddress reverseIP = new IPAddress(ip.GetAddressBytes().Reverse().ToArray());
			string query = reverseIP + ".dnsbl.sorbs.net";

			try
			{
				IPHostEntry ipEntry = Dns.GetHostEntry(query);
				return (SorbsQueryResult)ipEntry.AddressList.First().GetAddressBytes()[3];
			}
			catch (SocketException dnsEx)
			{
				if (dnsEx.ErrorCode == 11001)
					return SorbsQueryResult.NotFound;
				else 
					throw dnsEx;
			}
		}

		public static bool Detect(string domainHost, FraudDetectionRuleGroup group, string merchantNumber, int creditType, string ip, string email, int bin, decimal amount, int currencyID, int billingAddressCountryID, string cardHolderFullName, int installments, byte[] encryptedCCNumber, out List<FraudDetectionResult> results)
		{
			// prepare detection info
			FraudDetectionContext info = new FraudDetectionContext();
			info.Domain = DomainsManager.GetDomain(domainHost);
			info.MerchantNumber = merchantNumber;
			info.CreditType = (CreditType)creditType;
			info.CurrencyID = currencyID;
			info.Email = email;
			info.BinNumber = bin;
			info.BillingAddressCountryID = billingAddressCountryID;
			info.CardHolderFullName = cardHolderFullName;
			info.Amount = amount;
			info.Installments = installments;
			info.EncryptedCCNumber = encryptedCCNumber;
			IPAddress parsedIP;
			if (IPAddress.TryParse(ip, out parsedIP))
				info.IP = parsedIP;	 

			// detect
			results = new List<FraudDetectionResult>();
			foreach(FraudDetectionRule currentRule in Rules.Where(fdr => fdr.Group == group))
			{
				FraudDetectionResult currentResult = currentRule.Detect(info);
				if (currentResult != null)
					results.Add(currentResult);		
			}

			return results.Count > 0 && results.Where(r => r.Severity == FraudDetectionResultSeverity.High).Count() > 0;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="domainHost"></param>
		/// <param name="group"></param>
		/// <param name="merchantNumber"></param>
		/// <param name="creditType"></param>
		/// <param name="ip"></param>
		/// <param name="email"></param>
		/// <param name="bin"></param>
		/// <param name="amount"></param>
		/// <param name="currencyID"></param>
		/// <param name="billingAddressCountryID"></param>
		/// <param name="cardHolderFullName"></param>
		/// <param name="installments"></param>
		/// <param name="encryptedCCNumber"></param>
		/// <param name="result"></param>
		/// <returns>false when a rule failed with high risk</returns>
		public static bool Detect(string domainHost, FraudDetectionRuleGroup group, string merchantNumber, int creditType, string ip, string email, int bin, decimal amount, int currencyID, int billingAddressCountryID, string cardHolderFullName, int installments, byte[] encryptedCCNumber, out FraudDetectionResult result)
		{
			// prepare detection info
			FraudDetectionContext context = new FraudDetectionContext();
			context.Domain = DomainsManager.GetDomain(domainHost);
			context.MerchantNumber = merchantNumber;
			context.CreditType = (CreditType)creditType;
			context.CurrencyID = currencyID;
			context.Email = email;
			context.BinNumber = bin;
			context.BillingAddressCountryID = billingAddressCountryID;
			context.CardHolderFullName = cardHolderFullName;
			context.Amount = amount;
			context.Installments = installments;
			context.EncryptedCCNumber = encryptedCCNumber;
			IPAddress parsedIP;
			if (IPAddress.TryParse(ip, out parsedIP))
				context.IP = parsedIP;

			// detect
			result = null;
			foreach (FraudDetectionRule currentRule in Rules.Where(fdr => fdr.Group == group))
			{
				FraudDetectionResult currentResult = currentRule.Detect(context);
				if (currentResult != null && currentResult.Severity == FraudDetectionResultSeverity.High)
				{
					result = currentResult;
					return false;
				}
			}

			return true;
		}

		public static bool Detect(string domainHost, FraudDetectionRuleGroup group, string merchantNumber, int creditType, string ip, string email, int bin, decimal amount, string currencyIso, string billingAddressCountryIso, string cardHolderFullName, int installments, byte[] encryptedCCNumber, out List<FraudDetectionResult> results)
		{
			Domain domain = DomainsManager.GetDomain(domainHost);
			CurrencyVO currency = domain.Cache.GetCurrency(currencyIso);
			CountryVO country = domain.Cache.GetCountry(billingAddressCountryIso);
			
			return Detect(domainHost, group, merchantNumber, creditType, ip, email, bin, amount, currency.ID, country.ID, cardHolderFullName, installments, encryptedCCNumber, out results);
		}

		public static bool Detect(string domainHost, FraudDetectionRuleGroup group, string merchantNumber, int creditType, string ip, string email, int bin, decimal amount, string currencyIso, string billingAddressCountryIso, string cardHolderFullName, int installments, byte[] encryptedCCNumber, out FraudDetectionResult result)
		{
			Domain domain = DomainsManager.GetDomain(domainHost);
			CurrencyVO currency = domain.Cache.GetCurrency(currencyIso);
			CountryVO country = domain.Cache.GetCountry(billingAddressCountryIso);

			return Detect(domainHost, group, merchantNumber, creditType, ip, email, bin, amount, currency.ID, country.ID, cardHolderFullName, installments, encryptedCCNumber, out result);
		}
	}
}
