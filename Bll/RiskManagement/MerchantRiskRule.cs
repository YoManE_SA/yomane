﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.RiskManagement
{
	public class MerchantRiskRule : BaseDataObject
	{
		public static int[] RuleAnswers = new int[] { 505, 509, 525, 547, 561, 562, 563, 580, 581, 582, 583, 584, 585, 586, 587, 588, 589, 593, 594, 597, 598 };
		public enum WhiteListLevel { Bronze = 0, Silver = 1, Gold = 2, Platinum = 3 }
		public enum ReplySources { Passed = -1, All_Declines = 999, Gateway_Decline = 2, Risk_Decline = 1, Issuer_Decline = 0 }

		private Dal.Netpay.tblCreditCardRiskManagement _entity;
		public int ID { get { return _entity.CCRM_ID; } }
		public DateTime InsertDate { get { return _entity.CCRM_InsDate; } }
		public int? MerchantID { get { return _entity.CCRM_CompanyID; } }
		public bool IsActive { get { return _entity.CCRM_IsActive; } set { _entity.CCRM_IsActive = value; } }
		public bool ApplyVT { get { return _entity.CCRM_ApplyVT; } set { _entity.CCRM_ApplyVT = value; } }
		public byte CreditType { get { return _entity.CCRM_CreditType; } set { _entity.CCRM_CreditType = value; } }
		public int CurrencyID { get { return _entity.CCRM_Currency; } set { _entity.CCRM_Currency = (short)value; } }
		public decimal MaxAmount { get { return _entity.CCRM_Amount; } set { _entity.CCRM_Amount = value; } }
		public int MaxCount { get { return _entity.CCRM_MaxTrans; } set { _entity.CCRM_MaxTrans = value; } }
		public int ReplySource { get { return _entity.CCRM_ReplySource; } set { _entity.CCRM_ReplySource = value; } }
		public int TimeframeDays { get { return _entity.ccrm_days.GetValueOrDefault(); } set { _entity.ccrm_days = value; } }
		public int TimeframeHours { get { return _entity.CCRM_Hours; } set { _entity.CCRM_Hours = value; } }
		public int BlockDays { get { return _entity.ccrm_blockdays.GetValueOrDefault(); } set { _entity.ccrm_blockdays = value; } }
		public int BlockHours { get { return _entity.CCRM_BlockHours; } set { _entity.CCRM_BlockHours = value; } }
		public string MaxAmountReplyCode { get { return _entity.CCRM_ReplyAmount; } set { _entity.CCRM_ReplyAmount = value; } }
		public string MaxCountReplyCode { get { return _entity.CCRM_ReplyMaxTrans; } set { _entity.CCRM_ReplyMaxTrans = value; } }
		public int? WhitelistLevel { get { return _entity.CCRM_WhitelistLevel; } set { _entity.CCRM_WhitelistLevel = value; } }
		public short? PaymentMethod { get { return _entity.CCRM_PaymentMethod; } set { _entity.CCRM_PaymentMethod = value; } }

		internal MerchantRiskRule(Guid credentialsToken, Dal.Netpay.tblCreditCardRiskManagement entity)
			: base(credentialsToken)
		{
			_entity = entity;
		}

		public MerchantRiskRule(Guid credentialsToken, int merchantId)
			: base(credentialsToken)
		{
			_entity = new Dal.Netpay.tblCreditCardRiskManagement();
			_entity.CCRM_InsDate = DateTime.Now;
			_entity.CCRM_CompanyID = merchantId;
		}

		public void Delete()
		{
			if (_entity.CCRM_ID == 0) return;
			EnsureAttached();
			//User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
			DataContext.tblCreditCardRiskManagements.DeleteOnSubmit(_entity);
			DataContext.SubmitChanges();
		}

		private void EnsureAttached()
		{
			if (DataContext.tblCreditCardRiskManagements.GetOriginalEntityState(_entity) == null)
				DataContext.tblCreditCardRiskManagements.Attach(_entity);
		}

		public void Save()
		{
			if (_entity.CCRM_ID != 0)
			{
				EnsureAttached();
				DataContext.Refresh(System.Data.Linq.RefreshMode.KeepCurrentValues, _entity);
			} else {
				DataContext.tblCreditCardRiskManagements.InsertOnSubmit(_entity);
			}
			DataContext.SubmitChanges();
		}

		public static MerchantRiskRule Load(Guid credentialsToken, int id)
		{
			var context = CreateContext(credentialsToken);
			context.IsUserOfType(new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser });
			return (from b in context.DataContext.tblCreditCardRiskManagements where b.CCRM_ID == id select new MerchantRiskRule(credentialsToken, b)).SingleOrDefault();
		}

		public static List<MerchantRiskRule> Search(Guid credentialsToken, int merchantId)
		{
			var context = CreateContext(credentialsToken);
			context.IsUserOfType(new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser });
			return (from b in context.DataContext.tblCreditCardRiskManagements where b.CCRM_CompanyID == merchantId select new MerchantRiskRule(credentialsToken, b)).ToList();
		}
	}
}