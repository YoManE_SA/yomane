﻿using System;

namespace Netpay.Bll.RiskManagement
{
	public class FraudDetectionResult
	{
		public FraudDetectionResult ()
		{
		}

		public FraudDetectionResult(FraudDetectionResultSeverity severity)
		{
			Severity = severity;
		}

		public FraudDetectionResult(FraudDetectionResultSeverity severity, string description)
		{
			Severity = severity;
			Description = description;
		}

		public FraudDetectionResultSeverity Severity { get; set; }
		public string Description { get; set; }
		public bool CreateBlock { get; set; }
		public int RuleID { get; set; }
		public TimeSpan BlockDuration { get; set; }
		public string ReplyCode   { get; set; }
	}
}
