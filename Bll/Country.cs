﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll
{
	public class Country : BaseDataObject
	{
		private Dal.Netpay.CountryList _entity;

		public int ID { get { return _entity.CountryID.GetValueOrDefault(); } }
		public string Name { get { return _entity.Name; } set { _entity.Name = value; } }
		public byte? SortOrder { get { return _entity.SortOrder; } set { _entity.SortOrder = value; } }

		public string IsoCode2 { get { return _entity.CountryISOCode; } set { _entity.CountryISOCode = value; } }
		public string IsoCode3 { get { return _entity.ISOCode3; } set { _entity.ISOCode3 = value; } }
		public string IsoNumber { get { return _entity.ISONumber; } set { _entity.ISONumber = value; } }

		public string PhoneCode { get { return _entity.PhoneCode; } set { _entity.PhoneCode = value; } }
		public string ZipCodeRegExPattern { get { return _entity.ZipCodeRegExPattern; } set { _entity.ZipCodeRegExPattern = value; } }

		public byte IbanLength { get { return _entity.IBANLength; } set { _entity.IBANLength = value; } }
		public bool IsABARequired { get { return _entity.IsABARequired; } set { _entity.IsABARequired = value; } }
		public bool IsSepa { get { return _entity.IsSEPA; } set { _entity.IsSEPA = value; } }

		private Country(Guid credentialsToken, Dal.Netpay.CountryList entity)
			: base(credentialsToken)
		{ 
			_entity = entity;
		}

	}
}
