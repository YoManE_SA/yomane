﻿using System;

using Netpay.Infrastructure;
using Netpay.CommonTypes;

namespace Netpay.Bll
{
	/// <summary>
	/// Provides currency based operations.
	/// </summary>
	public struct Money
	{
		public Money(decimal amount, int currencyID) : this(amount, Currency.Get(currencyID)) { }
		public Money(decimal amount, string currencyIso) : this(amount, Currency.Get(currencyIso)) { }
		public Money(decimal amount, CommonTypes.Currency currency) : this(amount, Currency.Get(currency)) { }

		public Money(decimal amount, Currency currency)
		{
			if (currency == null || currency.BaseRate == 0)
				throw new ArgumentException("Invalid currency.");

			_amount = amount;
			_currency = currency;
		}
		
		private decimal _amount;
		private Currency _currency;

		public decimal Amount
		{
			get { return _amount; }
		}

		public Currency Currency
		{
			get { return _currency; }
		}

		public override string ToString()
		{
			return Amount.ToAmountFormat(Currency);
		}
		
		public string ToIsoString() 
		{
			return string.Format("{0} {1:0.00}", Currency.IsoCode, Amount);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		/// <summary>
		/// Base amount comparison
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static bool operator >(Money a, Money b)
		{
			return a.BaseAmount > b.BaseAmount;
		}

		/// <summary>
		/// Base amount comparison
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static bool operator ==(Money a, Money b)
		{
			return a.BaseAmount == b.BaseAmount;
		}

		/// <summary>
		/// Base amount comparison
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static bool operator !=(Money a, Money b)
		{
			return a.BaseAmount != b.BaseAmount;
		}

		/// <summary>
		/// Base amount comparison
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static bool operator <(Money a, Money b)
		{
			return a.BaseAmount < b.BaseAmount;
		}

		/// <summary>
		/// Adds two Currency amounts.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns>Money with the currency of 'a'</returns>
		public static Money operator +(Money a, Money b)
		{
			decimal convertedSum = a.Amount + b.ConvertTo(a.Currency).Amount;
			return new Money(convertedSum, a.Currency);
		}

		/// <summary>
		/// Subtracts two Currency amounts.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns>Money with the currency of 'a'</returns>
		public static Money operator -(Money a, Money b)
		{
			decimal convertedSum = a.Amount - b.ConvertTo(a.Currency).Amount;
			return new Money(convertedSum, a.Currency);
		}

		/// <summary>
		/// Multiplies two Currency amounts.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns>Money with the currency of 'a'</returns>
		public static Money operator *(Money a, Money b)
		{
			decimal convertedSum = a.Amount * b.ConvertTo(a.Currency).Amount;
			return new Money(convertedSum, a.Currency);
		}

		/// <summary>
		/// Division of two Currency amounts.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns>Money with the currency of 'a'</returns>
		public static Money operator /(Money a, Money b)
		{
			decimal convertedSum = a.Amount / b.ConvertTo(a.Currency).Amount;
			return new Money(convertedSum, a.Currency);
		}

		public decimal BaseAmount
		{
			get
			{
				return _amount * _currency.BaseRate;
			}
		}

		public Money ConvertTo(Currency currency)
		{
			decimal convertedAmount = BaseAmount / currency.BaseRate;
			return new Money(convertedAmount, currency);
		}

		public Money ConvertTo(CommonTypes.Currency currency)
		{
			Currency vo = Currency.Get(currency);
			return ConvertTo(vo);
		}	

		public Money ConvertTo(int currencyID)
		{
			Currency vo = Currency.Get(currencyID);
			return ConvertTo(vo);
		}

		public static decimal Convert(decimal amount, CommonTypes.Currency fromCurrency, CommonTypes.Currency toCurrency)
		{
			return Currency.Convert(amount, fromCurrency, toCurrency);
		}

		public static decimal ConvertRate(CommonTypes.Currency fromCurrency, CommonTypes.Currency toCurrency)
		{
			return Currency.ConvertRate(Currency.Get(fromCurrency), Currency.Get(toCurrency));
		}

		public static decimal ConvertRateWithFee(CommonTypes.Currency fromCurrency, CommonTypes.Currency toCurrency)
		{
			return Currency.ConvertRateWithFee(Currency.Get(fromCurrency), Currency.Get(toCurrency));
		}
		

	}
}
