﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure;
using Netpay.CommonTypes;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure.Domains;

namespace Netpay.Bll
{
	public static class TransactionAmounts
	{
		public static void AddAmount(Domain domain, TransactionAmountVO data)
		{
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			tblTransactionAmount entity = new tblTransactionAmount();
			entity.InsertDate = data.Date;
			entity.TypeID = (int)data.Type;
			entity.Amount = data.Amount.GetValueOrDefault();
			entity.MerchantID = data.MarchantID.GetValueOrDefault();
			entity.SettledAmount = data.SettledAmount.GetValueOrDefault();
			entity.SettlementDate = data.SettlementDate;
			switch (data.TransStatus)
			{
				case TransactionStatus.Captured:
					entity.TransPassID = data.TransactionID;
					break;
				case TransactionStatus.Declined:
					entity.TransPassID = data.TransactionID;
					break;
				case TransactionStatus.Authorized:
					entity.TransPassID = data.TransactionID;
					break;
				case TransactionStatus.Pending:
					entity.TransPassID = data.TransactionID;
					break;
				default:
					break;
			}

			dc.tblTransactionAmounts.InsertOnSubmit(entity);
			dc.SubmitChanges();
		}

		public static List<TransactionAmountVO> GetTransactionAmounts(TransactionStatus status, int transactionID, SearchFilters filters)
		{
			User user = SecurityManager.GetInternalUser(new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser, UserType.MerchantLimited, UserType.MerchantPrimary, UserType.Customer });
			if (user.Type == UserType.MerchantLimited || user.Type == UserType.MerchantPrimary)
			{
				var transaction = Transaction.Transaction.GetTransaction(null, transactionID, status);
				if (transaction == null)
					throw new ApplicationException("Transaction not found.");
			}

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			IQueryable<tblTransactionAmount> expression = from e in dc.tblTransactionAmounts select e;
			switch (status)
			{
				case TransactionStatus.Captured:
					expression = expression.Where(e => e.TransPassID == transactionID);
					break;
				case TransactionStatus.Declined:
					expression = expression.Where(e => e.TransFailID == transactionID);
					break;
				case TransactionStatus.Authorized:
					expression = expression.Where(e => e.TransApprovalID == transactionID);
					break;
				case TransactionStatus.Pending:
					expression = expression.Where(e => e.TransPendingID == transactionID);
					break;
				default:
					throw new ApplicationException("Invalid transaction status.");
			}

			if (filters != null)
			{
				// filter groups 
				if (filters.transactionAmountGroup != null)
				{
					List<TransactionAmountType> amountTypes = user.Domain.Cache.GetTransactionAmountGroup(filters.transactionAmountGroup.Value);
					expression = expression.Where(te => amountTypes.Contains((TransactionAmountType)te.TypeID));
				}

				if (filters.transactionAmountTypeIDs != null)
					expression = expression.Where(te => filters.transactionAmountTypeIDs.Contains(te.TypeID));
			}

			List<TransactionAmountVO> events = new List<TransactionAmountVO>();
			foreach (var currentEntity in expression)
			{
				TransactionAmountVO eventVO = new TransactionAmountVO(currentEntity);
				events.Add(eventVO);
			}
			return events;
		}

		public static List<TransactionAmountVO> GetPaymentTransactionAmounts(int payID)
		{
			User user = SecurityManager.GetInternalUser(new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser, UserType.MerchantLimited, UserType.MerchantPrimary });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var res = from p in dc.tblTransactionAmounts where p.SettlementID == payID && (p.TransFailID == null && p.TransPassID == null && p.TransPendingID == null && p.TransApprovalID == null) select new TransactionAmountVO(p);
			if (user.Type != UserType.NetpayAdmin) res.Where(p => p.MarchantID == user.ID);
			return res.ToList();
		}

		public static List<SettlementAmountVO> GetPaymentSettlmentAmounts(int payID)
		{
			User user = SecurityManager.GetInternalUser(new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser, UserType.MerchantLimited, UserType.MerchantPrimary });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var res = from p in dc.tblTransactionAmounts join s in dc.tblSettlementAmounts on p.SettlementAmountID equals s.ID where p.SettlementID == payID select new SettlementAmountVO(s, p);
			if (user.Type != UserType.NetpayAdmin) res.Where(p => p.MerchantID == user.ID);
			return res.ToList();
		}

		public static Dictionary<TransactionAmountType, CountAmount> GetTransactionAmountsTotals(TransactionStatus status, SearchFilters filters)
		{
			User user = SecurityManager.GetInternalUser(new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser, UserType.MerchantLimited, UserType.MerchantPrimary });
			if (user.Type == UserType.MerchantLimited || user.Type == UserType.MerchantPrimary)
				filters.merchantIDs = user.AllowedMerchantsIDs;
			return GetTransactionAmountsTotals(user.Domain, status, filters);
		}

		public static Dictionary<TransactionAmountType, CountAmount> GetTransactionAmountsTotals(Domain d, TransactionStatus status, SearchFilters filters)
		{
			NetpayDataContext dc = new NetpayDataContext(d.Sql1ConnectionString);
			var expression = from e in dc.tblTransactionAmounts select e;
			if (filters != null)
			{
				if (filters.merchantIDs != null) expression.Where(e => filters.merchantIDs.Contains(e.MerchantID.GetValueOrDefault()));
				if (filters.affiliateId != null) expression.Where(e => filters.affiliateId == e.Affiliates_id);
				if (filters.debitCompanyID != null) expression.Where(e => filters.debitCompanyID == e.DebitCompany_id);
			}
			return expression.GroupBy(e => e.TypeID).Select(g => new { amountTypeID = g.Key, sumAmount = g.Sum(e => e.Amount), rowCount = g.Count() }).ToDictionary(g => (TransactionAmountType)g.amountTypeID, g => new CountAmount(g.sumAmount, g.rowCount));
		}

		public enum GroupBy
		{
			Currency = 0x00000001,
			PaymentMethod = 0x00000002,
			DebitCompany = 0x00000004,
			Terminal = 0x00000008,
			BankAccount = 0x00000010,
			Merchant = 0x00000020,
			MerchantStatus = 0x00000040,
			Industry = 0x00000080,
			IpCountry = 0x00000100,
			Bin = 0x00000200,
			BinCountry = 0x00000400,
			CareOfUser = 0x00000800,
			Year = 0x00001000,
			Quarter = 0x00002000,
			Month = 0x00004000,
			Week = 0x00008000,
			Day = 0x00010000,
		}

		public enum EventUseTable
		{
			TransPass = 0x0001,
			TransFail = 0x0002,
			TransApproval = 0x0004,
			TransPending = 0x0008,
			Company = 0x0010,
			SystemCurrencies = 0x0020,
			PaymentMethod = 0x0040,
			Terminals = 0x0080,
			DebitCompany = 0x0100,
			Countries = 0x0200,
			SecurityUser = 0x0400,
			BankAccounts = 0x0800,
			CreditCard = 0x1000,
			GDMerchantStatus = 0x2000,
			GDIndusty = 0x4000,
		}

		public static System.Data.DataTable GetTransactionAmountsReport(SearchFilters filters, GroupBy gb)
		{
			System.Data.DataTable table = new System.Data.DataTable();
			EventUseTable tableUseage = EventUseTable.TransPass;
			User user = SecurityManager.GetInternalUser(new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser });
			string select = "", from = "", where = "", groupBy = "", orderBy = "", lockOptions = " WITH (NOLOCK) ";
			//add fields by group
			select = "TypeID, Count(*), Sum(tblTransactionAmount.Amount)";
			if ((gb & GroupBy.Currency) != 0)
			{
				tableUseage |= EventUseTable.SystemCurrencies;
				table.Columns.Add("CurrencyID", typeof(int)); table.Columns.Add("Currency", typeof(string));
				select += ", CUR_ID As CurrencyID, CUR_ISOName As Currency";
				groupBy += ", CUR_ID, CUR_ISOName";
				orderBy += ", CUR_ISOName Asc";
			}
			if ((gb & GroupBy.PaymentMethod) != 0)
			{
				tableUseage |= EventUseTable.PaymentMethod;
				table.Columns.Add("PaymentMethodID", typeof(short)); table.Columns.Add("PaymentMethod", typeof(string));
				select += ", tblCompanyTransPass.PaymentMethod As PaymentMethodID, pm_Name As PaymentMethod";
				groupBy += ", tblCompanyTransPass.PaymentMethod, pm_Name";
				orderBy += ", pm_Name Asc";
			}
			if ((gb & GroupBy.DebitCompany) != 0)
			{
				tableUseage |= EventUseTable.DebitCompany;
				table.Columns.Add("DebitCompanyID", typeof(int)); table.Columns.Add("DebitCompany", typeof(string));
				select += ", tblDebitCompany.debitCompany_id As DebitCompanyID, dc_Name As DebitCompany";
				groupBy += ", tblDebitCompany.debitCompany_id, dc_Name";
				orderBy += ", dc_Name Asc";
			}
			if ((gb & GroupBy.Terminal) != 0)
			{
				tableUseage |= EventUseTable.Terminals;
				table.Columns.Add("TerminalID", typeof(string)); table.Columns.Add("Terminal", typeof(string));
				string nameField = "(tblDebitTerminals.dt_name + ' (' + tblCompanyTransPass.TerminalNumber + ')' + CASE LTrim(RTrim(dt_ContractNumber)) WHEN '' THEN '' ELSE ', CN '+dt_ContractNumber END)";
				select += ", tblDebitTerminals.TerminalNumber As TerminalID, " + nameField + " As Terminal";
				groupBy += ", tblDebitTerminals.TerminalNumber, " + nameField;
				orderBy += ", tblDebitTerminals.TerminalNumber Asc";
			}
			if ((gb & GroupBy.BankAccount) != 0)
			{
				tableUseage |= EventUseTable.BankAccounts | EventUseTable.Terminals;
				table.Columns.Add("BankAccountID", typeof(int)); table.Columns.Add("BankAccount", typeof(string));
				select += ", tblBankAccounts.BA_ID As BankAccountID, (tblBankAccounts.BA_BankName + ': ' + tblBankAccounts.BA_AccountName + ' - ' + tblBankAccounts.BA_AccountNumber) As BankAccount";
				groupBy += ", BA_ID, (tblBankAccounts.BA_BankName + ': ' + tblBankAccounts.BA_AccountName + ' - ' + tblBankAccounts.BA_AccountNumber)";
				orderBy += ", tblBankAccounts.BA_ID Asc";
			}
			if ((gb & GroupBy.Merchant) != 0)
			{
				tableUseage |= EventUseTable.Company;
				table.Columns.Add("MerchantID", typeof(int)); table.Columns.Add("Merchant", typeof(string));
				select += ", tblTransactionAmount.MerchantID As MerchantID, CompanyName As Merchant";
				groupBy += ", tblTransactionAmount.MerchantID, CompanyName";
				orderBy += ", tblTransactionAmount.MerchantID Desc";
			}
			if ((gb & GroupBy.MerchantStatus) != 0)
			{
				tableUseage |= EventUseTable.Company | EventUseTable.GDMerchantStatus;
				table.Columns.Add("MerchantStatusID", typeof(int)); table.Columns.Add("MerchantStatus", typeof(string));
				select += ", gdms.GD_ID As MerchantStatusID, gdms.GD_Text As MerchantStatus";
				groupBy += ", gdms.GD_ID, gdms.GD_Text";
				orderBy += ", gdms.GD_Text Asc";
			}
			if ((gb & GroupBy.Industry) != 0)
			{
				tableUseage |= EventUseTable.Company | EventUseTable.GDIndusty;
				table.Columns.Add("IndustryID", typeof(int)); table.Columns.Add("Industry", typeof(string));
				select += ", gdi.GD_ID As IndustryID, gdi.GD_Text As Industry";
				groupBy += ", gdi.GD_ID, gdi.GD_Text";
				orderBy += ", gdi.GD_Text Asc";
			}
			if ((gb & GroupBy.IpCountry) != 0)
			{
				tableUseage |= EventUseTable.CreditCard;
				table.Columns.Add("IpCountryID", typeof(string)); table.Columns.Add("IpCountry", typeof(string));
				select += ", tblCreditCard.BINCountry as IpCountryID, tblCreditCard.BINCountry as IpCountry";
				groupBy += ", tblCreditCard.BINCountry";
				orderBy += ", tblCreditCard.BINCountry Asc";
			}
			if ((gb & GroupBy.Bin) != 0)
			{
				tableUseage |= EventUseTable.CreditCard;
				table.Columns.Add("BinId", typeof(string)); table.Columns.Add("Bin", typeof(string));
				select += ", tblCreditCard.CCard_First6 As BinID, tblCreditCard.CCard_First6 As Bin";
				groupBy += ", tblCreditCard.CCard_First6, tblCreditCard.CCard_First6";
				orderBy += ", tblCreditCard.CCard_First6 Asc";
			}
			if ((gb & GroupBy.BinCountry) != 0)
			{
				tableUseage |= EventUseTable.Countries;
				table.Columns.Add("BinCountryId", typeof(string)); table.Columns.Add("BinCountry", typeof(string));
                select += ", [List].[CountryList].CountryISOCode As BinCountryId, [List].[CountryList].Name As BinCountry";
                groupBy += ", [List].[CountryList].CountryISOCode, [List].[CountryList].Name";
                orderBy += ", [List].[CountryList].CountryISOCode Asc";
			}
			if ((gb & GroupBy.CareOfUser) != 0)
			{
				tableUseage |= EventUseTable.Company | EventUseTable.SecurityUser;
				table.Columns.Add("CareOfUserID", typeof(string)); table.Columns.Add("CareOfUser", typeof(string));
				select += ", su_Username As CareOfUserID, su_Name As CareOfUser";
				groupBy += ", su_Username, su_Name";
				orderBy += ", su_Name Asc";
			}
			if ((gb & GroupBy.Year) != 0)
			{
				table.Columns.Add("YearID", typeof(int)); table.Columns.Add("Year", typeof(string));
				select += ", Year(tblTransactionAmount.InsertDate) As YearID, Cast(Year(tblTransactionAmount.InsertDate) as nvarchar) as Year";
				groupBy += ", Year(tblTransactionAmount.InsertDate), Cast(Year(tblTransactionAmount.InsertDate) as nvarchar)";
				orderBy += ", Year(tblTransactionAmount.InsertDate) Asc";
			}
			if ((gb & GroupBy.Quarter) != 0)
			{
				table.Columns.Add("QuarterID", typeof(string)); table.Columns.Add("Quarter", typeof(string));
				select += ", Cast(Year(tblTransactionAmount.InsertDate) as nvarchar) + Cast(DATEPART(qq, tblTransactionAmount.InsertDate) as nvarchar) as QuarterID, Cast(Year(tblTransactionAmount.InsertDate) as nvarchar) + 'Q' + Cast(DATEPART(qq, tblTransactionAmount.InsertDate) as nvarchar) as Quarter";
				groupBy += ", Cast(Year(tblTransactionAmount.InsertDate) as nvarchar) + Cast(DATEPART(qq, tblTransactionAmount.InsertDate) as nvarchar), Cast(Year(tblTransactionAmount.InsertDate) as nvarchar) + 'Q' + Cast(DATEPART(qq, tblTransactionAmount.InsertDate) as nvarchar)";
				orderBy += ", Cast(Year(tblTransactionAmount.InsertDate) as nvarchar) + Cast(DATEPART(qq, tblTransactionAmount.InsertDate) as nvarchar) Asc";
			}
			if ((gb & GroupBy.Month) != 0)
			{
				table.Columns.Add("MonthID", typeof(string)); table.Columns.Add("Month", typeof(string));
				select += ", Cast(Year(tblTransactionAmount.InsertDate) as nvarchar) + Right('0' + Cast(Month(tblTransactionAmount.InsertDate) as nvarchar(2)), 2) As MonthID, Cast(Year(tblTransactionAmount.InsertDate) as nvarchar) + '/' + Right('0' + Cast(Month(tblTransactionAmount.InsertDate) as nvarchar(2)), 2) As Month";
				groupBy += ", Cast(Year(tblTransactionAmount.InsertDate) as nvarchar) + Right('0' + Cast(Month(tblTransactionAmount.InsertDate) as nvarchar(2)), 2), Cast(Year(tblTransactionAmount.InsertDate) as nvarchar) + '/' + Right('0' + Cast(Month(tblTransactionAmount.InsertDate) as nvarchar(2)), 2)";
				orderBy += ", Cast(Year(tblTransactionAmount.InsertDate) as nvarchar) + Right('0' + Cast(Month(tblTransactionAmount.InsertDate) as nvarchar(2)), 2) Asc";
			}
			if ((gb & GroupBy.Week) != 0)
			{
				table.Columns.Add("WeekID", typeof(string)); table.Columns.Add("Week", typeof(string));
				select += ", Cast(Year(tblTransactionAmount.InsertDate) as nvarchar) + Right('0' + Cast(DATEPART(ww, tblTransactionAmount.InsertDate) as nvarchar(2)), 2) As WeekID, Cast(Year(tblTransactionAmount.InsertDate) as nvarchar) + 'W' + Right('0' + Cast(DATEPART(ww, tblTransactionAmount.InsertDate) as nvarchar(2)), 2) As Week";
				groupBy += ", Cast(Year(tblTransactionAmount.InsertDate) as nvarchar) + Right('0' + Cast(DATEPART(ww, tblTransactionAmount.InsertDate) as nvarchar(2)), 2), Cast(Year(tblTransactionAmount.InsertDate) as nvarchar) + 'W' + Right('0' + Cast(DATEPART(ww, tblTransactionAmount.InsertDate) as nvarchar(2)), 2)";
				orderBy += ", Cast(Year(tblTransactionAmount.InsertDate) as nvarchar) + Right('0' + Cast(DATEPART(ww, tblTransactionAmount.InsertDate) as nvarchar(2)), 2) Asc";
			}
			if ((gb & GroupBy.Day) != 0)
			{
				table.Columns.Add("DayID", typeof(string)); table.Columns.Add("Day", typeof(DateTime));
				select += ", Convert(NVarChar, tblTransactionAmount.InsertDate, 112) As DayID, Cast(Floor(Cast(tblTransactionAmount.InsertDate As float)) As DateTime) As Day";
				groupBy += ", Convert(NVarChar, tblTransactionAmount.InsertDate, 112), Cast(Floor(Cast(tblTransactionAmount.InsertDate As float)) As DateTime)";
				orderBy += ", Convert(NVarChar, tblTransactionAmount.InsertDate, 112) Asc";
			}

			//format from clause
			from += "tblTransactionAmount";
			if ((tableUseage & EventUseTable.TransPass) != 0) from += " LEFT JOIN tblCompanyTransPass " + lockOptions + " ON(tblCompanyTransPass.ID = tblTransactionAmount.TransPassID)";
			if ((tableUseage & EventUseTable.PaymentMethod) != 0) from += " LEFT JOIN [List].[PaymentMethod] " + lockOptions + " ON(tblCompanyTransPass.PaymentMethod = [List].[PaymentMethod].PaymentMethod_id)";
			if ((tableUseage & EventUseTable.Company) != 0) from += " LEFT JOIN tblCompany " + lockOptions + " ON(tblTransactionAmount.MerchantID = tblCompany.ID)";
			if ((tableUseage & EventUseTable.SystemCurrencies) != 0) from += " LEFT JOIN tblSystemCurrencies " + lockOptions + " ON(tblCompanyTransPass.Currency = CUR_ID)";
			if ((tableUseage & EventUseTable.Terminals) != 0) from += " LEFT JOIN tblDebitTerminals " + lockOptions + " ON(tblDebitTerminals.terminalNumber=tblCompanyTransPass.TerminalNumber And tblDebitTerminals.DebitCompany=tblCompanyTransPass.DebitCompanyID)";
			if ((tableUseage & EventUseTable.DebitCompany) != 0) from += " LEFT JOIN tblDebitCompany " + lockOptions + " ON(tblCompanyTransPass.DebitCompanyID = tblDebitCompany.debitCompany_id)";
			if ((tableUseage & EventUseTable.BankAccounts) != 0) from += " LEFT JOIN tblBankAccounts " + lockOptions + " ON(tblBankAccounts.BA_ID = tblDebitTerminals.dt_BankAccountID)";
            if ((tableUseage & EventUseTable.CreditCard) != 0) from += " LEFT JOIN tblCreditCard " + lockOptions + " ON(tblCreditCard.ID = tblCompanyTransPass.CreditCardID)";
            if ((tableUseage & EventUseTable.Countries) != 0) from += " LEFT JOIN [List].[CountryList] " + lockOptions + " ON([List].[CountryList].CountryISOCode = tblCompanyTransPass.IPCountry)";
			if ((tableUseage & EventUseTable.SecurityUser) != 0) from += " LEFT JOIN tblSecurityUser " + lockOptions + " ON(tblCompany.careOfAdminUser = tblSecurityUser.su_Username) ";
			if ((tableUseage & EventUseTable.GDMerchantStatus) != 0) from += " LEFT JOIN tblGlobalData as gdms " + lockOptions + " ON(gdms.GD_Group=44 And gdms.GD_LNG=1 And tblCompany.ActiveStatus=gdms.GD_ID)";
			if ((tableUseage & EventUseTable.GDIndusty) != 0) from += " LEFT JOIN tblGlobalData as gdi " + lockOptions + " ON(gdi.GD_Group=51 And gdi.GD_LNG=1 And tblCompany.CompanyIndustry_id = gdi.GD_ID) ";

			//format where conditions
			if (filters.dateFrom != null) where += " And tblTransactionAmount.InsertDate > '" + filters.dateFrom.Value.ToSql() + "'";
			if (filters.dateTo != null) where += " And tblTransactionAmount.InsertDate < '" + filters.dateTo.Value.ToSql() + "'";
			if (filters.merchantIDs != null && filters.merchantIDs.Count > 0) where += " And tblTransactionAmount.MerchantID IN (" + filters.merchantIDs.ToDelimitedString(',') + ")";
			if (filters.merchantStatus != null) where += " And tblTransactionAmount.MerchantID IN (SELECT ID FROM tblCompany WHERE ActiveStatus=" + filters.merchantStatus + ")";
			if (filters.industryID != null) where += " And tblTransactionAmount.MerchantID IN (SELECT ID FROM tblCompany Where CompanyIndustry_id IN(" + filters.industryID + "))";

			if (filters.debitCompanyID != null) where += " And tblCompanyTransPass.DebitCompanyID IN(" + filters.debitCompanyID + ")";
			if (filters.paymentMethodsIds != null) where += " And tblCompanyTransPass.PaymentMethod IN(" + filters.paymentMethodsIds + ")";
			if (filters.terminalNumber != null)
			{
				//If nTerminalList = "-2" Then sWhere &= " And tblCompanyTransPass.TerminalNumber IN (Select terminalNumber From tblDebitTerminals Where isActive=1)" _
				where += " And tblCompanyTransPass.TerminalNumber ='" + filters.terminalNumber + "'";
			}
			//if (filters.bankAccount != null) sWhere += " And tblCompanyTransPass.TerminalNumber IN (Select terminalNumber From tblDebitTerminals Where dt_BankAccountID IN(" + filters.bankAccount + "))"
			if (filters.ipCountryIso2 != null) where += " And IPCountry IN('" + filters.ipCountryIso2.Replace(",", "','") + "')";
            if (filters.binCountryIso2 != null) where += " And tblCompanyTransPass.CreditCardID IN(Select tblCreditCard.ID From tblCreditCard Where tblCreditCard.BINCountry IN('" + filters.binCountryIso2.Replace(",", "','") + "'))";
            //if (filters.BinValue) sWhere += " And tblCompanyTransPass.CreditCardID IN(Select tblCreditCard.ID From tblCreditCard Where tblCreditCard.CCard_First6 IN(" + filters.BinValue + "))";
			if (filters.currencyID != null) where += " And Currency IN(" + filters.currencyID + ")";

			//format query
			groupBy += ", AmountTypeID"; groupBy = groupBy.Substring(2);
			orderBy += ", AmountTypeID"; orderBy = orderBy.Substring(2);
			if (where.Length > 4) where = " Where " + where.Substring(5);
			string query = string.Format("Select {0} From {1} {2} Group By {3} Order By {4}", select, from, where, groupBy, orderBy);
			//System.Web.HttpContext.Current.Response.Write(query); System.Web.HttpContext.Current.Response.Flush();

			//prepare output fields
			int lastGroupIndex = table.Columns.Count;
			string[] evNames = System.Enum.GetNames(typeof(TransactionAmountType));
			foreach (string s in evNames)
			{
				table.Columns.Add(s + "_Amount", typeof(decimal));
				table.Columns.Add(s + "_Count", typeof(int));
			}

			System.Data.DataRow row = null;
			using (System.Data.IDataReader reader = Totals.Helpers.ExecReader(user.Domain, query))
			{
				while (reader.Read())
				{
					if (row != null)
					{
						for (int i = 0; i < lastGroupIndex; i++)
							if (!row[i].Equals(reader[table.Columns[i].ColumnName]))
							{
								row = null;
								break;
							}
					}
					if (row == null)
					{
						table.Rows.Add(row = table.NewRow());
						for (int i = 0; i < lastGroupIndex; i++)
							row[i] = reader[table.Columns[i].ColumnName];
					}
					string evName = ((TransactionAmountType)reader[0]).ToString();
					if (table.Columns.Contains(evName + "_Count"))
					{
						row[evName + "_Count"] = reader[1];
						row[evName + "_Amount"] = reader[2];
					}
				}
				reader.Close();
			}
			//table.DefaultView.Sort = "assasd asc";
			return table;
		}
	}


}
