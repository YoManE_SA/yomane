﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll
{
	public interface IAddress
	{
		string AddressLine1 { get; set; }
		string AddressLine2 { get; set; }
		string City { get; set; }
		string PostalCode { get; set; }
		string StateISOCode { get; set; }
		string CountryISOCode { get; set; }
	}

	public class Address : IAddress
	{
		public string AddressLine1 { get; set; }
		public string AddressLine2 { get; set; }
		public string City { get; set; }
		public string PostalCode { get; set; }
		public string StateISOCode { get; set; }
		public string CountryISOCode { get; set; }
		public static void Copy(IAddress src, IAddress dest)
		{
			dest.AddressLine1 = src.AddressLine1;
			dest.AddressLine2 = src.AddressLine2;
			dest.City = src.City;
			dest.PostalCode = src.PostalCode;
			dest.StateISOCode = src.StateISOCode;
			dest.CountryISOCode = src.CountryISOCode;
		}

        public static bool IsEmpty(IAddress address)
        {
            return
                string.IsNullOrEmpty(address.AddressLine1) &&
                string.IsNullOrEmpty(address.AddressLine2) &&
                string.IsNullOrEmpty(address.City) &&
                string.IsNullOrEmpty(address.PostalCode) &&
                string.IsNullOrEmpty(address.StateISOCode) &&
                string.IsNullOrEmpty(address.CountryISOCode);
        }

        /*
		public static string GetLongString(IAddress value)
		{
			//var state
			return string.Format("{0}\r\n{1}\r\n{2}, {3}\r\n{4} {5}",
				value.AddressLine1, value.AddressLine2, value.City, value.PostalCode,
				(!string.IsNullOrEmpty(value.StateISOCode) ? WebUtils.GetStateName(value.StateISOCode) + ", " : null),
				(!string.IsNullOrEmpty(value.CountryISOCode) ? WebUtils.GetCountryName(value.CountryISOCode) : null)).Replace("<br/>,", "<br/>").Replace("<br/><br/>", "<br/>");
			
		}
		*/
    }

}
