﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll
{
	public interface ISortAndPage
	{
		string SortKey { get; set; }
		bool SortDesc { get; }
		int RowCount { get; set; }
		int PageSize { get; }
		int PageCurrent { get; set; }
		//bool CountOnly { get; set; }
		Func<Guid, ISortAndPage, System.Collections.IEnumerable> DataFunction { get; set; }		
	}

	public interface IHasSortAndPage
	{
		ISortAndPage SortAndPage { get; }
	}

	[Serializable]
	public class SortAndPage : ISortAndPage
	{
		public SortAndPage(int page, int pageSize, string sortKey = null, bool sortDesc = false) { PageCurrent = page; PageSize = pageSize; SortKey = sortKey; SortDesc = sortDesc; }
		public SortAndPage() { PageSize = 10; }
		public string SortKey { get; set; }
		public bool SortDesc { get; set; }
		public int RowCount { get; set; }
		public int PageSize { get; set; }
		public int PageCurrent { get; set; }
		public int PageCount { get { return (int)Math.Ceiling((double)RowCount / (double)PageSize); } }
		//public bool CountOnly { get; set; }
		public Func<Guid, ISortAndPage, System.Collections.IEnumerable> DataFunction { get; set; }

		public int RowFrom { get { return (PageCurrent * PageSize) + 1; } }
		public int RowTo { get { return Math.Min(RowCount, ((PageCurrent + 1) * PageSize)); } }
	
	}
}
