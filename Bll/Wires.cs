﻿using System;
using System.Collections.Generic;
using System.Linq;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure.VO;


namespace Netpay.Bll
{
    public static class Wires
    {
        public static List<WireMoneyVO> SearchWires(Guid credentialsToken, SearchFilters filters, PagingInfo pi, bool sortDesc)
        {
            User user = SecurityManager.GetInternalUser(credentialsToken);
            if ((user.Type != UserType.NetpayUser) && (user.Type != UserType.NetpayAdmin))
                filters.merchantIDs = SecurityManager.FilterRequestedIDs(filters.merchantIDs, user.AllowedMerchantsIDs);

            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            var expression = 
                from p in dc.tblWireMoneys
                join c in dc.tblCompanies on p.Company_id equals c.ID
                orderby p.WireMoney_id ascending
                select new { wire = p, companyName = c.CompanyName };

            if (sortDesc) expression = expression.OrderByDescending(p => p.wire.WireMoney_id);
            else expression = expression.OrderBy(p => p.wire.WireMoney_id);

            // add filters				
            if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
                expression = expression.Where(p => filters.merchantIDs.Contains(p.wire.Company_id));

            if (filters.dateFrom != null) expression = expression.Where(p => p.wire.wireInsertDate >= filters.dateFrom);
            if (filters.dateTo != null) expression = expression.Where(p => p.wire.wireInsertDate <= filters.dateTo);
            //if (filters.wireID != null) expression = expression.Where(p => p.wireInsertDate <= filters.dateTo);
            if (filters.minimumId != null) expression = expression.Where(p => p.wire.WireMoney_id > filters.minimumId);
			if (filters.maximumId != null) expression = expression.Where(p => p.wire.WireMoney_id < filters.maximumId);
            if (filters.wireId != null) expression = expression.Where(p => p.wire.WireMoney_id == filters.wireId);
            if (filters.wireStatus != null) expression = expression.Where(p => p.wire.WireStatus == (int)filters.wireStatus);
            if (filters.wireFlag != null) expression = expression.Where(p => p.wire.WireFlag == (int)filters.wireFlag);
            if (filters.wireApprovalLevel1 != null) expression = expression.Where(p => p.wire.wireApproveLevel1 == filters.wireApprovalLevel1);
            if (filters.wireApprovalLevel2 != null) expression = expression.Where(p => p.wire.wireApproveLevel2 == filters.wireApprovalLevel2);
            
            // handle paging if not null
            if (pi != null)
            {
                pi.TotalItems = expression.Count();
                expression = expression.Skip(pi.Skip).Take(pi.Take);
            }

            //var tst = expression.Select(e => e.Company_id).FirstOrDefault();
            return expression.Select(p => new WireMoneyVO(p.wire, p.companyName)).ToList<WireMoneyVO>();
        }

        public static void SetWireStatus(Guid credentialsToken, int wireMoneyID, bool? wireApproval, int approvalLevel) 
        { 

            User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayUser, UserType.NetpayAdmin });
            NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
            var wire = (from w in dc.tblWireMoneys where w.WireMoney_id == wireMoneyID select w).Single();
            
	        if (wire.WireFlag == (int) WireFlag.Done) return;
		    if (approvalLevel == 1) {
                wire.wireApproveLevel1 = wireApproval;
		    } else if(approvalLevel == 2) {
			    //if (user.UserName == FinanceUser) 
                    wire.wireApproveLevel2 = wireApproval;
		    } else throw new ApplicationException("invalid approvalLevel value (can only be 1 or 2)");

            if (wire.wireApproveLevel1 == null && wire.wireApproveLevel2 == null)
                wire.WireFlag = (int) WireFlag.Waiting;
            else if ((!wire.wireApproveLevel1.GetValueOrDefault(true)) || (!wire.wireApproveLevel2.GetValueOrDefault(true)))
                wire.WireFlag = (int) WireFlag.Rejected;
            else if(wire.wireApproveLevel1 == null || wire.wireApproveLevel2 == null)
                wire.WireFlag = (int) WireFlag.PartiallyApproved;
            else if(wire.wireApproveLevel1.Value && wire.wireApproveLevel2.Value)
                wire.WireFlag = (int) WireFlag.Approved;
            dc.SubmitChanges();

            string setValue = "";
            if (wireApproval == null) setValue = "Cleared";
            else if (wireApproval.Value) setValue = "Approved";
            else if (!wireApproval.Value) setValue = "Hold";

            tblWireMoneyLog eml = new tblWireMoneyLog();
            eml.WireMoney_id = wire.WireMoney_id;
            eml.wml_date = DateTime.Now;
            eml.wml_user = user.UserName;
            eml.wml_description = string.Format("Approval changed: {0}", setValue);
            dc.tblWireMoneyLogs.InsertOnSubmit(eml);
            dc.SubmitChanges();
        }
    }
}
