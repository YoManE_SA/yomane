﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Dal.Netpay;

namespace Netpay.Bll.RiskManagement
{
	public class CreditcardRiskRule
	{
		public CreditcardRiskRule(tblCreditCardRiskManagement entity)
		{
			ID = entity.CCRM_ID;
			IsActive = entity.CCRM_IsActive;
            ApplyVT = entity.CCRM_ApplyVT;
			InsertDate = entity.CCRM_InsDate;
			CreditType = entity.CCRM_CreditType;
			CurrencyID = entity.CCRM_Currency;
			MaxAmount = entity.CCRM_Amount;
			MaxAmountReplyCode = entity.CCRM_ReplyAmount;
			MaxCount = entity.CCRM_MaxTrans;
			MaxCountReplyCode = entity.CCRM_ReplyMaxTrans;
			ReplySource = entity.CCRM_ReplySource;
			TimeframeHours = entity.CCRM_Hours;
			BlockHours = entity.CCRM_BlockHours;
			TimeframeDays = entity.ccrm_days;
			BlockDays = entity.ccrm_blockdays;
			WhitelistLevel = entity.CCRM_WhitelistLevel;
			MerchantID = entity.CCRM_CompanyID;
			PaymentMethod = entity.CCRM_PaymentMethod;
		}
		
		public int ID { get; set; }
		public bool IsActive { get; set; }
		public bool ApplyVT { get; set; }
		public DateTime InsertDate { get; set; }
		public byte CreditType { get; set; }
		public int CurrencyID { get; set; }
		public decimal MaxAmount { get; set; }
		public int MaxCount { get; set; }
		public int ReplySource { get; set; }
		public int TimeframeHours { get; set; }
		public int BlockHours { get; set; }
		public int? TimeframeDays { get; set; }
		public int? BlockDays { get; set; }
		public string MaxAmountReplyCode { get; set; }
		public string MaxCountReplyCode { get; set; }
		public int? WhitelistLevel { get; set; }
		public int? MerchantID { get; set; }
		public short? PaymentMethod { get; set; }
	}
}