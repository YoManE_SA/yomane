﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.RiskManagement
{
	public class TransactionCache
	{
		public decimal Amount { get; set; }
		public DateTime InsertDate { get; set; }
		public int CurrencyID { get; set; }
		public int? MerchantID { get; set; }
		public string IP { get; set; }
		public int Installments { get; set; }
		public byte[] EncryptedCCNumber { get; set; }
	}
}
