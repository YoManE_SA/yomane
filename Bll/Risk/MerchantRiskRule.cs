﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Risk
{
	public class MerchantRiskRule : BaseDataObject
	{
		public const string SecuredObjectName = "MerchantRiskRule";
		public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Risk.Module.Current, SecuredObjectName); } }

		public static int[] RuleAnswers = new int[] { 505, 509, 525, 547, 561, 562, 563, 580, 581, 582, 583, 584, 585, 586, 587, 588, 589, 593, 594, 597, 598 };
		public enum WhiteListLevel { Bronze = 0, Silver = 1, Gold = 2, Platinum = 3 }
		public enum ReplySources { Passed = -1, All_Declines = 999, Gateway_Decline = 2, Risk_Decline = 1, Issuer_Decline = 0 }

		private Dal.Netpay.tblCreditCardRiskManagement _entity;
		public int ID { get { return _entity.CCRM_ID; } }
		public DateTime InsertDate { get { return _entity.CCRM_InsDate; } }
		public int? MerchantID { get { return _entity.CCRM_CompanyID; } }
		public bool IsActive { get { return _entity.CCRM_IsActive; } set { _entity.CCRM_IsActive = value; } }
		public bool ApplyVT { get { return _entity.CCRM_ApplyVT; } set { _entity.CCRM_ApplyVT = value; } }
		public byte CreditType { get { return _entity.CCRM_CreditType; } set { _entity.CCRM_CreditType = value; } }
		public int CurrencyID { get { return _entity.CCRM_Currency; } set { _entity.CCRM_Currency = (short)value; } }
		public decimal MaxAmount { get { return _entity.CCRM_Amount; } set { _entity.CCRM_Amount = value; } }
		public int MaxCount { get { return _entity.CCRM_MaxTrans; } set { _entity.CCRM_MaxTrans = value; } }
		public int ReplySource { get { return _entity.CCRM_ReplySource; } set { _entity.CCRM_ReplySource = value; } }
		public int CCRM_DAYS { get { return _entity.CCRM_Days.GetValueOrDefault(); } }
		public int CCRM_Hours { get { return _entity.CCRM_Hours; } set { _entity.CCRM_Hours = value; } }
		public int CCRM_BLOCKDAYS { get { return _entity.CCRM_Blockdays.GetValueOrDefault(); } }
		public int CCRM_BlockHours { get { return _entity.CCRM_BlockHours; } set { _entity.CCRM_BlockHours = value; } }
		public string MaxAmountReplyCode { get { return _entity.CCRM_ReplyAmount; } set { _entity.CCRM_ReplyAmount = value; } }
		public string MaxCountReplyCode { get { return _entity.CCRM_ReplyMaxTrans; } set { _entity.CCRM_ReplyMaxTrans = value; } }
		public int? WhitelistLevel { get { return _entity.CCRM_WhitelistLevel; } set { _entity.CCRM_WhitelistLevel = value; } }
		public short? PaymentMethod { get { return _entity.CCRM_PaymentMethod; } set { _entity.CCRM_PaymentMethod = value; } }

        public string CurrencyIso
        {
            get
            {
                if (CurrencyID == -1) return "";
                else
                {
                    return Bll.Currency.GetIsoCode((CommonTypes.Currency)CurrencyID);
                }
            }
        }

        public string TimeFrameText
        {
            get
            {
                if ((CCRM_DAYS * 24) - (CCRM_Hours) == 0)
                {
                    return CCRM_DAYS.ToString() + " Days";
                }
                else
                {
                    return CCRM_Hours.ToString() + " Hrs";
                }
            }
        }

        public string BlockDurationText
        {
            get
            {
                if ((CCRM_BLOCKDAYS * 24) - (CCRM_BlockHours) == 0)
                {
                    return CCRM_BLOCKDAYS.ToString() + " Days";
                }
                else
                {
                    return CCRM_BlockHours.ToString() + " Hrs";
                }
            }
        }

		internal MerchantRiskRule(Dal.Netpay.tblCreditCardRiskManagement entity)
		{
			_entity = entity;
		}

		public MerchantRiskRule(int merchantId)
		{
			_entity = new Dal.Netpay.tblCreditCardRiskManagement();
			_entity.CCRM_InsDate = DateTime.Now;
			_entity.CCRM_CompanyID = merchantId;
		}

		public void Delete()
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            if (_entity.CCRM_ID == 0) return;
			//User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
			DataContext.Writer.tblCreditCardRiskManagements.Delete(_entity);
			DataContext.Writer.SubmitChanges();
		}

		public void Save()
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

			DataContext.Writer.tblCreditCardRiskManagements.Update(_entity, (_entity.CCRM_ID != 0));
			DataContext.Writer.SubmitChanges();
		}

		public static MerchantRiskRule Load(int id)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);
			return (from b in DataContext.Reader.tblCreditCardRiskManagements where b.CCRM_ID == id select new MerchantRiskRule(b)).SingleOrDefault();
		}

		public static List<MerchantRiskRule> Search(int merchantId)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);
			return (from b in DataContext.Reader.tblCreditCardRiskManagements where b.CCRM_CompanyID == merchantId select new MerchantRiskRule(b)).ToList();
		}
	}
}