﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.RiskManagement
{
	public class FraudDetectionRule
	{
		public FraudDetectionRule(FraudDetectionRuleGroup group, Func<FraudDetectionContext, FraudDetectionResult> detectionFunction) 
		{
			Detect = detectionFunction;
			Group = group;
		}
		
		public Func<FraudDetectionContext, FraudDetectionResult> Detect { get; set; }
		public FraudDetectionRuleGroup Group { get; set; }
	}
}
