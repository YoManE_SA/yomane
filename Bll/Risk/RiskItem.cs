﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Risk
{

    public class RiskItem : BaseDataObject
    {
        public enum RiskItemType { Black_List = 0, White_List = 1, Item_Black_List = 2 } /* The Ids are also used to select multi-view display of RiskItem */
        public enum RiskListType { Black = -1, Bronze = 0, Silver = 1, Gold = 2, Platinum = 3 }
        public enum RiskSource { System, Merchant }
        public enum RiskValueType
        {
            //AccountValue2 = -2,
            AccountValue1 = -1,
            //Unknown = 0,
            Email = 1,
            FullName = 2,
            Phone = 3,
            PersonalNumber = 4,
            Bin = 5,
            BinCountry = 6,
            IP = 7
        }

        public enum BlackListLegendValue
        {
            System,
            Merchant,
            System_And_Merchant,
            Temporary,
            Temporary_CCRM
        }

        public static Dictionary<BlackListLegendValue, Tuple<System.Drawing.Color, System.Drawing.Color>> BlackListLegendMultiColor = new Dictionary<BlackListLegendValue, Tuple<System.Drawing.Color, System.Drawing.Color>> {
            { BlackListLegendValue.System, Tuple.Create<System.Drawing.Color,System.Drawing.Color>(System.Drawing.ColorTranslator.FromHtml("#cc99cc"),System.Drawing.ColorTranslator.FromHtml("#cc99cc")) },
            { BlackListLegendValue.Merchant, Tuple.Create<System.Drawing.Color,System.Drawing.Color>(System.Drawing.ColorTranslator.FromHtml("#6699cc"),System.Drawing.ColorTranslator.FromHtml("#6699cc")) },
            { BlackListLegendValue.System_And_Merchant, Tuple.Create<System.Drawing.Color,System.Drawing.Color>(System.Drawing.ColorTranslator.FromHtml("#cc99cc"),System.Drawing.ColorTranslator.FromHtml("#6699cc")) },
            { BlackListLegendValue.Temporary, Tuple.Create<System.Drawing.Color,System.Drawing.Color>(System.Drawing.Color.Green, System.Drawing.Color.Green) },
            { BlackListLegendValue.Temporary_CCRM, Tuple.Create<System.Drawing.Color,System.Drawing.Color>(System.Drawing.ColorTranslator.FromHtml("#cc99cc"), System.Drawing.Color.Green) }
        };

        public const string SecuredObjectName = "RiskItem";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Risk.Module.Current, SecuredObjectName); } }

        public class RiskItemValue { public string Value; public RiskValueType ValueType; }

        public enum TableFilter
        {
            BLCommons = 10,
            FraudCcBlackLists = 20,
            CreditCardWhitelists = 30
        }

        public class SearchFilters
        {
            public Range<int?> ID;
            public int? MerchantID;
            public List<RiskItemType> RiskTypes;
            public Range<DateTime?> InsertDate;
            public RiskSource? Source;
            public RiskListType? ListType;
            public RiskValueType? ValueType;
            public string Value;
            public string Account1First6;
            public string Account1Last4;
            public string Text;
            public TableFilter? Table;
        }

        public int ID { get; private set; }
        public string RiskItemUniqueId
        {
            get
            {
                return GenerateUniqueRiskItemStr(ID, ValueType, ListType);
            }
        }

        public RiskItemType RiskType { get; private set; }
        public int? MerchantID { get; private set; }
        public DateTime InsertDate { get; private set; }
        public RiskSource Source { get; private set; }
        public RiskListType ListType { get; private set; }
        public RiskValueType ValueType { get; private set; }
        public string Value { get; private set; }
        public string Display { get; private set; }
        public string Comment { get; private set; }
        public string CardHolder { get; private set; }
        public DateTime? Duration { get; private set; }
        public int? CCRMID { get; private set; }
        public BlockedItemSource? BlockedItemSrc { get; private set; }
        public int? Bin { get; private set; }
        public short? Last4 { get; private set; }
        public System.Data.Linq.Binary CCNumber256 { get; private set; }

        public BlackListLegendValue? BlackListLegendVal
        {
            get
            {
                BlackListLegendValue blackListLegendVal = BlackListLegendValue.Merchant;
                if ((int)Source > 1)
                {
                    if (CCRMID == 0)
                        blackListLegendVal = BlackListLegendValue.Temporary;
                    else
                        blackListLegendVal = BlackListLegendValue.Temporary_CCRM;
                }
                else if (MerchantID <= 0)
                    blackListLegendVal = BlackListLegendValue.System;
                else if ((int)Source == 1)
                    blackListLegendVal = BlackListLegendValue.System_And_Merchant;
                else
                    blackListLegendVal = BlackListLegendValue.Merchant;

                return blackListLegendVal;
            }
        }


        public RiskItem()
        { }

        public RiskItem(int itemId, RiskValueType valueType, RiskListType riskListType)
        {
            SearchFilters filter = new SearchFilters();
            filter.ID = new Range<int?>(itemId);
            filter.ValueType = valueType;
            filter.ListType = riskListType;

            RiskItem item = Search(filter, null).FirstOrDefault();

            if (item != null)
            {
                ID = item.ID;
                RiskType = item.RiskType;
                MerchantID = item.MerchantID;
                InsertDate = item.InsertDate;
                Source = item.Source;
                ListType = item.ListType;
                ValueType = item.ValueType;
                Value = item.Value;
                Display = item.Display;
                Comment = item.Comment;
                CardHolder = item.CardHolder;
                CCRMID = item.CCRMID;
                BlockedItemSrc = item.BlockedItemSrc;
                Bin = item.Bin;
                Last4 = item.Last4;
                CCNumber256 = item.CCNumber256;
            }
        }

        private RiskItem(Dal.Netpay.tblBLCommon entity)
        {
            ID = entity.BL_ID;
            RiskType = RiskItemType.Item_Black_List;
            MerchantID = entity.BL_CompanyID;
            InsertDate = entity.BL_InsertDate;
            Source = (entity.BL_CompanyID == null ? RiskSource.System : RiskSource.Merchant);
            ListType = RiskListType.Black;
            ValueType = (RiskValueType)entity.BL_Type;
            Value = entity.BL_Value;
            Display = entity.BL_Value;
            Comment = entity.BL_Comment;
            BlockedItemSrc = (BlockedItemSource)entity.BL_BlockLevel;
        }

        private RiskItem(Dal.Netpay.tblFraudCcBlackList entity)
        {
            ID = entity.fraudCcBlackList_id;
            RiskType = RiskItemType.Black_List;
            MerchantID = entity.company_id;
            InsertDate = entity.fcbl_InsertDate;
            Source = (RiskSource)entity.fcbl_BlockLevel;
            ListType = RiskListType.Black;
            ValueType = RiskValueType.AccountValue1;
            Display = entity.fcbl_ccDisplay;
            //if (entity.fcbl_ccNumber256 != null) Value = Infrastructure.Security.Encryption.Decrypt(entity.fcbl_ccNumber256.ToArray());
            Comment = entity.fcbl_comment;
            Duration = entity.fcbl_UnblockDate;
            CCRMID = entity.fcbl_CCRMID;
            CCNumber256 = entity.fcbl_ccNumber256;
        }

        private RiskItem(Dal.Netpay.tblCreditCardWhitelist entity)
        {
            ID = entity.ID;
            RiskType = RiskItemType.White_List;
            MerchantID = entity.ccwl_Merchant;
            InsertDate = entity.ccwl_InsertDate;
            Source = RiskSource.Merchant;
            if (entity.ccwl_Merchant <= 0)
                Source = RiskSource.System;
            ListType = (RiskListType)entity.ccwl_Level;
            ValueType = RiskValueType.AccountValue1;
            Display = entity.ccwl_Bin + "..." + entity.ccwl_Last4.Value.ToString("D" + 4);
            //if (entity.ccwl_CardNumber256 != null) Value = Infrastructure.Security.Encryption.Decrypt(entity.ccwl_CardNumber256.ToArray());
            Comment = entity.ccwl_Comment;
            CardHolder = entity.ccwl_CardHolder;
            Duration = entity.ccwl_BurnDate;
            Bin = entity.ccwl_Bin;
            Last4 = entity.ccwl_Last4;
            CCNumber256 = entity.ccwl_CardNumber256;
        }

        public static List<RiskItem> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            if (filters == null) filters = new SearchFilters();
            if (ObjectContext.Current.User.IsInRole(UserRole.Merchant)) filters.MerchantID = Merchants.Merchant.Current.ID;
            var result = new List<RiskItem>();
            string origSortKey = null;
            if (sortAndPage != null)
            {
                sortAndPage.RowCount = 0;
                origSortKey = sortAndPage.SortKey;
            }

            // BLCommon
            IQueryable<Dal.Netpay.tblBLCommon> expCommon = null;
            bool shouldIncludeCommon = (filters.ValueType == null || filters.ValueType != RiskValueType.AccountValue1) && (filters.ListType == null || filters.ListType == RiskListType.Black) && (filters.Table == null || filters.Table.Value == TableFilter.BLCommons);
            expCommon = (from s in DataContext.Reader.tblBLCommons where shouldIncludeCommon select s);
            if ((filters.RiskTypes != null && filters.RiskTypes.Count > 0 && !filters.RiskTypes.Contains(RiskItemType.Item_Black_List)) ||
                (filters.ListType != null && filters.ListType != RiskListType.Black /* since BLCommon's list type is set only to Black*/) ||
                (filters.Account1First6 != null /* since not keeping bin number */))
                expCommon = expCommon.Where(s => false); // Exclude since Black_List_Common is not in search
            else
            {
                if (filters.ID.From != null) expCommon = expCommon.Where(s => s.BL_ID >= filters.ID.From);
                if (filters.ID.To != null) expCommon = expCommon.Where(s => s.BL_ID <= filters.ID.To);
                if (filters.InsertDate.From != null) expCommon = expCommon.Where(s => s.BL_InsertDate >= ((DateTime)filters.InsertDate.From).DayStart());
                if (filters.InsertDate.To != null) expCommon = expCommon.Where(s => s.BL_InsertDate <= ((DateTime)filters.InsertDate.To).DayEnd());
                if (filters.ValueType != null) expCommon = expCommon.Where(s => s.BL_Type == (int)filters.ValueType);
                if (filters.Value != null) expCommon = expCommon.Where(s => s.BL_Value == filters.Value);
                if (filters.MerchantID != null) expCommon = expCommon.Where(s => s.BL_CompanyID == filters.MerchantID || s.BL_BlockLevel == (byte)BlockedItemSource.Netpay);
                if (filters.Source != null) expCommon = expCommon.Where(s => s.BL_BlockLevel == (byte)filters.Source);
                if (origSortKey == "InsertDate")
                    sortAndPage.SortKey = "BL_InsertDate";
            }

            // BlackList
            IQueryable<Dal.Netpay.tblFraudCcBlackList> expBlackList = null;
            bool shouldIncludeBlackList = (filters.ValueType == null || filters.ValueType == RiskValueType.AccountValue1) && (filters.ListType == null || filters.ListType == RiskListType.Black) && (filters.Table == null || filters.Table.Value == TableFilter.FraudCcBlackLists);
            expBlackList = (from s in DataContext.Reader.tblFraudCcBlackLists where shouldIncludeBlackList select s);
            if ((filters.RiskTypes != null && filters.RiskTypes.Count > 0 && !filters.RiskTypes.Contains(RiskItemType.Black_List)) ||
                (filters.ListType != null && filters.ListType != RiskListType.Black /* since Black's list type is set only to Black*/) ||
                (filters.Account1First6 != null /* since not keeping bin number */))
                expBlackList = expBlackList.Where(s => false); // Exclude since Black_List is not in search
            else
            {
                if (filters.ID.From != null) expBlackList = expBlackList.Where(s => s.fraudCcBlackList_id >= filters.ID.From);
                if (filters.ID.To != null) expBlackList = expBlackList.Where(s => s.fraudCcBlackList_id <= filters.ID.To);
                if (filters.InsertDate.From != null) expBlackList = expBlackList.Where(s => s.fcbl_InsertDate >= ((DateTime)filters.InsertDate.From).DayStart());
                if (filters.InsertDate.To != null) expBlackList = expBlackList.Where(s => s.fcbl_InsertDate <= ((DateTime)filters.InsertDate.To).DayEnd());
                if (filters.MerchantID != null) expBlackList = expBlackList.Where(s => s.company_id == filters.MerchantID || s.fcbl_BlockLevel == (byte)BlockedItemSource.Netpay);
                if (filters.Account1Last4 != null) expBlackList = expBlackList.Where(s => s.fcbl_ccDisplay.EndsWith(filters.Account1Last4));
                if (filters.Source != null) expBlackList = expBlackList.Where(s => s.fcbl_BlockLevel == (byte)filters.Source);
                if (origSortKey == "InsertDate")
                    sortAndPage.SortKey = "fcbl_InsertDate";
            }
            // White List
            IQueryable<Dal.Netpay.tblCreditCardWhitelist> expWhiteList = null;
            bool shouldIncludeWhiteList = (filters.ValueType == null || filters.ValueType == RiskValueType.AccountValue1) && (filters.ListType == null || filters.ListType != RiskListType.Black) && (filters.Table == null || filters.Table.Value == TableFilter.CreditCardWhitelists);
            expWhiteList = (from s in DataContext.Reader.tblCreditCardWhitelists where shouldIncludeWhiteList select s);
            if (filters.RiskTypes != null && filters.RiskTypes.Count > 0 && !filters.RiskTypes.Contains(RiskItemType.White_List))
                expWhiteList = expWhiteList.Where(s => false); // Exclude since White_List is not in search
            else
            {
                int? binNumber = null;
                if (filters.Account1First6 != null)
                    binNumber = filters.Account1First6.ToNullableInt();

                if (filters.ID.From != null) expWhiteList = expWhiteList.Where(s => s.ID >= filters.ID.From);
                if (filters.ID.To != null) expWhiteList = expWhiteList.Where(s => s.ID <= filters.ID.To);
                if (filters.InsertDate.From != null) expWhiteList = expWhiteList.Where(s => s.ccwl_InsertDate >= ((DateTime)filters.InsertDate.From).DayStart());
                if (filters.InsertDate.To != null) expWhiteList = expWhiteList.Where(s => s.ccwl_InsertDate <= ((DateTime)filters.InsertDate.To).DayEnd());
                //if (filters.Value != null) exp = exp.Where(s => s.ccwl_CardNumber256 == Infrastructure.Security.Encryption.Encrypt(filters.Value));
                if (filters.ListType != null) expWhiteList = expWhiteList.Where(s => s.ccwl_Level == (byte)filters.ListType.GetValueOrDefault());
                if (binNumber != null) expWhiteList = expWhiteList.Where(s => s.ccwl_Bin == (int)binNumber);
                if (filters.Account1Last4 != null) expWhiteList = expWhiteList.Where(s => s.ccwl_Last4 == filters.Account1Last4.ToNullableInt());
                if (filters.MerchantID != null) expWhiteList = expWhiteList.Where(s => s.ccwl_Merchant == filters.MerchantID || s.ccwl_Level == (byte)BlockedItemSource.Netpay);
                if (filters.Source != null) expWhiteList = expWhiteList.Where(s => s.ccwl_Level == (byte)filters.Source);
                if (origSortKey == "InsertDate")
                    sortAndPage.SortKey = "ccwl_InsertDate";
            }

            // Concat all
            // Concat = UNION ALL (The difference between union and union-all=concat is that Union requires that the lists be checked against each other and have 
            // duplicates removed. This checking costs time and I would expect your part numbers are globally unique (appears in a single list only) anyway. 
            // Union All skips this extra checking and duplicate removal.)

            var exp = expBlackList.Select(s =>
                                            new
                                            {
                                                ID = s.fraudCcBlackList_id,
                                                RiskType = RiskItemType.Black_List,
                                                InsertDate = s.fcbl_InsertDate,
                                                MerchantID = (int?)s.company_id,
                                                Source = (RiskSource)(s.fcbl_BlockLevel == 0 ? RiskSource.System : RiskSource.Merchant),
                                                ListType = (RiskListType)RiskListType.Black,
                                                ValueType = (RiskValueType)RiskValueType.AccountValue1,
                                                Value = (string)null,
                                                Display = (string)s.fcbl_ccDisplay,
                                                Comment = s.fcbl_comment,
                                                Duration = (DateTime?)s.fcbl_UnblockDate,
                                                BlockedItemSrc = (BlockedItemSource?)null,
                                                CCRMID = (int?)s.fcbl_CCRMID,
                                                CCNumber256 = s.fcbl_ccNumber256,
                                                Bin = (int?)null,
                                                Last4 = (short?)null
                                            });

            exp = exp.Concat(expWhiteList.Select(s =>
                                        new
                                        {
                                            ID = s.ID,
                                            RiskType = RiskItemType.White_List,
                                            InsertDate = s.ccwl_InsertDate,
                                            MerchantID = (int?)s.ccwl_Merchant,
                                            Source = (RiskSource)(s.ccwl_Level == 0 ? RiskSource.System : RiskSource.Merchant),
                                            ListType = (RiskListType)s.ccwl_Level,
                                            ValueType = (RiskValueType)RiskValueType.AccountValue1,
                                            Value = (string)null,
                                            Display = (string)((s.ccwl_Bin == null ? "" : s.ccwl_Bin.ToString()) + "..." + (s.ccwl_Last4 == null ? "" : s.ccwl_Last4.ToString())),
                                            Comment = s.ccwl_Comment,
                                            Duration = (DateTime?)s.ccwl_BurnDate,
                                            BlockedItemSrc = (BlockedItemSource?)null,
                                            CCRMID = (int?)null,
                                            CCNumber256 = s.ccwl_CardNumber256,
                                            Bin = (int?)s.ccwl_Bin,
                                            Last4 = (short?)s.ccwl_Last4
                                        }));

            exp = exp.Concat(expCommon.Select(s =>
                                        new
                                        {
                                            ID = s.BL_ID,
                                            RiskType = RiskItemType.Item_Black_List,
                                            InsertDate = s.BL_InsertDate,
                                            MerchantID = (int?)s.BL_CompanyID,
                                            Source = (RiskSource)(s.BL_CompanyID == null ? RiskSource.System : RiskSource.Merchant),
                                            ListType = RiskListType.Black,
                                            ValueType = (RiskValueType)s.BL_Type,
                                            Value = s.BL_Value,
                                            Display = "", // Setting value to s.BL_Value caused exception
                                            Comment = s.BL_Comment,
                                            Duration = (DateTime?)null,
                                            BlockedItemSrc = (BlockedItemSource?)((s.BL_BlockLevel == (byte)BlockedItemSource.Merchant) ? BlockedItemSource.Merchant : ((s.BL_BlockLevel == (byte)BlockedItemSource.Netpay) ? BlockedItemSource.Netpay : BlockedItemSource.Unknown)),
                                            CCRMID = (int?)null,
                                            CCNumber256 = (System.Data.Linq.Binary)null,
                                            Bin = (int?)null,
                                            Last4 = (short?)null
                                        }));

            var items = exp.ApplySortAndPage(sortAndPage).Select(s => new RiskItem()
            {
                ID = s.ID,
                RiskType = s.RiskType,
                InsertDate = s.InsertDate,
                MerchantID = s.MerchantID,
                Source = (RiskSource)s.Source,
                ListType = (RiskListType)s.ListType,
                ValueType = (RiskValueType)s.ValueType,
                Value = s.Value,
                Display = (string)s.Display,
                Comment = s.Comment,
                Duration = s.Duration,
                BlockedItemSrc = (BlockedItemSource?)s.BlockedItemSrc,
                CCRMID = s.CCRMID,
                CCNumber256 = s.CCNumber256,
                Bin = s.Bin,
                Last4 = s.Last4
            }).ToList();

            return items;
        }


        public static List<RiskItem> Search(bool? isActive, RiskSource? source, List<RiskItemValue> items, bool isCreditCardEncrypted = false)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var result = new List<RiskItem>();
            var ccItems = items.Where(i => i.ValueType == RiskValueType.AccountValue1).ToList();
            items = items.ToList();//create copy
            items.RemoveAll(i => ccItems.Contains(i));
            if (items.Count > 0)
            {
                var exp = (from s in DataContext.Reader.tblBLCommons select s);
                if (source != null) exp = exp.Where(s => s.BL_CompanyID != null);
                var predicate = PredicateBuilder.False<Dal.Netpay.tblBLCommon>();
                foreach (var item in items)
                    predicate = predicate.Or(s => s.BL_Type == (int)item.ValueType && s.BL_Value == item.Value);
                exp = exp.Where(predicate);
                result.AddRange(exp.Select(s => new RiskItem(s)));
            }
            if (ccItems.Count > 0)
            {
                var exp = (from s in DataContext.Reader.tblFraudCcBlackLists select s);
                var predicate = PredicateBuilder.False<Dal.Netpay.tblFraudCcBlackList>();
                if (!isCreditCardEncrypted)
                {
                    // ValueType1 holds CC number (not encrypted)
                    foreach (var item in ccItems)
                        predicate = predicate.Or(s => s.fcbl_ccNumber256 == Infrastructure.Security.Encryption.Encrypt(item.Value));
                }
                else
                {
                    // ValueType1 holds base64-string of encrypted CC number (which is byte[])
                    foreach (var item in ccItems)
                        predicate = predicate.Or(s => s.fcbl_ccNumber256 == Convert.FromBase64String(item.Value));
                }
                exp = exp.Where(predicate);
                result.AddRange(exp.Take(10).Select(s => new RiskItem(s)));
            }
            if (ccItems.Count > 0)
            {
                var exp = (from s in DataContext.Reader.tblCreditCardWhitelists select s);
                var predicate = PredicateBuilder.False<Dal.Netpay.tblCreditCardWhitelist>();
                if (!isCreditCardEncrypted)
                {
                    // ValueType1 holds CC number (not encrypted)
                    foreach (var item in ccItems)
                        predicate = predicate.Or(s => s.ccwl_CardNumber256 == Infrastructure.Security.Encryption.Encrypt(item.Value));
                }
                else
                {
                    // ValueType1 holds base64-string of encrypted CC number (which is byte[])
                    foreach (var item in ccItems)
                        predicate = predicate.Or(s => s.ccwl_CardNumber256 == Convert.FromBase64String(item.Value));
                }
                exp = exp.Where(predicate);
                result.AddRange(exp.Select(s => new RiskItem(s)));
            }
            return result;
        }


        public static void UnblockCardFromTransaction(int transactionId, TransactionStatus transactionStatus)
        {
            //New check , only for admin
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            //Old check
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, null, PermissionValue.Read);

            int? merchantId = null;
            RiskItem.RiskSource source = RiskSource.System;
            if (ObjectContext.Current.User.IsInRole(UserRole.Merchant))
            {
                merchantId = Merchants.Merchant.Current.ID;
                source = RiskSource.Merchant;
            }

            var transaction = Bll.Transactions.Transaction.GetTransaction(merchantId, transactionId, transactionStatus);
            if (transaction == null)
                return;

            byte[] encCard = transaction.PaymentData.MethodInstance.GetEncyptedValue1();
            var exp = from c in DataContext.Writer.tblFraudCcBlackLists where c.fcbl_ccNumber256 == encCard select c;
            if (source == RiskSource.Merchant)
                exp = exp.Where(c => c.company_id == merchantId);

            DataContext.Writer.tblFraudCcBlackLists.DeleteAllOnSubmit(exp);
            DataContext.Writer.SubmitChanges();
        }

        public static void BlockCardFromTransaction(int transactionId, TransactionStatus transactionStatus)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Edit);

            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, null, PermissionValue.Read);

            int? merchantId = null;
            RiskItem.RiskSource source = RiskSource.System;
            if (ObjectContext.Current.User.IsInRole(UserRole.Merchant))
            {
                merchantId = Merchants.Merchant.Current.ID;
                source = RiskSource.Merchant;
            }
                
            var transaction = Bll.Transactions.Transaction.GetTransaction(merchantId, transactionId, transactionStatus);
            if (transaction == null)
                return;

            var riskItems = new List<RiskItem.RiskItemValue>()
            {
                new RiskItem.RiskItemValue()
                {
                    ValueType = RiskItem.RiskValueType.AccountValue1,
                    Value = Netpay.Infrastructure.Security.Encryption.Decrypt(transaction.PaymentData.MethodInstance.GetEncyptedValue1())
                }
            };
            RiskItem.Create(source, RiskItem.RiskListType.Black, riskItems, null, null, null, null, null, null, transaction.ReplyCode, "Blocked from transaction " + transactionId, null);
        }
        

        public static void Create(RiskSource source, RiskListType listType, List<RiskItemValue> items, string cardHolder, int? merchantIdParam, byte? cardExpMonth, short? cardExpYear, int? blockMinuetes, int? riskRuleId, string replyCode, string comment, string ip)
        {
            Create(source, listType, items, cardHolder, merchantIdParam, cardExpMonth, cardExpYear, blockMinuetes, riskRuleId, null, null, null, null, null, replyCode, comment, ip, false);
        }

        public static void CreateCreditCardBlackList(RiskSource source, int? blockMinutes, int? riskRuleId, short? paymentMethodId, string maskedNumber, string replyCode, string comment, System.Data.Linq.Binary cc256)
        {
            RiskItemValue riskItemValue = new RiskItemValue();
            riskItemValue.ValueType = RiskValueType.AccountValue1;
            riskItemValue.Value = cc256.ToBase64();

            Create(source, RiskListType.Black, new List<RiskItemValue> { riskItemValue }, null,
                   null, null, null, blockMinutes, riskRuleId,
                   paymentMethodId, maskedNumber, null, null, null, replyCode,
                   comment, null, true);
        }

        public static void CreateCreditCardWhiteList(int? binNumber, string binCountry, string cardHolder, System.Data.Linq.Binary cc256, string comment, byte? expMonth, short? expYear, short ccLast4, RiskListType listType, int? merchantIdParam, byte? paymentMethodId, string ip)
        {
            RiskItemValue riskItemValue = new RiskItemValue();
            riskItemValue.ValueType = RiskValueType.AccountValue1;
            riskItemValue.Value = cc256.ToBase64();

            Create(RiskSource.Merchant /* TBD: Meir should check if it works*/, listType,
                new List<RiskItemValue> { riskItemValue },
                cardHolder, merchantIdParam, expMonth, expYear,
                null, null, paymentMethodId, null,
                binNumber, binCountry, ccLast4,
                null, comment, ip,
                true);
        }

        /// <summary>
        /// This Function is used to create : 'Item Black List' - Located at tblBlCommon table
        ///                                   'CCBlackList' - Located at tblFraudCcBlackList table
        ///                                   'CCWhiteList' - Located at tblCreditCardWhitelist table
        /// 
        /// This function is able to get a list of 'Items' that includes several types of the enum 'RiskItemType'
        /// The function seperates the different types and creates the items needed to be created.
        /// 
        /// In case that the function gets RiskItem with ValueType of "AccountValue1" that means CC number
        /// It has 2 possibilities , The function can get an Encrypted value(used from Risk -> RiskItems -> Add) or Decrypted value(used from Transactions Management -> 'BLOCKITEMS' tab) of a CC number
        /// The parameter "isCreditCardEncrypted" makes the logic of this issue during the function Flow. 
        /// 
        /// When creating CCBlackList or CCWhiteList items from "Tranasctions Management"-> "BLOCKITEMS" tab , it uses help methods
        /// This function is invoked through helping methods that are used in order to make order in the parameters that are sent
        /// To this function the help methods are "CreateCreditCardBlackList" "CreateCreditCardWhiteList".
        /// </summary>
        /// <param name="source"></param>
        /// <param name="listType"></param>
        /// <param name="items"></param>
        /// <param name="cardHolder"></param>
        /// <param name="merchantIdParam"></param>
        /// <param name="cardExpMonth"></param>
        /// <param name="cardExpYear"></param>
        /// <param name="blockMinuetes"></param>
        /// <param name="riskRuleId"></param>
        /// <param name="paymentMethodId"></param>
        /// <param name="maskedNumber"></param>
        /// <param name="binNumber"></param>
        /// <param name="binCountryIsoCode"></param>
        /// <param name="last4"></param>
        /// <param name="replyCode"></param>
        /// <param name="comment"></param>
        /// <param name="ip"></param>
        /// <param name="isCreditCardEncrypted"></param>
        private static void Create(RiskSource source, RiskListType listType, List<RiskItemValue> items,
            string cardHolder,
            int? merchantIdParam,
            byte? cardExpMonth, short? cardExpYear,
            int? blockMinuetes, int? riskRuleId,
            short? paymentMethodId, string maskedNumber, int? binNumber,
            string binCountryIsoCode, short? last4,
            string replyCode, string comment, string ip,
            bool isCreditCardEncrypted)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            int? merchantId = null;
            if (ObjectContext.Current.User.IsInRole(UserRole.Merchant))
                merchantId = Merchants.Merchant.Current.ID;
            else
                merchantId = merchantIdParam;
            var result = Search(null, source, items, isCreditCardEncrypted);
            items.RemoveAll(i => result.Exists(n => n.Value == i.Value && n.ValueType == i.ValueType));
            var ccItems = items.Where(i => i.ValueType == RiskValueType.AccountValue1).ToList();
            items.RemoveAll(i => ccItems.Contains(i));
            if (items.Count > 0)
            {
                if (listType != RiskListType.Black) throw new Exception("white list of items other then AccountValue1 are not supported");
                foreach (var item in items)
                {
                    // Create Item BlackList
                    var newRow = new Dal.Netpay.tblBLCommon();
                    newRow.BL_InsertDate = DateTime.Now;
                    newRow.BL_CompanyID = merchantId;
                    newRow.BL_BlockLevel = (byte)source;
                    newRow.BL_Value = item.Value;
                    newRow.BL_Type = (byte)item.ValueType;
                    newRow.BL_User = Infrastructure.Security.Login.Current.UserName;
                    newRow.BL_Comment = comment;
                    DataContext.Writer.tblBLCommons.InsertOnSubmit(newRow);
                }
            }
            if (ccItems.Count > 0)
            {
                foreach (var item in ccItems)
                {
                    PaymentMethods.CreditCard card = null;
                    if (!isCreditCardEncrypted)
                        card = PaymentMethods.CreditCard.FromCardNumber(item.Value);

                    //TBD: Handle case where card is null

                    if (listType == RiskListType.Black)
                    {
                        // Get cc256 value
                        System.Data.Linq.Binary cc256 = null;
                        if (!isCreditCardEncrypted)
                            cc256 = Infrastructure.Security.Encryption.Encrypt(item.Value);
                        else
                            cc256 = Convert.FromBase64String(item.Value);

                        // Create Black-List
                        var newRow = new Dal.Netpay.tblFraudCcBlackList();
                        newRow.fcbl_InsertDate = DateTime.Now;
                        newRow.company_id = merchantId.GetValueOrDefault(0);
                        newRow.fcbl_UnblockDate = DateTime.Now.AddMinutes(blockMinuetes.GetValueOrDefault(52560000)); /*100 years*/
                        newRow.fcbl_ccNumber256 = cc256;
                        newRow.fraudCcBlackList_id = riskRuleId.GetValueOrDefault(); // Check what is this.
                        if (card != null)
                        {
                            newRow.PaymentMethod_id = (short?)card.PaymentMethodId;
                            newRow.fcbl_ccDisplay = card.MaskedNumber;
                        }
                        else
                        {
                            newRow.PaymentMethod_id = paymentMethodId;
                            newRow.fcbl_ccDisplay = maskedNumber;
                        }
                        newRow.fcbl_ReplyCode = replyCode.EmptyIfNull();
                        newRow.fcbl_BlockLevel = (byte)source;
                        newRow.fcbl_comment = comment;
                        DataContext.Writer.tblFraudCcBlackLists.InsertOnSubmit(newRow);
                    }
                    else
                    {
                        // Create White-List

                        // Get cc256 value
                        System.Data.Linq.Binary cc256 = null;
                        if (!isCreditCardEncrypted)
                            cc256 = Infrastructure.Security.Encryption.Encrypt(item.Value);
                        else
                            cc256 = Convert.FromBase64String(item.Value);

                        //Check if this merchant and cc256 already writen in the whitelist table
                        Netpay.Dal.Netpay.tblCreditCardWhitelist whiteListItem = null;
                        if (result.Count > 0)
                        {
                            // The search that was performed in result didn't narrowed the search parameters to the specific merchant.
                            // Therefore searching here.
                            whiteListItem = (from whitelisttbl in DataContext.Writer.tblCreditCardWhitelists
                                             where whitelisttbl.ccwl_CardNumber256 == cc256 && whitelisttbl.ccwl_Merchant == merchantIdParam
                                             select whitelisttbl).SingleOrDefault();
                        }

                        if (whiteListItem != null) // need only to update the status of the white list 
                        {
                            // only update ...
                            whiteListItem.ccwl_Comment = comment;
                            whiteListItem.ccwl_Level = (byte)listType;
                            whiteListItem.ccwl_InsertDate = DateTime.Now;
                            whiteListItem.ccwl_Username = Infrastructure.Security.Login.Current.UserName; 
                            DataContext.Writer.tblCreditCardWhitelists.Update(whiteListItem, whiteListItem.ID > 0);
                        }
                        else
                        {
                            var newRow = new Dal.Netpay.tblCreditCardWhitelist();
                            if (binNumber == null && !isCreditCardEncrypted)
                                newRow.ccwl_Bin = item.Value.Substring(0, 6).ToNullableInt();
                            else
                                newRow.ccwl_Bin = binNumber;
                            if (!isCreditCardEncrypted)
                                newRow.ccwl_BinCountry = Netpay.Bll.PaymentMethods.CreditCard.GetBinCountryIsoCode(item.Value);
                            else
                                newRow.ccwl_BinCountry = binCountryIsoCode;
                            if (cardHolder != null)
                                newRow.ccwl_CardHolder = cardHolder;
                            newRow.ccwl_CardNumber256 = cc256;
                            newRow.ccwl_Comment = comment;
                            if (cardExpMonth != null)
                                newRow.ccwl_ExpMonth = cardExpMonth;
                            if (cardExpYear != null)
                                newRow.ccwl_ExpYear = cardExpYear;
                            if (!isCreditCardEncrypted)
                                newRow.ccwl_Last4 = (short?)item.Value.Substring(item.Value.Length - 4).ToNullableInt();
                            else
                                newRow.ccwl_Last4 = last4;
                            newRow.ccwl_Level = (byte)listType;
                            if (merchantIdParam != null)
                                newRow.ccwl_Merchant = (int)merchantIdParam;
                            if (card != null)
                                newRow.ccwl_PaymentMethod = (byte?)card.PaymentMethodId;
                            else
                                newRow.ccwl_PaymentMethod = (byte?)paymentMethodId;
                            if (ip != null)
                                newRow.ccwl_IP = ip;
                            newRow.ccwl_InsertDate = DateTime.Now;
                            newRow.ccwl_Username = Infrastructure.Security.Login.Current.UserName;
                            DataContext.Writer.tblCreditCardWhitelists.InsertOnSubmit(newRow);
                        }
                    }
                }
            }
            DataContext.Writer.SubmitChanges();
        }


        public void Delete()
        {
            RiskItem.Delete(this.ValueType, this.ListType, new List<int> { this.ID });
        }

        public static void Delete(RiskValueType valueType, RiskListType listType, List<int> ids)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            if (ids == null || ids.Count == 0) return;
            if (valueType == RiskValueType.AccountValue1)
            {
                if (listType == RiskListType.Black) DataContext.Writer.tblFraudCcBlackLists.DeleteAllOnSubmit((from n in DataContext.Writer.tblFraudCcBlackLists where ids.Contains(n.fraudCcBlackList_id) select n));
                else DataContext.Writer.tblCreditCardWhitelists.DeleteAllOnSubmit((from n in DataContext.Writer.tblCreditCardWhitelists where ids.Contains(n.ID) select n));
            }
            else DataContext.Writer.tblBLCommons.DeleteAllOnSubmit((from n in DataContext.Writer.tblBLCommons where ids.Contains(n.BL_ID) select n));
            DataContext.Writer.SubmitChanges();
        }

        public static List<RiskItem> Search(int transactionId, TransactionStatus status, List<RiskValueType> valueTypes, bool blocksOnly)
        {
            var trans = Transactions.Transaction.GetTransaction(null, transactionId, status);
            if (trans == null) throw new Exception("Transaction not found");
            string encData1 = null, encData12 = null;
            if (trans.PaymentData != null) trans.PaymentData.MethodInstance.GetDecryptedData(out encData1, out encData12);
            var searchItems = new List<RiskItemValue>();
            foreach (var vt in valueTypes)
            {
                switch (vt)
                {
                    case RiskValueType.AccountValue1:
                        if (!string.IsNullOrEmpty(encData1)) searchItems.Add(new RiskItemValue() { ValueType = RiskValueType.AccountValue1, Value = encData1 }); break;
                    case RiskValueType.Bin:
                        if (trans.PaymentData != null) searchItems.Add(new RiskItemValue() { ValueType = RiskValueType.Bin, Value = trans.PaymentData.MethodInstance.Value1First6 }); break;
                    case RiskValueType.BinCountry:
                        if (trans.PaymentData != null) searchItems.Add(new RiskItemValue() { ValueType = RiskValueType.BinCountry, Value = trans.PaymentData.IssuerCountryIsoCode }); break;
                    case RiskValueType.Email:
                        if (trans.PayerData != null) searchItems.Add(new RiskItemValue() { ValueType = RiskValueType.Email, Value = trans.PayerData.EmailAddress }); break;
                    case RiskValueType.Phone:
                        if (trans.PayerData != null) searchItems.Add(new RiskItemValue() { ValueType = RiskValueType.Phone, Value = trans.PayerData.PhoneNumber }); break;
                    case RiskValueType.PersonalNumber:
                        if (trans.PayerData != null) searchItems.Add(new RiskItemValue() { ValueType = RiskValueType.PersonalNumber, Value = trans.PayerData.PersonalNumber }); break;
                }
            }
            return Search(true, RiskSource.Merchant, searchItems);
        }



        public string UniqueRiskItemStr
        {
            get
            {
                string uniqueRiskItemStr = GenerateUniqueRiskItemStr(this.ID, this.ValueType, this.ListType);
                return uniqueRiskItemStr;
            }
        }

        public static string GenerateUniqueRiskItemStr(int itemId, RiskValueType valueType, RiskListType riskListType)
        {
            string uniqueRiskItemStr = itemId.ToString() + ";" + valueType.ToString() + ";" + riskListType.ToString();
            return uniqueRiskItemStr;
        }

        public static string GeneratateUniqueRiskItemStr(string itemIdStr, string valueTypeStr, string riskListTypeStr)
        {
            string uniqueRiskItemStr = itemIdStr + ";" + valueTypeStr + ";" + riskListTypeStr;
            return uniqueRiskItemStr;
        }


        public static RiskItem RiskItemFromUniqueStr(string uniqueRiskItemStr)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            string[] strParts = uniqueRiskItemStr.Split(';');
            string itemIdStr = strParts[0];
            string valueTypeStr = strParts[1];
            string riskListTypeStr = strParts[2];

            int itemId = int.Parse(itemIdStr);
            RiskValueType valueType = (RiskValueType)Enum.Parse(typeof(RiskValueType), valueTypeStr);
            RiskListType riskListType = (RiskListType)Enum.Parse(typeof(RiskListType), riskListTypeStr);

            RiskItem riskItem = new RiskItem(itemId, valueType, riskListType);
            return riskItem;
        }
    }
}
