﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll
{
	public class DataObjectContext
	{
		private Netpay.Dal.DataAccess.NetpayDataContext _dataContext;
		protected Infrastructure.Security.User OriginalUserContext { get; private set; }
		public Guid CredentialsToken { get; private set; }

		public Infrastructure.Security.User User { get; private set; }
		public Netpay.Infrastructure.Domains.Domain Domain { get; private set; }
		public bool EnableTracking { get; private set; }

		public bool IsUserOfType(Infrastructure.UserType[] types, bool throwError = true) 
		{
			if (User != null) {
				if (types == null) return true;
				if (types.Contains(User.Type)) return true;
			}
			if (throwError) throw new Infrastructure.MethodAccessDeniedException();
			return false;
		}


		//protected BaseDataObject(Infrastructure.Security.User user) { User = user; Domain = User.Domain; }
		//protected BaseDataObject(Netpay.Infrastructure.Domains.Domain domain) { CredentialsToken = domain.GuestCredentials; Domain = domain; }
		public DataObjectContext(Guid credentialsToken, bool withTracking = false)
		{
			EnableTracking = withTracking;
			CredentialsToken = credentialsToken;
			Domain = Infrastructure.Domains.DomainsManager.GetDomain(credentialsToken);
			User = Infrastructure.Security.SecurityManager.GetInternalUser(credentialsToken);
		}

		internal Netpay.Dal.DataAccess.NetpayDataContext DataContext
		{
			get
			{
				if (_dataContext == null) {
					_dataContext = new Dal.DataAccess.NetpayDataContext(Domain.Sql1ConnectionString);
					_dataContext.ObjectTrackingEnabled = EnableTracking;
				}
				return _dataContext;
			}
		}

		public void Impersonate(Guid credentialsToken)
		{
			var usr = Infrastructure.Security.SecurityManager.GetInternalUser(credentialsToken);
			if (usr == null) throw new Infrastructure.NotLoggedinException();
			OriginalUserContext = User;
			User = usr;
		}
		public void StopImpersonate() { User = OriginalUserContext; OriginalUserContext = null; }
	}

	public abstract class BaseDataObject
	{
		public DataObjectContext Context { get; private set; }
		protected BaseDataObject(Guid credentialsToken) { Context = new DataObjectContext(credentialsToken, true); }
			
		protected Guid CredentialsToken { get { return Context.CredentialsToken; } }
		protected Infrastructure.Security.User User { get { return Context.User; } }
		protected Netpay.Infrastructure.Domains.Domain Domain { get { return Context.Domain; } }
		protected Netpay.Dal.DataAccess.NetpayDataContext DataContext { get{ return Context.DataContext; } }
		protected static DataObjectContext CreateContext(Guid credentialsToken, bool withTracking = false) { return new DataObjectContext(credentialsToken, withTracking); }

		public virtual void ValidateSecurity() { }
		public virtual Netpay.Infrastructure.ValidationResult Validate() { return Infrastructure.ValidationResult.Success; }

	}
}
