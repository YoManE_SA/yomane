﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure.VO;
using Netpay.CommonTypes;

namespace Netpay.Bll
{
	public static class Partner
	{
		public static Dictionary<int, string> GetPartners(string domainHost)
		{
			var domain = Infrastructure.Domains.DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			return (from a in dc.tblAffiliates select a).ToDictionary(a => a.affiliates_id, a=> a.name);
		}

		public static PartnerVO GetPartner(Guid credentialsToken, int affiliateId)
		{
			var domain = Infrastructure.Domains.DomainsManager.GetDomain(credentialsToken);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			return (from a in dc.tblAffiliates where a.affiliates_id == affiliateId select new PartnerVO(a)).SingleOrDefault();
		}

		public static List<PartnerPaymentVO> GetPayments(Guid credentialsToken, SearchFilters filters, PagingInfo pageInfo)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.Partner });
			
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var expression = from ap in dc.tblAffiliatePayments join p in dc.tblTransactionPays on ap.AFP_TransPaymentID equals p.id where ap.AFP_Affiliate_ID == user.ID orderby ap.AFP_ID descending select new { AffiliatePay = ap, Payment = p };
			if (filters != null)
			{
				if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
					expression = expression.Where(t => t.Payment != null && filters.merchantIDs.Contains(t.Payment.CompanyID));
				if (filters.dateFrom != null)
					expression = expression.Where(t => t.AffiliatePay.AFP_InsertDate >= filters.dateFrom.Value.MinTime());
				if (filters.dateTo != null)
					expression = expression.Where(t => t.AffiliatePay.AFP_InsertDate <= filters.dateTo.Value.MaxTime()); 
			}
			if (pageInfo != null) 
			{
				pageInfo.TotalItems = expression.Count();
				pageInfo.DataFunction = (credToken, pi) => GetPayments(credToken, filters, pi);
				if (pageInfo.CountOnly) return null;
				expression = expression.Skip(pageInfo.Skip).Take(pageInfo.Take);
			}

			return expression.Select(t => new PartnerPaymentVO(t.AffiliatePay, t.Payment)).ToList<PartnerPaymentVO>();
		}

		private class PaymentTotalRawData 
		{ 
			public short? paymentMethod = null; 
			public int typeID = 0; 
			public int evCount = 0; 
			public decimal evAmount = 0m; 
		}
		
		public static PartnerSettlmentVO CreatePayment(Guid credentialsToken, int payID, int merchantID, Currency currencyID, int affiliateID)
		{
			Dictionary<PaymentMethodEnum, Dictionary<TransactionAmountType, CountAmount>> pmAmounts = new Dictionary<PaymentMethodEnum, Dictionary<TransactionAmountType, CountAmount>>();
			Infrastructure.Security.User user = Infrastructure.Security.SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayUser, UserType.NetpayAdmin });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);

			StringBuilder query = new StringBuilder();
			query.Append(" Where MerchantID =" + merchantID);
			query.Append(" And SettledCurrency =" + (int)currencyID);
			query.Append(" And IsNull(SettlementID, 0) =" + payID);
			var list = dc.ExecuteQuery<PaymentTotalRawData>("SELECT PaymentMethod, TypeID, Count(tblTransactionAmount.ID) As evCount, Sum(tblTransactionAmount.SettledAmount) As evAmount" +
				" From tblTransactionAmount Left Join tblCompanyTransPass ON(tblTransactionAmount.TransPassID = tblCompanyTransPass.ID)" + query.ToString() +
				" Group By PaymentMethod, TypeID").ToList();

			foreach (var item in list)
			{
                PaymentMethodEnum pm = (PaymentMethodEnum)item.paymentMethod.GetValueOrDefault(0);
				TransactionAmountType typeid = (TransactionAmountType)item.typeID;
				if (!pmAmounts.ContainsKey(pm)) pmAmounts.Add(pm, new Dictionary<TransactionAmountType, CountAmount>());
				if (!pmAmounts[pm].ContainsKey(typeid)) pmAmounts[pm].Add(typeid, new CountAmount());
				pmAmounts[pm][typeid].Add(item.evAmount, item.evCount);
			}

			tblAffiliatePayment payment = new tblAffiliatePayment();
			payment = (from t in dc.tblAffiliatePayments where t.AFP_Affiliate_ID == affiliateID && t.AFP_TransPaymentID == payID select t).Single();
			if (payment == null)
			{
				payment.AFP_InsertDate = DateTime.Now;
				payment.AFP_CompanyID = merchantID;
				payment.AFP_TransPaymentID = payID;
				payment.AFP_PaymentCurrency = (int)currencyID;
				dc.tblAffiliatePayments.InsertOnSubmit(payment);
			}

			PartnerSettlmentVO settlment = new PartnerSettlmentVO(payment);
			return settlment;
		}
	}
}
