using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Bll.Accounts;
using Netpay.CommonTypes;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System.Data.Linq.Mapping;

namespace Netpay.Bll.Customers
{
    public enum ActiveStatus
    {
        New = 1,
        Blocked = 2,
        LoginOnly = 10,
        TestingMode = 20,
        FullyActive = 30,
        Closed = 99,
    }
    public enum PinNotificationType
    {
        Email = 1,
        SMS = 2,
        Both = 3
    }

    //[Table(Name = "Data.Customer")]
    public class Customer : Bll.Accounts.Account, IPersonalInfo
    {
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Customers.Module.Current, SecuredObjectName); } }

        public class SearchFilters
        {
            public bool? MustHaveLoginDetails;
            public List<int> AccountsIdsList;
            public List<int> CustomersIdsList;
            public Range<int?> ID;
            public Range<DateTime?> RegistrationDate;
            public List<int> RefIdentityId;
            public bool? SearchNullAppIdentity;
            public string Name;
            public ActiveStatus? Status;
            public string FacebookUserID;
            public string EmailAddress;
            public string Text;
            public string Value1Last4Text;
            public string HasBalanceWithCurrencyIso; // Will search customer with balance rows that has this currency
            public string CustomerNumber; // Will search customer with balance rows that has this currency
        }

        public static Dictionary<ActiveStatus, System.Drawing.Color> ActiveStatusColor = new Dictionary<ActiveStatus, System.Drawing.Color> {
            { ActiveStatus.New, System.Drawing.ColorTranslator.FromHtml("#6699cc") },
            { ActiveStatus.Blocked, System.Drawing.ColorTranslator.FromHtml("#ff8040") },
            { ActiveStatus.LoginOnly, System.Drawing.ColorTranslator.FromHtml("#9e6cff") },
            { ActiveStatus.TestingMode, System.Drawing.ColorTranslator.FromHtml("#ffeb65") },
            { ActiveStatus.FullyActive, System.Drawing.ColorTranslator.FromHtml("#66cc66") },
            { ActiveStatus.Closed, System.Drawing.ColorTranslator.FromHtml("#ff6666") }
        };


        public int TransFailCount { get { return Bll.Transactions.Transaction.GetCustomerFailedTransactionCount(ID); } }

        public int TransPassCount { get { return Bll.Transactions.Transaction.GetCustomerPassedTransactionCount(ID); } }

        private Transactions.TransactionsStatus _processStatus;
        protected Netpay.Dal.Netpay.Customer _entity;
        private string _profileImagePath;

        [Column(Name = "Customer_id", AutoSync = AutoSync.OnInsert, DbType = "Int NOT NULL IDENTITY", IsPrimaryKey = true, IsDbGenerated = true)]
        public int ID { get { return _entity.Customer_id; } }

        [Column(Name = "ActiveStatus_id", DbType = "TinyInt NOT NULL")]
        public ActiveStatus ActiveStatus { get { return (ActiveStatus)_entity.ActiveStatus_id; } set { _entity.ActiveStatus_id = (byte)value; } }

        [Column(Name = "ApplicationIdentity_id", DbType = "Int")]
        public int? RefIdentityID { get { return _entity.ApplicationIdentity_id; } set { _entity.ApplicationIdentity_id = value; } }

        [Column(Name = "RegistrationDate", DbType = "DateTime2(0) NOT NULL")]
        public System.DateTime RegistrationDate { get { return _entity.RegistrationDate; } }

        public string FullName { get { return PersonalInfo.MakeFullName(FirstName, LastName); } set { string a, b; PersonalInfo.SplitFullName(value, out a, out b); FirstName = a; LastName = b; } }

        [Column(Name = "FirstName", DbType = "NVarChar(50)")]
        public string FirstName { get { return _entity.FirstName; } set { _entity.FirstName = value; base.AccountName = FullName; } }

        [Column(Name = "LastName", DbType = "NVarChar(50)")]
        public string LastName { get { return _entity.LastName; } set { _entity.LastName = value; base.AccountName = FullName; } }

        [Column(Name = "PersonalNumber", DbType = "NVarChar(50)")]
        public string PersonalNumber { get { return _entity.PersonalNumber; } set { _entity.PersonalNumber = value.NullIfEmpty().Truncate(50); } }

        [Column(Name = "PhoneNumber", DbType = "VarChar(50)")]
        public string PhoneNumber { get { return _entity.PhoneNumber; } set { _entity.PhoneNumber = value.NullIfEmpty().Truncate(50); } }

        [Column(Name = "DateOfBirth", DbType = "Date")]
        public System.DateTime? DateOfBirth { get { return _entity.DateOfBirth; } set { _entity.DateOfBirth = value; } }

        [Column(Name = "EmailAddress", DbType = "NVarChar(80)")]
        public string EmailAddress { get { return _entity.EmailAddress; } set { _entity.EmailAddress = value.EmptyIfNull().Trim(); Login.EmailAddress = _entity.EmailAddress; } }

        [Column(Name = "CellNumber", DbType = "VarChar(50)")]
        public string CellNumber { get { return _entity.CellNumber; } set { _entity.CellNumber = value.NullIfEmpty().Truncate(50); } }

        [Column(Name = "FacebookUserID", DbType = "VarChar(20)")]
        public string FacebookUserID { get { return _entity.FacebookUserID; } set { _entity.FacebookUserID = value.NullIfEmpty().Truncate(20); } }

        [Column(Name = "PinNotificationTypeId", DbType = "smallint")]
        public short? PinNotificationTypeId { get { return _entity.PinNotificationTypeId; } set { _entity.PinNotificationTypeId = value; } }

        public Transactions.TransactionsStatus ProcessStatus
        {
            get
            {
                if (_processStatus == null && ID != 0)
                {
                    _processStatus = Transactions.Transaction.GetTransactionCount(Transactions.Transaction.GetTransactionCountGroup.Customer, new List<int> { ID }).FirstOrDefault();
                }

                else if (_processStatus == null && ID == 0)
                {
                    _processStatus = new Transactions.TransactionsStatus() { ApprovalCount = 0, FailCount = 0, PassCount = 0, PendingCount = 0 };
                }

                return _processStatus;
            }
        }

        public static ApplicationIdentity GetCustomerAppIdentity(int customerid)
        {
            var entity = DataContext.Reader.Customers.Where(x => x.Customer_id == customerid).SingleOrDefault();
            if (entity != null)
            {
                if (entity.ApplicationIdentity_id.HasValue)
                {
                    return ApplicationIdentity.GetIdentity(entity.ApplicationIdentity_id.Value);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }


        public ApplicationIdentity RefIdentity { get { return ApplicationIdentity.GetIdentity(_entity.ApplicationIdentity_id.GetValueOrDefault()); } }

        //[Association(Name = "FK_Account_Customer_CustomerID", Storage = "_data_Accounts", ThisKey = "Customer_id", OtherKey = "Customer_id", IsUnique = true, IsForeignKey = false, DeleteRule = "NO ACTION")]
        //[Association(Name="FK_Cart_Customer_CustomerID", Storage="_data_Carts", ThisKey="Customer_id", OtherKey="Customer_id", DeleteRule="NO ACTION")]
        //[Association(Name = "FK_Customer_ActiveStatus", Storage = "_list_ActiveStatus", ThisKey = "ActiveStatus_id", OtherKey = "ActiveStatus_id", IsForeignKey = true)]
        //[Association(Name="FK_Customer_ApplicationIdentityID", Storage="_data_ApplicationIdentity", ThisKey="ApplicationIdentity_id", OtherKey="ApplicationIdentity_id", IsForeignKey=true)]
        //[Association(Name="FK_CustomerRelation_CustomerID", Storage="_data_CustomerRelations", ThisKey="Customer_id", OtherKey="Customer_id", DeleteRule="NO ACTION")]
        //[Association(Name="FK_CustomerRelation_TargetCustomerID", Storage="_customer_s", ThisKey="Customer_id", OtherKey="TargetCustomer_id", DeleteRule="NO ACTION")]
        //[Association(Name="FK_CustomerShippingDetail_CustomerID", Storage="_data_CustomerShippingDetails", ThisKey="Customer_id", OtherKey="Customer_id", DeleteRule="NO ACTION")]

        protected Customer(Dal.Netpay.Customer entity, AccountLoadCombined accountData)
            : base(accountData)
        {
            _entity = entity;
            _profileImagePath = Bll.Accounts.Account.MapPrivatePath(AccountID, "profileImage.png");
        }

        public Customer() : base(Accounts.AccountType.Customer)
        {
            _accountEntity.AccountType_id = (byte)Accounts.AccountType.Customer;
            _entity = new Dal.Netpay.Customer();
            _entity.ActiveStatus_id = 1;
        }

        public static new Customer Current
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return Accounts.Account.CurrentObject as Customer;
            }
        }

        public static Customer Load(int? id = null)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Customer, UserRole.Partner }, SecuredObject, PermissionValue.Read);

            AccountFilter.Current.Validate(Accounts.AccountType.Customer, ref id);
            var ret = (from a in Accounts.Account.LoadQuery(DataContext.Reader)
                       join c in DataContext.Reader.Customers on a.account.Customer_id equals c.Customer_id
                       where c.Customer_id == id
                       select new { c, a }).SingleOrDefault();
            if (ret == null) return null;
            return new Customer(ret.c, ret.a);
        }

        public static Customer Load(string accountNumber)
        {
            var sf = new SearchFilters() { CustomerNumber = accountNumber };
            return Search(sf, null).SingleOrDefault();
        }

        public static Customer LoadByAccountID(int accountId)
        {
            var sf = new SearchFilters() { ID = new Range<int?> { From = accountId, To = accountId } };
            return Search(sf, null).SingleOrDefault();
        }

        public static string GetFullName(string number, string email, out byte[] profileImage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var cst = (from c in DataContext.Reader.Customers
                       join a in DataContext.Reader.Accounts on c.Customer_id equals a.Customer_id
                       where c.CustomerNumber == number && c.EmailAddress == email
                       select new { c, a.Account_id }).FirstOrDefault();
            profileImage = null;
            if (cst == null) return null;
            string fileName = Bll.Accounts.Account.MapPrivatePath(cst.Account_id, "profileImage.png");
            if (System.IO.File.Exists(fileName)) profileImage = System.IO.File.ReadAllBytes(fileName);
            return string.Format("{0} {1}", cst.c.FirstName, cst.c.LastName).Trim();
        }

        public static List<Accounts.AccountInfo> AutoComplete(string text)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            int numValue = text.ToNullableInt().GetValueOrDefault(-1);
            return (from a in DataContext.Reader.Accounts
                    join c in DataContext.Reader.Customers on a.Customer_id equals c.Customer_id
                    where c.FirstName.Contains(text) || c.LastName.Contains(text) || a.Account_id == numValue
                    select new Accounts.AccountInfo()
                    {
                        AccountID = a.Account_id,
                        TargetID = c.Customer_id,
                        AccountName = string.Format("{0} {1}", c.FirstName, c.LastName),
                        AccountType = Accounts.AccountType.Customer,
                        AccountStatus = ((ActiveStatus)c.ActiveStatus_id).ToString(),
                        StatusColor = ActiveStatusColor[(ActiveStatus)c.ActiveStatus_id]
                    }).Take(10).ToList();
        }

        public static List<Customer> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            //If the current user is customer , don't invoke security check ,
            //Beacuse security check already performed with Signature
            if (!ObjectContext.Current.IsUserOfType(UserRole.Customer, throwError: false))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Partner }, SecuredObject, PermissionValue.Read);

            var exp = (from a in Accounts.Account.LoadQuery(DataContext.Reader, false)
                       join c in DataContext.Reader.Customers on a.account.Customer_id equals c.Customer_id
                       select new { c, a });

            if (filters != null)
            {
                if (filters.MustHaveLoginDetails.HasValue && filters.MustHaveLoginDetails.Value) exp = exp.Where(x => x.a.account.LoginAccount_id.HasValue);
                if (filters.AccountsIdsList != null && filters.AccountsIdsList.Count > 0) exp = exp.Where((x) => filters.AccountsIdsList.Contains(x.a.account.Account_id));
                if (filters.CustomersIdsList != null && filters.CustomersIdsList.Count > 0) exp = exp.Where((x) => filters.CustomersIdsList.Contains(x.c.Customer_id));
                if (filters.ID.From.HasValue) exp = exp.Where((x) => x.a.account.Account_id >= filters.ID.From.Value);
                if (filters.ID.To.HasValue) exp = exp.Where((x) => x.a.account.Account_id <= filters.ID.To.Value);
                if (filters.RegistrationDate.From.HasValue) exp = exp.Where((x) => x.c.RegistrationDate >= filters.RegistrationDate.From.Value);
                if (filters.RegistrationDate.To.HasValue) exp = exp.Where((x) => x.c.RegistrationDate <= filters.RegistrationDate.To.Value.AlignToEnd());
                if (filters.RefIdentityId != null) exp = exp.Where((x) => filters.RefIdentityId.Contains(x.c.ApplicationIdentity_id.Value));
                if (filters.SearchNullAppIdentity.HasValue && filters.SearchNullAppIdentity.Value == true) exp = exp.Where((x) => x.c.ApplicationIdentity_id == null);
                if (filters.Status.HasValue) exp = exp.Where((x) => x.c.ActiveStatus_id == (byte)filters.Status.Value);
                if (!string.IsNullOrEmpty(filters.FacebookUserID)) exp = exp.Where((x) => x.c.FacebookUserID == filters.FacebookUserID);
                if (!string.IsNullOrEmpty(filters.EmailAddress)) exp = exp.Where((x) => x.c.EmailAddress == filters.EmailAddress);
                if (!string.IsNullOrEmpty(filters.Name)) exp = exp.Where((x) => x.c.FirstName.Contains(filters.Name) || x.c.LastName.Contains(filters.Name) || x.c.EmailAddress.Contains(filters.Name));
                if (!string.IsNullOrEmpty(filters.Text)) exp = exp.Where((x) => x.c.FirstName.Contains(filters.Text) || x.c.LastName.Contains(filters.Text) || x.c.EmailAddress.Contains(filters.Text) || x.c.PhoneNumber.Contains(filters.Text) || x.c.CellNumber.Contains(filters.Text));
            }

            var result = exp.ApplySortAndPage(sortAndPage).Select(x => new Customer(x.c, x.a)).ToList();
            //The two lines below are commented in the meantime in order to get better performance:
            //var pcount = Transactions.Transaction.GetTransactionCount(Transactions.Transaction.GetTransactionCountGroup.Customer, result.Select(c => c.ID).Take(2000).ToList());
            //foreach (var r in result) r._processStatus = pcount.Where(c => c.Key == r.ID).SingleOrDefault();
            return result;
        }

        public static ValidationResult IsFreeEmailAddress(string emailAddress)
        {
            if (string.IsNullOrEmpty(emailAddress)) return ValidationResult.Invalid_Email;
            if ((from c in DataContext.Reader.Customers where c.EmailAddress == emailAddress select c.Customer_id).Any())
                return ValidationResult.EmailAlreadyExist;
            return ValidationResult.Success;
        }

        public override ValidationResult Validate()
        {
            var res = base.Validate();
            if (res != ValidationResult.Success) return res;
            if (string.IsNullOrEmpty(_entity.EmailAddress)) return ValidationResult.Invalid_Email;
            if (!_entity.ApplicationIdentity_id.HasValue) return ValidationResult.Invalid_AppIdentity;
            if ((from c in DataContext.Reader.Customers
                 where c.EmailAddress == _entity.EmailAddress &&
                       c.ApplicationIdentity_id.HasValue &&
                       c.ApplicationIdentity_id.Value == _entity.ApplicationIdentity_id.Value &&
                       c.Customer_id != _entity.Customer_id
                 select c.Customer_id).Count() > 1)
                return ValidationResult.EmailAlreadyExist;
            return ValidationResult.Success;
        }

        public void Save()
        {
            if (_entity.Customer_id != 0)
            {
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Customer, UserRole.Partner }, SecuredObject, PermissionValue.Edit);

                AccountFilter.Current.Validate(Accounts.AccountType.Customer, _entity.Customer_id);
            }
            ValidationException.RaiseIfNeeded(Validate());
            //_entity.CustomerAddress_id = base.SaveAddress();
            if (_entity.Customer_id == 0)
            {
                _entity.CustomerNumber = base.GenerateNumber();
                _entity.RegistrationDate = DateTime.Now;
            }

            DataContext.Writer.Customers.Update(_entity, (_entity.Customer_id != 0));
            DataContext.Writer.SubmitChanges();
            if (_accountEntity.Customer_id == null) _accountEntity.Customer_id = _entity.Customer_id;
            base.SaveAccount();
            _profileImagePath = Bll.Accounts.Account.MapPrivatePath(_accountEntity.Account_id, "profileImage.png");
        }

        public ValidationResult Register(System.Net.IPAddress address, string pinCode = null, string password = null)
        {
            //Security check based on Save function.

            var ret = Validate();
            if (ret != ValidationResult.Success) return ret;
            if (pinCode != null && pinCode.Length != 4) return ValidationResult.PinCodeWeak;
            if (!string.IsNullOrEmpty(password) && !Login.CheckPasswordStrength(password))
                return ValidationResult.PasswordWeak;
            SetPinCode(pinCode);
            Login.EmailAddress = EmailAddress;

            //Customer Login username is always null
            Login.UserName = null;

            Save();
            if (string.IsNullOrEmpty(password)) password = Login.GeneratePassword();
            Login.SetPassword(password, address.ToString());
            //Comment for test
            SendRegistrationEmail();
            //PendingEvents.CreateForCustomer(PendingEventType.WalletRegisterEmail, _entity.Customer_id, null); // wallet register async
            if (pinCode == null)
            {
                try { SendPinCode(); }
                catch { /*ret = ValidationResult.Success*/ }
            }
            return ret;
        }

        public void Delete()
        {
            if (_entity.Customer_id == 0) return;

            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            foreach (var v in ShippingAddress.LoadForCustomer(ID)) v.Delete();
            base.DeleteAccount();
            DataContext.Writer.Customers.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }

        public static bool ResetPassword(string emailAddress, int appIdentityId, string clientIp)
        {
            //Need to have the app identity here.
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            if (emailAddress.EmptyIfNull().Trim().Length == 0) throw new ArgumentNullException("emailAddress");
            var customer = (from a in Accounts.Account.LoadQuery(DataContext.Reader, false)
                            join c in DataContext.Reader.Customers on a.account.Customer_id equals c.Customer_id
                            where (c.EmailAddress.Trim().ToLower() == emailAddress.ToLower() &&
                                   c.ApplicationIdentity_id.HasValue &&
                                   c.ApplicationIdentity_id.Value == appIdentityId)
                            select new Customer(c, a)).SingleOrDefault();

            if (customer == null) return false;


            if (Affiliates.Affiliate.Search(new Affiliates.Affiliate.SearchFilters() { Text = "Thee Card Network", IsActive = true }, null).Count != 0)
            {
                #region The below does not work if a merchant is not linked to Affiliate 

                var pTheeCardNetwork = Affiliates.Affiliate.Search(new Affiliates.Affiliate.SearchFilters() { Text = "Thee Card Network", IsActive = true }, null)[0];
                var lstAppIDs = ApplicationIdentity.LoadForAffiliate(pTheeCardNetwork.AffiliateID.Value);
                lstAppIDs = lstAppIDs.Where(p => p.IsActive).ToList();

                #endregion

                if (lstAppIDs.Count != 0 && appIdentityId == lstAppIDs[0].ID)// pAppID.ID /* Thee Card Network */)
                {
                    var activePassword = (from p in DataContext.Reader.LoginPasswords where p.LoginAccount_id == customer.LoginID orderby p.LoginPassword_id descending select new { p.PasswordEncrypted, p.EncryptionKey, p.InsertDate }).FirstOrDefault();

                    string strOldPassword = Encryption.Decrypt(activePassword.EncryptionKey, activePassword.PasswordEncrypted.ToArray());

                    if (customer.Login.SetPassword(strOldPassword, customer.PersonalNumber) != Login.ChangePasswordResult.Success)
                        return false;

                    return true;
                }
                else
                {
                    customer.Login.ResetPassword(clientIp);
                    customer.Login.SendLoginAccountPasswordResetMail();
                }
            }
            else
            {
                customer.Login.ResetPassword(clientIp);
                customer.Login.SendLoginAccountPasswordResetMail();
            }

            return true;
        }

        public void SendRegistrationEmail()
        {
            SendAccountEmail("Wallet_Register.htm");
        }

        public static Dictionary<string, string> FindFacebookUsers(List<string> facebooksIds)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from c in DataContext.Reader.Customers
                    join a in DataContext.Reader.Accounts on c.Customer_id equals a.Customer_id
                    where facebooksIds.Contains(c.FacebookUserID)
                    select new { a.AccountNumber, c.FacebookUserID }).ToDictionary(c => c.AccountNumber, c => c.FacebookUserID);
        }

        public static bool IsCustomerHasBalanceRowWithCurrencyType(int customeraccountid, string currencyisocode)
        {
            return Balance.GetStatus(customeraccountid).Any(x => x.CurrencyIso == currencyisocode);
        }

        public bool CanAcceptMerchantGroup(int? merchantGroupId)
        {
            if (!merchantGroupId.HasValue) return true;
            var identity = RefIdentity;
            if (identity == null) return true;
            var refGroups = identity.RelatedMerchantGroups;
            if (refGroups.Count == 0 || refGroups.Contains(merchantGroupId.Value)) return true;
            return false;
        }
    }
}
