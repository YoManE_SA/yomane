﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Customers
{
	public class Module : Infrastructure.Module
	{
		public const string ModuleName = "Customers";
		public override string Name { get { return ModuleName; } }
		public override decimal Version { get { return 1.0m; } }
		public static Module Current { get { return Infrastructure.Module.Get(ModuleName) as Module; } }
		protected override void OnInstall(EventArgs e)
		{
			SecuredObject.Create(this, Customer.SecuredObjectName, PermissionGroup.ReadEditDelete);
			SecuredObject.Create(this, Customer.PinCodeSecuredObjectName, PermissionGroup.ReadEditDelete);
			SecuredObject.Create(this, ShippingAddress.SecuredObjectName, PermissionGroup.ReadEditDelete);
			base.OnInstall(e);
		}

        protected override void OnActivate(EventArgs e)
        {
            EmailTemplates.RegisterTemplates(ModuleName);

            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {
            EmailTemplates.UnRegisterTemplates(ModuleName);

            base.OnDeactivate(e);
        }

        protected override void OnUninstall(EventArgs e)
		{
			Infrastructure.Security.SecuredObject.Remove(this);
			base.OnUninstall(e);
		}

        private static object GetEmailTestData(Infrastructure.Email.Template template, string param)
        {
            var cstId = param.ToNullableInt();
            if (cstId == null) cstId = Customers.Customer.Search(null, new SortAndPage(0, 1)).Select(v=> (int?)v.ID).FirstOrDefault();
            if (cstId == null) throw new Exception("Could not find testing data");
            var cst = Customer.Load(cstId.Value);
            return cst.GetEmailData();
        }

    }
}
