﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll.Customers
{
    public class Relation : BaseDataObject
    {
        public enum RelationType { Parent, Child, Couple, Family, CloseFriend, Friend }
        public class RelationEmailData
        {
            public Domain Domain;
            public Relation Request;
            public Accounts.Account SourceAccount;
            public Accounts.Account TargetAccount;
        };

        private Dal.Netpay.CustomerRelation _entity;
        private bool _isNew = true;
        private bool statusChanged = false;

        public int CustomerId { get { return _entity.Customer_id; } }
        public int TargetCustomerId { get { return _entity.TargetCustomer_id; } }
        public bool? IsActive { get { return _entity.IsActive; } }
        public System.DateTime? ConfirmationDate { get { return _entity.ConfirmationDate; } }
        public RelationType? PeopleRelationType { get { return (RelationType?) _entity.PeopleRelationType_id; } set { _entity.PeopleRelationType_id = (byte?)value; } }

        public Relation(int customerId, int targetCustomerId) 
        {
            _entity = new Dal.Netpay.CustomerRelation() { Customer_id = customerId, TargetCustomer_id = targetCustomerId };
        }

        public static void setCustomerSubAccount(int parentid, int targetid)
        {
            var entity = new Dal.Netpay.CustomerRelation();
            entity.Customer_id = parentid;
            entity.TargetCustomer_id = targetid;
            entity.PeopleRelationType_id = (byte)CustomerSubAccountRelationId.ToNullableInt();
            entity.IsActive = true;

            DataContext.Writer.CustomerRelations.InsertOnSubmit(entity);
            DataContext.Writer.SubmitChanges();
        }

        private Relation(Dal.Netpay.CustomerRelation entity)
        {
            _entity = entity;
            _isNew = false;
        }
                
        public static Dictionary<int, string> RelationTypes
        {
            get {return Netpay.Infrastructure.DataContext.Reader.PeopleRelationTypes.ToDictionary(v => (int)v.PeopleRelationType_id, v => v.Name); }
        }

        public static int CustomerSubAccountRelationId
        {
            get
            {
                return RelationTypes.Where(x => x.Value == "CustomerSubAccount").Single().Key;
            }
        }
       

        public static Relation GetRelation(int customerId, int targetCustomerId)
        {
            return (from r in Netpay.Infrastructure.DataContext.Reader.CustomerRelations 
                    where (r.Customer_id == customerId || r.Customer_id == targetCustomerId) && (r.TargetCustomer_id == customerId || r.TargetCustomer_id == targetCustomerId)  && (r.Customer_id != r.TargetCustomer_id)
                    select new Relation(r)).SingleOrDefault();
        }
        
        public static List<Relation> GetFriends(int customerId) 
        {
            return (from r in Netpay.Infrastructure.DataContext.Reader.CustomerRelations where r.IsActive == true && (r.Customer_id == customerId || r.TargetCustomer_id == customerId) select new Relation(r)).ToList();
        }

        public static List<int> GetSubAccountCustomersIds(int parentcustomerid)
        {
            return DataContext.Reader.CustomerRelations
                .Where(x => x.Customer_id == parentcustomerid &&
                            x.IsActive.HasValue &&
                            x.IsActive.Value == true &&
                            x.PeopleRelationType_id.HasValue &&
                            x.PeopleRelationType_id.Value == CustomerSubAccountRelationId)
                .Select(x => x.TargetCustomer_id)
                .ToList();
        }

        public static List<Relation> GetRequests(int customerId)
        {
            return (from r in Netpay.Infrastructure.DataContext.Reader.CustomerRelations where r.IsActive == null && (r.TargetCustomer_id == customerId) select new Relation(r)).ToList();
        }

        public void Confirm() 
        {
            _entity.ConfirmationDate = DateTime.Now;
            _entity.IsActive = true;
            statusChanged = true;
        }

        public void Reject()
        {
            _entity.ConfirmationDate = DateTime.Now;
            _entity.IsActive = false;
            statusChanged = true;
        }

        public void Reset() 
        {
            _entity.ConfirmationDate = null;
            _entity.IsActive = null;
            statusChanged = true;
        }

        public void Save() 
        {
            if (_isNew) statusChanged = true;
            DataContext.Writer.CustomerRelations.Update(_entity, !_isNew);
            DataContext.Writer.SubmitChanges();
            _isNew = false;
            if (!statusChanged) return;
            try { System.Threading.ThreadPool.QueueUserWorkItem(SendNotifications, Domain); }
            catch(Exception ex) { Logger.Log(ex); }
            statusChanged = false;
        }

        public void Delete()
        {
            DataContext.Writer.CustomerRelations.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }

        public class RelationInfo : Relation
        {
            internal RelationInfo(Dal.Netpay.CustomerRelation entity) : base(entity) { }
            public string FullName { get; set; }
            public int AccountID { get; set; }
            public string AccountNumber { get; set; }
            public string ImagePath { get; set; }
        }

        public void SendNotifications(object stateInfo)
        {
            if (stateInfo is Domain) Domain.Current = stateInfo as Domain;
            var emailData = GetEmailData();
            if (IsActive.GetValueOrDefault()) {
                if (emailData.SourceAccount != null) emailData.SourceAccount.SendAccountEmail("Relation_Confirmed.htm", emailData);
            }else if (emailData.TargetAccount != null) emailData.TargetAccount.SendAccountEmail("Relation_Request.htm", emailData);
        }

        public RelationEmailData GetEmailData()
        {
            var ids = new List<int> { _entity.Customer_id, _entity.TargetCustomer_id };
            ObjectContext.Current.Impersonate(Domain.ServiceCredentials);
            List<Accounts.Account> accounts = null;
            try { accounts = Accounts.Account.LoadAccount(Accounts.AccountType.Customer, ids); }
            finally { ObjectContext.Current.StopImpersonate(); }
            var accountSource = accounts.Where(v => v.CustomerID == _entity.Customer_id).SingleOrDefault();
            var accountTarget = accounts.Where(v => v.CustomerID == _entity.TargetCustomer_id).SingleOrDefault();
            return new RelationEmailData { Domain = Infrastructure.Domain.Current, Request = this, SourceAccount = accountSource, TargetAccount = accountTarget };
        }

        public static List<RelationInfo> GetInfo(string term, bool? activeOnly, int? refIdentity, ISortAndPage sortAndPage)
        {
            var items = (from r in Netpay.Infrastructure.DataContext.Reader.Customers where (r.CustomerNumber == term || r.FirstName.Contains(term) || r.LastName.Contains(term))
                         select new { r.ActiveStatus_id, r.Customer_id, r.data_Accounts.Account_id, r.data_Accounts.AccountNumber, r.FirstName, r.LastName, r.ApplicationIdentity_id, r.data_CustomerRelations });
            if (activeOnly.HasValue) {
                if (activeOnly.Value) items = items.Where(v => v.ActiveStatus_id == (byte)ActiveStatus.FullyActive);
                else items = items.Where(v => v.ActiveStatus_id != (byte)ActiveStatus.FullyActive);
            }
            if (refIdentity.HasValue) items = items.Where(v => v.ApplicationIdentity_id == refIdentity.Value);

            return items.ApplySortAndPage(sortAndPage).ToList().Select(i =>
                new RelationInfo(new Dal.Netpay.CustomerRelation() { Customer_id = i.Customer_id })
                {
                    FullName = PersonalInfo.MakeFullName(i.FirstName, i.LastName),
                    AccountNumber = i.AccountNumber,
                    AccountID = i.Account_id,
                    ImagePath = Bll.Accounts.Account.MapPrivatePath(i.Account_id, "profileImage.png"),
                    PeopleRelationType = (RelationType)i.data_CustomerRelations.FirstOrDefault().PeopleRelationType_id.GetValueOrDefault()
                }).ToList();
        }

        public static List<RelationInfo> GetInfo(string[] accountIds)
        {
            var items = (from r in Netpay.Infrastructure.DataContext.Reader.Customers where accountIds.Contains(r.CustomerNumber) select new { r.Customer_id, r.data_Accounts.Account_id, r.data_Accounts.AccountNumber, r.FirstName, r.LastName }).ToList();
            return items.Select(i =>
                new RelationInfo(new Dal.Netpay.CustomerRelation() { Customer_id = i.Customer_id })
                {
                    FullName = PersonalInfo.MakeFullName(i.FirstName, i.LastName),
                    AccountNumber = i.AccountNumber,
                    AccountID = i.Account_id,
                    ImagePath = Bll.Accounts.Account.MapPrivatePath(i.Account_id, "profileImage.png")

                }).ToList();
        }

        public static List<RelationInfo> GetInfo(int toCustomerId, List<Relation> list)
        {
            var cstIds = list.SelectMany(r => new int[] { r.CustomerId, r.TargetCustomerId }).Distinct().Where(v => v != toCustomerId);
            var items = (from r in Netpay.Infrastructure.DataContext.Reader.Customers where cstIds.Contains(r.Customer_id) select new { r.Customer_id, r.data_Accounts.Account_id, r.data_Accounts.AccountNumber, r.FirstName, r.LastName }).ToList();
            return items.Select(i =>
                new RelationInfo(list.Where(r => r.CustomerId == i.Customer_id || r.TargetCustomerId == i.Customer_id).FirstOrDefault()._entity) { 
                    FullName = PersonalInfo.MakeFullName(i.FirstName, i.LastName), 
                    AccountNumber = i.AccountNumber,
                    AccountID = i.Account_id,
                    ImagePath = Bll.Accounts.Account.MapPrivatePath(i.Account_id, "profileImage.png")
                }).ToList();
        }

        public static object GetEmailTestData(Infrastructure.Email.Template template, string param)
        {
            var relId = param.ToNullableInt();
            var exp = (from r in DataContext.Reader.CustomerRelations select r);
            if (relId.HasValue) exp = exp.Where(r => r.Customer_id == relId.Value);
            var rel = exp.Select(r => new Relation(r)).FirstOrDefault();
            if (rel == null ) throw new Exception("Test data wasn't found, you must have at least one relation between two customers.");
            return rel.GetEmailData();
        }
    }
}
