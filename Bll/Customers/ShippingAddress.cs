﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using Netpay.Bll.Accounts;

namespace Netpay.Bll.Customers
{
    public class ShippingAddress : AccountAddress
    {
        public const string SecuredObjectName = "ShippingAddress";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Customers.Module.Current, SecuredObjectName); } }

        protected Netpay.Dal.Netpay.CustomerShippingDetail _entity;

        public int ID { get { return _entity.CustomerShippingDetail_id; } }
        public int Customer_id { get { return _entity.Customer_id; } set { _entity.Customer_id = value; } }
        public string Title { get { return _entity.Title; } set { _entity.Title = value.NullIfEmpty().TruncEnd(50); } }
        public string Comment { get { return _entity.Comment; } set { _entity.Comment = value.NullIfEmpty().TruncEnd(250); } }
        public bool IsDefault { get { return _entity.IsDefault; } set { _entity.IsDefault = value; } }

        private ShippingAddress(Dal.Netpay.CustomerShippingDetail entity, Dal.Netpay.AccountAddress addressEntity) : base(addressEntity) { _entity = entity; }
        public ShippingAddress()
        {
            _entity = new Dal.Netpay.CustomerShippingDetail();
        }

        public new static ShippingAddress Load(int id)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Customer, UserRole.Admin }, SecuredObject, PermissionValue.Read);
            //
            var exp = (from c in DataContext.Reader.CustomerShippingDetails
                       join a in DataContext.Reader.AccountAddresses on c.AccountAddress_id equals a.AccountAddress_id into na
                       from ca in na.DefaultIfEmpty()
                       where c.CustomerShippingDetail_id == id
                       select new { c, ca });
            //exp = exp.Where(r => AccountFilter.Current.Validate(r.c.Customer_id));
            var ret = exp.SingleOrDefault();
            if (ret == null) return null;
            return new ShippingAddress(ret.c, ret.ca);
        }

        public static List<ShippingAddress> LoadForCustomer(int? customerId = null)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Customer, UserRole.Admin }, SecuredObject, PermissionValue.Read);

            AccountFilter.Current.Validate(Accounts.AccountType.Customer, ref customerId);
            return (from c in DataContext.Reader.CustomerShippingDetails
                    join a in DataContext.Reader.AccountAddresses on c.AccountAddress_id equals a.AccountAddress_id into na
                    from ca in na.DefaultIfEmpty()
                    where c.Customer_id == customerId
                    select new ShippingAddress(c, ca)).ToList();
        }

        public void Save()
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Customer, UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            AccountFilter.Current.Validate(Accounts.AccountType.Customer, _entity.Customer_id);
            //User user = Customer.validateCustomer(_entity.Customer_id);
            ValidationException.RaiseIfNeeded(Validate());
            _entity.AccountAddress_id = base.SaveAddress();
            if (IsDefault) DataContext.Writer.ExecuteCommand(string.Format("Update Data.CustomerShippingDetail Set IsDefault=0 Where Customer_id={0}", _entity.Customer_id));
            DataContext.Writer.CustomerShippingDetails.Update(_entity, (_entity.CustomerShippingDetail_id != 0));
            DataContext.Writer.SubmitChanges();
        }

        public void Delete()
        {
            if (_entity.CustomerShippingDetail_id == 0) return;

            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Customer, UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            AccountFilter.Current.Validate(Accounts.AccountType.Customer, _entity.Customer_id);
            base.DeleteAddress();
            DataContext.Writer.CustomerShippingDetails.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }
    }
}
