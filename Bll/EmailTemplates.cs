﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Netpay.Infrastructure;

namespace Netpay.Bll
{
    public class EmailTemplates
    {
        public static List<Infrastructure.Email.Template> RegisterTemplates(string modulename = null, Dictionary<string, object> customData = null)
        {
            //Account module templates - all the types of ResetPassword templates
            //include Customer, Partner, Merchant, AdminUser
            if (modulename == null || modulename == Accounts.Module.ModuleName)
            {
                //Fields
                var resetPassEmailFields = Infrastructure.Email.Template.GetFields(typeof(Infrastructure.Security.Login.ResetEmailData), 1);

                //Register templates
                Infrastructure.Email.Template.Register("Account_ResetPinCode.htm", "Account Reset Pin Code").SetCustomData(resetPassEmailFields, "AccountID", GetCustomerResetPasswordEmailTestData);
                Infrastructure.Email.Template.Register("Merchant_ResetPassword.htm", "Merchant Password Reset").SetCustomData(resetPassEmailFields, "AccountID", GetMerchantEmailTestData);
                Infrastructure.Email.Template.Register("Wallet_ResetPassword.htm", "Wallet Password Reset").SetCustomData(resetPassEmailFields, "AccountID", GetCustomerResetPasswordEmailTestData);
                Infrastructure.Email.Template.Register("Affiliate_ResetPassword.htm", "Affiliate Password Reset").SetCustomData(resetPassEmailFields, "AccountID", GetAffiliateEmailTestData);
                Infrastructure.Email.Template.Register("AdminUser_ResetPassword.htm", "AdminUser Password Reset").SetCustomData(resetPassEmailFields, "ID", GetAdminUserEmailTestData);
            }

            //Merchant registration template
            if (modulename == null || modulename == Merchants.Registrations.Module.ModuleName)
            {
                var fields = Infrastructure.Email.Template.GetFields(typeof(Bll.Merchants.Registration.RegistrationEmailData), 1);
                Infrastructure.Email.Template.Register("Merchant_Register.htm","Merchant Registration").SetCustomData(fields , "FirstName", Bll.Merchants.Registration.GetEmailTestData);
            }

            //Balance templates
            if (modulename == null || modulename == Accounts.BalanceModule.Module.ModuleName)
            {
                var fields2 = Infrastructure.Email.Template.GetFields(typeof(Accounts.BalanceRequest.BalanceRequestEmailData), 1);
                Infrastructure.Email.Template.Register("Balance_Request.htm", "Balance Request").SetCustomData(fields2, "Request ID", Accounts.BalanceRequest.GetEmailTestData);
                Infrastructure.Email.Template.Register("Balance_RequestConfirmed.htm", "Balance Request Confirmed").SetCustomData(fields2, "Request ID", Accounts.BalanceRequest.GetEmailTestData);

            }
            //Customer templates
            if (modulename == null || modulename == Customers.Module.ModuleName)
            {
                var fields = Infrastructure.Email.Template.GetFields(new { Domain = (Infrastructure.Domain)null, Account = (Customers.Customer)null, AccountPassword = (string)null, AccountPinCode = (string)null }.GetType(), 1);
                Infrastructure.Email.Template.Register("Wallet_Register.htm", "Wallet Registration").SetCustomData(fields, "CustomerID", GetCustomerEmailTestData);

                var fields2 = Infrastructure.Email.Template.GetFields(typeof(Customers.Relation.RelationEmailData), 1);
                Infrastructure.Email.Template.Register("Relation_Request.htm", "Relation Request").SetCustomData(fields2, "RelationId", Customers.Relation.GetEmailTestData);
                Infrastructure.Email.Template.Register("Relation_Confirmed.htm", "Relation Confirmed").SetCustomData(fields2, "RelationId", Customers.Relation.GetEmailTestData);
            }
            //Netpay connector templates
            if (modulename == null || modulename == "NetpayConnector") //That module name should be the exact module name of Netpay.Process.NetpayConnector.Module.ModuleName 
            {
                object emailTestData;
                object fields;
                if (customData != null && customData.TryGetValue("EmailTestData", out emailTestData) && customData.TryGetValue("Fields", out fields))
                {
                    List<string> fieldsList = (List<string>)fields;
                    Infrastructure.Email.Template.EmailTestData testData = (Infrastructure.Email.Template.EmailTestData)emailTestData;
                    Infrastructure.Email.Template.Register("CustomerNotify_ApprovedTrans.htm", "CustomerNotify ApprovedTrans").SetCustomData(fieldsList, "TransID", testData);
                    Infrastructure.Email.Template.Register("MerchantNotify_ApprovedTrans.htm", "MerchantNotify ApprovedTrans").SetCustomData(fieldsList, "TransID", testData);
                    Infrastructure.Email.Template.Register("MerchantNotify_DeclinedTrans.htm", "MerchantNotify DeclinedTrans").SetCustomData(fieldsList, "TransID", testData);
                    Infrastructure.Email.Template.Register("PartnerNotify_ApprovedTrans.htm", "PartnerNotify ApprovedTrans").SetCustomData(fieldsList, "TransID", testData);
                    Infrastructure.Email.Template.Register("MerchantNotify_RiskMultipleCardsOnEmail.htm", "MerchantNotify RiskMultipleCardsOnEmail").SetCustomData(fieldsList, "TransID", testData);
                    Infrastructure.Email.Template.Register("MerchantNotify_Denied.htm", "MerchantNotify Denied").SetCustomData(fieldsList, "TransID", testData);
                    Infrastructure.Email.Template.Register("MerchantNotify_Clarify.htm", "MerchantNotify Clarify").SetCustomData(fieldsList, "TransID", testData);
                    Infrastructure.Email.Template.Register("CartSection.htm", "CartSection").SetCustomData(fieldsList, "TransID", testData);
                    Infrastructure.Email.Template.Register("CartItem.htm", "CartItem").SetCustomData(fieldsList, "TransID", testData);
                }
            }

            return Infrastructure.Email.Template.Items;
        }

        public static void UnRegisterTemplates(string modulename = null)
        {
            //Unregister accounts reset password templates
            if (modulename == Accounts.Module.ModuleName)
            {
                Infrastructure.Email.Template.UnRegister("Merchant_ResetPassword.htm");
                Infrastructure.Email.Template.UnRegister("Wallet_ResetPassword.htm");
                Infrastructure.Email.Template.UnRegister("Affiliate_ResetPassword.htm");
                Infrastructure.Email.Template.UnRegister("AdminUser_ResetPassword.htm");
            }
            //Unregister merchant registration
            if (modulename == Merchants.Registrations.Module.ModuleName)
            {
                Infrastructure.Email.Template.UnRegister("Merchant_Register.htm");
            }

            //Unregister balance templates
            if (modulename == Accounts.BalanceModule.Module.ModuleName)
            {
                Infrastructure.Email.Template.UnRegister("Balance_Request.htm");
                Infrastructure.Email.Template.UnRegister("Balance_RequestConfirmed.htm");
            }
            //Unregister customer templates
            if (modulename == Customers.Module.ModuleName)
            {
                Infrastructure.Email.Template.UnRegister("Wallet_Register.htm");
                Infrastructure.Email.Template.UnRegister("Relation_Request.htm");
                Infrastructure.Email.Template.UnRegister("Relation_Confirmed.htm");
            }
        }

        private static object GetCustomerResetPasswordEmailTestData(Infrastructure.Email.Template template, string param)
        {
            var cstId = param.ToNullableInt();
            if (cstId == null) cstId = Customers.Customer.Search(new Customers.Customer.SearchFilters() {MustHaveLoginDetails=true }, new SortAndPage(0, 1)).Select(v => (int?)v.ID).FirstOrDefault();
            if (cstId == null) throw new Exception("Test data wasn't found, you must have at least one customer with login details at customers-management list.");
            var cst = Customers.Customer.Load(cstId.Value);
            if (cst.Login == null) throw new Exception("Test data wasn't found, you must have at least one customer with login details at customers-management list.");
            return cst.Login.GetEmailData();
        }

        private static object GetCustomerEmailTestData(Infrastructure.Email.Template template, string param)
        {
            var cstId = param.ToNullableInt();
            if (cstId == null) cstId = Customers.Customer.Search(new Customers.Customer.SearchFilters() { MustHaveLoginDetails = true }, new SortAndPage(0, 1)).Select(v => (int?)v.ID).FirstOrDefault();
            if (cstId == null) throw new Exception("Test data wasn't found, you must have at least one customer with login details at customers-management list.");
            var cst = Customers.Customer.Load(cstId.Value);
            if (cst.Login == null) throw new Exception("Test data wasn't found, you must have at least one customer with login details at customers-management list.");
            return cst.GetEmailData();
        }

        private static object GetMerchantEmailTestData(Infrastructure.Email.Template template, string param)
        {
            var mrcId = param.ToNullableInt();
            if (mrcId == null) mrcId = Merchants.Merchant.Search(new Merchants.Merchant.SearchFilters() {MustHaveLoginDetails=true }, new SortAndPage(0, 1)).Select(v => (int?)v.ID).FirstOrDefault();
            if (mrcId == null) throw new Exception("Test data wasn't found, you must have at least one customer with login details at customers-management list.");
            var mrc = Merchants.Merchant.Load(mrcId.Value);
            if (mrc.Login == null) throw new Exception("Test data wasn't found, you must have at least one customer with login details at customers-management list.");
            return mrc.Login.GetEmailData();
        }

        private static object GetAffiliateEmailTestData(Infrastructure.Email.Template template, string param)
        {
            var affId = param.ToNullableInt();
            if (affId == null) affId = Affiliates.Affiliate.Search(new Affiliates.Affiliate.SearchFilters() {MustHaveLoginDetails=true }, new SortAndPage(0, 1)).Select(v => (int?)v.ID).FirstOrDefault();
            if (affId == null) throw new Exception("Test data wasn't found, you must have at least one affiliate with login details at affilates-management list.");
            var aff = Affiliates.Affiliate.Load(affId.Value);
            if (aff.Login == null) throw new Exception("Test data wasn't found, you must have at least one affiliate with login details at affilates-management list.");
            return aff.Login.GetEmailData();
        }

        private static object GetAdminUserEmailTestData(Infrastructure.Email.Template template, string param)
        {
            var auId = param.ToNullableInt();
            if (auId == null) auId = Infrastructure.Security.AdminUser.Search(new Infrastructure.Security.AdminUser.SearchFilters() {MustHaveLoginDetails=true}, new SortAndPage(0, 1)).Select(v => (int?)v.ID).FirstOrDefault();
            if (auId == null) throw new Exception("Test data wasn't found, you must have at least one admin-user with login details at Settings-Admin Users list.");
            var au = Infrastructure.Security.AdminUser.Load(auId.Value);
            if (au.Login == null) throw new Exception("Test data wasn't found, you must have at least one admin-user with login details at Settings-Admin Users list.");
            return au.Login.GetEmailData();
        }
    }
}