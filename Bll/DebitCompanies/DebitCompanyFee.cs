﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Netpay.Infrastructure;
using Netpay.Bll.PaymentMethods;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.DebitCompanies
{
    public class DebitCompanyFee : BaseDataObject
    {
        private Dal.Netpay.tblDebitCompanyFee _entity;

        public int DcfID { get { return _entity.DCF_ID; } }

        public int DcfDebitCompanyId { get { return _entity.DCF_DebitCompanyID; } set { _entity.DCF_DebitCompanyID = value; } }

        public byte DcfCurrencyId { get { return _entity.DCF_CurrencyID; } set { _entity.DCF_CurrencyID = value; } }

        public short DcfPaymentMethod { get { return _entity.DCF_PaymentMethod; } set { _entity.DCF_PaymentMethod = value; } }

        public string DcfTerminalNumber { get { return _entity.DCF_TerminalNumber; } set { _entity.DCF_TerminalNumber = value; } }

        public decimal DcfFixedFee { get { return _entity.DCF_FixedFee; } set { _entity.DCF_FixedFee = value; } }

        public decimal DcfRefundFixedFee { get { return _entity.DCF_RefundFixedFee; } set { _entity.DCF_RefundFixedFee = value; } }

        public decimal DcfPercentFee { get { return _entity.DCF_PercentFee; } set { _entity.DCF_PercentFee = value; } }

        public decimal DcfApproveFixedFee { get { return _entity.DCF_ApproveFixedFee; } set { _entity.DCF_ApproveFixedFee = value; } }

        public decimal DcfClarificationFee { get { return _entity.DCF_ClarificationFee; } set { _entity.DCF_ClarificationFee = value; } }

        public decimal DcfCbFixedFee { get { return _entity.DCF_CBFixedFee; } set { _entity.DCF_CBFixedFee = value; } }

        public decimal DcfFailFixedFee { get { return _entity.DCF_FailFixedFee; } set { _entity.DCF_FailFixedFee = value; } }

        public decimal DcfRegisterFee { get { return _entity.DCF_RegisterFee; } set { _entity.DCF_RegisterFee = value; } }

        public decimal DcfYearFee { get { return _entity.DCF_YearFee; } set { _entity.DCF_YearFee = value; } }

        public byte DcfStatIncDays { get { return _entity.DCF_StatIncDays; } set { _entity.DCF_StatIncDays = value; } }

        public string DcfPayTransDays { get { return _entity.DCF_PayTransDays; } set { _entity.DCF_PayTransDays = value; } }

        public string DcfPayInDays { get { return _entity.DCF_PayINDays; } set { _entity.DCF_PayINDays = value; } }

        public byte? DcfFixedCurrency { get { return _entity.DCF_FixedCurrency; } set { _entity.DCF_FixedCurrency = value; } }

        public byte? DcfChbCurrency { get { return _entity.DCF_CHBCurrency; } set { _entity.DCF_CHBCurrency = value; } }

        public decimal? DcfMinPrecFee { get { return _entity.DCF_MinPrecFee; } set { _entity.DCF_MinPrecFee = value; } }

        public decimal? DcfMaxPrecFee { get { return _entity.DCF_MaxPrecFee; } set { _entity.DCF_MaxPrecFee = value; } }


        //Private constructor with parameter
        private DebitCompanyFee(Dal.Netpay.tblDebitCompanyFee entity)
        {
            _entity = entity;
        }

        //Public constructor 
        public DebitCompanyFee()
        {
            _entity = new Dal.Netpay.tblDebitCompanyFee();
            DcfTerminalNumber = "";
        }


        public static List<DebitCompanyFee> GetFeesListByDebitCompanyId(int? debitcompanyid)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, DebitCompany.SecuredObject, PermissionValue.Read);


            if (debitcompanyid != null)
            {

                {
                    var list = (from dcf in DataContext.Reader.tblDebitCompanyFees
                                where dcf.DCF_DebitCompanyID == debitcompanyid && dcf.DCF_TerminalNumber == ""
                                select dcf);

                    if (list.Count() > 0)
                    {
                        return list.Select(x => new DebitCompanyFee(x)).ToList();
                    }
                    else
                    {
                        return new List<DebitCompanyFee>();
                    }
                }
            }

            return new List<DebitCompanyFee>();
        }

        public static List<DebitCompanyFee> GetFeesListByTerminalNumber(string terminalnumber)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, DebitCompany.SecuredObject, PermissionValue.Read);

            if (terminalnumber != "" && terminalnumber != null)
            {
                return (from dcf in DataContext.Reader.tblDebitCompanyFees
                        where dcf.DCF_TerminalNumber == terminalnumber
                        select dcf).Select(x => new DebitCompanyFee(x)).ToList();
            }
            return new List<DebitCompanyFee>();
        }

        public static DebitCompanyFee Load(int dcfid)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, DebitCompany.SecuredObject, PermissionValue.Read);


            if (dcfid > 0)
            {
                return (from dcf in DataContext.Reader.tblDebitCompanyFees
                        where dcf.DCF_ID == dcfid
                        select new DebitCompanyFee(dcf)).SingleOrDefault();
            }
            return null;
        }

        public void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, DebitCompany.SecuredObject, PermissionValue.Edit);


            DataContext.Writer.tblDebitCompanyFees.Update(_entity, (_entity.DCF_ID > 0));
            DataContext.Writer.SubmitChanges();
        }

        public void Delete()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, DebitCompany.SecuredObject, PermissionValue.Delete);

            if (_entity.DCF_ID == 0) return;
            DataContext.Writer.tblDebitCompanyFees.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }
    }
}
