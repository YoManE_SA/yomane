﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll
{
    public class TerminalsWithZeroFees : BaseDataObject
    {
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(DebitCompanies.TerminalWithoutFees.Module.Current, SecuredObjectName); } }

        public class SearchFilters
        {
            public Range<int?> ccfId;
            public bool TransFee;
            public bool ClearingFee;
            public bool PreAuthFee;
            public bool RefundFee;
            public bool CopyRFee;
            public bool CHBFee;
            public bool FailFee;
        }

        public int CcfId { get; private set; }

        public string MerchantName { get; private set; }

        public int MerchantId { get; private set; }

        public short PaymentMethodID { get; private set; }

        public string PaymentMethodName { get; private set; }

        public string CurrencyISOCode { get; private set; }

        public string CCBinList { get; private set; }

        public int DebitCompanyID { get; private set; }

        public string DebitCompanyName { get; private set; }

        public string TerminalName { get; private set; }

        public string TerminalNumber { get; private set; }

        public decimal TransFee { get; private set; }

        public decimal ClearingFee { get; private set; }

        public decimal PreAuthFee { get; private set; }

        public decimal RefundFee { get; private set; }

        public decimal CopyRequestFee { get; private set; }

        public decimal ChbFee { get; private set; }

        public decimal FailFee { get; private set; }

        public decimal PartialRefundFee { get; private set; }

        public decimal FraudRefundFee { get; private set; }

        public int ProcessTerminalId { get; private set; }

        private TerminalsWithZeroFees()
        {

        }

        public static List<TerminalsWithZeroFees> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var exp = from ccf in DataContext.Reader.tblCompanyCreditFees
                      join c in DataContext.Reader.tblCompanies on ccf.CCF_CompanyID equals c.ID
                      join cu in DataContext.Reader.CurrencyLists on ccf.CCF_CurrencyID equals cu.CurrencyID
                      join pm in DataContext.Reader.PaymentMethods on ccf.CCF_PaymentMethod equals pm.PaymentMethod_id
                      join ccft in DataContext.Reader.tblCompanyCreditFeesTerminals on ccf.CCF_ID equals ccft.CCFT_CCF_ID
                      join dt in DataContext.Reader.tblDebitTerminals on ccft.CCFT_Terminal equals dt.terminalNumber
                      join dc in DataContext.Reader.tblDebitCompanies on dt.DebitCompany equals dc.DebitCompany_ID
                      where c.ActiveStatus == 30 &&
                            dt.isNetpayTerminal == true &&
                            dt.isActive == true &&
                            dc.dc_isActive == true &&
                            ccf.CCF_IsDisabled == false &&
                            (ccf.CCF_FixedFee == 0 || ccf.CCF_PercentFee == 0 || ccf.CCF_ApproveFixedFee == 0 ||
                            ccf.CCF_RefundFixedFee == 0 || ccf.CCF_ClarificationFee == 0 || ccf.CCF_CBFixedFee == 0 ||
                            ccf.CCF_FailFixedFee == 0 || ccf.CCF_PartialRefundFixedFee == 0 || ccf.CCF_FraudRefundFixedFee == 0)
                      select new TerminalsWithZeroFees()
                      {
                          CcfId = ccf.CCF_ID,
                          MerchantName = c.CompanyName,
                          MerchantId = c.ID,
                          PaymentMethodID = pm.PaymentMethod_id,
                          PaymentMethodName = pm.Name,
                          CurrencyISOCode = cu.CurrencyISOCode,
                          CCBinList = ccf.CCF_ListBINs,
                          DebitCompanyID = dc.DebitCompany_ID,
                          DebitCompanyName = dc.dc_name,
                          TerminalName = dt.dt_name,
                          TerminalNumber = dt.terminalNumber,
                          TransFee = ccf.CCF_FixedFee,
                          ClearingFee = ccf.CCF_PercentFee,
                          PreAuthFee = ccf.CCF_ApproveFixedFee,
                          RefundFee = ccf.CCF_RefundFixedFee,
                          CopyRequestFee = ccf.CCF_ClarificationFee,
                          ChbFee = ccf.CCF_CBFixedFee,
                          FailFee = ccf.CCF_FailFixedFee,
                          PartialRefundFee = ccf.CCF_PartialRefundFixedFee,
                          FraudRefundFee = ccf.CCF_FraudRefundFixedFee,
                          ProcessTerminalId = ccft.CCFT_ID
                      };
            
            if (filters != null)
            {
                if (filters.ccfId.From.HasValue) exp = exp.Where(p => p.CcfId >= filters.ccfId.From.Value);
                if (filters.ccfId.To.HasValue) exp = exp.Where(p => p.CcfId <= filters.ccfId.To.Value);
                if (filters.TransFee) exp = exp.Where(p => p.TransFee == 0);
                if (filters.ClearingFee) exp = exp.Where(p => p.ClearingFee == 0);
                if (filters.PreAuthFee) exp = exp.Where(p => p.PreAuthFee == 0);
                if (filters.RefundFee) exp = exp.Where(p => p.RefundFee == 0);
                if (filters.CopyRFee) exp = exp.Where(p => p.CopyRequestFee == 0);
                if (filters.CHBFee) exp = exp.Where(p => p.ChbFee == 0);
                if (filters.FailFee) exp = exp.Where(p => p.FailFee == 0);
            }
            return exp.ApplySortAndPage(sortAndPage).ToList();
        }

        public static TerminalsWithZeroFees Load(int id)
        {
            var item = Search(new SearchFilters() { ccfId = Range<int?>.FromValue(id) }, null);
            return item.FirstOrDefault();
        }

        public Bll.Merchants.ProcessTerminals ProcessTerminal
        {
            get
            {
                if (ProcessTerminalId == 0)
                    return null;
                return Bll.Merchants.ProcessTerminals.LoadForMerchantByProcessTerminalId(MerchantId, CcfId);
            }
        }
    }
}
