﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Netpay.Infrastructure;

namespace Netpay.Bll.DebitCompanies
{
    public class DebitCompanyRule : BaseDataObject
    {
        private Dal.Netpay.tblDebitRule _entity;

        public int ID { get { return _entity.ID; } set { _entity.ID = value; } }

        public bool IsActive { get { return _entity.dr_IsActive; } set { _entity.dr_IsActive = value; } }

        public int DebitCompanyId { get { return _entity.dr_DebitCompany; } set { _entity.dr_DebitCompany = value; } }

        public string ReplyCodes { get { return _entity.dr_ReplyCodes; } set { _entity.dr_ReplyCodes = value; } }

        public string NotifyUsers { get { return _entity.dr_NotifyUsers; } set { _entity.dr_NotifyUsers = value; } }

        public int AttemptCount { get { return _entity.dr_AttemptCount; } set { _entity.dr_AttemptCount = value; } }

        public int FailCount { get { return _entity.dr_FailCount; } set { _entity.dr_FailCount = value; } }

        public bool IsAutoDisable { get { return _entity.dr_IsAutoDisable; } set { _entity.dr_IsAutoDisable = value; } }

        public bool IsAutoEnable { get { return _entity.dr_IsAutoEnable; } set { _entity.dr_IsAutoEnable = value; } }

        public int AutoEnableMinutes { get { return _entity.dr_AutoEnableMinutes; } set { _entity.dr_AutoEnableMinutes = value; } }

        public int AutoEnableAttempts { get { return _entity.dr_AutoEnableAttempts; } set { _entity.dr_AutoEnableAttempts = value; } }

        public DateTime LastFailDate { get { return _entity.dr_LastFailDate; } set { _entity.dr_LastFailDate = value; } }

        public DateTime LastUnblockDate { get { return _entity.dr_LastUnblockDate; } set { _entity.dr_LastUnblockDate = value; } }

        public string NotifyUsersSMS { get { return _entity.dr_NotifyUsersSMS; } set { _entity.dr_NotifyUsersSMS = value; } }

        public int Rating { get { return _entity.dr_Rating; } set { _entity.dr_Rating = value; } }

        public static List<string> NotifyByMailUsers
        {
            get
            {
                return (from au in DataContext.Reader.AdminUsers
                        where au.FullName != "" && au.NotifyEmail != null && au.IsActive == true
                        select au.FullName).ToList();
            }
        }

        public static List<string> NotifyBySmsUsers
        {
            get
            {
                return (from au in DataContext.Reader.AdminUsers
                        //add to notify cell phone check if its a real mobile phone number
                        where au.FullName != "" && au.NotifyCellPhone != null && au.NotifyCellPhone!="" && au.IsActive == true
                        select au.FullName).ToList();
            }
        }

        private DebitCompanyRule(Dal.Netpay.tblDebitRule entity)
        {
            _entity = entity;
        }

        public DebitCompanyRule()
        {
            _entity = new Dal.Netpay.tblDebitRule();
        }

        public static List<DebitCompanyRule> GetRulesByDebitCompanyId(int? debitcompanyid)
        {
            if (debitcompanyid != null)
            {
                var exp = (from dr in DataContext.Reader.tblDebitRules
                           where dr.dr_DebitCompany == debitcompanyid
                           select dr);
                return exp.Select(x => new DebitCompanyRule(x)).ToList();
            }
            return new List<DebitCompanyRule>();
        }

        public static DebitCompanyRule Load(int debitcompanyruleid)
        {
            if (debitcompanyruleid > 0)
            {
                return (from dr in DataContext.Reader.tblDebitRules
                        where dr.ID == debitcompanyruleid
                        select new DebitCompanyRule(dr)).SingleOrDefault();
            }
            return null;
        }

        public void Save()
        {
            DataContext.Writer.tblDebitRules.Update(_entity, (_entity.ID > 0));
            DataContext.Writer.SubmitChanges();
        }

        public void Delete()
        {
            if (_entity.ID == 0) return;
            DataContext.Writer.tblDebitRules.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }
    }
}
