﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web;
using Netpay.Infrastructure;

//SELECT 0 As BA_ID, 'None' As BA_Title, 0 As SortOrder, 'None' As BA_BankName, 'None' As BA_AccountName  
//UNION SELECT BA_ID, (BA_BankName + ': ' + BA_AccountName + '- ' + BA_AccountNumber) As BA_Title, 1 As SortOrder, BA_BankName, BA_AccountName
// FROM tblBankAccounts 
//ORDER BY SortOrder, BA_BankName, BA_AccountName" />

namespace Netpay.Bll.DebitCompanies
{
    public class BankAccountDetails : BaseDataObject
    {
        private Dal.Netpay.tblBankAccount _entity;

        public int BA_ID { get { return _entity.BA_ID; }  }

        public DateTime? BA_InsDate { get { return _entity.BA_InsDate; } }

        public DateTime? BA_Update { get { return _entity.BA_Update; } }

        public string BA_BankName { get { return _entity.BA_BankName; } }

        public string BA_AccountName { get { return _entity.BA_AccountName; } }

        public string BA_AccountNumber { get { return _entity.BA_AccountNumber; } }

        public int BA_Currency { get { return _entity.BA_Currency; } }

        public string Title
        {
            get
            {
                return BA_BankName + ":" + BA_AccountName + "- " + BA_AccountNumber;
            }
        }

        private BankAccountDetails(Dal.Netpay.tblBankAccount entity)
        {
            _entity = entity;
        }

        public static List<BankAccountDetails> Cache
        {
            get
            {
                return Domain.Current.GetCachData("BankAccountDetails", () =>
                {
                    return new Domain.CachData((from ba in DataContext.Reader.tblBankAccounts select new BankAccountDetails(ba)).ToList(), DateTime.Now.AddHours(6));
                }) as List<BankAccountDetails>;
            }
        }
    }
}
