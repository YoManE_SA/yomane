﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.DebitCompanies
{
    public class ResponseCode : BaseDataObject
    {
        public const string SecuredObjectName = "ResponseCodes";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(DebitCompanies.Module.Current, SecuredObjectName); } }

        public class SearchFilters
        {
            public Range<int?> ID;
            public int? DebitCompanyID;
            public string Text;
            public byte? FailSource;
        }

        private Dal.Netpay.tblDebitCompanyCode _entity;

        public int ID { get { return _entity.ID; } set { _entity.ID = value; } }
        public short DebitCompanyID { get { return _entity.DebitCompanyID; } set { _entity.DebitCompanyID = value; } }
        public string Code { get { return _entity.Code; } set { _entity.Code = value; } }
        public string Description { get { return _entity.DescriptionOriginal; } set { _entity.DescriptionOriginal = value; } }
        public string DescriptionMerchantHeb { get { return _entity.DescriptionMerchantHeb; } set { _entity.DescriptionMerchantHeb = value; } }
        public string DescriptionMerchantEng { get { return _entity.DescriptionMerchantEng; } set { _entity.DescriptionMerchantEng = value; } }
        public string DescriptionCustomerHeb { get { return _entity.DescriptionCustomerHeb; } set { _entity.DescriptionCustomerHeb = value; } }
        public string DescriptionCustomerEng { get { return _entity.DescriptionCustomerEng; } set { _entity.DescriptionCustomerEng = value; } }
        public bool ChargeFail { get { return _entity.ChargeFail; } set { _entity.ChargeFail = value; } }
        public int RecurringAttemptsCharge { get { return _entity.RecurringAttemptsCharge; } set { _entity.RecurringAttemptsCharge = value; } }
        public int RecurringAttemptsSeries { get { return _entity.RecurringAttemptsSeries; } set { _entity.RecurringAttemptsSeries = value; } }
        public byte FailSource { get { return _entity.FailSource; } set { _entity.FailSource = value; } }
        public string LocalError { get { return _entity.LocalError; } set { _entity.LocalError = value; } }
        public bool BlockCC { get { return _entity.BlockCC; } set { _entity.BlockCC = value; } }
        public bool BlockMail { get { return _entity.BlockMail; } set { _entity.BlockMail = value; } }
        public bool IsCascade { get { return _entity.IsCascade; } set { _entity.IsCascade = value; } }

        public static bool IsAnyReplyCodeForDebitCompany(int id)
        {
            return DataContext.Reader.tblDebitCompanyCodes.Any(x => x.DebitCompanyID == id);
        }

        public List<KeyValuePair<string, string>> LocalErrorList
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                var sf = new Bll.DebitCompanies.ResponseCode.SearchFilters();
                int? number = 1;
                sf.DebitCompanyID = number;
                var exp = Bll.DebitCompanies.ResponseCode.Search(sf, null)
                       .OrderBy(x => x.Code)
                       .Select(x => new KeyValuePair<string, string>(x.Code + " - " + x.Description, x.Code)).ToList();

                return exp;
            }
        }

        private ResponseCode(Dal.Netpay.tblDebitCompanyCode entity) { _entity = entity; }
        public ResponseCode()
        {
            //No need for security check , there no logic of adding response code.
            _entity = new Dal.Netpay.tblDebitCompanyCode();
        }

        public static List<ResponseCode> Load(List<int> ids)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from s in DataContext.Reader.tblDebitCompanyCodes where ids.Contains(s.ID) select new ResponseCode(s)).ToList();
        }

        public static ResponseCode GetItem(int debitCompanyId, string code)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var cachData = Domain.Current.GetCachData("DebitCompany.ResponseCode." + debitCompanyId, () =>
            {
                return new Domain.CachData(Search(new SearchFilters() { DebitCompanyID = debitCompanyId }, null), DateTime.Now.AddHours(12));
            }) as List<ResponseCode>;
            return cachData.Where(r => r.Code == code).FirstOrDefault();
        }

        public static ResponseCode GetItemByResponseCodeId(int? id)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var cachData = Domain.Current.GetCachData("DebitCompany.ResponseCode." + id, () =>
            {
                return new Domain.CachData(Search(new SearchFilters() { ID = new Netpay.Infrastructure.Range<int?>(id) }, null), DateTime.Now.AddHours(12));
            }) as List<ResponseCode>;
            return cachData.Where(r => r.ID == id).FirstOrDefault();
        }

        public void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            DataContext.Writer.tblDebitCompanyCodes.Update(_entity, (_entity.ID != 0));
            DataContext.Writer.SubmitChanges();
        }

        public void Delete()
        {
            if (_entity.ID == 0) return;
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            DataContext.Writer.tblDebitCompanyCodes.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }

        public static List<ResponseCode> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
            //if ((context.User.Type != UserType.NetpayUser) && (context.User.Type != UserType.NetpayAdmin))
            //	filters.AccountIDs = SecurityManager.FilterRequestedIDs(filters.AccountIDs, context.User.AllowedMerchantsIDs);

            var expression = from p in DataContext.Reader.tblDebitCompanyCodes select p;

            if (filters.ID.From != null) expression = expression.Where(p => p.ID >= filters.ID.From);
            if (filters.ID.To != null) expression = expression.Where(p => p.ID <= filters.ID.To);
            if (filters.DebitCompanyID != null) expression = expression.Where(p => p.DebitCompanyID == filters.DebitCompanyID);
            if (filters.Text != null) expression = expression.Where(p => p.DescriptionOriginal.Contains(filters.Text) || p.Code.Contains(filters.Text));
            if (filters.FailSource != null) expression = expression.Where(p => p.FailSource == filters.FailSource);

            return expression.ApplySortAndPage(sortAndPage).Select(p => new ResponseCode(p)).ToList();
        }
    }
}
