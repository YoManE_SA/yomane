﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.DebitCompanies
{
    public class Terminal : BaseDataObject
    {
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(DebitCompanies.Terminals.Module.Current, SecuredObjectName); } }

        public class SearchFilters
        {
            public Range<int?> ID;
            public string Number;
            public int? DebitCompanyId;
            public bool? IsActive;
        }

        private Dal.Netpay.tblDebitTerminal _entity;

        public int ID { get { return _entity.id; } }
        public byte DebitCompany { get { return _entity.DebitCompany; } set { _entity.DebitCompany = value; } }
        public string TerminalNumber { get { return _entity.terminalNumber; } set { _entity.terminalNumber = value; } }
        public string TerminalNumber3D { get { return _entity.terminalNumber3D; } set { _entity.terminalNumber3D = value; } }
        public string Name { get { return _entity.dt_name; } set { _entity.dt_name = value; } }
        public string Mcc { get { return _entity.dt_mcc; } set { _entity.dt_mcc = value; } }
        public string Descriptor { get { return _entity.dt_Descriptor; } set { _entity.dt_Descriptor = value; } }
        public string SMSShortCode { get { return _entity.dt_SMSShortCode; } set { _entity.dt_SMSShortCode = value; } }
        public string TerminalNotes { get { return _entity.terminalNotes; } set { _entity.terminalNotes = value; } }
        public string ContractNumber { get { return _entity.dt_ContractNumber; } set { _entity.dt_ContractNumber = value; } }
        public int? ManagingCompany { get { return _entity.dt_ManagingCompany; } set { _entity.dt_ManagingCompany = value; } }
        public int? ManagingPSP { get { return _entity.dt_ManagingPSP; } set { _entity.dt_ManagingPSP = value; } }

        public string AccssesName1 { get { return _entity.accountId; } set { _entity.accountId = value; } }
        public string AccssesName2 { get { return _entity.accountSubId; } set { _entity.accountSubId = value; } }
        public string AccssesName3 { get { return _entity.AuthenticationCode1; } set { _entity.AuthenticationCode1 = value; } }

        public string AccssesName1_3D { get { return _entity.accountId3D; } set { _entity.accountId3D = value; } }
        public string AccssesName2_3D { get { return _entity.accountSubId3D; } set { _entity.accountSubId3D = value; } }
        public string AccssesName3_3D { get { return _entity.AuthenticationCode3D; } set { _entity.AuthenticationCode3D = value; } }

        public string SearchTag { get { return _entity.SearchTag; } set { _entity.SearchTag = value; } }

        public static List<string> SearchTagsList
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return (from dt in DataContext.Reader.tblDebitTerminals
                        where (dt.SearchTag!=null && dt.SearchTag != string.Empty)
                        orderby dt.SearchTag ascending
                        select dt.SearchTag)
                          .Distinct().ToList();
            }
        }

        public void GetDecryptedData(out string password, out string password3d)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            password = _entity.accountPassword256 != null ? Infrastructure.Security.Encryption.Decrypt(_entity.accountPassword256.ToArray()) : null;
            password3d = _entity.accountPassword3D256 != null ? Infrastructure.Security.Encryption.Decrypt(_entity.accountPassword3D256.ToArray()) : null;
        }
        public void SetDecryptedData(string password, string password3d)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            _entity.accountPassword256 = Infrastructure.Security.Encryption.Encrypt(password);
            _entity.accountPassword3D256 = Infrastructure.Security.Encryption.Encrypt(password3d);
        }

        public int BankAccountID { get { return _entity.dt_BankAccountID; } set { _entity.dt_BankAccountID = value; } }
        public string BankExternalID { get { return _entity.dt_BankExternalID; } set { _entity.dt_BankExternalID = value; } }
        public int? MobileCategory { get { return _entity.dt_mobileCat; } set { _entity.dt_mobileCat = value; } }
        public decimal TerminalMonthlyCost { get { return _entity.terminalMonthlyCost; } set { _entity.terminalMonthlyCost = value; } }
        public int MonthlyLimitCHB { get { return _entity.dt_MonthlyLimitCHB; } set { _entity.dt_MonthlyLimitCHB = value; } }
        public int MonthlyCHB { get { return _entity.dt_MonthlyCHB; } set { _entity.dt_MonthlyCHB = value; } }
        public bool MonthlyCHBWasSent { get { return _entity.dt_MonthlyCHBWasSent; } set { _entity.dt_MonthlyCHBWasSent = value; } }
        public System.DateTime MonthlyCHBSendDate { get { return _entity.dt_MonthlyCHBSendDate; } set { _entity.dt_MonthlyCHBSendDate = value; } }
        public int MonthlyMCLimitCHB { get { return _entity.dt_MonthlyMCLimitCHB; } set { _entity.dt_MonthlyMCLimitCHB = value; } }
        public int MonthlyMCCHB { get { return _entity.dt_MonthlyMCCHB; } set { _entity.dt_MonthlyMCCHB = value; } }
        public bool MonthlyMCCHBWasSent { get { return _entity.dt_MonthlyMCCHBWasSent; } set { _entity.dt_MonthlyMCCHBWasSent = value; } }
        public System.DateTime MonthlyMCCHBSendDate { get { return _entity.dt_MonthlyMCCHBSendDate; } set { _entity.dt_MonthlyMCCHBSendDate = value; } }

        public int? MobileCat { get { return _entity.dt_mobileCat; } set { _entity.dt_mobileCat = value; } }
        public byte ProcessingMethod { get { return _entity.processingMethod; } set { _entity.processingMethod = value; } }
        public bool IsActive { get { return _entity.isActive; } set { _entity.isActive = value; } }
        public bool IsNetpayTerminal { get { return _entity.isNetpayTerminal; } set { _entity.isNetpayTerminal = value; } }
        public bool IsShvaMasterTerminal { get { return _entity.isShvaMasterTerminal; } set { _entity.isShvaMasterTerminal = value; } }
        public bool IsManipulateAmount { get { return _entity.dt_isManipulateAmount; } set { _entity.dt_isManipulateAmount = value; } }
        public bool IsUseBlackList { get { return _entity.dt_IsUseBlackList; } set { _entity.dt_IsUseBlackList = value; } }
        public bool IsRefundBlocked { get { return _entity.dt_IsRefundBlocked; } set { _entity.dt_IsRefundBlocked = value; } }
        public bool EnableRecurringBank { get { return _entity.dt_EnableRecurringBank; } set { _entity.dt_EnableRecurringBank = value; } }
        public bool EnableAuthorization { get { return _entity.dt_EnableAuthorization; } set { _entity.dt_EnableAuthorization = value; } }
        public bool Enable3dsecure { get { return _entity.dt_Enable3dsecure; } set { _entity.dt_Enable3dsecure = value; } }
        public bool IsPersonalNumberRequired { get { return _entity.dt_IsPersonalNumberRequired; } set { _entity.dt_IsPersonalNumberRequired = value; } }
        public bool IsTestTerminal { get { return _entity.dt_IsTestTerminal; } set { _entity.dt_IsTestTerminal = value; } }

        public string MonthlyLimitCHBNotifyUsers { get { return _entity.dt_MonthlyLimitCHBNotifyUsers; } set { _entity.dt_MonthlyLimitCHBNotifyUsers = value; } }
        public string MonthlyLimitCHBNotifyUsersSMS { get { return _entity.dt_MonthlyLimitCHBNotifyUsersSMS; } set { _entity.dt_MonthlyLimitCHBNotifyUsersSMS = value; } }
        public string MonthlyMCLimitCHBNotifyUsers { get { return _entity.dt_MonthlyMCLimitCHBNotifyUsers; } set { _entity.dt_MonthlyMCLimitCHBNotifyUsers = value; } }
        public string MonthlyMCLimitCHBNotifyUsersSMS { get { return _entity.dt_MonthlyMCLimitCHBNotifyUsersSMS; } set { _entity.dt_MonthlyMCLimitCHBNotifyUsersSMS = value; } }


        public string dt_CompanyNum_visa { get { return _entity.dt_CompanyNum_visa; } set { _entity.dt_CompanyNum_visa = value; } }
        public string dt_Comments_visa { get { return _entity.dt_Comments_visa; } set { _entity.dt_Comments_visa = value; } }
        public string dt_CompanyNum_isracard { get { return _entity.dt_CompanyNum_isracard; } set { _entity.dt_CompanyNum_isracard = value; } }
        public string dt_Comments_isracard { get { return _entity.dt_Comments_isracard; } set { _entity.dt_Comments_isracard = value; } }
        public string dt_CompanyNum_direct { get { return _entity.dt_CompanyNum_direct; } set { _entity.dt_CompanyNum_direct = value; } }
        public string dt_Comments_direct { get { return _entity.dt_Comments_direct; } set { _entity.dt_Comments_direct = value; } }
        public string dt_CompanyNum_mastercard { get { return _entity.dt_CompanyNum_mastercard; } set { _entity.dt_CompanyNum_mastercard = value; } }
        public string dt_Comments_mastercard { get { return _entity.dt_Comments_mastercard; } set { _entity.dt_Comments_mastercard = value; } }
        public string dt_CompanyNum_diners { get { return _entity.dt_CompanyNum_diners; } set { _entity.dt_CompanyNum_diners = value; } }
        public string dt_Comments_diners { get { return _entity.dt_Comments_diners; } set { _entity.dt_Comments_diners = value; } }
        public string dt_CompanyNum_americanexp { get { return _entity.dt_CompanyNum_americanexp; } set { _entity.dt_CompanyNum_americanexp = value; } }
        public string dt_Comments_americanexp { get { return _entity.dt_Comments_americanexp; } set { _entity.dt_Comments_americanexp = value; } }


        internal Terminal(Netpay.Dal.Netpay.tblDebitTerminal entity)
        {
            _entity = entity;
        }



        public Terminal(int deitCompanyId)
        {
            //The security check for adding terminal is located at Terminals/Module.cs           
                        
            _entity = new Dal.Netpay.tblDebitTerminal();
            _entity.DebitCompany = (byte)deitCompanyId;
            //Initializing all the properties that are not
            //Allowed to be null when saving the terminal to the database

            TerminalNumber = "";
            TerminalNumber3D = "";
            Name = DateTime.Now.ToShortDateString();
            AccssesName1 = "";
            AccssesName2 = "";
            AccssesName1_3D = "";
            AccssesName2_3D = "";
            AccssesName3_3D = "";
            IsNetpayTerminal = false;
            IsShvaMasterTerminal = false;
            ProcessingMethod = 1;
            TerminalMonthlyCost = 0;
            TerminalNotes = "";
            MobileCat = 0;
            dt_CompanyNum_visa = "";
            dt_Comments_visa = "";
            dt_CompanyNum_isracard = "";
            dt_Comments_isracard = "";
            dt_CompanyNum_direct = "";
            dt_Comments_direct = "";
            dt_CompanyNum_mastercard = "";
            dt_Comments_mastercard = "";
            dt_CompanyNum_diners = "";
            dt_Comments_diners = "";
            dt_CompanyNum_americanexp = "";
            dt_Comments_americanexp = "";
            IsActive = false;
            Mcc = "";
            IsManipulateAmount = false;
            IsUseBlackList = false;
            MonthlyLimitCHB = 0;
            MonthlyCHB = 0;
            MonthlyCHBWasSent = false;
            MonthlyCHBSendDate = DateTime.Now;
            MonthlyMCLimitCHB = 0;
            MonthlyMCCHB = 0;
            MonthlyMCCHBWasSent = false;
            MonthlyMCCHBSendDate = DateTime.Now;
            BankAccountID = 0;
            IsRefundBlocked = false;
            MonthlyLimitCHBNotifyUsers = "";
            MonthlyLimitCHBNotifyUsersSMS = "";
            MonthlyMCLimitCHBNotifyUsers = "";
            MonthlyMCLimitCHBNotifyUsersSMS = "";
            ContractNumber = "";
            EnableRecurringBank = false;
            EnableAuthorization = false;
            Enable3dsecure = false;
            IsPersonalNumberRequired = false;
            IsTestTerminal = false;
        }

        public static Terminal Load(int terminalId)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var entity = (from mmas in DataContext.Reader.tblDebitTerminals where mmas.id == terminalId select mmas).SingleOrDefault();
            if (entity == null) return null;
            return new Terminal(entity);
        }

        public static Terminal Load(int debitCompany, string terminalNumber)
        {
            return Load(debitCompany, new List<string>() { terminalNumber }).FirstOrDefault();
        }

        public static Terminal LoadWithExtAccountID(int debitCompany, string accountId)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from mmas in DataContext.Reader.tblDebitTerminals where mmas.DebitCompany == debitCompany && mmas.accountId == accountId select new Terminal(mmas)).FirstOrDefault();
        }

        public static List<Terminal> Load(int debitCompany, List<string> terminalNumbers)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from mmas in DataContext.Reader.tblDebitTerminals where mmas.DebitCompany == debitCompany && terminalNumbers.Contains(mmas.terminalNumber) select new Terminal(mmas)).ToList();
        }


        public class TerminalInfo
        {
            public int TerminalId { get; set; }
            public string TerminalName { get; set; }
            public string TerminalTag { get; set; }
            public string TerminalNumber { get; set; }
            public int DebitCompanyId { get; set; }
            public string DebitCompanyName { get; set; }
            public bool IsActive { get; set; }
        }
        public static List<TerminalInfo> AutoComplete(string text)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from t in DataContext.Reader.tblDebitTerminals
                    join d in DataContext.Reader.tblDebitCompanies on t.DebitCompany equals d.DebitCompany_ID
                    where (d.dc_name + " - " + t.dt_name).Contains(text) || t.terminalNumber.Contains(text)
                    orderby d.dc_name ascending, t.dt_name ascending
                    select new TerminalInfo() { TerminalId = t.id, TerminalName = t.dt_name, TerminalNumber = t.terminalNumber, IsActive = t.isActive, DebitCompanyId = d.DebitCompany_ID, DebitCompanyName = d.dc_name }).Take(10).ToList();
        }

        public static List<TerminalInfo> AutoCompleteTag(string text)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from t in DataContext.Reader.tblDebitTerminals
                    where t.SearchTag.Contains(text)
                    orderby t.SearchTag ascending
                    select new TerminalInfo() { TerminalId = t.id, TerminalTag = t.SearchTag }).Distinct().Take(10).ToList();
        }

        public static List<Terminal> LoadForDebitCompany(int debitCompanyId)
        {
            return Search(new SearchFilters() { DebitCompanyId = debitCompanyId }, null);
        }

        public static List<Terminal> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var exp = (from t in DataContext.Reader.tblDebitTerminals select t);
            if (filters != null)
            {
                if (filters.ID.From.HasValue) exp = exp.Where(t => t.id >= filters.ID.From.Value);
                if (filters.ID.To.HasValue) exp = exp.Where(t => t.id <= filters.ID.To.Value);
                if (filters.DebitCompanyId != null) exp = exp.Where(t => t.DebitCompany == filters.DebitCompanyId);
                if (filters.Number != null) exp = exp.Where(t => t.terminalNumber == filters.Number);
                if (filters.IsActive != null) exp = exp.Where(t => t.isActive == filters.IsActive.Value);
            }
            return exp.ApplySortAndPage(sortAndPage).Select(t => new Terminal(t)).ToList();
        }

        public void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            DataContext.Writer.tblDebitTerminals.Update(_entity, (_entity.id != 0));
            DataContext.Writer.SubmitChanges();
        }

        public void Delete()
        {
            if (_entity.id == 0) return;

            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            DataContext.Writer.tblDebitTerminals.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }
    }
}
