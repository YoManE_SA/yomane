﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.DebitCompanies
{
    public enum DebitRuleStatus
    {
        Routine = 1,
        Notify = 2,
        UnBlock = 3,
        Block = 4
    }

    public enum eRuleType
    {
        All = 1,
        None = 2,
        MonthlyCHB = 3,
        Block = 4,
        Warning = 5
    }

    public class DebitBlockLog : BaseDataObject
    {
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(DebitCompanyBlockLogs.Module.Current, SecuredObjectName); } }

        private static readonly string NONE_VALUE = "(none)";

        public static Dictionary<DebitRuleStatus, System.Drawing.Color> DebitRuleColor = new Dictionary<DebitRuleStatus, System.Drawing.Color>{
            {DebitRuleStatus.Routine, System.Drawing.ColorTranslator.FromHtml("#AAAAAA")},
            {DebitRuleStatus.Notify,System.Drawing.ColorTranslator.FromHtml("#6699CC") },
            {DebitRuleStatus.Block,System.Drawing.ColorTranslator.FromHtml("#FF6666") },
            {DebitRuleStatus.UnBlock,System.Drawing.ColorTranslator.FromHtml("#66CC66") }
        };


        public class SearchFilters
        {
            public int? ID;

            // The name is the "dc_name" coloum in tblDebitCompany table.
            // The dbl_DebitCompany in tblDebitBlockLog = DebitCompany_ID in tblDebitCompany.
            public string DebitCompany;

            // The dbl_DebitRule coulom in tblDebitBlockLog.
            public int? RuleNumber;

            //I need to understand this one.
            public eRuleType? RuleType;

            public string RecordType;

            public Infrastructure.Range<DateTime?> DateRange;
        }

        //This property includes a list of all posibilities that can be in the Record Type coloum
        //The list items are token from the tbldebitblocklog "dbl_type" coloum 
        //And from the code of AdminChas "Log_DebitRules.aspx" line 32
        public static List<string> RecordTypeList
        {
            get
            {
                List<string> RecordTypes = new List<string>
               {
                    "Actions And Routins",
                    "Actions Only",
                    "MAIL SENT",
                    "RULE APPLIED",
                    "RULE CHECK END",
                    "RULE CHECK",
                    "BLOCK CHECK END",
                    "BLOCK CHECK",
                    "MONTHLY MAIL UNSENT",
                    "MONTHLY SMS UNSENT"
               };
                return RecordTypes;
            }
        }

        public static List<string> DebitCompaniesNames
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                var exp = (from dc in DataContext.Reader.tblDebitCompanies
                           from dr in DataContext.Reader.tblDebitRules
                           where dc.DebitCompany_ID == dr.dr_DebitCompany
                           select dc.dc_name).Distinct();

                var result = exp.OrderBy(x => x).ToList();
                result.Insert(0, NONE_VALUE);
                return result;
            }
        }

        private Dal.Netpay.tblDebitBlockLog _entity;

        public int ID { get { return _entity.ID; } }

        public DateTime Date { get { return _entity.dbl_Date; } }

        public string DebitCompanyName { get; private set; }

        public bool? RuleType { get; private set; }

        public int RuleNumber { get { return _entity.dbl_DebitRule; } }

        public string RecordType { get { return _entity.dbl_Type; } }

        public string Text { get { return _entity.dbl_Text; } }

        public DebitRuleStatus Status
        {
            get
            {
                if (RecordType.Contains(" CHECK"))
                { return DebitRuleStatus.Routine; }
                else if (RecordType.Contains(" SENT"))
                { return DebitRuleStatus.Notify; }
                else if ((RecordType == "'BLOCK REMOVED'") || (RecordType == "UNBLOCK ATTEMPT OK"))
                { return DebitRuleStatus.UnBlock; }
                else return DebitRuleStatus.Block;
            }

        }

        private DebitBlockLog(Dal.Netpay.tblDebitBlockLog entity)
        {
            _entity = entity;
        }

        public DebitBlockLog()
        {
            _entity = new Dal.Netpay.tblDebitBlockLog();
        }
                       
        public static List<DebitBlockLog> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var exp = (from dbl in DataContext.Reader.tblDebitBlockLogs
                       join dc in DataContext.Reader.tblDebitCompanies on dbl.dbl_DebitCompany equals dc.DebitCompany_ID into dcAll
                       join dr in DataContext.Reader.tblDebitRules on dbl.dbl_DebitRule equals dr.ID into drAll
                       from dcincludempty in dcAll.DefaultIfEmpty()
                       from drincludempty in drAll.DefaultIfEmpty()
                       select new { dbl, dcincludempty, drincludempty });
            
            if (filters != null)
            {
                //Check dates range
                if (filters.DateRange.To.HasValue)
                {
                    //Set To date to EOD
                    filters.DateRange.To = filters.DateRange.To.Value.Date.AddDays(1).AddMilliseconds(-1);
                }

                //The ID search(Used for the Load method.)
                if (filters.ID != null) exp = exp.Where(x => x.dbl.ID == filters.ID);

                //The debit company name search
                if (filters.DebitCompany != null && filters.DebitCompany != NONE_VALUE) exp = exp.Where(x => x.dcincludempty.dc_name == filters.DebitCompany);
                if (filters.DebitCompany == NONE_VALUE) exp = exp.Where(x => x.dbl.dbl_DebitCompany == 0);

                //The debit rule number search
                if (filters.RuleNumber != null) exp = exp.Where(x => x.dbl.dbl_DebitRule == filters.RuleNumber);

                //The rule type search 
                if (filters.RuleType != eRuleType.All && filters.RuleType!=null)
                {
                    if (filters.RuleType == eRuleType.None) exp = exp.Where(x => x.dbl.dbl_DebitRule == 0);
                    if (filters.RuleType == eRuleType.Block) exp = exp.Where(x => x.drincludempty.dr_IsAutoDisable == true);
                    if (filters.RuleType == eRuleType.Warning) exp = exp.Where(x => x.drincludempty.dr_IsAutoDisable == false);
                    if (filters.RuleType == eRuleType.MonthlyCHB) exp = exp.Where(x => x.dbl.dbl_Type.StartsWith("MONTHLY"));
                }

                //The record type search
                if ((filters.RecordType != "Actions And Routins") && (filters.RecordType != null))
                {
                    if (filters.RecordType == "Actions Only")
                    {
                        var blackList = new List<string> { "BLOCK CHECK END", "BLOCK CHECK", "RULE CHECK END", "RULE CHECK", "MONTHLY MAIL UNSENT", "MONTHLY SMS UNSENT" };
                        exp = exp.Where(x => !blackList.Contains(x.dbl.dbl_Type));
                    }

                    else
                    {
                        exp = exp.Where(x => x.dbl.dbl_Type == filters.RecordType);
                    }
                }

                //The Date Range search
                if (filters.DateRange.From.HasValue) exp = exp.Where(x => x.dbl.dbl_Date >= filters.DateRange.From);
                if (filters.DateRange.To.HasValue) exp = exp.Where(x => x.dbl.dbl_Date <= filters.DateRange.To);

            }
            var result = exp.ApplySortAndPage(sortAndPage).Select(x => new DebitBlockLog(x.dbl) { DebitCompanyName = x.dcincludempty.dc_name, RuleType = x.drincludempty.dr_IsAutoDisable }).ToList();

            return result;
        }

        public static DebitBlockLog Load(int id)
        {
            if (id > 0)
            {
                return Search(new SearchFilters() { ID = id }, null).SingleOrDefault();
            }
            else
            {
                return null;
            }
        }
    }
}
