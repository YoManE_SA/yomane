﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.DebitCompanies
{
    public class DebitCompany : Accounts.Account
    {
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(DebitCompanies.Module.Current, SecuredObjectName); } }

        public class SearchFilters
        {
            public Infrastructure.Range<int?> ID;
            public bool? IsActive;
            public string Text;
        }

        public static Dictionary<Infrastructure.ActiveOrNot, System.Drawing.Color> StatusColor = new Dictionary<ActiveOrNot, System.Drawing.Color>
        {
            {Infrastructure.ActiveOrNot.Active, System.Drawing.Color.Green },
            {Infrastructure.ActiveOrNot.NotActive, System.Drawing.Color.Red }
        };

        public static Dictionary<Infrastructure.ActiveOrNot, string> StatusText = new Dictionary<ActiveOrNot, string>
        {
            {Infrastructure.ActiveOrNot.Active, "Active" },
            {Infrastructure.ActiveOrNot.NotActive, "Not-Active" },

        };

        private Dal.Netpay.tblDebitCompany _entity;

        public int ID { get { return _entity.DebitCompany_ID; } set { _entity.DebitCompany_ID = value; } }
        public string Name { get { return _entity.dc_name; } set { _entity.dc_name = value; base.AccountName = value; } }
        public string Description { get { return _entity.dc_description; } set { _entity.dc_description = value; } }
        public string EmergencyPhone { get { return _entity.dc_EmergencyPhone; } set { _entity.dc_EmergencyPhone = value; } }
        public string EmergencyContact { get { return _entity.dc_EmergencyContact; } set { _entity.dc_EmergencyContact = value; } }

        public bool IsActive { get { return _entity.dc_isActive; } set { _entity.dc_isActive = value; } }
        public bool IsBlocked { get { return _entity.dc_IsBlocked; } set { _entity.dc_IsBlocked = value; } }
        public bool IsDynamicDescriptor { get { return _entity.IsDynamicDescriptor.GetValueOrDefault(); } set { _entity.IsDynamicDescriptor = value; } }
        public bool IsAllowRefund { get { return _entity.dc_isAllowRefund; } set { _entity.dc_isAllowRefund = value; } }
        public bool IsAllowPartialRefund { get { return _entity.dc_isAllowPartialRefund; } set { _entity.dc_isAllowPartialRefund = value; } }
        public bool IsReturnCode { get { return _entity.dc_isReturnCode; } set { _entity.dc_isReturnCode = value; } }
        public bool IsAutoRefund { get { return _entity.dc_IsAutoRefund; } set { _entity.dc_IsAutoRefund = value; } }
        public bool IsAllowPayoutWithoutTransfer { get { return _entity.dc_IsAllowPayoutWithoutTransfer; } set { _entity.dc_IsAllowPayoutWithoutTransfer = value; } }
        public bool IsNotifyRetReqAfterRefund { get { return _entity.dc_IsNotifyRetReqAfterRefund.GetValueOrDefault(); } set { _entity.dc_IsNotifyRetReqAfterRefund = value; } }
        public string AutoRefundNotifyUsers { get { return _entity.dc_AutoRefundNotifyUsers; } set { _entity.dc_AutoRefundNotifyUsers = value; } }

        public int MonthlyLimitCHB { get { return _entity.dc_MonthlyLimitCHB; } set { _entity.dc_MonthlyLimitCHB = value; } }

        public string MonthlyLimitCHBNotifyUsers { get { return _entity.dc_MonthlyLimitCHBNotifyUsers; } set { _entity.dc_MonthlyLimitCHBNotifyUsers = value; } }
        public int MonthlyMCLimitCHB { get { return _entity.dc_MonthlyMCLimitCHB; } set { _entity.dc_MonthlyMCLimitCHB = value; } }

        public string MonthlyMCLimitCHBNotifyUsers { get { return _entity.dc_MonthlyMCLimitCHBNotifyUsers; } set { _entity.dc_MonthlyMCLimitCHBNotifyUsers = value; } }

        public string MonthlyLimitCHBNotifyUsersSMS { get { return _entity.dc_MonthlyLimitCHBNotifyUsersSMS; } set { _entity.dc_MonthlyLimitCHBNotifyUsersSMS = value; } }

        public string MonthlyMCLimitCHBNotifyUsersSMS { get { return _entity.dc_MonthlyMCLimitCHBNotifyUsersSMS; } set { _entity.dc_MonthlyMCLimitCHBNotifyUsersSMS = value; } }

        public string UnblockAttemptString { get { return _entity.dc_UnblockAttemptString; } set { _entity.dc_UnblockAttemptString = value; } }
        //public int NextTransID { get { return _entity.dc_nextTransID; } set { _entity.dc_nextTransID = value; } }
        //public int TempBlocks { get { return _entity.dc_TempBlocks; } set { _entity.dc_TempBlocks = value; } }
        //public string RegistrationUsrname { get { return _entity.RegistrationUsrname; } set { _entity.RegistrationUsrname = value; } }
        //public System.Data.Linq.Binary RegistrationPassword256 { get { return _entity.RegistrationPassword256; } set { _entity.RegistrationPassword256 = value; } }

        private DebitCompany(Dal.Netpay.tblDebitCompany entity, AccountLoadCombined accountData) : base(accountData) { _entity = entity; }

        public DebitCompany() : base(Accounts.AccountType.DebitCompany)
        {
            //No security check , because the code doesn't let to add DebitCompany anyway.

            _entity = new Dal.Netpay.tblDebitCompany();
        }

        public static new DebitCompany Current
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return Accounts.Account.CurrentObject as DebitCompany;
            }
        }

        public static Dictionary<int, DebitCompany> GetCache()
        {
            try
            {
                ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);
                return Domain.Current.GetCachData("DebitCompany", () =>
                {
                    return new Domain.CachData(Search(null, null).ToDictionary(d => d.ID), DateTime.Now.AddHours(12));
                }) as Dictionary<int, DebitCompany>;
            }
            finally
            {
                ObjectContext.Current.StopImpersonate();
            }
        }

        public static DebitCompany GetCachedItem(int id)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            DebitCompany ret;
            if (GetCache().TryGetValue(id, out ret)) return ret;
            return null;
        }

        public static List<DebitCompany> Load(List<int> ids)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from a in Accounts.Account.LoadQuery(DataContext.Reader)
                    join d in DataContext.Reader.tblDebitCompanies on a.account.DebitCompany_id equals d.DebitCompany_ID
                    where ids.Contains(d.DebitCompany_ID)
                    select new DebitCompany(d, a)).ToList();
        }

        public static DebitCompany LoadDebitCompanyById(int debitcompanyid)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from a in Accounts.Account.LoadQuery(DataContext.Reader)
                    join d in DataContext.Reader.tblDebitCompanies on a.account.DebitCompany_id equals d.DebitCompany_ID
                    where d.DebitCompany_ID == debitcompanyid
                    select new DebitCompany(d, a)).SingleOrDefault();
        }



        public void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            DataContext.Writer.tblDebitCompanies.Update(_entity, (_entity.DebitCompany_ID != 0));
            if (_accountEntity.DebitCompany_id == null)
                _accountEntity.DebitCompany_id = _entity.DebitCompany_ID;

            base.AccountName = Name;
            base.SaveAccount();

            DataContext.Writer.SubmitChanges();
        }

        public void Delete()
        {
            if (_entity.DebitCompany_ID == 0) return;

            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            DataContext.Writer.tblDebitCompanies.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }

        public static List<Accounts.AccountInfo> AutoComplete(string text)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            int numValue = text.ToNullableInt().GetValueOrDefault(-1);
            return (from a in DataContext.Reader.Accounts
                    join c in DataContext.Reader.tblDebitCompanies on a.DebitCompany_id equals c.DebitCompany_ID
                    where c.dc_name.Contains(text) || a.Account_id == numValue
                    select new Accounts.AccountInfo()
                    {
                        AccountID = a.Account_id,
                        TargetID = c.DebitCompany_ID,
                        AccountName = string.Format("{0}", c.dc_name),
                        AccountType = Accounts.AccountType.DebitCompany,
                        AccountStatus = (c.dc_isActive).ToString(),
                        StatusColor = c.dc_isActive ? System.Drawing.Color.Green : System.Drawing.Color.Red
                    }).Take(10).ToList();
        }

        public static List<DebitCompany> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            //if ((context.User.Type != UserType.NetpayUser) && (context.User.Type != UserType.NetpayAdmin))
            //	filters.AccountIDs = SecurityManager.FilterRequestedIDs(filters.AccountIDs, context.User.AllowedMerchantsIDs);
            var expression = from a in Accounts.Account.LoadQuery(DataContext.Reader, false)
                             join d in DataContext.Reader.tblDebitCompanies on a.account.DebitCompany_id equals d.DebitCompany_ID
                             select new { d, a };
            if (filters != null)
            {
                if (filters.ID.From != null) expression = expression.Where(p => p.a.account.Account_id >= filters.ID.From);
                if (filters.ID.To != null) expression = expression.Where(p => p.a.account.Account_id <= filters.ID.To);
                if (filters.Text != null) expression = expression.Where(p => p.d.dc_name.Contains(filters.Text) || p.d.dc_description.Contains(filters.Text));
                if (filters.IsActive != null) expression = expression.Where(p => p.d.dc_isActive == filters.IsActive);
            }
            return expression.ApplySortAndPage(sortAndPage).Select(p => new DebitCompany(p.d, p.a)).ToList();
        }

        public static Accounts.ExternalServiceLogin GetServiceLogin(int debitCompanyId, string serviceType)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var ret = (from l in DataContext.Reader.AccountExternalServices
                       join a in DataContext.Reader.Accounts on l.Account_id equals a.Account_id
                       where a.DebitCompany_id == debitCompanyId && l.ExternalServiceType_id == serviceType
                       select new Accounts.ExternalServiceLogin(l)).SingleOrDefault();
            if (ret == null)
            {
                int accountID = DataContext.Reader.Accounts.Where(a => a.DebitCompany_id == (int)debitCompanyId).Select(a => a.Account_id).SingleOrDefault();
                Infrastructure.Logger.Log("EpaLogin row not found. Created for " + Infrastructure.Domain.Current.Host + " , DebitCompanyID=" + debitCompanyId.ToString() + ", AccountID=" + accountID);
                ret = new Accounts.ExternalServiceLogin(accountID, serviceType) { IsActive = false };
                if (accountID != 0) ret.Save();
            }
            return ret;
        }

    }
}
