﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Bll.Accounts;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using Netpay.Bll.Merchants;

namespace Netpay.Bll.MerchantBlackList
{
    public class MerchantBlackListSettings : BaseDataObject
    {
        public class SearchFilters
        {
            public bool? IsOrCondition;
            public int? MerchantId;
            public string MerchantNo;
            public int? CompanyID;
            public string CompanyName;
            public string CompanyLegalNumber;
            public string IDNumber;
            public string FirstName;
            public string LastName;
            public string Phone;
            public string Fax;
            public string Cellular;
            public string Mail;
            public string URL;
        }

        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(MerchantBlackList.Module.Current, SecuredObjectName); } }

        private Dal.Netpay.BlacklistMerchant _entity;

        public int ID { get { return _entity.BlacklistMerchant_id; } }

        public System.Nullable<System.DateTime> InsertDate { get { return _entity.InsertDate; } set { _entity.InsertDate = value; } }
        public string InsertedBy { get { return _entity.InsertedBy; } set { _entity.InsertedBy = value; } }
        public string FirstName { get { return _entity.FirstName; } set { _entity.FirstName = value; } }

        public string LastName { get { return _entity.LastName; } set { _entity.LastName = value; } }

        public string Street { get { return _entity.Street; } set { _entity.Street = value; } }
        public string City { get { return _entity.City; } set { _entity.City = value; } }
        public string Phone { get { return _entity.Phone; } set { _entity.Phone = value; } }
        public string Cellular { get { return _entity.Cellular; } set { _entity.Cellular = value; } }

        public string IDNumber { get { return _entity.IDNumber; } set { _entity.IDNumber = value; } }

        public int? CompanyId { get { return _entity.Merchant_id; } set { _entity.Merchant_id = value; } }

        public string CompanyName { get { return _entity.CompanyName; } set { _entity.CompanyName = value; } }

        public string CompanyLegalName { get { return _entity.CompanyLegalName; } set { _entity.CompanyLegalName = value; } }
        public string CompanyLegalNumber { get { return _entity.CompanyLegalNumber; } set { _entity.CompanyLegalNumber = value; } }

        public string CompanyStreet { get { return _entity.CompanyStreet; } set { _entity.CompanyStreet = value; } }

        public string CompanyCity { get { return _entity.CompanyCity; } set { _entity.CompanyCity = value; } }
        public string CompanyPhone { get { return _entity.CompanyPhone; } set { _entity.CompanyPhone = value; } }
        public string CompanyFax { get { return _entity.CompanyFax; } set { _entity.CompanyFax = value; } }
        public string Mail { get { return _entity.Mail; } set { _entity.Mail = value; } }

        public string URL { get { return _entity.URL; } set { _entity.URL = value; } }
        public string MerchantSupportEmail { get { return _entity.MerchantSupportEmail; } set { _entity.MerchantSupportEmail = value; } }

        public string MerchantSupportPhoneNum { get { return _entity.MerchantSupportPhoneNum; } set { _entity.MerchantSupportPhoneNum = value; } }
        public string IpOnReg { get { return _entity.IpOnReg; } set { _entity.IpOnReg = value; } }
        public string PaymentPayeeName { get { return _entity.PaymentPayeeName; } set { _entity.PaymentPayeeName = value; } }

        public string PaymentBranch { get { return _entity.PaymentBranch; } set { _entity.PaymentBranch = value; } }

        public string PaymentAccount { get { return _entity.PaymentAccount; } set { _entity.PaymentAccount = value; } }

        public string SearchLink { get { return _entity.SearchLink; } set { _entity.SearchLink = value; } }
        public string DeleteButton { get { return _entity.DeleteButton; } set { _entity.DeleteButton = value; } }

        public string PaymentBank { get { return _entity.PaymentBank; } set { _entity.PaymentBank = value; } }

        public static Dictionary<int, string> a { get; private set; }

        private MerchantBlackListSettings(Netpay.Dal.Netpay.BlacklistMerchant entity)
        {
            _entity = entity;
        }

        public MerchantBlackListSettings()
        {
            _entity = new Dal.Netpay.BlacklistMerchant();
        }

        public static MerchantBlackListSettings Load(int ID)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var entity = (from mmas in DataContext.Reader.BlacklistMerchants where mmas.BlacklistMerchant_id == ID select mmas).SingleOrDefault();
            if (entity == null) return null;
            return new MerchantBlackListSettings(entity);
        }

        public void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            DataContext.Writer.BlacklistMerchants.Update(_entity, (_entity.BlacklistMerchant_id != 0));
            DataContext.Writer.SubmitChanges();
        }

        public void Delete()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            if (_entity.Merchant_id == 0) return;
            DataContext.Writer.BlacklistMerchants.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }

        public static List<MerchantBlackListSettings> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var exp = (from f in DataContext.Reader.BlacklistMerchants select f);
            if (filters != null)
            {
                if (filters.IsOrCondition.HasValue && filters.IsOrCondition.Value)
                {
                    var dc = DataContext.Reader;
                    int? MerchantID = filters.CompanyID ;
                    string MerchantNumber = Merchants.Merchant.GetCleanString(filters.MerchantNo);
                    string MerchantName = Merchants.Merchant.GetCleanString(filters.CompanyName);
                    string LegalNumber = Merchants.Merchant.GetCleanString(filters.CompanyLegalNumber);
                    string IDNumber = Merchants.Merchant.GetCleanString(filters.IDNumber);
                    string FirstName = Merchants.Merchant.GetCleanString(filters.FirstName);
                    string LastName = Merchants.Merchant.GetCleanString(filters.LastName);
                    string Phone = Merchants.Merchant.GetCleanString(filters.Phone);
                    string Fax = Merchants.Merchant.GetCleanString(filters.Fax);
                    string Cellular = Merchants.Merchant.GetCleanString(filters.Cellular);
                    string Mail = Merchants.Merchant.GetCleanString(filters.Mail);
                    string URL = Merchants.Merchant.GetCleanString(filters.URL);

                    exp = exp.Where(x => (
                                          (x.Merchant_id == MerchantID)  ||
                                          ((!string.IsNullOrEmpty(MerchantNumber)) ? ((x.Merchant_id.HasValue && (Bll.Merchants.Merchant.GetMerchantIdsByMerchantNumber(MerchantNumber)).Contains(x.Merchant_id.Value))) : (false)) ||
                                          ((!string.IsNullOrEmpty(MerchantName)) ? (dc.GetCleanText(x.CompanyName).Contains(MerchantName) || dc.GetCleanText(x.CompanyLegalName).Contains(MerchantName)) : (false)) ||
                                          ((!string.IsNullOrEmpty(LegalNumber)) ? (dc.GetCleanText(x.CompanyLegalNumber).Contains(LegalNumber)) : (false)) ||
                                          ((!string.IsNullOrEmpty(IDNumber)) ? (dc.GetCleanText(x.IDNumber).Contains(IDNumber)) : (false)) ||
                                          ((!string.IsNullOrEmpty(FirstName)) ? (dc.GetCleanText(x.FirstName).Contains(FirstName) || dc.GetCleanText(x.PaymentPayeeName).Contains(FirstName)) : (false)) ||
                                          ((!string.IsNullOrEmpty(LastName)) ? (dc.GetCleanText(x.LastName).Contains(LastName) || dc.GetCleanText(x.PaymentPayeeName).Contains(LastName)) : (false)) ||
                                          ((!string.IsNullOrEmpty(Phone)) ? ( dc.GetCleanText(x.Phone).Contains(Phone) || dc.GetCleanText(x.Cellular).Contains(Phone) || dc.GetCleanText(x.CompanyPhone).Contains(Phone) || dc.GetCleanText(x.CompanyFax).Contains(Phone) || dc.GetCleanText(x.MerchantSupportPhoneNum).Contains(Phone)) : (false)) ||
                                          ((!string.IsNullOrEmpty(Fax)) ? (dc.GetCleanText(x.Phone).Contains(Fax) || dc.GetCleanText(x.Cellular).Contains(Fax)  || dc.GetCleanText(x.CompanyPhone).Contains(Fax) || dc.GetCleanText(x.CompanyFax).Contains(Fax) || dc.GetCleanText(x.MerchantSupportPhoneNum).Contains(Fax)) : (false)) ||
                                          ((!string.IsNullOrEmpty(Cellular)) ? (dc.GetCleanText(x.Phone).Contains(Cellular) || dc.GetCleanText(x.Cellular).Contains(Cellular) || dc.GetCleanText(x.CompanyPhone).Contains(Cellular) || dc.GetCleanText(x.CompanyFax).Contains(Cellular) || dc.GetCleanText(x.MerchantSupportPhoneNum).Contains(Cellular)) : (false)) ||
                                          ((!string.IsNullOrEmpty(Mail)) ? (dc.GetCleanText(x.Mail).Contains(Mail) || dc.GetCleanText(x.MerchantSupportEmail).Contains(Mail)) : (false)) ||
                                          ((!string.IsNullOrEmpty(URL)) ? (dc.GetCleanText(x.URL).Contains(URL)) : (false))
                                        ));

                }
                else
                {
                    var dc = DataContext.Reader;
                  //  if (filters.MerchantId.HasValue) exp = exp.Where(l => (l.Merchant_id.HasValue && l.Merchant_id.Value == filters.MerchantId.Value));
                    if (!string.IsNullOrEmpty(filters.MerchantNo))
                    {
                        var listIds = Bll.Merchants.Merchant.GetMerchantIdsByMerchantNumber(filters.MerchantNo.Trim());
                        exp = exp.Where(l => (l.Merchant_id.HasValue && listIds.Contains(l.Merchant_id.Value)));
                    }

                    if (!string.IsNullOrEmpty(filters.CompanyName))
                    {
                        string cleanCompanyName = Merchant.GetCleanString(filters.CompanyName);
                        exp = exp.Where(l => dc.GetCleanText(l.CompanyName).Contains(cleanCompanyName) || dc.GetCleanText(l.CompanyLegalName).Contains(cleanCompanyName));
                    }

                    if (!string.IsNullOrEmpty(filters.CompanyLegalNumber))
                    {
                        string cleanCompanyLegalNumber = Merchant.GetCleanString(filters.CompanyLegalNumber);
                        exp = exp.Where(l => dc.GetCleanText(l.CompanyLegalNumber).Contains(cleanCompanyLegalNumber));
                    }

                    if (!string.IsNullOrEmpty(filters.IDNumber))
                    {
                        string cleanIDNumber = Merchant.GetCleanString(filters.IDNumber);
                        exp = exp.Where(l => dc.GetCleanText(l.IDNumber).Contains(cleanIDNumber));
                    }

                    if (!string.IsNullOrEmpty(filters.FirstName))
                    {
                        string cleanFirstName = Merchant.GetCleanString(filters.FirstName);
                        exp = exp.Where(l => dc.GetCleanText(l.FirstName).Contains(cleanFirstName) || dc.GetCleanText(l.PaymentPayeeName).Contains(cleanFirstName));
                    }

                    if (!string.IsNullOrEmpty(filters.LastName))
                    {
                        string cleanLastName = Merchant.GetCleanString(filters.LastName);
                        exp = exp.Where(l => dc.GetCleanText(l.LastName).Contains(cleanLastName) || dc.GetCleanText(l.PaymentPayeeName).Contains(cleanLastName));
                    }

                    if (!string.IsNullOrEmpty(filters.Phone)) 
                    {
                        string cleanPhone = Merchant.GetCleanString(filters.Phone);
                        exp = exp.Where(l => dc.GetCleanText(l.Phone).Contains(cleanPhone) || dc.GetCleanText(l.Cellular).Contains(cleanPhone) || dc.GetCleanText(l.CompanyPhone).Contains(cleanPhone) || dc.GetCleanText(l.CompanyFax).Contains(cleanPhone) || dc.GetCleanText(l.MerchantSupportPhoneNum).Contains(cleanPhone));
                    }

                    if (!string.IsNullOrEmpty(filters.Fax)) 
                    {
                        string cleanFax = Merchant.GetCleanString(filters.Fax);
                        exp = exp.Where(l => dc.GetCleanText(l.Phone).Contains(cleanFax) || dc.GetCleanText(l.Cellular).Contains(cleanFax) || dc.GetCleanText(l.Cellular).Contains(cleanFax) || dc.GetCleanText(l.CompanyPhone).Contains(cleanFax) || dc.GetCleanText(l.CompanyFax).Contains(cleanFax) || dc.GetCleanText(l.MerchantSupportPhoneNum).Contains(cleanFax) || dc.GetCleanText(l.MerchantSupportPhoneNum).Contains(cleanFax));
                    }

                    if (!string.IsNullOrEmpty(filters.Cellular))
                    {
                        string cleanCellular = Merchant.GetCleanString(filters.Cellular);
                        exp = exp.Where(l => dc.GetCleanText(l.Phone).Contains(cleanCellular) || dc.GetCleanText(l.Cellular).Contains(cleanCellular) || dc.GetCleanText(l.CompanyPhone).Contains(cleanCellular) || dc.GetCleanText(l.CompanyFax).Contains(cleanCellular) || dc.GetCleanText(l.MerchantSupportPhoneNum).Contains(cleanCellular));
                    }

                    if (!string.IsNullOrEmpty(filters.Mail))
                    {
                        string cleanMail = Merchant.GetCleanString(filters.Mail);
                        exp = exp.Where(l => dc.GetCleanText(l.Mail).Contains(cleanMail) || dc.GetCleanText(l.MerchantSupportEmail).Contains(cleanMail) || dc.GetCleanText(l.MerchantSupportEmail).Contains(cleanMail));
                    }
                    if (!string.IsNullOrEmpty(filters.URL))
                    {
                        string cleanUrl = Merchant.GetCleanString(filters.URL);
                        exp = exp.Where(l => dc.GetCleanText("http://" + l.URL).Contains(cleanUrl));
                    }
                }
            }
            return exp.ApplySortAndPage(sortAndPage).Select(f => new MerchantBlackListSettings(f)).ToList();
        }
                
        public static bool CheckIfMerchantIsBlackListed(int merchantid)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return DataContext.Reader.BlacklistMerchants.Any(x => x.Merchant_id == merchantid);
        }
    }
}
