﻿using System;
using System.Collections.Generic;
using System.Linq;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll
{
	/// <summary>
	/// Payments related logic.
	/// </summary>
	public static class Payments
	{
		public static List<PaymentVO> GetMerchantPayments(ISortAndPage sortAndPage)
		{
			User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantLimited, UserType.MerchantPrimary });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var expression = (IQueryable<viewPaymentsInfo>)from p in dc.viewPaymentsInfos where p.Company_id == user.ID  orderby p.CompanyMakePaymentsRequests_id descending select p;
			return expression.ApplySortAndPage(sortAndPage).Select(p => new PaymentVO(p)).ToList<PaymentVO>();
		}

		public static List<PaymentVO> GetMerchantPayments(SearchFilters filters, ISortAndPage sortAndPage)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken);
			if ((user.Type != UserType.NetpayUser) && (user.Type != UserType.NetpayAdmin))
				filters.merchantIDs = SecurityManager.FilterRequestedIDs(filters.merchantIDs, user.AllowedMerchantsIDs);

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var expression = (IQueryable<viewPaymentsInfo>)from p in dc.viewPaymentsInfos where p.Company_id == user.ID orderby p.CompanyMakePaymentsRequests_id descending select p;

			// add filters				
			if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
				expression = expression.Where(p => p.Company_id != null && filters.merchantIDs.Contains(p.Company_id.Value));
			if (filters.makePaymentsRequestsID != null)
				expression = expression.Where(pm => pm.CompanyMakePaymentsRequests_id == filters.makePaymentsRequestsID.Value);
			if (filters.basicInfoCustomerNumber != null)
				expression = from pm in expression where pm.basicInfo_costumerNumber.Contains(filters.basicInfoCustomerNumber) select pm;
			if (filters.basicInfoCustomerName != null)
				expression = from pm in expression where pm.basicInfo_costumerName.Contains(filters.basicInfoCustomerName) || pm.bankIsraelInfo_PayeeName.Contains(filters.basicInfoCustomerName) || pm.bankAbroadAccountName2.Contains(filters.basicInfoCustomerName) || pm.bankAbroadAccountName.Contains(filters.basicInfoCustomerName) select pm;
			if (filters.paymentType != null)
				expression = expression.Where(pm => pm.paymentType == filters.paymentType.Value);
			return expression.ApplySortAndPage(sortAndPage).Select(p => new PaymentVO(p)).ToList<PaymentVO>();
		}

		public static List<CreditFeesVO> GetSettlmentFees(int companyID, int payID)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken);
			if ((user.Type != UserType.NetpayUser) && (user.Type != UserType.NetpayAdmin)) companyID = user.ID;
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			if (payID != 0) {
				var expression = (IQueryable<tblTransactionPayFee>)from p in dc.tblTransactionPayFees where p.CCF_CompanyID == companyID && p.CCF_TransactionPayID == payID select p;
				return expression.Select(p => new CreditFeesVO(p)).ToList<CreditFeesVO>();
			}else{
				var expression = (IQueryable<tblCompanyCreditFee>)from p in dc.tblCompanyCreditFees where p.CCF_CompanyID == companyID && !p.CCF_IsDisabled select p;
				return expression.Select(p => new CreditFeesVO(p)).ToList<CreditFeesVO>();
			}
		}

		public static List<MerchantFeesFloorVO> GetFloorFees(int companyID, int payID)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken);
			if ((user.Type != UserType.NetpayUser) && (user.Type != UserType.NetpayAdmin)) companyID = user.ID;
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			if (payID != 0) {
				var expression = (IQueryable<tblPaymentFeesFloor>)from p in dc.tblPaymentFeesFloors where p.CFF_CompanyID == companyID && p.CFF_PayID == payID orderby p.CFF_TotalTo ascending select p;
				return expression.Select(p => new MerchantFeesFloorVO(p)).ToList<MerchantFeesFloorVO>();
			} else {
				var expression = (IQueryable<tblCompanyFeesFloor>) from p in dc.tblCompanyFeesFloors where p.CFF_CompanyID == companyID orderby p.CFF_TotalTo ascending select p;
				return expression.Select(p => new MerchantFeesFloorVO(p)).ToList<MerchantFeesFloorVO>();
			}
		}

	} 
}
