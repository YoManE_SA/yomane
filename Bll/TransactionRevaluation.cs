﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure.Domains;
using Netpay.Dal.DataAccess;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure.VO;

namespace Netpay.Bll
{
    public static class TransactionRevaluation
    {
        public static List<string> GetCodes(string domainHost, int merchantID) 
        {
            Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            var codes = (from c in dc.tblMerchantRevaluatedCodes where c.MerchantID == merchantID select c.ReplyCode).ToList();

            return codes;
        }

        public static void RemoveCode(string domainHost, int merchantID, string replyCode)
        {
            Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            replyCode = replyCode.Trim();
            tblMerchantRevaluatedCode entity = (from c in dc.tblMerchantRevaluatedCodes where c.MerchantID == merchantID && c.ReplyCode == replyCode select c).SingleOrDefault();
            if (entity == null)
                return;

            dc.tblMerchantRevaluatedCodes.DeleteOnSubmit(entity);
            dc.SubmitChanges();
        }

        public static void AddCode(string domainHost, int merchantID, string replyCode)
        {
            Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            replyCode = replyCode.Trim();
            tblMerchantRevaluatedCode entity = (from c in dc.tblMerchantRevaluatedCodes where c.MerchantID == merchantID && c.ReplyCode == replyCode select c).SingleOrDefault();
            if (entity != null)
                return;

            tblMerchantRevaluatedCode entry = new tblMerchantRevaluatedCode();
            entry.MerchantID = merchantID;
            entry.ReplyCode = replyCode;
            dc.tblMerchantRevaluatedCodes.InsertOnSubmit(entry);
            dc.SubmitChanges();
        }

        public static void AddTransaction(string domainHost, int merchantID, int transactionID, string replyCode) 
        {
            Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            replyCode = replyCode.Trim();
            tblMerchantTransRevaluation entity = (from c in dc.tblMerchantTransRevaluations where c.MerchantID == merchantID && c.TransactionID == transactionID && c.ReplyCode == replyCode select c).SingleOrDefault();
            if (entity != null)
                return;

            tblMerchantTransRevaluation entry = new tblMerchantTransRevaluation();
            entry.MerchantID = merchantID;
            entry.ReplyCode = replyCode;
            entry.TransactionID = transactionID;
            dc.tblMerchantTransRevaluations.InsertOnSubmit(entry);
            dc.SubmitChanges();
        }

        public static KeyValueVO<string, Transaction.Transaction> GetTransaction(string domainHost, int merchantID, int transactionID)
        {
            Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            tblMerchantTransRevaluation entity = (from c in dc.tblMerchantTransRevaluations where c.MerchantID == merchantID && c.TransactionID == transactionID select c).SingleOrDefault();
            if (entity != null)
                return null;

			return new KeyValueVO<string, Transaction.Transaction>(entity.ReplyCode, new Transaction.Transaction(domain.ServiceCredentials, entity.tblCompanyTransPass));
        }

		public static List<KeyValueVO<string, Transaction.Transaction>> GetTransactions(string domainHost, int merchantID)
        {
            Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			var transactions = (from c in dc.tblMerchantTransRevaluations where c.MerchantID == merchantID select new KeyValueVO<string, Transaction.Transaction>(c.ReplyCode, new Transaction.Transaction(domain.ServiceCredentials, c.tblCompanyTransPass))).ToList();

            return transactions;
        }

		public static List<KeyValueVO<string, Transaction.Transaction>> GetTransactions(string domainHost)
        {
            Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			var transactions = (from c in dc.tblMerchantTransRevaluations select new KeyValueVO<string, Transaction.Transaction>(c.ReplyCode, new Transaction.Transaction(domain.ServiceCredentials, c.tblCompanyTransPass))).ToList();

            return transactions;
        }

        public static void RemoveTransaction(string domainHost, int merchantID, int transactionID) 
        {
            Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            var transaction = (from t in dc.tblMerchantTransRevaluations where t.MerchantID == merchantID && t.TransactionID == transactionID select t).SingleOrDefault();
            if (transaction == null)
                return;

            dc.tblMerchantTransRevaluations.DeleteOnSubmit(transaction);
            dc.SubmitChanges();
        }

        public static void RemoveTransactions(string domainHost, int merchantID)
        {
            Domain domain = DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            var transactions = from t in dc.tblMerchantTransRevaluations where t.MerchantID == merchantID select t;

            dc.tblMerchantTransRevaluations.DeleteAllOnSubmit(transactions);
            dc.SubmitChanges();
        }
    }
}