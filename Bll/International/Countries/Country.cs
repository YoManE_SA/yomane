﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll
{
	public class Country : BaseDataObject
	{
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Bll.International.Countries.Module.Current, SecuredObjectName); } }


        private Dal.Netpay.CountryList _entity;

		public int ID { get { return _entity.CountryID.GetValueOrDefault(); } }
		public string Name { get { return _entity.Name; } set { _entity.Name = value; } }
		public byte? SortOrder { get { return _entity.SortOrder; } set { _entity.SortOrder = value; } }

		public string IsoCode2 { get { return _entity.CountryISOCode; } set { _entity.CountryISOCode = value; } }
		public string IsoCode3 { get { return _entity.ISOCode3; } set { _entity.ISOCode3 = value; } }
		public string IsoNumber { get { return _entity.ISONumber; } set { _entity.ISONumber = value; } }

		public string PhoneCode { get { return _entity.PhoneCode; } set { _entity.PhoneCode = value; } }
		public string ZipCodeRegExPattern { get { return _entity.ZipCodeRegExPattern; } set { _entity.ZipCodeRegExPattern = value; } }

		public byte IbanLength { get { return _entity.IBANLength; } set { _entity.IBANLength = value; } }
		public bool IsABARequired { get { return _entity.IsABARequired; } set { _entity.IsABARequired = value; } }
		public bool IsSepa { get { return _entity.IsSEPA; } set { _entity.IsSEPA = value; } }

        public string WorldRegionIsoCode { get { return _entity.WorldRegionISOCode; } set { _entity.WorldRegionISOCode = value; } }

		private Country(Dal.Netpay.CountryList entity)
		{ 
			_entity = entity;
		}
                

        public static Dictionary<string, Country> CacheByIso
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return Domain.Current.GetCachData("CountriesByIso", () => {
                    return new Domain.CachData((from c in Cache select c).ToDictionary(c => c.IsoCode2), DateTime.Now.AddHours(24));
                }) as Dictionary<string, Country>;
            }
        }

        public static List<Country> Cache
		{
			get
			{
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return Domain.Current.GetCachData("Countries", () => {
                    return new Domain.CachData((from c in DataContext.Reader.CountryLists select new Country(c)).ToList(), DateTime.Now.AddHours(24));
                }) as List<Country>;
			}
		}

		public static Country Get(int id)
		{
			return Cache.Where(c => c.ID == id).SingleOrDefault();
		}

		public static Country Get(string isoCode2)
		{
            // return Cache.Where(c => c.IsoCode2 == isoCode2).SingleOrDefault();
            if (isoCode2 == null || !CacheByIso.ContainsKey(isoCode2))
                return null;
            return CacheByIso[isoCode2];
		}

        public static string GetIsoCode(int id)
        {
            var v = Cache.Where(c => c.ID == id).SingleOrDefault();
            if (v == null) return null;
            return v.IsoCode2;
        }

		public static Country Get(System.Net.IPAddress ip)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            byte[] splitIP = ip.GetAddressBytes();
			long calculatedIP = (splitIP[0] * (long)System.Math.Pow(256, 3)) + (splitIP[1] * (long)System.Math.Pow(256, 2)) + (splitIP[2] * 256) + splitIP[3];
			string isoCode = (from gip in DataContext.Reader.tblGeoIPs where gip.GI_Start <= calculatedIP && gip.GI_End >= calculatedIP orderby gip.GI_Diff ascending select gip.GI_IsoCode).FirstOrDefault();
			if (isoCode == null) return null;
			return Get(isoCode);
		}

        public void Save()
        {
            if (Netpay.Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            DataContext.Writer.CountryLists.Update(_entity, true);
            DataContext.Writer.SubmitChanges();
        }

    }
}
