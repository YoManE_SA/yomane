﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll
{
	public class Region : BaseDataObject
	{
		private Dal.Netpay.WorldRegion _entity;

		public string IsoCode { get { return _entity.WorldRegionISOCode; } set { _entity.WorldRegionISOCode = value; } }
		public string Name { get { return _entity.Name; } set { _entity.Name = value; } }
        private List<Country> countriesCache;
        public List<Country> Countries
        {
            get
            {
                if (countriesCache == null)
                {
                    var countryIds = _entity.list_CountryLists.Select(cl => cl.CountryID.Value);
                    countriesCache = Country.Cache.Where(c => countryIds.Contains(c.ID)).ToList();
                }
                
                return countriesCache;
            }
        }

        public Region()
        {
            _entity = new Dal.Netpay.WorldRegion();
        }

        private Region(Dal.Netpay.WorldRegion entity)
		{ 
			_entity = entity;
		}

		public static List<Region> Cache
		{
			get
			{
                return Domain.Current.GetCachData("Regions", () => {
                    return new Domain.CachData((from r in DataContext.Reader.WorldRegions select new Region(r)).ToList(), DateTime.Now.AddHours(24));
                }) as List<Region>;
			}
		}

		public static Region Get(string isoCode)
		{
			return Cache.Where(c => c.IsoCode == isoCode).SingleOrDefault();
		}
	}
}
