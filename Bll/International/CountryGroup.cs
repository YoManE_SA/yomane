﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll
{
	public class CountryGroup : BaseDataObject
	{
		private Dal.Netpay.CountryGroup _entity;

		public int ID { get { return (int)_entity.CountryGroup_id; } }
		public string Name { get { return _entity.Name; } }

		private CountryGroup(Dal.Netpay.CountryGroup entity)
		{
			_entity = entity;
		}

		public static List<CountryGroup> Cache
		{
			get
			{
                return  Domain.Current.GetCachData("International.CountryGroup", () => {
                    return new Domain.CachData((from g in DataContext.Reader.CountryGroups select new CountryGroup(g)).ToList(), DateTime.Now.AddHours(5));
                }) as List<CountryGroup>;
			}
		}

		public static CountryGroup Get(int id)
		{
			return Cache.Where(c=> c.ID == id).SingleOrDefault();	
		}

		private List<Country> _countries;
		public List<Country> Countries
		{
			get{
				if (_countries != null) return _countries;
				_countries = Country.Cache.Where(x => (from c in DataContext.Reader.CountryListToCountryGroups where c.CountryGroup_id == _entity.CountryGroup_id select c.CountryISOCode).Contains(x.IsoCode2)).ToList();
				return _countries;
			}
		}


	}
}
