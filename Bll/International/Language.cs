﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll.International
{
	public class Language : BaseDataObject 
	{
		private Dal.Netpay.LanguageList _entity;

		public int ID { get { return (int)_entity.Language_id; } }
		public CommonTypes.Language EnumValue { get { return (CommonTypes.Language) _entity.Language_id; } }
		public string LanguageISOCode { get { return _entity.LanguageISOCode; } }
		public string IsoCode2 { get { return _entity.ISO2; } }
		public string IsoCode3 { get { return _entity.ISO3; } }
		public string Name { get { return _entity.Name; } }
		public string NativeName { get { return _entity.NativeName; } }
		public string Culture { get { return _entity.Culture; } }
		public bool IsRTL { get { return _entity.IsRTL; } }

		private Language(Dal.Netpay.LanguageList entity)
		{
			_entity = entity;
		}

		public static List<Language> Cache
		{
			get {
                return Domain.Current.GetCachData("Languages", () => {
                    return new Domain.CachData((from c in DataContext.Reader.LanguageLists select new Language(c)).ToList(), DateTime.Now.AddHours(24));
                }) as List<Language>;
			}
		}

		public static Language Get(int id)
		{
			return Cache.Where(c=> c.ID == id).SingleOrDefault();
		}

		public static Language Get(CommonTypes.Language id)
		{
			return Get((int)id);
		}

        public static CommonTypes.Language KnownLanguageFromCulture(string culture)
        {
            switch (culture.EmptyIfNull().TruncEnd(2).ToLower())
            {
                case "en": return CommonTypes.Language.English;
                case "he": return CommonTypes.Language.Hebrew;
                case "fr": return CommonTypes.Language.French;
                case "es": return CommonTypes.Language.Spanish;
                case "it": return CommonTypes.Language.Italian;
                case "pt": return CommonTypes.Language.Portuguese;
                case "lt": return CommonTypes.Language.Lithuanian;
                case "ru": return CommonTypes.Language.Russian;
                case "de": return CommonTypes.Language.German;
                case "zh": return CommonTypes.Language.Chinese;
            }
            return CommonTypes.Language.Unknown;
        }
    }
}
