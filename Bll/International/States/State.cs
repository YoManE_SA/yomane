﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll
{
	public class State : BaseDataObject
	{
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Bll.International.States.Module.Current, SecuredObjectName); } }


        private Dal.Netpay.StateList _entity;

		public int ID { get { return _entity.StateID.GetValueOrDefault(); } set { _entity.StateID = value; } }
		public string Name { get { return _entity.Name; } set { _entity.Name = value; } }
		public byte? SortOrder { get { return _entity.SortOrder; } set { _entity.SortOrder = value; } }
		public string IsoCode { get { return _entity.StateISOCode; } set { _entity.StateISOCode = value; } }
		public string CountryIsoCode { get { return _entity.CountryISOCode; } set { _entity.CountryISOCode = value; } }

		private State(Dal.Netpay.StateList entity)
		{ 
			_entity = entity;
		}

        public State()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            _entity = new Dal.Netpay.StateList();
        }

		public static List<State> Cache
		{
			get
			{
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return  Domain.Current.GetCachData("States", () => {
                    return new Domain.CachData((from c in DataContext.Reader.StateLists select new State(c)).ToList(), DateTime.Now.AddHours(24));
                }) as List<State>;
			}
		}

        //Function that gets the biggest value of StateID in the DB , and 
        //Retruns a value bigger in 1.
        public static int? GetNewStateIdValue()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            int? biggestStateId =  (from sl in DataContext.Reader.StateLists
                                    where sl.StateID.HasValue
                                    select sl.StateID.Value).Max();

            if (biggestStateId.HasValue)
            {
                return biggestStateId.Value + 1;
            }
            else
            {
                return null; 
            }
        }

        //Function that checks if a value of StateID exist in DB.
        public static bool CheckIfStateIdExistInDB(int stateid)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            int num = (from sl in DataContext.Reader.StateLists
                       where sl.StateID.Value == stateid
                       select sl).Count();

            return (num > 0);
        }

		public static State Get(int id)
		{
			return Cache.Where(c => c.ID == id).SingleOrDefault();
		}

        public void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            //Before saving check if the StateID exist in DB , if so
            //The item exist. else , not exsit.

            bool exist = CheckIfStateIdExistInDB(_entity.StateID.Value);
            DataContext.Writer.StateLists.Update(_entity, exist);
            DataContext.Writer.SubmitChanges();
        }

		public static State Get(string isoCode2)
		{
			return Cache.Where(c => c.IsoCode == isoCode2).SingleOrDefault();
		}

		public static string GetName(int id)
		{
			var item = Get(id);
			if (item == null) return null;
			return item.Name;
		}

		public static string GetName(string isoCode2)
		{
			var item = Get(isoCode2);
			if (item == null) return null;
			return item.Name;
		}

		public static List<State> GetForCountry(string countryIso)
		{
			return Cache.Where(c => c.CountryIsoCode == countryIso).ToList();
		}

		public static List<State> GetForCountry(int countryId)
		{
			var country = Country.Get(countryId);
			if (country == null) return new List<State>();
			return GetForCountry(country.IsoCode2);
		}

        public static bool CheckIfStateIsoCodeExist(string stateisocode)
        {
            if (Cache.Exists(x => x.IsoCode == stateisocode.ToUpper())) return true;
            else return false;
        }
	}
}
