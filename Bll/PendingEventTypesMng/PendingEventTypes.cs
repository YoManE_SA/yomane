﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll
{
    public class PendingEventTypes : BaseDataObject
    {
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(PendingEventTypesMng.Module.Current, SecuredObjectName); } }

        public class SearchFilters
        {
            public int? ID;
            public string Name;
            public int? Minutes;
        }

        private Dal.Netpay.EventPendingType _entity;

        public int ID { get { return _entity.EventPendingType_id; } set { } }
        public string Name { get { return _entity.Name; } set { _entity.Name = value; } }

        public byte? MinutesForWarning { get { return _entity.MinutesForWarning; } set { _entity.MinutesForWarning = value; } }

        private PendingEventTypes(Dal.Netpay.EventPendingType entity)
        {
            _entity = entity;
        }

        public PendingEventTypes()
        {
            _entity = new Dal.Netpay.EventPendingType();
        }

        public static List<PendingEventTypes> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var exp = (from ept in DataContext.Reader.EventPendingTypes select ept);
            if (filters!= null)
            {
                if (filters.ID != null) exp = exp.Where(ept => ept.EventPendingType_id == filters.ID);
                if (filters.Name != null) exp = exp.Where(ept => ept.Name == filters.Name);
                if (filters.Minutes != null) exp = exp.Where(ept => ept.MinutesForWarning == filters.Minutes);
            }
            return exp.ApplySortAndPage(sortAndPage).Select(ept => new PendingEventTypes(ept)).ToList();
        }

        public static PendingEventTypes Load(int id)
        {
            var Item = Search(new SearchFilters { ID = id } , null).SingleOrDefault();
            if (Item != null)
            {
                return Item;
            }
            else
            {
                return null;
            }
        }

        public void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
            {
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);
                if (_entity.EventPendingType_id == 0) ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);
            }

            DataContext.Writer.EventPendingTypes.Update(_entity, !(_entity.Name == null || _entity.Name == ""));
            DataContext.Writer.SubmitChanges();
        }

        public void Delete()
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Delete);

            if (_entity.Name == null || _entity.Name == "") return;
            DataContext.Writer.EventPendingTypes.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }
    }
}
