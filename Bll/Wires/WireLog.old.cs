﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Wires
{
	public class WireLog : BaseDataObject
	{
		//public const string SecuredObjectName = "WireFile";
		//public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Wires.Module.Current, SecuredObjectName); } }

		private Dal.Netpay.tblWireMoneyLog _entity;

		public int ID { get { return _entity.wireMoneyLog_id; } }
		public int WireID { get { return _entity.WireMoney_id; } }
		public string Description { get { return _entity.wml_description; } }
		public System.DateTime Date { get { return _entity.wml_date; } }
		public string UserName { get { return _entity.wml_user; } }

		private WireLog(Dal.Netpay.tblWireMoneyLog entity)
		{ 
			_entity = entity;
		}

		public static void AddLog(int wireId, string description)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Wire.SecuredObject, PermissionValue.Read);
			var entity = new Dal.Netpay.tblWireMoneyLog();
			entity.WireMoney_id = wireId;
			entity.wml_date = DateTime.Now;
			entity.wml_description = description;
			entity.wml_user = Login.Current.UserName;
			DataContext.Writer.tblWireMoneyLogs.InsertOnSubmit(entity);
			DataContext.Writer.SubmitChanges();
		}

		public static List<WireLog> Search(int wireId)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Wire.SecuredObject, PermissionValue.Read);
			return (from l in DataContext.Reader.tblWireMoneyLogs where l.WireMoney_id == wireId select new WireLog(l)).ToList();		
		}
	}
}
