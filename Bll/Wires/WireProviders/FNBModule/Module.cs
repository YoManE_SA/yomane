﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.Bll.Wires.WireProviders.FNBModule
{
    public class Module : Infrastructure.Module
    {
        public const string ModuleName = "WireProvider FNB";
        public override string Name { get { return ModuleName; } }
        public override decimal Version { get { return 1.0m; } }
        public static Module Current { get { return Infrastructure.Module.Get(ModuleName) as Module; } }

        protected override void OnInstall(EventArgs e)
        {
            base.OnInstall(e);
        }

        protected override void OnUninstall(EventArgs e)
        {
            base.OnUninstall(e);
        }
    }
}
