﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll.Wires
{
    public class FNB : Provider
    {
        private class FNBFileInfo
        {
            public string UserCode;
            public int VolumeNumber;
            public int TapeNumber;
            public int GenerationCode;

            public DateTime WireDate;
            public DateTime FirstDate;
            public DateTime LastDate;

            public int SectionStartTrans;
            public int CountSection;
            public int CountRow;
            public int CountContra;
            public int CountCredit;
            public int CountDebit;
            public int CountTrans { get { return CountCredit + CountDebit; } }
            public decimal TotalDebit;
            public decimal TotalCredit;
            public decimal TotalAmount { get { return TotalCredit - TotalDebit; } }

            public long AccountHashValues;
            public long AmountHashValues;
        }

        public static FNB Current { get { return Provider.Cache.Where(v => v is FNB).SingleOrDefault() as FNB; } }

        public override string Name { get { return "System.FNB"; } }
        public override string FriendlyName { get { return "FNB PACS FTP Wires"; } }
        public override bool IsOnline { get { return false; } }
        public override void Process(List<Wire> data, DateTime wireDate, string sourceAccountNumber)
        {
            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
            Logger.Log(LogTag.Wires, "Inside Process() method of FNB.");

            var batch = WiresBatch.CreateBatch(Name, sourceAccountNumber, CommonTypes.Currency.ZAR, data);
            var sourceAccount = batch.WireBankAccount;//WireBankAccount.Load(Name, sourceAccountNumber);
            if (sourceAccount == null)
            {
                var ex = new Exception(
                    string.Format("FNB Provider: Unable to find entry for {0} in the Finance.WireAccount table.", batch.SourceBankAccount));
                Logger.Log(ex);
                throw ex;
                //  sourceAccount = new WireBankAccount();
            }
            var total = new FNBFileInfo()
            {
                // K569 - TCN
                UserCode = sourceAccount.UserCode,// "K569",
                VolumeNumber = 0,
                GenerationCode = sourceAccount.NextWireBatchNum.HasValue ? (int)sourceAccount.NextWireBatchNum : 1,// NextBetchNumber,
                TapeNumber = batch.ID,
                WireDate = wireDate,
                FirstDate = data.Select(v => v.Date.GetValueOrDefault()).Min(),
                LastDate = data.Select(v => v.Date.GetValueOrDefault()).Max()
            };
            string fileName = Wire.GenerateOfflineProviderFileName(Name, "BATCH_" + batch.ID.ToString("0000000") + @"\FTPTCPIP", "ACBFILE1");
            using (var fw = new System.IO.StreamWriter(fileName, false))
            {
                WriteFileHeader(fw, total);
                WriteUserHeader(fw, total);
                foreach (var w in data)
                    WriteWireRecord(fw, total, w, sourceAccount);
                WriteContra(fw, total, sourceAccount);
                WriteUserTrailer(fw, total);
                WriteFileTrailer(fw, total);
                fw.Close();
            }

            #region Make endite file uppercase for fintegrate

            string strFileContents = System.IO.File.ReadAllText(fileName);
            System.IO.File.WriteAllText(fileName, strFileContents.ToUpper());

            #endregion

            CreateHashFile(System.IO.Path.ChangeExtension(fileName, "chash"), total);
            CreateConfirmFile(System.IO.Path.ChangeExtension(fileName, "CONFIRM1"), total);
            batch.SetPrepared(fileName, true);

            //SendSFTPFile(fileName);
            //compress to single file - only available since .net 4.5
        }

        public List<string> GetRelatedBatchFiles(int? batchId)
        {
            string folderName = null;
            if (batchId.HasValue)
                folderName = Wire.MapOfflineFile(Name, string.Format("BATCH_" + batchId.Value.ToString("0000000")));
            else
            {
                var dirInfo = new System.IO.DirectoryInfo(Wire.MapOfflineFile(Name));
                if (dirInfo != null && dirInfo.Exists)
                    folderName = dirInfo.GetDirectories("*.*", System.IO.SearchOption.TopDirectoryOnly).OrderByDescending(f => f.CreationTime).FirstOrDefault()?.FullName;
            }
            if (!System.IO.Directory.Exists(folderName)) return new List<string>();
            var ret = System.IO.Directory.GetFiles(folderName).ToList();
            var primaryFile = ret.Where(v => System.IO.Path.GetFileName(v) == "FTPTCPIP.ACBFILE1").SingleOrDefault();
            if (primaryFile != null) { ret.Remove(primaryFile); ret.Insert(0, primaryFile); }
            return ret;
        }

        public override List<string> LastRelatedBatchFiles { get { return GetRelatedBatchFiles(null); } }

        private void WriteFileHeader(System.IO.StreamWriter fw, FNBFileInfo info)
        {
            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
            Logger.Log(LogTag.Wires, "Inside WriteFileHeader() method of FNB");

            info.CountRow++;
            fw.Write("02");     //Record Identifier
            fw.Write(info.VolumeNumber.ToString("0000")); //Volume Number, Can be filled with anything, or left blank
            fw.Write(info.TapeNumber.ToString("00000000")); //Tape Serial Number, Blank Spaces
            fw.Write(info.UserCode);   //Installation ID Code From, User Code
            fw.Write("0021");   //Installation ID Code To
            fw.Write(info.WireDate.ToString("yyMMdd"));   //Creation Date, Date the file is created.
            fw.Write(info.LastDate.ToString("yyMMdd"));   //Purge Date, Equal or greater than the last action date within the entire data set
            fw.Write(info.GenerationCode.ToString("0000"));   //Installation Generation Number??
            fw.Write("1800");   //Block Length
            fw.Write("0180");   //Record Length
            fw.Write("MAGTAPE".PadRight(10));   //Record Length
            fw.Write("".PadRight(8, ' '));       //filler
            fw.WriteLine("".PadRight(116, ' ')); //filler

        }
        private void WriteFileTrailer(System.IO.StreamWriter fw, FNBFileInfo info)
        {
            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
            Logger.Log(LogTag.Wires, "Inside WriteFileTrailer() method of FNB.");

            info.CountRow++;
            fw.Write("94");     //Record Identifier
            fw.Write(info.VolumeNumber.ToString("0000")); //Volume Number, Can be filled with anything, or left blank
            fw.Write(info.TapeNumber.ToString("00000000")); //Tape Serial Number, Blank Spaces
            fw.Write(info.UserCode);   //Installation ID Code From, User Code
            fw.Write("0021");   //Installation ID Code To
            fw.Write(info.WireDate.ToString("yyMMdd"));   //Creation Date, Date the file is created.
            fw.Write(info.LastDate.ToString("yyMMdd"));   //Purge Date, Equal or greater than the last action date within the entire data set
            fw.Write(info.GenerationCode.ToString("0000"));   //Installation Generation Number??
            fw.Write("1800");   //Block Length
            fw.Write("0180");   //Record Length
            fw.Write("MAGTAPE".PadRight(10));   //Record Length
            fw.Write(System.Math.Ceiling(info.CountRow / 10.0d).ToString("000000"));   //Block Count
            fw.Write(info.CountRow.ToString("000000")); //Record Count
            fw.Write((info.CountSection * 2).ToString("000000")); //User Header / Trailer Count
            fw.WriteLine("".PadRight(106, ' ')); //filler
        }

        private void WriteUserHeader(System.IO.StreamWriter fw, FNBFileInfo info)
        {
            info.CountRow++;
            info.SectionStartTrans = info.CountTrans + 1;
            fw.Write("04");     //Record Identifier
            fw.Write(info.UserCode);   //User Code
            fw.Write(info.WireDate.ToString("yyMMdd"));   //Creation Date
            fw.Write(info.LastDate.ToString("yyMMdd"));   //Purge Date, Equal to the last action date in the file
            fw.Write(info.FirstDate.ToString("yyMMdd"));   //First Action Date, First action date on the standard transaction record within the respective user data set.
            fw.Write(info.FirstDate.ToString("yyMMdd"));   //Last action date on the standard transaction record within the respective user data set.
            fw.Write(info.SectionStartTrans.ToString("000000"));   //Equal to the User Sequence Number of the first Standard Transaction Record following this User Header Record.
            fw.Write(info.GenerationCode.ToString("0000"));      //User Generation Number, Helpdesk to inform, increments by one on successful transmission. Note: this can also be named TEST should you be sending test data.
            fw.Write("SAMEDAY".PadRight(10, ' '));      //Type of Service
            fw.WriteLine("".PadRight(130, ' ')); //filler
        }

        private void WriteUserTrailer(System.IO.StreamWriter fw, FNBFileInfo info)
        {
            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
            Logger.Log(LogTag.Wires, "Inside WriteUserTrailer() method of FNB");

            info.CountRow++;
            info.CountSection++;
            fw.Write("92");     //Record Identifier
            fw.Write(info.UserCode);   //User Code
            fw.Write(info.SectionStartTrans.ToString("000000"));   //First Sequence Number
            fw.Write(info.CountTrans.ToString("000000")); //Last Sequence Number
            fw.Write(info.FirstDate.ToString("yyMMdd"));   //First Action Date, Same as the User Header in the same Data Set.
            fw.Write(info.LastDate.ToString("yyMMdd"));   //Last Action Date, Same as the User Header in the same Data Set.

            fw.Write(info.CountDebit.ToString("000000")); //No. of Debit Records
            fw.Write(info.CountCredit.ToString("000000")); //No.of Credit Records
            fw.Write(info.CountContra.ToString("000000")); //No.of Contra Records

            fw.Write(((int)(info.TotalDebit * 100)).ToString("000000000000")); //No.of Contra Records
            fw.Write(((int)(info.TotalCredit * 100)).ToString("000000000000")); //No.of Contra Records
            fw.Write(info.AccountHashValues.ToString().TruncStart(12).PadLeft(12, '0')); //Hash Total of Homing Account Numbers????
            fw.WriteLine("".PadRight(96, ' ')); //filler
            info.CountRow++;
        }

        private void WriteWireRecord(System.IO.StreamWriter fw, FNBFileInfo info, Wire wire, WireBankAccount account)
        {
            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
            Logger.Log(LogTag.Wires, "Inside WriteWireRecord() method of FNB");

            if (wire.BankAccount == null)
            {
                string message = string.Format("BankAccount with currency {0} wasn't found for accountID {1}", wire.Currency.ToString(), wire.AccountID.ToString());
                throw new Exception(message);
            }

            info.CountRow++;
            info.AccountHashValues += (wire.BankAccount.AccountNumber.TruncEnd(11).ToNullableLong().GetValueOrDefault() + 0/*Non Standard Account Number*/);
            info.AmountHashValues +=
                (wire.BankAccount.AccountNumber.EmptyIfNull().TruncEnd(11).ToNullableLong().GetValueOrDefault() * (int)(wire.Amount * 100)) +
                (0 * (int)(wire.Amount * 100));

            if (wire.Amount > 0) { info.CountCredit++; info.TotalCredit += wire.Amount; }
            else { info.CountDebit++; info.TotalDebit += -wire.Amount; }
            fw.Write("10");     //Record Identifier
            fw.Write(account.AccountBranch.EmptyIfNull().TruncEnd(6).PadLeft(6, '0'));  //User Branch
            fw.Write(account.AccountNumber.EmptyIfNull().TruncEnd(11).PadLeft(11, '0'));//User Nominated Account Number??
            fw.Write(info.UserCode);   //User Code
            fw.Write(info.CountTrans.ToString("000000")); //User Sequence Number.
            fw.Write(wire.BankAccount.ABA.EmptyIfNull().TruncEnd(6).PadLeft(6, '0')); //Homing Branch??
            fw.Write(wire.BankAccount.AccountNumber.EmptyIfNull().PadLeft(11, '0')); // Homing Account Number??
            fw.Write("1"); //Type of Account
            fw.Write(((int)(System.Math.Abs(wire.Amount) * 100)).ToString("00000000000")); //Amount
            fw.Write(wire.Date.GetValueOrDefault().ToString("yyMMdd")); //Action Date
            fw.Write("88"); //Entry Class?? //XX
            fw.Write("0"); //Tax Code??
            fw.Write("00"); //Filler
            fw.Write("0"); //Filler
            try
            {
                int? nMerchantGroupID = Merchants.Merchant.Load(Settlements.Settlement.Load(wire.MerchantSettlementID.Value).CompanyID).MerchantGroup;
                if (nMerchantGroupID.HasValue && (nMerchantGroupID.Value == 1 || nMerchantGroupID == 2))
                    fw.Write(wire.ID.ToString("00000000") + "  " + ("OneSp " + wire.Date.GetValueOrDefault().ToString("yyyyMMdd")) + wire.Date.GetValueOrDefault().ToString("yyMMdd")); //User Reference
                else
                    fw.Write(wire.ID.ToString("00000000") + "  " + ("YOMANE " + wire.Date.GetValueOrDefault().ToString("yyyyMMdd")) + wire.Date.GetValueOrDefault().ToString("yyMMdd")); //User Reference
            }
            catch
            {
                fw.Write(wire.ID.ToString("00000000") + "  " + ("YOMANE " + wire.Date.GetValueOrDefault().ToString("yyyyMMdd")) + wire.Date.GetValueOrDefault().ToString("yyMMdd")); //User Reference
            }
            fw.Write(wire.BankAccount.AccountName.EmptyIfNull().PadRight(30)); //Homing Account Name??
            fw.Write("".PadLeft(20, '0')); //Non Standard Account Number??
            fw.Write("".PadLeft(16)); //Filler
            fw.Write("21"); //Homing Institution
            fw.WriteLine("".PadLeft(12)); //Filler
        }

        private void WriteContra(System.IO.StreamWriter fw, FNBFileInfo info, WireBankAccount account)
        {
            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
            Logger.Log(LogTag.Wires, "Inside WriteContra() method of FNB");

            info.CountRow++;
            info.CountContra++;
            decimal wireTotal = info.TotalAmount;
            info.AccountHashValues += account.AccountNumber.EmptyIfNull().TruncEnd(11).ToNullableLong().GetValueOrDefault();
            info.AmountHashValues += (account.AccountNumber.EmptyIfNull().TruncEnd(11).ToNullableLong().GetValueOrDefault() * (int)(wireTotal * 100));
            if (wireTotal < 0) { info.CountCredit++; info.TotalCredit += -wireTotal; } else { info.CountDebit++; info.TotalDebit += wireTotal; }

            fw.Write("12");     //Record Identifier
            fw.Write(account.AccountBranch.EmptyIfNull().TruncEnd(6).PadLeft(6, '0'));  //User Branch
            fw.Write(account.AccountNumber.EmptyIfNull().TruncEnd(11).PadLeft(11, '0'));     //User Nominated Account Number??
            fw.Write(info.UserCode);   //User Code
            fw.Write(info.CountTrans.ToString("000000")); //User Sequence Number.
            fw.Write(account.AccountBranch.EmptyIfNull().TruncEnd(6).PadLeft(6, '0')); //Homing Branch??
            fw.Write(account.AccountNumber.EmptyIfNull().TruncEnd(11).PadLeft(11, '0')); // Homing Account Number??
            fw.Write("1"); //Type of Account
            fw.Write((wireTotal * 100).ToString("00000000000")); //Amount
            fw.Write(info.WireDate.ToString("yyMMdd")); //Action Date
            fw.Write("10"); //Entry Class
            fw.Write("0000"); //Filler
            fw.Write(account.AccountName.EmptyIfNull().TruncEnd(10) + "CONTRA".PadRight(14) + info.WireDate.ToString("yyMMdd")); //User Reference
            fw.Write(account.AccountName.EmptyIfNull().TruncEnd(30).PadRight(30)); //Nominated Account Name
            fw.WriteLine("".PadLeft(50)); //Filler
        }

        private void CreateHashFile(string fileName, FNBFileInfo info)
        {
            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
            Logger.Log(LogTag.Wires, "Inside CreateHashFile() method of FNB. FileName - " + fileName);

            using (var fw = new System.IO.StreamWriter(fileName))
            {
                fw.Write(info.UserCode); //User Code
                fw.Write(info.GenerationCode.ToString("0000")); //User Generation Number
                fw.Write("92"); //User Trailer Record ID
                fw.Write(info.UserCode); //User Code
                fw.Write("000001"); //First sequence Number
                fw.Write(info.CountTrans.ToString("000000")); //Last Sequence Number
                fw.Write(info.FirstDate.ToString("yyMMdd")); //First Action Date
                fw.Write(info.LastDate.ToString("yyMMdd"));   //Last Action Date
                fw.Write(info.CountDebit.ToString("000000"));  //No.of Debit Records
                fw.Write(info.CountCredit.ToString("000000")); //No.of Credit Records
                fw.Write(info.CountContra.ToString("000000")); //No.of Contra Records
                fw.Write(((int)(info.TotalDebit * 100)).ToString("000000000000")); //Total Debit Value
                fw.Write(((int)(info.TotalCredit * 100)).ToString("000000000000")); //Total Credit Value
                fw.Write(info.AccountHashValues.ToString().TruncStart(12).PadLeft(12, '0')); //Hash Total of Homing Account Numbers
                fw.WriteLine(info.AmountHashValues.ToString().TruncStart(18).PadLeft(18, '0')); //Account-by-Account Hash Total
                fw.Close();
            }
        }

        private void CreateConfirmFile(string fileName, FNBFileInfo info)
        {
            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
            Logger.Log(LogTag.Wires, "Inside CreateConfirmFile() method of FNB. FileName - " + fileName);

            using (var fw = new System.IO.StreamWriter(fileName))
            {
                fw.Write("?"); //Confirmation Indicator
                fw.Write(info.UserCode); //User Code
                fw.WriteLine(info.GenerationCode.ToString("0000")); //User Generation Number
                fw.Close();
            }
        }

        private void UpdateConfirmFile(string fileName, bool isOk)
        {
            using (var f = System.IO.File.Open(fileName, System.IO.FileMode.Open, System.IO.FileAccess.Write))
            {
                f.Write(new byte[] { (isOk ? (byte)'Y' : (byte)'N') }, 0, 1);
                f.Close();
            }
        }

        private bool parseResults(WiresBatch batch)
        {
            var serverFnbFile = System.IO.Path.ChangeExtension(Domain.Current.MapPrivateDataPath(@"Wires\BatchFiles\" + batch.FileName), "ACBFILE1.FNBFILE");
            var data = batch.GetWires();
            bool allFileFailed = false;
            var failIds = new List<int>();
            using (var sr = new System.IO.StreamReader(serverFnbFile))
            {
                while (!sr.EndOfStream)
                {
                    var line = sr.ReadLine();
                    if (line.StartsWith("21"))
                    { //not paied
                        int wireId = line.Substring(66, 8).ToNullableInt().GetValueOrDefault();
                        failIds.Add(wireId);
                    }
                    else if (line.StartsWith("22") || line.StartsWith("05"))
                    {
                        allFileFailed = true;
                    }
                }
                sr.Close();
            }
            if (allFileFailed) failIds = data.Select(v => v.ID).ToList();
            foreach (var v in failIds)
            {
                var wire = data.Where(x => x.ID == v).SingleOrDefault();
                if (wire != null)
                {
                    wire.BatchFailed = true;
                    wire.Reset(string.Format("Batch Rejected: {0}", batch.ID));
                }
            }
            return allFileFailed;
        }

        public override void Send(WiresBatch batch)
        {
            if (batch.WireBankAccount == null)
            {
                var ex = new Exception(
                    string.Format("FNB Provider: Unable to find entry for {0} in the Finance.WireBankAccount table.", batch.SourceBankAccount));
                Logger.Log(ex);
                throw ex;
            }
            string fileName = Domain.Current.MapPrivateDataPath(@"Wires\BatchFiles\" + batch.FileName);
            var clientHashFile = System.IO.Path.ChangeExtension(fileName, "chash");
            var serverHashFile = System.IO.Path.ChangeExtension(fileName, "FNBHASH");
            var confirmFile = System.IO.Path.ChangeExtension(fileName, "CONFIRM1");
            var serverFnbFile = System.IO.Path.ChangeExtension(fileName, "ACBFILE1.FNBFILE");
            var serverFnbLoadFile = System.IO.Path.ChangeExtension(fileName, "ACBFILE1.FNBLOAD");
            var serverTsecFile = System.IO.Path.ChangeExtension(fileName, "TS70FIL.SEC1");
            var errorResult = System.IO.Path.ChangeExtension(fileName, "ACBFILE1.ERROR");

            var wireBankAccount = batch.WireBankAccount;

            var winscpLogin = new WinSCP.SessionOptions()
            {
                Protocol = WinSCP.Protocol.Sftp,
                HostName = ServerUrl,
                UserName = wireBankAccount.UserName,
                Password = Password,
                GiveUpSecurityAndAcceptAnySshHostKey = true,
                // Temp Hijack to test settlements
                SshPrivateKeyPath = Domain.Current.MapPrivateDataPath("FNB\\" + wireBankAccount.UserName + "\\PrivateKey.ppk"),
                SshPrivateKeyPassphrase = wireBankAccount.SshPrivateKeyPassphrase// "p4s5w0rD"

            };
            using (var session = new WinSCP.Session())
            {
                session.DisableVersionCheck = true;
                session.SessionLogPath = Domain.Current.MapPrivateDataPath(@"Logs\FNBWireSession_" + DateTime.Now.ToString("yyyy_MM_dd") + ".log");
                //session.XmlLogPath = Domain.Current.MapTempPath("FNBXmlLogPath.xml"); // System.IO.Path.GetDirectoryName(Infrastructure.Application.GetParameter("sftpExecutablePath")) + @"\";
                session.ExecutablePath = System.IO.Path.ChangeExtension(Infrastructure.Application.GetParameter("sftpExecutablePath"), ".exe");
                try
                {
                    session.Open(winscpLogin);
                    WinSCP.TransferOperationResult res;
                    //detect nect fileName
                    int fileGeneratedNumber = 0;
                    string topFnbFileName = "";
                    try
                    {
                        var files = session.EnumerateRemoteFiles("/", "FTPTCPIP.FNBHASH.G*", WinSCP.EnumerationOptions.None);
                        string topFileName = (from f in files where f.Name.Contains("FTPTCPIP.FNBHASH.") orderby f.LastWriteTime descending, f.Name descending select f.Name).FirstOrDefault();
                        if (!string.IsNullOrWhiteSpace(topFileName)) fileGeneratedNumber = topFileName.Substring(topFileName.Length - 7, 4).ToNullableInt().GetValueOrDefault(0);
                        fileGeneratedNumber++;
                        files = session.EnumerateRemoteFiles("/", "FTPTCPIP.FNBFILE.G*", WinSCP.EnumerationOptions.None);
                        topFnbFileName = (from f in files where f.Name.Contains("FTPTCPIP.FNBFILE.G") orderby f.LastWriteTime descending, f.Name descending select f.Name).FirstOrDefault();
                    }
                    catch
                    {
                        Logger.Log(LogSeverity.Info, LogTag.Wires, "Retrieving file number failed, defaulting to 1");
                    }
                    //start sending 
                    if (!(res = session.PutFiles(fileName, "FTPTCPIP.ACBFILE1", false, new WinSCP.TransferOptions() { TransferMode = WinSCP.TransferMode.Ascii })).IsSuccess)
                        throw new Exception("Send file FTPTCPIP.ACBFILE1 failed", res.Failures.First());
                    IncBetchNumber(10000);
                    //session.GetFiles(serverFnbFile, fileName + "_1", false, new WinSCP.TransferOptions() { TransferMode = WinSCP.TransferMode.Ascii });
                    for (int tryCount = 0; tryCount < 10; tryCount++)
                    {
                        try { System.Threading.Thread.Sleep(30 * 1000); }
                        catch { }
                        var searchFileName = "";
                        var hashfiles = session.EnumerateRemoteFiles("/", "FTPTCPIP.FNBHASH.G*", WinSCP.EnumerationOptions.None);
                        try
                        {
                            searchFileName = (from f in hashfiles where f.Name.Contains(string.Format("FTPTCPIP.FNBHASH.G{0:0000}V00", fileGeneratedNumber)) orderby f.LastWriteTime descending, f.Name descending select f.Name).FirstOrDefault();
                        }
                        catch
                        {
                            Logger.Log(LogSeverity.Info, LogTag.Wires, "Cannot find " + string.Format("FTPTCPIP.FNBHASH.G{0:0000}V00", fileGeneratedNumber) + " on server");
                            continue;
                        }
                        //var searchFileName = ("FTPTCPIP.FNBHASH.G0011V00");
                        if (!string.IsNullOrEmpty(searchFileName))
                            if ((res = session.GetFiles(searchFileName, serverHashFile, false, new WinSCP.TransferOptions() { TransferMode = WinSCP.TransferMode.Ascii })).IsSuccess)
                                break;
                        //throw new Exception("Get file " + topFileName + " failed", res.Failures.First());
                    }
                    if (!System.IO.File.Exists(serverHashFile)) throw new Exception("server hash file not exist (" + System.IO.Path.GetFileName(serverHashFile) + ")");
                    var hashOk = System.IO.File.ReadAllText(clientHashFile).Trim() == System.IO.File.ReadAllText(serverHashFile).Trim();
                    Logger.Log(LogSeverity.Info, LogTag.Wires, "hashOk " + hashOk.ToString() + "\r\n<br>clientHashFile:" + System.IO.File.ReadAllText(clientHashFile).Trim() + "\r\n<br>serverHashFile:" + System.IO.File.ReadAllText(serverHashFile).Trim());
                    UpdateConfirmFile(confirmFile, hashOk);
                    if (!(res = session.PutFiles(confirmFile, "FTPTCPIP.CONFIRM1", false, new WinSCP.TransferOptions() { TransferMode = WinSCP.TransferMode.Ascii })).IsSuccess)
                        throw new Exception("Put file " + confirmFile + " failed", res.Failures.First());

                    if (!string.IsNullOrWhiteSpace(topFnbFileName)) fileGeneratedNumber = topFnbFileName.Substring(topFnbFileName.Length - 7, 4).ToNullableInt().GetValueOrDefault(0);
                    fileGeneratedNumber++;

                    for (int tryCount = 0; tryCount < 20; tryCount++)
                    {
                        var allfiles = session.EnumerateRemoteFiles("/", "*.*", WinSCP.EnumerationOptions.None);

                        var fnbfile = (from f in allfiles where f.Name.Contains(string.Format("FTPTCPIP.FNBLOAD.G{0:0000}V00", fileGeneratedNumber)) orderby f.LastWriteTime descending, f.Name descending select f.Name).FirstOrDefault();

                        if (string.IsNullOrEmpty(fnbfile))
                        {
                            Logger.Log(LogTag.Wires, "Could not find '" + string.Format("FTPTCPIP.FNBLOAD.G{0:0000}V00", fileGeneratedNumber) + "' sleeping 30 seconds (" + tryCount.ToString() + ")");
                            System.Threading.Thread.Sleep(30 * 1000);
                            continue;
                        }

                        //try { System.Threading.Thread.Sleep(30 * 1000); } catch { }
                        res = session.GetFiles(fnbfile, serverFnbLoadFile, false, new WinSCP.TransferOptions() { TransferMode = WinSCP.TransferMode.Ascii });
                        res = session.GetFiles(string.Format("FTPTCPIP.TS70FIL.SEC1.G{0:0000}V00*", fileGeneratedNumber), serverTsecFile, false, new WinSCP.TransferOptions() { TransferMode = WinSCP.TransferMode.Ascii });
                        res = session.GetFiles("FTPTCPIP.ACBFILE1.ERROR", errorResult, false, new WinSCP.TransferOptions() { TransferMode = WinSCP.TransferMode.Ascii });
                        if ((res = session.GetFiles(string.Format("FTPTCPIP.FNBFILE.G{0:0000}V00*", fileGeneratedNumber), serverFnbFile, false, new WinSCP.TransferOptions() { TransferMode = WinSCP.TransferMode.Ascii })).IsSuccess)
                            break;
                    }
                    if (!System.IO.File.Exists(serverFnbFile)) throw new Exception("server result file not exist(" + string.Format("FTPTCPIP.FNBLOAD.G{0:0000}V00*", fileGeneratedNumber) + ")");
                    parseResults(batch);
                }
                finally
                {
                    if (session.Opened) session.Close();
                }
            }
        }

    }
}
