﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.Wires
{
    public class Manual : Provider
	{
        public override string Name { get { return "System.Manual"; } }
        public override string FriendlyName { get { return "Manual"; } }
        public override bool IsOnline { get { return true; } }
        public override bool IsCancelable { get { return true; } }
        public override Dictionary<string, string> GetAccounts() { return null; }
        public override void Cancel(Wire data) { }
        public override void Process(List<Wire> data, DateTime wireDate, string sourceAccountNumber) { }
	}
}
