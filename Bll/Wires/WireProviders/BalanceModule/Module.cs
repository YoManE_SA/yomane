﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.Bll.Wires.WireProviders.BalanceModule
{
    public class Module : Infrastructure.Module
    {
        public const string ModuleName = "WireProvider Balance";
        public override string Name { get { return ModuleName; } }
        public override decimal Version { get { return 1.0m; } }
        public static Module Current { get { return Infrastructure.Module.Get(ModuleName) as Module; } }

        protected override void OnInstall(EventArgs e)
        {
            Balance.RegisterTypes();
            base.OnInstall(e);
        }

        protected override void OnUninstall(EventArgs e)
        {
            //This line is commented because in the table List.BalanceSourceType there is a column "BalanceSourceType_id"
            //Which is a FK in the Data.AccountBalance table , so when trying to delete a row in List.BalanceSourceType table
            //An exception is thrown because in Data.AccountBalance table there are entities that point to List.BalanceSourceType - column BalanceSourceType_id
            //Balance.UnregisterTypes();

            base.OnUninstall(e);
        }
    }
}
