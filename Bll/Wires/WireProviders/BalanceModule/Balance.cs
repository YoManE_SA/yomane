﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.Wires
{
    public class Balance : Provider
    {
        public override string Name { get { return "System.Balance"; } }
        public override string FriendlyName { get { return "Insert into balance"; } }
        public override bool IsOnline { get { return true; } }
        public override bool IsCancelable { get { return true; } }
        public override Dictionary<string, string> GetAccounts() { return null; }

        public const string BALANCE_SOURCE_WIRE = "Wires.Wire";
        public const string BALANCE_SOURCE_SETTLEMENT_FEE = "Wires.SettlementFee";
        public const string BALANCE_SOURCE_SETTLEMENT = "Wires.Settlement";
        public const string BALANCE_SOURCE_AFF_SETTLEMENT = "Wires.AffiliateSettlement"; //need to remove when upgrading settlements
        public const string BALANCE_SOURCE_PAYMENTREQUEST = "Wires.PaymentRequest"; //need to change to wire after removing payment requests

        internal static void RegisterTypes()
        {
            Accounts.Balance.BalanceSource.Register(new Accounts.Balance.BalanceSource(BALANCE_SOURCE_SETTLEMENT, "Settlement", false));
            Accounts.Balance.BalanceSource.Register(new Accounts.Balance.BalanceSource(BALANCE_SOURCE_SETTLEMENT_FEE, "Settlement Wire Fee", true));
            Accounts.Balance.BalanceSource.Register(new Accounts.Balance.BalanceSource(BALANCE_SOURCE_AFF_SETTLEMENT, "Affiliate Settlement", false));
            Accounts.Balance.BalanceSource.Register(new Accounts.Balance.BalanceSource(BALANCE_SOURCE_PAYMENTREQUEST, "Payment Request", false));
            Accounts.Balance.BalanceSource.Register(new Accounts.Balance.BalanceSource(BALANCE_SOURCE_WIRE, "Wire", false));
        }

        internal static void UnregisterTypes()
        {
            Accounts.Balance.BalanceSource.Unregister(BALANCE_SOURCE_SETTLEMENT);
            Accounts.Balance.BalanceSource.Unregister(BALANCE_SOURCE_AFF_SETTLEMENT);
            Accounts.Balance.BalanceSource.Unregister(BALANCE_SOURCE_PAYMENTREQUEST);
            Accounts.Balance.BalanceSource.Unregister(BALANCE_SOURCE_WIRE);
        }

        private void GetTypeAndRef(Wire w, out string text, out string balanceType, out int? refId)
        {
            text = "";
            if (w.MerchantSettlementID.HasValue) {
                balanceType = BALANCE_SOURCE_SETTLEMENT;
                refId = w.MerchantSettlementID;
                text = string.Format("Settlement #{0}, ", refId);
            } else if (w.AffiliateSettlementID.HasValue) {
                balanceType = BALANCE_SOURCE_AFF_SETTLEMENT;
                refId = w.AffiliateSettlementID;
                text = string.Format("Aff-Settlement #{0}, ", refId);
            } else if (w.PayeeID.HasValue) {
                balanceType = BALANCE_SOURCE_PAYMENTREQUEST;
                refId = w.AffiliateSettlementID;
                text = string.Format("Payment Request #{0}, ", refId);
            } else {
                balanceType = BALANCE_SOURCE_WIRE; 
                refId = w.ID;
            }
            text += string.Format("Wire #{0}", w.ID);
        }

        public static decimal GetWireFee(int merchantId, string currencyIso, decimal wireAmount)
        {
            var wireFees = (from n in Infrastructure.DataContext.Reader.SetMerchantSettlements where n.Merchant_id == merchantId && n.Currency_id == Currency.Get(currencyIso).ID select n).SingleOrDefault();
            return wireFees.WireFee - (wireAmount * (wireFees.WireFeePercent / 100));
            //if (feeAmount != 0) Create(accountID, currencyIso, -feeAmount, "Bank transfer", SOURCE_WIRE_FEE, null, false, false);
        }

        public override void Process(List<Wire> data, DateTime wireDate, string sourceAccountNumber)
		{
            foreach (var w in data) {
                string balanceType, text; int? refId;
                GetTypeAndRef(w, out text, out balanceType, out refId);
                Accounts.Balance.Create(w.AccountID, Currency.Get(w.ProcessingCurrency).IsoCode, w.TransferAmount, text, balanceType, refId, false);
            }
        }

        public override void Cancel(Wire wire)
		{
            string balanceType, text; int? refId;
            GetTypeAndRef(wire, out text, out balanceType, out refId);
            Accounts.Balance.Create(wire.AccountID, Currency.Get(wire.ProcessingCurrency).IsoCode, -wire.TransferAmount, "Cancel - " + text, balanceType, refId, false);
		}
	}
}
