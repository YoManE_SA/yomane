﻿using Netpay.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.Wires
{
	public class Masav : Provider
	{
        public override string Name { get { return "System.Masav"; } }
        public override string FriendlyName { get { return "MASAV"; } }
        public override bool IsOnline { get { return false; } }
        public override bool IsCancelable { get { return false; } }
		public override void Cancel(Wire data) { throw new NotImplementedException(); }


        public override void Process(List<Wire> data, DateTime wireDate, string sourceAccountNumber)
		{
			string masavCode = sourceAccountNumber;
			int rowCount = 0;
			decimal rowTotal = 0;
			var sb = new StringBuilder();
			sb.AppendFormat("K{0}00{1}00010{1}65829000000{2:30}{3}KOT\r\n\r\n", masavCode, wireDate.ToString("yyMMdd"), "Netpay", new string(' ', 56));
		
			foreach (var w in data) {
				decimal wireAmount = w.TransferAmount;	
				//if (w.Currency != CommonTypes.Currency.ILS) wireAmount = (decimal)(wireAmount * w.ExchangeRate); //new Money(wireAmount, w.Currency).ConvertTo(domain.Host, CommonTypes.Currency.ILS) + 

				//string payeeID = w.CompanyLegalNumber;
				//if (string.IsNullOrEmpty(payeeID)) payeeID = w.IDnumber;

				var toAccount = new IsraelBankAccount(w.BankAccount);

				sb.AppendFormat("1{0}00000000", masavCode);
				sb.AppendFormat("{0:2}", toAccount.ILBankCode); //bank code
				sb.AppendFormat("{0:3}", toAccount.ILBranchCode); //branch code
				sb.AppendFormat("0000"); //account type
				sb.AppendFormat("{0:9}", toAccount.ILAccountNumber.TrimStart(new char[] { '0' })); //account number
				sb.AppendFormat("0");
                if (w.PayeeID != null) {
                    var payee = Accounts.Payee.Load(w.PayeeID.Value);
                    sb.Append(payee.LegalNumber.PadLeft(9, '0')); //payee id
                    sb.Append(payee.CompanyName.PadLeft(17, ' ')); //payee name
                } else {
                    sb.Append(toAccount.AccountNumber.PadLeft(9, '0')); //payee id
                    sb.Append(toAccount.AccountName.PadLeft(17, ' ')); //payee name
                }
                sb.Append(wireAmount.ToString("00000000000.00")); //amount
				sb.Append(w.ID.ToString().PadLeft(20, '0')); // rsData("CustomerNumber")
				sb.Append("03090308"); //payment period
				sb.AppendFormat("000006{0}  \r\n\r\n", new string('0', 18));
				/*
				'Add to masav log
				sPayeeBankDetails = trim(rsData("wirePaymentAccount")) & ", " & trim(rsData("wirePaymentBranch")) & ", " & trim(rsData("bankCode"))
				sSQL="INSERT INTO tblLogMasavDetails (Company_id, WireMoney_id, PayeeName, PayeeBankDetails, logMasavFile_id, Amount) " &_
					"VALUES(" & rsData("Company_id") & "," & rsData("WireMoney_id") & ", '" & rsData("wirePaymentPayeeName") & "', '" & sPayeeBankDetails & "', " & Session("WireAvailCur")(0) & " ," & sTransPay & ")"
				oledbData.Execute sSQL
				oledbData.Execute "Update tblLogMasavFile Set PayCount=PayCount+1, PayAmount=PayAmount+" & sTransPay & " Where logMasavFile_id=" & Session("WireAvailCur")(0)
				oledbData.Execute "UPDATE tblWireMoney SET WireFlag=3 WHERE WireMoney_id=" & rsData("WireMoney_id")
				*/
				rowTotal += wireAmount;
				rowCount++;
			}
			sb.AppendFormat("5{0}00{1}0001", masavCode, wireDate.ToString("yyMMdd"));
			sb.Append(rowTotal.ToString("0000000000000.00")); //amount
			sb.Append(new string('0', 15));
			sb.Append(rowCount.ToString("0000000")); //count
			sb.AppendFormat("{0}{1}\r\n\r\n", new string('0', 7), new string(' ', 63));
			sb.Append(new string('9', 128)); //end marker

			System.IO.File.WriteAllText(Wire.GenerateOfflineProviderFileName(Name, null, "csv"), sb.ToString());
		}

        public class IsraelBankAccount : Accounts.BankAccount
        {
            public IsraelBankAccount(Accounts.BankAccount original)
            {
                _entity = original._entity;
            }

            private string GetIbanPart(int start, int? length)
            {
                if (IBAN == null) return null;
                if (IBAN.Length <= start) return string.Empty;
                if (length != null && length.Value + start > IBAN.Length) length = IBAN.Length - start;
                return IBAN.Substring(start, length.GetValueOrDefault(IBAN.Length - start));
            }

            private void SetIbanPart(int start, int length, string value)
            {
                if (IBAN == null) IBAN = "IL" + (new string('0', 23));
                if (IBAN.Length <= start) IBAN = IBAN + (new string('0', 23 - IBAN.Length));
                IBAN = IBAN.Substring(0, start) + value + IBAN.Substring(start + length);
                IBAN = GetVerification();
            }

            public string ILBankCode { get { return GetIbanPart(4, 3); } set { SetIbanPart(4, 3, value.EmptyIfNull().ToInt32().ToString("000")); } }
            public string ILBranchCode { get { return GetIbanPart(7, 3); } set { SetIbanPart(7, 3, value.EmptyIfNull().ToInt32().ToString("000")); } }
            public string ILAccountNumber { get { return GetIbanPart(10, null); } set { SetIbanPart(10, 13, value.EmptyIfNull().ToInt32().ToString("D13")); AccountNumber = value; } }

            private string GetVerification()
            {
                string prechecked = string.Format("{0}{1}{2}{3}", ILBankCode.ToInt32().ToString("000"), ILBranchCode.ToInt32().ToString("000"), ILAccountNumber.ToInt32().ToString("D13"), "IL00");
                string checksumStr = "";
                for (int i = 0; i < prechecked.Length; i++)
                {
                    var c = prechecked[i];
                    if ((c >= '0') && (c <= '9')) checksumStr += c;
                    else if ((c >= 'A') && (c <= 'Z')) checksumStr += ((c - 'A') + 10);
                    else if ((c >= 'a') && (c <= 'z')) checksumStr += ((c - 'a') + 10);
                }

                int reminder = 0;
                while (checksumStr.Length >= 7)
                {
                    reminder = int.Parse(reminder + checksumStr.Substring(0, 7)) % 97;
                    checksumStr = checksumStr.Substring(7);
                }
                reminder = int.Parse(reminder + checksumStr) % 97;
                long checksum = (98 - reminder);
                return string.Format("IL{0}{1}{2}{3}", checksum.ToString("00"), ILBankCode.ToInt32().ToString("000"), ILBranchCode.ToInt32().ToString("000"), ILAccountNumber.ToInt32().ToString("D13"));
            }

            public bool IsValidILAccount { get { return IBAN.EmptyIfNull().Equals(GetVerification(), StringComparison.InvariantCulture); } }

        }

    }
}
