﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.Wires.BOGModule
{
	public enum FileType
	{
		MultipleDebitAccounts,
		Payroll,
		SingleDebitAccount
	}


	public enum TransferTransactionCosts
	{
		Ours,
		Beneficiary,
		Shared
	}


	public enum Currency
	{
		USD = 1,
		EUR = 2,
		GBP
	}

	public class BOG : Provider
	{
        public override string Name { get { return "System.BOG"; } }
        public override string FriendlyName { get { return "Bank of georgia"; } }
		public override bool IsOnline { get { return false; } }
        public override bool IsCancelable { get { return false; } }
        public override void Cancel(Wire data) { throw new NotImplementedException(); }

        public override void Process(List<Wire> data, DateTime wireDate, string sourceAccountNumber)
		{
			var hfRecords = new File(Wire.GenerateOfflineProviderFileName(Name, null, "txt"), sourceAccountNumber, wireDate);
			foreach(var w in data)
			{
				var brTransfer = new Record();
				brTransfer.SenderAccount = hfRecords.DebitAccount;
				brTransfer.TransferAmount = w.TransferAmount;
				switch(w.ProcessingCurrency){
					case CommonTypes.Currency.USD: brTransfer.TransferCurrency = Currency.USD; break;
					case CommonTypes.Currency.EUR: brTransfer.TransferCurrency = Currency.EUR; break;			
				}
				var toAccount = w.BankAccount;
				var toCorAccount = w.BankAccount;
				brTransfer.DocumentDate = wireDate;
				brTransfer.PaymentDetails = "Wire ID " + w.ID;
				string sSwift_Beneficiary = "", sSwift_Intermediate = "", sIBAN_Beneficiary = "", sIBAN_Intermediate = "", sBeneficiaryName = "";
                sBeneficiaryName = toAccount.AccountName;
				brTransfer.Beneficiary = sBeneficiaryName.ToUpper();
				brTransfer.BeneficiaryAccount = toAccount.AccountNumber;
				sSwift_Beneficiary = toAccount.SwiftNumber;
				sSwift_Intermediate = toCorAccount.SwiftNumber;
				sIBAN_Beneficiary = toAccount.IBAN;
				sIBAN_Intermediate = toCorAccount.IBAN;			
				if (!string.IsNullOrEmpty(sSwift_Beneficiary)) brTransfer.BeneficiaryBankSwiftCode = sSwift_Beneficiary;			
				else if (string.IsNullOrEmpty(sSwift_Beneficiary) && !string.IsNullOrEmpty(sIBAN_Beneficiary)) brTransfer.BeneficiaryAccount = sIBAN_Beneficiary;				
						
				if (!string.IsNullOrEmpty(sSwift_Intermediate)) brTransfer.IntermediaryBankSwiftCode = sSwift_Intermediate;
				else if (string.IsNullOrEmpty(sSwift_Intermediate) && !string.IsNullOrEmpty(sIBAN_Intermediate)) brTransfer.IntermediaryBankSwiftCode = sIBAN_Intermediate;
				brTransfer.IntermediaryBank = toCorAccount.BankName;
				hfRecords.AddRecord(brTransfer);
			}			
		}
	}

	public class Format
	{
		public static string FormatAccount(string sAccount)
		{
			StringBuilder sbOutput = new StringBuilder();
			for (int i = 0; i <= sAccount.Length - 1; i++) {
				if ("1234567890".Contains(sAccount[i]))
					sbOutput.Append(sAccount[i]);
			}
			return sbOutput.ToString();
		}

		public static string FormatCurrency(Currency fcCurrency)
		{
			return fcCurrency.ToString();
		}

		public static string FormatTransactionCosts(TransferTransactionCosts ttcCosts)
		{
			return ttcCosts.ToString().Substring(0, 1);
		}


		public static string FormatAmount(decimal nAmount)
		{
			if (nAmount <= 0m | nAmount >= 999999999999.995m)
				throw new OverflowException("The amount is out of range for BOG: " + nAmount);
			return nAmount.ToString("000.00");
		}

		public static string FormatFlag(bool bFlag)
		{
			return (bFlag ? "Y" : "N");
		}

		public static string FormatDate(System.DateTime dDate)
		{
			return dDate.ToString("dd/MM/yyyy");
		}

		public static string FormatNumber(int nNumber)
		{
			if (nNumber < 0 | nNumber > 99999)
				throw new OverflowException("The number is out of range (0..99999) for BOG: " + nNumber);
			return nNumber.ToString("00000");
		}

		public static string FormatAlphaNumeric(string sText, int nLength = 0)
		{
			if (string.IsNullOrEmpty(sText))
				return string.Empty;
			sText = sText.ToUpper();
			StringBuilder sbOutput = new StringBuilder();
			for (int i = 0; i <= sText.Length - 1; i++) {
				if (i > 0 & i % 31 == 0) {
					sbOutput.Append("/\\");
				}
				sbOutput.Append(("1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ -.,'()".Contains(sText[i]) ? sText[i] : ' '));
			}
			return sbOutput.ToString().ToUpper();
		}


		public static string GetExtension(FileType hftType)
		{
			switch (hftType) {
				case FileType.MultipleDebitAccounts: return "mda";
				case FileType.Payroll: return "prl";
				case FileType.SingleDebitAccount: return "sda";
				default: throw new OverflowException("The file type is unsupported by Hellenic: " + hftType.ToString());
			}
		}


	}

	public class Record
	{
		public string CommandType = "001013";
		public string Version = "01";
		public string Number = "00";
		public System.DateTime DocumentDate = System.DateTime.Today;
		public string DocumentType = "0000000101";
		public decimal TransferAmount = 0m;
		public Currency TransferCurrency = Currency.USD;
		public string BeneficiaryAccount = string.Empty;
		public string IntermediaryBank = string.Empty;
		public string IntermediaryBankSwiftCode = string.Empty;
		public string BeneficiaryBank = string.Empty;
		public string BeneficiaryBankSwiftCode = string.Empty;
		public string Beneficiary = string.Empty;
		public string PaymentDetails = string.Empty;
		public string LineBreak = "/\\";
		public string AdditionalInformation = string.Empty;
		public string Charges = "OUR";
		public string SenderAccount = string.Empty;
		public System.DateTime ValueDate {
			get { return DocumentDate; }
			set { DocumentDate = value; }
		}
		public override string ToString()
		{
			StringBuilder sbRecord = new StringBuilder(string.Empty);
			sbRecord.AppendLine("-----");
			sbRecord.AppendLine("COMMAND TYPE: " + CommandType);
			sbRecord.AppendLine("VERSION: " + Version);
			sbRecord.AppendLine("NUMBER: " + Number);
			sbRecord.AppendLine("DOCUMENT DATE: " + Format.FormatDate(DocumentDate));
			sbRecord.AppendLine("DOCUMENT TYPE: " + DocumentType);
			sbRecord.AppendLine("[Foreign Currency Transfer]");
			sbRecord.AppendLine("PARAMETER LIST");
			sbRecord.AppendLine("PARAMETER: S001\r\n[amount]\r\n" + Format.FormatAmount(TransferAmount));
			sbRecord.AppendLine("PARAMETER: S006\r\n[currency]\r\n" + Format.FormatCurrency(TransferCurrency));
			sbRecord.AppendLine("PARAMETER: S008\r\n[beneficiary account]\r\n" + Format.FormatAccount(BeneficiaryAccount));
			sbRecord.AppendLine("PARAMETER: U001\r\n[value date]\r\n" + Format.FormatDate(ValueDate));
			sbRecord.AppendLine("PARAMETER: U002\r\n[intermediary bank]\r\n" + IntermediaryBank);
			sbRecord.AppendLine("PARAMETER: U003\r\n[intermediary bank bic]\r\n" + IntermediaryBankSwiftCode);
			sbRecord.AppendLine("PARAMETER: U004\r\n[beneficiary bank]\r\n" + BeneficiaryBank);
			sbRecord.AppendLine("PARAMETER: U005\r\n[beneficiary bank bic]\r\n" + BeneficiaryBankSwiftCode);
			sbRecord.AppendLine("PARAMETER: U006\r\n[beneficiary]\r\n" + Format.FormatAlphaNumeric(Beneficiary));
			sbRecord.AppendLine("PARAMETER: U007\r\n[payment details]\r\n" + Format.FormatAlphaNumeric(PaymentDetails));
			sbRecord.AppendLine("PARAMETER: U008\r\n[additional information]\r\n" + Format.FormatAlphaNumeric(AdditionalInformation));
			sbRecord.AppendLine("PARAMETER: U010\r\n[charges]\r\n" + Charges);
			sbRecord.AppendLine("PARAMETER: S004\r\n[SENDER ACCOUNT]\r\n" + Format.FormatAccount(SenderAccount) + (TransferCurrency == Currency.USD ? "USD" : "EUR"));
			return sbRecord.ToString().Trim();
		}
	}



	public class File
	{
		private FileType myType = FileType.MultipleDebitAccounts;
		public FileType Type {
			get { return myType; }
		}

		private string myFileName = string.Empty;
		public string FileName {
			get { return myFileName; }
			set { myFileName = value; }
		}

		private Dictionary<string, Record> myRecords = new Dictionary<string, Record>();
		public Dictionary<string, Record> Records { get { return myRecords; }	}

		public int RecordCount { get { return myRecords.Count; } }

		private string myDebitAccount;
		public string DebitAccount {
			get { return myDebitAccount; }
			set {
				if (myType != FileType.MultipleDebitAccounts & string.IsNullOrEmpty(value.Trim()))
					throw new OverflowException("File of this type requires explicit Account specification: " + myType.ToString());
				myDebitAccount = value;
			}
		}

		private System.DateTime mySubmissionDate;
		public System.DateTime SubmissionDate {
			get { return mySubmissionDate; }
			set {
				if (value < System.DateTime.Today)
					throw new OverflowException("The date is out of range for BOG, cannot be past: " + value);
				mySubmissionDate = value;
			}
		}


		public File(string sFileName, string sDebitAccount, System.DateTime dSubmissionDate)
		{
			SubmissionDate = dSubmissionDate;
			DebitAccount = sDebitAccount;
			FileName = sFileName;
		}

		public int AddRecord(Record recTransfer, string sKey = null)
		{
			if (recTransfer.DocumentDate == null | recTransfer.DocumentDate < System.DateTime.Today)
				recTransfer.DocumentDate = SubmissionDate;
			if (string.IsNullOrEmpty(recTransfer.SenderAccount))
				recTransfer.SenderAccount = DebitAccount;
			if (string.IsNullOrEmpty(sKey)) sKey = Guid.NewGuid().ToString();
			Records.Add(sKey, recTransfer);
			return RecordCount;
		}

		public int RemoveRecord(string sKey)
		{
			if (!string.IsNullOrEmpty(sKey))
				Records.Remove(sKey);
			return RecordCount;
		}

		public string Save()
		{
			var swFile = new System.IO.StreamWriter(myFileName, false, System.Text.Encoding.ASCII);
			foreach (Record recTransfer in Records.Values) {
				swFile.WriteLine(recTransfer);
			}
			swFile.Close();
			return myFileName;
		}
	}
}