﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.Wires.HellenicModule
{
	public enum FileType
	{
		MultipleDebitAccounts,
		Payroll,
		SingleDebitAccount
	}

	public enum RecordType
	{
		OwnAccount,
		Hellenic,
		CyprusBank,
		SEPA,
		Worldwide,
		UtilityPayment
	}

	public enum TransferTransactionCosts
	{
		Ours,
		Beneficiary,
		Shared
	}

	public enum CyprusBank
	{
		Bank_Of_Cyprus_Ltd = 2,
		Laiki_Bank_Ltd,
		National_Bank_of_Greece_Cyprus_Ltd = 6,
		COOP_Central_Bank_Ltd,
		Piraeus_Bank_Plc_Cyprus,
		Alpha_Bank_Ltd,
		Commercial_Bank_of_Greece_Cyprus_Ltd,
		Universal_Bank_Ltd,
		Societe_General_Cyprus_Ltd
	}

	public enum UtilityPaymentCode
	{
		Electricity_Authority_of_Cyprus,
		Cyprus_Telecommunications_Authority,
		Waterboard_of_Nicosia,
		Waterboard_of_Limasol,
		Sewerage_Board_of_Limasol_Amathus,
		Telepassport_Telecommunications_Cyprus_Ltd,
		Housing_Finance_Corporation,
		Hellenic_Bank_Finance_Ltd,
		Hellenic_Bank_Services
	}

	public enum Currency
	{
		USD = 1,
		EUR,
		GBP
	}

	public class Hellenic : Provider
	{

        public override string Name { get { return "System.Hellenic"; } }
        public override string FriendlyName { get { return "Hellenic bank"; } }
        public override bool IsOnline { get { return false; } }
        public override bool IsCancelable { get { return false; } }
		public override void Cancel(Wire data) { throw new NotImplementedException(); }

        public override void Process(List<Wire> data, DateTime wireDate, string sourceAccountNumber)
		{
			//string fileName = Server.MapPath("/Data/WireTransfer/Hellenic");
			var hfRecords = new File(Wire.GenerateOfflineProviderFileName(Name, null, "txt"), FileType.MultipleDebitAccounts, sourceAccountNumber, wireDate);
			foreach(var w in data)
			{
				var hrTransfer = new WorldwideRecord();
				hrTransfer.DebitAccount = hfRecords.DebitAccount;
				hrTransfer.TransferAmount = w.TransferAmount;
				switch(w.ProcessingCurrency){
					case CommonTypes.Currency.USD: hrTransfer.TransferCurrency = Currency.USD; break;
					case CommonTypes.Currency.EUR: hrTransfer.TransferCurrency = Currency.EUR; break;
					case CommonTypes.Currency.GBP: hrTransfer.TransferCurrency = Currency.GBP; break;
				}
				hrTransfer.SubmissionDate = hfRecords.SubmissionDate;
				hrTransfer.Notes1 = "Wire ID " + w.ID;

				var toAccount = w.BankAccount;
				var toCorAccount = w.BankAccount;
				hrTransfer.CreditAccount = toAccount.AccountNumber;
				hrTransfer.BeneficiaryName = toAccount.AccountName;
				hrTransfer.BeneficiaryAddress1 = String.Empty;  //drData3("basicInfo_address");
				hrTransfer.BeneficiaryAddress2 = String.Empty;
				hrTransfer.BeneficiaryBankName = toAccount.BankName;
				hrTransfer.BeneficiaryBankAddress1 = toAccount.BankStreet1;
				hrTransfer.BeneficiaryBankAddress3 = toAccount.BankStreet2;
				hrTransfer.BeneficiaryBankAddress3 = Country.Get(toAccount.BankCountryISOCode).Name;
				hrTransfer.BeneficiaryBankSwiftCode = toAccount.SwiftNumber;
				//hrTransfer.SortingCode = toAccount.SortCode;
				hrTransfer.ReceiverCorrespondentSwiftCode = toCorAccount.SwiftNumber;
				hrTransfer.ReceiverCorrespondentBankName = toCorAccount.BankName;
				hrTransfer.ReceiverCorrespondentAddress1 = toCorAccount.BankStreet1;
				hrTransfer.ReceiverCorrespondentAddress2 = toCorAccount.BankStreet2;
				hrTransfer.ReceiverCorrespondentAddress3 = Country.Get(toCorAccount.BankCountryISOCode).Name;

				hfRecords.AddRecord(hrTransfer);
			}
		}
	}

	public class Format
	{
		public static string FormatHellenicAccount(string sAccount)
		{
			StringBuilder sbOutput = new StringBuilder();
			for (int i = 0; i <= sAccount.Length - 1; i++)
			{
				if ("1234567890".Contains(sAccount[i]))
					sbOutput.Append(sAccount[i]);
			}
			return sbOutput.ToString();
		}

		public static string FormatCurrency(Currency hcCurrency)
		{
			return hcCurrency.ToString();
		}

		public static string FormatTransactionCosts(TransferTransactionCosts ttcCosts)
		{
			return ttcCosts.ToString().Substring(0, 1);
		}

		public static string FormatCyprusBank(CyprusBank hcbBank)
		{
			return Convert.ToInt32(hcbBank).ToString("00");
		}

		public static string FormatCyprusBankName(CyprusBank hcbBank)
		{
			return hcbBank.ToString().Replace("_", " ").Replace("Greece Cyprus", "Greece (Cyprus)");
		}

		public static string FormatAmount(decimal nAmount)
		{
			if (nAmount <= 0m | nAmount >= 999999999999.995m)
				throw new OverflowException("The amount is out of range for Hellenic: " + nAmount);
			StringBuilder sbResult = new StringBuilder(nAmount.ToString("0,000.00"));
			while ("0,".Contains(sbResult[0]))
			{
				sbResult.Remove(0, 1);
			}
			if (sbResult[0] == '.')
				sbResult.Insert(0, "0");
			return sbResult.Replace(",", "`").Replace(".", ",").Replace("`", ".").ToString().Trim();
		}

		public static string FormatFlag(bool bFlag)
		{
			return (bFlag ? "Y" : "N");
		}

		public static string FormatDate(System.DateTime dDate)
		{
			return dDate.ToString("yyyyMMdd");
		}

		public static string FormatNumber(int nNumber)
		{
			if (nNumber < 0 | nNumber > 99999)
				throw new OverflowException("The number is out of range (0..99999) for Hellenic: " + nNumber);
			return nNumber.ToString("00000");
		}

		public static string FormatAlphaNumeric(string sText, int nLength = 0)
		{
			if (string.IsNullOrEmpty(sText))
				return string.Empty;
			sText = sText.ToUpper();
			StringBuilder sbOutput = new StringBuilder();
			for (int i = 0; i <= sText.Length - 1; i++)
			{
				sbOutput.Append(("1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ -.,'()".Contains(sText[i]) ? sText[i] : ' '));
			}
			return sbOutput.ToString().ToUpper();
		}

		public static string FormatPaymentUtilityCode(UtilityPaymentCode hpuCode)
		{
			switch (hpuCode)
			{
				case UtilityPaymentCode.Electricity_Authority_of_Cyprus: return "51";
				case UtilityPaymentCode.Cyprus_Telecommunications_Authority: return "52";
				case UtilityPaymentCode.Waterboard_of_Nicosia: return "60";
				case UtilityPaymentCode.Waterboard_of_Limasol: return "61";
				case UtilityPaymentCode.Sewerage_Board_of_Limasol_Amathus: return "AS";
				case UtilityPaymentCode.Telepassport_Telecommunications_Cyprus_Ltd: return "E2";
				case UtilityPaymentCode.Housing_Finance_Corporation: return "HF";
				case UtilityPaymentCode.Hellenic_Bank_Finance_Ltd: return "FI";
				case UtilityPaymentCode.Hellenic_Bank_Services: return "VI";
				default: throw new OverflowException("This PaymentUtipilyCode is not supported by Hellenic: " + hpuCode.ToString());
			}
		}

		public static string GetExtension(FileType hftType)
		{
			switch (hftType)
			{
				case FileType.MultipleDebitAccounts: return "mda";
				case FileType.Payroll: return "prl";
				case FileType.SingleDebitAccount: return "sda";
				default: throw new OverflowException("The file type is unsupported by Hellenic: " + hftType.ToString());
			}
		}

		public static string FormatRecordType(RecordType hrtType)
		{
			switch (hrtType)
			{
				case RecordType.OwnAccount: return "O";
				case RecordType.Hellenic: return "B";
				case RecordType.CyprusBank: return "C";
				case RecordType.SEPA: return "S";
				case RecordType.Worldwide: return "I";
				case RecordType.UtilityPayment: return "P";
				default: throw new OverflowException("The record type is unsupported by Hellenic: " + hrtType.ToString());
			}
		}
	}

	public abstract class Record
	{
		public RecordType TransactionType;
		public string DebitAccount = string.Empty;
		public string CreditAccount = string.Empty;
		public decimal TransferAmount = 0m;
		public Currency TransferCurrency = Currency.USD;
		public System.DateTime SubmissionDate = System.DateTime.Today;
		public System.DateTime ValueDate
		{
			get { return SubmissionDate; }
			set { SubmissionDate = value; }
		}
		public override abstract string ToString();
	}

	public class OwnAccountRecord : Record
	{
		public new RecordType TransactionType = RecordType.OwnAccount;
		public string Notes = string.Empty;
		public override string ToString()
		{
			StringBuilder sbRecord = new StringBuilder(Format.FormatRecordType(TransactionType));
			sbRecord.AppendFormat("|{0}", Format.FormatHellenicAccount(DebitAccount));
			sbRecord.AppendFormat("|{0}", Format.FormatHellenicAccount(CreditAccount));
			sbRecord.AppendFormat("|{0}", Format.FormatAmount(TransferAmount));
			sbRecord.AppendFormat("|{0}", Format.FormatCurrency(TransferCurrency));
			sbRecord.AppendFormat("|{0}", Format.FormatDate(SubmissionDate));
			sbRecord.AppendFormat("|{0}", Format.FormatDate(ValueDate));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(Notes, 20));
			return sbRecord.ToString();
		}
	}

	public class HellenicRecord : Record
	{
		public new RecordType TransactionType = RecordType.Hellenic;
		public string BeneficiaryName = string.Empty;
		public string BeneficiaryAddress1 = string.Empty;
		public string BeneficiaryAddress2 = string.Empty;
		public string BeneficiaryAddress3 = string.Empty;
		public string Notes = string.Empty;
		public override string ToString()
		{
			StringBuilder sbRecord = new StringBuilder(Format.FormatRecordType(TransactionType));
			sbRecord.AppendFormat("|{0}", Format.FormatHellenicAccount(DebitAccount));
			sbRecord.AppendFormat("|{0}", Format.FormatHellenicAccount(CreditAccount));
			sbRecord.AppendFormat("|{0}", Format.FormatAmount(TransferAmount));
			sbRecord.AppendFormat("|{0}", Format.FormatCurrency(TransferCurrency));
			sbRecord.AppendFormat("|{0}", Format.FormatDate(SubmissionDate));
			sbRecord.AppendFormat("|{0}", Format.FormatDate(ValueDate));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryName, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryAddress1, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryAddress2, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryAddress3, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(Notes, 20));
			return sbRecord.ToString();
		}
	}

	public class CyprusBankRecord : Record
	{
		public new RecordType TransactionType = RecordType.CyprusBank;
		public string BeneficiaryName = string.Empty;
		public string BeneficiaryAddress1 = string.Empty;
		public string BeneficiaryAddress2 = string.Empty;
		public string BeneficiaryBankName = string.Empty;
		public string BeneficiaryBankAddress1 = string.Empty;
		public string BeneficiaryBankAddress2 = string.Empty;
		public CyprusBank BeneficiaryBankCode;
		public string Notes1 = string.Empty;
		public string Notes2 = string.Empty;
		public override string ToString()
		{
			StringBuilder sbRecord = new StringBuilder(Format.FormatRecordType(TransactionType));
			sbRecord.AppendFormat("|{0}", Format.FormatHellenicAccount(DebitAccount));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(CreditAccount, 20));
			sbRecord.AppendFormat("|{0}", Format.FormatAmount(TransferAmount));
			sbRecord.AppendFormat("|{0}", Format.FormatCurrency(TransferCurrency));
			sbRecord.AppendFormat("|{0}", Format.FormatDate(SubmissionDate));
			sbRecord.AppendFormat("|{0}", Format.FormatDate(ValueDate));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryName, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryAddress1, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryAddress2, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryBankName, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryBankAddress1, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryBankAddress2, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatCyprusBank(BeneficiaryBankCode));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(Notes1, 20));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(Notes2, 20));
			return sbRecord.ToString();
		}
	}

	public class SepaRecord : Record
	{
		public new RecordType TransactionType = RecordType.SEPA;
		public new Currency TransferCurrency = Currency.EUR;
		public string BeneficiaryName = string.Empty;
		public string BeneficiaryAddress1 = string.Empty;
		public string BeneficiaryAddress2 = string.Empty;
		public string BicCode = string.Empty;
		public bool ConfirmationByFax = false;
		public string OurReference = string.Empty;
		public string Description1 = string.Empty;
		public string Description2 = string.Empty;
		public string Description3 = string.Empty;
		public override string ToString()
		{
			StringBuilder sbRecord = new StringBuilder(Format.FormatRecordType(TransactionType));
			sbRecord.AppendFormat("|{0}", Format.FormatHellenicAccount(DebitAccount));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(CreditAccount, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAmount(TransferAmount));
			sbRecord.AppendFormat("|{0}", Format.FormatCurrency(TransferCurrency));
			sbRecord.AppendFormat("|{0}", Format.FormatDate(SubmissionDate));
			sbRecord.AppendFormat("|{0}", Format.FormatDate(ValueDate));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryName, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryAddress1, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryAddress2, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BicCode, 11));
			sbRecord.AppendFormat("|{0}", Format.FormatFlag(ConfirmationByFax));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(OurReference, 20));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(Description1, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(Description2, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(Description3, 35));
			return sbRecord.ToString();
		}
	}

	public class WorldwideRecord : Record
	{
		public new RecordType TransactionType = RecordType.Worldwide;
		public string BeneficiaryName = string.Empty;
		public string BeneficiaryAddress1 = string.Empty;
		public string BeneficiaryAddress2 = string.Empty;
		public string BeneficiaryBankName = string.Empty;
		public string BeneficiaryBankAddress1 = string.Empty;
		public string BeneficiaryBankAddress2 = string.Empty;
		public string BeneficiaryBankAddress3 = string.Empty;
		public string BeneficiaryBankSwiftCode = string.Empty;
		public TransferTransactionCosts TransactionCosts = TransferTransactionCosts.Beneficiary;
		public bool ConfirmationByFax = false;
		public string Notes1 = string.Empty;
		public string Notes2 = string.Empty;
		public string Notes3 = string.Empty;
		public string Notes4 = string.Empty;
		public string SortingCode = string.Empty;
		public string ReceiverCorrespondentSwiftCode = string.Empty;
		public string ReceiverCorrespondentBankName = string.Empty;
		public string ReceiverCorrespondentAddress1 = string.Empty;
		public string ReceiverCorrespondentAddress2 = string.Empty;
		public string ReceiverCorrespondentAddress3 = string.Empty;
		public string ThirdReimbursementInsitutionSwiftCode = string.Empty;
		public string ThirdReimbursementInsitutionBankName = string.Empty;
		public string ThirdReimbursementInsitutionAddress1 = string.Empty;
		public string ThirdReimbursementInsitutionAddress2 = string.Empty;
		public string ThirdReimbursementInsitutionAddress3 = string.Empty;
		public string IntermediaryBankSwiftCode = string.Empty;
		public override string ToString()
		{
			StringBuilder sbRecord = new StringBuilder(Format.FormatRecordType(TransactionType));
			sbRecord.AppendFormat("|{0}", Format.FormatHellenicAccount(DebitAccount));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(CreditAccount, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAmount(TransferAmount));
			sbRecord.AppendFormat("|{0}", Format.FormatCurrency(TransferCurrency));
			sbRecord.AppendFormat("|{0}", Format.FormatDate(SubmissionDate));
			sbRecord.AppendFormat("|{0}", Format.FormatDate(ValueDate));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryName, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryAddress1, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryAddress2, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryBankName, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryBankAddress1, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryBankAddress2, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryBankAddress3, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(BeneficiaryBankSwiftCode, 11));
			sbRecord.AppendFormat("|{0}", Format.FormatTransactionCosts(TransactionCosts));
			sbRecord.AppendFormat("|{0}", Format.FormatFlag(ConfirmationByFax));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(Notes1, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(Notes2, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(Notes3, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(Notes4, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(SortingCode, 15));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(ReceiverCorrespondentSwiftCode, 11));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(ReceiverCorrespondentBankName, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(ReceiverCorrespondentAddress1, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(ReceiverCorrespondentAddress2, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(ReceiverCorrespondentAddress3, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(ThirdReimbursementInsitutionSwiftCode, 11));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(ThirdReimbursementInsitutionBankName, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(ThirdReimbursementInsitutionAddress1, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(ThirdReimbursementInsitutionAddress2, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(ThirdReimbursementInsitutionAddress3, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(IntermediaryBankSwiftCode, 11));
			return sbRecord.ToString();
		}
	}

	public class UtilityPaymentRecord : Record
	{
		public new RecordType TransactionType = RecordType.UtilityPayment;
		public string UtilityAccount = string.Empty;
		public string UtilityName = string.Empty;
		public UtilityPaymentCode UtilityCode;
		public new Currency TransferCurrency = Currency.EUR;
		public override string ToString()
		{
			StringBuilder sbRecord = new StringBuilder(Format.FormatRecordType(TransactionType));
			sbRecord.AppendFormat("|{0}", Format.FormatHellenicAccount(DebitAccount));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(UtilityAccount, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatAlphaNumeric(UtilityName, 35));
			sbRecord.AppendFormat("|{0}", Format.FormatPaymentUtilityCode(UtilityCode));
			sbRecord.AppendFormat("|{0}", Format.FormatAmount(TransferAmount));
			sbRecord.AppendFormat("|{0}", Format.FormatCurrency(TransferCurrency));
			sbRecord.AppendFormat("|{0}", Format.FormatDate(SubmissionDate));
			return sbRecord.ToString();
		}
	}

	public class File
	{
		private FileType myType = FileType.MultipleDebitAccounts;
		public FileType Type
		{
			get { return myType; }
		}

		private string myFolder = string.Empty;
		public string Folder
		{
			get { return myFolder; }
			set
			{
				if (string.IsNullOrEmpty(value) | value == "/")
				{
					myFolder = ".";
				}
				else
				{
					if (value.EndsWith("/"))
						value = value.Substring(0, value.Length - 1);
					myFolder = value;
				}
			}
		}

		private string myFileName = string.Empty;
		public string Name
		{
			get { return myFileName; }
			set { myFileName = value; }
		}

		private Dictionary<string, Record> myRecords = new Dictionary<string, Record>();
		public Dictionary<string, Record> Records { get { return myRecords; } }

		public int RecordCount { get { return myRecords.Count(); } }

		private string myDebitAccount;
		public string DebitAccount
		{
			get { return myDebitAccount; }
			set
			{
				if (myType != FileType.MultipleDebitAccounts & string.IsNullOrEmpty(value.Trim()))
					throw new OverflowException("File of this type requires explicit Account specification: " + myType.ToString());
				myDebitAccount = value;
			}
		}

		private System.DateTime mySubmissionDate;
		public System.DateTime SubmissionDate
		{
			get { return mySubmissionDate; }
			set
			{
				if (value < System.DateTime.Today)
					throw new OverflowException("The date is out of range for Hellenic, cannot be past: " + value);
				mySubmissionDate = value;
			}
		}

		public string Header
		{
			get
			{
				StringBuilder sbHeader = new StringBuilder();
				sbHeader.AppendFormat("{0}|{1}|{2}", myType.ToString().Substring(0, 1).ToUpper(), RecordCount, Format.FormatAmount(0.01m));
				//Format.FormatNumber(myRecords.Count)
				sbHeader.AppendFormat("|{0}", (myType == FileType.MultipleDebitAccounts ? string.Empty : Format.FormatDate(mySubmissionDate)));
				sbHeader.AppendFormat("|{0}", (myType == FileType.MultipleDebitAccounts ? string.Empty : myDebitAccount));
				return sbHeader.ToString();
			}
		}

		public File(string sFileName, FileType hftType, string sDebitAccount, System.DateTime dSubmissionDate)
		{
			SubmissionDate = dSubmissionDate;
			myType = hftType;
			DebitAccount = sDebitAccount;
			Name = sFileName;
		}

		public int AddRecord(Record recTransfer, string sKey = null)
		{
			if (myType != FileType.MultipleDebitAccounts | recTransfer.SubmissionDate == null | recTransfer.SubmissionDate < System.DateTime.Today)
				recTransfer.SubmissionDate = SubmissionDate;
			if (myType != FileType.MultipleDebitAccounts | string.IsNullOrEmpty(recTransfer.DebitAccount))
				recTransfer.DebitAccount = DebitAccount;
			if (string.IsNullOrEmpty(sKey)) sKey = Guid.NewGuid().ToString();
			Records.Add(sKey, recTransfer);
			return RecordCount;
		}

		public int RemoveRecord(string sKey)
		{
			if (!string.IsNullOrEmpty(sKey))
				Records.Remove(sKey);
			return RecordCount;
		}

		public string Save()
		{
			var swFile = new System.IO.StreamWriter(myFileName, false);
			swFile.WriteLine(Header);
			foreach (Record recTransfer in Records.Values)
			{
				swFile.WriteLine(recTransfer);
			}
			swFile.Close();
			return myFileName;
		}
	}
}
