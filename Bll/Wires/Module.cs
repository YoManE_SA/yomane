﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Wires
{
	public class Module : Infrastructure.Module
	{
		public const string ModuleName = "Wires";
		public override string Name { get { return ModuleName; } }
		public override decimal Version { get { return 1.12m; } }
		public static Module Current { get { return Infrastructure.Module.Get(ModuleName) as Module; } }
		protected override void OnInstall(EventArgs e)
		{            
			Infrastructure.Security.SecuredObject.Create(this, Wire.SecuredObjectName, PermissionGroup.ReadEditDelete);
			Infrastructure.Security.SecuredObject.Create(this, WireLog.SecuredObjectName, PermissionGroup.ReadEditDelete);
			Infrastructure.Security.SecuredObject.Create(this, WiresBatch.SecuredObjectName, PermissionGroup.ExecuteReadEditDelete);
			base.OnInstall(e);
		}

		protected override void OnUninstall(EventArgs e)
		{
			Infrastructure.Security.SecuredObject.Remove(this);
            base.OnUninstall(e);
		}
	}
}
