﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll.Wires
{
    public abstract class Provider
    {
        private Dal.Netpay.WireProvider _entity;
        public abstract string Name { get; }

        public string ShortName
        {
            get
            {
                return Name.Split('.')[1];
            }
        }

        public abstract string FriendlyName { get; }
        public abstract bool IsOnline { get; }
        public virtual bool IsCancelable { get { return false; } }

        public string ServerUrl { get { return _entity.ServerURL; } set { _entity.ServerURL = value; } }
        public string UserName { get { return _entity.Username; } set { _entity.Username = value; } }
        public string Password { get { return Infrastructure.Security.Encryption.Decrypt(_entity.EncryptionKey, _entity.PasswordEncrypted.ToArray()); } set { _entity.PasswordEncrypted = Infrastructure.Security.Encryption.Encrypt(value); } }

        private static List<Provider> _providers;
                
        public static void RemoveCache()
        {
            _providers = null;
        }

        public static List<Provider> Cache
        {
            get
            {
                if (_providers != null) return _providers;
                _providers = new List<Provider>();

                if (WireProviders.ManualModule.Module.Current.IsActive)
                {
                    _providers.Add(new Manual());
                }

                if (WireProviders.BalanceModule.Module.Current.IsActive)
                {
                    _providers.Add(new Balance());
                }

                //Masav
                if (WireProviders.MasavModule.Module.Current.IsActive)
                {
                    _providers.Add(new Masav());
                }

                //BOG
                if (WireProviders.BOGModule.Module.Current.IsActive)
                {
                    _providers.Add(new BOGModule.BOG());
                }

                //Hellenic
                if (WireProviders.HellenicModule.Module.Current.IsActive)
                {
                    _providers.Add(new HellenicModule.Hellenic());
                }

                //FNB
                if (WireProviders.FNBModule.Module.Current.IsActive)
                {
                    _providers.Add(new FNB());
                }


                var dbItems = DataContext.Reader.WireProviders.ToList();
                foreach (var p in _providers) p._entity = dbItems.Where(v => v.WireProvider_id == p.Name).SingleOrDefault();
                foreach (var p in _providers.Where(v => v._entity == null)) { p._entity = new Dal.Netpay.WireProvider(); p.Save(); }
                return _providers;
            }
        }

        public static List<Provider> ActionPageCache
        {
            get
            {
                if (_providers != null) return _providers;
                _providers = new List<Provider>();

                if (WireProviders.FNBModule.Module.Current.IsActive)
                {
                    _providers.Add(new FNB());
                }

                var dbItems = DataContext.Reader.WireProviders.ToList();
                foreach (var p in _providers) p._entity = dbItems.Where(v => v.WireProvider_id == p.Name).SingleOrDefault();
                foreach (var p in _providers.Where(v => v._entity == null)) { p._entity = new Dal.Netpay.WireProvider(); p.Save(); }
                return _providers;
            }
        }

        public static Provider Get(string name) { return Cache.Where(v => v.Name == name).SingleOrDefault(); }

        public void Save()
        {
            var isExist = _entity.WireProvider_id == Name;
            if (!isExist) {
                _entity.WireProvider_id = Name;
                _entity.Name = FriendlyName;
            }
            DataContext.Writer.WireProviders.Update(_entity, isExist);
            DataContext.Writer.SubmitChanges();
        }

        public int NextBetchNumber 
        { 
            get{
                return System.Math.Max((from n in Infrastructure.DataContext.Reader.WireProviders where n.WireProvider_id == Name select n.NextWireBatchNum).SingleOrDefault().GetValueOrDefault(1), 1);
            }
            set {
                Infrastructure.DataContext.Reader.ExecuteCommand("Update List.WireProvider Set NextWireBatchNum={0} Where WireProvider_id={1}", value, Name);
            }
        }

        public void IncBetchNumber(int max = int.MaxValue)
        {
            Infrastructure.DataContext.Reader.ExecuteCommand("Update List.WireProvider Set NextWireBatchNum = (IsNull(NextWireBatchNum, 1) + 1) % {0} Where WireProvider_id={1}", max, Name);
        }

        public virtual Dictionary<string, string> GetAccounts() 
        { 
            return Wire.GetSourceAccountList(Name);
        }
        
        public abstract void Process(List<Wire> data, DateTime wireDate, string sourceAccountNumber);

        public virtual void Cancel(Wire data) { throw new NotImplementedException(); }
        public virtual void Send(WiresBatch data) { throw new NotImplementedException(); }

        public virtual List<string> LastRelatedBatchFiles
        {
            get { 
                var ret = new List<string>();
                string filePath = Wire.MapOfflineFile(Name, null);
                if (!System.IO.Directory.Exists(filePath)) return ret;
                var file = new System.IO.DirectoryInfo(filePath).GetFiles("Wire_*.*", System.IO.SearchOption.TopDirectoryOnly).OrderByDescending(f => f.CreationTime).FirstOrDefault();
                if (file != null) ret.Add(file.FullName);
                return ret;
            }
        }


    }
}
