﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Wires
{
	public class WireFile : BaseDataObject
	{
		public const string SecuredObjectName = "WireFile";
		public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Wires.Module.Current, SecuredObjectName); } }

		private Dal.Netpay.tblWireMoneyFile _entity;

		public int ID { get { return _entity.ID; } }
		public int WireID { get { return _entity.wmf_WireMoneyID; } }
		public string Description { get { return _entity.wmf_Description; } }
		public System.DateTime Date { get { return _entity.wmf_Date; } }
		public string UserName { get { return _entity.wmf_User; } }
		public string FileName { get { return _entity.wmf_FileName; } }
		public byte ParseResult { get { return _entity.wmf_ParseResult; } }
		public string ParseLog { get { return _entity.wmf_ParseLog; } }

		private WireFile(Dal.Netpay.tblWireMoneyFile entity)
		{ 
			_entity = entity;
		}

		public static void AddFile(int wireId, string description, string fileName, System.IO.Stream fileData)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
			var entity = new Dal.Netpay.tblWireMoneyFile();
			entity.wmf_WireMoneyID = wireId;
			entity.wmf_Date = DateTime.Now;
			entity.wmf_Description = description;
			entity.wmf_User = Login.Current.UserName;
			entity.wmf_FileName = fileName;
			DataContext.Writer.tblWireMoneyFiles.InsertOnSubmit(entity);
			DataContext.Writer.SubmitChanges();
		}

		public static WireFile GetLastFile(int wireId)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);
			return (from l in DataContext.Reader.tblWireMoneyFiles where l.wmf_WireMoneyID == wireId orderby l.ID descending select new WireFile(l)).Take(1).SingleOrDefault();
		}

		public static List<WireFile> Search(int wireId, ISortAndPage sortAndPage)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
			return (from l in DataContext.Reader.tblWireMoneyFiles where l.wmf_WireMoneyID == wireId select l).ApplySortAndPage(sortAndPage).Select(l => new WireFile(l)).ToList();		
		}

        private void Save()
        {
            DataContext.Writer.tblWireMoneyFiles.Update(_entity, (_entity.ID != 0));
            DataContext.Writer.SubmitChanges();
        }

	}
}
