﻿using System;
using System.Collections.Generic;
using System.Linq;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Wires
{
    public class WireLog
    {
        public const string SecuredObjectName = "WireLog";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Wires.Module.Current, SecuredObjectName); } }

        private Dal.Netpay.WireHistory _entity;

        public int ID { get { return _entity.WireHistory_id; } }
        public int Wire_id { get { return _entity.Wire_id; } }
        public System.DateTime InsertDate { get { return _entity.InsertDate; } }
        public string UserName { get { return _entity.UserName; } }
        public string Description { get { return _entity.Description; } }
        public string FileName { get { return _entity.FileName; } }

        private WireLog(Dal.Netpay.WireHistory entity)
        {
            _entity = entity;
        }

        public static void AddLog(int wireId, string description)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Wire.SecuredObject, PermissionValue.Add);

            var entity = new Dal.Netpay.WireHistory();
            entity.Wire_id = wireId;
            entity.InsertDate = DateTime.Now;
            entity.Description = description;
            entity.UserName = Login.Current.UserName;
            DataContext.Writer.WireHistories.InsertOnSubmit(entity);
            DataContext.Writer.SubmitChanges();
        }

        public static WireLog AddFile(int wireId, string description, string fileName, System.IO.Stream fileData)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            var entity = new Dal.Netpay.WireHistory();
            entity.Wire_id = wireId;
            entity.InsertDate = DateTime.Now;
            entity.Description = description;
            entity.UserName = Login.Current.UserName;
            entity.FileName = fileName;
            DataContext.Writer.WireHistories.InsertOnSubmit(entity);
            DataContext.Writer.SubmitChanges();
            var ret = new WireLog(entity);
            var dir = Domain.Current.MapPrivateDataPath(@"Wires\Log\");
            if (!System.IO.Directory.Exists(dir)) System.IO.Directory.CreateDirectory(dir);
            using (var f = System.IO.File.OpenWrite(ret.PhysicalFileName)) {
                fileData.CopyTo(f);
                f.Close();
            }
            return ret;
        }

        public string PhysicalFileName {
            get {
                if (string.IsNullOrEmpty(FileName)) return null;
                return System.IO.Path.Combine(Domain.Current.MapPrivateDataPath(@"Wires\Log\"), "File_" + ID.ToString()) + System.IO.Path.GetExtension(FileName);
            }
        }

        public static WireLog GetLastFile(int wireId)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);
            return (from l in DataContext.Reader.WireHistories where l.Wire_id == wireId && l.FileName != null orderby l.WireHistory_id descending select new WireLog(l)).Take(1).SingleOrDefault();
        }

        public static List<WireLog> Search(int wireId, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Wire.SecuredObject, PermissionValue.Read);

            return (from l in DataContext.Reader.WireHistories where l.Wire_id == wireId select l).ApplySortAndPage(sortAndPage).Select(l => new WireLog(l)).ToList();
        }

    }
}
