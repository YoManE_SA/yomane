﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Wires
{
	public class Wire : BaseDataObject
	{
		public const string SecuredObjectName = "Manage";
		public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Wires.Module.Current, SecuredObjectName); } }

		public enum WireSource { PaymentToMerchants = 1,  MerchantMakePayments = 2, Manual = 3, Fee = 4 }
		public enum WireStatus { Pending = 0, Canceled = 1, Hold = 2, Sent = 3, Completed = 10 }
		public enum WireApprovalStatus { Waiting = 0, Approved = 1, Rejected = 2, PartiallyApproved = 4 }
		//public enum WireFlag { Waiting = 0, Approved = 1, Rejected = 2, Done = 3, PartiallyApproved = 4 } old values
		

		public static Dictionary<WireStatus, System.Drawing.Color> WireStatusColor = new Dictionary<WireStatus, System.Drawing.Color> { 
			{ WireStatus.Pending, System.Drawing.Color.FromName("#7d7d7d") } , 
			{ WireStatus.Canceled, System.Drawing.Color.FromName("#ff6666") }, 
			{ WireStatus.Hold, System.Drawing.Color.FromName("#ffd400") }, 
			{ WireStatus.Completed, System.Drawing.Color.FromName("#6699cc") } 
		};
		public static Dictionary<WireApprovalStatus, System.Drawing.Color> WireApprovalStatusColor = new Dictionary<WireApprovalStatus, System.Drawing.Color> { 
			{ WireApprovalStatus.Waiting, System.Drawing.Color.FromName("#7d7d7d") } , 
			{ WireApprovalStatus.Rejected, System.Drawing.Color.FromName("#ff6666") }, 
			{ WireApprovalStatus.PartiallyApproved, System.Drawing.Color.FromName("#ffd400") }, 
			{ WireApprovalStatus.Approved, System.Drawing.Color.FromName("#66cc66") } 
		};


        private static List<Provider> _providers;
        public static List<Provider> Providers
		{
			get{
				if(_providers != null) return _providers;
				_providers = new List<Provider>();
				_providers.Add(new Manual());
				_providers.Add(new Balance());
				_providers.Add(new Masav());
				_providers.Add(new BOG.BOG());
				_providers.Add(new Helenic.Hellenic());
                _providers.Add(new FNB());
                return _providers;
			}
		}

		public class SearchFilters {
			public Range<DateTime?> Date;
			public Range<DateTime?> InsertDate;
			public Range<int?> ID;
			public Range<decimal?> Amount;
			public CommonTypes.Currency? Currency;
			public List<int> AccountIDs;
			public string Provider;
			public List<WireStatus> Status;
			public List<WireApprovalStatus> ApprovalStatus;
			public List<WireSource> Source;

			public bool? ApprovalLevel1;
			public bool? ApprovalLevel2;
			public string PayeeName;
			public string PayeeNumber;
			public int? MerchantSettlementID, AffiliateSettlementID, PayeeID;
		}

		public enum OldWireStatus { New = 0, Canceled = 1, Pending = 2, SentByMasav = 3, SentByPrint = 4, SetBalance = 5, Wired = 6, Hellenic = 7, Georgia = 8, FNB = 9 }
		public static WireStatus StatusOldToNew(OldWireStatus value)
		{
			switch(value){
				case OldWireStatus.New: return WireStatus.Pending;
				case OldWireStatus.Pending: return WireStatus.Hold;
				case OldWireStatus.Canceled: return WireStatus.Canceled;
				//case OldWireStatus.SentByMasav: return WireStatus.Completed;
				case OldWireStatus.SentByPrint: return WireStatus.Sent;
				//case OldWireStatus.SetBalance: return WireStatus.Completed;
				//case OldWireStatus.Wired: return WireStatus.Completed;
				//case OldWireStatus.Hellenic: return WireStatus.Completed;
				//case OldWireStatus.Georgia: return WireStatus.Completed;
			}
			return WireStatus.Completed;
		}

        public static OldWireStatus StatusNewToOld(WireStatus value, string providerName)
        {
            switch (value)
            {
                case WireStatus.Pending: return OldWireStatus.New;
                case WireStatus.Canceled: return OldWireStatus.Canceled;
                case WireStatus.Hold: return OldWireStatus.Pending;
                case WireStatus.Sent:
                case WireStatus.Completed:
                    switch (providerName) {
                        case "System.Manual": return OldWireStatus.SentByPrint;
                        case "System.Balance": return OldWireStatus.SetBalance;
                        case "System.Masav": return OldWireStatus.SentByMasav;
                        case "System.BOG": return OldWireStatus.Georgia;
                        case "System.Hellenic": return OldWireStatus.Hellenic;
                        case "System.FNB": return OldWireStatus.FNB;
                    }
                    break;
            }
            return OldWireStatus.Canceled;
        }

        private Dal.Netpay.tblWireMoney _entity;

		public int ID { get{ return _entity.WireMoney_id; } }
		public System.DateTime InsertDate { get { return _entity.wireInsertDate; } }
		public System.DateTime Date { get { return _entity.WireDate; } }
		public decimal Amount { get { return _entity.WireAmount; } }
		public CommonTypes.Currency Currency { get { return (CommonTypes.Currency) _entity.WireCurrency; } }
		public string Comment { get { return _entity.WireComment; } }

		public WireApprovalStatus ApprovalStatus { get { return (WireApprovalStatus) _entity.WireFlag; } }
		public bool? ApprovalLevel1 { get { return _entity.wireApproveLevel1; } }
		public bool? ApprovalLevel2 { get { return _entity.wireApproveLevel2; } }

		public System.DateTime CommitDate { get { return _entity.WireStatusDate; } }
		public string CommitUser { get { return _entity.WireStatusUser; } }
		public string CommitProvider { get { return null; /*return _entity.WireProvider_id;*/ } }
		public WireStatus Status { get { return StatusOldToNew((OldWireStatus) _entity.WireStatus); } }

		public string ConfirmationNumber { get { return _entity.WireConfirmationNum; } }
		public System.DateTime? LastLogDate { get { return _entity.LastLogDate; } }

		public CommonTypes.Currency ProcessingCurrency 
		{ 
			get { return (CommonTypes.Currency)_entity.WireProcessingCurrency; }
			set { _entity.WireProcessingCurrency = (int)value; _entity.WireExchangeRate = (double)Money.ConvertRateWithFee(Currency, ProcessingCurrency); } 
		}

		public decimal ExchangeRate { get { return (decimal)_entity.WireExchangeRate; } }
		public decimal Fee { get { return _entity.wireFee; } }
		public decimal TransferAmount { get { return (Amount - Fee) * ExchangeRate; } }

		public bool IsShow { get { return _entity.isShow; } }

		private Dal.Netpay.tblCompanyMakePaymentsRequest _paymentRequest;
		public int AccountID { get; private set; }
		public int? PayeeID { get { return _paymentRequest.CompanyMakePaymentsProfiles_id; } }
		public int? MerchantSettlementID { get { return _entity.SettlementID; } }
		public int? AffiliateSettlementID { get { return _entity.AffiliatePaymentsID; } }

		public string AccountName { get { return _entity.wireCompanyName; } set { _entity.wireCompanyName = value; } }
		public string PayeeName { get { return _entity.wirePaymentPayeeName; } set { _entity.wirePaymentPayeeName = value; } }
		public string PayeeLegalNumber { get { return _entity.wireCompanyLegalNumber; } set { _entity.wireIDnumber = value; } }

		public WireSource Source { get { return (WireSource) _entity.WireType; } }
		//public int SourceID { get { return _entity.WireSourceTbl_id; } }
		//public string IDnumber { get { return _entity.wireIDnumber; } set { _entity.wireIDnumber = value; } }
		public string PaymentMethod { get { return _entity.wirePaymentMethod; } set { _entity.wirePaymentMethod = value; } }

        public bool BatchFailed { get; set; }


		private Accounts.BankAccount _bankAccount;
		public Accounts.BankAccount BankAccount { 
			get{
				if (_bankAccount == null) {
					var bankAccounts = Accounts.BankAccount.LoadOldForAccount(AccountID);
					_bankAccount = bankAccounts.Where(ba => ba.CurrencyISOCode == Bll.Currency.GetIsoCode(Currency)).FirstOrDefault();
					if (_bankAccount == null) _bankAccount = bankAccounts.Where(ba => ba.IsDefault).FirstOrDefault();
				}
				return _bankAccount;
			}
		}

		private Accounts.Account _account;
		public Accounts.Account Account
		{
			get
			{
				if (_account == null) _account = Accounts.Account.LoadAccount(AccountID);
				return _account;
			}
		}

		private Wire(Dal.Netpay.tblWireMoney entity, int accountId)
		{ 
			AccountID = accountId;
			_entity = entity;
		}

		public Wire(int accountId, int? settlementId) 
		{
			AccountID = accountId;
			_entity = new Dal.Netpay.tblWireMoney();
			_entity.wireInsertDate = DateTime.Now;
			_entity.Company_id = Account.MerchantID.GetValueOrDefault();
			_entity.AffiliateID = Account.AffiliateID.GetValueOrDefault();
			_entity.SettlementID = settlementId;
		}

		public Wire(Accounts.Payee payee, decimal amount, CommonTypes.Currency currency, string comment) 
		{
			_entity = new Dal.Netpay.tblWireMoney();
			_entity.wireInsertDate = DateTime.Now;
			_entity.WireDate = DateTime.Now;
			AccountID = payee.AccountID;
			if (payee.ID != 0) {
				_paymentRequest = new Dal.Netpay.tblCompanyMakePaymentsRequest();
				_paymentRequest.CompanyMakePaymentsProfiles_id = payee.ID;
				_paymentRequest.Company_id = Account.MerchantID.GetValueOrDefault();
				_paymentRequest.paymentAmount = (double)amount;
				_paymentRequest.paymentCurrency = (int)currency;
				_paymentRequest.paymentMerchantComment = comment;
			}
			_entity.wireCompanyName = payee.CompanyName;
			_entity.wirePaymentPayeeName = payee.ContactName;
			_entity.wireCompanyLegalNumber = payee.LegalNumber;
			_entity.WireAmount = amount;
			_entity.WireCurrency = (int)currency;
			_entity.WireComment = comment;
			if (Account.MerchantID != null) {
				var merchant = Merchants.Merchant.Load(Account.MerchantID);
				if (merchant != null) _entity.wireCompanyName = merchant.Name;
				var feeSetting = Merchants.Merchant.GetCurrencySettings(Account.MerchantID.GetValueOrDefault()).Values.Where(c=> c.CurrencyID == (int) currency).SingleOrDefault();
				if (feeSetting != null) _entity.wireFee = feeSetting.WireFee + ((feeSetting.WireFeePercent != 0 ? (feeSetting.WireFeePercent / 100m) : 0) * _entity.WireAmount);
			}
			ProcessingCurrency = Currency;
			Save();
		}

		public static Dictionary<string, string> GetSourceAccountList(string providerName) 
		{
			return (from a in DataContext.Reader.WireAccounts where a.WireProvider_id == providerName select a).ToDictionary(a => a.AccountNumber.EmptyIfNull(), a => string.Format("{0} {1} {2}", a.CurrencyISOCode, a.AccountNumber, a.AccountName));
		}


        public static string MapOfflineFile(string fileName)
        {
            return System.IO.Path.Combine(Domain.Current.MapPrivateDataPath(@"Wires\BatchFiles"), fileName.EmptyIfNull());
        }

		public static string GenerateOfflineProviderFileName(string providerName, string fileName, string fileExt)
		{
            if (fileName == null) fileName = string.Format("Wire_{0}", DateTime.Now.ToString("yyyyMMdd"));
            fileName += "." + fileExt;
            fileName = System.IO.Path.Combine(MapOfflineFile(providerName), fileName);
            if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(fileName)))
                System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(fileName));
            fileName = Domain.GetUniqueFileName(fileName);
			return fileName;
		}

		public static string GetLastGeneratedFile(string providerName)
		{
            string filePath = MapOfflineFile(providerName);
			if (!System.IO.Directory.Exists(filePath)) return null;
			var file = new System.IO.DirectoryInfo(filePath).GetFiles("Wire_*.*", System.IO.SearchOption.TopDirectoryOnly).OrderByDescending(f => f.CreationTime).FirstOrDefault();
			if (file == null) return null;
			return file.FullName;
		}

		private void Save()
		{
			_entity.WireStatusDate = DateTime.Now;
			_entity.WireStatusUser = Login.Current.UserName;
			if (_entity.WireMoney_id == 0)
			{
				if (_paymentRequest != null) {
					if(_paymentRequest.CompanyMakePaymentsRequests_id == 0){
						DataContext.Writer.tblCompanyMakePaymentsRequests.Insert(_paymentRequest);
						_entity.PaymentOrderID = _paymentRequest.CompanyMakePaymentsRequests_id;
					}
				}
				if (_entity.WireComment == null) _entity.WireComment = "";
				if (_entity.WireConfirmationNum == null) _entity.WireConfirmationNum = "";
				if (_entity.wireIDnumber == null) _entity.wireIDnumber = "";
				if (_entity.wireCompanyLegalName == null) _entity.wireCompanyLegalName = "";
				if (_entity.wirePaymentMethod == null) _entity.wirePaymentMethod = "";
				//_entity.WireStatus = "";
			}
			DataContext.Writer.tblWireMoneys.Update(_entity, (_entity.WireMoney_id != 0));
			DataContext.Writer.SubmitChanges();
		}

		public void Approve(int approveLevel, bool? value)
		{
			if (approveLevel == 1) _entity.wireApproveLevel1 = value;
			else if (approveLevel == 2) _entity.wireApproveLevel2 = value;
			string stringValue = value != null ? (value.Value ? "Approved" : "Hold") : "Cleared";

			if (_entity.wireApproveLevel1 == null && _entity.wireApproveLevel2 == null) _entity.WireFlag = (byte) WireApprovalStatus.Waiting;
			else if (_entity.wireApproveLevel1.GetValueOrDefault() && _entity.wireApproveLevel2.GetValueOrDefault()) _entity.WireFlag = (byte)WireApprovalStatus.Approved;
			//else if (_entity.wireApproveLevel1.GetValueOrDefault() || _entity.wireApproveLevel2.GetValueOrDefault()) _entity.WireFlag = (byte)WireApprovalStatus.PartiallyApproved;
			else if (!_entity.wireApproveLevel1.GetValueOrDefault(true) || !_entity.wireApproveLevel2.GetValueOrDefault(true)) _entity.WireFlag = (byte)WireApprovalStatus.Rejected;
			else _entity.WireFlag = (byte)WireApprovalStatus.PartiallyApproved;
			Save();
			WireLog.AddLog(_entity.WireMoney_id, string.Format("Approval changed: {0}", stringValue));
		}

		public void SetComment(string comment) 
		{
			_entity.WireComment = comment;
			Save();
			WireLog.AddLog(_entity.WireMoney_id, string.Format("Comment changed: {0}", comment));
		}

		public void SetSent()
		{
			_entity.WireStatus = (byte) WireStatus.Sent;
			Save();
			WireLog.AddLog(_entity.WireMoney_id, string.Format("Status changed: {0}", _entity.WireStatus));
		}

		public void SetConfirmationNumber(string confirm)
		{
			_entity.WireConfirmationNum = confirm;
			Save();
			WireLog.AddLog(_entity.WireMoney_id, string.Format("Confirmation number changed: {0}", confirm));
		}

		public void Reset(string reason = null)
		{
			if (!IsResetable) return;
			_entity.WireStatus = (byte)WireStatus.Pending;
			Save();
            WireLog.AddLog(_entity.WireMoney_id, "Wire Reset " + reason.EmptyIfNull());
		}

		public bool IsResetable
		{
			get{
				if (Status == WireStatus.Pending || Status == WireStatus.Hold) return false;
				return true;
			}
		}

		public void Cancel()
		{
			if (Status != WireStatus.Completed && Status != WireStatus.Sent) return;
			var ri = Providers.Where(p => p.Name == CommitProvider).SingleOrDefault();
			if (ri == null || !ri.IsCancelable) throw new Exception("Wire Provider not exist or not support cancel"); 
			ri.Cancel(this);
			_entity.WireStatus = (byte)WireStatus.Canceled;
			//_entity.WireProvider_id = null;
			Save();
			WireLog.AddLog(_entity.WireMoney_id, "Wire Canceled");
		}
		
		public bool IsCalcelable{
			get {
				if (Status != WireStatus.Completed && Status != WireStatus.Sent) return false;
				var ri = Providers.Where(p=>p.Name == CommitProvider).SingleOrDefault();
				if (ri == null) return false;
				return ri.IsCancelable;
			}
		}

		public static void Commit(List<Wire> wires, DateTime wireDate, string providerName, string sourceAccountNumber)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Execute);
			var provider = Providers.Where(p=> p.Name == providerName).SingleOrDefault();
			if (provider == null) throw new Exception("Wire provider not found");
			foreach(var w in wires) {
				w._entity.WireStatusDate = DateTime.Now;
				w._entity.WireStatusUser = Login.Current.UserName;
				//w._entity.WireProvider_id = providerName;
				w._entity.WireDate = wireDate;
                w.BatchFailed = false;
            }

			var allCurrencies = wires.Select(w => w.ProcessingCurrency).Distinct().ToList();
			foreach(var cur in allCurrencies) {
				var curWires = wires.Where(w => w.ProcessingCurrency == cur).ToList(); 
				provider.Process(curWires, wireDate, sourceAccountNumber);
				foreach (var w in curWires) {
                    if (w.BatchFailed) w._entity.WireStatus = (byte)StatusNewToOld(WireStatus.Pending, providerName);
                    else w._entity.WireStatus = (byte)StatusNewToOld(WireStatus.Completed, providerName);
					w.Save();
				}
				if (!provider.IsOnline) WiresBatch.CreateBatch(providerName, sourceAccountNumber, cur, curWires);
			}
		}

        public static Wire Load(int id)
        {
            return Load(new List<int>() { id }).SingleOrDefault();
        }

        public static List<Wire> Load(List<int> ids) 
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);
            var ret = (
                from p in DataContext.Reader.tblWireMoneys.Where(Accounts.AccountFilter.Current.QueryValidate(Accounts.AccountType.Merchant, (Dal.Netpay.tblWireMoney w) => w.Company_id))
                join a in DataContext.Reader.Accounts on p.Company_id equals a.Merchant_id
                where ids.Contains(p.WireMoney_id)
                orderby p.wireInsertDate descending
                select new Wire(p, a.Account_id)).ToList();
            var accounts = Accounts.Account.LoadAccount(ret.Select(w => w.AccountID).ToList());
            foreach (var w in ret) w._account = accounts.Where(a => a.AccountID == w.AccountID).FirstOrDefault();
            return ret;
        }

		public static List<Wire> Search(SearchFilters filters, ISortAndPage sortAndPage)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);
			if (filters == null) filters = new SearchFilters();
			//if (pi == null) pi = new PagingInfo() { CurrentPage = 1, PageSize = 200 };  
			var expression = (
				from p in DataContext.Reader.tblWireMoneys.Where(Accounts.AccountFilter.Current.QueryValidate(Accounts.AccountType.Merchant, (Dal.Netpay.tblWireMoney w) => w.Company_id))
				join a in DataContext.Reader.Accounts on p.Company_id equals a.Merchant_id
				orderby p.wireInsertDate descending
				select new { p, a.Account_id });
			if (filters.ID.From != null) expression = expression.Where(x => x.p.WireMoney_id >= filters.ID.From);
			if (filters.ID.To != null) expression = expression.Where(x => x.p.WireMoney_id <= filters.ID.To);
			if (filters.InsertDate.From != null) expression = expression.Where(x => x.p.wireInsertDate >= filters.InsertDate.From.Value);
			if (filters.InsertDate.To != null) expression = expression.Where(x => x.p.wireInsertDate <= filters.InsertDate.To.Value.AlignToEnd());
			if (filters.Date.From != null) expression = expression.Where(x => x.p.WireDate >= filters.Date.From.Value);
			if (filters.Date.To != null) expression = expression.Where(x => x.p.WireDate <= filters.Date.To.Value.AlignToEnd());
			if (filters.Amount.From != null) expression = expression.Where(x => x.p.WireAmount >= filters.Amount.From);
			if (filters.Amount.To != null) expression = expression.Where(x => x.p.WireAmount <= filters.Amount.To);

			if (filters.AccountIDs != null && filters.AccountIDs.Count > 0) expression = expression.Where(x => filters.AccountIDs.Contains(x.Account_id));
			if (filters.Currency != null) expression = expression.Where(x => x.p.WireCurrency == (int)filters.Currency);
			if (filters.ApprovalLevel1 != null) expression = expression.Where(x => x.p.wireApproveLevel1 == filters.ApprovalLevel1);
			if (filters.ApprovalLevel2 != null) expression = expression.Where(x => x.p.wireApproveLevel2 == filters.ApprovalLevel2);

			if (filters.Status != null && filters.Status.Count() > 0) expression = expression.Where(x => filters.Status.Contains((WireStatus)x.p.WireStatus));
			if (filters.ApprovalStatus != null && filters.ApprovalStatus.Count() > 0) expression = expression.Where(x => filters.ApprovalStatus.Contains((WireApprovalStatus)x.p.WireFlag));
			//if (filters.Provider != null) expression = expression.Where(p => p.WireProvider_id == filters.Provider);

			if (filters.Source != null && filters.Source.Count() > 0) expression = expression.Where(x => filters.Source.Contains((WireSource)x.p.WireType));
			
			if (filters.MerchantSettlementID != null) expression = expression.Where(x => x.p.SettlementID == filters.MerchantSettlementID);
			if (filters.AffiliateSettlementID != null) expression = expression.Where(x => x.p.AffiliatePaymentsID == filters.AffiliateSettlementID);
			if (filters.PayeeID != null) expression = expression.Where(x => x.p.PaymentOrderID == filters.PayeeID);

			var ret = expression.ApplySortAndPage(sortAndPage, "p.").Select(x => new Wire(x.p, x.Account_id)).ToList();
			var accounts = Accounts.Account.LoadAccount(ret.Select(w => w.AccountID).ToList());
			foreach(var w in ret) w._account = accounts.Where(a => a.AccountID == w.AccountID).FirstOrDefault();
			return ret;
		}

		public static Dictionary<int, string> IsraelBankList
		{
			get{
				var ret = Domain.Current.GetCachData("IsraelBankList") as Dictionary<int, string>;
				if (ret == null) Domain.Current.SetCachData("IsraelBankList", ret = DataContext.Reader.tblSystemBankLists.ToDictionary(x => x.id, x => string.Format("{0} - {1}", x.bankCode, x.bankName)));
				return ret;
			}
		}

	}
}
