﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Wires
{
    public class Wire : BaseDataObject
    {
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Wires.Module.Current, SecuredObjectName); } }

        public enum WireSource { AccountBankAccount, MerchantBeneficiary }
        public enum WireStatus { Pending = 0, Canceled = 1, Hold = 2, Sent = 3, Completed = 10 }
        public enum WireApprovalStatus { Waiting = 0, Approved = 1, Rejected = 2, PartiallyApproved = 4 }
        public static Dictionary<WireStatus, System.Drawing.Color> WireStatusColor = new Dictionary<WireStatus, System.Drawing.Color> {
            { WireStatus.Pending, System.Drawing.Color.FromName("#7d7d7d") } ,
            { WireStatus.Canceled, System.Drawing.Color.FromName("#ff6666") },
            { WireStatus.Hold, System.Drawing.Color.FromName("#ffd400") },
            { WireStatus.Completed, System.Drawing.Color.FromName("#6699cc") }
        };
        public static Dictionary<WireApprovalStatus, System.Drawing.Color> WireApprovalStatusColor = new Dictionary<WireApprovalStatus, System.Drawing.Color> {
            { WireApprovalStatus.Waiting, System.Drawing.Color.FromName("#7d7d7d") } ,
            { WireApprovalStatus.Rejected, System.Drawing.Color.FromName("#ff6666") },
            { WireApprovalStatus.PartiallyApproved, System.Drawing.Color.FromName("#ffd400") },
            { WireApprovalStatus.Approved, System.Drawing.Color.FromName("#66cc66") }
        };
        
		public class SearchFilters {
			public Range<DateTime?> Date;
			public Range<DateTime?> InsertDate;
			public Range<int?> ID;
			public Range<decimal?> Amount;
			public CommonTypes.Currency? Currency;
			public List<int> AccountIDs;
			public string Provider;
			public List<WireStatus> Status;
			public List<WireApprovalStatus> ApprovalStatus;
            public bool? IsShow;
			public bool? ApprovalLevel1;
			public bool? ApprovalLevel2;
			public string PayeeName;
			public string PayeeNumber;
            public int? BatchID;
            public int? MerchantSettlementID, AffiliateSettlementID, PayeeID;
        }

        private Dal.Netpay.Wire _entity;

        public int ID { get { return _entity.Wire_id; } }
        public System.DateTime InsertDate { get { return _entity.CreateDate; } }
        public System.DateTime? Date { get { return _entity.WireDate; } }
        public decimal Amount { get { return _entity.Amount; } }
        public CommonTypes.Currency Currency { get { return Bll.Currency.Get(_entity.CurrencyISOCode).EnumValue; } }
        public string Comment { get { return _entity.Comment; } }

        public bool? ApprovalLevel1 { get { return _entity.IsApproveLevel1; } }
        public bool? ApprovalLevel2 { get { return _entity.IsApproveLevel2; } }

        public System.DateTime? CommitDate { get { return _entity.WireDate; } }
        public WireStatus Status { get { return (WireStatus)_entity.WireStatus; } }

        public string CommitProvider { get { return _entity.WireProvider_id; } }
        public string ConfirmationNumber { get { return _entity.ProviderReference1; } }

        public System.DateTime? LastLogDate { get { return _entity.ActionDate; } }
        public string CommitUser { get { return _entity.ActionUser; } }

        public CommonTypes.Currency ProcessingCurrency
        {
            get { return Bll.Currency.Get(_entity.CurrencyISOCodeProcessed).EnumValue; }
            set { _entity.CurrencyISOCodeProcessed = Bll.Currency.Get(value).IsoCode; }
        }
        public bool CanUpdateProcessingCurrency { get { return (Status != WireStatus.Sent) && (Status != WireStatus.Completed); } }
        public bool CanSetProvider { get { return (Status == WireStatus.Pending) && (ApprovalStatus == WireApprovalStatus.Approved); } }
        public bool CanApprove { get { return (Status == WireStatus.Pending); } }
        public bool CanSetAmount { get { return (Status == WireStatus.Pending) || (Status == WireStatus.Hold); } }

        //public decimal ExchangeRate { get { return (decimal)_entity.WireExchangeRate; } }
        public decimal Fee { get { return _entity.WireFee; } }
        public decimal TransferAmount { get { return _entity.AmountProcessed; } }

        public bool IsShow { get { return _entity.IsShow; } }

        public int? BatchID { get { return _entity.WireBatch_id; } internal set { _entity.WireBatch_id = value; } }
        public int AccountID { get { return _entity.Account_id; } }
        public int? PayeeID { get { return _entity.AccountPayee_id; } }
        public int? MerchantSettlementID { get { return _entity.MerchantSettlement_id; } }
        public int? AffiliateSettlementID { get { return _entity.AffiliateSettlement_id; } }

        public string AccountInfo { get { return _entity.TargetBankAccountText; } set { _entity.TargetBankAccountText = value; } }
        public string PayeeName { get { return _entity.TargetTitle; } set { _entity.TargetTitle = value; } }

        public bool IsDone { get { return Status == WireStatus.Canceled || Status == WireStatus.Sent; } }
        public bool BatchFailed { get; set; }

        public Accounts.BankAccount _bankAccount;
        public Accounts.BankAccount BankAccount
        {
            get
            {
                if (_bankAccount == null)
                {
                    if (PayeeID.HasValue)
                    {
                        _bankAccount = Accounts.Payee.Load(PayeeID.Value).BankAccount;
                    }
                    else
                    {
                        _bankAccount = Accounts.BankAccount.LoadForAccount(AccountID, Currency);
                    }
                }
                return _bankAccount;
            }
        }

        private Accounts.Account _account;
        public Accounts.Account Account
        {
            get
            {
                if (_account == null) _account = Accounts.Account.LoadAccount(_entity.Account_id);
                return _account;
            }
        }

        private Wire(Dal.Netpay.Wire entity)
        {
            _entity = entity;
        }

        public Wire(int accountId, int? settlementId, bool isAffiliateSettlementId = false)
        {
            _entity = new Dal.Netpay.Wire();
            _entity.CreateDate = DateTime.Now;
            _entity.Account_id = accountId;
            if (isAffiliateSettlementId) _entity.AffiliateSettlement_id = settlementId;
            else _entity.MerchantSettlement_id = settlementId;
            _entity.TargetTitle = Accounts.Account.GetNames(new List<int> { accountId }).FirstOrDefault().Value;
        }

        public Wire(Accounts.Payee payee, decimal amount, CommonTypes.Currency currency, string comment)
        {
            _entity = new Dal.Netpay.Wire();
            _entity.CreateDate = DateTime.Now;
            _entity.Account_id = payee.AccountID;
            if (payee.ID != 0) _entity.AccountPayee_id = payee.ID;
            _entity.TargetTitle = payee.CompanyName;
            _entity.Amount = amount;
            _entity.CurrencyISOCode = Bll.Currency.Get(currency).IsoCode;
            _entity.Comment = comment;
            if (Account.MerchantID != null)
            {
                //var merchant = Merchants.Merchant.Load(Account.MerchantID);
                //if (merchant != null) _entity.wireCompanyName = merchant.Name;
                var feeSetting = Settlements.MerchantSettings.LoadForMerchant(Account.MerchantID.GetValueOrDefault()).Values.Where(c => c.CurrencyID == (int)currency).SingleOrDefault();
                if (feeSetting != null) _entity.WireFee = feeSetting.WireFee + ((feeSetting.WireFeePercent != 0 ? (feeSetting.WireFeePercent / 100m) : 0) * _entity.Amount);
            }
            ProcessingCurrency = Currency;
            Save();
        }

        public void SetAmount(CommonTypes.Currency currency, decimal amount)
        {
            _entity.CurrencyISOCode = Bll.Currency.Get(currency).IsoCode;
            _entity.AmountProcessed = _entity.Amount = amount;
            Save();
        }

        public static Dictionary<string, string> GetSourceAccountList(string providerName)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from a in DataContext.Reader.WireAccounts where a.WireProvider_id == providerName select a).ToDictionary(a => a.AccountNumber.EmptyIfNull(), a => string.Format("{0} {1} {2}", a.CurrencyISOCode, a.AccountNumber, a.AccountName));
        }

        public static string MapOfflineFile(string providerName, string fileName = null)
        {
            providerName = System.IO.Path.Combine(Domain.Current.MapPrivateDataPath(@"Wires\BatchFiles"), providerName.EmptyIfNull());
            if (fileName != null) providerName = System.IO.Path.Combine(providerName, fileName);
            return providerName;
        }

        public static string GenerateOfflineProviderFileName(string providerName, string fileName, string fileExt)
        {
            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
            //Logger.Log(LogTag.Wires, "Inside GenerateOfflineProviderFileName() method of " + providerName);

            if (fileName == null) fileName = string.Format("Wire_{0}", DateTime.Now.ToString("yyyyMMdd"));
            fileName += "." + fileExt;
            fileName = MapOfflineFile(providerName, fileName);
            if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(fileName)))
                System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(fileName));
            fileName = Domain.GetUniqueFileName(fileName);
            return fileName;
        }

        public static Wire Load(int id)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from p in DataContext.Reader.Wires where p.Wire_id == id select new Wire(p)).SingleOrDefault();
        }

        public static List<Wire> Load(List<int> ids)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);
            var ret = (
                from p in DataContext.Reader.Wires.Where(Accounts.AccountFilter.Current.QueryValidate(null, (Dal.Netpay.Wire w) => w.Account_id))
                join a in DataContext.Reader.Accounts on p.Account_id equals a.Account_id
                where ids.Contains(p.Wire_id)
                orderby p.CreateDate descending
                select new Wire(p)).ToList();
            var accounts = Accounts.Account.LoadAccount(ret.Select(w => w.AccountID).ToList());
            foreach (var w in ret) w._account = accounts.Where(a => a.AccountID == w.AccountID).FirstOrDefault();
            return ret;
        }

        private void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            _entity.ActionDate = DateTime.Now;
            _entity.ActionUser = Login.Current.UserName;
            if (_entity.CurrencyISOCodeProcessed == null) _entity.CurrencyISOCodeProcessed = _entity.CurrencyISOCode;
            if (_entity.Wire_id == 0)
            {
                if (_entity.Comment == null) _entity.Comment = "";
                if (_entity.ProviderReference1 == null) _entity.ProviderReference1 = "";
                //if (_entity.wireIDnumber == null) _entity.wireIDnumber = "";
                //if (_entity.wireCompanyLegalName == null) _entity.wireCompanyLegalName = "";
                //if (_entity.wirePaymentMethod == null) _entity.wirePaymentMethod = "";
                //_entity.WireStatus = "";
            }
            DataContext.Writer.Wires.Update(_entity, (_entity.Wire_id != 0));
            DataContext.Writer.SubmitChanges();
        }

        public WireApprovalStatus ApprovalStatus
        {
            get
            {
                if (_entity.IsApproveLevel1 == null && _entity.IsApproveLevel2 == null) return WireApprovalStatus.Waiting;
                else if (_entity.IsApproveLevel1.GetValueOrDefault() && _entity.IsApproveLevel2.GetValueOrDefault()) return WireApprovalStatus.Approved;
                else if (!_entity.IsApproveLevel1.GetValueOrDefault(true) || !_entity.IsApproveLevel2.GetValueOrDefault(true)) return WireApprovalStatus.Rejected;
                return WireApprovalStatus.PartiallyApproved;
            }
        }

        public void Approve(int approveLevel, bool? value)
        {
            if (approveLevel == 1) _entity.IsApproveLevel1 = value;
            else if (approveLevel == 2) _entity.IsApproveLevel2 = value;
            string stringValue = value != null ? (value.Value ? "Approved" : "Hold") : "Cleared";
            Save();
            WireLog.AddLog(_entity.Wire_id, string.Format("Approval changed: {0}", stringValue));
        }

        public void SetComment(string comment)
        {
            _entity.Comment = comment;
            Save();
            WireLog.AddLog(_entity.Wire_id, string.Format("Comment changed: {0}", comment));
        }

        public void SetSent()
        {
            _entity.WireStatus = (byte)WireStatus.Sent;
            Save();
            WireLog.AddLog(_entity.Wire_id, string.Format("Status changed: {0}", _entity.WireStatus));
        }

        public void SetConfirmationNumber(string confirm)
        {
            _entity.ProviderReference1 = confirm;
            Save();
            WireLog.AddLog(_entity.Wire_id, string.Format("Confirmation number changed: {0}", confirm));
        }

        public void Reset(string reason = null)
        {
            if (!IsResetable) return;
            _entity.WireStatus = (byte)WireStatus.Pending;
            Save();
            WireLog.AddLog(_entity.Wire_id, "Wire Reset " + reason.EmptyIfNull());
        }

        public bool IsResetable
		{
			get{
				if (Status == WireStatus.Pending || Status == WireStatus.Hold) return false;
				return true;
			}
		}

        public void Delete()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            if (_entity.Wire_id == 0) return;
            
            if (!CanSetAmount) throw new Exception("Can;t delete wire on status:" + Status);
            DataContext.Writer.Wires.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }

        public void Cancel()
		{
			if (Status != WireStatus.Completed && Status != WireStatus.Sent) return;
			var ri = Provider.Get(CommitProvider);
			if (ri == null || !ri.IsCancelable) throw new Exception("Wire Provider not exist or not support cancel"); 
			ri.Cancel(this);
			_entity.WireStatus = (byte)WireStatus.Canceled;
			_entity.WireProvider_id = null;
			Save();
			WireLog.AddLog(_entity.Wire_id, "Wire Canceled");
		}
		
		public bool IsCalcelable{
			get {
				if (Status != WireStatus.Completed && Status != WireStatus.Sent) return false;
                var ri = Provider.Get(CommitProvider);
                if (ri == null) return false;
				return ri.IsCancelable;
			}
		}

		public static void Commit(List<Wire> wires, DateTime wireDate, string providerName, string sourceAccountNumber)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            var provider = Provider.Get(providerName);
			if (provider == null) throw new Exception("Wire provider not found");
			foreach(var w in wires) {
				w._entity.ActionDate = DateTime.Now;
				w._entity.ActionUser = Login.Current.UserName;
				w._entity.WireProvider_id = providerName;
				w._entity.WireDate = wireDate;
			}

			var allCurrencies = wires.Select(w => w.ProcessingCurrency).Distinct().ToList();
			foreach(var cur in allCurrencies) {
				var curWires = wires.Where(w => w.ProcessingCurrency == cur).ToList(); 
				provider.Process(curWires, wireDate, sourceAccountNumber);
				foreach (var w in curWires){
					w._entity.WireStatus = (byte)WireStatus.Completed;
					w.Save(); 
				}
				if (!provider.IsOnline && providerName != "System.FNB") WiresBatch.CreateBatch(providerName, sourceAccountNumber, cur, curWires);
			}
		}

        private static IQueryable<Dal.Netpay.Wire> SearchExpression(SearchFilters filters)
        {            
            if (filters == null) filters = new SearchFilters();
            //if (pi == null) pi = new PagingInfo() { CurrentPage = 1, PageSize = 200 };  
            var expression = (from p in DataContext.Reader.Wires.Where(Accounts.AccountFilter.Current.QueryValidate(null, (Dal.Netpay.Wire x) => x.Account_id)) orderby p.Wire_id descending select p) as IQueryable<Dal.Netpay.Wire>;
            if (filters.ID.From != null) expression = expression.Where(p => p.Wire_id >= filters.ID.From);
            if (filters.ID.To != null) expression = expression.Where(p => p.Wire_id <= filters.ID.To);
            if (filters.InsertDate.From != null) expression = expression.Where(p => p.CreateDate >= filters.InsertDate.From);
            if (filters.InsertDate.To != null) expression = expression.Where(p => p.CreateDate <= filters.InsertDate.To.Value.AlignToEnd());
            if (filters.Date.From != null) expression = expression.Where(p => p.WireDate >= filters.Date.From);
            if (filters.Date.To != null) expression = expression.Where(p => p.WireDate <= filters.Date.To.Value.AlignToEnd());
            if (filters.Amount.From != null) expression = expression.Where(p => p.Amount >= filters.Amount.From);
            if (filters.Amount.To != null) expression = expression.Where(p => p.Amount <= filters.Amount.To);

            if (filters.AccountIDs != null && filters.AccountIDs.Count > 0) expression = expression.Where(p => filters.AccountIDs.Contains(p.Account_id));
            if (filters.Currency != null) expression = expression.Where(p => p.CurrencyISOCode == Bll.Currency.Get(filters.Currency.Value).IsoCode);
            if (filters.ApprovalLevel1 != null) expression = expression.Where(p => p.IsApproveLevel1 == filters.ApprovalLevel1);
            if (filters.ApprovalLevel2 != null) expression = expression.Where(p => p.IsApproveLevel2 == filters.ApprovalLevel2);

            if (filters.Status != null && filters.Status.Count() > 0) expression = expression.Where(p => filters.Status.Contains((WireStatus)p.WireStatus));
            if (filters.Provider != null) expression = expression.Where(p => p.WireProvider_id == filters.Provider);

            if (filters.BatchID != null) expression = expression.Where(p => p.WireBatch_id == filters.BatchID.Value);

            if (filters.MerchantSettlementID != null) expression = expression.Where(p => p.MerchantSettlement_id == filters.MerchantSettlementID);
            if (filters.AffiliateSettlementID != null) expression = expression.Where(p => p.AffiliateSettlement_id == filters.AffiliateSettlementID);
            if (filters.PayeeID != null) expression = expression.Where(p => p.AccountPayee_id == filters.PayeeID);
            if (filters.IsShow.HasValue) expression = expression.Where(p => p.IsShow == filters.IsShow.Value);

            //Approval status filter:
            //if (filters.ApprovalStatus != null && filters.ApprovalStatus.Count() > 0) expression = expression.Where(p => filters.ApprovalStatus.Contains(GetWireEntityApprovalStatus(p.IsApproveLevel1, p.IsApproveLevel2)));
            if (filters.ApprovalStatus != null)
            {
                if (filters.ApprovalStatus.Contains(WireApprovalStatus.Waiting)) expression = expression.Where(p => p.IsApproveLevel1 == null && p.IsApproveLevel2 == null);
                if (filters.ApprovalStatus.Contains(WireApprovalStatus.Approved)) expression = expression.Where(p => p.IsApproveLevel1.GetValueOrDefault() && p.IsApproveLevel2.GetValueOrDefault());
                if (filters.ApprovalStatus.Contains(WireApprovalStatus.Rejected)) expression = expression.Where(p => !p.IsApproveLevel1.GetValueOrDefault(true) || !p.IsApproveLevel2.GetValueOrDefault(true));
                if (filters.ApprovalStatus.Contains(WireApprovalStatus.PartiallyApproved)) expression = expression.Where(p => ((p.IsApproveLevel1==null && p.IsApproveLevel2.GetValueOrDefault()) || (p.IsApproveLevel2 == null && p.IsApproveLevel1.GetValueOrDefault())));
            }
            
            return expression;
        }

		public static List<Wire> Search(SearchFilters filters, ISortAndPage sortAndPage)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var expression = SearchExpression(filters);

            var ret = expression.ApplySortAndPage(sortAndPage).Select(p => new Wire(p)).ToList();
            var accounts = Accounts.Account.LoadAccount(ret.Select(w => w.AccountID).ToList());
            foreach (var w in ret) w._account = accounts.Where(a => a.AccountID == w.AccountID).FirstOrDefault();
            return ret;
        }

        public static int Count(SearchFilters filters)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var expression = SearchExpression(filters);
            return expression.Count(x=>x.Wire_id>0);
        }

        public enum OldWireStatus { New = 0, Canceled = 1, Pending = 2, SentByMasav = 3, SentByPrint = 4, SetBalance = 5, Wired = 6, Hellenic = 7, Georgia = 8, FNB = 9 }
        public static WireStatus StatusOldToNew(OldWireStatus value)
        {
            switch (value)
            {
                case OldWireStatus.New: return WireStatus.Pending;
                case OldWireStatus.Pending: return WireStatus.Hold;
                case OldWireStatus.Canceled: return WireStatus.Canceled;
                //case OldWireStatus.SentByMasav: return WireStatus.Completed;
                case OldWireStatus.SentByPrint: return WireStatus.Sent;
                    //case OldWireStatus.SetBalance: return WireStatus.Completed;
                    //case OldWireStatus.Wired: return WireStatus.Completed;
                    //case OldWireStatus.Hellenic: return WireStatus.Completed;
                    //case OldWireStatus.Georgia: return WireStatus.Completed;
            }
            return WireStatus.Completed;
        }

        public static OldWireStatus StatusNewToOld(WireStatus value, string providerName)
        {
            switch (value)
            {
                case WireStatus.Pending: return OldWireStatus.New;
                case WireStatus.Canceled: return OldWireStatus.Canceled;
                case WireStatus.Hold: return OldWireStatus.Pending;
                case WireStatus.Sent:
                case WireStatus.Completed:
                    switch (providerName)
                    {
                        case "System.Manual": return OldWireStatus.SentByPrint;
                        case "System.Balance": return OldWireStatus.SetBalance;
                        case "System.Masav": return OldWireStatus.SentByMasav;
                        case "System.BOG": return OldWireStatus.Georgia;
                        case "System.Hellenic": return OldWireStatus.Hellenic;
                        case "System.FNB": return OldWireStatus.FNB;
                    }
                    break;
            }
            return OldWireStatus.Canceled;
        }
    }
}
