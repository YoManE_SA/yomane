﻿using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.Wires
{
    public class WireBankAccount
    {
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(WiresBankAccounts.Module.Current, SecuredObjectName); } }

        public class SearchFilters {
			public Range<int?> ID;
			public List<string> ProviderID;

            //Provider Id string filter
            public string Provider;

        }
        private Dal.Netpay.WireAccount _entity;

        public short ID { get { return _entity.WireAccount_id; } }
        public string ProviderID { get { return _entity.WireProvider_id; } set { _entity.WireProvider_id = value; } }
        public string AccountName { get { return _entity.AccountName; } set { _entity.AccountName = value; } }
        public string AccountNumber { get { return _entity.AccountNumber; } set { _entity.AccountNumber = value; } }
        public string AccountBranch { get { return _entity.AccountBranch; } set { _entity.AccountBranch = value; } }
        public string CurrencyISOCode { get { return _entity.CurrencyISOCode; } set { _entity.CurrencyISOCode = value; } }

       // public string SshPrivateKeyPath { get { return string.Empty; } set { var test = value; } }
       // public string UserCode { get { return string.Empty; } set { var test = value; } }
       // public string UserName { get { return string.Empty; } set { var test = value; } }
       // public System.Data.Linq.Binary EncryptedPassword { get { return null; } set { var test = value; } }
       // public int? EncryptionKey { get { return null; } set { var test = value; } }


        public string SshPrivateKeyPath { get { return _entity.SshPrivateKeyPath; } set { _entity.SshPrivateKeyPath = value; } }
        public string UserName { get { return _entity.UserName; } set { _entity.UserName = value; } }
        public string SshPrivateKeyPassphrase { get { return _entity.SshPrivateKeyPassphrase; } set { _entity.SshPrivateKeyPassphrase = value; } }
        public string UserCode { get { return _entity.UserCode; } set { _entity.UserCode = value; } }
        public int? NextWireBatchNum { get { return _entity.NextWireBatchNum; } set { _entity.NextWireBatchNum = value; } }
        public int? EncryptionKey { get { return _entity.EncryptionKey; } set { _entity.EncryptionKey = value; } }
        public System.Data.Linq.Binary EncryptedPassword { get { return _entity.EncryptedPassword; } set { _entity.EncryptedPassword = value; } }

        public string Title { get { return string.Format("{0} {1}, {2}-{3}", CurrencyISOCode, AccountName, AccountBranch, AccountName); } }

        private WireBankAccount(Dal.Netpay.WireAccount entity) { _entity = entity; }
        public WireBankAccount() { _entity = new Dal.Netpay.WireAccount(); }

        public static WireBankAccount Load(int id)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from a in Infrastructure.DataContext.Reader.WireAccounts where a.WireAccount_id == id select new WireBankAccount(a)).SingleOrDefault();
        }

        public static WireBankAccount Load(string providerName, string accountNumber)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from a in Infrastructure.DataContext.Reader.WireAccounts where a.WireProvider_id == providerName && a.AccountNumber == accountNumber select new WireBankAccount(a)).SingleOrDefault();
        }

        public static List<WireBankAccount> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

			if (filters == null) filters = new SearchFilters();
            var expression = (from p in DataContext.Reader.WireAccounts select p);
            if (filters.ID.From != null) expression = expression.Where(p => p.WireAccount_id >= filters.ID.From);
            if (filters.ID.To != null) expression = expression.Where(p => p.WireAccount_id <= filters.ID.To);
            if (filters.ProviderID != null && filters.ProviderID.Count() > 0) expression = expression.Where(p => filters.ProviderID.Contains(p.WireProvider_id));

            //String Provider search
            if (filters.Provider != null) expression = expression.Where(p => p.WireProvider_id == filters.Provider);

            return expression.ApplySortAndPage(sortAndPage).Select(p => new WireBankAccount(p)).ToList();
        }

        public static List<KeyValuePair<string, string>> AutoCompleteByAcountNumber(string text)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            int numValue = text.ToNullableInt().GetValueOrDefault(-1);
            return (from c in DataContext.Reader.WireAccounts
                    where c.AccountNumber.StartsWith(text)
                    select new { c.AccountNumber, c.AccountName })
                    .ToDictionary(x => x.AccountNumber, x => x.AccountName)
                    .Take(10).ToList();
        }

        public void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
            {
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);
                if (_entity.WireAccount_id == 0) ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);
            }

            DataContext.Writer.WireAccounts.Update(_entity, (_entity.WireAccount_id != 0));
            DataContext.Writer.SubmitChanges();
        }

        public void Delete()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            DataContext.Writer.WireAccounts.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }
    }
}
