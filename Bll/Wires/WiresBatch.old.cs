﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Wires
{
	public class WiresBatch : BaseDataObject
	{
		public const string SecuredObjectName = "WiresBatch";
		public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Wires.Module.Current, SecuredObjectName); } }

		public class WiresBatchItem : BaseDataObject
		{
			private Dal.Netpay.tblLogMasavDetail _entity;

			public int ID { get { return _entity.logMasavDetails_id; } }
			public int BatchID { get { return _entity.logMasavFile_id; } }
			public int AccountID { get; set; }
			public int WireID { get { return _entity.WireMoney_id; } }
			public string PayeeName { get { return _entity.PayeeName; } }
			public string PayeeBankDetails { get { return _entity.PayeeBankDetails; } }
			public decimal Amount { get { return (decimal)_entity.Amount; } }
			public CommonTypes.Currency Currency { get { return (CommonTypes.Currency) _entity.Currency; } }
			public bool IsDone { get { return _entity.SendStatus != 0; } }
			public string Comment { get { return _entity.StatusNote; } }
			//public byte LogStatus { get { return _entity.LogStatus; } }
			public string UserName { get { return _entity.StatusUserName; } }

			private WiresBatchItem(Dal.Netpay.tblLogMasavDetail entity)
				
			{ 
				_entity = entity;
			}

			public static WiresBatchItem Load(int id)
			{
				ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
				return (from l in DataContext.Reader.tblLogMasavDetails where l.logMasavDetails_id == id select new WiresBatchItem(l)).SingleOrDefault();
			}

			public static List<WiresBatchItem> Search(int batchId)
			{
				ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
				return (from l in DataContext.Reader.tblLogMasavDetails where l.logMasavFile_id == batchId select new WiresBatchItem(l)).ToList();
			}

			private void Save()
			{
				DataContext.Writer.tblLogMasavDetails.Update(_entity, (_entity.logMasavDetails_id != 0));
				DataContext.Writer.SubmitChanges();
			}

			public void MarkAsDone()
			{
				_entity.SendStatus = 1;
				_entity.StatusUserName = Login.Current.UserName;
				Save();
				Wire.Load(WireID).SetSent();

				var notDoneItems = Search(BatchID).Where(i => !i.IsDone).Count();
				if (notDoneItems == 0) {
					var batch = WiresBatch.Load(BatchID);
					batch._entity.DoneFlag = 1;
					batch.Save();
				}
			}

			public void SetComment(string comment)
			{
				_entity.StatusUserName = Login.Current.UserName;
				_entity.StatusNote = comment;
				Save();
				WireLog.AddLog(WireID, "Batch Item Comment change: " + comment);

			}

		}

		public class SearchFilters
		{
			public Range<int?> ID;
			public Range<DateTime?> Date;
			public CommonTypes.Currency? Currency;
			public int? WireID;
			public string Acceptor;
		}

		private Dal.Netpay.tblLogMasavFile _entity;

		public int ID { get{ return _entity.logMasavFile_id; } }
		public System.DateTime Date { get { return _entity.insertDate; } }
        public string ProviderID { get { return _entity.WireProvider_Id; } }
		public string BankCode { get { return _entity.PayedBankCode; } }
		public string SourceBankAccount { get { return _entity.PayedBankDesc; } }
		public string UserName { get { return _entity.LogonUser; } }
		public CommonTypes.Currency Currency { get { return (CommonTypes.Currency)_entity.PayCurrency; } }
		public decimal Amount { get { return (decimal)_entity.PayAmount; } }
		public int Count { get { return _entity.PayCount; } }
		public bool Completed { get { return _entity.DoneFlag == 1; } }
		public byte Flag { get { return _entity.Flag; } }
        public string FileName { get { return Wire.MapOfflineFile(_entity.FileName); } }
		//public string AttachedFileExt { get { return _entity.AttachedFileExt; } }

		private WiresBatch(Dal.Netpay.tblLogMasavFile entity)
		{ 
			_entity = entity;
		}

		public static WiresBatch CreateBatch(string providerId, string sourceAccountNumber, CommonTypes.Currency currency, List<Wire> wires)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Execute);
			var context = new DataContext(true);
			var fileEntity = new Dal.Netpay.tblLogMasavFile();
			fileEntity.insertDate = DateTime.Now;
            fileEntity.WireProvider_Id = providerId;
            fileEntity.PayedBankCode = Wire.Providers.Where(p => p.Name == providerId).Single().FriendlyName;
            if (!string.IsNullOrEmpty(sourceAccountNumber)) fileEntity.PayedBankDesc = Wire.GetSourceAccountList(providerId)[sourceAccountNumber];
			else fileEntity.PayedBankDesc = "";
			fileEntity.LogonUser = Login.Current.UserName;
			fileEntity.PayAmount = 0;
			fileEntity.PayCount = 0;
			fileEntity.PayCurrency = (byte)currency;
			fileEntity.AttachedFileExt = "";
			context.tblLogMasavFiles.InsertOnSubmit(fileEntity);
			context.SubmitChanges();

			foreach(var wire in wires) {
				var entity = new Dal.Netpay.tblLogMasavDetail();
				entity.logMasavFile_id = fileEntity.logMasavFile_id;
				entity.WireMoney_id = wire.ID;
				entity.Company_id = wire.Account.MerchantID.GetValueOrDefault();
				entity.Amount = (double)wire.TransferAmount;
				entity.Currency = (byte) wire.ProcessingCurrency;
				entity.PayeeName = string.Format("{0} - {1}", wire.Account.AccountName, wire.BankAccount.AccountName);
				entity.PayeeBankDetails = (wire.BankAccount != null) ? wire.BankAccount.ToString() : string.Empty;
				//if (wire.BankAccount.RefAccountBankAccount_id)
				entity.StatusNote = "";
				entity.StatusUserName = wire.CommitUser;
				fileEntity.PayAmount += entity.Amount;
				fileEntity.PayCount += 1;
				context.tblLogMasavDetails.InsertOnSubmit(entity);
			}
			context.SubmitChanges();
            return new WiresBatch(fileEntity);
		}

		public void AttachFile(string description, string fileName, System.IO.Stream fileData)
		{
			var items = WiresBatchItem.Search(ID);
			foreach(var item in items)
				WireFile.AddFile(item.WireID, description, fileName, fileData);
		}
		
		public void AddComment(string description)
		{
			var items = WiresBatchItem.Search(ID);
			foreach (var item in items)
				WireLog.AddLog(item.WireID, description);
		}

		public void SetFlag(byte value)
		{
			_entity.Flag = value;
			Save();
		}

        public void SetPrepared(string fileName, bool requireSend) 
        {
            if(!string.IsNullOrEmpty(fileName)){
                var strBase = Wire.MapOfflineFile(null);
                if (fileName.StartsWith(strBase)) fileName = fileName.Substring(strBase.Length).TrimStart(new char[] { '/', '\\' });
                _entity.FileName = fileName;
            }
            _entity.IsSendRequire = requireSend;
            Save();
        }

        public void SetSentComplete() 
        {
            _entity.IsSendRequire = false;
            Save();
        } 

		private void Save()
		{
			DataContext.Writer.tblLogMasavFiles.Update(_entity, (_entity.logMasavFile_id != 0));
			DataContext.Writer.SubmitChanges();
		}

		public static WiresBatch Load(int id)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
			return (from l in DataContext.Reader.tblLogMasavFiles where l.logMasavFile_id == id select new WiresBatch(l)).SingleOrDefault();
		}

        public static WiresBatch Load(string fileName)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
            return (from l in DataContext.Reader.tblLogMasavFiles where l.FileName == fileName select new WiresBatch(l)).SingleOrDefault();
        }

        public static List<WiresBatch> Search(SearchFilters filters, ISortAndPage sortAndPage)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
			var expression = (from l in DataContext.Reader.tblLogMasavFiles orderby l.insertDate descending select l);
			if (filters != null)
			{
			
			}
			return expression.ApplySortAndPage(sortAndPage).Select(p => new WiresBatch(p)).ToList();
		}

        public List<Wire> GetWires() 
        {
            return Wire.Load((from l in DataContext.Reader.tblLogMasavDetails where l.logMasavFile_id == ID select l.WireMoney_id).ToList());
        }

        public List<WiresBatch> GetWaitingSend(string providerId) 
        { 
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
            return (from l in DataContext.Reader.tblLogMasavFiles where l.WireProvider_Id == providerId orderby l.insertDate descending select new WiresBatch(l)).ToList();
        }

        private static System.Threading.Timer _timerSend;
        public static bool EnableRealTimeSend
        {
            get { return (_timerSend == null); }
            set
            {
                if (value) {
                    if (_timerSend == null) _timerSend = new System.Threading.Timer(RealTimeSendTimer, null, 0, 3 * 60 * 1000);
                } else if (_timerSend != null) _timerSend.Dispose();
            }
        }

        private static void RealTimeSendTimer(object state)
        {
            foreach (var d in Domain.Domains.Values)
            {
                Domain.Current = d;
                if (!Infrastructure.Tasks.Lock.TryLock("WIRE_Batch", DateTime.Now.AddMinutes(-3))) continue;
                try {
                    int successCount = 0;
                    ObjectContext.Current.Impersonate(d.ServiceCredentials);
                    var rows = (from l in DataContext.Reader.tblLogMasavFiles where l.IsSendRequire orderby l.insertDate descending select new WiresBatch(l)).ToList();
                    foreach (var batch in rows) { 
                        var provider = Wire.Providers.Where(p => p.Name == batch.ProviderID).SingleOrDefault();
                        try {
                            provider.Send(batch);
                            batch.SetSentComplete();
                            successCount++;
                        } catch (Exception ex) {
                            Logger.Log(ex, string.Format("WireBatch #{0} send Failed, Domain: {1}", batch.ID, d.Host));
                        }
                    }
                    if (rows.Count > 0) Logger.Log(LogTag.Wires, string.Format("WireBatch Send Completed for domain: {0}, success {1} of {2} wires", d.Host, successCount, rows.Count));
                } catch (Exception ex) {
                    Logger.Log(ex, "WireBatch Send for domain: " + d.Host);
                } finally {
                    Infrastructure.Tasks.Lock.Release("WIRE_Batch");
                    ObjectContext.Current.StopImpersonate();
                }
            }
        }
	}
}
