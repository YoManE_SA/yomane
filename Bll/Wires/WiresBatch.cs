﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Wires
{
    public class WiresBatch : BaseDataObject
    {
        public const string SecuredObjectName = "WiresBatch";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Wires.Module.Current, SecuredObjectName); } }
        /*
		public class WiresBatchItem : BaseDataObject
		{
			private Dal.Netpay.tblLogMasavDetail _entity;

			public int ID { get { return _entity.logMasavDetails_id; } }
			public int BatchID { get { return _entity.logMasavFile_id; } }
			public int AccountID { get { return _entity.Account_id.GetValueOrDefault(); } }
			public int WireID { get { return _entity.WireMoney_id; } }
			public string PayeeName { get { return _entity.PayeeName; } }
			public string PayeeBankDetails { get { return _entity.PayeeBankDetails; } }
			public decimal Amount { get { return (decimal)_entity.Amount; } }
			public CommonTypes.Currency Currency { get { return (CommonTypes.Currency) _entity.Currency; } }
			public bool IsDone { get { return _entity.SendStatus != 0; } }
			public string Comment { get { return _entity.StatusNote; } }
			//public byte LogStatus { get { return _entity.LogStatus; } }
			public string UserName { get { return _entity.StatusUserName; } }

			private WiresBatchItem(Dal.Netpay.tblLogMasavDetail entity)
			{ 
				_entity = entity;
			}

			public static WiresBatchItem Load(int id)
			{
				ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
				return (from l in DataContext.Reader.tblLogMasavDetails where l.logMasavDetails_id == id select new WiresBatchItem(l)).SingleOrDefault();
			}

			public static List<WiresBatchItem> Search(int batchId)
			{
				ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
				return (from l in DataContext.Reader.tblLogMasavDetails where l.logMasavFile_id == batchId select new WiresBatchItem(l)).ToList();
			}

			private void Save()
			{
				DataContext.Writer.tblLogMasavDetails.Update(_entity, (_entity.logMasavDetails_id != 0));
				DataContext.Writer.SubmitChanges();
			}

			public void MarkAsDone()
			{
				_entity.SendStatus = 1;
				_entity.StatusUserName = Login.Current.UserName;
				Save();
				Wire.Load(WireID).SetSent();

				var notDoneItems = Search(BatchID).Where(i => !i.IsDone).Count();
				if (notDoneItems == 0) {
					var batch = WiresBatch.Load(BatchID);
					batch._entity.DoneFlag = 1;
					batch.Save();
				}
			}

			public void SetComment(string comment)
			{
				_entity.StatusUserName = Login.Current.UserName;
				_entity.StatusNote = comment;
				Save();
				WireLog.AddLog(WireID, "Batch Item Comment change: " + comment);

			}

		}
        */
        public class SearchFilters
        {
            public Range<int?> ID;
            public Range<DateTime?> Date;
            public Range<decimal?> Amount;
            public CommonTypes.Currency? Currency;
            public int? WireID;
        }

        private Dal.Netpay.WireBatch _entity;

        public int ID { get { return _entity.WireBatch_id; } }
        public System.DateTime Date { get { return _entity.InsertDate; } }
        public string ProviderID { get { return _entity.WireProvider_Id; } }
        public string FileName { get { return _entity.ConfirmationFileName; } }
        public string SourceBankAccount { get { return _entity.SourceAccountText; } }
        public string UserName { get { return _entity.InsertUserName; } }
        public CommonTypes.Currency Currency { get { return Bll.Currency.Get(_entity.BatchCurrencyISOCode).EnumValue; } }
        public decimal Amount { get { return (decimal)_entity.BatchAmount; } }
        public int Count { get { return _entity.WireCount; } }
        public bool Completed { get { return _entity.BatchStatus == 1; } }
        public WireBankAccount WireBankAccount
        {
            get
            {
                var name = @"System.FNB";
                var sourceAccountNumber = string.Empty;
                var temp = SourceBankAccount.Split(" ".ToCharArray());
                if (temp.Length > 1)
                    sourceAccountNumber = temp[1].Trim();
              return  WireBankAccount.Load(name, sourceAccountNumber);
            }
        }
        //public Dal.Netpay.WireAccount WireAccount
        //{
        //    get
        //    {
        //        try
        //        {
        //            return (from p in new Dal.Netpay.NetpayDataContextBase(Domain.Current.Sql1ConnectionString).WireAccounts
        //                    where p.AccountNumber == _entity.SourceAccountText
        //                    select p).SingleOrDefault();
        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.Log(LogSeverity.Error, LogTag.Wires, "Could not find source bank account: " + ex.Message);
        //            return null;
        //        }
        //    }
        //}
        //public byte Flag { get { return _entity.Flag; } }
        //public string AttachedFileExt { get { return _entity.AttachedFileExt; } }

        private WiresBatch(Dal.Netpay.WireBatch entity)
        {
            _entity = entity;
        }

        public static WiresBatch CreateBatch(string providerName, string sourceAccountNumber, CommonTypes.Currency currency, List<Wire> wires)
		{
            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
            //Logger.Log(LogTag.Wires, "Inside CreateBatch() method of FNB.");

            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            var fileEntity = new Dal.Netpay.WireBatch();
            using (var context = new DataContext(true))
            {
                fileEntity.InsertDate = DateTime.Now;
                fileEntity.WireProvider_Id = providerName; //Wire.Providers.Where(p => p.Name == providerName).Single().FriendlyName;
                if (!string.IsNullOrEmpty(sourceAccountNumber)) fileEntity.SourceAccountText = Wire.GetSourceAccountList(providerName)[sourceAccountNumber];
                else fileEntity.SourceAccountText = "";
                //fileEntity.LogonUser = Login.Current.UserName;
                fileEntity.BatchAmount = 0;
                fileEntity.WireCount = 0;
                fileEntity.BatchCurrencyISOCode = Bll.Currency.Get(currency).IsoCode;
                //fileEntity.AttachedFileExt = "";
                context.WireBatches.InsertOnSubmit(fileEntity);
                context.SubmitChanges();
                foreach (var wire in wires)
                {
                    wire.BatchID = fileEntity.WireBatch_id;
                    fileEntity.BatchAmount += wire.TransferAmount;
                    fileEntity.WireCount += 1;
                }
                context.SubmitChanges();
                context.WireBatches.Detach(fileEntity);
            }
            return new WiresBatch(fileEntity);
        }

        public List<Wire> GetWires() { return Wires.Wire.Search(new Wire.SearchFilters() { BatchID = ID }, null); }

        public void AttachFile(string description, string fileName, System.IO.Stream fileData)
        {
            var items = GetWires();
            foreach (var item in items)
                WireLog.AddFile(item.ID, description, fileName, fileData);
        }

        public void AddComment(string description)
        {
            var items = GetWires();
            foreach (var item in items)
                WireLog.AddLog(item.ID, description);
        }

        public void SetPrepared(string fileName, bool requireSend)
        {
            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
            //Logger.Log(LogTag.Wires, "Inside SetPrepared() method. FileName - " + fileName + " requiredSend=" + requireSend.ToString());

            if (!string.IsNullOrEmpty(fileName))
            {
                var strBase = Wire.MapOfflineFile(null);
                if (fileName.StartsWith(strBase)) fileName = fileName.Substring(strBase.Length).TrimStart(new char[] { '/', '\\' });
                _entity.ConfirmationFileName = fileName;
            }
            _entity.IsSendRequire = requireSend;
            Save();
        }

        public void SetSentComplete()
        {
            _entity.IsSendRequire = false;
            Save();
        }

        private void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            DataContext.Writer.WireBatches.Update(_entity, (_entity.WireBatch_id != 0));
            DataContext.Writer.SubmitChanges();
        }

        public static WiresBatch Load(int id)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from l in DataContext.Reader.WireBatches where l.WireBatch_id == id select new WiresBatch(l)).SingleOrDefault();
        }

        public static List<WiresBatch> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var expression = (from l in DataContext.Reader.WireBatches orderby l.InsertDate descending select l) as IQueryable<Dal.Netpay.WireBatch>;
            if (filters != null)
            {
                if (filters.ID.From != null) expression = expression.Where(p => p.WireBatch_id >= filters.ID.From);
                if (filters.ID.To != null) expression = expression.Where(p => p.WireBatch_id <= filters.ID.To);
                if (filters.Date.From != null) expression = expression.Where(p => p.InsertDate >= filters.Date.From);
                if (filters.Date.To != null) expression = expression.Where(p => p.InsertDate <= filters.Date.To.Value.AlignToEnd());
                if (filters.Amount.From != null) expression = expression.Where(p => p.BatchAmount >= filters.Amount.From);
                if (filters.Amount.To != null) expression = expression.Where(p => p.BatchAmount <= filters.Amount.To);
                if (filters.Currency != null) expression = expression.Where(p => p.BatchCurrencyISOCode == Bll.Currency.Get(filters.Currency.Value).IsoCode);
                if (filters.WireID != null) expression = expression.Where(p => DataContext.Reader.Wires.Where(v => v.Wire_id == filters.WireID).Select(v => v.WireBatch_id).Contains(p.WireBatch_id));
                //if (filters.Status != null && filters.Status.Count() > 0) expression = expression.Where(p => filters.Status.Contains((WireStatus)p.WireStatus));
            }
            return expression.ApplySortAndPage(sortAndPage).Select(p => new WiresBatch(p)).ToList();
        }

        public List<WiresBatch> GetWaitingSend(string providerId)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from l in DataContext.Reader.WireBatches where l.WireProvider_Id == providerId orderby l.InsertDate descending select new WiresBatch(l)).ToList();
        }

        private static System.Threading.Timer _timerSend;
        public static bool EnableRealTimeSend
        {

            //Used from service no need for admin permission check.
            get { return (_timerSend == null); }
            set
            {
                if (value)
                {
                    if (_timerSend == null) _timerSend = new System.Threading.Timer(RealTimeSendTimer, null, 0, 3 * 60 * 1000);
                }
                else if (_timerSend != null) _timerSend.Dispose();
            }
        }

        private static void RealTimeSendTimer(object state)
        {

            //Used only from service no need for admin permission check
            foreach (var d in Domain.Domains.Values)
            {
                Domain.Current = d;
                if (!Infrastructure.Tasks.Lock.TryLock("WIRE_Batch", DateTime.Now.AddMinutes(-3))) continue;
               var wbaList = new List<WireBankAccount>();
                try
                {
                    int successCount = 0;
                    ObjectContext.Current.Impersonate(d.ServiceCredentials);
                    var rows = (from l in DataContext.Reader.WireBatches where l.IsSendRequire orderby l.InsertDate descending select new WiresBatch(l)).ToList();
                    foreach (var batch in rows)
                    {
                        var provider = Provider.Get(batch.ProviderID);
                        try
                        {
                            provider.Send(batch);
                            batch.SetSentComplete();
                            successCount++;
                            wbaList.Add(batch.WireBankAccount);
                        }
                        catch (Exception ex)
                        {
                            Logger.Log(ex, string.Format("WireBatch #{0} send Failed, Domain: {1}", batch.ID, d.Host));
                        }
                    }
                    if (rows.Count > 0)
                    {
                        Logger.Log(LogTag.Wires, string.Format("WireBatch Send Completed for domain: {0}, success {1} of {2} wires", d.Host, successCount, rows.Count));
                        UpdateNextWireBatchNumber(wbaList);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(ex, "WireBatch Send for domain: " + d.Host);
                }
                finally
                {
                    Infrastructure.Tasks.Lock.Release("WIRE_Batch");
                    ObjectContext.Current.StopImpersonate();
                }
            }
        }

        private static void UpdateNextWireBatchNumber(List<WireBankAccount> wbaList)
        {
            foreach(var wba in wbaList)
            {
                if (wba.NextWireBatchNum.HasValue)
                    wba.NextWireBatchNum++;
                else
                    wba.NextWireBatchNum = 2;
                wba.Save();
            }
        }
    }
}
