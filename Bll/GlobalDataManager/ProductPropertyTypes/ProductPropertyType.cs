﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.DataManager
{
    public class ProductPropertyType : BaseDataObject
    {
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Bll.GlobalDataManager.ProductPropertyTypes.Module.Current, SecuredObjectName); } }

        private Dal.Netpay.ProductPropertyType _entity;

        public byte ProductPropertyTypeId { get { return _entity.ProductPropertyType_id; } }

        public string Name { get { return _entity.Name; } set { _entity.Name = value; } }

        private ProductPropertyType(Dal.Netpay.ProductPropertyType entity)
        {
            _entity = entity;
        }

        public ProductPropertyType()
        {
            _entity = new Dal.Netpay.ProductPropertyType();
        }

        public static List<ProductPropertyType> Cache
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return Domain.Current.GetCachData("ProductPropertyTypes", () =>
                {
                    return new Domain.CachData((from ppt in DataContext.Reader.ProductPropertyTypes select new ProductPropertyType(ppt)).ToList(), DateTime.Now.AddHours(5));
                }) as List<ProductPropertyType>;
            }
        }

        public static ProductPropertyType Get(int productpropertytypeid)
        {
            return Cache.Where(x => x.ProductPropertyTypeId == productpropertytypeid).SingleOrDefault();
        }

        public void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            DataContext.Writer.ProductPropertyTypes.Update(_entity, true);
            DataContext.Writer.SubmitChanges();
        }
    }
}
