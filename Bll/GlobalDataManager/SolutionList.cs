﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Netpay.Infrastructure;

namespace Netpay.Bll.DataManager
{
    public class SolutionList : BaseDataObject
    {
        private Dal.Netpay.SolutionList _entity;

        public byte SolutionListId { get { return _entity.SolutionList_id; } }

        public string Name { get { return _entity.Name; } set { _entity.Name = value; } }

        private SolutionList(Dal.Netpay.SolutionList entity)
        {
            _entity = entity;
        }

        public SolutionList()
        {
            _entity = new Dal.Netpay.SolutionList();
        }

        public static List<SolutionList> Cache
        {
            get
            {
                return Domain.Current.GetCachData("SolutionLists", () =>
                {
                    return new Domain.CachData((from sl in DataContext.Reader.SolutionLists select new SolutionList(sl)).ToList(), DateTime.Now.AddHours(5));
                }) as List<SolutionList>;
            }
        }

        public static SolutionList Get(int solutionlistid)
        {
            return Cache.Where(x => x.SolutionListId == solutionlistid).SingleOrDefault();
        }

        public void Save()
        {
            DataContext.Writer.SolutionLists.Update(_entity, true);
            DataContext.Writer.SubmitChanges();
        }
    }
}
