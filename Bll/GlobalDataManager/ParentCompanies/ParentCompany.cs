﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.DataManager
{
    public class ParentCompany : BaseDataObject
    {
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Bll.GlobalDataManager.ParentCompanies.Module.Current, SecuredObjectName); } }


        private Dal.Netpay.tblParentCompany _entity;

        public int ID { get { return _entity.ID; } }

        public string Code { get {return _entity.pc_Code; } set { _entity.pc_Code = value; } }

        public string ReccuringUrl { get { return _entity.pc_RecurringURL; } set { _entity.pc_RecurringURL = value; } }

        public bool IsDefault { get { return _entity.pc_IsDefault; } set { _entity.pc_IsDefault = value; } }
        
        public string ReccuringEcheckUrl { get {return _entity.pc_RecurringEcheckURL; } set { _entity.pc_RecurringEcheckURL = value; } }
        
        public string SmsUrl { get { return _entity.pc_SMS_URL; } set { _entity.pc_SMS_URL = value; } }

        public string TemplateName { get { return _entity.pc_TemplateName; } set { _entity.pc_TemplateName = value; } }
        
        private ParentCompany(Dal.Netpay.tblParentCompany entity)
        {
            _entity = entity;            
        }

        public ParentCompany()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            _entity = new Dal.Netpay.tblParentCompany();
        }

        public static List<ParentCompany> GetAllParentCompanies()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from pc in DataContext.Reader.tblParentCompanies
                    select new ParentCompany(pc)).ToList();
        }

        public static int GetDefaultParentCompanyIdValue()
        {
            //(SELECT TOP 1 ID FROM tblParentCompany ORDER BY pc_IsDefault DESC, pc_Code)
            var defaultID =  GetAllParentCompanies().OrderByDescending(x => x.IsDefault).ThenBy(x => x.Code).Select(x => x.ID).First().ToNullableInt();
            if (!defaultID.HasValue) return 0;
            else return defaultID.Value;
        }
        
        public static ParentCompany Get(int id)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            if (id <= 0) return null;

            return (from pc in DataContext.Reader.tblParentCompanies
                    where pc.ID == id
                    select new ParentCompany(pc))
                    .SingleOrDefault();
        } 


        public static bool IsNameExist(string name)
        {
            if (string.IsNullOrEmpty(name)) return false;

            return DataContext.Reader.tblParentCompanies.Any(x => x.pc_Code == name);
        }

        public static ParentCompany Get(string code)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            if (string.IsNullOrEmpty(code)) return null;

            return (from pc in DataContext.Reader.tblParentCompanies
                    where pc.pc_Code == code
                    select new ParentCompany(pc))
                    .SingleOrDefault();
        }

        public void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
            {
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);
                if (_entity.ID == 0)ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);
            }
            
                DataContext.Writer.tblParentCompanies.Update(_entity, (_entity.ID != 0));
            DataContext.Writer.SubmitChanges();
        }
    }
}
