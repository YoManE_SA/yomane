﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.DataManager
{
    public class ProductType : BaseDataObject
    {
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Bll.GlobalDataManager.ProductTypes.Module.Current, SecuredObjectName); } }

        private Dal.Netpay.ProductType _entity;

        public byte ProductTypeId { get { return _entity.ProductType_id; } }

        public string Name { get { return _entity.Name; } set { _entity.Name = value; } }

        private ProductType(Dal.Netpay.ProductType entity)
        {
            _entity = entity;
        }

        public ProductType()
        {
            _entity = new Dal.Netpay.ProductType();
        }
               

        public static List<ProductType> Cache
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return Domain.Current.GetCachData("ProductTypes", () =>
                {
                    return new Domain.CachData((from pt in DataContext.Reader.ProductTypes select new ProductType(pt)).ToList(), DateTime.Now.AddHours(5));
                }) as List<ProductType>;
            }
        }

        public static ProductType Get(int producttypeid)
        {
            return Cache.Where(x => x.ProductTypeId == producttypeid).SingleOrDefault();
        }

        public void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            DataContext.Writer.ProductTypes.Update(_entity, true);
            DataContext.Writer.SubmitChanges();
        }
    }
}
