﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.DataManager
{
    public class PeopleRelationType : BaseDataObject
    {
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Bll.GlobalDataManager.PeopleRelationTypes.Module.Current, SecuredObjectName); } }

        private Dal.Netpay.PeopleRelationType _entity;

        public byte PeopleRelationTypeId { get {return _entity.PeopleRelationType_id; } }

        public string Name { get { return _entity.Name; } set { _entity.Name = value; } }

        public string Description { get { return _entity.Description; } set { _entity.Description = value; } }

        private PeopleRelationType(Dal.Netpay.PeopleRelationType entity)
        {
            _entity = entity;
        }

        public PeopleRelationType()
        {
            _entity = new Dal.Netpay.PeopleRelationType();
        }

        public static List<PeopleRelationType> Cache
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return Domain.Current.GetCachData("PeopleRelationTypes", () =>
                {
                    return new Domain.CachData((from prt in DataContext.Reader.PeopleRelationTypes select new PeopleRelationType(prt)).ToList(), DateTime.Now.AddHours(5));
                }) as List<PeopleRelationType>;
            }
        }

        public static PeopleRelationType Get(int peoplerelationtypeid)
        {
            return Cache.Where(x => x.PeopleRelationTypeId == peoplerelationtypeid).SingleOrDefault();
        }

        public void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            DataContext.Writer.PeopleRelationTypes.Update(_entity, true);
            DataContext.Writer.SubmitChanges();
        }
    }
}
