﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.GlobalDataManager.FileItemTypes
{
    public class FileItemType : BaseDataObject
    {
        public const string SecuredObjectName = "Manage";

        public static SecuredObject SecuredObject { get { return SecuredObject.Get(Module.Current, SecuredObjectName); } }

        private Dal.Netpay.FileItemType _entity;

        public int ID { get { return _entity.FileItemType_id; } set { _entity.FileItemType_id = (byte)value; } }

        public string Name { get { return _entity.Name; } set { _entity.Name = value; } }

        private FileItemType(Dal.Netpay.FileItemType entity)
        {
            _entity = entity;
        }

        public FileItemType()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            _entity = new Dal.Netpay.FileItemType();
        }

        public static List<FileItemType> Cache
        {
            
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return Domain.Current.GetCachData("FilesManager.FileTypes", () =>
                {
                    return new Domain.CachData((from ft in DataContext.Reader.FileItemTypes select new FileItemType(ft)).ToList(), DateTime.Now.AddDays(3));
                }) as List<FileItemType>;
            }
        }

        public static FileItemType Get(int id)
        {
            if (id <= 0) return null;

            return Cache.Where(x => x.ID == id).SingleOrDefault();
        }

        public static byte GetNewItemId
        {
            get
            {
                return (byte)(DataContext.Reader.FileItemTypes.Max(x => x.FileItemType_id) + 1);
            }
        }

        public void Save(bool isExist)
        {
            if (!isExist)
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

                DataContext.Writer.FileItemTypes.InsertOnSubmit(_entity);
                DataContext.Writer.SubmitChanges();
            }
            else
            {
                DataContext.Writer.FileItemTypes.Update(_entity, isExist);
                DataContext.Writer.SubmitChanges();
            }
        }

        public void Delete()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            if (_entity.FileItemType_id == 0) return;

            DataContext.Writer.FileItemTypes.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }
    }
}
