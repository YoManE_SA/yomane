﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Faqs
{
	public class Group : BaseDataObject
	{
		public class SearchFilters
		{
			public Infrastructure.Range<int?> ID;
			public CommonTypes.Language? LanguageId;
			public bool? IsVisible;
			public bool? IsMerchantCP, IsDevCenter, IsWallet, IsWebsite;
			public string Theme;
			public bool AllForTheme;
		}

		private Dal.Netpay.QNAGroup _entity;

		public int ID { get { return _entity.QNAGroup_id; }  set { _entity.QNAGroup_id = value; } }
		public byte LanguageId { get { return _entity.Language_id; } set { _entity.Language_id = value; } }
		public bool IsVisible { get { return _entity.IsVisible; } set { _entity.IsVisible = value; } }
		public string Name { get { return _entity.name; } set { _entity.name = value; } }
		public bool IsMerchantCP { get { return _entity.IsMerchantCP; } set { _entity.IsMerchantCP = value; } }
		public bool IsDevCenter { get { return _entity.IsDevCenter; } set { _entity.IsDevCenter = value; } }
		public bool IsWallet { get { return _entity.IsWallet; } set { _entity.IsWallet = value; } }
		public bool IsWebsite { get { return _entity.IsWebsite; } set { _entity.IsWebsite = value; } }
		public string TemplateRestriction { get { return _entity.TemplateRestriction; } set { _entity.TemplateRestriction = value; } }

        public int? ParentCompanyId { get { return _entity.ParentCompny_id; } set { _entity.ParentCompny_id = value; } }


		private Group(Dal.Netpay.QNAGroup entity) { _entity = entity; }
		public Group() { _entity = new Dal.Netpay.QNAGroup(); }

		public static Group Load(int id)
		{
			return Search(new SearchFilters() { ID = new Range<int?>(id) }, null).SingleOrDefault();
		}

		public void Save(bool isnew)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
            {
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Faq.SecuredObject, PermissionValue.Edit);
                if (isnew) ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Faq.SecuredObject, PermissionValue.Add);
            }

            DataContext.Writer.QNAGroups.Update(_entity, !isnew);
			DataContext.Writer.SubmitChanges();
		}

		public void Delete()
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Faq.SecuredObject, PermissionValue.Delete);

            DataContext.Writer.QNAGroups.Delete(_entity);
			DataContext.Writer.SubmitChanges();
		}

        public static int NewID
        {
            get
            {
                bool isAnyItem = DataContext.Reader.QNAGroups.Any();
                if (!isAnyItem)
                {
                    return 1;
                }
                else
                {
                    return DataContext.Reader.QNAGroups.Max(x => x.QNAGroup_id) + 1;
                }
            }
        }

        public List<Faq> CurrentGroupQna
        {
            get
            {
                return Faq.Search(new Faq.SearchFilters() { GroupId = ID }, null);
            }
        }

		public static List<Group> Search(SearchFilters filters, ISortAndPage sortAndPage)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Faq.SecuredObject, PermissionValue.Read);

            var exp = from q in DataContext.Reader.QNAGroups select q;
			if (filters != null)
			{
				if (filters.ID.From.HasValue) exp = exp.Where((x) => x.QNAGroup_id >= filters.ID.From.Value);
				if (filters.ID.To.HasValue) exp = exp.Where((x) => x.QNAGroup_id <= filters.ID.To.Value);
				if (filters.LanguageId.HasValue) exp = exp.Where((x) => x.Language_id == (byte)filters.LanguageId.Value);

				if (filters.IsVisible.HasValue) exp = exp.Where((x) => x.IsVisible == filters.IsVisible.Value);
				if (filters.IsMerchantCP.HasValue) exp = exp.Where((x) => x.IsMerchantCP == filters.IsMerchantCP.Value);
				if (filters.IsDevCenter.HasValue) exp = exp.Where((x) => x.IsDevCenter == filters.IsDevCenter.Value);
				if (filters.IsWallet.HasValue) exp = exp.Where((x) => x.IsWallet == filters.IsWallet.Value);
				if (filters.IsWebsite.HasValue) exp = exp.Where((x) => x.IsWebsite == filters.IsWebsite.Value);

				if (filters.Theme != null) {
					if (filters.AllForTheme) exp = exp.Where((x) => string.IsNullOrEmpty(x.TemplateRestriction) || x.TemplateRestriction == filters.Theme);
					else exp = exp.Where((x) => x.TemplateRestriction == filters.Theme);
				}
			}
			return exp.ApplySortAndPage(sortAndPage).Select(q => new Group(q)).ToList();
		}
	}
}
