﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Faqs
{
	public class Faq : BaseDataObject
	{
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Bll.Faqs.Module.Current, SecuredObjectName); } }

        public class SearchFilters
		{
			public Infrastructure.Range<int?> ID;
			public int? GroupId;
		}

		private Dal.Netpay.QNA _entity;

		public int ID { get { return _entity.QNA_id; } }
		public int? GroupId { get { return _entity.QNAGroup_id; } set { _entity.QNAGroup_id = value; } }
		public string Question { get { return _entity.Question; } set { _entity.Question = value; } }
		public string Answer { get { return _entity.Answer; } set { _entity.Answer = value; } }
		public int Rating { get { return _entity.Rating; } set { _entity.Rating = value; } }
		public bool IsVisible { get { return _entity.IsVisible; } set { _entity.IsVisible = value; } }

		private Faq(Dal.Netpay.QNA entity) { _entity = entity; }
		public Faq() { _entity = new Dal.Netpay.QNA(); }

		public static List<Faq> Search(SearchFilters filters, ISortAndPage sortAndPage)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var exp = from q in DataContext.Reader.QNAs select q;
			if (filters != null)
			{
				if (filters.ID.From.HasValue) exp = exp.Where((x) => x.QNA_id >= filters.ID.From.Value);
				if (filters.ID.To.HasValue) exp = exp.Where((x) => x.QNA_id <= filters.ID.To.Value);
				if (filters.GroupId.HasValue) exp = exp.Where((x) => x.QNAGroup_id == filters.GroupId.Value);
			}
			return exp.ApplySortAndPage(sortAndPage).Select(q => new Faq(q)).ToList();
		}

		public static Faq Load(int id)
		{
			return Search(new SearchFilters() { ID = new Range<int?>(id) }, null).SingleOrDefault();
		}

		public void Save()
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
            {
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);
                if (_entity.QNA_id == 0) ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);
            }

            DataContext.Writer.QNAs.Update(_entity, _entity.QNA_id != 0);
			DataContext.Writer.SubmitChanges();
		}

		public void Delete()
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            DataContext.Writer.QNAs.Delete(_entity);
			DataContext.Writer.SubmitChanges();
		}
	}
}
