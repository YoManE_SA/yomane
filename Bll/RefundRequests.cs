﻿using System;
using System.Collections.Generic;
using System.Linq;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure.VO;

namespace Netpay.Bll
{
	public static class RefundRequests
	{
		public const int RefundRequestStatusAllCompleted = 100;

		public static List<RefundRequestVO> GetRequests(Guid credentialsToken, int transactionID)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			List<RefundRequestVO> results = (from rr in dc.tblRefundAsks where rr.companyID == user.ID && rr.transID == transactionID select new RefundRequestVO(user.Domain.Host, rr)).ToList<RefundRequestVO>();

			return results;
		}

		public static bool CancelRequest(Guid credentialsToken, int ID)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			tblRefundAsk rr = (from tbl in dc.tblRefundAsks where tbl.companyID == user.ID && tbl.id == ID select tbl).SingleOrDefault();
			if (rr == null) return false;
			if (rr.RefundAskStatus != (int)RefundRequestStatus.Pending) return false;
			rr.RefundAskStatus = (int)RefundRequestStatus.CanceledByMerchant;
			dc.SubmitChanges();
			return true;
		}

        public static RefundRequestVO GetRequestByOriginalTransId(string domainHost, int transId)
        {
            NetpayDataContext dc = new NetpayDataContext(Infrastructure.Domains.DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
            return (from tbl in dc.tblRefundAsks where tbl.transID == transId select new RefundRequestVO(domainHost, tbl)).SingleOrDefault();
        }

        public static List<RefundRequestVO> GetRequests(Guid credentialsToken, SearchFilters filters, PagingInfo pagingInfo, SortingInfo sortingInfo)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });
			if (user.Type != UserType.NetpayAdmin)
				filters.merchantIDs = SecurityManager.FilterRequestedIDs(filters.merchantIDs, user.AllowedMerchantsIDs);

			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var expression = from r in dc.tblRefundAsks join t in dc.tblCompanyTransPasses on r.transID equals t.ID orderby r.RefundAskDate descending select new { Request = r, Transaction = t };

			if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
				expression = expression.Where(t => filters.merchantIDs.Contains(t.Request.companyID));
			if (filters.isChargeback == null)
			{
				if (filters.dateFrom != null) expression = expression.Where(t => t.Request.RefundAskDate >= filters.dateFrom.Value.MinTime());
				if (filters.dateTo != null) expression = expression.Where(t => t.Request.RefundAskDate <= filters.dateTo.Value.MaxTime());
			}
			if (filters.transactionID != null)
				expression = expression.Where(t => t.Request.transID == filters.transactionID.Value);
			if (filters.refundAskID != null)
				expression = expression.Where(t => t.Request.id == filters.refundAskID.Value);
			if (filters.paymentMethodID != null)
				expression = expression.Where(t => t.Transaction.PaymentMethod == filters.paymentMethodID.Value);
			if (filters.creditType != null)
				expression = expression.Where(t => t.Transaction.CreditType == filters.creditType.Value);
			if (filters.orderID != null)
				expression = expression.Where(t => t.Transaction.OrderNumber == filters.orderID);
			if (filters.currencyID != null)
				expression = expression.Where(t => t.Request.RefundAskCurrency == filters.currencyID);
			if (filters.amountFrom != null)
				expression = expression.Where(t => t.Request.RefundAskAmount >= filters.amountFrom);
			if (filters.amountTo != null)
				expression = expression.Where(t => t.Request.RefundAskAmount <= filters.amountTo);
			if (filters.replyCode != null)
				expression = expression.Where(t => t.Transaction.replyCode == filters.replyCode);
			if (filters.refundRequestStatuses != null)
				if (filters.refundRequestStatuses.Count > 0)
					expression = expression.Where(t => filters.refundRequestStatuses.Contains((RefundRequestStatus)t.Request.RefundAskStatus));
			if (filters.refundAskStatus != null)
			{
				if (filters.refundAskStatus == RefundRequestStatusAllCompleted)
					expression = expression.Where(t => (t.Request.RefundAskStatus == (int)RefundRequestStatus.Processing) || (t.Request.RefundAskStatus == (int)RefundRequestStatus.ProcessedAccepted) || (t.Request.RefundAskStatus == (int)RefundRequestStatus.CreatedAccepted));
				else expression = expression.Where(t => t.Request.RefundAskStatus == filters.refundAskStatus);
			}

			// handle paging if not null
			if (pagingInfo != null)
			{
				pagingInfo.TotalItems = expression.Count();
				pagingInfo.DataFunction = (credToken, pi) => GetRequests(credToken, filters, pi, sortingInfo);
				if (pagingInfo.CountOnly) return null;
				expression = expression.Skip(pagingInfo.Skip).Take(pagingInfo.Take);
			}

			// run query and return collection
			List<RefundRequestVO> requests = expression.Select(t => new RefundRequestVO(user.Domain.Host, t.Request, t.Transaction)).ToList<RefundRequestVO>();
			return requests;
		}

		public static RequestRefundResult RequestsRefund(Guid credentialsToken, int transactionID, TransactionStatus transactionStatus, decimal amount, string comment)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

			var merchant = Merchant.Merchant.Load(credentialsToken);
			if (!merchant.ProcessSettings.IsAskRefund)
				return RequestRefundResult.NoPermission;

			var transaction = Transaction.Transaction.GetTransaction(credentialsToken, null, transactionID, transactionStatus);
			if (transaction == null)
				return RequestRefundResult.TransactionNotFound;
			if (transaction.InsertDate < DateTime.Now.AddMonths(-12)) return RequestRefundResult.TransactionTooOld;


			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			List<RefundRequestVO> refunds = (from rr in dc.tblRefundAsks where rr.companyID == user.ID && rr.transID == transactionID select new RefundRequestVO(user.Domain.Host, rr)).ToList<RefundRequestVO>();
			decimal totalRefunded = refunds.Select(rr => (rr.Status == RefundRequestStatus.Canceled || rr.Status == RefundRequestStatus.CanceledByMerchant ? 0 : rr.Amount)).Sum();
			decimal availableForRefund = transaction.Amount - totalRefunded;
			if (availableForRefund <= 0)
				return RequestRefundResult.TransactionFullyRefunded;
			if (availableForRefund < amount)
				return RequestRefundResult.AmountTooLarge;

			tblRefundAsk refund = new tblRefundAsk();
			refund.companyID = user.ID;
			refund.transID = transaction.ID;
			refund.RefundAskStatus = (int)RefundRequestStatus.Pending;
			refund.RefundAskAmount = amount;
			refund.RefundAskComment = comment;
			refund.RefundAskCurrency = transaction.CurrencyID;
			refund.RefundAskDate = DateTime.Now;
			refund.RefundAskConfirmationNum = "";
			refund.RefundAskStatusHistory = "";

			dc.tblRefundAsks.InsertOnSubmit(refund);
			dc.SubmitChanges();

			return RequestRefundResult.Success;
		}

		/*
		public static decimal GetAvailableAmount(Guid credentialsToken, int transactionID)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited });

			NetpayDataContext dc = new NetpayDataContext();
			decimal result = (from rr in dc.tblRefundAsks where rr.companyID == user.ID && rr.transID == transactionID select rr.RefundAskAmount).Sum();

			return result;
		}
		*/

		public static TransactionRefundAbility GetRefundAbility(Guid credentialsToken, Transaction.Transaction trans)
		{
			if (trans == null) return TransactionRefundAbility.None; // no transaction
			if (trans.Status != TransactionStatus.Captured) return TransactionRefundAbility.None; // not pass
			if (trans.CreditType == 0) return TransactionRefundAbility.None; // not debit
			if (trans.DebitCompanyID == null) return TransactionRefundAbility.None; // not bank transaction
			if ((trans.IsChargeback != null) && (bool)trans.IsChargeback) return TransactionRefundAbility.None; // charged back

			int debitCompanyID = (int)trans.DebitCompanyID;
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.MerchantLimited, UserType.NetpayUser, UserType.NetpayAdmin });
			List<TransactionHistoryChargebackVO> retrievals = Transaction.Transaction.GetHistoryChargebacks(credentialsToken, trans.ID);
			foreach (TransactionHistoryChargebackVO retrieval in retrievals)
			{
				if ((retrieval.IsPendingChargeback) || (retrieval.TypeID == CommonTypes.TransactionHistoryType.Chargeback)) return TransactionRefundAbility.None;
			}
			DebitCompanyVO bank = user.Domain.Cache.GetDebitCompany(debitCompanyID);
			if (bank == null) return TransactionRefundAbility.None; // bank not found

			if (!bank.IsRefundAllowed) return TransactionRefundAbility.None; // refund not allowed

			return bank.IsRefundAllowedPartial ? TransactionRefundAbility.FullOrPartial : TransactionRefundAbility.FullOnly;
		}
	}
}