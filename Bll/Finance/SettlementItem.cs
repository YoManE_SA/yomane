﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll.Finance
{
	public class SettlementItem : BaseDataObject
	{
		private Dal.Netpay.SettlementItem _entity;

		public int ID { get { return _entity.SettlementItem_id; } set { _entity.SettlementItem_id = value; } }
		public int SettlementID { get { return _entity.Settlement_id; } set { _entity.Settlement_id = value; } }
		public AmountType AmountType { get { return (AmountType)_entity.AmountType_id.GetValueOrDefault(); } set { _entity.AmountType_id = (byte)value; } }
		public string LineText { get { return _entity.LineText; } set { _entity.LineText = value; } }
		public decimal? Amount { get { return _entity.Amount; } set { _entity.Amount = value; } }
		public int? Quantity { get { return _entity.Quantity; } set { _entity.Quantity = value; } }
		public decimal? Total { get { return _entity.Total; } set { _entity.Total = value; } }
		public decimal? PercentValue { get { return _entity.PercentValue; } set { _entity.PercentValue = value; } }
		public byte? SortOrder { get { return _entity.SortOrder; } set { _entity.SortOrder = value; } }
		public bool IsFee { get { return _entity.IsFee; } set { _entity.IsFee = value; } }

		private SettlementItem(Dal.Netpay.SettlementItem entity)  { _entity = entity; }
		public SettlementItem(int settlementId, AmountType amountType)
			
		{
			_entity = new Dal.Netpay.SettlementItem();
			_entity.Settlement_id = settlementId;
			_entity.AmountType_id = (byte)amountType;
		}
	}
}
