﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll.Finance
{
	public class AccountSettings : BaseDataObject
	{
		private Dal.Netpay.SetSettlement _entity;

		public int ID { get { return _entity.SetSettlement_id; } }
		public SettlementType SettlementType { get { return (SettlementType) _entity.SettlementType_id; } }
		public int AccountID { get { return _entity.Account_id.GetValueOrDefault(); } }
		public string CurrencyISOCode { get { return _entity.CurrencyISOCode; } set { _entity.CurrencyISOCode = value; } }

		public bool IsShowToSettle { get { return _entity.IsShowToSettle; } set { _entity.IsShowToSettle = value; } }
		public decimal? MinPayoutAmount { get { return _entity.MinPayoutAmount; } set { _entity.MinPayoutAmount = value; } }
		public decimal? MinSettlementAmount { get { return _entity.MinSettlementAmount; } set { _entity.MinSettlementAmount = value; } }
		public byte? SettleInitialHoldDays { get { return _entity.SettleInitialHoldDays; } set { _entity.SettleInitialHoldDays = value; } }
		public byte? SettleHoldDays { get { return _entity.SettleHoldDays; } set { _entity.SettleHoldDays = value; } }

		public byte? InvoiceIssuer_id { get { return _entity.InvoiceIssuer_id; } set { _entity.InvoiceIssuer_id = value; } }
		public decimal? WireFeePercent { get { return _entity.WireFeePercent; } set { _entity.WireFeePercent = value; } }
		public decimal? WireFeeAmount { get { return _entity.WireFeeAmount; } set { _entity.WireFeeAmount = value; } }
		public string WireCurrencyIsoCode { get { return _entity.WireCurrencyIsoCode; } set { _entity.WireCurrencyIsoCode = value; } }
		public decimal? StorageFeeAmount { get { return _entity.StorageFeeAmount; } set { _entity.StorageFeeAmount = value; } }
		public string StorageFeeCurrencyIsoCode { get { return _entity.StorageFeeCurrencyIsoCode; } set { _entity.StorageFeeCurrencyIsoCode = value; } }


		public List<int> Accounts { get; set; }
		public List<int> PaymentMethods { get; set; }
		public List<string> Currencies { get; set; }
		public List<string> Countries { get; set; }

		private AccountSettings(Dal.Netpay.SetSettlement entity)  { _entity = entity; }
		public AccountSettings(int accountId, SettlementType settlementType)
		{ 
			_entity = new Dal.Netpay.SetSettlement();
			_entity.Account_id = accountId;
			_entity.SettlementType_id = (byte)settlementType; 
		}

		public static List<AccountSettings> Load(List<int> ids)
		{
			return (from s in DataContext.Reader.SetSettlements where ids.Contains(s.SetSettlement_id) select new AccountSettings(s)).ToList();
		}

	}
}
