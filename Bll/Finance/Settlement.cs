﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Finance
{
	public class Settlement : BaseDataObject
	{
		public const string SecuredObjectName = "Manage";
		public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Affiliates.Module.Current, SecuredObjectName); } }

		public class SearchFilters
		{
			public Infrastructure.Range<int?> ID;
			public Infrastructure.Range<DateTime?> InsertDate;
			public Infrastructure.Range<DateTime?> SettlementDate;
			public Infrastructure.Range<decimal?> InvoiceTotal;
			public Infrastructure.Range<decimal?> WireTotal;
			public List<int> AccountID;
		}
		
		private Dal.Netpay.Settlement _entity;

		public int ID { get { return _entity.Settlement_id; } set { _entity.Settlement_id = value; } }
		public int? TransactionPay_id { get { return _entity.TransactionPay_id; } set { _entity.TransactionPay_id = value; } }
		public byte SettlementType_id { get { return _entity.SettlementType_id; } set { _entity.Settlement_id = value; } }
		public int? Account_id { get { return _entity.Account_id; } set { _entity.Account_id = value; } }

		public bool IsVisible { get { return _entity.IsVisible; } set { _entity.IsVisible = value; } }
		public System.DateTime InsertDate { get { return _entity.InsertDate; } set { _entity.InsertDate = value; } }
		public System.DateTime SettlementDate { get { return _entity.SettlementDate; } set { _entity.SettlementDate = value; } }
		public decimal? SettlementTotal { get { return _entity.SettlementTotal; } set { _entity.SettlementTotal = value; } }
		public string SettlementCurrencyISOCode { get { return _entity.SettlementCurrencyISOCode; } set { _entity.SettlementCurrencyISOCode = value; } }
		public string PayeeName { get { return _entity.PayeeName; } set { _entity.PayeeName = value; } }
		public string CommentText { get { return _entity.CommentText; } set { _entity.CommentText = value; } }

		public byte? InvoiceIssuer_id { get { return _entity.InvoiceIssuer_id; } set { _entity.InvoiceIssuer_id = value; } }
		public byte? InvoiceProvider_id { get { return _entity.InvoiceProvider_id; } set { _entity.InvoiceProvider_id = value; } }
		public string InvoiceURL { get { return _entity.InvoiceURL; } set { _entity.InvoiceURL = value; } }
		public int? InvoiceNumber { get { return _entity.InvoiceNumber; } set { _entity.InvoiceNumber = value; } }
		public decimal? InvoiceTotal { get { return _entity.InvoiceTotal; } set { _entity.InvoiceTotal = value; } }
		public string InvoiceCurrencyISOCode { get { return _entity.InvoiceCurrencyISOCode; } set { _entity.InvoiceCurrencyISOCode = value; } }
		public decimal? InvoiceVATPercent { get { return _entity.InvoiceVATPercent; } set { _entity.InvoiceVATPercent = value; } }
		public decimal? InvoiceCurrencyExchangeRate { get { return _entity.InvoiceCurrencyExchangeRate; } set { _entity.InvoiceCurrencyExchangeRate = value; } }

		public decimal? WireTotal { get { return _entity.WireTotal; } set { _entity.WireTotal = value; } }
		public string WireCurrencyISOCode { get { return _entity.WireCurrencyISOCode; } set { _entity.WireCurrencyISOCode = value; } }
		public decimal? WireCurrencyExchangeRate { get { return _entity.WireCurrencyExchangeRate; } set { _entity.WireCurrencyExchangeRate = value; } }
		public decimal? WireFeeFixedAmount { get { return _entity.WireFeeFixedAmount; } set { _entity.WireFeeFixedAmount = value; } }
		public decimal? WireFeePercentValue { get { return _entity.WireFeePercentValue; } set { _entity.WireFeePercentValue = value; } }
		public decimal? WireFee { 
			get { 
				var wireValue = _entity.SettlementTotal.GetValueOrDefault() * _entity.WireCurrencyExchangeRate;
				return (_entity.WireFeeFixedAmount + (System.Math.Abs(wireValue.GetValueOrDefault()) * (_entity.WireFeePercentValue / 100)));
			} 
		}

		public List<SettlementItem> Items { get; set; }

		private Settlement(Dal.Netpay.Settlement entity)  { _entity = entity; }
		public Settlement(int settlementSettingId, string currencyIso)
			
		{
			Items = new List<SettlementItem>();
			var setSettlement = (from a in DataContext.Reader.SetSettlements where a.SetSettlement_id == settlementSettingId select a).SingleOrDefault();
			_entity = new Dal.Netpay.Settlement();
			_entity.InsertDate = DateTime.Now;
			_entity.SettlementCurrencyISOCode = currencyIso;
			_entity.SettlementDate = DateTime.Now; //options.SettlementDate;
			if (setSettlement == null) return;
			_entity.SettlementType_id = setSettlement.SettlementType_id;
			_entity.Account_id = setSettlement.Account_id;
			_entity.InvoiceIssuer_id = setSettlement.InvoiceIssuer_id;
			_entity.WireFeeFixedAmount = setSettlement.WireFeeAmount;
			_entity.WireFeePercentValue = setSettlement.WireFeePercent;
			_entity.WireCurrencyISOCode = setSettlement.WireCurrencyIsoCode ?? currencyIso;
			_entity.IsVisible = true;
			var account = Accounts.Account.Load(setSettlement.Account_id.GetValueOrDefault());

			switch ((SettlementType)_entity.SettlementType_id)
			{
			case SettlementType.DebitCompany:
				_entity.PayeeName = (from d in DataContext.Reader.tblDebitCompanies where d.DebitCompany_ID == account.DebitCompanyID select d.dc_name).SingleOrDefault(); break;
			case SettlementType.Merchant:
				_entity.PayeeName = (from d in DataContext.Reader.tblCompanies where d.ID == account.MerchantID select d.CompanyName).SingleOrDefault(); break;
			case SettlementType.DebitCompanyAffiliate:
			case SettlementType.MerchantAffiliate:
				_entity.PayeeName = (from d in DataContext.Reader.tblAffiliates where d.affiliates_id == account.AffiliateID select d.name).SingleOrDefault(); break;
			}
		}

		public class AddTransactionsOptions
		{
			public string CurrencyIso { get; set; }
			public DateTime? amountDateFrom;
			public DateTime? amountDateTo;

			public System.DateTime SettlementDate { get; set; }
			public bool includeTest;
			public bool includeStorage;
			public bool? includeHolded;
		}

		public void AddTransactions(AddTransactionsOptions options)
		{
			var setSettlement = (from a in DataContext.Reader.SetSettlements where a.SetSettlement_id == _entity.SetSettlement_id select a).SingleOrDefault();
			var account = Accounts.Account.Load(setSettlement.Account_id.GetValueOrDefault());
			string updateStatment = "Update Trans.TransactionAmount Set Settlement_id = {0} Where Settlement_id Is Null And SetSettlement_id = {1} And CurrencyISOCode = {2} And SettlementDate <= {3}";
			if (options.amountDateFrom != null) updateStatment += " And InsertDate >= {4}";
			if (options.amountDateTo != null) updateStatment += " And InsertDate <= {5}";
			DataContext.Writer.ExecuteCommand(updateStatment, _entity.Settlement_id, setSettlement.SetSettlement_id, options.CurrencyIso, options.SettlementDate, options.amountDateFrom.GetValueOrDefault(DateTime.Now), options.amountDateTo.GetValueOrDefault(DateTime.Now));
			var totalList = 
				(from a in DataContext.Reader.TransactionAmounts
					where a.Settlement_id == _entity.Settlement_id
					group a by new { a.AmountType_id, a.IsFee, a.PercentValue, a.FixedAmount } into g
					select new { Key = g.Key, Count = g.Count(), TotalFixed = g.Sum(i => i.FixedAmount), TotalPercent = g.Sum(i => i.PercentAmount) }).ToList();
			//var items = new List<Netpay.Dal.Netpay.SettlementItem>();
			var groups = totalList.GroupBy(t => new { t.Key.AmountType_id, t.Key.IsFee });
			foreach (var group in groups)
			{
				foreach (var subGroup in group.GroupBy(t => t.Key.FixedAmount))
				{
					if (subGroup.Key == 0) continue;
					var rowItem = new SettlementItem(_entity.Settlement_id, (AmountType) group.Key.AmountType_id);
					rowItem.IsFee = group.Key.IsFee;
					rowItem.LineText = "Fixed";
					if (!rowItem.IsFee) rowItem.LineText += " Transfer";
					rowItem.Amount = subGroup.Key;
					rowItem.Quantity = subGroup.Sum(s => s.Count);
					rowItem.Total = subGroup.Sum(s => s.TotalFixed);
					Items.Add(rowItem);
				}

				foreach (var subGroup in group.GroupBy(t => t.Key.PercentValue))
				{
					if (subGroup.Key == 0) continue;
					var rowItem = new SettlementItem(_entity.Settlement_id, (AmountType) group.Key.AmountType_id);
					rowItem.IsFee = group.Key.IsFee;
					rowItem.LineText = "Relative";
					if (!rowItem.IsFee) rowItem.LineText = " Transfer";
					rowItem.PercentValue = subGroup.Key;
					rowItem.Quantity = subGroup.Sum(s => s.Count);
					rowItem.Total = subGroup.Sum(s => s.TotalPercent);
					Items.Add(rowItem);
				}
			}

			if (setSettlement.StorageFeeAmount != null && (setSettlement.StorageFeeCurrencyIsoCode == null || setSettlement.StorageFeeCurrencyIsoCode == _entity.SettlementCurrencyISOCode))
			{
				if (_entity.SettlementType_id == (byte)SettlementType.Merchant)
				{
					var storageCount = (from s in DataContext.Reader.tblCCStorages where s.companyID == account.MerchantID && s.payID == 0 select s.ID).Count();
					if (storageCount > 0){
						var rowItem = new SettlementItem(_entity.Settlement_id, AmountType.CCStorage);
						rowItem.IsFee = true;
						rowItem.Quantity = storageCount;
						rowItem.Amount = setSettlement.StorageFeeAmount;
						rowItem.Total = -(storageCount * setSettlement.StorageFeeAmount.Value);
						Items.Add(rowItem);
					}
				}
			}
		}

		private void Save()
		{
			//if (items == null) items = (from a in dc.SettlementItems where a.Settlement_id == settlementId select a).ToList();
			_entity.SettlementTotal = Items.Sum(i => i.Total).GetValueOrDefault();
			_entity.InvoiceTotal = -Items.Where(i => i.IsFee).Sum(i => i.Total).GetValueOrDefault();
			var invoiceIssuer = (from ii in DataContext.Reader.InvoiceIssuers where ii.InvoiceIssuer_id == _entity.InvoiceIssuer_id select ii).SingleOrDefault();
			if (invoiceIssuer != null)
			{
				_entity.InvoiceProvider_id = invoiceIssuer.InvoiceProvider_id;
				_entity.InvoiceVATPercent = invoiceIssuer.VATPercent;
				_entity.InvoiceCurrencyISOCode = invoiceIssuer.InvoiceCurrencyISOCode;
				if (_entity.InvoiceCurrencyISOCode != _entity.SettlementCurrencyISOCode)
					_entity.InvoiceCurrencyExchangeRate = Money.ConvertRate(Currency.Get(_entity.SettlementCurrencyISOCode).EnumValue, Currency.Get(_entity.InvoiceCurrencyISOCode).EnumValue);
				else _entity.InvoiceCurrencyExchangeRate = 1;
				_entity.InvoiceTotal = _entity.InvoiceTotal.GetValueOrDefault() * _entity.InvoiceCurrencyExchangeRate;
				var vatAmount = (_entity.InvoiceTotal / 100) * _entity.InvoiceVATPercent;
				_entity.InvoiceTotal += vatAmount;
			}
			_entity.WireTotal = _entity.SettlementTotal;
			if (_entity.WireCurrencyISOCode != _entity.SettlementCurrencyISOCode)
				_entity.WireCurrencyExchangeRate = Money.ConvertRate(Currency.Get(_entity.SettlementCurrencyISOCode).EnumValue, Currency.Get(_entity.WireCurrencyISOCode).EnumValue);
			else _entity.WireCurrencyExchangeRate = 1;
			_entity.WireTotal = _entity.SettlementTotal.GetValueOrDefault() * _entity.WireCurrencyExchangeRate;

			_entity.WireTotal -= (_entity.WireFeeFixedAmount + (System.Math.Abs(_entity.WireTotal.GetValueOrDefault()) * (_entity.WireFeePercentValue / 100)));
			
			DataContext.Writer.Settlements.Update(_entity, (_entity.SetSettlement_id != 0));
			DataContext.Writer.SubmitChanges();
		}

		public static Settlement Load(int id)
		{
			return Search(new SearchFilters() { ID = new Infrastructure.Range<int?>(id) }, null).SingleOrDefault();
		}

		public static List<Settlement> Search(SearchFilters filters, ISortAndPage sortAndPage)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Account }, SecuredObject, PermissionValue.Read);
			var exp = (from a in DataContext.Reader.Settlements.Where(Accounts.AccountFilter.Current.QueryValidate(null, (Dal.Netpay.Settlement s) => s.Account_id)) select a);
			if (filters != null)
			{
				if (filters.ID.From.HasValue) exp = exp.Where((x) => x.Account_id >= filters.ID.From.Value);
				if (filters.ID.To.HasValue) exp = exp.Where((x) => x.Account_id <= filters.ID.To.Value);
				if (filters.InsertDate.From.HasValue) exp = exp.Where((x) => x.InsertDate >= filters.InsertDate.From.Value);
				if (filters.InsertDate.To.HasValue) exp = exp.Where((x) => x.InsertDate <= filters.InsertDate.To.Value);
				if (filters.SettlementDate.From.HasValue) exp = exp.Where((x) => x.SettlementDate >= filters.SettlementDate.From.Value);
				if (filters.SettlementDate.To.HasValue) exp = exp.Where((x) => x.SettlementDate <= filters.SettlementDate.To.Value);
				if (filters.InvoiceTotal.From.HasValue) exp = exp.Where((x) => x.InvoiceTotal >= filters.InvoiceTotal.From.Value);
				if (filters.InvoiceTotal.To.HasValue) exp = exp.Where((x) => x.InvoiceTotal <= filters.InvoiceTotal.To.Value);
				if (filters.WireTotal.From.HasValue) exp = exp.Where((x) => x.WireTotal >= filters.WireTotal.From.Value);
				if (filters.WireTotal.To.HasValue) exp = exp.Where((x) => x.WireTotal <= filters.WireTotal.To.Value);
				if (filters.AccountID != null && filters.AccountID.Count > 0) exp = exp.Where((x) => filters.AccountID.Contains(x.Account_id.GetValueOrDefault()));
			}
			var result = exp.ApplySortAndPage(sortAndPage).Select(x => new Settlement(x)).ToList();
			return result;
		}

		public class FutureSettlementsVO
		{
			public int SetSettlement_id { get; set; }
			public CommonTypes.Currency Currency { get; set; }
			public SettlementType SettlementType { get; set; }
			public string AccountName { get; set; }
			public int AccountID { get; set; }
			public decimal TotalAmount { get; set; }
			public int TotalCount { get; set; }
			//public string TotalRRAmount { get; set; }
			public DateTime? LastSettlementDate { get; set; }
			public decimal? LastSettlementAmount { get; set; }
		}

		public class FutureSettlementsFilters
		{
			public DateTime settlementDate;
			public SettlementType settlementType;
			public Netpay.CommonTypes.Currency? currency;
		}

		public static List<FutureSettlementsVO> GetFutureSettlements(FutureSettlementsFilters filters, ISortAndPage sortAndPage)
		{
			var dc = DataContext.Reader;
			dc.SetupLogger(true);
			string currencyIso = null;
			if (filters == null) filters = new FutureSettlementsFilters();
			if (filters.currency != null) currencyIso = Currency.Get(filters.currency.Value).IsoCode;
			var exp = (from a in dc.TransactionAmounts
				where
					a.SettlementType_id == (byte)filters.settlementType && (a.CurrencyISOCode == currencyIso || currencyIso == null) &&
					a.Settlement_id == null && a.SetSettlement_id != null && a.SettlementDate <= filters.settlementDate.Date
				group a by new { a.CurrencyISOCode, a.SetSettlement_id } into g 
				select new { details = g.Key, count = g.Count(), total = g.Sum(t => t.Total.GetValueOrDefault()) }).ApplySortAndPage(sortAndPage).ToList();
			var ret = new List<FutureSettlementsVO>();
			var setSettlements = AccountSettings.Load(exp.Select( g => g.details.SetSettlement_id.GetValueOrDefault()).ToList()).ToDictionary(a => a.AccountID);
			var accounts = Accounts.Account.Load(setSettlements.Select(s => s.Value.AccountID).ToList());

			var merchants = Merchants.Merchant.Load(accounts.Where(s => s.MerchantID != null).Select(s => s.MerchantID.GetValueOrDefault()).ToList()).ToDictionary(i=> i.MerchantID);
			var debitCompanies = DebitCompanies.DebitCompany.Load(accounts.Where(s => s.DebitCompanyID != null).Select(s => s.DebitCompanyID.GetValueOrDefault()).ToList()).ToDictionary(i => i.DebitCompanyID);
			var affiliates = Affiliates.Affiliate.Load(accounts.Where(s => s.AffiliateID != null).Select(s => s.AffiliateID.GetValueOrDefault()).ToList()).ToDictionary(i => i.AffiliateID);

			var loadCach = new System.Collections.Generic.Dictionary<string, object>();
			foreach(var item in exp){
				var nItem = new FutureSettlementsVO();
				nItem.SettlementType = filters.settlementType;
				nItem.SetSettlement_id = item.details.SetSettlement_id.GetValueOrDefault();
				nItem.TotalAmount = item.total;
				nItem.TotalCount = item.count;
				nItem.Currency = Currency.Get(item.details.CurrencyISOCode).EnumValue;
				ret.Add(nItem);

				AccountSettings setSettlement = null;
				if (!setSettlements.TryGetValue(nItem.SetSettlement_id, out setSettlement)) continue;
				nItem.AccountID = setSettlement.AccountID;
				switch (nItem.SettlementType)
				{
				case SettlementType.Merchant:
					Merchants.Merchant merchant;
					if (merchants.TryGetValue(nItem.AccountID, out merchant))
						nItem.AccountName = merchant.LegalName;
					break;
				case SettlementType.DebitCompany:
					DebitCompanies.DebitCompany debitCompany;
					if (debitCompanies.TryGetValue(nItem.AccountID, out debitCompany))
						nItem.AccountName = debitCompany.Name;
					break;
				case SettlementType.MerchantAffiliate:
				case SettlementType.DebitCompanyAffiliate:
					Affiliates.Affiliate affiliate;
					if (affiliates.TryGetValue(nItem.AccountID, out affiliate))
						nItem.AccountName = affiliate.Name;
					break;
				}
				var lastSettlment = (from s in dc.Settlements where
					(s.SettlementType_id == (int)filters.settlementType) &&
					(s.WireCurrencyISOCode == item.details.CurrencyISOCode) &&
					(s.Account_id == setSettlement.AccountID)
					select new { s.SettlementDate, s.SettlementTotal }).FirstOrDefault();
				if(lastSettlment != null) {
					nItem.LastSettlementAmount = lastSettlment.SettlementTotal;
					nItem.LastSettlementDate = lastSettlment.SettlementDate;
				}

			}
			return ret;
		}


	}
}
