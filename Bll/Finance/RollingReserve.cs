﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll
{
	public class RollingReserve
	{
		public enum RollingReserveType
		{
			None = 0,				//do not take rolling reserve
			Dynamic = 1,			//take every time
			FirstPaymentOnly = 2,	//single time
			FixedAmount = 3,		//take up to fixed amount
			External = 4,			//record and external hold
		}
		public int ID { get; set; }
		public decimal RollingPercent { get; set; }
		public int ReserveMonths { get; set; }
		public RollingReserveType ReserveType { get; set; }
		public bool EnableReserve { get; set; }
		public bool EnableReturn { get; set; }
		public decimal LimitReserveAmount { get; set; }
		public int LimitReserveCurrency { get; set; }

		public RollingReserve(Infrastructure.Domains.Domain domain, int merchantId)
		{
			var dc = new Dal.DataAccess.NetpayDataContext(domain.Sql1ConnectionString);
			var cmp = (from m in dc.tblCompanies where m.ID == merchantId select m).SingleOrDefault();
			if (cmp == null) throw new Exception(string.Format("unable to load rolling reserve settings for merchant {0}", cmp.ID));
			ID = merchantId;
			RollingPercent = cmp.SecurityDeposit;
			ReserveMonths = cmp.SecurityPeriod;
			ReserveType = (RollingReserveType)cmp.RRState;
			EnableReserve = cmp.RREnable;
			EnableReturn = cmp.RRAutoRet.GetValueOrDefault();
			LimitReserveAmount = cmp.RRKeepAmount;
			LimitReserveCurrency = cmp.RRKeepCurrency;
		}

		public decimal EvalTransactionReserve(Dal.DataAccess.NetpayDataContext dc, decimal amount, int currency, decimal? forcePercent, out DateTime? returnDate)
		{
			if (forcePercent == null) forcePercent = RollingPercent;
			returnDate = null;
			switch (ReserveType) {
			case RollingReserveType.None:
			case RollingReserveType.External:
				return 0;
			case RollingReserveType.FirstPaymentOnly:
				if ((from s in dc.Settlements where s.Merchant_id == ID && s.SettlementType_id == (int)SettlementType.Merchant select s.Settlement_id).Any())
					return 0;
				returnDate = DateTime.Now.AddMonths(ReserveMonths).Date;
				return amount * (forcePercent.Value / 100);
			case RollingReserveType.Dynamic:
				if (!EnableReserve) return 0;
				returnDate = DateTime.Now.AddMonths(ReserveMonths).Date;
				return amount * (forcePercent.Value / 100);
			case RollingReserveType.FixedAmount:
				decimal curReserve = 0;
				if (curReserve >= LimitReserveAmount) return 0;
				returnDate = DateTime.Now.AddMonths(ReserveMonths).Date;
				amount = amount * (forcePercent.Value / 100);
				if ((curReserve + amount) > LimitReserveAmount)
					amount = (LimitReserveAmount - curReserve);
				return amount;
			}
			return 0;
		}
	}
}
