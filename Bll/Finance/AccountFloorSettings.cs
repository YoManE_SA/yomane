﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll.Finance
{
	public class AccountFloorSettings : BaseDataObject
	{
		public class FloorFee : BaseDataObject
		{
			private Dal.Netpay.SetTransactionFloorFee _entity;

			public int ID { get { return _entity.SetTransactionFloorFee_id; } }
			public int AccountFloorSettingsID { get { return _entity.SetTransactionFloor_id; } }
			public decimal AmountTop { get { return _entity.AmountTop; } set { _entity.AmountTop = value; } }
			public decimal? PercentValue { get { return _entity.PercentValue; } set { _entity.PercentValue = value; } }
			public decimal? FixedAmount { get { return _entity.FixedAmount; } set { _entity.FixedAmount = value; } }

			private FloorFee(Dal.Netpay.SetTransactionFloorFee entity)  { _entity = entity; }
			public FloorFee(int accountFloorSettingsID)
				
			{
				_entity = new Dal.Netpay.SetTransactionFloorFee() { SetTransactionFloor_id = accountFloorSettingsID };
			}
	
		}

		private Dal.Netpay.SetTransactionFloor _entity;

		public int ID { get { return _entity.SetTransactionFloor_id; } }
		//public byte SettlementType { get { return _entity.SettlementType_id; } set { _entity.SettlementType_id = value; } }
		public int AccountID { get { return _entity.Account_id.GetValueOrDefault(); } set { _entity.Account_id = value; } }
		public string CurrencyISOCode { get { return _entity.CurrencyISOCode; } set { _entity.CurrencyISOCode = value; } }
		public string Title { get { return _entity.Title; } set { _entity.Title = value; } }
		
		private List<FloorFee> _fees;
		public List<FloorFee> Fees { get { return _fees; } }

		private AccountFloorSettings(Dal.Netpay.SetTransactionFloor entity)  { _entity = entity; _fees = new List<FloorFee>(); }
		public AccountFloorSettings(int accountId) 
		{
			_entity = new Dal.Netpay.SetTransactionFloor() { Account_id = accountId };
			_fees = new List<FloorFee>();
		}

		public static List<AccountFloorSettings> Search(int accountId)
		{
			return (from s in DataContext.Reader.SetTransactionFloors where s.Account_id == accountId select new AccountFloorSettings(s)).ToList();
		}
	}
}
