﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.DataAccess;

namespace Netpay.Bll
{
	public class FinanceSettlementVO
	{
		public int Settlement_id { get; set; }
		public int? TransactionPay_id { get; set; }
		public System.DateTime InsertDate { get; set; }
		public System.DateTime SettlementDate { get; set; }
		public byte SettlementType_id { get; set; }
		public int? Merchant_id { get; set; }
		public int? DebitCompany_id { get; set; }
		public int? Affiliate_id { get; set; }
		public string PayeeName { get; set; }
		public string CommentText { get; set; }
		public bool IsVisible { get; set; }

		public decimal? SettlementTotal { get; set; }
		public string SettlementCurrencyISOCode { get; set; }

		public decimal? VATPercent { get; set; }
		public byte? InvoiceIssuer_id { get; set; }
		public string InvoiceCurrencyISOCode { get; set; }
		public decimal? InvoiceCurrencyExchangeRate { get; set; }
		public decimal? InvoiceTotal { get; set; }
		public byte? InvoiceProvider_id { get; set; }
		public string InvoiceURL { get; set; }
		public int? InvoiceNumber { get; set; }
		public decimal? VATAmount { get { return InvoiceTotal.GetValueOrDefault() - (InvoiceTotal.GetValueOrDefault() * (100 / (100 + VATPercent))); } }

		public decimal? WireTotal { get; set; }
		public string WireCurrencyISOCode { get; set; }
		public decimal? WireCurrencyExchangeRate { get; set; }
		public decimal? WireFeePercentValue { get; set; }
		public decimal? WireFeeFixedAmount { get; set; }
		public decimal? WireFee { get { return WireFeeFixedAmount + ((Math.Abs(WireTotal.GetValueOrDefault()) - WireFeeFixedAmount) - ((Math.Abs(WireTotal.GetValueOrDefault()) - WireFeeFixedAmount) * (100 / (100 + WireFeePercentValue)))); } }


		//public FinanceSettlementVO() { }
		public FinanceSettlementVO(Netpay.Dal.Netpay.Settlement entity)
		{
			Settlement_id = entity.Settlement_id;
			TransactionPay_id = entity.TransactionPay_id;
			InsertDate = entity.InsertDate;
			SettlementDate = entity.SettlementDate;
			SettlementType_id = entity.SettlementType_id;
			Merchant_id = entity.Merchant_id;
			DebitCompany_id = entity.DebitCompany_id;
			Affiliate_id = entity.Affiliate_id;
			CommentText = entity.CommentText;
			IsVisible = entity.IsVisible;
			PayeeName = entity.PayeeName;

			SettlementCurrencyISOCode = entity.SettlementCurrencyISOCode;
			SettlementTotal = entity.SettlementTotal;
			VATPercent = entity.InvoiceVATPercent;

			InvoiceIssuer_id = entity.InvoiceIssuer_id;
			InvoiceTotal = entity.InvoiceTotal;
			InvoiceCurrencyISOCode = entity.InvoiceCurrencyISOCode;
			InvoiceCurrencyExchangeRate = entity.InvoiceCurrencyExchangeRate;
			InvoiceProvider_id = entity.InvoiceProvider_id;
			InvoiceURL = entity.InvoiceURL;
			InvoiceNumber = entity.InvoiceNumber;

			WireFeePercentValue = entity.WireFeePercentValue;
			WireFeeFixedAmount = entity.WireFeeFixedAmount;
			WireTotal = entity.WireTotal;
			WireCurrencyISOCode = entity.WireCurrencyISOCode;
			WireCurrencyExchangeRate = entity.WireCurrencyExchangeRate;
		}
	}

	public class FinanceSettlementItemVO
	{
		public int SettlementItem_id { get; set; }
		public int Settlement_id { get; set; }
		public AmountType? AmountType_id { get; set; }
		public string LineText { get; set; }
		public decimal? Amount { get; set; }
		public decimal? PercentValue { get; set; }
		public int? Quantity { get; set; }
		public decimal? Total { get; set; }
		public byte? SortOrder { get; set; }
		public bool IsFee { get; set; }

		public FinanceSettlementItemVO() { }
		public FinanceSettlementItemVO(Netpay.Dal.Netpay.SettlementItem entity)
		{
			SettlementItem_id = entity.SettlementItem_id;
			Settlement_id = entity.Settlement_id;
			AmountType_id = (AmountType?) entity.AmountType_id;
			LineText = entity.LineText;
			Amount = entity.Amount;
			Quantity = entity.Quantity;
			PercentValue = entity.PercentValue;
			Total = entity.Total;
			IsFee = entity.IsFee;
			SortOrder = entity.SortOrder;
		}
	}


	public static class FinanceSettlement
	{

		public class CreateSettlementOptions
		{
			public int SetSettlement_id { get; set; }
			public string CurrencyIso { get; set; }
			public DateTime? amountDateFrom;
			public DateTime? amountDateTo;

			public System.DateTime SettlementDate { get; set; }
			public bool includeTest;
			public bool includeStorage;
			public bool? includeHolded;
		}

		public static FinanceSettlementVO GetSettlement(string domainHost, int settlement_Id)
		{
			Infrastructure.Domains.Domain domain = Infrastructure.Domains.DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			return (from s in dc.Settlements where s.Settlement_id == settlement_Id select new FinanceSettlementVO(s)).SingleOrDefault();
		}

		public static List<FinanceSettlementVO> GetSettlements(string domainHost, SearchFilters sf)
		{
			Infrastructure.Domains.Domain domain = Infrastructure.Domains.DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			return (from s in dc.Settlements select new FinanceSettlementVO(s)).ToList();
		}

		public static void DeleteSettlement(string domainHost, int settlement_Id)
		{
			Infrastructure.Domains.Domain domain = Infrastructure.Domains.DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			dc.ExecuteCommand("Update Trans.TransactionAmount Set Settlement_id=null Where Settlement_id={0}", settlement_Id);
			dc.ExecuteCommand("Delete Finance.SettlementItem Where Settlement_id={0}", settlement_Id);
			dc.ExecuteCommand("Delete Finance.Settlement Where Settlement_id={0}", settlement_Id);
		}

		public static List<FinanceSettlementItemVO> GetSettlementItems(string domainHost, int settlement_Id)
		{
			Infrastructure.Domains.Domain domain = Infrastructure.Domains.DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			return (from i in dc.SettlementItems where i.Settlement_id == settlement_Id select new FinanceSettlementItemVO(i)).ToList();
		}

		public static FinanceSettlementVO CreateSettlement(string domainHost, CreateSettlementOptions options)
		{
			Infrastructure.Domains.Domain domain = Infrastructure.Domains.DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			var setSettlement = (from a in dc.SetSettlements where a.SetSettlement_id == options.SetSettlement_id select a).SingleOrDefault();
			var settlement = new Netpay.Dal.Netpay.Settlement();

			settlement.InsertDate = DateTime.Now;
			settlement.SettlementCurrencyISOCode = options.CurrencyIso;
			settlement.SettlementDate = options.SettlementDate;
			settlement.SettlementType_id = setSettlement.SettlementType_id;
			settlement.Account_id = setSettlement.Account_id;
			settlement.InvoiceIssuer_id = setSettlement.InvoiceIssuer_id;
			settlement.WireFeeFixedAmount = setSettlement.WireFeeAmount;
			settlement.WireFeePercentValue = setSettlement.WireFeePercent;
			settlement.WireCurrencyISOCode = setSettlement.WireCurrencyIsoCode ?? options.CurrencyIso;
			settlement.IsVisible = true;
			switch ((SettlementType)settlement.SettlementType_id) {
			case SettlementType.DebitCompany:
				settlement.PayeeName = (from d in dc.tblDebitCompanies where d.DebitCompany_ID == settlement.DebitCompany_id select d.dc_name).SingleOrDefault(); break;
			case SettlementType.Merchant:
				settlement.PayeeName = (from d in dc.tblCompanies where d.ID == settlement.Merchant_id select d.CompanyName).SingleOrDefault(); break;
			case SettlementType.DebitCompanyAffiliate:
			case SettlementType.MerchantAffiliate:
				settlement.PayeeName = (from d in dc.tblAffiliates where d.affiliates_id == settlement.Affiliate_id select d.name).SingleOrDefault(); break;
			}
			dc.Settlements.InsertOnSubmit(settlement);
			dc.SubmitChanges();

			string updateStatment = "Update Trans.TransactionAmount Set Settlement_id = {0} Where Settlement_id Is Null And SetSettlement_id = {1} And CurrencyISOCode = {2} And SettlementDate <= {3}";
			if (options.amountDateFrom != null) updateStatment += " And InsertDate >= {4}";
			if (options.amountDateTo != null) updateStatment += " And InsertDate <= {5}";
			dc.ExecuteCommand(updateStatment, settlement.Settlement_id, setSettlement.SetSettlement_id, options.CurrencyIso, options.SettlementDate, options.amountDateFrom.GetValueOrDefault(DateTime.Now), options.amountDateTo.GetValueOrDefault(DateTime.Now));

			var totalList = (from a in dc.TransactionAmounts 
				where a.Settlement_id == settlement.Settlement_id
				group a by new { a.AmountType_id, a.IsFee, a.PercentValue, a.FixedAmount } into g
				select new { Key = g.Key, Count = g.Count(), TotalFixed = g.Sum(i => i.FixedAmount), TotalPercent = g.Sum(i => i.PercentAmount) }).ToList();
			var items = new List<Netpay.Dal.Netpay.SettlementItem>();
			var groups = totalList.GroupBy(t => new { t.Key.AmountType_id, t.Key.IsFee });
			foreach (var group in groups)
			{
				foreach (var subGroup in group.GroupBy(t => t.Key.FixedAmount))
				{
					if (subGroup.Key == 0) continue;
					var rowItem = new Netpay.Dal.Netpay.SettlementItem();
					rowItem.Settlement_id = settlement.Settlement_id;
					rowItem.AmountType_id = group.Key.AmountType_id;
					rowItem.IsFee = group.Key.IsFee;
					rowItem.LineText = "Fixed";
					if (!rowItem.IsFee) rowItem.LineText += " Transfer";
					rowItem.Amount = subGroup.Key;
					rowItem.Quantity = subGroup.Sum(s => s.Count);
					rowItem.Total = subGroup.Sum(s => s.TotalFixed);
					items.Add(rowItem);
				}

				foreach (var subGroup in group.GroupBy(t => t.Key.PercentValue))
				{
					if (subGroup.Key == 0) continue;
					var rowItem = new Netpay.Dal.Netpay.SettlementItem();
					rowItem.Settlement_id = settlement.Settlement_id;
					rowItem.AmountType_id = group.Key.AmountType_id;
					rowItem.IsFee = group.Key.IsFee;
					rowItem.LineText = "Relative";
					if (!rowItem.IsFee) rowItem.LineText = " Transfer";
					rowItem.PercentValue = subGroup.Key;
					rowItem.Quantity = subGroup.Sum(s => s.Count);
					rowItem.Total = subGroup.Sum(s => s.TotalPercent);
					items.Add(rowItem);
				}
			}
			if (setSettlement.StorageFeeAmount != null && (setSettlement.StorageFeeCurrencyIsoCode == null || setSettlement.StorageFeeCurrencyIsoCode == settlement.SettlementCurrencyISOCode))
			{
				if (settlement.SettlementType_id == (byte) SettlementType.Merchant){
					var storageCount = (from s in dc.tblCCStorages where s.companyID == settlement.Merchant_id && s.payID == 0 select s.ID).Count();
					if (storageCount > 0){
						var rowItem = new Netpay.Dal.Netpay.SettlementItem();
						rowItem.Settlement_id = settlement.Settlement_id;
						rowItem.AmountType_id = (byte)AmountType.CCStorage;
						rowItem.IsFee = true;
						rowItem.Quantity = storageCount;
						rowItem.Amount = setSettlement.StorageFeeAmount;
						rowItem.Total = -(storageCount * setSettlement.StorageFeeAmount.Value);
						items.Add(rowItem);
					}
				}
			}
			dc.SettlementItems.InsertAllOnSubmit(items);
			dc.SubmitChanges();
			return UpdateSettlementTotal(domainHost, settlement.Settlement_id, items);
		}

		private static FinanceSettlementVO UpdateSettlementTotal(string domainHost, int settlementId, List<Netpay.Dal.Netpay.SettlementItem> items)
		{
			Infrastructure.Domains.Domain domain = Infrastructure.Domains.DomainsManager.GetDomain(domainHost);
			var dc = new NetpayDataContext(domain.Sql1ConnectionString);
			var settlement = (from a in dc.Settlements where a.Settlement_id == settlementId select a).SingleOrDefault();
			if (items == null) items = (from a in dc.SettlementItems where a.Settlement_id == settlementId select a).ToList();
			settlement.SettlementTotal = items.Sum(i => i.Total).GetValueOrDefault();
			settlement.InvoiceTotal = -items.Where(i => i.IsFee).Sum(i => i.Total).GetValueOrDefault();
			var invoiceIssuer = (from ii in dc.InvoiceIssuers where ii.InvoiceIssuer_id == settlement.InvoiceIssuer_id select ii).SingleOrDefault();
			if (invoiceIssuer != null)
			{
				settlement.InvoiceProvider_id = invoiceIssuer.InvoiceProvider_id;
				settlement.InvoiceVATPercent = invoiceIssuer.VATPercent;
				settlement.InvoiceCurrencyISOCode = invoiceIssuer.InvoiceCurrencyISOCode;
				if (settlement.InvoiceCurrencyISOCode != settlement.SettlementCurrencyISOCode)
					settlement.InvoiceCurrencyExchangeRate = Money.ConvertRate(domain, domain.Cache.GetCurrency(settlement.SettlementCurrencyISOCode).EnumValue, domain.Cache.GetCurrency(settlement.InvoiceCurrencyISOCode).EnumValue);
				else settlement.InvoiceCurrencyExchangeRate = 1;
				settlement.InvoiceTotal = settlement.InvoiceTotal.GetValueOrDefault() * settlement.InvoiceCurrencyExchangeRate;
				var vatAmount = (settlement.InvoiceTotal / 100) * settlement.InvoiceVATPercent;
				settlement.InvoiceTotal += vatAmount;
			}
			settlement.WireTotal = settlement.SettlementTotal;
			if (settlement.WireCurrencyISOCode != settlement.SettlementCurrencyISOCode)
				settlement.WireCurrencyExchangeRate = Money.ConvertRate(domain, domain.Cache.GetCurrency(settlement.SettlementCurrencyISOCode).EnumValue, domain.Cache.GetCurrency(settlement.WireCurrencyISOCode).EnumValue);
			else settlement.WireCurrencyExchangeRate = 1;
			settlement.WireTotal = settlement.SettlementTotal.GetValueOrDefault() * settlement.WireCurrencyExchangeRate;

			settlement.WireTotal -= (settlement.WireFeeFixedAmount + (System.Math.Abs(settlement.WireTotal.GetValueOrDefault()) * (settlement.WireFeePercentValue / 100)));
			dc.SubmitChanges();
			return new FinanceSettlementVO(settlement);
		}

		public static FinanceSettlementVO AddSettlementLine(string domainHost, FinanceSettlementItemVO item)
		{
			Infrastructure.Domains.Domain domain = Infrastructure.Domains.DomainsManager.GetDomain(domainHost);
			var dc = new NetpayDataContext(domain.Sql1ConnectionString);
			var entity = new Dal.Netpay.SettlementItem();
			dc.SettlementItems.InsertOnSubmit(entity);
			entity.Settlement_id = item.Settlement_id;
			entity.LineText = item.LineText;
			entity.Quantity = item.Quantity;
			entity.PercentValue = item.PercentValue;
			entity.Amount = item.Amount;
			entity.Total = item.Total;
			entity.IsFee = item.IsFee;
			dc.SubmitChanges();
			return UpdateSettlementTotal(domainHost, item.Settlement_id, null);
		}

		public class FutureSettlementsVO
		{
			public int SetSettlement_id { get; set; }
			public CommonTypes.Currency Currency { get; set; }
			public SettlementType SettlementType { get; set; }
			public string acceptorName { get; set; }
			public int acceptorID { get; set; }
			public decimal TotalAmount { get; set; }
			public int TotalCount { get; set; }
			//public string TotalRRAmount { get; set; }
			public DateTime? LastSettlementDate { get; set; }
			public decimal? LastSettlementAmount { get; set; }
		}

		public static List<FutureSettlementsVO> GetFutureSettlements(string domainHost, DateTime settlementDate, SettlementType settlementType, Netpay.CommonTypes.Currency? currency)
		{
			Infrastructure.Domains.Domain domain = Infrastructure.Domains.DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			dc.SetupLogger(true);
			string currencyIso = null;
			if (currency != null) currencyIso = domain.Cache.GetCurrency((int)currency).IsoCode;
			var exp = (from a in dc.TransactionAmounts
				where
					a.SettlementType_id == (byte)settlementType && (a.CurrencyISOCode == currencyIso || currencyIso == null) &&
					a.Settlement_id == null && a.SetSettlement_id != null && a.SettlementDate <= settlementDate.Date
				group a by new { a.CurrencyISOCode, a.SetSettlement_id } into g 
				select new { details = g.Key, count = g.Count(), total = g.Sum(t => t.Total.GetValueOrDefault()) }).ToList();
			var ret = new List<FutureSettlementsVO>();
			var loadCach = new System.Collections.Generic.Dictionary<string, object>();
			foreach(var item in exp){
				var nItem = new FutureSettlementsVO();
				nItem.SettlementType = settlementType;
				nItem.SetSettlement_id = item.details.SetSettlement_id.GetValueOrDefault();
				nItem.TotalAmount = item.total;
				nItem.TotalCount = item.count;
				nItem.Currency = (CommonTypes.Currency) domain.Cache.GetCurrency(item.details.CurrencyISOCode).ID;

				object dataItem = null;
				if (!loadCach.TryGetValue("S" + nItem.SetSettlement_id, out dataItem)) {
					dataItem = (from a in dc.SetSettlements where a.SetSettlement_id == nItem.SetSettlement_id select a).SingleOrDefault();
					loadCach.Add("S" + nItem.SetSettlement_id, dataItem);
				}
				var setSettlement = dataItem as Dal.Netpay.SetSettlement;
				switch (nItem.SettlementType)
				{
				case SettlementType.Merchant:
					nItem.acceptorID = setSettlement.Merchant_id.GetValueOrDefault();
					if (!loadCach.TryGetValue("M" + nItem.acceptorID, out dataItem)) {
						dataItem = (from a in dc.tblCompanies where a.ID == nItem.acceptorID select a).SingleOrDefault();
						loadCach.Add("M" + nItem.acceptorID, dataItem);
					}
					if (dataItem != null) nItem.acceptorName = (dataItem as Dal.Netpay.tblCompany).CompanyName;
					break;
				case SettlementType.DebitCompany:
					nItem.acceptorID = setSettlement.DebitCompany_id.GetValueOrDefault();
					if (!loadCach.TryGetValue("D" + nItem.acceptorID, out dataItem)) {
						dataItem = (from a in dc.tblDebitCompanies where a.DebitCompany_ID == nItem.acceptorID select a).SingleOrDefault();
						loadCach.Add("D" + nItem.acceptorID, dataItem);
					}
					if (dataItem != null) nItem.acceptorName = (dataItem as Dal.Netpay.tblDebitCompany).dc_name;
					break;
				case SettlementType.MerchantAffiliate:
				case SettlementType.DebitCompanyAffiliate:
					nItem.acceptorID = setSettlement.Affiliate_id.GetValueOrDefault();
					if (!loadCach.TryGetValue("A" + nItem.acceptorID, out dataItem)){
						dataItem = (from a in dc.tblAffiliates where a.affiliates_id == nItem.acceptorID select a).SingleOrDefault();
						loadCach.Add("A" + nItem.acceptorID, dataItem);
					}
					if (dataItem != null) nItem.acceptorName = (dataItem as Dal.Netpay.tblAffiliate).name;
					break;
				}
				var lastSettlment = (from s in dc.Settlements where
					(s.SettlementType_id == (int)settlementType) &&
					(s.WireCurrencyISOCode == item.details.CurrencyISOCode) &&
					((s.Affiliate_id == setSettlement.Affiliate_id) || (s.Affiliate_id == null && setSettlement.Affiliate_id == null)) &&
					((s.Merchant_id == setSettlement.Merchant_id) || (s.Merchant_id == null && setSettlement.Merchant_id == null)) &&
					((s.DebitCompany_id == setSettlement.DebitCompany_id) || (s.DebitCompany_id == null && setSettlement.DebitCompany_id == null)) 
					select new { s.SettlementDate, s.SettlementTotal }).FirstOrDefault();
				if(lastSettlment != null) {
					nItem.LastSettlementAmount = lastSettlment.SettlementTotal;
					nItem.LastSettlementDate = lastSettlment.SettlementDate;
				}

				ret.Add(nItem);
			}
			return ret;
		}

	}
}

