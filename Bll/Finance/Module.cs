﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Finance
{
	public class Module : Infrastructure.Module
	{
		public const string ModuleName = "Finance";
		public override string Name { get { return ModuleName; } }
		public override decimal Version { get { return 1.0m; } }
		public static Module Current { get { return Infrastructure.Module.Get(ModuleName) as Module; } }
		protected override void OnInstall(EventArgs e)
		{
			//Infrastructure.Security.SecuredObject.Create(this, Settlement.SecuredObjectName, PermissionGroup.ReadEditDelete);
			base.OnInstall(e);
		}

		protected override void OnUninstall(EventArgs e)
		{
			Infrastructure.Security.SecuredObject.Remove(this);
			base.OnUninstall(e);
		}
	}
}
