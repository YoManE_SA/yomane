﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll.Finance
{
	public class AccountFeeSettings : BaseDataObject
	{
		private Dal.Netpay.SetTransactionFee _entity;

		public int ID { get { return _entity.SettlementType_id; } }
		public int AccountID { get { return _entity.Account_id.GetValueOrDefault(); } }
		//public int AccountSettingsID { get { return (AmountType) _entity.SetSettlement_id; } }
		public FeeCalcMethod FeeCalcMethod { get { return (FeeCalcMethod)_entity.FeeCalcMethod_id; } set { _entity.FeeCalcMethod_id = (byte)value; } }
		public AmountType AmountType { get { return (AmountType) _entity.AmountType_id; } set { _entity.AmountType_id = (byte)value; } }
		//public byte SettlementType_id { get { return _entity.SettlementType_id; } set { _entity.SettlementType_id = value; } }

		public string CurrencyISOCode { get { return _entity.CurrencyISOCode; } set { _entity.CurrencyISOCode = value; } }
		public int? SetTransactionFloor_id { get { return _entity.SetTransactionFloor_id; } set { _entity.SetTransactionFloor_id = value; } }
		public decimal? PercentValue { get { return _entity.PercentValue; } set { _entity.PercentValue = value; } }
		public decimal? FixedAmount { get { return _entity.FixedAmount; } set { _entity.FixedAmount = value; } }
		public decimal? SettlementPercentValue { get { return _entity.SettlementPercentValue; } set { _entity.SettlementPercentValue = value; } }

		private AccountFeeSettings(Dal.Netpay.SetTransactionFee entity)  { _entity = entity; }
		public AccountFeeSettings(int accountId, int settingsId) 
			 { 
			_entity = new Dal.Netpay.SetTransactionFee();
			_entity.Account_id = accountId;
			//_entity.setSettlement_id = settingsId
		}
		
	}
}
