﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll.Finance
{
	public class TransactionAmount : BaseDataObject
	{
		public class SearchFilters
		{
			public Infrastructure.Range<DateTime?> Date;
			public Infrastructure.Range<decimal?> Amount;
			public List<Netpay.CommonTypes.TransactionHistoryType> Types;
			public List<int> merchantIDs;
		}

		protected Netpay.Dal.Netpay.TransactionAmount _entity;

		public int ID { get { return _entity.TransactionAmount_id; } }
		public System.DateTime Date { get { return _entity.InsertDate; } }
		public int? AccountId { get { return _entity.Account_id; } }
		public AmountType AmountType { get { return (AmountType) _entity.AmountType_id; } }

		public int? TransPassId { get { return _entity.TransPass_id; } }
		public int? TransPreAuthId { get { return _entity.TransPreAuth_id; } }
		public int? TransFailId { get { return _entity.TransFail_id; } }

		public SettlementType SettlementType { get { return (SettlementType)_entity.SettlementType_id; } }
		public int? SetSettlementId { get { return _entity.SetSettlement_id; } }
		public int? SettlementId { get { return _entity.Settlement_id; } }
		public System.DateTime? SettlementDate { get { return _entity.SettlementDate; } }

		public int? Installment { get { return _entity.Installment; } }
		public string CurrencyISOCode { get { return _entity.CurrencyISOCode; } }
		public decimal? PercentValue { get { return _entity.PercentValue; } }
		public decimal PercentAmount { get { return _entity.PercentAmount; } }
		public decimal FixedAmount { get { return _entity.FixedAmount; } }
		public decimal Total { get { return _entity.Total.GetValueOrDefault(); } }
		public bool IsFee { get { return _entity.IsFee; } }

		protected TransactionAmount(Dal.Netpay.TransactionAmount entity)  { _entity = entity; }

		public static List<TransactionAmount> LoadForTransaction(int transID, Netpay.Infrastructure.TransactionStatus status)
		{
			//var user = context.IsUserOfType(credentialsToken);
			var exp = from th in DataContext.Reader.TransactionAmounts select th;
			switch (status)
			{
				case Netpay.Infrastructure.TransactionStatus.Captured:
					exp = exp.Where(th => th.TransPass_id == transID);
					break;
				case Netpay.Infrastructure.TransactionStatus.Declined:
					exp = exp.Where(th => th.TransFail_id == transID);
					break;
				case Netpay.Infrastructure.TransactionStatus.Authorized:
					exp = exp.Where(th => th.TransPreAuth_id == transID);
					break;
				default:
					return null;
			}

			var results = exp.Select(th => new TransactionAmount(th)).ToList();
			return results;
		}		

	}
}
