﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.Finance
{
	public enum SettlementType
	{
		Merchant = 1,
		DebitCompany = 2,
		MerchantAffiliate = 3,
		DebitCompanyAffiliate = 4
	}

	public enum FeeCalcMethod
	{
		TransactionAmount = 1,
		MerchantFee = 2,
		BankFee = 3,
		RevenueFee = 4,
	}

	public enum AmountType
	{

		Authorization = 1,
		Sale = 2,
		Capture = 3,
		Installment = 4,
		Refund = 5,
		Clarification = 6,
		Chargeback = 7,
		Finance = 8,
		Decline = 9,
		RollingReserve = 10,

		CCStorage = 13,
		System = 14,

		InvoiceTransaction = 15,
		Transaction = 16,
		InstallmentItem = 17,
		//Tax = 11,
		//Wire = 12,

	}
}
