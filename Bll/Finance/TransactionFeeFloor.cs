﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure.Domains;
using Netpay.Dal.DataAccess;

namespace Netpay.Bll
{
	public class TransactionFloor
	{
		public TransactionFloor(NetpayDataContext dc, int floorId)
		{
			dataItem = (from ff in dc.SetTransactionFloors where ff.SetTransactionFloor_id == floorId select ff ).SingleOrDefault();
			if (dataItem == null) throw new ApplicationException("SetTransactionFloors not found for " + floorId);
			fees = (from ff in dc.SetTransactionFloorFees where ff.SetTransactionFloor_id == floorId orderby ff.AmountTop ascending select ff).ToList();
		}
		public Netpay.Dal.Netpay.SetTransactionFloor dataItem { get; set; }
		private List<Netpay.Dal.Netpay.SetTransactionFloorFee> fees;
		private DateTime lastTransDate;
		private decimal currentFloor;
		public void LoadToDate(Domain domain, DateTime updateTime)
		{
			currentFloor = 0;
			lastTransDate = updateTime;
			var fromData = new DateTime(updateTime.Year, updateTime.Month, 1);
			var dc = new NetpayDataContext(domain.Sql1ConnectionString);
			var floorTrans = (from t in dc.tblCompanyTransPasses
				where t.InsertDate >= fromData && t.InsertDate < updateTime 
				group t by t.Currency into g select new { cur = g.Key, sum = g.Sum(t => t.Amount) }).ToList();
			foreach(var v in floorTrans)
			{
				decimal translatedAmount = new Bll.Money(v.sum, domain.Cache.GetCurrency(v.cur.GetValueOrDefault())).ConvertTo(domain.Cache.GetCurrency(dataItem.CurrencyISOCode)).Amount;
				currentFloor += translatedAmount;
			}
		}

		private bool ShouldReset(DateTime dtFloor, DateTime dtTrans)
		{
			return dtFloor.Month != dtTrans.Month || dtFloor.Year != dtTrans.Year;
		}

		public Netpay.Dal.Netpay.SetTransactionFloorFee EvaluateNext(Domain domain, decimal amount, int amountCurrency, DateTime transDate, bool dbUpdate)
		{
			decimal translatedAmount = new Bll.Money(amount, domain.Cache.GetCurrency((int)amountCurrency)).ConvertTo(domain.Cache.GetCurrency(dataItem.CurrencyISOCode)).Amount;
			if (dbUpdate){
				var dc = new NetpayDataContext(domain.Sql1ConnectionString);
				var monthlyFloorTotal = (from t in dc.MonthlyFloorTotals where t.MonthlyFloorTotal_id == dataItem.SetTransactionFloor_id select t).SingleOrDefault();
				if (monthlyFloorTotal == null) {
					monthlyFloorTotal = new Netpay.Dal.Netpay.MonthlyFloorTotal();
					monthlyFloorTotal.DateInFocus = DateTime.Now;
					monthlyFloorTotal.Amount = 0;
					dc.MonthlyFloorTotals.InsertOnSubmit(monthlyFloorTotal);
				}
				if (ShouldReset(monthlyFloorTotal.DateInFocus.Value, transDate))
				{
					monthlyFloorTotal.DateInFocus = transDate;
					monthlyFloorTotal.Amount = 0;
				}
				currentFloor = monthlyFloorTotal.Amount.GetValueOrDefault() + translatedAmount;
				monthlyFloorTotal.Amount = currentFloor;
				dc.SubmitChanges();

			} else {
				if (ShouldReset(lastTransDate, transDate)) {
					lastTransDate = transDate;
					currentFloor = 0;
				}
				currentFloor += translatedAmount;
			}
			var floorFee = (from f in fees where f.AmountTop > currentFloor select f).FirstOrDefault();
			if (floorFee == null) throw new ApplicationException(string.Format("No Fees Settings for SetTransactionFloorFees => SetTransactionFloor_id : {0}, amount : {1}", dataItem.SetTransactionFloor_id, currentFloor));
			return floorFee;
		}
	}
}
