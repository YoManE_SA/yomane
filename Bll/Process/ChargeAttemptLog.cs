﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Process
{
    public class ChargeAttemptLog : BaseDataObject
    {
        public const string SecuredObjectName = "ChargeAttemptLog";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Process.Module.Current, SecuredObjectName); } }

        public static Dictionary<Infrastructure.SuccessOrFail, System.Drawing.Color> SuccessOrFailStatus = new Dictionary<SuccessOrFail, System.Drawing.Color>
        {
            {Infrastructure.SuccessOrFail.Success, System.Drawing.Color.Green },
            {Infrastructure.SuccessOrFail.Fail, System.Drawing.Color.Red }
        };

        public class SearchFilters
        {
            public int? specificChargeAttemptLogId;
            public Infrastructure.Range<int?> ID;
            public Infrastructure.Range<DateTime?> Date;
            public bool WithoutTransaction;
            public string MerchantNumber;
            public string ReplyCode;
            public string Text;
            public int? TransactionId;
            public bool? IsSuccess;
            public Infrastructure.TransactionStatus? TransactionStatus;
        }

        private Dal.Netpay.tblLogChargeAttempt _entity;

        public int ID { get { return _entity.LogChargeAttempts_id; } }
        public Infrastructure.TransactionSource TransactionTypeId { get { return (Infrastructure.TransactionSource)_entity.TransactionType_id; }
            set { _entity.TransactionType_id = (short)value; } }
        public string MerchantNumber { get { return _entity.Lca_MerchantNumber; } set { _entity.Lca_MerchantNumber = value; } }
        public string RemoteIPAddress { get { return _entity.Lca_RemoteAddress; } set { _entity.Lca_RemoteAddress = value; } }
        public string LocalIPAddress { get { return _entity.Lca_LocalAddr; } set { _entity.Lca_LocalAddr = value; } }
        public string RequestMethod { get { return _entity.Lca_RequestMethod; } set { _entity.Lca_RequestMethod = value; } }
        public string PathTranslate { get { return _entity.Lca_PathTranslate; } set { _entity.Lca_PathTranslate = value; } }
        public string HttpReferer { get { return _entity.Lca_HttpReferer; } set { _entity.Lca_HttpReferer = value; } }
        public string QueryString { get { return _entity.Lca_QueryString; } set { _entity.Lca_QueryString = value; } }
        public string RequestForm { get { return _entity.Lca_RequestForm; } set { _entity.Lca_RequestForm = value; } }
        public string SessionContents { get { return _entity.Lca_SessionContents; } set { _entity.Lca_SessionContents = value; } }
        public string ReplyCode { get { return _entity.Lca_ReplyCode; } set { _entity.Lca_ReplyCode = value; } }
        public string ReplyDescription { get { return _entity.Lca_ReplyDesc; } set { _entity.Lca_ReplyDesc = value; } }
        public int? TransactionID { get { return _entity.Lca_TransNum; } set { _entity.Lca_TransNum = value; } }
        public System.DateTime? DateStart { get { return _entity.Lca_DateStart; } set { _entity.Lca_DateStart = value; } }
        public System.DateTime? DateEnd { get { return _entity.Lca_DateEnd; } set { _entity.Lca_DateEnd = value; } }
        public string HttpHost { get { return _entity.Lca_HttpHost; } set { _entity.Lca_HttpHost = value; } }
        public string TimeString { get { return _entity.Lca_TimeString; } set { _entity.Lca_TimeString = value; } }
        //public string RequestString { get { return _entity.Lca_RequestString; } set { _entity.Lca_RequestString = value; } }
        //public string ResponseString { get { return _entity.Lca_ResponseString; } set { _entity.Lca_ResponseString = value; } }
        public bool IsSuccess { get { return TransactionID.HasValue && (ReplyCode == "000" || ReplyCode == "001"); } }

        public Infrastructure.TransactionStatus TransactionStatus
        {
            get
            {
                switch (ReplyCode)
                {
                    case "000":
                        return TransactionStatus.Captured;
                    case "001":
                        return TransactionStatus.Pending;
                }
                return TransactionStatus.Declined;
            }
        }

        public TimeSpan? Duration
        {
            get
            {
                if (DateStart == null || DateEnd == null)
                    return null;
                return DateEnd.GetValueOrDefault() - DateStart.GetValueOrDefault();
            }
        }

        private ChargeAttemptLog(Dal.Netpay.tblLogChargeAttempt entity)
        {
            _entity = entity;
        }

        public ChargeAttemptLog()
        {
            _entity = new Dal.Netpay.tblLogChargeAttempt();
        }
        
        private static IQueryable<Dal.Netpay.tblLogChargeAttempt> SearchExpression(SearchFilters filters)
        {            
            if (ObjectContext.Current.User.IsInRole(UserRole.Merchant))
            {
                if (filters == null) filters = new SearchFilters();
                filters.MerchantNumber = Merchants.Merchant.Current.AccountNumber;
            }

            var exp = (from l in DataContext.Reader.tblLogChargeAttempts orderby l.LogChargeAttempts_id descending select l) as IQueryable<Dal.Netpay.tblLogChargeAttempt>;
            if (filters != null)
            {
                if (filters.ID.From.HasValue)
                    exp = exp.Where(l => l.LogChargeAttempts_id >= filters.ID.From);
                if (filters.ID.To.HasValue)
                    exp = exp.Where(l => l.LogChargeAttempts_id <= filters.ID.To);
                if (filters.Date.From.HasValue)
                    exp = exp.Where(l => l.Lca_DateStart >= filters.Date.From.Value.MinTime());
                if (filters.Date.To.HasValue)
                    exp = exp.Where(l => l.Lca_DateStart <= filters.Date.To.Value.AlignToEnd());
                if (!string.IsNullOrEmpty(filters.MerchantNumber))
                    exp = exp.Where(l => l.Lca_MerchantNumber == filters.MerchantNumber);
                if (filters.ReplyCode != null)
                    exp = exp.Where(l => l.Lca_ReplyCode == filters.ReplyCode);
                if (filters.TransactionId != null)
                    exp = exp.Where(l => l.Lca_TransNum == filters.TransactionId.Value);
                if (filters.WithoutTransaction == true)
                    exp = exp.Where(l => l.Lca_TransNum == null);
                if (filters.specificChargeAttemptLogId.HasValue)
                    exp = exp.Where(l => l.LogChargeAttempts_id == filters.specificChargeAttemptLogId);

                if (filters.IsSuccess.HasValue)
                {
                    if (filters.IsSuccess.Value)
                    {
                        exp = exp.Where(l => l.Lca_TransNum.HasValue && (l.Lca_ReplyCode == "000" || l.Lca_ReplyCode == "001"));
                    }
                    else
                    {
                        exp = exp.Where(l => !(l.Lca_ReplyCode == "000" || l.Lca_ReplyCode == "001"));
                    }
                }

                if (filters.TransactionStatus.HasValue)
                {
                    if (filters.TransactionStatus.Value == TransactionStatus.Captured)
                    {
                        exp = exp.Where(l => (l.Lca_ReplyCode == "000"));
                    }
                    else if (filters.TransactionStatus.Value == TransactionStatus.Pending)
                    {
                        exp = exp.Where(l => (l.Lca_ReplyCode == "001"));
                    }
                    else if (filters.TransactionStatus.Value == TransactionStatus.Declined)
                    {
                        exp = exp.Where(l => !(l.Lca_ReplyCode == "000" || l.Lca_ReplyCode == "001"));
                    }
                }
            }
            return exp;
        }


        public static List<ChargeAttemptLog> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var exp = SearchExpression(filters);

            return exp.ApplySortAndPage(sortAndPage, "Lca_").ToList().Select(l => new ChargeAttemptLog(l)).ToList();
        }

        public class ChargeAttemptsWithoutTransactions
        {
            public int? FirstID { get; set; }
            public int? LastID { get; set; }
            public DateTime? FirstDate { get; set; }
            public DateTime? LastDate { get; set; }
            public int Count { get; set; }
        }

        public static ChargeAttemptsWithoutTransactions GetChargeAttemptsWithoutTransaction(SearchFilters filters)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);


            var expression = SearchExpression(filters);

            if (expression.Count() == 0)
                return new ChargeAttemptsWithoutTransactions()
                {
                    FirstID = null,
                    FirstDate = null,
                    LastDate = null,
                    LastID = null,
                    Count = 0
                };
            else

            return new ChargeAttemptsWithoutTransactions()
            {
                FirstID = expression.Min(x=>x.LogChargeAttempts_id),
                FirstDate = expression.Min(x=>x.Lca_DateStart),
                LastID = expression.FirstOrDefault().LogChargeAttempts_id,//the SearchExpression function orders the items by id desc , So the first id is the biggest 
                LastDate = expression.Max(x=>x.Lca_DateStart),
                Count = expression.Count()
            };
        }


        public void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            DataContext.Writer.tblLogChargeAttempts.Update(_entity, (_entity.LogChargeAttempts_id != 0));
            DataContext.Writer.SubmitChanges();
        }

        public static ChargeAttemptLog Load(int id)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var ret = (from c in DataContext.Reader.tblLogChargeAttempts
                       where c.LogChargeAttempts_id == id
                       select c).SingleOrDefault();
            if (ret == null) return null;
            return new ChargeAttemptLog(ret);
        }

        public Transactions.Transaction Transaction
        {
            get
            {
                if (TransactionID == null)
                    return null;
                return Transactions.Transaction.GetTransaction(null, TransactionID.Value, this.TransactionStatus);
            }
        }
    }
}
