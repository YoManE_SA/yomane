﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;


namespace Netpay.Bll.Process
{
    public class Batch : BaseDataObject
    {
        public enum BatchStatus { New = 0, Prepared = 50, Sent = 51, FailSend = 100 }
        private Dal.Netpay.AuthorizationBatch _entity;

        public int ID { get { return _entity.AuthorizationBatch_id; } }
        public BatchStatus Status { get { return (BatchStatus)_entity.ActionStatus_id; } }
        public System.DateTime BatchDate { get { return _entity.BatchDate; } }
        public int InternalKey { get { return _entity.BatchInternalKey; } }
        public string TerminalNumber { get { return _entity.TransTerminalNumber; } }
        public int? DebitCompanyID { get { return _entity.TransDebitCompanyID; } }
        public int TransCount { get { return _entity.TransCount.GetValueOrDefault(); } }
        public decimal TransTotalDebit { get { return _entity.TransTotalDebit.GetValueOrDefault(); } }
        public decimal TransTotalCredit { get { return _entity.TransTotalCredit.GetValueOrDefault(); } }
        public string ResultDescription { get { return _entity.ResultDescription; } }

        private Batch(Dal.Netpay.AuthorizationBatch entity)
        {
            _entity = entity;
        }

        public Batch(int debitCompanyId, string terminalNumber)
        {
            _entity = new Dal.Netpay.AuthorizationBatch() { 
                BatchDate = DateTime.Now, 
                TransDebitCompanyID = debitCompanyId, 
                TransTerminalNumber = terminalNumber,
                ActionStatus_id = (byte)BatchStatus.New 
            };
        }

        public void SetInternalID(int? predef)
        {
            if (predef == null)
            {
                var exp = (from n in DataContext.Reader.AuthorizationBatches select n);
                exp = exp.Where(n => n.TransDebitCompanyID == DebitCompanyID);
                if (!string.IsNullOrEmpty(TerminalNumber)) exp = exp.Where(n => n.TransTerminalNumber == TerminalNumber);
                predef = exp.Select(v => (int?)v.BatchInternalKey).Max().GetValueOrDefault() + 1;
            }
            _entity.BatchInternalKey = predef.Value;
        }

        public void MarkTransationsForBatch() 
        {
            if (_entity.AuthorizationBatch_id == 0) Save();
            DataContext.Writer.ExecuteCommand("Update tblCompanyTransPass Set AuthorizationBatchID={0} Where AuthorizationBatchID Is Null And DebitCompanyID={1}" + (string.IsNullOrEmpty(TerminalNumber) ? "" : " And TerminalNumber={2}"), _entity.AuthorizationBatch_id, DebitCompanyID.GetValueOrDefault(), TerminalNumber.EmptyIfNull());
        }

        public void UnMarkTransationsForBatch()
        {
            if (_entity.AuthorizationBatch_id == 0) return;
            DataContext.Writer.ExecuteCommand("Update tblCompanyTransPass Set AuthorizationBatchID=Null Where AuthorizationBatchID={0} And DebitCompanyID={1}" + (string.IsNullOrEmpty(TerminalNumber) ? "" : " And TerminalNumber={2}"), _entity.AuthorizationBatch_id, DebitCompanyID.GetValueOrDefault(), TerminalNumber.EmptyIfNull());
        }

        public List<Transactions.Transaction> GetTransactions(bool loadPaymentData = false)
        {
            return Transactions.Transaction.SearchTransCaptured(new Transactions.Transaction.LoadOptions(TransactionStatus.Captured, null, loadPaymentData, false) { loadBatchData = true }, new Transactions.Transaction.SearchFilters() { BatchID = new List<int>() { ID } });
        }

        public static List<string> GetTerminalNumbers(List<int> debitCompanyIds)
        {
            return (from t in Infrastructure.DataContext.Reader.tblCompanyTransPasses
                    join d in Infrastructure.DataContext.Reader.AuthorizationTransDatas on t.ID equals d.TransPass_id
                    where debitCompanyIds.Contains(t.DebitCompanyID.Value) && t.AuthorizationBatchID == null
                    select t.TerminalNumber).Distinct().ToList();
        }

        private void Save() 
        {
            DataContext.Writer.AuthorizationBatches.Update(_entity, (_entity.AuthorizationBatch_id != 0));
            DataContext.Writer.SubmitChanges();
        }

        private void SetStatus(BatchStatus status) 
        {
            _entity.ActionStatus_id = (byte)status;
            Save();
        }

        public void SetPrepared(string fileName, int transCount, decimal transTotalDebit, decimal transTotalCredit)
        {
            _entity.BatchFileName = fileName;
            _entity.TransCount = transCount;
            _entity.TransTotalDebit = transTotalDebit;
            _entity.TransTotalCredit = transTotalCredit;
            SetStatus(BatchStatus.Prepared);
            DataContext.Writer.ExecuteCommand("Delete From Trans.AuthorizationTransData Where TransPass_id IN(Select ID From tblCompanyTransPass Where AuthorizationBatchID={0})", _entity.AuthorizationBatch_id);
        }

        public void SetSent() { SetStatus(BatchStatus.Sent); }
        public void SetFailSent(string errorDescription) 
        { 
            _entity.ResultDescription = errorDescription; 
            SetStatus(BatchStatus.FailSend); 
        }

        public static Batch Load(List<int> debitCompanyIds, string fileName)
        {
            return DataContext.Reader.AuthorizationBatches.Where(b => debitCompanyIds.Contains(b.TransDebitCompanyID.Value) && b.BatchFileName == fileName).Select(b => new Batch(b)).FirstOrDefault();
        }

        public static Batch Load(int debitCompanyIds, string fileName) 
        {
            return Load(new List<int> { debitCompanyIds }, fileName);
        }

    }
}
