﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll.Process
{
	public class TransactionSource : Infrastructure.BaseDataObject
	{
		private Dal.Netpay.TransSource _entity;
		public byte ID { get { return _entity.TransSource_id; } }
		public string Name { get { return _entity.Name; } }
		//public string NameHeb { get { return _entity.NameHeb; } }
		//public string NameEng { get { return _entity.NameEng; } }
		public string Description { get { return _entity.Description; } }

		private TransactionSource(Dal.Netpay.TransSource entity)
		{
			_entity = entity;
		}

		public static List<TransactionSource> Cache
		{
			get
			{
                return Domain.Current.GetCachData("Process.TransactionSource", () => {
                    return new Domain.CachData((from c in DataContext.Reader.TransSources select new TransactionSource(c)).ToList(), DateTime.MaxValue);
                }) as List<TransactionSource>;
			}
		}

		public static TransactionSource Get(int id)
		{
			return Cache.Where(c => c.ID == id).SingleOrDefault();
		}

	}
}
