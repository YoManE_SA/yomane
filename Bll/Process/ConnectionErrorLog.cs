﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Process
{
	public class ConnectionErrorLog : BaseDataObject
	{
        public const string SecuredObjectName = "ConErrorLog";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Process.Module.Current, SecuredObjectName); } }

        public class RefundLogItem
        {
            public int ID { get; set; }
            public int LogNoConnectionId { get; set; }
            public string Answer { get; set; }
            public DateTime InsertDate { get; set; }
            public string ReplyCode { get; set; }
            public int TransFail { get; set; }
            public string DescriptionOriginal { get; set; }
        }

        public static Dictionary<RefundStatus, System.Drawing.Color> RefundStatusColor = 
            new Dictionary<RefundStatus, System.Drawing.Color> {
                { RefundStatus.New, System.Drawing.ColorTranslator.FromHtml("#6699cc") },
                { RefundStatus.Refunded, System.Drawing.ColorTranslator.FromHtml("#66cc66") },
                { RefundStatus.Never_Charged, System.Drawing.ColorTranslator.FromHtml("#66cc66") },
                { RefundStatus.Failed, System.Drawing.ColorTranslator.FromHtml("#ff6666") },
                { RefundStatus.Void, System.Drawing.ColorTranslator.FromHtml("#999999")}
        };


        public class SearchFilters
		{
			public Infrastructure.Range<int?> ID;
			public Infrastructure.Range<DateTime?> Date;
			public int? MerchanId;
			public string PaymentMethodLast4;
			public string PaymentMethodOwnerName;
			public string Text;
            public List<DebitReturnCodes> DebitReturnCodeValues;
            public List<int> TransactionFailID;
            public DebitCompany? DebitCompany;
            public string TerminalNumber;
            public bool? AttemptedRefund;
            public List<RefundStatus> AutoRefundStatus;
        }

		private Dal.Netpay.tblLog_NoConnection _entity;

		public int ID { get { return _entity.LogNoConnection_id; } }
		public System.DateTime InsertDate { get { return _entity.Lnc_InsertDate; } }
		public DebitReturnCodes? DebitReturnCode
        {
            get
            {
                if (_entity.Lnc_DebitReturnCode == null)
                {
                    return null;
                }
                if (_entity.Lnc_DebitReturnCode.RemoveWhitespaces().Equals(((int)DebitReturnCodes.Code520).ToString()))
                    return DebitReturnCodes.Code520;
                if (_entity.Lnc_DebitReturnCode.RemoveWhitespaces().Equals(((int)DebitReturnCodes.Code521).ToString()))
                    return DebitReturnCodes.Code521;

                return null;
            }
        }
		public string DebitReferenceCode { get { return _entity.Lnc_DebitReferenceCode; } }
		public string MerchantName { get { return _entity.Lnc_companyName; } }
		public int MerchantID { get { return _entity.Lnc_CompanyID; } }
		public int TransactionTypeID { get { return _entity.Lnc_TransactionTypeID; } }
		public decimal Amount { get { return _entity.Lnc_Amount; } }
		public byte Currency { get { return _entity.Lnc_Currency; } }
		public string TerminalNumber { get { return _entity.Lnc_TerminalNumber; } }
		public string IpAddress { get { return _entity.Lnc_IpAddress; } }
		public DebitCompany DebitCompanyID
        {
            get {
                if (!Enum.IsDefined(typeof(DebitCompany), (int)_entity.Lnc_DebitCompany))
                    throw new ArgumentOutOfRangeException("DebitCompany enum is not defined for value " + _entity.Lnc_DebitCompany);
                return (DebitCompany)_entity.Lnc_DebitCompany;
            }
        }

		public int TransactionFailID { get { return _entity.lnc_TransactionFailID; } }
		public string HTTP_Error { get { return _entity.Lnc_HTTP_Error; } }
		public RefundStatus AutoRefundStatus
        {
            get {
                if (!Enum.IsDefined(typeof(RefundStatus), _entity.lnc_AutoRefundStatus))
                    throw new ArgumentOutOfRangeException("RefundStatus enum is not defined for value " + _entity.lnc_AutoRefundStatus);
                return (RefundStatus)_entity.lnc_AutoRefundStatus;
            }
        }
		public System.DateTime AutoRefundDate { get { return _entity.lnc_AutoRefundDate; } }
		public string AutoRefundReply { get { return _entity.lnc_AutoRefundReply; } }

        public string DebitCompanyName
        {
            get
            {
                var debitCompany = Bll.DebitCompanies.DebitCompany.GetCache().Where(i => i.Key == (int)DebitCompanyID).Select(i => i.Value).FirstOrDefault();
                if (debitCompany != null)
                    return debitCompany.Name;

                return null;
            }
        }

		private ConnectionErrorLog(Dal.Netpay.tblLog_NoConnection entity)
		{
			_entity = entity;
		}

        public ConnectionErrorLog()
        {
            _entity = new Dal.Netpay.tblLog_NoConnection();
        }

        private static IQueryable<Dal.Netpay.tblLog_NoConnection> SearchExpression(SearchFilters filters)
        {
            var filterForceMerchant = Accounts.AccountFilter.Current.QueryValidate(Accounts.AccountType.Merchant, (Dal.Netpay.tblLog_NoConnection x) => x.Lnc_CompanyID);
            var filterDebitCompany = Accounts.AccountFilter.Current.QueryValidate(Accounts.AccountType.DebitCompany, (Dal.Netpay.tblLog_NoConnection x) => x.Lnc_DebitCompany);

            var expression = (IQueryable<Dal.Netpay.tblLog_NoConnection>)(from fcl in DataContext.Reader.tblLog_NoConnections
                                                                        .Where(filterForceMerchant)
                                                                         .Where(filterDebitCompany)
                                                                          orderby fcl.Lnc_InsertDate descending
                                                                          select fcl);

            if (filters != null)
            {
                if (filters.ID.From.HasValue) expression = expression.Where(t => t.LogNoConnection_id >= filters.ID.From);
                if (filters.ID.To.HasValue) expression = expression.Where(t => t.LogNoConnection_id <= filters.ID.To);
                if (filters.Date.From.HasValue) expression = expression.Where(t => t.Lnc_InsertDate >= filters.Date.From);
                if (filters.Date.To.HasValue) expression = expression.Where(t => t.Lnc_InsertDate <= filters.Date.To.Value.AlignToEnd());
                if (filters.MerchanId.HasValue) expression = expression.Where(t => t.Lnc_CompanyID == filters.MerchanId.Value);
                if (filters.DebitReturnCodeValues != null && filters.DebitReturnCodeValues.Count > 0) expression = expression.Where(t => filters.DebitReturnCodeValues.Select(i => ((int)i).ToString()).ToList().Contains(t.Lnc_DebitReturnCode));
                if (filters.TransactionFailID != null && filters.TransactionFailID.Count > 0) expression = expression.Where(t => filters.TransactionFailID.Contains(t.lnc_TransactionFailID));
                if (filters.DebitCompany != null) expression = expression.Where(t => t.Lnc_DebitCompany == (byte)filters.DebitCompany.Value);
                if (filters.TerminalNumber != null && filters.TerminalNumber.Length > 0) expression = expression.Where(t => t.Lnc_TerminalNumber == filters.TerminalNumber);
                if (filters.AttemptedRefund != null && (bool)filters.AttemptedRefund) expression = expression.Where(t => t.lnc_AutoRefundStatus > 0);
                if (filters.AttemptedRefund != null && !(bool)filters.AttemptedRefund) expression = expression.Where(t => t.lnc_AutoRefundStatus == 0);
                if (filters.AutoRefundStatus != null && filters.AutoRefundStatus.Count > 0) expression = expression.Where(t => filters.AutoRefundStatus.Contains((RefundStatus)t.lnc_AutoRefundStatus));
            }

            return expression;
        }

        public static List<ConnectionErrorLog> Search(SearchFilters filters, ISortAndPage sortAndPage)
		{
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);

            var expression = SearchExpression(filters);
            var items = expression.ApplySortAndPage(sortAndPage).Select(fcl => new ConnectionErrorLog(fcl)).ToList();
            return items;
		}

        public static Dal.Netpay.tblLog_NoConnection Max()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            //Assuming search expression sorts by InsertDate
            var expression = SearchExpression(null);
            return expression.FirstOrDefault();
        }

        public static int Count(SearchFilters filters)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var expression = SearchExpression(filters);
            return expression.Count();
        }

        public static DateTime GetLastErrorTime()
        {
            return ConnectionErrorLog.Max().Lnc_InsertDate;
        } 

        public static int GetLastSixHoursErrorsCount()
        {
            var sf = new SearchFilters();
            sf.Date.From = DateTime.Now.AddHours(-6);
            return Count(sf);
        }

        public static int GetLastThreeHoursErrorsCount()
        {
            var sf = new SearchFilters();
            sf.Date.From = DateTime.Now.AddHours(-3);
            return Count(sf);
        }

        public static int GetLastHourErrorsCount()
        {
            var sf = new SearchFilters();
            sf.Date.From = DateTime.Now.AddHours(-1);
            return Count(sf);
        }

        public static ConnectionErrorLog Load(int id)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var ret = (from c in DataContext.Reader.tblLog_NoConnections
                       where c.LogNoConnection_id == id
                       select c).SingleOrDefault();
            if (ret == null) return null;
            return new ConnectionErrorLog(ret);
        }

        public Transactions.Transaction Transaction
        {
            get
            {
                if (TransactionFailID <= 0)
                    return null;
                return Transactions.Transaction.GetTransaction(null, TransactionFailID, TransactionStatus.Declined);
            }
        }

        public List<RefundLogItem> GetRefundLog(int transactionId)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            /* 
                The following expression should be identical to:
                    SELECT tblLogDebitRefund.*, DescriptionOriginal FROM tblLogDebitRefund
	                INNER JOIN tblLog_NoConnection ON ldr_LogNoConnection=LogNoConnection_ID
	                LEFT JOIN tblDebitCompanyCode ON ldr_ReplyCode=Code AND lnc_DebitCompany=DebitCompanyID
	                WHERE ldr_TransFail=@TransID ORDER BY ID DESC
            */
            var expression = from ldr in DataContext.Reader.tblLogDebitRefunds
                             join fcl in DataContext.Reader.tblLog_NoConnections
                                on ldr.ldr_LogNoConnection equals fcl.LogNoConnection_id
                             join dcc in DataContext.Reader.tblDebitCompanyCodes
                                on new { ReplyCode = ldr.ldr_ReplyCode , DebitCompanyId = (short)fcl.Lnc_DebitCompany } equals new { ReplyCode = dcc.Code , DebitCompanyId = dcc.DebitCompanyID }
                                into gp
                             from gpi in gp.DefaultIfEmpty()
                             where ldr.ldr_TransFail == transactionId 
                             orderby ID descending
                             select new RefundLogItem()
                                         {
                                                ID = ldr.ID,
                                                LogNoConnectionId = ldr.ldr_LogNoConnection ,
                                                Answer = ldr.ldr_Answer ,
                                                InsertDate = ldr.ldr_InsertDate ,
                                                ReplyCode = ldr.ldr_ReplyCode ,
                                                TransFail = ldr.ldr_TransFail,
                                                DescriptionOriginal = gpi.DescriptionOriginal ?? string.Empty
                                        };

            var result = expression.ToList();

            return result;
        }
    }
}
