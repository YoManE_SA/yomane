﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll.Process
{
	public class GenericLog
	{
		public static void Log(HistoryLogType logType, int? merchantId, string request, string response)
		{
			DataContext.Writer.Log_SpInsertHistory((int)logType, merchantId, null, null, request, response, null, null, null, null);
		}

		public static void Log(CommonTypes.LogHistoryType logType, int? merchantId, string action, string requestData, string responseData, string successCode)
		{
			DataContext.Writer.Log_SpInsertHistory((int)logType, merchantId, action, successCode, requestData, responseData, null, null, null, null);
		}

	}
}
