﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure.VO;
using Netpay.CommonTypes;

namespace Netpay.Bll
{
	public static class MerchantSettelment
	{
		public static List<FinanceSettelmentVO> GetSettelments(Guid credentialsToken, SearchFilters filters, PagingInfo pageInfo)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser, UserType.MerchantPrimary, UserType.MerchantLimited });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var expression = from ap in dc.MerchantSettelments select ap;
			if (filters != null)
			{
				if (filters.merchantIDs != null && filters.merchantIDs.Count > 0)
					expression = expression.Where(ap => filters.merchantIDs.Contains(ap.Merchant_id.Value));
				if (filters.dateFrom != null)
					expression = expression.Where(ap => ap.InsertDate >= filters.dateFrom.Value.MinTime());
				if (filters.dateTo != null)
					expression = expression.Where(ap => ap.InsertDate <= filters.dateTo.Value.MaxTime());
			}
			if (pageInfo != null)
			{
				pageInfo.TotalItems = expression.Count();
				pageInfo.DataFunction = (credToken, pi) => GetSettelments(credToken, filters, pi);
				if (pageInfo.CountOnly) return null;
				expression = expression.Skip(pageInfo.Skip).Take(pageInfo.Take);
			}

			return expression.Select(t => new FinanceSettelmentVO(t)).ToList();
		}

		public static List<SettelmentItemVO> GetSettlmentItems(Guid credentialsToken, int settlmentId)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser, UserType.MerchantPrimary, UserType.MerchantLimited });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			return (from si in dc.MerchantSettelmentItems where si.MerchantSettelment_id == settlmentId select new SettelmentItemVO(si)).ToList();
		}

		public static void DeleteSettlment(Guid credentialsToken, int affiliateSettlmentId)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser, UserType.Partner });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			int? affiliateId = (from p in dc.AffiliateSettelments where p.AffiliateSettelment_id == affiliateSettlmentId select p.Affiliate_id).SingleOrDefault();
			if (affiliateId == null) throw new ApplicationException("settlment does not exist");
			dc.ExecuteCommand("Delete From Finance.MerchantSettelmentItem Where AffiliateSettelment_id={0}", affiliateSettlmentId);
			dc.ExecuteCommand("Update tblTransactionAmount Set SettlementID=Null Where MerchantID={0} And SettlementID={1}", affiliateId.GetValueOrDefault(), affiliateSettlmentId);
			dc.ExecuteCommand("Delete From Finance.MerchantSettelment Where AffiliateSettelment_id={0}", affiliateSettlmentId);
		}

		public class CreateSettlmentOptions {
			
			public int merchantId;
			public int currency;
			public DateTime createDate;
			public int? affiliateId;
			public int? debitCompanyId;
			public bool? payedStatus;
			public bool onlyUpdatetodate;

			public List<int> includeTransactions;
			public List<int> includeInstallmets;

			public List<SettelmentItemVO> customItems;
		}

		public static int CreateSettelment(Guid credentialsToken, CreateSettlmentOptions options)
		{
			User user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser, UserType.Partner });
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			dc.SetupLogger();

			var exp = from e in dc.tblTransactionAmounts where e.MerchantID == options.merchantId && e.Currency == options.currency && e.SettlementID == null select e;
			if (options.affiliateId != null) exp = exp.Where(e => e.Affiliates_id == options.affiliateId);
			if (options.debitCompanyId != null) exp = exp.Where(e => e.DebitCompany_id == options.debitCompanyId);
			if (options.onlyUpdatetodate) exp = exp.Where(e => e.SettlementDate <= options.createDate);
			if (options.payedStatus != null)
			{
				exp = exp.Where(e => (from ee in dc.tblLogImportEPAs where ee.TransID == e.TransPassID && ee.Installment == e.Installment select ee.TransID).Any());
			}

			var IDs = (from e in exp select e.ID).ToArray();
			if (IDs.Length == 0) return 0;

			Netpay.Dal.Netpay.MerchantSettelment afp = null;
			if (afp == null)
			{
				afp = new Netpay.Dal.Netpay.MerchantSettelment();
				dc.MerchantSettelments.InsertOnSubmit(afp);
				afp.InsertDate = DateTime.Now;
				//afp.settlmentDate = createDate;
				afp.Merchant_id = options.merchantId;
				afp.Currency = options.currency;
				afp.DebitCompany_id = options.debitCompanyId;
				afp.DebitCompany_id = options.debitCompanyId;
			}
			else
			{
				dc.ExecuteCommand("Delete From Finance.AffiliateSettelmentItem Where AffiliateSettelment_id={0}", afp.MerchantSettelment_id);
			}
			dc.SubmitChanges();

			dc.ExecuteCommand(string.Format("Update tblTransactionAmount Set SettlementID={0} Where ID IN({1})", afp.MerchantSettelment_id, string.Join(",", IDs)));

			var expGroup = from e in dc.tblTransactionAmounts
						   where e.MerchantID == options.merchantId && e.SettlementID == afp.MerchantSettelment_id
						   group e by new { e.TypeID, e.PercentValue } into g
						   select new { KeyPercent = g.Key.PercentValue, KeyTypeID = g.Key.TypeID, Value = new CountAmount(g.Sum(e => e.Amount), g.Count()) };
			var totals = expGroup.ToList();

			var items = new List<MerchantSettelmentItem>();
			foreach (var t in totals)
			{
				var pl = new MerchantSettelmentItem();
				items.Add(pl);
				pl.MerchantSettelment_id = afp.MerchantSettelment_id;
				pl.TransAmountType_id = t.KeyTypeID;
				if (t.KeyPercent == null) pl.ItemText = string.Format("For {0}", t.KeyTypeID.ToString());
				else pl.ItemText = string.Format("For {0} in {1}%", t.KeyTypeID.ToString(), t.KeyPercent.GetValueOrDefault().ToString("0.00"));
				pl.Quantity = t.Value.Count;
				pl.Amount = t.Value.Amount;
			}

			//if (options.takeRollingReserve) {
				//get roliong 
			//}

			foreach (var item in options.customItems)
			{
				var pl = new MerchantSettelmentItem();
				items.Add(pl);
				pl.MerchantSettelment_id = afp.MerchantSettelment_id;
				pl.TransAmountType_id = null;
				pl.ItemText = item.ItemText;
				pl.Quantity = item.Quantity;
				pl.Amount = item.Amount;
				pl.Total = item.Total;
			}

			afp.Amount = items.Sum(t => t.Total);

			dc.MerchantSettelmentItems.InsertAllOnSubmit(items);
			dc.SubmitChanges();

			return afp.MerchantSettelment_id;
		}

	}
}
