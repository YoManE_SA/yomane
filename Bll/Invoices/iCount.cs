﻿using System;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Invoices
{
	public static class iCount
	{
		private const string ServiceUrl = "https://www.icount.co.il/api/create_doc.php";
		private static Encoding ServiceEncoding = Encoding.UTF8; 

		private static int mapCurrency(Netpay.CommonTypes.Currency currency)
		{
			switch(currency){
				case CommonTypes.Currency.EUR: return 1;
				case CommonTypes.Currency.USD: return 2;
				case CommonTypes.Currency.JPY: return 3;
				case CommonTypes.Currency.GBP: return 4;
				case CommonTypes.Currency.ILS: return 5;
				//case CommonTypes.Currency.SGP: return 6;
				case CommonTypes.Currency.CAD: return 7;
				case CommonTypes.Currency.RUB: return 8;
				case CommonTypes.Currency.NZD: return 9;
				case CommonTypes.Currency.AUD: return 10;
				//case CommonTypes.Currency.KES: return 11;
				//case CommonTypes.Currency.BRL: return 12;
			}
			return 0;
		}

		public static int CreateInvoice(Transactions.Transaction ctp, MerchantSettings csi)
		{
			//Infrastructure.Domain domain = Infrastructure.Domain.Get(domainHost);
			string retValue, genType = null;
			StringBuilder sendParams = new StringBuilder();
			var invData = InvoiceCreator.GetInvoiceData(ctp, csi);
			if (csi.IsCreateInvoice || csi.IsCreateReceipt)
			{
				if (csi.IsCreateInvoice && csi.IsCreateReceipt) genType = "invrec";
				else if (csi.IsCreateInvoice) genType = (ctp.CreditType == (int)CreditType.Refund) ? "refund" : "invoice";
				else if (csi.IsCreateReceipt) genType = "receipt";
			}
			if (genType == null) throw new ApplicationException("iCount does not support this transType");
			sendParams.Append("compID=" + System.Web.HttpUtility.UrlEncode(csi.ExternalUserID, ServiceEncoding));
			sendParams.Append("&user=" + System.Web.HttpUtility.UrlEncode(csi.ExternalUserName, ServiceEncoding));
			sendParams.Append("&pass=" + System.Web.HttpUtility.UrlEncode(csi.ExternalPassword, ServiceEncoding));
			sendParams.Append("&dateissued=" + System.Web.HttpUtility.UrlEncode(DateTime.Now.ToString("yyyMMdd")));
			sendParams.Append("&clientname=" + System.Web.HttpUtility.UrlEncode(ctp.PayerData.FirstName));
			if (ctp.PaymentData != null && ctp.PaymentData.BillingAddress != null)
			{
				sendParams.Append("&client_street=" + System.Web.HttpUtility.UrlEncode(ctp.PaymentData.BillingAddress.AddressLine1));
				sendParams.Append("&client_street_number=" + System.Web.HttpUtility.UrlEncode(ctp.PaymentData.BillingAddress.AddressLine2));
				sendParams.Append("&client_city=" + System.Web.HttpUtility.UrlEncode(ctp.PaymentData.BillingAddress.City));
				sendParams.Append("&client_country=" + System.Web.HttpUtility.UrlEncode(Country.Get(ctp.PaymentData.BillingAddress.CountryISOCode).Name));
				sendParams.Append("&client_zip=" + System.Web.HttpUtility.UrlEncode(ctp.PaymentData.BillingAddress.PostalCode));
			}

			sendParams.Append("&sanity_string=" + System.Web.HttpUtility.UrlEncode(ctp.ID.ToString(), ServiceEncoding));
			sendParams.Append("&docType=" + System.Web.HttpUtility.UrlEncode(genType, ServiceEncoding));
			sendParams.Append("&currency=" + System.Web.HttpUtility.UrlEncode(mapCurrency(invData.Curreny).ToString(), ServiceEncoding));
			sendParams.Append("&rate=" + System.Web.HttpUtility.UrlEncode(invData.ConvertionRate.ToString("0.000")));
			sendParams.Append("&hwc=" + System.Web.HttpUtility.UrlEncode(string.Format("{0}, ID:{1}", ctp.Comment, ctp.ID)));
			
			if (genType == "receipt") {
				sendParams.Append("&ammountb4nicui=" + System.Web.HttpUtility.UrlEncode(invData.Amount_ILS.ToString("0.00"), ServiceEncoding));
				sendParams.Append("&nicui=" + System.Web.HttpUtility.UrlEncode("0", ServiceEncoding));
				sendParams.Append("&totalamount=" + System.Web.HttpUtility.UrlEncode(invData.Amount_ILS.ToString("0.00"), ServiceEncoding));
			} else if (genType == "invrec") {
				sendParams.Append("&totalpaid=" + System.Web.HttpUtility.UrlEncode(invData.Amount_ILS.ToString("0.00"), ServiceEncoding));
				sendParams.Append("&paid=" + System.Web.HttpUtility.UrlEncode(invData.Amount_ILS.ToString("0.00"), ServiceEncoding));
			}

			if (genType == "invoice" || genType == "invrec" || genType == "refund")
			{
				sendParams.Append("&desc[0]=" + System.Web.HttpUtility.UrlEncode(invData.InvoiceItemName, ServiceEncoding));
				sendParams.Append("&quantity[0]=1");
				sendParams.Append("&unitprice[0]=" + System.Web.HttpUtility.UrlEncode(invData.NoTaxAmount.ToString("0.00"), ServiceEncoding));
				sendParams.Append("&totalsum=" + System.Web.HttpUtility.UrlEncode(invData.NoTaxAmount_ILS.ToString("0.00"), ServiceEncoding));
				sendParams.Append("&totalvat=" + System.Web.HttpUtility.UrlEncode(invData.TaxAmount_ILS.ToString("0.00"), ServiceEncoding));
				sendParams.Append("&totalwithvat=" + System.Web.HttpUtility.UrlEncode(invData.Amount_ILS.ToString("0.00"), ServiceEncoding));
				if (!invData.IncludeTax) sendParams.Append("&taxexempt=1");
				sendParams.Append("&maampercent=" + System.Web.HttpUtility.UrlEncode(invData.TaxRate.ToString("0.00"), ServiceEncoding));
			}

			if (genType == "receipt" || genType == "invrec")
			{
				if (ctp.PaymentData.MethodInstance.PaymentMethodType == CommonTypes.PaymentMethodType.CreditCard)
				{
					sendParams.Append("&credit=1");
					switch ((Infrastructure.CreditType)ctp.CreditType) {
					case CreditType.Installments: sendParams.Append("&cc_paymenttype=" + System.Web.HttpUtility.UrlEncode("2", ServiceEncoding)); break;
					case CreditType.CreditCharge: sendParams.Append("&cc_paymenttype=" + System.Web.HttpUtility.UrlEncode("3", ServiceEncoding)); break;
					default: sendParams.Append("&cc_paymenttype=" + System.Web.HttpUtility.UrlEncode("1", ServiceEncoding)); break;
					}
					sendParams.Append("&cc_cardtype=" + System.Web.HttpUtility.UrlEncode(ctp.PaymentMethodName, ServiceEncoding));
					sendParams.Append("&cctotal=" + System.Web.HttpUtility.UrlEncode(invData.Amount.ToString("0.00").ToString(), ServiceEncoding));
					sendParams.Append("&ccfirstpayment=" + System.Web.HttpUtility.UrlEncode(ctp.FirstInstallment.ToString("0.00").ToString(), ServiceEncoding));
					sendParams.Append("&cc_numofpayments=" + System.Web.HttpUtility.UrlEncode(ctp.Installments.ToString(), ServiceEncoding));
					sendParams.Append("&cc_peraondate=" + System.Web.HttpUtility.UrlEncode(DateTime.Now.ToString(), ServiceEncoding));
					sendParams.Append("&cc_shovar=" + System.Web.HttpUtility.UrlEncode(ctp.ApprovalNumber, ServiceEncoding));
					sendParams.Append("&cc_cardnumber=" + System.Web.HttpUtility.UrlEncode(ctp.PaymentData.MethodInstance.Value1Last4, ServiceEncoding));
					sendParams.Append("&cc_holdername=" + System.Web.HttpUtility.UrlEncode(ctp.PayerData.FullName, ServiceEncoding));
					
				}
				else if (ctp.PaymentData.MethodInstance.PaymentMethodType == CommonTypes.PaymentMethodType.ECheck)
				{
					sendParams.Append("&cheque=1");
					sendParams.Append("&chequeDateY=" + System.Web.HttpUtility.UrlEncode(DateTime.Now.Year.ToString(), ServiceEncoding));
					sendParams.Append("&chequeDateM=" + System.Web.HttpUtility.UrlEncode(DateTime.Now.Month.ToString(), ServiceEncoding));
					sendParams.Append("&chequeDateD=" + System.Web.HttpUtility.UrlEncode(DateTime.Now.Day.ToString(), ServiceEncoding));
					sendParams.Append("&chequeNumber=" + System.Web.HttpUtility.UrlEncode(ctp.PaymentMethodDisplay, ServiceEncoding));
					//sendParams.Append("&chequeSnif=" + System.Web.HttpUtility.UrlEncode(ctp.PaymentData.BankCity, ServiceEncoding));
					sendParams.Append("&chequeBank=" + System.Web.HttpUtility.UrlEncode(ctp.PaymentData.IssuerCountryIsoCode, ServiceEncoding));
					sendParams.Append("&chequeTotal=" + System.Web.HttpUtility.UrlEncode(invData.Amount.ToString("0.00").ToString(), ServiceEncoding));
				}
				else if (ctp.PaymentData.MethodInstance.PaymentMethodType == CommonTypes.PaymentMethodType.BankTransfer)
				{
					sendParams.Append("&banktransfer=1");
					sendParams.Append("&peraonDateBT=" + System.Web.HttpUtility.UrlEncode(DateTime.Now.ToString(), ServiceEncoding));
					sendParams.Append("&depositedTo=" + System.Web.HttpUtility.UrlEncode(ctp.PaymentMethodDisplay, ServiceEncoding));
					sendParams.Append("&totalBT=" + System.Web.HttpUtility.UrlEncode(invData.Amount.ToString("0.00").ToString(), ServiceEncoding));
				}
				sendParams.Append("&cash=0");
			}

			sendParams.Append("&sendOrig=" + System.Web.HttpUtility.UrlEncode(ctp.PayerData.EmailAddress, ServiceEncoding));
			sendParams.Append("&show_response=1");
			//sendParams.Append("&debug=yes"); 

			System.Net.HttpStatusCode hsc = Infrastructure.HttpClient.SendHttpRequest(ServiceUrl, out retValue, sendParams.ToString(), ServiceEncoding);
			InvoiceCreator.AddInvoiceLog(ctp.MerchantID.GetValueOrDefault(), "iCount create document", null, sendParams.ToString(), retValue);
			if (hsc == System.Net.HttpStatusCode.OK)
			{
				retValue = retValue.Replace("\n", "&");
				int docNumber = Infrastructure.HttpClient.GetQueryStringValue(retValue, "DOCNUM_RESULT").ToInt32();
				if (docNumber != 0)
				{
					InvoiceCreator.AddTransInvoiceNumber(ctp, docNumber, "iCount", Infrastructure.HttpClient.GetQueryStringValue(retValue, "EMAIL_LINK"));
					return docNumber;
				}
				else throw new System.Net.WebException(string.Format("Create failed, error- request={0}, response={1}", sendParams, retValue));
			}
			throw new System.Net.WebException(string.Format("Unable to connect:{0}, return:{1}", sendParams, retValue));
		}
	}
}
