﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Netpay.Infrastructure;

namespace Netpay.Bll.Invoices
{
    public class InvoiceLine : BaseDataObject
    {
        public class SearchFilters
        {
            public int? ID;
            public int? DocumentID;
            public int? MerchantID;
            public int? TransactionPayID;
            public bool? WithoutDocument;
            public string BillToName;
        }

        private Dal.Netpay.tblInvoiceLine _entity;

        public int ID { get { return _entity.ID; } }

        public int? DocumentID { get { return _entity.il_DocumentID; } }
            
        public DateTime Date { get { return _entity.il_Date; } }
        
        public int TranasctionPayID { get { return _entity.il_TransactionPayID; } }

        public string UserName { get { return _entity.il_Username; } }

        public string BillToName { get { return _entity.il_BillToName; } }

        public int MerchantID { get { return _entity.il_MerchantID; } }

        public string Text { get { return _entity.il_Text; } }

        public float Quantity { get { return _entity.il_Quantity; } }

        public decimal Price { get { return _entity.il_Price; } }

        public int Currency { get { return _entity.il_Currency; } }

        public decimal CurrecnyRate { get { return _entity.il_CurrencyRate; } }

        public decimal? DocumentCurrencyRate { get; set; }

        public decimal? Amount { get { return _entity.il_Amount; } }

        private InvoiceLine(Dal.Netpay.tblInvoiceLine entity)
        {
            _entity = entity;
        }

        public static void Delete(int id)
        {
            DataContext.Writer.tblInvoiceLines.DeleteAllOnSubmit(DataContext.Writer.tblInvoiceLines.Where(x => x.ID == id));
            DataContext.Writer.SubmitChanges();
        }

        public InvoiceLine()
        {
            _entity = new Dal.Netpay.tblInvoiceLine();
        }

        public static List<InvoiceLine> Search(SearchFilters filters)
        {
            var expression = (from InvoiceLines in DataContext.Reader.tblInvoiceLines
                              join InvoiceDocuments in DataContext.Reader.tblInvoiceDocuments on InvoiceLines.il_DocumentID equals InvoiceDocuments.ID into LinesAndDocuments
                              join Currencies in DataContext.Reader.CurrencyLists on InvoiceLines.il_Currency equals Currencies.CurrencyID into IncvoicesAndCurrency
                              from LinesAndDocumentsIncludeEmpty in LinesAndDocuments.DefaultIfEmpty()
                              select new { InvoiceLines, CurrencyRate = LinesAndDocumentsIncludeEmpty.id_CurrencyRate });

            if (filters != null)
            {
                if (filters.ID.HasValue) expression = expression.Where(x => x.InvoiceLines.ID == filters.ID.Value);
                if (filters.DocumentID.HasValue) expression = expression.Where(x => x.InvoiceLines.il_DocumentID == filters.DocumentID);
                if (filters.WithoutDocument.HasValue) expression = expression.Where(x => x.InvoiceLines.il_DocumentID == null);
                if (filters.MerchantID.HasValue) expression = expression.Where(x => x.InvoiceLines.il_MerchantID == filters.MerchantID.Value);
                if (!string.IsNullOrEmpty(filters.BillToName)) expression = expression.Where(x => x.InvoiceLines.il_BillToName == filters.BillToName.Trim());
            }

            return expression.Select(x => new InvoiceLine(x.InvoiceLines) { DocumentCurrencyRate = x.CurrencyRate }).ToList();
        }

        //Function that implements stored procedure dbo.InvoiceGetUnattendedLines
        public static List<UnattendedLines> InvoiceGetUnattendedLines(int? nMerchant, string sBillTo, int nCurrency)
        {
            Currency reqestedCurrency = Bll.Currency.Get(nCurrency);
            if (reqestedCurrency == null) throw new Exception("requested currecny was not found.");

            decimal? requestedCurrencyBaseRate = (from currencies in DataContext.Reader.CurrencyLists
                                                  where currencies.CurrencyID == nCurrency
                                                  select currencies.BaseRate)
                                                  .SingleOrDefault();

            var sf = new SearchFilters();
            sf.WithoutDocument = true;
            sf.MerchantID = nMerchant;
            sf.BillToName = sBillTo;

            var result = (from invoiceLines in Search(sf) join currency in DataContext.Reader.CurrencyLists
                            on invoiceLines.Currency equals currency.CurrencyID
                            orderby invoiceLines.ID ascending
                         select new UnattendedLines()
                         {
                             Line = invoiceLines,
                             CUR_ISOName = currency.CurrencyISOCode,
                             CUR_BaseRate = currency.BaseRate.HasValue ? currency.BaseRate.Value.ToNullableDecimal() : null,
                             AmountConverted = currency.BaseRate.HasValue && requestedCurrencyBaseRate.HasValue ? (((decimal)invoiceLines.Quantity * invoiceLines.Price * currency.BaseRate.Value) / requestedCurrencyBaseRate.Value).ToNullableDecimal() : null
                         })
                         .ToList();

            return result;
        }

        public class UnattendedLines
        {
            public InvoiceLine Line { get; set; }
            public string CUR_ISOName { get; set; }
            public decimal? CUR_BaseRate { get; set; }
            public decimal? AmountConverted { get; set; }
        }
       
    }
}
