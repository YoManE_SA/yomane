﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Invoices
{
	public class MerchantSettings : BaseDataObject
	{
		public const string SecuredObjectName = "MerchantSettings";
		public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(MaxMind.Module.Current, SecuredObjectName); } }

		private Dal.Netpay.SetMerchantInvoice _entity;
		public int ID { get { return _entity.SetMerchantInvoice_id; } }
		public int Merchant_id { get { return _entity.Merchant_id; } }
		public bool IsEnable { get { return _entity.IsEnable; } set { _entity.IsEnable = value; } }
		public byte ExternalProviderID { get { return _entity.ExternalProviderID; } set { _entity.ExternalProviderID = value; } }
        public string ExternalProviderName { get; set; }
        public string ExternalUserName { get { return _entity.ExternalUseName; } set { _entity.ExternalUseName = value; } }
		public string ExternalUserID { get { return _entity.ExternalUserID; } set { _entity.ExternalUserID = value; } }
		public string ExternalPassword { get { return _entity.ExternalPassword; } set { _entity.ExternalPassword = value; } }

		public string ItemText { get { return _entity.ItemText; } set { _entity.ItemText = value; } }
		public bool IsAutoGenerateILS { get { return _entity.IsAutoGenerateILS; } set { _entity.IsAutoGenerateILS = value; } }
		public bool IsAutoGenerateOther { get { return _entity.IsAutoGenerateOther; } set { _entity.IsAutoGenerateOther = value; } }
		public bool IsAutoGenerateRefund { get { return _entity.IsAutoGenerateRefund; } set { _entity.IsAutoGenerateRefund = value; } }
		public bool IsIncludeTax { get { return _entity.IsIncludeTax; } set { _entity.IsIncludeTax = value; } }
		public bool IsCreateInvoice { get { return _entity.IsCreateInvoice; } set { _entity.IsCreateInvoice = value; } }
		public bool IsCreateReceipt { get { return _entity.IsCreateReceipt; } set { _entity.IsCreateReceipt = value; } }

		private MerchantSettings(Netpay.Dal.Netpay.SetMerchantInvoice entity)
			
		{ 
			_entity = entity;
		}

		public MerchantSettings(int merchantId)
			
		{
			_entity = new Dal.Netpay.SetMerchantInvoice();
			_entity.Merchant_id = merchantId;
		}

		public void Save()
		{
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Edit);
			DataContext.Writer.SetMerchantInvoices.Update(_entity, (_entity.SetMerchantInvoice_id != 0));
			DataContext.Writer.SubmitChanges();
		}

		public static MerchantSettings Load(int merchantId)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);
			Accounts.AccountFilter.Current.Validate(Accounts.AccountType.Merchant, merchantId);

            var entity = (from lu in DataContext.Reader.SetMerchantInvoices
                          join provider in DataContext.Reader.InvoiceProviders on lu.ExternalProviderID equals provider.InvoiceProvider_id into luAndprovider
                          from luAndProvider in luAndprovider.DefaultIfEmpty()
                          where lu.Merchant_id == merchantId
                          select new { settings = lu, providerName = luAndProvider.Name })
                          .SingleOrDefault();

            if (entity == null) return null;

            MerchantSettings settings = new MerchantSettings(entity.settings);
            settings.ExternalProviderName = entity.providerName;

            return settings;
        }

	}
}
