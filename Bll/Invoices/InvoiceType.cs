﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Bll.Invoices
{
    public class InvoiceType : BaseDataObject
    {
        private Dal.Netpay.InvoiceType _entity;

        public int ID { get {return _entity.InvoiceType_id; } }

        public string Text { get { return _entity.Name; } }

        private InvoiceType(Dal.Netpay.InvoiceType entity)
        {
            _entity = entity;
        }

        public static List<InvoiceType> Cache
        {
            get
            {
                return Domain.Current.GetCachData("InvoiceType", () =>
                {
                    return new Domain.CachData((from it in DataContext.Reader.InvoiceTypes
                                                select new InvoiceType(it)).ToList(), DateTime.Now.AddHours(6));
                }) as List<InvoiceType>;
            }
        }

        public static string GetInvoiceTypeText(int id)
        {
            return Cache.Where(x => x.ID == id).FirstOrDefault().Text;
        }
    }
}
