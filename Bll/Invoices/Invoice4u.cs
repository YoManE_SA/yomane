﻿using System;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Invoices
{
	public static class Invoice4u
	{
        private const string ServiceUrl = "https://account.invoice4u.co.il/Public/";
        private static Encoding ServiceEncoding = Encoding.GetEncoding("Windows-1255");
		private static System.Collections.Generic.Dictionary<string, int> _currencies;

		private static int? getCurrencyID(string currencyIso)
		{
			if (_currencies == null)
			{
				System.Data.DataSet ds = new System.Data.DataSet();
                ds.ReadXml(ServiceUrl + "w_currency.asmx/GetList");
				_currencies = new System.Collections.Generic.Dictionary<string, int>();
				foreach (System.Data.DataRow r in ds.Tables[0].Rows)
					_currencies.Add((string)r["Var"], ((string)r["Value"]).ToInt32());
				ds.Dispose();
			}
			int retValue;
			if (!_currencies.TryGetValue(currencyIso, out retValue)) return null;
			return retValue;
		}

        public static int CreateInvoice(Transactions.Transaction ctp, MerchantSettings csi)
		{
            string retValue, genType = null;
            StringBuilder sendParams = new StringBuilder();
			var invData = InvoiceCreator.GetInvoiceData(ctp, csi);
            if (csi.IsCreateInvoice || csi.IsCreateReceipt) {
                if (csi.IsCreateInvoice && csi.IsCreateReceipt) genType = "IR:CreateWithPaymentMethods3";
                else if (csi.IsCreateInvoice) genType = "I:CREATE102";
                else if (csi.IsCreateReceipt) genType = "R:CreateWithPaymentMethods";
            }
            if (genType == null) throw new ApplicationException("Invoice4u does not support this transType");
            sendParams.Append("TransType=" + System.Web.HttpUtility.UrlEncode(genType, ServiceEncoding));
			sendParams.Append("&Username=" + System.Web.HttpUtility.UrlEncode(csi.ExternalUserName, ServiceEncoding));
            sendParams.Append("&Key=" + System.Web.HttpUtility.UrlEncode(csi.ExternalPassword, ServiceEncoding));
            if (genType == "R:CreateWithPaymentMethods") {
			    sendParams.Append("&ReceiptSubject=" + System.Web.HttpUtility.UrlEncode(ctp.PayerData.FullName, ServiceEncoding));
    			sendParams.Append("&ReceiptComments=" + System.Web.HttpUtility.UrlEncode(ctp.Comment, ServiceEncoding));
                sendParams.Append("&CompanyCode=" + System.Web.HttpUtility.UrlEncode("99999", ServiceEncoding));
            } else if (genType == "IR:CreateWithPaymentMethods3" || genType == "I:CREATE102") {
				sendParams.Append("&InvoiceSubject=" + System.Web.HttpUtility.UrlEncode(ctp.PayerData.FullName, ServiceEncoding));
			    sendParams.Append("&InvoiceItemCode=000");
				sendParams.Append("&InvoiceItemDescription=" + System.Web.HttpUtility.UrlEncode(invData.InvoiceItemName, ServiceEncoding));
			    sendParams.Append("&InvoiceItemQuantity=1");
			    sendParams.Append("&InvoiceItemPrice=" + System.Web.HttpUtility.UrlEncode(invData.NoTaxAmount_ILS.ToString("0.00"), ServiceEncoding));
			    sendParams.Append("&InvoiceDiscount=0");
    			sendParams.Append("&InvoiceComments=" + System.Web.HttpUtility.UrlEncode(ctp.Comment, ServiceEncoding));
                if (ctp.CurrencyID != (int)CommonTypes.Currency.ILS)
				    sendParams.Append("&InvoiceTaxRate=0");
            }
			sendParams.Append("&CompanyInfo=" + System.Web.HttpUtility.UrlEncode(ctp.PayerData.FullName, ServiceEncoding));
            if (genType == "R:CreateWithPaymentMethods" || genType == "IR:CreateWithPaymentMethods3") {
				if (ctp.PaymentData.MethodInstance.PaymentMethodType == CommonTypes.PaymentMethodType.CreditCard)
				{
				    sendParams.Append("&CcDate=" + System.Web.HttpUtility.UrlEncode(DateTime.Now.ToString(), ServiceEncoding));
					sendParams.Append("&CcNumber=" + System.Web.HttpUtility.UrlEncode(ctp.PaymentData.MethodInstance.Value1Last4, ServiceEncoding));
					sendParams.Append("&CcExpiration=" + ctp.PaymentData.MethodInstance.ExpirationDate.Value.ToString("MMyy"));
					sendParams.Append("&CcType=" + System.Web.HttpUtility.UrlEncode(ctp.PaymentMethodName, ServiceEncoding));
					sendParams.Append("&CcAmount=" + System.Web.HttpUtility.UrlEncode(invData.Amount_ILS.ToString("0.00").ToString(), ServiceEncoding));
				}
				else if (ctp.PaymentData.MethodInstance.PaymentMethodType == CommonTypes.PaymentMethodType.ECheck)
				{
				    sendParams.Append("&CheckDate=" + System.Web.HttpUtility.UrlEncode(DateTime.Now.ToString(), ServiceEncoding));
				    sendParams.Append("&CheckAccount=" + System.Web.HttpUtility.UrlEncode(ctp.PaymentMethodDisplay, ServiceEncoding));
					sendParams.Append("&CheckBranch=" + System.Web.HttpUtility.UrlEncode(ctp.PaymentData.IssuerCountryIsoCode, ServiceEncoding));
					//sendParams.Append("&CheckBank=" + System.Web.HttpUtility.UrlEncode(ctp.PaymentData.BankName, ServiceEncoding));
					sendParams.Append("&CheckAmount=" + System.Web.HttpUtility.UrlEncode(invData.Amount_ILS.ToString("0.00").ToString(), ServiceEncoding));
				}
				else if (ctp.PaymentData.MethodInstance.PaymentMethodType == CommonTypes.PaymentMethodType.BankTransfer)
				{
				    sendParams.Append("&TransDate=" + System.Web.HttpUtility.UrlEncode(DateTime.Now.ToString(), ServiceEncoding));
				    sendParams.Append("&TransAccount=" + System.Web.HttpUtility.UrlEncode(ctp.PaymentMethodDisplay, ServiceEncoding));
					sendParams.Append("&TransBranch=" + System.Web.HttpUtility.UrlEncode(ctp.PaymentData.IssuerCountryIsoCode, ServiceEncoding));
					//sendParams.Append("&TransBank=" + System.Web.HttpUtility.UrlEncode(ctp.PaymentData.BankName, ServiceEncoding));
					sendParams.Append("&TransAmount=" + System.Web.HttpUtility.UrlEncode(invData.Amount_ILS.ToString("0.00").ToString(), ServiceEncoding));
			    }
			    sendParams.Append("&Cash=0");
            }
            sendParams.Append("&UniqueIdentifier=" + System.Web.HttpUtility.UrlEncode(ctp.ID.ToString(), ServiceEncoding));
            sendParams.Append("&MailTo=" + System.Web.HttpUtility.UrlEncode(ctp.PayerData.EmailAddress, ServiceEncoding));
            System.Net.HttpStatusCode hsc = Infrastructure.HttpClient.SendHttpRequest(ServiceUrl + "HttpPost.aspx", out retValue, sendParams.ToString(), ServiceEncoding);
			InvoiceCreator.AddInvoiceLog(ctp.MerchantID.GetValueOrDefault(), "Invoice4u create document", null, sendParams.ToString(), retValue);
            if (hsc == System.Net.HttpStatusCode.OK)
			{
				retValue = retValue.Replace("<br", "<BR").Replace("<BR>", "<BR/>").Replace("<BR/>", "&").Replace(":", "=");
				string responseCode = Infrastructure.HttpClient.GetQueryStringValue(retValue, "ResponseCode");
				if (responseCode == "100") {
                    int docNumber = Infrastructure.HttpClient.GetQueryStringValue(retValue, "DocNumber").ToInt32();
					InvoiceCreator.AddTransInvoiceNumber(ctp, docNumber, "Invoice4u", Infrastructure.HttpClient.GetQueryStringValue(retValue, "DocURL"));
					return docNumber;
                } else throw new System.Net.WebException(string.Format("Create failed, error code:{0}, request={1}, response={2}", responseCode, sendParams, retValue));
			}
            throw new System.Net.WebException(string.Format("Unable to connect:{0}, return:{1}", sendParams, retValue));
		}
	}
}
