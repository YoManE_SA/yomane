﻿using System;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Invoices
{
	public static class Tamal
	{
		private const string ServiceUrl = "https://cloud.tamal.co.il/"; //"http://212.143.181.190/TiuditWeb/"; 
        private static System.Text.Encoding ServiceEncoding = System.Text.Encoding.UTF8;
        private static System.Collections.Generic.Dictionary<CommonTypes.PaymentMethodEnum, int> _paymentMethodMap;
		private static System.Collections.Generic.Dictionary<CommonTypes.Currency, int> _currencies;

        private class Doc300VO
        {
            public int EsekNum;
            public int ClientNumber;
            public short TypeCode;
            public string DocNumber;
            public string ClientName;
            public string ClientAddress;
            public string ClientCityZip;
            public string ClientOsekNum;
            public string ProducedDateString;
            public decimal SumBeforeDiscount;
            public decimal DiscountAmount;
            public decimal DiscountPercentage;
            public decimal TotalBeforeMaam;
            public decimal DocMaamAmount;
            public decimal DocTotalDue;
            public decimal DocNikuyBamakorSum;
            public string DocUserFullName;
            public string DocDetail;
            public byte DocNotMaam;
            public decimal MaamRate;
            public string GeneralRemark;
            public string DocTypeRemark;
            public string DocProductsXML;
            public string DocChargesXML;
            public string DocClosureXML;
            public byte DocIsSourceProduced;
            public byte DocSigned;
        }

        public static int mapPaymentMethod(CommonTypes.PaymentMethodEnum pm)
        {
            if(_paymentMethodMap == null) {
                _paymentMethodMap = new System.Collections.Generic.Dictionary<CommonTypes.PaymentMethodEnum, int>();
                _paymentMethodMap.Add(Netpay.CommonTypes.PaymentMethodEnum.CCIsracard, 1); //Isracart
                _paymentMethodMap.Add(Netpay.CommonTypes.PaymentMethodEnum.CCMastercard, 1); //Isracart
                _paymentMethodMap.Add(Netpay.CommonTypes.PaymentMethodEnum.CCJcb, 1); //Isracart
                _paymentMethodMap.Add(Netpay.CommonTypes.PaymentMethodEnum.CCDirect, 1); //Isracart
                _paymentMethodMap.Add(Netpay.CommonTypes.PaymentMethodEnum.CCVisa, 2); //CAL
                _paymentMethodMap.Add(Netpay.CommonTypes.PaymentMethodEnum.CCDiners, 3); //Diners
                _paymentMethodMap.Add(Netpay.CommonTypes.PaymentMethodEnum.CCAmex, 4); //AMEX
                //_paymentMethodMap.Add(Netpay.CommonTypes.PaymentMethod.CCVisa, 5); //Leumi Card
            }
            if (_paymentMethodMap.ContainsKey(pm)) return _paymentMethodMap[pm];
            return 0;
        }

        public static int mapCurrency(string token, Netpay.CommonTypes.Currency currency)
        {
			if(_currencies == null) {
				string retValue;
				string sendParams = string.Format("CredentialsToken={0}", token.ToEncodedUrl(ServiceEncoding));
				System.Net.HttpStatusCode hsc = Infrastructure.HttpClient.SendHttpRequest(ServiceUrl + "GetSupportedCurrenciesList/", out retValue, sendParams, ServiceEncoding);
				if (hsc != System.Net.HttpStatusCode.OK) throw new Exception("Unable to fetch Tamal curency list");
				object[] values = (new System.Web.Script.Serialization.JavaScriptSerializer()).DeserializeObject(retValue) as object[];
				_currencies = new System.Collections.Generic.Dictionary<CommonTypes.Currency, int>();
				foreach (System.Collections.Generic.Dictionary<string, object> o in values){
					string isoName = o["ISOCode"].ToString();
					if (isoName.ToUpper() == "NIS") isoName = "ILS";
					if (Currency.Get(isoName) != null)
						_currencies.Add(Currency.Get(isoName).EnumValue, o["ID"].ToString().ToInt32());
				}
			}
			return _currencies[currency];
		}

        public static void Login(MerchantSettings csi, out string token) 
        { 
            string retValue;
            string sendParams = string.Format("userName={0}&userPass={1}", csi.ExternalUserName.ToEncodedUrl(ServiceEncoding), csi.ExternalPassword.ToEncodedUrl(ServiceEncoding));
            System.Net.HttpStatusCode hsc = Infrastructure.HttpClient.SendHttpRequest(ServiceUrl + "GetToken/", out retValue, sendParams, ServiceEncoding);
			InvoiceCreator.AddInvoiceLog(csi.Merchant_id, "Tamal login request", null, sendParams, retValue);
            token = null;
            if (hsc != System.Net.HttpStatusCode.OK)
                throw new ApplicationException(string.Format("Unable to login tamal with account {0} http Error: {1}", sendParams, hsc));
            string[] retValues = retValue.Split(',');
            if (retValues.Length == 3) {
                token = retValues[0];
            } else if (retValues.Length == 2)
				throw new ApplicationException(string.Format("Unable to login tamal {0} with account {1}, {2} = {3}", ServiceUrl, sendParams, retValues[0], retValues[1]));
             else
				throw new ApplicationException(string.Format("Unable to login tamal {0} with account {1}, invalid reply: {2}", ServiceUrl, sendParams, retValue));
        }

		public static int CreateInvoice(Transactions.Transaction ctp, MerchantSettings csi)
		{
            string tamalCredToken = null;
			var invData = InvoiceCreator.GetInvoiceData(ctp, csi);
			Login(csi, out tamalCredToken);
            Doc300VO doc = new Doc300VO();
            if (csi.IsCreateInvoice || csi.IsCreateReceipt)
            {
                if (csi.IsCreateInvoice && csi.IsCreateReceipt) doc.TypeCode = 320;
                else if (csi.IsCreateInvoice) doc.TypeCode = 305;
                else if (csi.IsCreateReceipt) doc.TypeCode = 400;
            }
            if (doc.TypeCode == 0) throw new ApplicationException("tamal does not support this transType");
            DateTime producedDate = DateTime.Now; //ctp.InsertDate;
            //if (doc.TypeCode == 320 && invData.Amount < 0) doc.TypeCode = 305; /force create invoice only on refund when original was inv+rct
            //tamalCredToken
            doc.EsekNum = csi.ExternalUserID.ToInt32();
            doc.ClientNumber = 200000;
            doc.DocNumber = "";
            doc.ClientName = ctp.PayerData.FullName;
			if (ctp.PaymentData != null && ctp.PaymentData.BillingAddress != null)
			{
				doc.ClientAddress = ctp.PaymentData.BillingAddress.AddressLine1 + " " + ctp.PaymentData.BillingAddress.City;
				doc.ClientCityZip = ctp.PaymentData.BillingAddress.PostalCode;
			}
            doc.ClientOsekNum = "";
            doc.ProducedDateString = producedDate.ToString("dd/MM/yyyy");
            doc.SumBeforeDiscount = 0;
            doc.DiscountAmount = 0;
            doc.DiscountPercentage = 0;
            doc.TotalBeforeMaam = 0;
			doc.DocMaamAmount = 0; //System.Math.Round(invData.TaxAmount_ILS, 2);
			doc.DocTotalDue = System.Math.Round(invData.Amount_ILS, 2);
            doc.DocNikuyBamakorSum = 0;
            doc.DocUserFullName = Domain.Current.BrandName;
            doc.DocDetail = ctp.Comment;
			doc.DocNotMaam = (byte)(invData.IncludeTax ? 0 : 1);
			doc.MaamRate = System.Math.Round(invData.IncludeTax ? invData.TaxRate : 0, 2);
            doc.GeneralRemark = "";
            doc.DocTypeRemark = "";
            doc.DocClosureXML = "";
            doc.DocIsSourceProduced = 0;
            doc.DocSigned = 0;

            if (csi.IsCreateInvoice) {
                doc.DocProductsXML =
                    "<ROWS><ROW " +
                    " DDP_LineID=\"0\" " +
                    " DDP_EsekNum=\"" + doc.EsekNum + "\" " +
                    " DDP_ProductNum=\"0\"" +
					" DDP_ProductDetail=\"" + invData.InvoiceItemName + "\" " +
                    " DDP_ProductUnitMeasurementUnitCode=\"1\" " +
                    " DDP_Quantity=\"1\" " +
					" DDP_ProductCurrencyID=\"" + mapCurrency(tamalCredToken, invData.Curreny).ToString() + "\" " +
					" DDP_ProductUnitPrice=\"" + invData.Amount.ToString("0.00") + "\"" +
					" DDP_ExchangeRate=\"" + invData.ConvertionRate.ToString("0.00") + "\" " +
                    " DDP_UnitPriceILS=\"" + invData.Amount_ILS.ToString("0.00") + "\"" +
                    " DDP_DiscountPercentage=\"0\" " +
                    " DDP_DiscountSum=\"0\" " +
					" DDP_LineTotal=\"" + invData.Amount_ILS.ToString("0.00") + "\"" +
                    " SrcDocType=\"\" " +
                    " SrcDocNumber=\"\" " +
                    " /></ROWS>";
            }
            if (csi.IsCreateReceipt) {
                doc.DocChargesXML =
                    "<ROWS><ROW " +
                    "DDR_LineID=\"1\" " +
					"DDR_Sum=\"" + invData.Amount_ILS.ToString("0.00") + "\" " +
                    "DDR_ForInvProduction=\"False\" " +
                    "DDR_CheckPaymentDate=\"" + producedDate.ToString("yyyy-MM-dd") + "\" ";
				if (ctp.PaymentData.MethodInstance.PaymentMethodType == CommonTypes.PaymentMethodType.CreditCard)
				{
                    doc.DocChargesXML +=
                    "DDR_PayMethodTypeCode=\"3\" " +
                    "DDR_BankCode=\"\" DDR_BankBranchCode=\"\" " +
                    "DDR_CreditCardIssuerCode=\"" + mapPaymentMethod(ctp.PaymentMethodID).ToString() + "\" " +
                    "DDR_CreditPayments=\"" + ctp.Installments + "\" " +
					"DDR_CheckNum=\"" + ctp.PaymentData.MethodInstance.Value1Last4 + "\" " +
					"DDR_BankAccountNum=\"" + ctp.PaymentData.MethodInstance.ExpirationDate.Value.ToString("MMyy") + "\" " +
                    "DDR_CreditTransactionTypeCode=\"2\" " +
                    "DDR_FirstCreditPayment=\"" + ctp.FirstInstallment.ToString("0.00") + "\" " +
                    "DDR_AddCreditPaymentsSum=\"" + ctp.AnyOtherInstallment.ToString("0.00") + "\" ";
				}
				else if (ctp.PaymentData.MethodInstance.PaymentMethodType == CommonTypes.PaymentMethodType.ECheck)
				{
                    doc.DocChargesXML +=
                    "DDR_PayMethodTypeCode=\"2\" " +
                    "DDR_CreditCardIssuerCode=\"\" DDR_CreditPayments=\"\" DDR_CreditPayments=\"\" DDR_FirstCreditPayment=\"\" DDR_AddCreditPaymentsSum=\"\" " +
                    "DDR_CheckNum=\"" + ctp.PaymentMethodDisplay + "\" ";
				}
				else if (ctp.PaymentData.MethodInstance.PaymentMethodType == CommonTypes.PaymentMethodType.BankTransfer)
				{
                    doc.DocChargesXML +=
                    "DDR_PayMethodTypeCode=\"4\" " +
                    "DDR_CreditCardIssuerCode=\"\" DDR_CreditPayments=\"\" DDR_CreditPayments=\"\" DDR_FirstCreditPayment=\"\" DDR_AddCreditPaymentsSum=\"\" " +
                    "DDR_BankCode=\"\" " +
                    "DDR_BankBranchCode=\"\" " +
                    "DDR_BankAccountNum=\"" + ctp.PaymentMethodDisplay + "\" ";
			    }
                doc.DocChargesXML += " /></ROWS>";
            }
            string retValue;
            StringBuilder reqString = new StringBuilder();
            reqString.AppendFormat("credentialsToken={0}&", System.Web.HttpUtility.UrlEncode(tamalCredToken, ServiceEncoding));
            reqString.AppendFormat("clientIdentifier={0}&", ctp.ID);
            reqString.Append("toSign=1&");
            if (!string.IsNullOrEmpty(ctp.PayerData.EmailAddress))
            {
                reqString.Append("toMail=1&");
				reqString.AppendFormat("randomClientMail={0}&", System.Web.HttpUtility.UrlEncode(ctp.PayerData.EmailAddress.Trim(), ServiceEncoding));
            }
            else
                reqString.Append("toMail=0&");
			reqString.AppendFormat("isItemPriceWithTax={0}&", (invData.IncludeTax ? "1" : "0"));
            reqString.AppendFormat("jsonString={0}", System.Web.HttpUtility.UrlEncode(doc.ToJson(), ServiceEncoding));

            string sendParams = reqString.ToString();
            System.Net.HttpStatusCode hsc = Infrastructure.HttpClient.SendHttpRequest(ServiceUrl + "CreateDocument/", out retValue, sendParams);
			InvoiceCreator.AddInvoiceLog(csi.Merchant_id, "Tamal create document", null, sendParams, retValue);
			if (hsc != System.Net.HttpStatusCode.OK)
                throw new System.Net.WebException(string.Format("Unable to connect:{0}, return:{1}", sendParams, retValue));
            string[] retValues = retValue.Split(',');
            if (retValues.Length == 4) {
                int docNumber = retValues[1].ToInt32();
				InvoiceCreator.AddTransInvoiceNumber(ctp, docNumber, "Tamal document", retValues[2]);
                return docNumber;
            } else if (retValues.Length >= 2)
                throw new ApplicationException(string.Format("tamal createDocument error with account {0}, {1} => {2}", csi.ExternalUserName, sendParams, retValue));
            else
				throw new ApplicationException(string.Format("Unable to connect tamal createDocument with account {0}, send:{1} invalid reply: {2}", csi.ExternalUserName, sendParams, retValue));
		}
	}
}
