﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Invoices
{
    public class InvoiceDocument : BaseDataObject
    {
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Invoices.Module.Current, SecuredObjectName); } }


        public class TotalsResult
        {
            public string DocumentsCurrencyIso { get; set; }
            public int DocumentsTypeID { get; set; }
            public string DocumentsTypeName { get; set; }
            public int DocumentsCount { get; set; }
            public decimal Amount { get; set; }
            public decimal VAT { get; set; }
            public decimal Total { get; set; }
        }

        public class PrintInvoiceDetails
        {
            public int DocumentID { get; set; }

            public int? TransPayId { get; set; }

            public DateTime? payDate { get; set; }

            public int? payType { get; set; }

            public string sInvoiceTypeText { get; set; }

            public int? Currency { get; set; }

            public int? MerchantID { get; set; }

            public bool? sIsChargeVat { get; set; }

            public float? sVarAmount { get; set; }

            public DateTime? InsertDate { get; set; }

            public bool? IsPrinted { get; set; }

            public string CompanyLegalName { get; set; }

            public string FirstName { get; set; }

            public string LastName { get; set; }

            public string PayingDates1 { get; set; }

            public string PayingDates2 { get; set; }

            public string PayingDates3 { get; set; }

            public string PaymentMethod { get; set; }

            public string PaymentPayeeName { get; set; }

            public int? PaymentBank { get; set; }

            public string PaymentBranch { get; set; }

            public string PaymentAccount { get; set; }

        }

        public static PrintInvoiceDetails GetInvoicePrintDetails(int billingcompanyid, int invoicenumber, int invoicetype)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from InvoiceDocuments in DataContext.Reader.tblInvoiceDocuments
                    join TblCompanies in DataContext.Reader.tblCompanies on InvoiceDocuments.id_MerchantID equals TblCompanies.ID into TblCompaniesIncludeEmpty
                    join TransactionPays in DataContext.Reader.tblTransactionPays on InvoiceDocuments.ID equals TransactionPays.InvoiceDocumentID into InvoicesAndPays
                    from InvoicesAndCompanies in TblCompaniesIncludeEmpty.DefaultIfEmpty()
                    from InvoicesLeftJoinPays in InvoicesAndPays.DefaultIfEmpty()
                    where (InvoiceDocuments.id_BillingCompanyID == billingcompanyid &&
                    InvoiceDocuments.id_InvoiceNumber == invoicenumber &&
                    InvoiceDocuments.id_Type == invoicetype)

                    select new PrintInvoiceDetails()
                    {
                        TransPayId = InvoicesLeftJoinPays.id,
                        payDate = InvoicesLeftJoinPays.PayDate == null ? InvoiceDocuments.id_InsertDate : InvoicesLeftJoinPays.PayDate,
                        payType = InvoicesLeftJoinPays.PayType,
                        MerchantID = InvoiceDocuments.id_MerchantID, //"nCompanyID
                        DocumentID = InvoiceDocuments.ID,
                        sIsChargeVat = InvoiceDocuments.id_ApplyVAT,
                        sVarAmount = (!InvoiceDocuments.id_ApplyVAT) ? 0 : InvoiceDocuments.id_VATPercent,
                        IsPrinted = InvoiceDocuments.id_IsPrinted,
                        InsertDate = InvoiceDocuments.id_InsertDate,
                        Currency = InvoiceDocuments.id_Currency,
                        CompanyLegalName = InvoicesAndCompanies.CompanyLegalName,
                        FirstName = InvoicesAndCompanies.FirstName,
                        LastName = InvoicesAndCompanies.LastName,
                        PaymentMethod = InvoicesAndCompanies.PaymentMethod,
                        PayingDates1 = InvoicesAndCompanies.payingDates1,
                        PayingDates2 = InvoicesAndCompanies.payingDates2,
                        PayingDates3 = InvoicesAndCompanies.payingDates3,
                        PaymentPayeeName = InvoicesAndCompanies.PaymentPayeeName,
                        PaymentBank = InvoicesAndCompanies.PaymentBank,
                        PaymentBranch = InvoicesAndCompanies.PaymentBranch,
                        PaymentAccount = InvoicesAndCompanies.PaymentAccount,
                    })
                    .SingleOrDefault();
        }

        public class SearchFilters
        {
            public int? ID;
            public Range<DateTime?> DateRange;
            public Range<int?> InvoiceNumberRange;
            public int? DocumentType;
            public bool? IsManual;
            public int? BillingCompanyID;
        }

        private class InvoiceDocumentWithOrderNumber
        {
            public Dal.Netpay.tblInvoiceDocument Entity { get; set; }
            public int InvoiceOrderNumber { get; set; }
        }

        public static Dictionary<bool, System.Drawing.Color> InvoiceColor = new Dictionary<bool, System.Drawing.Color>
        {
            {true , System.Drawing.ColorTranslator.FromHtml("#9e6cff")},
            {false , System.Drawing.ColorTranslator.FromHtml("#6699cc")}
        };

        public static Dictionary<bool, string> InvoiceColorText = new Dictionary<bool, string>
        {
            {true, "MANUAL ISSUANCE" },
            {false, "TRANSACTION FEES" }
        };

        private Dal.Netpay.tblInvoiceDocument _entity;

        public int ID { get { return _entity.ID; } }

        public int InvoiceOrderNumber { get; set; }

        public int BillingCompanyID { get { return _entity.id_BillingCompanyID; } }

        public int Type { get { return _entity.id_Type; } }

        public int InvoiceNumber { get { return _entity.id_InvoiceNumber; } }

        public int TransactionPayID { get { return _entity.id_TransactionPayID; } }

        public string BillToName { get { return _entity.id_BillToName; } }

        public string sBillTo
        {
            get
            {
                string result = BillToName.Replace(System.Environment.NewLine, " - ").Replace("<br />", " - ").Replace("<br />", " - ");
                if (result.Length < 22) return result;
                else return result.Substring(0, 19) + "...";
            }
        }

        public int MerchantID { get { return _entity.id_MerchantID; } }

        public DateTime InsertDate { get { return _entity.id_InsertDate; } }

        public DateTime PrintDate { get { return _entity.id_PrintDate; } }

        public string UserName { get { return _entity.id_Username; } }

        public int Lines { get { return _entity.id_Lines; } }

        public decimal TotalLines { get { return _entity.id_TotalLines; } }

        public bool ApplyVat { get { return _entity.id_ApplyVAT; } }

        public float VatPercent { get { return _entity.id_VATPercent; } }

        public decimal TotalVat { get { return _entity.id_TotalVAT; } }

        public decimal TotalDocument { get { return _entity.id_TotalDocument; } }

        public int Currency { get { return _entity.id_Currency; } }

        public decimal CurrencyRate { get { return _entity.id_CurrencyRate; } }

        public bool? IsPrinted { get { return _entity.id_IsPrinted; } }

        public string BillingCompanyName { get { return _entity.id_BillingCompanyName; } }

        public string BillingCompanyFirstLetter { get { return "(" + BillingCompanyName[0] + ")"; } }

        public string BillingCompanyAddress { get { return _entity.id_BillingCompanyAddress; } }

        public string BillingCompanyNumber { get { return _entity.id_BillingCompanyNumber; } }

        public string BillingCompanyEmail { get { return _entity.id_BillingCompanyEmail; } }

        public string BillingCompanyLanguage { get { return _entity.id_BillingCompanyLanguage; } }

        public bool IsManual { get { return _entity.id_IsManual; } }

        public string MerchantName { get; private set; }

        public string DocumentTypeText { get; private set; }

        public string CurrencySymbol { get; private set; }

        private InvoiceDocument(Dal.Netpay.tblInvoiceDocument entity)
        {
            _entity = entity;
        }

        public InvoiceDocument()
        {
            _entity = new Dal.Netpay.tblInvoiceDocument();
        }


        public static List<TotalsResult> GetTotalsResult(SearchFilters filters)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var expression = (from InvoiceDocuments in DataContext.Reader.tblInvoiceDocuments
                              select InvoiceDocuments);

            if (filters != null)
            {
                if (filters.ID.HasValue) expression = expression.Where(x => x.ID == filters.ID.Value);
                if (filters.DateRange.From.HasValue) expression = expression.Where(x => x.id_InsertDate >= filters.DateRange.From.Value);
                if (filters.DateRange.To.HasValue) expression = expression.Where(x => x.id_InsertDate <= filters.DateRange.To.Value);
                if (filters.InvoiceNumberRange.From.HasValue) expression = expression.Where(x => x.id_InvoiceNumber >= filters.InvoiceNumberRange.From.Value);
                if (filters.InvoiceNumberRange.To.HasValue) expression = expression.Where(x => x.id_InvoiceNumber <= filters.InvoiceNumberRange.To.Value);
                if (filters.DocumentType.HasValue) expression = expression.Where(x => x.id_Type == filters.DocumentType.Value);
                if (filters.IsManual.HasValue) expression = expression.Where(x => x.id_IsManual == filters.IsManual.Value);
                if (filters.BillingCompanyID.HasValue) expression = expression.Where(x => x.id_BillingCompanyID == filters.BillingCompanyID.Value);
            }

            var expressionGroup = expression
                .GroupBy(i => new
                {
                    i.id_Currency,
                    i.id_Type
                })
                .OrderBy(x => x.Key.id_Currency)
                .ThenBy(x => x.Key.id_Type)
                .Select(z => new
                {
                    CurrencyID = z.Key.id_Currency,
                    TypeID = z.Key.id_Type,
                    Values = z.ToList()
                });

            return expressionGroup
                .Select(y => new TotalsResult()
                {
                    DocumentsCount = y.Values.Count(),
                    DocumentsCurrencyIso = Bll.Currency.GetIsoCode((CommonTypes.Currency)y.CurrencyID),
                    DocumentsTypeName = InvoiceType.GetInvoiceTypeText(y.TypeID),
                    DocumentsTypeID = y.TypeID,
                    Amount = y.Values.Sum(x => x.id_TotalLines),
                    VAT = y.Values.Sum(x => x.id_TotalVAT),
                    Total = y.Values.Sum(x => x.id_TotalLines) + y.Values.Sum(x => x.id_TotalVAT)
                })
                .OrderBy(x => x.DocumentsTypeID)
                .ToList();
        }


        public static List<InvoiceDocument> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var expression = (from InvoiceDocuments in DataContext.Reader.tblInvoiceDocuments
                              join TblCompanies in DataContext.Reader.tblCompanies on InvoiceDocuments.id_MerchantID equals TblCompanies.ID into FirstLeftJoin

                              from FirstLeftJoinIncludeEmpty in FirstLeftJoin.DefaultIfEmpty()
                              join InvoiceTypes in DataContext.Reader.InvoiceTypes on InvoiceDocuments.id_Type equals InvoiceTypes.InvoiceType_id into SecondLeftJoin

                              from SecondLeftJoinIncludeEmpty in SecondLeftJoin.DefaultIfEmpty()
                              join cu in DataContext.Reader.CurrencyLists on InvoiceDocuments.id_Currency equals cu.CurrencyID into ThirdLeftJoin

                              from ThirdLeftJoinIncludeEmpty in ThirdLeftJoin.DefaultIfEmpty()

                              orderby InvoiceDocuments.id_Type ascending
                              orderby InvoiceDocuments.id_BillingCompanyID ascending

                              select new
                              {
                                  InvoiceDocumentItem = InvoiceDocuments,
                                  MerchantName = string.IsNullOrEmpty(FirstLeftJoinIncludeEmpty.CompanyName) ? "---" : FirstLeftJoinIncludeEmpty.CompanyName,
                                  DocumentType = SecondLeftJoinIncludeEmpty.Name,
                                  CurrencySymbol = ThirdLeftJoinIncludeEmpty.Symbol
                              });

            if (filters != null)
            {
                if (filters.ID.HasValue) expression = expression.Where(x => x.InvoiceDocumentItem.ID == filters.ID.Value);
                if (filters.DateRange.From.HasValue) expression = expression.Where(x => x.InvoiceDocumentItem.id_InsertDate >= filters.DateRange.From.Value);
                if (filters.DateRange.To.HasValue) expression = expression.Where(x => x.InvoiceDocumentItem.id_InsertDate <= filters.DateRange.To.Value);
                if (filters.InvoiceNumberRange.From.HasValue) expression = expression.Where(x => x.InvoiceDocumentItem.id_InvoiceNumber >= filters.InvoiceNumberRange.From.Value);
                if (filters.InvoiceNumberRange.To.HasValue) expression = expression.Where(x => x.InvoiceDocumentItem.id_InvoiceNumber <= filters.InvoiceNumberRange.To.Value);
                if (filters.DocumentType.HasValue) expression = expression.Where(x => x.InvoiceDocumentItem.id_Type == filters.DocumentType.Value);
                if (filters.IsManual.HasValue) expression = expression.Where(x => x.InvoiceDocumentItem.id_IsManual == filters.IsManual.Value);
                if (filters.BillingCompanyID.HasValue) expression = expression.Where(x => x.InvoiceDocumentItem.id_BillingCompanyID == filters.BillingCompanyID.Value);
            }

            var expressionGroup = expression
                .GroupBy(i => new
                {
                    i.InvoiceDocumentItem.id_Type,
                    i.InvoiceDocumentItem.id_BillingCompanyID
                })
                .OrderBy(x => x.Key.id_Type)
                .ThenBy(x => x.Key.id_BillingCompanyID)
                .Select((i) => new
                {
                    Values = i.OrderByDescending(z => z.InvoiceDocumentItem.id_InvoiceNumber),
                });


            var GroupValuesBackToOneListOfItems = expressionGroup.SelectMany((x) => x.Values)
                 .OrderByDescending(x => x.InvoiceDocumentItem.id_InsertDate)
                 .ThenByDescending(x => x.InvoiceDocumentItem.id_InvoiceNumber)
                 .ApplySortAndPage(sortAndPage, "InvoiceDocumentItem.");

            if (filters.DocumentType.HasValue)
            {
                GroupValuesBackToOneListOfItems = GroupValuesBackToOneListOfItems.Where(x => x.InvoiceDocumentItem.id_Type == filters.DocumentType.Value);
            }

            return GroupValuesBackToOneListOfItems
            .Select(z => new InvoiceDocument(z.InvoiceDocumentItem)
            {
                MerchantName = z.MerchantName,
                CurrencySymbol = z.CurrencySymbol,
                DocumentTypeText = z.DocumentType
            }).ToList();
        }

        public static InvoiceDocument GetInvoiceDocumentByID(int id)
        {
            var sf = new SearchFilters();
            sf.ID = id;
            return Search(sf, null).SingleOrDefault();
        }

        public static void UpdateBillToName(int id, string newname)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            Dal.Netpay.tblInvoiceDocument entityToUpdate = (from InvoiceDocuments in DataContext.Writer.tblInvoiceDocuments
                                                            where (InvoiceDocuments.ID == id && InvoiceDocuments.id_IsPrinted.Value == false)
                                                            select InvoiceDocuments)
                                                            .SingleOrDefault();

            if (entityToUpdate == null) return;

            newname = newname.Replace(System.Environment.NewLine, "<br />");

            entityToUpdate.id_BillToName = newname;
            DataContext.Writer.tblInvoiceDocuments.Update(entityToUpdate, true);
            DataContext.Writer.SubmitChanges();
        }

        public static void SetPrintDateValue(int id , DateTime now)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            Dal.Netpay.tblInvoiceDocument entityToUpdate = (from InvoiceDocuments in DataContext.Writer.tblInvoiceDocuments
                                                            where (InvoiceDocuments.ID == id && InvoiceDocuments.id_IsPrinted.Value == false)
                                                            select InvoiceDocuments)
                                                            .SingleOrDefault();

            if (entityToUpdate == null) return;
                        
            entityToUpdate.id_PrintDate = now;
            DataContext.Writer.tblInvoiceDocuments.Update(entityToUpdate, true);
            DataContext.Writer.SubmitChanges();
        }
    }
}
