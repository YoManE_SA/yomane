﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Invoices
{
    public static class Local
    {
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return InvoiceDocument.SecuredObject; } }

        public static int CreateInvoice(Transactions.Transaction ctp, MerchantSettings csi)
        {
            /*
			dc.InvoiceLineCreate(0, csi.
			ctp.Amount
			sSQL="EXEC InvoiceLineCreate " & nTransactionPayID & ", '', " & nMerchantID & ", '" & sText & "', " & sQuantity & ", " & FormatNumber(sUnitAmount * curExchangeRate, 2, True, False, False) & ", " & nCurrency & ", " & FormatNumber(curExchangeRate, 4, True, False, False) & ", '" & PageSecurity.Username & "';"

			sSQL = "EXEC InvoiceDocumentCreate" & _
				" " & nPayID & "," & _
				" " & IIF(excludeAmounts <> "", "1", "0") & "," & _
				" " & rsData("BillingCompanys_id") & "," & _
				" " & rsData("isChargeVAT") & "," & _
				" 0," & _
				" " & rsData("ID") & "," & _
				" ''," & _
				" " & invCurrency & "," & _
				" " & FormatNumber(curExchangeRate, 4, True, False, False) & "," & _
				" '" & PageSecurity.Username & "';"
			ExecSQL sSQL
			*/
            return 0;
        }

        public static int CreateInvoice(int payId)
        {
            /*
			dc.InvoiceLineCreate(0, csi.
			ctp.Amount
			sSQL="EXEC InvoiceLineCreate " & payId & ", '', " & nMerchantID & ", '" & sText & "', " & sQuantity & ", " & FormatNumber(sUnitAmount * curExchangeRate, 2, True, False, False) & ", " & nCurrency & ", " & FormatNumber(curExchangeRate, 4, True, False, False) & ", '" & PageSecurity.Username & "';"

			sSQL = "EXEC InvoiceDocumentCreate" & _
				" " & nPayID & "," & _
				" " & IIF(excludeAmounts <> "", "1", "0") & "," & _
				" " & rsData("BillingCompanys_id") & "," & _
				" " & rsData("isChargeVAT") & "," & _
				" 0," & _
				" " & rsData("ID") & "," & _
				" ''," & _
				" " & invCurrency & "," & _
				" " & FormatNumber(curExchangeRate, 4, True, False, False) & "," & _
				" '" & PageSecurity.Username & "';"
			ExecSQL sSQL
			*/
            return 0;
        }

        //Ask Eran if i have to return the same int values like in the stored procedure when there is an error 
        public static int InvoiceDocumentCreate(int nTransactionPay, int nDocumentType, int nBillingCompany, bool bApplyVat, bool bLinesReduceVAT, int nMerchant, string sBillTo, int nCurrency, decimal nCurrencyRate, string sUserName, string sDocumentDate = null, string sLanguage = null)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);


            //Check if Document Type id exist. 
            if (!DataContext.Writer.InvoiceTypes.Any(x => x.InvoiceType_id == nDocumentType)) throw new Exception ("Invoice type id doesn't exist in db, invoice type id:"+nDocumentType);


            //Check if billing copmpany exist.
            if (!DataContext.Writer.tblBillingCompanies.Any(x => x.BillingCompanys_id == nBillingCompany)) throw new Exception("error -2 : Billing company not found"); //-2 Billing company not found

            //Billing company issue 
            string sBillingCompanyName = "";
            string sBillingCompanyAddress = "";
            string sBillingCompanyNumber = "";
            string sBillingCompanyEmail = "";
            string sBillingCompanyLanguage = "";
            double nVATPercent = 0;
                        
            var billingCompanyEntity = (from BillingCompanies in DataContext.Writer.tblBillingCompanies
                                        where BillingCompanies.BillingCompanys_id == nBillingCompany
                                        select BillingCompanies).SingleOrDefault();

            //Insert values to Billing Company variables
            sBillingCompanyName = billingCompanyEntity.name;
            sBillingCompanyAddress = billingCompanyEntity.address;
            sBillingCompanyNumber = billingCompanyEntity.number;
            sBillingCompanyEmail = billingCompanyEntity.email;
            sBillingCompanyLanguage = string.IsNullOrEmpty(sLanguage) ? billingCompanyEntity.LanguageShow : sLanguage;
            nVATPercent = billingCompanyEntity.VATamount;

            //Check if there is a merchant or sBillto values
            if (nMerchant == 0 && string.IsNullOrEmpty(sBillTo)) throw new Exception("error -3 : neither merchant nor Invoice name specified"); //-3 neither merchant nor Invoice name specified

            //Check if both merchant and invoice name specified.
            if (nMerchant > 0 && (!string.IsNullOrEmpty(sBillTo))) throw new Exception("error -4 : both merchant and Invoice name specified"); //-4 both merchant and Invoice name specified

            //Check if merchantId exist
            if (nMerchant > 0 && (!DataContext.Writer.tblCompanies.Any(x => x.ID == nMerchant))) throw new Exception("error -5 : merchant not found, id-"+nMerchant); //-5 merchant not found 

            //Check if transactionPay exist
            //Ask Eran why in StoredProcedure there is IsNull check for InvoiceDocumentID value, even thoght its a not null column
            if ((nTransactionPay > 0) && (!DataContext.Writer.tblTransactionPays.Any(x => x.id == nTransactionPay && x.InvoiceDocumentID == 0))) throw new Exception("error -7 : unattended transaction pay not found"); //-7 unattended transaction pay not found 

            //Check if no lines for this merchant/transaction pay
            if ((nMerchant > 0) && (!DataContext.Writer.tblInvoiceLines.Any(x => x.il_MerchantID == nMerchant && x.il_TransactionPayID == nTransactionPay && !x.il_DocumentID.HasValue))) throw new Exception("error -8 : No invoice line found for that merchant or payment"); //-8

            
            if ((!string.IsNullOrEmpty(sBillTo)) && (!DataContext.Writer.tblInvoiceLines.Any(x => x.il_BillToName == sBillTo && x.il_TransactionPayID == nTransactionPay && !x.il_DocumentID.HasValue)))
            {
                if (nTransactionPay > 0)
                {
                    InvoiceLineCreate(nTransactionPay, sBillTo, 0, "EMPTY LINE", 1, (decimal)0.001, nCurrency, nCurrencyRate, sUserName);
                }

                if (!string.IsNullOrEmpty(sBillTo) && (!DataContext.Writer.tblInvoiceLines.Any(x => x.il_BillToName == sBillTo && x.il_TransactionPayID == nTransactionPay && !x.il_DocumentID.HasValue))) throw new Exception("error -9"); //-9
            }

            if (nMerchant > 0)
            {
                if (string.IsNullOrEmpty((from TblCompanies in DataContext.Writer.tblCompanies
                                          where TblCompanies.ID == nMerchant
                                          select TblCompanies.CompanyLegalName).SingleOrDefault().Trim()))
                    throw new Exception("error -10 : \"Company Legal Name\" not specified for this merchant"); //-10 "Company Legal Name" not specified for this merchant

                //store billing name and address in one field
                sBillTo = (from TblCompanies in DataContext.Writer.tblCompanies
                           where TblCompanies.ID == nMerchant
                           select new { Name = TblCompanies.CompanyLegalName +
                                        ((TblCompanies.CompanyStreet!="" || TblCompanies.CompanyCity!="" || TblCompanies.CompanyZIP!="") ? "<br />" : "") +
                                        TblCompanies.CompanyStreet + 
                                        " " + TblCompanies.CompanyCity + 
                                        " " + TblCompanies.CompanyZIP })
                           .SingleOrDefault().Name;
            }

            int nInvoiceNumber = 0;

            if (nTransactionPay > 0)
            {
                var transactionPayEntity = (from tp in DataContext.Writer.tblTransactionPays where tp.id == nTransactionPay select tp).SingleOrDefault();
                if (!string.IsNullOrEmpty(transactionPayEntity.BillingCompanyName)) sBillingCompanyName = transactionPayEntity.BillingCompanyName;
                if (!string.IsNullOrEmpty(transactionPayEntity.BillingCompanyAddress)) sBillingCompanyAddress = transactionPayEntity.BillingCompanyAddress;
                if (!string.IsNullOrEmpty(transactionPayEntity.BillingCompanyNumber)) sBillingCompanyNumber = transactionPayEntity.BillingCompanyNumber;
                if (!string.IsNullOrEmpty(transactionPayEntity.BillingCompanyEmail)) sBillingCompanyEmail = transactionPayEntity.BillingCompanyEmail;
                if (!string.IsNullOrEmpty(transactionPayEntity.BillingToInfo)) sBillTo = transactionPayEntity.BillingToInfo;
                if (transactionPayEntity.VatAmount != 0) nVATPercent = (double)transactionPayEntity.VatAmount; //Ask Eran if thats ok to cast decimal to double that way
                nInvoiceNumber = transactionPayEntity.InvoiceNumber;
            }

            if (nInvoiceNumber == 0)
            {
                nInvoiceNumber = DataContext.Writer.tblInvoiceDocuments.Where(x => x.id_BillingCompanyID == nBillingCompany && x.id_Type == nDocumentType).Max(x => x.id_InvoiceNumber) + 1;
            }

            //Validated - can create.
            //Should get this format of date - yyyy-mm-dd hh:mi:ss (24h) - ODBC canonical
            IFormatProvider culture = new System.Globalization.CultureInfo("en-US", true);
            DateTime dtDocumentDate = sDocumentDate != null ? DateTime.ParseExact(sDocumentDate, "dd/MM/yyyy HH:mm:ss.fff", culture) : DateTime.Now;

            var newInvoiceDocument = new Dal.Netpay.tblInvoiceDocument();
            newInvoiceDocument.id_Type = nDocumentType;
            newInvoiceDocument.id_BillingCompanyID = nBillingCompany;
            newInvoiceDocument.id_InvoiceNumber = nInvoiceNumber;
            newInvoiceDocument.id_ApplyVAT = bApplyVat;
            newInvoiceDocument.id_MerchantID = nMerchant;
            newInvoiceDocument.id_BillToName = sBillTo;
            newInvoiceDocument.id_Currency = nCurrency;
            newInvoiceDocument.id_CurrencyRate = nCurrencyRate;
            newInvoiceDocument.id_VATPercent = (float)nVATPercent; // Ask Eran if that casting is ok.
            newInvoiceDocument.id_Username = sUserName;
            newInvoiceDocument.id_TransactionPayID = nTransactionPay;
            newInvoiceDocument.id_BillingCompanyName = sBillingCompanyName;
            newInvoiceDocument.id_BillingCompanyAddress = sBillingCompanyAddress;
            newInvoiceDocument.id_BillingCompanyNumber = sBillingCompanyNumber;
            newInvoiceDocument.id_BillingCompanyEmail = sBillingCompanyEmail;
            newInvoiceDocument.id_BillingCompanyLanguage = sBillingCompanyLanguage;
            newInvoiceDocument.id_InsertDate = dtDocumentDate;
            newInvoiceDocument.id_PrintDate = dtDocumentDate;

            DataContext.Writer.tblInvoiceDocuments.InsertOnSubmit(newInvoiceDocument);
            DataContext.Writer.SubmitChanges();
                       
            //Get the entity that was saved in db
            int nID = newInvoiceDocument.ID;

            InvoiceDocumentRebuild(nID, bLinesReduceVAT);

            if (nTransactionPay > 0)
            {
                var transactionPayEntityToUpdate = (from transactionPaysTable in DataContext.Writer.tblTransactionPays
                                                    where transactionPaysTable.id == nTransactionPay
                                                    select transactionPaysTable)
                                                    .SingleOrDefault();

                if (transactionPayEntityToUpdate != null)
                {
                    transactionPayEntityToUpdate.InvoiceDocumentID = nID;
                    transactionPayEntityToUpdate.InvoiceNumber = nInvoiceNumber;

                    DataContext.Writer.tblTransactionPays.Update(transactionPayEntityToUpdate, true);
                    DataContext.Writer.SubmitChanges();
                }
                else
                {
                    throw new Exception("Payment to update wasn't found.");
                }
            }

            return newInvoiceDocument.id_InvoiceNumber;
        }

        public static void InvoiceDocumentRebuild(int nID, bool bLinesReduceVAT)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            if (!DataContext.Writer.tblInvoiceDocuments.Any(x => x.ID == nID)) return; //Document not found.

            bool? bIsPrinted = (DataContext.Writer.tblInvoiceDocuments.Where(x => x.ID == nID).Select(x => x.id_IsPrinted)).Single();
            if (bIsPrinted.Value) return; // Document is already printed.

            if (bLinesReduceVAT)
            {
                var entityToUpdateAndRelevantVal =
                    (from InvoiceLinesTable in DataContext.Writer.tblInvoiceLines
                     join InvoiceDocumentsTable in DataContext.Writer.tblInvoiceDocuments on InvoiceLinesTable.il_DocumentID equals InvoiceDocumentsTable.ID
                     where InvoiceDocumentsTable.ID == nID
                     select new { entityToUpdate = InvoiceLinesTable, vatVal = InvoiceDocumentsTable.id_VATPercent })
                     .SingleOrDefault();

                if (entityToUpdateAndRelevantVal != null)
                {
                    entityToUpdateAndRelevantVal.entityToUpdate.il_Price = ((entityToUpdateAndRelevantVal.entityToUpdate.il_Price) / (decimal)(entityToUpdateAndRelevantVal.vatVal + 1));
                    DataContext.Writer.tblInvoiceLines.Update(entityToUpdateAndRelevantVal.entityToUpdate, true);
                    DataContext.Writer.SubmitChanges();
                }
            }

            //Ask Eran if this lines should be inside the last IF or not
            var invoiceDocumentEntityToUpdate = DataContext.Writer.tblInvoiceDocuments.Where(x => x.ID == nID).Single();

            invoiceDocumentEntityToUpdate.id_Lines = DataContext.Writer.tblInvoiceLines.Where(x => x.il_DocumentID == nID).Select(x => x.ID).Count();
            invoiceDocumentEntityToUpdate.id_TotalLines = (DataContext.Writer.tblInvoiceLines
                                                          .Where(x => x.il_DocumentID == nID)
                                                          .Sum(x => x.il_Amount.Value * x.il_CurrencyRate)) / invoiceDocumentEntityToUpdate.id_CurrencyRate;

            DataContext.Writer.tblInvoiceDocuments.Update(invoiceDocumentEntityToUpdate, true);
            DataContext.Writer.SubmitChanges();
            //

            invoiceDocumentEntityToUpdate = DataContext.Writer.tblInvoiceDocuments.Where(x => x.ID == nID).Single();
            invoiceDocumentEntityToUpdate.id_TotalVAT = invoiceDocumentEntityToUpdate.id_TotalLines * (invoiceDocumentEntityToUpdate.id_ApplyVAT ? (decimal)invoiceDocumentEntityToUpdate.id_VATPercent : 0);
            DataContext.Writer.tblInvoiceDocuments.Update(invoiceDocumentEntityToUpdate, true);
            DataContext.Writer.SubmitChanges();
            //

            invoiceDocumentEntityToUpdate = DataContext.Writer.tblInvoiceDocuments.Where(x => x.ID == nID).Single();
            invoiceDocumentEntityToUpdate.id_TotalDocument = invoiceDocumentEntityToUpdate.id_TotalLines + invoiceDocumentEntityToUpdate.id_TotalVAT;
            DataContext.Writer.tblInvoiceDocuments.Update(invoiceDocumentEntityToUpdate, true);
            DataContext.Writer.SubmitChanges();
            //
        }

        /// <summary>
        ///  /// Implementation of a stored-procedure in DB [dbo].[InvoiceDocumentCreate]
        /// </summary>
        /// <param name="nTransactionPayID"></param>
        /// <param name="sBillTo"></param>
        /// <param name="nMerchant"></param>
        /// <param name="sText"></param>
        /// <param name="nQuantity"></param>
        /// <param name="nPrice"></param>
        /// <param name="nCurrency"></param>
        /// <param name="nCurrencyRate"></param>
        /// <param name="sUserName"></param>
        /// <returns></returns>
        public static int InvoiceLineCreate(int nTransactionPayID, string sBillTo, int nMerchant, string sText, Single nQuantity, decimal nPrice, int nCurrency, decimal nCurrencyRate, string sUserName)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            if ((nMerchant == 0) && (string.IsNullOrEmpty(sBillTo))) throw new Exception("Bill to value is empty.");
            if ((nMerchant > 0) && (!string.IsNullOrEmpty(sBillTo))) throw new Exception("You must choose a merchant or Bill to value, not both.");
            if ((nMerchant > 0) && (!DataContext.Writer.tblCompanies.Any(x => x.ID == nMerchant))) throw new Exception("Merchant not found, merchant id:"+nMerchant);

            var newInvoiceLine = new Dal.Netpay.tblInvoiceLine();
            newInvoiceLine.il_TransactionPayID = nTransactionPayID;
            newInvoiceLine.il_BillToName = sBillTo;
            newInvoiceLine.il_MerchantID = nMerchant;
            newInvoiceLine.il_Text = sText;
            newInvoiceLine.il_Quantity = nQuantity;
            newInvoiceLine.il_Price = nPrice;
            newInvoiceLine.il_Currency = nCurrency;
            newInvoiceLine.il_CurrencyRate = nCurrencyRate;
            newInvoiceLine.il_Username = sUserName;
            newInvoiceLine.il_Date = DateTime.Now;

            DataContext.Writer.tblInvoiceLines.InsertOnSubmit(newInvoiceLine);
            DataContext.Writer.SubmitChanges();
            
            return newInvoiceLine.ID;
        }

        public static void AddInvoiceLine(int nTransactionPayID, int nMerchantID, int nCurrency, decimal curExchangeRate, string sText, string sUnitAmount, int sQuantity, decimal sTotal, string userName)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);


            decimal sUnitAmountForInvoiceLineCreate = 0;
            if (sQuantity == 0) return;

            if (sUnitAmount == "---")
            {
                //sUnitAmount = (sTotal / sQuantity).ToString();
                sUnitAmountForInvoiceLineCreate = (sTotal / sQuantity);
            }

            InvoiceLineCreate(nTransactionPayID, "", nMerchantID, sText, sQuantity, decimal.Parse((sUnitAmountForInvoiceLineCreate * curExchangeRate).ToString("F")), nCurrency, decimal.Parse(curExchangeRate.ToString("F4")), userName);
        }

        public static int GetPaymentDebitCompany(int payId)
        {
            /*  Sql statement:
            	Select DebitCompanyID 
				From tblCompanyTransPass 
                Where PrimaryPayedID=" & nPayID & " And IsNull(DebitCompanyID, 0) Not IN(0, 1) 
				Group By DebitCompanyID"
			*/
            List<int> dcIdsFromTblTransPass = (from ctp in DataContext.Reader.tblCompanyTransPasses
                                               where ctp.PrimaryPayedID == payId && (!(ctp.DebitCompanyID == null || ctp.DebitCompanyID == 1))
                                               group ctp by ctp.DebitCompanyID.Value into ctpGroupByDcId
                                               select ctpGroupByDcId.Key).ToList();

            if (dcIdsFromTblTransPass.Count == 0) return 0;

            int check = (from dcFeeExcludeInvoice in DataContext.Reader.DebitCompanyFeeExcludeInvoices
                         where dcIdsFromTblTransPass.Contains(dcFeeExcludeInvoice.DebitCompany_id)
                         group dcFeeExcludeInvoice by dcFeeExcludeInvoice.DebitCompany_id into dcFeeGroupByDcId
                         select dcFeeGroupByDcId.Key)
                         .Distinct()
                         .Count();

            if (check > 1)
            {
                return -1;
            }
            else 
            {
                return dcIdsFromTblTransPass.First();
            }
           
        }

        public static List<int> GetDebitCompanyFeeExcludeInvoice(int dbId)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            List<int> transAmountTypeIdList = (from DcFeeExcludeInvoice in DataContext.Reader.DebitCompanyFeeExcludeInvoices
                                               where DcFeeExcludeInvoice.DebitCompany_id == dbId
                                               select DcFeeExcludeInvoice.TransAmountType_id).ToList();
            return transAmountTypeIdList;
        }

        public static string CreateInvoiceForSettlement(int payId, string userName)
        {
            if (Infrastructure.Security.Login.Current != null && Infrastructure.Security.Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);


            int invDebitCompany = GetPaymentDebitCompany(payId);
            if (invDebitCompany == -1)
            {
                return "the settlemnt include transactions from more than one direct debit copmany with invoice exclude rows, can't produce invoice";
            }
            else // Regular flow...
            {
                List<int> excludeAmounts = GetDebitCompanyFeeExcludeInvoice(invDebitCompany);

                var rsData = (from TblTransactionPay in DataContext.Reader.tblTransactionPays
                              join TblCompany in DataContext.Reader.tblCompanies on TblTransactionPay.CompanyID equals TblCompany.ID
                              join TblBillingCompany in DataContext.Reader.tblBillingCompanies on TblTransactionPay.BillingCompanys_id equals TblBillingCompany.BillingCompanys_id
                              where TblTransactionPay.id == payId
                              select new
                              {
                                  ID = TblCompany.ID,
                                  BillingCompanys_id = TblCompany.BillingCompanys_id,
                                  isChargeVAT = TblCompany.isChargeVAT,
                                  LanguageShow = TblBillingCompany.LanguageShow,
                                  CurrencyVal = TblTransactionPay.Currency,
                                  CurrencyShow = TblBillingCompany.currencyShow
                              }).FirstOrDefault();


                //Check if merchant found , if not return.
                if (rsData == null)
                {
                    return "Merchant not found";
                }
                else
                {
                    //Only english in our flow.
                    string[] nTextList = new string[] { "Authorized transaction fee", "Refund transaction fee", "Processing fee", "Financing fees", "Transaction clarification fee", "Transaction Chargeback fee", "Other fees", "Handling Fee" };
                    string textAdd = "";

                    var AmountSum = Totals.Totals.CalcCompanyPaymentTotal(payId, rsData.ID, rsData.CurrencyVal.Value);
                    var gTotal = AmountSum.PaymentMethodTotal((CommonTypes.PaymentMethodEnum.CC_MIN), (CommonTypes.PaymentMethodEnum.EC_MAX));
                    
                    short? invCurrency = (from IIC in DataContext.Reader.InvoiceIssuerCurrencies
                                          where IIC.InvoiceIssuer_id == rsData.BillingCompanys_id && IIC.CurrencySettlement == AmountSum.Currency
                                          select IIC.CurrencyInvoice).FirstOrDefault();

                    if (!invCurrency.HasValue) invCurrency = (short)rsData.CurrencyShow;

                    if (invCurrency.Value != (short)AmountSum.Currency)
                    {
                        textAdd = " (" + Currency.GetIsoCode((CommonTypes.Currency)AmountSum.Currency) + ")";
                    }

                    Currency curFrom = Currency.Get(AmountSum.Currency);
                    Currency curTo = Currency.Get(invCurrency.Value);
                    decimal curExchangeRate = Currency.ConvertRate(curFrom, curTo);


                    if (!excludeAmounts.Contains((int)CommonTypes.TransactionAmountType.FeeLine)) // TAT_TransactionFee = 1002
                    {
                        AddInvoiceLine(payId, rsData.ID, rsData.CurrencyVal.Value, curExchangeRate, nTextList[0] + textAdd, "---", gTotal.NormCount, gTotal.NormLineFee, userName);
                    }

                    if (!excludeAmounts.Contains((int)CommonTypes.TransactionAmountType.FeeRefund))
                    {
                        AddInvoiceLine(payId, rsData.ID, rsData.CurrencyVal.Value, curExchangeRate, nTextList[1] + textAdd, "---", gTotal.RefCount, gTotal.RefFees, userName);
                    }

                    if (!excludeAmounts.Contains((int)CommonTypes.TransactionAmountType.FeeTransaction))
                    {
                        AddInvoiceLine(payId, rsData.ID, rsData.CurrencyVal.Value, curExchangeRate, nTextList[2] + textAdd, "---", gTotal.NormCount, gTotal.NormRatioFee, userName);
                    }

                    AddInvoiceLine(payId, rsData.ID, rsData.CurrencyVal.Value, curExchangeRate, nTextList[3] + textAdd, "---", gTotal.FinanceCount, gTotal.FinanceFee, userName);

                    if (!excludeAmounts.Contains((int)CommonTypes.TransactionAmountType.FeeClarification))
                    {
                        AddInvoiceLine(payId, rsData.ID, rsData.CurrencyVal.Value, curExchangeRate, nTextList[4] + textAdd, "---", gTotal.ClrfCount, gTotal.ClrfFees, userName);
                    }

                    if (!excludeAmounts.Contains((int)CommonTypes.TransactionAmountType.FeeChb))
                    {
                        AddInvoiceLine(payId, rsData.ID, rsData.CurrencyVal.Value, curExchangeRate, nTextList[5] + textAdd, "---", gTotal.DenCount, gTotal.DenFees, userName);
                    }

                    if (!excludeAmounts.Contains((int)CommonTypes.TransactionAmountType.SystemCharge))
                    {
                        AddInvoiceLine(payId, rsData.ID, rsData.CurrencyVal.Value, curExchangeRate, nTextList[6] + textAdd, "---", (int)AmountSum.FeesAutoAndManual.TotalCount, AmountSum.FeesAutoAndManual.TotalFees, userName);
                    }

                    if (!excludeAmounts.Contains((int)CommonTypes.TransactionAmountType.HandlingFee))
                    {
                        AddInvoiceLine(payId, rsData.ID, rsData.CurrencyVal.Value, curExchangeRate, nTextList[7] + textAdd, "---", gTotal.HandlingCount, gTotal.HandlingFee, userName);
                    }

                    // Meir test
                    //if (!excludeAmounts.Contains((int)CommonTypes.TransactionAmountType.FeeLine)) // TAT_TransactionFee = 1002
                    //{
                    //    AddInvoiceLine(payId, rsData.ID, rsData.CurrencyVal.Value, curExchangeRate, nTextList[0] + textAdd, "---", AmountSum.Totals.NormCount, AmountSum.Totals.NormLineFee, userName);
                    //}

                    //if (!excludeAmounts.Contains((int)CommonTypes.TransactionAmountType.FeeRefund))
                    //{
                    //    AddInvoiceLine(payId, rsData.ID, rsData.CurrencyVal.Value, curExchangeRate, nTextList[1] + textAdd, "---", AmountSum.Totals.RefCount, AmountSum.Totals.RefFees, userName);
                    //}

                    //if (!excludeAmounts.Contains((int)CommonTypes.TransactionAmountType.FeeTransaction))
                    //{
                    //    AddInvoiceLine(payId, rsData.ID, rsData.CurrencyVal.Value, curExchangeRate, nTextList[2] + textAdd, "---", AmountSum.Totals.NormCount, AmountSum.Totals.NormFees, userName);
                    //}

                    //AddInvoiceLine(payId, rsData.ID, rsData.CurrencyVal.Value, curExchangeRate, nTextList[3] + textAdd, "---", AmountSum.Totals.FinanceCount, AmountSum.Totals.FinanceFee, userName);

                    //if (!excludeAmounts.Contains((int)CommonTypes.TransactionAmountType.FeeClarification))
                    //{
                    //    AddInvoiceLine(payId, rsData.ID, rsData.CurrencyVal.Value, curExchangeRate, nTextList[4] + textAdd, "---", AmountSum.Totals.ClrfCount, AmountSum.Totals.ClrfFees, userName);
                    //}

                    //if (!excludeAmounts.Contains((int)CommonTypes.TransactionAmountType.FeeChb))
                    //{
                    //    AddInvoiceLine(payId, rsData.ID, rsData.CurrencyVal.Value, curExchangeRate, nTextList[5] + textAdd, "---", AmountSum.Totals.DenCount, AmountSum.Totals.DenFees, userName);
                    //}

                    //if (!excludeAmounts.Contains((int)CommonTypes.TransactionAmountType.SystemCharge))
                    //{
                    //    AddInvoiceLine(payId, rsData.ID, rsData.CurrencyVal.Value, curExchangeRate, nTextList[6] + textAdd, "---", (int)AmountSum.FeesAutoAndManual.TotalCount, AmountSum.FeesAutoAndManual.TotalFees, userName);
                    //}

                    //if (!excludeAmounts.Contains((int)CommonTypes.TransactionAmountType.HandlingFee))
                    //{
                    //    AddInvoiceLine(payId, rsData.ID, rsData.CurrencyVal.Value, curExchangeRate, nTextList[7] + textAdd, "---", AmountSum.Totals.HandlingCount, AmountSum.Totals.HandlingFee, userName);
                    //}


                    //CreateInvoiceDocument Execution - 
                    InvoiceDocumentCreate(payId, excludeAmounts.Count() > 0 ? 1 : 0, rsData.BillingCompanys_id.Value, rsData.isChargeVAT, false, rsData.ID, "", invCurrency.Value, decimal.Parse(curExchangeRate.ToString("F4")), userName);

                    return DataContext.Writer.tblInvoiceDocuments.Where(x => x.id_TransactionPayID == payId).Select(x => x.id_InvoiceNumber).ToString();
                }
            }
        }
    }
}
