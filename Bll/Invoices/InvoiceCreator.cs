﻿using System;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Invoices
{
	public static class InvoiceCreator
    {
		public class InvoiceData
		{
			public string InvoiceItemName { get; set; }
			public bool IncludeTax { get; set; }

			public CommonTypes.Currency Curreny { get; set; }
			public decimal TaxRate { get; set; }
			public decimal Amount { get; set; }
			public decimal NoTaxAmount { get { if (TaxRate == 0) return Amount; return System.Math.Round(Amount / (1 + (TaxRate / 100)), 2); } }
			public decimal TaxAmount { get { if (TaxRate == 0) return 0; return Amount - NoTaxAmount; } }

			public decimal Amount_ILS { get; set; }
			public decimal NoTaxAmount_ILS { get { if (TaxRate == 0) return Amount_ILS; return System.Math.Round(Amount_ILS / (1 + (TaxRate / 100)), 2); } }
			public decimal TaxAmount_ILS { get { if (TaxRate == 0) return 0; return Amount_ILS - NoTaxAmount_ILS; } }
			public decimal ConvertionRate { get { return Amount_ILS / Amount; } }
		}

		public static InvoiceData GetInvoiceData(Transactions.Transaction ctp, MerchantSettings csi)
		{
			var invData = new InvoiceData();
			invData.IncludeTax = false;
			invData.Amount = ctp.Amount;
			invData.Curreny = (CommonTypes.Currency) ctp.CurrencyID;
			invData.InvoiceItemName = string.IsNullOrEmpty(ctp.PayForText) ? csi.ItemText : ctp.PayForText; 
			if (ctp.CurrencyID == (int)CommonTypes.Currency.ILS)
			{
				invData.Amount_ILS = invData.Amount;
				if (csi.IsIncludeTax) {
					invData.TaxRate = 100 * (decimal)(from b in DataContext.Reader.tblBillingCompanies where b.BillingCompanys_id == ctp.Merchant.BillingCompanysId select b.VATamount).SingleOrDefault();
					invData.IncludeTax = true;
				}
			} else {
				var orAmount = new Money(ctp.Amount, ctp.CurrencyID);
				invData.Amount_ILS = orAmount.ConvertTo(CommonTypes.Currency.ILS).Amount;
				invData.InvoiceItemName += string.Format(" ({0})", orAmount.ToString());
			}
			if (ctp.CreditType == (int)CreditType.Refund) {
				invData.Amount = -invData.Amount;
				invData.Amount_ILS = -invData.Amount_ILS;
			}
			return invData;
		}
		
		public static bool IsEnabled(int merchantID)
        {
			return (from c in DataContext.Reader.SetMerchantInvoices where c.Merchant_id == merchantID select c.IsEnable).SingleOrDefault();
        }

        public static int? InvoiceStatus(int transID, out string invoiceUrl)
        {
			invoiceUrl = null;
            var hist = (from h in DataContext.Reader.TransHistories where h.TransPass_id == transID && h.TransHistoryType_id == (byte)CommonTypes.TransactionHistoryType.CreateInvoice select h).SingleOrDefault();
			if (hist == null) return null;
			invoiceUrl = hist.ReferenceUrl;
			return hist.ReferenceNumber;
        }

		public static int? CreateInvoice(int transID) 
        {
            Transactions.Transaction ctp = Transactions.Transaction.GetTransaction(null, transID, TransactionStatus.Captured);
            if (ctp == null)
                return null;

            if (ctp.CreditType == CreditType.Refund)
                return null;

            //var user = Infrastructure.Security.SecurityManager.GetInternalUser(credentialsToken);
            var hist = (from h in DataContext.Reader.TransHistories where h.TransPass_id == transID && h.TransHistoryType_id == (byte)CommonTypes.TransactionHistoryType.CreateInvoice select h).SingleOrDefault();
			if (hist != null)
                return hist.ReferenceNumber;

			if (ctp == null) throw new ApplicationException("transaction not found");
            if (ctp.Merchant == null) throw new ApplicationException("transaction.Merchant not found");
            var csi = MerchantSettings.Load(ctp.MerchantID.Value);
            if (csi == null || !csi.IsEnable) throw new ApplicationException(string.Format("Company {0} does not support Invoices", ctp.MerchantID));
            switch ((InvoicesProvider)csi.ExternalProviderID)
            {
				case InvoicesProvider.Invoice4u: return Invoice4u.CreateInvoice(ctp, csi);
				case InvoicesProvider.Tamal: return Tamal.CreateInvoice(ctp, csi);
				case InvoicesProvider.iCount: return iCount.CreateInvoice(ctp, csi);
				case InvoicesProvider.Local: return Local.CreateInvoice(ctp, csi);
			}

            return null;
        }

        public static void AddTransInvoiceNumber(Transactions.Transaction ctp, int docNumber, string description, string referenceUrl) 
        {
            TransHistory ta = new TransHistory();
			ta.InsertDate = DateTime.Now;
			ta.Merchant_id = ctp.MerchantID.GetValueOrDefault();
            ta.TransPass_id = ctp.ID;
			ta.TransHistoryType_id = (byte) CommonTypes.TransactionHistoryType.CreateInvoice;
			ta.IsSucceeded = true;
            ta.Description = description;
            ta.ReferenceUrl = referenceUrl;
            ta.ReferenceNumber = docNumber;
			DataContext.Writer.TransHistories.InsertOnSubmit(ta);
			DataContext.Writer.SubmitChanges();
        }

        public static void AddInvoiceLog(int merchantId, string action, string successCode, string request, string response)
        {
            DataContext.Writer.Log_SpInsertHistory(3, merchantId, action, successCode, request, response, null, null, null, null);
        }
    }
}
