﻿using System;
using System.Linq;
using System.Text;
using Netpay.Dal.Netpay;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll
{
    public static class Invoices
    {
		public class InvoiceData
		{
			public string InvoiceItemName { get; set; }
			public bool IncludeTax { get; set; }

			public CommonTypes.Currency Curreny { get; set; }
			public decimal TaxRate { get; set; }
			public decimal Amount { get; set; }
			public decimal NoTaxAmount { get { if (TaxRate == 0) return Amount; return System.Math.Round(Amount / (1 + (TaxRate / 100)), 2); } }
			public decimal TaxAmount { get { if (TaxRate == 0) return 0; return Amount - NoTaxAmount; } }

			public decimal Amount_ILS { get; set; }
			public decimal NoTaxAmount_ILS { get { if (TaxRate == 0) return Amount_ILS; return System.Math.Round(Amount_ILS / (1 + (TaxRate / 100)), 2); } }
			public decimal TaxAmount_ILS { get { if (TaxRate == 0) return 0; return Amount_ILS - NoTaxAmount_ILS; } }
			public decimal ConvertionRate { get { return Amount_ILS / Amount; } }
		}

		public static InvoiceData GetInvoiceData(string domainHost, Transaction.Transaction ctp, Infrastructure.VO.SetMerchantInvoiceVO csi)
		{
			var invData = new InvoiceData();
			Infrastructure.Domains.Domain domain = Infrastructure.Domains.DomainsManager.GetDomain(domainHost);
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			invData.IncludeTax = false;
			invData.Amount = ctp.Amount;
			invData.Curreny = (CommonTypes.Currency) ctp.CurrencyID;
			invData.InvoiceItemName = string.IsNullOrEmpty(ctp.PayForText) ? csi.ItemText : ctp.PayForText; 
			if (ctp.CurrencyID == (int)CommonTypes.Currency.ILS)
			{
				invData.Amount_ILS = invData.Amount;
				if (csi.IsIncludeTax) {
					invData.TaxRate = 100 * (decimal)(from b in dc.tblBillingCompanies where b.BillingCompanys_id == ctp.Merchant.BillingCompanysId select b.VATamount).SingleOrDefault();
					invData.IncludeTax = true;
				}
			} else {
				var orAmount = new Money(domain, ctp.Amount, ctp.CurrencyID);
				invData.Amount_ILS = orAmount.ConvertTo(domain, CommonTypes.Currency.ILS).Amount;
				invData.InvoiceItemName += string.Format(" ({0})", orAmount.ToString());
			}
			if (ctp.CreditType == (int)CreditType.Refund) {
				invData.Amount = -invData.Amount;
				invData.Amount_ILS = -invData.Amount_ILS;
			}
			return invData;
		}
		
		public static bool IsEnabled(string domainHost, int merchantID)
        {
            NetpayDataContext dc = new NetpayDataContext(Infrastructure.Domains.DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
            return (from c in dc.SetMerchantInvoices where c.Merchant_id == merchantID select c.IsEnable).SingleOrDefault();
        }

        public static int? InvoiceStatus(string domainHost, int transID, out string invoiceUrl)
        {
            NetpayDataContext dc = new NetpayDataContext(Infrastructure.Domains.DomainsManager.GetDomain(domainHost).Sql1ConnectionString);
			invoiceUrl = null;
            var hist = (from h in dc.TransHistories where h.TransPass_id == transID && h.TransHistoryType_id == (byte)CommonTypes.TransactionHistoryType.CreateInvoice select h).SingleOrDefault();
			if (hist == null) return null;
			invoiceUrl = hist.ReferenceUrl;
			return hist.ReferenceNumber;
        }

		public static int? CreateInvoice(Guid credentialsToken, int transID) 
        {
			var user = Infrastructure.Security.SecurityManager.GetInternalUser(credentialsToken);
			NetpayDataContext dc = new NetpayDataContext(user.Domain.Sql1ConnectionString);
			var hist = (from h in dc.TransHistories where h.TransPass_id == transID && h.TransHistoryType_id == (byte)CommonTypes.TransactionHistoryType.CreateInvoice select h).SingleOrDefault();
			if (hist != null) return hist.ReferenceNumber;
			Transaction.Transaction ctp = Transaction.Transaction.GetTransaction(credentialsToken, null, transID, TransactionStatus.Captured);
			if (ctp == null) throw new ApplicationException("transaction not found");
            if (ctp.Merchant == null) throw new ApplicationException("transaction.Merchant not found");
            Infrastructure.VO.SetMerchantInvoiceVO csi = (from c in dc.SetMerchantInvoices where c.Merchant_id == ctp.MerchantID select new Infrastructure.VO.SetMerchantInvoiceVO(c)).SingleOrDefault();
            if (csi == null || !csi.IsEnable) throw new ApplicationException(string.Format("Company {0} does not support Invoices", ctp.MerchantID));
            switch ((InvoicesProvider)csi.ExternalProviderID)
            {
				case InvoicesProvider.Invoice4u: return Invoice4u.CreateInvoice(user.Domain.Host, ctp, csi);
				case InvoicesProvider.Tamal: return Tamal.CreateInvoice(user.Domain.Host, ctp, csi);
				case InvoicesProvider.iCount: return iCount.CreateInvoice(user.Domain.Host, ctp, csi);
				case InvoicesProvider.Local: return Local.CreateInvoice(user.Domain.Host, ctp, csi);
			}
            return null;
        }

        public static void AddTransInvoiceNumber(string domainHost, Transaction.Transaction ctp, int docNumber, string description, string referenceUrl) 
        {
            Infrastructure.Domains.Domain domain = Infrastructure.Domains.DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            TransHistory ta = new TransHistory();
			ta.InsertDate = DateTime.Now;
			ta.Merchant_id = ctp.MerchantID.GetValueOrDefault();
            ta.TransPass_id = ctp.ID;
			ta.TransHistoryType_id = (byte) CommonTypes.TransactionHistoryType.CreateInvoice;
			ta.IsSucceeded = true;
            ta.Description = description;
            ta.ReferenceUrl = referenceUrl;
            ta.ReferenceNumber = docNumber;
            dc.TransHistories.InsertOnSubmit(ta);
			dc.SubmitChanges();
        }

        public static void AddInvoiceLog(string domainHost, int merchantId, string action, string successCode, string request, string response)
        {
            Infrastructure.Domains.Domain domain = Infrastructure.Domains.DomainsManager.GetDomain(domainHost);
            NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
            dc.Log_SpInsertHistory(3, merchantId, action, successCode, request, response, null, null, null, null);
        }
    }
}
