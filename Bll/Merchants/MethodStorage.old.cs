﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll.Merchants
{
	public class MethodStorage : BaseDataObject, PaymentMethods.IStoredMethod /*, IAddress */
	{
		public const string SecuredObjectName = "MethodStorage";
		public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Merchants.Module.Current, SecuredObjectName); } }

		public class SearchFilters
		{
			public Infrastructure.Range<int?> ID;
			public Infrastructure.Range<DateTime?> InsertDate;
			public CommonTypes.PaymentMethodEnum? PaymentMethod;
			public Infrastructure.Range<DateTime?> PM_ExpirationDate;
			public int? MerchantID;
			public bool? IsActive;
			public string Owner_Name;
			public string PM_First6;
			public string PM_Last4;
			public string Value1;
		}

		private Dal.Netpay.tblCCStorage _entity;
		private string _firstName, _lastName;

		public int ID { get { return _entity.ID; } set { _entity.ID = value; } }
		public string ReferenceCode { get { return null; /*_entity.ReferenceCode; */} set { /*_entity.ReferenceCode = value;*/ } }
		public int Merchant_id { get { return _entity.companyID.GetValueOrDefault(); } set { _entity.companyID = value; } }
		public int? Settlement_id { get { return _entity.payID; } set { _entity.payID = value.GetValueOrDefault(); } }
		public string Comment { get { return _entity.Comment; } set { _entity.Comment = value; } }
		public System.DateTime InsertDate { get { return _entity.InsertDate; } set { _entity.InsertDate = value; } }
		public bool IsActive { get { return _entity.isDeleted; } set { _entity.isDeleted = value; } }
		public string IPAddress { get { return _entity.IPAddress; } set { _entity.IPAddress = value; } }

		/* PaymentMethods.IStoredMethod */
		System.DateTime? PaymentMethods.IStoredMethod.ExpirationDate {
			get { int nYear = _entity.ExpYY.ToNullableInt().GetValueOrDefault(2000); if (nYear < 100) nYear += 2000; return new DateTime(nYear, _entity.ExpMM.ToNullableInt().GetValueOrDefault(1), 1).AddMonths(1).AddDays(-1); }
			set { if (value == null) { _entity.ExpYY = _entity.ExpMM = null; return; } _entity.ExpYY = value.Value.Year.ToString(); _entity.ExpMM = value.Value.Month.ToString(); } 
		}
		int PaymentMethods.IStoredMethod.PaymentMethodId { get { return _entity.PaymentMethod; } set { _entity.PaymentMethod = (short)value; } }
		System.Data.Linq.Binary PaymentMethods.IStoredMethod.Value1Encrypted { get { return _entity.CCard_number256; } set { _entity.CCard_number256 = value; } }
		System.Data.Linq.Binary PaymentMethods.IStoredMethod.Value2Encrypted { get { return Infrastructure.Security.Encryption.Encrypt(_entity.cc_cui); } set { _entity.cc_cui = Infrastructure.Security.Encryption.Decrypt(value.ToArray()); } }
		string PaymentMethods.IStoredMethod.Value1First6Data { get; set; /*{ return _entity.6; } set { _entity.PM_Value1First6Text = value; } */}
		string PaymentMethods.IStoredMethod.Value1Last4Data { get { return _entity.CCardLast4; } set { _entity.CCardLast4 = value; } }
		byte PaymentMethods.IStoredMethod.EncryptionKey { get { return (byte)Domain.Current.EncryptionKeyNumber; } set { /*_entity.PM_EncryptionKey = value;*/ } }
		public string PM_IssuerCountryIsoCode { get { return _entity.BINCountry; } set { _entity.BINCountry = value; } }
		public PaymentMethods.MethodData MethodInstance { get; set; }

		public string Owner_FirstName { get { return _firstName; } set {_firstName = value; } }
		public string Owner_LastName { get { return _lastName; } set { _lastName = value; } }
		public string Owner_InvoiceName { get { return null; /*_entity.CH_InvoiceName; */ } set { /*_entity.CH_InvoiceName = value;*/ } }
		public string Owner_PersonalNumber { get { return _entity.CHPersonalNum; } set { _entity.CHPersonalNum = value; } }
		public string Owner_PhoneNumber { get { return _entity.CHPhoneNumber; } set { _entity.CHPhoneNumber = value; } }
		public string Owner_EmailAddress { get { return _entity.CHEmail; } set { _entity.CHEmail = value; } }
		public System.DateTime? Owner_DateOfBirth { get { return null;/* return null; */ } set { /*_entity.CH_DateOfBirth = value; */} }
		public string Owner_FullName
		{
			get { return PersonalInfo.MakeFullName(_firstName, _lastName); }
			set { PersonalInfo.SplitFullName(value, out _firstName, out _lastName); }
		}

		public string BA_Street1 { get { return _entity.CHStreet; } set { _entity.CHStreet = value; } }
		public string BA_Street2 { get { return _entity.CHStreet1; } set { _entity.CHStreet1 = value; } }
		public string BA_City { get { return _entity.CHSCity; } set { _entity.CHSCity = value; } }
		public string BA_PostalCode { get { return _entity.CHSZipCode; } set { _entity.CHSZipCode = value; } }
		public string BA_StateISOCode { get { if(_entity.stateId == null) return null; return State.Get(_entity.stateId.Value).IsoCode; } set { if(string.IsNullOrEmpty(value)) _entity.stateId = null; else _entity.stateId = State.Get(value).ID; } }
		public string BA_CountryISOCode { get { if (_entity.countryId == null) return null; return Country.Get(_entity.countryId.Value).IsoCode2; } set { if (string.IsNullOrEmpty(value)) _entity.countryId = null; else _entity.countryId = Country.Get(value).ID; } }

		private MethodStorage(Netpay.Dal.Netpay.tblCCStorage entity)
		{ 
			_entity = entity;
			MethodInstance = PaymentMethods.MethodData.Create(this);
			PersonalInfo.SplitFullName(_entity.CHFullName, out _firstName, out _lastName);
		}

		public MethodStorage(int merchantID)
		{
			_entity = new Dal.Netpay.tblCCStorage();
			_entity.InsertDate = DateTime.Now;
			_entity.companyID = merchantID;
			MethodInstance = PaymentMethods.MethodData.Create(this);
		}

		public static MethodStorage Load(int merchantID, int id)
		{
			//context.IsUserOfType(new UserType[] { UserType.MerchantPrimary, UserType.NetpayAdmin, UserType.NetpayUser });
			var entity = (from pt in DataContext.Reader.tblCCStorages where pt.companyID == merchantID && pt.ID == id select pt).SingleOrDefault();
			if (entity == null) return null;
			return new MethodStorage(entity);
		}

		public void Save()
		{
			_entity.CHFullName = PersonalInfo.MakeFullName(_firstName, _lastName);
			if (MethodInstance!= null) _entity.CCard_display = MethodInstance.Display;
			if (_entity.IPAddress == null) _entity.IPAddress = "";
			DataContext.Writer.tblCCStorages.Update(_entity, (_entity.ID != 0));
			DataContext.Writer.SubmitChanges();
		}

		public static void MarkIsActive(List<int> Ids, bool value)
		{
            string joinIds = string.Join(",", Ids);
            string sql = string.Format("Update tblCCStorage Set IsDeleted={0} Where ID IN({1})", value ? 0 : 1, joinIds);
            DataContext.Writer.ExecuteCommand(sql);
		}

		public static List<MethodStorage> Search(SearchFilters filters, ISortAndPage sortAndPage)
		{
			var exp = (from t in DataContext.Reader.tblCCStorages.Where(Accounts.AccountFilter.Current.QueryValidate(Accounts.AccountType.Merchant, (Dal.Netpay.tblCCStorage x) => x.companyID)) select t);
			if (filters != null) 
			{
				if (filters.ID.From.HasValue) exp = exp.Where(t => t.ID >= filters.ID.From.Value);
				if (filters.ID.To.HasValue) exp = exp.Where(t => t.ID <= filters.ID.To.Value);
				if (filters.InsertDate.From.HasValue) exp = exp.Where(t => t.InsertDate >= filters.InsertDate.From.Value);
				if (filters.InsertDate.To.HasValue) exp = exp.Where(t => t.InsertDate <= filters.InsertDate.To.Value.AlignToEnd());
				if (filters.MerchantID.HasValue) exp = exp.Where(t => t.companyID == filters.MerchantID.Value);
				if (filters.PaymentMethod.HasValue) exp = exp.Where(t => t.PaymentMethod == (short)filters.PaymentMethod);
                if (filters.IsActive.HasValue) exp = exp.Where(t => t.isDeleted == !filters.IsActive.Value);
                
                //if (filters.PM_First6 != null) exp = exp.Where(t => t.bin == filters.PM_First6);
                if (filters.PM_Last4 != null) exp = exp.Where(t => t.CCardLast4 == filters.PM_Last4);
				//if (filters.PM_ExpirationDate.From.HasValue) exp = exp.Where(t => t.ExpMM >= filters.PM_ExpirationDate.From.Value.Month.ToString() && t.ExpYY >= filters.PM_ExpirationDate.From.Value.Year.ToString());
				//if (filters.PM_ExpirationDate.To.HasValue) exp = exp.Where(t => t.ExpMM <= filters.PM_ExpirationDate.To.Value);
				if (filters.Value1 != null) exp = exp.Where(t => t.CCard_number256 == Infrastructure.Security.Encryption.Encrypt(filters.Value1));
                //if (filters.cardHolderName != null)
                //	expression = expression.Where(ccs => ccs.CHFullName.StartsWith(filters.cardHolderName.Trim()));
            }
			return exp.ApplySortAndPage(sortAndPage).Select(t => new MethodStorage(t)).ToList();
		}
	}
}
