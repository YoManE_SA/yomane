﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Bll.Accounts;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Merchants
{
    public class ProcessTerminals : BaseDataObject
    {
        public const string SecuredObjectName = "ProcessTerminals";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Merchants.Module.Current, SecuredObjectName); } }

        public class TerminalReference
        {
            private Dal.Netpay.tblCompanyCreditFeesTerminal _entity;
            public int ID { get { return _entity.CCFT_ID; } }
            public string TerminalNumber { get { return _entity.CCFT_Terminal; } set { _entity.CCFT_Terminal = value; } }
            public int Ratio { get { return _entity.CCFT_Ratio; } set { _entity.CCFT_Ratio = value; } }
            public int UseCount { get { return _entity.CCFT_UseCount; } }
            public string Text { get; private set; }

            internal TerminalReference(Netpay.Dal.Netpay.tblCompanyCreditFeesTerminal entity, string text) { _entity = entity; Text = text; }
            public TerminalReference() { _entity = new Netpay.Dal.Netpay.tblCompanyCreditFeesTerminal(); }

            internal void Save(Dal.DataAccess.NetpayDataContext DataContext, ProcessTerminals tr)
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

                _entity.CCFT_CCF_ID = tr.ID;
                _entity.CCFT_CompanyID = tr.MerchantID;
                if (_entity.CCFT_ID != 0)
                {
                    if (DataContext.tblCompanyCreditFeesTerminals.GetOriginalEntityState(_entity) == null)
                        DataContext.tblCompanyCreditFeesTerminals.Attach(_entity);
                    DataContext.Refresh(System.Data.Linq.RefreshMode.KeepCurrentValues, _entity);
                }
                else
                {
                    DataContext.tblCompanyCreditFeesTerminals.InsertOnSubmit(_entity);
                }
            }

            public static TerminalReference Get(int id)
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return DataContext.Reader.tblCompanyCreditFeesTerminals
                    .Where(x => x.CCFT_ID == id)
                    .Select(x => new TerminalReference(x, ""))
                    .SingleOrDefault();
            }


            public void Delete()
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

                //If it's new item(was not saved yet) , return
                if (_entity.CCFT_ID == 0) return;

                DataContext.Writer.tblCompanyCreditFeesTerminals.Delete(_entity);
                DataContext.Writer.SubmitChanges();
            }
        }

        public enum RefTerminalSelectionMode { RatioSelection = 0, BW_ListSelection = 1, PriorityList = 2, MerchantControl = 3 }

        private Dal.Netpay.tblCompanyCreditFee _entity;
        public int ID { get { return _entity.CCF_ID; } }
        public int MerchantID { get { return _entity.CCF_CompanyID; } }
        public int CurrencyID { get { return _entity.CCF_CurrencyID; } set { _entity.CCF_CurrencyID = value; } }
        public CommonTypes.PaymentMethodEnum? PaymentMethodId { get { return (CommonTypes.PaymentMethodEnum?)_entity.CCF_PaymentMethod; } set { _entity.CCF_PaymentMethod = (short)value; } }
        //public int CreditTypeID { get { return _entity.CCF_CreditTypeID; } set { _entity.CCF_CreditTypeID = value; } }
        //public string TerminalNumber { get { return _entity.CCF_TerminalNumber; } set { _entity.CCF_TerminalNumber = value; } }
        //public decimal MaxAmount { get { return _entity.CCF_MaxAmount; } set { _entity.CCF_MaxAmount = value; } }
        public bool IsDisabled { get { return _entity.CCF_IsDisabled; } set { _entity.CCF_IsDisabled = value; } }
        public string ListBINs { get { return _entity.CCF_ListBINs; } set { _entity.CCF_ListBINs = value; } }
        public int ExchangeTo { get { return _entity.CCF_ExchangeTo; } set { _entity.CCF_ExchangeTo = (short)value; } }
        public decimal PercentFee { get { return _entity.CCF_PercentFee; } set { _entity.CCF_PercentFee = value; } }
        public decimal FixedFee { get { return _entity.CCF_FixedFee; } set { _entity.CCF_FixedFee = value; } }
        public decimal ApproveFixedFee { get { return _entity.CCF_ApproveFixedFee; } set { _entity.CCF_ApproveFixedFee = value; } }
        public decimal RefundFixedFee { get { return _entity.CCF_RefundFixedFee; } set { _entity.CCF_RefundFixedFee = value; } }
        public decimal PartialRefundFixedFee { get { return _entity.CCF_PartialRefundFixedFee; } set { _entity.CCF_PartialRefundFixedFee = value; } }
        public decimal FraudRefundFixedFee { get { return _entity.CCF_FraudRefundFixedFee; } set { _entity.CCF_FraudRefundFixedFee = value; } }
        public decimal ClarificationFee { get { return _entity.CCF_ClarificationFee; } set { _entity.CCF_ClarificationFee = value; } }
        public decimal CBFixedFee { get { return _entity.CCF_CBFixedFee; } set { _entity.CCF_CBFixedFee = value; } }
        public decimal FailFixedFee { get { return _entity.CCF_FailFixedFee; } set { _entity.CCF_FailFixedFee = value; } }
        public decimal CashbackPercent { get { return _entity.CCF_PercentCashback; } set { _entity.CCF_PercentCashback = value; } }
        public byte TSelMode { get { return _entity.CCF_TSelMode; } set { _entity.CCF_TSelMode = value; } }

        public PaymentMethods.PaymentMethod PaymentMethod { get { return PaymentMethods.PaymentMethod.Get(PaymentMethodId.GetValueOrDefault()); } }

        private ProcessTerminals(Netpay.Dal.Netpay.tblCompanyCreditFee entity)
        {
            _entity = entity;
        }

        public ProcessTerminals(int merchantID)
        {
            _entity = new Dal.Netpay.tblCompanyCreditFee();
            _entity.CCF_TerminalNumber = "";
            _entity.CCF_CompanyID = merchantID;
        }

        public static ProcessTerminals Load(int refTerminalId)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);

            var entity = (from mmas in DataContext.Reader.tblCompanyCreditFees where mmas.CCF_ID == refTerminalId select mmas).SingleOrDefault();
            if (entity == null) return null;
            return new ProcessTerminals(entity);
        }

        public static List<ProcessTerminals> LoadForMerchant(int merchantId, bool? isActive)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);

            AccountFilter.Current.Validate(Accounts.AccountType.Merchant, merchantId);
            var exp = (from mmas in DataContext.Reader.tblCompanyCreditFees where mmas.CCF_CompanyID == merchantId select mmas);
            if (isActive.HasValue) exp = exp.Where(mmas => mmas.CCF_IsDisabled == !isActive.Value);
            return exp.Select(mmas => new ProcessTerminals(mmas)).ToList();
        }

        public static ProcessTerminals LoadForMerchantByProcessTerminalId(int merchantId, int ccfId)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);

            AccountFilter.Current.Validate(Accounts.AccountType.Merchant, merchantId);
            var exp = (from mmas in DataContext.Reader.tblCompanyCreditFees where mmas.CCF_ID == ccfId && mmas.CCF_CompanyID == merchantId select mmas);
            return exp.Select(mmas => new ProcessTerminals(mmas)).SingleOrDefault();
        }

        public static ProcessTerminals GetTerminalForTransaction(int merchantId, CommonTypes.Currency currency, CommonTypes.PaymentMethodEnum paymentMethod, string binCountry)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            string countrySearch = string.Format(",{0},", binCountry);
            return (from n in DataContext.Reader.tblCompanyCreditFees
                    where n.CCF_CompanyID == merchantId
                        && n.CCF_CurrencyID == (int)currency
                        && n.CCF_PaymentMethod == (short)paymentMethod
                        && (n.CCF_ListBINs == null || n.CCF_ListBINs == "" || n.CCF_ListBINs.Contains(countrySearch))
                    orderby n.CCF_IsDisabled ascending, n.CCF_ListBINs descending
                    select new ProcessTerminals(n)).FirstOrDefault();
        }

        public void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            DataContext.Writer.tblCompanyCreditFees.Update(_entity, (_entity.CCF_ID != 0));
            //save terminals
            DataContext.Writer.SubmitChanges();
            if (_terminals != null)
            {
                DataContext.Writer.tblCompanyCreditFeesTerminals.DeleteAllOnSubmit((from t in DataContext.Writer.tblCompanyCreditFeesTerminals where t.CCFT_CCF_ID == _entity.CCF_ID && !_terminals.Select(b => b.ID).Contains(t.CCFT_ID) select t));
                foreach (var t in _terminals)
                {
                    if (!string.IsNullOrEmpty(t.TerminalNumber))
                        t.Save(DataContext.Writer, this);
                }
                DataContext.Writer.SubmitChanges();
            }
        }

        public void Delete()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            if (_entity.CCF_ID == 0) return;
            DataContext.Writer.tblCompanyCreditFeesTerminals.DeleteAllOnSubmit((from t in DataContext.Writer.tblCompanyCreditFeesTerminals where t.CCFT_CCF_ID == _entity.CCF_ID select t));
            DataContext.Writer.tblCompanyCreditFees.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }

        private List<TerminalReference> _terminals;
        public List<TerminalReference> Terminals
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                if (_terminals != null) return _terminals;
                _terminals =
                    (from ccft in DataContext.Reader.tblCompanyCreditFeesTerminals
                     join trm in DataContext.Reader.tblDebitTerminals on ccft.CCFT_Terminal equals trm.terminalNumber
                     join dcn in DataContext.Reader.tblDebitCompanies on trm.DebitCompany equals dcn.DebitCompany_ID
                     where ccft.CCFT_CCF_ID == _entity.CCF_ID
                     select new TerminalReference(ccft, string.Format("{0} - {1}", dcn.dc_name, trm.dt_name))).ToList();
                return _terminals;
            }
            set
            {
                _terminals = value;
            }
        }

        public class TerminalSummaryItem : DebitCompanies.Terminal.TerminalInfo
        {
            public int CurrencyId { get; set; }
            public int? PaymentMethodId { get; set; }
            public PaymentMethods.PaymentMethod PaymentMethod { get { return PaymentMethods.PaymentMethod.Get(PaymentMethodId.GetValueOrDefault()); } }

        }

        public static List<TerminalSummaryItem> GetTerminalSummary(int merchantId, bool? isActive)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            AccountFilter.Current.Validate(Accounts.AccountType.Merchant, merchantId);
            var retList = (
                from ccft in DataContext.Reader.tblCompanyCreditFeesTerminals
                join ccf in DataContext.Reader.tblCompanyCreditFees on ccft.CCFT_CCF_ID equals ccf.CCF_ID
                join trm in DataContext.Reader.tblDebitTerminals on ccft.CCFT_Terminal equals trm.terminalNumber
                join dcn in DataContext.Reader.tblDebitCompanies on trm.DebitCompany equals dcn.DebitCompany_ID
                where ccf.CCF_CompanyID == merchantId && (isActive.HasValue ? (ccf.CCF_IsDisabled == !isActive.Value && ccft.CCFT_Ratio > 0) : true)
                orderby ccf.CCF_ID
                select new TerminalSummaryItem()
                {
                    CurrencyId = ccf.CCF_CurrencyID,
                    PaymentMethodId = ccf.CCF_PaymentMethod,
                    DebitCompanyId = trm.DebitCompany,
                    DebitCompanyName = dcn.dc_name,
                    TerminalId = trm.id,
                    TerminalName = trm.dt_name,
                    IsActive = trm.isActive,
                    TerminalNumber = ccf.CCF_TerminalNumber
                }).ToList();
            return retList;
        }
    }
}
