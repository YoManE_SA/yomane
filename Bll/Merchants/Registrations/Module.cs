﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.Bll.Merchants.Registrations
{
    public class Module : Infrastructure.Module
    {
        public const string ModuleName = "Registration";
        public override string Name { get { return ModuleName; } }
        public override decimal Version { get { return 1.0m; } }
        public static Module Current { get { return Infrastructure.Module.Get(ModuleName) as Module; } }

        protected override void OnActivate(EventArgs e)
        {
            EmailTemplates.RegisterTemplates(ModuleName);

            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {
            EmailTemplates.UnRegisterTemplates(ModuleName);

            base.OnDeactivate(e);
        }

        protected override void OnInstall(EventArgs e)
        {
            Infrastructure.Security.SecuredObject.Create(this, Registration.SecuredObjectName, Infrastructure.Security.PermissionGroup.ReadEditDelete);
            base.OnInstall(e);
        }

        protected override void OnUninstall(EventArgs e)
        {
            Infrastructure.Security.SecuredObject.Remove(this);
            base.OnUninstall(e);
        }
    }
}
