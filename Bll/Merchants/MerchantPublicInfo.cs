﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll.Merchants
{
	public class MerchantPublicInfo : Infrastructure.BaseDataObject
	{
		public class SearchFilters
		{
			public List<int> ID;
			public string MerchantNumber;
			public string ShopSubDomain;
            public string Text;
            public int? Department;
			public int? Group;
			public int? ApplicationIdentityId;
			public Netpay.Infrastructure.MerchantStatus? Status;
            public bool? IsShopEnabled;
            public bool? HasActiveShops;
        }

		
		public int AccountID { private set; get; }
		public int ID { private set; get; }
		public string Name { private set; get; }
		public string Number { private set; get; }
		public string EmailAddress { private set; get; }
		public string WebsiteUrl { private set; get; }
		public string Phone { private set; get; }
		public string Fax { private set; get; }
		public string HashCode { private set; get; }
		public int? GroupID { private set; get; }

		
		public Shop.MerchantSettings ShopSettings { get; private set; }
		public Accounts.AccountAddress BusinessAddress { get; private set; }

		public string ShopAddress 
		{
			get { return Shop.Module.GetUrl(Number, null, null); } // ShopSettings.SubDomainName
        }

		public static MerchantPublicInfo Load(int id)
		{
			return Load(new List<int>() { id }, null).SingleOrDefault();
		}

		public static List<MerchantPublicInfo> Load(List<int> ids, ISortAndPage sortAndPage)
		{
			return Search(new SearchFilters() { ID = ids }, sortAndPage);
		}

		public static MerchantPublicInfo Load(string subDomain)
		{
			var hasChar = subDomain.Any(c => !Char.IsDigit(c));
			var sf = new SearchFilters();
			if (hasChar) sf.ShopSubDomain = subDomain;
			else sf.MerchantNumber = subDomain;
			return Search(sf, null).SingleOrDefault();
		}

		public static List<MerchantPublicInfo> Search(SearchFilters filters, ISortAndPage sortAndPage)
		{
			var exp =  from m in DataContext.Reader.tblCompanies 
				join a in DataContext.Reader.Accounts on m.ID equals a.Merchant_id
				join ba in DataContext.Reader.AccountAddresses on a.BusinessAddress_id equals ba.AccountAddress_id into ba from baa in ba.DefaultIfEmpty()
				join h in DataContext.Reader.MerchantSetCarts on m.ID equals h.Merchant_id into hj from lj in hj.DefaultIfEmpty()
                join s in DataContext.Reader.MerchantSetShops on m.ID equals s.Merchant_id into shops
				select new { a.Account_id, m.ID, m.ActiveStatus, m.CompanyLegalName, m.CustomerNumber, m.merchantSupportEmail, m.URL, m.Phone, m.CompanyFax, m.GroupID, m.MerchantDepartment_id, m.HashKey, lj, baa, activeShopsCount = shops.Where(s => s.IsEnabled).Count() };
			
			if (filters != null)
			{
				if (filters.ID != null) exp = exp.Where(x => filters.ID.Contains(x.ID));
				if (filters.MerchantNumber != null) exp = exp.Where(x => filters.MerchantNumber == x.CustomerNumber);
				if (filters.ShopSubDomain != null) exp = exp.Where(x => filters.ShopSubDomain == x.lj.ShopSubDomainName);
				if (filters.Group != null) exp = exp.Where(x => filters.Group == x.GroupID);
				if (filters.Department != null) exp = exp.Where(x => filters.Department == x.MerchantDepartment_id);
				if (filters.Status.HasValue) exp = exp.Where(m => m.ActiveStatus == (byte)filters.Status.Value);
                if (filters.Text != null)
                    exp = exp.Where(m => m.CompanyLegalName.Contains(filters.Text) || m.CustomerNumber.Contains(filters.Text) || m.lj.ShopSubDomainName.Contains(filters.Text));
                if (filters.IsShopEnabled != null)
                    exp = exp.Where(x => x.lj.IsEnabled);
                if (filters.HasActiveShops != null)
                    exp = exp.Where(m => m.activeShopsCount > 0);
			}

            var result = exp.ApplySortAndPage(sortAndPage).Select(m => new MerchantPublicInfo() { 
				AccountID = m.Account_id,
				ID = m.ID,
				Name = m.CompanyLegalName,
				Number = m.CustomerNumber,
				EmailAddress = m.merchantSupportEmail,
				WebsiteUrl = m.URL,
				Phone = m.Phone,
				Fax = m.CompanyFax,
				GroupID = m.GroupID,
				HashCode = m.HashKey,
				ShopSettings = m.lj != null ? new Shop.MerchantSettings(m.lj) : null,
				BusinessAddress = m.baa != null ? new Accounts.AccountAddress(m.baa) : null
			}).ToList();

            return result;
		}

	}
}
