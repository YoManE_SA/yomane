﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Bll.Accounts;
using Netpay.CommonTypes;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Merchants.Hosted
{
	public class Translation
	{
		public const string SecuredObjectName = "Translation";
		public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Hosted.Module.Current, SecuredObjectName); } }

		public static void DeleteTranslation(int merchantId, Language languageID, TranslationGroup translationGroup)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);
			AccountFilter.Current.Validate(Accounts.AccountType.Merchant, merchantId);
			var languageData = (from ld in DataContext.Writer.MerchantSetTexts where ld.Merchant_id == merchantId && ld.Language_id == (int)languageID && ld.SolutionList_id == (int)translationGroup select ld).ToList();
			foreach (var l in languageData)
				DataContext.Writer.MerchantSetTexts.DeleteOnSubmit(l);
			DataContext.Writer.SubmitChanges();
		}

		public static void SetTranslation(int merchantId, Language translationLanguage, TranslationGroup translationGroup, string key, string value)
		{
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);
            AccountFilter.Current.Validate(Accounts.AccountType.Merchant, merchantId);
			Netpay.Dal.Netpay.MerchantSetText transalation = (from t in DataContext.Writer.MerchantSetTexts where t.Merchant_id == merchantId && t.Language_id == (int)translationLanguage && t.SolutionList_id == (int)translationGroup && t.TextKey == key select t).SingleOrDefault();
			if (transalation == null)
			{                
                transalation = new Netpay.Dal.Netpay.MerchantSetText();
				transalation.SolutionList_id = (byte)translationGroup;
				transalation.Language_id = (byte)translationLanguage;
				transalation.TextKey = key;
				transalation.Merchant_id = merchantId;
				transalation.TextValue = value.Truncate(2500);
				DataContext.Writer.MerchantSetTexts.InsertOnSubmit(transalation);
			}
			else
				transalation.TextValue = value.Truncate(2500);

			DataContext.Writer.SubmitChanges();
		}

		public static string GetTranslation(string merchantNumber, Language translationLanguage, TranslationGroup translationGroup, string key)
		{
            var merchantId = (from c in DataContext.Reader.tblCompanies where c.CustomerNumber == merchantNumber select c.ID).SingleOrDefault();
			return GetTranslation(merchantId, translationLanguage, translationGroup, key);
		}

		public static string GetTranslation(int merchantId, Language translationLanguage, TranslationGroup translationGroup, string key)
		{
            Netpay.Dal.Netpay.MerchantSetText transalation = (from t in DataContext.Reader.MerchantSetTexts where t.Merchant_id == merchantId && t.Language_id == (int)translationLanguage && t.SolutionList_id == (int)translationGroup && t.TextKey == key select t).SingleOrDefault();
			if (transalation == null) return null;
			return transalation.TextValue;
		}

		public static Dictionary<string, string> GetTranslations(int merchantId, Language translationLanguage, TranslationGroup translationGroup)
		{
            var transalations = from t in DataContext.Reader.MerchantSetTexts where t.Merchant_id == merchantId && t.Language_id == (int)translationLanguage && t.SolutionList_id == (int)translationGroup select t;
			var ret = transalations.ToDictionary(t => t.TextKey, t => t.TextValue);
			if (ret.Count == 0) return null;
			return ret;
		}
	}
}
