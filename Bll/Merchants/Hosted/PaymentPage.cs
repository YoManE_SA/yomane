﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Bll.Accounts;
using Netpay.CommonTypes;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Merchants.Hosted
{
	public class PaymentPage : BaseDataObject
	{
		public const string SecuredObjectName = "PaymentPage";
		public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Hosted.Module.Current, SecuredObjectName); } }

		private bool isNew = false;
		private Dal.Netpay.tblCompanySettingsHosted _entity;

		public int MerchantID { get { return _entity.CompanyID; } }
		public bool IsEnabled { get { return _entity.IsEnabled; } set { _entity.IsEnabled = value; } }
		public bool IsCreditCard
        {
            get
            {
                return _entity.IsCreditCard;
            }
            set
            {
                _entity.IsCreditCard = value;
                if (value) _entity.IsEnabled = true;
            }
        }
		public bool IsCreditCardRequiredEmail { get { return _entity.IsCreditCardRequiredEmail; } set { _entity.IsCreditCardRequiredEmail = value; } }
		public bool IsCreditCardRequiredPhone { get { return _entity.IsCreditCardRequiredPhone; } set { _entity.IsCreditCardRequiredPhone = value; } }
		public bool IsCreditCardRequiredID { get { return _entity.IsCreditCardRequiredID; } set { _entity.IsCreditCardRequiredID = value; } }
		public bool IsCreditCardRequiredCVV { get { return _entity.IsCreditCardRequiredCVV; } set { _entity.IsCreditCardRequiredCVV = value; } }
		public bool IsEcheck
        {
            get
            {
                return _entity.IsEcheck;
            }
            set
            {
                _entity.IsEcheck = value;
                if (value) _entity.IsEnabled = true;
            }
        }
		public bool IsEcheckRequiredEmail { get { return _entity.IsEcheckRequiredEmail; } set { _entity.IsEcheckRequiredEmail = value; } }
		public bool IsEcheckRequiredPhone { get { return _entity.IsEcheckRequiredPhone; } set { _entity.IsEcheckRequiredPhone = value; } }
		public bool IsEcheckRequiredID { get { return _entity.IsEcheckRequiredID; } set { _entity.IsEcheckRequiredID = value; } }
		public bool IsDirectDebit
        {
            get
            {
                return _entity.IsDirectDebit;
            }
            set
            {
                _entity.IsDirectDebit = value;
                if (value) _entity.IsEnabled = true;
            }
        }
		public bool IsDirectDebitRequiredEmail { get { return _entity.IsDirectDebitRequiredEmail; } set { _entity.IsDirectDebitRequiredEmail = value; } }
		public bool IsDirectDebitRequiredPhone { get { return _entity.IsDirectDebitRequiredPhone; } set { _entity.IsDirectDebitRequiredPhone = value; } }
		public bool IsDirectDebitRequiredID { get { return _entity.IsDirectDebitRequiredID; } set { _entity.IsDirectDebitRequiredID = value; } }
		public bool IsCustomer
        {
            get
            {
                return _entity.IsCustomer;
            }
            set
            {
                _entity.IsCustomer = value;
                if (value) _entity.IsEnabled = true;
            }
        }

		public bool IsPhoneDebit
        {
            get
            {
                return _entity.IsPhoneDebit;
            }
            set
            {
                _entity.IsPhoneDebit = value;
                if (value) _entity.IsEnabled = true;
            }
        }

		public bool IsPhoneDebitRequiredEmail { get { return _entity.IsPhoneDebitRequiredEmail; } set { _entity.IsPhoneDebitRequiredEmail = value; } }
		public bool IsPhoneDebitRequiredPhone { get { return _entity.IsPhoneDebitRequiredPhone; } set { _entity.IsPhoneDebitRequiredPhone = value; } }
		public bool IsSignatureOptional { get { return _entity.IsSignatureOptional; } set { _entity.IsSignatureOptional = value; } }
		public bool HideCustomParametersInRedirection { get { return _entity.HideCustomParametersInRedirection; } set { _entity.HideCustomParametersInRedirection = value; } }
		public bool IsWebMoney { get { return _entity.IsWebMoney; } set { _entity.IsWebMoney = value; } }
		public bool IsPayPal { get { return _entity.IsPayPal; } set { _entity.IsPayPal = value; } }
		public bool IsGoogleCheckout { get { return _entity.IsGoogleCheckout; } set { _entity.IsGoogleCheckout = value; } }
		public bool IsWhiteLabel { get { return _entity.IsWhiteLabel.GetValueOrDefault(); } set { _entity.IsWhiteLabel = value; } }
		public bool IsShowAddress1 { get { return _entity.IsShowAddress1; } set { _entity.IsShowAddress1 = value; } }
		public bool IsShowAddress2 { get { return _entity.IsShowAddress2; } set { _entity.IsShowAddress2 = value; } }
		public bool IsShowCity { get { return _entity.IsShowCity; } set { _entity.IsShowCity = value; } }
		public bool IsShowZipCode { get { return _entity.IsShowZipCode; } set { _entity.IsShowZipCode = value; } }
		public bool IsShowState { get { return _entity.IsShowState; } set { _entity.IsShowState = value; } }
		public bool IsShowCountry { get { return _entity.IsShowCountry; } set { _entity.IsShowCountry = value; } }
		public bool IsShowEmail { get { return _entity.IsShowEmail; } set { _entity.IsShowEmail = value; } }
		public bool IsShowPhone { get { return _entity.IsShowPhone; } set { _entity.IsShowPhone = value; } }
		public bool IsShowPersonalID { get { return _entity.IsShowPersonalID; } set { _entity.IsShowPersonalID = value; } }
		public bool IsShowBirthDate { get { return _entity.IsShowBirthDate; } set { _entity.IsShowBirthDate = value; } }
		public bool IsShippingAddressRequire { get { return _entity.IsShippingAddressRequired; } set { _entity.IsShippingAddressRequired = value; } }
		public string WebMoneyPurse { get { return _entity.WebMoneyMerchantPurse; } set { _entity.WebMoneyMerchantPurse = value; } }
		public string WebMoneySecretKey { get { return _entity.WebMoneySecretKey; } set { _entity.WebMoneySecretKey = value; } }
		public string PayPalMerchantID { get { return _entity.PayPalMerchantID; } set { _entity.PayPalMerchantID = value; } }
		public string GoogleCheckoutMerchantID { get { return _entity.GoogleCheckoutMerchantID; } set { _entity.GoogleCheckoutMerchantID = value; } }
		public bool IsMoneyBookers { get { return _entity.IsMoneyBookers; } set { _entity.IsMoneyBookers = value; } }
		public string MoneyBookersAccount { get { return _entity.MoneyBookersAccount; } set { _entity.MoneyBookersAccount = value; } }
		public string LogoImage { get { return _entity.LogoPath; } set { _entity.LogoPath = value; } }
		public string NotificationUrl { get { return _entity.NotificationUrl; } set { _entity.NotificationUrl = value; } }
		public string RedirectionUrl { get { return _entity.RedirectionUrl; } set { _entity.RedirectionUrl = value; } }
		public bool IsNotification { get { return !string.IsNullOrWhiteSpace(NotificationUrl); } }
		public byte UIVersion { get { return _entity.UIVersion; } set { _entity.UIVersion = value; } }
		public string ThemeCssFileName { get { return _entity.ThemeCssFileName; } set { _entity.ThemeCssFileName = value; } }
		public CommonTypes.Language MerchantTextDefaultLanguage { get { return ((CommonTypes.Language?)_entity.MerchantTextDefaultLanguage).GetValueOrDefault(Language.English); } set { _entity.MerchantTextDefaultLanguage = (byte) value; } }

		private PaymentPage(Netpay.Dal.Netpay.tblCompanySettingsHosted entity)
		{
            isNew = false;
            _entity = entity;
		}

		public PaymentPage(int merchantID)
		{
			isNew = true;
			_entity = new Dal.Netpay.tblCompanySettingsHosted();
			_entity.CompanyID = merchantID;
		}

		public static PaymentPage Load(int merchantID)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
			//AccountFilter.Current.Validate(Accounts.AccountType.Merchant, merchantID);
			var entity = (from mmas in DataContext.Reader.tblCompanySettingsHosteds where mmas.CompanyID == merchantID select mmas).SingleOrDefault();
			if (entity == null) return null;
			return new PaymentPage(entity);
		}

		public void Save()
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Edit);

			AccountFilter.Current.Validate(Accounts.AccountType.Merchant, _entity.CompanyID);
			DataContext.Writer.tblCompanySettingsHosteds.Update(_entity, !isNew);
			DataContext.Writer.SubmitChanges();
		}

		public static CommonTypes.Language GetDefaultLanguage(int merchantId)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var value = (from ppo in DataContext.Reader.tblCompanySettingsHosteds where ppo.CompanyID == merchantId select ppo.MerchantTextDefaultLanguage).SingleOrDefault();
			if (value == null) return Netpay.CommonTypes.Language.English;
			return (Netpay.CommonTypes.Language)value;
		}

		public List<Bll.PaymentMethods.Group> GetPaymentMethodGroups(bool isPopular)
		{
			var groups = PaymentMethods.Group.Search(null);
			if (isPopular) groups = groups.Where(g => g.IsPopular == isPopular).ToList();
			for (int i = groups.Count - 1; i >= 0; i--)
			{
				var group = groups[i];
				switch (group.Type)
				{
					case PaymentMethodType.CreditCard: if (!IsCreditCard) groups.Remove(group); break;
					case PaymentMethodType.ECheck: if (!IsEcheck) groups.Remove(group); break;
					case PaymentMethodType.BankTransfer: if (!IsDirectDebit) groups.Remove(group); break;
					case PaymentMethodType.PhoneDebit: if (!IsPhoneDebit) groups.Remove(group); break;
					case PaymentMethodType.Wallet: if (!IsCustomer) groups.Remove(group); break;
					case PaymentMethodType.ExternalWallet: if (!IsWebMoney && !IsPayPal && !IsGoogleCheckout && !IsMoneyBookers) groups.Remove(group); break;
				}
			}

			return groups;
		}

		/*should move to terminals*/
		public static Bll.PaymentMethods.PaymentMethod[] GetAvailablePaymentMethods(int merchantID, int currencyID, bool popularOnly)
		{
			// get available terminal methods
			List<short> terminalPaymentMethods = (from o in DataContext.Reader.tblCompanyCreditFees where o.CCF_CompanyID == merchantID && o.CCF_CurrencyID == currencyID && o.CCF_ExchangeTo != (short)CommonTypes.Currency.UnsignedUnknown && o.CCF_IsDisabled == false && o.CCF_PaymentMethod.HasValue select o.CCF_PaymentMethod.Value).Distinct().ToList();

			// filter popular
			var paymentMethods = PaymentMethods.PaymentMethod.Search(new PaymentMethods.PaymentMethod.SearchFilters() { IsPopular = popularOnly ? (bool?)true : null }, null);
			var results = new List<Bll.PaymentMethods.PaymentMethod>();
			foreach (var currentPaymentMethod in paymentMethods)
			{
				if (currentPaymentMethod.IsTerminalRequired)
				{
					if (terminalPaymentMethods.Contains((short)currentPaymentMethod.ID))
						results.Add(currentPaymentMethod);
				}
				else
					results.Add(currentPaymentMethod);
			}
			return results.ToArray();
		}
	}
}
