﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Merchant.Hosted
{
	public class Shop : BaseDataObject
	{
		private Dal.Netpay.tblPublicPayOption _entity;

		public int ID { get{ return _entity.ID; } }
		public int MerchantID { get { return _entity.MerchantID; } set { _entity.MerchantID = value; } }
		public bool IsPublicPay { get { return _entity.IsPublicPay; } set { _entity.IsPublicPay = value; } }
		public string LogoPic { get { return _entity.LogoPic; } set { _entity.LogoPic = value; } }
		public bool IsAsk4PersonalInfo { get { return _entity.IsAsk4PersonalInfo; } set { _entity.IsAsk4PersonalInfo = value; } }
		public Netpay.Infrastructure.PublicPayChargeOptions? ChargeOptions { get { return PublicPayChargeOptions.Both /*(Netpay.Infrastructure.PublicPayChargeOptions?) _entity.ChargeOptions*/; } set { _entity.ChargeOptions = (int?)value; } }
		public bool IsSendAnswer  { get { return _entity.IsSendAnswer; } set { _entity.IsSendAnswer = value; } }
		public string SendAnswerURL  { get { return _entity.SendAnswerURL; } set { _entity.SendAnswerURL = value; } }
		public string CurrencyOptions  { get { return _entity.CurrencyOptions; } set { _entity.CurrencyOptions = value; } }
		public string PromotionBannerUrl { get { return _entity.BannerUrl; } set { _entity.BannerUrl = value; } }
		public string BannerImageName  { get { return _entity.BannerImageName; } set { _entity.BannerImageName = value; } }
		public string MobileAppLogo { get { return _entity.MobileAppLogo; } set { _entity.MobileAppLogo = value; } }
		public bool IsFullName { get { return _entity.IsFullName; } set { _entity.IsFullName = value; } }
		public bool IsRequiredFullName  { get { return _entity.IsRequiredFullName; } set { _entity.IsRequiredFullName = value; } }
		public bool IsAddress  { get { return _entity.IsAddress; } set { _entity.IsAddress = value; } }
		public bool IsRequiredAddress  { get { return _entity.IsRequiredAddress; } set { _entity.IsRequiredAddress = value; } }
		public bool IsPhone  { get { return _entity.IsPhone; } set { _entity.IsPhone = value; } }
		public bool IsRequiredPhone  { get { return _entity.IsRequiredPhone; } set { _entity.IsRequiredPhone = value; } }
		public bool IsCellular  { get { return _entity.IsCellular; } set { _entity.IsCellular = value; } }
		public bool IsRequiredCellular  { get { return _entity.IsRequiredCellular; } set { _entity.IsRequiredCellular = value; } }
		public bool IsEmail { get { return _entity.IsEmail; } set { _entity.IsEmail = value; } }
		public bool IsRequiredEmail { get { return _entity.IsRequiredEmail; } set { _entity.IsRequiredEmail = value; } }
		public bool IsHowGetHere  { get { return _entity.IsHowGetHere; } set { _entity.IsHowGetHere = value; } }
		public bool IsRequiredHowGetHere  { get { return _entity.IsRequiredHowGetHere; } set { _entity.IsRequiredHowGetHere = value; } }
		public bool IsVar1 { get { return _entity.IsVar1.GetValueOrDefault(); } set { _entity.IsVar1 = value; } }
		public bool IsRequiredVar1 { get { return _entity.IsRequiredVar1.GetValueOrDefault(); } set { _entity.IsRequiredVar1 = value; } }
		public bool IsVar2 { get { return _entity.IsVar2.GetValueOrDefault(); } set { _entity.IsVar2 = value; } }
		public bool IsRequiredVar2  { get { return _entity.IsRequiredVar2.GetValueOrDefault(); } set { _entity.IsRequiredVar2 = value; } }
		public bool IsVar3  { get { return _entity.IsVar3.GetValueOrDefault(); } set { _entity.IsVar3 = value; } }
		public bool IsRequiredVar3 { get { return _entity.IsRequiredVar3.GetValueOrDefault(); } set { _entity.IsRequiredVar3 = value; } }
		public bool IsVar4  { get { return _entity.IsVar4.GetValueOrDefault(); } set { _entity.IsVar4 = value; } }
		public bool IsRequiredVar4 { get { return _entity.IsRequiredVar4.GetValueOrDefault(); } set { _entity.IsRequiredVar4 = value; } }


		private Shop(Netpay.Infrastructure.Security.User user, Netpay.Dal.Netpay.tblPublicPayOption entity)
			: base(user)
		{ 
			_entity = entity;
		}
		private Shop(Infrastructure.Domains.Domain domain, Netpay.Dal.Netpay.tblPublicPayOption entity)
			: base(domain)
		{
			_entity = entity;
		}

		public Shop(Guid credentialsToken, int merchantID)
			: base(credentialsToken)
		{
			_entity = new Dal.Netpay.tblPublicPayOption();
			_entity.MerchantID = merchantID;
		}

		public static Shop Load(Guid credentialsToken, int merchantID)
		{
			var user = SecurityManager.GetInternalUser(credentialsToken, new UserType[] { UserType.MerchantPrimary, UserType.NetpayAdmin, UserType.NetpayUser });
			var dc = createContext(user.Domain.Host);
			var entity = (from mmas in dc.tblPublicPayOptions where mmas.MerchantID == merchantID select mmas).SingleOrDefault();
			if (entity == null) return null;
			return new Shop(user, entity);
		}

		public static Shop Load(string domainHost, string merchantNumber)
		{
			var domain = Infrastructure.Domains.DomainsManager.GetDomain(domainHost);
			var dc = createContext(domain.Host);
			var entity = (from mmas in dc.tblPublicPayOptions join c in dc.tblCompanies on mmas.MerchantID equals c.ID where c.CustomerNumber == merchantNumber select mmas).SingleOrDefault();
			if (entity == null) return null;
			return new Shop(domain, entity);
		}

		private void EnsureAttached()
		{
			if (DataContext.tblPublicPayOptions.GetOriginalEntityState(_entity) == null)
				DataContext.tblPublicPayOptions.Attach(_entity);
		}

		public void Save()
		{
			if (_entity.ID != 0) {
				EnsureAttached();
				DataContext.Refresh(System.Data.Linq.RefreshMode.KeepCurrentValues, _entity);
			} else {
				DataContext.tblPublicPayOptions.InsertOnSubmit(_entity);
			}
			DataContext.SubmitChanges();
		}
	}
}
