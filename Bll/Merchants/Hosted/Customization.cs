﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using Netpay.Bll.Accounts;

namespace Netpay.Bll.Merchants.Hosted
{
	public class Customization : BaseDataObject
	{
		public const string SecuredObjectName = "Customization";
		public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Hosted.Module.Current, SecuredObjectName); } }

		public struct MerchantCustomizationLogo
		{
			public string Align;
			public int Width;
			public int Height;

			public MerchantCustomizationLogo(string align = null, int? width = null, int? height = null)
			{
				Align = align;
				Width = width.GetValueOrDefault(0);
				Height = height.GetValueOrDefault(0);
			}
		}

		public struct MerchantCustomizationText
		{
			public string BackColor;
			public string ForeColor;
			public string FontFamily;
			public string FontSize;
			public bool FontIsBold;
			public bool FontIsItalic;

			public MerchantCustomizationText(string backColor = null, string foreColor = null, string fontFamily = null, string fontSize = null, bool? isBold = null, bool? isItalic = null)
			{
				BackColor = backColor;
				ForeColor = foreColor;
				FontFamily = fontFamily;
				FontSize = fontSize;
				FontIsBold = isBold.GetValueOrDefault(false);
				FontIsItalic = isItalic.GetValueOrDefault(false);
			}
		}
		private int _merchantID;
		public int? SkinID;
		public MerchantCustomizationLogo LogoMain;
		public MerchantCustomizationLogo LogoTopRight;
		public MerchantCustomizationLogo LogoTopLeft;
		public MerchantCustomizationLogo LogoBottomRight;
		public MerchantCustomizationLogo LogoBottomLeft;
		public string BackImageRepeat;
		public string BackColor;
		public MerchantCustomizationText Text;
		public MerchantCustomizationText TextTitle;
		public MerchantCustomizationText TextFormTitle;
		public MerchantCustomizationText TextGroupTitle;
		public MerchantCustomizationText TextField;
		public MerchantCustomizationText TextButton;


		private Customization(Netpay.Dal.Netpay.tblMerchantCustomization entity)
		{
			_merchantID = entity.MerchantID;
			SkinID = entity.SkinID;
			BackImageRepeat = entity.BackImageRepeat;
			BackColor = entity.PageBackColor;
			LogoMain = new MerchantCustomizationLogo(entity.LogoMainAlign, entity.LogoMainWidth, entity.LogoMainHeight);
			LogoTopRight = new MerchantCustomizationLogo(entity.LogoTopRightAlign, entity.LogoTopRightWidth, entity.LogoTopRightHeight);
			LogoTopLeft = new MerchantCustomizationLogo(entity.LogoTopLeftAlign, entity.LogoTopLeftWidth, entity.LogoTopLeftHeight);
			LogoBottomRight = new MerchantCustomizationLogo(entity.LogoBottomRightAlign, entity.LogoBottomRightWidth, entity.LogoBottomRightHeight);
			LogoBottomLeft = new MerchantCustomizationLogo(entity.LogoBottomLeftAlign, entity.LogoBottomLeftWidth, entity.LogoBottomLeftHeight);
			Text = new MerchantCustomizationText(entity.BackColor, entity.ForeColor, entity.FontFamily, entity.FontSize, entity.FontIsBold, entity.FontIsItalic);
			TextTitle = new MerchantCustomizationText(entity.TitleBackColor, entity.TitleForeColor, entity.TitleFontFamily, entity.TitleFontSize, entity.TitleFontIsBold, entity.TitleFontIsItalic);
			TextFormTitle = new MerchantCustomizationText(entity.FormTitleBackColor, entity.FormTitleForeColor, entity.FormTitleFontFamily, entity.FormTitleFontSize, entity.FormTitleFontIsBold, entity.FormTitleFontIsItalic);
			TextGroupTitle = new MerchantCustomizationText(entity.GroupTitleBackColor, entity.GroupTitleForeColor, entity.GroupTitleFontFamily, entity.GroupTitleFontSize, entity.GroupTitleFontIsBold, entity.GroupTitleFontIsItalic);
			TextField = new MerchantCustomizationText(entity.FieldBackColor, entity.FieldForeColor, entity.FieldFontFamily, entity.FieldFontSize, entity.FieldFontIsBold, entity.FieldFontIsItalic);
			TextButton = new MerchantCustomizationText(entity.ButtonBackColor, entity.ButtonForeColor, entity.ButtonFontFamily, entity.ButtonFontSize, entity.ButtonFontIsBold, entity.ButtonFontIsItalic);
		}		

		public Customization(int merchantId)
		{
			//_entity = new Netpay.Dal.Netpay.tblMerchantCustomization();
			_merchantID = merchantId;
		}


		public static string GetCustomizations(int? merchantID = null)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);

			AccountFilter.Current.Validate(Accounts.AccountType.Merchant, ref merchantID);
			return DataContext.Reader.GetMerchantCustomizationSkins(merchantID);
		}

		public static Customization Load(int? skinID = null, int? merchantID = null)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

			if (ObjectContext.Current.User != null && merchantID == null)
				AccountFilter.Current.Validate(Accounts.AccountType.Merchant, ref merchantID);
			var expression = from t in DataContext.Reader.tblMerchantCustomizations where t.MerchantID == merchantID select t;
			if (skinID.HasValue) expression = expression.Where(t => t.SkinID == skinID.Value);
			var result = expression.SingleOrDefault();
			if (result == null) return null;
			return new Customization(result);
		}
        
        public void Delete()
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Delete);

            AccountFilter.Current.Validate(Accounts.AccountType.Merchant, _merchantID);
            var entity = (from t in DataContext.Writer.tblMerchantCustomizations where t.MerchantID == _merchantID && t.SkinID == SkinID.Value select t).SingleOrDefault();
            if (entity == null) {
                DataContext.Writer.tblMerchantCustomizations.Delete(entity);
                DataContext.Writer.SubmitChanges();
            }
            string merchantNumber = Merchant.CachedNumbersForDomain().Where(v => v.Value == _merchantID).Select(v=> v.Key).SingleOrDefault();
            if (merchantNumber == null) return;
            var skinFolder = Account.MapPublicPath(merchantNumber, GetImageFileName(SkinID.Value, ""));
            if (System.IO.Directory.Exists(skinFolder))
                System.IO.Directory.Delete(skinFolder, false);

        }

		public int Save()
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Edit);

			AccountFilter.Current.Validate(Accounts.AccountType.Merchant, _merchantID);
			var expression = from t in DataContext.Writer.tblMerchantCustomizations where t.MerchantID == _merchantID select t;
			expression = (SkinID.HasValue ? expression.Where(t => t.SkinID == SkinID.Value) : expression.OrderByDescending(t => t.SkinID));
			Netpay.Dal.Netpay.tblMerchantCustomization entity;
			if (SkinID.HasValue && expression.Count() > 0)
				entity = expression.First();
			else
			{
				if (!SkinID.HasValue) SkinID = (expression.Count() == 0 ? 1 : expression.First().SkinID + 1);
				entity = new Netpay.Dal.Netpay.tblMerchantCustomization();
				entity.MerchantID = _merchantID;
				entity.SkinID = SkinID.Value;
				DataContext.Writer.tblMerchantCustomizations.InsertOnSubmit(entity);
			}
			entity.BackImageRepeat = BackImageRepeat;
			entity.PageBackColor = BackColor;

			entity.LogoMainAlign = LogoMain.Align;
			entity.LogoMainWidth = LogoMain.Width;
			entity.LogoMainHeight = LogoMain.Height;

			entity.LogoTopRightAlign = LogoTopRight.Align;
			entity.LogoTopRightWidth = LogoTopRight.Width;
			entity.LogoTopRightHeight = LogoTopRight.Height;

			entity.LogoTopLeftAlign = LogoTopLeft.Align;
			entity.LogoTopLeftWidth = LogoTopLeft.Width;
			entity.LogoTopLeftHeight = LogoTopLeft.Height;

			entity.LogoBottomRightAlign = LogoBottomRight.Align;
			entity.LogoBottomRightWidth = LogoBottomRight.Width;
			entity.LogoBottomRightHeight = LogoBottomRight.Height;

			entity.LogoBottomLeftAlign = LogoBottomLeft.Align;
			entity.LogoBottomLeftWidth = LogoBottomLeft.Width;
			entity.LogoBottomLeftHeight = LogoBottomLeft.Height;

			entity.BackColor = Text.BackColor;
			entity.ForeColor = Text.ForeColor;
			entity.FontFamily = Text.FontFamily;
			entity.FontSize = Text.FontSize;
			entity.FontIsBold = Text.FontIsBold;
			entity.FontIsItalic = Text.FontIsItalic;

			entity.TitleBackColor = TextTitle.BackColor;
			entity.TitleForeColor = TextTitle.ForeColor;
			entity.TitleFontFamily = TextTitle.FontFamily;
			entity.TitleFontSize = TextTitle.FontSize;
			entity.TitleFontIsBold = TextTitle.FontIsBold;
			entity.TitleFontIsItalic = TextTitle.FontIsItalic;

			entity.FormTitleBackColor = TextFormTitle.BackColor;
			entity.FormTitleForeColor = TextFormTitle.ForeColor;
			entity.FormTitleFontFamily = TextFormTitle.FontFamily;
			entity.FormTitleFontSize = TextFormTitle.FontSize;
			entity.FormTitleFontIsBold = TextFormTitle.FontIsBold;
			entity.FormTitleFontIsItalic = TextFormTitle.FontIsItalic;

			entity.GroupTitleBackColor = TextGroupTitle.BackColor;
			entity.GroupTitleForeColor = TextGroupTitle.ForeColor;
			entity.GroupTitleFontFamily = TextGroupTitle.FontFamily;
			entity.GroupTitleFontSize = TextGroupTitle.FontSize;
			entity.GroupTitleFontIsBold = TextGroupTitle.FontIsBold;
			entity.GroupTitleFontIsItalic = TextGroupTitle.FontIsItalic;

			entity.FieldBackColor = TextField.BackColor;
			entity.FieldForeColor = TextField.ForeColor;
			entity.FieldFontFamily = TextField.FontFamily;
			entity.FieldFontSize = TextField.FontSize;
			entity.FieldFontIsBold = TextField.FontIsBold;
			entity.FieldFontIsItalic = TextField.FontIsItalic;
			entity.ButtonBackColor = TextButton.BackColor;
			entity.ButtonForeColor = TextButton.ForeColor;
			entity.ButtonFontFamily = TextButton.FontFamily;
			entity.ButtonFontSize = TextButton.FontSize;
			entity.ButtonFontIsBold = TextButton.FontIsBold;
			entity.ButtonFontIsItalic = TextButton.FontIsItalic;
			DataContext.Writer.SubmitChanges();
			return SkinID.Value;
		}

		public static string GetCss(string cssClassName, MerchantCustomizationText text)
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine(cssClassName + "{");
			if (!string.IsNullOrEmpty(text.BackColor)) sb.AppendLine("background-color:" + text.BackColor + " !important;");
			if (!string.IsNullOrEmpty(text.ForeColor)) sb.AppendLine("color:" + text.ForeColor + " !important;");
			if (!string.IsNullOrEmpty(text.FontSize)) sb.AppendLine("font-size:" + text.FontSize + "px !important;");
			if (!string.IsNullOrEmpty(text.FontFamily)) sb.AppendLine("font-family:" + text.FontFamily + " !important;");
			sb.AppendLine("font-weight:" + (text.FontIsBold ? "bold" : "normal") + " !important;");
			sb.AppendLine("font-style:" + (text.FontIsItalic ? "italic" : "normal") + " !important;");
			sb.AppendLine("}");
			return sb.ToString();
		}

		public static string GetCss(string cssClassName, MerchantCustomizationLogo logo)
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine(cssClassName + "{");
			if (!string.IsNullOrEmpty(logo.Align)) sb.AppendLine("align:" + logo.Align + " !important;");
			if (logo.Width > 0) sb.AppendLine("width:" + logo.Width + "px !important;");
			if (logo.Height > 0) sb.AppendLine("width:" + logo.Height + "px !important;");
			sb.AppendLine("}");
			return sb.ToString();
		}

		public static string GetImageFileName(int skinId, string fileName)
		{
            return string.Format("UIServices/V1/{0}/{1}", skinId, fileName);
		}
	}
}
