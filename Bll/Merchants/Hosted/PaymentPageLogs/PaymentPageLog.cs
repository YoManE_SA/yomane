﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Merchants.Hosted
{
    public enum EBigOrSmall {Bigger_Than , Smaller_Than}
	public class PaymentPageLog : BaseDataObject
	{
        public static Dictionary<Infrastructure.SuccessOrFail, System.Drawing.Color> SuccessOrFailStatus = new Dictionary<SuccessOrFail, System.Drawing.Color>
        {
            {Infrastructure.SuccessOrFail.Success, System.Drawing.Color.Green },
            {Infrastructure.SuccessOrFail.Fail, System.Drawing.Color.Red }
        };

        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Bll.Merchants.Hosted.PaymentPageLogs.Module.Current, SecuredObjectName); } }


        public class SearchFilters
		{
            public Infrastructure.Range<int?> ID;
            public Infrastructure.Range<int?> MerchantId;
            public Infrastructure.Range<DateTime?> DateRange;
            public Infrastructure.Range<int?> LogIdRange;
            public string ReplyCode;
            public bool? IsSuccess;
            public TimeSpan Duration; // Duration in seconds            
            public EBigOrSmall? BigOrSmall; //Enum value - it indicates smaller or bigger than the Duration input 
        }

		private Dal.Netpay.tblLogPaymentPage _entity;
		public int ID { get { return _entity.LogPaymentPage_id; } }
		public string MerchantNumber { get { return _entity.Lpp_MerchantNumber; } set { _entity.Lpp_MerchantNumber = value.Truncate(7); } }
		public string RemoteIPAddress { get { return _entity.Lpp_RemoteAddress; } set { _entity.Lpp_RemoteAddress = value.Truncate(20); } }
		public string RequestMethod { get { return _entity.Lpp_RequestMethod; } set { _entity.Lpp_RequestMethod = value.Truncate(12); } }
		public string PathTranslate { get { return _entity.Lpp_PathTranslate; } set { _entity.Lpp_PathTranslate = value.Truncate(4000); } }
		public string HttpHost { get { return _entity.Lpp_HttpHost; } set { _entity.Lpp_HttpHost = value.Truncate(400); } }
		public string HttpReferer { get { return _entity.Lpp_HttpReferer; } set { _entity.Lpp_HttpReferer = value.Truncate(4000); } }
		public string QueryString { get { return _entity.Lpp_QueryString; } set { _entity.Lpp_QueryString = value.Truncate(4000); } }
		public string RequestForm { get { return _entity.Lpp_RequestForm; } set { _entity.Lpp_RequestForm = value.Truncate(4000); } }
		public string SessionContents { get { return _entity.Lpp_SessionContents; } set { _entity.Lpp_SessionContents = value.Truncate(4000); } }
		public string ReplyCode { get { return _entity.Lpp_ReplyCode; } set { _entity.Lpp_ReplyCode = value.Truncate(50); } }
		public string ReplyDesc { get { return _entity.Lpp_ReplyDesc; } set { _entity.Lpp_ReplyDesc = value.Truncate(500); } }
		public int? TransactionID { get { return _entity.Lpp_TransNum; } set { _entity.Lpp_TransNum = value; } }
		public System.DateTime? DateStart { get { return _entity.Lpp_DateStart; } set { _entity.Lpp_DateStart = value; } }
		public System.DateTime? DateEnd { get { return _entity.Lpp_DateEnd; } set { _entity.Lpp_DateEnd = value; } }
		public string LocalIPAddress { get { return _entity.Lpp_LocalAddr; } set { _entity.Lpp_LocalAddr = value.Truncate(20); } }
		public bool? IsSecure { get { return _entity.Lpp_IsSecure; } set { _entity.Lpp_IsSecure = value; } }
		public bool IsSuccess { get { return TransactionID.HasValue && (ReplyCode == "000" || ReplyCode == "001"); } }

		private PaymentPageLog(Dal.Netpay.tblLogPaymentPage entity)
		{
			_entity = entity;
		}

		public PaymentPageLog()
		{
			_entity = new Dal.Netpay.tblLogPaymentPage();
		}

		public static List<PaymentPageLog> Search(SearchFilters filters, ISortAndPage sortAndPage)
		{
            //ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var exp = (from l in DataContext.Reader.tblLogPaymentPages orderby l.LogPaymentPage_id descending
                       join c in DataContext.Reader.tblCompanies on l.Lpp_MerchantNumber equals c.CustomerNumber
                       select new { c, l });
			if (filters != null)
			{
                // Check dates range
                if (filters.DateRange.To.HasValue)
                {
                    // Set To date to EOD
                    filters.DateRange.To = filters.DateRange.To.Value.Date.AddDays(1).AddMilliseconds(-1);
                }

                if (filters.ID.From.HasValue) exp = exp.Where(x => x.l.LogPaymentPage_id >= filters.ID.From.Value);
                if (filters.ID.To.HasValue) exp = exp.Where(x => x.l.LogPaymentPage_id <= filters.ID.To.Value);
                if (filters.MerchantId.From.HasValue) exp = exp.Where(x => x.c.ID >= filters.MerchantId.From.Value);
                if (filters.MerchantId.To.HasValue) exp = exp.Where(x => x.c.ID <= filters.MerchantId.To.Value);
                if (filters.LogIdRange.From.HasValue) exp = exp.Where(x => x.l.LogPaymentPage_id >= filters.LogIdRange.From);
                if (filters.LogIdRange.To.HasValue) exp = exp.Where(x => x.l.LogPaymentPage_id <= filters.LogIdRange.To);
                if (filters.DateRange.From.HasValue) exp = exp.Where(x => x.l.Lpp_DateStart >= filters.DateRange.From);
				if (filters.DateRange.To.HasValue) exp = exp.Where(x => x.l.Lpp_DateStart <= filters.DateRange.To);
                //if (!string.IsNullOrEmpty(filters.MerchantNumber)) exp = exp.Where(l => l.Lpp_MerchantNumber == filters.MerchantNumber);
                if (filters.ReplyCode!=null) exp = exp.Where(x => x.l.Lpp_ReplyCode == filters.ReplyCode);
                
                if (filters.Duration!=null && filters.BigOrSmall.HasValue)
                {
                    if ((filters.BigOrSmall) == EBigOrSmall.Bigger_Than)
                    {
                        exp = exp.Where(x => (x.l.Lpp_DateEnd - x.l.Lpp_DateStart) > filters.Duration);
                    }

                    if ((filters.BigOrSmall) == EBigOrSmall.Smaller_Than)
                    {
                        exp = exp.Where(x => (x.l.Lpp_DateEnd - x.l.Lpp_DateStart) < filters.Duration);
                    }
                }

                if (filters.IsSuccess.HasValue)
                {
                    if (filters.IsSuccess.Value == true) exp = exp.Where(x => (x.l.Lpp_TransNum.HasValue && (x.l.Lpp_ReplyCode == "000" || x.l.Lpp_ReplyCode == "001")));
                    if (filters.IsSuccess.Value == false) exp = exp.Where(x => ((!x.l.Lpp_TransNum.HasValue) || (!(x.l.Lpp_ReplyCode == "000" || x.l.Lpp_ReplyCode == "001"))));
                }
                

			}
            var items = exp.ApplySortAndPage(sortAndPage).ToList().Select(x => new PaymentPageLog(x.l)).ToList();
            return items;
		}
		
		public void Save()
		{
            //ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            DataContext.Writer.tblLogPaymentPages.Update(_entity, _entity.LogPaymentPage_id != 0);
			DataContext.Writer.SubmitChanges();
		}

        public void SaveState(string newSession)
        {
            //ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            SessionContents = newSession;
            DataContext.Reader.ExecuteCommand("Update tblLogPaymentPage Set Lpp_SessionContents={0} Where LogPaymentPage_id={1}", SessionContents, _entity.LogPaymentPage_id);
        }

        public static PaymentPageLog Load(int id)
		{
			return Search(new SearchFilters(){ ID = new Range<int?>(id) }, null).SingleOrDefault();
		}

		/*
		public static LogPaymentPageTranVO GetPaymentPageLastTransLog(string domainHost, int paymentPageLogId)
		{
			NetpayDataContext dc = new NetpayDataContext(Domain.Get(domainHost).Sql1ConnectionString);
			return (from t in dc.tblLogPaymentPageTrans where t.Lppt_LogPaymentPage_id == paymentPageLogId orderby t.LogPaymentPageTrans_id descending select new LogPaymentPageTranVO(t)).FirstOrDefault();
		}
		*/

		public void AddTransactionLog(CommonTypes.PaymentMethodType methodType, string requestData, string responseData, int? TransactionId)
		{
            //ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            var entity = new TransactionLog(ID);
			entity.PaymentMethodTypeId = methodType;
			entity.RequestString = requestData;
			entity.ResponseString = responseData;
			entity.TransactionId = TransactionId;
			entity.Save();
		}

		public class TransactionLog : BaseDataObject
		{
			public class SearchFilters{
				public Infrastructure.Range<int?> ID;
				public Infrastructure.Range<DateTime?> Date;
				public int? TransactionId;
				public int? PaymentPageLogId;
			}
			private Netpay.Dal.Netpay.tblLogPaymentPageTran _entity;
			private TransactionLog(Dal.Netpay.tblLogPaymentPageTran entity) { _entity = entity; }

			public TransactionLog(int paymentPageLogId) { _entity = new Dal.Netpay.tblLogPaymentPageTran() { Lppt_Date = DateTime.Now, Lppt_LogPaymentPage_id = paymentPageLogId }; }

			public int ID { get { return _entity.LogPaymentPageTrans_id; } }
			public int PaymentPageLogId { get { return _entity.Lppt_LogPaymentPage_id; }  }
			public DateTime Date { get { return _entity.Lppt_Date; } }
			public CommonTypes.PaymentMethodType PaymentMethodTypeId { get { return (CommonTypes.PaymentMethodType)_entity.Lppt_PaymentMethodType; } set { _entity.Lppt_PaymentMethodType = (short)value; } }
			public string RequestString { get { return _entity.Lppt_RequestString; } set { _entity.Lppt_RequestString = value.Truncate(3800); } }
			public string ResponseString { get { return _entity.Lppt_ResponseString; } set { _entity.Lppt_ResponseString = value.Truncate(3800); } }
			public int? TransactionId { get { return _entity.Lppt_TransNum; } set { _entity.Lppt_TransNum = value; } }

			public void Save() 
			{
                //ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

                DataContext.Writer.tblLogPaymentPageTrans.Update(_entity, _entity.LogPaymentPageTrans_id != 0); 
				DataContext.Writer.SubmitChanges();
			}

			public static List<TransactionLog> Search(SearchFilters filters, ISortAndPage sortAndPage)
			{
                //ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                var exp = (from l in DataContext.Reader.tblLogPaymentPageTrans select l);
				if (filters != null)
				{
					if (filters.ID.From.HasValue) exp = exp.Where(l => l.LogPaymentPageTrans_id >= filters.ID.From);
					if (filters.ID.To.HasValue) exp = exp.Where(l => l.LogPaymentPageTrans_id <= filters.ID.To);
					if (filters.Date.From.HasValue) exp = exp.Where(l => l.Lppt_Date >= filters.Date.From);
					if (filters.Date.To.HasValue) exp = exp.Where(l => l.Lppt_Date <= filters.Date.To);
					if (filters.PaymentPageLogId.HasValue) exp = exp.Where(l => l.Lppt_LogPaymentPage_id == filters.PaymentPageLogId);
					if (filters.TransactionId.HasValue) exp = exp.Where(l => l.Lppt_TransNum == filters.TransactionId);
				}
                string prefix = "";
                if (sortAndPage.SortKey != "LogPaymentPageTrans_id")
                    prefix = "Lppt_";
				return exp.ApplySortAndPage(sortAndPage, prefix).ToList().Select(l => new TransactionLog(l)).ToList();
			}
		}

		public TransactionLog GetLastTransactionLog()
		{
			return TransactionLog.Search(new TransactionLog.SearchFilters() { PaymentPageLogId = ID }, new SortAndPage(0, 1, "LogPaymentPage_id", true)).SingleOrDefault();
		}

	}
}
