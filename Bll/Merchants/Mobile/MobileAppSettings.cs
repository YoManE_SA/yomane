﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Merchants.Mobile
{
	public class MobileAppSettings : BaseDataObject
	{
		public const string SecuredObjectName = "MobileAppSettings";
		public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Mobile.Module.Current, SecuredObjectName); } }

		private Netpay.Dal.Netpay.SetMerchantMobileApp _entity;
		//public string MerchantNumber { set; get; }
		//public string MerchantName { set; get; }
		//public string MerchantAddress { set; get; }

		public bool IsMobileAppEnabled { get { return _entity.IsEnableMobileApp; } set { _entity.IsEnableMobileApp = value; } }
		public bool IsEmailRequired { get { return _entity.IsRequireEmail; } set { _entity.IsRequireEmail = value; } }
		public bool IsPersonalNumberRequired { get { return _entity.IsRequirePersonalNumber; } set { _entity.IsRequirePersonalNumber = value; } }
		public bool IsCardNotPresent { get { return _entity.IsAllowCardNotPresent; } set { _entity.IsAllowCardNotPresent = value; } }
		public bool IsInstallments { get { return _entity.IsAllowInstallments; } set { _entity.IsAllowInstallments = value; } }
		public bool IsRefund { get { return _entity.IsAllowRefund; } set { _entity.IsAllowRefund = value; } }
		public bool IsAuthorization { get { return _entity.IsAllowAuthorization; } set { _entity.IsAllowAuthorization = value; } }
		public bool IsFullNameRequired { get { return _entity.IsRequireFullName; } set { _entity.IsRequireFullName = value; } }
		public bool IsPhoneRequired { get { return _entity.IsRequirePhoneNumber; } set { _entity.IsRequirePhoneNumber = value; } }
		public bool IsOwnerSignRequired { get { return _entity.IsRequireCardholderSignature; } set { _entity.IsRequireCardholderSignature = value; } }
		public bool IsAllowTaxRateChange { get { return _entity.IsAllowTaxRateChange; } set { _entity.IsAllowTaxRateChange = value; } }
		public decimal? ValueAddedTax { get { return _entity.ValueAddedTax; } set { _entity.ValueAddedTax = value; } }
		public string SupportedCurrencies { get; private set; }

		public bool IsCVVRequired { get { return _entity.IsRequireCVV; } set { _entity.IsRequireCVV = value; } }
		public short MaxDeviceCount { get { return _entity.MaxDeviceCount; } set { _entity.MaxDeviceCount = value; } }



		private MobileAppSettings(Netpay.Dal.Netpay.SetMerchantMobileApp entity)
			
		{ 
			_entity = entity;
			string[] currencies = Merchant.GetSupportedCurrencies(entity.Merchant_id).Select(vo => vo.IsoCode).ToArray();
		}

		public MobileAppSettings(int merchantID) 
		{
			_entity = new Dal.Netpay.SetMerchantMobileApp();
			_entity.Merchant_id = merchantID;
		}

		public static MobileAppSettings Load(int merchantID)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);
			Netpay.Dal.Netpay.SetMerchantMobileApp settings = (from mmas in DataContext.Reader.SetMerchantMobileApps where mmas.Merchant_id == merchantID select mmas).SingleOrDefault();
			if (settings == null) return null;
			return new MobileAppSettings(settings);
		}

		public void Save()
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Edit);
			DataContext.Writer.SetMerchantMobileApps.Update(_entity, (_entity.SetMerchantMobileApp_id != 0));
			DataContext.Writer.SubmitChanges();
		}

		public static bool IsMobileEnabled(int merchantID)
		{
			return (from mmas in DataContext.Reader.SetMerchantMobileApps where mmas.Merchant_id == merchantID select mmas.IsEnableMobileApp).SingleOrDefault();
		}

		public static bool IsSyncToken(int merchantID, Guid syncToken)
		{
			var token = (from mmas in DataContext.Reader.SetMerchantMobileApps where mmas.Merchant_id == merchantID && mmas.SyncToken == syncToken select mmas.SyncToken).SingleOrDefault();
			return token != null;
		}

	}
}
