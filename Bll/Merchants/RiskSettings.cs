﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Bll.Accounts;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Merchants
{
	public class RiskSettings : BaseDataObject
	{
		public const string SecuredObjectName = "RiskSettings";
		public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Merchants.Module.Current, SecuredObjectName); } }

		protected Netpay.Dal.Netpay.SetMerchantRisk _entity;
        
        public int Merchant_id { get { return _entity.Merchant_id; } }

		public byte LimitCcForEmailAllowedCount { get { return _entity.LimitCcForEmailAllowedCount.GetValueOrDefault(); } set { _entity.LimitCcForEmailAllowedCount = value; } }
		public bool IsLimitCcForEmailBlockNewCc { get { return _entity.IsLimitCcForEmailBlockNewCc.GetValueOrDefault(); } set { _entity.IsLimitCcForEmailBlockNewCc = value; } }
		public bool IsLimitCcForEmailBlockAllCc { get { return _entity.IsLimitCcForEmailBlockAllCc.GetValueOrDefault(); } set { _entity.IsLimitCcForEmailBlockAllCc = value; } }
		public bool IsLimitCcForEmailBlockEmail { get { return _entity.IsLimitCcForEmailBlockEmail.GetValueOrDefault(); } set { _entity.IsLimitCcForEmailBlockEmail = value; } }
		public bool IsLimitCcForEmailDeclineTrans { get { return _entity.IsLimitCcForEmailDeclineTrans.GetValueOrDefault(); } set { _entity.IsLimitCcForEmailDeclineTrans = value; } }
		public bool IsLimitCcForEmailNotifyByEmail { get { return _entity.IsLimitCcForEmailNotifyByEmail.GetValueOrDefault(); } set { _entity.IsLimitCcForEmailNotifyByEmail = value; } }
		public string LimitCcForEmailNotifyByEmailList { get { return _entity.LimitCcForEmailNotifyByEmailList; } set { _entity.LimitCcForEmailNotifyByEmailList = value; } }

		public byte LimitEmailForCcAllowedCount { get { return _entity.LimitEmailForCcAllowedCount.GetValueOrDefault(); } set { _entity.LimitEmailForCcAllowedCount = value; } }
		public bool IsLimitEmailForCcBlockNewEmail { get { return _entity.IsLimitEmailForCcBlockNewEmail.GetValueOrDefault(); } set { _entity.IsLimitEmailForCcBlockNewEmail = value; } }
		public bool IsLimitEmailForCcBlockAllEmails { get { return _entity.IsLimitEmailForCcBlockAllEmails.GetValueOrDefault(); } set { _entity.IsLimitEmailForCcBlockAllEmails = value; } }
		public bool IsLimitEmailForCcBlockCc { get { return _entity.IsLimitEmailForCcBlockCc.GetValueOrDefault(); } set { _entity.IsLimitEmailForCcBlockCc = value; } }
		public bool IsLimitEmailForCcDeclineTrans { get { return _entity.IsLimitEmailForCcDeclineTrans.GetValueOrDefault(); } set { _entity.IsLimitEmailForCcDeclineTrans = value; } }
		public bool IsLimitEmailForCcNotifyByEmail { get { return _entity.IsLimitEmailForCcNotifyByEmail.GetValueOrDefault(); } set { _entity.IsLimitEmailForCcNotifyByEmail = value; } }
		public string LimitEmailForCcNotifyByEmailList { get { return _entity.LimitEmailForCcNotifyByEmailList; } set { _entity.LimitEmailForCcNotifyByEmailList = value; } }

		public List<string> CountryWhitelist { get { return _entity.WhitelistCountry.EmptyIfNull().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList(); } set { _entity.WhitelistCountry = string.Join(",", value).NullIfEmpty(); } }
		public List<string> StateWhitelist { get { return _entity.WhitelistState.EmptyIfNull().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList(); } set { _entity.WhitelistState = string.Join(",", value).NullIfEmpty(); } }
		public List<string> CountryBlacklist { get { return _entity.BlacklistCountry.EmptyIfNull().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList(); } set { _entity.BlacklistCountry = string.Join(",", value).NullIfEmpty(); } }
		public List<string> StateBlacklist { get { return _entity.BlacklistState.EmptyIfNull().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList(); } set { _entity.BlacklistState = string.Join(",", value).NullIfEmpty(); } }
		public List<decimal> AllowedAmounts { get { return _entity.AmountAllowedList.EmptyIfNull().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(amount => decimal.Parse(amount)).ToList(); } set { _entity.AmountAllowedList = string.Join(",", value.Select(v => v.ToString())).NullIfEmpty(); } }

		public bool IsCreditCardWhitelistEnabled { get { return _entity.IsEnableCcWhiteList.GetValueOrDefault(); } set { _entity.IsEnableCcWhiteList = value; } }


        // Four properties that belong to tblCompany table:
        public bool IsForce3dSecure { get; set; }
        public bool IsForceBillingAddress { get; set; }
        public bool IsForceBillingAddressOnlyZipcodeAndCountryAreRequired { get; set; }
        public bool IsForceInstantDebitBillingAddress { get; set; }

        private bool _isNew = true;
		private RiskSettings(Netpay.Dal.Netpay.SetMerchantRisk entity, Netpay.Dal.Netpay.tblCompany company)
		{
            _entity = entity;
                       
            IsForce3dSecure = company.IsAllow3DTrans;
            IsForceBillingAddress = company.IsBillingAddressMust;
            IsForceBillingAddressOnlyZipcodeAndCountryAreRequired = company.IsBillingCityOptional;
            IsForceInstantDebitBillingAddress = company.IsBillingAddressMustIDebit;
            _isNew = false;
		}

		public RiskSettings(int merchantID)
		{
            _entity = new Dal.Netpay.SetMerchantRisk();
			_entity.Merchant_id = merchantID;
		}

		public static RiskSettings Load(int merchantID)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
			//AccountFilter.Current.Validate(Accounts.AccountType.Merchant, merchantID);
			var entity = (from mmas in DataContext.Reader.SetMerchantRisks 
				join m in DataContext.Reader.tblCompanies on mmas.Merchant_id equals m.ID
				where mmas.Merchant_id == merchantID select new { mmas, m }).SingleOrDefault();
			if (entity == null) return null;
			return new RiskSettings(entity.mmas, entity.m);
		}

		public void Save()
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

			DataContext.Writer.SetMerchantRisks.Update(_entity, !_isNew);
			DataContext.Writer.SubmitChanges();
			_isNew = false;
		}
	}
}
