﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Merchants
{
    public class GradedFees : BaseDataObject
    {
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Settlements.MerchantSettings.SecuredObject; } }

        private Dal.Netpay.tblCompanyFeesFloor _entity;

        public int ID { get { return _entity.CFF_ID; } }

        public int CompanyID {get { return _entity.CFF_CompanyID; } set { _entity.CFF_CompanyID = value; } }

        public decimal TotalMin { get { return _entity.CFF_TotalTo; } set { _entity.CFF_TotalTo = value; } }

        public decimal FeePercent { get { return _entity.CFF_Precent; } set { _entity.CFF_Precent = value; } }

        private GradedFees(Dal.Netpay.tblCompanyFeesFloor entity)
        {
            _entity = entity;
        }

        public GradedFees()
        {
            _entity = new Dal.Netpay.tblCompanyFeesFloor();
        }

        public static List<GradedFees> LoadForMerchant(int merchantid)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            if (merchantid <= 0) return null;

            var list = (from gf in DataContext.Reader.tblCompanyFeesFloors
                        where gf.CFF_CompanyID == merchantid
                        select new GradedFees(gf)).ToList();

            return list;
        }

        public static GradedFees Load(int id)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            if (id <= 0) return null;
            return DataContext.Reader.tblCompanyFeesFloors.Where(x => x.CFF_ID == id).Select(x => new GradedFees(x)).SingleOrDefault();
        }
        
        public void Delete()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            if (_entity.CFF_ID == 0) return;

            DataContext.Writer.tblCompanyFeesFloors.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }

        public void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            DataContext.Writer.tblCompanyFeesFloors.Update(_entity, (_entity.CFF_ID != 0));
            DataContext.Writer.SubmitChanges();
        }
    }
}
