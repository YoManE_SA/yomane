using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Bll.Accounts;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System.IO;
using System.Linq.Expressions;

namespace Netpay.Bll.Merchants
{

    public class Merchant : Bll.Accounts.Account
    {
        private class BlackListSearchResultItem
        {
            public Dal.Netpay.tblCompany c { get; set; }
            public AccountLoadCombined a { get; set; }
            public Dal.Netpay.MerchantActivity maIncludeEmpty { get; set; }
        }

        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Merchants.Module.Current, SecuredObjectName); } }

        public static Infrastructure.Security.SecuredObject StatusSecuredObject { get { return SecuredObject.Get(Merchants.Status.Module.Current, Merchants.Status.Module.SecuredObjectName); } }

        public static Dictionary<Infrastructure.MerchantStatus, System.Drawing.Color> MerchantStatusColor = new Dictionary<Infrastructure.MerchantStatus, System.Drawing.Color> {
            { Infrastructure.MerchantStatus.Archived, System.Drawing.ColorTranslator.FromHtml("#7d7d7d") } ,
            { Infrastructure.MerchantStatus.Blocked, System.Drawing.ColorTranslator.FromHtml("#ff8040") },
            { Infrastructure.MerchantStatus.Closed, System.Drawing.ColorTranslator.FromHtml("#ff6666") },
            { Infrastructure.MerchantStatus.Integration, System.Drawing.ColorTranslator.FromHtml("#ffeb65") },
            { Infrastructure.MerchantStatus.LoginOnly, System.Drawing.ColorTranslator.FromHtml("#9e6cff") },
            { Infrastructure.MerchantStatus.New, System.Drawing.ColorTranslator.FromHtml("#6699cc") },
            { Infrastructure.MerchantStatus.Processing, System.Drawing.ColorTranslator.FromHtml("#66cc66") }
        };

        public static void SetMerchantsGroups(int groupID)
        {
            if (!DataContext.Reader.tblMerchantGroups.Any(x => x.ID == groupID)) throw new Exception("GroupID value not valid");
            DataContext.Reader.ExecuteCommand("UPDATE tblCompany SET GroupID={0} WHERE GroupID is null", groupID);
        }

        public class BlackListSearchFilters
        {
            public int? CompanyID;
            public string CompanyName;
            public string CompanyLegalNumber;
            public string IDNumber;
            public string FirstName;
            public string LastName;
            public string Phone;
            public string CompanyFax;
            public string Cellular;
            public string Mail;
            public string URL;
        }

        public class SearchFilters
        {
            public bool? MustHaveLoginDetails;
            public int? MerchantId;
            public BlackListSearchFilters BlackListSearchFilters;
            public bool ShowOnlyInactiveMerchants;
            public Range<int?> ID;
            public Range<DateTime?> OpenDate;
            public Range<DateTime?> CloseDate;
            public int? IndustryID;
            public int? Department;
            public List<Netpay.Infrastructure.MerchantStatus> Status;
            public string Name;
            public string Text;
            public int? Group;
            public string AccountManager;
        }

        private Transactions.TransactionsStatus _processStatus;
        protected Netpay.Dal.Netpay.tblCompany _entity;

        public int ID { get { return _entity.ID; } }
        public string Number { get { return _entity.CustomerNumber; } }
        public Netpay.Infrastructure.MerchantStatus ActiveStatus
        {
            get { return (Netpay.Infrastructure.MerchantStatus)_entity.ActiveStatus; }
            set
            {
                _entity.ActiveStatus = (byte)value;
            }
        }
        //public decimal HandlingFee { get; set; }


        //Risk settings properties , the are being edited in merchant managment -> risk tab:
        public bool IsAllow3DTrans { get { return _entity.IsAllow3DTrans; } set { _entity.IsAllow3DTrans = value; } }

        public bool IsBillingAddressMust { get { return _entity.IsBillingAddressMust; } set { _entity.IsBillingAddressMust = value; } }

        public bool IsBillingCityOptional { get { return _entity.IsBillingCityOptional; } set { _entity.IsBillingCityOptional = value; } }

        public bool IsBillingAddressMustIDebit { get { return _entity.IsBillingAddressMustIDebit; } set { _entity.IsBillingAddressMustIDebit = value; } }


        public string Name { get { return _entity.CompanyName; } set { _entity.CompanyName = value; base.AccountName = value; } }
        public string ChargeBackNotifyEmail { get { return _entity.ChargebackNotifyMail; } set { _entity.ChargebackNotifyMail = value; } }
        public string EmailAddress { get { return _entity.Mail; } set { _entity.Mail = value; } }
        public string UserName { get { return _entity.UserName; } set { _entity.UserName = value; } }
        public override string HashKey { get { return _entity.HashKey.NullIfEmpty().ValueIfNull(base.HashKey); } set { base.HashKey = _entity.HashKey = value; } }
        public string SecurityKey { get { return _entity.SecurityKey; } set { _entity.SecurityKey = value; } }
        public bool ShowSensativeData { get { return _entity.IsShowSensitiveData; } set { _entity.IsShowSensitiveData = value; } }
        public bool IsCompanyTerminal { get { return _entity.isNetpayTerminal; } set { _entity.isNetpayTerminal = value; } }

        public bool IsInBlackList
        {
            get
            {
                if (ID != 0)
                {
                    return MerchantBlackList.MerchantBlackListSettings.CheckIfMerchantIsBlackListed(ID);
                }
                else
                {
                    return false;
                }
            }
        }


        public string ContactFirstName { get { return _entity.FirstName; } set { _entity.FirstName = value; } }
        public string ContactLastName { get { return _entity.LastName; } set { _entity.LastName = value; } }
        public string ContactPhone { get { return _entity.Phone; } set { _entity.Phone = value; } }
        public string ContactMobilePhone { get { return _entity.cellular; } set { _entity.cellular = value; } }
        public string ContactEmail { get { return _entity.ContactMail; } set { _entity.ContactMail = value; } }
        public string Mail { get { return _entity.Mail; } set { _entity.Mail = value; } }
        public bool IsInterestedInNewsletter { get { return _entity.IsInterestedInNewsletter; } set { _entity.IsInterestedInNewsletter = value; } }

        public string LegalName { get { return _entity.CompanyLegalName; } set { _entity.CompanyLegalName = value; } }
        public string LegalNumber { get { return _entity.CompanyLegalNumber; } set { _entity.CompanyLegalNumber = value; } }
        public string BillingName { get { return _entity.BillingFor; } set { _entity.BillingFor = value; } }
        public string OwnerSSN { get { return _entity.IDnumber; } set { _entity.IDnumber = value; } }
        public string Url { get { return _entity.URL; } set { _entity.URL = value; } }
        public string Phone { get { return _entity.CompanyPhone; } set { _entity.CompanyPhone = value; } }
        public string Fax { get { return _entity.CompanyFax; } set { _entity.CompanyFax = value; } }
        public string SupportEmail { get { return _entity.merchantSupportEmail; } set { _entity.merchantSupportEmail = value; } }
        public string SupportPhone { get { return _entity.merchantSupportPhoneNum; } set { _entity.merchantSupportPhoneNum = value; } }
        public string Descriptor { get { return _entity.descriptor; } set { _entity.descriptor = value; } }
        public string Identifier { get { return _entity.debitCompanyExID; } set { _entity.debitCompanyExID = value; } }
        public int Industry { get { return _entity.CompanyIndustry_id; } set { _entity.CompanyIndustry_id = (short)value; } }
        public string IpOnReg { get { return _entity.IpOnReg; } set { _entity.IpOnReg = value; } }

        public string LanguagePreference { get { return _entity.languagePreference; } set { _entity.languagePreference = value; } }
        public DateTime? InsertDate { get { return _entity.InsertDate; } set { _entity.InsertDate = value.GetValueOrDefault(); } }
        public DateTime? OpenDate { get { return _entity.merchantOpenningDate; } set { _entity.merchantOpenningDate = value.GetValueOrDefault(); } }
        public DateTime? CloseDate { get { return _entity.merchantClosingDate; } set { _entity.merchantClosingDate = value.GetValueOrDefault(); } }
        public string LinkName { get { return _entity.MerchantLinkName; } set { _entity.MerchantLinkName = value; } }
        public int? ParentCompany { get { return _entity.ParentCompany; } set { _entity.ParentCompany = value; } }
        public int? MerchantGroup { get { return _entity.GroupID; } set { _entity.GroupID = value; } }
        public int BillingCompanysId { get { return _entity.BillingCompanys_id.GetValueOrDefault(); } set { _entity.BillingCompanys_id = value; } }
        public int? Department { get { return _entity.MerchantDepartment_id; } set { _entity.MerchantDepartment_id = (byte?)value; } }
        public string AccountManager { get { return _entity.careOfAdminUser; } set { _entity.careOfAdminUser = value; } }

        public string GetAccountManagerIdAsString
        {
            get
            {
                if (string.IsNullOrEmpty(AccountManager))
                {
                    return "0";
                }
                else
                {
                    int num;
                    if (int.TryParse(AccountManager, out num))
                    {
                        return num.ToString();
                    }
                    else
                    {
                        return AdminUser.GetAdminUserIdByName(AccountManager);
                    }
                }
            }
        }

        public string Comment { get { return _entity.comment; } set { _entity.comment = value; } }
        public bool? ForceCCStorageMD5 { get { return _entity.ForceCCStorageMD5; } set { _entity.ForceCCStorageMD5 = value; } }


        //Payout issue
        public byte? PreferredWireType { get { return _entity.PreferredWireType; } set { _entity.PreferredWireType = value; } }
        public bool IsChargeVAT { get { return _entity.isChargeVAT; } set { _entity.isChargeVAT = value; } }
        public decimal PayPercent { get { return _entity.PayPercent; } set { _entity.PayPercent = value; } }
        public Netpay.CommonTypes.Currency? GradedFeeCurrency
        {
            get
            {
                return _entity.CFF_Currency.ToNullableEnum<Netpay.CommonTypes.Currency>();
            }
            set
            {
                _entity.CFF_Currency = value == null ? null : ((int)value).ToNullableInt();
            }
        }
        public int? PaymentRecieveCurrency { get { return _entity.PaymentReceiveCurrency; } set { _entity.PaymentReceiveCurrency = value; } }
        public byte PayingDaysMarginInitial { get { return _entity.PayingDaysMarginInitial; } set { _entity.PayingDaysMarginInitial = value; } }

        public byte PayingDaysMargin { get { return _entity.PayingDaysMargin; } set { _entity.PayingDaysMargin = value; } }
        public string PayingDates1 { get { return _entity.payingDates1; } set { _entity.payingDates1 = value; } }
        public string PayingDates2 { get { return _entity.payingDates2; } set { _entity.payingDates2 = value; } }
        public string PayingDates3 { get { return _entity.payingDates3; } set { _entity.payingDates3 = value; } }
        public string RRComment { get { return _entity.RRComment; } set { _entity.RRComment = value; } }
        public decimal SecurityDeposit { get { return _entity.SecurityDeposit; } set { _entity.SecurityDeposit = value; } }
        public short SecurityPeriod { get { return _entity.SecurityPeriod; } set { _entity.SecurityPeriod = value; } }

        //Rolling Reserve issue
        public byte RRState { get { return _entity.RRState; } set { _entity.RRState = value; } }
        public bool RREnabled { get { return _entity.RREnable; } set { _entity.RREnable = value; } }
        public bool? RRAutoRet { get { return _entity.RRAutoRet; } set { _entity.RRAutoRet = value; } }
        public byte RRKeepCurrency { get { return _entity.RRKeepCurrency; } set { _entity.RRKeepCurrency = value; } }
        public decimal RRKeepAmount { get { return _entity.RRKeepAmount; } set { _entity.RRKeepAmount = value; } }

        public int RRPayID { get { return _entity.RRPayID; } set { _entity.RRPayID = value; } }

        public Transactions.TransactionsStatus ProcessStatus { get { if (_processStatus == null) _processStatus = Transactions.Transaction.GetTransactionCount(Transactions.Transaction.GetTransactionCountGroup.Merchant, new List<int> { ID }).FirstOrDefault(); return _processStatus; } }

        //Adding Date - Time properties that comes from the Track-MerchantActivities table
        public DateTime? DateFirstTransPass { get; private set; }

        public DateTime? DateLastTransPass { get; private set; }

        public DateTime? DateLastTransPending { get; private set; }

        public DateTime? DateLastTransFail { get; private set; }

        public DateTime? DateLastTransPreAuth { get; private set; }

        public DateTime? DateLastSettlement { get; private set; }


        //public Netpay.Infrastructure.VO.MerchantDepositVO DepositInfo { get { return new Netpay.Infrastructure.VO.MerchantDepositVO(_entity); } }
        public ProcessSettings ProcessSettings { get; private set; }

        protected Merchant(Dal.Netpay.tblCompany entity, AccountLoadCombined accountData)
            : base(accountData)
        {
            _entity = entity;
            ProcessSettings = new ProcessSettings(_entity);
        }

        public Merchant() : base(Accounts.AccountType.Merchant)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            _entity = new Dal.Netpay.tblCompany();
            _entity.InsertDate = DateTime.Now;
            _entity.ActiveStatus = (byte)MerchantStatus.New;
            _entity.CFF_ResetDate = DataContext.SQLMinDateValue;
            _entity.lastInsertDate = DataContext.SQLMinDateValue;
            _entity.PasswordUpdate = DataContext.SQLMinDateValue;
            ProcessSettings = new ProcessSettings(_entity);
        }
        public static new Merchant Current
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

                return Accounts.Account.CurrentObject as Merchant;
            }
        }

        public static bool IsMerchantWithoutGroupExist
        {
            get
            {
                return DataContext.Reader.tblCompanies.Any(x => x.GroupID == null);
            }
        }

        public static bool IsMerchantDuplicatesExist(Merchant merchantvar)
        {
            // check new merchant duplicate values as per fraud specs 321
            var result = from account in DataContext.Reader.Accounts
                         join merchant in DataContext.Reader.tblCompanies on account.Merchant_id equals merchant.ID
                         join address in DataContext.Reader.AccountAddresses on account.BusinessAddress_id equals address.AccountAddress_id
                         where
                            merchant.FirstName == merchantvar.ContactFirstName ||
                            merchant.LastName == merchantvar.ContactLastName ||
                            (merchant.CompanyStreet == merchantvar.BusinessAddress.AddressLine1 && merchant.CompanyCity == merchantvar.BusinessAddress.City) ||
                            (address.Street1 == merchantvar.BusinessAddress.AddressLine1 && address.City == merchantvar.BusinessAddress.City) ||
                            merchant.CompanyName == merchantvar.Name ||
                            merchant.CompanyPhone == merchantvar.Phone ||
                            merchant.Phone == merchantvar.Phone ||
                            merchant.IDnumber == merchantvar.OwnerSSN ||
                            merchant.CompanyLegalName == merchantvar.LegalName ||
                            merchant.CompanyLegalNumber == merchantvar.LegalNumber ||
                            merchant.Mail == merchantvar.EmailAddress ||
                            merchant.URL == merchantvar.Url ||
                            merchant.merchantSupportEmail == merchantvar.SupportEmail ||
                            merchant.merchantSupportPhoneNum == merchantvar.SupportPhone
                         select merchant;
            if (result.Count() > 0)
                return true;
            else return false;
        }

        // check new merchant duplicate values as per fraud specs 321
        public void CheckDuplicates()
        {
            if (_entity == null || _entity.ID == 0)
            {
                // merchants
                var predicate = PredicateBuilder.False<Dal.Netpay.tblCompany>();
                if (!string.IsNullOrEmpty(ContactFirstName))
                    predicate = predicate.Or(m => m.FirstName == ContactFirstName);
                if (!string.IsNullOrEmpty(ContactLastName))
                    predicate = predicate.Or(m => m.LastName == ContactLastName);
                if (BusinessAddress != null && !string.IsNullOrEmpty(BusinessAddress.AddressLine1) && !string.IsNullOrEmpty(BusinessAddress.City))
                    predicate = predicate.Or(m => m.CompanyStreet == BusinessAddress.AddressLine1 && m.CompanyCity == BusinessAddress.City);
                if (!string.IsNullOrEmpty(Name))
                    predicate = predicate.Or(m => m.CompanyName == Name);
                if (!string.IsNullOrEmpty(Phone))
                    predicate = predicate.Or(m => m.Phone == Phone);
                if (!string.IsNullOrEmpty(Phone))
                    predicate = predicate.Or(m => m.CompanyPhone == Phone);
                if (!string.IsNullOrEmpty(OwnerSSN))
                    predicate = predicate.Or(m => m.IDnumber == OwnerSSN);
                if (!string.IsNullOrEmpty(LegalName))
                    predicate = predicate.Or(m => m.CompanyLegalName == LegalName);
                if (!string.IsNullOrEmpty(LegalNumber))
                    predicate = predicate.Or(m => m.CompanyLegalNumber == LegalNumber);
                if (!string.IsNullOrEmpty(EmailAddress))
                    predicate = predicate.Or(m => m.Mail == EmailAddress);
                if (!string.IsNullOrEmpty(Url))
                    predicate = predicate.Or(m => m.URL == Url);
                if (!string.IsNullOrEmpty(SupportEmail))
                    predicate = predicate.Or(m => m.merchantSupportEmail == SupportEmail);
                if (!string.IsNullOrEmpty(SupportPhone))
                    predicate = predicate.Or(m => m.merchantSupportPhoneNum == SupportPhone);

                var result = from merchant in DataContext.Reader.tblCompanies select merchant;
                result = result.Where(predicate);

                if (result.Count() > 0)
                {
                    string idlist = string.Join(",", result.Select(x => x.ID.ToString()).ToArray());
                    throw new MerchantDuplicateFoundException("Active merchant duplicate value found. fraud spec 312<br>Merchants Id's list : " + idlist + "<br>If you want to save anyway, click Save.;");
                }

                // merchant black list
                var predicateBlackList = PredicateBuilder.False<Dal.Netpay.BlacklistMerchant>();
                if (!string.IsNullOrEmpty(ContactFirstName))
                    predicateBlackList = predicateBlackList.Or(m => m.FirstName == ContactFirstName);
                if (!string.IsNullOrEmpty(ContactLastName))
                    predicateBlackList = predicateBlackList.Or(m => m.LastName == ContactLastName);
                if (BusinessAddress != null && !string.IsNullOrEmpty(BusinessAddress.AddressLine1) && !string.IsNullOrEmpty(BusinessAddress.City))
                    predicateBlackList = predicateBlackList.Or(m => m.CompanyStreet == BusinessAddress.AddressLine1 && m.CompanyCity == BusinessAddress.City);
                if (!string.IsNullOrEmpty(Name))
                    predicateBlackList = predicateBlackList.Or(m => m.CompanyName == Name);
                if (!string.IsNullOrEmpty(Phone))
                    predicateBlackList = predicateBlackList.Or(m => m.Phone == Phone);
                if (!string.IsNullOrEmpty(Phone))
                    predicateBlackList = predicateBlackList.Or(m => m.CompanyPhone == Phone);
                if (!string.IsNullOrEmpty(OwnerSSN))
                    predicateBlackList = predicateBlackList.Or(m => m.IDNumber == OwnerSSN);
                if (!string.IsNullOrEmpty(LegalName))
                    predicateBlackList = predicateBlackList.Or(m => m.CompanyLegalName == LegalName);
                if (!string.IsNullOrEmpty(LegalNumber))
                    predicateBlackList = predicateBlackList.Or(m => m.CompanyLegalNumber == LegalNumber);
                if (!string.IsNullOrEmpty(EmailAddress))
                    predicateBlackList = predicateBlackList.Or(m => m.Mail == EmailAddress);
                if (!string.IsNullOrEmpty(Url))
                    predicateBlackList = predicateBlackList.Or(m => m.URL == Url);
                if (!string.IsNullOrEmpty(SupportEmail))
                    predicateBlackList = predicateBlackList.Or(m => m.MerchantSupportEmail == SupportEmail);
                if (!string.IsNullOrEmpty(SupportPhone))
                    predicateBlackList = predicateBlackList.Or(m => m.MerchantSupportPhoneNum == SupportPhone);

                var resultBlackList = from merchant in DataContext.Reader.BlacklistMerchants select merchant;
                resultBlackList = resultBlackList.Where(predicateBlackList);

                if (resultBlackList.Count() > 0)
                    throw new Exception("Blocked merchant duplicate value found. fraud spec 312");
            }
        }

        public void Save(bool checkDuplicates = true)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            if (checkDuplicates) CheckDuplicates();

            if (_entity.merchantClosingDate < DataContext.SQLMinDateValue) _entity.merchantClosingDate = DataContext.SQLMinDateValue;
            if (_entity.merchantOpenningDate < DataContext.SQLMinDateValue) _entity.merchantOpenningDate = DataContext.SQLMinDateValue;
            if (_entity.ID == 0)
            {
                if (_entity.ParentCompany == null) _entity.ParentCompany = DataContext.Reader.tblParentCompanies.First().ID;
                _entity.CustomerNumber = GenerateNumber();
                _entity.HashKey = Infrastructure.Security.Encryption.GetHashKey(10);
            }

            if (BusinessAddress != null)
            {
                _entity.CompanyStreet = BusinessAddress.AddressLine1;
                _entity.CompanyCity = BusinessAddress.City;
                _entity.CompanyZIP = BusinessAddress.PostalCode;
                if (!string.IsNullOrEmpty(BusinessAddress.CountryISOCode)) _entity.CompanyCountry = Country.Get(BusinessAddress.CountryISOCode).ID;
                if (!string.IsNullOrEmpty(BusinessAddress.StateISOCode)) _entity.CompanyState = State.Get(BusinessAddress.StateISOCode).Name;
            }
            if (PersonalAddress != null)
            {
                _entity.Street = PersonalAddress.AddressLine1;
                _entity.City = PersonalAddress.City;
                _entity.ZIP = PersonalAddress.PostalCode;
                if (!string.IsNullOrEmpty(PersonalAddress.CountryISOCode)) _entity.Country = Country.Get(PersonalAddress.CountryISOCode).ID;
                if (!string.IsNullOrEmpty(PersonalAddress.StateISOCode)) _entity.State = State.Get(PersonalAddress.StateISOCode).Name;
            }

            /***update nvarcar non null fields set string.Empty*/
            var notnullvarcharFields = "AllowedAmounts,BillingFor,careOfAdminUser,CCardCUI,CCardExpMM,CCardExpYY,CCardHolderName,cellular,ChargebackNotifyMail,City,comment,comment_misc," +
                "CompanyCity,CompanyFax,CompanyLegalName,CompanyLegalNumber,CompanyName,CompanyPhone,CompanyState,CompanyStreet,CompanyZIP,CustomerNumber,CustomerPurchasePayerIDText," +
                "CyclePeriod,debitCompanyExID,descriptor,FirstName,HashKey,IDnumber,IpOnReg,languagePreference,LastName,Mail,merchantSupportEmail,merchantSupportPhoneNum,payingDates1,payingDates2," +
                "payingDates3,,PaymentAbroadABA,PaymentAbroadABA2,PaymentAbroadAccountName,PaymentAbroadAccountName2,PaymentAbroadAccountNumber,PaymentAbroadAccountNumber2,PaymentAbroadBankAddress," +
                "PaymentAbroadBankAddress2,,PaymentAbroadBankAddressCity,PaymentAbroadBankAddressCity2,PaymentAbroadBankAddressSecond,PaymentAbroadBankAddressSecond2,PaymentAbroadBankAddressState,PaymentAbroadBankAddressState2," +
                "PaymentAbroadBankAddressZip,,PaymentAbroadBankAddressZip2,PaymentAbroadBankName,PaymentAbroadBankName2,PaymentAbroadIBAN,PaymentAbroadIBAN2,PaymentAbroadSortCode,PaymentAbroadSortCode2,PaymentAbroadSwiftNumber," +
                "PaymentAbroadSwiftNumber2,PaymentAccount,PaymentBranch,PaymentMethod,PaymentPayeeName,Phone,PslWalletCode,PslWalletId,referralName,RemotePullIPs,RemoteRefundRequestIPs,SecurityKey,State,Street,URL,UserName,UserNameAlt,ZIP," +
                "countryBlackList,countryWhiteList,IPWhiteList,PassNotifyEmail,PaymentAbroadSepaBic,PaymentAbroadSepaBic2,RiskMailCcNotifyEmail,ZecureAccount,ZecurePassword,ZecureUsername";

            foreach (var n in notnullvarcharFields.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                var x = _entity.GetType().GetProperty(n).GetValue(_entity, null);
                if (x == null) _entity.GetType().GetProperty(n).SetValue(_entity, string.Empty, null);
            }

            DataContext.Writer.tblCompanies.Update(_entity, (_entity.ID != 0));
            DataContext.Writer.SubmitChanges();
            if (_accountEntity.Merchant_id == null) _accountEntity.Merchant_id = _entity.ID;
            base.SaveAccount();
            Domain.RemoveCachData("MerchantNames");
        }

        public static Dictionary<int, string> CachedNamesForDomain()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var cachData = Domain.Current.GetCachData("MerchantNames") as Dictionary<int, string>;
            if (cachData != null) return cachData;
            cachData = (from m in DataContext.Reader.tblCompanies select new { m.ID, m.CompanyName }).ToDictionary(m => m.ID, m => m.CompanyName);
            Domain.Current.SetCachData("MerchantNames", cachData, DateTime.Now.AddHours(2));
            return cachData;
        }

        public static Dictionary<string, int> CachedNumbersForDomain()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var cachData = Domain.Current.GetCachData("MerchantNumbers") as Dictionary<string, int>;
            if (cachData != null) return cachData;
            cachData = (from m in DataContext.Reader.tblCompanies select new { m.ID, m.CustomerNumber }).ToDictionary(m => m.CustomerNumber, m => m.ID);
            Domain.Current.SetCachData("MerchantNumbers", cachData, DateTime.Now.AddHours(2));
            return cachData;
        }

        public static List<Accounts.AccountInfo> AutoComplete(string text)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            //context.IsUserOfType(null);
            int numValue = text.ToNullableInt().GetValueOrDefault(-1);
            return (from a in DataContext.Reader.Accounts
                    join c in DataContext.Reader.tblCompanies on a.Merchant_id equals c.ID
                    where c.CompanyName.StartsWith(text) || c.CompanyLegalName.StartsWith(text) || a.Account_id == numValue
                    select new Accounts.AccountInfo()
                    {
                        AccountID = a.Account_id,
                        TargetID = c.ID,
                        AccountName = c.CompanyName,
                        AccountType = Accounts.AccountType.Merchant,
                        AccountStatus = ((MerchantStatus)c.ActiveStatus).ToString(),
                        StatusColor = MerchantStatusColor[(MerchantStatus)c.ActiveStatus]
                    }).Take(10).ToList();
        }

        public static List<KeyValuePair<string, string>> AutoCompleteByMerchantNumber(string text)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            int numValue = text.ToNullableInt().GetValueOrDefault(-1);
            return (from c in DataContext.Reader.tblCompanies
                    where c.CustomerNumber.StartsWith(text)
                    select new { c.CustomerNumber, c.CompanyName })
                    .ToDictionary(x => x.CustomerNumber, x => x.CompanyName)
                    .Take(10).ToList();
        }

        public static Merchant Load(int? id = null)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant, UserRole.Partner }, SecuredObject, PermissionValue.Read);

            AccountFilter.Current.Validate(Accounts.AccountType.Merchant, ref id);
            return Load(new List<int> { id.Value }).FirstOrDefault();
        }

        public static List<Merchant> Load(List<int> Ids)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant, UserRole.Partner }, SecuredObject, PermissionValue.Read);

            AccountFilter.Current.FilterIDList(Accounts.AccountType.Merchant, Ids);
            return (from a in Accounts.Account.LoadQuery(DataContext.Reader)
                    join c in DataContext.Reader.tblCompanies on a.account.Merchant_id equals c.ID
                    where Ids.Contains(c.ID)
                    select new Merchant(c, a)).ToList();
        }

        public static Merchant LoadByAccountId(int accountId)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant, UserRole.Partner }, SecuredObject, PermissionValue.Read);

            return (from a in Accounts.Account.LoadQuery(DataContext.Reader)
                    join c in DataContext.Reader.tblCompanies on a.account.Merchant_id equals c.ID
                    where accountId == a.account.Account_id
                    select new Merchant(c, a))
                    .SingleOrDefault();
        }

        public static Merchant Load(string merchantNumber)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from a in Accounts.Account.LoadQuery(DataContext.Reader, false)
                    join c in DataContext.Reader.tblCompanies on a.account.Merchant_id equals c.ID
                    where c.CustomerNumber == merchantNumber
                    select new Merchant(c, a)).SingleOrDefault();
        }

        public static IList<MerchantDto> GetMerchantList(string customerNumber,int? merchantId,int? accountId,int?accountTypeId)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);


            Dal.Netpay.NetpayDataContextBase dc = new Dal.Netpay.NetpayDataContextBase(Domain.Current.Sql1ConnectionString);
            var merchantlist = dc.Merchants_Get(customerNumber,merchantId,accountId,accountTypeId);
            IList<MerchantDto> dicMerchants = new List<MerchantDto>();
            foreach (var m in merchantlist)
            {
                var item = new MerchantDto();
                if (m.MerchantId.HasValue)
                    item.MerchantId = Convert.ToInt32(m.MerchantId);
                if (m.AccountId.HasValue)
                    item.AccountId = Convert.ToInt32(m.AccountId);
                if (m.AccountTypeId.HasValue)
                    item.AccountTypeId = Convert.ToInt32(m.AccountTypeId);
                item.CompanyName = m.CompanyName;
                item.CurrencyISOCode = m.CurrencyISOCode;
                item.CustomerNumber = m.CustomerNumber;

              
                dicMerchants.Add(item);
            }

            return dicMerchants;
        }

        public static List<int> GetMerchantIdsByMerchantNumber(string merchantnumber)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from c in DataContext.Reader.tblCompanies
                    where c.CustomerNumber == merchantnumber
                    select c.ID).ToList();
        }

        public Dictionary<string, string> GetLinkedMerchants()
        {
            //Check for Admin
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            //Check for ControlPanel
            ObjectContext.Current.IsUserOfType(UserRole.Merchant);
            return (from c in DataContext.Reader.tblCompanies
                    where c.MerchantLinkName == LinkName
                    orderby c.CustomerNumber ascending
                    select new { id = c.CustomerNumber, name = c.CustomerNumber + " - " + c.CompanyLegalName }).ToDictionary(c => c.id, c => c.name);
        }

        public LoginResult LoginLinkedMerchant(string merchantNumber, out System.Guid retCredentialsToken)
        {
            ObjectContext.Current.IsUserOfType(UserRole.Merchant);
            var merchantLogin = Merchant.Load(merchantNumber);
            if (merchantLogin == null || Merchant.Current.LinkName != merchantLogin.LinkName || merchantLogin.Login == null) throw new ApplicationException("Specified merchant not found:" + merchantNumber);

            string merchantPassword = null;
            ObjectContext.Current.Impersonate(Domain.ServiceCredentials);
            try
            {
                merchantPassword = Infrastructure.Security.Login.GetPassword(merchantLogin.LoginID.GetValueOrDefault(), null); // get password (from p in DataContext.Reader.tblPasswordHistories where p.LPH_RefID == merchantLogin.ID && p.LPH_RefType == (int)UserRole.MerchantPrimary && p.LPH_ID == 1 select p.LPH_Password256).SingleOrDefault();
            }
            finally
            {
                ObjectContext.Current.StopImpersonate();
            }

            if (merchantPassword == null) throw new ApplicationException("Merchant's login information could not be found:" + merchantNumber);
            LoginResult lr = Login.DoLogin(new UserRole[] { UserRole.Merchant }, merchantLogin.Login.UserName, merchantLogin.Login.EmailAddress, merchantPassword, null, out retCredentialsToken);
            return lr;
        }

        public static Dictionary<int, string> GetCompanyNames(List<int> ids)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from tc in DataContext.Reader.tblCompanies
                    where ids.Contains(tc.ID)
                    select new { tc.ID, tc.CompanyName })
                    .ToDictionary(v => v.ID, v => v.CompanyName);
        }

        public static List<Bll.Currency> GetSupportedCurrencies(int merchantID)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            int[] ids = (from ccf in DataContext.Reader.tblCompanyCreditFees where ccf.CCF_CompanyID == merchantID && !ccf.CCF_IsDisabled select ccf.CCF_CurrencyID).Distinct().ToArray();
            return Bll.Currency.Search().Where(c => ids.Contains(c.ID)).ToList();
        }

        public static Netpay.Dal.Reports.DailyRiskReportByMerchant GetRiskStats(int merchantId)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);

            AccountFilter.Current.Validate(Accounts.AccountType.Merchant, merchantId);
            using (var dc = new Netpay.Dal.DataAccess.ReportsDataContext(Domain.Current.ReportsConnectionString))
            {
                Netpay.Dal.Reports.DailyRiskReportByMerchant data = (from drrc in dc.DailyRiskReportByMerchants where drrc.CompanyID == merchantId select drrc).SingleOrDefault();
                return data;
            }
        }

        public static System.Collections.Generic.Dictionary<CommonTypes.Currency, decimal> GetMonthlyTotalDebit(int merchantID)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);

            AccountFilter.Current.Validate(Accounts.AccountType.Merchant, merchantID);
            using (var dc = new Netpay.Dal.DataAccess.ReportsDataContext(Domain.Current.ReportsConnectionString))
                return dc.spGetMerchantMonthlyTotal(merchantID).ToDictionary(f => (CommonTypes.Currency)f.CurrencyID.GetValueOrDefault(), f => f.MonthlyDebitAmount.GetValueOrDefault());
        }

        public static List<string> GetAvailableContentLanguges(int accountId)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);


            List<string> languages = new List<string>();
            string contentPath = Accounts.Account.MapPrivatePath(accountId, "Content/");
            if (!Directory.Exists(contentPath))
                return languages;

            string[] files = Directory.GetFiles(contentPath);
            if (files.Length == 0)
                return languages;

            foreach (string file in files)
            {
                string fileName = Path.GetFileNameWithoutExtension(file);
                if (!fileName.Contains('_'))
                    continue;

                string languege = fileName.Split('_')[1];
                if (languege.Length == 2)
                    languages.Add(languege.ToLower());
            }

            languages = languages.Distinct().ToList();
            return languages;
        }

        public static void SaveContent(int accountId, CommonTypes.Language? language, string fileName, string contents)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);


            string path = System.IO.Path.GetFileNameWithoutExtension(fileName);
            if (language != null) path += "_" + International.Language.Get(language.Value).IsoCode2;
            path += System.IO.Path.GetExtension(fileName);
            path = Accounts.Account.MapPrivatePath(accountId, "Content/" + path); //"RulesAndRegulations.html"
            if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(path)))
                System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(path));
            System.IO.File.WriteAllText(path, contents);
        }

        public static void RemoveContent(int accountId, CommonTypes.Language? language, string fileName)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);


            string path = System.IO.Path.GetFileNameWithoutExtension(fileName);
            if (language != null) path += "_" + International.Language.Get(language.Value).IsoCode2;
            path += System.IO.Path.GetExtension(fileName);
            path = Accounts.Account.MapPrivatePath(accountId, "Content/" + path);

            if (System.IO.File.Exists(path))
                System.IO.File.Delete(path);
        }
        public static string GetCleanString(string dirtyString)
        {
            if (string.IsNullOrEmpty(dirtyString)) return string.Empty;

            string removeChars = " ?&^$#@!()+-,:;<>’\'-_*";
            string result = dirtyString;

            foreach (char c in removeChars)
            {
                result = result.Replace(c.ToString(), string.Empty);
            }

            return result;
        }
        public static string GetContent(int accountId, CommonTypes.Language? language, string fileName)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);


            string path = System.IO.Path.GetFileNameWithoutExtension(fileName);
            if (language != null) path += "_" + International.Language.Get(language.Value).IsoCode2;
            path += System.IO.Path.GetExtension(fileName);
            path = Accounts.Account.MapPrivatePath(accountId, "Content/" + path); //"RulesAndRegulations.html"
            if (System.IO.File.Exists(path)) return System.IO.File.ReadAllText(path);
            path = Accounts.Account.MapPrivatePath(accountId, "Content/" + fileName); //specific language not exist
            if (!System.IO.File.Exists(path)) return null;
            return System.IO.File.ReadAllText(path);
        }


        public static List<Merchant> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var exp = (from a in Accounts.Account.LoadQuery(DataContext.Reader)
                       join c in DataContext.Reader.tblCompanies on a.account.Merchant_id equals c.ID
                       join ma in DataContext.Reader.MerchantActivities on c.ID equals ma.Merchant_id into maAll
                       from maIncludeEmpty in maAll.DefaultIfEmpty()
                       select new { c = c, a = a, maIncludeEmpty = maIncludeEmpty });
            var dc = DataContext.Reader;

            if (filters.BlackListSearchFilters != null)
            {
                var expMerchants = from m in DataContext.Reader.tblCompanies select m;
                var BLF = filters.BlackListSearchFilters;
                var predicate = PredicateBuilder.False<Dal.Netpay.tblCompany>();

                if (!string.IsNullOrEmpty(BLF.CompanyName)) predicate = predicate.Or(p => ((dc.GetCleanText(p.CompanyName)).Contains(BLF.CompanyName)));
                if (BLF.CompanyID.HasValue) predicate = predicate.Or(p => p.ID == BLF.CompanyID);
                if (!string.IsNullOrEmpty(BLF.CompanyLegalNumber)) predicate = predicate.Or(p => ((dc.GetCleanText(p.CompanyLegalNumber)).Contains(BLF.CompanyLegalNumber)));
                if (!string.IsNullOrEmpty(BLF.IDNumber)) predicate = predicate.Or(p => ((dc.GetCleanText(p.IDnumber)).Contains(BLF.IDNumber)));
                if (!string.IsNullOrEmpty(BLF.FirstName)) predicate = predicate.Or(p => ((dc.GetCleanText(p.FirstName)).Contains(BLF.FirstName)));
                if (!string.IsNullOrEmpty(BLF.LastName)) predicate = predicate.Or(p => ((dc.GetCleanText(p.LastName)).Contains(BLF.LastName)));
                if (!string.IsNullOrEmpty(BLF.Phone)) predicate = predicate.Or(p => ((dc.GetCleanText(p.Phone)).Contains(BLF.Phone)));
                if (!string.IsNullOrEmpty(BLF.Cellular)) predicate = predicate.Or(p => ((dc.GetCleanText(p.cellular)).Contains(BLF.Cellular)));
                if (!string.IsNullOrEmpty(BLF.Mail)) predicate = predicate.Or(p => ((dc.GetCleanText(p.Mail)).Contains(BLF.Mail)));
                if (!string.IsNullOrEmpty(BLF.URL)) predicate = predicate.Or(p => ((dc.GetCleanText(p.URL)).Contains(BLF.URL)));

                List<int> merchantIDs = expMerchants.Where(predicate).Select(x => x.ID).ToList();
                if (merchantIDs.Count > 0) exp = exp.Where(x => merchantIDs.Contains(x.c.ID));
            }

            else if (filters != null)
            {
                if (filters.MustHaveLoginDetails.HasValue && filters.MustHaveLoginDetails.Value) exp = exp.Where(x => x.a.account.LoginAccount_id.HasValue);
                if (filters.MerchantId.HasValue) exp = exp.Where((x) => x.c.ID == filters.MerchantId.Value);
                if (filters.ID.From.HasValue) exp = exp.Where((x) => x.a.account.Account_id >= filters.ID.From.Value);
                if (filters.ID.To.HasValue) exp = exp.Where((x) => x.a.account.Account_id <= filters.ID.To.Value);
                if (filters.OpenDate.From.HasValue) exp = exp.Where((x) => x.c.merchantOpenningDate >= filters.OpenDate.From.Value);
                if (filters.OpenDate.To.HasValue) exp = exp.Where((x) => x.c.merchantOpenningDate <= filters.OpenDate.To.Value.AlignToEnd());
                if (filters.CloseDate.From.HasValue) exp = exp.Where((x) => x.c.merchantClosingDate >= filters.CloseDate.From.Value);
                if (filters.CloseDate.To.HasValue) exp = exp.Where((x) => x.c.merchantClosingDate <= filters.CloseDate.To.Value.AlignToEnd());

                if (filters.ShowOnlyInactiveMerchants) exp = exp.Where((x) => x.c.ActiveStatus != (byte)MerchantStatus.Processing);
                if (filters.IndustryID.HasValue) exp = exp.Where((x) => x.c.CompanyIndustry_id == filters.IndustryID.Value);
                if (filters.Department.HasValue) exp = exp.Where((x) => x.c.MerchantDepartment_id == filters.Department.Value);
                if (filters.Status != null && filters.Status.Count > 0) exp = exp.Where((x) => filters.Status.Contains((MerchantStatus)x.c.ActiveStatus));
                if (filters.Group != null)
                {
                    if (filters.Group.Value == 0)
                    {
                        exp = exp.Where((x) => x.c.GroupID == null);
                    }
                    else
                    {
                        exp = exp.Where((x) => x.c.GroupID == filters.Group);
                    }
                }
                if (!string.IsNullOrEmpty(filters.AccountManager))
                {
                    int num;
                    if (int.TryParse(filters.AccountManager, out num))
                    {
                        exp = exp.Where((x) => (x.c.careOfAdminUser == filters.AccountManager) || (x.c.careOfAdminUser == AdminUser.GetAdminUserNameById(int.Parse(filters.AccountManager))));
                    }
                    else
                    {
                        exp = exp.Where((x) => (x.c.careOfAdminUser == filters.AccountManager) || (x.c.careOfAdminUser == AdminUser.GetAdminUserIdByName(filters.AccountManager)));
                    }
                }
                if (!string.IsNullOrEmpty(filters.Name)) exp = exp.Where((x) => x.c.CompanyName.Contains(filters.Name));
                //if (!string.IsNullOrEmpty(filters.Name)) exp = exp.Where((x) => x.c.FirstName.Contains(filters.Name) || x.c.LastName.Contains(filters.Name) || x.c.EmailAddress.Contains(filters.Name));
            }
            var result = exp.ApplySortAndPage(sortAndPage).Select(x => new Merchant(x.c, x.a)
            {
                DateFirstTransPass = x.maIncludeEmpty.DateFirstTransPass,
                DateLastTransFail = x.maIncludeEmpty.DateLastTransFail,
                DateLastTransPending = x.maIncludeEmpty.DateLastTransPending,
                DateLastTransPass = x.maIncludeEmpty.DateLastTransPass,
                DateLastTransPreAuth = x.maIncludeEmpty.DateLastTransPreAuth,
                DateLastSettlement = x.maIncludeEmpty.DateLastSettlement
            }).ToList();
            var pcount = Transactions.Transaction.GetTransactionCount(Transactions.Transaction.GetTransactionCountGroup.Merchant, result.Select(c => c.ID).Take(2000).ToList());
            foreach (var r in result) r._processStatus = pcount.Where(c => c.Key == r.ID).SingleOrDefault();
            return result;
        }

        public static void UpdateMerchantStatus(int merchantId, byte newStatus)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, Status.Module.SecuredObject, PermissionValue.Edit);

            //merchantId have to be bigger than 0 , newStatus have to be bigger or equal to 0.
            if (merchantId <= 0 || newStatus < 0) return;

            //Declare the IsActive boolean value - will be updated in the Data.LoginAccount table according to the new status of the merchant
            bool accountDisable = (newStatus == (byte)MerchantStatus.Closed || newStatus == (byte)MerchantStatus.Blocked || newStatus == (byte)MerchantStatus.Archived);
            bool IsActive = accountDisable ? false : true;

            //Change the Merchant status in the DB
            DataContext.Reader.ExecuteCommand("UPDATE tblCompany SET ActiveStatus={0} WHERE ID={1}", newStatus, merchantId);
            int? LoginAccountId = GetLoginAccountIdByMerchantId(merchantId);
            if (LoginAccountId.HasValue)
            {
                //Change the IsActive value in the DB
                DataContext.Reader.ExecuteCommand("UPDATE Data.LoginAccount SET IsActive={0} Where LoginAccount_id={1}", IsActive, LoginAccountId);
            }
        }

        public static void UpdateMerchantRollingReserveID(int merchantid, int rollingreserveid)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            var entity = (from merchants in DataContext.Writer.tblCompanies
                          where merchants.ID == merchantid
                          select merchants).Single();
            entity.RRPayID = rollingreserveid;

            DataContext.Writer.tblCompanies.Update(entity, entity.ID > 0);
            DataContext.Writer.SubmitChanges();
        }

        public void SetMerchantGradedFeeCurrency(int? currencyid)
        {
            if (currencyid.HasValue && (!DataContext.Writer.CurrencyLists.Any(x => x.CurrencyID == currencyid))) throw new Exception("currency id not valid");
            _entity.CFF_Currency = currencyid;
            DataContext.Writer.tblCompanies.Update(_entity, _entity.ID != 0);
            DataContext.Writer.SubmitChanges();
        }

        public static decimal GetMerchantTaxRate(int merchantID)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (decimal)(from b in DataContext.Reader.tblBillingCompanies
                             join m in DataContext.Reader.tblCompanies on b.BillingCompanys_id equals m.BillingCompanys_id
                             where m.ID == merchantID
                             select b.VATamount).SingleOrDefault();
        }
        /***************************old settlements code *********************/
        //public static Dictionary<int, Netpay.Bll.OldVO.SetMerchantSettlements> GetCurrencySettings(int merchantId) - moved to SetMerchantSellement

        public class MerchantPayDate
        {
            public int From, To, When;
            public static List<MerchantPayDate> Parse(params string[] p)
            {
                var ret = new List<MerchantPayDate>();
                foreach (var v in p)
                {
                    if (v.Length != 6) continue;
                    ret.Add(new MerchantPayDate()
                    {
                        From = v.Substring(0, 2).ToInt32(0),
                        To = v.Substring(2, 2).ToInt32(0),
                        When = v.Substring(4, 2).ToInt32(0),
                    });
                }
                return ret;
            }
        }

        public static DateTime GetMerchantNextSettlementDate(int merchantID)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            Netpay.Dal.Netpay.tblCompany merchant = (from t in DataContext.Reader.tblCompanies where t.ID == merchantID select t).SingleOrDefault();

            DayOfWeek defaultPayDate = DayOfWeek.Wednesday;
            if (merchant.PayingDaysMarginInitial > 0 || merchant.PayingDaysMargin > 0)
            {
                return DateTime.Now.AddDays(DateTime.Now.DayOfWeek < defaultPayDate ? (int)(defaultPayDate - DateTime.Now.DayOfWeek) : (int)((DayOfWeek.Saturday - DateTime.Now.DayOfWeek) + defaultPayDate));
            }
            else
            {
                DateTime returnDate;
                DateTime today = DateTime.Now;
                DateTime payDate = DateTime.Now;
                int payDate1 = merchant.payingDates1.Length < 6 ? 0 : merchant.payingDates1.Substring(4, 2).ToInt32(0);
                int payDate2 = merchant.payingDates2.Length < 6 ? 0 : merchant.payingDates2.Substring(4, 2).ToInt32(0);
                int payDate3 = merchant.payingDates3.Length < 6 ? 0 : merchant.payingDates3.Substring(4, 2).ToInt32(0);

                if (payDate1 == 0 && payDate2 == 0 && payDate3 == 0) return DateTime.Now;
                while (true)
                {
                    if (payDate1 > 0 && (returnDate = new DateTime(payDate.Year, payDate.Month, payDate1)) > today) break;
                    if (payDate2 > 0 && (returnDate = new DateTime(payDate.Year, payDate.Month, payDate2)) > today) break;
                    if (payDate3 > 0 && (returnDate = new DateTime(payDate.Year, payDate.Month, payDate3)) > today) break;
                    payDate = payDate.AddMonths(1);
                }

                return returnDate;
            }
        }

        public static List<DateTime> GetMerchantNextSettlementDates(int merchantID, DateTime startDate)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            Netpay.Dal.Netpay.tblCompany merchant = (from t in DataContext.Reader.tblCompanies where t.ID == merchantID select t).SingleOrDefault();
            if (merchant == null) return null;
            var ret = new List<DateTime>();
            if (merchant.PayingDaysMargin > 0)
            {
                ret.Add(startDate.AddDays(merchant.PayingDaysMarginInitial));
                for (int i = 1; i <= 10; i++)
                {
                    startDate = startDate.AddDays(merchant.PayingDaysMargin);
                    ret.Add(startDate);
                }
            }
            else
            {
                var payDates = MerchantPayDate.Parse(merchant.payingDates1, merchant.payingDates2, merchant.payingDates3);
                for (var x = 1; x <= 3; x++)
                {
                    foreach (var p in payDates)
                    {
                        var add = new DateTime(startDate.Year, startDate.Month, p.When);
                        if (add >= startDate) ret.Add(add);
                    }
                    startDate = new DateTime(startDate.AddMonths(1).Year, startDate.AddMonths(1).Month, 1);
                    //startDate = new DateTime(startDate.Year, startDate.Month + 1, 1);
                }
            }
            if (ret.Count == 0) ret.Add(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day));
            return ret.Distinct().OrderBy(x => x).ToList();
        }

        private static int GetTransactionSettlementDateInRangeValue(string dateRange, int day)
        {
            int fromDay, toDay;
            if (dateRange.Length < 6) return -1;
            if (!(int.TryParse(dateRange.Substring(0, 2), out fromDay) && int.TryParse(dateRange.Substring(2, 2), out toDay))) return -1;
            if ((fromDay <= day) && (toDay >= day)) return dateRange.Substring(4, 2).ToInt32(-1);
            return -1;
        }

        public static DateTime GetTransactionSettlementDate(int merchantID, DateTime transDate)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            DateTime dateFirstTrans = DateTime.Now;
            Netpay.Dal.Netpay.tblCompany merchant = (from t in DataContext.Reader.tblCompanies where t.ID == merchantID select t).SingleOrDefault();
            if (merchant.PayingDaysMarginInitial > 0) dateFirstTrans = (from a in DataContext.Reader.MerchantActivities where a.Merchant_id == merchantID select a.DateFirstTransPass).SingleOrDefault().GetValueOrDefault(DateTime.Now);
            if ((merchant.PayingDaysMarginInitial > 0) && ((transDate - dateFirstTrans).TotalDays < merchant.PayingDaysMarginInitial))
                return transDate.AddDays(merchant.PayingDaysMarginInitial);
            else if (merchant.PayingDaysMargin > 0)
                return transDate.AddDays(merchant.PayingDaysMargin);
            else
            {
                string[] dateValue = new string[] { merchant.payingDates1, merchant.payingDates2, merchant.payingDates3 };
                for (int i = 0; i < dateValue.Length; i++)
                {
                    int payDay = GetTransactionSettlementDateInRangeValue(dateValue[i], transDate.Day);
                    if (payDay > -1) return (new DateTime(transDate.Year, transDate.Month, payDay)).AddMonths(1);
                }
                return new DateTime(1901, 1, 1);
            }
        }

        public static void AddMerchantToBlackList(int merchantid, string insertedby)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
            {
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, MerchantBlackList.MerchantBlackListSettings.SecuredObject, PermissionValue.Add);
            }

            if (merchantid <= 0) return;
            Bll.Merchants.Merchant Merchant = Netpay.Bll.Merchants.Merchant.Load(merchantid);
            Bll.MerchantBlackList.MerchantBlackListSettings BlackListItem = new Bll.MerchantBlackList.MerchantBlackListSettings();

            BlackListItem.InsertedBy = insertedby; // V
            BlackListItem.CompanyId = Merchant.ID; // V

            BlackListItem.FirstName = Merchant.ContactFirstName; // V
            BlackListItem.LastName = Merchant.ContactLastName; // V
            BlackListItem.Street = null; // V
            BlackListItem.City = null;  // V
            BlackListItem.Phone = Merchant.Phone; // V
            BlackListItem.Cellular = Merchant.ContactMobilePhone; // V
            BlackListItem.IDNumber = Merchant.OwnerSSN; // V
            BlackListItem.CompanyName = Merchant.Name; // V
            BlackListItem.CompanyLegalName = Merchant.LegalName; // V
            BlackListItem.CompanyLegalNumber = Merchant.LegalNumber; // V
            BlackListItem.CompanyStreet = null; //V
            BlackListItem.CompanyCity = null; //V 
            BlackListItem.CompanyPhone = Merchant.Phone; //V
            BlackListItem.CompanyFax = Merchant.Fax; //V
            BlackListItem.Mail = Merchant.EmailAddress; //V
            BlackListItem.URL = Merchant.Url; //V
            BlackListItem.MerchantSupportEmail = Merchant.SupportEmail; // V
            BlackListItem.MerchantSupportPhoneNum = Merchant.SupportPhone; //V
            BlackListItem.IpOnReg = Merchant.IpOnReg; //V
            BlackListItem.PaymentPayeeName = null; //V
            BlackListItem.PaymentBank = null;//V
            BlackListItem.PaymentBranch = null;//V
            BlackListItem.PaymentAccount = null;//V

            BlackListItem.Save();
        }
    }
}
