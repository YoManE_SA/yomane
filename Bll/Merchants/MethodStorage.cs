﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll.Merchants
{
	public class MethodStorage : BaseDataObject, PaymentMethods.IStoredMethod /*, IAddress */
	{
		public const string SecuredObjectName = "MethodStorage";
		public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Merchants.Module.Current, SecuredObjectName); } }

		public class SearchFilters
		{
			public Infrastructure.Range<int?> ID;
			public Infrastructure.Range<DateTime?> InsertDate;
			public CommonTypes.PaymentMethodEnum? PaymentMethod;
			public Infrastructure.Range<DateTime?> PM_ExpirationDate;
			public int? MerchantID;
			public bool? IsActive;
			public string Owner_Name;
			public string PM_First6;
			public string PM_Last4;
			public string Value1;
		}

		private Dal.Netpay.PaymentTokenization _entity;

		public int ID { get { return _entity.PaymentTokenization_id; } set { _entity.PaymentTokenization_id = value; } }
		public string ReferenceCode { get { return _entity.ReferenceCode; } set { _entity.ReferenceCode = value; } }
		public int Merchant_id { get { return _entity.Merchant_id; } set { _entity.Merchant_id = value; } }
		public int? Settlement_id { get { return _entity.Settlement_id; } set { _entity.Settlement_id = value; } }
		public string Comment { get { return _entity.Comment; } set { _entity.Comment = value; } }
		public System.DateTime InsertDate { get { return _entity.InsertDate; } set { _entity.InsertDate = value; } }
		public bool IsActive { get { return _entity.IsDeleted; } set { _entity.IsDeleted = value; } }

		/* PaymentMethods.IStoredMethod */
		System.DateTime? PaymentMethods.IStoredMethod.ExpirationDate { get { return _entity.PM_ExpirationDate; } set { _entity.PM_ExpirationDate = value; } }
		int PaymentMethods.IStoredMethod.PaymentMethodId { get { return _entity.PM_PaymentMethod_id; } set { _entity.PM_PaymentMethod_id = (short)value; } }
		System.Data.Linq.Binary PaymentMethods.IStoredMethod.Value1Encrypted { get { return _entity.PM_Value1Encrypted; } set { _entity.PM_Value1Encrypted = value; } }
		System.Data.Linq.Binary PaymentMethods.IStoredMethod.Value2Encrypted { get { return _entity.PM_Value2Encrypted; } set { _entity.PM_Value2Encrypted = value; } }
		string PaymentMethods.IStoredMethod.Value1First6Data { get { return _entity.PM_Value1First6Text; } set { _entity.PM_Value1First6Text = value; } }
		string PaymentMethods.IStoredMethod.Value1Last4Data { get { return _entity.PM_Value1Last4Text; } set { _entity.PM_Value1Last4Text = value; } }
		byte PaymentMethods.IStoredMethod.EncryptionKey { get { return _entity.PM_EncryptionKey; } set { _entity.PM_EncryptionKey = value; } }
		public string PM_IssuerCountryIsoCode { get { return _entity.PM_IssuerCountryIsoCode; } set { _entity.PM_IssuerCountryIsoCode = value; } }
		public PaymentMethods.MethodData MethodInstance { get; set; }

		public string Owner_FirstName { get { return _entity.CH_FirstName; } set { _entity.CH_FirstName = value; } }
		public string Owner_LastName { get { return _entity.CH_LastName; } set { _entity.CH_LastName = value; } }
		public string Owner_InvoiceName { get { return _entity.CH_InvoiceName; } set { _entity.CH_InvoiceName = value; } }
		public string Owner_PersonalNumber { get { return _entity.CH_PersonalNumber; } set { _entity.CH_PersonalNumber = value; } }
		public string Owner_PhoneNumber { get { return _entity.CH_PhoneNumber; } set { _entity.CH_PhoneNumber = value; } }
		public string Owner_EmailAddress { get { return _entity.CH_EmailAddress; } set { _entity.CH_EmailAddress = value; } }
		public System.DateTime? Owner_DateOfBirth { get { return _entity.CH_DateOfBirth; } set { _entity.CH_DateOfBirth = value; } }
		public string Owner_FullName
		{
			get { return PersonalInfo.MakeFullName(Owner_FirstName, Owner_LastName); }
			set { PersonalInfo.SplitFullName(value, out _entity.CH_FirstName, out _entity.CH_LastName); }
		}

		public string BA_Street1 { get { return _entity.BA_Street1; } set { _entity.BA_Street1 = value; } }
		public string BA_Street2 { get { return _entity.BA_Street2; } set { _entity.BA_Street2 = value; } }
		public string BA_City { get { return _entity.BA_City; } set { _entity.BA_City = value; } }
		public string BA_PostalCode { get { return _entity.BA_PostalCode; } set { _entity.BA_PostalCode = value; } }
		public string BA_StateISOCode { get { return _entity.BA_StateISOCode; } set { _entity.BA_StateISOCode = value; } }
		public string BA_CountryISOCode { get { return _entity.BA_CountryISOCode; } set { _entity.BA_CountryISOCode = value; } }

		private MethodStorage(Netpay.Dal.Netpay.PaymentTokenization entity)
			
		{ 
			_entity = entity;
			MethodInstance = PaymentMethods.MethodData.Create(this);
		}

		public MethodStorage(int merchantID)
			
		{
			_entity = new Dal.Netpay.PaymentTokenization();
			_entity.Merchant_id = merchantID;
			MethodInstance = PaymentMethods.MethodData.Create(this);
		}

		public static MethodStorage Load(int merchantID, int id)
		{
			//context.IsUserOfType(new UserType[] { UserType.MerchantPrimary, UserType.NetpayAdmin, UserType.NetpayUser });
			var entity = (from pt in DataContext.Reader.PaymentTokenizations where pt.Merchant_id == merchantID && pt.PaymentTokenization_id == id select pt).SingleOrDefault();
			if (entity == null) return null;
			return new MethodStorage(entity);
		}

		public void Save()
		{
			DataContext.Writer.PaymentTokenizations.Update(_entity, (_entity.PaymentTokenization_id != 0));
			DataContext.Writer.SubmitChanges();
		}

		public static void MarkIsActive(List<int> Ids, bool value)
		{
			DataContext.Writer.ExecuteCommand("Update Data.PaymentTokenization Set UsDeleted={0} Where PaymentTokenization_id IN({1})", value, Ids);
		}

		public static List<MethodStorage> Search(SearchFilters filters, ISortAndPage sortAndPage)
		{
			var exp = (from t in DataContext.Reader.PaymentTokenizations select t);
			if (filters != null) 
			{
				if (filters.ID.From.HasValue) exp = exp.Where(t => t.PaymentTokenization_id >= filters.ID.From.Value);
				if (filters.ID.To.HasValue) exp = exp.Where(t => t.PaymentTokenization_id <= filters.ID.To.Value);
				if (filters.InsertDate.From.HasValue) exp = exp.Where(t => t.InsertDate >= filters.InsertDate.From.Value);
				if (filters.InsertDate.To.HasValue) exp = exp.Where(t => t.InsertDate <= filters.InsertDate.To.Value);
				if (filters.MerchantID.HasValue) exp = exp.Where(t => t.Merchant_id == filters.MerchantID.Value);
				if (filters.PaymentMethod.HasValue) exp = exp.Where(t => t.PM_PaymentMethod_id == (short)filters.PaymentMethod);
				if (filters.PM_First6 != null) exp = exp.Where(t => t.PM_Value1First6Text == filters.PM_First6);
				if (filters.PM_Last4 != null) exp = exp.Where(t => t.PM_Value1Last4Text == filters.PM_Last4);
				if (filters.PM_ExpirationDate.From.HasValue) exp = exp.Where(t => t.PM_ExpirationDate >= filters.PM_ExpirationDate.From.Value);
				if (filters.PM_ExpirationDate.To.HasValue) exp = exp.Where(t => t.PM_ExpirationDate <= filters.PM_ExpirationDate.To.Value);
				if (filters.Value1 != null) exp = exp.Where(t => t.PM_Value1Encrypted == Infrastructure.Security.Encryption.Encrypt(filters.Value1));
				//if (filters.cardHolderName != null)
				//	expression = expression.Where(ccs => ccs.CHFullName.StartsWith(filters.cardHolderName.Trim()));
			}
			return exp.ApplySortAndPage(sortAndPage).Select(t => new MethodStorage(t)).ToList();
		}
	}
}
