﻿using Netpay.Dal.Netpay;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.Merchants
{
    public class NoticatonUrlSettings : BaseDataObject
    {
        public const string SecuredObjectName = "NoticatonUrlSettings";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Merchants.Module.Current, SecuredObjectName); } }

        private Netpay.Dal.Netpay.tblCompanyChargeAdmin _entity;
        public bool IsRecurringReply { get { return _entity.isRecurringReply; } set { _entity.isRecurringReply = value; } }
        public string RecurringReplyURL { get { return _entity.recurringReplyUrl; } set { _entity.recurringReplyUrl = value; } }
        public bool IsRecurringModifyReply { get { return _entity.isRecurringModifyReply; } set { _entity.isRecurringModifyReply = value; } }
        public string RecurringModifyReplyURL { get { return _entity.recurringModifyReplyUrl; } set { _entity.recurringModifyReplyUrl = value; } }
        public bool IsPendingReply { get { return _entity.isPendingReply; } set { _entity.isPendingReply = value; } }
        public string PendingReplyURL { get { return _entity.PendingReplyUrl; } set { _entity.PendingReplyUrl = value; } }
        public bool IsWalletReply { get { return _entity.IsWalletReply; } set { _entity.IsWalletReply = value; } }
        public string WalletReplyURL { get { return _entity.WalletReplyURL; } set { _entity.WalletReplyURL = value; } }
        public bool UseHasKeyInWalletReply { get { return _entity.UseHasKeyInWalletReply; } set { _entity.UseHasKeyInWalletReply = value; } }
        public string NotifyRefundRequestApprovedUrl { get { return _entity.NotifyRefundRequestApprovedUrl; } set { _entity.NotifyRefundRequestApprovedUrl = value; } }
        public bool IsNotifyRefundRequestApproved { get { return _entity.isNotifyRefundRequestApproved; } set { _entity.isNotifyRefundRequestApproved = value; } }
        public string NotifyProcessURL { get { return _entity.NotifyProcessURL; } set { _entity.NotifyProcessURL = value; } }

        private NoticatonUrlSettings(tblCompanyChargeAdmin entity)
        {
            _entity = entity;
        }

        private NoticatonUrlSettings(int merchantId)
        {
            _entity = new tblCompanyChargeAdmin();
            _entity.company_id = merchantId;
        }

        public static NoticatonUrlSettings Load(int merchantId)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);
            if (merchantId == 0)
                throw new ArgumentException("Merchant id not set");

            var entity = (from cca in DataContext.Reader.tblCompanyChargeAdmins where cca.company_id == merchantId select cca).SingleOrDefault();
            if (entity == null)
            {
                NoticatonUrlSettings settings = new NoticatonUrlSettings(merchantId);
                settings.IsNotifyRefundRequestApproved = false;
                settings.IsPendingReply = false;
                settings.IsRecurringModifyReply = false;
                settings.IsRecurringReply = false;
                settings.IsWalletReply = false;
                settings.UseHasKeyInWalletReply = true;
                settings.NotifyRefundRequestApprovedUrl = string.Empty;
                settings.PendingReplyURL = string.Empty;
                settings.RecurringModifyReplyURL = string.Empty;
                settings.RecurringReplyURL = string.Empty;
                settings.WalletReplyURL = string.Empty;

                DataContext.Writer.tblCompanyChargeAdmins.Update(settings._entity, false);
                DataContext.Writer.SubmitChanges();
                return settings;
            }
            else
                return new NoticatonUrlSettings(entity);
        }

        public void Save()
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Edit);
            DataContext.Writer.tblCompanyChargeAdmins.Update(_entity, true);
            DataContext.Writer.SubmitChanges();
        }
    }
}
