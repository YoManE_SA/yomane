﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Merchants
{
	public class Department : BaseDataObject
	{
        public const string SecuredObjectName = "Manage";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Bll.GlobalDataManager.MerchantDepartments.Module.Current, SecuredObjectName); } }


        private Dal.Netpay.MerchantDepartment _entity;
		public int ID { get { return (int)_entity.MerchantDepartment_id; } set { _entity.MerchantDepartment_id = (byte)value; } }
		public string Name { get { return _entity.Name; } set { _entity.Name = value; } }

		private Department(Dal.Netpay.MerchantDepartment entity)
		{
            _entity = entity;
		}

        public Department()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            _entity = new Dal.Netpay.MerchantDepartment();
        }

        public static List<Department> Cache
		{
			get
			{
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return Domain.Current.GetCachData("Merchant.Department", () => { 
                    return new Domain.CachData((from c in DataContext.Reader.MerchantDepartments select new Department(c)).ToList(), DateTime.Now.AddHours(6));
                }) as List<Department>;
			}
		}

        public static Department Get(int id)
        {
            if (id <= 0) return null;

            return Cache.Where(x => x.ID == id).SingleOrDefault();
        }

        public static byte GetNewItemId
        {
            get
            {
                return (byte)(DataContext.Reader.MerchantDepartments.Max(x => x.MerchantDepartment_id) + 1);
            }
        }

        public void Save(bool isexist)
        {
            if (!isexist)
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

                DataContext.Writer.ExecuteCommand("Insert into dbo.MerchantDepartment values({0});", _entity.Name);
            }
            else
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

                DataContext.Writer.MerchantDepartments.Update(_entity, (isexist));
                DataContext.Writer.SubmitChanges();
            }
        }

        public void Delete()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            if (_entity.MerchantDepartment_id == 0) return;

            DataContext.Writer.MerchantDepartments.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }
	}
}
