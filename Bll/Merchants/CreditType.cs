﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web;
using Netpay.Infrastructure;

namespace Netpay.Bll.Merchants
{
    public class CreditType : BaseDataObject
    {
        private Dal.Netpay.TransCreditType _entity;
        
        public int ID { get { return _entity.TransCreditType_id; } }

        public string NameHebrew { get { return _entity.NameHeb; } }

        public string NameEng { get { return _entity.NameEng; } }

        public bool IsShow { get { return _entity.IsShow.HasValue ? _entity.IsShow.Value : false; } }

        private CreditType(Dal.Netpay.TransCreditType entity)
        {
            _entity = entity;
        }

        public static List<CreditType> Cache
        {
            get
            {
                return Domain.Current.GetCachData("CreditType", () =>
                {
                    return new Domain.CachData((from ct in DataContext.Reader.TransCreditTypes select new CreditType(ct)).ToList(), DateTime.Now.AddHours(6));
                }) as List<CreditType>;
            }

        }

        public static string GetCreditName(byte transcredittype_id , CommonTypes.Language language)
        {
            if (language==CommonTypes.Language.Hebrew)
                return Cache.Where(ct => ct.ID == transcredittype_id).SingleOrDefault().NameHebrew; 
            else
                return Cache.Where(ct => ct.ID == transcredittype_id).SingleOrDefault().NameEng;
        }
    }
}
