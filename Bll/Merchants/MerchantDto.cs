﻿using System;

namespace Netpay.Bll.Merchants
{
    [Serializable]
    public class MerchantDto
    {
        public string CustomerNumber { get; set; }
        public int MerchantId { get; set; }
        public string CompanyName { get; set; }
        public int AccountId { get; set; }
        public int AccountTypeId { get; set; }
        public string CurrencyISOCode { get; set; }
    }
}