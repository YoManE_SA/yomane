using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Merchants
{
    public class Registration : BaseDataObject
    {
        private const string FileDataPath = @"Merchant Registrations\";
        public const string SecuredObjectName = "Registration";
        public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Merchants.Module.Current, SecuredObjectName); } }

        public class SearchFilters
        {
            public bool? RegisteredLocally;
            public bool? RegisteredAtCardConnect;
            public bool? ShowAll;
        }

        public Guid ID { get; set; }
        public int? MerchantID { get; set; }
        public string DbaName { get; set; }
        public string LegalBusinessName { get; set; }
        public string LegalBusinessNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zipcode { get; set; }
        public DateTime? OwnerDob { get; set; }
        public string OwnerSsn { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Url { get; set; }
        public string PhisicalAddress { get; set; }
        public string PhisicalCity { get; set; }
        public string PhisicalState { get; set; }
        public string PhisicalZip { get; set; }
        public string StateOfIncorporation { get; set; }
        public int? TypeOfBusiness { get; set; }
        public DateTime? BusinessStartDate { get; set; }
        public int? Industry { get; set; }
        public string BusinessDescription { get; set; }
        public decimal? AnticipatedMonthlyVolume { get; set; }
        public decimal? AnticipatedAverageTransactionAmount { get; set; }
        public decimal? AnticipatedLargestTransactionAmount { get; set; }
        public string CanceledCheckImageFileName { get; set; }
        public string CanceledCheckImageMimeType { get; set; }
        public string CanceledCheckImageContent { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankRoutingNumber { get; set; }
        public int PercentDelivery0to7 { get; set; }
        public int PercentDelivery8to14 { get; set; }
        public int PercentDelivery15to30 { get; set; }
        public int PercentDeliveryOver30 { get; set; }

        public Netpay.Infrastructure.PropertyList IntegrationData { get; set; }
        public Registration()
        {
            IntegrationData = new PropertyList();
        }

        private void EncryptBankInfo()
        {
            try
            {
                BankAccountNumber = Netpay.Infrastructure.Security.Encryption.Encrypt(BankAccountNumber).ToBase64();
                BankRoutingNumber = Netpay.Infrastructure.Security.Encryption.Encrypt(BankRoutingNumber).ToBase64();
            }
            catch (Exception e)
            {
                BankAccountNumber = ""; BankRoutingNumber = "";
                Infrastructure.Logger.Log(e);
            }
        }

        private void DecryptBankInfo()
        {
            try
            {
                BankAccountNumber = Netpay.Infrastructure.Security.Encryption.Decrypt(BankAccountNumber.FromBase64());
                BankRoutingNumber = Netpay.Infrastructure.Security.Encryption.Decrypt(BankRoutingNumber.FromBase64());
            }
            catch (Exception e)
            {
                BankAccountNumber = ""; BankRoutingNumber = "";
                Infrastructure.Logger.Log(e);
            }
        }

        public static List<Registration> Search(SearchFilters filter)
        {
            //User user = SecurityManager.GetInternalUser(new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser });
            string path = MapPath(null);
            string[] files = new DirectoryInfo(path).GetFiles().OrderByDescending(f => f.CreationTime).Select(f => f.FullName).ToArray();

            List<Registration> registrations = new List<Registration>();
            foreach (string file in files)
            {
            try
              {
                string content = File.ReadAllText(file);
                content = content.Replace("MerchantRegistrationVO", "Registration");
                Registration registration = content.FromXml<Registration>(UnicodeEncoding.UTF8);
                registration.DecryptBankInfo();
                registrations.Add(registration);
              }
              catch
              {
				Logger.Log(LogSeverity.Error, LogTag.AdminCash, $"Error loading registration file: {file}", ex.ToString());
              }
            }
            if (filter != null)
            {
                if (!filter.ShowAll.Value)
                {

                    if (filter.RegisteredLocally != null)
                    {
                        if (filter.RegisteredLocally.Value)
                        {
                            registrations = registrations.Where(r => r.MerchantID != null).ToList();
                        }
                        else
                        {
                            registrations = registrations.Where(r => r.MerchantID == null).ToList();
                        }
                    }

                    if (filter.RegisteredAtCardConnect != null)
                    {
                        if (filter.RegisteredAtCardConnect.Value)
                        {
                            registrations = registrations.Where(r => r.IntegrationData.ContainsKey("CardConnectID") == filter.RegisteredAtCardConnect).ToList();
                        }
                    }
                }
            }
            return registrations;
        }

        public static string MapPath(Guid? registrationID = null)
        {
            var ret = Domain.Current.MapPrivateDataPath(FileDataPath);
            if (!System.IO.Directory.Exists(ret)) System.IO.Directory.CreateDirectory(ret);
            if (registrationID != null) ret = System.IO.Path.Combine(ret, registrationID + ".xml");
            return ret;
        }


        public static Registration Load(Guid registrationID)
        {
            //User user = SecurityManager.GetInternalUser(new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser });
            string path = MapPath(registrationID);
            if (!System.IO.File.Exists(path)) return null;
            string content = File.ReadAllText(path);
            content = content.Replace("MerchantRegistrationVO", "Registration");
            Registration registration = content.FromXml<Registration>(UnicodeEncoding.UTF8);
            //registration.DecryptBankInfo();
            return registration;
        }

        public void Save()
        {
            if (ID == Guid.Empty) ID = Guid.NewGuid();
            string path = MapPath(ID);
            //EncryptBankInfo();
            string content = this.ToXml();
            File.WriteAllText(path, content);
        }

        public void Delete()
        {
            //User user = SecurityManager.GetInternalUser(new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser });
            string path = MapPath(ID);
            if (System.IO.File.Exists(path)) File.Delete(path);
        }

        public bool CreateMerchantLocally()
        {
            //User user = SecurityManager.GetInternalUser(new UserType[] { UserType.NetpayAdmin, UserType.NetpayUser });
            string path = MapPath(ID);
            string content = File.ReadAllText(path);
            //DecryptBankInfo();

            // insert merchant to netpay  
            NetpayDataContext dc = new NetpayDataContext(Domain.Sql1ConnectionString);
            string sql = "SET NOCOUNT ON;INSERT INTO tblCompany (CompanyName, ActiveStatus) VALUES ('New Company', " + (int)MerchantStatus.New + ");SELECT SCOPE_IDENTITY() AS NewCompanyID;SET NOCOUNT OFF";
            IEnumerable<decimal> result = dc.ExecuteQuery<decimal>(sql);
            int merchantId = (int)result.First();

            var merchant = Merchant.Load(merchantId);
            if (merchant == null) merchant = new Merchant();
            merchant.ActiveStatus = MerchantStatus.New;
            //merchant.InsertDate = DateTime.Now;
            merchant.Name = DbaName;
            merchant.LegalName = LegalBusinessName;
            merchant.LegalNumber = LegalBusinessNumber;
            merchant.HashKey = Netpay.Infrastructure.Security.Encryption.GetHashKey(10);
            merchant.Industry = Industry.HasValue ? Industry.Value : 0;
            //merchant.merchantOpenningDate = DateTime.Now;
            //merchant.merchantClosingDate = DateTime.Now;
            //merchant.CFF_ResetDate = DateTime.Now;
            //merchant.lastInsertDate = DateTime.Now;
            //merchant.PasswordUpdate = DateTime.Now;
            merchant.EmailAddress = Email;
            //merchant.ContactEmail = Email;
            //merchant.UserName = merchantData.UserName;
            //merchant.CompanyCountry = merchantData.Country;
            //merchant.Country = merchantData.ContactCountry;
            merchant.OwnerSSN = OwnerSsn;

            if (merchant.BusinessAddress == null) merchant.BusinessAddress = new Accounts.AccountAddress();
            merchant.BusinessAddress.City = PhisicalCity;
            merchant.BusinessAddress.AddressLine1 = PhisicalAddress;
            merchant.BusinessAddress.PostalCode = PhisicalZip;
            merchant.BusinessAddress.StateISOCode = PhisicalState;
            merchant.BusinessAddress.CountryISOCode = Bll.Country.Get(228).IsoCode2;

            if (merchant.PersonalAddress == null) merchant.PersonalAddress = new Accounts.AccountAddress();
            merchant.PersonalAddress.City = City;
            merchant.PersonalAddress.AddressLine1 = Address;
            merchant.PersonalAddress.PostalCode = Zipcode;
            merchant.PersonalAddress.StateISOCode = State;
            merchant.PersonalAddress.CountryISOCode = Bll.Country.Get(228).IsoCode2;

            merchant.ContactFirstName = FirstName;
            merchant.ContactLastName = LastName;
            merchant.ContactPhone = Phone;
            merchant.ContactMobilePhone = Phone;
            merchant.Comment = "Created from registration";


            merchant.SupportEmail = Email;
            merchant.SupportPhone = Phone;
            merchant.Url = Url;
            merchant.Phone = Phone;
            merchant.Fax = Fax;
            var processSettings = merchant.ProcessSettings;
            processSettings.MerchantNotifyEmails = Email;

            processSettings.IsSystemPayCVV2 = false;
            processSettings.IsSystemPayPhoneNumber = true;
            processSettings.IsSystemPayPersonalNumber = false;
            processSettings.IsSystemPayEmail = true;
            processSettings.IsRefund = false;
            //processSettings.IsConfirmation = false;
            //processSettings.Blocked = true;
            //processSettings.Closed = false;
            processSettings.IsAllowBatchFiles = false;
            processSettings.IsAllowMakePayments = false;
            processSettings.IsAllowRemotePull = false;
            //processSettings.IsAllowSilentPostCcDetails = false;
            processSettings.IsAskRefund = false;
            processSettings.IsAskRefundRemote = false;
            processSettings.IsCcStorage = false;
            processSettings.IsCcStorageCharge = false;

            //processSettings.IsAllowRecurring = false;
            //processSettings.IsAllowRecurringFromTransPass = false;
            //processSettings.IsBillingAddressMust = true;
            //processSettings.IsAllow3DTrans = false;

            //processSettings.isNetpayTerminal = false;
            //processSettings.IsPublicPay = true;
            bool IsDuplicateExist = Bll.Merchants.Merchant.IsMerchantDuplicatesExist(merchant);
            if (IsDuplicateExist)
            {
                return false;
            }
            else
            {
                merchant.Save();

                // update reg file
                MerchantID = merchant.ID;
                Save();

                //Create BankAccount to the current merchant
                //The creation is made only after saving the merchant because the AccountID value is needed.
                if (!string.IsNullOrEmpty(BankAccountNumber) && !string.IsNullOrEmpty(BankRoutingNumber))
                {
                    var merchantBankAccount = new Netpay.Bll.Accounts.BankAccount();
                    merchantBankAccount.AccountName = LegalBusinessName;
                    merchantBankAccount.AccountNumber = BankAccountNumber;
                    merchantBankAccount.ABA = BankRoutingNumber;
                    merchantBankAccount.IsDefault = true;
                    merchantBankAccount.Account_id = merchant.AccountID;
                    merchantBankAccount.Save();
                }

                // save canceled check doc
                var file = new Accounts.File(merchant.AccountID);
                file.FileTitle = "Registration canceled check";
                byte[] cancelledCheckImageData = CanceledCheckImageContent.FromBase64();
                file.Save(CanceledCheckImageFileName, cancelledCheckImageData);

                //string cancelledCheckImagePath = FilesManager.GetPrivateAccessPath(user.Domain.Host, UserType.MerchantPrimary, merchantEntity.ID, FilesManager.SubFolders.RegistrationDocs) + reg.CanceledCheckImageFileName;
                //byte[] cancelledCheckImageData = reg.CanceledCheckImageContent.FromBase64();
                //File.WriteAllBytes(cancelledCheckImagePath, cancelledCheckImageData);

                return true;
            }
        }

        public void SendRegistrationEmail()
        {
            var d = Netpay.Infrastructure.Domain.Get(Domain.Host);
            System.Globalization.CultureInfo cultureInfo = System.Globalization.CultureInfo.GetCultureInfo("en-us");
            string subject, body = Infrastructure.Email.FormatMesage.GetMessageText(System.IO.Path.Combine(d.EmailTemplatePath, "Merchant_Register.htm"), out subject);
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();

            var dataObj = new { Domain = d, Registration = this };
            message.IsBodyHtml = true;
            message.Subject = Infrastructure.Email.FormatMesage.FormatMessage(subject, dataObj, null, cultureInfo);
            message.Body = Infrastructure.Email.FormatMesage.FormatMessage(body, dataObj, null, cultureInfo);
            message.From = new System.Net.Mail.MailAddress(d.MailAddressFrom, d.MailNameFrom);
            message.To.Add(new System.Net.Mail.MailAddress(Email, string.Format("{0} {1}", FirstName, LastName)));
            Infrastructure.Email.SmtpClient.Send(message);
        }
    }
}
