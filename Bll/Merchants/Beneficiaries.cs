﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Merchants
{
	public class Beneficiary : BaseDataObject
	{
		private Netpay.Dal.Netpay.tblCompanyMakePaymentsProfile _entity;

		public int ID { get { return _entity.CompanyMakePaymentsProfiles_id; } }
		public int? MerchantID { get { return _entity.company_id; }}
		public byte ProfileType { get { return _entity.ProfileType; } set { _entity.ProfileType = value; } }
		public MerchantBeneficiaryProfileType ProfileTypeEnum { get { return (MerchantBeneficiaryProfileType)_entity.ProfileType; } set { _entity.ProfileType = (byte)value; } }
		public string BasicInfoCostumerNumber { get { return _entity.basicInfo_costumerNumber; } set { _entity.basicInfo_costumerNumber = value; } }
		public string BasicInfoCostumerName { get { return _entity.basicInfo_costumerName; } set { _entity.basicInfo_costumerName = value; } }
		public string BasicInfoContactPersonName { get { return _entity.basicInfo_contactPersonName; } set { _entity.basicInfo_contactPersonName = value; } }
		public string BasicInfoPhoneNumber { get { return _entity.basicInfo_phoneNumber; } set { _entity.basicInfo_phoneNumber = value; } }
		public string BasicInfoFaxNumber { get { return _entity.basicInfo_faxNumber; } set { _entity.basicInfo_faxNumber = value; } }
		public string BasicInfoEmail { get { return _entity.basicInfo_email; } set { _entity.basicInfo_email = value; } }
		public string BasicInfoAddress { get { return _entity.basicInfo_address; } set { _entity.basicInfo_address = value; } }
		public string BasicInfoComment { get { return _entity.basicInfo_comment; } set { _entity.basicInfo_comment = value; } }
		public string BankIsraelInfoPayeeName { get { return _entity.bankIsraelInfo_PayeeName; } set { _entity.bankIsraelInfo_PayeeName = value; } }
		public string BankIsraelInfoCompanyLegalNumber { get { return _entity.bankIsraelInfo_CompanyLegalNumber; } set { _entity.bankIsraelInfo_CompanyLegalNumber = value; } }
		public string BankIsraelInfoPersonalIDNumber { get { return _entity.bankIsraelInfo_personalIdNumber; } set { _entity.bankIsraelInfo_personalIdNumber = value; } }
		public string BankIsraelInfoBankBranch { get { return _entity.bankIsraelInfo_bankBranch; } set { _entity.bankIsraelInfo_bankBranch = value; } }
		public string BankIsraelInfoAccountNumber { get { return _entity.bankIsraelInfo_AccountNumber; } set { _entity.bankIsraelInfo_AccountNumber = value; } }
		public string BankIsraelInfoPaymentMethod { get { return _entity.bankIsraelInfo_PaymentMethod; } set { _entity.bankIsraelInfo_PaymentMethod = value; } }
		public string BankIsraelInfoBankCode { get { return _entity.bankIsraelInfo_BankCode; } set { _entity.bankIsraelInfo_BankCode = value; } }
		public string BankAbroadAccountName { get { return _entity.bankAbroadAccountName; } set { _entity.bankAbroadAccountName = value; } }
		public string BankAbroadAccountNumber { get { return _entity.bankAbroadAccountNumber; } set { _entity.bankAbroadAccountNumber = value; } }
		public string BankAbroadBankName { get { return _entity.bankAbroadBankName; } set { _entity.bankAbroadBankName = value; } }
		public string BankAbroadBankAddress { get { return _entity.bankAbroadBankAddress; } set { _entity.bankAbroadBankAddress = value; } }
		public string BankAbroadSwiftNumber { get { return _entity.bankAbroadSwiftNumber; } set { _entity.bankAbroadSwiftNumber = value; } }
		public string BankAbroadIBAN { get { return _entity.bankAbroadIBAN; } set { _entity.bankAbroadIBAN = value; } }
		public string BankAbroadABA { get { return _entity.bankAbroadABA; } set { _entity.bankAbroadABA = value; } }
		public string BankAbroadSortCode { get { return _entity.bankAbroadSortCode; } set { _entity.bankAbroadSortCode = value; } }
		public string BankAbroadAccountName2 { get { return _entity.bankAbroadAccountName2; } set { _entity.bankAbroadAccountName2 = value; } }
		public string BankAbroadAccountNumber2 { get { return _entity.bankAbroadAccountNumber2; } set { _entity.bankAbroadAccountNumber2 = value; } }
		public string BankAbroadBankName2 { get { return _entity.bankAbroadBankName2; } set { _entity.bankAbroadBankName2 = value; } }
		public string BankAbroadBankAddress2 { get { return _entity.bankAbroadBankAddress2; } set { _entity.bankAbroadBankAddress2 = value; } }
		public string BankAbroadSwiftNumber2 { get { return _entity.bankAbroadSwiftNumber2; } set { _entity.bankAbroadSwiftNumber2 = value; } }
		public string BankAbroadIBAN2 { get { return _entity.bankAbroadIBAN2; } set { _entity.bankAbroadIBAN2 = value; } }
		public string BankAbroadABA2 { get { return _entity.bankAbroadABA2; } set { _entity.bankAbroadABA2 = value; } }
		public string BankAbroadSortCode2 { get { return _entity.bankAbroadSortCode2; } set { _entity.bankAbroadSortCode2 = value; } }
		public string BankAbroadBankAddressSecond { get { return _entity.bankAbroadBankAddressSecond; } set { _entity.bankAbroadBankAddressSecond = value; } }
		public string BankAbroadBankAddressCity { get { return _entity.bankAbroadBankAddressCity; } set { _entity.bankAbroadBankAddressCity = value; } }
		public string BankAbroadBankAddressState { get { return _entity.bankAbroadBankAddressState; } set { _entity.bankAbroadBankAddressState = value; } }
		public string BankAbroadBankAddressZip { get { return _entity.bankAbroadBankAddressZip; } set { _entity.bankAbroadBankAddressZip = value; } }
		public int BankAbroadBankAddressCountry { get { return _entity.bankAbroadBankAddressCountry; } set { _entity.bankAbroadBankAddressCountry = value; } }
		public string BankAbroadBankAddressSecond2 { get { return _entity.bankAbroadBankAddressSecond2; } set { _entity.bankAbroadBankAddressSecond2 = value; } }
		public string BankAbroadBankAddressCity2 { get { return _entity.bankAbroadBankAddressCity2; } set { _entity.bankAbroadBankAddressCity2 = value; } }
		public string BankAbroadBankAddressState2 { get { return _entity.bankAbroadBankAddressState2; } set { _entity.bankAbroadBankAddressState2 = value; } }
		public string BankAbroadBankAddressZip2 { get { return _entity.bankAbroadBankAddressZip2; } set { _entity.bankAbroadBankAddressZip2 = value; } }
		public int BankAbroadBankAddressCountry2 { get { return _entity.bankAbroadBankAddressCountry2; } set { _entity.bankAbroadBankAddressCountry2 = value; } }
		public string BankAbroadSepaBic { get { return _entity.bankAbroadSepaBic; } set { _entity.bankAbroadSepaBic = value; } }
		public string BankAbroadSepaBic2 { get { return _entity.bankAbroadSepaBic2; } set { _entity.bankAbroadSepaBic2 = value; } }
		public bool IsSystem { get { return _entity.isSystem; } set { _entity.isSystem = value; } }

		private Beneficiary(Netpay.Dal.Netpay.tblCompanyMakePaymentsProfile entity)
		{ 
			_entity = entity;
		}

		public Beneficiary(int merchantId)
		{
			_entity = new Dal.Netpay.tblCompanyMakePaymentsProfile();
			_entity.company_id = merchantId;
		}
		
		public static List<Beneficiary> GetBeneficiaries()
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Merchant });
			var beneficiaries = (from b in DataContext.Reader.tblCompanyMakePaymentsProfiles where b.company_id == Merchants.Merchant.Current.ID orderby b.isSystem descending, b.basicInfo_costumerName select new Beneficiary(b)).ToList();
			return beneficiaries;
		}

		public void Delete()
		{
			if (_entity.CompanyMakePaymentsProfiles_id == 0) return;
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Merchant });
			Accounts.AccountFilter.Current.Validate(Accounts.AccountType.Merchant, _entity.company_id.GetValueOrDefault());
			DataContext.Writer.tblCompanyMakePaymentsProfiles.Delete(_entity);
			DataContext.Writer.SubmitChanges();
		}

		public void Save()
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Merchant, UserRole.Admin });
			Accounts.AccountFilter.Current.Validate(Accounts.AccountType.Merchant, _entity.company_id.GetValueOrDefault());

            var notnullvarcharFields = "basicInfo_costumerNumber,basicInfo_costumerName,basicInfo_contactPersonName,basicInfo_phoneNumber,basicInfo_faxNumber," +
                "basicInfo_email,basicInfo_address,basicInfo_comment,bankIsraelInfo_PayeeName,bankIsraelInfo_CompanyLegalNumber,bankIsraelInfo_personalIdNumber," +
                "bankIsraelInfo_bankBranch,bankIsraelInfo_AccountNumber,bankIsraelInfo_PaymentMethod,bankAbroadAccountName,bankAbroadAccountNumber,bankAbroadBankName," +
                "bankAbroadBankAddress,bankAbroadSwiftNumber,bankAbroadIBAN,bankAbroadABA,bankAbroadSortCode,bankAbroadAccountName2,bankAbroadAccountNumber2," +
                "bankAbroadBankName2,bankAbroadBankAddress2,bankAbroadSwiftNumber2,bankAbroadIBAN2,bankAbroadABA2,bankAbroadSortCode2,bankAbroadBankAddressSecond," +
                "bankAbroadBankAddressCity,bankAbroadBankAddressState,bankAbroadBankAddressZip,bankAbroadBankAddressSecond2,bankAbroadBankAddressCity2," +
                "bankAbroadBankAddressState2,bankAbroadBankAddressZip2,bankAbroadSepaBic,bankAbroadSepaBic2,bankIsraelInfo_BankCode";
            foreach (var n in notnullvarcharFields.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)) {
                var x = _entity.GetType().GetProperty(n).GetValue(_entity, null);
                if (x == null) _entity.GetType().GetProperty(n).SetValue(_entity, string.Empty, null);
            }

            DataContext.Writer.tblCompanyMakePaymentsProfiles.Update(_entity, (_entity.CompanyMakePaymentsProfiles_id != 0));
			DataContext.Writer.SubmitChanges();
		}
		
		public static Beneficiary GetBeneficiary(int benficiaryID)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Merchant });
			Netpay.Dal.Netpay.tblCompanyMakePaymentsProfile beneficiary = (from b in DataContext.Reader.tblCompanyMakePaymentsProfiles where b.company_id == Merchants.Merchant.Current.ID && b.CompanyMakePaymentsProfiles_id == benficiaryID select b).SingleOrDefault();
			if (beneficiary == null) return null;
			return new Beneficiary(beneficiary);
		}


	}
}
