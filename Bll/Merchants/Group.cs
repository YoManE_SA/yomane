﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Merchants
{
	public class Group : BaseDataObject
	{
		private Dal.Netpay.tblMerchantGroup _entity;
		public int ID { get { return _entity.ID; } }
		public string Name { get { return _entity.mg_Name; } set { _entity.mg_Name = value; } }

		private Group(Dal.Netpay.tblMerchantGroup entity)
		{
			_entity = entity;
		}

        public Group() { _entity = new Dal.Netpay.tblMerchantGroup(); }

        public void Delete()
        {
            if (_entity.ID == 0) return;
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin });
            DataContext.Writer.tblMerchantGroups.Delete(_entity);
            DataContext.Writer.SubmitChanges();
            ClearCache();
        }

        public void Save()
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin });
            DataContext.Writer.tblMerchantGroups.Update(_entity, (_entity.ID != 0));
            DataContext.Writer.SubmitChanges();
            ClearCache();
        }

        public override string ToString() { return Name; }
		public static List<Group> Cache
		{
			get
			{
                return Domain.Current.GetCachData("Merchant.Group", () => { 
                    return new Domain.CachData((from c in DataContext.Reader.tblMerchantGroups select new Group(c)).ToList(), DateTime.Now.AddHours(3));
                }) as List<Group>;
			}
		}

        public static void ClearCache()
        {
            Domain.Current.RemoveCachData("Merchant.Group");
        }

        public static Group Get(int id)
		{
			return Cache.Where(c => c.ID == id).SingleOrDefault();
		}

	}
}
