﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Bll.Accounts;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Merchants
{
	public class RollingReserve : BaseDataObject
	{
		public const string SecuredObjectName = "Manage";
		public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(RollingReserves.Module.Current, SecuredObjectName); } }

		protected Netpay.Dal.Netpay.SetMerchantRollingReserve _entity;
		public decimal SecurityDeposit { get { return (decimal)_entity.ReservePercentage.GetValueOrDefault(); } set { _entity.ReservePercentage = (double?)value; } }
		public int SecurityPeriod { get { return _entity.ReservePeriod.GetValueOrDefault(); } set { _entity.ReservePeriod = (short)value; } }
		public bool Enable { get { return _entity.IsAutoReserve; } set { _entity.IsAutoReserve = value; } }
		public bool AutoRet { get { return _entity.IsAutoReturn; } set { _entity.IsAutoReturn = value; } }
		public decimal KeepAmount { get { return _entity.FixHoldAmount.GetValueOrDefault(); } set { _entity.FixHoldAmount = value; } }
		public Netpay.CommonTypes.Currency KeepCurrency { get { return (Netpay.CommonTypes.Currency)_entity.FixHoldCurrency; } set { _entity.FixHoldCurrency = (byte)value; } }
		public Netpay.Infrastructure.MerchantDepositState State { get { return (Netpay.Infrastructure.MerchantDepositState)_entity.ActionType; } set { _entity.ActionType = (byte)value; } }

		private bool _isNew = true;
		private RollingReserve(Netpay.Dal.Netpay.SetMerchantRollingReserve entity)
		{ 
			_entity = entity;
			_isNew = false;
		}

		public static RollingReserve Load(int merchantID)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);

			AccountFilter.Current.Validate(Accounts.AccountType.Merchant, merchantID);
			var entity = (from mmas in DataContext.Reader.SetMerchantRollingReserves where mmas.Merchant_id == merchantID select mmas).SingleOrDefault();
			if (entity == null) return null;
			return new RollingReserve(entity);
		}

		public void Save()
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            DataContext.Writer.SetMerchantRollingReserves.Update(_entity, !_isNew);
			DataContext.Writer.SubmitChanges();
		}

		public class RollingReserveStatus
        { 
			public CommonTypes.Currency Currency { get; set; }
			public decimal Reserve { get; set; }
			public decimal Release { get; set; }
			public decimal Balance { get { return Reserve - Release; }}
		}

		public static List<RollingReserveStatus> GetMerchantStatus(int merchantID)
		{            
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);
			AccountFilter.Current.Validate(Accounts.AccountType.Merchant, merchantID);
			var items = (from t in DataContext.Reader.tblCompanyTransPasses 
				where t.companyID == merchantID && (t.PaymentMethod == (short)CommonTypes.PaymentMethodEnum.RollingReserve)
				group t by new { t.Currency, t.CreditType } into tg
				orderby tg.Key.Currency
				select new { Currency = (CommonTypes.Currency)tg.Key.Currency, creditType = tg.Key.CreditType, sumAmount = tg.Sum(t => t.Amount) });
			var ret = new List<RollingReserveStatus>();
			RollingReserveStatus lItem = null;
			foreach(var i in items){
				if (lItem == null || lItem.Currency != i.Currency){
					lItem = new RollingReserveStatus();
					lItem.Currency = i.Currency;
					ret.Add(lItem);
				}
				if (i.creditType == 0) lItem.Reserve = i.sumAmount;
				else if (i.creditType == 1) lItem.Release = i.sumAmount;
			}
			return ret;
		}
	}
}
