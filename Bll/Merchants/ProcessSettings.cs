﻿using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Netpay.Bll.Merchants
{
	public class ProcessSettings
	{
		public const string SecuredObjectName = "ProcessSettings";
		public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Merchants.Module.Current, SecuredObjectName); } }

		protected Netpay.Dal.Netpay.tblCompany _entity;
		internal ProcessSettings(Netpay.Dal.Netpay.tblCompany entity) { _entity = entity; }

        public ProcessSettings() 
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);
            
            _entity = new Netpay.Dal.Netpay.tblCompany();
        }

        public bool IsSystemPayCreditCard { get { return _entity.IsSystemPay; } set { _entity.IsSystemPay = value; } }
		public bool IsSystemPayECheck { get { return _entity.IsSystemPayEcheck; } set { _entity.IsSystemPayEcheck = value; } }
		public bool IsSystemPayCVV2 { get { return _entity.IsSystemPayCVV2; } set { _entity.IsSystemPayCVV2 = value; } }
		public bool IsSystemPayPhoneNumber { get { return _entity.IsSystemPayPhoneNumber; } set { _entity.IsSystemPayPhoneNumber = value; } }
		public bool IsSystemPayPersonalNumber { get { return _entity.IsSystemPayPersonalNumber; } set { _entity.IsSystemPayPersonalNumber = value; } }
		public bool IsSystemPayEmail { get { return _entity.IsSystemPayEmail; } set { _entity.IsSystemPayEmail = value; } }

		//public bool IsBillingAddressMust { get { return _entity.IsBillingAddressMust; } set { _entity.IsBillingAddressMust = value; } }
		//public bool IsBillingCityOptional { get { return _entity.IsBillingCityOptional; } set { _entity.IsBillingCityOptional = value; } }
		//public bool IsBillingAddressMustIDebit { get { return _entity.IsBillingAddressMustIDebit; } set { _entity.IsBillingAddressMustIDebit = value; } }
		//public bool IsCustomerPurchasePersonalNumber { get { return _entity.IsCustomerPurchasePersonalNumber; } set { _entity.IsCustomerPurchasePersonalNumber = value; } }
		//public bool IsCustomerPurchasePhoneNumber { get { return _entity.IsCustomerPurchasePhoneNumber; } set { _entity.IsCustomerPurchasePhoneNumber = value; } }
		//public bool IsCustomerPurchaseEmail { get { return _entity.IsCustomerPurchaseEmail; } set { _entity.IsCustomerPurchaseEmail = value; } }

		public bool IsRemoteCharge { get { return _entity.IsRemoteCharge; } set { _entity.IsRemoteCharge = value; } }
		public bool IsRemoteChargeCVV2 { get { return _entity.IsRemoteChargeCVV2; } set { _entity.IsRemoteChargeCVV2 = value; } }
		public bool IsRemoteChargePersonalNumber { get { return _entity.IsRemoteChargePersonalNumber; } set { _entity.IsRemoteChargePersonalNumber = value; } }
		public bool IsRemoteChargePhoneNumber { get { return _entity.IsRemoteChargePhoneNumber; } set { _entity.IsRemoteChargePhoneNumber = value; } }
		public bool IsRemoteChargeEmail { get { return _entity.IsRemoteChargeEmail; } set { _entity.IsRemoteChargeEmail = value; } }

		public bool IsRemoteChargeEcheck { get { return _entity.IsRemoteChargeEcheck; } set { _entity.IsRemoteChargeEcheck = value; } }
		public bool IsRemoteChargeEcheckCVV2 { get { return _entity.IsRemoteChargeEchCVV2; } set { _entity.IsRemoteChargeEchCVV2 = value; } }
		public bool IsRemoteChargeEcheckPersonalNumber { get { return _entity.IsRemoteChargeEchPersonalNumber; } set { _entity.IsRemoteChargeEchPersonalNumber = value; } }
		public bool IsRemoteChargeEcheckPhoneNumber { get { return _entity.IsRemoteChargeEchPhoneNumber; } set { _entity.IsRemoteChargeEchPhoneNumber = value; } }
		public bool IsRemoteChargeEcheckEmail { get { return _entity.IsRemoteChargeEchEmail; } set { _entity.IsRemoteChargeEchEmail = value; } }

		public bool IsAllowSilentPostCcDetails { get { return _entity.IsAllowSilentPostCcDetails; } set { _entity.IsAllowSilentPostCcDetails = value; } }		
		public bool IsRequiredClientIP { get { return _entity.IsRequiredClientIP; } set { _entity.IsRequiredClientIP = value; } }

		//public bool ForceRecurringMD5 { get { return _entity.ForceRecurringMD5.GetValueOrDefault(); } set { _entity.ForceRecurringMD5 = value; } }
		/*Misc*/
		public int MultiChargeProtectionMins { get { return _entity.MultiChargeProtectionMins.GetValueOrDefault(); } set { _entity.MultiChargeProtectionMins = (short)value; } }	

		public bool IsAutoPersonalSignup { get { return _entity.IsAutoPersonalSignup; } set { _entity.IsAutoPersonalSignup = value; } }
		public bool IsCcStorage { get { return _entity.IsCcStorage; } set { _entity.IsCcStorage = value; } }
		public bool IsTerminalProbLog { get { return _entity.IsTerminalProbLog; } set { _entity.IsTerminalProbLog = value; } }
		public bool IsConnectionProbLog { get { return _entity.IsConnectionProbLog; } set { _entity.IsConnectionProbLog = value; } }
		public bool IsAllowMakePayments { get { return _entity.IsAllowMakePayments; } set { _entity.IsAllowMakePayments = value; } }
		public bool IsAllowBatchFiles { get { return _entity.IsAllowBatchFiles; } set { _entity.IsAllowBatchFiles = value; } }
		public bool IsCcStorageCharge { get { return _entity.IsCcStorageCharge; } set { _entity.IsCcStorageCharge = value; } }
		public bool IsTransLookup { get { return _entity.IsTransLookup; } set { _entity.IsTransLookup = value; } }

		/*authorization*/
		public bool IsApprovalOnly { get { return _entity.IsApprovalOnly; } set { _entity.IsApprovalOnly = value; } }
		public bool IsCaptureApprovalOnly { get { return _entity.IsConfirmation; } set { _entity.IsConfirmation = value; } }
		public int AutoCaptureHours { get { return _entity.AutoCaptureHours; } set { _entity.AutoCaptureHours = value; } }

		/*refund*/
		public bool IsRefund { get { return _entity.IsRefund; } set { _entity.IsRefund = value; } }
		public bool IsAskRefund { get { return _entity.IsAskRefund; } set { _entity.IsAskRefund = value; } }
		public bool IsAskRefundRemote { get { return _entity.IsAskRefundRemote; } set { _entity.IsAskRefundRemote = value; } }
		public string RemoteRefundRequestIPs { get { return _entity.RemoteRefundRequestIPs; } set { _entity.RemoteRefundRequestIPs = value; } }

		/*data pulling*/
		public bool IsAllowRemotePull { get { return _entity.IsAllowRemotePull; } set { _entity.IsAllowRemotePull = value; } }
		public string RemotePullIPs { get { return _entity.RemotePullIPs; } set { _entity.RemotePullIPs = value; } }

		/*notifications*/
		public bool IsPassNotificationSentToPayer { get { return _entity.IsSendUserConfirmationEmail; } set { _entity.IsSendUserConfirmationEmail = value; } }
		public string MerchantNotifyEmails { get { return _entity.PassNotifyEmail; } set { _entity.PassNotifyEmail = value; } }
		public bool IsPassNotificationSentToMerchant { get { return _entity.IsMerchantNotifiedOnPass.GetValueOrDefault(); } set { _entity.IsMerchantNotifiedOnPass = value; } }
		public bool IsFailNotificationSentToMerchant { get { return _entity.IsMerchantNotifiedOnFail; } set { _entity.IsMerchantNotifiedOnFail = value; } }


		/*recurring*/
		/*
		public bool IsAllowRecurring { get { return _entity.IsAllowRecurring; } set { _entity.IsAllowRecurring = value; } }
		public bool IsAllowRecurringFromTrans { get { return _entity.IsAllowRecurringFromTransPass; } set { _entity.IsAllowRecurringFromTransPass = value; } }
		public int RecurringLimitCharges { get { return _entity.RecurringLimitCharges; } set { _entity.RecurringLimitCharges = value; } }
		public int RecurringLimitStages { get { return _entity.RecurringLimitStages; } set { _entity.RecurringLimitStages = value; } }
		public int RecurringLimitYears { get { return _entity.RecurringLimitYears; } set { _entity.RecurringLimitYears = value; } }
		*/
	}
}
