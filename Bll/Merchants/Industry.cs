﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll.Merchants
{
	public class Industry : BaseDataObject
	{
		private Dal.Netpay.SICCode _entity;
		public int ID { get { return (int)_entity.SICCodeNumber; } }
		public string Name { get { return _entity.IndustryName; } }

		private Industry(Dal.Netpay.SICCode entity)
		{
			_entity = entity;
		}

		public static List<Industry> Cache
		{
			get
			{
                return Domain.Current.GetCachData("Merchant.SICCode", () => { 
                    return new Domain.CachData((from c in DataContext.Reader.SICCodes select new Industry(c)).ToList(), DateTime.Now.AddHours(6));
                }) as List<Industry>;
			}
		}

        public static string GetNameById(int id)
        {
            if (id <= 0) return "";
            
            var indusrtyitem = Cache.Where(x => x.ID == id).SingleOrDefault();

            if (indusrtyitem != null) return indusrtyitem.Name;
            else return "";
        }


	}
}
