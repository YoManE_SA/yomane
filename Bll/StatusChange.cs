﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll
{
	public class StatusChange : BaseDataObject
	{
		public enum StatusItemType
		{
			RefundReq = 1,		
		}

		public enum ActionStatus
		{
			New = 0,
			Pending = 1,
			InProgress = 2,
			Created = 50,
			Processed = 51,
			Batched = 52,
			Printed = 53,
			Error = 100,
			AdminCancel = 101,
			SourceCancel = 102,
		}

		public class SearchFilters
		{
			public Infrastructure.Range<int?> ID;
			public Infrastructure.Range<DateTime?> InsertDate;
			public int? ItemID;
			public StatusItemType? ItemType;
			public ActionStatus? Status;
		}

		private Dal.Netpay.StatusHistory _entity;

		public int ID { get { return _entity.StatusHistory_id; } }
		public StatusItemType ItemType { get { return _entity.StatusHistoryType_id.ToNullableEnumByName<StatusItemType>().GetValueOrDefault(); } }
		public int? ItemID { get { return _entity.SourceIdentity; } }
		public System.DateTime InsertDate { get { return _entity.InsertDate; } }
		public ActionStatus Status { get { return (ActionStatus)_entity.ActionStatus_id; } }
		public string UserName { get { return _entity.InsertUserName; } }
		public string IPAddress { get { return _entity.InsertIPAddress; } }
		public string Text { get { return _entity.VariableChar; } }

		private StatusChange(Dal.Netpay.StatusHistory entity)
		{ 
			_entity = entity;
		}

		public static void Create(StatusItemType itemType, int itemId, ActionStatus status, string text)
		{
			var entity = new Dal.Netpay.StatusHistory();
			entity.InsertDate = DateTime.Now;
			entity.ActionStatus_id = (byte)status;
			entity.SourceIdentity = itemId;
			entity.StatusHistoryType_id = "System." + itemType.ToString();
            entity.VariableChar = text;
			if (Infrastructure.Security.Login.Current != null) entity.InsertUserName = Infrastructure.Security.Login.Current.UserName;
			DataContext.Writer.StatusHistories.InsertOnSubmit(entity);
			DataContext.Writer.SubmitChanges();
		}

		public static List<StatusChange> Search(SearchFilters filters, ISortAndPage sortAndPage)
		{
			var expression = (from p in DataContext.Reader.StatusHistories select p);
			if (filters != null)
			{
				if (filters.ID.From != null) expression = expression.Where(p => p.StatusHistory_id >= filters.ID.From);
				if (filters.ID.To != null) expression = expression.Where(p => p.StatusHistory_id <= filters.ID.To);
				if (filters.InsertDate.From != null) expression = expression.Where(p => p.InsertDate >= filters.InsertDate.From);
				if (filters.InsertDate.To != null) expression = expression.Where(p => p.InsertDate <= filters.InsertDate.To);

				if (filters.ItemID.HasValue) expression = expression.Where(p => p.SourceIdentity == filters.ItemID.Value);
				if (filters.ItemType.HasValue) expression = expression.Where(p => p.StatusHistoryType_id == "System." + filters.ItemType.Value.ToString());
				if (filters.Status.HasValue) expression = expression.Where(p => p.ActionStatus_id == (byte)filters.Status.Value);
			}
			return expression.ApplySortAndPage(sortAndPage).Select(p => new StatusChange(p)).ToList();
		
		}
	}
}
