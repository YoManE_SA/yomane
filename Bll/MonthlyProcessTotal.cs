﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure.Domains;
using Netpay.Dal.DataAccess;

namespace Netpay.Bll
{
	public static class MonthlyProcessTotal
	{
		public static decimal GetCurrentMonthlyTotal(Domain domain, int? merchantId, int? affiliateId, int? debitCompanyId, CommonTypes.Currency targetcurrency, CommonTypes.Currency currency, decimal amount)
		{
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			var exp = (from mpd in dc.MonthlyProcessTotals select mpd);
			if (merchantId != null) exp = exp.Where(mpd => mpd.Merchant_id == merchantId);
			if (affiliateId != null) exp = exp.Where(mpd => mpd.Affiliate_id == affiliateId);
			if (debitCompanyId != null) exp = exp.Where(mpd => mpd.DebitCompany_id == debitCompanyId);
			//if (currency != null) exp = exp.Where(mpd => mpd.Currency_id == (byte)currency || mpd.Currency_id == null);
			Dal.Netpay.MonthlyProcessTotal pd = exp.OrderByDescending(mpd => mpd.Currency_id).FirstOrDefault();
			if (pd == null) {
				pd = new Dal.Netpay.MonthlyProcessTotal();
				pd.Currency_id = (byte)targetcurrency;
				pd.Merchant_id = merchantId;
				pd.Affiliate_id = affiliateId;
				pd.DebitCompany_id = debitCompanyId;
				dc.MonthlyProcessTotals.InsertOnSubmit(pd);
			}
			if (currency != targetcurrency) amount = (new Netpay.Bll.Money(domain, amount, currency).ConvertTo(domain, targetcurrency)).Amount;
			if ((DateTime.Now.Month != pd.DateInFocus.Month) || (DateTime.Now.Year != pd.DateInFocus.Year)) {
				pd.DateInFocus = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
				pd.Amount = amount;
			} else pd.Amount += amount;
			dc.SubmitChanges();
			return pd.Amount - amount;
		}

		public static decimal GetMonthlyTotalForDate(Domain domain, DateTime toDate, int? merchantId, int? affiliateId, int? debitCompanyId, CommonTypes.Currency targetcurrency)
		{
			NetpayDataContext dc = new NetpayDataContext(domain.Sql1ConnectionString);
			decimal retValue = 0;
			var texp = (from a in dc.tblTransactionAmounts 
				where a.TypeID == (int) CommonTypes.TransactionAmountType.Capture &&
				a.InsertDate >= new DateTime(toDate.Year, toDate.Month, 1) &&
				a.InsertDate < toDate
				select a);
			if (merchantId != null) texp = texp.Where(mpd => mpd.MerchantID == merchantId);
			if (affiliateId != null) texp = texp.Where(mpd => mpd.Affiliates_id == affiliateId);
			if (debitCompanyId != null) texp = texp.Where(mpd => mpd.DebitCompany_id == debitCompanyId);
			var totals = texp.GroupBy(a => a.Currency).Select(g => new { Currency = g.Key, TotalAmount = g.Sum(a => a.Amount) });
			foreach (var t in totals) {
				retValue += (new Netpay.Bll.Money(domain, t.TotalAmount, t.Currency).ConvertTo(domain, targetcurrency)).Amount;
			}
			return retValue;
		}
	}
}
