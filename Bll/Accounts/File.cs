﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Accounts
{
    public class File : BaseDataObject
    {
        public const string SecuredObjectName = "FilesAndNotes";
        public static SecuredObject SecuredObject { get { return SecuredObject.Get(Accounts.Module.Current, SecuredObjectName); } }

        public class SearchFilters
        {
            public bool? IsOnlyAdmin;
            public int? AccountId;
            public string FileName;
            public Range<int?> ID;
            public Range<DateTime?> FileDate;
            public List<string> FileTypes;
        }

        private Netpay.Dal.Netpay.AccountFile _entity;

        public int ID { get { return _entity.AccountFile_id; } }
        public System.DateTime InsertDate { get { return _entity.InsertDate; } }
        public int? Account_id { get { return _entity.Account_id; } set { _entity.Account_id = value; } }
        public string FileTitle { get { return _entity.FileTitle; } set { _entity.FileTitle = value; } }
        public string FileName { get { return _entity.FileName; } }
        public string FileExt { get { return _entity.FileExt; } }
        public byte? FileItemType_id { get { return _entity.FileItemType_id; } set { _entity.FileItemType_id = value; } }
        public string AdminComment { get { return _entity.AdminComment; } set { _entity.AdminComment = value; } }
        public System.DateTime? AdminApprovalDate { get { return _entity.AdminApprovalDate; } private set { _entity.AdminApprovalDate = value; } }
        public string AdminApprovalUser { get { return _entity.AdminApprovalUser; } private set { _entity.AdminApprovalUser = value; } }
        public bool IsApproved { get { return AdminApprovalDate.HasValue; } }

        public File()
        {
            _entity = new Dal.Netpay.AccountFile();
        }

        public File(int accountId)
        {
            _entity = new Dal.Netpay.AccountFile();
            _entity.Account_id = accountId;
            _entity.InsertDate = DateTime.Now;
        }

        private File(Netpay.Dal.Netpay.AccountFile entity)
        {
            _entity = entity;
        }

        public static File Load(int id)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var entity = (from mmas in DataContext.Reader.AccountFiles where mmas.AccountFile_id == id select mmas).SingleOrDefault();
            if (entity == null) return null;
            return new File(entity);
        }

        public static List<File> LoadForAccount(int accountId)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from mmas in DataContext.Reader.AccountFiles where mmas.Account_id == accountId select new File(mmas)).ToList();
        }

        public static List<File> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var expression = (from f in DataContext.Reader.AccountFiles select f);
            if (filters != null)
            {
                if (filters.ID.From != null) expression = expression.Where(f => f.AccountFile_id >= filters.ID.From);
                if (filters.ID.To != null) expression = expression.Where(f => f.AccountFile_id <= filters.ID.To);
                if (!string.IsNullOrEmpty(filters.FileName)) expression = expression.Where((f) => f.FileName.Contains(filters.FileName));
                if (filters.FileDate.From.HasValue) expression = expression.Where((f) => f.InsertDate >= filters.FileDate.From.Value.Date);
                if (filters.FileDate.To.HasValue) expression = expression.Where((f) => f.InsertDate <= filters.FileDate.To.Value.Date.AddDays(1).AddMilliseconds(-1));
                if (filters.AccountId.HasValue) expression = expression.Where((f) => f.Account_id.Value == filters.AccountId.Value);
                if (filters.IsOnlyAdmin.HasValue && filters.IsOnlyAdmin.Value) expression = expression.Where((f) => f.Account_id == null);
                if (filters.FileTypes != null && filters.FileTypes.Count > 0) expression = expression.Where((f) => filters.FileTypes.Contains(f.FileExt));
            }
            return expression.ApplySortAndPage(sortAndPage).Select(f => new File(f)).ToList();
        }

        public void Save(string fileName, byte[] data)
        {
            if (ID == 0)
            {
                // Save the file only if this is a new file (i.e. not in update mode)
                // This is the desired behavior, for example, in:
                // Merchants Management -> Files&Notes Tab -> Edit File -
                // i.e. when opening edit file modal dialog, and then saving, don't change the file name and data.
                fileName = Domain.GetUniqueFileName(Account.MapPrivatePath(Account_id, "Files/" + System.IO.Path.GetFileName(fileName)));
                if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(fileName)))
                    System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(fileName));
                System.IO.File.WriteAllBytes(fileName, data);
                _entity.FileName = System.IO.Path.GetFileName(fileName);
                _entity.FileExt = System.IO.Path.GetExtension(fileName).Replace(".", "");
            }
            Save();
        }

        public string AccountName
        {
            get
            {
                if (Account_id.HasValue)
                {
                    return Account.GetAccountInfo(new List<int>() { Account_id.Value }).Values.SingleOrDefault().AccountName;
                }
                else
                {
                    return null;
                }
            }
        }

        public string FilePath { get { if (string.IsNullOrEmpty(_entity.FileName)) return null; return Account.MapPrivatePath(Account_id, "Files/" + _entity.FileName); } }

        public string DeletedFilePath { get { if (string.IsNullOrEmpty(_entity.FileName)) return null; return Account.MapDeletedFilesPath(_entity.FileName); } }

        public void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            if (_entity.AccountFile_id == 0 && _entity.InsertDate == default(DateTime))
                _entity.InsertDate = DateTime.Now;
            DataContext.Writer.AccountFiles.Update(_entity, (_entity.AccountFile_id != 0));
            DataContext.Writer.SubmitChanges();
        }

        public void SetApprove(bool unApprove)
        {
            AdminApprovalDate = unApprove ? (DateTime?)null : DateTime.Now;
            AdminApprovalUser = Login.Current.UserName;

            Save();
        }

        public void Delete()
        {
            //Save needed data path's
            string oldFilePath = FilePath;
            string deletedFilePath = DeletedFilePath;

            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            if (_entity.AccountFile_id == 0) return;
            DataContext.Writer.AccountFiles.Delete(_entity);
            DataContext.Writer.SubmitChanges();


            //Move file to "Deleted" folder
            if (System.IO.File.Exists(oldFilePath))
            {
                //Create the Deleted directory if needed
                if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(deletedFilePath)))
                    System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(deletedFilePath));

                //Change file name if needed
                if (System.IO.File.Exists(deletedFilePath))
                {
                    string fileName = System.IO.Path.GetFileNameWithoutExtension(deletedFilePath);
                    string newPath = deletedFilePath.Replace(fileName, fileName + DateTime.Now.Ticks.ToString());
                    System.IO.File.Move(oldFilePath, newPath);
                }
                else
                {
                    System.IO.File.Move(oldFilePath, deletedFilePath);
                }
            }
        }

        public static Dictionary<int, string> FileTypes
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
                                
                return Bll.GlobalDataManager.FileItemTypes.FileItemType.Cache.ToDictionary(x => x.ID, x => x.Name);
            }
        }

        public static System.Collections.Generic.List<string> AllFileExt
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                return Domain.Current.GetCachData("FilesManager.FileExt", () =>
                {
                    List<string> allFileExt =
                            (from t in DataContext.Reader.AccountFiles
                             select t.FileExt.ToUpper())
                            .Distinct()
                            .ToList();

                    return new Domain.CachData(allFileExt, DateTime.Now.AddDays(1));
                }) as System.Collections.Generic.List<string>;
            }
        }

        public static string GetFileTypeText(int? typeId)
        {
            if (typeId == null) return string.Empty;
            string ret;
            if (!FileTypes.TryGetValue(typeId.Value, out ret)) return string.Empty;
            return ret;
        }


        public class FileInfo
        {
            public int FileId { get; set; }
            public int? AccountId { get; set; }
            public string FileName { get; set; }
            public string FileExt { get; set; }
        }
        public static List<FileInfo> AutoComplete(string text)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from t in DataContext.Reader.AccountFiles
                    where (t.FileName).Contains(text)
                    orderby t.FileName ascending
                    select new FileInfo() { FileId = t.AccountFile_id, AccountId = t.Account_id, FileName = t.FileName, FileExt = t.FileExt }).Take(10).ToList();
        }

        public static List<FileInfo> AutoComplete(string text, int accountID)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from t in DataContext.Reader.AccountFiles
                    where (t.FileName).Contains(text) && t.Account_id == accountID
                    orderby t.FileName ascending
                    select new FileInfo() { FileId = t.AccountFile_id, AccountId = t.Account_id, FileName = t.FileName, FileExt = t.FileExt }).Take(10).ToList();
        }
    }
}
