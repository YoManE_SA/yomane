﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll.Accounts
{
    public class ExternalServiceLogin : BaseDataObject
    {
        public class Protocols
        {
            //public const string Unknown = "Unknown";
            public const string HTTP = "HTTP";
            public const string HTTPS = "HTTPS";
            public const string FTP = "FTP";
            public const string SFTP = "SFTP";
            public const string FTPS = "FTPS";
        }

        public class ServiceTypes
        {
            public const string Auth = "System.Auth";
            public const string Deposit = "System.Deposit";
            public const string CHB = "System.CHB";
            public const string EPA = "System.EPA";
            public const string Fraud = "System.Fraud";
        }

        private Dal.Netpay.AccountExternalService _entity;

        public int ID { get { return _entity.AccountExternalService_id; } }
        public int AccountID { get { return _entity.Account_id; } }
        public string ServiceType { get { return _entity.ExternalServiceType_id; } }
        public string ProtocolType { get { return _entity.ProtocolType_id; } }
        public bool IsActive { get { return _entity.IsActive; } set { _entity.IsActive = value; } }
        public string ServerURL { get { return _entity.ServerURL; } set { _entity.ServerURL = value; } }
        public string Username { get { return _entity.Username; } set { _entity.Username = value; } }
        public string Reference1 { get { return _entity.Identifier1; } set { _entity.Identifier1 = value; } }
        public string Password { get { return Infrastructure.Security.Encryption.Decrypt(_entity.EncryptionKey, _entity.PasswordEncrypted); } set { _entity.EncryptionKey = (byte)Domain.EncryptionKeyNumber; _entity.PasswordEncrypted = Infrastructure.Security.Encryption.Encrypt(value); } }
        public string Method { get { return _entity.ProtocolType_id; } set { _entity.ProtocolType_id = value; } }
        public string HostIp { get { return _entity.HostIp; } set { _entity.HostIp = value; } }
        public int Port {
            get {
                return _entity.Port.HasValue?Convert.ToInt32(_entity.Port):21; //default port  = 21
            } set { _entity.Port = value; } }

        public string FtpRootFolder { get { return _entity.RemoteRoot; } set { _entity.RemoteRoot = value; } }
        public string SearchableFolderList { get { return _entity.SearchableFolderList; } set { _entity.SearchableFolderList = value; } }
        public string SearchableExtensionList { get { return _entity.SearchableExtensionList; } set { _entity.SearchableExtensionList = value; } }

        


        internal ExternalServiceLogin(Dal.Netpay.AccountExternalService entity) { _entity = entity; }

        public ExternalServiceLogin(int accountId, string serviceType)
        {
            _entity = new Dal.Netpay.AccountExternalService()
            {
                Account_id = accountId,
                ExternalServiceType_id = serviceType,
            };
        }

        public static void RegisterServiceType(string serviceType, string description)
        {
            if (DataContext.Reader.ExternalServiceTypes.Any(v => v.ExternalServiceType_id == serviceType)) return;
            DataContext.Writer.ExternalServiceTypes.InsertOnSubmit(new Dal.Netpay.ExternalServiceType() { ExternalServiceType_id = serviceType, Name = description });
            DataContext.Writer.SubmitChanges();
        }

        public void Save()
        {
            DataContext.Writer.AccountExternalServices.Update(_entity, (_entity.AccountExternalService_id != 0));
            DataContext.Writer.SubmitChanges();
        }

        public void Delete()
        {
            if (_entity.AccountExternalService_id == 0) return;
            DataContext.Writer.AccountExternalServices.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }

        public static ExternalServiceLogin Load(int id)
        {
            return (from n in DataContext.Reader.AccountExternalServices where n.AccountExternalService_id == id select new ExternalServiceLogin(n)).SingleOrDefault();
        }

        public static List<ExternalServiceLogin> LoadForAccount(int accountId)
        {
            return (from n in DataContext.Reader.AccountExternalServices where n.Account_id == accountId select new ExternalServiceLogin(n)).ToList();
        }

        public static ExternalServiceLogin LoadForAccount(int accountId, string serviceType)
        {
            return (from n in DataContext.Reader.AccountExternalServices where n.Account_id == accountId && n.ExternalServiceType_id == serviceType select new ExternalServiceLogin(n)).FirstOrDefault();
        }

        public override ValidationResult Validate()
        {
            if (!IsActive) return ValidationResult.Success;
            if (string.IsNullOrEmpty(ServerURL)) return ValidationResult.Invalid_ServiceUrl;
            /* || (string.IsNullOrEmpty(Username) ^ string.IsNullOrEmpty(Password))*/
            return base.Validate();
        }

        public string GetProtocolLoginCommand(string privatekeyFile = null, string hostKey = null)
        {
            string addressOnly = _entity.ServerURL.EmptyIfNull();
            int protocolEndIndex = addressOnly.IndexOf("://");
            if (protocolEndIndex > -1) addressOnly = addressOnly.Substring(protocolEndIndex + 3);
            string loginKeys = "";
            if (!string.IsNullOrEmpty(privatekeyFile)) loginKeys += " -privatekey=" + privatekeyFile;
            if (!string.IsNullOrEmpty(hostKey)) loginKeys += " -hostkey=\"" + hostKey + "\"";
            switch (Method)
            {
                case Protocols.FTP: return "open ftp://" + Username + ":" + Password + "@" + addressOnly + loginKeys;
                case Protocols.FTPS: return "open ftps://" + Username + ":" + Password + "@" + addressOnly;
                case Protocols.SFTP: return "open sftp://" + Username + ":" + Password + "@" + addressOnly + loginKeys;
                case Protocols.HTTP: return "open http://" + Username + ":" + Password + "@" + addressOnly;
                case Protocols.HTTPS: return "open https://" + Username + ":" + Password + "@" + addressOnly;
            }
            throw new Exception(string.Format("not supported method type '{0}'", Method));
        }

        public Dictionary<string,string> GetSftpCredentials()
        {
            var dic = new Dictionary<string, string>();

            dic.Add("Username", Username);
            dic.Add("Password", Password);
            dic.Add("url", _entity.ServerURL.EmptyIfNull());

            return dic;
        }

        public int ExecuteFTPScript(string scriptFileName, string logFileName)
        {
            /// TODO: Insane unwanted, unneeded, annoying as hell logging is here
            //Logger.Log("Inside ExecuteFTPScript() method, scriptFileName:" + scriptFileName);

            System.Diagnostics.ProcessStartInfo processInfo = new System.Diagnostics.ProcessStartInfo();
            processInfo.FileName = Infrastructure.Application.GetParameter("sftpExecutablePath");
            if (string.IsNullOrEmpty(processInfo.FileName))
            {
                Logger.Log(LogSeverity.Error, LogTag.Process, "Could not find sftp client." + processInfo.FileName);
                throw new ApplicationException("Could not find sftp client.");
            }
            processInfo.Arguments = " /script=" + scriptFileName + " /log=" + logFileName;

            using (System.Diagnostics.Process process = new System.Diagnostics.Process())
            {
                process.StartInfo = processInfo;
                if (!process.Start())
                {
                    Logger.Log(LogSeverity.Error, LogTag.Process, "Unable to start SFTP process: " + processInfo.FileName);
                    throw new ApplicationException("Unable to start SFTP process: " + processInfo.FileName);
                }
                process.WaitForExit();
                var ret = process.ExitCode;
                process.Close();
                return ret;
            }
        }

        public bool TestConnection(out string logResult, string privatekeyFile = null, string hostKey = null)
        {
            try
            {
                string scriptFileName = Domain.MapTempPath("ExternalServiceTest.ftp");
                string logFile = Domain.MapTempPath("ExternalServiceTest.log");
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(scriptFileName))
                {
                    sw.WriteLine(GetProtocolLoginCommand(privatekeyFile, hostKey));
                    sw.WriteLine("option batch continue");
                    sw.WriteLine("option confirm off");
                    sw.WriteLine("exit");
                    sw.Close();
                }
                var ret = ExecuteFTPScript(scriptFileName, logFile);
                if (System.IO.File.Exists(scriptFileName)) System.IO.File.Delete(scriptFileName);
                if (System.IO.File.Exists(logFile))
                {
                    logResult = System.IO.File.ReadAllText(logFile);
                    System.IO.File.Delete(logFile);
                }
                else logResult = String.Format("Log file '{0}' not created", logFile);
                return (ret == 0);
            }
            catch (Exception ex)
            {
                logResult = ex.Message;
                return false;
            }
        }

    }
}
