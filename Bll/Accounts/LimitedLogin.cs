﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Accounts
{
	public class LimitedLogin : Login
	{
        // This class is used for MCP so no admin permission check needed.

		private Dal.Netpay.AccountSubUser _entity;
		public int ID { get { return _entity.AccountSubUser_id; } }
		public int AccountID { get { return _entity.Account_id; } }
		public string Description { get { return _entity.Description; } set { _entity.Description = value; } }
		private string _initialPassword;

		public const string LimitedSecuredObjectName = "LimitedLogin";
		public static SecuredObject LimitedSecuredObject { get { return SecuredObject.Get(Accounts.Module.Current, LimitedSecuredObjectName); } }

		private LimitedLogin(Dal.Netpay.AccountSubUser entity, Dal.Netpay.LoginAccount loginEntity)
			: base(loginEntity)
		{ 
			_entity = entity;
		}

		public LimitedLogin(int? accountId, string userName, string initialPassword = null) : base(UserRole.MerchantSubUser)
		{
			AccountFilter.Current.Validate(null, ref accountId);
			//if (merchantId == null && User.Type != UserType.MerchantPrimary) throw new Exception("can't create LimitedUser merchantId not specified");
			_entity = new Dal.Netpay.AccountSubUser();
			_entity.Account_id = accountId.Value;
			_initialPassword = initialPassword;
			base.UserName = userName;
		}

		protected override bool VerifySecurity(bool throwError = true)
		{
			if ((Login.Current.Role == UserRole.Merchant) && (AccountID == Accounts.Account.Current.AccountID)) return true;
			return base.VerifySecurity(throwError);
		}

		public override void Delete()
		{            
            if (_entity.AccountSubUser_id == 0) return;
			VerifySecurity();
			DataContext.Writer.AccountSubUsers.Delete(_entity);
			DataContext.Writer.SubmitChanges();
			base.Delete();
		}

		public new CreateLimitedUserResult Save()
		{
			Netpay.Dal.Netpay.MerchantSubUser foundUser = (from lu in DataContext.Reader.MerchantSubUsers where lu.UserName == UserName.Trim() select lu).SingleOrDefault();
			if (foundUser != null) return CreateLimitedUserResult.UserAlreadyExists;
			bool isNewUser = (_entity.AccountSubUser_id == 0);
			base.Save();
			_entity.LoginAccount_id = base.LoginID;
			DataContext.Writer.AccountSubUsers.Update(_entity, !isNewUser);
			DataContext.Writer.SubmitChanges();
			if (isNewUser) {
				if (_initialPassword == null) _initialPassword = GeneratePassword();
				SetPassword(_initialPassword, null);
			}
			return CreateLimitedUserResult.Success;
		}

		public static LimitedLogin Load(int id)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Account }, LimitedSecuredObject, PermissionValue.Read);
			//AccountFilter.Current.Validate(null, ref accountId);
			var entity = (from lu in DataContext.Reader.AccountSubUsers.Where(AccountFilter.Current.QueryValidate(null, (Dal.Netpay.AccountSubUser z) => z.Account_id)) join l in DataContext.Reader.LoginAccounts on lu.LoginAccount_id equals l.LoginAccount_id where lu.AccountSubUser_id == id select new { l, lu }).SingleOrDefault();
			if (entity == null) return null;
			return new LimitedLogin(entity.lu, entity.l);
		}

		public static LimitedLogin LoadForLogin(int loginId)
		{
			//.Where(AccountFilter.Current.QueryValidate(null, (Dal.Netpay.AccountSubUser z) => z.Account_id))
			var entity = (from lu in DataContext.Reader.AccountSubUsers join l in DataContext.Reader.LoginAccounts on lu.LoginAccount_id equals l.LoginAccount_id where lu.LoginAccount_id == loginId select new { l, lu }).SingleOrDefault();
			if (entity == null) return null;
			return new LimitedLogin(entity.lu, entity.l);
		}

		public static List<LimitedLogin> Search(int? accountId)
		{
			ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Account }, LimitedSecuredObject, PermissionValue.Read);
			AccountFilter.Current.Validate(null, ref accountId);
			var limitedUsers = (from lu in DataContext.Reader.AccountSubUsers join l in DataContext.Reader.LoginAccounts on lu.LoginAccount_id equals l.LoginAccount_id where lu.Account_id == accountId select new LimitedLogin(lu, l)).ToList();
			return limitedUsers;
		}

		public static bool IsLimitedLogin { get { return (Login.Current != null) && (Login.Current.Role == UserRole.MerchantSubUser); } }
		public new static LimitedLogin Current
		{
			get{
				var login = Login.Current;
				if (login == null) return null;
				if (!login.IsInRole(UserRole.Account)) return null;
				object obj;
				if (!login.Items.TryGetValue("LimitedLogin", out obj))
				{
					var ret = LimitedLogin.LoadForLogin(login.LoginID);
					login.Items["LimitedLogin"] = obj = ret;
				}
				return obj as LimitedLogin;
			}
		}

		public static void SetEmailForAccount(int accountId, string newEmail)
		{
			DataContext.Writer.ExecuteCommand("Update Data.LoginAccount Set LoginEmail={0} Where LoginAccount_id IN (Select LoginAccount_id From Data.AccountSubUser Where Account_id={1})", newEmail, accountId);
		}

		/*
		public ChangePasswordResult ChangePassword(string password, string passwordConfirm)
		{
			//User user = SecurityManager.GetInternalUser(new UserType[] { UserType.MerchantPrimary });
			if (password != passwordConfirm)
				return ChangePasswordResult.PasswordConfirmationMissmatch;
			if (!SecurityManager.CheckPasswordStrength(passwordConfirm))
				return ChangePasswordResult.WeakPassword;

			if(_entity.MerchantSubUser_id == 0) {
				_initialPassword = password;
				return ChangePasswordResult.Success;
			}
			var updateResult = DataContext.UpdatePasswordChange((int)UserType.MerchantLimited, ID, "", password).FirstOrDefault();
			if (updateResult.IsInserted.Value)
				return ChangePasswordResult.Success;
			else
				return ChangePasswordResult.PasswordUsedInThePast;
		}
		
		public static void SetLimitedUserPermission(int limitedUserID, string objectName, PermissionObjectType objectType, bool isAllowed)
		{
			context.IsUserOfType(new UserType[] { UserType.MerchantPrimary });
			var foundEntries = from lls in DataContext.Reader.tblLimitedLoginSecurities where lls.lls_Item == objectName && lls.lls_MerchantID == context.User.ID && lls.lls_UserID == limitedUserID select lls;
			if (foundEntries.Count() > 0)
			{
				DataContext.Current.tblLimitedLoginSecurities.DeleteAllOnSubmit(foundEntries);
				DataContext.Writer.SubmitChanges();
			}

			if (isAllowed)
			{
				Netpay.Dal.Netpay.tblLimitedLoginSecurity newEntry = new Netpay.Dal.Netpay.tblLimitedLoginSecurity();
				newEntry.lls_MerchantID = context.User.ID;
				newEntry.lls_Item = objectName;
				newEntry.lls_UserID = limitedUserID;
				newEntry.lls_ItemType = (int)objectType;

				DataContext.Current.tblLimitedLoginSecurities.InsertOnSubmit(newEntry);
				DataContext.Writer.SubmitChanges();
			}
		}
		 * */

	}
}
