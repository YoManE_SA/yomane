﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Netpay.Infrastructure;

namespace Netpay.Bll.Accounts
{
	public enum AccountType
	{
		Merchant = 1,
		Customer = 2,
		Affiliate = 3,
		DebitCompany = 4,
	}

	public class AccountFilter : BaseDataObject
	{
		public List<int> AccountIDs { get; private set; }
		public Dictionary<Bll.Accounts.AccountType, List<int>> ObjectIDs { get; private set; }

		public AccountFilter()
		{
			AccountIDs = new List<int>();
			ObjectIDs = new Dictionary<Bll.Accounts.AccountType, List<int>>();
			ObjectIDs.Add(Bll.Accounts.AccountType.Merchant, new List<int>());
			ObjectIDs.Add(Bll.Accounts.AccountType.Customer, new List<int>());
			ObjectIDs.Add(Bll.Accounts.AccountType.DebitCompany, new List<int>());
			ObjectIDs.Add(Bll.Accounts.AccountType.Affiliate, new List<int>());
		}

		public static AccountFilter LoadForAdminUser(int userId)
		{
			var ret = new AccountFilter();
			ret.AccountIDs = (from s in DataContext.Reader.AdminUserToAccounts where s.AdminUser_id == userId select s.Account_id).ToList();
			if(ret.AccountIDs.Count == 0) return ret;
			var accData = (from a in DataContext.Reader.Accounts where ret.AccountIDs.Contains(a.Account_id) select a).ToList(); 
			foreach (var a in accData) {
				if (a.Merchant_id.HasValue) ret.ObjectIDs[Bll.Accounts.AccountType.Merchant].Add(a.Merchant_id.Value);
				if (a.Customer_id.HasValue) ret.ObjectIDs[Bll.Accounts.AccountType.Customer].Add(a.Customer_id.Value);
				if (a.DebitCompany_id.HasValue) ret.ObjectIDs[Bll.Accounts.AccountType.DebitCompany].Add(a.DebitCompany_id.Value);
				if (a.Affiliate_id.HasValue) ret.ObjectIDs[Bll.Accounts.AccountType.Affiliate].Add(a.Affiliate_id.Value);
			}
			/*
			if (ret.ObjectIDs.ContainsKey(Bll.Accounts.AccountType.Merchant)) ret.ObjectIDs.Remove(Bll.Accounts.AccountType.Merchant);
			ret.ObjectIDs.Add(Bll.Accounts.AccountType.Merchant, new List<int>() { 35 });
			ret.AccountIDs = new List<int>();
			for (int i = 0; i < 2000; i++) 
				ret.AccountIDs.Add(i);
			*/
			return ret;
		}

        public AccountFilter Reduce()
        {
            foreach (var k in ObjectIDs.Keys.ToList())
                if (ObjectIDs[k].Count == 0) ObjectIDs.Remove(k);
            if (AccountIDs.Count == 0) AccountIDs = null;
            return this;
		}

		public List<int> FilterIDList(Bll.Accounts.AccountType? accountType, List<int> dstList)
		{
			List<int> srcList = AccountIDs;
			if (accountType.HasValue)
				if (!ObjectIDs.TryGetValue(accountType.Value, out srcList)) srcList = null;
			if (srcList == null) return dstList;
			if (dstList == null) return srcList; 
			dstList.RemoveAll(v => !srcList.Contains(v));
			return dstList;
		}

		public System.Linq.Expressions.Expression<Func<TEntity, bool>> QueryValidate<TEntity, TProperty>(Bll.Accounts.AccountType? accountType, System.Linq.Expressions.Expression<Func<TEntity, TProperty>> property)
		{
			List<int> srcList = AccountIDs;
			if (accountType.HasValue)
				if (!ObjectIDs.TryGetValue(accountType.Value, out srcList)) srcList = null;
			var parameter = Expression.Parameter(typeof(TEntity));
            if (srcList == null) return System.Linq.Expressions.Expression.Lambda<Func<TEntity, bool>>(System.Linq.Expressions.Expression.Constant(true), parameter);
			var propBody = property.Body as MemberExpression;
			if (propBody == null) throw new Exception("property should be a member expression");
			var propertyInfo = propBody.Member as System.Reflection.PropertyInfo;
			if (propertyInfo == null) throw new Exception("property member expression should be a property");

			var innerMethod = typeof(List<int>).GetMethod("Contains", new[] { typeof(int) });
			var listExpression = System.Linq.Expressions.Expression.Constant(srcList, srcList.GetType());
			var propGet = Expression.Property(parameter, propertyInfo) as Expression;
			if (Nullable.GetUnderlyingType(propertyInfo.PropertyType) != null)
				propGet = Expression.Call(propGet, typeof(Nullable<int>).GetMethod("GetValueOrDefault", new Type[] {} ));
			var containsCall = System.Linq.Expressions.Expression.Call(listExpression, innerMethod, new[] { propGet });
			return System.Linq.Expressions.Expression.Lambda<Func<TEntity, bool>>(containsCall, parameter);
		}

		public int? GetCurrentValue(Bll.Accounts.AccountType? accountType)
		{
			var account = Account.Current;
			if (account == null) return null;
			if (accountType == null) return account.AccountID;
			switch(accountType.Value){
			case AccountType.Merchant: return account.MerchantID;
			case AccountType.DebitCompany: return account.DebitCompanyID;
			case AccountType.Customer: return account.CustomerID;
			case AccountType.Affiliate: return account.AffiliateID;
			}
			return null;
		}

		public bool Validate(Bll.Accounts.AccountType? accountType, int id, bool throwError = true)
		{
			List<int> srcList = AccountIDs;
			if(accountType.HasValue) 
				if (!ObjectIDs.TryGetValue(accountType.Value, out srcList)) srcList = null;
			bool ret = srcList == null || srcList.Contains(id);
			if (!ret && throwError) new MethodAccessDeniedException();
			return ret;
		}

		public bool Validate(Bll.Accounts.AccountType? accountType, ref int? id, bool throwError = true)
		{
			if (id == null) id = GetCurrentValue(accountType);
			if (id == null) throw new InvalidOperationException("paramter must have value for this login type");
			return Validate(accountType, id.Value, throwError);
		}

		private static void AttachAccountToUser(int userId, int accountId)
		{
			var row = new Dal.Netpay.AdminUserToAccount() { AdminUser_id = (short)userId, Account_id = accountId };
			DataContext.Writer.AdminUserToAccounts.InsertOnSubmit(row);
			DataContext.Writer.SubmitChanges();
		}

		private static void DeattachAccountToUser(int userId, int accountId)
		{
			var row = DataContext.Writer.AdminUserToAccounts.Where(s => s.AdminUser_id == userId && s.Account_id == accountId);
			DataContext.Writer.AdminUserToAccounts.DeleteAllOnSubmit(row);
			DataContext.Writer.SubmitChanges();
		}

		private static AccountFilter LoadForLogin(Infrastructure.Security.Login login)
		{
			if(login.Role == Infrastructure.Security.UserRole.Admin){
				if (Infrastructure.Security.AdminUser.Current != null) return LoadForAdminUser(Infrastructure.Security.AdminUser.Current.ID).Reduce();
				return new AccountFilter();
			}
			var account = Accounts.Account.Current;
			var ret = new AccountFilter();
			ret.AccountIDs = new  List<int>() { account.AccountID };
			if (account.MerchantID.HasValue) ret.ObjectIDs[AccountType.Merchant].Add(account.MerchantID.Value);
			if (account.CustomerID.HasValue) ret.ObjectIDs[AccountType.Customer].Add(account.CustomerID.Value);
			if (account.DebitCompanyID.HasValue) ret.ObjectIDs[AccountType.DebitCompany].Add(account.DebitCompanyID.Value);
			if (account.AffiliateID.HasValue) ret.ObjectIDs[AccountType.Affiliate].Add(account.AffiliateID.Value);
			//affiliates permissions
			if (account.MerchantID.HasValue) {
				var afl = (from s in DataContext.Reader.SetMerchantAffiliates join a in DataContext.Reader.Accounts on s.Affiliate_id equals a.Affiliate_id where s.Merchant_id == account.MerchantID select new { a.Account_id, s.Affiliate_id }).ToList();
				foreach(var n in afl)
				{
					ret.AccountIDs.Add(n.Account_id);
					ret.ObjectIDs[AccountType.Affiliate].Add(n.Affiliate_id);
				}
			}
			if (account.AffiliateID.HasValue)
			{
				var afl = (from s in DataContext.Reader.SetMerchantAffiliates join a in DataContext.Reader.Accounts on s.Merchant_id equals a.Merchant_id where s.Affiliate_id == account.AffiliateID select new { a.Account_id, s.Merchant_id }).ToList();
				foreach (var n in afl)
				{
					ret.AccountIDs.Add(n.Account_id);
					ret.ObjectIDs[AccountType.Merchant].Add(n.Merchant_id);
				}
                var idt = (from s in DataContext.Reader.tblAffiliatesToApplicationIdentities where s.Affiliates_id == account.AffiliateID select s.ApplicationIdentity_id).ToList();
                var csts = (from c in DataContext.Reader.Customers join a in DataContext.Reader.Accounts on c.Customer_id equals a.Customer_id where idt.Contains(c.ApplicationIdentity_id.GetValueOrDefault()) select new { a.Account_id, c.Customer_id }).ToList();
                foreach (var n in csts)
                {
                    ret.AccountIDs.Add(n.Account_id);
                    ret.ObjectIDs[AccountType.Customer].Add(n.Customer_id);
                }
			}
            //SQL subquery limit 2100 on subquery
            foreach (var k in ret.ObjectIDs.Keys.ToList())
                ret.ObjectIDs[k] = ret.ObjectIDs[k].Take(2001).ToList();
            ret.AccountIDs = ret.AccountIDs.Take(2001).ToList(); 

            if (account.AffiliateID.HasValue) return ret; //no reduce for partner
            return ret.Reduce();
		}

		public static AccountFilter Current 
		{
			get {
				var login = Infrastructure.Security.Login.Current;
				if (login == null) return null;
                lock (login) { 
				    var ret = login.Items.GetValue<AccountFilter>("Security.AccountFilter"); 
				    if (ret == null) {
					    ret = LoadForLogin(login);
					    login.Items.Add("Security.AccountFilter", ret);
				    }
				    return ret;
                }
			}	
		}

        public static void ClearCurrentAccountFilter()
        {
            var login = Infrastructure.Security.Login.Current;
            if (login == null) return;
            lock (login)
            {
                login.Items.Remove("Security.AccountFilter");
            }
        }
    }
}
