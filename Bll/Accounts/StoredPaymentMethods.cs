using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure.Security;
using Netpay.Infrastructure;

namespace Netpay.Bll.Accounts
{
    public interface IStoredPaymentMethodProvider
    {
        bool SupportCreate { get; }
        StoredPaymentMethod Create(Customers.Customer customer, IAddress address, System.Collections.Specialized.NameValueCollection extendedParams);
        bool SupportBalance { get; }
        List<Accounts.Balance> GetBalace(StoredPaymentMethod pm, Range<DateTime> range);
        bool SupportStatus { get; }
        StoredPaymentMethod.MethodStatus GetStatus(StoredPaymentMethod pm);
        bool SupportLoad { get; }
        Accounts.Balance Load(StoredPaymentMethod pm, decimal amount, CommonTypes.Currency currency, string refCode);
    }

    public class StoredPaymentMethod : BaseDataObject, PaymentMethods.IStoredMethod
    {
        public enum MethodStatus
        {
            Active = 0,
            Disabled = 1,
            WaitingSend = 2,
            WaitingRecieve = 3,
            WaitingActivate = 4,
        }

        public class SearchFilters
        {
            public List<int> AccountsIds;
            public string Value1Last4TextFilter;
            public Infrastructure.Range<int?> ID;
            public MethodStatus? Status;
            public string Provider;
        }

        public const string SecuredObjectName = "StoredPaymentMethod";
        public static SecuredObject SecuredObject { get { return SecuredObject.Get(Accounts.Module.Current, SecuredObjectName); } }
        public static Dictionary<string, IStoredPaymentMethodProvider> Providers = new Dictionary<string, IStoredPaymentMethodProvider>();

        protected Netpay.Dal.Netpay.AccountPaymentMethod _entity;

        public int ID { get { return _entity.AccountPaymentMethod_id; } }
        public int Account_id { get { return _entity.Account_id; } set { _entity.Account_id = value; } }
        //public int? BillingAddress_id { get { return _entity.CustomerAddress_id; } set { _entity.CustomerAddress_id = value; } }
        public string Title { get { return _entity.Title; } set { _entity.Title = value.NullIfEmpty().Truncate(30); } }

        public string OwnerName { get { return _entity.OwnerName; } set { _entity.OwnerName = value.NullIfEmpty().Truncate(80); } }
        public string OwnerSSN { get { return _entity.OwnerPersonalID; } set { _entity.OwnerPersonalID = value.NullIfEmpty().Truncate(15); } }
        public bool IsDefault { get { return _entity.IsDefault; } set { _entity.IsDefault = value; } }

        //PaymentMethods.IStoredMethod
        int PaymentMethods.IStoredMethod.PaymentMethodId { get { return _entity.PaymentMethod_id; } set { _entity.PaymentMethod_id = (short)value; } }
        System.Data.Linq.Binary PaymentMethods.IStoredMethod.Value1Encrypted { get { return _entity.Value1Encrypted; } set { _entity.Value1Encrypted = value; } }
        System.Data.Linq.Binary PaymentMethods.IStoredMethod.Value2Encrypted { get { return _entity.Value2Encrypted; } set { _entity.Value2Encrypted = value; } }
        string PaymentMethods.IStoredMethod.Value1First6Data { get { return _entity.Value1First6Text; } set { _entity.Value1First6Text = value; } }
        string PaymentMethods.IStoredMethod.Value1Last4Data { get { return _entity.Value1Last4Text; } set { _entity.Value1Last4Text = value; } }
        byte PaymentMethods.IStoredMethod.EncryptionKey { get { return _entity.EncryptionKey; } set { _entity.EncryptionKey = value; } }
        System.DateTime? PaymentMethods.IStoredMethod.ExpirationDate { get { return _entity.ExpirationDate; } set { _entity.ExpirationDate = value; } }
        public string IssuerCountryIsoCode { get { return _entity.IssuerCountryIsoCode; } set { _entity.IssuerCountryIsoCode = value; } }
        public PaymentMethods.MethodData MethodInstance { get; set; }
        public MethodStatus Status { get { return (MethodStatus)_entity.PaymentMethodStatus_id; } set { _entity.PaymentMethodStatus_id = (byte)value; } }
        public string ProviderID { get { return _entity.PaymentMethodProvider_id; } set { _entity.PaymentMethodProvider_id = value; } }
        public string ProviderReference1 { get { return _entity.ProviderReference1; } set { _entity.ProviderReference1 = value; } }

        public string PaymentMethodText { get { return _entity.PaymentMethodText; } set { _entity.PaymentMethodText = value; } }

        public AccountAddress BillingAddress { get; set; }


        protected StoredPaymentMethod(Dal.Netpay.AccountPaymentMethod entity, Dal.Netpay.AccountAddress addressEntity)
        {
            _entity = entity;
            if (addressEntity != null) BillingAddress = new AccountAddress(addressEntity);
            MethodInstance = PaymentMethods.MethodData.Create(this);
        }

        public StoredPaymentMethod()
        {
            _entity = new Dal.Netpay.AccountPaymentMethod();
            MethodInstance = PaymentMethods.MethodData.Create(this);
        }

        public static StoredPaymentMethod Load(int id)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            //User user = Account.ValidateUser(credentialsToken);
            var retExp = (from c in DataContext.Reader.AccountPaymentMethods
                          join a in DataContext.Reader.AccountAddresses on c.AccountAddress_id equals a.AccountAddress_id into na
                          from ca in na.DefaultIfEmpty()
                          where c.AccountPaymentMethod_id == id
                          where c.AccountPaymentMethod_id == id
                          select new { c, ca });

            var ret = retExp.SingleOrDefault();
            if (ret == null) return null;
            return new StoredPaymentMethod(ret.c, ret.ca);
        }

        public static List<AccountAddress> BillingAddressesForAccount(int? accountId = null)
        {
            //Not used from Admin , no need for security check
            /*
			if (customerId == null)
			{
				if (user.Type == Infrastructure.UserType.Customer) customerId = user.ID;
				else throw new ApplicationException("can't load BillingAddressesForCustomer without customerId");
			}
			*/
            //context.IsUserOfType(null);
            return (from c in DataContext.Reader.AccountPaymentMethods
                    join a in DataContext.Reader.AccountAddresses on c.AccountAddress_id equals a.AccountAddress_id
                    where c.Account_id == accountId
                    select new AccountAddress(a)).ToList();
        }

        public static List<StoredPaymentMethod> LoadForAccount(int? accountId = null)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            AccountFilter.Current.Validate(null, ref accountId);
            ObjectContext.Current.IsUserOfType(null, SecuredObject, PermissionValue.Read);
            return (from c in DataContext.Reader.AccountPaymentMethods
                    where c.Account_id == accountId
                    where c.Account_id == accountId
                    join a in DataContext.Reader.AccountAddresses on c.AccountAddress_id equals a.AccountAddress_id into na
                    from ca in na.DefaultIfEmpty()
                    select new StoredPaymentMethod(c, ca)).ToList();
        }

        public bool HasBalance
        {
            get
            {
                var provider = Provider;
                if (provider == null) return false;
                return provider.SupportBalance;
            }
        }

        public IStoredPaymentMethodProvider Provider
        {
            get
            {
                if (string.IsNullOrEmpty(ProviderID)) return null;
                IStoredPaymentMethodProvider ret;
                if (!Providers.TryGetValue(ProviderID, out ret)) return null;
                return ret;
            }
        }

        public static List<StoredPaymentMethod> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var exp = (from c in DataContext.Reader.AccountPaymentMethods
                       join a in DataContext.Reader.AccountAddresses on c.AccountAddress_id equals a.AccountAddress_id into na
                       from ca in na.DefaultIfEmpty()
                       select new { c, ca });
            if (filters != null)
            {
                if (filters.AccountsIds != null && filters.AccountsIds.Count > 0) exp = exp.Where(v => filters.AccountsIds.Contains(v.c.Account_id));
                if (filters.ID.From != null) exp = exp.Where(v => v.c.AccountPaymentMethod_id >= filters.ID.From.Value);
                if (filters.ID.To != null) exp = exp.Where(v => v.c.AccountPaymentMethod_id <= filters.ID.To.Value);
                if (filters.Provider != null) exp = exp.Where(v => v.c.PaymentMethodProvider_id == filters.Provider);
                if (filters.Status != null) exp = exp.Where(v => v.c.PaymentMethodStatus_id == (byte)filters.Status);
                if (!string.IsNullOrEmpty(filters.Value1Last4TextFilter)) exp = exp.Where(v => v.c.Value1Last4Text == filters.Value1Last4TextFilter);
            }
            return exp.ApplySortAndPage(sortAndPage).Select(v => new StoredPaymentMethod(v.c, v.ca)).ToList();
        }

        public static List<KeyValuePair<string, int>> NewLoadForAccount(int? accountId = null)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            AccountFilter.Current.Validate(null, ref accountId);
            ObjectContext.Current.IsUserOfType(null, SecuredObject, PermissionValue.Read);
            var exp = (from apm in DataContext.Reader.AccountPaymentMethods
                       where apm.Account_id == accountId
                       select new KeyValuePair<string, int>(apm.PaymentMethodText, apm.AccountPaymentMethod_id));
            var list = exp.ToList();
            if (list.Count() == 0)
            {
                var EmptyList = new List<KeyValuePair<string, int>>();
                EmptyList.Add(new KeyValuePair<string, int>("No payment method stored yet", -1));
                return EmptyList;
            }
            return list;
        }

        public override ValidationResult Validate()
        {
            if (string.IsNullOrEmpty(OwnerName))
                return ValidationResult.Invalid_OwnerName;
            //if (paymentMethod.)
            //	if (string.IsNullOrEmpty(OwnerSSN)) return ValidationResult.Invalid_SSN;
            return MethodInstance.Validate();
        }

        public void Save()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            ValidationException.RaiseIfNeeded(Validate());
            if (BillingAddress != null)
            {
                _entity.AccountAddress_id = BillingAddress.SaveAddress();
            }
            else
            {
                if (_entity.AccountAddress_id != null)
                {
                    var address = AccountAddress.Load(_entity.AccountAddress_id.Value);
                    if (address != null) address.DeleteAddress();
                }
                _entity.AccountAddress_id = null;
            }

            if (IsDefault) DataContext.Writer.ExecuteCommand(string.Format("Update Data.AccountPaymentMethod Set IsDefault=0 Where Account_id={0}", _entity.Account_id));
            DataContext.Writer.AccountPaymentMethods.Update(_entity, (_entity.AccountPaymentMethod_id != 0));
            DataContext.Writer.SubmitChanges();
        }

        public static List<StoredPaymentMethod> FindByPAN(string value1, string value2)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var ret = (from c in DataContext.Reader.AccountPaymentMethods
                       join a in DataContext.Reader.AccountAddresses on c.AccountAddress_id equals a.AccountAddress_id into na
                       from ca in na.DefaultIfEmpty()
                       select new { c, ca });
            ret = ret.Where(r => DataContext.Reader.GetDecrypted256(r.c.Value1Encrypted) == value1);
            if (value2 != null) ret = ret.Where(r => DataContext.Reader.GetDecrypted256(r.c.Value2Encrypted) == value2);
            return ret.ToList().Select(r => new StoredPaymentMethod(r.c, r.ca)).ToList();
        }

        public static bool IsCardNumberExist(string cardNumber)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return DataContext.Reader.AccountPaymentMethods.Any(x => DataContext.Reader.GetDecrypted256(x.Value1Encrypted) == cardNumber);
        }

        public void Delete()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            if (_entity.AccountAddress_id == 0) return;
            if (BillingAddress != null) BillingAddress.DeleteAddress();
            DataContext.Writer.AccountPaymentMethods.Delete(_entity);
            DataContext.Writer.SubmitChanges();
        }

        public class CreditCardDetails
        {
            public int CustomerID { get; set; }
            public string OwnerName { get; set; }
            public string CardNumber { get; set; }
        }

        public static List<CreditCardDetails> GetAccountsCreditCardDetails(List<int> accountIds)
        {
            if (accountIds.Count > 2000)
            {
                throw new Exception("Cant send more than 2000 account id's to DB.");
            }

            var exp = (from apm in DataContext.Reader.AccountPaymentMethods
                       where accountIds.Contains(apm.Account_id)
                       join accounts in DataContext.Reader.Accounts
                       on apm.Account_id equals accounts.Account_id
                       select new { PaymentMethod = apm, CustomerID = accounts.Customer_id.Value });

            var result = exp.Select(x => new CreditCardDetails()
            {
                CustomerID = x.CustomerID,
                OwnerName = x.PaymentMethod.OwnerName,
                CardNumber = Encryption.Decrypt(x.PaymentMethod.Value1Encrypted.ToArray())
            })
            .OrderByDescending(x => x.CustomerID)
            .ToList();

            return result;
        }

        public static string GetStatus(int nStoredPaymentMethodID)
        {
            try
            {
                var pSPM = StoredPaymentMethod.Load(nStoredPaymentMethodID);

                if (pSPM.MethodInstance.ExpirationDate.HasValue && pSPM.MethodInstance.ExpirationDate.Value.Date >= DateTime.Now)
                    return "Activated";
                else
                    return "Deactivated";
            }
            catch
            {
                return "N/A";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bIsActive">True to activate card. False to deactivate card</param>
        /// <param name="nStoredPaymentMethodID"></param>
        public static void ChangeCardStatus(bool bIsActive, int nStoredPaymentMethodID)
        {
            var pSPM = StoredPaymentMethod.Load(nStoredPaymentMethodID);
            if (bIsActive)
            {
                pSPM.MethodInstance.SetExpirationMonth(DateTime.Now.AddYears(1).Year, DateTime.Now.AddYears(1).Month);
            }
            else
            {
                pSPM.MethodInstance.SetExpirationMonth(DateTime.Now.AddMonths(-1).Year, DateTime.Now.AddMonths(-1).Month);
            }
            pSPM.Save();
        }
    }
}
