﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Accounts
{
	public class BankAccount : BaseDataObject
	{        
        public static SecuredObject SecuredObject { get { return StoredPaymentMethod.SecuredObject; } }
        public enum AccountStorageType { SelfAccount, BeneficiraryAccount }
		protected internal Netpay.Dal.Netpay.AccountBankAccount _entity;

		public int ID { get { return _entity.AccountBankAccount_id; } }
		public int Account_id { get { return _entity.Account_id; } set { _entity.Account_id = value; } }
		public string CurrencyISOCode { get { return _entity.CurrencyISOCode; } set { _entity.CurrencyISOCode = value; } }
		public int? RefAccountBankAccount_id { get { return _entity.RefAccountBankAccount_id; } set { _entity.RefAccountBankAccount_id = value; } }
		public AccountStorageType StorageType { get; set; }
		public bool IsDefault { get { return _entity.IsDefault; } set { _entity.IsDefault = value; } }
		public string AccountName { get { return _entity.AccountName; } set { _entity.AccountName = value; } }
		public string AccountNumber { get { return _entity.AccountNumber; } set { _entity.AccountNumber = value; } }
		public string ABA { get { return _entity.ABA; } set { _entity.ABA = value; } }
		public string IBAN { get { return _entity.IBAN; } set { _entity.IBAN = value; } }
		public string SwiftNumber { get { return _entity.SwiftNumber; } set { _entity.SwiftNumber = value; } }
		public string BankName { get { return _entity.BankName; } set { _entity.BankName = value; } }
		public string BankStreet1 { get { return _entity.BankStreet1; } set { _entity.BankStreet1 = value; } }
		public string BankStreet2 { get { return _entity.BankStreet2; } set { _entity.BankStreet2 = value; } }
		public string BankCity { get { return _entity.BankCity; } set { _entity.BankCity = value; } }
		public string BankPostalCode { get { return _entity.BankPostalCode; } set { _entity.BankPostalCode = value; } }
		public string BankStateISOCode { get { return _entity.BankStateISOCode; } set { _entity.BankStateISOCode = value; } }
		public string BankCountryISOCode { get { return _entity.BankCountryISOCode; } set { _entity.BankCountryISOCode = value; } }

		public BankAccount()
		{
			_entity = new Dal.Netpay.AccountBankAccount();
		}

		protected BankAccount(Netpay.Dal.Netpay.AccountBankAccount entity)
		{ 
			_entity = entity;
		}

        public static bool IsAccountHaveAnotherBankAccountWithCurrency(string currencyiso, int accountid, int bankaccountid)
        {
            if (currencyiso.Length != 3) throw new Exception("currencyiso value not valid.");
            if (accountid <= 0) throw new Exception("accountid value not valid.");
            if (bankaccountid < 0) throw new Exception("bankaccountid value not valid.");
              
            return DataContext.Reader.AccountBankAccounts.Any(x => x.CurrencyISOCode == currencyiso && x.Account_id == accountid && x.AccountBankAccount_id != bankaccountid);
        }

        public static List<BankAccount> LoadOldForAccount(int accountId)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var merchantId = (from m in DataContext.Reader.Accounts where m.Account_id == accountId select m.Merchant_id).SingleOrDefault();
            var oldList = (from a in DataContext.Reader.tblMerchantBankAccounts where a.mba_Merchant== merchantId select a).ToList();
            return oldList.Select(v => new BankAccount() {
                CurrencyISOCode = Currency.GetIsoCode((CommonTypes.Currency)v.mba_Currency),
                Account_id = Merchants.Merchant.Load(merchantId).AccountID,
                ABA = v.mba_ABA,
                IBAN = v.mba_IBAN,
                SwiftNumber = v.mba_SwiftNumber,
                AccountName = v.mba_AccountName,
                AccountNumber = v.mba_AccountNumber,
                BankName = v.mba_BankName,
                BankStreet1 = v.mba_BankAddress,
                BankStreet2 = v.mba_BankAddressSecond,
                BankCity = v.mba_BankAddressCity,
                BankPostalCode = v.mba_BankAddressZip,
                BankStateISOCode = v.mba_BankAddressState,
                BankCountryISOCode = Country.GetIsoCode(v.mba_BankAddressCountry),
            }).ToList();
        }

		public static BankAccount Load(int id)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var entity = (from mmas in DataContext.Reader.AccountBankAccounts where mmas.AccountBankAccount_id == id select mmas).SingleOrDefault();
			if (entity == null) return null;
			return new BankAccount(entity);
		}

		public static List<BankAccount> LoadForAccount(int accountId)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from mmas in DataContext.Reader.AccountBankAccounts where mmas.Account_id == accountId select new BankAccount(mmas)).ToList();
		}

        public static BankAccount LoadForAccount(int accountId, CommonTypes.Currency currency)
        {
            var bankAccounts = Accounts.BankAccount.LoadForAccount(accountId);
            var ret = bankAccounts.Where(ba => ba.CurrencyISOCode == Bll.Currency.Get(currency).IsoCode).FirstOrDefault();
            if (ret == null) ret = bankAccounts.Where(ba => ba.IsDefault).FirstOrDefault();
            if (ret == null) ret = bankAccounts.FirstOrDefault();
            return ret;
        }

        private BankAccount _correspondentAccount;
        public BankAccount CorrespondentAccount
        {
            get {
                if (_correspondentAccount != null) return _correspondentAccount;
                if (RefAccountBankAccount_id.HasValue) _correspondentAccount = Load(RefAccountBankAccount_id.Value);
                return _correspondentAccount;
            }
            set {
                _correspondentAccount = value;
            }
        }

        public void Save()
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            if (_correspondentAccount != null) {
                if (_correspondentAccount.Account_id == 0) _correspondentAccount.Account_id = _entity.Account_id;
                _correspondentAccount.Save();
                _entity.RefAccountBankAccount_id = _correspondentAccount.ID;
            }
            DataContext.Writer.AccountBankAccounts.Update(_entity, (_entity.AccountBankAccount_id != 0));
			DataContext.Writer.SubmitChanges();
		}

		public void Delete()
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

            if (_entity.AccountBankAccount_id == 0) return;
			DataContext.Writer.AccountBankAccounts.Delete(_entity);
			DataContext.Writer.SubmitChanges();
		}

		public override string ToString()
		{
			return string.Format("{0} {1} - {2}, {3}, {4} ", AccountName, AccountNumber, IBAN, ABA, SwiftNumber);
		}
    }
}
