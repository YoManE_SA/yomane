﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Netpay.Bll.Accounts.BalanceModule
{
    public class Module : Infrastructure.Module
    {
        public const string ModuleName = "Balance";
        public override string Name { get { return ModuleName; } }
        public override decimal Version { get { return 1.0m; } }
        public static Module Current { get { return Infrastructure.Module.Get(ModuleName) as Module; } }

        protected override void OnInstall(EventArgs e)
        {
            Infrastructure.Security.SecuredObject.Create(this, Balance.SecuredObjectName, Infrastructure.Security.PermissionGroup.ReadEditDelete);
            Infrastructure.Security.SecuredObject.Create(this, BalanceRequest.SecuredObjectName, Infrastructure.Security.PermissionGroup.ReadEditDelete);
            Balance.RegisterSources();
            base.OnInstall(e);
        }

        protected override void OnUninstall(EventArgs e)
        {
            Infrastructure.Security.SecuredObject.Remove(this);
            base.OnUninstall(e);
        }

        protected override void OnActivate(EventArgs e)
        {
            EmailTemplates.RegisterTemplates(ModuleName);

            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {
            EmailTemplates.UnRegisterTemplates(ModuleName);

            base.OnDeactivate(e);
        }
    }
}
