﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Accounts
{
    public class BalanceRequest : BaseDataObject
    {
        public const string SecuredObjectName = "BalanceRequest";
        public static SecuredObject SecuredObject { get { return SecuredObject.Get(BalanceModule.Module.Current, SecuredObjectName); } }

        public class SearchFilters
        {
            public int? SourceAccountID;
            public int? TargetAccountID;
            public int? AnyAccountID;
            public string CurrencyISOCode;
            public bool? pendingOnly;
        }


        public class BalanceRequestEmailData
        {
            public Domain Domain;
            public BalanceRequest Request;
            public Accounts.Account SourceAccount;
            public Accounts.Account TargetAccount;
        }

        private Dal.Netpay.AccountBalanceMoneyRequest _entity;

        public int ID { get { return _entity.AccountBalanceMoneyRequest_id; } }
        public System.DateTime RequestDate { get { return _entity.RequestDate; } }
        public int SourceAccountID { get { return _entity.SourceAccount_id; } }
        public int TargetAccountID { get { return _entity.TargetAccount_id; } }
        public string SourceText { get { return _entity.SourceText; } }
        public string TargetText { get { return _entity.TargetText; } }

        public bool IsPush { get { return _entity.IsPush; } }
        public decimal Amount { get { return _entity.Amount; } }
        public string CurrencyISOCode { get { return _entity.CurrencyISOCode; } }

        public bool? IsApproved { get { return _entity.IsApproved; } }
        public System.DateTime? ConfirmDate { get { return _entity.ConfirmDate; } }
        public int? SourceBalanceID { get { return _entity.SourceAccountBalance_id; } }
        public int? TargetBalanceID { get { return _entity.TargetAccountBalance_id; } }

        public string FormattedAmount { get { return Amount.ToAmountFormat(CurrencyISOCode); } } 

        private BalanceRequest(Netpay.Dal.Netpay.AccountBalanceMoneyRequest entity)
        {
            _entity = entity;
        }

        public BalanceRequest(int srcAccountId, int destAccountId, decimal amount, string currencyIso, string text, bool push = false)
        {
            _entity = new Dal.Netpay.AccountBalanceMoneyRequest();
            _entity.RequestDate = DateTime.Now;
            _entity.SourceAccount_id = srcAccountId;
            _entity.TargetAccount_id = destAccountId;
            _entity.CurrencyISOCode = currencyIso;
            _entity.Amount = -amount;
            _entity.SourceText = text;

            var names = Account.GetNames(new List<int> { srcAccountId, destAccountId });
            var srcName = names.Where(a => a.Key == srcAccountId).Select(a => a.Value).SingleOrDefault();
            var destName = names.Where(a => a.Key == destAccountId).Select(a => a.Value).SingleOrDefault();

            if (_entity.IsPush = push) {
                _entity.ConfirmDate = DateTime.Now;
                _entity.IsApproved = true;
                _entity.Amount = -_entity.Amount;
                _entity.SourceText = string.Format("Transfer To {0}", destName);
                _entity.TargetText = string.Format("Transfer From {0}", srcName);
            } else {
                _entity.SourceText = string.Format("Transfer From {0}", destName);
                _entity.TargetText = string.Format("Transfer To {0}", srcName);
            }
        }

        public void Reply(bool approved, string text)
        {
            _entity.ConfirmDate = DateTime.Now;
            _entity.IsApproved = approved;
            _entity.TargetText = text;
        }

        public BalanceRequestEmailData GetEmailData()
        {
            var ids = new List<int>() { _entity.SourceAccount_id, _entity.TargetAccount_id };
            List<Account> accounts = null;
            ObjectContext.Current.Impersonate(Domain.ServiceCredentials);
            try { accounts = Account.LoadAccount(ids); }
            finally { ObjectContext.Current.StopImpersonate(); }
            var ret = new BalanceRequestEmailData() { Domain = Infrastructure.Domain.Current, Request = this };
            ret.SourceAccount = accounts.Where(v => v.AccountID == _entity.SourceAccount_id).SingleOrDefault();
            ret.TargetAccount = accounts.Where(v => v.AccountID == _entity.TargetAccount_id).SingleOrDefault();
            return ret;
        }

        public void SendNotifications(object stateInfo)
        {
            if (stateInfo is Domain) Domain.Current = stateInfo as Domain;
            var dataObj = GetEmailData();
            if (!IsPush) {
                if (!IsApproved.GetValueOrDefault()) {
                    if (dataObj.TargetAccount != null) {
                        dataObj.TargetAccount.SendAccountEmail("Balance_Request.htm", dataObj);
                    }
                } else if (dataObj.SourceAccount != null) {
                    dataObj.SourceAccount.SendAccountEmail("Balance_RequestConfirmed.htm", dataObj);
                }
            } else if (dataObj.TargetAccount != null) {
                dataObj.TargetAccount.SendAccountEmail("Balance_RequestConfirmed.htm", dataObj);
            }
        }

        public void Save(bool sendNotifications = true)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            if (_entity.IsApproved.GetValueOrDefault(false)) {
                if (_entity.AccountBalanceMoneyRequest_id == 0)
                {
                    DataContext.Writer.AccountBalanceMoneyRequests.Update(_entity, (_entity.AccountBalanceMoneyRequest_id != 0));
                    DataContext.Writer.SubmitChanges();
                }
                if (SourceBalanceID == null) _entity.SourceAccountBalance_id = Balance.Create(SourceAccountID, CurrencyISOCode, -Amount, SourceText, Balance.SOURCE_TRANSFER, ID, false).ID;
                if (TargetBalanceID == null) _entity.TargetAccountBalance_id = Balance.Create(TargetAccountID, CurrencyISOCode, Amount, SourceText, Balance.SOURCE_TRANSFER, ID, false).ID;
            }
            DataContext.Writer.AccountBalanceMoneyRequests.Update(_entity, (_entity.AccountBalanceMoneyRequest_id != 0));
            DataContext.Writer.SubmitChanges();
            try { System.Threading.ThreadPool.QueueUserWorkItem(SendNotifications, Domain); }
            catch (Exception ex) { Logger.Log(ex); }
        }

        public static BalanceRequest Load(int id)
        {
            return Search(new SearchFilters { AnyAccountID = Account.Current.AccountID }, null).Where(x => x.ID == id).SingleOrDefault();
        }

        public static List<BalanceRequest> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
                        
            var exp = (from s in DataContext.Reader.AccountBalanceMoneyRequests select s);
            if (filters != null) {
                if (filters.SourceAccountID.HasValue) exp = exp.Where(s => s.TargetAccount_id == filters.SourceAccountID);
                if (filters.TargetAccountID.HasValue) exp = exp.Where(s => s.TargetAccount_id == filters.TargetAccountID);
                if (filters.AnyAccountID.HasValue) exp = exp.Where(s=> s.SourceAccount_id == filters.AnyAccountID || s.TargetAccount_id == filters.AnyAccountID);
                if (!string.IsNullOrEmpty(filters.CurrencyISOCode)) exp = exp.Where(s => s.CurrencyISOCode == filters.CurrencyISOCode);
                if (filters.pendingOnly != null) {
                    if (filters.pendingOnly.Value) exp = exp.Where(s => s.IsApproved == null);
                    else exp = exp.Where(s => s.IsApproved != null);
                }
            }
            return exp.ApplySortAndPage(sortAndPage).Select(s => new BalanceRequest(s)).ToList();
        }

        public static object GetEmailTestData(Infrastructure.Email.Template template, string param)
        {
            var relId = param.ToNullableInt();
            var exp = (from r in DataContext.Reader.AccountBalanceMoneyRequests select r);
            if (relId.HasValue) exp = exp.Where(r => r.AccountBalanceMoneyRequest_id == relId.Value);
            var rel = exp.Select(r => new BalanceRequest(r)).FirstOrDefault();
            if (rel == null) throw new Exception("Test data wasn't found, you must have at least one balance request.");
            return rel.GetEmailData();
        }
    }
}
