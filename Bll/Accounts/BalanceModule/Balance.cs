using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.CommonTypes;
using Netpay.Dal.DataAccess;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Accounts
{
    public class Balance : BaseDataObject
    {
        //general types
        public const string SOURCE_CAPTURE = "System.PedingCapture"; //refid is original pending
        public const string SOURCE_ADMIN = "System.Admin"; //refid is null
        public const string SOURCE_DEPOSIT = "System.Deposit"; //general deposit, refid is null
        public const string SOURCE_DEPOSIT_FEE = "System.DepositFee"; //refid is deposit row
        public const string SOURCE_WITHDRAWAL = "System.Withdrawal"; //general withdrawal, refid is null
        public const string SOURCE_WITHDRAWAL_FEE = "System.WithdrawalFee"; //refid is withdrawal row
        public const string SOURCE_PURCHASE = "System.Purchase"; //refid is transPass
        public const string SOURCE_TRANSFER = "System.Transfer"; //refid is balanceRequest
        public const string SOURCE_TRANSFER_FEE = "System.TransferFee"; //refid is transfer row

        public class BalanceSource
        {
            private Dal.Netpay.BalanceSourceType _entity;
            public string ID { get { return _entity.BalanceSourceType_id; } }
            public string Text { get { return _entity.Name; } }
            public bool IsFee { get { return _entity.IsFee; } }
            private BalanceSource(Dal.Netpay.BalanceSourceType entity) { _entity = entity; }
            public BalanceSource(string id, string name, bool isFee)
            {
                _entity = new Dal.Netpay.BalanceSourceType() { BalanceSourceType_id = id, Name = name, IsFee = isFee };
            }
            public static List<BalanceSource> Items
            {
                get
                {
                    return Domain.Current.GetCachData("Balance.Sources", () =>
                    {
                        //Causes stackoverflow
                        //ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Read);

                        return new Domain.CachData(DataContext.Reader.BalanceSourceTypes.Select(v => new BalanceSource(v)).ToList(), DateTime.Now.AddHours(2));
                    }) as List<BalanceSource>;
                }
            }
            public static string GetText(string id)
            {
                var ret = Items.Where(v => v.ID == id).SingleOrDefault();
                if (ret == null) return null;
                return ret.Text;
            }

            public static void Register(BalanceSource source)
            {
                //Causes stackoverflow.
                //ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Merchant }, SecuredObject, PermissionValue.Add);

                if (Items.Any(v => v.ID == source.ID)) return;
                DataContext.Writer.BalanceSourceTypes.Update(source._entity, false);
                DataContext.Writer.SubmitChanges();
                Items.Add(source);
            }

            public static bool Unregister(string id)
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

                var source = Items.Where(v => v.ID == id).FirstOrDefault();
                if (source == null) return true;
                Items.Remove(source);
                try
                {
                    DataContext.Writer.BalanceSourceTypes.Delete(source._entity);
                    DataContext.Writer.SubmitChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }
        internal static void RegisterSources()
        {
            BalanceSource.Register(new BalanceSource(SOURCE_CAPTURE, "Admin", false));
            BalanceSource.Register(new BalanceSource(SOURCE_ADMIN, "Admin", false));
            BalanceSource.Register(new BalanceSource(SOURCE_DEPOSIT, "Deposit", false));
            BalanceSource.Register(new BalanceSource(SOURCE_DEPOSIT_FEE, "Deposit Fee", true));
            BalanceSource.Register(new BalanceSource(SOURCE_WITHDRAWAL, "Withdrawal", false));
            BalanceSource.Register(new BalanceSource(SOURCE_WITHDRAWAL_FEE, "Withdrawal Fee", true));
            BalanceSource.Register(new BalanceSource(SOURCE_PURCHASE, "Purchase", false));
            BalanceSource.Register(new BalanceSource(SOURCE_TRANSFER, "Transfer", false));
            BalanceSource.Register(new BalanceSource(SOURCE_TRANSFER_FEE, "Transfer Fee", true));
        }

        /*
        System.UserTr
        System.UserTrFee
        System.WL
        System.WalletLoad
        System.Wire
        System.WireFee
        public enum BalanceSource {
            PayedTransactions = 1, //Wires.Settlement
            Merchant = 2, //Wires.PaymentRequest
            Admin = 3, //System.Admin
            Fee = 4, //Wires.SettlementFee
            //CreditCardDeposit = 5, - NOT IS USE
            //PaymentForPurchase = 6, - NOT IS USE
            WalletTransferToMerchant = 7, //System.Transfer
            FeeWalletTransfer = 8, //System.Transfer-Fee
            //EBankingDeposit = 9,  - NOT IS USE
            //PayoneerTransaction = 10, - NOT IS USE
            //PayoneerFee = 11,  - NOT IS USE
            UserTr = 12, //System.Transfer - NEW
        }
        //oldCode OldBalanceStatus { Cleared = 0, Processing = 1, Void = 2 }
        */

        public class SearchFilters
		{
			public Range<int?> ID;
			public Range<DateTime?> InsertDate;
			public Range<decimal?> Amount;
			public Range<decimal?> Balance;
			public int? AccountID {
                get { if (AccountIDs != null && AccountIDs.Count > 0) return AccountIDs[0]; return null; }
                set { AccountIDs = value.HasValue ? new List<int>() { value.Value } : null; }
            }
            public List<int> AccountIDs;
            public string CurrencyIso;
			public string SourceType;
            public int? SourceID;
            public string Text;
            public bool? IsPending;
            public bool OnlyCurrentValues;
            public bool OnlyNotProcessedPendings;
            public AccountType? AccountType;
        }

        public class BalanceStatus
        {
            public int AccountId { get; set; }
            public string CurrencyIso { get; set; }
            public DateTime LastAction { get; set; }
            public decimal Current { get; set; }
            public decimal Pending { get; set; }
            public decimal Expected { get { return Current + Pending; } }

            public class SearchFilters : Balance.SearchFilters
            {

            }
           
            public static List<BalanceStatus> Search(SearchFilters filters, SortAndPage sortAndPage)
            {
                return GetStatus(filters, sortAndPage);
            }
        }

        private Dal.Netpay.AccountBalance _entity;

        public int ID { get { return _entity.AccountBalance_id; } }
        public int AccountID { get { return _entity.Account_id; } }
        public string SourceType { get { return _entity.BalanceSourceType_id; } }
        public int? SourceID { get { return _entity.SourceID; } }
        public string CurrencyIso { get { return _entity.CurrencyISOCode; } }
        public decimal Amount { get { return _entity.Amount; } }
        public decimal Total { get { return _entity.TotalBalance; } }
        public System.DateTime InsertDate { get { return _entity.InsertDate; } }
        public string Text { get { return _entity.SystemText; } }
        public bool IsPending { get { return _entity.IsPending; } }

        public const string SecuredObjectName = "Balance";
        public static SecuredObject SecuredObject { get { return SecuredObject.Get(BalanceModule.Module.Current, SecuredObjectName); } }

        public Balance(Netpay.Dal.Netpay.AccountBalance entity) { _entity = entity; }

        /*
		 * amount = minus to credit, plus to debit
		*/
        public static Balance Create(int accountID, string currencyIso, decimal amount, string text, string sourceType, int? sourceId, bool isPending)
        {

            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            var currentTotal = (from b in DataContext.Reader.AccountBalances where b.Account_id == accountID && b.CurrencyISOCode == currencyIso orderby b.AccountBalance_id descending select b.TotalBalance).FirstOrDefault();
            var requestedTotal = currentTotal + amount;
            if (requestedTotal < 0)
                throw new Exception("Insufficient funds");


            var entity = new Dal.Netpay.AccountBalance();
            entity.Account_id = accountID;
            entity.Amount = amount;
            entity.SystemText = text.Truncate(200);
            entity.InsertDate = DateTime.Now;
            entity.CurrencyISOCode = currencyIso;
            entity.SourceID = sourceId.GetValueOrDefault();
            entity.BalanceSourceType_id = sourceType;
            entity.IsPending = isPending;

            using (var dataContext = new Infrastructure.DataContext(true))
            {
                dataContext.AccountBalances.InsertOnSubmit(entity);
                dataContext.SubmitChanges();
            }

            //If the Balance is from type System.Deposit - Send Notification email
            if (sourceType == SOURCE_DEPOSIT)
            {
                var Account = Bll.Accounts.Account.LoadAccount(accountID);
                Account.SendAccountEmail("Balance_RequestConfirmed.htm");
            }
            //no need, done by trigger
            //dataContext.ExecuteCommand("Update Data.AccountBalance Set TotalBalance = Amount + IsNull((Select Top 1 b1.TotalBalance From Data.AccountBalance b1 Where b1.Account_id={0} And b1.CurrencyISOCode={1} And b1.AccountBalance_id < {2} And b1.IsPending={3} Order By b1.AccountBalance_id Desc), 0) Where AccountBalance_id={2}", accountID, currencyIso, entity.AccountBalance_id, entity.IsPending);
            return new Balance(entity);
        }

        public static List<Balance> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Account, UserRole.Admin }, SecuredObject, PermissionValue.Read);

            if (filters == null) filters = new SearchFilters();
            if (ObjectContext.Current.User.IsInRole(UserRole.Account)) filters.AccountIDs = AccountFilter.Current.FilterIDList(null, filters.AccountIDs);
            if (sortAndPage == null) sortAndPage = new SortAndPage(0, 0);
            if (string.IsNullOrEmpty(sortAndPage.SortKey)) { sortAndPage.SortKey = "InsertDate"; sortAndPage.SortDesc = true; }
            var exp = (from b in DataContext.Reader.AccountBalances.Where(AccountFilter.Current.QueryValidate(null, (Dal.Netpay.AccountBalance x) => x.Account_id)) select b);
            if (filters != null)
            {
                if (filters.ID.From.HasValue) exp = exp.Where(t => t.AccountBalance_id >= filters.ID.From);
                if (filters.ID.To.HasValue) exp = exp.Where(t => t.AccountBalance_id <= filters.ID.To);
                if (filters.InsertDate.From.HasValue) exp = exp.Where(t => t.InsertDate >= filters.InsertDate.From);
                if (filters.InsertDate.To.HasValue) exp = exp.Where(t => t.InsertDate <= filters.InsertDate.To);
                if (filters.Amount.From.HasValue) exp = exp.Where(t => t.Amount >= filters.Amount.From);
                if (filters.Amount.To.HasValue) exp = exp.Where(t => t.Amount <= filters.Amount.To);
                if (filters.Balance.From.HasValue) exp = exp.Where(t => t.TotalBalance >= filters.Balance.From);
                if (filters.Balance.To.HasValue) exp = exp.Where(t => t.TotalBalance <= filters.Balance.To);
                if (filters.AccountIDs != null) exp = exp.Where(t => filters.AccountIDs.Contains(t.Account_id));
                if (!string.IsNullOrEmpty(filters.CurrencyIso)) exp = exp.Where(t => t.CurrencyISOCode == filters.CurrencyIso);
                if (!string.IsNullOrEmpty(filters.SourceType)) exp = exp.Where(t => t.BalanceSourceType_id == filters.SourceType);
                if (filters.SourceID != null) exp = exp.Where(t => t.SourceID == filters.SourceID.Value);
                if (filters.IsPending.HasValue) exp = exp.Where(t => t.IsPending == filters.IsPending);
                if (filters.Text != null) exp = exp.Where(t => t.SystemText == filters.Text);
                if (filters.AccountType.HasValue) exp = exp.Where(t => t.data_Account.AccountType_id == (byte)filters.AccountType);
                if (filters.OnlyNotProcessedPendings)
                    exp = exp.Where(t => t.IsPending && (t.BalanceSourceType_id != SOURCE_CAPTURE) && !(from n in DataContext.Reader.AccountBalances where n.BalanceSourceType_id == SOURCE_CAPTURE && n.SourceID == t.AccountBalance_id select n.AccountBalance_id).Any());
                if (filters.OnlyCurrentValues)
                    exp = exp.Where(t => (from n in DataContext.Reader.AccountBalances group n by new { n.Account_id, n.CurrencyISOCode, n.IsPending } into ng select ng.Max(n => n.AccountBalance_id)).Contains(t.AccountBalance_id));
            }
            return exp.ApplySortAndPage(sortAndPage).ToList().Select(b => new Balance(b)).ToList();
        }

        public static List<BalanceStatus> GetStatus(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var ret = new Dictionary<int, List<BalanceStatus>>();
            if (filters == null) filters = new SearchFilters();
            filters.OnlyCurrentValues = true;
            filters.IsPending = null;
            var balances = Search(filters, sortAndPage);
            foreach (var v in balances)
            {
                List<BalanceStatus> curAcc;
                if (!ret.TryGetValue(v.AccountID, out curAcc)) ret.Add(v.AccountID, curAcc = new List<BalanceStatus>());
                BalanceStatus curAccCur = curAcc.Where(a => a.CurrencyIso == v.CurrencyIso).SingleOrDefault();
                if (curAccCur == null) curAcc.Add(curAccCur = new BalanceStatus() { AccountId = v.AccountID, CurrencyIso = v.CurrencyIso });
                if (v.IsPending) curAccCur.Pending = v.Total;
                else curAccCur.Current += v.Total;
                if (v.InsertDate > curAccCur.LastAction) curAccCur.LastAction = v.InsertDate;
            }
            return ret.SelectMany(v => v.Value).ToList();
        }

        public static Dictionary<int, List<BalanceStatus>> GetStatus(List<int> accountIDs, ISortAndPage sortAndPage)
        {
            return GetStatus(new SearchFilters() { AccountIDs = accountIDs }, sortAndPage).GroupBy(v => v.AccountId).ToDictionary(v => v.Key, v => v.ToList());
        }

        public static List<BalanceStatus> GetStatusMultipleAccountsOneCurrency(List<int> accountIds, string currencyIso, ISortAndPage sortAndPage)
        {
            return GetStatus(new SearchFilters() { AccountIDs = accountIds, CurrencyIso = currencyIso }, sortAndPage);
        }

        public static List<BalanceStatus> GetStatus(int accountID)
        {
            return GetStatus(new SearchFilters() { AccountID = accountID.ToNullableInt() }, null);
        }

        public static BalanceStatus GetStatus(int accountID, string currencyIso)
        {
            var ret = GetStatus(new SearchFilters() { AccountID = accountID, CurrencyIso = currencyIso }, null).SingleOrDefault();
            if (ret == null) ret = new BalanceStatus() { AccountId = accountID, CurrencyIso = currencyIso };
            return ret;
        }

        public static string GetTotalBalanceForMultipleAccountsSpecificCurrency(List<int> accountids, string currencyIso)
        {
            if (accountids.Count == 0) return "0";

            decimal sum = 0;
            foreach (int id in accountids)
            {
                sum += GetStatus(id, currencyIso).Current;
            }
            return sum.ToAmountFormat();
        }

        public void CapturePending()
        {
            if (!IsPending) throw new Exception("balnace can only capture pending rows");
            Create(AccountID, CurrencyIso, Amount, Text, SourceType, SourceID, false);
            Create(AccountID, CurrencyIso, -Amount, Text, SOURCE_CAPTURE, ID, IsPending);
        }

        public Balance Reverese(string text = null)
        {
            if (text == null) text = Text;
            return Create(AccountID, CurrencyIso, -Amount, text, SourceType, SourceID, IsPending);
        }

        public static decimal GetTotalBalanceOfAffiliateCustomersByCurrency(List<int> appidentitiesids, string currency)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            if (string.IsNullOrEmpty(currency)) return 0;
            if (appidentitiesids.Count == 0) return 0;

            //Convert the int list into string in order to insert it to the sql query.
            string appIdentityForSql = String.Join(",", appidentitiesids);

            //The query that will return the total balance of a specific currency for all the customers that are issued with 
            //The affiliate/partner that was signed in - detected by the app identity. the partner and all the customers that
            string sql = "SELECT SUM(TotalBalance) FROM Data.AccountBalance Where AccountBalance_id IN " +
                         "(SELECT Max(AccountBalance_id) FROM Data.AccountBalance " +
                         "Where IsPending=0 And CurrencyISOCode=" + "'" + currency + "'" + "AND Account_id IN (SELECT Account_id From Data.Account Where Customer_id IN(SELECT Customer_id FROM Data.Customer Where ApplicationIdentity_id IN(" + appIdentityForSql + ")))" +
                         "Group By Account_id)";

            //Invoke the query , if there is no balance with the currency , the value is null , and the return value will be 0.
            decimal? result = DataContext.Reader.ExecuteQuery<decimal?>(sql).SingleOrDefault();

            if (result.HasValue)
            {
                return result.Value;
            }
            else
            {
                return 0;
            };
        }
    }
}
