﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Accounts
{
	public class AccountAddress : BaseDataObject, IAddress
	{
		protected Netpay.Dal.Netpay.AccountAddress _addressEntity;

		public int? AddressID { get { return (_addressEntity.AccountAddress_id == 0) ? null : (int?)_addressEntity.AccountAddress_id; } }
		public string AddressLine1 { get { return _addressEntity.Street1; } set { _addressEntity.Street1 = value.NullIfEmpty().TruncEnd(100); } }
		public string AddressLine2 { get { return _addressEntity.Street2; } set { _addressEntity.Street2 = value.NullIfEmpty().TruncEnd(100); } }
		public string City { get { return _addressEntity.City; } set { _addressEntity.City = value.NullIfEmpty().TruncEnd(60); } }
		public string PostalCode { get { return _addressEntity.PostalCode; } set { _addressEntity.PostalCode = value.NullIfEmpty().TruncEnd(15); } }
		public string StateISOCode { get { return _addressEntity.StateISOCode; } set { _addressEntity.StateISOCode = value.NullIfEmpty().TruncEnd(2); } }
		public string CountryISOCode { get { return _addressEntity.CountryISOCode; } set { _addressEntity.CountryISOCode = value.NullIfEmpty().TruncEnd(2); } }

		internal AccountAddress(Dal.Netpay.AccountAddress entity)
		{ 
			_addressEntity = entity;
			if (_addressEntity == null) _addressEntity = new Dal.Netpay.AccountAddress();
		}

		public AccountAddress()
		{
			_addressEntity = new Dal.Netpay.AccountAddress();
		}

		internal static AccountAddress Load(int id)
		{
			var ret = (from c in DataContext.Reader.AccountAddresses where c.AccountAddress_id == id select c).SingleOrDefault();
			if (ret == null) return null;
			return new AccountAddress(ret);
		}

		public override ValidationResult Validate()
		{
			if (CountryISOCode != null && Country.Get(CountryISOCode) == null) return ValidationResult.Invalid_Country;
			if (StateISOCode != null && State.Get(StateISOCode) == null) return ValidationResult.Invalid_State;
			return ValidationResult.Success;
		}

		public int? SaveAddress()
		{
			ValidationException.RaiseIfNeeded(Validate());
			if (string.IsNullOrEmpty(_addressEntity.CountryISOCode)) return null;
			DataContext.Writer.AccountAddresses.Update(_addressEntity, (_addressEntity.AccountAddress_id != 0));
			DataContext.Writer.SubmitChanges();
			return _addressEntity.AccountAddress_id;
		}

		public void DeleteAddress()
		{
			if (_addressEntity.AccountAddress_id == 0) return;
			DataContext.Writer.AccountAddresses.Delete(_addressEntity);
		}
	}
}
