﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Bll.Customers;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Accounts
{
    public class AccountInfo
    {
        public int AccountID { get; set; }
        public int TargetID { get; set; }
        public AccountType AccountType { get; set; }
        public string AccountName { get; set; }
        public string AccountStatus { get; set; }
        public string AccountNumber { get; set; }
        public System.Drawing.Color StatusColor { get; set; }
    }

    public class Account : BaseDataObject
    {
        public const string PinCodeSecuredObjectName = "PinCode";
        public static Infrastructure.Security.SecuredObject PinCodeSecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Accounts.Module.Current, PinCodeSecuredObjectName); } }
        private const string PINCODE_SECRET = "PNC:";

        protected Netpay.Dal.Netpay.Account _accountEntity;

        public int AccountID { get { return _accountEntity.Account_id; } }

        public string AccountNumber { get { return _accountEntity.AccountNumber; } }
        public string AccountName { get { return _accountEntity.Name; } set { _accountEntity.Name = value; } }
        public AccountType AccountType { get { return (AccountType)_accountEntity.AccountType_id; } }

        public string PreferredWireProvider { get { return _accountEntity.PreferredWireProvider_id; } set { _accountEntity.PreferredWireProvider_id = value; } }
        public int? MerchantID { get { return _accountEntity.Merchant_id; } }
        public int? CustomerID { get { return _accountEntity.Customer_id; } }
        public int? AffiliateID { get { return _accountEntity.Affiliate_id; } }
        public int? DebitCompanyID { get { return _accountEntity.DebitCompany_id; } }

        public virtual string HashKey { get { return _accountEntity.HashKey; } set { _accountEntity.HashKey = value; } }
        public int? LoginID { get { return _accountEntity.LoginAccount_id; } }

        public AccountAddress PersonalAddress { get; set; }
        public AccountAddress BusinessAddress { get; set; }

        private string _newPinCode;

        protected class AccountLoadCombined
        {
            public Dal.Netpay.Account account;
            public Dal.Netpay.AccountAddress personalAddress;
            public Dal.Netpay.AccountAddress businessAddress;
        }

        public const string AccountSecuredObjectName = "Manage";
        public static SecuredObject AccountSecuredObject { get { return SecuredObject.Get(Accounts.Module.Current, AccountSecuredObjectName); } }

        protected Account(AccountType accountType) { _accountEntity = new Dal.Netpay.Account(); _accountEntity.AccountType_id = (byte)accountType; }

        public readonly static Account Empty = new Account(AccountType.Merchant);

        protected Account(AccountLoadCombined entities)
        {
            _accountEntity = entities.account;
            PersonalAddress = new AccountAddress(entities.personalAddress);
            BusinessAddress = new AccountAddress(entities.businessAddress);
        }

        protected static IQueryable<AccountLoadCombined> LoadQuery(Netpay.Dal.DataAccess.NetpayDataContext context, bool withAccountFilter = true)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, AccountSecuredObject, PermissionValue.Read);

            var src = withAccountFilter ? context.Accounts.Where(AccountFilter.Current.QueryValidate(null, (Dal.Netpay.Account z) => z.Account_id)) : context.Accounts;
            return (from a in src
                    join ap in context.AccountAddresses on a.PersonalAddress_id equals ap.AccountAddress_id into pa
                    from paa in pa.DefaultIfEmpty()
                    join ab in context.AccountAddresses on a.BusinessAddress_id equals ab.AccountAddress_id into ba
                    from baa in ba.DefaultIfEmpty()
                    select new AccountLoadCombined() { account = a, personalAddress = paa, businessAddress = baa });
        }

        public string ProfileImageFileName { get { return Bll.Accounts.Account.MapPrivatePath(AccountID, "profileImage.png"); } }
        public byte[] ProfileImageData
        {
            get
            {
                if (!System.IO.File.Exists(ProfileImageFileName)) return null;
                return System.IO.File.ReadAllBytes(ProfileImageFileName);
            }
            set
            {
                if (value.Length > 0x80000) throw new ApplicationException("Image too big"); //more then 196,608
                if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(ProfileImageFileName)))
                    System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(ProfileImageFileName));
                if (System.IO.File.Exists(ProfileImageFileName)) System.IO.File.Delete(ProfileImageFileName);
                using (var ms = new System.IO.MemoryStream(value))
                    System.Drawing.Image.FromStream(ms).Save(ProfileImageFileName, System.Drawing.Imaging.ImageFormat.Png);
            }
        }

        public string ProfileImageBase64
        {
            get
            {
                var ret = ProfileImageData;
                if (ret == null) return null;
                return System.Convert.ToBase64String(ret);
            }
        }

        public static Account LoadObject(int id)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, AccountSecuredObject, PermissionValue.Read);

            AccountFilter.Current.Validate(null, id);
            var ret = (from d in DataContext.Reader.Accounts where d.Account_id == id select d).SingleOrDefault();

            if (ret == null) return null;
            return LoadObject(ret);
        }

        private static Account LoadObject(Dal.Netpay.Account account)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, AccountSecuredObject, PermissionValue.Read);

            switch (((AccountType?)account.AccountType_id).Value)
            {
                case AccountType.Customer:
                    return Customers.Customer.Load(account.Customer_id);
                case AccountType.Merchant:
                    return Merchants.Merchant.Load(account.Merchant_id);
                case AccountType.Affiliate:
                    return Affiliates.Affiliate.Load(account.Affiliate_id);
                case AccountType.DebitCompany:
                    return DebitCompanies.DebitCompany.Load(new List<int> { account.DebitCompany_id.Value }).SingleOrDefault();
            }
            return null;
        }

        protected string GenerateNumber()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                if (AccountID == 0)
                {
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, AccountSecuredObject, PermissionValue.Add);
                }
                else
                {
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, AccountSecuredObject, PermissionValue.Edit);
                }

            if (!string.IsNullOrEmpty(_accountEntity.AccountNumber)) return _accountEntity.AccountNumber;
            string ret = null;
            do { ret = (new Random()).Next(1000000, 9999999).ToString(); }
            while ((from c in DataContext.Reader.Accounts where c.AccountNumber == ret select c.Account_id).Any());
            _accountEntity.AccountNumber = ret;
            return ret;
        }

        public static Dictionary<int, string> GetNames(List<int> ids, AccountType? idType = null)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, AccountSecuredObject, PermissionValue.Read);
            //ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Account, UserRole.Admin });
            if (idType == null)
                return DataContext.Reader.Accounts.Where(v => ids.Contains(v.Account_id)).Select(a => new { id = a.Account_id, a.Name }).ToDictionary(v => v.id, v => v.Name);
            switch (idType.Value)
            {
                case AccountType.Merchant:
                    return DataContext.Reader.Accounts.Where(v => ids.Contains(v.Merchant_id.Value)).Select(a => new { id = a.Merchant_id.Value, a.Name }).ToDictionary(v => v.id, v => v.Name);
                case AccountType.Customer:
                    return DataContext.Reader.Accounts.Where(v => ids.Contains(v.Customer_id.Value)).Select(a => new { id = a.Customer_id.Value, a.Name }).ToDictionary(v => v.id, v => v.Name);
                case AccountType.Affiliate:
                    return DataContext.Reader.Accounts.Where(v => ids.Contains(v.Affiliate_id.Value)).Select(a => new { id = a.Affiliate_id.Value, a.Name }).ToDictionary(v => v.id, v => v.Name);
                case AccountType.DebitCompany:
                    return DataContext.Reader.Accounts.Where(v => ids.Contains(v.DebitCompany_id.Value)).Select(a => new { id = a.DebitCompany_id.Value, a.Name }).ToDictionary(v => v.id, v => v.Name);
            }
            return null;
        }

        public static string GetName(int id, AccountType? idType = null)
        {
            return GetNames(new List<int>() { id }, idType).SingleOrDefault().Value;
        }

        public static Dictionary<int, string> GetMerchantNamesByMerchantId(List<int> ids)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, AccountSecuredObject, PermissionValue.Read);

            return (from a in DataContext.Reader.Accounts where ids.Contains(a.Merchant_id.Value) select new { a.Merchant_id.Value, a.Name }).ToDictionary(v => v.Value, v => v.Name);
        }

        public static Dictionary<int, int?> GetMerchantsGroup(List<int> merchantIds)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Account, UserRole.Admin }, AccountSecuredObject, PermissionValue.Read);

            return (from a in DataContext.Reader.tblCompanies where merchantIds.Contains(a.ID) select new { a.ID, a.GroupID }).ToDictionary(v => v.ID, v => v.GroupID);
        }


        public static Dictionary<int, string> GetMerchantNames(List<int> merchantIds)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Account, UserRole.Admin }, AccountSecuredObject, PermissionValue.Read);

            return (from a in DataContext.Reader.Accounts where a.Merchant_id != null && merchantIds.Contains((int)a.Merchant_id) select new { a.Merchant_id, a.Name }).ToDictionary(v => (int)v.Merchant_id, v => v.Name);
        }

        public static Dictionary<string, string> GetMerchantNamesByAccountNumbers(List<string> accountNumbers)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Account, UserRole.Admin }, AccountSecuredObject, PermissionValue.Read);

            return (from a in DataContext.Reader.Accounts where a.AccountNumber != null && accountNumbers.Contains(a.AccountNumber) select new { a.AccountNumber, a.Name }).ToDictionary(v => v.AccountNumber, v => v.Name);
        }

        public static string GetMerchantNameByMerchantId(int merchantid)
        {
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Account, UserRole.Admin }, AccountSecuredObject, PermissionValue.Read);

            return (from a in DataContext.Reader.Accounts where a.Merchant_id.Value == merchantid select a.Name).SingleOrDefault();
        }


        public static Dictionary<int, AccountInfo> GetAccountInfo(List<int> ids)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, AccountSecuredObject, PermissionValue.Read);
            //ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Account, UserRole.Admin });
            return (from a in DataContext.Reader.Accounts where ids.Contains(a.Account_id) select new AccountInfo() { AccountID = a.Account_id, AccountName = a.Name, AccountNumber = a.AccountNumber }).ToDictionary(v => v.AccountID);
        }
        public static List<Account> LoadAccount(List<int> ids)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, AccountSecuredObject, PermissionValue.Read);

            return (from d in LoadQuery(DataContext.Reader) where ids.Contains(d.account.Account_id) select new Account(d)).ToList();
        }

        public static List<Account> LoadAccount(AccountType accountType, List<int> ids)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, AccountSecuredObject, PermissionValue.Read);

            var exp = from d in LoadQuery(DataContext.Reader) select d;
            switch (accountType)
            {
                case AccountType.Customer: exp = exp.Where(d => ids.Contains(d.account.Customer_id.GetValueOrDefault())); break;
                case AccountType.Merchant: exp = exp.Where(d => ids.Contains(d.account.Merchant_id.GetValueOrDefault())); break;
                case AccountType.Affiliate: exp = exp.Where(d => ids.Contains(d.account.Affiliate_id.GetValueOrDefault())); break;
                case AccountType.DebitCompany: exp = exp.Where(d => ids.Contains(d.account.DebitCompany_id.GetValueOrDefault())); break;
            }
            return exp.Select(d => new Account(d)).ToList();
        }

        public static Account LoadAccount(AccountType accountType, int id)
        {
            return LoadAccount(accountType, new List<int>() { id }).SingleOrDefault();
        }

        public static Account LoadAccount(int id)
        {
            return LoadAccount(new List<int>() { id }).SingleOrDefault();
        }

        public static Account LoadAccount(AccountType accountType, string accountNumber)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, AccountSecuredObject, PermissionValue.Read);

            return (from d in LoadQuery(DataContext.Reader) where d.account.AccountType_id == (byte)accountType && d.account.AccountNumber == accountNumber select new Account(d)).FirstOrDefault();
        }

        public static int? AccountIDByNumber(AccountType accountType, string accountNumber)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, AccountSecuredObject, PermissionValue.Read);

            return (from d in DataContext.Reader.Accounts where d.AccountType_id == (byte)accountType && d.AccountNumber == accountNumber select (int?)d.Account_id).FirstOrDefault();
        }

        public static Account AccountByPersonalNumber(string personalNumber)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, AccountSecuredObject, PermissionValue.Read);

            try
            {
                var pAccount = (from p in LoadQuery(DataContext.Reader)
                                where p.account.Customer_id.HasValue
                       && p.account.data_Customer.PersonalNumber == personalNumber
                                select p).SingleOrDefault();

                if (pAccount != null)
                    return new Account(pAccount);
                else
                    return null;
            }
            catch
            {
                return null;
            }

        }

        public override Netpay.Infrastructure.ValidationResult Validate()
        {
            var ret = Netpay.Infrastructure.ValidationResult.Success;
            if (PersonalAddress != null) if ((ret = PersonalAddress.Validate()) != Infrastructure.ValidationResult.Success) return ret;
            if (BusinessAddress != null) if ((ret = BusinessAddress.Validate()) != Infrastructure.ValidationResult.Success) return ret;
            return ret;
        }

        public void SaveAccount()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, AccountSecuredObject, PermissionValue.Edit);

            if (string.IsNullOrEmpty(_accountEntity.PincodeSHA256)) SetPinCode((new Random()).Next(9999).ToString("0000"));
            if (string.IsNullOrEmpty(_accountEntity.AccountNumber)) GenerateNumber();
            if (PersonalAddress != null) _accountEntity.PersonalAddress_id = PersonalAddress.SaveAddress();
            else
            {
                if (_accountEntity.PersonalAddress_id != null) AccountAddress.Load(_accountEntity.PersonalAddress_id.Value).DeleteAddress();
                _accountEntity.PersonalAddress_id = null;
            }

            if (BusinessAddress != null) _accountEntity.BusinessAddress_id = (_accountEntity.PersonalAddress_id.HasValue && _accountEntity.PersonalAddress_id == BusinessAddress.AddressID) ? _accountEntity.PersonalAddress_id : BusinessAddress.SaveAddress();

            else
            {
                if (_accountEntity.BusinessAddress_id != null) AccountAddress.Load(_accountEntity.BusinessAddress_id.Value).DeleteAddress();
                _accountEntity.BusinessAddress_id = null;
            }
            if (_login != null && !string.IsNullOrEmpty(_login.EmailAddress))
            {
                _login.Save(_accountEntity);
                _accountEntity.LoginAccount_id = _login.LoginID;
            }

            DataContext.Writer.Accounts.Update(_accountEntity, (_accountEntity.Account_id != 0));
            DataContext.Writer.SubmitChanges();
        }


        public void SetWireProvider(string wireprovider)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, AccountSecuredObject, PermissionValue.Edit);

            if (string.IsNullOrEmpty(wireprovider)) return;
            if (_accountEntity.AccountType_id != (byte)AccountType.Merchant) return;

            _accountEntity.PreferredWireProvider_id = wireprovider;
            DataContext.Writer.Accounts.Update(_accountEntity, (_accountEntity.Account_id != 0));
            DataContext.Writer.SubmitChanges();
        }

        protected void DeleteAccount()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, AccountSecuredObject, PermissionValue.Delete);

            if (_accountEntity.Account_id == 0) return;
            if (_accountEntity.PersonalAddress_id.HasValue) AccountAddress.Load(_accountEntity.PersonalAddress_id.Value).DeleteAddress();
            if (_accountEntity.BusinessAddress_id.HasValue) AccountAddress.Load(_accountEntity.BusinessAddress_id.Value).DeleteAddress();
            foreach (var v in StoredPaymentMethod.LoadForAccount(AccountID)) v.Delete();
            if (_accountEntity.LoginAccount_id.HasValue) Login.Delete();
            DataContext.Writer.Accounts.Delete(_accountEntity);
            DataContext.Writer.SubmitChanges();
        }

        public void SetPinCode(string newPinCode)
        {
            if (AccountID != 0) ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Account }, PinCodeSecuredObject, PermissionValue.Edit);

            if (newPinCode == null) newPinCode = (new Random()).Next(9999).ToString("0000");
            if (!Netpay.Infrastructure.Application.IsProduction)
                Logger.Log(LogSeverity.Info, LogTag.None, "Setting Pin to '" + newPinCode + "'; '" + Infrastructure.Security.Encryption.GetSHA256Hash(PINCODE_SECRET + newPinCode) + "'");
            _accountEntity.PincodeSHA256 = Infrastructure.Security.Encryption.GetSHA256Hash(PINCODE_SECRET + newPinCode);
            _newPinCode = newPinCode;
        }

        public ValidationResult UpdatePinCode(string password, string newPincode)
        {
            //User user = validateCustomer(ID);
            if (AccountID != 0) ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Account }, PinCodeSecuredObject, PermissionValue.Edit);

            if (newPincode == null) newPincode = (new Random()).Next(9999).ToString("0000");
            if (string.IsNullOrEmpty(password)) return ValidationResult.PasswordWrong;
            if (string.IsNullOrEmpty(newPincode)) return ValidationResult.PinCodeWeak;
            newPincode = newPincode.Trim();
            if (!Login.ValidatePassword(Login.Current.LoginID, password, null)) return ValidationResult.PasswordWrong;
            if (newPincode == null || newPincode.Length != 4) return ValidationResult.PinCodeWeak;
            _accountEntity.PincodeSHA256 = Infrastructure.Security.Encryption.GetSHA256Hash(PINCODE_SECRET + newPincode);
            _newPinCode = newPincode;
            SaveAccount();
            return ValidationResult.Success;
        }

        public System.Net.Mail.Attachment ProfileImageAttachment
        {
            get
            {
                object message;
                if (ProfileImageSize == 0) return null;
                var imageAttach = new System.Net.Mail.Attachment(new System.IO.MemoryStream(ProfileImageData), "ProfileImage.png", "image/png");
                imageAttach.ContentDisposition.Inline = true;
                if (ObjectContext.Current.Items.TryGetValue("EmailMessage", out message))
                    (message as System.Net.Mail.MailMessage).Attachments.Add(imageAttach);
                return imageAttach;
            }
        }

        public object GetEmailData()
        {
            string password = null;
            ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);
            try
            {
                password = Login.GetPassword(LoginID.GetValueOrDefault(), null);
            }
            finally
            {
                ObjectContext.Current.StopImpersonate();
            }
            return new { Domain = Domain.Current, Account = this, AccountPassword = password };
        }

        public void SendAccountEmail(string templateName, object dataObj = null)
        {
            var template = Infrastructure.Email.Template.Get(templateName);
            if (template == null) throw new Exception("Email Template not found " + templateName);
            if (dataObj == null) dataObj = GetEmailData();

            //Declare the current 'customer-app identity' variable.
            Bll.ApplicationIdentity currentCustomerAppIdentity = null;

            //Get the current customer app identity.
            if (AccountType == AccountType.Customer && this.CustomerID.HasValue)
            {
                currentCustomerAppIdentity = Bll.Customers.Customer.GetCustomerAppIdentity(CustomerID.Value);
            }

            //Check if the template included in the app identity templates ,
            //And also if the account is customer 
            if (Infrastructure.Email.Template.AppIdentityTemplates.Contains(templateName) &&
                AccountType == AccountType.Customer &&
                currentCustomerAppIdentity != null &&
                !string.IsNullOrEmpty(currentCustomerAppIdentity.ThemeName) &&
                System.IO.File.Exists(template.MapPhysicalPathWithThemeFolder(null, currentCustomerAppIdentity.ThemeName)))
            {
                string themeName = currentCustomerAppIdentity.ThemeName;
                string ret = template.MapPhysicalPathWithThemeFolder(null, themeName);
                template.HtmlData = System.IO.File.ReadAllText(ret);
                template.Send(dataObj, null, new System.Net.Mail.MailAddress(Login.EmailAddress, AccountName), themename: themeName);
            }
            else
            {
                template.Send(dataObj, null, new System.Net.Mail.MailAddress(Login.EmailAddress, AccountName));
            }
        }

        public string LoginEmailAddress
        {
            get
            {
                return Login.EmailAddress;
            }
        }

        public void SendAccountResetPasswordMail()
        {
            switch (AccountType)
            {
                case AccountType.Affiliate:
                    SendAccountEmail("Affiliate_ResetPassword.htm");
                    break;
                case AccountType.Customer:
                    SendAccountEmail("Wallet_ResetPassword.htm");
                    break;
                case AccountType.Merchant:
                    SendAccountEmail("Merchant_ResetPassword.htm");
                    break;
            }
        }

        protected void SendPinCode()
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
            {
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, PinCodeSecuredObject, PermissionValue.Read);
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, AccountSecuredObject, PermissionValue.Read);
            }

            if (_newPinCode == null) throw new ArgumentNullException("newPinCode");
            string templateName = "Account_ResetPinCode.htm";
            var dataObj = new { Domain = Domain.Current, Account = this, PinCode = _newPinCode };
            SendAccountEmail(templateName, dataObj);
        }

        public void ResetPinCode()
        {
            try
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, PinCodeSecuredObject, PermissionValue.Edit);

                ObjectContext.Current.Impersonate(Domain.Current.ServiceCredentials);
                string pinCode = "0000";
                var pCustomer = Bll.Customers.Customer.Load(this.CustomerID);
                Affiliates.Affiliate.PinGenerationMode pinGenType;
                if (pCustomer != null && CustomerID.HasValue)
                {
                    var aff = (
                            from w in DataContext.Reader.ApplicationIdentities
                            join x in DataContext.Reader.tblAffiliatesToApplicationIdentities on w.ApplicationIdentity_id equals x.ApplicationIdentity_id
                            join y in DataContext.Reader.Customers on x.ApplicationIdentity_id equals y.ApplicationIdentity_id
                            join z in DataContext.Reader.tblAffiliates on x.Affiliates_id equals z.affiliates_id
                            where y.Customer_id == Convert.ToInt32(this.CustomerID)
                            select new { z.GenerateRandomCardPin, z.LegalName }
                            ).FirstOrDefault();
                    if (aff != null && aff.GenerateRandomCardPin.HasValue)
                    {
                        pinGenType = (Affiliates.Affiliate.PinGenerationMode)Enum.Parse(typeof(Affiliates.Affiliate.PinGenerationMode), Convert.ToInt32(aff.GenerateRandomCardPin).ToString());
                    }
                    else
                    {
                        Logger.Log(LogSeverity.Warning, LogTag.Process, string.Format("Affiliate '{0}' has not set a preferred pin generation method, therefore the random numbers will be used.", aff.LegalName));
                        pinGenType = Affiliates.Affiliate.PinGenerationMode.RandomNumbers;
                    }
                    if (pinGenType == Affiliates.Affiliate.PinGenerationMode.LastFourIdDigits)
                    {
                        if (!string.IsNullOrEmpty(pCustomer.PersonalNumber))
                        {
                            pinCode = pCustomer.PersonalNumber.Length < 4 ?
                                 pCustomer.PersonalNumber.PadLeft(4, '0') :
                                 pCustomer.PersonalNumber.Substring(pCustomer.PersonalNumber.Length - 4);
                        }
                        else
                        {
                            throw new Exception(string.Format("Unable to generate pin because the customer's personal ID is not captured"));
                        }

                    }
                    else
                    {
                        var random = new Random().Next(1, 9999) + 258;//to reduce chance of gettting a sequential number
                        pinCode = random.ToString().Length < 4 ? random.ToString().PadLeft(4, '0') :
                            random.ToString().Substring(random.ToString().Length - 4);
                    }
                    SetPinCode(pinCode);
                    SaveAccount();
                    SendPinToCustomer(pCustomer, pinCode);
                }
                //if (pCustomer != null && !string.IsNullOrEmpty(pCustomer.PersonalNumber))
                //{
                //    if (pCustomer.RefIdentityID.HasValue)
                //    {


                //        var filter = new Affiliates.Affiliate.SearchFilters();
                //        filter.ID = new Infrastructure.Range<int?>(pCustomer.AffiliateID);
                //        var affiliate = Affiliates.Affiliate.Search(filter, null).FirstOrDefault();
                //        if (affiliate != null && affiliate.GenerateRandomCardPin.HasValue)
                //        {
                //            if (Convert.ToInt16(affiliate.GenerateRandomCardPin.HasValue) == Convert.ToInt16(Affiliates.Affiliate.PinGenerationMode.RandomNumbers))
                //            {
                //                var random = new Random().Next(1, 9999) + 258;//to avoid sequential numbers
                //                pinCode = random.ToString().Length < 4 ? random.ToString().PadLeft(4, '0') : random.ToString().Substring(random.ToString().Length - 4);
                //            }
                //        }
                //        else
                //        {
                //            if (!string.IsNullOrEmpty(pCustomer.PersonalNumber))
                //            {
                //                pinCode = pCustomer.PersonalNumber.Length < 4 ?
                //                    pCustomer.PersonalNumber.PadLeft(4, '0') : 
                //                    pCustomer.PersonalNumber.Substring(pCustomer.PersonalNumber.Length - 4);

                //            }
                //        }
                //        SetPinCode(pinCode);
                //        SaveAccount();
                //        SendPinToCustomer(pCustomer, pinCode);
                //    }

                //    //if (Infrastructure.Application.IsProduction)
                //    //{
                //    //    try
                //    //    {
                //    //        var pTheeCardNetwork = Affiliates.Affiliate.Search(new Affiliates.Affiliate.SearchFilters() { Text = "Thee Card Network", IsActive = true }, null)[0];
                //    //        var lstAppIDs = ApplicationIdentity.LoadForAffiliate(pTheeCardNetwork.AffiliateID.Value);
                //    //        lstAppIDs = lstAppIDs.Where(p => p.IsActive).ToList();

                //    //        var pAppID = lstAppIDs[0];

                //    //        if (pAppID == null)
                //    //            throw new Exception("TCN not found");
                //    //        else
                //    //            Logger.Log("Affiliate found:" + pAppID.Name + "(" + pAppID.ID + ")");

                //    //        Logger.Log("pCustomer Affiliate: (" + pCustomer.RefIdentityID + ")");

                //    //        if (pCustomer.RefIdentityID == pAppID.ID /* Thee Card Network */)
                //    //        {
                //    //            // Set PIN to last 4 digits of card.
                //    //            SetPinCode(pCustomer.PersonalNumber.TruncStart(4));
                //    //            SaveAccount();
                //    //        }
                //    //        else
                //    //        {
                //    //            SetPinCode(null);
                //    //            SendPinCode();
                //    //        }
                //    //    }
                //    //    catch (Exception ex2)
                //    //    {
                //    //        SetPinCode(null);
                //    //        SendPinCode();
                //    //    }
                //    //}
                //    //else
                //    //{
                //    //    SetPinCode(pCustomer.PersonalNumber.TruncStart(4));
                //    //    SaveAccount();
                //    //}
                //}
                else
                    throw new Exception("Customer does not have Personal ID number.");
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                throw;
            }
            finally
            {
                ObjectContext.Current.StopImpersonate();
            }
        }

        private void SendPinToCustomer(Customer pCustomer, string pinCode)
        {
            switch (pCustomer.PinNotificationTypeId)
            {
                case (short)PinNotificationType.Both:

                    if (!string.IsNullOrEmpty(pCustomer.CellNumber) && !string.IsNullOrEmpty(pCustomer.EmailAddress))
                    {
                        SMS.SMSService.SendSms(pCustomer.CellNumber, string.Format("Your Pin number is: {0}. For more, call YoManE on  (+27) 011 888 1520", pinCode));
                        SendPinViaEmail(pCustomer, pinCode);
                    }
                    else
                    {

                        throw new Exception(string.Format("Neither email address not cellphone number have been setup for the customer"));
                    }
                    break;
                case (short)PinNotificationType.Email:
                    if (!string.IsNullOrEmpty(pCustomer.EmailAddress))
                    {
                        SendPinViaEmail(pCustomer, pinCode);
                    }
                    else
                    {
                        throw new Exception(string.Format("Email address not available, yet the customer would like to receive the pin via email"));
                    }
                    break;
                case (short)PinNotificationType.SMS:
                    if (!string.IsNullOrEmpty(pCustomer.CellNumber))
                    {
                        SMS.SMSService.SendSms(pCustomer.CellNumber, "The pin number to your card(s) is: " + pinCode);
                        //send Email
                    }
                    else
                    {
                        throw new Exception(string.Format("Cellphone not available, yet the customer would like to receive the pin via SMS"));
                    }
                    break;
                default:

                    if (!string.IsNullOrEmpty(pCustomer.EmailAddress))
                    {
                        SendPinViaEmail(pCustomer, pinCode);
                    }
                    else
                    if (!string.IsNullOrEmpty(pCustomer.CellNumber))
                    {
                        SMS.SMSService.SendSms(pCustomer.CellNumber, string.Format("Your Pin number is: {0}. For more, call YoManE on  (+27) 011 888 1520", pinCode));

                    }

                    else
                    {
                        throw new Exception(string.Format("Neither email address not cellphone number have been setup for the customer"));
                    }

                    break;
            }
        }

        private void SendPinViaEmail(Customer pCustomer, string pinCode)
        {
            var xem = new ExchangeEmailManager();
            var body = string.Format("<p>Good day {0} {1},</p>  <p>your card pin number has been set as {2}.</p> <p>Regards,<br/>Support@yomane.com", pCustomer.FirstName, pCustomer.LastName, pinCode);

            var toEmails = new List<string> { pCustomer.EmailAddress };
            xem.SendEmail("Card Pin", body, null, toEmails);
        }

        public bool ValidatePinCode(string pinCode)
        {
            if (string.IsNullOrEmpty(_accountEntity.PincodeSHA256) || string.IsNullOrEmpty(pinCode)) return false;
            return (_accountEntity.PincodeSHA256 == Infrastructure.Security.Encryption.GetSHA256Hash(PINCODE_SECRET + pinCode));
        }

        public static bool ValidatePinCodeWithoutLoadingAccount(string strAccountPIN, string strSuppliedPIN)
        {
            return (strAccountPIN == Infrastructure.Security.Encryption.GetSHA256Hash(PINCODE_SECRET + strSuppliedPIN));
        }

        public static bool ValidatePinCode(int id, string pinCode)
        {
            var accountrPinCode = (from c in DataContext.Reader.Accounts where c.Account_id == id select c.PincodeSHA256).FirstOrDefault();
            if (string.IsNullOrEmpty(accountrPinCode) || string.IsNullOrEmpty(pinCode)) return false;
            return (accountrPinCode == Infrastructure.Security.Encryption.GetSHA256Hash(PINCODE_SECRET + pinCode));
        }

        private Login _login;
        public Login Login
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                {
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, AccountSecuredObject, PermissionValue.Read);
                }


                if (_login != null) return _login;

                if (_accountEntity.LoginAccount_id.GetValueOrDefault() != 0)
                {
                    _login = Login.LoadLogin(_accountEntity.LoginAccount_id.GetValueOrDefault());
                }
                else
                {
                    switch (AccountType)
                    {
                        case Accounts.AccountType.Merchant: _login = new Infrastructure.Security.Login(UserRole.Merchant); break;
                        case Accounts.AccountType.Customer: _login = new Infrastructure.Security.Login(UserRole.Customer); break;
                        case Accounts.AccountType.DebitCompany: _login = new Infrastructure.Security.Login(UserRole.DebitCompany); break;
                        case Accounts.AccountType.Affiliate: _login = new Infrastructure.Security.Login(UserRole.Partner); break;
                    }
                }
                return _login;
            }
        }

        public static Account Current
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, AccountSecuredObject, PermissionValue.Read);

                var login = Login.Current;
                if (login == null) return null;
                if (!login.IsInRole(UserRole.Account)) return null;
                object obj;
                if (!login.Items.TryGetValue("Account", out obj))
                {
                    Account ret = null;
                    if (LimitedLogin.Current != null) ret = (from d in DataContext.Reader.Accounts where d.Account_id == LimitedLogin.Current.AccountID select new Account(new AccountLoadCombined() { account = d })).SingleOrDefault();
                    else ret = (from d in DataContext.Reader.Accounts where d.LoginAccount_id == login.LoginID select new Account(new AccountLoadCombined() { account = d })).SingleOrDefault();
                    obj = ret;
                    if (obj != null) login.Items["Account"] = obj;
                }
                return obj as Account;
            }
        }

        public static Account CurrentObject
        {
            get
            {
                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, AccountSecuredObject, PermissionValue.Read);

                var current = Current;
                if (current != null && current.GetType() == typeof(Account))
                {
                    current = Account.LoadObject(current._accountEntity);
                    Login.Current.Items["Account"] = current;
                }
                return current;
            }
        }

        public static string MapPrivatePath(int? accountId, string fileName)
        {
            string path = Domain.Current.MapPrivateDataPath("Account Files");
            if (accountId != null)
            {
                path = System.IO.Path.Combine(path, accountId.ToString() + "/");
            }
            else
            {
                path = System.IO.Path.Combine(path, "Admin" + "/");
            }
            if (fileName != null) path = System.IO.Path.Combine(path, fileName);
            return path;
        }

        public static string DeletedFilesPath()
        {
            string path = Domain.Current.MapPrivateDataPath("Account Files");
            path = System.IO.Path.Combine(path, "Deleted/");
            return path;
        }

        public static string MapDeletedFilesPath(string fileName)
        {
            string path = Domain.Current.MapPrivateDataPath("Account Files");
            path = System.IO.Path.Combine(path, "Deleted/");
            if (!string.IsNullOrEmpty(fileName)) path = System.IO.Path.Combine(path, fileName);
            return path;
        }

        public static string MapAccountContentPrivatePath(int? id, string fileName)
        {
            string path = Domain.Current.MapPrivateDataPath("Account Files");
            if (id != null) path = System.IO.Path.Combine(path, id.ToString());
            if (fileName != null) path = System.IO.Path.Combine(path, fileName);
            return path;
        }

        public static string MapAppIdentityContentPrivatePath(int? id, string fileName)
        {
            string path = Domain.Current.MapPrivateDataPath("AppIdentityFiles");
            if (id != null) path = System.IO.Path.Combine(path, id.ToString());
            if (fileName != null) path = System.IO.Path.Combine(path, fileName);
            return path;
        }

        public static string MapPublicOffset(string accountNumber, string fileName)
        {
            string path = "Accounts/";
            if (accountNumber != null) path = System.IO.Path.Combine(path, accountNumber + "/");
            if (fileName != null) path = System.IO.Path.Combine(path, fileName);
            return path;
        }

        public static string MapPublicPath(string accountNumber, string fileName)
        {
            return Domain.Current.MapPublicDataPath(MapPublicOffset(accountNumber, fileName));
        }

        public static string MapPublicVirtualPath(string accountNumber, string fileName)
        {
            return Domain.Current.MapPublicDataVirtualPath(MapPublicOffset(accountNumber, fileName));
        }


        public long ProfileImageSize
        {
            get
            {
                if (string.IsNullOrEmpty(ProfileImageFileName)) return 0;
                if (!System.IO.File.Exists(ProfileImageFileName)) return 0;
                return new System.IO.FileInfo(ProfileImageFileName).Length;
            }
        }

        public string MapPrivatePath(string fileName = null)
        {
            return MapPrivatePath(AccountID, fileName);
        }

        public string MapPublicOffset(string fileName)
        {
            return MapPublicOffset(AccountNumber, fileName);
        }

        public string MapPublicPath(string fileName = null)
        {
            return MapPublicPath(AccountNumber, fileName);
        }

        public string MapPublicVirtualPath(string fileName = null)
        {
            return MapPublicVirtualPath(AccountNumber, fileName);
        }

        public static int? GetLoginAccountIdByMerchantId(int merchantId)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, AccountSecuredObject, PermissionValue.Read);

            if (merchantId <= 0) return null;

            int? loginAccountId = (from a in DataContext.Reader.Accounts
                                   where a.Merchant_id == merchantId
                                   select a.LoginAccount_id)
                                  .SingleOrDefault();

            return loginAccountId;
        }

        public static int? GetAccountIdByMerchantId(int merchantId)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, AccountSecuredObject, PermissionValue.Read);

            if (merchantId <= 0) return null;

            int? accountId = (from a in DataContext.Reader.Accounts
                              where a.Merchant_id == merchantId
                              select a.Account_id)
                                  .SingleOrDefault();

            return accountId;
        }
    }
}
