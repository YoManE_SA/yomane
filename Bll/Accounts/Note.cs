﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Accounts
{
	public class Note : BaseDataObject
	{
		private Netpay.Dal.Netpay.AccountNote _entity;

		public int ID { get { return _entity.AccountNote_id; } }
		public System.DateTime InsertDate { get { return _entity.InsertDate; }}
		public int Account_id { get { return _entity.Account_id.GetValueOrDefault(); } }
		public string UserName { get { return _entity.InsertUser; } }
		public string Text { get { return _entity.NoteText; } }

		public const string SecuredObjectName = "Note";
		public static SecuredObject SecuredObject { get { return File.SecuredObject; } }

		private Note(Netpay.Dal.Netpay.AccountNote entity)
		{ 
			_entity = entity;
		}

		public static Note Load(int id)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
            
			var entity = (from mmas in DataContext.Reader.AccountNotes where mmas.AccountNote_id == id select mmas).SingleOrDefault();
			if (entity == null) return null;
			return new Note(entity);
		}

		public static List<Note> LoadForAccount(int accountId)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

			return (from mmas in DataContext.Reader.AccountNotes where mmas.Account_id == accountId select new Note(mmas)).ToList();
		}

		public void Save()
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            DataContext.Writer.AccountNotes.Update(_entity, (_entity.AccountNote_id != 0));
			DataContext.Writer.SubmitChanges();
		}

		public static void AddNote(int accountId, string userName, string Text)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            if (userName == null && AdminUser.Current != null) userName = AdminUser.Current.FullName;
			var ret = new Note(new Dal.Netpay.AccountNote() { Account_id = accountId, InsertUser = userName, NoteText = Text, InsertDate = DateTime.Now });
			ret.Save();
		}

		public void Delete()
		{
			if (_entity.AccountNote_id == 0) return;

            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Delete);

			DataContext.Writer.AccountNotes.Delete(_entity);
			DataContext.Writer.SubmitChanges();
		}

        public static List<Note> AutoCompleteForAccount(string text, int accountID)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from n in DataContext.Reader.AccountNotes
                    where (n.NoteText).Contains(text) && n.Account_id == accountID
                    orderby n.NoteText ascending
                    select new Note(n)).Take(10).ToList();
        }
	
	}
}
