﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Accounts
{
	public class Module : Infrastructure.Module
	{
		public const string ModuleName = "Accounts";
		public override string Name { get { return ModuleName; } }
		public override decimal Version { get { return 1.13m; } }
		public static Module Current { get { return Infrastructure.Module.Get(ModuleName) as Module; } }
		protected override void OnInstall(EventArgs e)
		{
			Infrastructure.Security.SecuredObject.Create(this, Account.AccountSecuredObjectName, PermissionGroup.ReadEditDelete);
			Infrastructure.Security.SecuredObject.Create(this, LimitedLogin.LimitedSecuredObjectName, PermissionGroup.ReadEditDelete);
			Infrastructure.Security.SecuredObject.Create(this, File.SecuredObjectName, PermissionGroup.ReadEditDelete);
			Infrastructure.Security.SecuredObject.Create(this, Payee.SecuredObjectName, PermissionGroup.ReadEditDelete);
            Infrastructure.Security.SecuredObject.Create(this, MobileDevice.SecuredObjectName, PermissionGroup.ReadEditDelete);
            Infrastructure.Security.SecuredObject.Create(this, StoredPaymentMethod.SecuredObjectName, PermissionGroup.ReadEditDelete);
            base.OnInstall(e);
		}

		protected override void OnUninstall(EventArgs e)
		{
			Infrastructure.Security.SecuredObject.Remove(this);
			base.OnUninstall(e);
		}

        protected override void OnActivate(EventArgs e)
        {
            EmailTemplates.RegisterTemplates(ModuleName);
            base.OnActivate(e);
        }

        protected override void OnDeactivate(EventArgs e)
        {
            EmailTemplates.UnRegisterTemplates(ModuleName);
            base.OnDeactivate(e);
        }


        /*
		private void Login_LoggedIn(object sender, EventArgs e)
		{	
			var login = sender as Login;
			if (login == null) return;
			switch(login.Role)
			{
			case UserRole.Customer:				
				//AccountFilter.Current.AccountIDs
			case UserRole.Partner:
			case UserRole.DebitCompany:
			case UserRole.Merchant:
				break;
			}
			throw new NotImplementedException();
		}
		*/


    }
}
