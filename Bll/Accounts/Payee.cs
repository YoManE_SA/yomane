﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Accounts
{
	
	public class Payee : BaseDataObject
	{
		public const string SecuredObjectName = "Payee";
		public static SecuredObject SecuredObject { get { return SecuredObject.Get(Accounts.Module.Current, SecuredObjectName); } }

		private Dal.Netpay.AccountPayee _entity;
		public int ID { get { return _entity.AccountPayee_id; } }
		public int AccountID { get { return _entity.Account_id; } }
		public string SearchTag { get { return _entity.SearchTag; } set { _entity.SearchTag = value; } }
		public string LegalNumber { get { return _entity.LegalNumber; } set { _entity.LegalNumber = value; } }
		public string CompanyName { get { return _entity.CompanyName; } set { _entity.CompanyName = value; } }
		public string ContactName { get { return _entity.ContactName; } set { _entity.ContactName = value; } }
		public string PhoneNumber { get { return _entity.PhoneNumber; } set { _entity.PhoneNumber = value; } }
		public string FaxNumber { get { return _entity.faxNumber; } set { _entity.faxNumber = value; } }
		public string EmailAddress { get { return _entity.EmailAddress; } set { _entity.EmailAddress = value; } }
		public string StreetAddress { get { return _entity.StreetAddress; } set { _entity.StreetAddress = value; } }
		public string Comment { get { return _entity.Comment; } set { _entity.Comment = value; } }

		private BankAccount _bankAccount;
		public BankAccount BankAccount {
            get {
                if (_bankAccount == null) _bankAccount = new BankAccount();
                return _bankAccount;
            }
        }

		public Payee(int accountId)
		{
			AccountFilter.Current.Validate(null, accountId);
			ObjectContext.Current.IsUserOfType(null, SecuredObject, PermissionValue.Add);
			_entity = new Dal.Netpay.AccountPayee();
			_entity.Account_id = accountId;
		}

		private Payee(Netpay.Dal.Netpay.AccountPayee entity)
        { 
            _entity = entity;
			if (_entity.AccountBankAccount_id != null)
				_bankAccount = BankAccount.Load(_entity.AccountBankAccount_id.Value);
		}

		public static Payee Load(int id)
		{
			ObjectContext.Current.IsUserOfType(null, SecuredObject, PermissionValue.Read);
			var entity = (from mmas in DataContext.Reader.AccountPayees where mmas.AccountPayee_id == id select mmas).SingleOrDefault();
			if (entity == null) return null;
			return new Payee(entity);
		}

		public static List<Payee> LoadForAccount(int accountId)
		{
			AccountFilter.Current.Validate(null, accountId);
			ObjectContext.Current.IsUserOfType(null, SecuredObject, PermissionValue.Read);
			return (from mmas in DataContext.Reader.AccountPayees where mmas.Account_id == accountId select new Payee(mmas)).ToList();
		}

		public void Save()
		{
			ObjectContext.Current.IsUserOfType(null, SecuredObject, PermissionValue.Edit);
            if (_bankAccount != null) {
                if (_bankAccount.ID == 0 && _bankAccount.Account_id == 0) {
                    _bankAccount.Account_id = _entity.Account_id;
                }
                _bankAccount.Save();
                _entity.AccountBankAccount_id = _bankAccount.ID;
            }
            DataContext.Writer.AccountPayees.Update(_entity, (_entity.AccountPayee_id != 0));
			DataContext.Writer.SubmitChanges();
		}

		public void Delete()
		{
			ObjectContext.Current.IsUserOfType(null, SecuredObject, PermissionValue.Delete);
			DataContext.Writer.AccountPayees.Delete(_entity);
			DataContext.Writer.SubmitChanges();
		}

	}
}
