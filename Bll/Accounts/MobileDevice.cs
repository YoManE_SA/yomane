﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Netpay.Bll.Accounts;
using Netpay.Infrastructure;
using Netpay.Infrastructure.Security;

namespace Netpay.Bll.Accounts
{
	public class MobileDevice : BaseDataObject
	{
		public const string SecuredObjectName = "MobileDevice";
		public static Infrastructure.Security.SecuredObject SecuredObject { get { return Infrastructure.Security.SecuredObject.Get(Accounts.Module.Current, SecuredObjectName); } }

        public class SearchFilters
        {
            public int? AccountID;
            public AccountType? AccountType;
            public string DeviceID;
        }

        private Netpay.Dal.Netpay.MobileDevice _entity;
        public int ID { get { return _entity.MobileDevice_id; } }
        public int Account_id { get { return _entity.Account_id; } }
        public int? SubUserID { get { return _entity.AccountSubUser_id; } }
        public System.DateTime InsertDate { get { return _entity.InsertDate; } }
		public string DeviceIdentity { get { return _entity.DeviceIdentity; } }
        public string PushToken { get { return _entity.AppPushToken; } }
        public string DeviceUserAgent { get { return _entity.DeviceUserAgent; } }
		public string DevicePhoneNumber { get { return _entity.DevicePhoneNumber; } }
		public string PassCode { get { return _entity.PassCode; } }
		public System.DateTime? LastLogin { get { return _entity.LastLogin; } }
		public bool IsActivated { get { return _entity.IsActivated; } }
		public bool IsActive { get { return _entity.IsActive; } }
		public string AppVersion { get { return _entity.AppVersion; } }
        
        public int SignatureFailCount { get { return _entity.SignatureFailCount; } }
		public string FriendlyName { get { return _entity.FriendlyName; } set { _entity.FriendlyName = value; } }
		private string _subUseName;
		public string SubUserName { 
			get {
				if (_subUseName != null) return _subUseName;
				if (_entity.AccountSubUser_id == null) return null;
				return (_subUseName = _entity.data_AccountSubUser.data_LoginAccount.LoginUser); 
			}
		}

		private MobileDevice(Netpay.Dal.Netpay.MobileDevice entity)
		{ 
			_entity = entity;
		}

        public MobileDevice()
		{            
            _entity = new Dal.Netpay.MobileDevice();
		}

        public static List<MobileDevice> Search(int accountId)
		{
            ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin, UserRole.Account }, SecuredObject, PermissionValue.Read);

            AccountFilter.Current.Validate(null, accountId);
            return (from m in DataContext.Reader.MobileDevices where m.Account_id == accountId select new MobileDevice(m)).ToList();
		}

        public static List<MobileDevice> Search(SearchFilters filters, ISortAndPage sortAndPage)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            var exp = (from m in DataContext.Reader.MobileDevices select m);
            if (filters != null)
            {
                if (filters.AccountID.HasValue) exp = exp.Where(v => v.Account_id == filters.AccountID.Value);
                if (filters.DeviceID != null) exp = exp.Where(v => v.DeviceIdentity == filters.DeviceID);
                if (filters.AccountType.HasValue) exp = exp.Where(v => v.data_Account.AccountType_id == (int) filters.AccountType.Value);
            }
            return exp.ApplySortAndPage(sortAndPage).Select(v => new MobileDevice(v)).ToList();
        }

        public static MobileDevice Load(int id)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from m in DataContext.Reader.MobileDevices where m.MobileDevice_id == id select new MobileDevice(m)).SingleOrDefault();
		}

        public static MobileDevice LoadByDeviceID(int accountId, string deviceId)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

            return (from m in DataContext.Reader.MobileDevices where m.Account_id == accountId && m.DeviceIdentity == deviceId select new MobileDevice(m)).SingleOrDefault();
		}

        public static MobileDevice Current
        { 
			get
            {                
                object obj;
				if (Account.Current == null) return null;

                if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                    ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);

                if (!Login.Current.Items.TryGetValue("MobileDevice", out obj))
				{
					obj = LoadByDeviceID(Account.Current.AccountID, Login.Current.LoggedInDevice);
					if (obj == null) return null;
                    Login.Current.Items.Add("MobileDevice", obj);
				}
                return obj as MobileDevice;
			} 
		}

        public static MobileDeviceRegistrationResult RegisterMobileDevice(int limitDeviceCount, string deviceUniqueID, string devicePushToken, string devicePhoneNumber, string deviceUserAgent, out MobileDevice outDevice)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Add);

            int accountId = Account.Current.AccountID;

            //AccountFilter.Current.Validate(null, ref accountId);
            outDevice = null;

            Netpay.Dal.Netpay.MobileDevice device = null; 
            if (Accounts.LimitedLogin.IsLimitedLogin)
                device = (from d in DataContext.Reader.MobileDevices where d.Account_id == accountId && d.AccountSubUser_id == Login.Current.LoginID && d.DeviceIdentity == deviceUniqueID select d).SingleOrDefault();
            else
                device = (from d in DataContext.Reader.MobileDevices where d.Account_id == accountId && d.AccountSubUser_id == null && d.DeviceIdentity == deviceUniqueID select d).SingleOrDefault();
            if (device == null)
            {
                int deviceCount = (from d in DataContext.Reader.MobileDevices where d.Account_id == accountId && d.IsActive select d).Count();
                if (deviceCount >= limitDeviceCount) return MobileDeviceRegistrationResult.MaxDevicesExceeded;
                device = new Netpay.Dal.Netpay.MobileDevice();
                device.InsertDate = DateTime.Now;
                device.Account_id = accountId;
                //if (LimitedLogin.IsLimitedLogin) device.AccountSubUser_id = Login.Current.LoginID;
                device.IsActive = true;
                device.IsActivated = false;
                device.DeviceIdentity = deviceUniqueID;
                DataContext.Writer.MobileDevices.InsertOnSubmit(device);
            }
            else
            { 
                DataContext.Writer.MobileDevices.Attach(device);

            }
            device.PassCode = ((new Random()).Next(800000000) + 100000000).ToString();
            var deviceChanged = false;
            if (devicePushToken != null && devicePushToken != device.AppPushToken)
            {
                device.AppPushToken = devicePushToken;
                deviceChanged = true;
            }
            if (devicePhoneNumber != null && devicePhoneNumber != device.DevicePhoneNumber)
            {
                device.DevicePhoneNumber = devicePhoneNumber;
                if(!deviceChanged)
                    deviceChanged = true;
            }
            if (deviceUserAgent != null && deviceUserAgent != device.DeviceUserAgent)
            {
                device.DeviceUserAgent = deviceUserAgent;
                if (!deviceChanged)
                    deviceChanged = true;
            }
            DataContext.Writer.SubmitChanges();
           
            outDevice = new MobileDevice(device);

            if(deviceChanged)
            {
                //reload the mobile deveice object from the database
                object obj;
                if (Login.Current.Items.TryGetValue("MobileDevice", out obj))
                {
                    obj = LoadByDeviceID(Account.Current.AccountID, Login.Current.LoggedInDevice);
                    if (obj != null)
                    {
                        Login.Current.Items.Remove("MobileDevice");
                        Login.Current.Items.Add("MobileDevice", obj);
                    }
                } 
            }
            return MobileDeviceRegistrationResult.Success;
        }


		public bool SendMobilePassCode()
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Read);
            
            AccountFilter.Current.Validate(null, _entity.Account_id);
			bool smsResult = SMS.SMSService.SendSms(DevicePhoneNumber, string.Format("You PassCode Is: {0}", PassCode));
			return smsResult;
		}

		public static void SetMobileDevicesFriendlyName(int deviceId, string friendlyName)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            int? accountId = null;
            AccountFilter.Current.Validate(null, ref accountId);
            Netpay.Dal.Netpay.MobileDevice mdv = (from md in DataContext.Writer.MobileDevices where md.Account_id == accountId && md.MobileDevice_id == deviceId select md).SingleOrDefault();
			mdv.FriendlyName = friendlyName;
			DataContext.Writer.SubmitChanges();
		}

		public bool ActivateMobileDevice(string activationCode)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);
            
            AccountFilter.Current.Validate(null, _entity.Account_id);
			if (PassCode != activationCode) return false;
			_entity.IsActivated = true;
			_entity.LastLogin = DateTime.Now;
            DataContext.Writer.MobileDevices.Update(_entity, _entity.MobileDevice_id != 0);
			DataContext.Writer.SubmitChanges();
			return true;
		}

		public bool RegisterDeviceSignatureFailure(int deviceID)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);
            
            AccountFilter.Current.Validate(null, _entity.Account_id);
            _entity.SignatureFailCount++;
            DataContext.Writer.MobileDevices.Update(_entity, _entity.MobileDevice_id != 0);
            DataContext.Writer.SubmitChanges();
			if (_entity.SignatureFailCount >= 3) return true;
			return false;
		}

		public static void BlockMobileDevice(int deviceId, bool isActive)
		{
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            var device = (from d in DataContext.Writer.MobileDevices where d.MobileDevice_id == deviceId select d).SingleOrDefault();
			device.IsActive = isActive;
            DataContext.Writer.SubmitChanges();
		}

        public static void ResetSignatureLockCount(int accountid)
        {
            if (Login.Current != null && Login.Current.IsInRole(UserRole.Admin))
                ObjectContext.Current.IsUserOfType(new UserRole[] { UserRole.Admin }, SecuredObject, PermissionValue.Edit);

            var MobileDeviceSettings = (from md in DataContext.Writer.MobileDevices where md.Account_id == accountid select md).ToList();
            if (MobileDeviceSettings != null)
            {
                foreach (var item in MobileDeviceSettings)
                {
                    item.SignatureFailCount = 0;
                    DataContext.Writer.SubmitChanges();
                }
            }
        }
	}
}
