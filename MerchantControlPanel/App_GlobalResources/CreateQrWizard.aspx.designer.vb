'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.42000
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Imports System

Namespace Resources.CreateQrWizard
    
    'This class was auto-generated by the StronglyTypedResourceBuilder
    'class via a tool like ResGen or Visual Studio.
    'To add or remove a member, edit your .ResX file then rerun ResGen
    'with the /str option or rebuild the Visual Studio project.
    '''<summary>
    '''  A strongly-typed resource class, for looking up localized strings, etc.
    '''</summary>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "15.0.0.0"),  _
     Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute()>  _
    Friend Class aspx
        
        Private Shared resourceMan As Global.System.Resources.ResourceManager
        
        Private Shared resourceCulture As Global.System.Globalization.CultureInfo
        
        <Global.System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>  _
        Friend Sub New()
            MyBase.New
        End Sub
        
        '''<summary>
        '''  Returns the cached ResourceManager instance used by this class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend Shared ReadOnly Property ResourceManager() As Global.System.Resources.ResourceManager
            Get
                If Object.ReferenceEquals(resourceMan, Nothing) Then
                    Dim temp As Global.System.Resources.ResourceManager = New Global.System.Resources.ResourceManager("Resources.CreateQrWizard.aspx", Global.System.Reflection.[Assembly].Load("App_GlobalResources"))
                    resourceMan = temp
                End If
                Return resourceMan
            End Get
        End Property
        
        '''<summary>
        '''  Overrides the current thread's CurrentUICulture property for all
        '''  resource lookups using this strongly typed resource class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend Shared Property Culture() As Global.System.Globalization.CultureInfo
            Get
                Return resourceCulture
            End Get
            Set
                resourceCulture = value
            End Set
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Amount.
        '''</summary>
        Friend Shared ReadOnly Property Amount() As String
            Get
                Return ResourceManager.GetString("Amount", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Amount is required.
        '''</summary>
        Friend Shared ReadOnly Property Amountrequired() As String
            Get
                Return ResourceManager.GetString("Amountrequired", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Available.
        '''</summary>
        Friend Shared ReadOnly Property Available() As String
            Get
                Return ResourceManager.GetString("Available", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Quantity Available is required.
        '''</summary>
        Friend Shared ReadOnly Property Availablerequired() As String
            Get
                Return ResourceManager.GetString("Availablerequired", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Add a download link to your email confirmation .
        '''</summary>
        Friend Shared ReadOnly Property chkDownloadlnk() As String
            Get
                Return ResourceManager.GetString("chkDownloadlnk", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SELECT ALL.
        '''</summary>
        Friend Shared ReadOnly Property Copylink() As String
            Get
                Return ResourceManager.GetString("Copylink", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Credit Installments.
        '''</summary>
        Friend Shared ReadOnly Property CreditInstallments() As String
            Get
                Return ResourceManager.GetString("CreditInstallments", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to CreditType is required.
        '''</summary>
        Friend Shared ReadOnly Property CreditTyperequired() As String
            Get
                Return ResourceManager.GetString("CreditTyperequired", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Currency is required.
        '''</summary>
        Friend Shared ReadOnly Property Currencyrequired() As String
            Get
                Return ResourceManager.GetString("Currencyrequired", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Delete.
        '''</summary>
        Friend Shared ReadOnly Property Delete() As String
            Get
                Return ResourceManager.GetString("Delete", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Description (500 Characters).
        '''</summary>
        Friend Shared ReadOnly Property Description() As String
            Get
                Return ResourceManager.GetString("Description", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to * Quantity options list is defined by 3 fields:  The Start field defines the list starting number, The End field defines the list ending number, 
        '''				The Step field defines the step between each number in the list..
        '''</summary>
        Friend Shared ReadOnly Property Disclaimer() As String
            Get
                Return ResourceManager.GetString("Disclaimer", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Edit Photo.
        '''</summary>
        Friend Shared ReadOnly Property Editphoto() As String
            Get
                Return ResourceManager.GetString("Editphoto", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Width: 320px / Height 100px.
        '''</summary>
        Friend Shared ReadOnly Property Imagesize() As String
            Get
                Return ResourceManager.GetString("Imagesize", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to INSERT PRODUCTS.
        '''</summary>
        Friend Shared ReadOnly Property insertp() As String
            Get
                Return ResourceManager.GetString("insertp", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Installments.
        '''</summary>
        Friend Shared ReadOnly Property Installments() As String
            Get
                Return ResourceManager.GetString("Installments", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Link SQR:.
        '''</summary>
        Friend Shared ReadOnly Property LinkSqr() As String
            Get
                Return ResourceManager.GetString("LinkSqr", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Max.
        '''</summary>
        Friend Shared ReadOnly Property max() As String
            Get
                Return ResourceManager.GetString("max", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Min.
        '''</summary>
        Friend Shared ReadOnly Property min() As String
            Get
                Return ResourceManager.GetString("min", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Payment Type.
        '''</summary>
        Friend Shared ReadOnly Property PaymentType() As String
            Get
                Return ResourceManager.GetString("PaymentType", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Price.
        '''</summary>
        Friend Shared ReadOnly Property Price() As String
            Get
                Return ResourceManager.GetString("Price", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Product Name.
        '''</summary>
        Friend Shared ReadOnly Property ProductName() As String
            Get
                Return ResourceManager.GetString("ProductName", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Quantity To is required.
        '''</summary>
        Friend Shared ReadOnly Property Quantityrequired() As String
            Get
                Return ResourceManager.GetString("Quantityrequired", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Receipt Text.
        '''</summary>
        Friend Shared ReadOnly Property ReceiptText() As String
            Get
                Return ResourceManager.GetString("ReceiptText", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Allow customer to set amount.
        '''</summary>
        Friend Shared ReadOnly Property SetAmount() As String
            Get
                Return ResourceManager.GetString("SetAmount", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to SKU.
        '''</summary>
        Friend Shared ReadOnly Property SKU() As String
            Get
                Return ResourceManager.GetString("SKU", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Sort.
        '''</summary>
        Friend Shared ReadOnly Property Sort() As String
            Get
                Return ResourceManager.GetString("Sort", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Start.
        '''</summary>
        Friend Shared ReadOnly Property Start() As String
            Get
                Return ResourceManager.GetString("Start", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Quantity Start is required.
        '''</summary>
        Friend Shared ReadOnly Property Startrequired() As String
            Get
                Return ResourceManager.GetString("Startrequired", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Quantity Step is required.
        '''</summary>
        Friend Shared ReadOnly Property Steprequired() As String
            Get
                Return ResourceManager.GetString("Steprequired", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Stock Managment.
        '''</summary>
        Friend Shared ReadOnly Property StockManagment() As String
            Get
                Return ResourceManager.GetString("StockManagment", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Tags.
        '''</summary>
        Friend Shared ReadOnly Property Tags() As String
            Get
                Return ResourceManager.GetString("Tags", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Text is required.
        '''</summary>
        Friend Shared ReadOnly Property Textisrequired() As String
            Get
                Return ResourceManager.GetString("Textisrequired", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to upload photo.
        '''</summary>
        Friend Shared ReadOnly Property upload() As String
            Get
                Return ResourceManager.GetString("upload", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Step.
        '''</summary>
        Friend Shared ReadOnly Property _Step() As String
            Get
                Return ResourceManager.GetString("Step", resourceCulture)
            End Get
        End Property
    End Class
End Namespace
