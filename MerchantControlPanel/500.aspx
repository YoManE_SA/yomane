﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="500.aspx.vb" Inherits="Netpay.MerchantControlPanel._500" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>500 internal server error - <%= WebUtils.CurrentDomain.BrandName %></title>
    <style>
        * {
            margin: 0;
        }

        html, body, form {
            height: 100%;
        }

        body {
            background-color: #d2d1d0;
        }

        .wrapper {
            min-height: 100%;
            height: auto !important;
            height: 100%;
            margin: 0 auto -40px; /* the bottom margin is the negative value of the footer's height */
            width: 1000px;
        }

        .footer, .push {
            height: 40px;
            width: 1000px;
            margin: 0 auto; /* .push must be the same height as .footer */
        }

        h1 {
            padding: 5% 0 0 0;
            font-family: 'Open Sans';
            color: #22262E;
            margin: 0;
            font-size: 130px;
        }

        p {
            font-family: 'Open Sans';
            font-size: 18px;
            font-weight: 300;
            color: #22262E;
            text-align: center;
        }

        .copyright {
            font-size: 16px;
            color: #969696;
            padding-top: 10px;
        }

        .link-site {
            background-color: #003970;
            padding: 5px 10px;
            color: #fff;
            border-radius: 5px;
            text-decoration: none;
        }

            .link-site:hover {
                text-decoration: underline;
            }

        .description {
            text-align: left;
            padding-bottom: 20px;
            background: #fff;
            -webkit-border-radius: 30px;
            -moz-border-radius: 30px;
            border-radius: 30px;
            padding: 30px;
            text-align: center;
            margin-bottom: 20px;
        }
    </style>
</head>
<body>
    <form runat="server">
        <div class="wrapper">
            <img src="<%= WebUtils.MapTemplateVirPath(Me, "Images/404Logo.png")%>" />
            <header style="text-align: center;">
                <h1>500</h1>
            </header>
            <section>
                <p>internal server error </p>
                <br />
                <p class="description">
                    we have some server issues at the moment
                    <br />
                    please give us time to fix the problem.<br />
                    lean back and keep this tab open it will automatically reload.
                </p>
                <p>
                    <asp:HyperLink runat="server" NavigateUrl="~/Website/Login.aspx" CssClass="link-site" Text="Go back to our site" /></p>
            </section>
            <div class="push"></div>
        </div>
        <div class="footer">
            <p class="copyright">
                All rights reserved &copy; 2016 <%= WebUtils.CurrentDomain.BrandName %>
            </p>
        </div>
    </form>
</body>
</html>
