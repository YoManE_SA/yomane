﻿Imports Netpay.Infrastructure.Extensions

Partial Class ErrorPage
    Inherits System.Web.UI.Page

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblError.Text = Request("error").ToSentence()
    End Sub
End Class
