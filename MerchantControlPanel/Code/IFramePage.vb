﻿Public Class IFramePage
	Inherits NetpayPage

	Public Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreInit
		If Not IsAuthorized() Then
			Response.Write(String.Format("<script>parent.location.href = '{0}'</script>", Request.ApplicationPath))
			Response.End()
		End If
	End Sub
End Class

