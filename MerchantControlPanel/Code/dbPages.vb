Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web
Imports System.Xml
Imports Netpay.Infrastructure
Imports Netpay.Web

Public Class dbPages
	Public Shared Function getConfig(sKey As String) As String
		Return System.Configuration.ConfigurationManager.AppSettings.Item(sKey)
	End Function

	Public Shared ReadOnly Property DSN() As String
		Get
			Return WebUtils.CurrentDomain.Sql1ConnectionString
		End Get
	End Property

	Public Shared Function ExecSql(ByVal sqlStr As String) As Long
		Dim DBCon As New System.Data.SqlClient.SqlConnection(DSN)
		Dim DBCom As New System.Data.SqlClient.SqlCommand(sqlStr, DBCon)
		Try
			DBCon.Open()
			ExecSql = DBCom.ExecuteNonQuery()
		Catch ex As Exception
			Netpay.Infrastructure.Logger.Log(LogTag.MerchantControlPanel, ex)
			Throw New ApplicationException("An error has occured, see log.")
		Finally
			DBCon.Close()
			DBCom.Dispose()
			DBCon.Dispose()
		End Try
	End Function

	Public Shared Function ExecScalar(ByVal sqlStr As String) As Object
		Dim DBCon As New SqlConnection(DSN)
		Dim DBCom As New SqlCommand(sqlStr, DBCon)
		Try
			DBCon.Open()
			ExecScalar = DBCom.ExecuteScalar()
		Catch ex As Exception
			Netpay.Infrastructure.Logger.Log(LogTag.MerchantControlPanel, ex)
			Throw New ApplicationException("An error has occured, see log.")
		Finally
			DBCon.Close()
			DBCom.Dispose()
			DBCon.Dispose()
		End Try
	End Function

	Public Shared Function ExecReader(ByVal sqlStr As String) As SqlDataReader
		Dim DBCon As New SqlConnection(DSN)
		Dim DBCom As New SqlCommand(sqlStr, DBCon)
		Try
			DBCon.Open()
			ExecReader = DBCom.ExecuteReader(System.Data.CommandBehavior.CloseConnection)
			'Catch ex As Exception
			'Netpay.Infrastructure.Logger.Log(LogTag.MerchantControlPanel, ex)
			'Throw New ApplicationException("An error has occured, see log.")
		Finally
			DBCom.Dispose()
		End Try
	End Function

	Public Shared Function dbtextShow(ByVal sText As String) As String
		If sText Is Nothing Then Return ""
		Return sText.Trim.Replace("``", """").Replace("`", "'").Replace(vbCr, "<br />")
	End Function

	Public Shared Function TestVar(ByVal value As Object, ByVal min As Integer, ByVal max As Integer, ByVal defaultValue As Integer) As Integer
		Dim returnValue As Decimal = defaultValue
		If Not value Is Nothing Then
			If IsNumeric(value) Then
				Try
					returnValue = Integer.Parse(value)
					If (min <= max) And ((returnValue < min) Or (returnValue > max)) Then returnValue = defaultValue
				Catch
				End Try
			End If
		End If

		Return returnValue
	End Function

	Public Shared Function TestVar(ByVal value As Object, ByVal min As Decimal, ByVal max As Decimal, ByVal defaultValue As Decimal) As Decimal
		Dim returnValue As Decimal = defaultValue
		Try
			returnValue = Decimal.Parse(value.ToString())
			If ((min <= max) And ((returnValue < min) Or (returnValue > max))) Then returnValue = defaultValue
		Catch
		End Try

		Return returnValue
	End Function

	Public Shared Function TestVar(ByVal value As Object, ByVal max As Integer, ByVal defaultValue As String) As String
		Dim returnValue As String = defaultValue
		Try
			returnValue = value.ToString()
			If (returnValue.Length > max) Then returnValue = returnValue.Substring(0, max)
		Catch
		End Try

		Return returnValue
	End Function

	Public Shared Function TestVar(ByVal value As Object, ByVal min As Date, ByVal max As Date, ByVal defaultValue As Date) As Date
		Dim returnValue As Date = defaultValue
		Try
			returnValue = Date.Parse(value.ToString())
			If ((min <= max) And ((returnValue < min) Or (returnValue > max))) Then returnValue = defaultValue
		Catch
		End Try

		Return returnValue
	End Function

    Public Shared Function TestVar(ByVal xval As Object, ByVal lDef As Boolean) As Boolean
        Dim rVal As Boolean = lDef
        Try
            Boolean.TryParse(xval.ToString(), rVal)
        Catch
        End Try
        Return rVal
    End Function

	Public Shared Function ConvertCurrencyRate(ByVal currencyFrom As Integer, ByVal currencyTo As Integer) As Decimal
		Dim converted As Decimal
		If currencyFrom <> currencyTo Then
			converted = Bll.Currency.Get(currencyFrom).BaseRate / Bll.Currency.Get(currencyTo).BaseRate
		Else
			converted = 1
		End If

		Return converted
	End Function

	Public Shared Function ConvertCurrency(ByVal xCurFrom As Integer, ByVal xCurTo As Integer, ByVal xAmount As Decimal) As Decimal
		ConvertCurrency = ConvertCurrencyRate(xCurFrom, xCurTo) * xAmount
	End Function

	Public Shared Function GetCurText(ByVal trnCurr As Byte) As String
		Return Bll.Currency.Get(trnCurr).Symbol
	End Function

	Public Shared Function FormatDatesTimes(ByVal sString As String, ByVal bShowDate As Integer, ByVal bShowTime As Integer, ByVal bShowSec As Integer) As String
		Dim sDateTime As String = ""
		If bShowDate = 1 Then
			If Day(sString) < 10 Then sDateTime = sDateTime & "0"
			sDateTime = sDateTime & Day(sString) & "/"
			If Month(sString) < 10 Then sDateTime = sDateTime & "0"
			sDateTime = sDateTime & Month(sString) & "/" & Right(Year(sString), 2)
		End If
		If bShowDate = 1 And bShowTime = 1 Then sDateTime = sDateTime & "&nbsp;&nbsp;"
		If bShowTime = 1 Then
			If Hour(sString) < 10 Then sDateTime = sDateTime & "0"
			sDateTime = sDateTime & Hour(sString) & ":"
			If Minute(sString) < 10 Then sDateTime = sDateTime & "0"
			sDateTime = sDateTime & Minute(sString)
			If bShowSec = 1 Then sDateTime = sDateTime & ":" & Second(sString)
		End If
		FormatDatesTimes = sDateTime
	End Function

	Public Shared Function FormatCurrWCT(ByVal trnCurr As Byte, ByVal trnValue As Decimal, ByVal crType As Byte) As String
		If CInt(crType) = 0 Then trnValue = -trnValue
		FormatCurrWCT = FormatCurr(trnCurr, trnValue)
	End Function

	Public Shared Function FormatCurr(ByVal trnCurr As Byte, ByVal trnValue As Decimal) As String
		FormatCurr = GetCurText(trnCurr) & FormatNumber(trnValue, 2, TriState.True, TriState.False, TriState.True)
	End Function

	Public Shared Function ExecDataset(ByVal sqlStr As String, ByVal lPageSize As Integer, ByVal lPage As Integer) As System.Data.DataSet
		Dim DBCon As New SqlConnection(DSN)
		Dim iAdpt As New SqlDataAdapter(sqlStr, DBCon)
		Dim pRet As New System.Data.DataSet()
		Try
			DBCon.Open()
			If lPageSize <= 0 Then
				iAdpt.Fill(pRet)
			Else
				iAdpt.Fill(pRet, lPageSize * lPage, lPageSize + 1, "Table1")
			End If
		Catch ex As Exception
			Netpay.Infrastructure.Logger.Log(LogTag.MerchantControlPanel, ex)
			Throw New ApplicationException("An error has occured, see log.")
		Finally
			DBCon.Close()
		End Try
		Return pRet
	End Function

	Public Shared Function SetUrlValue(ByVal qString As String, ByVal strParam As String, ByVal sValue As String) As String
		Dim idx As Integer, idxEnd As Integer
		SetUrlValue = qString
		idx = InStr(1, SetUrlValue, strParam & "=")
		If (idx < 1) Then
			If sValue = Nothing Then Return qString
			Return qString & "&" & strParam & "=" & sValue
		End If
		idxEnd = InStr(idx, SetUrlValue, "&")
		If (idxEnd < 1) Then idxEnd = Len(SetUrlValue)
		SetUrlValue = Left(SetUrlValue, idx - 1) & Right(SetUrlValue, Len(SetUrlValue) - idxEnd) & "&" & strParam & "=" & sValue
	End Function

	Public Shared Function GetFileText(ByVal sFileName As String) As String
		Dim output As String = ""
		If System.IO.File.Exists(sFileName) Then
			Dim sr As New System.IO.StreamReader(sFileName)
			output = sr.ReadToEnd()
			sr.Close()
		End If
		Return output
	End Function

	Public Shared Function SendHttpRequest(ByVal sUrl As String, ByRef sReturnText As String, Optional ByVal sPostData As String = Nothing) As Integer
		Try
			Dim loHttp As System.Net.HttpWebRequest = System.Net.WebRequest.Create(sUrl)
			If Not String.IsNullOrEmpty(sPostData) Then
				loHttp.Method = "POST"
				Dim bytesPostData As Byte() = Encoding.ASCII.GetBytes(sPostData)
				loHttp.ContentType = "application/x-www-form-urlencoded"
				loHttp.ContentLength = bytesPostData.Length
				loHttp.GetRequestStream.Write(bytesPostData, 0, bytesPostData.Length)
				loHttp.GetRequestStream.Close()
			End If
			Dim loWebResponse As System.Net.HttpWebResponse = loHttp.GetResponse()
			Dim enc As Encoding = Encoding.GetEncoding(1252) ' Windows default Code Page
			Dim loResponseStream As System.IO.StreamReader = New System.IO.StreamReader(loWebResponse.GetResponseStream(), enc)
			If loWebResponse.StatusCode = Net.HttpStatusCode.OK Then
				sReturnText = loResponseStream.ReadToEnd()
			Else
				sReturnText = loWebResponse.StatusDescription
			End If
			loWebResponse.Close()
			loResponseStream.Close()
			Return loWebResponse.StatusCode
		Catch e As System.Net.WebException
			If e.Response Is Nothing Then Return -1
			Return CType(e.Response, System.Net.HttpWebResponse).StatusCode
		End Try
		Return 0
	End Function

	Public Shared Function GetXmlNodeAttributeValue(ByVal node As XmlNode, ByVal attributeName As String) As String
		Dim attribute As XmlNode = node.Attributes.GetNamedItem(attributeName)
		If attribute Is Nothing Then
			Return String.Empty
		Else
			Return attribute.Value
		End If
	End Function

	Public Shared Function GetUrlValue(ByVal sUrl As String, ByVal sValue As String) As String
		Dim sIndex As Integer = sUrl.IndexOf(sValue & "=", System.StringComparison.OrdinalIgnoreCase)
		If sIndex < 0 Then Return ""
		sIndex = sIndex + sValue.Length + 1
		Dim eIndex As Integer = sUrl.IndexOf("&", sIndex, System.StringComparison.OrdinalIgnoreCase)
		If eIndex < 0 Then eIndex = sUrl.Length
		Return sUrl.Substring(sIndex, eIndex - sIndex)
	End Function

	'---------------------------------------------------------------------------------------------------
	' checks the validity of a cc number using LUHN Mod 10 algorithm
	' sCCNum: the card number to check
	' return Bollean idf CreditCard is OK or not
	'---------------------------------------------------------------------------------------------------
	Public Shared Function ValidCreditCard(ByVal cNum As String) As Integer
		Dim total As Integer = 0, TempMultiplier As Integer = 0
		For i As Integer = cNum.Length To 2 Step -2
			total = total + CInt(cNum.Chars(i - 1).ToString())
			TempMultiplier = CInt(cNum.Chars(i - 2).ToString()) * 2
			total = total + CInt(TempMultiplier.ToString().Chars(0).ToString())
			If TempMultiplier.ToString().Length > 1 Then _
			  total = total + CInt(Right(TempMultiplier.ToString(), 1))
		Next
		If (cNum.Length Mod 2) = 1 Then total = total + CInt(cNum.Chars(1).ToString())
		Return total
	End Function

	Public Shared Function IsValidCreditCard(ByVal cNum As String) As Boolean
		Return (ValidCreditCard(cNum) Mod 10) = 0
	End Function

End Class
