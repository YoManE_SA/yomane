Imports Microsoft.VisualBasic
Imports Netpay.Bll
Imports Netpay.Infrastructure
Imports System.Collections.Generic
Imports Netpay.Web.Controls
Imports System.Linq
Imports Netpay.Web

Public Class SecurityModule 
	Inherits Infrastructure.Module 

	Public Overrides ReadOnly Property Name As String
		Get
			Return "MCP.Security"
		End Get
	End Property

	Public Shared ReadOnly Property Current as SecurityModule 
		Get
			Return Infrastructure.Module.Get("MCP.Security")
		End Get
	End Property

    Public Overrides ReadOnly Property Version As Decimal
        Get
            Return 1D
        End Get
    End Property

    Public Shared Function GetSecuredObject(name As String, Optional addIfNotExist As Boolean = False) As Infrastructure.Security.SecuredObject
        'name = name.ToLower()
        Dim obj = Infrastructure.Security.SecuredObject.Get(SecurityModule.Current, name)
        If obj Is Nothing And addIfNotExist Then obj = Infrastructure.Security.SecuredObject.Create(SecurityModule.Current, name, Security.PermissionGroup.Execute)
        Return obj
    End Function

    Public Shared Function GetPermissionValue(values As Dictionary(Of Integer, Infrastructure.Security.PermissionValue?), name As String) As Infrastructure.Security.PermissionValue 
		Dim sobj = GetSecuredObject(name)
		If sobj Is Nothing Then Return Security.PermissionValue.None
		If Not values.ContainsKey(sobj.ID) Then Return Security.PermissionValue.None
		Return values(sobj.ID).GetValueOrDefault(Security.PermissionValue.None)
	End Function

End Class

Public Class NetpayPage
	Inherits BasePage

	Private Shared _freeAccessPages As List(Of String) = Nothing
    Private Shared _limitedUserFreeAccessPages As List(Of String) = Nothing
    Private Shared _synclock As Object = New Object

    Private Shared ReadOnly Property FreeAccessPages() As List(Of String)
		Get
            If _freeAccessPages Is Nothing Then
                SyncLock _synclock
                    If _freeAccessPages Is Nothing Then
                        _freeAccessPages = New List(Of String)
                        _freeAccessPages.Add("login.aspx")
                        _freeAccessPages.Add("contactus.aspx")
                        _freeAccessPages.Add("frequentlyaskedquestions.aspx")
                        _freeAccessPages.Add("faq.aspx")
                        _freeAccessPages.Add("content.aspx")
                        _freeAccessPages.Add("contentpage.aspx")
                        _freeAccessPages.Add("merchantregistration.aspx")
                    End If
                End SyncLock
            End If

            Return _freeAccessPages
		End Get
	End Property

	Private Shared ReadOnly Property LimitedUserFreeAccessPages() As List(Of String)
		Get
            If _limitedUserFreeAccessPages Is Nothing Then
                SyncLock _synclock
                    If _limitedUserFreeAccessPages Is Nothing Then
                        _limitedUserFreeAccessPages = New List(Of String)
                        _limitedUserFreeAccessPages.Add("default.aspx".ToLower())
                        _limitedUserFreeAccessPages.Add("TransDetails.aspx".ToLower())
                        _limitedUserFreeAccessPages.Add("FeatureDisabled.aspx".ToLower())
                        _limitedUserFreeAccessPages.Add("CreateQrWizard.aspx".ToLower())
                    End If
                End SyncLock
            End If

            Return _limitedUserFreeAccessPages
        End Get
	End Property

	Public Overridable Sub Page_PreInit(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreInit
		If Not IsAuthorized Then
            Response.End()
        End If
		ClientScript.RegisterClientScriptBlock(Me.GetType(), "checkFrame", "if(top.location != window.location) top.location.href = window.location.href;", True)
    End Sub
	
	Public Function IsAuthorized() As Boolean
		Dim pageFilePath As String = System.IO.Path.GetFileName(Request.Path)	
        Return IsAuthorized(pageFilePath)
	End Function

    Public Function IsAuthorized(pageFileName As String) As Boolean
        pageFileName = pageFileName.Trim().ToLower()
        If FreeAccessPages.Contains(pageFileName) Then Return True
        If Not IsLoggedin Then Return False
        If LimitedLogin Is Nothing Then Return True
        If LimitedUserFreeAccessPages.Contains(pageFileName) Then Return True
        Dim objectName = IIf(pageFileName.StartsWith("showtotals"), "ShowTotals", pageFileName)
        Dim obj = Infrastructure.Security.SecuredObject.Get(SecurityModule.Current, objectName)
        Return obj.HasPermission(Security.PermissionValue.Execute)
    End Function

    Public ReadOnly Property Domain As Netpay.Infrastructure.Domain  
		Get
			Return Netpay.Infrastructure.Domain.Current
		End Get
	End Property

    Public ReadOnly Property Account As Bll.Accounts.Account
        Get
            Return Bll.Accounts.Account.Current
        End Get
    End Property

    Public ReadOnly Property LimitedLogin As Bll.Accounts.LimitedLogin
        Get
            Return Bll.Accounts.LimitedLogin.Current
        End Get
    End Property

    Public ReadOnly Property Merchant As Bll.Merchants.Merchant
        Get
            Return Bll.Merchants.Merchant.Current
        End Get
    End Property


End Class
