﻿Public Class MasteredPage 
	Inherits NetpayPage
	
	'Protected Property IsMobile As Boolean

    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
		If Not IsAuthorized() Then Response.Redirect("~/WebSite/Login.aspx", True)
        If IsMobile Then Page.MasterPageFile = MapTemplateVirPath("") & "/Mobile.master" _
        Else Page.MasterPageFile = MapTemplateVirPath("") & "/page.master"
        'If Not String.IsNullOrEmpty(Title) Then Title = GetLocalResourceObject(Title)
        MyBase.OnPreInit(e)
    End Sub

	Protected Overrides Sub OnPreRender(e As System.EventArgs)
		Dim obj as Localize = Page.FindControl("ctl00:cphTitle:locPageTitle")
		If obj IsNot Nothing Then Page.Title = obj.Text
		'Page.Title = CType(GetLocalResourceObject("PageTitle"), String)
		MyBase.OnPreRender(e)
	End Sub

End Class
