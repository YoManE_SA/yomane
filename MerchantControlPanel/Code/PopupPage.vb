﻿Public Class PopupPage
	Inherits NetpayPage
 
  	Public Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreInit
		If Not IsAuthorized then 
			Response.Write(string.Format("<script>top.opener.top.location.href = '{0}';top.window.close();</script>", Request.ApplicationPath))
			Response.End
		End If
    End Sub       
End Class
