﻿Imports Netpay.Web
Public Class MerchantInfo
    Inherits System.Web.UI.UserControl

	Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        If WebUtils.IsLoggedin And Not String.IsNullOrEmpty(Request("SwitchMerchant")) Then
            If Request("SwitchMerchant") <> Bll.Accounts.Account.Current.AccountNumber Then
                Dim newCred As Guid
                Dim lr = Bll.Merchants.Merchant.Current.LoginLinkedMerchant(Request("SwitchMerchant"), newCred)
                If lr = Infrastructure.Security.LoginResult.Success Then
                    WebUtils.Login(newCred)
                Else
                    Dim errorMessage As String = "<span style=""font-size:14px;"">Switching account has failed</span><br/><br/>" + Replace(CType(GetGlobalResourceObject("menu", Infrastructure.Security.LoginResult.WrongPassword.ToString()), String), vbCrLf, " ")
                    Dim loginErrorScript As String = "netpay.Common.alert('<img src=""/NPCommon/images/alert_small.png"" align=""" + WebUtils.CurrentAlignment + """>" + errorMessage.Replace("'", "\'") + "', '" + Bll.Merchants.Merchant.Current.GetLinkedMerchants()(Request("SwitchMerchant")) + "');"
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "MerchantLinkAuto", loginErrorScript, True)
                End If
            End If
        End If
		MyBase.OnInit(e)
	End Sub

	Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
        If WebUtils.IsLoggedin Then
            If Bll.Merchants.Merchant.Current IsNot Nothing Then

                If String.IsNullOrEmpty(Bll.Merchants.Merchant.Current.LinkName) Or Bll.Accounts.LimitedLogin.Current IsNot Nothing Then
                    ddlMerchantLink.Visible = False
                    merchantInfo.Text = String.Format("{0} - {1} &nbsp;&nbsp;", Bll.Accounts.Account.Current.AccountNumber, WebUtils.LoggedUser.UserName)
                    'merchantInfo.Text = String.Format("{0} {1} &nbsp;&nbsp;", Resources.Menu.MerchantNumber, WebUtils.LoggedUser.Number)
                Else
                    merchantInfo.Text = Resources.Menu.MerchantNumber
                    ddlMerchantLink.DataSource = Bll.Merchants.Merchant.Current.GetLinkedMerchants()
                    ddlMerchantLink.DataTextField = "Value"
                    ddlMerchantLink.DataValueField = "Key"
                    ddlMerchantLink.DataBind()
                    ddlMerchantLink.SelectedValue = Bll.Accounts.Account.Current.AccountNumber
                End If
            End If

        End If
        MyBase.OnPreRender(e)
	End Sub

End Class