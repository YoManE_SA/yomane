﻿Imports System.Xml
Imports Netpay.Web
Imports Netpay.Infrastructure

Public Class SiteMap
	Inherits System.Web.UI.UserControl

	'Private _width As String = ""
	'Public Property Width() As String
	'	Get
	'		Return _width
	'	End Get
	'	Set(ByVal value As String)
	'		_width = value
	'	End Set
	'End Property

	Protected Sub siteMapAsBulletedList_OnItemDataBound(sender As Object, e As RepeaterItemEventArgs)
		Dim currentMenuLink As HyperLink = CType(e.Item.FindControl("hlMenuItem"), HyperLink)

		If Not currentMenuLink Is Nothing Then
			Dim currentElement As System.Xml.XmlElement = CType(e.Item.DataItem, System.Xml.XmlElement)
			Dim isMenuItem As Boolean = Boolean.Parse(currentElement.Attributes("isMenuItem").Value)
			Dim url As XmlAttribute = currentElement.Attributes("url")
			Dim title As XmlAttribute = currentElement.Attributes("title")

			If isMenuItem Then
				currentMenuLink.Text = GetGlobalResourceObject("menu", title.Value)
				If currentElement.HasChildNodes Then
					currentMenuLink.ForeColor = Drawing.Color.Black
				Else
					If url Is Nothing Then
						currentMenuLink.Enabled = False
					Else
						Dim pageFileName As String = System.IO.Path.GetFileName(url.Value)
						currentMenuLink.Enabled = CType(Me.Page, NetpayPage).IsAuthorized(pageFileName)
						currentMenuLink.NavigateUrl = url.Value
						If Not currentMenuLink.Text.StartsWith("<span>") Then currentMenuLink.Text = "<span>" & currentMenuLink.Text & "</span>"
					End If
				End If
			Else
				currentMenuLink.Visible = False
			End If
		End If
	End Sub
End Class