﻿Imports Netpay.Web.Controls

Partial Class Recurring
	Inherits System.Web.UI.UserControl

	Dim sAmount As String
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
		ddlCharge.Items.Add(String.Empty)
		For i As Integer = 1 To 52
			ddlCharge.Items.Add(i)
		Next
		ddlInterval.Items.Add(String.Empty)
		For i As Integer = 1 To 52
			ddlInterval.Items.Add(i)
		Next
	End Sub

	Private txtRecurring As String = ""
	Public ReadOnly Property AddRecurring() As String
		Get
			sAmount = "A" + txtAmount.Text
			If sAmount = "A" Then
				txtRecurring = ddlCharge.SelectedValue + ddlGapUnit.SelectedValue + ddlInterval.SelectedValue
			Else
				txtRecurring = ddlCharge.SelectedValue + ddlGapUnit.SelectedValue + ddlInterval.SelectedValue + sAmount
			End If

			Return txtRecurring
		End Get
	End Property

	Private bValidate As Boolean = False
	Public ReadOnly Property CheckValidate() As Boolean
		Get
			If ddlCharge.SelectedValue = "" Or ddlGapUnit.SelectedValue = "" Or ddlInterval.SelectedValue = "" Then
				bValidate = False
				imgMsg.ImageUrl = "/NPCommon/Images/icon_x.gif"
				imgMsg.Visible = True
			Else
				bValidate = True
				imgMsg.Visible = True
				imgMsg.ImageUrl = "/NPCommon/Images/icon_v.gif"
			End If
			If ddlCharge.SelectedValue = "" And ddlGapUnit.SelectedValue = "" And ddlInterval.SelectedValue = "" Then
				bValidate = True
				imgMsg.Visible = False
			End If
			Return bValidate
		End Get
	End Property

	Private bIsTopHeader As Boolean = True
	Public Property IsTopHeader() As Boolean
		Get
			Return bIsTopHeader
		End Get
		Set(ByVal value As Boolean)
			bIsTopHeader = value
			If bIsTopHeader = False Then
				Top_Header.Visible = False
			Else
				Top_Header.Visible = True
			End If
		End Set
	End Property
End Class