﻿Imports Netpay.Web
Imports Netpay.Infrastructure

Public Class Counter
    Inherits UserControl
    Implements ICallbackEventHandler

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        If Not IsPostBack Then
            Dim cm As ClientScriptManager = Page.ClientScript
            Dim reference As String = cm.GetCallbackEventReference(Me, "arg", "HandleResult", "")
            Dim script As String = "function CallServer(arg, context){" + reference + ";}"
            cm.RegisterClientScriptBlock(Me.GetType(), "CallServer", script, True)
        End If
    End Sub

    Private returnString As String = Nothing
    Public Sub RaiseCallbackEvent(eventArgument As String) Implements ICallbackEventHandler.RaiseCallbackEvent
        Dim argsSplit = eventArgument.Split(",")
        Dim command = argsSplit(0)
        If Bll.Merchants.Merchant.Current Is Nothing Then Exit Sub
        Select Case command
            Case "getCurrencies"
                Dim totals = Bll.Merchants.Merchant.GetMonthlyTotalDebit(Bll.Merchants.Merchant.Current.ID)
                If totals.Count > 0 Then
                    returnString = "getCurrencies," + totals.Select(Function(t) t.Key).ToDelimitedString()
                Else
                    returnString = "getCurrencies"
                End If
            Case "getTotals"
                Dim currency As CommonTypes.Currency = DirectCast([Enum].Parse(GetType(CommonTypes.Currency), argsSplit(1)), CommonTypes.Currency)
                Dim totals = Bll.Merchants.Merchant.GetMonthlyTotalDebit(Bll.Merchants.Merchant.Current.ID)

                Dim currencyTotal As Decimal = totals.Where(Function(t) t.Key = currency).SingleOrDefault().Value
                Dim currencyTotalString As String = currencyTotal.ToString("000,000.00")
                Dim images As List(Of String) = New List(Of String)
                For Each currentChar As Char In currencyTotalString.ToCharArray()
                    Dim currentCharStr As String = currentChar.ToString()
                    If currentCharStr = "." Then currentCharStr = "dot"
                    If currentCharStr = "," Then currentCharStr = "sign"
                    images.Add(String.Format("../Website/images/Counter/{0}.png", currentCharStr))
                Next
                returnString = "getTotals," + images.ToDelimitedString()
            Case Else
                Exit Sub
        End Select
    End Sub

    Public Function GetCallbackResult() As String Implements ICallbackEventHandler.GetCallbackResult
        Return returnString
    End Function
End Class
