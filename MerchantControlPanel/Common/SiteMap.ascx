﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="SiteMap.ascx.vb" Inherits="Netpay.MerchantControlPanel.SiteMap" %>
<%@ Import Namespace="Netpay.MerchantControlPanel" %>
<table width="100%" border="0">
	<tr>
		<td>
			<asp:Repeater ID="siteMapAsBulletedList" EnableViewState="false" OnItemDataBound="siteMapAsBulletedList_OnItemDataBound" runat="server" DataSourceID="sitemapDatasource">
				<ItemTemplate>
					<table cellpadding="0" cellspacing="0">
						<tr>		
							<asp:Repeater runat="server" ID="siteMapColumns" DataSource='<%# XPathSelect("./*") %>'>
								<ItemTemplate>
									<td valign="top">
										<asp:Repeater OnItemDataBound="siteMapAsBulletedList_OnItemDataBound" runat="server" ID="siteMapColumn" DataSource='<%# XPathSelect("./*") %>'>
											<ItemTemplate>
												<asp:HyperLink ID="hlMenuItem" Font-Bold="true" ForeColor="Black" style="white-space:nowrap" runat="server" />
												<asp:Repeater OnItemDataBound="siteMapAsBulletedList_OnItemDataBound" runat="server" id="SecondLevel"  DataSource='<%# XPathSelect("./*") %>'>
													<ItemTemplate>
														<table>
															<tr>
																<td>
																	<asp:HyperLink ID="hlMenuItem" runat="server" CssClass="MenuItem1" />
																	<asp:Repeater OnItemDataBound="siteMapAsBulletedList_OnItemDataBound" runat="server" id="treeLevel" DataSource='<%# XPathSelect("./*") %>'>
																		<ItemTemplate>
																			<table>
																				<tr>
																					<td>
																						<asp:HyperLink ID="hlMenuItem" runat="server" CssClass="MenuItem2" />
																					</td>
																				</tr>
																			</table>
																		</ItemTemplate>
																	</asp:Repeater>																					
																</td>
															</tr>
														</table>
													</ItemTemplate>
												</asp:Repeater>
												<br />
											</ItemTemplate>
										</asp:Repeater>	
									</td>
								</ItemTemplate>		
							</asp:Repeater>
						</tr>
					</table>
				</ItemTemplate>
			</asp:Repeater>
		</td>
	</tr>
</table>
<asp:XmlDataSource ID="sitemapDatasource" DataFile="~/Web.sitemap" runat="server" />
