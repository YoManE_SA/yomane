﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Recurring.ascx.vb" Inherits="Netpay.MerchantControlPanel.Recurring" %>

<table border="0" width="100%">
	<tr id="Top_Header" runat="server">
		<td>
			<asp:Literal ID="Literal1" runat="server" Text="<%$Resources:MultiLang,Agreement %>"></asp:Literal>:
		</td>
		<td>
			<asp:Literal ID="Literal2" runat="server" Text="<%$Resources:MultiLang,Interval %>"></asp:Literal>:
		</td>
		<td>
			<asp:Literal ID="Literal3" runat="server" Text="<%$Resources:MultiLang,Amount %>"></asp:Literal>:
		</td>
	</tr>
	<tr>
		<td style="white-space: nowrap;">
			<asp:DropDownList Width="70px" ID="ddlCharge" runat="server">
			</asp:DropDownList>
		</td>
		<td style="white-space: nowrap;">
			<asp:DropDownList ID="ddlInterval" runat="server">
			</asp:DropDownList>
			<asp:DropDownList ID="ddlGapUnit" runat="server">
				<asp:ListItem></asp:ListItem>
				<asp:ListItem Text="day(s)" Value="D"></asp:ListItem>
				<asp:ListItem Text="week(s)" Value="W"></asp:ListItem>
				<asp:ListItem Text="month(s)" Value="M"></asp:ListItem>
				<asp:ListItem Text="Quarter(s)" Value="Q"></asp:ListItem>
				<asp:ListItem Text="year(s)" Value="Y"></asp:ListItem>
			</asp:DropDownList>
		</td>
		<td>
			<asp:TextBox ID="txtAmount" Width="100px" runat="server"></asp:TextBox>
		</td>
		<td width="100%" />
		<asp:Image ID="imgMsg" runat="server" Visible="false" />
	</tr>
</table>