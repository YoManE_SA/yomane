﻿Imports System.Drawing
Imports System.Drawing.Text

Public Class Captcha
	Inherits System.Web.UI.UserControl

	Private Function generateVCode(ByVal CodeLength As Integer) As String
		Dim VCode As String = String.Empty
		Dim randObj As New Random()
		Dim c As Integer = 63
		For i As Byte = 1 To CodeLength
			c = randObj.Next(35)
			c += IIf(c < 10, 48, 55)
			VCode += Chr(c)
		Next
		Return VCode.Replace("0", "D").Replace("O", "B")
	End Function

	Private Function generateHatchStyle() As Drawing2D.HatchStyle
		Dim sList As New ArrayList
		For Each style As Drawing2D.HatchStyle In System.Enum.GetValues(GetType(Drawing2D.HatchStyle))
			sList.Add(style)
		Next
		Dim randObj As New Random()
		Dim index As Integer = randObj.Next(sList.Count - 1)
		Return CType(sList(index), Drawing2D.HatchStyle)
	End Function

    Private Sub SendImage(value As String)
        Using oBitmap As Bitmap = New Bitmap(120, 32)
            Using oGraphic As Graphics = Graphics.FromImage(oBitmap)
                Dim foreColor As Color = Color.FromArgb(220, 220, 220)
                Dim backColor As Color = Color.FromArgb(190, 190, 190)
                Using oBrush As New Drawing2D.HatchBrush(CType(generateHatchStyle(), Drawing2D.HatchStyle), foreColor, backColor)
                    oGraphic.FillRectangle(oBrush, 0, 0, 120, 32)
                End Using
                oGraphic.TextRenderingHint = TextRenderingHint.AntiAlias
                Using oBrushWrite As New SolidBrush(Color.Maroon)
                    Using oFont As New Font("Comic Sans MS", 17, FontStyle.Italic)
                        Dim oPoint As New PointF(5.0F, 0.0F)
                        oGraphic.DrawString(value, oFont, oBrushWrite, oPoint)
                    End Using
                End Using
                For i As Integer = 4 To 32 Step 8
                    oGraphic.DrawLine(Pens.Black, 0, i, 120, i)
                Next
                For i As Integer = 5 To 120 Step 10
                    oGraphic.DrawLine(Pens.Black, i, 0, i, 32)
                Next
            End Using
            Response.ContentType = "image/jpeg"
            oBitmap.Save(Response.OutputStream, Imaging.ImageFormat.Jpeg)
        End Using
    End Sub

    Private Function GetHashOfValue(value As String)
        Return Infrastructure.Security.Encryption.GetSHA256Hash(String.Format("CAPTCA!_{0}_!CAPTCA", value.ToLower().Trim()))
    End Function

    Public Function Validate(value As String) As Boolean?
        Dim savedValue As String = Web.WebUtils.GetGlobalParameter(ClientID + "_Value") 'get from cookie
        If savedValue Is Nothing Then Return Nothing
        Return (savedValue = GetHashOfValue(value))
    End Function

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        If Request("Get" & ClientID) = "1" Then
            Page.Response.ClearHeaders()
            Page.Response.Clear()
            Dim value As String = generateVCode(6)
            Web.WebUtils.SetGlobalParameter(ClientID + "_Value", GetHashOfValue(value)) 'save on cookie
            'Infrastructure.Security.Login.Current.SetSessionItem("Captcha_" & ID, )
            SendImage(value)
            Response.End()
        End If
        MyBase.OnInit(e)
    End Sub
End Class