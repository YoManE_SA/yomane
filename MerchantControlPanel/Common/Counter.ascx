﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Counter.ascx.vb" Inherits="Netpay.MerchantControlPanel.Counter" %>

<script type="text/javascript">
    function HandleResult(arg, context) {
        var args = arg.split(',');
        var command = args.shift();
        switch (command) {
            case "getCurrencies":
                if (args.length > 0)
                {
                    var currencySelect = $("#ddlCurrencies");
                    currencySelect.children().remove()
                    for (var i = 0; i < args.length; i++)
                        $(currencySelect).append('<option value="' + args[i] + '">' + args[i] + '</option>');
                    currencySelect.change(getTotals);
                    getTotals();
                }
                else
                {
                    $("#divContent").hide();
                }
                break;
            case "getTotals":
                $('#divCounterImages').empty();
                $(args).each(function () {
                    $('#divCounterImages').append('<img src="' + this + '" />')
                });
                break;
            default:
                break;
        }
    }

    function getCurrencies() {
        CallServer("getCurrencies", null)
    }

    function getTotals() {
        var currency = $("#ddlCurrencies").val();
        CallServer("getTotals," + currency, null)
    }

    function checkPostbacks()
    {
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm.get_isInAsyncPostBack())
            window.setTimeout(checkPostbacks, 1000);
        else
            getCurrencies();
    }

    $(function () {
        checkPostbacks();
    });

</script>

<div id="divContent" class="box-counter">
    <div class="counter">
        <div class="text-counter">
            <div class="counter-total">
                <asp:Literal ID="ltCounterText" runat="server" Text="<%$Resources:MultiLang,MerchantMontlyTotal %>" />
                <select id="ddlCurrencies" class="CurrencyDropDown"></select>
            </div>
        </div>
        <div class="counterplugin">
            <div id="divCounterImages"></div>
            <div class="counter-comment">
                <asp:Literal ID="ltCounterComment" runat="server" Text="<%$Resources:MultiLang,Countercomment %>" />
            </div>
        </div>
        <div class="spacer">
        </div>
    </div>
</div>
