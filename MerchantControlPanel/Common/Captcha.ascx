﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Captcha.ascx.vb" Inherits="Netpay.MerchantControlPanel.Captcha" %>
<script language="javascript" type="text/javascript">
	var nAttempt=0;
	function ChangeImage<%= Me.ClientID %>()
	{
		document.getElementById("img_<%=Me.ClientID%>").src="?Get<%=ClientID%>=1&Attempt="+(++nAttempt);
	}
</script>
    <div style="display: inline-table; vertical-align: middle;">
      <img id="img_<%=Me.ClientID%>" src="?Get<%=ClientID%>=1" style="vertical-align: middle;" alt="Type this text into the text field" />
      <a href="javascript:ChangeImage<%= Me.ClientID %>()"><asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:CaptchaChangeImageLink %>" /></a>
      </div>
