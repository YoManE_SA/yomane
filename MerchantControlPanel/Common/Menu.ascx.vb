﻿Imports System.Xml
Imports Netpay.Web

Public Class Menu
	Inherits System.Web.UI.UserControl

	Protected _menuMax As Integer = 1
    'Protected _menuXmlUrl As String = "../Web.sitemap"

	Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
		If WebUtils.LoggedUser IsNot Nothing And Not String.IsNullOrEmpty(Request("SwitchMerchant")) Then
			If Request("SwitchMerchant") <> Bll.Accounts.Account.Current.AccountNumber Then
				Dim newCred As Guid
                Dim lr As Infrastructure.Security.LoginResult = Bll.Merchants.Merchant.Current.LoginLinkedMerchant(Request("SwitchMerchant"), newCred)
                If lr = Infrastructure.Security.LoginResult.Success Then
                    WebUtils.Login(newCred)
                Else
                    Dim errorMessage As String = "<span style=""font-size:14px;"">Switching account has failed</span><br/><br/>" + Replace(CType(GetGlobalResourceObject("menu", lr.ToString()), String), vbCrLf, " ")
                    Dim loginErrorScript As String = "netpay.Common.alert('<img src=""/NPCommon/images/alert_small.png"" align=""" + WebUtils.CurrentAlignment + """>" + errorMessage.Replace("'", "\'") + "', '" + Bll.Merchants.Merchant.Current.GetLinkedMerchants()(Request("SwitchMerchant")) + "');"
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "MerchantLinkAuto", loginErrorScript, True)
                End If
			End If
		End If
		MyBase.OnInit(e)
	End Sub

    Public ReadOnly Property MenuXmlUrl() As String
        Get
            Return Request.PhysicalApplicationPath + "\Web.sitemap"
        End Get
        'Get
        '	If String.IsNullOrEmpty(_menuXmlUrl) Then
        '		Return ""
        '	ElseIf _menuXmlUrl.Contains("://") Then
        '		Return _menuXmlUrl
        '	Else
        '		Return Server.MapPath(_menuXmlUrl)
        '	End If
        'End Get
        'Set(ByVal value As String)
        '	_menuXmlUrl = value
        'End Set
    End Property

	Protected _useLocalResource As Boolean = False
	Public Property UseLocalResource() As Boolean
		Get
			Return _useLocalResource
		End Get
		Set(ByVal value As Boolean)
			_useLocalResource = value
			If value Then _useGlobalResource = False
		End Set
	End Property

	Protected _useGlobalResource As Boolean = False
	Public Property UseGlobalResource() As Boolean
		Get
			Return _useGlobalResource
		End Get
		Set(ByVal value As Boolean)
			_useGlobalResource = value
			If value Then _useLocalResource = False
		End Set
	End Property

	Protected _globalResourceClassName As String = "Menu"
	Public Property GlobalResourceClassName() As String
		Get
			Return _globalResourceClassName
		End Get
		Set(ByVal value As String)
			_globalResourceClassName = value
		End Set
	End Property

	Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
		If WebUtils.LoggedUser IsNot Nothing Then
            If String.IsNullOrEmpty(Bll.Merchants.Merchant.Current.LinkName) Or Bll.Accounts.LimitedLogin.Current IsNot Nothing Then
                ddlMerchantLink.Visible = False
                merchantNo.Text = String.Format("{0} {1} &nbsp;&nbsp;", Resources.Menu.MerchantNumber, Bll.Accounts.Account.Current.AccountNumber)
            Else
                merchantNo.Text = Resources.Menu.MerchantNumber
                ddlMerchantLink.DataSource = Bll.Merchants.Merchant.Current.GetLinkedMerchants()
                ddlMerchantLink.DataTextField = "Value"
                ddlMerchantLink.DataValueField = "Key"
                ddlMerchantLink.DataBind()
                ddlMerchantLink.SelectedValue = Bll.Accounts.Account.Current.AccountNumber
            End If
		End If
		MyBase.OnPreRender(e)
	End Sub

	Protected Function drawSubMenu(ByVal menuName As String, ByVal node As XmlNode, Optional ByVal isPrimary As Boolean = True) As String
		Dim result As String = "", subMenu As String = "", id As Integer = 1
		Dim submenuTemp, onClick, cursor, onMouseOver, onMouseOut As String
		'Dim sRoles As String
		Dim isAllowed As Boolean

		If node.ChildNodes.Count > 0 Then
			Dim caption As String, url As String, pageFile As String

			If isPrimary Then
				result &= "<table id=""" & menuName & """ class=""DROPDOWNMENU"" cellspacing=""0"" cellpadding=""0"" border=""0"" style=""position:absolute;visibility:hidden;z-index:10;"">"
				result &= "<tr><td style=""margin:0;padding:0;height:2px;background-color:#939393;border-width:0;"" height=""2""></td></tr>"
			Else
				result &= "<table id=""" & menuName & """ class=""DROPDOWNMENU"" cellspacing=""0"" cellpadding=""0"" border=""0"" style=""position:absolute;visibility:hidden;z-index:10;;border-top:1px solid #939393;"">"
			End If

			For Each currentXmlNode As XmlNode In node.ChildNodes
				If currentXmlNode.NodeType = XmlNodeType.Comment Then
					Continue For
				End If
				Dim isMenuItem As Boolean = Boolean.Parse(currentXmlNode.Attributes.GetNamedItem("isMenuItem").Value)
				If Not isMenuItem Then Continue For

				caption = dbPages.GetXmlNodeAttributeValue(currentXmlNode, "title")
				url = dbPages.GetXmlNodeAttributeValue(currentXmlNode, "url")
				If Request.ApplicationPath = "/" Then
					url = url.Replace("~", "")
				Else
					url = url.Replace("~", Request.ApplicationPath)
				End If

				pageFile = System.IO.Path.GetFileName(url).ToLower()

				If Not WebUtils.IsLoggedin Then
					isAllowed = False
				ElseIf url.Trim() = "" Then
					isAllowed = True
				Else
					isAllowed = CType(Me.Page, NetpayPage).IsAuthorized(System.IO.Path.GetFileName(url))
				End If

				If _useLocalResource Then
					caption = CType(GetLocalResourceObject(caption), String)
				ElseIf _useGlobalResource And Not String.IsNullOrEmpty(_globalResourceClassName) Then
					caption = CType(GetGlobalResourceObject(_globalResourceClassName, caption), String)
				End If

				submenuTemp = drawSubMenu(menuName & "_" & id, currentXmlNode, False)
				If Not isAllowed Or currentXmlNode.HasChildNodes Then
					caption = "<span class=""denied"">" & caption & "</span>"
					onClick = ""
					cursor = "default"
					onMouseOver = "showMenu(document.getElementById('" & menuName & "_" & id & "'), this, '" & IIf(WebUtils.CurrentAlignment = "right", "LR", "RL") & "TT');style.backgroundColor='#DAEBFE';"
					onMouseOut = "style.backgroundColor='';"
				Else
					caption = "<a href=""" & url & """>" & caption & "</a>"
					onClick = "location.href='" & url & "';"
					cursor = "pointer"
					onMouseOver = "firstChild.style.textDecoration='underline';showMenu(document.getElementById('" & menuName & "_" & id & "'), this, '" & IIf(WebUtils.CurrentAlignment = "right", "LR", "RL") & "TT');style.backgroundColor='#DAEBFE';"
					onMouseOut = "firstChild.style.textDecoration='none';style.backgroundColor='';"
				End If

				If Not String.IsNullOrEmpty(submenuTemp) Then caption = "<image align=""middle"" style=""border:0;float:" & GetGlobalResourceObject("MultiLang", "CssAlignRev") & ";margin-top:4px;"" src=""../" & CType(Me.Page, NetpayPage).CurrentDomain.ThemeFolder & "/Images/menuArrow_" & IIf(WebUtils.CurrentAlignment = "right", "rtl", "ltr") & ".gif"" />" & caption
				result &= "<tr><td style=""cursor:" & cursor & """ onclick=""" & onClick & """ onmouseover=""" & onMouseOver & """ onmouseout=""" & onMouseOut & """>" & caption & "</td></tr>"
				subMenu &= submenuTemp
				id = id + 1
			Next

			result &= "</table>"
		End If

		Return result & subMenu
	End Function
End Class