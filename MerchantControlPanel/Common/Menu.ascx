﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Menu.ascx.vb" Inherits="Netpay.MerchantControlPanel.Menu" %>
<%@ Import Namespace="Netpay.MerchantControlPanel" %>
<%@ Import Namespace="System.Xml" %>

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td style="text-align:<%=WebUtils.CurrentAlignment%>;padding:3px;">
  <table cellpadding="0" cellspacing="0" border="0" class="MenuBar">
	<tr>
		<%
			Dim subMenu As String = "", caption As String, url As String, isAllowed As Boolean
			Dim xmlDoc As New XmlDocument : xmlDoc.Load(MenuXmlUrl)
			Dim relevantNodes As XmlNodeList = xmlDoc.SelectNodes("//siteMap/siteMapNode/siteMapNode[@isColumn='true']/*")
			
			For Each menuNode As XmlNode In relevantNodes
				Dim isMenuItem As Boolean = Boolean.Parse(menuNode.Attributes.GetNamedItem("isMenuItem").Value)
				If Not isMenuItem Then Continue For

				url = IIf(isAllowed, dbPages.GetXmlNodeAttributeValue(menuNode, "url"), "")
				If Request.ApplicationPath = "/" Then
					url = url.Replace("~", "")
				Else
					url = url.Replace("~", Request.ApplicationPath)
				End If
				
				caption = menuNode.Attributes.GetNamedItem("title").Value
				
				If Not WebUtils.IsLoggedin Then
					isAllowed = False
				ElseIf url.Trim() = "" Then
					isAllowed = True
				Else
					isAllowed = CType(Me.Page, NetpayPage).IsAuthorized(System.IO.Path.GetFileName(url))
				End If
				
				If _useLocalResource Then
					caption = CType(GetLocalResourceObject(caption), String)
				ElseIf _useGlobalResource And Not String.IsNullOrEmpty(_globalResourceClassName) Then
					caption = CType(GetGlobalResourceObject(_globalResourceClassName, caption), String)
				End If
				
				If String.IsNullOrEmpty(url) Then
					caption = "<span class=""" & IIf(isAllowed, "mn_allow", "mn_denied") & """>" & caption & "</span>"
				Else
					caption = "<a href=""" & url & """>" & caption & "</a>"
				End If
				
				If isAllowed Then
					Response.Write("<td onmouseover=""showMenu(document.getElementById('MENU_" & _menuMax & "'), this, '" & IIf(WebUtils.CurrentAlignment = "right", "RR", "LL") & "BT');"">" & caption & "</td>")
					subMenu += drawSubMenu("MENU_" & _menuMax, menuNode)
					_menuMax = _menuMax + 1
				Else
					Response.Write("<td>" & caption & "</td>")
				End If
			Next
		%>
	</tr>
  </table>
 </td>
 <td>
<% 
If CType(Me.Page, NetpayPage).IsLoggedin
	literalLastLogin.Text = CType(Me.Page, NetpayPage).LoggedUser.LastLogin.ToString("dd/MM/yyyy hh:mm")
	%>
	<span style='color:White; float:<%=WebUtils.CurrentReverseAlignment %>; padding:0 10px; padding-top:1px;'>
		<asp:Literal ID="merchantNo" runat="server" />
		<asp:DropDownList runat="server" ID="ddlMerchantLink" align="absmiddle" onchange="document.location=netpay.Common.addQueryParams(location.href, {'SwitchMerchant': options[selectedIndex].value});" CssClass="MenuSelectbox" />
		<netpay:PopupButton ID="PopupButton1" Height="80" Width="150" IconSrc="../Templates/Tmp_netpayintl/Images/icon_arrowDown.gif" runat="server" DirectionFlip="true" >
			<asp:Localize ID="Localize2" Text="<%$Resources:Menu,lastLogin %>" runat="server" /><br />
			<asp:Literal ID="literalLastLogin" runat="server"></asp:Literal><br/><br />
			<a href="logout.aspx"><asp:Localize ID="Localize3" Text="<%$Resources:MultiLang,Logout %>" runat="server" /></a>
		</netpay:PopupButton>
	</span>
<%
End If
Response.Write(subMenu)
%>
 </td>
</tr>
</table>
