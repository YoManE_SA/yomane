<%@ Application Language="VB" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="Netpay.Infrastructure" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>

<script runat="server">
    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        Netpay.Web.ContextModule.AppName = "MerchantControlPanel"
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        Dim ex As Exception = Server.GetLastError()
        If (TypeOf ex Is System.Web.HttpException) Then
            Dim hex = CType(ex, System.Web.HttpException)
            If hex.GetHttpCode() = 404 Then Server.Transfer(VirtualPathUtility.ToAbsolute("~/404.aspx"))
            If (ex.Message.StartsWith("Validation of viewstate MAC failed.")) Then
                Response.Redirect("~/Default.aspx")
                Exit Sub
            End If
            If (ex.Message.StartsWith("A potentially dangerous Request.")) Then
                Response.Redirect("~/500.aspx")
                Exit Sub
            End If
        End If
        Netpay.Infrastructure.Logger.Log(LogTag.MerchantControlPanel, ex)
        Dim config As System.Configuration.Configuration = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~")
        Dim configSection As System.Web.Configuration.CompilationSection = CType(config.GetSection("system.web/compilation"), System.Web.Configuration.CompilationSection)
        If Not configSection.Debug Then
            Response.Write("Unexpected system error occurred.")
            Response.End()
        End If
    End Sub

    Private Shared overrideFileCache As Dictionary(Of String, String()) = New Dictionary(Of String, String())
    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' theme page override
        If Request.Url.ToString().ToLower().Contains(".aspx") Then
            Dim fileName As String = Path.GetFileName(Request.PhysicalPath)
            Dim themeFolder = WebUtils.CurrentDomain.ThemeFolder
            Dim themeFolderFullPath = Request.PhysicalApplicationPath & "Templates\" & themeFolder & "\Website\"
            Dim overrideFileFullPath As String = themeFolderFullPath + fileName
            If Not overrideFileCache.ContainsKey(themeFolder) Then
                SyncLock overrideFileCache
                    If Not overrideFileCache.ContainsKey(themeFolder) Then
                        Dim filesPaths As String() = Directory.GetFiles(themeFolderFullPath).Select(Function(path) path.ToLower()).Where(Function(path) path.EndsWith(".aspx")).ToArray()
                        overrideFileCache.Add(themeFolder, filesPaths)
                    End If
                End SyncLock
            End If

            Dim overrideFiles As String() = overrideFileCache(themeFolder)
            If overrideFiles.Contains(overrideFileFullPath.ToLower()) Then
                Dim urlRewrite As String = Request.Url.PathAndQuery.ToLower().Replace("/website/", "/Templates/" + themeFolder + "/website/")
                Context.RewritePath(urlRewrite, False)
            End If
        End If

        If ((sender.GetType() Is GetType(System.Web.HttpApplication))) Then
            Dim app = TryCast(sender, System.Web.HttpApplication)
            If (app IsNot Nothing And app.Context IsNot Nothing) Then
                app.Context.Response.Headers.Remove("Server")
            End If
        End If

    End Sub

    Sub Application_PreRequestHandlerExecute(ByVal sender As Object, ByVal e As EventArgs)
        WebUtils.SelectLanguage(Nothing)
    End Sub

    Public Sub Application_AcquireRequestState(ByVal sender As Object, ByVal e As EventArgs)
        If Request.IsSecureConnection Then Exit Sub
        If Request.ServerVariables("HTTP_HOST").StartsWith("192.168") Or Request.ServerVariables("HTTP_HOST") = "127.0.0.1" _
            Or Request.ServerVariables("HTTP_HOST") = "localhost" Or Request.ServerVariables("HTTP_HOST").StartsWith("80.179.180.") _
            Or Request.ServerVariables("HTTP_HOST") = "merchantstest.netpay-intl.com" Then Exit Sub
        If WebUtils.CurrentDomain.ForceSSL Then Response.Redirect("https://" & Request.Url.ToString().Substring(7))
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
    End Sub
</script>