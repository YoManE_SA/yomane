<%@ Page Language="VB" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_billingShow_pages" EnableEventValidation="false" EnableViewStateMac="false" ValidateRequest="false" EnableViewState="false" Codebehind="billingShow_pages.aspx.vb" %>

<html>
	<head runat="server">
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
		 <!-- CSS -->
    <link rel="stylesheet" href="../Templates/Tmp_MahalaUS/Website/styles/Style.css" />
    <link rel="stylesheet" href="../Templates/Tmp_MahalaUS/Website/styles/button.css" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../Website/Plugins/jquery-ui-1.8.10.custom/css/custom-theme/jquery-ui-1.8.10.custom.css" type="text/css" />
    <link rel="stylesheet" href="../Website/Plugins/fancybox/jquery.fancybox-1.3.4.css" />
    <link href="../Website/Plugins/font-awesome-4.5.0/css/font-awesome.css" rel="stylesheet" />

		<style type="text/css">.grandTotal { font-size:12px; font-weight:bold; }</style>
		<script type="text/javascript">
			function showSection(SecNum) 
			{
				if (trSec)
				{
					if (trSec.length) 
					{
						for (i = 0; i < trSec.length; i++) 
						{
							if (i == SecNum) { trSec[SecNum].style.display = ''; }
							else { trSec[i].style.display = 'none'; }
						}
					}
					else 
					{ 
						trSec.style.display = '';
					} 
				}
			}
		</script>	
	</head>
	<body class="itext" style="scrollbar-track-color: WhiteSmoke;" dir="rtl">
		<form name="frmBillingPages" id="frmBillingPages" method="post" runat="server" class="formNormal">
			<asp:Panel ID="pnlOriginalNotPrinted" runat="server" Enabled="false">
				<asp:Literal ID="litPreviewOriginalCopiesNumber" runat="server" Visible="false" />
				<asp:Literal ID="litPreviewOriginalPagesPayment" runat="server" Visible="false" />
				<asp:Literal ID="litPreviewOriginalInnerHtmlScript" runat="server" Visible="false" />		
			</asp:Panel>
			<asp:Panel ID="pnlBillCopy" runat="server" Enabled="false">
				<asp:Literal ID="litBillCopyPrintNumber" runat="server" Visible="false" />
				<asp:Literal ID="litBillCopyPagesPayment" runat="server" Visible="false" />		
				<asp:Literal ID="litBillCopyInnerHtmlScript" runat="server" Visible="false" />
			</asp:Panel>
			<asp:Panel ID="pnlChargeList" runat="server" Enabled="false">
				<asp:Literal ID="litChargeListPrintNumber" runat="server" Visible="false" />
				<asp:Literal ID="litChargeListTransList" runat="server" Visible="false" />
				<asp:Literal ID="litChargeListInnerHtmlScript" runat="server" Visible="false" />			
			</asp:Panel>

			<script type="text/javascript" language="JavaScript">
				<%
				If sType="preview" then
					Dim sQuery as String
					sQuery = "&BillingCompanysID=" & nBillingCompanysID
					sQuery = sQuery & "&billingNumber=" & nBillingNumber
					sQuery = sQuery & "&InvoiceType=" & nInvoiceType
					sQuery = sQuery & "&billOriginal=" & request("billOriginal")
					sQuery = sQuery & "&billCopy=" & request("billCopy")
					sQuery = sQuery & "&chargeList=" & request("chargeList")
					sQuery = sQuery & "&billCopyNum=" & request("billCopyNum")
					sQuery = sQuery & "&chargeListNum=" & request("chargeListNum")
					%>
					top.fraBillprint.location.href="billingShow_pages.aspx?type=print<%= sQuery %>";
					<%
				Else
					%>
					top.fraBillMenu.document.getElementById("bPrint").style.cursor = "pointer";
					top.fraBillMenu.document.getElementById("bPrint").disabled = false;
					<%
				End If
				%>
			</script>
		</form>
	</body>
</html>
