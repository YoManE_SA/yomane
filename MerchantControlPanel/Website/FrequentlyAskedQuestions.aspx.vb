﻿Imports System.Collections.Generic
Imports Netpay.Web
Imports Netpay.Bll

Partial Class Website_FrequentlyAskedQuestions
    Inherits MasteredPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim categories = Bll.Faqs.Group.Search(New Bll.Faqs.Group.SearchFilters() With {.LanguageId = WebUtils.CurrentLanguage, .Theme = Domain.Current.ThemeFolder, .IsMerchantCP = True, .IsVisible = True}, Nothing)
        'rptCategories.DataSource = categories
        'rptCategories.DataBind()
        If categories.Count > 0 Then Cat_Command(sender, New System.Web.UI.WebControls.CommandEventArgs("", categories.First().ID))
    End Sub

    Protected Sub Cat_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
        Dim categoryID As Integer
        If Not Integer.TryParse(e.CommandArgument, categoryID) Then Return
        Dim src = Bll.Faqs.Faq.Search(New Bll.Faqs.Faq.SearchFilters() With { .GroupId = categoryID }, Nothing)
        rptQuestions.DataSource = src : rptQuestions.DataBind()
    End Sub

    'Protected Sub Questions_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
    'Dim script As String = "$('#QST_" & e.Item.ItemIndex & "').click(function () { var ans = $('#ANS_" & e.Item.ItemIndex & "'); if(ans.css('display') != 'none') ans.slideUp('slow'); else ans.slideDown('slow'); });"
    'ClientScript.RegisterStartupScript(Me.GetType(), "QstJS_" & e.Item.ItemIndex, script, True)
    'End Sub
End Class
