﻿Imports System.Collections.Generic
Imports Netpay.Web
Imports Netpay.Bll

Partial Class FAQuestions
    Inherits MasteredPage

    'Protected Overrides Sub OnPreInit(e As System.EventArgs)
    'IsMobile = (Request.QueryString("IsMobile") = "1")
    ' MyBase.OnPreInit(e)
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim template As String =
        Dim categories = Netpay.Bll.Faqs.Group.Search(New Bll.Faqs.Group.SearchFilters() With {.LanguageId = WebUtils.CurrentLanguage, .Theme = Domain.Current.ThemeFolder, .IsMerchantCP = True, .IsVisible = True}, Nothing)
        'rptCategories.DataSource = categories
        'rptCategories.DataBind()
        If categories.Count > 0 Then Cat_Command(sender, New System.Web.UI.WebControls.CommandEventArgs("", categories.First().ID))
    End Sub

    Protected Sub Cat_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
        Dim categoryID As Integer
        If Not Integer.TryParse(e.CommandArgument, categoryID) Then Return
        Dim src = Bll.Faqs.Faq.Search(New Bll.Faqs.Faq.SearchFilters() With {.GroupId = categoryID}, Nothing)
        rptQuestions.DataSource = src : rptQuestions.DataBind()
    End Sub
End Class
