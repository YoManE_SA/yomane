﻿Imports System.Collections.Generic
Imports Netpay.Web.Controls
Imports Netpay.Bll.Reports
Imports Netpay.Infrastructure
Imports Netpay.CommonTypes
Imports Netpay.Dal.Netpay
Imports Netpay.Bll
Imports Netpay.Web

Partial Class Website_PendingTransactions
	Inherits MasteredPage

 	Protected Sub OnPageChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim filters = New Bll.Transactions.Transaction.SearchFilters()
		filters.paymentMethodID = ddPaymentMethod.SelectedValue.ToNullableInt32()
		filters.transactionID = txtTransactionID.Text.ToNullableInt32()
		filters.date.From = wcDateRangePicker.FromDate
		filters.date.To = wcDateRangePicker.ToDate
        filters.PayerFilters = New Transactions.Payer.SearchFilters()
		filters.PayerFilters.FirstName = txtPayerFirstName.Text.NullIfEmpty()
		filters.PayerFilters.LastName = txtPayerLastName.Text.NullIfEmpty()

		If TypeOf e Is ExcelButton.ExportEventArgs Then
			wcFiltersView.LoadFilters(filters)
            btnExportExcel.SetExportInfo(wcFiltersView.RenderText, ReportType.MerchantPendingTransactions, Function(pi) Bll.Transactions.Transaction.Search(TransactionStatus.Pending, filters, pi, True, False))
        Else
            repeaterResults.DataSource = Bll.Transactions.Transaction.Search(TransactionStatus.Pending, filters, pager.Info, True, False)
			repeaterResults.DataBind()
			btnExportExcel.ResultCount = Pager.Info.RowCount
			phResults.Visible = repeaterResults.Items.Count > 0
		End If
	End Sub

	Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
		Pager.Info.PageCurrent = 0
		OnPageChanged(sender, e)
		Pager.Visible = True
	End Sub

	Protected Sub ExportExcel_Click(ByVal sender As Object, ByVal e As EventArgs)
		OnPageChanged(sender, e)
		btnExportExcel.Export()
	End Sub

	Protected Sub btnShow_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim filters = New Bll.Transactions.Transaction.SearchFilters()
		filters.transactionID = txtTransactionID.Text.ToNullableInt32()
		If txtTransactionID.Text.ToNullableInt32() Is Nothing Then
			repeaterResults.DataSource = Nothing
			repeaterResults.DataBind()
			Exit Sub
		End If

		Pager.Info.PageCurrent = 0
        repeaterResults.DataSource = Bll.Transactions.Transaction.Search(TransactionStatus.Pending, filters, pager.Info, False, False)
		repeaterResults.DataBind()

		Pager.Visible = False
		btnExportExcel.ResultCount = Pager.Info.RowCount
	End Sub

End Class