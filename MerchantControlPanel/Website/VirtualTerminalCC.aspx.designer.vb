﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Website_VirtualTerminalCC
    
    '''<summary>
    '''locPageTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents locPageTitle As Global.System.Web.UI.WebControls.Localize
    
    '''<summary>
    '''hfTransID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfTransID As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''wcElementBlocker control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents wcElementBlocker As Global.Netpay.Web.Controls.ElementBlocker
    
    '''<summary>
    '''divResult control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divResult As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''phTransApproved control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents phTransApproved As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''lblSuccessMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSuccessMessage As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''phTransDecline control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents phTransDecline As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''lblErrorMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblErrorMessage As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''btnChargeAgain control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnChargeAgain As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''phVTForm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents phVTForm As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''phStorageCard control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents phStorageCard As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''BtnCUS control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents BtnCUS As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''phAddcardStorage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents phAddcardStorage As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''txStorageID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txStorageID As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''btnLoadStorage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnLoadStorage As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''smSwipe control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents smSwipe As Global.Netpay.Web.Controls.SwipeManager
    
    '''<summary>
    '''lblCreditCardNum control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCreditCardNum As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ltCreditCardNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ltCreditCardNumber As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''wcCCInput control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents wcCCInput As Global.Netpay.Web.Controls.CreditcardInput
    
    '''<summary>
    '''ltExpiritionDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ltExpiritionDate As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''ddlsExpMonth control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlsExpMonth As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''ddlsExpYear control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlsExpYear As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''CvvTip control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CvvTip As Global.Netpay.Web.Controls.PageItemHelp
    
    '''<summary>
    '''ltCvv control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ltCvv As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''txtCHCvv2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCHCvv2 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''ltCardHolderName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ltCardHolderName As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''txtCHFullName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCHFullName As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''litRequiredCCHolderPhone control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litRequiredCCHolderPhone As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''txtPhoneNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPhoneNumber As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''litRequiredMail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litRequiredMail As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''txtEmail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtEmail As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''litRequiredIP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litRequiredIP As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''txtClientIP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtClientIP As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''litRequiredCCHolderID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litRequiredCCHolderID As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''txtCCHolderID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCCHolderID As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''phCreditCardStorage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents phCreditCardStorage As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''ChkStorageCard control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ChkStorageCard As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''rblTransactionType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rblTransactionType As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''ValidAmount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ValidAmount As Global.System.Web.UI.WebControls.RequiredFieldValidator
    
    '''<summary>
    '''txtAmount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAmount As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''ddlCreditType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCreditType As Global.Netpay.Web.Controls.CreditTypeDropDown
    
    '''<summary>
    '''ValidCurrency control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ValidCurrency As Global.System.Web.UI.WebControls.RequiredFieldValidator
    
    '''<summary>
    '''ddlCurrency control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCurrency As Global.Netpay.Web.Controls.CurrencyDropDown
    
    '''<summary>
    '''ddlPayment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPayment As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''pnlApproval control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlApproval As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''litApprove control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litApprove As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''pihApprove control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pihApprove As Global.Netpay.Web.Controls.PageItemHelp
    
    '''<summary>
    '''txtApprove control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtApprove As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''ltAddress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ltAddress As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''ltOptionalAddress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ltOptionalAddress As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''litRequiredAddress1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litRequiredAddress1 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''PageItemHelp4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PageItemHelp4 As Global.Netpay.Web.Controls.PageItemHelp
    
    '''<summary>
    '''txtAddress1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAddress1 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''PageItemHelp1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PageItemHelp1 As Global.Netpay.Web.Controls.PageItemHelp
    
    '''<summary>
    '''txtAddress2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAddress2 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''litRequiredCountry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litRequiredCountry As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''ddlCountry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCountry As Global.Netpay.Web.Controls.CountryDropDown
    
    '''<summary>
    '''DivUsa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents DivUsa As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''ddlStateUsa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlStateUsa As Global.Netpay.Web.Controls.StateDropDown
    
    '''<summary>
    '''DivCanada control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents DivCanada As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''ddlStateCanada control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlStateCanada As Global.Netpay.Web.Controls.StateDropDown
    
    '''<summary>
    '''litRequiredCity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litRequiredCity As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''txtCity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCity As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''litRequiredZipcode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litRequiredZipcode As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''txtZipCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtZipCode As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''pnRecurring control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnRecurring As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''mvRecurring control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mvRecurring As Global.System.Web.UI.WebControls.MultiView
    
    '''<summary>
    '''vDisabled control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents vDisabled As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''litRecurringNotAllowed control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litRecurringNotAllowed As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''vEnabled control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents vEnabled As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''chkIsUseRecurring control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkIsUseRecurring As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''lblRecurringMode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRecurringMode As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblRecurringRequiredMode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRecurringRequiredMode As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''RecurringCycle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RecurringCycle As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''RecurringMode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RecurringMode As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''lblRecurringCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRecurringCount As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblRecurringRequiredCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRecurringRequiredCount As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''RecurringCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RecurringCount As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtComment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtComment As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''BtnCharge control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents BtnCharge As Global.System.Web.UI.WebControls.Button
End Class
