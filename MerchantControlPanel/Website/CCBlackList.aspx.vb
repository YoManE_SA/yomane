﻿Imports System.Collections.Generic
Imports Netpay.Bll.Risk
Imports Netpay.Bll
Imports Netpay.Web.Controls
Imports Netpay.Infrastructure

Partial Class Website_CCBlackList
    Inherits MasteredPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack And Request("transactionID") IsNot Nothing Then
            Dim transactionStatus As TransactionStatus = Request("transactionStatus")
            Dim transactionID As Integer = Request("transactionID")
			If transactionID <> 0 Then
                repeaterResults.DataSource = Bll.Risk.RiskItem.Search(transactionID, transactionStatus, New List(Of Bll.Risk.RiskItem.RiskValueType) From {Bll.Risk.RiskItem.RiskValueType.AccountValue1}, True)
                repeaterResults.DataBind()
				phResults.Visible = True
				pager.Visible = False
			End If
        End If

        lblError.Text = ""
    End Sub

 	Protected Sub OnPageChanged(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim filters = New Bll.Risk.RiskItem.SearchFilters()
		filters.Source = ddItemSourceFilter.SelectedValue.ToNullableEnumByValue(Of BlockedItemSource)()
        filters.ValueType = RiskItem.RiskValueType.AccountValue1
        filters.Account1Last4 = txtFindLastDigit.Text.NullIfEmpty()

        repeaterResults.DataSource = Bll.Risk.RiskItem.Search(filters, pager.Info)
		repeaterResults.DataBind()

        phResults.Visible = (repeaterResults.Items.Count > 0)
        pager.Visible = True
	End Sub

	Sub ToggleAddCard(ByVal sender As Object, ByVal e As System.EventArgs)
        phButtonAdd.Visible = False
        phAddCardForm.Visible = True
	End Sub

	Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs)
		pager.Info.PageCurrent = 0
		OnPageChanged(sender, e)
		pager.Visible = True
	End Sub

	Protected Sub AddBlockedCard(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim card = ccNumber.Card
		If card.Validate() <> ValidationResult.Success  Then
            lblError.Text = "<div class=""error"">" & GetGlobalResourceObject("MultiLang", "ErrorAdding") & ": " & GetGlobalResourceObject("MultiLang", "invalidCard") & "</div>"
            Return
		End If
		Dim ccList = New List(Of Bll.Risk.RiskItem.RiskItemValue)
        ccList.Add(New Bll.Risk.RiskItem.RiskItemValue() With {.ValueType = RiskItem.RiskValueType.AccountValue1, .Value = card.CardNumber.ToString()})
        Bll.Risk.RiskItem.Create(RiskItem.RiskSource.Merchant, Risk.RiskItem.RiskListType.Black, ccList, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, txtComment.Value, Nothing)
        'If result <> AddBlockedCardResult.Success Then
        '	lblError.Text = "<div class=""Error2"">" & GetGlobalResourceObject("MultiLang", "ErrorAdding") & ": " & GetLocalResourceObject(result.ToString()) & "</div>"
        '	Return
        'End If

        Page_Load(Nothing, Nothing)
	End Sub

	Protected Sub DeleteSelectedCards(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim blocked As String = Request.Form("blockedItemID")
		If blocked Is Nothing Then Return
		Dim blockedList As String() = blocked.Split(",")
		Dim blockedListConverted As List(Of Integer) = New List(Of Integer)
		For Each currentID In blockedList
			blockedListConverted.Add(Integer.Parse(currentID))
		Next

        Bll.Risk.RiskItem.Delete(RiskItem.RiskValueType.AccountValue1, Risk.RiskItem.RiskListType.Black, blockedListConverted)
        Page_Load(Nothing, Nothing)
	End Sub

	Protected Function GetLegend(item As Risk.RiskItem) As String
        If item.Source = RiskItem.RiskSource.Merchant Then
            Return "<td><div style='background-color:#6699CC; width: 15px; height: 15px'>&nbsp;</div></td><td>" + GetGlobalResourceObject("MultiLang", "Merchant") + "</td>"
        Else
            Return "<td><div style='text-align:center;background-color:#484848; width: 15px; height: 15px'>&nbsp;</div></td><td>" + GetGlobalResourceObject("MultiLang", "System") + "</td>"
		End If
	End Function

	Protected Function GetCheckbox(item As Risk.RiskItem) As String
        If item.Source = RiskItem.RiskSource.Merchant Then
            Return "<input type='checkbox' name='blockedItemID' value='" + item.ID.ToString() + "' style='background-color:#ffffff!important;' />"
        Else
            Return "<input type='checkbox' disabled='true' name='blockedItemID' value='" + item.ID.ToString() + "' style='background-color:#ffffff!important;' />"
		End If
	End Function

	Protected Function GetComment(ByVal item As Risk.RiskItem) As String
        If item.Source = RiskItem.RiskSource.Merchant Then
            Return item.Comment
        Else
            Return "---"
		End If
	End Function
End Class
