﻿Imports Netpay.Web
Imports Netpay.Web.Controls

Partial Class Website_ShowTotals
    Inherits PopupPage


	Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        Dim cssTag As String = String.Format("<link rel=""stylesheet"" type=""text/css"" href=""{0}/Website/Styles/style.css"" />", MapTemplateVirPath(""))
        Page.Header.Controls.Add(New LiteralControl(cssTag))
        If System.Threading.Thread.CurrentThread.CurrentUICulture.TextInfo.IsRightToLeft Then
            Dim cssLangTag As String = String.Format("<link rel=""stylesheet"" type=""text/css"" href=""{0}/Website/Styles/styleRTL.css"" />", MapTemplateVirPath(""))
            Page.Header.Controls.Add(New LiteralControl(cssLangTag))
        End If
        litCloseWindow.Text = "<a href=""javascript:window.close();"">" & GetGlobalResourceObject("Multilang", "CloseWindow") & "</a>"
        MyBase.OnInit(e)
    End Sub

	Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
		ShowTotalsButton.LoadTotalsParams(Total1)
	End Sub
End Class
