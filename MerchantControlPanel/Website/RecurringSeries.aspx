﻿<%@ Page Title="PageTitle" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master"
    AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_RecurringSeries"
    CodeBehind="RecurringSeries.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
    <script language="javascript" type="text/javascript">		
		function deleteConfirmation()
		{
			return confirm("<asp:Literal Text="<%$Resources:DeleteConfirmation %>" runat="server"></asp:Literal>");
		}  
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <asp:HiddenField ID="hdLastSearchMode" runat="server" />
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">
            <div class="legend-menu">
                <div class="align-right">
                    <netpay:PopupButton ID="PopupButton1" IconSrc="Images/iconLegend.gif" Width="100" Height="120" Text="<%$Resources:MultiLang,Legend  %>" runat="server">
                    <netpay:Legend ID="ttLegend" Active="true" Ended="true" Blocked="true" Suspended="true" Precreated="true" runat="server" />
                    </netpay:PopupButton>
                </div>
            </div>
            <div class="section">
                <div class="filter align-right">
                    <div class="quick-search">
                        <asp:Literal runat="server" Text="<%$Resources:MultiLang,Quicksearch  %>" />
                        <div class="field-quick">
                            <asp:TextBox ID="txtSeriesID" runat="server" CssClass="Field_110" placeholder="<%$Resources:MultiLang,SeriesNumber  %>" ClientIDMode="Static" />
                        </div>
                        <div class="button-quick">
                            <asp:LinkButton ID="buttonSearchSingle" CssClass="btn" runat="server" OnClientClick="javascript:return document.getElementById('txtSeriesID').value!=''"><i class="fa fa-search"></i> <asp:Literal runat="server" Text="<%$Resources:MultiLang,Search  %>" /></asp:LinkButton></div>
                       </div>
                </div>

               <div class="RecurringSeries align-left percent80">
                    <div class="margin-bottom-10">
                        <div><asp:Literal runat="server" Text="<%$Resources:MultiLang,InsertDate %>" /></div>
                        <div><netpay:DateRangePicker ID="wcDateRangePicker" CssClass="percent100" HtmlLayout="Flow" runat="server" /></div>
                    
                    </div>
                  <div class="percent23 align-left margin-right-10">
                      <div><asp:Literal runat="server" Text="<%$Resources:MultiLang,Amount %>" /></div>
                      <div><asp:TextBox ID="txtAmount" runat="server" CssClass="percent100" placeholder="<%$Resources:MultiLang,Amount  %>" /></div>
                  </div>
                     <div class="percent23 align-left margin-right-10">
                         <div><asp:Literal runat="server" Text="<%$Resources:MultiLang,Currency %>" /></div>
                         <div><netpay:CurrencyDropDown ID="ddCurrency" runat="server" CssClass="percent100" BlankSelectionText="<%$Resources:MultiLang,Currency  %>" /></div>
                     </div>
                   <div class="percent23 align-left margin-right-10">
                        <div><asp:Literal runat="server" Text="<%$Resources:MultiLang,PaymentMethod %>" /></div>
                        <div> <netpay:PaymentMethodDropDown ID="ddPaymentMethod" BlankSelectionText="<%$Resources:MultiLang,PaymentMethod  %>" CssClass="percent100" runat="server" /></div>
                   </div>
                    <div class="percent23 align-left margin-right-10">
                         <div><asp:Literal runat="server" Text="<%$Resources:MultiLang,Status %>" /></div>
                         <div>
                            <asp:DropDownList ID="ddStatus" runat="server" CssClass="percent100" AppendDataBoundItems="true">
                            <asp:ListItem Text="<%$Resources:MultiLang,Status  %>" Value="" Selected="True"  />
                            </asp:DropDownList>
                        </div>
                  </div>
                  <div class="spacer"></div>
               </div>
               <div class="spacer"></div>
               <div class="wrap-button-bar">
                  <asp:LinkButton ID="buttonSearch" CssClass="btn btn-cons-short btn-inverse" runat="server"><i class="fa fa-search"></i> <asp:Literal runat="server" Text="<%$Resources:MultiLang,Search %>" /></asp:LinkButton>
               </div>
        </div>
        <asp:PlaceHolder ID="phResults" Visible="false" runat="server">
            <div class="section">
                <netpay:SearchFiltersView ID="wcFiltersView" runat="server" Visible="false" />
                <table class="exspand-table">
                    <tr>
                        <th>
                            &nbsp;
                        </th>
                        <th>
                            <asp:Literal Text="<%$Resources:MultiLang,Series %>" runat="server" />
                        </th>
                        <th>
                            <asp:Literal Text="<%$Resources:MultiLang,Date %>" runat="server" />
                        </th>
                        <th>
                            <asp:Literal Text="<%$Resources:MultiLang,Type %>" runat="server" />
                        </th>
                        <th>
                            <asp:Literal Text="<%$Resources:MultiLang,PaymentMethod %>" runat="server" />
                        </th>
                        <th>
                            <asp:Literal Text="<%$Resources:MultiLang,Amount %>" runat="server" />
                        </th>
                        <th>
                            <asp:Literal Text="<%$Resources:MultiLang,Charges %>" runat="server" />
                        </th>
                        <th>
                            <asp:Literal runat="server" />
                        </th>
                        <th colspan="2" style="text-align: center!important;">
                            <asp:Literal ID="Literal14" Text="<%$Resources:MultiLang,Action %>" runat="server" />
                        </th>
                    </tr>
                    <asp:Repeater ID="repeaterResults" EnableViewState="true" runat="server" OnItemDataBound="ResultsItemDataBound">
                        <ItemTemplate>
                            <tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
                                <td>
                                    <div style="width: 15px; height: 15px;" class="<%# GetStatusClass(CType(Container.DataItem, Transactions.Recurring.Series)) %>">&nbsp;</div>
                                </td>
                                <td>
                                    <span>
                                        <asp:Literal ID="Literal15" Text="<%# CType(Container.DataItem, Transactions.Recurring.Series).ID %>"
                                            runat="server"></asp:Literal>
                                    </span>
                                </td>
                                <td>
                                    <%# CType(Container.DataItem, Transactions.Recurring.Series).StartDate.ToString("dd/MM/yyyy HH:mm")%>
                                </td>
                                <td>
                                    <%# GetTypeText(CType(Container.DataItem, Transactions.Recurring.Series).IsPreAuthorized)%>
                                </td>
                                <td>
                                    <netpay:PaymentMethodView ID="PaymentMethodView1" Visible="<%# Not CType(Container.DataItem, Transactions.Recurring.Series).IsPrecreated %>" StoredData="<%# CType(Container.DataItem, Transactions.Recurring.Series).Payment %>" runat="server" />
                                </td>
                                <td>
                                    <%# IIf(CType(Container.DataItem, Transactions.Recurring.Series).IsPrecreated, String.Empty, CType(Container.DataItem, Transactions.Recurring.Series).Amount.ToAmountFormat(CType(Container.DataItem, Transactions.Recurring.Series).Currency.GetValueOrDefault()))%>
                                </td>
                                <td style="white-space: nowrap;"> 
                                   <div class="align-left"> <%# GetChargesText(CType(Container.DataItem, Transactions.Recurring.Series)) %></div>
                                     <asp:Panel ID="Panel1" CssClass="align-left" runat="server" Visible="<%# Not CType(Container.DataItem, Transactions.Recurring.Series).IsPrecreated %>">&nbsp;
                                        [<a href="RecurringChargesMain.aspx?seriesID=<%#CType(Container.DataItem, Transactions.Recurring.Series).ID %>"><asp:Literal
                                            ID="Literal16" Text="<%$Resources:MultiLang,ShowChargelist %>" runat="server" /></a>]
                                    </asp:Panel>
                                </td>
                                
                                <td>
                                    <netpay:PageItemComment ID="PageItemComment1" Text="<%#CType(Container.DataItem, Transactions.Recurring.Series).Comments %>"
                                        runat="server" />
                                </td>
                                
                                <td>
                                      <asp:ImageButton ID="btnSuspend" ImageUrl="/NPCommon/Images/iconB_suspend.png" AlternateText="<%$Resources:MultiLang,Suspend %>"
                                        ToolTip="<%$Resources:MultiLang,Suspend %>" OnCommand="SuspendSeries" CommandArgument="<%# CType(Container.DataItem, Transactions.Recurring.Series).ID %>"
                                        runat="server" />
                                    <asp:ImageButton ID="btnResume" ImageUrl="/NPCommon/Images/iconB_resume.png" AlternateText="<%$Resources:MultiLang,Resume %>"
                                        ToolTip="<%$Resources:MultiLang,Resume %>" OnCommand="ResumeSeries" CommandArgument="<%# CType(Container.DataItem, Transactions.Recurring.Series).ID %>"
                                        runat="server" />
                                    <asp:ImageButton ID="btnDelete" ImageUrl="/NPCommon/Images/iconB_delete.png" AlternateText="<%$Resources:MultiLang,Delete %>"
                                        ToolTip="<%$Resources:MultiLang,Delete %>" OnCommand="DeleteSeries" CommandArgument="<%# CType(Container.DataItem, Transactions.Recurring.Series).ID %>"
                                        OnClientClick="return deleteConfirmation()" runat="server" />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
                   <div class="xls-wrap">
                    <netpay:ExcelButton ID="btnExportExcel" Text="<%$Resources:MultiLang,ExportToXls %>"
                        Enabled="false" IconImageOn="xls.png" IconImageOff="xls_off.png" runat="server"
                        OnClick="ExportExcel_Click" />
                </div>
                <netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
             
            </div>
        </asp:PlaceHolder>
    </div>
    </div>
</asp:Content>
