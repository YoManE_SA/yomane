﻿Imports System.Collections.Generic
Imports System.Linq
Imports Netpay.Infrastructure
Imports Netpay.Web
Imports Netpay.Bll
Imports Netpay.Dal.Netpay

Partial Class Website_CompanyBalance
	Inherits MasteredPage


    Protected Function FormatPayment(ByVal item As Netpay.Bll.Accounts.Balance) As String
        Dim htmlBuilder As New StringBuilder(String.Empty)
        Dim source As String
        Dim Items = item.SourceType.EmptyIfNull().Split(New Char() {"."c}, 2)
        If (Items.Length < 2) Then source = item.SourceType
        If (Items(0).ToUpper() = "SYSTEM") Then source = "<span style=""color:blue;"">" & Items(1) & "</span>" _
        Else source = String.Format("{1} ({0})", Items(0), Items(1))

        Select Case item.SourceType
            Case Wires.Balance.BALANCE_SOURCE_SETTLEMENT
                htmlBuilder.Append("<a href=""SettlementList.aspx?payID=" & Trim(item.SourceID.ToString()) & "&Currency=" & Bll.Currency.Get(item.CurrencyIso).ID & """>")
                htmlBuilder.Append(source & " - " & item.Text)
                htmlBuilder.Append("</a>")
            Case Wires.Balance.BALANCE_SOURCE_PAYMENTREQUEST
                htmlBuilder.Append("<a href=""ViewPaymentsReport.aspx?Action=Search&RequestsID=" & Trim(item.SourceID.ToString()) & """>")
                htmlBuilder.Append(source & " - " & item.Text)
                htmlBuilder.Append("</a>")
            Case Else
                htmlBuilder.Append(source & " - " & item.Text)
        End Select

        Return htmlBuilder.ToString()
    End Function

    Protected Function FormatAmount(amount As Decimal, CurrencyIso As String) As String
        Dim ret As String = (New Money(amount, CurrencyIso)).ToIsoString()
        If amount < 0 Then ret = String.Format("<span style=""color:red;"">{0}</span>", ret)
        Return ret
    End Function

    Protected Function FormatBalanceStatus(item As Netpay.Bll.Accounts.Balance) As String
        Return "<span style=""cursor:help;"" title=""" & IIf(item.IsPending, "Pending", "Cleared") & """>" & IIf(item.IsPending, "Pending", "") & "</span>"
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim balanceSummaryResult = Netpay.Bll.Accounts.Balance.GetStatus(Merchant.AccountID)
            If balanceSummaryResult IsNot Nothing And balanceSummaryResult.Count >= 1 Then
                For Each currentResult In balanceSummaryResult
                    Dim currency = Bll.Currency.Get(currentResult.CurrencyIso)
                    Dim itemText As String = currency.Name & " " & currency.IsoCode & " " & currentResult.Expected.ToString("0.00")
                    ddlCurrencyBalance.Items.Add(New ListItem(itemText, currency.IsoCode))
                Next
                If ddlCurrencyBalance.Items.Count > 0 Then ddlCurrencyBalance.SelectedIndex = 0 Else ddlCurrencyBalance.Visible = False
            End If
        End If
        If ddlCurrencyBalance.Items.Count > 0 Then OnPageChanged(Me, e)
    End Sub

    Protected Sub OnPageChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If String.IsNullOrEmpty(ddlCurrencyBalance.SelectedValue) Then Exit Sub
        Dim balanceStatus = Netpay.Bll.Accounts.Balance.GetStatus(Merchant.AccountID, ddlCurrencyBalance.SelectedValue)
        ltAvalableBalace.Visible = Not balanceStatus Is Nothing
        If ltAvalableBalace.Visible Then ltAvalableBalace.Text = String.Format("<tr><th colspan=""7"">{0}: <b>{1}</b></th></tr>", GetGlobalResourceObject("CompanyBalance.aspx", "AvailableBalance"), balanceStatus.Expected.ToIsoAmountFormat(balanceStatus.CurrencyIso))

        If pager.Info.PageCurrent = 0 Then
            repeaterPending.DataSource = Netpay.Bll.Accounts.Balance.Search(New Bll.Accounts.Balance.SearchFilters() With {.CurrencyIso = ddlCurrencyBalance.SelectedValue, .OnlyNotProcessedPendings = True}, Nothing)
            repeaterPending.DataBind()
            phBalancePending.Visible = repeaterPending.Items.Count > 0
        Else
            phBalancePending.Visible = False
        End If

        repeaterResults.DataSource = Netpay.Bll.Accounts.Balance.Search(New Bll.Accounts.Balance.SearchFilters() With {.CurrencyIso = ddlCurrencyBalance.SelectedValue, .IsPending = False}, pager.Info)
        repeaterResults.DataBind()
        phBalanceCurrent.Visible = repeaterResults.Items.Count > 0

        phNoData.Visible = (Not phBalancePending.Visible) And (Not phBalanceCurrent.Visible)
        If phNoData.Visible Then litNoData.Text = "<tr><td colspan=""7"">" & GetGlobalResourceObject("CompanyBalance.aspx", "NoBalanceFound") & "</td></tr>"
    End Sub
End Class
