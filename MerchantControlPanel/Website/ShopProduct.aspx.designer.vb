﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ShopProduct
    
    '''<summary>
    '''locPageTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents locPageTitle As Global.System.Web.UI.WebControls.Localize
    
    '''<summary>
    '''upPublished control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upPublished As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''divNotActive control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divNotActive As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''divActive control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divActive As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''btnEnableProduct control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnEnableProduct As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''btnDisableProduct control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnDisableProduct As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''btnDelete control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnDelete As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''btnPreview control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnPreview As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''ddProdType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddProdType As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''iuImage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents iuImage As Global.Netpay.MerchantControlPanel.ImageUpload
    
    '''<summary>
    '''Literal19 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal19 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''upLanguages control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upLanguages As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''dvLanguageData control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dvLanguageData As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''txtName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtName As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtDescription control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDescription As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtReceiptText control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtReceiptText As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''chkReceiptLink control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkReceiptLink As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''txtReceiptLink control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtReceiptLink As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtMetaTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMetaTitle As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtMetaKeyword control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMetaKeyword As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtMetaDescription control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMetaDescription As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtProductLink control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtProductLink As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''ctlShareLinks control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ctlShareLinks As Global.Netpay.MerchantControlPanel.ShareLinks
    
    '''<summary>
    '''imgQrCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgQrCode As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''txtSortOrder control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSortOrder As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtPrice control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPrice As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''ValidRequiredfieldAmount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ValidRequiredfieldAmount As Global.System.Web.UI.WebControls.RequiredFieldValidator
    
    '''<summary>
    '''ValidNegativeNumbersfield control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ValidNegativeNumbersfield As Global.System.Web.UI.WebControls.RangeValidator
    
    '''<summary>
    '''ddlIsAuthorize control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlIsAuthorize As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''chkIsDynamicProduct control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkIsDynamicProduct As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''chkRecurring control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkRecurring As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''btnAddRecurring control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAddRecurring As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Literal7 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal7 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''UpdatePanel3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel3 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''rptRecurring control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rptRecurring As Global.Netpay.Web.Controls.DynamicRepeater
    
    '''<summary>
    '''Literal16 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal16 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''mvProductType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mvProductType As Global.System.Web.UI.WebControls.MultiView
    
    '''<summary>
    '''viewPhysical control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents viewPhysical As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Literal14 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal14 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Literal15 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal15 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''txtSKU control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSKU As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''RegularExpressionValidator1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RegularExpressionValidator1 As Global.System.Web.UI.WebControls.RegularExpressionValidator
    
    '''<summary>
    '''txtQtyAvailable control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtQtyAvailable As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtQtyStart control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtQtyStart As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtQtyEnd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtQtyEnd As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''RegularExpressionValidator2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RegularExpressionValidator2 As Global.System.Web.UI.WebControls.CompareValidator
    
    '''<summary>
    '''txtQtyStep control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtQtyStep As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtPropertyName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPropertyName As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''btnAddProperty control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAddProperty As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''UpdatePanel2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel2 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''phProperties control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents phProperties As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''rptProperties control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rptProperties As Global.Netpay.Web.Controls.DynamicRepeater
    
    '''<summary>
    '''btnGenerate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnGenerate As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''rptStock control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rptStock As Global.Netpay.Web.Controls.DynamicRepeater
    
    '''<summary>
    '''viewVirtual control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents viewVirtual As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''viewDownload control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents viewDownload As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Literal17 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal17 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''phMediaFileUpload control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents phMediaFileUpload As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''fuProductFile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fuProductFile As Global.System.Web.UI.WebControls.FileUpload
    
    '''<summary>
    '''phMediaFileRemove control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents phMediaFileRemove As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''btnRemoveMediaFile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRemoveMediaFile As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''phMediaFileError control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents phMediaFileError As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''literalMediaFileError control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents literalMediaFileError As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''txtTags control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTags As Global.Netpay.Web.Controls.TagsInput
    
    '''<summary>
    '''dlCatagory control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dlCatagory As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''UpdatePanel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''cblSubCategory control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cblSubCategory As Global.System.Web.UI.WebControls.CheckBoxList
    
    '''<summary>
    '''btnSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSave As Global.System.Web.UI.WebControls.LinkButton
End Class
