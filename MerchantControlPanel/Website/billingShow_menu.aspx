<%@ Page Language="VB" Inherits="Netpay.MerchantControlPanel.PopupPage" EnableEventValidation="false" EnableViewStateMac="false" ValidateRequest="false" EnableViewState="false" %>

<%@ Import Namespace="Netpay.MerchantControlPanel" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<script runat="server">	
    Dim nBillingCompanysID As Integer = HttpContext.Current.Request("BillingCompanysID").ToInt32(0)
    Dim nBillingNumber As Integer = HttpContext.Current.Request("billingNumber").ToInt32(0)
    Dim nInvoiceType As Integer = HttpContext.Current.Request("invoiceType").ToInt32(0)

    Dim sWhere As String = String.Empty, sSQL As String = String.Empty

    Dim rsData2 As SqlDataReader

    Dim sBillOriginalDisabled As String = String.Empty, sBillOriginalText As String = String.Empty
	
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        'Not admin need to check company owns this invoice
        If Merchant.ID > 0 Then sWhere = "id_MerchantID=" & Merchant.ID & " AND "
        sSQL = "SELECT IsNull(payDate, id_InsertDate) payDate, payType, id_MerchantID, id_InsertDate, id_IsPrinted " & _
        " FROM tblInvoiceDocument LEFT JOIN tblTransactionPay ON tblInvoiceDocument.ID=tblTransactionPay.InvoiceDocumentID" & _
        " WHERE " & sWhere & " id_BillingCompanyID=" & nBillingCompanysID & " AND id_InvoiceNumber=" & nBillingNumber & " AND id_Type=" & nInvoiceType
		
        rsData2 = dbPages.ExecReader(sSQL)
        If Not rsData2.Read() Then
            rsData2.Close()
            Response.Write("No record found<br>")
            Response.End()
        Else
            If Not IsDBNull(rsData2("id_IsPrinted")) And rsData2("id_IsPrinted") Then
                sBillOriginalDisabled = "disabled"
                sBillOriginalText = "(�����)"
            End If
            If Not IsDBNull(rsData2("payType")) And Int(rsData2("payType")) = 0 Then
             
            End If
           
        End If
    End Sub
</script>

<html>
<head runat="server">
    <title></title>
   
     <!-- CSS -->
    <link rel="stylesheet" href="../Templates/Tmp_MahalaUS/Website/styles/Style.css" />
    <link rel="stylesheet" href="../Templates/Tmp_MahalaUS/Website/styles/button.css" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../Website/Plugins/jquery-ui-1.8.10.custom/css/custom-theme/jquery-ui-1.8.10.custom.css" type="text/css" />
    <link rel="stylesheet" href="../Website/Plugins/fancybox/jquery.fancybox-1.3.4.css" />
    <link href="../Website/Plugins/font-awesome-4.5.0/css/font-awesome.css" rel="stylesheet" />

</head>
<body style="direction: rtl; font-size: 13px;">
    <script type="text/javascript">
        function CheckForm() {
            if (document.getElementById('chargeList').checked == false & document.getElementById('billOriginal').checked == false & document.getElementById('billCopy').checked == false) {
                alert('�� ����� �� ������ ���� ������');
                return false;
            }

            return true;
        }

        function printAction() {
            if (document.getElementById('billOriginal').checked == true) {
                window.parent.frames['fraBillactions'].location.href = 'billingShow_actions.aspx?BillingCompanysID=<%= nBillingCompanysID %>&billingNumber=<%= nBillingNumber %>&invoiceType=<%= nInvoiceType %>';
                document.getElementById('billOriginal').disabled = true;
                document.getElementById('billOriginal').checked = false;
            }
            parent.fraBillprint.focus();
            parent.fraBillprint.print();
        }

        function removeViewState() {
            var vobj;
            if (vobj = document.getElementById('frmBilling').elements.namedItem('__VIEWSTATE')) vobj.parentNode.removeChild(vobj);
            if (vobj = document.getElementById('frmBilling').elements.namedItem('__VIEWSTATEENCRYPTED')) vobj.parentNode.removeChild(vobj);
        }

        function printStatus() {
            document.getElementById("bPrint").style.cursor = "not-allowed";
            document.getElementById("bPrint").disabled = true;
        }
    </script>
    <form action="billingShow_pages.aspx" name="frmBilling" id="frmBilling" method="get" target="fraBillprintPreview" onsubmit="return CheckForm();" runat="server" enableviewstate="false">
        <div class="billingMenu">
            <div class="percent50 align-right">
                <div>����� ������� ��' <%= nBillingNumber %></div> 
            </div>
            <div class="percent50 align-right">
                <div>����� ����� <%= rsData2("id_InsertDate") %></div>
            </div>
            <div class="spacer"></div>
     </div>
        <input type="hidden" name="type" value="preview" />
        <input type="hidden" name="BillingCompanysID" value="<%= nBillingCompanysID %>" />
        <input type="hidden" name="billingNumber" value="<%= nBillingNumber %>" />
        <input type="hidden" name="InvoiceType" value="<%= nInvoiceType %>" />
        
        <div class="padding-10">
         <div class="margin-bottom-10">��� �� ������ ����� ����</div>
          <div class="OptionBillingMenu">
              <div>
              <input <%= sBillOriginalDisabled %> type="checkbox" id="billOriginal" name="billOriginal"  value="1" <% If sBillOriginalDisabled = "" Then%>onclick="printStatus();" <% End If%>>
              ���� ������� ���� <%= sBillOriginalText %>
              </div>
              <div><input type="checkbox" name="billCopy" id="billCopy" value="1"  onclick="printStatus();">  ���� ������� ���� </div>
              <div><asp:CheckBox ID="chargeList" runat="server" /> ���� ����� ������</div>    
            </div>
           </div>
      
         <div class="padding-10">
            <input type="submit" name="action" value="����� ������"  class="btn btn-cons-short btn-inverse" onclick="removeViewState();">
            <input type="button" name="bPrint" id="bPrint" value="����" disabled class="btn btn-cons-short btn-inverse"  onclick="printAction();"><br>
        </div>
        <asp:Literal ID="litNoMerchant" runat="server" Visible="false" />

    </form>
</body>
</html>
<% rsData2.Close()%>