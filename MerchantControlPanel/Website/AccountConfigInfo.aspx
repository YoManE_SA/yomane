﻿<%@ Page Language="VB" Inherits="Netpay.MerchantControlPanel.MasteredPage" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" Title="ACCOUNT CONFIGURATION" %>

<script runat="server">
    Sub Page_Load()
        mcsData.MerchantID = Merchant.ID
    End Sub
</script>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTitle" runat="Server">
     <asp:Literal Text="<%$Resources: Title%>" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
             <asp:Literal Text="<%$Resources: Title%>" runat="server" />
        </div>
        <div class="content-background">
        
            <div class="section">
                <div class="margin-bottom-10 frame-border-option">
                      <div class="margin-bottom-10"><asp:Literal Text="<%$Resources: AccountInfo%>" runat="server" /></div>  
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">
                         <netpay:MerchantConfigSnapshot TableCssClass="FieldsTable" ID="mcsData" runat="server" />
                    </div>
                </div>
                 
            </div>
        </div>
    </div>
</asp:Content>
