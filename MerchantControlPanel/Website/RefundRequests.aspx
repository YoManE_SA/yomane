﻿<%@ Page Title="PageTitle" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master"
    AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_RefundRequests"
    CodeBehind="RefundRequests.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <script language="JavaScript" type="text/javascript">
        function ExpandNode(cellID) {
            trObj = document.getElementById("trInfo" + cellID)
            imgObj = document.getElementById("oListImg" + cellID)
            if (trObj) {
                if (trObj.style.display == '') {
                    imgObj.src = '/NPCommon/Images/tree_expand.gif';
                    trObj.style.display = 'none';
                }
                else {
                    imgObj.src = '/NPCommon/Images/tree_collapse.gif';
                    trObj.style.display = '';
                }
            }
        }  
    </script>
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">
            <div class="legend-menu">
                <div class="align-right">
                    <netpay:PopupButton ID="PopupButton1" IconSrc="Images/iconLegend.gif"
                        Width="110" Height="88" Text="<%$Resources:MultiLang,Legend  %>" runat="server">
                        <netpay:LegendView runat="server" ID="lvRefundRequests" ViewEnumName="Netpay.Bll.Transactions.RefundRequest+RequestStatus" />
                    </netpay:PopupButton>
                </div>
            </div>
            <div class="section">
                <div>
                    <div class="align-left percent80">
                        <div class="percent40 align-left">
                            <div><asp:Literal  runat="server" Text="<%$Resources:MultiLang,InsertDates %>" /></div>
                            <div><netpay:DateRangePicker ID="wcDateRangePicker"  HtmlLayout="Flow" runat="server" /></div>
                        </div>
                         <div class="percent40 align-left">
                               <div><asp:Literal runat="server" Text="<%$Resources:MultiLang,Status %>" /></div>
                               <div><netpay:RefundRequestStatusDropDown runat="server" ID="cbStatus" ShowClient="true" BlankSelectionText="<%$Resources:MultiLang,Status %>" CssClass="percent100" /></div>
                         </div>
                        
                    </div>
                    <div class="filter-double">
                        <div class="quick-search">
                            <asp:Literal runat="server" Text="<%$Resources:FilterIDType %>" />
                            <asp:Literal runat="server" Text="<%$Resources:FilterIDValue %>" />
                            <div class="field-quick">
                                <asp:DropDownList runat="server" ID="cbNumType" CssClass="Field_132">
                                    <asp:ListItem Text="<%$Resources:FilterRequestID %>" Value="Request" />
                                    <asp:ListItem Text="<%$Resources:FilterTransID %>" Value="Transaction" />
                                </asp:DropDownList>
                                <div style="height: 10px;"></div>
                                <asp:TextBox runat="server" ID="txtSearchID" CssClass="Field_132" />
                            </div>
                            <div class="button-quick">
                                <asp:LinkButton ID="btnIDSearch" runat="server" CssClass="btn" Text="<%$Resources:MultiLang,Show %>"
                                    OnClick="btnIDSearch_Click" />
                            </div>
                        </div>
                    </div>
            </div>
            <div class="spacer">
            </div>
                  <div class="wrap-button-bar">
                       <asp:LinkButton ID="BtnSearch" runat="server" CssClass="btn btn-cons-short btn-inverse" OnClick="btnSearch_Click"><i class="fa fa-search"></i> <asp:Literal runat="server" Text="<%$Resources:MultiLang,Search %>" /> </asp:LinkButton>
                  </div>
        </div>
        <asp:PlaceHolder ID="phResults" Visible="false" runat="server">
            <div class="section">
                <table class="exspand-table">
                    <tr>
                        <th>
                            &nbsp;
                        </th>
                        <th style="text-align: <%= WebUtils.CurrentAlignment %>;">
                            <asp:Literal ID="Literal5" runat="server" Text="<%$Resources:RequestNum %>" />
                        </th>
                        <th style="text-align: <%= WebUtils.CurrentAlignment %>;">
                            <asp:Literal ID="Literal6" runat="server" Text="<%$Resources:RequestDate %>" />
                        </th>
                        <th style="text-align: <%= WebUtils.CurrentAlignment %>;">
                            <asp:Literal ID="Literal7" runat="server" Text="<%$Resources:MultiLang,PaymentMethod %>" />
                        </th>
                        <th style="text-align: <%= WebUtils.CurrentAlignment %>;">
                            <asp:Literal ID="Literal8" runat="server" Text="<%$Resources:RefundAmount %>" />
                        </th>
                        <th style="text-align: <%= WebUtils.CurrentAlignment %>;">
                            <asp:Literal ID="Literal9" runat="server" Text="<%$Resources:MultiLang,Status %>" />
                        </th>
                        <th style="text-align: <%= WebUtils.CurrentAlignment %>;">
                            <asp:Literal ID="Literal11" runat="server" Text="<%$Resources:OriginalTrans %>" />
                        </th>
                        <th nowrap style="text-align: <%= WebUtils.CurrentAlignment %>; ">
                            <asp:Literal ID="Literal12" runat="server" Text="<%$Resources:OriginalDate %>" />
                        </th>
                    </tr>
                    <asp:Repeater ID="repeaterResults" runat="server">
                        <ItemTemplate>
                            <tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='#FFFFFF';">
                                <td>
                                    <div style="width: 15px; height: 15px;" class="<%# GetStatusStyle(CType(Container.DataItem, Transactions.RefundRequest)) %>">
                                    </div>
                                </td>
                                <td>
                                    <%# CType(Container.DataItem, Transactions.RefundRequest).ID%>
                                </td>
                                <td>
                                    <%# CType(Container.DataItem, Transactions.RefundRequest).Date.ToString("dd/MM/yyyy")%>
                                </td>
                                <td>
                                    <netpay:PaymentMethodView ID="PaymentMethodView1" runat="server" PaymentMethodID="<%# CType(Container.DataItem, Transactions.RefundRequest).Transaction.PaymentMethodID %>"
                                        PaymentMethodDisplay="<%# CType(Container.DataItem, Transactions.RefundRequest).Transaction.PaymentMethodDisplay%>" />
                                </td>
                                <td class="Numeric">
                                    <%# CType(Container.DataItem, Transactions.RefundRequest).RequestAmount.ToAmountFormat(CType(Container.DataItem, Transactions.RefundRequest).CurrencyIso) %>
                                </td>
                                <td>
                                    <asp:Literal runat="server" ID="ltStatusText" Text="<%# GetRefundStatusText(CType(Container.DataItem, Transactions.RefundRequest).Status)%>" />
                                    <asp:LinkButton ID="btnDelete" Text="<%$Resources:MultiLang,CancelRequest %>" OnCommand="CancelRefundRequest"
                                        CommandArgument="<%# CType(Container.DataItem, Transactions.RefundRequest).ID %>" Visible="<%# CType(Container.DataItem, Transactions.RefundRequest).Status = Transactions.RefundRequest.RequestStatus.Pending %>"
                                        OnClientClick="return deleteConfirmation()" runat="server" />
                                </td>
                                <td>
                                    <a href="TransApproved.aspx?TransID=<%# CType(Container.DataItem, Transactions.RefundRequest).TransactionID%>">
                                        <%# CType(Container.DataItem, Transactions.RefundRequest).TransactionID%></a>
                                </td>
                                <td>
                                    <%# CType(Container.DataItem, Transactions.RefundRequest).Transaction.InsertDate.ToString("dd/MM/yyyy HH:mm")%>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
                <div class="xls-wrap">
                    <netpay:ExcelButton ID="btnExportExcel" Text="<%$Resources:MultiLang,ExportToXls %>"
                        Enabled="false" IconImageOn="xls.png" IconImageOff="xls_off.png" runat="server" OnClick="ExportExcel_Click" />
                </div>
                <netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
            </div>
        </asp:PlaceHolder>
    </div>
    </div>
</asp:Content>
