﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_PayeeMakePayment" CodeBehind="PayeeMakePayment.aspx.vb" %>
<%@ Register Src="~/Website/Controls/BankAccountInfo.ascx" TagPrefix="UC" TagName="BankAccountInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$ Resources:PageTitle %>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$ Resources:PageTitle %>"
                runat="server" />
        </div>
        <div class="content-background">
            <div class="section">
                <h3>
                    <asp:Literal ID="litProfileTitle" runat="server" /></h3>
                <asp:Literal ID="litDataHeading" runat="server" />
            </div>
            <div class="section">
                <div>
                    <div class="Beneficiary-detalis">
                        <h3>
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources: PersonalBusinessDetails %>" /></h3>
                        <span class="bold">
                            <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources: Multilang, Identifier %>" />:</span>&nbsp;<%# Item.LegalNumber %><br />
                        <span class="bold">
                            <asp:Literal ID="Literal9" runat="server" Text="<%$ Resources: PersonalName %>" /></span>:&nbsp;<%# Item.CompanyName %>
                        <span runat="server" visible="<%# Not String.IsNullOrEmpty(Item.ContactName) %>">
                            <span class="bold">
                                <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources: ContactPerson %>" />:</span>&nbsp;<%# Item.ContactName %></span><br />
                        <span runat="server" visible="<%# Not String.IsNullOrEmpty(Item.PhoneNumber) %>">
                            <span class="bold">
                                <asp:Literal ID="Literal11" runat="server" Text="<%$ Resources: Multilang, Phone %>" />:</span>&nbsp;<%# Item.PhoneNumber %></span><br />
                        <span runat="server" visible="<%# Not String.IsNullOrEmpty(Item.FaxNumber) %>">
                            <span class="bold">
                                <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources: Multilang, Fax %>" />:</span>&nbsp;<%# Item.FaxNumber %></span><br />
                        <span runat="server" visible="<%# Not String.IsNullOrEmpty(Item.EmailAddress) %>"><span
                            class="bold">
                            <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources: Multilang, Email %>" />:</span>&nbsp;<%# Item.EmailAddress %></span><br />
                        <span runat="server" visible="<%# Not String.IsNullOrEmpty(Item.StreetAddress) %>"><span
                            class="bold">
                            <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources: Multilang, Address %>" />:</span>&nbsp;<%# Item.StreetAddress %></span><br />
                        <span runat="server" visible="<%# Not String.IsNullOrEmpty(Item.Comment) %>"><span
                            class="bold">
                            <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources: Multilang, Comment %>" />:</span>&nbsp;<%# Item.Comment %></span>
                    </div>
                    <div class="Beneficiary-detalis">
                        <asp:PlaceHolder runat="server" ID="phIsraeliBank">
                            <h3>
                                <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources: IsraeliBankAccountDetails %>" /></h3>
                            <span runat="server" visible="<%# Not String.IsNullOrEmpty(Item.CompanyName) %>"></span><span class="bold">
                                <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources: AccountName %>" />:</span>&nbsp;<%# Item.CompanyName %><br />
                            <span runat="server" visible="<%# Not String.IsNullOrEmpty(Item.LegalNumber) %>"></span><span class="bold">
                                <asp:Literal ID="Literal19" runat="server" Text="<%$ Resources: CompanyLegalNumber %>" />:</span>&nbsp;<%# Item.LegalNumber %><br />
                            <span runat="server" visible="<%# Not String.IsNullOrEmpty(ILBankAccount.ILBranchCode) %>"></span><span class="bold">
                                <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources: BranchNumber %>" />:</span>&nbsp;<%# ILBankAccount.ILBranchCode %><br />
                            <span runat="server" visible="<%# Not String.IsNullOrEmpty(MasterAccount.AccountNumber) %>"></span><span class="bold">
                                <asp:Literal ID="Literal22" runat="server" Text="<%$ Resources: AccountNumber %>" />:</span>&nbsp;<%# MasterAccount.AccountNumber %><br />
                            <span runat="server" visible="<%# Not String.IsNullOrEmpty(ILBankAccount.ILBankCode) %>"></span><span class="bold">
                                <asp:Literal ID="Literal23" runat="server" Text="<%$ Resources: BankName %>" />:</span>&nbsp;<%# ILBankAccount.ILBankCode %><br />
                        </asp:PlaceHolder>
                    </div>
                    <div class="Beneficiary-detalis">
                        <asp:PlaceHolder runat="server" ID="phAbroadBank">
                            <UC:BankAccountInfo runat="server" id="BankAccountInfo" Item='<%# MasterAccount %>' />
                        </asp:PlaceHolder>
                    </div>
                    <div class="Beneficiary-detalis">
                        <asp:PlaceHolder runat="server" ID="phCorrespondingBank">
                            <UC:BankAccountInfo runat="server" id="BankAccountInfo1" Item='<%# CorAccount %>' />
                        </asp:PlaceHolder>
                    </div>
                    <div class="spacer">
                    </div>
                </div>
            </div>
            <div class="section">
                <table>
                    <tr id="trConfirmationFile" runat="server">
                        <td class="Field_100">
                            <asp:Literal ID="Literal1" Text="<%$ Resources:MultiLang,File %>" runat="server" />
                        </td>
                        <td>
                            <asp:FileUpload ID="fuConfirmation" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="Field_100">
                            <asp:Literal ID="Literal18" Text="<%$ Resources:MultiLang,Date %>" runat="server" />
                        </td>
                        <td>
                            <netpay:DateRangePicker ID="wcDateRangePicker" class="Field_150" Layout="Horizontal"
                                runat="server" IsToDateVisible="false" />
                        </td>
                    </tr>
                    <tr>
                        <td class="Field_100">
                            <asp:Literal ID="Literal3" Text="<%$ Resources:MultiLang,Comment %>" runat="server" />
                        </td>
                        <td>
                            <asp:TextBox ID="paymentMerchantComment" runat="server" TextMode="MultiLine" Width="600"
                                Height="80" />
                        </td>
                    </tr>
                    <tr>
                        <td class="Field_100">
                            <asp:Literal ID="Literal4" Text="<%$ Resources:MultiLang,Currency %>" runat="server" />
                        </td>
                        <td>
                            <netpay:CurrencyDropDown ID="ddPaymentCurrency" EnableBlankSelection="false" runat="server"
                                Width="150" />
                        </td>
                    </tr>
                    <tr>
                        <td class="Field_100">
                            <asp:Literal ID="Literal5" Text="<%$ Resources:MultiLang,Amount %>" runat="server" />
                        </td>
                        <td>
                            <asp:TextBox ID="paymentAmount" runat="server" Width="150" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Literal ID="litLimits" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="BtnSubmit" class="btn btn-primary" BorderWidth="0" OnClick="BtnSubmit_OnClick"
                                Text="<%$ Resources:SubmitOrder %>" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
            <div class="section">
                <asp:Literal ID="litInsertText" runat="server" />
                <asp:Literal ID="litNoBalance" runat="server" />
                <asp:Literal ID="litOrderBeneficiary" runat="server" />
                <asp:Literal ID="Literal6" Text="<%$ Resources:CompleteOrdersList %>" runat="server" />
                <a class="btn btn-primary" href="ViewPaymentsReport.aspx">
                    <asp:Literal ID="Literal7" Text="<%$ Resources:PaymentsView %>" runat="server" /></a>
            </div>
        </div>
    </div>
</asp:Content>
