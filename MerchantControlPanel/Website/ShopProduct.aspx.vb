﻿Imports Netpay.Infrastructure
Imports Netpay.Bll
Imports Netpay.Web
Imports Netpay.Bll.Shop
Imports Netpay.Web.Controls

Public Class ShopProduct
    Inherits MasteredPage
    'Protected ItemData As Bll.Shop.Products.Product
    Public ItemRecurring As System.Collections.Generic.List(Of Bll.Transactions.Recurring.Series)

    Private _shop As RegionShop
    Protected ReadOnly Property CurrentShop As RegionShop
        Get
            If _shop Is Nothing Then
                Dim id As Integer
                Dim result As Boolean = Integer.TryParse(Request("shopId"), id)
                If result Then
                    _shop = RegionShop.Load(id)
                End If
                If _shop Is Nothing Then Response.Redirect("ShopSelection.aspx", True)
            End If
            Return _shop
        End Get
    End Property

    Private _product As Bll.Shop.Products.Product
    Protected ReadOnly Property CurrentProduct As Bll.Shop.Products.Product
        Get
            If _product Is Nothing Then
                Dim id As Integer
                Dim result As Boolean = Integer.TryParse(Request("ID"), id)
                If result Then
                    _product = Bll.Shop.Products.Product.Load(id)
                Else
                    _product = New Bll.Shop.Products.Product(Merchant.ID)
                    _product.Texts.Add(New Bll.Shop.Products.ProductText(0, Bll.International.Language.Get(WebUtils.CurrentLanguage).Culture.Substring(0, 2)))
                    _product.ShopID = CurrentShop.ShopId
                    btnDelete.Visible = False
                    btnPreview.Visible = False
                    upPublished.Visible = False
                End If
            End If
            Return _product
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            If CurrentProduct.IsDynamicProduct Then
                ValidRequiredfieldAmount.Enabled = False
                ValidNegativeNumbersfield.Enabled = False
            End If

            Dim texts = CurrentProduct.Texts.FirstOrDefault()
            If texts IsNot Nothing Then
                txtName.Text = texts.Name
                txtDescription.Text = texts.Description
                txtReceiptText.Text = texts.ReceiptText
                chkReceiptLink.Checked = Not String.IsNullOrEmpty(texts.ReceiptLink)
                txtReceiptLink.Text = texts.ReceiptLink
                txtReceiptLink.Enabled = Not String.IsNullOrEmpty(texts.ReceiptLink)
                txtMetaTitle.Text = texts.MetaTitle
                txtMetaKeyword.Text = texts.MetaKeyword
                txtMetaDescription.Text = texts.MetaDescription
            End If

            txtProductLink.Text = ProductLink
            ctlShareLinks.ShareUrl = ProductLink

            Dim urlParams = $"merchantNumber={Merchant.Number}&shopId={CurrentShop.ShopId}&productId={CurrentProduct.ID}"
            imgQrCode.ImageUrl = String.Format("{0}QrCodes.asmx/RenderGenerateCode?target=ShopProductPage&urlParams={1}", WebUtils.CurrentDomain.WebServicesUrl, HttpUtility.UrlEncode(urlParams))

            rptStock.DataSource = CurrentProduct.Stocks

            Dim categories As List(Of Netpay.Bll.Shop.Products.Category) = Bll.Shop.Products.Category.GetLevel(Nothing)
            For Each category As Netpay.Bll.Shop.Products.Category In categories
                category.Name = Netpay.CommonResources.Utils.GetResourceValue("ProductCategories", category.Name)
            Next
            dlCatagory.DataSource = categories

            ItemRecurring = Bll.Transactions.Recurring.Series.ParseList(CurrentProduct.RecurringString.EmptyIfNull())
            If ItemRecurring.Count > 0 Then ItemRecurring(0).Amount = CurrentProduct.Price
            rptRecurring.DataSource = ItemRecurring
            DataBind()

            Select Case CurrentProduct.Type
                Case Products.ProductType.Physical
                    ddProdType.SelectedIndex = 0
                Case Products.ProductType.Virtual
                    ddProdType.SelectedIndex = 1
                Case Products.ProductType.Download
                    ddProdType.SelectedIndex = 2
                Case Else
                    ddProdType.SelectedIndex = 0
            End Select
            mvProductType.ActiveViewIndex = ddProdType.SelectedIndex

            If CurrentProduct.Categories.Count > 0 Then dlCatagory.SelectedValue = CurrentProduct.Categories.First()
            Catagory_SelectedIndexChanged(sender, e)
            For Each v As Short In CurrentProduct.Categories
                Dim li = cblSubCategory.Items.FindByValue(v.ToString())
                If li IsNot Nothing Then li.Selected = True
            Next

            'If rptText.Items.Count > 0 Then CType(rptText.Items(0).FindControl("dvLanguageData"), System.Web.UI.HtmlControls.HtmlContainerControl).Style.Add("display", "")
            'ClientScript.RegisterStartupScript(Me.GetType(), "Load", "document.getElementById('" + chkIsDynamicProduct.ClientID + "').onclick();" & vbCrLf, True)

            SetPublished()
        End If
    End Sub

    Protected Sub ToggleProduct(sender As Object, e As EventArgs)
        CurrentProduct.IsActive = Not CurrentProduct.IsActive
        CurrentProduct.Save()
        SetPublished()
    End Sub

    Private Sub SetPublished()
        btnEnableProduct.Visible = Not CurrentProduct.IsActive
        btnDisableProduct.Visible = CurrentProduct.IsActive
        divNotActive.Visible = Not CurrentProduct.IsActive
        divActive.Visible = CurrentProduct.IsActive
    End Sub

    Public ReadOnly Property SqrImageLink As String
        Get
            'If CurrentProduct Is Nothing Then Return ""
            Return WebUtils.CurrentDomain.WebServicesUrl + "QrCodes.asmx/RenderGenerateCode?target=PublicPaymentPageV2&urlParams=" + Server.UrlEncode("merchantID=" & Bll.Accounts.Account.Current.AccountNumber & "&item=" & CurrentProduct.ID)
        End Get
    End Property

    Public ReadOnly Property ProductLink As String
        Get
            Return Bll.Shop.Module.GetUrl(Account.AccountNumber, CurrentShop?.ShopId, CurrentProduct?.ID)
        End Get
    End Property

    Protected Overrides Sub OnPreRender(e As EventArgs)
        Dim tabs_script =
            "$('#productProperties_Tabs li').click(function () {" & vbCrLf &
            "   $('#productProperties_Tabs li').removeClass('active');" & vbCrLf &
            "   $(this).addClass('active');" & vbCrLf &
            "   $('#productProperties_TabViews div.tab_content').hide();" & vbCrLf &
            "   var selected_tab = $(this).find('a').attr('href');" & vbCrLf &
            "   $(selected_tab).fadeIn();" & vbCrLf &
            "   return false;" & vbCrLf &
            "});"
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "tabs_script_productProperties", tabs_script, True)

        phProperties.Visible = rptProperties.Items.Count > 0
        rptStock.Visible = rptStock.Items.Count > 0
        rptRecurring.Visible = rptRecurring.Items.Count > 0
        MyBase.OnPreRender(e)
    End Sub

    Protected Sub Button_OnCommand(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "Recurring_Add"
                Dim series As Bll.Transactions.Recurring.Series = New Transactions.Recurring.Series()
                series.Amount = txtPrice.Text
                rptRecurring.AddItem(series)
            Case "Recurring_Remove"
                rptRecurring.RemoveItem(CType(e, RepeaterCommandEventArgs).Item.ItemIndex)
            Case "Property_Add"
                If String.IsNullOrEmpty(txtPropertyName.Text) Then Return
                Dim newProp = Bll.Shop.Products.Property.Load(Merchant.ID, txtPropertyName.Text)
                If newProp Is Nothing Then newProp = New Bll.Shop.Products.Property(Merchant.ID, Shop.Products.Property.PropertyType.Text) With {.Name = txtPropertyName.Text}
                rptProperties.AddItem(newProp)
                txtPropertyName.Text = ""
            Case "Property_Remove"
                rptProperties.RemoveItem(CType(e, RepeaterCommandEventArgs).Item.ItemIndex)
            Case "Stock_DeleteAll"
                While rptStock.Items.Count > 0
                    rptStock.RemoveItem(0)
                End While
            Case "Stock_Remove"
                rptStock.RemoveItem(CType(e, RepeaterCommandEventArgs).Item.ItemIndex)
            Case "Stock_Generate"
                Dim options As List(Of Netpay.Bll.Shop.Products.Property) = GetProperties()
                Dim stocks As List(Of Netpay.Bll.Shop.Products.Stock) = Bll.Shop.Products.Stock.AddStock(Request("ID").ToNullableInt().GetValueOrDefault(), options)
                rptStock.DataSource = stocks
                rptStock.DataBind()
            Case "RemoveMediaFile"
                'If ItemData Is Nothing Then ItemData = Bll.Shop.Products.Product.Load(Request("ID").ToNullableInt().GetValueOrDefault())
                'If ItemData IsNot Nothing Then
                If String.IsNullOrEmpty(CurrentProduct.MediaFileName) Then Exit Select
                If System.IO.File.Exists(CurrentProduct.MediaPysicalPath) Then System.IO.File.Delete(CurrentProduct.MediaPysicalPath)
                CurrentProduct.MediaFileName = Nothing
                CurrentProduct.Save()
                viewDownload.DataBind()
                'End If
        End Select
    End Sub

    Protected Sub OnImageDeleteClick(sender As Object, e As EventArgs)
        'If ItemData Is Nothing Then ItemData = Bll.Shop.Products.Product.Load(Request("ID").ToNullableInt().GetValueOrDefault())
        'If ItemData IsNot Nothing Then
        If String.IsNullOrEmpty(CurrentProduct.ImageFileName) Then Exit Sub
        If System.IO.File.Exists(CurrentProduct.ImagePhysicalPath) Then System.IO.File.Delete(CurrentProduct.ImagePhysicalPath)
        If System.IO.File.Exists(CurrentProduct.ImageSmallPhysicalPath) Then System.IO.File.Delete(CurrentProduct.ImageSmallPhysicalPath)
        CurrentProduct.ImageFileName = Nothing
        CurrentProduct.Save()
        iuImage.DataBind()
        'End If
    End Sub


    Private Function GetProperties() As List(Of Bll.Shop.Products.Property)
        Dim propIds As New List(Of Integer)
        For Each item As RepeaterItem In rptProperties.Items
            Dim pId As Integer = CType(item.FindControl("hfID"), HiddenField).Value.ToNullableInt32().GetValueOrDefault()
            If pId <> 0 Then propIds.Add(pId)
        Next

        Dim orgList = Bll.Shop.Products.Property.Load(propIds, False, False)
        Dim retList As New List(Of Bll.Shop.Products.Property)
        For Each item As RepeaterItem In rptProperties.Items
            Dim pId As Integer = CType(item.FindControl("hfID"), HiddenField).Value.ToNullableInt32().GetValueOrDefault()
            Dim dataItem = orgList.Where(Function(t) t.ID = pId).SingleOrDefault()
            If dataItem Is Nothing Then dataItem = New Bll.Shop.Products.Property(Merchant.ID, Shop.Products.Property.PropertyType.Text)
            dataItem.Name = CType(item.FindControl("hfName"), HiddenField).Value
            dataItem.ValuesString = CType(item.FindControl("txtValuesString"), TextBox).Text
            retList.Add(dataItem)
        Next
        Return retList
    End Function

    Protected Sub Delete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'If ItemData Is Nothing Then ItemData = Bll.Shop.Products.Product.Load(Request("ID").ToNullableInt().GetValueOrDefault())
        'If ItemData IsNot Nothing Then
        CurrentProduct.Delete()
        Response.Redirect("ProductManagment.aspx")
        'End If
    End Sub

    Protected Sub Save_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        CurrentProduct.CurrencyISOCode = CurrentShop.CurrencyIsoCode
        CurrentProduct.Price = txtPrice.Text.ToDecimal(0)
        CurrentProduct.SKU = txtSKU.Text
        'ItemData.IsActive = ddlIsActive.SelectedIndex = 1
        CurrentProduct.IsAuthorize = ddlIsAuthorize.SelectedIndex = 0
        CurrentProduct.Installments = 1
        CurrentProduct.QtyStart = txtQtyStart.Text.ToNullableInt32().GetValueOrDefault()
        CurrentProduct.QtyEnd = txtQtyEnd.Text.ToNullableInt32().GetValueOrDefault()
        CurrentProduct.QtyStep = txtQtyStep.Text.ToNullableInt32().GetValueOrDefault()
        CurrentProduct.QtyAvailable = txtQtyAvailable.Text.ToNullableInt32()
        CurrentProduct.SortOrder = txtSortOrder.Text.ToNullableInt32().GetValueOrDefault()
        CurrentProduct.IsDynamicProduct = chkIsDynamicProduct.Checked
        'ItemData.IsActive = ddlIsActive.SelectedValue = "1"
        'ItemData.Type = ddlProductType.Value.ToNullableInt().GetValueOrDefault(Bll.Shop.Products.ProductType.Physical)
        CurrentProduct.Type = ddProdType.SelectedValue.ToNullableInt().GetValueOrDefault(Bll.Shop.Products.ProductType.Physical)

        If iuImage.HasFile Then
            Dim imageFileName = "UIServices/PP_" + DateTime.Now.Ticks.ToString() + ".jpg"
            If iuImage.SaveAs(Account.MapPublicPath(imageFileName), True) Then CurrentProduct.ImageFileName = imageFileName
        End If

        CurrentProduct.RecurringString = ""
        If Not chkIsDynamicProduct.Checked And chkRecurring.Checked Then
            For Each item As RepeaterItem In rptRecurring.Items
                Dim series As New Bll.Transactions.Recurring.Series
                series.Charges = CType(item.FindControl("txtCharges"), TextBox).Text.ToNullableInt().GetValueOrDefault()
                If series.Charges = 0 Then Continue For
                series.IntervalUnit = CType(item.FindControl("ddlIntervalUnit"), Web.Controls.EnumDropDown).Value.ToNullableEnumByValue(Of Netpay.Infrastructure.ScheduleInterval)().GetValueOrDefault()
                series.IntervalCount = CType(item.FindControl("txtIntervalCount"), TextBox).Text.ToNullableInt().GetValueOrDefault()
                If item.ItemIndex > 0 Then series.Amount = CType(item.FindControl("txtRcurringAmount"), TextBox).Text.ToDecimal(0)

                If Not String.IsNullOrEmpty(CurrentProduct.RecurringString) Then CurrentProduct.RecurringString += ";"
                CurrentProduct.RecurringString += series.RecurringString()
            Next
        End If

        CurrentProduct.Tags = txtTags.Text.Split(New Char() {",", " "}, StringSplitOptions.RemoveEmptyEntries).ToList()
        CurrentProduct.Categories.Clear()
        CurrentProduct.Categories.Add(dlCatagory.SelectedValue.ToNullableInt())
        For Each li As ListItem In cblSubCategory.Items
            If li.Selected Then CurrentProduct.Categories.Add(li.Value.ToNullableInt())
        Next

        CurrentProduct.Save()

        Dim prodText = CurrentProduct.Texts.FirstOrDefault()
        If prodText Is Nothing Then prodText = New Bll.Shop.Products.ProductText(CurrentProduct.ID, "EN")
        prodText.Name = txtName.Text
        prodText.Description = txtDescription.Text
        prodText.ReceiptLink = txtReceiptLink.Text
        prodText.ReceiptText = txtReceiptText.Text
        prodText.MetaTitle = txtMetaTitle.Text
        prodText.MetaDescription = txtMetaDescription.Text
        prodText.MetaKeyword = txtMetaKeyword.Text
        prodText.ProductID = CurrentProduct.ID
        prodText.Save()

        If CurrentProduct.Type = Shop.Products.ProductType.Physical Then
            Dim productProps = GetProperties()
            For Each item In productProps
                If item.ID = 0 Then item.Save()
                For Each subItem In item.Values
                    If subItem.ID = 0 Then subItem.Save()
                Next
            Next
            Dim extstingStock = CurrentProduct.Stocks.ToList()
            For Each item As RepeaterItem In rptStock.Items
                Dim sId As Integer = CType(item.FindControl("hfID"), HiddenField).Value.ToNullableInt32().GetValueOrDefault()
                Dim dataItem As Bll.Shop.Products.Stock = extstingStock.Where(Function(t) t.ID = sId).SingleOrDefault()
                If dataItem Is Nothing Then
                    Dim propValues As New List(Of String)
                    For Each sp As RepeaterItem In CType(item.FindControl("rptProperties"), Repeater).Items
                        propValues.Add(CType(sp.FindControl("hfPropName"), HiddenField).Value)
                    Next
                    dataItem = New Bll.Shop.Products.Stock(CurrentProduct.ID, Bll.Shop.Products.Property.MapValues(productProps, propValues))
                Else
                    extstingStock.Remove(dataItem)
                End If
                If dataItem IsNot Nothing Then
                    dataItem.SKU = CType(item.FindControl("txtSKU"), TextBox).Text
                    dataItem.QtyAvailable = CType(item.FindControl("txtQtyAvailable"), TextBox).Text.ToNullableInt32()
                    dataItem.Save()
                End If
            Next
            For Each s In extstingStock
                s.Delete()
            Next
        ElseIf CurrentProduct.Type = Shop.Products.ProductType.Download Then
            If fuProductFile.HasFile Then
                If fuProductFile.PostedFile.ContentLength > 20480000 Then
                    literalMediaFileError.Text = GetLocalResourceObject("ErrorSizeFileStr")
                    phMediaFileError.Visible = True
                    Exit Sub
                End If

                Dim allowedFileTypes = {".mp3", ".wav", ".avi", ".ogg", ".mkv", ".mp4", ".flac", ".jpg", ".jpeg", ".gif", ".png"}
                If Not allowedFileTypes.Contains(System.IO.Path.GetExtension(fuProductFile.FileName)) Then
                    literalMediaFileError.Text = GetLocalResourceObject("ValidationMediaFileType")
                    phMediaFileError.Visible = True
                    Exit Sub
                End If

                If Not String.IsNullOrEmpty(CurrentProduct.MediaFileName) Then _
                    If System.IO.File.Exists(CurrentProduct.MediaPysicalPath) Then System.IO.File.Delete(CurrentProduct.MediaPysicalPath)
                CurrentProduct.MediaFileName = "MediaFile_" & CurrentProduct.ID & System.IO.Path.GetExtension(fuProductFile.FileName)
                If Not System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(CurrentProduct.MediaPysicalPath)) Then _
                    System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(CurrentProduct.MediaPysicalPath))
                fuProductFile.SaveAs(CurrentProduct.MediaPysicalPath)
                CurrentProduct.Save()
            End If
        End If

        If CurrentProduct.IsActive Then
            Response.Redirect("ShopProducts.aspx?shopId=" + CurrentShop.ShopId.ToString() + "&ShareProductLink=" + ProductLink)
        Else
            Response.Redirect("ShopProducts.aspx?shopId=" + CurrentShop.ShopId.ToString())
        End If
    End Sub

    Protected Sub Catagory_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim categories As List(Of Netpay.Bll.Shop.Products.Category) = Bll.Shop.Products.Category.GetLevel(dlCatagory.SelectedValue)
        For Each category As Netpay.Bll.Shop.Products.Category In categories
            category.Name = Netpay.CommonResources.Utils.GetResourceValue("ProductCategories", category.Name)
        Next

        cblSubCategory.DataSource = categories
        cblSubCategory.DataBind()
    End Sub

    Protected Sub chkReceiptLink_CheckedChanged(sender As Object, e As EventArgs)
        Dim box As CheckBox = sender
        Dim text As TextBox = box.Parent.FindControl("txtReceiptLink")
        text.Enabled = box.Checked
    End Sub

    Protected Sub rptRecurring_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        '    If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
        '        Dim currencyList As Netpay.Web.Controls.CurrencyDropDown = e.Item.FindControl("ddlRecurringCurrencyISOCode")
        '        currencyList.SetCurrencyIDs(Bll.Merchants.Merchant.GetSupportedCurrencies(Merchant.ID).Select(Function(c) c.ID).ToArray())
        '        Dim series As Netpay.Bll.Transactions.Recurring.Series = e.Item.DataItem
        '        If series.Currency.HasValue Then
        '            Dim seriesCurrency As Currency = Currency.Get(series.Currency.Value)
        '            currencyList.SelectedCurrencyIso = seriesCurrency.IsoCode
        '        End If
        '    End If
    End Sub

    Protected Sub ddProdType_SelectedIndexChanged(sender As Object, e As EventArgs)
        mvProductType.ActiveViewIndex = ddProdType.SelectedIndex
    End Sub

    Protected Sub rptStock_ItemCreated(sender As Object, e As RepeaterItemEventArgs)
        Dim scriptManager = System.Web.UI.ScriptManager.GetCurrent(Page)
        Dim button As LinkButton = e.Item.FindControl("btnRemove")
        If button IsNot Nothing Then
            scriptManager.RegisterAsyncPostBackControl(button)
        End If
    End Sub
End Class