﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Website/Login.Master" CodeBehind="Login-n.aspx.vb" Inherits="Netpay.MerchantControlPanel.Login" %>

<%@ Register Src="~/Website/Controls/Faq.ascx" TagName="Faq" TagPrefix="uc" %>
<%@ Register Src="~/Website/Controls/Bulletins.ascx" TagName="Bulletins" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BoxInfoPlaceholder" runat="server">
    <script>
        $(document).keypress(function (event)
        {
            if (event.keyCode == 13)
            {
                __doPostBack('ctl00$ContentPlaceHolder$btnLogin', '');
            }
        });
    </script>
    <ul class="box-info">
         <li class="box">
            <h4>
                <asp:Localize runat="server" Text="<%$ Resources:Login.aspx, BoxNotificationsTitle %>" /></h4>
            <uc:Bulletins MaxRows="3" RenderMode="Compact" runat="server" />
            <div class="button-wrap">
                <a href="javascript:showContent('divNotificationsContent')" id="test" class="btn"><i class="fa fa-plus-circle"></i>
                    <asp:Localize runat="server" Text="<%$ Resources:Login.aspx, More %>" /></a>
            </div>
            <div class="shadow-box">
                <img src="images/Images.Login/shadow-box.png" alt="" />
            </div>
        </li>
           <li class="box">
            <h4>
                <asp:Localize runat="server" Text="<%$ Resources:Login.aspx, BoxFaqTitle %>" /></h4>
            <uc:Faq MaxRows="3" runat="server" />
            <div class="button-wrap">
                <a href="javascript:showContent('divFaqContent')" class="btn"><i class="fa fa-plus-circle"></i>
                    <asp:Localize runat="server" Text="<%$ Resources:Login.aspx, More %>" /></a>
            </div>
            <div class="shadow-box">
                <img src="images/Images.Login/shadow-box.png" alt="" />
            </div>
        </li>
       
            <li class="box">
            <h4 class="dev-title">
                <asp:Localize runat="server" Text="<%$ Resources:Login.aspx, BoxApiTitle %>" /></h4>
            <p>
                  <asp:Localize runat="server" Text="<%$ Resources:Login.aspx, BoxApiDesc %>" />

        
            </p>
            <div class="button-wrap">
                <a href="DevDocs.aspx" class="btn">
                        <asp:Localize runat="server" Text="<%$ Resources:Login.aspx, Go %>" /> <i class="fa fa-arrow-circle-right"></i></a>
            </div>
            <div class="shadow-box">
                <img src="images/Images.Login/shadow-box.png" alt="" />
            </div>
        </li>
    </ul>
    <div class="clearfix"></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
   
        <!-- NOTIFICATION -->
        <div id="divNotificationsContent" class="notifications" style="display: block;">
            <div class="header-notifications">
                <div class="pull-left">
                    <h4>
                        <asp:Localize runat="server" Text="<%$ Resources:Login.aspx, BoxNotificationsTitle %>" /></h4>
                </div>
                <div class="pull-right"><i class="fa fa-list color-icon-header"></i></div>
                <div class="clearfix"></div>
            </div>
            <uc:Bulletins RenderMode="Full" runat="server" />
            <div class="shadow-middle">
                <img src="images/Images.Login/shadow-middle.png" alt="" />
            </div>
        </div>
        <!-- FAQ -->
        <div id="divFaqContent" class="wrap-faq-table" style="display: none;">
            <div class="header-api-table">
                <div class="pull-left">
                    <h4>
                        <asp:Localize runat="server" Text="<%$ Resources:Login.aspx, BoxFaqTitle %>" /></h4>
                </div>
                <div class="pull-right"><i class="fa fa-list color-icon-header"></i></div>
                <div class="clearfix"></div>
            </div>
            <div>
                <uc:Faq RenderMode="Full" runat="server" />
            </div>
            <div class="shadow-middle">
                <img src="images/Images.Login/shadow-middle.png" alt="" />
            </div>
        </div>

        <!-- LOGIN BOX -->
        <asp:UpdateProgress ID="updateProgress" AssociatedUpdatePanelID="updatePanel" runat="server">
            <ProgressTemplate>
                <div class="wrap-loader">
                    <div class="loader">
                        <p>
                            <asp:Localize runat="server" Text="<%$ Resources:Login.aspx, Loading %>" /></p>
                        <img src="images/gears.svg" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>

        <div class="login-box">
            <asp:UpdatePanel ID="updatePanel" RenderMode="Block" UpdateMode="Conditional" ChildrenAsTriggers="true" runat="server">
                <ContentTemplate>
                    <div class="header-login-box">
                        <div class="pull-left">
                            <h4>
                                <asp:Localize runat="server" Text="<%$ Resources:Login.aspx, BoxLoginTitle %>" /></h4>
                        </div>
                        <div class="pull-right"><i class="fa fa-user color-icon-header"></i></div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <div class="title">
                            <i class="fa fa-envelope fa-fw"></i>
                            <asp:Localize runat="server" Text="<%$ Resources:Login.aspx, Mail %>" />
                        </div>
                        <asp:TextBox ID="loginEmail" TabIndex="1" class="form-control" runat="server" />
                    </div>
                    <div class="form-group">
                        <div class="title">
                            <i class="fa fa-user fa-fw"></i>
                            <asp:Localize runat="server" Text="<%$ Resources:Login.aspx, Username %>" />
                        </div>
                        <asp:TextBox ID="loginUsername" TabIndex="2" class="form-control" runat="server" />
                    </div>
                    <div class="form-group">
                        <div class="title">
                            <i class="fa fa-lock fa-fw"></i>
                            <asp:Localize runat="server" Text="<%$ Resources:Login.aspx, Password %>" />
                        </div>
                        <asp:TextBox ID="loginPassword" TabIndex="3" TextMode="Password" class="form-control" runat="server" />
                    </div>
                    <div class="wrap-action-form">
                        <div class="sign-up">
                            <asp:Localize runat="server" Text="<%$ Resources:Login.aspx, HaveNoAccount %>" />
                            <span class="padding-5-left"><i class="fa fa-hand-o-right"></i>
                                <a href="MerchantRegistration.aspx" class="hyperlink">
                                    <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:Login.aspx, SignUpHere %>" /></a>
                            </span>
                        </div>
                      <div class="wrap-login-box-button">
                            <asp:LinkButton ID="btnLogin" ClientIDMode="Static" TabIndex="4" OnClick="btnLogin_Click" CssClass="btn primary" runat="server">
                                <asp:Localize runat="server" Text="<%$ Resources:Login.aspx, ButtonLogin %>" />  <i class="fa fa-arrow-circle-right"></i>
                            </asp:LinkButton>
                       </div>
                      

                          
                    </div>

                    <div id="divError" class="alert-error" visible="false" runat="server">
                        <asp:Literal ID="literalError" runat="server"></asp:Literal>
                    </div>
                    <div class="shadow-login">
                        <img src="images/Images.Login/shadow-login.png" alt="" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="clearfix"></div>
</asp:Content>
