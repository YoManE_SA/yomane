﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class DevDocs
    
    '''<summary>
    '''ddApi control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddApi As Global.Netpay.MerchantControlPanel.DeveloperDocs
    
    '''<summary>
    '''ddInfo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddInfo As Global.Netpay.MerchantControlPanel.DeveloperDocs
    
    '''<summary>
    '''ddGeneral control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddGeneral As Global.Netpay.MerchantControlPanel.AdminDocs
    
    '''<summary>
    '''ddObsolete control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddObsolete As Global.Netpay.MerchantControlPanel.DeveloperDocs
End Class
