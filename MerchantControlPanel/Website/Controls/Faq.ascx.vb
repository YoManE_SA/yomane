﻿Imports Netpay.Web

Public Class Faq
    Inherits System.Web.UI.UserControl

    Public Enum Mode
        Compact
        Full
    End Enum
    Private _renderMode As Mode = Mode.Compact
    Private _take As Integer = Integer.MaxValue
    Public Property MaxRows As Integer
        Get
            Return _take
        End Get
        Set(value As Integer)
            _take = value
        End Set
    End Property
    Public Property RenderMode As Mode
        Get
            Return _renderMode
        End Get
        Set(value As Mode)
            _renderMode = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        panelCompactMode.Visible = RenderMode = Mode.Compact
        panelFullMode.Visible = RenderMode = Mode.Full

        Dim group = Bll.Faqs.Group.Search(New Bll.Faqs.Group.SearchFilters() With {.LanguageId = WebUtils.CurrentLanguage, .IsMerchantCP = True, .IsVisible = True}, Nothing).FirstOrDefault()
        If group Is Nothing Then Exit Sub

        Dim snp As Netpay.Infrastructure.SortAndPage = New Infrastructure.SortAndPage(0, MaxRows)
        Dim data = Bll.Faqs.Faq.Search(New Bll.Faqs.Faq.SearchFilters() With {.GroupId = group.ID}, snp)

        If RenderMode = Mode.Compact Then
            repeaterCompactMode.DataSource = data
            repeaterCompactMode.DataBind()
        Else
            repeaterFullMode.DataSource = data
            repeaterFullMode.DataBind()
        End If
    End Sub

End Class