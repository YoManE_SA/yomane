﻿Imports System.IO
Imports Netpay.Bll.Shop

Public Class ShopsWizard
    Inherits System.Web.UI.UserControl

    Private _shopId As Integer?
    Protected ReadOnly Property ShopId As Integer?
        Get
            If _shopId Is Nothing Then
                Dim id As Integer
                Dim result As Boolean = Integer.TryParse(Request("shopId"), id)
                If result Then
                    _shopId = id
                End If
            End If
            Return _shopId
        End Get
    End Property

    Private Sub ShopsWizard_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then

            btnStepSelection.NavigateUrl = "../shopselection.aspx"
            btnShopLocation.NavigateUrl = "../shoplocations.aspx?shopId=" & ShopId
            btnShopSetup.NavigateUrl = "../shopsetup.aspx?shopId=" & ShopId
            btnShopProucts.NavigateUrl = "../shopproducts.aspx?shopId=" & ShopId

            If ShopId Is Nothing Then
                btnShopLocation.Enabled = False
                btnShopSetup.Enabled = False
                btnShopProucts.Enabled = False
                liShopLocations.Attributes.Add("class", "disable")
                liShopSetup.Attributes.Add("class", "disable")
                liShopProducts.Attributes.Add("class", "disable")
            End If

            Dim fileName As String = Path.GetFileName(Request.Url.AbsolutePath).ToLower()
            Select Case fileName
                Case "shopselection.aspx"
                    liShopSelection.Attributes.Add("class", "active")
                Case "shoplocations.aspx"
                    liShopLocations.Attributes.Add("class", "active")
                    btnShopLocation.Enabled = True
                Case "shopsetup.aspx"
                    liShopSetup.Attributes.Add("class", "active")
                Case "shopproducts.aspx"
                    liShopProducts.Attributes.Add("class", "active")
                Case Else
                    Throw New Exception("Invalid page for shop wizard")
            End Select
        End If
    End Sub
End Class