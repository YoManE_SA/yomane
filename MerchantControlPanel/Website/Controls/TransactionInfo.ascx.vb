﻿Imports Netpay.Web
 
Public Class TransactionInfo
    Inherits System.Web.UI.UserControl
	Implements System.Web.UI.IPostBackEventHandler

    Public Info As Bll.Transactions.Transaction
    Public Cart As Bll.Shop.Cart.Basket

	Public InvoiceURL As String
	Public InvoiceNumber As Integer?

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Info.PaymentMethodID = CommonTypes.PaymentMethodEnum.Admin Then
                phTransactionAdmin.DataBind()

                phButtons.Visible = False
                phFees.Visible = False
                phCart.Visible = False
                phTransaction.Visible = False
                phTransactionAdmin.Visible = True
            Else
                phTransaction.DataBind()
                phFees.DataBind()

                If Info.Status = Infrastructure.TransactionStatus.Declined Or Info.Status = Infrastructure.TransactionStatus.DeclinedArchived Then
                    phDeclineTransaction.DataBind() : phDeclineTransaction.Visible = True
                End If

                If Info.PaymentData IsNot Nothing Then
                    phPayment.DataBind() : phPayment.Visible = True
                    If Info.PaymentData.BillingAddress IsNot Nothing Then
                        phBillingAddress.DataBind() : phBillingAddress.Visible = True
                    End If
                End If

                If Info.PayerData IsNot Nothing Then
                    phPayer.DataBind() : phPayer.Visible = True
                    If Info.PayerData.ShippingDetails IsNot Nothing Then
                        phShippingAddress.DataBind() : phShippingAddress.Visible = True
                    End If
                End If

                If Info.ReplyDescription IsNot Nothing Then
                    If WebUtils.CurrentLanguage = CommonTypes.Language.Hebrew Then ltReplyText.Text = Info.ReplyDescription.DescriptionMerchantHeb _
                    Else ltReplyText.Text = Info.ReplyDescription.DescriptionMerchantEng
                End If

                phReferences.Visible = (Info.Status = Infrastructure.TransactionStatus.Captured)
                If phReferences.Visible Then
                    rptRefunds.DataSource = Bll.Transactions.Transaction.GetRefunds(Info.ID)
                    phReferences.DataBind()
                    rptRefunds.Visible = rptRefunds.Items.Count > 0
                    phReferences.Visible = rptRefunds.Visible Or (Info.OriginalTransactionID > 0)
                End If

                phChargeback.Visible = (Info.Status = Infrastructure.TransactionStatus.Captured) And Info.DeniedStatusID > 0
                If phChargeback.Visible Then
                    rptChbInfo.DataSource = Bll.Transactions.Transaction.ChargebackHistory.LoadForTransaction(Info.ID)
                    phChargeback.DataBind()
                End If

                Cart = Bll.Shop.Cart.Basket.LoadForTransaction(Info.Status, Info.ID)
                phCart.Visible = Cart IsNot Nothing
                If phCart.Visible Then phCart.DataBind()

                phBtnCapture.Visible = (Info.Status = Infrastructure.TransactionStatus.Authorized) And (Info.PassedTransactionID Is Nothing)
                phBtnCaptureInfo.Visible = (Info.Status = Infrastructure.TransactionStatus.Authorized) And (Info.PassedTransactionID IsNot Nothing)
                phBtnSendClientEmail.Visible = (Info.Status = Infrastructure.TransactionStatus.Authorized) Or (Info.Status = Infrastructure.TransactionStatus.Captured)
                phBtnPrintSlip.Visible = (Info.Status = Infrastructure.TransactionStatus.Captured)

                If Bll.Invoices.InvoiceCreator.IsEnabled(Bll.Merchants.Merchant.Current.ID) Then
                    InvoiceNumber = Bll.Invoices.InvoiceCreator.InvoiceStatus(Info.ID, InvoiceURL)
                    'if Not String.IsNullOrEmpty(InvoiceUrl) Then InvoiceUrl = String.Format("<a href=""{0}"" target=""_blank"">{1}</a>", InvoiceUrl, InvoiceNumber)
                    phBtnCreateInvoice.Visible = (Info.Status = Infrastructure.TransactionStatus.Captured) And Not InvoiceNumber.HasValue
                    phBtnInvoiceNumber.Visible = (Info.Status = Infrastructure.TransactionStatus.Captured) And InvoiceNumber.HasValue
                Else
                    phBtnCreateInvoice.Visible = False
                    phBtnInvoiceNumber.Visible = False
                End If
                phBtnAskRefund.Visible = (Info.Status = Infrastructure.TransactionStatus.Captured) And (Bll.Transactions.RefundRequest.GetRefundAbility(Info) <> Infrastructure.TransactionRefundAbility.None)

                If Info.RecurringSeriesID Is Nothing Then
                    phBtnCreateRecurring.Visible = (Info.RecurringSeriesID Is Nothing) And Bll.Transactions.Recurring.MerchantSettings.Current.CanCreateFromExistingTransaction
                    phBtnRecurringNumber.Visible = False
                Else
                    phBtnCreateRecurring.Visible = False
                    phBtnRecurringNumber.Visible = True
                End If

                Dim blockedCard = Bll.Risk.RiskItem.Search(Info.ID, Info.Status, New List(Of Bll.Risk.RiskItem.RiskValueType) From {Bll.Risk.RiskItem.RiskValueType.AccountValue1}, True).FirstOrDefault()
                phBtnAddBlackList.Visible = blockedCard Is Nothing
                phBtnBlackListItem.Visible = blockedCard IsNot Nothing

                phButtons.DataBind()
            End If

            If phBtnPrintSlip.Visible Then phBtnPrintSlip.Visible = SecurityModule.GetSecuredObject(Infrastructure.PermissionObjects.TransactionPrintSlip.ToString(), False).HasPermission(Infrastructure.Security.PermissionValue.Execute)
            If phBtnAskRefund.Visible Then phBtnAskRefund.Visible = SecurityModule.GetSecuredObject(Infrastructure.PermissionObjects.RefundRequest.ToString(), False).HasPermission(Infrastructure.Security.PermissionValue.Execute)
            If phBtnCreateRecurring.Visible Then phBtnCreateRecurring.Visible = SecurityModule.GetSecuredObject(Infrastructure.PermissionObjects.CreateRecurringSeries.ToString(), False).HasPermission(Infrastructure.Security.PermissionValue.Execute)
            If phBtnCreateInvoice.Visible Then phBtnCreateInvoice.Visible = SecurityModule.GetSecuredObject("billingShow_actions.aspx", False).HasPermission(Infrastructure.Security.PermissionValue.Execute)
        End If
    End Sub

    Protected Function GetCardPresent()
        If Info.IsCardPresent = True Then Return "Yes"
        If Info.IsCardPresent = False Then Return "No"
        Return "No record"
    End Function

    Protected Sub SetNotifyMessage(message As String)
		dvNotify.Visible = True
		dvNotify.InnerText = message
	End Sub

	Protected Sub RaisePostBackEvent(eventArgument As String) Implements IPostBackEventHandler.RaisePostBackEvent
		If eventArgument = "SendClientEmail" Then
			Info.SendClientEmail()
            SetNotifyMessage("Email message will be sent in a few minutes")
        End If
	End Sub
End Class