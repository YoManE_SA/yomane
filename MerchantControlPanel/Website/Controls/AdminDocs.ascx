﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AdminDocs.ascx.vb" Inherits="Netpay.MerchantControlPanel.AdminDocs" %>
<%@ Import Namespace="System.IO" %>

<asp:Panel ID="panelCompactMode" runat="server">
    <div class="wrap-api-documents">
        <table class="api-documents">
            <tbody>
                <asp:Repeater ID="repeaterCompactMode" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td><i class="fa fa-file-pdf-o"></i> <a href="<%# Eval("Url")%>" target="_blank"> <%# Eval("FileName").ToString().Truncate(40)%></a></td>
                          
                            <td class="updated-file"><%# Eval("LastUpdated", "{dd/MM/yyyy:d}")%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
</asp:Panel>

<asp:Panel ID="panelFullMode" runat="server">
    <table class="api-table">
        <thead>
            <tr>
                <th>
                    <asp:Literal Text="<%$ Resources:DeveloperDocs.ascx, document %>" runat="server" /> </th>
                <th>
                    <asp:Literal Text="<%$ Resources:DeveloperDocs.ascx, lastUpdated %>" runat="server" /></th>
                <th>
                    <asp:Literal Text="<%$ Resources:DeveloperDocs.ascx, size %>" runat="server" /></th>
            </tr>
        </thead>
        <tbody>
            <asp:Repeater ID="repeaterFullMode" runat="server">
                <ItemTemplate>
                    <tr>
                        <td class="document-name"><i class="fa fa-file-pdf-o  pdf-color"></i> <a href="<%# Eval("Url")%>" target="_blank"> <%# Eval("FileName")%></a></td>
                        <td><%# Eval("LastUpdated", "{0:dd/MM/yyyy HH:mm}")%></td>
                        <td><%# Eval("Size")%> </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </tbody>
    </table>
   
</asp:Panel>


