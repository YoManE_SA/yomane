﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="TransactionInfo.ascx.vb" Inherits="Netpay.MerchantControlPanel.TransactionInfo" %>
<div runat="server" id="dvNotify" visible="false" viewstatemode="Disabled" class="alert-warning">Email was sent</div>
<div class="TransactionInfo">
    <div class="TransactionInfoMain">
        <asp:PlaceHolder ID="phButtons" runat="server">
            <h3 class="title-transactionInfo"><i class="fa fa-bars"></i>
                <asp:Literal runat="server" ID="ltMessage" Text="<%$Resources: NotifcationBar %>" /></h3>
            <div class="TransactionInfoMainlnk">
                <ul>
                    <asp:PlaceHolder runat="server" ID="phBtnCapture">
                        <li><a href="#" onclick="top.netpay.ajaxApi.AjaxMethods.GetCapture('<%# Info.ID %>', function(response){{top.netpay.Common.openPopup(response.Data);}})"><i class="fa fa-bolt"></i>&nbsp;
							<asp:Literal runat="server" Text="<%$Resources: btnCapture  %>" /></a></li>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="phBtnCaptureInfo">
                        <li><a href="TransApproved.aspx?transID=<%# Info.PassedTransactionID %>" target="_top"><i class="fa fa-capture"></i>&nbsp;
							<asp:Literal ID="Literal1" runat="server" Text="<%$Resources: TransactionAlreadyCaptured  %>" /></a></li>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="phBtnPrintSlip">
                        <li><a href="#" onclick="top.netpay.ajaxApi.AjaxMethods.GetTransactionPrintSlip('<%# Info.ID %>', '<%# Info.StatusText %>', function(response){{top.netpay.Common.openPopup(response.Data);}})"><i class="fa fa-print"></i>&nbsp;
							<asp:Literal runat="server" Text="<%$Resources: btnPrintSlip  %>" /></a></li>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="phBtnCreateInvoice">
                        <li><a href="#" onclick="top.netpay.ajaxApi.AjaxMethods.GetTransactionCreateInvoice('<%# Info.ID %>', function(response){{top.netpay.Common.alert(response.Data);}})"><i class="fa fa-download"></i>&nbsp;
							<asp:Literal runat="server" Text="<%$Resources: btnCreateInvoice  %>" /></a></li>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="phBtnInvoiceNumber">
                        <li><a href="<%# InvoiceURL %>" target="_blank"><i class="fa fa-download"></i>&nbsp;
							<asp:Literal runat="server" Text="<%$Resources: InvoiceCreated %>" />: <%# InvoiceNumber %></a></li>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="phBtnAskRefund">
                        <li><a href="#" onclick="top.netpay.ajaxApi.AjaxMethods.GetRefundRequest('<%# Info.ID %>', '<%# Info.StatusText %>', function(response){{top.netpay.Common.openPopup(response.Data);}})"><i class="fa fa-reply"></i>&nbsp;
							<asp:Literal runat="server" Text="<%$Resources: btnRequestRefund  %>" /></a></li>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="phBtnCreateRecurring">
                        <li><a href="#" onclick="top.netpay.ajaxApi.AjaxMethods.GetSeriesFromTransaction('<%# Info.ID %>', '<%# Info.StatusText %>', function(response){{top.netpay.Common.openPopup(response.Data);}})"><i class="fa fa-share-square"></i>&nbsp;
							<asp:Literal ID="Literal4" runat="server" Text="<%$Resources: CreateRecurringSeries  %>" /></a></li>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="phBtnRecurringNumber">
                        <li><a style="color: #111111;" href="RecurringChargesMain.aspx?seriesID=<%# Info.RecurringSeriesID %>" target="_top"><i class="fa fa-share-square"></i>&nbsp;
							<asp:Literal ID="Literal3" runat="server" Text="<%$Resources: RecurringNumber  %>" />: <%# Info.RecurringSeriesID %></a></li>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="phBtnAddBlackList">
                        <li><a href="javascript:top.netpay.ajaxApi.AjaxMethods.AddToBlacklist('<%# Info.ID %>', '<%# Info.StatusText %>', function(response){top.netpay.Common.openPopup(response.Data);})"><i class="fa fa-external-link "></i>&nbsp;
							<asp:Literal runat="server" Text="<%$Resources: blacklistAdd %>" /></a></li>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="phBtnBlackListItem">
                        <li><a href="CCBlackList.aspx?transactionID=<%# Info.ID %>&transactionStatus=<%# CType(Info.Status, Integer)%>"><i class="fa fa-external-link "></i>&nbsp;
							<asp:Literal runat="server" Text="<%$Resources: blacklistExists %>" /></a></li>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="phBtnSendClientEmail">
                        <li><a href="<%# Page.ClientScript.GetPostBackClientHyperlink(Me, "SendClientEmail") %>"><i class="fa fa-envelope-o"></i>&nbsp;
							<asp:Literal runat="server" Text="<%$Resources: EmailClient %>" /></a></li>
                    </asp:PlaceHolder>
                </ul>
            </div>
        </asp:PlaceHolder>
    </div>
    <div class="TransactionInfo-offset"></div>
    <div class="TransactionInfoDetalis">
        <asp:PlaceHolder ID="phPayer" runat="server" Visible="false">
            <h3 class="title-transactionInfo"><i class="fa fa-user"></i>
                <asp:Literal runat="server" Text="<%$Resources: CardHolderDetails  %>" />
            </h3>
            <div class="TransactionBoxInfo">
                <div class="table">
                    <div class="table-row">
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: PaymentName  %>" />:
							<%# Info.PayerData.FirstName %>&nbsp;<%# Info.PayerData.LastName %>
                        </div>
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: PaymentPhone  %>" />:
							<%# Info.PayerData.PhoneNumber%>
                        </div>
                    </div>
                    <div class="table-row">
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: PaymentEmail  %>" />:
							<%# Info.PayerData.EmailAddress %>
                        </div>
                        <div class="table-cell">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$Resources: PersonalNumber  %>" />:
							<%# Info.PayerData.PersonalNumber %>
                        </div>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phDeclineTransaction" runat="server" Visible="false">
            <h3 class="title-transactionInfo"><i class="fa fa-times-circle"></i>
                <asp:Literal runat="server" Text="<%$Resources: TitleDecline  %>" />
            </h3>
            <div class="TransactionBoxInfo">
                <div class="table">
                    <div class="table-row">
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: ReplayCode  %>" />: <%# Info.ReplyCode %>
                        </div>
                    </div>
                    <div class="table-row">
                        <div class="table-cell">
                            <%# Info.ReplyDescriptionText %>
                            <asp:Literal runat="server" ID="ltReplyText" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phChargeback" runat="server" Visible="false">
            <h3 class="title-transactionInfo"><i class="fa fa fa-money"></i>
                <asp:Literal runat="server" Text="<%$Resources: ChargebackDetails %>" />
            </h3>
            <div class="TransactionBoxInfo">
                <asp:Repeater runat="server" ID="rptChbInfo">
                    <ItemTemplate>
                        <div class="table">
                            <div class="table-row">
                                <div class="table-cell">
                                    <asp:Literal runat="server" Text="<%$Resources: Chargeback %>" Visible="<%# CType(Container.DataItem, Transactions.Transaction.ChargebackHistory).TypeID = Netpay.CommonTypes.TransactionHistoryType.Chargeback %>" />
                                    <asp:Literal runat="server" Text='<%# Eval("RecordDate") %>' />
                                </div>

                                <div class="table-cell">
                                    <asp:Literal runat="server" Text="<%$ Resources: ChargebackReasonCode %>" />
                                    <asp:Literal runat="server" Text='<%# Eval("ReasonCode") %>' />

                                </div>
                            </div>
                            <div class="table-row">
                                <div class="table-cell">
                                    <asp:Literal runat="server" Text="<%$Resources: RetrievalRequest %>" Visible="<%# CType(Container.DataItem, Transactions.Transaction.ChargebackHistory).TypeID = Netpay.CommonTypes.TransactionHistoryType.RetrievalRequest %>" />
                                </div>
                                <div class="table-cell">
                                    <asp:Literal runat="server" Text="<%$Resources: NonRefundable %>" Visible="<%# CType(Container.DataItem, Transactions.Transaction.ChargebackHistory).IsPendingChargeback %>" />
                                </div>
                            </div>
                        </div>
                        <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(CType(Container.DataItem, Transactions.Transaction.ChargebackHistory).Title) %>">
                            <div class="transaction-cell">
                                <asp:Literal runat="server" Text='<%# Eval("Title")%>' />
                            </div>
                            <div class="transaction-cell">
                                <b>
                                    <asp:Literal runat="server" Text="<%$ Resources: ChargebackReasonDescription %>" />:</b>
                                <br />
                                <asp:Literal runat="server" Text='<%# Eval("Description")%>' />
                            </div>
                            <div class="transaction-cell">
                                <b>
                                    <asp:Literal runat="server" Text="<%$ Resources: ChargebackReasonRequiredMedia %>" />:</b>
                                <br />
                                <asp:Literal runat="server" Text='<%# Eval("RequiredMedia")%>' />
                            </div>
                            <div class="transaction-cell">
                                <b>
                                    <asp:Literal runat="server" Text="<%$ Resources: ChargebackReasonRefundInfo %>" />:</b>
                                <br />
                                <asp:Literal runat="server" Text='<%# Eval("RefundInfo")%>' />
                            </div>
                        </asp:PlaceHolder>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phPayment" runat="server" Visible="false">
            <h3 class="title-transactionInfo"><i class="fa fa-credit-card"></i>
                <asp:Literal runat="server" Text="<%$Resources: CCDetails  %>" />
            </h3>
            <div class="TransactionBoxInfo">
                <div class="table">
                    <div class="table-row">
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: CCExpirationDate  %>" />:
							<%# Info.PaymentData.MethodInstance.ExpirationDate.GetValueOrDefault().ToString("MM/yyyy")%>
                        </div>
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: CCardType  %>" />:
							<%# Info.PaymentData.MethodInstance.Display  %>
                        </div>
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: BinContryCode  %>" />:
							<%# Info.PaymentData.IssuerCountryIsoCode %>
                        </div>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phBillingAddress" runat="server" Visible="false">
            <h3 class="title-transactionInfo"><i class="fa fa-globe"></i>
                <asp:Literal runat="server" Text="<%$Resources: BillingAddress  %>" />
            </h3>
            <div class="TransactionBoxInfo">
                <div class="table">
                    <div class="table-row">
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: PaymentCountry  %>" />:
							<%# Info.PaymentData.BillingAddress.CountryISOCode %>
                        </div>
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: PaymentZipCode  %>" />:
							<%# Info.PaymentData.BillingAddress.PostalCode  %>
                        </div>
                    </div>
                    <div class="table-row">
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: PaymentCity  %>" />:
							<%# Info.PaymentData.BillingAddress.City %>
                        </div>
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: PaymentAddress  %>" />:
							<%# Info.PaymentData.BillingAddress.AddressLine1 %>   <%# Info.PaymentData.BillingAddress.AddressLine2 %>
                        </div>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phShippingAddress" runat="server" Visible="false">
            <h3 class="title-transactionInfo"><i class="fa fa-truck"></i>
                <asp:Literal runat="server" Text="<%$Resources: ShippingAddress  %>" />
            </h3>
            <div class="TransactionBoxInfo">
                <div class="table">
                    <div class="table-row">
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: ShippingTitle  %>" />:
							<%# Info.PayerData.ShippingDetails.Title%>
                        </div>
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: PaymentCountry  %>" />:
							<%# Info.PayerData.ShippingDetails.CountryISOCode%>
                        </div>
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: PaymentZipCode  %>" />:
							<%# Info.PayerData.ShippingDetails.PostalCode  %>
                        </div>
                    </div>
                    <div class="table-row">
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: PaymentCity  %>" />:
							<%# Info.PayerData.ShippingDetails.City %>
                        </div>
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: PaymentAddress  %>" />:
							<%# Info.PayerData.ShippingDetails.AddressLine1 %>   <%# Info.PayerData.ShippingDetails.AddressLine2 %>
                        </div>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phTransactionAdmin" Visible="false" runat="server">
            <h3 class="title-transactionInfo"><i class="fa fa-pencil-square-o"></i>
                <asp:Literal runat="server" Text="<%$Resources: TransactionInfo  %>" />
            </h3>
            <div class="TransactionBoxInfo">
                <div class="table">
                    <div class="table-row">
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: InsertDate  %>" />:
							<%# Info.InsertDate %>
                        </div>

                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: Amount  %>" />:
							<%# Info.Amount.ToString("0.00")  %>
                        </div>
                    </div>
                </div>
                <div class="padding-10-left padding-5-top  padding-5-bottom">
                    <asp:Literal runat="server" Text="<%$Resources: Comment  %>" />: <%# Info.Comment%>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phTransaction" runat="server">
            <h3 class="title-transactionInfo"><i class="fa fa-pencil-square-o"></i>
                <asp:Literal runat="server" Text="<%$Resources: TransactionInfo  %>" />
                -
                <asp:Literal runat="server" Text="<%$Resources: PayFor  %>" />: <%# Info.PayForText %>
                <asp:PlaceHolder runat="server" Visible='<%# Info.Is3dSecure %>'>-  <span class="secure-3d"><i class="fa fa-unlock-alt"></i>3D secure</span></asp:PlaceHolder>
            </h3>
            <div class="TransactionBoxInfo">
                <div class="table">
                    <div class="table-row">
                          <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: Amount  %>" />:
							<%# Info.Amount.ToString("0.00")  %>
                        </div>
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: CreditType  %>" />: <%# Info.CreditTypeText %>
                        </div>
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: Installments  %>" />: <%# Info.Installments %>
                        </div>
                    </div>
                    <div class="table-row">
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: MoneyTransfer  %>" />: <%# Info.MoneyTransferText %>
                        </div>
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: IPAddress  %>" />: <%# Info.IP %>
                        </div>
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: OrderNumber  %>" />: <%# Info.OrderNumber%>
                        </div>
                    </div>
                    <div class="table-row">
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: TransactionSource  %>" />: <%# Info.TransactionSource %>
                        </div>
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: InsertDate  %>" />:
							<span><%# Info.InsertDate.ToString("dd/MM/yyyy HH:mm") %></span>
                        </div>
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: ApprovalNumber  %>" />: <%# IIf(String.IsNullOrEmpty(Info.ApprovalNumber), "no info", Info.ApprovalNumber) %>
                        </div>
                    </div>
                </div>
                <div visible="<%# Info.AcquirerReferenceNum IsNot Nothing %>" class="padding-10-left padding-5-top padding-5-bottom" runat="server">
                    <asp:Literal runat="server" Text="<%$Resources: BankReferenceNumber  %>" />:
				    <%# Info.AcquirerReferenceNum%>
                </div>
                <div class="padding-10-left padding-5-top  padding-5-bottom">
                    <asp:Literal runat="server" Text="<%$Resources: Comment  %>" />: <%# Info.Comment%>
                </div>
                <div class="padding-10-left padding-5-top  padding-5-bottom">
                    <asp:Literal runat="server" Text="<%$Resources: DebitReferenceCode  %>" />: <%# Info.DebitReferenceCode %>
                </div>
                <div class="padding-10-left padding-5-top  padding-5-bottom">
                    <asp:Literal runat="server" Text="<%$Resources: CardPresent  %>" />: <%# GetCardPresent() %>
                </div>
                <div class="padding-10-left padding-5-top  padding-5-bottom">
                    <asp:Literal runat="server" Text="<%$Resources: DebitReferenceNum  %>" />: <%# Info.DebitReferenceNum %>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phReferences" runat="server" Visible="false">
            <h3 class="title-transactionInfo"><i class="fa fa fa-money"></i>
                <asp:Literal runat="server" Text="<%$Resources: TransactionReferences %>" /></h3>
            <div class="TransactionBoxInfo">
                <div class="table">
                    <div class="table-row">
                        <div class="table-cell">
                            <asp:Repeater runat="server" ID="rptRefunds">
                                <HeaderTemplate>
                                    <asp:Literal ID="Literal5" runat="server" Text="<%$Resources: RefundTransaction  %>" />:
                                </HeaderTemplate>
                                <ItemTemplate><a href="?backgroundColor=F6FEF6&transactionStatus=Captured&transactionID=<%# Eval("ID") %>"><%# Eval("ID") %></a></ItemTemplate>
                                <SeparatorTemplate>, </SeparatorTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                    <asp:PlaceHolder runat="server" Visible="<%# Info.OriginalTransactionID > 0 %>">
                        <div class="table-row">
                            <div class="table-cell">
                                <asp:Literal ID="Literal5" runat="server" Text="<%$Resources: OriginalTransaction  %>" />:
								<a href="?backgroundColor=F6FEF6&transactionStatus=Captured&transactionID=<%# Info.OriginalTransactionID %>"><%# Info.OriginalTransactionID %></a>
                            </div>
                        </div>
                    </asp:PlaceHolder>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phFees" runat="server">
            <h3 class="title-transactionInfo"><i class="fa  fa-usd"></i>
                <asp:Literal runat="server" Text="<%$Resources: Fees  %>" />
            </h3>
            <div class="TransactionBoxInfo">
                <div class="table">
                    <div class="table-row">
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: RatioFee  %>" />:
							<%# Info.RatioFee.ToString("0.00")%>
                        </div>
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: TransactionFee  %>" />:
							<%# Info.TransactionFee.ToString("0.00")%>
                        </div>
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: ChargebackFee  %>" />:
							<%# Info.ChargebackFee.ToString("0.00")%>
                        </div>
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: HandlingFee  %>" />:
							<%# Info.HandlingFee.ToString("0.00")%>
                        </div>
                        <div class="table-cell">
                            <asp:Literal runat="server" Text="<%$Resources: ClarificationFee  %>" />:
							<%# Info.ClarificationFee.ToString("0.00")%>
                        </div>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="phCart">
            <h3 class="title-transactionInfo"><i class="fa fa-shopping-cart"></i>
                <asp:Literal ID="Literal6" runat="server" Text="<%$Resources: Cart  %>" />
            </h3>
            <div class="TransactionBoxInfo">
                <asp:Repeater runat="server" ID="rptCartItems" DataSource="<%# Cart.BasketItems %>">
                    <ItemTemplate>
                        <div class="transaction-cell">
                            <b>
                                <asp:Literal runat="server" Text="<%$Resources: CartID %>" /></b>
                            <%# Eval("ID") %> ,

                            <b>
                                <asp:Literal runat="server" Text="<%$Resources: CartName %>" /></b>
                            <%# Eval("Name")%>
                        </div>

                        <asp:Repeater runat="server" DataSource='<%# Eval("ItemProperties") %>'>
                            <ItemTemplate>
                                <div class="transaction-cell">
                                    <asp:Literal runat="server" Text="<%$Resources: CartValue %>" />
                                    <b><%# Eval("Value")%></b>
                                </div>
                            </ItemTemplate>
                            <SeparatorTemplate>, </SeparatorTemplate>
                        </asp:Repeater>
                        <div class="transaction-cell">
                            <b>
                                <asp:Literal runat="server" Text="<%$Resources: CartQuentity %>" /></b>
                            <%# Eval("Quantity") %>
                              ,
                              <b>
                                  <asp:Literal runat="server" Text="<%$Resources: CartPrice %>" /></b>
                            <%# Eval("Price") %>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        <div class="transaction-cell">
                            <b>
                                <asp:Literal runat="server" Text="<%$Resources: CartTotal %>" /></b>
                            <%# Cart.BasketItems.Count %>
                        </div>
                        <div class="transaction-cell">
                            <b>
                                <asp:Literal runat="server" Text="<%$Resources: CartAmount %>" /></b>
                            <%# Cart.Total %>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </asp:PlaceHolder>
    </div>
    <div class="spacer"></div>
</div>
