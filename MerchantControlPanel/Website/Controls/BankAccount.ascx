﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="BankAccount.ascx.vb" Inherits="Netpay.MerchantControlPanel.BankAccount" %>
<div class="section" id="<%# ClientID %>">
    <table>
        <tr>
            <td class="Field_132">
                <asp:Literal ID="Literal20" runat="server" Text="<%$ Resources:AccountName %>" /><span>**</span>
            </td>
            <td>
                <asp:TextBox ID="txtAccountName" CssClass="meduim" runat="server" TabIndex="17" Text='<%# Item.AccountName %>' />
            </td>
            <td class="Field_132">
                <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:AccountNumber %>" /><span>*</span>
            </td>
            <td>
                <asp:TextBox ID="txtAccountNumber" CssClass="meduim" runat="server" TabIndex="18" Text='<%# Item.AccountNumber %>' />
            </td>
        </tr>
        <tr>
            <td class="Field_132">
                <asp:Literal ID="Literal22" runat="server" Text="<%$ Resources:BankName %>" /><span>*</span>
            </td>
            <td>
                <asp:TextBox ID="txtBankName" CssClass="meduim" runat="server" TabIndex="19" Text='<%# Item.BankName %>' />
            </td>
            <td class="Field_132">
                <asp:Literal ID="Literal23" Text="<%$Resources:MultiLang,City%>" runat="server" /><span>*</span>
            </td>
            <td>
                <asp:TextBox ID="txtBankCity" CssClass="meduim" runat="server" TabIndex="20" Text='<%# Item.BankCity %>' />
            </td>
        </tr>
        <tr>
            <td class="Field_132">
                <asp:Literal ID="Literal24" runat="server" Text="<%$ Resources:BankAddressLine1 %>" />
                <netpay:PageItemHelp ID="PageItemHelp4" Text="<%$Resources:MultiLang,AddressLine1Help%>" Width="300" Height="28" runat="server" />
            </td>
            <td>
                <asp:TextBox ID="txtBankStreet1" CssClass="meduim" runat="server" TabIndex="21" Text='<%# Item.BankStreet1 %>' />
            </td>
            <td class="Field_132">
                <asp:Literal ID="Literal25" runat="server" Text="<%$ Resources:BankAddressLine2 %>" />
                <netpay:PageItemHelp ID="PageItemHelp3" Text="<%$Resources:MultiLang,AddressLine2Help%>" Width="300" Height="28" runat="server" />
            </td>
            <td>
                <asp:TextBox ID="txtBankStreet2" CssClass="meduim" runat="server" TabIndex="22" Text='<%# Item.BankStreet2 %>' />
            </td>
        </tr>
        <tr>
            <td class="Field_132">
                <asp:Literal ID="Literal26" runat="server" Text="<%$ Resources:Region %>" /><span>*</span>
            </td>
            <td>
                <netpay:StateDropDown ID="ddlBankStateISOCode" CssClass="meduim" runat="server" TabIndex="23" Text='<%# Item.BankStateISOCode %>' />
            </td>
            <td class="Field_132">
                <asp:Literal ID="Literal27" runat="server" Text="<%$ Resources:PostalCode %>" />
            </td>
            <td>
                <asp:TextBox ID="txtBankPostalCode" CssClass="meduim" runat="server" TabIndex="24" Text='<%# Item.BankPostalCode %>' />
            </td>
        </tr>
        <tr>
            <td class="Field_132">
                <asp:Literal ID="Literal28" Text="<%$Resources:MultiLang,Country%>" runat="server" /><span>*</span>
            </td>
            <td>
                <netpay:CountryDropDown ID="ddlBankCountryISOCode" CssClass="meduim" runat="server" TabIndex="25" Value='<%# Item.BankCountryISOCode %>' />
            </td>
            <td class="Field_132">SEPA BIC
            </td>
            <td>
                <asp:TextBox ID="bankAbroadSepaBic" CssClass="meduim" runat="server" TabIndex="26" />
            </td>
        </tr>
        <tr>
            <td>Swift Code<span>*</span>
            </td>
            <td>
                <asp:TextBox ID="txtSwiftNumber" CssClass="meduim" runat="server" TabIndex="27" Text='<%# Item.SwiftNumber %>' />
            </td>
            <td class="Field_132">IBAN<span>****</span>
            </td>
            <td>
                <asp:TextBox ID="txtIBAN" CssClass="meduim" runat="server" TabIndex="28" Text='<%# Item.IBAN %>' />
            </td>
        </tr>
        <tr>
            <td class="Field_132">ABA<span>****</span>
            </td>
            <td>
                <asp:TextBox ID="txtABA" CssClass="meduim" runat="server" TabIndex="29" Text='<%# Item.ABA %>' />
            </td>
            <td class="Field_132">Sort Code/BLZ/BSB
            </td>
            <td>
                <asp:TextBox ID="bankAbroadSortCode" CssClass="meduim" runat="server" TabIndex="30" />
            </td>
        </tr>
    </table>
</div>
