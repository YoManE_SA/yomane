﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Bulletins.ascx.vb" Inherits="Netpay.MerchantControlPanel.Bulletins" %>

<asp:Panel ID="panelCompactMode" runat="server">
    <div class="wrap-api-documents">
        <table class="api-documents">
            <tbody>
                <asp:Repeater ID="repeaterCompactMode" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td style="cursor:pointer;" onclick="javascript:showContent('divNotificationsContent')"><i class="fa fa-flag-o secondary-color"></i> <%# Eval("Text").ToString().Truncate(35)%></td>
                            <td class="updated-file"><%# Eval("InsertDate", "{0:dd/MM/yyyy}")%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
</asp:Panel>

<asp:Panel ID="panelFullMode" runat="server">
    <asp:Repeater ID="repeaterFullMode" runat="server">
        <ItemTemplate>
            <div class="notification">
                <div class="date"><%# Eval("InsertDate", "{0:dd/MM/yyyy}")%></div>   
                <%# GetIconHtml(Container.DataItem)%>
                <div class="content">
                    <!--<h5>Lorem Ipsum is</h5>-->
                    <p>
                        <%# Eval("Text")%>
                    </p>
                </div>
                <div class="clearfix"></div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</asp:Panel>
