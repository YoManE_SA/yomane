﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ImageUpload.ascx.vb" Inherits="Netpay.MerchantControlPanel.ImageUpload" %>

<style>
    /*
        fix to aviary messgae box bug
    */
    #avpw_messaging {
        position: absolute;
        top: 0px;
        left: 0px;
        height: 100%;
        width: 100%;
        z-index: 4000;
    }
</style>

<script type='text/javascript'>
    function invokeImageLoaded()
    {
        if (typeof onImageLoaded !== 'undefined' && $.isFunction(onImageLoaded))
            onImageLoaded();
    }
    
    function showImage(input) {
        if (input.files.length > 0 && input.files[0] != null) {
            var limit = <%=BytesSizeLimit%>;
            var limitMsg = "<%=BytesSizeLimitMessage%>";
            if (limit > 0 && input.files[0].size > limit)
            {
                alert(limitMsg);
                return;
            }
            
            var wrongFileTypeMsg = "<%=FileTypeLimitMessage%>";
            var allowedFiles = "<%=AllowedFiles.ToLower()%>".split(',');
            var fileExt = "." + input.files[0].name.split('.').pop().toLowerCase();
            if ($.inArray(fileExt, allowedFiles) == -1)
            {
                alert(wrongFileTypeMsg);
                return;
            }

            var reader = new FileReader();
            reader.onload = function (e) {
                $('#<%# imgImage.ClientID %>').attr('src', e.target.result);
                $('#hEditImage').fadeIn('fast');
                $('#hDeleteImage').fadeIn('fast');
                featherEditor.launch({ image: '<%# imgImage.ClientID %>', url: e.target.result, forceCropPreset: ['Upload image', '<%= Width.Value & "x" & Height.Value %>'], noCloseButton: false });
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function editImage() {
        return featherEditor.launch({ image: '<%# imgImage.ClientID %>', noCloseButton: false });
    }

    var featherEditor = new Aviary.Feather({
        apiKey: '2e1d65b2cd166f65',
        apiVersion: 3,
        theme: 'light',
        tools: ['crop', 'enhance', 'effects', 'frames', 'warmth', 'brightness', 'contrast', 'saturation', 'sharpness', 'text'],
        appendTo: '',
        cropPresets: ['Upload image', '<%# Width.Value & "x" & Height.Value %>'],
        displayImageSize: true,
        forceCropMessage: 'Please crop your image to this size: <%# Width.Value & " x " & Height.Value %>',
        cropPresetsStrict: true,
        onClose: function (isDirty)
        {
            //console.info(isDirty);
        },
        onSave: function (imageID, newURL) {
            $('#<%# imgImage.ClientID %>').attr('src', newURL);
            $('#<%# hfImageUrl.ClientID %>').val(newURL); 
            featherEditor.close();
            envokeImageLoaded();
        },
        onError: function (errorObj) {
            alert(errorObj.message);
        }
    });
</script>
<div class="margin-top-10 margin-bottom-10">
    <div class="align-right">
        <div class="fileUpload btn btn-cons-short btn-inverse margin-bottom-10 ">
            <span><i class="fa fa-folder-open"></i>
                <asp:Literal runat="server" Text="<%$Resources: ImageUpload.ascx, UploadStr %>" /></span>
            <asp:FileUpload runat="server" ID="fuImage" CssClass="upload" onchange="showImage(this)" />
            <asp:HiddenField runat="server" ID="hfImageUrl" />
        </div>
        <div id='injection_site'></div>
        <div>
            <a id="hEditImage" onclick="editImage()" class="btn btn-cons-short btn-default  margin-top-10" style="<%# IIf(ImageExist, "", "display:none")%>;" href="#"><i class="fa fa-pencil-square-o"></i>
                <asp:Literal ID="Literal2" runat="server" Text="<%$Resources: ImageUpload.ascx, EditPhotoStr %>" /></a>
        </div>
        <div>
            <a id="btnDeleteImage" onclick="<%# "if(confirm('Are you sure?'))" & Page.ClientScript.GetPostBackEventReference(Me, "DeleteImage") %>" class="btn btn-cons-short btn-danger margin-top-10" style="<%# IIf(ImageExist, "", "display:none;") %>">
                <i class="fa fa-trash-o"></i>
                <asp:Literal ID="Literal3" runat="server" Text="<%$Resources: ImageUpload.ascx, DeleteStr %>" /></a>
        </div>
    </div>

    <div class="align-left">
        <div>
            <asp:Image runat="server" ID="imgImage" Style="border: 1px solid #b9b9b9;" />
        </div>
        <div class="spinner-wrap" style="display: none;"><i class="fa fa-refresh fa-spin fa-5x"></i></div>
        <div class="spacer"></div>
    </div>
</div>
