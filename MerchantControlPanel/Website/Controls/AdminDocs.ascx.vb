﻿Public Class AdminDocs
    Inherits System.Web.UI.UserControl

    Public Enum Mode
        Compact
        Full
    End Enum

    Private _renderMode As Mode = Mode.Compact
    Private _take As Integer = Integer.MaxValue
    'Private _filters As String = String.Empty
    Public Property Rows As Integer

    Public Property MaxRows As Integer
        Get
            Return _take
        End Get
        Set(value As Integer)
            _take = value
        End Set
    End Property

    Public Property RenderMode As Mode
        Get
            Return _renderMode
        End Get
        Set(value As Mode)
            _renderMode = value
        End Set
    End Property

    'Public Property DocFilters As String
    '    Get
    '        Return _filters
    '    End Get
    '    Set(value As String)
    '        _filters = value
    '    End Set
    'End Property


    Protected Overrides Sub DataBindChildren()
        Dim data As List(Of Bll.Content.AdminDocs.DocFile) = Bll.Content.AdminDocs.GetDocs(MaxRows)
        For Each doc As Bll.Content.AdminDocs.DocFile In data
            doc.Url = Request.ApplicationPath + doc.Url
        Next

        panelCompactMode.Visible = RenderMode = Mode.Compact
        panelFullMode.Visible = RenderMode = Mode.Full

        If RenderMode = Mode.Compact Then repeaterCompactMode.DataSource = data _
        Else repeaterFullMode.DataSource = data
        MyBase.DataBindChildren()
    End Sub

End Class