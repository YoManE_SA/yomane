﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ShareLinks.ascx.vb" Inherits="Netpay.MerchantControlPanel.ShareLinks" %>
<div class="align-left">
    <a href="http://www.facebook.com/sharer.php?u=<%# Server.UrlEncode(ShareUrl)%>" target="_blank"><span class="icon-facebook"></span></a>
    <a href="https://twitter.com/home?status=<%# Server.UrlEncode(ShareUrl)%>" target="_blank"><span class="icon-twitter"></span></a>
    <a href="https://plus.google.com/share?url=<%# Server.UrlEncode(ShareUrl)%>" target="_blank"><span class="icon-googleplus"></span></a>
    <a href="https://www.linkedin.com/shareArticle?mini=true&url=<%# Server.UrlEncode(ShareUrl) %>" target="_blank"><span class="icon-linkedin"></span></a>
    <a href="mailto:?subject=Share%20Your%20Product%20&amp;body=<%# Server.UrlEncode(ShareUrl) %>"><span class="icon-envelope"></span></a>
</div>
