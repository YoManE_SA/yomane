﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DeveloperDocs.ascx.vb" Inherits="Netpay.MerchantControlPanel.DeveloperDocs" %>
<%@ Import Namespace="System.IO" %>

<asp:Panel ID="panelCompactMode" runat="server">
    <div>
        <asp:LinkButton OnClick="DownloadZipped" runat="server"><asp:Literal runat="server" Text="<%$ Resources:DeveloperDocs.ascx, downloadZipped%>" /></asp:LinkButton>
    </div>
    <div class="wrap-api-documents">
        <table class="api-documents">
            <tbody>
                <asp:Repeater ID="repeaterCompactMode" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td><a href="<%# Eval("Url")%>" target="_blank"> <%# Eval("FileName").ToString().Truncate(40)%></a></td>
                          
                            <td class="updated-file"><%# Eval("LastUpdated", "{0:dd/MM/yyyy}")%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
</asp:Panel>

<asp:Panel ID="panelFullMode" runat="server">
    <div style="padding: 10px 0 0;">
         <asp:LinkButton OnClick="DownloadZipped" CssClass="download-zip" runat="server"> <i class="fa fa-files-o"></i> <asp:Literal runat="server" Text="<%$ Resources:DeveloperDocs.ascx, downloadZipped%>" /></asp:LinkButton>
    </div>
    <table class="api-table">
        <thead>
            <tr>
                <th>
                    <asp:Literal Text="<%$ Resources:DeveloperDocs.ascx, document %>" runat="server" /> </th>
                <th>
                    <asp:Literal Text="<%$ Resources:DeveloperDocs.ascx, lastUpdated %>" runat="server" /></th>
                <th>
                    <asp:Literal Text="<%$ Resources:DeveloperDocs.ascx, size %>" runat="server" /></th>
            </tr>
        </thead>
        <tbody>
            <asp:Repeater ID="repeaterFullMode" runat="server">
                <ItemTemplate>
                    <tr>
                        <td class="document-name"><i class="fa fa-file-pdf-o pdf-color"></i> <a href="<%# Eval("Url")%>" target="_blank"> <%# Eval("FileName")%></a></td>
                        <td><%# Eval("LastUpdated", "{0:dd/MM/yyyy HH:mm}")%></td>
                        <td><%# Eval("Size")%> </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </tbody>
    </table>
   
</asp:Panel>


