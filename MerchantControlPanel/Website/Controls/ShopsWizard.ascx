﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ShopsWizard.ascx.vb" Inherits="Netpay.MerchantControlPanel.ShopsWizard" %>


<ul class="steps-wizard">
    <li id="liShopSelection" runat="server">
        <span>1</span>
        <asp:HyperLink ID="btnStepSelection" NavigateUrl="../ShopSelection.aspx" runat="server" Text="<%$Resources:MultiLang,ShopsSelection %>" />
    </li>
    <li id="liShopLocations" runat="server">
        <span>2</span>
        <asp:HyperLink ID="btnShopLocation" NavigateUrl="../ShopLocations.aspx" runat="server" Text="<%$Resources:MultiLang,ShopLocations %>" />
    </li>
    <li id="liShopSetup" runat="server">
        <span>3</span>
        <asp:HyperLink ID="btnShopSetup" NavigateUrl="../ShopSetup.aspx" runat="server" Text="<%$Resources:MultiLang,ShopsSetup %>" />
    </li>
    <li id="liShopProducts" runat="server">
        <span>4</span>
        <asp:HyperLink ID="btnShopProucts" NavigateUrl="../ShopProducts.aspx" runat="server" Text="<%$Resources:MultiLang,ShopsProducts %>" />
    </li>
</ul>
