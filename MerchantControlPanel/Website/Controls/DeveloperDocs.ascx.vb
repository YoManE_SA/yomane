﻿Public Class DeveloperDocs
    Inherits System.Web.UI.UserControl

    Public Enum Mode
        Compact
        Full
    End Enum

    Private _renderMode As Mode = Mode.Compact
    Private _take As Integer = Integer.MaxValue
    Private _filters As String = String.Empty
    Private _zipFileName As String = "DevDocs.zip"

    Public Property Rows As Integer

    Public Property MaxRows As Integer
        Get
            Return _take
        End Get
        Set(value As Integer)
            _take = value
        End Set
    End Property

    Public Property RenderMode As Mode
        Get
            Return _renderMode
        End Get
        Set(value As Mode)
            _renderMode = value
        End Set
    End Property

    Public Property DocFilters As String
        Get
            Return _filters
        End Get
        Set(value As String)
            _filters = value
        End Set
    End Property

    Public Property ZipFileName As String
        Get
            Return _zipFileName
        End Get
        Set(value As String)
            _zipFileName = value
        End Set
    End Property

    Private Function GetFilters() As Bll.Content.DeveloperDoc.Filters
        Dim filters As Bll.Content.DeveloperDoc.Filters = New Bll.Content.DeveloperDoc.Filters()
        filters.Take = MaxRows
        filters.FileNames = New List(Of String)
        Dim split As String() = DocFilters.Split(",")
        For Each filter As String In split
            filters.FileNames.Add(filter)
        Next

        Return filters
    End Function


    Protected Overrides Sub DataBindChildren()
        Dim data As List(Of Bll.Content.DeveloperDoc) = Bll.Content.DeveloperDoc.Get(GetFilters())
        For Each doc As Bll.Content.DeveloperDoc In data
            doc.Url = Request.ApplicationPath + doc.Url
        Next
        Rows = data.Count
        panelCompactMode.Visible = RenderMode = Mode.Compact
        panelFullMode.Visible = RenderMode = Mode.Full

        If RenderMode = Mode.Compact Then repeaterCompactMode.DataSource = data _
        Else repeaterFullMode.DataSource = data
        MyBase.DataBindChildren()
    End Sub

    Protected Sub DownloadZipped(sender As Object, e As EventArgs)
        Dim zipFile As Byte() = Bll.Content.DeveloperDoc.GetZipped(GetFilters(), ZipFileName)
        Response.Clear()
        Response.ClearHeaders()
        Response.ClearContent()
        Response.AddHeader("Content-Disposition", "attachment; filename=" + ZipFileName)
        Response.AddHeader("Content-Length", zipFile.Length)
        Response.ContentType = "application/zip"
        Response.Flush()
        Response.BinaryWrite(zipFile)
        Response.End()
    End Sub
End Class