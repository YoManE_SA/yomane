﻿Public Class Bulletins
    Inherits System.Web.UI.UserControl

    Public Enum Mode
        Compact
        Full
    End Enum
    Private _renderMode As Mode = Mode.Compact
    Private _take As Integer = Integer.MaxValue

    Public Property MaxRows As Integer
        Get
            Return _take
        End Get
        Set(value As Integer)
            _take = value
        End Set
    End Property

    Public Property RenderMode As Mode
        Get
            Return _renderMode
        End Get
        Set(value As Mode)
            _renderMode = value
        End Set
    End Property

    Protected Function GetIconHtml(item As Bll.Content.Bulletin) As String
        Select Case item.Type
            Case Bll.Content.Bulletin.TypeGeneralInfo
                Return "<div class='wrap-icon primary'><i class='fa fa-credit-card'></i></div>"
            Case Bll.Content.Bulletin.TypeSystemInfo
                Return "<div class='wrap-icon warning'><i class='fa fa-exclamation-triangle'></i></div>"
            Case Else
                Return ""
        End Select
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        panelCompactMode.Visible = RenderMode = Mode.Compact
        panelFullMode.Visible = RenderMode = Mode.Full

        Dim filters = New Bll.Content.Bulletin.SearchFilters() With {.Solution = Bll.Solution.MerchantControlPanel, .IncludeExpired = False}
        Dim ps As Netpay.Infrastructure.SortAndPage = New Infrastructure.SortAndPage(0, _take, "MessageDate", True)
        Dim data = Netpay.Bll.Content.Bulletin.Search(filters, ps)

        If RenderMode = Mode.Compact Then
            repeaterCompactMode.DataSource = data
            repeaterCompactMode.DataBind()
        Else
            repeaterFullMode.DataSource = data
            repeaterFullMode.DataBind()
        End If
    End Sub
End Class