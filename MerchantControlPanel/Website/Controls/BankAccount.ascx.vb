﻿Public Class BankAccount
    Inherits System.Web.UI.UserControl
    Public Item As Bll.Accounts.BankAccount

    Public Sub Save(item As Bll.Accounts.BankAccount)
        item.AccountName = txtAccountName.Text
        item.AccountNumber = txtAccountNumber.Text
        item.BankName = txtBankName.Text
        item.BankCity = txtBankCity.Text
        item.BankStreet1 = txtBankStreet1.Text
        item.BankStreet2 = txtBankStreet2.Text
        item.BankPostalCode = txtBankPostalCode.Text
        item.BankStateISOCode = ddlBankStateISOCode.ISOCode
        item.BankCountryISOCode = ddlBankCountryISOCode.ISOCode
        item.SwiftNumber = txtSwiftNumber.Text
        item.IBAN = txtIBAN.Text
        item.ABA = txtABA.Text
    End Sub

    Public ReadOnly Property IsEmpty As Boolean
        Get
            Return String.IsNullOrEmpty(txtAccountName.Text.Trim()) And String.IsNullOrEmpty(txtAccountNumber.Text.Trim()) And String.IsNullOrEmpty(txtBankName.Text.Trim()) And
                String.IsNullOrEmpty(txtBankCity.Text.Trim()) And String.IsNullOrEmpty(txtBankStreet1.Text.Trim()) And String.IsNullOrEmpty(txtBankStreet2.Text.Trim()) And
                String.IsNullOrEmpty(txtBankPostalCode.Text.Trim()) And String.IsNullOrEmpty(ddlBankStateISOCode.SelectedValue) And String.IsNullOrEmpty(ddlBankCountryISOCode.SelectedValue) And
                String.IsNullOrEmpty(txtSwiftNumber.Text.Trim()) And String.IsNullOrEmpty(txtIBAN.Text.Trim()) And String.IsNullOrEmpty(txtABA.Text.Trim())
        End Get
    End Property

    Protected Overrides Sub OnDataBinding(e As EventArgs)
        If Item Is Nothing Then Item = New Bll.Accounts.BankAccount()
        MyBase.OnDataBinding(e)
    End Sub
End Class