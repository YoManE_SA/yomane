﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ShopsWizard
    
    '''<summary>
    '''liShopSelection control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents liShopSelection As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''btnStepSelection control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnStepSelection As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''liShopLocations control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents liShopLocations As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''btnShopLocation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnShopLocation As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''liShopSetup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents liShopSetup As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''btnShopSetup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnShopSetup As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''liShopProducts control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents liShopProducts As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''btnShopProucts control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnShopProucts As Global.System.Web.UI.WebControls.HyperLink
End Class
