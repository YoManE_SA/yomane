﻿Imports System.Drawing.Imaging
Imports System.IO
Imports Netpay.Infrastructure
Public Class ImageUpload
    Inherits System.Web.UI.UserControl
    Implements System.Web.UI.IPostBackEventHandler
    Public ImageExist As Boolean
    Public Event ImageDelete As EventHandler

    Private Shared _allowedFileExtensions As String() = New String() {".jpeg", ".jpg", ".png"}

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        fuImage.Attributes.Add("accept", AllowedFiles)
        Page.ClientScript.RegisterClientScriptInclude("Aviary", ResolveUrl("~/Website/Plugins/Aviary/feather.js"))
        imgImage.Height = Height.Value
        imgImage.Width = Width.Value
    End Sub

    Public Shared ReadOnly Property AllowedFiles As String
        Get
            Return _allowedFileExtensions.ToDelimitedString(",")
        End Get
    End Property

    Public Property Width As System.Web.UI.WebControls.Unit
    Public Property Height As System.Web.UI.WebControls.Unit
    Public Property BytesSizeLimit As Long
    Public ReadOnly Property BytesSizeLimitMessage As String
        Get
            Return String.Format(GetGlobalResourceObject("ImageUpload.ascx", "LimitSizeMsg"), BytesSizeLimit.ToFileSize())
        End Get
    End Property

    Public ReadOnly Property FileTypeLimitMessage As String
        Get
            Return String.Format(GetGlobalResourceObject("ImageUpload.ascx", "LimitFileType"), BytesSizeLimit.ToFileSize())
        End Get
    End Property

    Public WriteOnly Property ImageUrl As String
        Set(value As String)
            imgImage.ImageUrl = value & "?v=" & DateTime.Now.Ticks
            ImageExist = Not String.IsNullOrEmpty(value)
            If ImageExist Then ImageExist = System.IO.File.Exists(Domain.Current.ConvertPublicVirtualToPhysical(value))
        End Set
    End Property

    Public ReadOnly Property ImageBytes() As Byte()
        Get
            If Not String.IsNullOrEmpty(hfImageUrl.Value) Then
                Dim wc = New System.Net.WebClient()
                Return wc.DownloadData(hfImageUrl.Value)
            Else
                Return fuImage.FileBytes
            End If
        End Get
    End Property

    Public Function SaveAs(fileName As String) As Boolean
        Return SaveAs(fileName, False)
    End Function

    Public Function SaveAs(fileName As String, createThumbnail As Boolean) As Boolean
        Dim ext As String = Path.GetExtension(fuImage.PostedFile.FileName).ToLower()
        If Not _allowedFileExtensions.Contains(ext) Then
            Exit Function
        End If

        ' checks png & jpg signature
        If Not ImageBytes.IsImage Then
            Exit Function
        End If

        If (ImageBytes.Length > BytesSizeLimit) Then Return False
        Dim filePath = Path.GetDirectoryName(fileName)
        If Not Directory.Exists(filePath) Then Directory.CreateDirectory(filePath)
        File.WriteAllBytes(fileName, ImageBytes)

        If createThumbnail Then
            Dim samllFileName = $"{filePath}\{Path.GetFileNameWithoutExtension(fileName)}_small{Path.GetExtension(fileName)}"
            Dim bitmap = ImageUtils.FromBytes(ImageBytes)
            Dim resized = ImageUtils.Resize(bitmap, 25)
            Dim compressed = ImageUtils.Compress(resized, ImageFormat.Jpeg, 90)
            File.WriteAllBytes(samllFileName, compressed)
        End If

        Return True
    End Function

    Public ReadOnly Property HasFile As Boolean
        Get
            If Not String.IsNullOrEmpty(hfImageUrl.Value) Then Return True
            Return fuImage.HasFile
        End Get
    End Property

    Protected Overrides Sub OnPreRender(e As EventArgs)
        If Not ImageExist Then imgImage.ImageUrl = String.Format("~/PlaceHolderHandler.axd?width={0}&height={1}", IIf(Me.Width.IsEmpty, 20, Me.Width.Value), IIf(Me.Height.IsEmpty, 20, Me.Height.Value))
        MyBase.OnPreRender(e)
    End Sub

    Public Sub RaisePostBackEvent(eventArgument As String) Implements IPostBackEventHandler.RaisePostBackEvent
        If eventArgument = "DeleteImage" Then RaiseEvent ImageDelete(Me, EventArgs.Empty)
    End Sub
End Class