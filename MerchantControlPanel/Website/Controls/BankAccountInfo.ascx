﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="BankAccountInfo.ascx.vb" Inherits="Netpay.MerchantControlPanel.BankAccountInfo" %>
<h3><asp:Literal ID="Literal25" runat="server" Text="<%$ Resources: BankAccountDetails %>" /></h3>
<span runat="server" visible="<%# Not String.IsNullOrEmpty(Item.AccountName) %>"></span><span class="bold"><asp:Literal ID="Literal26" runat="server" Text="<%$ Resources: AccountName %>" />:</span>&nbsp;<%# Item.AccountName %><br />
<span runat="server" visible="<%# Not String.IsNullOrEmpty(Item.AccountNumber) %>"></span><span class="bold"><asp:Literal ID="Literal27" runat="server" Text="<%$ Resources: AccountNumber %>" />:</span>&nbsp;<%# Item.AccountNumber %><br />
<span runat="server" visible="<%# Not String.IsNullOrEmpty(Item.BankName) %>"></span><span class="bold"><asp:Literal ID="Literal28" runat="server" Text="<%$ Resources: BankName %>" />:</span>&nbsp;<%# Item.BankName %><br />
<span runat="server" visible="<%# Not String.IsNullOrEmpty(Item.BankStreet1) %>"></span><span class="bold"><asp:Literal ID="Literal29" runat="server" Text="<%$ Resources: BankAddress %>" />:</span>&nbsp;<%# Item.BankStreet1 %><br />
<span runat="server" visible="<%# Not String.IsNullOrEmpty(Item.BankStreet2) %>"></span><span class="bold"> <%# Item.BankStreet2 %></span><br /><%# Item.BankCity %>, <%# Item.BankStateISOCode %>, <%# Item.BankPostalCode %><br />
<span runat="server" visible="<%# Not String.IsNullOrEmpty(Item.BankCountryISOCode) %>"></span><span class="bold"><%# Item.BankCountryISOCode %></span><br />
<span runat="server" visible="<%# Not String.IsNullOrEmpty(Item.SwiftNumber) %>"></span><span class="bold"><asp:Literal ID="Literal30" runat="server" Text="<%$ Resources: SwiftCode %>" /></span><%# Item.SwiftNumber %><br />
<span runat="server" visible="<%# Not String.IsNullOrEmpty(Item.IBAN) %>"></span><span class="bold"><asp:Literal ID="Literal31" runat="server" Text="<%$ Resources: IBAN %>" /></span><%# Item.IBAN %><br />
<span runat="server" visible="<%# Not String.IsNullOrEmpty(Item.ABA) %>"></span><span class="bold"><asp:Literal ID="Literal32" runat="server" Text="<%$ Resources: ABA %>" /></span><%# Item.ABA %><br />
