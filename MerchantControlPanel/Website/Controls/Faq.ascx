﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Faq.ascx.vb" Inherits="Netpay.MerchantControlPanel.Faq" %>

<script>
    function toggleAnswer_<%= ClientID%>(divId, iconId) {
        var answer = $("#" + divId);
        var icon = $("#" + iconId);
        if (answer.is(":visible"))
        {
            answer.hide();
            icon.removeClass('fa-minus-circle').addClass('fa-plus-circle');
        }  
        else
        {
            answer.show();
            icon.removeClass('fa-plus-circle').addClass('fa-minus-circle');
        }
    }
</script>

<asp:Panel ID="panelCompactMode" runat="server">
    <div class="wrap-faq-list"> 
    <ul>
        <asp:Repeater ID="repeaterCompactMode" runat="server">
            <ItemTemplate>
                <li class="cursor" onclick="javascript:showContent('divFaqContent')">
                    <i class="fa fa-minus-circle list-color"></i>
                    <%# Eval("Question").ToString().Truncate(45)%>
                </li>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
        </div>
</asp:Panel>

<asp:Panel ID="panelFullMode" runat="server">
    <ul class="faq-list">
        <asp:Repeater ID="repeaterFullMode" runat="server">
            <ItemTemplate>
                <li >
                    <i id='iAnswer_<%= ClientID%>_<%# Eval("ID")%>' class="fa fa-plus-circle secondary-color"></i>
                    <a href='javascript:toggleAnswer_<%= ClientID%>("divAnswer_<%= ClientID%>_<%# Eval("ID")%>", "iAnswer_<%= ClientID%>_<%# Eval("ID")%>")'><%# Eval("Question")%></a>
                </li>
                <div id='divAnswer_<%= ClientID%>_<%# Eval("ID")%>' style="display: none; background: #fff; font-size: 12px; padding: 10px; line-height: 25px;  border: 1px solid #E8E8E8;"><%# Eval("Answer")%></div>
            </ItemTemplate>
        </asp:Repeater>
    </ul>
     
</asp:Panel>








