﻿Imports Netpay.CommonTypes
Imports System.Data.SqlClient
Imports Netpay.Infrastructure
Imports Netpay.Web

''// Builds "heshbonit mas"
Partial Class Website_billingShow_pages
	Inherits PopupPage

	Protected nBillingCompanysID As Integer = HttpContext.Current.Request("BillingCompanysID").ToInt32(0)
	Protected nBillingNumber As Integer = HttpContext.Current.Request("billingNumber").ToInt32(0)
	Protected nInvoiceType As Integer = HttpContext.Current.Request("invoiceType").ToInt32(0)
	Protected sType As String = HttpContext.Current.Request("type").ToSql(True)
	Dim sWhere As String = ""
	Dim sSQL As String
	Dim rsData2 As SqlDataReader
	Dim nTransactionPay As String
	Dim sPayDate As String
	Dim sInvoiceTypeTextHeb As String
	Dim sInvoiceTypeTextEng As String
	Dim sBillTypeTextHeb As String
	Dim sBillTypeTextEng As String
	Dim sStyle As String
	Dim sInvoiceTypeText As String
	Dim sBillTypeText As String
	Dim sBillType As String
	Dim sAmount As String
	Dim pNumCopy As Integer
	Dim nPayID As String
	Dim nCurrency As String
	Dim nCompanyID As String
	Dim fExchangeRate As Integer

    Protected Function PagesPayment() As String
        Dim sWhere As String = "", sSQL As String = ""
        Dim rsDataPaydate As SqlDataReader
        Dim rsData As SqlDataReader
        Dim nBillingCompanys_id As String
        Dim sIsChargeVAT As Boolean
        Dim sVATamount As String
        Dim sTextDisplay_1 As String
        Dim sTextDisplay_2 As String
        Dim sTextDisplay_5 As String
        Dim sTextDisplay_6 As String
        Dim sTextDisplay_7 As String
        Dim sHeadTextDisplay_1 As String
        Dim sHeadTextDisplay_2 As String
        Dim sHeadTextDisplay_3 As String
        Dim sHeadTextDisplay_4 As String
        Dim sFeeTextDisplay_1 As String
        Dim sFeeTextDisplay_2 As String
        Dim sFeeTextDisplay_3 As String
        Dim sFeeTextDisplay_4 As String
        Dim sFeeTextDisplay_5 As String
        Dim sFeeTextDisplay_6 As String
        Dim sFeeTextDisplay_7 As String
        Dim sFeeTextDisplay_8 As String
        Dim sFeeTextDisplay_9 As String
        Dim sStyleTable As String
        Dim sStyleAlign As String
        Dim sStyleAlignReverse As String
        Dim sbHTML As New StringBuilder(String.Empty)
        'Not admin, need to check if company owns this invoice
        If Bll.Merchants.Merchant.Current.ID > 0 Then
            sWhere = "id_MerchantID=" & Merchant.ID & " AND "
        End If
        sSQL = "SELECT * FROM tblInvoiceDocument WHERE " & sWhere & _
        " id_BillingCompanyID=" & nBillingCompanysID & " AND id_InvoiceNumber=" & nBillingNumber & " AND id_Type=" & nInvoiceType
        rsDataPaydate = dbPages.ExecReader(sSQL)
        rsDataPaydate.Read()
        nPayID = dbPages.TestVar(rsDataPaydate("id_TransactionPayID"), 0, -1, 0)
        nCurrency = dbPages.TestVar(rsDataPaydate("id_Currency"), 0, -1, 0)
        nCompanyID = dbPages.TestVar(rsDataPaydate("id_MerchantID"), 0, -1, 0)
        nBillingCompanys_id = dbPages.TestVar(rsDataPaydate("id_BillingCompanyID"), 0, -1, 0)
        nBillingNumber = Trim(rsDataPaydate("id_InvoiceNumber"))

        If rsDataPaydate("id_Type") = "0" Then
            sInvoiceTypeTextHeb = "חשבונית מס/קבלה"
            sInvoiceTypeTextEng = "Tax Invoice / Receipt"
        ElseIf rsDataPaydate("id_Type") = "1" Then
            sInvoiceTypeTextHeb = "חשבונית מס"
            sInvoiceTypeTextEng = "Tax Invoice"
        ElseIf rsDataPaydate("id_Type") = "2" Then
            sInvoiceTypeTextHeb = "קבלה"
            sInvoiceTypeTextEng = "Receipt"
        ElseIf Int(rsData2("id_Type")) = "3" Then
            sInvoiceTypeTextHeb = "חשבונית זיכוי"
            sInvoiceTypeTextEng = "Refund Invoice"
        End If

        If Trim(Request("PrintType")) = "billCopy" Then
            sBillTypeTextHeb = " - העתק"
            sBillTypeTextEng = " "
        ElseIf Trim(Request("PrintType")) = "billOriginal" Then
            If rsDataPaydate("isBillingPrintOriginal") Then
                sBillTypeTextHeb = " - העתק"
                sBillTypeTextEng = " "
            Else
                sBillTypeTextHeb = " - מקור"
                sBillTypeTextEng = " "
            End If
        End If

        sIsChargeVAT = rsDataPaydate("id_ApplyVAT")
        sVATamount = rsDataPaydate("id_VATPercent")
        If Not sIsChargeVAT Then sVATamount = 0
        'nPrimePercent = rsDataPaydate("PrimePercent")

        fExchangeRate = 1
        'If rsDataPaydate("Currency") <> rsDataPaydate("BillingCurrencyShow") Then
        '	If rsDataPaydate("exchangeRate") > 0 Then fExchangeRate = rsDataPaydate("exchangeRate")
        'End if

        'All sep 2006 invoices should not be calculated because of a mistake
        'If int(nPayID)<= 4557 AND int(nPayID)>= 4439 Then fExchangeRate = 1

        If Trim(LCase(rsDataPaydate("id_BillingCompanyLanguage"))) = "eng" Then
            sInvoiceTypeText = sInvoiceTypeTextEng
            sBillTypeText = sBillTypeTextEng
            sTextDisplay_1 = "To"
            sTextDisplay_2 = "Date"
            sHeadTextDisplay_1 = "Description"
            sHeadTextDisplay_2 = "Amount"
            sHeadTextDisplay_3 = "Qty"
            sHeadTextDisplay_4 = "Total"
            sFeeTextDisplay_1 = "Transaction fee"
            sFeeTextDisplay_2 = "Refund transaction fee"
            sFeeTextDisplay_3 = "Processing fee"
            sFeeTextDisplay_4 = "Financing fees"
            sFeeTextDisplay_5 = "Transaction clarification fee - credit cards"
            sFeeTextDisplay_6 = "Transaction clarification fee - eCheck"
            sFeeTextDisplay_7 = "Transaction Chargeback fee - credit cards"
            sFeeTextDisplay_8 = "Transaction Chargeback fee - eCheck"
            sFeeTextDisplay_9 = "System transaction"
            sTextDisplay_5 = "Total Amount"
            sTextDisplay_6 = "VAT"
            sTextDisplay_7 = "Total Amount Inc VAT"
            sStyleTable = "direction:ltr;"
            sStyleAlign = "text-align:left;"
            sStyleAlignReverse = "text-align:right;"
        Else
            sInvoiceTypeText = sInvoiceTypeTextHeb
            sBillTypeText = sBillTypeTextHeb
            sTextDisplay_1 = "לכבוד"
            sTextDisplay_2 = "תאריך"
            sHeadTextDisplay_1 = "פירוט"
            sHeadTextDisplay_2 = "סכום"
            sHeadTextDisplay_3 = "כמות"
            sHeadTextDisplay_4 = "סה&quot;כ"
            sFeeTextDisplay_1 = "עמלת עסקת חיוב"
            sFeeTextDisplay_2 = "עמלת עסקת זיכוי"
            sFeeTextDisplay_3 = "עמלת סליקה"
            sFeeTextDisplay_4 = "עמלת מימון אשראי"
            sFeeTextDisplay_5 = "עמלת בירור עסקה - כרטיס אשראי"
            sFeeTextDisplay_6 = "עמלת בירור עסקה - צ'ק אלקטרוני"
            sFeeTextDisplay_7 = "עמלת קיזוז עסקה מוכחשת - כרטיס אשראי"
            sFeeTextDisplay_8 = "עמלת קיזוז עסקה מוכחשת - צ'ק אלקטרוני"
            sFeeTextDisplay_9 = "עמלת עסקת מערכת"
            sTextDisplay_5 = "סה&quot;כ סכום"
            sTextDisplay_6 = "סה&quot;כ מע&quot;מ"
            sTextDisplay_7 = "סה&quot;כ סכום כולל מע&quot;מ"
            sStyleTable = "direction:rtl;"
            sStyleAlign = "text-align:right;"
            sStyleAlignReverse = "text-align:left;"
        End If
        sbHTML.Append("<br><table align=""center"" border=""0"" cellpadding=""1"" cellspacing=""2"" width=""620"" style=""" & sStyleTable & """>")
        sbHTML.Append("<tr><td valign=""bottom"" nowrap style=""" & sStyleAlign & """>")
        sbHTML.Append("<span class=""txt14b"">" & dbPages.dbtextShow(rsDataPaydate("id_BillingCompanyName")) & "</span><br>")
        sbHTML.Append("<span class=""txt12"">" & dbPages.dbtextShow(rsDataPaydate("id_BillingCompanyAddress")) & "</span><br></td>")
        sbHTML.Append("<td valign=""bottom"" nowrap style=""" & sStyleAlignReverse & """>")
        sbHTML.Append("<span class=""txt14b"">" & dbPages.dbtextShow(rsDataPaydate("id_BillingCompanyNumber")) & "</span><br>")
        sbHTML.Append("<span class=""txt12"">" & dbPages.dbtextShow(rsDataPaydate("id_BillingCompanyEmail")) & "</span><br></td></tr>")
        sbHTML.Append("<tr><td colspan=""2""><hr width=""100%"" size=""1"" style=""border:1px dashed black"" noshade></td></tr>")
        sbHTML.Append("<tr><td colspan=""2"" height=""10""></td></tr>")
        sbHTML.Append("<tr><td colspan=""2"" align=""center"" nowrap class=""txt15b"" dir=""rtl"">")
        sbHTML.Append(sInvoiceTypeText & " " & nBillingNumber & sBillTypeText & "<br>" & sBillType)
        sbHTML.Append("</td></tr><tr><td colspan=""2"" height=""16""></td></tr>")
        sbHTML.Append("<tr><td colspan=""2"" align=""right"">")
        sbHTML.Append("<table align=""right"" width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"" style=""" & sStyleTable & """>")
        sbHTML.Append("<tr><td valign=""top"" class=""txt14"" style=""" & sStyleAlign & """>")
        sbHTML.Append("<span class=""txt14b"">" & sTextDisplay_1 & "</span><br>")
        sbHTML.Append(dbPages.dbtextShow(rsDataPaydate("id_BillToName")) & "</td>")
        sbHTML.Append("<td width=""25""></td>")
        sbHTML.Append("<td valign=""top"" class=""txt14"" style=""" & sStyleAlignReverse & """>")
        sbHTML.Append("<span class=""txt14b"">" & sTextDisplay_2 & "</span><br>")
        sbHTML.Append("<span class=""txt12"">" & dbPages.FormatDatesTimes(rsDataPaydate("id_InsertDate"), 1, 0, 0) & "</span><br></td></tr></table>")
        sbHTML.Append("</td></tr><tr><td colspan=""2""><br></td></tr></table><br/>")
        sbHTML.Append("<table align=""center"" border=""0"" cellpadding=""1"" cellspacing=""2"" width=""620"" style=""" & sStyleTable & """>")
        sbHTML.Append("<tr><td><table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""1"" style=""" & sStyleTable & """>")
        sbHTML.Append("<tr><td height=""1"" colspan=""4"" style=""padding:0px; border-top:1px solid black""><img src=""../images/img/1_space.gif"" alt="""" width=""1"" height=""1"" border=""0""><br></td></tr>")
        sbHTML.Append("<tr><td class=""txt14b"" style=""" & sStyleAlign & """>" & sHeadTextDisplay_1 & "<br></td>")
        sbHTML.Append("<td class=""txt14b"" align=""right"">" & sHeadTextDisplay_2 & "<br></td>")
        sbHTML.Append("<td class=""txt14b"" align=""right"">" & sHeadTextDisplay_3 & "<br></td>")
        sbHTML.Append("<td class=""txt14b"" align=""right"">" & sHeadTextDisplay_4 & "<br></td></tr>")
        sbHTML.Append("<tr><td height=""1"" colspan=""4"" style=""padding:0px; border-top:1px solid black""><img src=""../images/img/1_space.gif"" alt="""" width=""1"" height=""1"" border=""0""><br></td></tr>")
        sbHTML.Append("<tr><td height=""3""></td></tr>")


        sSQL = "SELECT * FROM tblInvoiceLine WHERE il_DocumentID=" & rsDataPaydate("ID")
        rsData = dbPages.ExecReader(sSQL)
        Do While rsData.Read()
            If nTransactionPay > 0 Then
                sAmount = "---"
            Else
                sAmount = rsData("il_Price") * rsData("il_CurrencyRate")
            End If
            sbHTML.Append(drawInvoiceLine(rsData("il_Text"), sAmount, rsData("il_Quantity"), rsData("il_Amount") * rsData("il_CurrencyRate")))
        Loop
        rsData.Close()
        sbHTML.Append("<tr><td height=""1"" colspan=""4"" style=""border-bottom:1px solid black""><img src=""../images/img/1_space.gif"" alt="""" width=""1"" height=""1"" border=""0""><br></td></tr>")
        sbHTML.Append("<tr bgcolor=""white"">")
        sbHTML.Append("<td colspan=""3"" style=""" & sStyleAlignReverse & """>")
        sbHTML.Append("<table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""170"">")
        sbHTML.Append("<tr><td class=""txt13b"" style=""" & sStyleAlign & """>" & sTextDisplay_5 & "<br></td></tr></table></td>")
        sbHTML.Append("<td class=""txt13"" dir=""ltr"" align=""right"">")
        sbHTML.Append(dbPages.FormatCurr(nCurrency, rsDataPaydate("id_TotalLines")) & "<br></td></tr>")
        sbHTML.Append("<tr bgcolor=""white""><td colspan=""3"" style=""" & sStyleAlignReverse & """>")
        sbHTML.Append("<table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""170"">")
        sbHTML.Append("<tr><td class=""txt13b"" style=""" & sStyleAlign & """>" & sTextDisplay_6 & " (" & sVATamount * 100 & "%)<br></td></tr></table></td>")
        sbHTML.Append("<td class=""txt13"" dir=""ltr"" align=""right"">")
        sbHTML.Append(dbPages.FormatCurr(nCurrency, rsDataPaydate("id_TotalVAT")) & "<br></td></tr>")
        sbHTML.Append("<tr bgcolor=""white""><td colspan=""3"" style=""" & sStyleAlignReverse & """>")
        sbHTML.Append("<table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""170"">")
        sbHTML.Append("<tr><td class=""txt13b"" style=""" & sStyleAlign & """>" & sTextDisplay_7 & "<br></td></tr></table></td>")
        sbHTML.Append("<td class=""txt13"" dir=""ltr"" align=""right"">" & dbPages.FormatCurr(nCurrency, rsDataPaydate("id_TotalDocument")) & "<br></td></tr></table>")
        sbHTML.Append("</td></tr></table><br/>")
        rsDataPaydate.Close()
        Return sbHTML.ToString()
    End Function

	Protected Function TransList() As String
		Dim sSQL As String
		Dim rsData2 As SqlDataReader
		Dim rsData As SqlDataReader
		Dim sHtml As New StringBuilder(String.Empty)
		If Trim(nCompanyID) <> "" Then
			sSQL = "SELECT CompanyName FROM tblCompany WHERE ID=" & nCompanyID
			rsData2 = dbPages.ExecReader(sSQL)
			If rsData2.Read() Then
				sHtml.Append("<br/><table width=""620"" style=""border:1px solid black;"" border=""0"" dir=""rtl"" cellspacing=""2"" cellpadding=""1"" align=""center"">")
				sHtml.Append("<tr><td rowspan=""2"" align=""right"" valign=""top"" class=""txt15b"">" & dbPages.dbtextShow(rsData2("CompanyName")) & "<br/></td>")
				sHtml.Append("<td nowrap align=""left""><span class=""txt13b"">חיובים ששולמו בתאריך</span> <span class=""txt11b"">" & dbPages.FormatDatesTimes(Replace(sPayDate, "_", " "), 1, 0, 0) & "</span><br></td></tr></table><br/>")
			End If
			rsData2.Close()
            sSQL = "SELECT List.TransCreditType.Name AS CreditTypeName, tblCompanyTransPass.* FROM tblCompanyTransPass " & _
             "LEFT OUTER JOIN List.TransCreditType ON tblCompanyTransPass.CreditType = List.TransCreditType.TransCreditType_id " & _
             "WHERE (tblCompanyTransPass.PayID LIKE ('%;" & Trim(nPayID) & ";%')) AND tblCompanyTransPass.CompanyID=" & nCompanyID & " " & _
             "ORDER BY tblCompanyTransPass.InsertDate DESC"
			rsData = dbPages.ExecReader(sSQL)
			If rsData.HasRows Then
				sHtml.Append("<table width=""620"" align=""center"" border=""0"" cellpadding=""1"" cellspacing=""0"" dir=""rtl"">")
				sHtml.Append("<tr><td class=""txt12b"" bgcolor=""#e9e9e9"" style=""border-bottom:1px solid gray;"" valign=""bottom"">תאריך<br></td>")
				sHtml.Append("<td class=""txt12b"" bgcolor=""#e9e9e9"" style=""border-bottom:1px solid gray;"" valign=""bottom"">אמצעי תשלום<br></td>")
				sHtml.Append("<td class=""txt12b"" bgcolor=""#e9e9e9"" style=""border-bottom:1px solid gray;"" valign=""bottom"">סכום<br></td>")
				sHtml.Append("<td class=""txt12b"" bgcolor=""#e9e9e9"" style=""border-bottom:1px solid gray;"" valign=""bottom"">סוג אשראי<br></td>")
				sHtml.Append("<td class=""txt12b"" bgcolor=""#e9e9e9"" style=""border-bottom:1px solid gray;"" valign=""bottom"">תש'<br></td>")
				sHtml.Append("<td class=""txt12b"" bgcolor=""#e9e9e9"" style=""border-bottom:1px solid gray;"" valign=""bottom"">עמלת<br>עסקה<br></td>")
				sHtml.Append("<td class=""txt12b"" bgcolor=""#e9e9e9"" style=""border-bottom:1px solid gray;"" valign=""bottom"">עמלת<br>סליקה<br></td></tr><tr><td height=""3""></td></tr>")

				Dim nCount As Integer = 0
				Dim sShowFeeTrans As Boolean
				Dim sShowDeniedMessege As Boolean
				Dim rsDataPayments As SqlDataReader
				Do While rsData.Read()
					nCount = nCount + 1
					sShowFeeTrans = False
					sShowDeniedMessege = False

					If rsData("PaymentMethod_id") = 1 Or rsData("PaymentMethod_id") = 2 Then
						'Credit cards and eChecks
						If Int(rsData("CreditType")) = 8 Then
							sSQL = "SELECT * FROM tblCompanyTransInstallments WHERE PayID='" & nPayID & "' AND transAnsID=" & rsData("id") & " ORDER BY id"
							rsDataPayments = dbPages.ExecReader(sSQL)
							Dim sIsFirstPayment As Boolean = False
							If rsDataPayments.Read Then
								If Left(rsDataPayments("comment"), 2) = "1/" Then sIsFirstPayment = True
							End If
							rsDataPayments.Close()
							If rsData("deniedstatus") = 3 Then
								If sIsFirstPayment Then
									sShowDeniedMessege = True
								End If
							ElseIf rsData("deniedstatus") = 4 Then
								sShowDeniedMessege = True
							End If
						Else  ' אם חיוב לא בתשלומים
							If rsData("deniedstatus") = 3 Then
								sShowDeniedMessege = True
							ElseIf rsData("deniedstatus") = 4 Then
								sShowDeniedMessege = True
							ElseIf rsData("deniedstatus") = 5 Then
								sShowDeniedMessege = True
							ElseIf rsData("deniedstatus") = 6 Then
								sShowDeniedMessege = True
							End If
						End If
					ElseIf Int(rsData("PaymentMethod_id")) = 5 Then
						'Fees transactions
						If Int(rsData("TransSource_id")) = 1 Or Int(rsData("TransSource_id")) = 2 Then
							sShowFeeTrans = True
						End If
					End If

					If nCount <> 1 Then
						sHtml.Append("<tr><td height=""1"" colspan=""7""  bgcolor=""#ffffff""></td></tr>")
						sHtml.Append("<tr><td height=""1"" colspan=""7""  bgcolor=""silver""></td></tr>")
						sHtml.Append("<tr><td height=""1"" colspan=""7""  bgcolor=""#ffffff""></td></tr>")

					End If
					If Not sShowFeeTrans Then
						sHtml.Append("<tr><td class=""txt11"" align=""right"" nowrap>" & dbPages.FormatDatesTimes(rsData("InsertDate"), 1, 0, 0) & "<br></td>")
						sHtml.Append("<td class=""txt10"" align=""right"">" & rsData("PaymentMethodDisplay") & "<br></td>")
						sHtml.Append("<td class=""txt11"" align=""right"" nowrap dir=""ltr"">" & dbPages.FormatCurrWCT(rsData("Currency"), rsData("Amount"), rsData("CreditType")) & "<br></td>")
                        sHtml.Append("<td class=""txt11"" dir=""rtl"" align=""right"">" & rsData("CreditTypeName") & "<br></td>")
						sHtml.Append("<td class=""txt11"" align=""right"">" & rsData("Payments") & "<br></td>")
						sHtml.Append("<td class=""txt11"" align=""right"" nowrap dir=""ltr"">" & dbPages.FormatCurr(rsData("Currency"), rsData("netpayFee_transactionCharge")) & "<br></td>")
						sHtml.Append("<td class=""txt11"" align=""right"" nowrap dir=""ltr"">" & dbPages.FormatCurr(rsData("Currency"), rsData("NetpayFee_RatioCharge")) & "<br></td></tr>")

					ElseIf sShowFeeTrans Then
						Dim sDate As String = ""
						sHtml.Append("<tr><td class=""txt11"" align=""right"" nowrap>" & sDate & "<br></td>")
						sHtml.Append("<td class=""txt11"" align=""right"" colspan=""4"">" & dbPages.dbtextShow(rsData("Comment")) & "<br></td>")
						sHtml.Append("<td class=""txt11"" align=""right"" nowrap dir=""ltr"">" & dbPages.FormatCurr(rsData("Currency"), -rsData("Amount")) & "<br></td></tr>")
					End If
					If rsData("PaymentMethod_id") = "4" Then
						sHtml.Append("<tr><td colspan=""7"">")
						sHtml.Append("<table border=""0"" align=""right"" width=""100%"" cellspacing=""0"" cellpadding=""0""><tr>")
						sHtml.Append("<td bgcolor=""#ffffff"" width=""40""></td>")
						sHtml.Append("<td align=""right"" class=""txt11"" dir=""rtl"" style=""padding-right:6px;"">" & dbPages.dbtextShow("הערה: " & rsData("Comment")) & "</td>")
						sHtml.Append("</tr></table></td></tr>")
					End If
					If sShowDeniedMessege Then ' חיוב שהוא מוכחש
						sHtml.Append("<tr><td colspan=""7""><table border=""0"" align=""right"" width=""100%"" cellspacing=""0"" cellpadding=""0""><tr>")
						sHtml.Append("<td width=""6""></td><td align=""right"" class=""txt11"" dir=""rtl"" style=""padding-right:6px;"">")
						Select Case CType(rsData("deniedstatus"), Integer)
							Case 3
								sHtml.Append("חיוב זה היה בבירור הכחשה ונמצא תקין, מתווסף עמלת בירור")
							Case 4
								sHtml.Append("חיוב זה היה בבירור הכחשה ונמצא לא תקין, סכום חיוב לא יעובר, מתווסף עמלת בירור והכחשה")
							Case 5
								sHtml.Append("חיוב זה כבר שולם בעבר והיה בבירור הכחשה ונמצא תקין, סכום חיוב לא יעובר, מתווסף עמלת בירור")
							Case 6
								sHtml.Append("חיוב זה כבר שולם בעבר והיה בבירור הכחשה ונמצא לא תקין, סכום חיוב יכוזז, מתווסף עמלת בירור והכחשה")
						End Select
						sHtml.Append("</td></tr></table></td></tr>")

					End If
					If rsData("CreditType") = "8" And rsData("deniedstatus") <> 4 Then ' חיוב שהוא בתשלומים וגם לא מוכחש שקוזז
						sSQL = "SELECT * FROM tblCompanyTransInstallments WHERE PayID='" & nPayID & "' AND transAnsID=" & rsData("id") & " ORDER BY id"
						rsDataPayments = dbPages.ExecReader(sSQL)
						Do While rsDataPayments.Read
							sHtml.Append("<tr><td colspan=""7""><table border=""0"" align=""right"" cellspacing=""0"" cellpadding=""0""><tr>")
							sHtml.Append("<td bgcolor=""#ffffff"" width=""40""></td>")
							sHtml.Append("<td dir=""rtl"" align=""right"" class=""txt12"" style=""padding-right:10px;"">תשלום <span class=""txt11"">" & rsDataPayments("comment") & "</span></td>")
							sHtml.Append("<td width=""20""></td>")
							sHtml.Append("<td dir=""rtl"" align=""right"" class=""txt12"">סכום <span class=""txt11"" dir=""ltr"">" & dbPages.FormatCurr(rsData("Currency"), rsDataPayments("Amount")) & "</span></td></tr></table></td></tr>")
						Loop
						rsDataPayments.Close()
					End If
				Loop
				sHtml.Append("<tr><td height=""1"" colspan=""7""  bgcolor=""#ffffff""></td></tr>")
				sHtml.Append("<tr><td height=""1"" colspan=""7""  bgcolor=""silver""></td></tr>")
				sHtml.Append("<tr><td height=""1"" colspan=""7""  bgcolor=""#ffffff""></td></tr></table><br/>")
                'sHtml.Append("<iframe marginheight=""0"" marginwidth=""0"" align=""center"" frameborder=""0"" width=""100%"" height=""250"" scrolling=""No"" src=""totalTransShow.aspx?LNG=0&CompanyID=" & nCompanyID & "&PayID=" &  nPayID & "&isIframe=1""></iframe><br>")				
                sHtml.Append("<div style=""text-align:center;height:250px;"">")
                Dim totals As New Netpay.Web.Controls.Totals()
                totals.CompanyID = nCompanyID
                totals.PayID = nPayID
                totals.HideTitle = True
                totals.RenderControl(New System.Web.UI.Html32TextWriter(New System.IO.StringWriter(sHtml)))
                sHtml.Append("</div>")
			End If
			TransList = sHtml.ToString()
			rsData.Close()
		Else
			TransList = ""
		End If
	End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Not admin need to check company owns this invoice
        If Bll.Merchants.Merchant.Current.ID > 0 Then sWhere = "id_MerchantID=" & Merchant.ID & " AND "

        sSQL = "SELECT id_MerchantID, IsNull(payDate, id_InsertDate) payDate, payType, id_Currency," & _
        " tblTransactionPay.id AS transPayId, id_InsertDate, id_IsPrinted, id_Type FROM tblInvoiceDocument" & _
        " LEFT JOIN tblTransactionPay ON tblInvoiceDocument.ID=tblTransactionPay.InvoiceDocumentID" & _
        " WHERE " & sWhere & " id_BillingCompanyID=" & nBillingCompanysID & " AND id_InvoiceNumber=" & nBillingNumber & " AND id_Type=" & nInvoiceType
        rsData2 = dbPages.ExecReader(sSQL)
        If Not rsData2.HasRows Then
            Response.Write("לא נמצאו רשומות<br>")
            rsData2.Close()
            Response.End()
        End If
        rsData2.Read()
        nTransactionPay = rsData2("transPayId")
        nCompanyID = Trim(rsData2("id_MerchantID"))
        nPayID = Trim(rsData2("transPayId"))
        sPayDate = Trim(rsData2("payDate"))
        nCurrency = Trim(rsData2("id_Currency"))
        If Int(rsData2("id_Type")) = 0 Then
            sInvoiceTypeTextHeb = "חשבונית מס/קבלה"
            sInvoiceTypeTextEng = "Tax Invoice / Receipt"
        ElseIf Int(rsData2("id_Type")) = 1 Then
            sInvoiceTypeTextHeb = "חשבונית מס"
            sInvoiceTypeTextEng = "Tax Invoice"
        ElseIf Int(rsData2("id_Type")) = 2 Then
            sInvoiceTypeTextHeb = "קבלה"
            sInvoiceTypeTextEng = "Receipt"
        ElseIf Int(rsData2("id_Type")) = "3" Then
            sInvoiceTypeTextHeb = "חשבונית זיכוי"
            sInvoiceTypeTextEng = "Refund Invoice"
        End If

        If Not rsData2("id_IsPrinted") And Request("billOriginal") <> "" Then
            pnlOriginalNotPrinted.Enabled = True
            sBillTypeTextHeb = " - מקור"
            sBillTypeTextEng = " "
            If sType = "preview" Then
                litPreviewOriginalCopiesNumber.Text = "<table align=""center"" border=""0"" dir=""rtl"" cellpadding=""2"" cellspacing=""0"" width=""650""><tr>" & _
                   "<td align=""right"" dir=""rtl"" class=""txt14"" style=""color:#00254a;"">" & _
                   "<li type=""square"">" & sInvoiceTypeTextHeb & sBillTypeTextHeb & "&nbsp;&nbsp;&nbsp;&nbsp; מספר עותקים להדפסה: <span dir=""ltr"" class=""txt13"" style=""color:#00254a;"">1</span></li><br></td></tr></table>"
                litPreviewOriginalCopiesNumber.Visible = True
                litPreviewOriginalPagesPayment.Text = "<table align=""center"" border=""0"" style=""background-color:white; border:1px solid black; filter:progid:DXImageTransform.Microsoft.Shadow(color='gray', Direction=135, Strength=5)"" dir=""rtl"" cellpadding=""1"" cellspacing=""2"" width=""650"">" & _
                   "<tr><td style=""padding-top:10px;""><div id=""divbillOriginal"">" & PagesPayment() & "</div></td></tr></table><br/><br/>"
                litPreviewOriginalPagesPayment.Visible = True
            Else
                pNumCopy = 1
                'Check to see if need to page break
                If Trim(Request("billCopy")) <> "" Or Trim(Request("chargeList")) <> "" Then
                    sStyle = "page-break-after:always;"
                Else
                    sStyle = "page-break-after:auto;"
                End If
                litPreviewOriginalInnerHtmlScript.Text = "<div id=""divbillOriginal1"" style=""" & sStyle & """><script language=""JavaScript"">" & _
                   "divbillOriginal1.innerHTML=top.fraBillprintPreview.document.getElementById('divbillOriginal').innerHTML;</s" & "cript></div>"
                litPreviewOriginalInnerHtmlScript.Visible = True
            End If
        End If

        If Trim(Request("billCopy")) <> "" Then
            pnlBillCopy.Enabled = True
            sBillTypeTextHeb = " - העתק"
            sBillTypeTextEng = " "
            Dim nLast As Integer = 1 : Integer.TryParse(Request("billCopyNum"), nLast)
            If nLast < 1 Then nLast = 1
            If sType = "preview" Then
                litBillCopyPrintNumber.Text = "<table align=""center"" border=""0"" dir=""rtl"" cellpadding=""2"" cellspacing=""0"" width=""650""><tr>" & _
                  "<td align=""right"" dir=""rtl"" class=""txt14"" style=""color:#00254a;"">" & _
                  "<li type=""square"">" & sInvoiceTypeTextHeb & sBillTypeTextHeb & "&nbsp;&nbsp;&nbsp;&nbsp; מספר עותקים להדפסה: <span dir=""ltr"" class=""txt13"" style=""color:#00254a;"">" & nLast & "</span></li><br>" & _
                  "</td></tr><tr><td height=""3""></td></tr></table>"
                litBillCopyPrintNumber.Visible = True
                litBillCopyPagesPayment.Text = "<table align=""center"" border=""0"" dir=""rtl"" cellpadding=""2"" cellspacing=""0"" width=""650"">" & _
                  "<tr><td style=""padding-top:10px;""><div id=""divBillCopy"">" & PagesPayment() & "</div></td></tr></table><br/><br/>"
                litBillCopyPagesPayment.Visible = True
            Else
                Dim sCopyHTML As New StringBuilder(String.Empty)
                For pNumCopy As Integer = 1 To nLast
                    'Check to see if need to page break
                    If Trim(Request("chargeList")) <> "" Or Int(pNumCopy) <> nLast Then
                        sStyle = "page-break-after:always;"
                    Else
                        sStyle = "page-break-after:auto;"
                    End If
                    sCopyHTML.Append("<div id=""divBillCopy" & pNumCopy & """ style=""" & sStyle & """>")
                    sCopyHTML.Append("<script language=""JavaScript"">")
                    sCopyHTML.Append("divBillCopy" & pNumCopy & ".innerHTML=top.fraBillprintPreview.document.getElementById('divBillCopy').innerHTML;")
                    sCopyHTML.Append("</s" & "cript></div>")
                Next
                litBillCopyInnerHtmlScript.Text = sCopyHTML.ToString()
                litBillCopyInnerHtmlScript.Visible = True
            End If
        End If

        If Trim(HttpContext.Current.Request("chargeList")) <> "" Then
            pnlChargeList.Enabled = True
            If sType = "preview" Then
                litChargeListPrintNumber.Text = "<table align=""center"" border=""0"" dir=""rtl"" cellpadding=""2"" cellspacing=""0"" width=""650"">" & _
                 "<tr><td align=""right"" dir=""rtl"" class=""txt14"" style=""color:#00254a;"">" & _
                 "<li type=""square"">רשימת עסקאות &nbsp;&nbsp;&nbsp;&nbsp; מספר עותקים להדפסה: <span dir=""ltr"" class=""txt13"" style=""color:#00254a;"">" & HttpContext.Current.Request("chargeListNum") & "</span></li><br>" & _
                 "</td></tr></table>"
                litChargeListPrintNumber.Visible = True
                litChargeListTransList.Text = "<table align=""center"" border=""0"" style=""background-color:white; border:1px solid black; filter:progid:DXImageTransform.Microsoft.Shadow(color='gray', Direction=135, Strength=5)"" dir=""rtl"" cellpadding=""1"" cellspacing=""2"" width=""650"">" & _
                 "<tr><td style=""padding-top:10px;""><div id=""divChargeList"">" & TransList() & "</div></td></tr></table><br/><br/>"
                litChargeListTransList.Visible = True
            Else
                Dim sCopyHTML As New StringBuilder(String.Empty)
                For pNumCopy As Integer = 1 To HttpContext.Current.Request("chargeListNum").ToInt32(1)
                    'Check to see if need to page break
                    If pNumCopy <> HttpContext.Current.Request("chargeListNum").ToInt32(1) Then
                        sStyle = "page-break-after:always;"
                    Else
                        sStyle = "page-break-after:auto;"
                    End If
                    sCopyHTML.Append("<div id=""divChargeList" & pNumCopy & """ style=""" & sStyle & """>")
                    sCopyHTML.Append("<script language=""JavaScript"">")
                    sCopyHTML.Append("divChargeList" & pNumCopy & ".innerHTML=top.fraBillprintPreview.document.getElementById('divChargeList').innerHTML;")
                    sCopyHTML.Append("</s" & "cript></div>")
                Next
                litChargeListInnerHtmlScript.Text = sCopyHTML.ToString()
                litChargeListInnerHtmlScript.Visible = True
            End If
        End If
        If Not rsData2.IsClosed Then rsData2.Close()
    End Sub

	Function drawInvoiceLine(sText As String, sUnitAmount As String, sQuantity As Integer, sTotal As String) As String
		If sQuantity = 0 Then
			drawInvoiceLine = ""
			Exit Function
		End If
		drawInvoiceLine = "<tr bgcolor=""white"">" & _
		 "<td class=""txt14"">" & sText & "<br></td>" & _
		 "<td class=""txt13"" dir=""ltr"" align=""right"" width=""18%"">"
		If IsNumeric(sUnitAmount) Then drawInvoiceLine = drawInvoiceLine & dbPages.FormatCurr(nCurrency, sUnitAmount * fExchangeRate) _
		Else drawInvoiceLine = drawInvoiceLine & sUnitAmount
		drawInvoiceLine = drawInvoiceLine & "<br></td>" & _
		 "<td class=""txt13"" dir=""ltr"" align=""right"" width=""12%"">" & sQuantity & "</td>" & _
		 "<td class=""txt13"" dir=""ltr"" align=""right"" width=""18%"">" & dbPages.FormatCurr(nCurrency, sTotal * fExchangeRate) & "<br></td></tr>"
	End Function

End Class
