﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" CodeBehind="DevSecurity_Settings.aspx.vb" Inherits="Netpay.MerchantControlPanel.Security_Settings" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:menu, Security %>" runat="server" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Literal Text="<%$Resources: Secure_Settings %>" runat="server" />
        </div>
        <div class="content-background">
            <div class="section">
                <div class="margin-bottom-10 frame-border-option">
                    <div class="margin-bottom-10">
                        <i class="fa fa-key dark-grey"></i>
                        <asp:Literal Text="<%$Resources: Personal_Hash_Key %>" runat="server" />
                    </div>
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">
                        <div class="alert alert-info margin-bottom-10">
                            <asp:Literal Text="<%$Resources: IntroHashKey %>" runat="server" />
                        </div>
                        <span class="hashkey align-left">
                            <i class="fa fa-key dark-grey"></i>
                            <asp:Label runat="server" ID="lblHashKey" />
                            <script type="text/javascript">
                                function ConfirmHashKeyAction() 
                                {
                                    var sText = "<asp:Literal runat="server" Text="<%$Resources: Personal_Hash_Key_Edit %>" />";        
                                    if (confirm(sText))
                                        <%= Page.ClientScript.GetPostBackEventReference(btnHashKeyChange, "") %>;
                                }
                            </script>
                        </span>
                        <div class="margin-top-10 align-right">
                            <asp:Button ID="btnHashKeyChange" OnClick="NewHashKey" Text="<%$Resources: Change_Personal_Hash_Key %>" UseSubmitBehavior="false" OnClientClick="return ConfirmHashKeyAction();" CssClass="btn btn-info btn-cons-short" runat="server" />
                            <asp:Button ID="btnHashKeyCreate" OnClick="NewHashKey" Text="<%$Resources: Create_Personal_Hash_Key %>" UseSubmitBehavior="false" CssClass="btn btn-success btn-cons-short" runat="server" />
                        </div>
                        <div class="spacer"></div>
                    </div>
                      <div class="margin-bottom-10">
                        <i class="fa fa-lock"></i>
                        <asp:Literal Text="<%$Resources: Options %>" runat="server" />
                    </div>
                    <ul class="group-list">
                        <li>
                            <asp:CheckBox ID="chkForceCCStorageMD5" runat="server" />
                            <asp:Literal Text="<%$Resources: MD5_Signature_CC %>" runat="server" />
                        </li>
                        <li>
                            <asp:Panel ID="pnlRecurring" runat="server">
                                <asp:CheckBox ID="chkRecurringForceMD5OnModify" runat="server" />
                                <asp:Literal Text="<%$Resources: MD5_Signature_Edit %>" runat="server" />
                            </asp:Panel>
                        </li>

                    </ul>
                     <div class="wrap-button margin-top-10">
                        <asp:LinkButton CssClass="btn btn-success btn-cons-short" runat="server" ID="btnSave" OnClick="btnSave_Save">
                               <i class="fa fa-floppy-o"></i>  <asp:Literal Text="<%$Resources: Save %>" runat="server" />
                        </asp:LinkButton>
                </div>
                </div>

               
            </div>

        </div>
    </div>
</asp:Content>

