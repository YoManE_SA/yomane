﻿Imports Netpay.Bll
Imports Netpay.Web
Imports Netpay.Web.Controls

Partial Class Website_VirtualTerminalEcheck
	Inherits MasteredPage

	Dim strstate As String = ""
	Dim bHasRows As Boolean, bCVV2 As String, bPersonalNumber As String, bPhoneNumber As String, bEmail As String, bConfirmation As String, sWhere As String
	Dim IsAllowRecurring As Boolean, bRefund As Boolean
	Dim IssuanceFee As Decimal = 0D, nTotalItems As Decimal = 0D
	Dim sErrorRet As String = "", pRet As String = "", replyCode As String = "", replyDesc As String = ""

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Merchant.ProcessSettings.IsSystemPayECheck Then wcElementBlocker.Visible = False

        IssuanceFee = dbPages.TestVar(dbPages.getConfig("IssuanceFee"), 0D, -1D, 0D)

		If Not Page.IsPostBack Then
			DivUsa.Style.Add("display", "block")
			DivCanada.Style.Add("display", "none")
		End If
		ShowHideRecurring()
	End Sub

	Protected Sub BtnCharge_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
		If Not String.IsNullOrEmpty(RecurringCount.Text) Then
            If Not Bll.Transactions.Recurring.MerchantSettings.Current.IsEnabled Then
                lblResult.Text = "<div class=""error"">" & GetLocalResourceObject("RecurringNotAllowed").ToString & "</div>"
                Exit Sub
            End If
		End If

		Dim strState = ""
		ddlStateUsa.Enabled = True
		ddlStateCanada.Enabled = True
		If (ddlCountry.SelectedValue = "228") Then ' Usa for state
			strState = ddlStateUsa.SelectedValue
			DivUsa.Style.Add("display", "block")
			DivCanada.Style.Add("display", "none")
		End If

		If (ddlCountry.SelectedValue = "43") Then	' Canada for state
			strState = ddlStateCanada.SelectedValue
			DivCanada.Style.Add("display", "block")
			DivUsa.Style.Add("display", "none")
		End If
		lblResult.Text = ""
		If Validation.IsEmptyField(txtaccountName, lblResult, CType(GetLocalResourceObject("BexactName"), String)) Then Exit Sub
		If Validation.IsEmptyField(txtroutingNumber, lblResult, CType(GetLocalResourceObject("RoutingNumber"), String)) Then Exit Sub
		If Validation.IsEmptyField(txtAccountNumber, lblResult, CType(GetLocalResourceObject("AccountNumber"), String)) Then Exit Sub
		If Validation.IsEmptyField(txtBankName, lblResult, CType(GetLocalResourceObject("BankName"), String)) Then Exit Sub
		If Validation.IsEmptyField(txtPersonalNum, lblResult, CType(GetLocalResourceObject("IDNumber"), String)) Then Exit Sub
		If Validation.IsEmptyField(txtBankCity, lblResult, CType(GetLocalResourceObject("BankCity"), String)) Then Exit Sub
		If Validation.IsEmptyField(txtBankPhone, lblResult, CType(GetLocalResourceObject("BankPhone"), String)) Then Exit Sub
		If Validation.IsEmptyField(txtEmail, lblResult, CType(GetGlobalResourceObject("MultiLang", "CCHolderMail"), String)) Then Exit Sub
		If Validation.IsNotMail(txtEmail, lblResult, CType(GetGlobalResourceObject("MultiLang", "CCHolderMail"), String)) Then Exit Sub
		If Validation.IsEmptyField(txtBillingAddress1, lblResult, CType(GetGlobalResourceObject("MultiLang", "Address"), String)) Then Exit Sub
		If Validation.IsEmptyField(txtBillingZipCode, lblResult, CType(GetGlobalResourceObject("MultiLang", "ZipCode"), String)) Then Exit Sub
		If Validation.IsEmptyField(txtBillingCity, lblResult, CType(GetGlobalResourceObject("MultiLang", "BCity"), String)) Then Exit Sub
		If Validation.IsEmptyField(txtAmount, lblResult, CType(GetGlobalResourceObject("MultiLang", "Amount"), String)) Then Exit Sub
		If Validation.IsNotLimitedTo(txtAmount, "1234567890 *", lblResult, CType(GetGlobalResourceObject("MultiLang", "Amount"), String)) Then Exit Sub

		Dim sRecurring1 As String = String.Empty
		If chkIsUseRecurring.Checked Then
			If Validation.IsNotSelected(RecurringMode, lblResult, CType(GetGlobalResourceObject("MultiLang", "IntervalUnit"), String)) Then Exit Sub
			If Validation.IsEmptyField(RecurringCycle, lblResult, CType(GetGlobalResourceObject("MultiLang", "IntervalLength"), String)) Then Exit Sub
			If Validation.IsNotLimitedTo(RecurringCycle, "1234567890", lblResult, CType(GetGlobalResourceObject("MultiLang", "IntervalLength"), String)) Then Exit Sub
			If Validation.IsEmptyField(RecurringCount, lblResult, CType(GetGlobalResourceObject("MultiLang", "NumberOfCharges"), String)) Then Exit Sub
			If Validation.IsNotLimitedTo(RecurringCount, "1234567890", lblResult, CType(GetGlobalResourceObject("MultiLang", "NumberOfCharges"), String)) Then Exit Sub
			sRecurring1 = RecurringCount.Text & RecurringMode.SelectedValue & RecurringCycle.Text
		End If

		Dim sendStr As String = "remoteCharge_echeck.asp"
		sendStr &= "?CompanyNum=" & Bll.Accounts.Account.Current.AccountNumber
		sendStr &= "&AccountName=" & Server.UrlEncode(txtaccountName.Text)
		sendStr &= "&RoutingNumber=" & Server.UrlEncode(txtroutingNumber.Text)
		sendStr &= "&AccountNumber=" & Server.UrlEncode(txtAccountNumber.Text)
		sendStr &= "&BankAccountType=" & Server.UrlEncode(ddlBankAccountType.SelectedValue)
		sendStr &= "&BankName=" & Server.UrlEncode(txtBankName.Text)
		sendStr &= "&BankCity=" & Server.UrlEncode(txtBankCity.Text)
		sendStr &= "&BankPhone=" & Server.UrlEncode(txtBankPhone.Text)
		sendStr &= "&BankState=" & ddlStateUsa.SelectedValue
		sendStr &= "&Amount=" & (txtAmount.Text + IssuanceFee).ToString("0.00")
		sendStr &= "&Currency=" & 1
		sendStr &= "&CreditType =" & ddlCreditType.SelectedValue
		sendStr &= "&BirthDate=" & txtBirthDate.Text
		sendStr &= "&BillingAddress1=" & Server.UrlEncode(txtBillingAddress1.Text)
		sendStr &= "&BillingAddress2=" & Server.UrlEncode(txtBillingAddress2.Text)
		sendStr &= "&BillingCity=" & Server.UrlEncode(txtBillingCity.Text)
		sendStr &= "&BillingZipCode=" & Server.UrlEncode(txtBillingZipCode.Text)
		sendStr &= "&BillingState=" & ddlBankState.SelectedValue
		sendStr &= "&BillingCountry=" & ddlCountry.SelectedValue
		sendStr &= "&Email=" & Server.UrlEncode(txtEmail.Text)
		sendStr &= "&PersonalNum=" & Server.UrlEncode(txtPersonalNum.Text)
		sendStr &= "&PhoneNumber=" & Server.UrlEncode(txtPhoneNumber.Text)
		sendStr &= "&Comment=TC:" & txtComment.Text & ", MID:" & Merchant.ID
		If Not String.IsNullOrEmpty(sRecurring1) Then sendStr &= "&Recurring1=" & sRecurring1

		Dim resultNum As Integer = dbPages.SendHttpRequest(WebUtils.CurrentDomain.ProcessUrl & sendStr, sendStr)
		If resultNum = 200 Then
			pRet = dbPages.GetUrlValue(sendStr, "replyCode")
			Dim pTransID As Integer = dbPages.TestVar(dbPages.GetUrlValue(sendStr, "transId"), 0, -1, 0)
			replyCode = dbPages.TestVar(dbPages.GetUrlValue(sendStr, "replyCode"), 0, -1, 0)
			replyDesc = dbPages.GetUrlValue(sendStr, "replyMessage")
			If pRet = "000" Or pRet = "001" Then
				sErrorRet = "<div class=""noerror"">" & "The transaction was successful : Transaction number:" & pTransID & "</div>"
			Else
				sErrorRet = "<div class=""error"">" & "The transaction has failed - error code " & pRet & " Error description: " & replyDesc & "</div>"
			End If
		Else
			sErrorRet = "<div class=""error"">" & "Error (" & resultNum & "): Comunication problem" & "</div>"
		End If
		lblResult.Text = sErrorRet
	End Sub

	Sub ShowHideRecurring(Optional ByVal o As Object = Nothing, Optional ByVal e As EventArgs = Nothing)
		If TypeOf (o) Is CheckBox And chkIsUseRecurring.Checked Then
			litRecurringNotAllowed.Text = String.Empty
            If Not Bll.Transactions.Recurring.MerchantSettings.Current.IsEnabled Then
                chkIsUseRecurring.Checked = False
                litRecurringNotAllowed.Text = "<div class=""error"" style=""margin:0px 0px 8px 0px;"">" & GetLocalResourceObject("RecurringNotAllowed").ToString & "</div>"
            End If
		End If
		If chkIsUseRecurring.Checked Then
			RecurringCount.Enabled = True
			RecurringCycle.Enabled = True
			RecurringMode.Enabled = True
			lblRecurringCount.ForeColor = Drawing.Color.Black
			lblRecurringMode.ForeColor = Drawing.Color.Black
			lblRecurringRequiredCount.ForeColor = Drawing.Color.Maroon
			lblRecurringRequiredMode.ForeColor = Drawing.Color.Maroon
			RecurringCount.ForeColor = Drawing.Color.Black
			RecurringCycle.ForeColor = Drawing.Color.Black
			RecurringMode.ForeColor = Drawing.Color.Black
		Else
			RecurringCount.Enabled = False
			RecurringCycle.Enabled = False
			RecurringMode.Enabled = False
			lblRecurringCount.ForeColor = Drawing.Color.Gray
			lblRecurringMode.ForeColor = Drawing.Color.Gray
			lblRecurringRequiredCount.ForeColor = Drawing.Color.Gray
			lblRecurringRequiredMode.ForeColor = Drawing.Color.Gray
			RecurringCount.ForeColor = Drawing.Color.Gray
			RecurringCycle.ForeColor = Drawing.Color.Gray
			RecurringMode.ForeColor = Drawing.Color.Gray
		End If
	End Sub
End Class
