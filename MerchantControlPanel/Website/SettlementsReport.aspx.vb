﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Linq
Imports Netpay.Bll
Imports Netpay.Bll.Reports
Imports System.Collections.Generic
Imports Netpay.CommonTypes
Imports Netpay.Web
Imports Netpay.Infrastructure
Imports Netpay.Web.Controls
Imports Netpay.MerchantControlPanel

Partial Class Website_SettlementsReport
	Inherits MasteredPage
    Implements IPostBackEventHandler

	Dim xCounter As Integer = 0

	Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
		If Request("PayID") <> "" And Not IsAuthorized() Then
			Response.Write("REDIRECT:Default.aspx")
			Response.End()
		End If
		MyBase.OnPreInit(e)
	End Sub

	Function DisplayPayStatus(ByVal nWireStatus As Integer, ByVal dtWireStatusDate As Date, ByVal nCurrency As Integer, ByVal nTransPayTotal As Decimal, ByVal nWireFee As Decimal, ByVal sWireFileName As String) As String
		Dim sOutputStr As String = ""
		If (nWireStatus > 4 Or nWireStatus = 3) Then
			sOutputStr = sOutputStr & nWireStatus.EnumText("Enums")
			If IsDate(dtWireStatusDate) And dtWireStatusDate <> "01/01/1900" And Year(dtWireStatusDate) > 1900 Then
				sOutputStr = sOutputStr & " - " & dbPages.FormatDatesTimes(dtWireStatusDate, 1, 0, 0) & "<br />"
				sOutputStr = sOutputStr & dbPages.FormatCurr(nCurrency, nTransPayTotal - CType(dbPages.TestVar(nWireFee, 0D, -1D, 0D), Decimal))
			End If
			If CType(dbPages.TestVar(nWireFee, 0D, -1D, 0D), Decimal) <> 0 Then
				sOutputStr = sOutputStr & " (" & dbPages.FormatCurr(nCurrency, CType(dbPages.TestVar(nWireFee, 0D, -1D, 0D), Decimal)) & " fee)"
			End If
			If Len("" & sWireFileName) > 0 Then
                Dim sExt As String = Mid(sWireFileName, InStrRev(sWireFileName, ".") + 1)
                sOutputStr = sOutputStr & " <a href=""" & ClientScript.GetPostBackClientHyperlink(Me, "Download$" & sWireFileName) & """><img src=""/NPCommon/ImgFileExt/14X14/" & sExt & ".gif"" style=""border-width:0;"" /></a>"
			End If
		Else
			sOutputStr = sOutputStr & "---"
		End If
		DisplayPayStatus = sOutputStr
	End Function

    Public Shadows Sub RaisePostBackEvent(eventArgument As String) Implements IPostBackEventHandler.RaisePostBackEvent
        Dim values = eventArgument.Split("$")
        If (values.Length = 2 And values(0) = "Download") Then
            WebUtils.SendFile(Domain.MapPrivateDataPath("Wires/WireFiles/" + values(1)), Nothing, "attachment")
        End If
    End Sub

    Function DisplayInvoicePrintLink(ByVal InvoiceNumber As Integer, ByVal ID_BillingCompanyID As Integer, ByVal ID_Type As Integer?) As String
        If InvoiceNumber = 0 Or WebUtils.CurrentLanguage <> Language.Hebrew Then Return String.Empty
        Return "<br/><a href=""javascript:netpay.Common.openPopupWindow('billingShow_frameset.aspx?BillingCompanysID=" & ID_BillingCompanyID & "&billingNumber=" & InvoiceNumber & "&invoiceType=" & ID_Type.GetValueOrDefault() & "','BillingWin',700,500,1);"">" & GetLocalResourceObject("PrintInvoice") & "</a>"
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request("PayID") <> "" Then
            ShowTotalsButton.LoadTotalsParams(Total1)
            Dim sti As Settlements.SettlementInfo = Settlements.SettlementInfo.GetSettlementInfo(Total1.PayID)
            If sti.MinTrans IsNot Nothing Then ltTransRangeOld.Text = sti.MinTrans.Value.ToString(DateTimeFormatInfo.CurrentInfo.ShortDatePattern & " hh:mm") & " (" & sti.MinTransID.GetValueOrDefault() & ")"
            If sti.MaxTrans IsNot Nothing Then ltTransRangeNew.Text = sti.MaxTrans.Value.ToString(DateTimeFormatInfo.CurrentInfo.ShortDatePattern & " hh:mm") & " (" & sti.MaxTransID.GetValueOrDefault() & ")"
            TransView.Visible = True
            TransView.RenderControl(New HtmlTextWriter(Response.Output))
            Response.End()
        End If
		
        lvLegend.Add(Bll.Wires.Wire.WireStatus.Pending, "New", "", "WireStatus_Pending")
        lvLegend.Add(Bll.Wires.Wire.WireStatus.Canceled, "Canceled", "", "WireStatus_Canceled")
        lvLegend.Add(Bll.Wires.Wire.WireStatus.Sent, "Sent", "", "WireStatus_Sent")
        lvLegend.Add(Bll.Wires.Wire.WireStatus.Completed, "Completed", "", "WireStatus_Completed")
        If Not IsPostBack Then OnPageChanged(sender, e)
    End Sub

    Protected Sub OnPageChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim settlementID As Integer = Request("settlementID").ToInt32(0)
        If settlementID = 0 Then
            repeaterSettlements.DataSource = Netpay.Bll.Settlements.Settlement.LoadForMerchant(Merchant.ID, pager.Info)
            repeaterSettlements.DataBind()
            pager.Visible = True
        Else
            Dim settlement = Netpay.Bll.Settlements.Settlement.Load(settlementID)
            If settlement Is Nothing Then Exit Sub
            Dim list As New List(Of Netpay.Bll.Settlements.Settlement)
            list.Add(settlement)

            repeaterSettlements.DataSource = list
            repeaterSettlements.DataBind()

            pager.Visible = False
        End If
    End Sub

	Protected Function GetStatusStyle(ByVal wireStatus As Bll.Wires.Wire.WireStatus) As String
		Dim result As String = lvLegend.GetItemClass(wireStatus)
		If result Is Nothing Then result = lvLegend.GetItemClass(Bll.Wires.Wire.WireStatus.Completed)
		Return result
	End Function

	Protected Sub CreateCSV(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
		Dim args As String() = Split(e.CommandArgument, ",")
		Dim payID As Integer = args(0).ToInt32(0)
        btnExportExcel.Export("SETTLEMENT REPORT", ReportType.MerchantCapturedTransactions, Function(pi) Bll.Transactions.Transaction.GetSettlementTransactions(payID, pi))
	End Sub
End Class
