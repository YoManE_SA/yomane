﻿Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports Netpay.Bll.Reports
Imports Netpay.Bll
Imports Netpay.Infrastructure
Imports Netpay.Web.Controls

Partial Class Website_SettlementList
	Inherits MasteredPage

	Dim wireStatusText As String()
	Dim counter As Integer = 0

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Len("" & Request("payDate")) > 0 Then
			ltSettlement.Text = GetGlobalResourceObject("Multilang", "SettlementNumber") & " " & Request("payID")
			ltTime.Text = dbPages.FormatDatesTimes(Request("payDate"), 1, 1, 0)
		End If

		Dim currency As Integer, payID As Integer
		If Not Integer.TryParse(Request("Currency"), currency) Then Response.Redirect("~/", True)
		If Not Integer.TryParse(Request("payID"), payID) Then Response.Redirect("~/", True)
		ucShowTotalsButton.CurrencyID = currency
		ucShowTotalsButton.PayID = payID
		If Not IsPostBack Then OnPageChanged(sender, e)
	End Sub

 	Protected Sub OnPageChanged(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim currencyID As Integer, payID As Integer
		If Not Integer.TryParse(Request("Currency"), currencyID) Then Response.Redirect("~/", True)
		If Not Integer.TryParse(Request("payID"), payID) Then Response.Redirect("~/", True)

		If TypeOf e Is ExcelButton.ExportEventArgs Then
            btnExportExcel.SetExportInfo("SETTLEMENT REPORTS", ReportType.MerchantCapturedTransactions, Function(pi) Bll.Transactions.Transaction.GetSettlementTransactions(payID, pi))
		Else
            repeaterResults.DataSource = Bll.Transactions.Transaction.GetSettlementTransactions(payID, pager.Info)
			repeaterResults.DataBind()
			btnExportExcel.ResultCount = pager.Info.RowCount
		End If
	End Sub

	Protected Sub ExportExcel_Click(ByVal sender As Object, ByVal e As EventArgs)
		OnPageChanged(sender, e)
		btnExportExcel.Export()
	End Sub

	Protected Function GetSettlemntContext() As String
		Return Request("payID")
	End Function
End Class
