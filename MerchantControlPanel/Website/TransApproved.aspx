﻿<%@ Page Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" EnableEventValidation="false" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_TransApproved" Title="PageTitle" CodeBehind="TransApproved.aspx.vb" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources: TransApproved.aspx,PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
    <asp:HiddenField ID="hdLastSearchMode" runat="server" />
    <asp:HiddenField ID="hdDeviceId" runat="server" />
    <div class="area-content">
        <asp:RegularExpressionValidator Display="Dynamic" ID="Last4digits" ClientIDMode="Static" ControlToValidate="txtLast4Digits" ValidationExpression="\d{4,4}$" runat="server" ErrorMessage="<%$Resources:MultiLang,Last4digitsValid  %>"></asp:RegularExpressionValidator>
        <div class="top-nav">
            <asp:Literal ID="Literal4" runat="server" Text="<%$Resources: TransApproved.aspx,PageTitle%>" />
        </div>
        <div class="content-background">
            <div class="legend-menu">
                <div class="align-right">
                    <netpay:PopupButton ID="PopupButton1" IconSrc="Images/iconLegend.gif" Text="<%$Resources:MultiLang,Legend  %>" runat="server">
                    <netpay:Legend ID="ttLegend" Chargeback="true" Clarification="true" PendingChargeback="true" Admin="true" System="true" Test="true" Capture="true" Authorize="true" Bank="true" runat="server" />
                    </netpay:PopupButton>
                </div>
            </div>
            <div class="section">
                <div class="filter">
                    <div class="quick-search">
                        <asp:Literal ID="Literal6" runat="server" Text="<%$Resources: TransApproved.aspx,QuickSearch%>" />
                        <div class="field-quick">
                            <asp:TextBox ID="txtTransactionNumber" runat="server" placeholder="<%$Resources:MultiLang,TransactionID  %>" CssClass="Field_110"></asp:TextBox>
                        </div>
                        <div class="button-quick">
                            <asp:LinkButton ID="btnShowTransaction" OnClick="btnShowTransaction_Click" CssClass="btn" runat="server" OnClientClick="javascript:return document.getElementById('ctl00_cphBody_txtTransactionNumber').value!=''"><i class="fa fa-search"></i> <asp:Literal Text="<%$Resources:MultiLang,Search %>" runat="server" /></asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="Approved">
                    <div>
                        <asp:RadioButtonList CssClass="RadioButtonWidth" ID="rblSearchType" RepeatLayout="Flow" runat="server"
                            RepeatDirection="Horizontal" CellSpacing="0">
                            <asp:ListItem Selected="True" Value="0" Text="<%$Resources: TransApproved.aspx,FilterCaptured%>"></asp:ListItem>
                            <asp:ListItem Value="1" Text="<%$Resources: TransApproved.aspx,FilterAuthorized%>"></asp:ListItem>
                            <asp:ListItem Value="2" Text="<%$Resources: TransApproved.aspx,FilterChargebacks%>"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="margin-bottom-10">
                        <netpay:DateRangePicker Visible="true" ID="wcDateRangePicker" HtmlLayout="Flow" Layout="Horizontal" runat="server" class="Field_100" />
                        <netpay:PaymentMethodDropDown BlankSelectionText="<%$Resources:MultiLang,PaymentMethod  %>" ID="ddPaymentMethod" class="Field_200" runat="server" />
                        <netpay:CurrencyDropDown ID="ddCurrency" BlankSelectionText="<%$Resources:MultiLang,Currency %>" class="Field_100" runat="server" />
                        <netpay:PaymentsStatusDropDown ID="ddPaymentsStatus" BlankSelectionText="<%$Resources:MultiLang,PayoutStatus %>" class="Field_140" runat="server" />
                    </div>
                    <div class="margin-bottom-10">
                        <netpay:CreditTypeDropDown ID="ddCreditType" BlankSelectionText="<%$Resources:MultiLang,CreditType %>" class="Field_140" runat="server" />
                        <asp:TextBox ID="txtPayerId" runat="server" class="Field_75" placeholder="<%$Resources: MultiLang,PayerID%>" />
                        <asp:TextBox ID="txtOrderNumber" class="Field_132" runat="server" placeholder="<%$Resources: MultiLang,Order%>" />
                        <asp:TextBox ID="txtPayerFirstName" class="Field_160" runat="server" placeholder="<%$Resources: MultiLang,PayerFirstName%>" />
                        <asp:TextBox ID="txtPayerLastName" class="Field_160" runat="server" placeholder="<%$Resources: MultiLang,PayerLastName%>" />
                    </div>
                    <div>
                        <asp:TextBox ID="txtCardHolderEmail" class="Field_160" runat="server" placeholder="<%$Resources: MultiLang,CCHolderMail%>" />
                        <asp:TextBox ID="txtLast4Digits" runat="server" class="Field_100" placeholder="<%$Resources: MultiLang,Last4Digits%>" />
                    </div>
                </div>
                <div class="spacer">
                </div>
                <div class="wrap-button-bar">
                    <asp:LinkButton ID="btnSearch" OnClick="btnSearch_Click" CssClass="btn btn-cons-short btn-inverse" runat="server"><i class="fa fa-search"></i> <asp:Literal Text="<%$Resources:MultiLang,Search %>" runat="server" /></asp:LinkButton>
                </div>
            </div>
            <asp:PlaceHolder ID="phResults" Visible="false" runat="server">
                <div class="section">
                    <netpay:SearchFiltersView ID="wcFiltersView" runat="server" Visible="false" />
                    <table class="exspand-table">
                        <thead>
                            <tr>
                                <th colspan="2"></th>
                                <th style="text-align: <%= WebUtils.CurrentAlignment %>;">
                                    <asp:Literal Text="<%$Resources:MultiLang,Transaction %>" runat="server" />
                                </th>
                                <th style="text-align: <%= WebUtils.CurrentAlignment %>;">
                                    <asp:Literal ID="litDateTitle" Text="<%$Resources:MultiLang,Date %>" runat="server" />
                                </th>
                                <th style="text-align: <%= WebUtils.CurrentAlignment %>;">
                                    <asp:Literal Text="<%$Resources:MultiLang,PaymentMethod %>" runat="server" />
                                </th>
                                <th style="text-align: <%= WebUtils.CurrentAlignment %>;">
                                    <asp:Literal Text="<%$Resources:MultiLang,Amount %>" runat="server" />
                                </th>
                                <asp:Literal ID="lblTableHeadings" runat="server" />
                            </tr>
                        </thead>
                        <asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server" OnItemDataBound="SetRowView">
                            <ItemTemplate>
                                <netpay:MerchantTransactionRowView ID="ucTransactionView" Transaction="<%# CType(Container.DataItem, Transactions.Transaction) %>"
                                    ShowStatus="true" runat="server" />
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <div class="xls-wrap">
                        <netpay:ExcelButton ID="btnExportExcel" Text="<%$Resources:MultiLang,ExportToXls %>"
                            Enabled="false" IconImageOn="xls.png" IconImageOff="xls_off.png" runat="server" OnClick="ExportExcel_Click" />
                    </div>
                    <netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />

                </div>
            </asp:PlaceHolder>
            <div id="Div1" class="Error" visible="false" runat="server">
                <asp:Literal ID="ltError" Visible="false" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
