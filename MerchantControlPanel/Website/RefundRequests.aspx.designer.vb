﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Website_RefundRequests
    
    '''<summary>
    '''locPageTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents locPageTitle As Global.System.Web.UI.WebControls.Localize
    
    '''<summary>
    '''Localize1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Localize1 As Global.System.Web.UI.WebControls.Localize
    
    '''<summary>
    '''PopupButton1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PopupButton1 As Global.Netpay.Web.Controls.PopupButton
    
    '''<summary>
    '''lvRefundRequests control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lvRefundRequests As Global.Netpay.Web.Controls.LegendView
    
    '''<summary>
    '''wcDateRangePicker control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents wcDateRangePicker As Global.Netpay.Web.Controls.DateRangePicker
    
    '''<summary>
    '''cbStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbStatus As Global.Netpay.Web.Controls.RefundRequestStatusDropDown
    
    '''<summary>
    '''cbNumType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbNumType As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''txtSearchID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSearchID As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''btnIDSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnIDSearch As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''BtnSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents BtnSearch As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''phResults control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents phResults As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Literal5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal5 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Literal6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal6 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Literal7 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal7 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Literal8 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal8 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Literal9 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal9 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Literal11 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal11 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Literal12 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal12 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''repeaterResults control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents repeaterResults As Global.System.Web.UI.WebControls.Repeater
    
    '''<summary>
    '''btnExportExcel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnExportExcel As Global.Netpay.Web.Controls.ExcelButton
    
    '''<summary>
    '''pager control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pager As Global.Netpay.Web.Controls.LinkPager
End Class
