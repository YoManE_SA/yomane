﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Tmp_netpayintl/page.master" CodeBehind="ExternalCardPayments.aspx.vb" Inherits="Netpay.MerchantControlPanel.ExternalCardPayments" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
	<h1><asp:Localize ID="txtTitle" Text="" runat="server" /></h1>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
	<div id="divBlocker">
        <asp:PlaceHolder ID="phBalance" runat="server">
            <table align="center" cellpadding="1" cellspacing="1" style="width: 92%">
			    <tr><td><span><asp:Literal Text="<%$Resources:Balance %>" runat="server"/>: <span id="divBalance" runat="server"></span></span></td></tr>
			    <tr><td><span><asp:Literal Text="<%$Resources:DueFees %>" runat="server"/>: <span id="divDueFees" runat="server"></span></span></td></tr>
		    </table>
		    <br />        
        </asp:PlaceHolder>
		<table border="0" align="center" style="width:92%; background-color:#f5f5f5; border:1px solid silver;">
			<tr>
				<td valign="top">
					<table border="0" cellspacing="0" cellpadding="1" width="98%" align="center">
						<tr>
							<td><asp:Literal Text="<%$Resources:MultiLang,Date %>" runat="server"></asp:Literal></td>
							<td><asp:Literal Text="<%$Resources:MultiLang,ReplyCode %>" runat="server"></asp:Literal></td>
							<td><asp:Literal Text="<%$Resources:MultiLang,Amount %>" runat="server"></asp:Literal></td>
							<td>&nbsp;</td>	
						</tr>
						<tr>
							<td><netpay:DateRangePicker ID="wcDate" Layout="Horizontal" runat="server" /></td>
							<td><input type="text" id="txtReplyCodeFilter" style="width:70px;" runat="server" /></td>
							<td><netpay:NumberRangeSlider ID="wcAmountSlider" ShowSlider="false" runat="server" /></td>
							<td>
								<asp:Button ID="btnSearch" Text="<%$Resources:MultiLang,Search %>" CssClass="button" OnClick="GetPayments" tabindex="18" runat="server" />
							</td>						
						</tr>
					</table>	
				</td>
				<td valign="top" style="border-<%= WebUtils.CurrentAlignment %>:1px dotted silver; padding-<%= WebUtils.CurrentAlignment %>:14px;">
					<table border="0" cellspacing="0" cellpadding="1" width="98%" align="center">					
						<tr><td><asp:Literal ID="Literal2" runat="server" Text="<%$Resources:PaymentID %>"></asp:Literal></td></tr>
						<tr>
							<td><asp:TextBox ID="txtPaymentID" style="width:70px;" runat="server" CssClass="input"></asp:TextBox></td>
							<td><asp:Button ID="btnShowPayment" OnClick="ShowPayment" Text="<%$Resources:MultiLang,Show %>" CssClass="button" Width="100px" runat="server" OnClientClick="javascript:return document.getElementById('ctl00_cphBody_txtPaymentID').value!=''" /></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<br />
		<asp:PlaceHolder ID="phResults" Visible="false" runat="server">
			<table class="formNormal" align="center" cellpadding="1" cellspacing="1" style="width: 92%">
				<tr>
					<th>
						&nbsp;
					</th>
					<th>
						<asp:Literal Text="<%$Resources:Payout %>" runat="server"></asp:Literal>
					</th>
					<th>
						<asp:Literal Text="<%$Resources:MultiLang,Date %>" runat="server"></asp:Literal>
					</th>
					<th>
						<asp:Literal Text="<%$Resources:MultiLang,Amount %>" runat="server"></asp:Literal>
					</th>
					<th>
						<asp:Literal Text="<%$Resources:Result %>" runat="server"></asp:Literal>
					</th>
					<td style="width:30px;"></td>
					<th>
						<asp:Literal ID="Literal1" Text="<%$Resources:Payee %>" runat="server"></asp:Literal>
					</th>
				</tr>					
				<asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server">
					<ItemTemplate>
						<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
							<td width="8px" class="<%# GetCssClass(CType(Container.DataItem, ExternalCardCustomerPaymentVO))%>">
								&nbsp;
							</td>
							<td>						
								<%# CType(Container.DataItem, ExternalCardCustomerPaymentVO).ID%>
							</td>
							<td>						
								<%# CType(Container.DataItem, ExternalCardCustomerPaymentVO).InsertDate%>
							</td>
							<td>						
								$<%# CType(Container.DataItem, ExternalCardCustomerPaymentVO).Amount.ToAmountFormat()%>
							</td>
							<td>
								<%# CType(Container.DataItem, ExternalCardCustomerPaymentVO).Result%> - <%# CType(Container.DataItem, ExternalCardCustomerPaymentVO).ResultDescription%>
							</td>
							<td style="background-color:#ffffff;"></td>
							<td>						
								<a href="ExternalCardCustomer.aspx?CustomerID=<%# CType(Container.DataItem, ExternalCardCustomerPaymentVO).CustomerID%>"><%# CType(Container.DataItem, ExternalCardCustomerPaymentVO).CustomerName%></a>
							</td>
						</tr>				
						<tr>
							<td height="1" colspan="5" bgcolor="silver"></td>
							<td></td>
							<td height="1" colspan="1" bgcolor="silver"></td>
						</tr>
					</ItemTemplate>
				</asp:Repeater>
			</table>		
			<table class="pager" align="center">
				<tr>			
					<td>				
						<netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
					</td>			
				</tr>
			</table>	
		</asp:PlaceHolder>
	</div>
	<netpay:ElementBlocker ID="wcElementBlocker" Visible="true" ElementClientID="divBlocker" Text="<%$ Resources:MultiLang,FeatureDisabled %>" runat="server" />
</asp:Content>
