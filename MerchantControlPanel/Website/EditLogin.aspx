﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_EditLogin" CodeBehind="EditLogin.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:MultiLang,EditLoginDetails%>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$Resources:MultiLang,EditLoginDetails%>" runat="server" />
        </div>
        <div class="content-background">


            <div class="section">
                <div class="margin-bottom-10 frame-border-option">
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">



                        <div class="align-left percent50">
                            <div class="form-group">
                                <div><asp:Localize runat="server" Text="<%$Resources:MultiLang,Email%>" /> <span class="require"><%=Validation.Mandatory%></span></div>
                                <div><asp:TextBox ID="txtMail" runat="server" Text="" CssClass="percent100" /></div>
                            </div>
                        </div>

                        <div class="align-left percent50">
                            <div class="form-group">
                                <div><asp:Localize runat="server" Text="<%$Resources:MultiLang,ValidateEmail%>" /> <span class="require"><%=Validation.Mandatory%></span></div>
                                <div><asp:TextBox ID="txtMail2" runat="server" Text="" CssClass="percent100" /><asp:HiddenField ID="hidMailOld" runat="server" Value="" /></div>
                            </div>
                        </div>

                        <div class="align-left percent50 margin-top-10">
                            <div class="form-group">
                                <div><asp:Localize runat="server" Text="<%$Resources:MultiLang,Password%>" /> <span class="require"><%=Validation.Mandatory%></span></div>
                                <div><asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Text="" CssClass="percent100" /></div>
                            </div>
                        </div>
                        <div class="align-left percent50 margin-top-10">
                            <div class="form-group">
                            <div><asp:Localize runat="server" Text="<%$Resources:MultiLang,ValidatePassword%>" /> <span class="require"><%=Validation.Mandatory%></span></div>
                            <div><asp:TextBox ID="txtPassword2" runat="server" TextMode="Password" Text="" CssClass="percent100" /></div>
                             </div>
                        </div>
                        <div class="spacer"> </div>
                    </div>
                        <div class="align-right">
                            <asp:Button ID="btnSave" runat="server" Text="<%$Resources:MultiLang,ButtonSave%>" CssClass="btn btn-success btn-cons-short" OnClick="SaveEditForm" />
                        </div>
                        <div class="align-left">
                            <div class="require">*<asp:Literal Text="<%$Resources:MultiLang,RequiredFields%>" runat="server" /></div>
                        </div>
                        <div class="spacer"></div>
                </div>

                <div class="margin-bottom-10 frame-border-option">
                    <div class="alert-info margin-bottom-10">
                        <asp:Localize runat="server" Text="<%$Resources:MultiLang,SignupPasswordText%>" />
                    </div>

                    <ul class="group-list">
                        <li>
                            <asp:Localize runat="server" Text="<%$Resources:MultiLang,SignupPasswordText1%>" /></li>
                        <li>
                            <asp:Localize runat="server" Text="<%$Resources:MultiLang,SignupPasswordText2%>" /></li>
                        <li>
                            <asp:Localize runat="server" Text="<%$Resources:MultiLang,SignupPasswordText3%>" /></li>
                        <li>
                            <asp:Localize runat="server" Text="<%$Resources:MultiLang,SignupPasswordText4%>" /></li>
                        <li>
                            <asp:Localize runat="server" Text="<%$Resources:MultiLang,SignupPasswordText5%>" /></li>
                    </ul>
                </div>
            </div>
            <div class="All-alerts">
                <asp:Label ID="lblResult" CssClass="error" Style="display: block;" runat="server" Visible="false" />
                <asp:Label ID="lblText" CssClass="noerror" Style="display: block;" runat="server" Visible="false" />
                <asp:Label ID="lblMailText" CssClass="error" Style="display: block;" runat="server" Visible="false" />
            </div>
        </div>
    </div>
</asp:Content>
