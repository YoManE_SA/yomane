﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_Default" Codebehind="Default.aspx.vb" %>
<%@ Register Src="~/Common/SiteMap.ascx" TagName="SiteMap" TagPrefix="UC1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" Runat="Server">
	<asp:Localize ID="locPageTitle" Text="<%$Resources:default.aspx, HomePage %>" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" Runat="Server">
    <script type="text/javascript">
        $(function() {
            <%= GetPostBackClientEvent(hiddenAsyncTrigger, "") %>
        });
    </script>
	<br />
	<table style="width:92%;" height="100%" border="0" cellpadding="0" cellspacing="0" align="center">
		<tr>
			<td valign="top" align="center" style="width:100%;">
				<UC1:SiteMap Width="500px" runat="server" />
			</td>
			<td valign="top">
				<div style="width:400px;" id="Notifications" runat="server">
                    <asp:UpdatePanel ID="upNotifications" runat="server">
                        <ContentTemplate>
                            <asp:PlaceHolder ID="phNotifications" Visible="false" runat="server">
                                <netpay:DynamicRepeater runat="server" ID="newNotifications">
                                <ItemTemplate>
                                    <netpay:NotificationSlip runat="server" Type="Reminder" Text='<%# Eval("Text") %>' />
                                </ItemTemplate>
                            </netpay:DynamicRepeater>
					            <netpay:NotificationSlip ID="wcNotificationSlipNews" Type="Reminder" Text="<%$Resources:default.aspx, SlipSiteNews %>" Title="<%$Resources:default.aspx, SlipSiteNewsTitle %>" runat="server" />
					            <br />
					            <netpay:NotificationSlip ID="wcNotificationSlipVerify" Type="Reminder" Text="<%$Resources:default.aspx, SlipRetrievalRequest %>" Title="<%$Resources:default.aspx, SlipRetrievalRequestTitle %>" runat="server" />
					            <br />
					            <netpay:NotificationSlip ID="wcNotificationSlipTest" Type="Warning" Text="<%$Resources:default.aspx, SlipTestTrans %>" Title="<%$Resources:default.aspx, SlipTestTransTitle %>" runat="server" />
					            <br />
					            <netpay:NotificationSlip ID="wcNotificationSlipVisaChbRatio" Visible="false" Type="Reminder" Text="<%$Resources:default.aspx, SlipChbViaRatio %>" Title="<%$Resources:default.aspx, SlipChbViaRatioTitle %>" runat="server" />
					            <br />
					            <netpay:NotificationSlip ID="wcNotificationSlipMastercardChbRatio" Visible="false" Type="Reminder" Text="<%$Resources:default.aspx, SlipChbMastercardRatio %>" Title="<%$Resources:default.aspx, SlipChbMastercardRatioTitle %>" runat="server" />
                            </asp:PlaceHolder>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="hiddenAsyncTrigger" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:Button ID="hiddenAsyncTrigger" runat="server" Text="AsyncUpdate" style="display:none;" />
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td colspan="2" valign="bottom" class="Disclaimer">
				<table align="center">
					<tr>
						<td>
							<asp:Literal ID="litDisclaimer" runat="server" Text="<%$Resources:MultiLang,Disclaimer%>" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</asp:Content>

