﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_BeneficiaryAddUpdate" CodeBehind="BeneficiaryAddUpdate.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">
            <asp:Label ID="lblError" Style="display: block;" runat="server" />
            <div class="section">
                <div class="margin-bottom-10 frame-border-option">
                    <div class="margin-bottom-10">
                       <i class="fa fa-credit-card"></i> <asp:Literal runat="server" Text="<%$ Resources:MultiLang,ProfileType %>" /><span>*</span>
                    </div>
                    <div class="frame-border-option-inline padding-10">
                        <div class="align-left percent33">
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:DropDownList ID="ddlProfileType" CssClass="percent85" runat="server" AutoPostBack="false" TabIndex="1" /></div>
                            </div>
                        </div>
                        <div class="spacer"></div>
                       
                    </div>
                     <div class="margin-bottom-10 margin-top-10">
                        <i class="fa fa-user"></i> <asp:Literal  runat="server" Text="<%$ Resources:MultiLang,PersonalInfo %>" />
                     </div>
                    <div class="frame-border-option-inline padding-10">

                        <div class="align-left percent33">
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal Text="<%$Resources:MultiLang,Identifier%>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="basicInfo_costumerNumber" CssClass="percent85" runat="server" TabIndex="2" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal runat="server" Text="<%$ Resources:MultiLang,Phone %>" />
                                </div>
                                <div>
                                    <asp:TextBox ID="basicInfo_phoneNumber" CssClass="percent85" runat="server" TabIndex="5" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal Text="<%$Resources:MultiLang,Address%>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="basicInfo_address" CssClass="percent85" runat="server" TabIndex="8" />
                                </div>
                            </div>
                        </div>
                        <div class="align-left percent33">
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal runat="server" Text="<%$ Resources:PersonalName %>" /><span>*</span>
                                </div>
                                <div>
                                    <asp:TextBox ID="basicInfo_costumerName" CssClass="percent85" runat="server" TabIndex="3" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal Text="<%$Resources:MultiLang,Fax%>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="basicInfo_faxNumber" CssClass="percent85" runat="server" TabIndex="6" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal Text="<%$Resources:MultiLang,Comment%>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="basicInfo_comment" CssClass="percent85" runat="server" TabIndex="9" />
                                </div>
                            </div>
                        </div>
                        <div class="align-left percent33">
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal runat="server" Text="<%$ Resources:ContactPerson %>" />
                                </div>
                                <div>
                                    <asp:TextBox ID="basicInfo_contactPersonName" CssClass="percent85" runat="server" TabIndex="4" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal Text="<%$Resources:MultiLang,Email%>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="basicInfo_email" CssClass="percent85" runat="server" TabIndex="7" />
                                </div>
                            </div>
                        </div>
                        <div class="spacer"></div>
                    </div>

                    <div class="margin-bottom-10 margin-top-10">
                       <i class="fa fa-bank"></i> <asp:Literal runat="server" Text="<%$ Resources:IsraeliBankDetails %>" />
                    </div>
                    <div class="frame-border-option-inline padding-10">

                        <div class="align-left percent33">
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal runat="server" Text="<%$ Resources:BeneficiaryName %>" /><span>**</span>
                                </div>
                                <div>
                                    <asp:TextBox ID="bankIsraelInfo_PayeeName" CssClass="percent85" runat="server" TabIndex="10" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal runat="server" Text="<%$ Resources:BranchNumber %>" /><span>*</span>
                                </div>
                                <div>
                                    <asp:TextBox ID="bankIsraelInfo_bankBranch" CssClass="percent85" runat="server" TabIndex="13" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal runat="server" Text="<%$ Resources:BankName %>" /><span>*</span>
                                </div>
                                <div>
                                    <netpay:BanksDropDown ID="bankIsraelInfo_BankCode" CssClass="percent85" runat="server" TabIndex="16" />
                                </div>
                            </div>

                        </div>
                        <div class="align-left percent33">
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal runat="server" Text="<%$ Resources:CompanyLegalNumber %>" /><span>***</span>
                                </div>
                                <div>
                                    <asp:TextBox ID="bankIsraelInfo_CompanyLegalNumber" CssClass="percent85" runat="server" TabIndex="11" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal runat="server" Text="<%$ Resources:AccountNumber %>" /><span>*</span>
                                </div>
                                <div>
                                    <asp:TextBox ID="bankIsraelInfo_AccountNumber" CssClass="percent85" runat="server" TabIndex="14" />
                                </div>
                            </div>

                        </div>
                        <div class="align-left percent33">
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal runat="server" Text="<%$ Resources:PersonalID %>" /><span>***</span>
                                </div>
                                <div>
                                    <asp:TextBox ID="bankIsraelInfo_personalIdNumber" CssClass="percent85" runat="server" TabIndex="12" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal runat="server" Text="<%$ Resources:PaymentType %>" />
                                </div>
                                <div>
                                    <asp:TextBox ID="bankIsraelInfo_PaymentMethod" CssClass="percent85" runat="server" TabIndex="15" />
                                </div>
                            </div>
                        </div>

                        <div class="spacer"></div>
                    </div>
                    <div class="margin-bottom-10 margin-top-10">
                      <i class="fa fa-bank"></i>  <asp:Literal runat="server" Text="<%$ Resources:AbroadBankDetails %>" />
                    </div>
                    <div class="frame-border-option-inline padding-10">
                        <div class="align-left percent33">
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal runat="server" Text="<%$ Resources:AccountName %>" /><span>**</span>
                                </div>
                                <div>
                                    <asp:TextBox ID="bankAbroadAccountName" CssClass="percent85" runat="server" TabIndex="17" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal Text="<%$Resources:MultiLang,City%>" runat="server" /><span>*</span>
                                </div>
                                <div>
                                    <asp:TextBox ID="bankAbroadBankAddressCity" CssClass="percent85" runat="server" TabIndex="20" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal runat="server" Text="<%$ Resources:Region %>" /><span>*</span>
                                </div>
                                <div>
                                    <asp:TextBox ID="bankAbroadBankAddressState" CssClass="percent85" runat="server" TabIndex="23" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>SEPA BIC</div>
                                <div>
                                    <asp:TextBox ID="bankAbroadSepaBic" CssClass="percent85" runat="server" TabIndex="26" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>ABA<span>****</span></div>
                                <div>
                                    <asp:TextBox ID="bankAbroadABA" CssClass="percent85" runat="server" TabIndex="29" />
                                </div>
                            </div>
                        </div>
                        <div class="align-left percent33">
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal runat="server" Text="<%$ Resources:AccountNumber %>" /><span>*</span>
                                </div>
                                <div>
                                    <asp:TextBox ID="bankAbroadAccountNumber" CssClass="percent85" runat="server" TabIndex="18" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal runat="server" Text="<%$ Resources:BankAddressLine1 %>" />
                                     <netpay:PageItemHelp ID="PageItemHelp4" Text="<%$Resources:MultiLang,AddressLine1Help%>" Width="300" Height="28" runat="server" />
                                </div>
                              
                                <div>
                                    <asp:TextBox ID="bankAbroadBankAddress" CssClass="percent85" runat="server" TabIndex="21" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal runat="server" Text="<%$ Resources:PostalCode %>" />
                                </div>
                                <div>
                                    <asp:TextBox ID="bankAbroadBankAddressZip" CssClass="percent85" runat="server" TabIndex="24" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>Swift Code<span>*</span></div>
                                <div>
                                    <asp:TextBox ID="bankAbroadSwiftNumber" CssClass="percent85" runat="server" TabIndex="27" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>Sort Code/BLZ/BSB</div>
                                <div>
                                    <asp:TextBox ID="bankAbroadSortCode" CssClass="percent85" runat="server" TabIndex="30" />
                                </div>
                            </div>
                        </div>
                        <div class="align-left percent33">
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal runat="server" Text="<%$ Resources:BankName %>" /><span>*</span>
                                </div>
                                <div>
                                    <asp:TextBox ID="bankAbroadBankName" Text="" CssClass="percent85" runat="server" TabIndex="19" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal runat="server" Text="<%$ Resources:BankAddressLine2 %>" />
                                     <netpay:PageItemHelp ID="PageItemHelp3" Text="<%$Resources:MultiLang,AddressLine2Help%>" Width="300" Height="28" runat="server" />
                                </div>
                               
                                <div>
                                    <asp:TextBox ID="bankAbroadBankAddressSecond" CssClass="percent85" runat="server" TabIndex="22" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal Text="<%$Resources:MultiLang,Country%>" runat="server" /><span>*</span>
                                </div>
                                <div>
                                    <netpay:CountryDropDown ID="ddlCountry" CssClass="percent85" runat="server" TabIndex="25" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>IBAN<span>****</span></div>
                                <div>
                                    <asp:TextBox ID="bankAbroadIBAN" CssClass="percent85" runat="server" TabIndex="28" />
                                </div>
                            </div>
                        </div>
                        <div class="spacer"></div>
                    </div>
                    <div class="margin-bottom-10 margin-top-10">
                       <i class="fa fa-bank"></i> <asp:Literal runat="server" Text="<%$ Resources:CorrespondentBankDetails %>" /></div>
                    <div class="frame-border-option-inline padding-10">
                        <div class="align-left percent33">
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal runat="server" Text="<%$ Resources:AccountName %>" /><span>**</span>
                                </div>
                                <div>
                                    <asp:TextBox ID="bankAbroadAccountName2" CssClass="percent85" runat="server" TabIndex="31" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal Text="<%$Resources:MultiLang,City%>" runat="server" /><span>*</span>
                                </div>
                                <div>
                                    <asp:TextBox ID="bankAbroadBankAddressCity2" CssClass="percent85" runat="server" TabIndex="34" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal runat="server" Text="<%$ Resources:Region %>" /><span>*</span>
                                </div>
                                <div>
                                    <asp:TextBox ID="bankAbroadBankAddressState2" CssClass="percent85" runat="server" TabIndex="37" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>SEPA BIC</div>
                                <div>
                                    <asp:TextBox ID="bankAbroadSepaBic2" CssClass="percent85" runat="server" TabIndex="40" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>ABA<span>****</span></div>
                                <div>
                                    <asp:TextBox ID="bankAbroadABA2" runat="server" CssClass="percent85" TabIndex="43" />
                                </div>
                            </div>
                        </div>
                        <div class="align-left percent33">
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal runat="server" Text="<%$ Resources:AccountNumber %>" /><span>*</span>
                                </div>
                                <div>
                                    <asp:TextBox ID="bankAbroadAccountNumber2" CssClass="percent85" runat="server" TabIndex="32" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal runat="server" Text="<%$ Resources:BankAddressLine1 %>" />*
                                    <netpay:PageItemHelp ID="PageItemHelp1" Text="<%$Resources:MultiLang,AddressLine1Help%>" Width="300" Height="28" runat="server" />
                                </div>
                                
                                
                            <div>
                                <asp:TextBox ID="bankAbroadBankAddress2" CssClass="percent85" runat="server" TabIndex="35" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal runat="server" Text="<%$ Resources:PostalCode %>" />
                                </div>
                                <div>
                                    <asp:TextBox ID="bankAbroadBankAddressZip2" CssClass="percent85" runat="server" TabIndex="38" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>Swift Code <span>*</span></div>
                                <div>
                                    <asp:TextBox ID="bankAbroadSwiftNumber2" CssClass="percent85" runat="server" TabIndex="41" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>Sort Code/BLZ/BSB </div>
                                <div>
                                    <asp:TextBox ID="bankAbroadSortCode2" CssClass="percent85" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="align-left percent33">
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal runat="server" Text="<%$ Resources:BankName %>" /><span>*</span>
                                </div>
                                <div>
                                    <asp:TextBox ID="bankAbroadBankName2" CssClass="percent85" runat="server" TabIndex="33" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal runat="server" Text="<%$ Resources:BankAddressLine2 %>" />
                                    <netpay:PageItemHelp ID="PageItemHelp2" Text="<%$Resources:MultiLang,AddressLine2Help%>" Width="300" Height="28" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="bankAbroadBankAddressSecond2" runat="server" CssClass="percent85" TabIndex="36" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>
                                    <asp:Literal Text="<%$Resources:MultiLang,Country%>" runat="server" /><span>*</span>
                                </div>
                                <div>
                                    <netpay:CountryDropDown ID="ddlCountry2" runat="server" CssClass="percent85" TabIndex="39" />
                                </div>
                            </div>
                            <div class="margin-bottom-10">
                                <div>IBAN<span>****</span></div>
                                <div>
                                    <asp:TextBox ID="bankAbroadIBAN2" CssClass="percent85" runat="server" TabIndex="42" />
                                </div>
                            </div>

                        </div>
                        <div class="spacer"></div>
                    </div>
                    <div class="alert-info margin-top-10">
                        *<asp:Literal runat="server" Text="<%$ Resources:MandatoryField %>" /><br />
                        **<asp:Literal runat="server" Text="<%$ Resources:MandatoryField1 %>" /><br />
                        ***<asp:Literal runat="server" Text="<%$ Resources:MandatoryField2 %>" /><br />
                        ****<asp:Literal runat="server" Text="<%$ Resources:MandatoryField3 %>" /><br />
                    </div>
                    <div class="wrap-button margin-top-10">
                            <% If Request("CompanyMakePaymentsProfiles_id") > 0 Then %>
                            <div class="align-right"><asp:LinkButton BorderWidth="0" CssClass="btn btn-success btn-cons-short" OnClick="BtnUpdate_OnClick" runat="server" TabIndex="45"><i class="fa fa-floppy-o"></i> <asp:Literal runat="server" Text="<%$Resources:MultiLang,Update%>" /></asp:LinkButton></div>
                            <div class="align-left"><asp:LinkButton BorderWidth="0" CssClass="btn btn-danger btn-cons-short" OnClick="BtnDelete_OnClick" runat="server" TabIndex="46"><i class="fa fa-trash"></i> <asp:Literal runat="server" Text="<%$Resources:MultiLang,Delete%>" /></asp:LinkButton></div>
                            <div class="spacer"></div>
                        
                            <% Else %>
                            <asp:LinkButton BorderWidth="0" CssClass="btn btn-success btn-cons-short" OnClick="BtnInsert_OnClick" runat="server" TabIndex="47"><i class="fa fa-plus-circle"></i> <asp:Literal runat="server" Text="<%$Resources:MultiLang, CREATE%>" /></asp:LinkButton>
                        <%  End If  %>
                    </div>
                </div>
              </div>
        </div>
      
       
    
    </div>
</asp:Content>
