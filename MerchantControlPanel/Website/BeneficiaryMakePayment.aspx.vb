﻿Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Linq
Imports Netpay.CommonTypes
Imports Netpay.Dal.Netpay
Imports Netpay.Infrastructure
Imports Netpay.Bll
Imports Netpay.Web
Imports Netpay.Web.Controls

Partial Class Website_BeneficiaryMakePayment
	Inherits MasteredPage

	Protected sSQL, sSQLWhere, sIP, CcStorageID, strState As String
	Protected IsCreditOk, i, nCurrencyFirst As Integer
	Protected bIsAvailableBalance As Boolean
	Private _availableBalance As Dictionary(Of Integer, Decimal)
	Protected dr As SqlDataReader
	Protected basicInfo_costumerNumber, basicInfo_costumerName, basicInfo_email, basicInfo_comment, basicInfo_address, bankIsraelInfo_CompanyLegalNumber, bankIsraelInfo_PayeeName, basicInfo_contactPersonName, basicInfo_faxNumber, basicInfo_phoneNumber As String
	Protected bankIsraelInfo_personalIdNumber, bankIsraelInfo_bankBranch, bankIsraelInfo_AccountNumber, bankAbroadInfo_BankAddressCountry2, bankAbroadInfo_SwiftNumber2, bankAbroadInfo_IBAN2, bankAbroadInfo_ABA2, bankAbroadInfo_SortCode2, bankIsraelInfo_PaymentMethod, bankIsraelInfo_BankCode, bankName, bankAbroadInfo_AccountName, bankAbroadInfo_AccountNumber, bankAbroadInfo_BankName, bankAbroadInfo_BankAddress, bankAbroadInfo_BankAddressSecond, bankAbroadInfo_BankAddressCity, bankAbroadInfo_BankAddressState, bankAbroadInfo_BankAddressZip, bankAbroadInfo_BankAddressCountry, bankAbroadInfo_SwiftNumber, bankAbroadInfo_IBAN, bankAbroadInfo_ABA, bankAbroadInfo_SortCode, bankAbroadInfo_AccountName2, bankAbroadInfo_AccountNumber2, bankAbroadInfo_BankName2, bankAbroadInfo_BankAddress2, bankAbroadInfo_BankAddressSecond2, bankAbroadInfo_BankAddressCity2, bankAbroadInfo_BankAddressState2, bankAbroadInfo_BankAddressZip2 As String
	Public nCurrencyDefault As Integer = 0

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Page.IsPostBack Then
			If wcDateRangePicker.FromDate Is Nothing Then wcDateRangePicker.FromDate = DateTime.Now.Date
		Else
			wcDateRangePicker.FromDate = DateTime.Today
			ddPaymentCurrency.SetCurrencyIDs(AvailableBalance.Keys.ToDelimitedString())
		End If

		Dim merchantBeneficiaryID As Nullable(Of Integer) = Request("CompanyMakePaymentsProfiles_id").ToNullableInt32()
		Dim merchantBeneficiaryType As Nullable(Of MerchantBeneficiaryProfileType) = Request("ProfileType").ToNullableEnumByValue(Of MerchantBeneficiaryProfileType)()
		If merchantBeneficiaryType Is Nothing Then merchantBeneficiaryType = MerchantBeneficiaryProfileType.SelfPay

		If trConfirmationFile IsNot Nothing Then trConfirmationFile.Visible = (merchantBeneficiaryType = MerchantBeneficiaryProfileType.Payoneer)

        If merchantBeneficiaryID IsNot Nothing Then
            Dim beneficiary = Netpay.Bll.Merchants.Beneficiary.GetBeneficiary(merchantBeneficiaryID)
            If beneficiary Is Nothing Then Response.Redirect("BeneficiaryList.aspx", True)
        End If

        '------------------------------------------------------------------------------------------
        '	Get Payment profile
        '------------------------------------------------------------------------------------------
        If merchantBeneficiaryType = Infrastructure.MerchantBeneficiaryProfileType.MerchantCard Then
            sSQL = "SELECT merchant.IDNumber, merchant.PaymentPayeeName, merchant.CompanyLegalNumber, merchant.PaymentAbroadAccountName, merchant.PaymentAbroadAccountNumber," & _
            " merchant.PaymentAbroadBankName, merchant.PaymentAbroadBankAddress, merchant.PaymentAbroadBankAddressSecond, merchant.PaymentAbroadBankAddressCity," & _
            " merchant.PaymentAbroadBankAddressState, merchant.PaymentAbroadBankAddressZip, merchant.PaymentAbroadBankAddressCountry, merchant.PaymentAbroadSwiftNumber, merchant.PaymentAbroadIBAN," & _
            " merchant.PaymentAbroadABA, merchant.PaymentAbroadSortCode, merchant.PaymentAbroadAccountName2, merchant.PaymentAbroadAccountNumber2, merchant.PaymentAbroadBankName2," & _
            " merchant.PaymentAbroadBankAddress2, merchant.PaymentAbroadBankAddressSecond2, merchant.PaymentAbroadBankAddressCity2, merchant.PaymentAbroadBankAddressState2," & _
            " merchant.PaymentAbroadBankAddressZip2, merchant.PaymentAbroadBankAddressCountry2, merchant.PaymentAbroadSwiftNumber2, merchant.PaymentAbroadIBAN2, merchant.PaymentAbroadABA2," & _
            " merchant.PaymentAbroadSortCode2, merchant.customerNumber, merchant.firstName +' '+merchant.lastName FullName, merchant.companyPhone, merchant.companyFax, merchant.mail, merchant.PaymentMethod," & _
            " merchant.PaymentAccount, merchant.PaymentBank, merchant.PaymentBranch, cl1.Name PaymentAbroadBankAddressCountryName, cl2.Name PaymentAbroadBankAddressCountryName2, blackList.bankName" & _
            " FROM tblCompany merchant LEFT OUTER JOIN tblSystemBankList blackList ON merchant.PaymentBank = blackList.ID" & _
            " LEFT OUTER JOIN [List].[CountryList] AS cl1 ON merchant.PaymentAbroadBankAddressCountry = cl1.CountryID" & _
            " LEFT OUTER JOIN [List].[CountryList] AS cl2 ON merchant.PaymentAbroadBankAddressCountry2 = cl2.CountryID WHERE merchant.id=" & Merchant.ID
            dr = dbPages.ExecReader(sSQL)
            If dr.Read() Then
                If Not IsDBNull(dr("customerNumber")) Then basicInfo_costumerNumber = dbPages.dbtextShow(dr("customerNumber"))
                If Not IsDBNull(dr("fullName")) Then basicInfo_costumerName = dbPages.dbtextShow(dr("fullName"))
                If Not IsDBNull(dr("companyPhone")) Then basicInfo_phoneNumber = dbPages.dbtextShow(dr("companyPhone"))
                If Not IsDBNull(dr("companyFax")) Then basicInfo_faxNumber = dbPages.dbtextShow(dr("companyFax"))
                If Not IsDBNull(dr("mail")) Then basicInfo_email = dbPages.dbtextShow(dr("mail"))
                If Not IsDBNull(dr("PaymentPayeeName")) Then bankIsraelInfo_PayeeName = dbPages.dbtextShow(dr("PaymentPayeeName"))
                If Not IsDBNull(dr("CompanyLegalNumber")) Then bankIsraelInfo_CompanyLegalNumber = dbPages.dbtextShow(dr("CompanyLegalNumber"))
                If Not IsDBNull(dr("IDNumber")) Then bankIsraelInfo_personalIdNumber = dbPages.dbtextShow(dr("IDNumber"))
                If Not IsDBNull(dr("PaymentBranch")) Then bankIsraelInfo_bankBranch = dbPages.dbtextShow(dr("PaymentBranch"))
                If Not IsDBNull(dr("PaymentAccount")) Then bankIsraelInfo_AccountNumber = dbPages.dbtextShow(dr("PaymentAccount"))
                If Not IsDBNull(dr("PaymentMethod")) Then bankIsraelInfo_PaymentMethod = dbPages.dbtextShow(dr("PaymentMethod"))
                If Not IsDBNull(dr("PaymentBank")) Then bankIsraelInfo_BankCode = dbPages.dbtextShow(dr("PaymentBank"))
                If Not IsDBNull(dr("bankName")) Then bankName = dbPages.dbtextShow(dr("bankName"))
                If Not IsDBNull(dr("PaymentAbroadAccountName")) Then bankAbroadInfo_AccountName = dbPages.dbtextShow(dr("PaymentAbroadAccountName"))
                If Not IsDBNull(dr("PaymentAbroadAccountNumber")) Then bankAbroadInfo_AccountNumber = dbPages.dbtextShow(dr("PaymentAbroadAccountNumber"))
                If Not IsDBNull(dr("PaymentAbroadBankName")) Then bankAbroadInfo_BankName = dbPages.dbtextShow(dr("PaymentAbroadBankName"))
                If Not IsDBNull(dr("PaymentAbroadBankAddress")) Then bankAbroadInfo_BankAddress = dbPages.dbtextShow(dr("PaymentAbroadBankAddress"))
                If Not IsDBNull(dr("PaymentAbroadBankAddressSecond")) Then bankAbroadInfo_BankAddressSecond = dbPages.dbtextShow(dr("PaymentAbroadBankAddressSecond"))
                If Not IsDBNull(dr("PaymentAbroadBankAddressCity")) Then bankAbroadInfo_BankAddressCity = dbPages.dbtextShow(dr("PaymentAbroadBankAddressCity"))
                If Not IsDBNull(dr("PaymentAbroadBankAddressState")) Then bankAbroadInfo_BankAddressState = dbPages.dbtextShow(dr("PaymentAbroadBankAddressState"))
                If Not IsDBNull(dr("PaymentAbroadBankAddressZip")) Then bankAbroadInfo_BankAddressZip = dbPages.dbtextShow(dr("PaymentAbroadBankAddressZip"))
                If Not IsDBNull(dr("PaymentAbroadBankAddressCountryName")) Then bankAbroadInfo_BankAddressCountry = dbPages.dbtextShow(dr("PaymentAbroadBankAddressCountryName"))
                If Not IsDBNull(dr("PaymentAbroadSwiftNumber")) Then bankAbroadInfo_SwiftNumber = dbPages.dbtextShow(dr("PaymentAbroadSwiftNumber"))
                If Not IsDBNull(dr("PaymentAbroadIBAN")) Then bankAbroadInfo_IBAN = dbPages.dbtextShow(dr("PaymentAbroadIBAN"))
                If Not IsDBNull(dr("PaymentAbroadABA")) Then bankAbroadInfo_ABA = dbPages.dbtextShow(dr("PaymentAbroadABA"))
                If Not IsDBNull(dr("PaymentAbroadSortCode")) Then bankAbroadInfo_SortCode = dbPages.dbtextShow(dr("PaymentAbroadSortCode"))
                If Not IsDBNull(dr("PaymentAbroadAccountName2")) Then bankAbroadInfo_AccountName2 = dbPages.dbtextShow(dr("PaymentAbroadAccountName2"))
                If Not IsDBNull(dr("PaymentAbroadAccountNumber2")) Then bankAbroadInfo_AccountNumber2 = dbPages.dbtextShow(dr("PaymentAbroadAccountNumber2"))
                If Not IsDBNull(dr("PaymentAbroadBankName2")) Then bankAbroadInfo_BankName2 = dbPages.dbtextShow(dr("PaymentAbroadBankName2"))
                If Not IsDBNull(dr("PaymentAbroadBankAddress2")) Then bankAbroadInfo_BankAddress2 = dbPages.dbtextShow(dr("PaymentAbroadBankAddress2"))
                If Not IsDBNull(dr("PaymentAbroadBankAddressSecond2")) Then bankAbroadInfo_BankAddressSecond2 = dbPages.dbtextShow(dr("PaymentAbroadBankAddressSecond2"))
                If Not IsDBNull(dr("PaymentAbroadBankAddressCity2")) Then bankAbroadInfo_BankAddressCity2 = dbPages.dbtextShow(dr("PaymentAbroadBankAddressCity2"))
                If Not IsDBNull(dr("PaymentAbroadBankAddressState2")) Then bankAbroadInfo_BankAddressState2 = dbPages.dbtextShow(dr("PaymentAbroadBankAddressState2"))
                If Not IsDBNull(dr("PaymentAbroadBankAddressZip2")) Then bankAbroadInfo_BankAddressZip2 = dbPages.dbtextShow(dr("PaymentAbroadBankAddressZip2"))
                If Not IsDBNull(dr("PaymentAbroadBankAddressCountryName2")) Then bankAbroadInfo_BankAddressCountry2 = dbPages.dbtextShow(dr("PaymentAbroadBankAddressCountryName2"))
                If Not IsDBNull(dr("PaymentAbroadSwiftNumber2")) Then bankAbroadInfo_SwiftNumber2 = dbPages.dbtextShow(dr("PaymentAbroadSwiftNumber2"))
                If Not IsDBNull(dr("PaymentAbroadIBAN2")) Then bankAbroadInfo_IBAN2 = dbPages.dbtextShow(dr("PaymentAbroadIBAN2"))
                If Not IsDBNull(dr("PaymentAbroadABA2")) Then bankAbroadInfo_ABA2 = dbPages.dbtextShow(dr("PaymentAbroadABA2"))
                If Not IsDBNull(dr("PaymentAbroadSortCode2")) Then bankAbroadInfo_SortCode2 = dbPages.dbtextShow(dr("PaymentAbroadSortCode2"))
            End If
            dr.Close()
        Else
            sSQL = "SELECT tblCompanyMakePaymentsProfiles.*, tblSystemBankList.bankName, " & _
            "cl1.Name bankAbroadBankAddressCountryName, cl2.Name bankAbroadBankAddressCountryName2 " & _
            "FROM tblCompanyMakePaymentsProfiles " & _
            "LEFT OUTER JOIN tblSystemBankList ON tblCompanyMakePaymentsProfiles.bankIsraelInfo_BankCode = tblSystemBankList.ID  " & _
            "LEFT OUTER JOIN [List].[CountryList] AS cl1 ON tblCompanyMakePaymentsProfiles.bankAbroadBankAddressCountry = cl1.CountryID  " & _
            "LEFT OUTER JOIN [List].[CountryList] AS cl2 ON tblCompanyMakePaymentsProfiles.bankAbroadBankAddressCountry2 = cl2.CountryID  " & _
            "WHERE company_id=" & Merchant.ID & " AND tblCompanyMakePaymentsProfiles.CompanyMakePaymentsProfiles_id=" & IIf(merchantBeneficiaryID Is Nothing, 0, merchantBeneficiaryID)
            dr = dbPages.ExecReader(sSQL)
            If dr.Read() Then
                merchantBeneficiaryType = dr("ProfileType").ToString().ToNullableEnumByValue(Of MerchantBeneficiaryProfileType)()
                'ProfileType = dr("ProfileType")
                basicInfo_costumerNumber = dr("basicInfo_costumerNumber")
                basicInfo_costumerName = dr("basicInfo_costumerName")
                basicInfo_contactPersonName = dr("basicInfo_contactPersonName")
                basicInfo_phoneNumber = dr("basicInfo_phoneNumber")
                basicInfo_faxNumber = dr("basicInfo_faxNumber")
                basicInfo_email = dr("basicInfo_email")
                basicInfo_address = dr("basicInfo_address")
                basicInfo_comment = dr("basicInfo_comment")
                If Not IsDBNull(dr("bankIsraelInfo_PayeeName")) Then bankIsraelInfo_PayeeName = dbPages.dbtextShow(dr("bankIsraelInfo_PayeeName").ToString)
                If Not IsDBNull(dr("bankIsraelInfo_CompanyLegalNumber")) Then bankIsraelInfo_CompanyLegalNumber = dr("bankIsraelInfo_CompanyLegalNumber").ToString
                If Not IsDBNull(dr("bankIsraelInfo_personalIdNumber")) Then bankIsraelInfo_personalIdNumber = dr("bankIsraelInfo_personalIdNumber").ToString
                If Not IsDBNull(dr("bankIsraelInfo_bankBranch")) Then bankIsraelInfo_bankBranch = dr("bankIsraelInfo_bankBranch").ToString
                If Not IsDBNull(dr("bankIsraelInfo_AccountNumber")) Then bankIsraelInfo_AccountNumber = dr("bankIsraelInfo_AccountNumber").ToString
                If Not IsDBNull(dr("bankIsraelInfo_PaymentMethod")) Then bankIsraelInfo_PaymentMethod = dr("bankIsraelInfo_PaymentMethod").ToString
                If Not IsDBNull(dr("bankIsraelInfo_BankCode")) Then bankIsraelInfo_BankCode = dr("bankIsraelInfo_BankCode").ToString
                If Not IsDBNull(dr("bankName")) Then bankName = dbPages.dbtextShow(dr("bankName").ToString)
                If Not IsDBNull(dr("bankAbroadAccountName")) Then bankAbroadInfo_AccountName = dbPages.dbtextShow(dr("bankAbroadAccountName").ToString)
                If Not IsDBNull(dr("bankAbroadAccountNumber")) Then bankAbroadInfo_AccountNumber = dbPages.dbtextShow(dr("bankAbroadAccountNumber").ToString)
                If Not IsDBNull(dr("bankAbroadBankName")) Then bankAbroadInfo_BankName = dbPages.dbtextShow(dr("bankAbroadBankName").ToString)
                If Not IsDBNull(dr("bankAbroadBankAddress")) Then bankAbroadInfo_BankAddress = dbPages.dbtextShow(dr("bankAbroadBankAddress").ToString)
                If Not IsDBNull(dr("bankAbroadBankAddressSecond")) Then bankAbroadInfo_BankAddressSecond = dbPages.dbtextShow(dr("bankAbroadBankAddressSecond").ToString)
                If Not IsDBNull(dr("bankAbroadBankAddressCity")) Then bankAbroadInfo_BankAddressCity = dbPages.dbtextShow(dr("bankAbroadBankAddressCity").ToString)
                If Not IsDBNull(dr("bankAbroadBankAddressState")) Then bankAbroadInfo_BankAddressState = dbPages.dbtextShow(dr("bankAbroadBankAddressState").ToString)
                If Not IsDBNull(dr("bankAbroadBankAddressZip")) Then bankAbroadInfo_BankAddressZip = dbPages.dbtextShow(dr("bankAbroadBankAddressZip").ToString)
                If Not IsDBNull(dr("bankAbroadBankAddressCountryName")) Then bankAbroadInfo_BankAddressCountry = dbPages.dbtextShow(dr("bankAbroadBankAddressCountryName").ToString)
                If Not IsDBNull(dr("bankAbroadSwiftNumber")) Then bankAbroadInfo_SwiftNumber = dbPages.dbtextShow(dr("bankAbroadSwiftNumber").ToString)
                If Not IsDBNull(dr("bankAbroadIBAN")) Then bankAbroadInfo_IBAN = dbPages.dbtextShow(dr("bankAbroadIBAN").ToString)
                If Not IsDBNull(dr("bankAbroadABA")) Then bankAbroadInfo_ABA = dbPages.dbtextShow(dr("bankAbroadABA").ToString)
                If Not IsDBNull(dr("bankAbroadSortCode")) Then bankAbroadInfo_SortCode = dbPages.dbtextShow(dr("bankAbroadSortCode").ToString)
                If Not IsDBNull(dr("bankAbroadAccountName2")) Then bankAbroadInfo_AccountName2 = dbPages.dbtextShow(dr("bankAbroadAccountName2").ToString)
                If Not IsDBNull(dr("bankAbroadAccountNumber2")) Then bankAbroadInfo_AccountNumber2 = dbPages.dbtextShow(dr("bankAbroadAccountNumber2"))
                If Not IsDBNull(dr("bankAbroadBankName2")) Then bankAbroadInfo_BankName2 = dbPages.dbtextShow(dr("bankAbroadBankName2").ToString)
                If Not IsDBNull(dr("bankAbroadBankAddress2")) Then bankAbroadInfo_BankAddress2 = dbPages.dbtextShow(dr("bankAbroadBankAddress2"))
                If Not IsDBNull(dr("bankAbroadBankAddressSecond2")) Then bankAbroadInfo_BankAddressSecond2 = dbPages.dbtextShow(dr("bankAbroadBankAddressSecond2"))
                If Not IsDBNull(dr("bankAbroadBankAddressCity2")) Then bankAbroadInfo_BankAddressCity2 = dbPages.dbtextShow(dr("bankAbroadBankAddressCity2"))
                If Not IsDBNull(dr("bankAbroadBankAddressState2")) Then bankAbroadInfo_BankAddressState2 = dbPages.dbtextShow(dr("bankAbroadBankAddressState2"))
                If Not IsDBNull(dr("bankAbroadBankAddressZip2")) Then bankAbroadInfo_BankAddressZip2 = dbPages.dbtextShow(dr("bankAbroadBankAddressZip2"))
                If Not IsDBNull(dr("bankAbroadBankAddressCountryName2")) Then bankAbroadInfo_BankAddressCountry2 = dbPages.dbtextShow(dr("bankAbroadBankAddressCountryName2")).ToString
                If Not IsDBNull(dr("bankAbroadSwiftNumber2")) Then bankAbroadInfo_SwiftNumber2 = dbPages.dbtextShow(dr("bankAbroadSwiftNumber2"))
                If Not IsDBNull(dr("bankAbroadIBAN2")) Then bankAbroadInfo_IBAN2 = dbPages.dbtextShow(dr("bankAbroadIBAN2"))
                If Not IsDBNull(dr("bankAbroadABA2")) Then bankAbroadInfo_ABA2 = dbPages.dbtextShow(dr("bankAbroadABA2"))
                If Not IsDBNull(dr("bankAbroadSortCode2")) Then bankAbroadInfo_SortCode2 = dbPages.dbtextShow(dr("bankAbroadSortCode2"))
            End If
            dr.Close()
        End If

		phIsraeliBank.Visible = (WebUtils.CurrentLanguageShort = "he" And Not String.IsNullOrEmpty(bankIsraelInfo_PayeeName))
		phAbroadBank.Visible = (bankAbroadInfo_AccountName <> "")
		phCorrespondingBank.Visible = (bankAbroadInfo_AccountName2 <> "")
		Page.DataBind()

        If merchantBeneficiaryID Is Nothing Then
            ' self pay
            litProfileTitle.Text = Infrastructure.GlobalData.GetText(GlobalDataGroup.BeneficiaryProfileType, WebUtils.CurrentLanguage, 0)
            litDataHeading.Text = GetLocalResourceObject("DataDisplayHeading").ToString().Replace("%IDENTITY%", WebUtils.CurrentDomain.BrandName)
        Else
            litProfileTitle.Text = basicInfo_costumerName
            litDataHeading.Text = Infrastructure.GlobalData.GetText(GlobalDataGroup.BeneficiaryProfileType, WebUtils.CurrentLanguage, Request("ProfileType"))
            litDataHeading.Text &= ", " & GetLocalResourceObject("BeneficiaryNumber").ToString & ": "

            If merchantBeneficiaryType = MerchantBeneficiaryProfileType.Payoneer Then
                litDataHeading.Text &= merchantBeneficiaryID.ToString()
            Else
                litDataHeading.Text &= "<a href=""BeneficiaryAddUpdate.aspx?CompanyMakePaymentsProfiles_id=" & merchantBeneficiaryID & """>" & merchantBeneficiaryID & "</a>"
            End If
        End If

        If merchantBeneficiaryType = MerchantBeneficiaryProfileType.MerchantCard Then sSQLWhere = " AND paymentType = 0"
        If merchantBeneficiaryType <> MerchantBeneficiaryProfileType.MerchantCard Then sSQLWhere = " AND CompanyMakePaymentsProfiles_id = " & merchantBeneficiaryID
        sSQL = "SELECT COUNT(CompanyMakePaymentsRequests_id) AS NumOfRequests FROM tblCompanyMakePaymentsRequests WHERE company_id=" & Merchant.ID & sSQLWhere
        'Logger.Log(sSQL)
        dr = dbPages.ExecReader(sSQL)
        If dr.Read() Then
            If Int(dr("NumOfRequests")) = 1 Then
                litOrderBeneficiary.Text = GetLocalResourceObject("OneOrderBeneficiary")
            ElseIf Int(dr("NumOfRequests")) > 1 Then
                litOrderBeneficiary.Text = GetLocalResourceObject("MultipleOrderBeneficiary")
            Else
                litOrderBeneficiary.Text = GetLocalResourceObject("NoOrderBeneficiary")
            End If
        End If
        dr.Close()

        bIsAvailableBalance = False
        Dim currencies As Dictionary(Of Integer, Bll.Currency) = Bll.Currency.Cache.ToDictionary(Function(k) k.ID)
        For Each currentCurrency As KeyValuePair(Of Integer, Bll.Currency) In currencies
            If AvailableBalance.ContainsKey(currentCurrency.Value.ID) Then
                If AvailableBalance(currentCurrency.Value.ID) > 0 Then bIsAvailableBalance = True
            End If
        Next
        litNoBalance.Text = IIf(bIsAvailableBalance, String.Empty, "<tr><td></td><td style=""color:maroon;""><br />" & GetLocalResourceObject("BalanceTooLow").ToString & "</td></tr>")
        If bIsAvailableBalance Then
            litLimits.Text = "<table class=""detalis-table"">"
            litLimits.Text &= "<tr>"
            litLimits.Text &= "<th>" & GetLocalResourceObject("MinimumAmount") & "</th>"
            litLimits.Text &= "<th>" & GetLocalResourceObject("MaximumAmount") & "</th>"
            litLimits.Text &= "<th>" & GetLocalResourceObject("FeeAmount") & "</th>"
            litLimits.Text &= "</tr>"

            Dim merchantCurrencySettings As Dictionary(Of Integer, Settlements.MerchantSettings) = Settlements.MerchantSettings.LoadForMerchant(Merchant.ID)
            nCurrencyFirst = 1 ' hebrew Or english
            For Each currentCurrency As System.Collections.Generic.KeyValuePair(Of Integer, Bll.Currency) In currencies
                If AvailableBalance.ContainsKey(currentCurrency.Value.ID) Then
                    If AvailableBalance(currentCurrency.Value.ID) > 0 Then
                        Dim currencyFee As Settlements.MerchantSettings = Nothing
                        i = currentCurrency.Value.ID
                        If merchantCurrencySettings.ContainsKey(currentCurrency.Value.ID) Then currencyFee = merchantCurrencySettings(i)
                        'If ddPaymentCurrency.SelectedIndex = 0 Then ddPaymentCurrency.SelectedValue = i
                        litLimits.Text &= "<tr>"
                        litLimits.Text &= "<td valign=""top"">"
                        If (currencyFee IsNot Nothing) Then litLimits.Text &= dbPages.FormatCurr(i, currencyFee.MinPayoutAmount) Else litLimits.Text &= dbPages.FormatCurr(i, 0)
                        litLimits.Text &= "</td>"
                        litLimits.Text &= "<td valign=""top"">" & dbPages.FormatCurr(i, AvailableBalance(i)) & "</td>"
                        litLimits.Text &= "<td valign=""top"" style=""white-space: nowrap;"">"
                        If (currencyFee IsNot Nothing) Then litLimits.Text &= dbPages.FormatCurr(i, currencyFee.WireFee) & " + " & currencyFee.WireFeePercent.ToString("0.00") & "%"
                        litLimits.Text &= "</td>"
                        litLimits.Text &= "</tr>"
                    End If
                End If
            Next
            litLimits.Text &= "</table>"
        Else
            litLimits.Text = String.Empty
        End If
    End Sub

    Protected Sub BtnSubmit_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim amount As Decimal = dbPages.TestVar(Replace(paymentAmount.Text, ",", ""), 0D, -1D, 0D)
        Dim merchantCurrencySettings As Dictionary(Of Integer, Settlements.MerchantSettings) = Settlements.MerchantSettings.LoadForMerchant(Merchant.ID)
        Dim merchantBeneficiaryID As Nullable(Of Integer) = Request("CompanyMakePaymentsProfiles_id").ToNullableInt32()
        Dim merchantBeneficiaryType As Nullable(Of MerchantBeneficiaryProfileType) = Request("ProfileType").ToNullableEnumByValue(Of MerchantBeneficiaryProfileType)()
        Dim bDone As Boolean = False
        Dim currencyID = ddPaymentCurrency.Value.ToNullableInt().GetValueOrDefault(-1)
        If merchantBeneficiaryType = MerchantBeneficiaryProfileType.Payoneer Then
            If fuConfirmation.PostedFile.ContentLength = 0 Then
                litInsertText.Text = "<span style=""color:maroon;"">" & GetLocalResourceObject("OrderNotAccepted") & "<br />" & GetLocalResourceObject("NoFileUploaded") & "</span><br />"
                Return
            End If
        End If

        If Not AvailableBalance.ContainsKey(currencyID) Then
            litInsertText.Text = "<span style=""color:maroon;"">" & GetLocalResourceObject("OrderNotAccepted") & "<br />" & GetLocalResourceObject("InvalidCurrency") & "</span><br />"
            Return
        End If

        If wcDateRangePicker.FromDate.GetValueOrDefault().Date < DateTime.Now.Date Then
            litInsertText.Text = "<span style=""color:maroon;"">" & GetLocalResourceObject("OrderNotAccepted") & "<br />" & GetLocalResourceObject("DateTooEarly") & "</span><br />"
            Return
        End If

        Dim minPayout As Decimal = 0
        If ddPaymentCurrency.Value <> "" Then
            If merchantCurrencySettings.ContainsKey(dbPages.TestVar(currencyID, 0, -1, 0)) Then minPayout = merchantCurrencySettings(dbPages.TestVar(currencyID, 0, -1, 0)).MinPayoutAmount
            If amount - minPayout < 0 Then
                litInsertText.Text = "<span style=""color:maroon;"">" & GetLocalResourceObject("OrderNotAccepted") & "<br />" & GetLocalResourceObject("AmountTooLow") & "</span><br />"
                Return
            End If
        End If

        Dim availableBalanceTmp As Decimal = AvailableBalance(currencyID)
        If System.Math.Round(amount, 2) > System.Math.Round(availableBalanceTmp, 2) Then
            litInsertText.Text = "<span style=""color:maroon;"">" & GetLocalResourceObject("OrderNotAccepted") & "<br />" & GetLocalResourceObject("AmountTooHigh") & "</span><br />"
            Return
        End If

        Dim nExchangeRate As String = dbPages.ConvertCurrencyRate(currencyID, CommonTypes.Currency.ILS)
        sSQL = "SET NOCOUNT ON;INSERT tblCompanyMakePaymentsRequests(CompanyMakePaymentsProfiles_id, Company_id," &
        " paymentType, paymentDate, paymentAmount, paymentCurrency, paymentExchangeRate, paymentMerchantComment," &
        " bankIsraelInfo_PayeeName, bankIsraelInfo_CompanyLegalNumber, bankIsraelInfo_personalIdNumber, " &
        " bankIsraelInfo_bankBranch, bankIsraelInfo_AccountNumber, bankIsraelInfo_PaymentMethod, bankIsraelInfo_BankCode)" &
        " SELECT " & IIf(merchantBeneficiaryID Is Nothing, "NULL", merchantBeneficiaryID) & ", " & Merchant.ID & ", " & merchantBeneficiaryType & "," &
        " '" & wcDateRangePicker.FromDate.Value.ToString("yyyyMMdd") & "', " & amount & ", " & currencyID & "," &
        " " & nExchangeRate & ", Left('" & paymentMerchantComment.Text.ToSql(True) & "', 250), "

        If merchantBeneficiaryType = MerchantBeneficiaryProfileType.MerchantCard Then
            'Get data from company table
            sSQL = sSQL & "Left(PaymentPayeeName, 80), Left(CompanyLegalNumber, 80), Left(IDNumber, 80)," &
            " Left(PaymentBranch, 5), Left(PaymentAccount, 80), Left(PaymentMethod, 80), IsNull(PaymentBank, 0)" &
            " FROM tblCompany WHERE ID=" & Merchant.ID
        Else
            'Get data from card
            sSQL = sSQL & "Left(bankIsraelInfo_PayeeName, 80), Left(bankIsraelInfo_CompanyLegalNumber, 80)," &
            " Left(bankIsraelInfo_personalIdNumber, 80), Left(bankIsraelInfo_bankBranch, 5)," &
            " Left(bankIsraelInfo_AccountNumber, 80), Left(bankIsraelInfo_PaymentMethod, 80), IsNull(bankIsraelInfo_BankCode, 0)" &
            " FROM tblCompanyMakePaymentsProfiles WHERE CompanyMakePaymentsProfiles_id=" & Request("CompanyMakePaymentsProfiles_id").ToInt32(0)
        End If
        sSQL = sSQL & ";SELECT @@IDENTITY AS NewID;SET NOCOUNT OFF"

        dr = dbPages.ExecReader(sSQL)
        If dr.Read() Then merchantBeneficiaryID = dr("NewID").ToString().ToNullableInt32()
        dr.Close()
        'Add a transaction with this Payments request data
        sIP = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
        If sIP = "" Then
            sIP = Request.ServerVariables("REMOTE_ADDR")
        End If

        ' save confirmation file
        Dim confiramtionFile As HttpPostedFile = fuConfirmation.PostedFile
        Dim confirmationFileName As String = "NULL"
        If confiramtionFile IsNot Nothing Then
            Dim confirmationFileFullName As String = Infrastructure.Domain.GetUniqueFileName(System.IO.Path.Combine(Domain.MapPrivateDataPath("Wires"), confiramtionFile.FileName))
            confiramtionFile.SaveAs(confirmationFileFullName)
            confirmationFileName = "'" + System.IO.Path.GetFileName(confirmationFileFullName) + "'"
        End If

        'Add this Payments requests to wire table
        sSQL = "INSERT tblWireMoney(Company_id, WireSourceTbl_id, WireType, WireDate, WireAmount, WireCurrency, WireExchangeRate, ConfirmationFileName, " &
        " wireIDnumber, wireCompanyName, wireCompanyLegalName, wireCompanyLegalNumber," &
        " wirePaymentMethod, wirePaymentPayeeName, wirePaymentAccount, wirePaymentBank, wirePaymentBranch," &
        " wirePaymentAbroadAccountName," &
        " wirePaymentAbroadAccountNumber," &
        " wirePaymentAbroadBankName," &
        " wirePaymentAbroadBankAddress," &
        " wirePaymentAbroadBankAddressSecond," &
        " wirePaymentAbroadBankAddressCity," &
        " wirePaymentAbroadBankAddressState," &
        " wirePaymentAbroadBankAddressZip," &
        " wirePaymentAbroadBankAddressCountry," &
        " wirePaymentAbroadSwiftNumber," &
        " wirePaymentAbroadIBAN," &
        " wirePaymentAbroadABA," &
        " wirePaymentAbroadSortCode," &
        " wirePaymentAbroadAccountName2," &
        " wirePaymentAbroadAccountNumber2," &
        " wirePaymentAbroadBankName2," &
        " wirePaymentAbroadBankAddress2," &
        " wirePaymentAbroadBankAddressSecond2," &
        " wirePaymentAbroadBankAddressCity2," &
        " wirePaymentAbroadBankAddressState2," &
        " wirePaymentAbroadBankAddressZip2," &
        " wirePaymentAbroadBankAddressCountry2," &
        " wirePaymentAbroadSwiftNumber2," &
        " wirePaymentAbroadIBAN2," &
        " wirePaymentAbroadABA2," &
        " wirePaymentAbroadSortCode2," &
        " isShow)" &
        " SELECT " & Merchant.ID & ", " & merchantBeneficiaryID & ", 2, '" & wcDateRangePicker.FromDate.Value.ToString("yyyyMMdd") & "'," &
        " " & amount & ", " & currencyID & ", " & nExchangeRate & ", " & confirmationFileName & ", "
        If merchantBeneficiaryType = MerchantBeneficiaryProfileType.MerchantCard Then
            'Get data from company table
            sSQL = sSQL & " IDNumber, PaymentPayeeName, PaymentPayeeName, CompanyLegalNumber,"
            sSQL = sSQL & " PaymentMethod, PaymentPayeeName, PaymentAccount, PaymentBank, PaymentBranch,"
            sSQL = sSQL & " PaymentAbroadAccountName,"
            sSQL = sSQL & " PaymentAbroadAccountNumber,"
            sSQL = sSQL & " PaymentAbroadBankName,"
            sSQL = sSQL & " PaymentAbroadBankAddress,"
            sSQL = sSQL & " PaymentAbroadBankAddressSecond,"
            sSQL = sSQL & " PaymentAbroadBankAddressCity,"
            sSQL = sSQL & " PaymentAbroadBankAddressState,"
            sSQL = sSQL & " PaymentAbroadBankAddressZip,"
            sSQL = sSQL & " PaymentAbroadBankAddressCountry,"
            sSQL = sSQL & " PaymentAbroadSwiftNumber,"
            sSQL = sSQL & " PaymentAbroadIBAN,"
            sSQL = sSQL & " PaymentAbroadABA,"
            sSQL = sSQL & " PaymentAbroadSortCode,"
            sSQL = sSQL & " PaymentAbroadAccountName2,"
            sSQL = sSQL & " PaymentAbroadAccountNumber2,"
            sSQL = sSQL & " PaymentAbroadBankName2,"
            sSQL = sSQL & " PaymentAbroadBankAddress2,"
            sSQL = sSQL & " PaymentAbroadBankAddressSecond2,"
            sSQL = sSQL & " PaymentAbroadBankAddressCity2,"
            sSQL = sSQL & " PaymentAbroadBankAddressState2,"
            sSQL = sSQL & " PaymentAbroadBankAddressZip2,"
            sSQL = sSQL & " PaymentAbroadBankAddressCountry2,"
            sSQL = sSQL & " PaymentAbroadSwiftNumber2,"
            sSQL = sSQL & " PaymentAbroadIBAN2,"
            sSQL = sSQL & " PaymentAbroadABA2,"
            sSQL = sSQL & " PaymentAbroadSortCode2,"
            sSQL = sSQL & " 1 FROM tblCompany WHERE id=" & Merchant.ID
        Else
            'Get data from card
            sSQL = sSQL & " bankIsraelInfo_personalIdNumber, bankIsraelInfo_PayeeName, bankIsraelInfo_PayeeName, bankIsraelInfo_CompanyLegalNumber,"
            sSQL = sSQL & " bankIsraelInfo_PaymentMethod, bankIsraelInfo_PayeeName, bankIsraelInfo_AccountNumber, bankIsraelInfo_BankCode, bankIsraelInfo_bankBranch,"
            sSQL = sSQL & " bankAbroadAccountName,"
            sSQL = sSQL & " bankAbroadAccountNumber,"
            sSQL = sSQL & " bankAbroadBankName,"
            sSQL = sSQL & " bankAbroadBankAddress,"
            sSQL = sSQL & " bankAbroadBankAddressSecond,"
            sSQL = sSQL & " bankAbroadBankAddressCity,"
            sSQL = sSQL & " bankAbroadBankAddressState,"
            sSQL = sSQL & " bankAbroadBankAddressZip,"
            sSQL = sSQL & " bankAbroadBankAddressCountry,"
            sSQL = sSQL & " bankAbroadSwiftNumber,"
            sSQL = sSQL & " bankAbroadIBAN,"
            sSQL = sSQL & " bankAbroadABA,"
            sSQL = sSQL & " bankAbroadSortCode,"
            sSQL = sSQL & " bankAbroadAccountName2,"
            sSQL = sSQL & " bankAbroadAccountNumber2,"
            sSQL = sSQL & " bankAbroadBankName2,"
            sSQL = sSQL & " bankAbroadBankAddress2,"
            sSQL = sSQL & " bankAbroadBankAddressSecond2,"
            sSQL = sSQL & " bankAbroadBankAddressCity2,"
            sSQL = sSQL & " bankAbroadBankAddressState2,"
            sSQL = sSQL & " bankAbroadBankAddressZip2,"
            sSQL = sSQL & " bankAbroadBankAddressCountry2,"
            sSQL = sSQL & " bankAbroadSwiftNumber2,"
            sSQL = sSQL & " bankAbroadIBAN2,"
            sSQL = sSQL & " bankAbroadABA2,"
            sSQL = sSQL & " bankAbroadSortCode2,"
            sSQL = sSQL & " 1 FROM tblCompanyMakePaymentsProfiles WHERE CompanyMakePaymentsProfiles_id=" & Request("CompanyMakePaymentsProfiles_id").ToInt32(0)
        End If

        'Response.Write(sSQL) : Response.End()

        dbPages.ExecSql(sSQL)
        Dim payyName = dbPages.ExecScalar("Select bankIsraelInfo_PayeeName From tblCompanyMakePaymentsRequests Where CompanyMakePaymentsRequests_id =" & merchantBeneficiaryID)

        'Add to balance table (status pending)
        Dim balanceText As String = "Beneficiary Payment #" & merchantBeneficiaryID & " - " & payyName
        Dim bl = Netpay.Bll.Accounts.Balance.Create(Account.AccountID, Bll.Currency.Get(currencyID).IsoCode, -amount, balanceText, Wires.Balance.BALANCE_SOURCE_PAYMENTREQUEST, merchantBeneficiaryID, True)
        Call Netpay.Bll.Accounts.Balance.Create(Account.AccountID, Bll.Currency.Get(currencyID).IsoCode, -Wires.Balance.GetWireFee(Merchant.ID, Bll.Currency.Get(currencyID).IsoCode, amount), balanceText, Accounts.Balance.SOURCE_WITHDRAWAL_FEE, bl.ID, False)
        litInsertText.Text = "<span style=""color:green;"">" & GetLocalResourceObject("OrderAccepted").ToString & "</span><br />"
        bDone = True
        BtnSubmit.Visible = False
    End Sub

    'Sub BalanceInsert(ByVal companyID As Integer, ByVal sourceTableID As Integer, ByVal sourceType As String, ByVal sourceInfo As String, ByVal amount As String, ByVal currency As Integer, ByVal status As String, ByVal comment As String, ByVal isAddFee As String)
    '    Dim sSQL As String, currencyName As String, sSourceInfoTmp As String, MakePaymentsFee As String
    '    sSQL = "INSERT INTO tblCompanyBalance(company_id, sourceTbl_id, sourceType, sourceInfo, amount, currency, status, comment) VALUES" & _
    '    " (" & companyID & ", " & sourceTableID & ", " & sourceType & ", '" & sourceInfo.ToSql(True) & "', " & amount & "," & _
    '    " " & currency & "," & status & ", '" & comment & "')"
    '    dbPages.ExecSql(sSQL)

    '    If CBool(isAddFee) Then
    '        currencyName = IIf(currency = 0, "Shekel", "Dollar")
    '        sSourceInfoTmp = "Bank transfer"    'IIF(fCurrency = 0, "העברה בנקאית", "Bank transfer")
    '        sSQL = "SELECT MakePaymentsFee" & currencyName & " AS MakePaymentsFee, MakePaymentsFixFee FROM tblCompany WHERE id=" & companyID
    '        dr = dbPages.ExecReader(sSQL)
    '        If Not dr.Read() Then
    '            MakePaymentsFee = dr("MakePaymentsFee") - amount * (dr("MakePaymentsFixFee") / 100)
    '            If MakePaymentsFee > 0 Then
    '                'Insert fee line into balance
    '                sSQL = "INSERT INTO tblCompanyBalance(company_id, sourceTbl_id, sourceType, sourceInfo, amount, currency, status, comment)" & _
    '                " VALUES (" & companyID & ", 0, 4, '" & sSourceInfoTmp & "', " & -MakePaymentsFee & "," & currency & ", 0, '')"
    '                dbPages.ExecSql(sSQL)
    '            End If
    '        End If
    '        dr.Close()
    '    End If
    'End Sub

    Protected ReadOnly Property AvailableBalance() As Dictionary(Of Integer, Decimal)
        Get
            If _availableBalance Is Nothing Then
                Dim merchantBalance = Bll.Accounts.Balance.GetStatus(Account.AccountID)
                Dim merchantCurrencySettings As Dictionary(Of Integer, Settlements.MerchantSettings) = Settlements.MerchantSettings.LoadForMerchant(Merchant.ID)
                _availableBalance = New Dictionary(Of Integer, Decimal)
                For Each currentBalance In merchantBalance
                    Dim calculatedBalance As Decimal = 0
                    Dim currencyId = Bll.Currency.Get(currentBalance.CurrencyIso).ID
                    Dim merchantCurrencySetting As Settlements.MerchantSettings = Nothing
                    If merchantCurrencySettings.ContainsKey(currencyId) Then merchantCurrencySetting = merchantCurrencySettings(currencyId)
                    If merchantCurrencySetting Is Nothing Then calculatedBalance = currentBalance.Expected _
                    Else calculatedBalance = (currentBalance.Expected / (1 + merchantCurrencySetting.WireFeePercent / 100)) - merchantCurrencySetting.WireFee
                    If calculatedBalance < 0 Then calculatedBalance = 0
                    If calculatedBalance > 0 Then _availableBalance.Add(currencyId, calculatedBalance)
                Next
            End If

            Return _availableBalance
        End Get
    End Property

    Function ProfileItem(ByVal sValue As String, ByVal sTitle As String, Optional ByVal bResource As Boolean = True, Optional ByVal sGlobalResourceClass As String = Nothing) As String
        If String.IsNullOrEmpty(sValue) Then Return String.Empty
        If bResource Then
            If String.IsNullOrEmpty(sGlobalResourceClass) Then
                sTitle = GetLocalResourceObject(sTitle).ToString
            Else
                sTitle = GetGlobalResourceObject(sGlobalResourceClass, sTitle).ToString
            End If
        End If
        If Not String.IsNullOrEmpty(sTitle) Then sTitle &= ":"
        Return "<tr><td class=""txt11""><span class=""DataHeading"">" & sTitle & "</span> " & sValue & "</td></tr>"
    End Function

    Function ProfileHead(ByVal sTitle As String, Optional ByVal bResource As Boolean = True, Optional ByVal sGlobalResourceClass As String = Nothing) As String
        If String.IsNullOrEmpty(sTitle) Then Return String.Empty
        If bResource Then
            If String.IsNullOrEmpty(sGlobalResourceClass) Then
                sTitle = GetLocalResourceObject(sTitle).ToString
            Else
                sTitle = GetGlobalResourceObject(sGlobalResourceClass, sTitle).ToString
            End If
        End If
        Return "<tr><td class=""MainHead"" style=""text-decoration:underline;""><br />" & sTitle & "</td></tr>"
    End Function

    Function ProfileAddressThird(ByVal sCity As String, ByVal sState As String, ByVal sZip As String) As String
        Dim sAddress As String = sCity
        If sState <> "none" And Not String.IsNullOrEmpty(sState) Then sAddress &= ", " & sState
        If Not String.IsNullOrEmpty(sZip) Then sAddress &= ", " & sZip
        Return sAddress
    End Function
End Class
