﻿Imports Netpay.Bll
Imports Netpay.Web

Partial Class Website_StoreMethoodPage_Wizard
    Inherits MasteredPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
        merchantID.Text = Merchant.Number
        If Not IsPostBack Then
            Dim options As Merchants.Hosted.PaymentPage = Merchants.Hosted.PaymentPage.Load(Bll.Merchants.Merchant.Current.ID)
            If options Is Nothing Then
                phMD5Required.Visible = True
                phOptionalMD5.Visible = False
            Else
                phMD5Required.Visible = Not options.IsSignatureOptional
                phOptionalMD5.Visible = options.IsSignatureOptional
            End If
            btnGenerate_Click(Nothing, Nothing)
        End If
    End Sub

    Protected Function GetOpenString() As String
        Dim sb As System.Text.StringBuilder = New System.Text.StringBuilder()
        sb.Append(WebUtils.CurrentDomain.ProcessV2Url & "PaymentMethodStorage/")
        sb.Append("?merchantID=" + HttpUtility.UrlEncode(merchantID.Text))
        sb.Append("&url_redirect=" + HttpUtility.UrlEncode(url_redirect.Text))
        sb.Append("&url_notify=" + HttpUtility.UrlEncode(url_notify.Text))
        sb.Append("&trans_comment=" + HttpUtility.UrlEncode(trans_comment.Text))
        'If trans_installments.SelectedItem IsNot Nothing Then sb.Append("&trans_installments=" + HttpUtility.UrlEncode(trans_installments.SelectedValue))
        sb.Append("&disp_payFor=" + HttpUtility.UrlEncode(disp_payFor.Text))
        If disp_lng.SelectedItem IsNot Nothing Then sb.Append("&disp_lng=" + HttpUtility.UrlEncode(disp_lng.SelectedValue))
        If Not String.IsNullOrEmpty(client_fullName.Text) Then sb.Append("&client_fullName=" + HttpUtility.UrlEncode(client_fullName.Text))
        If Not String.IsNullOrEmpty(client_email.Text) Then sb.Append("&client_email=" + HttpUtility.UrlEncode(client_email.Text))
        If Not String.IsNullOrEmpty(client_phoneNum.Text) Then sb.Append("&client_phoneNum=" + HttpUtility.UrlEncode(client_phoneNum.Text))
        If Not String.IsNullOrEmpty(client_idNum.Text) Then sb.Append("&client_idNum=" + HttpUtility.UrlEncode(client_idNum.Text))
        If Not String.IsNullOrEmpty(client_billaddress1.Text) Then sb.Append("&client_billaddress1=" + HttpUtility.UrlEncode(client_billaddress1.Text))
        If Not String.IsNullOrEmpty(client_billaddress2.Text) Then sb.Append("&client_billaddress2=" + HttpUtility.UrlEncode(client_billaddress2.Text))
        If Not String.IsNullOrEmpty(client_billcity.Text) Then sb.Append("&client_billcity=" + HttpUtility.UrlEncode(client_billcity.Text))
        If Not String.IsNullOrEmpty(client_billzipcode.Text) Then sb.Append("&client_billzipcode=" + HttpUtility.UrlEncode(client_billzipcode.Text))
        If Not String.IsNullOrEmpty(client_billstate.Text) Then sb.Append("&client_billstate=" + HttpUtility.UrlEncode(client_billstate.Text))
        If Not String.IsNullOrEmpty(client_billcountry.Text) Then sb.Append("&client_billcountry=" + HttpUtility.UrlEncode(client_billcountry.Text))
        If (Not String.IsNullOrEmpty(merchantID.Text) And (phMD5Required.Visible Or (phOptionalMD5.Visible And cbMD5Optional.Checked))) Then
            Dim merchant As Merchants.Merchant = Bll.Merchants.Merchant.Load(merchantID.Text)
            If (merchant IsNot Nothing) Then
                If merchant.HashKey IsNot Nothing Then
                    If merchant.HashKey.Trim() <> "" Then
                        Dim sign As String = merchantID.Text + merchant.HashKey
                        Dim md As System.Security.Cryptography.MD5 = System.Security.Cryptography.MD5.Create()
                        sign = System.Convert.ToBase64String(md.ComputeHash(System.Text.Encoding.Default.GetBytes(sign.ToString())))
                        sb.Append("&signature=" + HttpUtility.UrlEncode(sign))
                    End If
                End If
            End If
        End If
        If (Request("DebugTest") = "1") Then sb.Append("&DebugTest=1")
        Return sb.ToString()
    End Function

    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As EventArgs)
        txQuery.Text = GetOpenString()
        txQuery.Style.Remove("display")
        OpenScript.Visible = False
    End Sub

    Protected Sub btnOpenPage_Click(ByVal sender As Object, ByVal e As EventArgs)
        OpenScript.Text = "<script type=""text/javascript"">window.open('" & GetOpenString() & "', 'fraPay', 'scrollbars=1, width=700, height=800, resizable=1, Status=1, top=200, left=250');</" & "script>"
        OpenScript.Visible = True
    End Sub
End Class
