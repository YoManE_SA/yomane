﻿
Partial Class Website_Logout
    Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Netpay.Web.WebUtils.Logout()
        Response.Redirect("Login.aspx?" & Request.QueryString.ToString())
    End Sub
End Class
