﻿Imports Netpay.Web.Controls

Partial Class Website_UpdateAccount
	Inherits MasteredPage

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
		If Not IsPostBack Then		
			LoadMerchantData()
		End If
	End Sub
	
	Protected Sub LoadMerchantData()
        ddMerchantCountry.ISOCode = Merchant.BusinessAddress.CountryISOCode
        literalMerchantName.Text = Merchant.LegalName
        txtMerchantCity.Text = Merchant.BusinessAddress.City
		txtMerchantAddress.Text = Merchant.BusinessAddress.AddressLine1
		txtMerchantZIP.Text = Merchant.BusinessAddress.PostalCode
		txtUrl.Text = Merchant.Url
		txtMerchantPhone.Text = Merchant.Phone
		txtMerchantFax.Text = Merchant.Fax
		txtMerchantSupportEmail.Text = Merchant.SupportEmail
		txtMerchantSupportPhone.Text = Merchant.SupportPhone
		txtFirstName.Text = Merchant.ContactFirstName
		txtLastName.Text = Merchant.ContactLastName
		txtContactAddress.Text = Merchant.PersonalAddress.AddressLine1
		txtContactCity.Text = Merchant.PersonalAddress.City
		txtContactZipcode.Text = Merchant.PersonalAddress.PostalCode
        ddContactCountry.ISOCode = Merchant.PersonalAddress.CountryISOCode
		txtContactMobile.Text = Merchant.ContactMobilePhone
		txtContactPhone.Text = Merchant.ContactPhone
		txtEmail.Text = Merchant.EmailAddress
		txtUserName.Text = Merchant.UserNAme
	End Sub
	
	Sub UpdateMerchant(sender As Object, e As EventArgs) Handles btnUpdate.Click  
		lblError.Visible = false
		lblSuccess.Visible = false
		If txtMerchantAddress.Text.Trim() = "" or txtMerchantCity.Text.Trim() = "" or txtMerchantPhone.Text.Trim() = "" or txtFirstName.Text.Trim() = "" or txtLastName.Text.Trim() = "" or txtUserName.Text.Trim() = "" or txtEmail.Text.Trim() = ""
			lblError.Text = GetLocalResourceObject("lblError")
			lblError.Visible = True
			Return
		End If
		Dim needEmailUpdate As Boolean = Merchant.Login.EmailAddress <> txtEmail.Text

		Merchant.BusinessAddress.CountryISOCode = ddMerchantCountry.ISOCode
		Merchant.BusinessAddress.AddressLine1 = txtMerchantAddress.Text
		Merchant.BusinessAddress.City = txtMerchantCity.Text
		Merchant.BusinessAddress.PostalCode = txtMerchantZIP.Text
		Merchant.Url = txtUrl.Text
		Merchant.Phone = txtMerchantPhone.Text
		Merchant.Fax = txtMerchantFax.Text
		Merchant.SupportEmail = txtMerchantSupportEmail.Text
		Merchant.SupportPhone = txtMerchantSupportPhone.Text
		Merchant.ContactFirstName = txtFirstName.Text
		Merchant.ContactLastName = txtLastName.Text
		Merchant.PersonalAddress.AddressLine1 = txtContactAddress.Text
		Merchant.PersonalAddress.City = txtContactCity.Text
		Merchant.PersonalAddress.PostalCode = txtContactZipcode.Text
		Merchant.PersonalAddress.CountryISOCode = ddContactCountry.ISOCode
		Merchant.ContactMobilePhone = txtContactMobile.Text
		Merchant.ContactPhone = txtContactPhone.Text
		Merchant.EmailAddress = txtEmail.Text
		Merchant.UserName = txtUserName.Text
		Merchant.Login.EmailAddress = txtEmail.Text
		Merchant.Login.UserName = txtUserName.Text 

        Merchant.Save()
		If needEmailUpdate Then Bll.Accounts.LimitedLogin.SetEmailForAccount(Merchant.AccountID, txtEmail.Text)


		lblSuccess.Text = GetLocalResourceObject("lblSuccess")
		lblSuccess.Visible = true
		LoadMerchantData()
	End Sub
End Class
