﻿<%@ Page Title="PageTitle" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master"
    AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_LimitedUserManagement"
    MaintainScrollPositionOnPostback="true" CodeBehind="LimitedUserManagement.aspx.vb" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <netpay:PageHelpBox Text="<%$Resources:HelpBox %>" runat="server" />
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <script type="text/javascript">
        function post(event) {
            var srcElement = event.srcElement ? event.srcElement : event.target;

            if (srcElement.tagName == "INPUT" && srcElement.type == "checkbox")
                __doPostBack("", "");
        }

        netpay.Blocker.enabled = false;
    </script>
    <asp:PlaceHolder ID="phResultMsg" runat="server" Visible="False"></asp:PlaceHolder>
    <div class="area-content">
        <asp:Label ID="lblResult" runat="server" />
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">
            <div class="section">
                <netpay:TabControl runat="server" ID="tcTabs" TabStyle="OrganicTabs" CssStyle="organicTabs">
                    <netpay:TabView runat="server" Text="<%$Resources:Addnewuser%>">
                        <div class="percent32 align-left margin-right-10">
                            <div>
                                <asp:Localize runat="server" Text="<%$Resources:MultiLang,UserName %>" /><%=Validation.Mandatory%>
                            </div>
                            <div>
                                <asp:TextBox ID="txtNewUserName" CssClass="percent100" runat="server" />
                            </div>

                        </div>
                        <div class="percent32 align-left margin-right-10">
                            <div>
                                <asp:Localize runat="server" Text="<%$Resources:MultiLang,Password%>" /><%=Validation.Mandatory%>
                                <netpay:PageItemHelp ID="pihPassword" runat="server" />
                            </div>
                            <div>
                                <asp:TextBox ID="txtNewPassword" CssClass="percent100" runat="server" TextMode="Password" />
                            </div>
                        </div>
                        <div class="percent32 align-left">
                            <div>
                                <asp:Localize runat="server" Text="<%$Resources:MultiLang,ValidatePassword%>" /><%=Validation.Mandatory%>
                            </div>
                            <div>
                                <asp:TextBox ID="txtNewPasswordConfirm" CssClass="percent100" runat="server" TextMode="Password" />
                            </div>

                        </div>
                        <div class="spacer"></div>
                        <div class="wrap-button-bar">
                            <asp:Button ID="btnCreate" Text="<%$Resources:MultiLang,Add%>" OnClick="CreateUser" CssClass="btn btn-cons-short btn-primary" runat="server" />
                        </div>
                    </netpay:TabView>
                    <netpay:TabView runat="server" Text="<%$Resources:Edituser%>">
                        <div class="line-height-35">
                            <asp:Label ID="Label1" Text="<%$Resources:ManageUser%>" runat="server" />
                            <asp:DropDownList ID="ddUsers" CssClass="percent33" AutoPostBack="true" runat="server" />
                        </div>
                        <asp:PlaceHolder ID="phUserDetails" Visible="false" runat="server">


                            <div class="margin-bottom-10 frame-border-option margin-top-10">

                                <asp:Localize ID="locChangePassword" Text="<%$Resources:SubHeader1%>" runat="server" />
                                <div class="frame-border-option-inline padding-10 margin-top-10">
                                    <div class="percent32 align-left margin-right-10">
                                        <div>
                                            <asp:Localize ID="Localize5" runat="server" Text="<%$Resources:MultiLang,Password%>" /><%=Validation.Mandatory%>
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtChangePassword" runat="server" CssClass="percent100" TextMode="Password" />&nbsp;
                                        </div>
                                    </div>
                                    <div class="percent32 align-left margin-right-10">
                                        <div>
                                            <asp:Localize ID="Localize6" runat="server" Text="<%$Resources:MultiLang,ValidatePassword%>" /><%=Validation.Mandatory%>
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtChangePasswordConfirm" runat="server" CssClass="percent100" TextMode="Password" />
                                        </div>
                                    </div>
                                    <div class="percent32 align-left">
                                        <div class="padding-22-top">
                                            <asp:Button ID="btnChangePassword" runat="server" Text="<%$Resources:MultiLang,Change%>" CssClass="btn btn-primary btn-cons-short" OnClick="ChangePassword" OnClientClick="netpay.Blocker.enabled=true" />
                                        </div>
                                    </div>
                                    <div class="spacer"></div>
                                </div>
                                <div class="alert-info margin-top-10">
                                    <asp:Localize ID="Localize7" runat="server" Text="<%$Resources:MultiLang,SignupPasswordText%>" />
                                    <ul>
                                        <li>
                                            <asp:Localize ID="Localize8" runat="server" Text="<%$Resources:MultiLang,SignupPasswordText1%>" /></li>
                                        <li>
                                            <asp:Localize ID="Localize9" runat="server" Text="<%$Resources:MultiLang,SignupPasswordText2%>" /></li>
                                        <li>
                                            <asp:Localize ID="Localize10" runat="server" Text="<%$Resources:MultiLang,SignupPasswordText3%>" /></li>
                                        <li>
                                            <asp:Localize ID="Localize11" runat="server" Text="<%$Resources:MultiLang,SignupPasswordText4%>" /></li>
                                        <li>
                                            <asp:Localize ID="Localize12" runat="server" Text="<%$Resources:MultiLang,SignupPasswordText5%>" /></li>
                                    </ul>
                                </div>
                            </div>




                            <div class="margin-bottom-10 frame-border-option margin-top-10 line-height-35">

                                <asp:Localize ID="Localize14" Text="<%$Resources:SubHeader2%>" runat="server" />
                                <div class="frame-border-option-inline padding-10">
                                    <ul class="group-list" id="SiteMapAccess">
                                        <asp:PlaceHolder runat="server" ID="phSiteMapAccess" />
                                     
                                    </ul>
                                </div>
                                <div><i class="fa fa-cog"></i> <asp:Localize Text="<%$Resources:Viewoptions%>" runat="server" /></div>

                                <ul class="group-list">
                                    <li>
                                        <asp:CheckBox ID="cbPrintSlipPermission" Text="<%$Resources:printTransactionSlip%>" runat="server" /></li>
                                    <li>
                                        <asp:CheckBox ID="cbRefundRequest" Text="<%$Resources:requestRefund%>" runat="server" /></li>
                                    <li>
                                        <asp:CheckBox ID="cbCreateRecurring" Text="<%$Resources:createRecurringFromTransaction%>" runat="server" /></li>
                                    <li>
                                        <asp:CheckBox ID="cdAllowInvoices" Text="<%$Resources:AllowInvoices%>" runat="server" /></li>
                                    <li>
                                        <asp:CheckBox ID="cbBlockCard" Text="<%$Resources:blockCard%>" runat="server" /></li>
                                    <li>
                                        <asp:CheckBox ID="cbShowTotals" Text="<%$Resources:ShowTotals%>" runat="server" /></li>
                                    <li>
                                        <asp:CheckBox ID="cbShowMainNotifications" Text="<%$Resources:ShowMainPageNotifications%>" runat="server" /></li>

                                </ul>
                            </div>


                            <div class="wrap-button-bar">
                                <asp:Button ID="btnSave" OnClick="Save_Click" Text="<%$Resources:MultiLang, ButtonSave%>" runat="server" CssClass="btn btn-cons-short btn-success" />
                            </div>

                        </asp:PlaceHolder>
                    </netpay:TabView>
                    <netpay:TabView runat="server" Text="<%$Resources:MultiLang, Deleteuser%>">
                        <div>
                            <asp:DropDownList ID="ddlDeleteSelectUser" CssClass="percent33" AutoPostBack="true" runat="server" OnSelectedIndexChanged="DeleteSelectUser_Change" />
                        </div>
                        <div class="margin-top-10">
                            <asp:Localize ID="locDeleteWarning" runat="server" Text="<%$Resources:DeleteWarning%>" Visible="false" />
                        </div>
                        <div class="wrap-button-bar">
                            <asp:Button ID="btnDeleteUser" Visible="false" Text="<%$Resources:MultiLang, Delete%>" CssClass="btn btn-cons-short btn-danger" OnClientClick="if (!confirm('Are you sure ?!')) return false;" runat="server" />
                        </div>
                    </netpay:TabView>
                </netpay:TabControl>
            </div>
        </div>
    </div>
</asp:Content>
