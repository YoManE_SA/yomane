﻿Imports System.Globalization
Imports System.Threading

Public Class OuterLogin
	Inherits System.Web.UI.Page

    Protected Overrides Sub InitializeCulture()
        Dim sendCulture As String = dbPages.TestVar(Request("Language"), -1, "en-us")
        Culture = sendCulture
        UICulture = sendCulture
        MyBase.InitializeCulture()
    End Sub

	Protected Overrides Sub OnLoad(e As System.EventArgs)
		Dim sourceCss As String = Request("Style")
		If String.IsNullOrEmpty(sourceCss) Then sourceCss = "NetpayIntl"
		phHeader.Controls.Add(New LiteralControl(String.Format("<link rel=""stylesheet"" type=""text/css"" href=""../OuterLoginStyles/{0}.css"" />", sourceCss)))
		phHeader.Controls.Add(New LiteralControl(String.Format("<style> body {{ direction:{0}; }} .loginbtn,.divLink{{ float:{1}; }}</style>", Netpay.Web.WebUtils.CurrentDirection, Netpay.Web.WebUtils.CurrentAlignment)))
		MyBase.OnLoad(e)
	End Sub

    Protected Sub LoginSuccess(ByVal sender As Object, ByVal e As System.EventArgs) Handles wcLogin.LoginSuccess
        Response.Write("<script>top.location.href='Default.aspx'</script>")
    End Sub
End Class