<%@ Page Title="" Language="VB" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_AskTransactionRefundPopup" Codebehind="AskTransactionRefundPopup.aspx.vb" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Refund Request For Transaction </title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1255" />
	<asp:Literal ID="litCSS" runat="server" />
</head>
<body onload="focus();" style="text-align:center;overflow:auto;padding:0 10px;">
	<form id="Form1" runat="server">
		<h1><asp:label ID="lblPermissions" runat="server" /> Transaction Management<span> - Refund Requests</span></h1>
		<div style="text-align:center;">
			<asp:MultiView ID="mvNewRequest" ActiveViewIndex="0" runat="server">
				<asp:View ID="View1" runat="server">
					<div class="bordered">
						Create New Refund Request:
						<div>
							&nbsp; Amount <asp:TextBox ID="txtNewAmount" CssClass="text short" runat="server" />
							&nbsp; Comment <asp:TextBox ID="txtNewComment" CssClass="text" runat="server" />
							<asp:Button ID="Button1" UseSubmitBehavior="false" CssClass="buttonWhite" runat="server" Text="Add" OnClick="AddNewRequest" />
						</div>
					</div>
				</asp:View>
				<asp:View ID="View2" runat="server">
					<div class="bordered">
						Create New Refund Request:
						<div>
							The refund request has been created successfully.
							<input type="button" class="buttonWhite" value="Close" onclick="top.close();" />
						</div>
					</div>
				</asp:View>
				<asp:View ID="View3" runat="server">
					<div class="bordered">
						Create New Refund Request:
						<div style="color:red;">
							The refund request was not created!
							<asp:Button ID="Button2" UseSubmitBehavior="false" CssClass="buttonWhite" runat="server" Text="Back" OnClick="ResetView" />
						</div>
					</div>
				</asp:View>
				<asp:View ID="View4" runat="server">
					<div class="bordered">
						Create New Refund Request:
						<div style="color:red;">
							Full refund is already requested.
						</div>
					</div>
				</asp:View>
			</asp:MultiView>
		</div>
		<asp:GridView ID="gvData" CssClass="grid" DataSourceID="dsData" runat="server" 
            Width="100%" AlternatingRowStyle-CssClass="gridRow2" BackColor="White" 
            BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" 
            ForeColor="Black" GridLines="Horizontal" >
<AlternatingRowStyle CssClass="gridRow2"></AlternatingRowStyle>
            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F7F7F7" />
            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
            <SortedDescendingCellStyle BackColor="#E5E5E5" />
            <SortedDescendingHeaderStyle BackColor="#242121" />
        </asp:GridView>
		<asp:SqlDataSource ID="dsData" runat="server" SelectCommand="SELECT * FROM GetRefundRequests(@nTransID) ORDER BY ID DESC">
			<SelectParameters>
				<asp:QueryStringParameter QueryStringField="TransID" Name="nTransID" Type="Int32" ConvertEmptyStringToNull="true" />
			</SelectParameters>
		</asp:SqlDataSource>
	</form>
</body>
</html>
