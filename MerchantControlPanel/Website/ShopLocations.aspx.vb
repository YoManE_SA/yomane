﻿Imports Netpay.Bll
Imports Netpay.Bll.Shop
Imports System.Linq

Public Class ShopLocations
    Inherits MasteredPage

    Private _shop As RegionShop
    Private ReadOnly Property CurrentShop As RegionShop
        Get
            If _shop Is Nothing Then
                Dim id As Integer
                Dim result As Boolean = Integer.TryParse(Request("shopId"), id)
                If result Then
                    _shop = RegionShop.Load(id)
                    If _shop Is Nothing Then Response.Redirect("ShopSelection.aspx", True)
                Else
                    _shop = New RegionShop(Merchant.ID)
                End If
            End If

            Return _shop
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim usedLocations As RegionShop.ShopLocations = CurrentShop.GetUsedLocations()

            If CurrentShop.Locations.IsGlobal Then
                cbToggleGlobal.Checked = True
            ElseIf usedLocations.IsGlobal
                cbToggleGlobal.Disabled = True
            End If

            clRegions.DataSource = Region.Cache
            clRegions.DataTextField = "Name"
            clRegions.DataValueField = "IsoCode"
            clRegions.DataBind()
            CurrentShop.Locations.Regions.ForEach(Sub(r As Region) clRegions.Items.FindByValue(r.IsoCode).Selected = True)
            usedLocations.Regions.ForEach(Sub(r As Region) clRegions.Items.FindByValue(r.IsoCode).Enabled = False)

            clCountries.DataSource = Country.Cache
            clCountries.DataTextField = "Name"
            clCountries.DataValueField = "IsoCode2"
            clCountries.DataBind()
            CurrentShop.Locations.Countries.ForEach(Sub(c As Country) clCountries.Items.FindByValue(c.IsoCode2).Selected = True)
            usedLocations.Countries.ForEach(Sub(c As Country) clCountries.Items.FindByValue(c.IsoCode2).Enabled = False)

            ' add flags & translate
            For Each item As ListItem In clRegions.Items
                Dim text As String = Web.WebUtils.GetResourceValue("Regions", item.Value)
                item.Text = text
                item.Attributes.Add("searchterm", text)
            Next

            For Each item As ListItem In clCountries.Items
                Dim text As String = Web.WebUtils.GetResourceValue("Countries", item.Value)
                item.Attributes.Add("searchterm", text)
                Dim imgUrl As String = "/NPCommon/ImgCountry/50X30/" + item.Value + ".png"
                If Not System.IO.File.Exists(MapPath(imgUrl)) Then imgUrl = "/NPCommon/ImgCountry/18X12/--.gif"
                item.Text = "<img  height=""12px"" src=""" + imgUrl + """style=""border: 1px solid #eee;"" /> " + text
            Next
        End If
    End Sub

    Protected Sub btnNext_Click(sender As Object, e As EventArgs)
        Dim isSelected As Boolean = cbToggleGlobal.Checked

        CurrentShop.Locations.Regions.Clear()
        For Each item As ListItem In clRegions.Items
            If item.Selected Then
                isSelected = True
                CurrentShop.Locations.Regions.Add(Region.Get(item.Value))
            End If
        Next

        CurrentShop.Locations.Countries.Clear()
        For Each item As ListItem In clCountries.Items
            If item.Selected Then
                isSelected = True
                CurrentShop.Locations.Countries.Add(Country.Get(item.Value))
            End If
        Next

        If Not isSelected Then
            divAlert.Visible = True
            Exit Sub
        End If

        Dim shopId As Integer = CurrentShop.Save()
        Response.Redirect("ShopSetup.aspx?shopId=" & shopId.ToString())
    End Sub

    Protected Sub btnPrev_Click(sender As Object, e As EventArgs)
        Response.Redirect("ShopSelection.aspx")
    End Sub
End Class