﻿<%@ Page Title="PageTitle" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_PaymentManagment" MaintainScrollPositionOnPostback="true" CodeBehind="PaymentManagment.aspx.vb" %>

<%@ Register Src="~/Website/Controls/ImageUpload.ascx" TagPrefix="uc1" TagName="ImageUpload" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var deleteConfirmation = '<asp:Literal runat="server" Text="<%$Resources: deleteConfirmation%>" />';
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            var version = new Number($('#hfVersion').val());
            $('#dvVersion' + version).show();
            if (version == 2) {
                $("ul.version-ul > li[vername=1]").hide();
                $("#divVersionSelection").hide();
            }
            else {
                $("ul.version-ul > li[vername=2]").click(selectVersion);
            }

            $("ul.theme-ul > li").click(selectTheme);
            var selObj = $("ul.theme-ul > li[tmpName=" + $('#hfTheme').val() + "]");
            selObj.find('div').css('display', 'block');
        });

        function selectVersion(obj) {
            $('#hfVersion').val(2)
            $("ul.version-ul > li[vername=1]").hide();
            $('#dvVersion1').slideUp();
            $('#dvVersion2').slideDown();
        }

        function selectTheme(obj) {
            $('ul.theme-ul div').css('display', 'none');
            $(obj.currentTarget).find('div').css('display', 'block');
            $('#hfTheme').val($(obj.currentTarget).attr('tmpName'));
        }
    </script>

    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">

            <div class="section">
                <div class="margin-bottom-10 frame-border-option">
                    <div class="margin-bottom-10">
                        <i class="fa fa-film"></i>
                        <asp:Literal Text="<%$Resources: Theme%>" runat="server" />
                    </div>
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">

                        <span id="divVersionSelection">
                            <div class="margin-bottom-10">
                                <asp:Literal Text="<%$Resources:Version%>" runat="server" />
                            </div>
                            <ul class="version-ul">
                                <li vername="2">
                                    <img src="Images/V2-PaymentPage.png" /><div class="check-version"></div>
                                </li>
                                <li vername="1" id="liVer1">
                                    <img src="Images/V1-PaymentPage.png" /><div class="check-version"></div>
                                </li>
                            </ul>
                            <asp:HiddenField runat="server" ID="hfVersion" ClientIDMode="Static" />
                        </span>
                        <div class="spacer"></div>


                        <div id="dvVersion1" style="display: none;">

                            <h3>
                                <asp:Literal Text="<%$Resources:Attention%>" runat="server" /></h3>
                            <p>
                                <asp:Literal Text="<%$Resources:VersionTheme%>" runat="server" />
                            </p>

                        </div>
                        <div id="dvVersion2" style="display: none;">
                            <ul class="theme-ul">
                                <li tmpname="blue">
                                    <img src="Images/Themes/blue.png" /><div class="check-theme"></div>
                                </li>
                                <li tmpname="green">
                                    <img src="Images/Themes/green.png" /><div class="check-theme"></div>
                                </li>
                                <li tmpname="grey">
                                    <img src="Images/Themes/grey.png" /><div class="check-theme"></div>
                                </li>
                                <li tmpname="orange">
                                    <img src="Images/Themes/orange.png" /><div class="check-theme"></div>
                                </li>
                                <li tmpname="purple">
                                    <img src="Images/Themes/purple.png" /><div class="check-theme"></div>
                                </li>
                                <li tmpname="red">
                                    <img src="Images/Themes/red.png" /><div class="check-theme"></div>
                                </li>
                            </ul>
                            <asp:HiddenField runat="server" ID="hfTheme" ClientIDMode="Static" />
                            <div class="spacer"></div>
                        </div>
                    </div>
                </div>
                <div class="margin-bottom-10 frame-border-option">
                    <div class="margin-bottom-10">
                        <i class="fa fa-picture-o"></i>
                        <asp:Literal Text="<%$Resources: Company%>" runat="server" />
                    </div>
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">
                        <div id="logo-company">
                            <div class="alert-logo">
                                <asp:Literal Text="<%$Resources: ValidFileUpload%>" runat="server" />
                            </div>
                            <uc1:ImageUpload runat="server" ID="ImageUpload" Width="350" Height="80" BytesSizeLimit="2048000" OnImageDelete="DeleteImage_Command" />
                            <div class="spacer"></div>
                        </div>
                    </div>
                </div>
                <div class="margin-bottom-10 frame-border-option">

                    <div class="margin-bottom-10">
                        <i class="fa fa-list"></i>
                        <asp:Literal Text="<%$Resources: Input%>" runat="server" />
                    </div>
                    <div class="alert-info margin-bottom-10">
                        <asp:Literal Text="<%$Resources: RequiredfieldAlert%>" runat="server" />
                    </div>
                    <div class="align-left percent percent48">
                        <div class="margin-bottom-10"><i class="fa fa-building fa-fw"></i> <asp:Literal Text="<%$Resources: BillingAddress%>" runat="server" /></div>
                        <ul class="group-list">
                            <li>
                                <asp:CheckBox runat="server" ID="chIsShowAddress1" Text="<%$Resources: ShowAddress1%>" /></li>
                            <li>
                                <asp:CheckBox runat="server" ID="chIsShowAddress2" Text="<%$Resources: ShowAddress2%>" /></li>
                            <li>
                                <asp:CheckBox runat="server" ID="chIsShowCity" Text="<%$Resources: ShowCity%>" /></li>
                            <li>
                                <asp:CheckBox runat="server" ID="chIsShowZipCode" Text="<%$Resources: ShowZipCode%>" /></li>
                            <li>
                                <asp:CheckBox runat="server" ID="chIsShowState" Text="<%$Resources: Showstate%>" /></li>
                            <li>
                                <asp:CheckBox runat="server" ID="chIsShowCountry" Text="<%$Resources: ShowCountry%>" /></li>

                            <li>
                                <asp:CheckBox runat="server" ID="chkIsShippingAddressRequire" Text="<%$Resources: IsShippingAddressRequire%>" /></li>

                        </ul>
                    </div>
                    <div class="align-right percent percent48">
                        <div class="margin-bottom-10"><i class="fa fa-user fa-fw"></i> <asp:Literal Text="<%$Resources: PersonalDetails%>" runat="server" /></div>
                        <ul class="group-list">
                            <li>
                                <asp:CheckBox runat="server" ID="chIsShowEmail" Text="<%$Resources: ShowEmail%>" /></li>
                            <li>
                                <asp:CheckBox runat="server" ID="chIsShowPhone" Text="<%$Resources: ShowPhone%>" /></li>
                            <li>
                                <asp:CheckBox runat="server" ID="chIsShowPersonalID" Text="<%$Resources: ShowPersonalID%>" /></li>
                            <li>
                                <asp:CheckBox runat="server" ID="chkIsShowBirthDate" Text="<%$Resources: ShowBirthDate%>" /></li>
                        </ul>
                    </div>
                    <div class="spacer"></div>
                </div>
                <div class="margin-bottom-10 frame-border-option">

                    <netpay:TabControl runat="server" ID="tbLanguageText" TabStyle="OrganicTabs" CssStyle="organicTabs frame-border-option-inline padding-10 margin-bottom-10">
                        <tabtemplate>
                            <div class="tabs-edit">
                                <div class="alert-info">
                                <asp:Literal ID="txtInfoTabs" Text="<%$Resources: txtInfotabs%>" runat="server" /></div>
                                <div class="wrap-check-default">
                                <asp:checkbox CssClass="checklistInput" runat="server" id="chkLanguageIsDefault" Text="<%$Resources: SetDefault%>" onclick="EnsureSingleSelect(this)" /></div>
						        <div><asp:Literal Text="<%$Resources: TBSabovePayment%>" runat="server" /></div>
						        <div><asp:TextBox runat="server" ID="txtFrontTop" MaxLength="1500" class="percent100 percent-height-10" TextMode="MultiLine" /></div>

						        <div><asp:Literal Text="<%$Resources: TBSbelowpayment%>"  runat="server" /></div>
						        <div><asp:TextBox runat="server" ID="txtFrontBottom" MaxLength="1500" class="percent100 percent-height-10"  TextMode="MultiLine" /></div>

						        <div><asp:Literal Text="<%$Resources: CTScharge%>"  runat="server" /></div>
						        <div><asp:TextBox runat="server" ID="txtTransSuccess" MaxLength="1500" class="percent100 percent-height-10" TextMode="MultiLine" /></div>

						        <div><asp:Literal Text="<%$Resources: CTCcharge%>"  runat="server" /></div>
						        <div><asp:TextBox runat="server" ID="txtTransDecline" MaxLength="1500" class="percent100 percent-height-10" TextMode="MultiLine" /></div>

						        <div><asp:Literal Text="<%$Resources: CTCPending%>"  runat="server" /></div>
						        <div><asp:TextBox runat="server" ID="txtTransPending" MaxLength="1500" class="percent100 percent-height-10" TextMode="MultiLine" /></div>
						        <div class="line-height-35 margin-top-10">
                             <asp:LinkButton runat="server" id="ddlRemoveLanguage" CssClass="btn btn-danger" Text="<%$Resources: Remove%>" OnCommand="RemoveLanguage_Command" OnClientClick="if(!confirm(deleteConfirmation)) return false;" /></div>
					        </div>
                                <div class="">
                                </div>
			        	</tabtemplate>
                        <tabs>
					<netpay:TabView ID="TabView3" runat="server" Text="<%$Resources: AddLngshort%>">
						<div class="tabs-edit">
                            <div><asp:Literal  Text="<%$Resources: AddLng%>" runat="server" /></div>
							<div><asp:DropDownList ID="ddlLanguage" CssClass="Field_200" runat="server" /></div>
							<div class="margin-top-10"><asp:LinkButton runat="server" CssClass="btn btn-primary btn-cons-short" OnClick="AddLanguage_Click"><i class="fa fa-plus-circle"></i> <asp:Literal  Text="<%$Resources: Add%>" runat="server" /> </asp:LinkButton></div>
                            
						</div>
					</netpay:TabView>
				</tabs>
                    </netpay:TabControl>
                    <div class="wrap-button">
                        <asp:LinkButton ID="btnUpdate" CssClass="btn btn-cons-short btn-success" runat="server" OnClick="btnUpdate_Click">
                 <i class="fa fa-floppy-o"></i>
                 <asp:Literal  Text="<%$Resources: Update %>" runat="server" />
                        </asp:LinkButton>
                    </div>

                </div>
            </div>
        </div>
    </div>

</asp:Content>
