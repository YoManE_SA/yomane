﻿Imports System.Collections.Generic
Imports Netpay.Bll.Reports
Imports Netpay.CommonTypes
Imports Netpay.Bll
Imports Netpay.Web
Imports Netpay.Web.Controls
Imports Netpay.Infrastructure

Partial Class Website_RollingReserveTransactions
	Inherits MasteredPage

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
            Dim filters As New Bll.Transactions.Transaction.SearchFilters()
            filters.paymentMethodID = PaymentMethodEnum.RollingReserve
            Dim usedCurrencies As List(Of Integer) = Bll.Transactions.Transaction.GetUsedCurrencies(filters)
			If usedCurrencies.Count > 0 Then
				hIsTransactions.Value = "true"
				mvMain.SetActiveView(vRollingReserves)
				ddCurrency.IncludeCurrencyIDs = usedCurrencies.ToDelimitedString()
			Else
				mvMain.SetActiveView(vNoRollingReserves)
			End If
		End If
	End Sub

	Protected Sub Page_PreRenderComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRenderComplete
		If Not IsPostBack Then btnSearch_Click(sender, e)
	End Sub

	Protected Sub ChangeCurrency(ByVal sender As Object, ByVal e As EventArgs)
		btnSearch_Click(sender, e)
	End Sub

 	Protected Sub OnPageChanged(ByVal sender As Object, ByVal e As System.EventArgs)
		If hIsTransactions.Value = "false" Then Exit Sub
        Dim filters = New Bll.Transactions.Transaction.SearchFilters()
        filters.paymentMethodID = PaymentMethodEnum.RollingReserve
		filters.currencyID = ddCurrency.SelectedValue

        repeaterResults.DataSource = Bll.Transactions.Transaction.Search(TransactionStatus.Captured, filters, pager.Info, False, False)
		repeaterResults.DataBind()

        Dim balance As Decimal = Bll.Transactions.Transaction.GetBalance(filters)
		If balance < 0 Then
			txtBalance.Style.Add("color", "red")
		Else
			txtBalance.Style.Remove("color")
		End If
		txtBalance.Text = balance.ToAmountFormat(filters.currencyID.Value)

		btnExportExcel.ResultCount = pager.Info.RowCount 
	End Sub

    Protected Function GetRollingLine(ByVal transaction As Bll.Transactions.Transaction) As String
        Dim html As String = Nothing
        If transaction.CreditType = CreditType.Refund Then
            html = "<td class='Numeric' style='color:red;'>" + transaction.Amount.ToAmountFormat(ddCurrency.SelectedValue.ToInt32(CommonTypes.Currency.Unknown), True) + "</td><td class='Numeric'></td>"
        Else
            html = "<td class='Numeric'></td><td class='Numeric'>" + transaction.Amount.ToAmountFormat(ddCurrency.SelectedValue.ToInt32(CommonTypes.Currency.Unknown)) + "</td>"
        End If

        Return html
    End Function

    Protected Function GetRollingLineArrow(ByVal transaction As Bll.Transactions.Transaction) As String
        Dim html As String = Nothing
        If transaction.CreditType = CreditType.Refund Then
            html = "<img src=""/NPCommon/Images/icon_ArrowR.gif"" />"
        Else
            html = "<img src=""/NPCommon/Images/icon_ArrowG.gif"" />"
        End If

        Return html
    End Function

	Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
		pager.Info.PageCurrent = 0
		OnPageChanged(sender, e)
		pager.Visible = True
	End Sub

	Protected Sub ExportExcel_Click(ByVal sender As Object, ByVal e As ExcelButton.ExportEventArgs)
        Dim filters = New Bll.Transactions.Transaction.SearchFilters()
        filters.paymentMethodID = PaymentMethodEnum.RollingReserve
		filters.currencyID = ddCurrency.SelectedValue

        btnExportExcel.SetExportInfo("Merchant Rolling Reserve", ReportType.MerchantRollingReserve, Function(pi) Bll.Transactions.Transaction.Search(TransactionStatus.Captured, filters, pi, False, False))
		btnExportExcel.Export()
	End Sub
End Class