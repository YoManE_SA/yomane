﻿<%@ Page Language="vb" AutoEventWireup="true" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" CodeBehind="DevUrlNotificationSettings.aspx.vb" Inherits="Netpay.MerchantControlPanel.UrlNotificationSettings" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:menu, GeneralNotifications%>" runat="server" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphBody" runat="Server">

    <script type="text/javascript">
        function showSection(SecNum) {
            trSecObj = document.getElementById("trSec" + SecNum)
            oListToggleObj = document.getElementById("oListToggle" + SecNum)
            if (trSecObj.style.display == "none") {
                oListToggleObj.src = "/NPCommon/Images/tree_collapse.gif";
                trSecObj.style.display = "";
            }
            else {
                oListToggleObj.src = "/NPCommon/Images/tree_expand.gif";
                trSecObj.style.display = "none";
            }
        }
    </script>

    <div class="area-content">
        <div class="top-nav">
            <asp:Literal Text="<%$Resources:Notification%>" runat="server" />
        </div>
        <div class="content-background">
            <div class="alert alert-info margin-bottom-10">
                <asp:Literal Text="<%$Resources:intro %>" runat="server" />
            </div>

            <div class="section">
                <div class="margin-bottom-10 frame-border-option">
                    <div class="margin-bottom-10">
                        <i class="fa fa-refresh fa-fw"></i>
                        <asp:Literal Text="<%$Resources:Recurring%>" runat="server" />
                    </div>
                    <ul class="group-list">
                        <li>
                            <asp:CheckBox runat="server" ID="chkIsRecurringReply" />
                            <asp:Literal Text="<%$Resources:SendResult %>" runat="server" />
                        </li>
                    </ul>
                    <div class="frame-border-option-inline padding-10 margin-top-10 margin-bottom-10">
                        <asp:Panel ID="panelRecurringAlert" Visible="false" runat="server">
                            <div class="error">
                                <asp:Literal Text="<%$ Resources:Common, DisabledFeatureMsg %>" runat="server" />
                            </div>
                        </asp:Panel>

                        <div class="form-group">
                            <span class="form-group-title-eg">
                                <asp:Literal Text="<%$Resources:ValidUrl %>" runat="server" /></span>
                            <asp:TextBox CssClass="percent100 url-input" runat="server" ID="txtRecurringReplyUrl" MaxLength="100" />
                        </div>

                        <div class="wrap-expand-details">
                            <img onclick="showSection('0');" class="tree-expand" id="oListToggle0" src="/NPCommon/Images/tree_expand.gif" alt="" border="0">
                            <a href="javascript:showSection('0');" class="tree-expand-text">
                                <asp:Literal Text="<%$Resources:ShowResponse %>" runat="server" /></a>
                            <div id="trSec0" style="display: none;">
                                <div class="margin-top-10">
                                    <asp:Literal Text="<%$Resources: ResponseSentBack %>" runat="server" />

                                </div>
                                <table class="detalis-table">
                                    <thead>
                                        <tr>
                                            <th>
                                                <asp:Literal Text="<%$Resources: Field%>" runat="server" />
                                            </th>
                                            <th>
                                                <asp:Literal Text="<%$Resources:Description %>" runat="server" />
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <asp:Literal Text="<%$Resources: recur_initialID %>" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$Resources: Identification_ID%>" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Literal Text="<%$Resources: recur_seriesID%>" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$Resources: Identification_series_ID%>" runat="server" />
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <asp:Literal Text="<%$Resources: recur_chargeCount%>" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$Resources: num_charges%>" runat="server" /></td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <asp:Literal Text="<%$Resources: recur_chargeNum %>" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$Resources: charge_number %>" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Literal Text="<%$Resources: recur_status %>" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Repeater ID="repeaterRecurringStatus" runat="server">
                                                    <ItemTemplate>
                                                        <asp:Literal Text='<%# Eval("ID")%>' runat="server" />
                                                        -
                                                         <asp:Literal Text='<%# Eval("Value") %>' runat="server" />
                                                    </ItemTemplate>
                                                    <SeparatorTemplate>, </SeparatorTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                     <ul class="group-list">
                        <li>
                            <asp:CheckBox runat="server" ID="chkIsRecurringModifyReply" />
                            <asp:Literal Text="<%$Resources: SendResultSeries %>" runat="server" />
                        </li>
                    </ul>
                    <div class="frame-border-option-inline padding-10 margin-top-10 margin-bottom-10">
                        <div class="form-group">
                            <asp:Literal Text="<%$Resources: Url %>" runat="server" />&nbsp;<span class="form-group-title-eg">&nbsp;<asp:Literal Text="<%$Resources: ValidUrl %>" runat="server" /></span>
                            <asp:TextBox CssClass="percent100 url-input" runat="server" ID="txtRecurringModifyReplyUrl" MaxLength="100" />
                        </div>

                    </div>
                    <div class="margin-bottom-10">
                        <i class="fa fa-pause fa-fw"></i>
                        <asp:Literal Text="<%$Resources:PendingTransaction%>" runat="server" />
                    </div>
                   

                    <ul class="group-list">
                        <li>
                            <asp:CheckBox runat="server" ID="chkIsPendingReply" />
                            <asp:Literal Text="<%$Resources:ReceivingCharge%>" runat="server" />
                        </li>
                    </ul>
                    <div class="frame-border-option-inline padding-10 margin-top-10 margin-bottom-10">
                        <div class="form-group">
                            <asp:Literal Text="<%$Resources: Url%>" runat="server" />&nbsp;<span class="form-group-title-eg">&nbsp;<asp:Literal Text="<%$Resources:ValidUrl %>" runat="server" /></span>
                            <asp:TextBox CssClass="percent100 url-input" runat="server" ID="txtPendingReplyUrl" MaxLength="100" />
                        </div>
                        <div class="wrap-expand-details">

                            <img onclick="showSection('1');" id="oListToggle1" src="/NPCommon/Images/tree_expand.gif" class="tree-expand" alt="">
                            <a href="javascript:showSection('1');" class="tree-expand-text">
                                <asp:Literal Text="<%$Resources: ShowResponse %>" runat="server" /></a><br />
                            <div id="trSec1" style="display: none;">
                                <table class="detalis-table">
                                    <thead>
                                        <tr>
                                            <th>
                                                <asp:Literal Text="<%$Resources: Field %>" runat="server" /></th>
                                            <th>
                                                <asp:Literal Text="<%$Resources: Description %>" runat="server" /></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <asp:Literal Text="<%$Resources: Reply %>" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$Resources: Transaction_Reply %>" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Literal Text="<%$Resources: transID %>" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$Resources: New_transID%>" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Literal Text="<%$Resources: Date %>" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$Resources: New_transDate %>" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Literal Text="<%$Resources: TypeCredit %>" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$Resources: TypeCredit_Orginal %>" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Literal Text="<%$Resources: Payments %>" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$Resources: Installments %>" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Literal Text="<%$Resources: Amount %>" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$Resources: Amount_Transaction %>" runat="server" /></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Literal Text="<%$Resources:Currency %>" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$Resources:Currency_Transaction %>" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Literal Text="<%$Resources:Order %>" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$Resources:Order_Transaction %>" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Literal Text="<%$Resources: reference %>" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$Resources: Original_transaction_ID %>" runat="server" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="query-test">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <asp:Button CssClass="btn btn-primary query-test-button align-right" runat="server" Text="<%$Resources: Send %>" ID="btnSendTest" OnClick="btnTestPendingNotification" />
                                            <b>
                                                <asp:Literal Text="<%$Resources: Test_Page %>" runat="server" /></b><br />
                                            <%=PendingTestSendData.Replace(vbCrLf, "<br/>") %><br />
                                            <asp:Literal Visible="false" runat="server" ID="ltPendingTextResult" Mode="PassThrough" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="margin-bottom-10">
                        <i class="fa fa-retweet fa-fw"></i>
                        <asp:Literal Text="<%$Resources: Refund_Request %>" runat="server" />
                    </div>
                    <ul class="group-list">
                        <li>
                            <asp:CheckBox runat="server" ID="chkIsNotifyRefundRequestApproved" />
                            <asp:Literal Text="<%$Resources: Receiving_refund %>" runat="server" />
                        </li>
                    </ul>
                    <div class="frame-border-option-inline padding-10 margin-top-10 margin-bottom-10">
                        <div class="form-group">
                            <asp:Literal Text="<%$Resources: Url %>" runat="server" />
                            <span class="form-group-title-eg">
                                <asp:Literal Text="<%$Resources: ValidUrl %>" runat="server" /></span>
                            <asp:TextBox runat="server" CssClass="percent100 url-input" ID="txtNotifyRefundRequestApprovedUrl" MaxLength="100" />
                        </div>
                        <div class="wrap-expand-details">
                            <img onclick="showSection('2');" class="tree-expand" id="oListToggle2" src="/NPCommon/Images/tree_expand.gif" alt="" border="0">
                            <a href="javascript:showSection('2');">
                                <asp:Literal Text="<%$Resources: Show_Response%>" runat="server" /></a>

                            <div id="trSec2" style="display: none;">

                                <table class="detalis-table">
                                    <thead>
                                        <tr>
                                            <th>
                                                <asp:Literal Text="<%$Resources: Field%>" runat="server" />
                                            </th>
                                            <th>
                                                <asp:Literal Text="<%$Resources: Description%>" runat="server" />
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <asp:Literal Text="<%$Resources:RequestId %>" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$Resources: Identification_num_ID %>" runat="server" /></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Literal Text="<%$Resources:OriginalTransId %>" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$Resources: Identification_Transaction_ID%>" runat="server" /></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Literal Text="<%$Resources: RefundTransId %>" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$Resources: Identification_Transaction_refund%>" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Literal Text="<%$Resources: Amount %>" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$Resources: Amount_refund%>" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Literal Text="<%$Resources: Currency %>" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$Resources: Currency_refund%>" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Literal Text="<%$Resources: Status %>" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Literal Text="<%$Resources: refund_completed%>" runat="server" /></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="query-test">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <asp:Button ID="Button1" CssClass="btn btn-primary query-button" runat="server" Text="<%$Resources: Send %>" OnClick="btnTestRefundRequestNotification" />
                                            <b>
                                                <asp:Literal Text="<%$Resources: Test_Page %>" runat="server" /></b><br />
                                            <%=RefundReqTestSendData.Replace(vbCrLf, "<br/>") %>
                                            <br />
                                            <asp:Literal Visible="false" runat="server" ID="ltRefundRequestTextResult" Mode="PassThrough" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="margin-bottom-10">
                        <i class="fa fa-money fa-fw"></i>
                        <asp:Literal Text="<%$Resources: Wallet_Ballance%>" runat="server" />
                    </div>
                    <asp:Panel ID="panelEWalletAlert" Visible="false" runat="server">
                        <div class="frame-border-option-inline padding-10 margin-top-10 margin-bottom-10">
                            <div class="alert alert-warning">
                                <asp:Literal ID="Literal4" Text="<%$ Resources:Common, DisabledFeatureMsg %>" runat="server" />
                            </div>
                        </div>
                    </asp:Panel>
                    <ul class="group-list">
                        <li>
                            <asp:CheckBox runat="server" ID="chkIsWalletReply" />
                            <asp:Literal Text="<%$Resources: Allow_Wallet %>" runat="server" />
                        </li>
                    </ul>
                    <div class="frame-border-option-inline padding-10 margin-top-10 margin-bottom-10">
                        <div class="form-group">
                            <asp:Literal Text="<%$Resources: Url %>" runat="server" />&nbsp;<span class="form-group-title-eg"><asp:Literal Text="<%$Resources: ValidUrl %>" runat="server" /></span>
                            <asp:TextBox CssClass="percent100 url-input" runat="server" ID="txtWalletReplyUrl" MaxLength="100" />
                        </div>
                    </div>
                    <ul class="group-list">
                        <li>
                            <asp:CheckBox runat="server" ID="chkUseHasKeyInWalletReply" />
                            <asp:Literal Text="<%$Resources: Append_hashed %>" runat="server" />
                        </li>
                        <li>
                            <asp:Label CssClass="form-group-title-eg" runat="server" ID="lblHash" />
                        </li>
                    </ul>
                    <div class="margin-bottom-10 margin-top-10">
                        <i class="fa fa-flag fa-fw"></i>
                        <asp:Literal Text="<%$Resources: Process_Notification%>" runat="server" />
                    </div>
                    <div class="frame-border-option-inline padding-10 margin-top-10 margin-bottom-10">
                        <div class="form-group">
                            <asp:Literal Text="<%$Resources: Url %>" runat="server" />&nbsp;<span class="form-group-title-eg"><asp:Literal Text="<%$Resources: ValidUrl %>" runat="server" /></span>&nbsp;
					    <asp:TextBox CssClass="percent100 url-input" runat="server" ID="txtNotifyProcessURL" MaxLength="100" />
                        </div>
                    </div>
                    <div class="wrap-button">
                    <asp:LinkButton CssClass="btn btn-success btn-cons-short" runat="server" UseSubmitBehavior="false" OnClick="SaveForm"><i class="fa fa-floppy-o"></i> <asp:Literal runat="server" Text="<%$Resources: Update %>" /></asp:LinkButton>
                </div>
                </div>

                
            </div>
        </div>
    </div>
</asp:Content>

