﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OuterLogin.aspx.vb" Inherits="Netpay.MerchantControlPanel.OuterLogin" %>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
		<title><asp:Literal runat="server" Text="<%$ Resources:PageTitle %>" /></title>
		<asp:placeholder runat="server" id="phHeader" />
	</head>
	<body>
		<form id="form1" runat="server">
			<div>
				<netpay:Login ID="wcLogin" UserType="Merchant" Layout="Vertical" ShowMessageBox="true" runat="server" />
			</div>
			<div class="divLink" style="padding:5px;"><a href="http://merchants.netpay-intl.com" target="_blank"><asp:Literal runat="server" Text="<%$Resources:Merchants_Control_Panel%>" /></a></div>
		</form>
	</body>
</html>
