﻿Imports System.Xml
Imports Netpay.Web
Imports Netpay.Infrastructure


Partial Class LoginOLD
    Inherits Page
    Public ShowRegister As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lnkIcon.Href = WebUtils.MapTemplateVirPath(Me, "favicon.ico?1=1")

        If Not Page.IsPostBack And Request.QueryString("login") = "outer" Then
            Dim email As String = WebUtils.DecryptUrlBase64String(Request("mail").Trim())
            'Dim username As String = WebUtils.DecryptUrlBase64String(Request("username").Trim())
            Dim password As String = WebUtils.DecryptUrlBase64String(Request("userpassword").Trim())

            'Email
            loginEmail.Visible = False
            loginEmail.Text = email

            'Username
            'loginUsername.Visible = False
            'loginUsername.Text = username

            'Password
            loginPassword.TextMode = TextBoxMode.SingleLine
            loginPassword.Visible = False
            loginPassword.Text = password

        ElseIf Not IsPostBack Then
            ShowRegister = Request.QueryString("ShowRegister").ToNullableBool().GetValueOrDefault(False)
        Else
            If WebUtils.IsLoggedin Then Response.Redirect("~/Website/default.aspx")
        End If
        dvSocialMedia.DataBind()
    End Sub

    Sub AlertAndFocus(ByVal message As String, ByRef txtField As TextBox)
        lblError.Text = "<div class=""error"">" & message & "</div>"
        lblError.Visible = True
        txtField.Focus()
    End Sub

    Sub SuccessAndFocus(ByVal message As String, ByRef txtField As TextBox)
        lblError.Text = "<div class=""success"">" & message & "</div>"
        lblError.Visible = True
        txtField.Focus()
    End Sub

    Protected Sub btnLogin_Click(ByVal o As Object, ByVal e As System.EventArgs)
        ShowRegister = False
        lblError.Text = ""
        Dim isValidData As Boolean = True
        If Validation.IsEmptyField(loginEmail, lblError, CType(GetGlobalResourceObject("Login.aspx", "Mail"), String)) Then isValidData = False
        If Validation.IsNotMail(loginEmail, lblError, CType(GetGlobalResourceObject("Login.aspx", "Mail"), String)) Then isValidData = False
        'If Validation.IsEmptyField(loginUsername, lblError, CType(GetGlobalResourceObject("Login.aspx", "Username"), String)) Then isValidData = False
        If Validation.IsEmptyField(loginPassword, lblError, CType(GetGlobalResourceObject("Login.aspx", "Password"), String)) Then isValidData = False

        If Not isValidData Then
            'dvAlert.Visible = True
            Exit Sub
        End If

        Dim email As String = loginEmail.Text.ToSql(True).Trim()
        'Dim username As String = loginUsername.Text.ToSql(True).Trim()
        Dim password As String = loginPassword.Text.ToSql(True).Trim()

        Dim result = WebUtils.Login(New Infrastructure.Security.UserRole() {Infrastructure.Security.UserRole.Merchant, Infrastructure.Security.UserRole.MerchantSubUser}, Nothing, email, password)

        If result = Infrastructure.Security.LoginResult.Success Then
            If WebUtils.LoggedUser.IsFirstLogin Then Response.Redirect("EditLogin.aspx", True)
            Dim url As String = Request.RawUrl

            If url.ToLower.Contains("signup.aspx") Then url = "."
            If url.ToLower.Contains("login.aspx") Then url = "."
            If url.ToLower.Contains("logout.aspx") Then url = "."
            If url.ToLower.Contains("userlockedout") Then url = "."
            If url.ToLower.Contains("userblocked") Then url = "."
            If url.ToLower.Contains("password") Then url = "."

            'Server.Transfer(url, False)
            'Response.End()
            Response.Redirect(url, True)
        ElseIf result = Security.LoginResult.ForcePasswordChange Then
            dlgChangePassword.RegisterShow()

        Else AlertAndFocus(CType(GetGlobalResourceObject("menu", Infrastructure.Security.LoginResult.WrongPassword.ToString()), String), loginPassword)

        End If
    End Sub

    Protected Overrides Sub OnPreRender(e As System.EventArgs)
        ltCopyright.Text = CType(GetGlobalResourceObject("Login.aspx", "Copyright"), String).Replace("%BRAND%", WebUtils.CurrentDomain.BrandName)
        Dim link As New HtmlControls.HtmlLink()
        link.Href = WebUtils.MapTemplateVirPath(Me, "WebSite/Styles/login.css")
        link.Attributes.Add("rel", "stylesheet")
        link.Attributes.Add("type", "text/css")
        link.Attributes.Add("media", "screen")
        Page.Header.Controls.Add(link)
        If System.Threading.Thread.CurrentThread.CurrentUICulture.TextInfo.IsRightToLeft Then
            link = New HtmlControls.HtmlLink()
            link.Href = WebUtils.MapTemplateVirPath(Me, "WebSite/Styles/loginRTL.css")
            link.Attributes.Add("rel", "stylesheet")
            link.Attributes.Add("type", "text/css")
            link.Attributes.Add("media", "screen")
            Page.Header.Controls.Add(link)
        End If

        btnClose.OnClientClick = dlgChangePassword.HideJSCommand
        btnForgotPassword.OnClientClick = "event.preventDefault();" + ConfirmationModalDialog.ShowJSCommand
        btnCancelReset.OnClientClick = ConfirmationModalDialog.HideJSCommand

        MyBase.OnPreRender(e)
    End Sub

    Protected Sub UpdatePassword_Click(sender As Object, e As EventArgs)
        'Handle the case that the user inserted the old password again.
        If (loginPassword.Text.ToSql(True).Trim() = txtNewPassword.Text.ToSql(True).Trim()) Then
            acnPasswordMessage.SetMessage("Please insert a new password.", True)
            dlgChangePassword.BindAndUpdate()
        End If

        'Handle case when both passwords not equal
        If (txtNewPassword.Text.ToSql(True).Trim() <> txtConfirmPassword.Text.ToSql(True).Trim()) Then
            acnPasswordMessage.SetMessage("Passwords must be the same.", True)
            dlgChangePassword.BindAndUpdate()
        Else
            Dim email As String = loginEmail.Text.ToSql(True).Trim()
            'Dim userName As String = loginUsername.Text.ToSql(True).Trim()
            Dim oldPassword As String = txtOldPassword.Text.ToSql(True).Trim()
            Dim newPassword As String = txtNewPassword.Text.ToSql(True).Trim()

            Dim ret = Security.Login.SetNewPasswordAfterExpiredOrReset(New Infrastructure.Security.UserRole() {Infrastructure.Security.UserRole.Merchant, Infrastructure.Security.UserRole.MerchantSubUser}, Nothing, email, oldPassword, newPassword, Request.UserHostAddress)
            If (ret = Infrastructure.Security.Login.ChangePasswordResult.Success) Then
                dlgChangePassword.RegisterHide()
                loginPassword.TextMode = TextBoxMode.Password
                loginPassword.Visible = True
                SuccessAndFocus("Password changed, login with the new one.", loginPassword)
            Else
                acnPasswordMessage.SetMessage("Unable to change password: " + ret.ToString(), True)
            End If
        End If
    End Sub

    Protected Sub btnForgotPassword_Click(sender As Object, e As EventArgs)
        'Close the confirmation modal.
        ConfirmationModalDialog.RegisterHide()

        'Validations
        'If (String.IsNullOrEmpty(loginUsername.Text.Trim())) Then
        '    AlertAndFocus("User name is not valid.", loginUsername)
        '    Return
        'End If

        If (Validation.IsNotMail(loginEmail)) Then
            AlertAndFocus("Email is not valid.", loginEmail)
            Return
        End If

        'Try reset pass
        Dim isReset = Infrastructure.Security.Login.ResetPassword(New Infrastructure.Security.UserRole() {Infrastructure.Security.UserRole.Merchant, Infrastructure.Security.UserRole.MerchantSubUser},
                                                                 Nothing, 'loginUsername.Text.Trim(), 
        loginEmail.Text.Trim(), Request.UserHostAddress)

        If isReset Then
            SuccessAndFocus("New password generated and sent via email.", loginEmail)
        Else
            AlertAndFocus("User wasn't found.", loginEmail)
        End If
    End Sub
End Class