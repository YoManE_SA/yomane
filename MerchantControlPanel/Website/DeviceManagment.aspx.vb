﻿Imports System.Xml
Imports Netpay.Web
Imports Netpay.Infrastructure

Partial Class DeviceManagment
    Inherits Netpay.MerchantControlPanel.MasteredPage

	Protected Overrides Sub OnLoad(e As System.EventArgs)
		MyBase.OnLoad(e)
		If Not IsPostBack Then getDeviceItem()
	End Sub

	Protected sub getDeviceItem()
        Dim data = Netpay.Bll.Accounts.MobileDevice.Search(Merchant.AccountID)
		pager.Info.PageSize = 1
        pager.Info.RowCount = data.Count
        Dim deviceItem = data.Skip(pager.Info.RowFrom - 1).Take(pager.Info.PageSize)
        rptDevices.DataSource = deviceItem
        rptDevices.DataBind()
    End Sub

 	Protected Sub OnPageChanged(ByVal sender As Object, ByVal e As System.EventArgs)
		getDeviceItem()
	End Sub

    Protected Sub Update_Click(sender As Object, e As System.EventArgs)
        Netpay.Bll.Accounts.MobileDevice.SetMobileDevicesFriendlyName(CType(rptDevices.Items(0).FindControl("hdDeviceId"), HiddenField).Value.ToInt32(),
            CType(rptDevices.Items(0).FindControl("txtDeviceName"), TextBox).Text)
        getDeviceItem()
    End Sub

    Protected Sub btnBlock_Click(sender As Object, e As EventArgs)
        Dim deviceId = CType(rptDevices.Items(0).FindControl("hdDeviceId"), HiddenField).Value.ToInt32()
        Netpay.Bll.Accounts.MobileDevice.BlockMobileDevice(deviceId, False)
        getDeviceItem()
    End Sub

    Protected Sub btnUnblock_Click(sender As Object, e As EventArgs)
        Dim deviceId = CType(rptDevices.Items(0).FindControl("hdDeviceId"), HiddenField).Value.ToInt32()
        Netpay.Bll.Accounts.MobileDevice.BlockMobileDevice(deviceId, True)
        getDeviceItem()
    End Sub
End Class

