﻿<%@ Page Title="PageTitle" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master"
    AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_SettlementsReport"
    CodeBehind="SettlementsReport.aspx.vb" %>

<%@ Import Namespace="Netpay.MerchantControlPanel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$ Resources:PageTitle %>" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBody" runat="Server">
    <script type="text/javascript">
        function showRow(imageElement, detailsRow, detailsCell, loadUrl, themeFolder) {
            detailsRow.style.display = detailsRow.style.display != '' ? '' : 'none';
            imageElement.src = '../' + themeFolder + '/Images/' + (detailsRow.style.display != '' ? 'plus.png' : 'minus.png');
            $(detailsCell).load(loadUrl, null, function (s, t) { if (s.substr(0, 9) == 'REDIRECT:') document.location.href = s.substr(9); });
        }
    </script>
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$Resources:PageTitle%>" runat="server" /></div>
        <div class="content-background">
            <div class="legend-menu">
                <div class="align-right">
                    <netpay:PopupButton ID="PopupButton1" IconSrc="Images/iconLegend.gif"
                        Text="<%$Resources:MultiLang,Legend  %>" runat="server">
                        <netpay:LegendView ID="lvLegend" runat="server" />
                    </netpay:PopupButton>
                </div>
            </div>
            <div class="section" style="display: none;">
                <asp:PlaceHolder runat="server" ID="TransView" Visible="false">
                    <table width="100%">
                        <tr>
                            <td valign="top" style="border: 0px;">
                                <h3>
                                    Info</h3>
                                <span class="Bold">
                                    <asp:Literal runat="server" Text="<%$Resources:SettlementMaxTrans %>" />:</span>
                                <asp:Literal runat="server" ID="ltTransRangeNew" />
                                | <span class="Bold">
                                    <asp:Literal runat="server" Text="<%$Resources:SettlementMinTrans %>" />:</span>
                                <asp:Literal runat="server" ID="ltTransRangeOld" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="left" width="100%" style="border: 0px;">
                                <netpay:Totals runat="server" ID="Total1" HideTitle="true" />
                            </td>
                        </tr>
                    </table>
                    <netpay:ExcelButton runat="server" ID="btnExportExcel" Visible="false" IconImageOn="xls.png"
                        IconImageOff="xls_off.png" />
                </asp:PlaceHolder>
            </div>
            <div class="section">
                <table class="exspand-table">
                    <tbody id="tblData">
                        <tr>
                            <th colspan="2">
                                &nbsp;
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:Number %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:SettlDateTime %>" runat="server"></asp:Literal>
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:HeadPaymentToMerchant %>" runat="server"></asp:Literal>
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:TotalFees %>" runat="server"></asp:Literal>
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:VAT%>" runat="server"></asp:Literal>
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:Action %>" runat="server"></asp:Literal>
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:HeadTransactions %>" runat="server"></asp:Literal>
                            </th>
                        </tr>
                        <asp:Repeater ID="repeaterSettlements" runat="server">
                            <ItemTemplate>
                                <tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='#FFFFFF';"
                                    id='tr_<%# Eval("ID") %>'>
                                    <td>
                                        <div class="<%#GetStatusStyle(CType(Eval("WireStatus"), Netpay.Bll.Wires.Wire.WireStatus?).GetValueOrDefault()) %>"
                                            style="width: 15px; height: 15px">
                                        </div>
                                    </td>
                                    <td>
                                        <img alt="" src="<%= MapTemplateVirPath("Images/plus.png")%>" onclick="showRow(this, document.getElementById('detailsRow<%# Eval("ID") %>'), document.getElementById('detailsCell<%# Eval("ID") %>'), 'SettlementsReport.aspx?PayID=<%# Eval("ID") %>&CurrencyID=<%# Eval("CurrencyID") %>', '<%=MapTemplateVirPath("") %>');" />
                                    </td>
                                    <td>
                                        <asp:Literal ID="ltNumber" runat="server" Text='<%# Eval("ID") %>' />
                                    </td>
                                    <td>
                                        <asp:Literal ID="ltDate" runat="server" Text='<%# dbPages.FormatDatesTimes(CType(Eval("PayDate"), DateTime),1,1,0)%>' />
                                    </td>
                                     <td class="Numeric">
                                        <asp:Literal ID="ltPayToMerchant" runat="server" Text='<%# CType(Eval("TransPayTotal"), Decimal).ToAmountFormat(CType(Eval("CurrencyID"), Integer)) %>' />
                                    </td>
                                    <td class="Numeric">
                                        <asp:Literal ID="ltFees" runat="server" Text='<%# CType(Eval("TransChargeTotal"), Decimal).ToAmountFormat(CType(Eval("CurrencyID"), Integer)) %>' />
                                    </td>
                                    <td>
                                        <asp:Literal ID="ltVat" runat="server" Text='<%# IIf(CType(Container.DataItem, Netpay.Bll.Settlements.Settlement).VatAmount <> 0, CType(Container.DataItem, Netpay.Bll.Settlements.Settlement).VatAmount, "---") %>' />
                                    </td>
                                    <td>
                                        <asp:Literal ID="ltAction" runat="server" Text='<%# DisplayPayStatus(CType(Eval("WireStatus"), Netpay.Bll.Wires.Wire.WireStatus?).GetValueOrDefault(), CType(Eval("WireStatusDate"), DateTime?).GetValueOrDefault(), Eval("CurrencyID"), Eval("TransPayTotal"), CType(Eval("WireFee"), Decimal?).GetValueOrDefault(), Eval("WireFileName"))%>' />
                                    </td>
                                    <td>
                                        <asp:ImageButton ImageUrl="/NPCommon/ImgFileExt/14X14/xls.png" runat="server" OnCommand="CreateCSV"
                                            CommandArgument='<%# Eval("ID") & "," & Eval("PayDate") & "," & Eval("CurrencyID") %>' />
                                        &nbsp; <a href="SettlementList.aspx?<%# "payID=" & Eval("ID") & "&payDate=" & Eval("PayDate") &  "&Currency=" & Eval("CurrencyID") %>" />
                                        <img alt="" src="/NPCommon/images/icon_transList_ltr.png" title="Show List" /></a>
                                        <asp:Literal ID="ltPrintInvoice" runat="server" Text='<%#DisplayInvoicePrintLink(Eval("InvoiceNumber"), Eval("ID_BillingCompanyID"), Eval("ID_Type"))%>' />
                                    </td>
                                </tr>
                                <tr style="display: none; border: 0px;" id="detailsRow<%# Eval("ID") %>">
                                    <td colspan="9" style="border: 0px;" id="detailsCell<%# Eval("ID") %>">
                                        <img alt="Loading" src="images/ajax-loader.gif" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                <netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
            </div>
        </div>
    </div>
</asp:Content>
