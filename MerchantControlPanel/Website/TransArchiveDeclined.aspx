﻿<%@ Page Title="PageTitle" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master"
    AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_TransArchiveDeclined"
    CodeBehind="TransArchiveDeclined.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="Localize2" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <asp:HiddenField ID="hdLastSerachMode" runat="server" />
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$Resources:PageTitle%>" runat="server" /></div>
        <div class="content-background">
            <div class="legend-menu">
                <div class="align-right">
                    <netpay:PopupButton ID="PopupButton1" IconSrc="Images/iconLegend.gif" Width="130" Height="85" Text="<%$Resources:MultiLang,Legend  %>" runat="server">
                        <netpay:Legend ID="ttLegend" Decline="true" AuthorizeDecline="true" Test="true" runat="server" />
                    </netpay:PopupButton>
                </div>
            </div>
            <div class="section">
                <div class="filter">
                    <div class="quick-search">
                        <asp:Literal ID="Literal5" runat="server" Text="<%$Resources:MultiLang,Transaction %>" />
                        <div class="field-quick">
                            <asp:TextBox ID="txtTransactionID" runat="server" CssClass="middle-value"></asp:TextBox>
                        </div>
                        <div class="button-quick">
                            <asp:Button ID="btnShowTransaction" OnClick="btnShow_Click" Text="<%$Resources:MultiLang,Search %>" CssClass="btn" runat="server" OnClientClick="javascript:return document.getElementById('ctl00_cphBody_txtTransactionNumber').value!=''" /></div>
                    </div>
                </div>
                <div class="align-left" style="border-right: 1px solid #eee;padding-right: 20px; margin-right: 20px; width: 685px; min-height: 130px;" >
                   <div class="percent-40 form-group align-left">
                        <div class="margin-bottom-10">
                            <div><asp:Literal runat="server" Text="<%$Resources:MultiLang,Date %>" /></div>
                            <div><netpay:DateRangePicker ID="wcDateRangePicker" HtmlLayout="Flow" Layout="Horizontal" runat="server" /></div>
                        </div>
                    </div>
                   <div class="percent-40 form-group align-left">
                        <div class="margin-bottom-10">
                            <div><asp:Literal runat="server" Text="<%$Resources:MultiLang,PaymentMethod %>" /></div>
                            <div><netpay:DeclinedTypeDropDown runat="server" CssClass="meduim" ID="ddDeclinedTypeDropDown" BlankSelectionText="<%$Resources:MultiLang,PaymentMethod %>" /></div>
                        </div>
                   </div>
                   <div class="percent-40 form-group align-left">
                       <div><asp:Literal runat="server" Text="<%$Resources:MultiLang,ReplyCode %>" /></div>
                       <div><asp:TextBox ID="txtReplyCode" runat="server" placeholder="<%$Resources:MultiLang,ReplyCode %>" CssClass="Field_90" /></div>
                   </div>  
                                
                                                           
                </div>
                <div class="spacer">
                </div>
                <div class="wrap-button-bar">
                    <asp:LinkButton ID="btnShow" runat="server" CssClass="btn btn-inverse btn-cons-short" OnClick="btnShow_Click"><i class="fa fa-search"></i> <asp:Literal  runat="server" Text="<%$Resources:MultiLang,Search %>" /></asp:LinkButton>
                </div>
            </div>
            <asp:PlaceHolder ID="phResults" Visible="false" runat="server">
                <div class="section">
                    <netpay:SearchFiltersView ID="wcFiltersView" runat="server" Visible="false" />
                    <table class="exspand-table" align="center" cellpadding="1" cellspacing="1">
                        <tr>
                            <th>
                                &nbsp;
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:MultiLang,Transaction %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:MultiLang,Date %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:MultiLang,PaymentMethod %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:MultiLang,Amount %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:MultiLang,ReplyCode %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:MultiLang,Fee%>" runat="server" />
                            </th>
                        </tr>
                        <asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server">
                            <ItemTemplate>
                                <tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';"
                                    bgcolor="#F6FEF6">
                                    <td height="16" width="8px" style="text-align: center; <%# IIf(CType(Container.DataItem, Transactions.Transaction).TransType = FailedTransactionType.PreAuthorized, "border:1px solid #ff6666;background-color:#ffffff;", "background-color:#ff6666;") %>">
                                        <%# IIf(CType(Container.DataItem, Transactions.Transaction).IsTest, "X", "&nbsp;") %>
                                    </td>
                                    <td>
                                        <%# CType(Container.DataItem, Transactions.Transaction).ID%>
                                    </td>
                                    <td>
                                        <%# CType(Container.DataItem, Transactions.Transaction).InsertDate%>
                                    </td>
                                    <td>
                                        <netpay:PaymentMethodView ID="PaymentMethodView1" runat="server" PaymentMethodID="<%# CType(Container.DataItem, Transactions.Transaction).PaymentMethodID%>"
                                            PaymentMethodDisplay="<%# CType(Container.DataItem, Transactions.Transaction).PaymentMethodDisplay%>" />
                                    </td>
                                    <td>
                                        <%# CType(Container.DataItem, Transactions.Transaction).Amount.ToAmountFormat(WebUtils.DomainHost, CType(Container.DataItem, Transactions.Transaction).CurrencyID)%>
                                    </td>
                                    <td>
                                        <%# CType(Container.DataItem, Transactions.Transaction).ReplyCode%>
                                    </td>
                                    <td>
                                        <%# IIf(CType(Container.DataItem, Transactions.Transaction).PrimaryPaymentID <> 0, CType(Container.DataItem, Transactions.Transaction).TransactionFee.ToAmountFormat(WebUtils.DomainHost, CType(Container.DataItem, Transactions.Transaction).CurrencyID), "")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="1" colspan="10" bgcolor="silver">
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
                    <div class="xls-wrap">
                        <netpay:ExcelButton ID="btnExportExcel" Text="<%$Resources:MultiLang,ExportToXls %>" Enabled="false" runat="server" OnClick="ExportExcel_Click" />
                    </div>
                </div>
            </asp:PlaceHolder>
        </div>
    </div>
</asp:Content>
