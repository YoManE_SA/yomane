﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Tmp_netpayintl/page.master" CodeBehind="PayoneerBulkPayment.aspx.vb" Inherits="Netpay.MerchantControlPanel.PayoneerBulkPayment" %>

<%@ Import Namespace="Netpay.Bll.ExternalCards" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
	<asp:Localize ID="Localize1" Text="<%$Resources:title %>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">

    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize2" Text="<%$Resources:Contactinfo%>" runat="server" /></div>
        <div class="content-background">
            <div class="section">
                <asp:Label ID="lblContent" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>

	<asp:HiddenField ID="hfCustomerID" runat="server" />
	
	<asp:Label ID="lblError" CssClass="error" runat="server"></asp:Label>
	<asp:Label ID="lblSuccess" CssClass="noError" runat="server"></asp:Label>
    <input type="file" id="filePayments" runat="server" />
	<asp:Button ID="btnProcess" Text="<%$Resources:MultiLang,Pay %>" CssClass="button" OnClick="BulkProcess" runat="server" />
	<netpay:ElementBlocker ID="wcElementBlocker" Visible="true" ElementClientID="divBlocker" Text="<%$ Resources:MultiLang,FeatureDisabled %>" runat="server" />
	<asp:PlaceHolder ID="phResults" Visible="false" runat="server">
		<table class="exspand-table" align="center" cellpadding="1" cellspacing="1" style="width: 92%">
			<tr>
				<th>
					&nbsp;
				</th>
				<th>
					<asp:Literal Text="<%$Resources:MultiLang,ID %>" runat="server"></asp:Literal>
				</th>
				<th>
					<asp:Literal Text="<%$Resources:MultiLang,Amount %>" runat="server"></asp:Literal>
				</th>
				<th>
					<asp:Literal Text="<%$Resources:Result %>" runat="server"></asp:Literal>
				</th>
				<th>
					<asp:Literal Text="<%$Resources:ResultCode %>" runat="server"></asp:Literal>
				</th>
				<th>
					<asp:Literal Text="<%$Resources:ResultDescription %>" runat="server"></asp:Literal>
				</th>
			</tr>					
			<asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server">
				<ItemTemplate>
					<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
						<td width="8px" class="<%# GetCssClass(CType(Container.DataItem, ExternalCardPaymentResult))%>">
							&nbsp;
						</td>
						<td>						
							<%# CType(Container.DataItem, ExternalCardPaymentResult).CustomerID%>
						</td>
						<td>						
							$<%# CType(Container.DataItem, ExternalCardPaymentResult).Amount.ToAmountFormat()%>
						</td>
						<td>
							<%# CType(Container.DataItem, ExternalCardPaymentResult).Result.ToString()%>
						</td>
						<td>
							<%# CType(Container.DataItem, ExternalCardPaymentResult).ResultCode%>
						</td>
						<td>
							<%# CType(Container.DataItem, ExternalCardPaymentResult).ResultDescription%>
						</td>
					</tr>				
					<tr><td height="1" colspan="11" bgcolor="silver"></td></tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>	
	</asp:PlaceHolder>
</asp:Content>