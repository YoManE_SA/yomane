﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" CodeBehind="Payee.aspx.vb" Inherits="Netpay.MerchantControlPanel.Payee" %>

<%@ Register Src="~/Website/Controls/BankAccount.ascx" TagPrefix="uc1" TagName="BankAccount" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTitle" runat="server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBody" runat="server">
    <div class="area-content" runat="server" id="container">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">
            <div class="section">
                <table>
                    <tr>
                        <td>
                            <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:MultiLang,ProfileType %>"></asp:Literal><span>*</span>
                            <asp:TextBox ID="txtSearchTag" CssClass="meduim" runat="server" Text='<%# Item.SearchTag %>' TabIndex="1" />
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:MultiLang,PersonalInfo %>" />
            </div>
            <div class="section">
                <table>
                    <tr>
                        <td class="Field_132">
                            <asp:Literal ID="Literal3" Text="<%$Resources:MultiLang,Identifier%>" runat="server" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtLegalNumber" CssClass="meduim" runat="server" TabIndex="2" Text='<%# Item.LegalNumber %>' />
                        </td>
                        <td class="Field_132">
                            <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:PersonalName %>"></asp:Literal><span>*</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCompanyName" CssClass="meduim" runat="server" TabIndex="3" Text='<%# Item.CompanyName %>' />
                        </td>
                    </tr>
                    <tr>
                        <td class="Field_132">
                            <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:ContactPerson %>" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtContactName" CssClass="meduim" runat="server" TabIndex="4" Text='<%# Item.ContactName %>' />
                        </td>
                        <td class="Field_132">
                            <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:MultiLang,Phone %>"></asp:Literal>
                        </td>
                        <td>
                            <asp:TextBox ID="txtPhoneNumber" CssClass="meduim" runat="server" TabIndex="5" Text='<%# Item.PhoneNumber %>' />
                        </td>
                    </tr>
                    <tr>
                        <td class="Field_132">
                            <asp:Literal ID="Literal7" Text="<%$Resources:MultiLang,Fax%>" runat="server" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtFaxNumber" CssClass="meduim" runat="server" TabIndex="6" Text='<%# Item.FaxNumber %>' />
                        </td>
                        <td class="Field_132">
                            <asp:Literal ID="Literal8" Text="<%$Resources:MultiLang,Email%>" runat="server" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmailAddress" CssClass="meduim" runat="server" TabIndex="7" Text='<%# Item.EmailAddress %>' />
                        </td>
                    </tr>
                    <tr>
                        <td class="Field_132">
                            <asp:Literal ID="Literal9" Text="<%$Resources:MultiLang,Address%>" runat="server" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtStreetAddress" CssClass="meduim" runat="server" TabIndex="8" Text='<%# Item.StreetAddress %>' />
                        </td>
                        <td class="Field_132">
                            <asp:Literal ID="Literal10" Text="<%$Resources:MultiLang,Comment%>" runat="server" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtComment" CssClass="meduim" runat="server" TabIndex="9" Text='<%# Item.Comment %>' />
                        </td>
                    </tr>
                </table>
            </div>
            <asp:MultiView runat="server" ID="mvIsraelBankAccount" ActiveViewIndex='<%# IIf(WebUtils.CurrentLanguageShort = "he", 0, 1) %>'>
                <asp:View runat="server">
                    <div><asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:IsraeliBankDetails %>" /></div>
                    <div class="section">
                        <table>
                            <tr>
                                <td class="Field_132">
                                    <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:BeneficiaryName %>" /><span>**</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtILAccountName" CssClass="meduim" runat="server" TabIndex="10" Text='<%# ILBankAccount.AccountName %>' />
                                </td>
                                <td class="Field_132">
                                    <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:BranchNumber %>" /><span>*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtILBranchCode" CssClass="meduim" runat="server" TabIndex="13" Text='<%# ILBankAccount.ILBranchCode %>' />
                                </td>
                            </tr>
                            <tr>
                                <td class="Field_132">
                                    <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:AccountNumber %>" /><span>*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtILAccountNumber" CssClass="meduim" runat="server" TabIndex="14" Text='<%# ILBankAccount.ILAccountNumber %>'></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Literal ID="Literal18" runat="server" Text="<%$ Resources:BankName %>" /><span>*</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlILBankCode" CssClass="meduim" runat="server" TabIndex="16" >
                                        <asp:ListItem Value="000">[בחר]</asp:ListItem>
                                        <asp:ListItem Value="001">01 - בנק יורו טרייד בע&quot;מ</asp:ListItem>
                                        <asp:ListItem Value="004">04 - בנק יהב לעובדי המדינה בע&quot;מ</asp:ListItem>
                                        <asp:ListItem Value="007">07 - בנק לפתוח התעשיה בישראל בע&quot;מ</asp:ListItem>
                                        <asp:ListItem Value="009">09 - בנק הדואר</asp:ListItem>
                                        <asp:ListItem Value="010">10 - בנק לאומי לישראל בע&quot;מ</asp:ListItem>
                                        <asp:ListItem Value="011">11 - בנק דיסקונט לישראל בע&quot;מ</asp:ListItem>
                                        <asp:ListItem Value="012">12 - בנק הפועלים בע&quot;מ</asp:ListItem>
                                        <asp:ListItem Value="013">13 - בנק אגוד לישראל בע&quot;מ</asp:ListItem>
                                        <asp:ListItem Value="014">14 - בנק אוצר החייל בע&quot;מ</asp:ListItem>
                                        <asp:ListItem Value="017">17 - בנק מרכנתיל דיסקונט בע&quot;מ</asp:ListItem>
                                        <asp:ListItem Value="019">19 - בנק החקלאות לישראל בע&quot;מ</asp:ListItem>
                                        <asp:ListItem Value="020">20 - בנק המזרחי המאוחד בע&quot;מ</asp:ListItem>
                                        <asp:ListItem Value="021">21 - Standard Chartered Bank Ltd</asp:ListItem>
                                        <asp:ListItem Value="022">22 - Citibank N.A</asp:ListItem>
                                        <asp:ListItem Value="023">23 - HSBC  Bank plc</asp:ListItem>
                                        <asp:ListItem Value="026">26 - בנק אינווסטק (ישראל) בע&quot;מ</asp:ListItem>
                                        <asp:ListItem Value="028">28 - בנק קונטיננטל לישראל בע&quot;מ</asp:ListItem>
                                        <asp:ListItem Value="030">30 - בנק למסחר בע&quot;מ (לבנק מונה מפרק זמני)</asp:ListItem>
                                        <asp:ListItem Value="031">31 - הבנק הבינלאומי הראשון לישראל בע&quot;מ</asp:ListItem>
                                        <asp:ListItem Value="034">34 - בנק ערבי ישראלי בע&quot;מ</asp:ListItem>
                                        <asp:ListItem Value="035">35 - בנק פולסקא קאסא אופיקי תל אביב (בנק פאקאו) בע&quot;מ</asp:ListItem>
                                        <asp:ListItem Value="046">46 - בנק מסד בע&quot;מ</asp:ListItem>
                                        <asp:ListItem Value="047">47 - בנק עולמי להשקעות (ב.ה.) בע&quot;מ</asp:ListItem>
                                        <asp:ListItem Value="048">48 - קופת העובד הלאומי לאשראי וחיסכון נתניה</asp:ListItem>
                                        <asp:ListItem Value="052">52 - בנק פועלי אגודת ישראל בע&quot;מ</asp:ListItem>
                                        <asp:ListItem Value="054">54 - בנק ירושלים בע&quot;מ</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:View>
                <asp:View runat="server">
                    <div><asp:Literal ID="Literal19" runat="server" Text="<%$ Resources:AbroadBankDetails %>" /></div>
                    <uc1:BankAccount runat="server" id="BankAccountMaster" />
                    <asp:CheckBox ID="chkCorrespondentAccount" runat="server" Text="<%$ Resources:CorrespondentBankDetails %>" />
                    <div runat="server" id="dvBankAccountCorresponded">
                        <uc1:BankAccount runat="server" id="BankAccountCorresponded" />
                    </div>
                </asp:View>
            </asp:MultiView>
            <div class="Beneficiary-button">
                <div>
                    <asp:Button ID="BtnUpdate" BorderWidth="0" CssClass="btn btn-primary" Text="<%$Resources:MultiLang,Update%>" OnCommand="Button_Command" runat="server" TabIndex="45" Visible='<%# Item.ID > 0 %>' />
                    <asp:Button ID="BtnDelete" BorderWidth="0" CssClass="btn btn-primary" Text="<%$Resources:MultiLang,Delete%>" OnCommand="BtnDelete_Command" runat="server" TabIndex="46" Visible='<%# Item.ID > 0 %>' />
                    <asp:Button ID="Button1" BorderWidth="0" CssClass="btn btn-primary" OnCommand="Button_Command" Text="CREATE " runat="server" TabIndex="47"  Visible='<%# Item.ID = 0 %>' />
                </div>
            </div>
            <div class="spacer">
                <div class="section">
                    <div class="Disclaimer">
                        *
                        <asp:Literal ID="Literal39" runat="server" Text="<%$ Resources:MandatoryField %>" /><br />
                        **
                        <asp:Literal ID="Literal40" runat="server" Text="<%$ Resources:MandatoryField1 %>" /><br />
                        ***
                        <asp:Literal ID="Literal41" runat="server" Text="<%$ Resources:MandatoryField2 %>" /><br />
                        ****
                        <asp:Literal ID="Literal42" runat="server" Text="<%$ Resources:MandatoryField3 %>" /><br />
                    </div>
                </div>
            </div>
            <asp:Label ID="lblError" Style="display: block;" runat="server" />
        </div>
    </div>
</asp:Content>
