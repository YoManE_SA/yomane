﻿<%@ Page Title="PageTitle" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master"
    AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_Reports"
    CodeBehind="Reports.aspx.vb" %>

<%@ Import Namespace="Netpay.Bll.Reports" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="Netpay.Infrastructure.Tasks" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources: Reports.aspx,  PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphhead" runat="Server">
    <script type="text/javascript">
		netpay.Blocker.enabled = false;		
		
		function deleteConfirmation()
		{
			return confirm("<asp:Literal Text="<%$Resources: Reports.aspx,  DeleteConfirm %>" runat="server"></asp:Literal>");
		}  
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
        <asp:Localize ID="locPageTitles" Text="<%$Resources: Reports.aspx,  PageTitle%>" runat="server" /> - <asp:Localize Text="<%$ Resources: Reports.aspx,  Section2Msg %>" runat="server" />
        </div>
        <div class="content-background">
            <div id="txtError" class="error" visible="false" runat="server">
            </div>
            
            <div class="section">
                <div class="alert alert-info margin-bottom-10">
                   <i class="fa fa-info-circle"></i> <asp:Literal Text="<%$ Resources: Reports.aspx, Info_alert %>" runat="server" />
                </div>
                <div>
                  <asp:Localize  Text="<%$ Resources: Reports.aspx,  Section1Msg %>" runat="server" />
					<asp:DropDownList runat="server" ID="ddlReportType" CssClass="Field_200" >
						<asp:ListItem Value="CreateMerchantCapturedTransactionsReport" Text="<%$ Resources: Reports.aspx, ApprovedTransactions %>" />
						<asp:ListItem Value="CreateMerchantDeclinedTransactionsReport" Text="<%$ Resources: Reports.aspx, DeclinedTransactions %>" />
						<asp:ListItem Value="CreateMerchantChargebackTransactionsReport" Text="<%$ Resources: Reports.aspx, ChbTransactions %>" />
						<asp:ListItem Value="CreateMerchantSummaryReport" Text="<%$ Resources: Reports.aspx, ProcessingSummary %>" />
						<asp:ListItem Value="CreateMerchantSettlementReport" Text="<%$ Resources: Reports.aspx, SettlementsReport %>" />
						<asp:ListItem Value="CreateMerchantCustomerTransactionsReport" Text="<%$ Resources: Reports.aspx, CustomersReport %>" />
					</asp:DropDownList>
              &nbsp; &nbsp;
                    <asp:Localize Text="<%$ Resources: Reports.aspx,  TimeFrame %>" runat="server" />
                    <netpay:DateRangePicker ID="wcMonthlyReportDate" HtmlLayout="Flow" OutOfRangeError="<%$ Resources: Reports.aspx,  DateOffsetError %>" MaxRange="366.00:00:00.0000000" runat="server" />
                
                </div>
                <div class="wrap-button-bar">
                   <asp:LinkButton ID="btnGenerateReport" OnClientClick="return netpay.webControls.DateRangePicker.validate()" runat="server" CssClass="btn btn-cons-short btn-inverse"><i class="fa fa-magic"></i> <asp:Literal Text="<%$ Resources: MultiLang, Generate %>" runat="server" /> </asp:LinkButton>
                </div>
            </div>
            <asp:PlaceHolder ID="phQueuedTasks" runat="server">
                <div class="section">
                    <table  cellpadding="1" style="border: 1px solid #d9d9d9; width: 100%; border-spacing: 0;">
                        <tr>
                            <th style="text-align: center; background-color: #e9e9e9;">
                                <asp:Literal ID="literalDescription" Text="<%$ Resources: MultiLang, queued %>" runat="server" />
                            </th>
                            <td style="padding-<%=WebUtils.CurrentAlignment %>: 8px;">
                                <asp:Label ID="lblMessage" Text="<%$ Resources: Reports.aspx, RequestMsg %>" runat="server" />
                            </td>
                            <td>
                                <asp:LinkButton ID="btnRefreshFiles" CssClass="btn btn-default" runat="server"><i class="fa fa-refresh"></i> <asp:Literal Text="<%$ Resources: MultiLang, Refresh %>" runat="server" /></asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phGeneratedReports" runat="server">
                <div class="section">
                    <table class="exspand-table">
                        <thead>
                            <tr>
                                <th>
                                    <asp:Literal ID="Literal1" Text="<%$ Resources: Reports.aspx, FileName %>" runat="server"></asp:Literal>
                                </th>
                                <th>
                                    <asp:Literal ID="Literal2" Text="<%$ Resources: MultiLang, Size %>" runat="server"></asp:Literal>
                                </th>
                                <th>
                                    <asp:Literal ID="Literal3" Text="<%$ Resources: MultiLang, Created %>" runat="server"></asp:Literal>
                                </th>
                                <th>
                                    &nbsp;
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="repeaterQueuedReports" runat="server">
                                <ItemTemplate>
                                    <tr style="background-color: #FFFFCC;">
                                        <td>
                                            <%# CType(Container.DataItem, TaskInfo).Description %>
                                        </td>
                                        <td>
                                            --
                                        </td>
                                        <td>
                                             <asp:Literal runat="server" Text="<%$ Resources: MultiLang, Queued%>" />
                                        </td>
                                        <td style="text-align: center;">
                                            <asp:ImageButton ID="btnCancelReport" CommandArgument="<%# CType(Container.DataItem, TaskInfo).TaskID.ToString() %>"
                                                OnCommand="btnCancelReport_click" ImageUrl="/NPCommon/Images/close-icon.png"
                                                runat="server" AlternateText="<%$ Resources: MultiLang, Cancel %>" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <asp:Repeater ID="repeaterGeneratedReports" runat="server">
                                <ItemTemplate>
                                    <tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='#ffffff';">
                                        <td>
                                            <img alt="" style="vertical-align: middle;" src="/NPCommon/ImgFileExt/14X14/<%# CType(Container.DataItem, GeneratedReportInfo).ReportFile.Extension.Remove(0,1) %>.png" />
                                            <asp:LinkButton ID="btnDownloadFile" CommandArgument='<%# CType(Container.DataItem, GeneratedReportInfo).ReportFile.Name + "|" + CType(Container.DataItem, GeneratedReportInfo).ReportFileType.ToString() %>'
                                                OnCommand="btnDownloadFile_click" runat="server" ForeColor="Silver"><span style="color:black;"><%# CType(Container.DataItem, GeneratedReportInfo).ReportFile.Name %></span></asp:LinkButton><br />
                                        </td>
                                        <td>
                                            <%# CType(Container.DataItem, GeneratedReportInfo).ReportFile.Length.ToFileSize() %><br />
                                        </td>
                                        <td>
                                            <%# CType(Container.DataItem, GeneratedReportInfo).ReportFile.CreationTime.ToString() %><br />
                                        </td>
                                        <td style="text-align: center; padding: 1px 0px;">
                                            <asp:ImageButton ID="btnDeleteFile" CommandArgument='<%# CType(Container.DataItem, GeneratedReportInfo).ReportFile.Name + "|" + CType(Container.DataItem, GeneratedReportInfo).ReportFileType.ToString() %>'
                                                OnCommand="btnDeleteFile_click" ImageUrl="/NPCommon/Images/close-icon.png" OnClientClick="return deleteConfirmation();"
                                                runat="server" AlternateText="<%$ Resources: MultiLang, Delete %>" /><br />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
            </asp:PlaceHolder>
        </div>
    </div>
</asp:Content>
