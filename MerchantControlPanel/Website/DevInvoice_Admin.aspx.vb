﻿Public Class Invoice_Admin
    Inherits MasteredPage

    Protected Sub SaveForm(ByVal o As Object, ByVal e As EventArgs)
        Dim settings As Netpay.Bll.Invoices.MerchantSettings = Netpay.Bll.Invoices.MerchantSettings.Load(Merchant.ID)
        'If settings Is Nothing Then settings = New Invoices.MerchantSettings(Merchant.ID)


        settings.IsCreateInvoice = chkCreateInvoice.Checked
        settings.IsCreateReceipt = chkCreateReceipt.Checked
        settings.IsAutoGenerateILS = chkAutoGenerateILS.Checked
        settings.IsAutoGenerateOther = chkAutoGenerateOther.Checked
        settings.IsAutoGenerateRefund = chkAutoGenerateRefund.Checked
        settings.IsIncludeTax = chkIncludeTax.Checked
        settings.ItemText = txtItemText.Text
        settings.Save()
    End Sub

    Protected Sub LoadData()
        Dim settings As Netpay.Bll.Invoices.MerchantSettings = Netpay.Bll.Invoices.MerchantSettings.Load(Merchant.ID)
        chkCreateInvoice.Checked = settings.IsCreateInvoice
        chkCreateReceipt.Checked = settings.IsCreateReceipt
        chkAutoGenerateILS.Checked = settings.IsAutoGenerateILS
        chkAutoGenerateOther.Checked = settings.IsAutoGenerateOther
        chkAutoGenerateRefund.Checked = settings.IsAutoGenerateRefund
        chkIncludeTax.Checked = settings.IsIncludeTax
        txtItemText.Text = settings.ItemText
    End Sub

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        If Not IsPostBack Then
            Dim enabled As Boolean = False
            Dim settings As Netpay.Bll.Invoices.MerchantSettings = Netpay.Bll.Invoices.MerchantSettings.Load(Merchant.ID)
            If Not settings Is Nothing Then
                enabled = settings.IsEnable
                LoadData()
            End If

            literalInvoiceAccount.Text = settings.ExternalProviderName

            elementBlocker.Visible = Not enabled
            'ebIsEnabled.Visible = Not enabled
            chkCreateInvoice.Enabled = enabled
            chkCreateReceipt.Enabled = enabled
            chkAutoGenerateILS.Enabled = enabled
            chkAutoGenerateOther.Enabled = enabled
            chkAutoGenerateRefund.Enabled = enabled
            chkIncludeTax.Enabled = enabled
            txtItemText.Enabled = enabled
            btnSave.Enabled = enabled
        End If
    End Sub

End Class