﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_RecurringCharges" Codebehind="RecurringCharges.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
    <script language="javascript" type="text/javascript">
        function expandNode(chargeID) {
            trchargeInfo = document.getElementById("trchargeInfo" + chargeID)
            imgExpandNode = document.getElementById("imgExpandNode" + chargeID)

            if (trchargeInfo.style.display == '') {
                imgExpandNode.src = '/NPCommon/Images/tree_expand.gif';
                trchargeInfo.style.display = 'none';
            }
            else {
                imgExpandNode.src = '/NPCommon/Images/tree_collapse.gif';
                trchargeInfo.style.display = '';
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">
            <div class="section">
                <table class="exspand-table" cellpadding="1" cellspacing="1">
                    <tr>
                        <th>
                            &nbsp;
                        </th>
                        <th>
                            <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:MultiLang,ChargeNo  %>" />
                        </th>
                        <th>
                            <asp:Literal ID="Literal2" runat="server" Text="<%$Resources:MultiLang,SeriesNumber  %>" />
                        </th>
                        <th>
                            <asp:Literal ID="Literal3" runat="server" Text="<%$Resources:MultiLang,Type  %>" />
                        </th>
                        <th>
                            <asp:Literal ID="Literal4" runat="server" Text="<%$Resources:MultiLang,PaymentMethod  %>" />
                        </th>
                        <th>
                            <asp:Literal ID="Literal5" runat="server" Text="<%$Resources:MultiLang,ChargeDate  %>" />
                        </th>
                        <th>
                            <asp:Literal ID="Literal6" runat="server" Text="<%$Resources:MultiLang,Amount  %>" />
                        </th>
                    </tr>
                    <asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:PlaceHolder ID="PlaceHolder1" Visible="<%# Not CType(Container.DataItem, RecurringChargeVO).IsBlocked And Not CType(Container.DataItem, RecurringChargeVO).IsPaid %>"
                                        runat="server">
                                        <img height="16" border="0" align="middle" width="16" alt="" src="/NPCommon/Images/tree_expand.gif"
                                            id="imgExpandNode<%# CType(Container.DataItem, RecurringchargeVO).ID %>" style="cursor: pointer;"
                                            onclick="expandNode(<%# CType(Container.DataItem, RecurringChargeVO).ID %>);" />
                                    </asp:PlaceHolder>
                                </td>
                                <td>
                                    <span class="<%# GetStatusClass(CType(Container.DataItem, RecurringChargeVO)) %>">&nbsp;</span>
                                    <span style="width: 25px; text-align: right;">
                                        <asp:Literal ID="Literal7" Text="<%# CType(Container.DataItem, RecurringChargeVO).Number %>"
                                            runat="server"></asp:Literal>
                                    </span>
                                </td>
                                <td>
                                    <asp:Literal ID="Literal8" Text="<%# _series.ID %>" runat="server"></asp:Literal>
                                </td>
                                <td>
                                    <asp:Literal ID="Literal9" Text="<%# GetTypeText(_series.IsPreAuthorized) %>" runat="server"></asp:Literal>
                                </td>
                                <td>
                                    <netpay:PaymentMethodView ID="PaymentMethodView1" PaymentMethodID="<%# _series.PaymentMethodID %>"
                                        BinCountryCode="<%# _series.BinCountryCode %>" PaymentMethodDisplay="<%# _series.PaymentMethodDisplay %>"
                                        runat="server" />
                                </td>
                                <td>
                                    <asp:Literal ID="Literal10" Text='<%# CType(Container.DataItem, RecurringChargeVO).ChargeDate.ToString("dd.MM.yyyy") %>'
                                        runat="server"></asp:Literal>
                                </td>
                                <td align="right">
                                    <asp:Literal ID="Literal11" Text="<%# CType(Container.DataItem, RecurringChargeVO).Amount.ToAmountFormat(CType(Container.DataItem, RecurringChargeVO).Currency) %>"
                                        runat="server"></asp:Literal>
                                </td>
                            </tr>
                            <tr style="display: none;" id="trchargeInfo<%# CType(Container.DataItem, RecurringChargeVO).ID %>">
                                <td />
                                <td class="showDetails" colspan="8">
                                    <asp:LinkButton ID="btnCancel" Text="<%$Resources:MultiLang,Cancel %>" OnCommand="SuspendCharge"
                                        CommandArgument="<%# CType(Container.DataItem, RecurringChargeVO).ID %>" Visible="<%# Not CType(Container.DataItem, RecurringChargeVO).IsSuspended And Not CType(Container.DataItem, RecurringChargeVO).IsBlocked And Not CType(Container.DataItem, RecurringChargeVO).IsPaid %>"
                                        runat="server" />
                                    <asp:LinkButton ID="btnReinstate" Text="<%$Resources:MultiLang,Reinstate %>" OnCommand="ResumeCharge"
                                        CommandArgument="<%# CType(Container.DataItem, RecurringChargeVO).ID %>" Visible="<%# CType(Container.DataItem, RecurringChargeVO).IsSuspended And Not CType(Container.DataItem, RecurringChargeVO).IsBlocked And Not CType(Container.DataItem, RecurringChargeVO).IsPaid %>"
                                        runat="server" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:HyperLink ID="btnEdit" NavigateUrl="<%# GetEditUrl(CType(Container.DataItem, RecurringChargeVO)) %>"
                                        Text="<%$Resources:MultiLang,Edit %>" Visible="<%# Not CType(Container.DataItem, RecurringChargeVO).IsPaid And Not CType(Container.DataItem, RecurringChargeVO).IsBlocked %>"
                                        runat="server"></asp:HyperLink>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
