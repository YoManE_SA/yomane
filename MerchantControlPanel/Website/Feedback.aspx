﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Feedback.aspx.vb" Inherits="Netpay.MerchantControlPanel.Feedback" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Send Feedback</title>
    <meta charset="utf-8" />
    <meta name="robots" content="noindex, nofollow" />
    <link rel="stylesheet" href="<%= WebUtils.MapTemplateVirPath(Me, "Website/Styles/style.css")%>" />
    <script type="text/javascript" src="Plugins/jquery-ui-1.8.10.custom/js/jquery-1.4.4.min.js"></script>
        <script type="text/javascript" src="Plugins/jquery-ui-1.8.10.custom/js/jquery-ui-1.8.10.custom.min.js"></script>
    <%  If System.Threading.Thread.CurrentThread.CurrentUICulture.TextInfo.IsRightToLeft Then%>
    <link rel="stylesheet" href="<%= WebUtils.MapTemplateVirPath(Me, "Website/Styles/loginRTL.css")%>" />
    <% End If%>

    <script type="text/javascript">
        function SiteFeedbackUpdateView() {
            document.getElementById("SiteFeedback_Subject").disabled = !document.getElementById("SiteFeedback_Other").checked;
            if (document.getElementById("SiteFeedback_Site").checked) document.getElementById("SiteFeedback_Subject").value = 'SITE';
            else if (document.getElementById("SiteFeedback_Page").checked) document.getElementById("SiteFeedback_Subject").value = document.URL;
            else document.getElementById("SiteFeedback_Subject").value = '';
        }

        function SiteFeedbackSend() {
            var subject = document.getElementById("SiteFeedback_Subject").value;
            var body = document.getElementById("SiteFeedback_Body").value;
            if (body.length < 1 || subject.length < 1) { $('#siteFeedbackErrorLine').show(); return; };

            top.netpay.ajaxApi.AjaxMethods.GetSiteFeedback(subject, body, function (response) { $("body").html('<center><h3><%= WebUtils.GetResource("Ajax.SiteFeedback").GetString("SiteFeedbackDone") %></h3></center>') });
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            SiteFeedbackUpdateView();
            $('#SiteFeedback_Body').text(function (index, currentText) {
                return currentText.substr(0, 175);
            });

            //$('#SiteFeedback_Body').limit("140", "#charsLeft");

        });
    </script>
</head>
<body>
    <div class="wrap-feedback">
      
        <div class="body-feedback">
            <div class="margin-bottom-10">
                <%= WebUtils.GetResource("Ajax.SiteFeedback").GetString("Header") %>
            </div>
            <div>
                <div id='siteFeedbackErrorLine' style='display: none;' class="error"><%= WebUtils.GetResource("Ajax.SiteFeedback").GetString("NoContent")%></div>
            </div>
            <div class="form-group percent100">
                <ul class="feedback-subject">
                    <li>
                        <%= WebUtils.GetResource("Common").GetString("Subject") %>:
                    </li>
                    <li>
                        <input id="SiteFeedback_Page" onclick="SiteFeedbackUpdateView()" checked="checked" name="SiteFeedback_Type" type="radio" />
                        <%= WebUtils.GetResource("Ajax.SiteFeedback").GetString("FeedBackCurrentPage")%></li>
                    <li>
                        <input id="SiteFeedback_Site" onclick="SiteFeedbackUpdateView()" name="SiteFeedback_Type" type="radio" />
                        <%= WebUtils.GetResource("Ajax.SiteFeedback").GetString("FeedBackOveralSite")%></li>
                    <li>
                        <input id="SiteFeedback_Other" onclick="SiteFeedbackUpdateView()" name="SiteFeedback_Type" type="radio" />
                        <%= WebUtils.GetResource("Ajax.SiteFeedback").GetString("FeedBackOther")%></li>
                </ul>
            </div>
            <div class="form-group percent100">
                <span class="form-group-title">Url:</span>
                <div>
                    <input id="SiteFeedback_Subject" type="text" class="percent100" />
                </div>
            </div>
            <div class="form-group percent100">
                <span class="form-group-title">Body:</span>
                <div>
                    <textarea id="SiteFeedback_Body" class="textarea-feedback percent100"></textarea>
                </div>
            </div>
            <div class="margin-top-10 text-align-right">
                <input type="button" class="btn btn-primary" onclick="SiteFeedbackSend()" value="<%= WebUtils.GetResource("Common").GetString("Send") %>" id="SiteFeedback_SendButton" />
            </div>
            <div class="alert">
                <%= WebUtils.GetResource("Ajax.SiteFeedback").GetString("Footer") %>
            </div>
        </div>
    </div>
</body>
</html>
