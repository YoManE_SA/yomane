﻿Imports System.IO
Imports Netpay.Infrastructure.VO
Imports Netpay.Bll
Imports Netpay.Bll.ExternalCards
Imports Netpay.Infrastructure

Public Class PayoneerBulkPayment
    Inherits MasteredPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            wcElementBlocker.Visible = Not Netpay.Bll.ExternalCards.IsProviderAvailable(ExternalCardProviderEnum.Payoneer)
        End If
    End Sub

    Protected Sub BulkProcess(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim reader As New StreamReader(filePayments.PostedFile.InputStream)
        Dim results As List(Of ExternalCardPaymentResult) = Netpay.Bll.ExternalCards.PayToCustomers(LoggedUser.CredentialsToken, reader.ReadToEnd())
        repeaterResults.DataSource = results
        repeaterResults.DataBind()

        If results IsNot Nothing Then
            phResults.Visible = (results.Count > 0)
        End If
    End Sub

    Protected Function GetCssClass(result As ExternalCardPaymentResult) As String
        If result.Result = Bll.ExternalCardPaymentResultEnum.Success Then
            Return "payoneerNoError"
        Else
            Return "payoneerError"
        End If
    End Function
End Class