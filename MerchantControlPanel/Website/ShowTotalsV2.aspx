﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_ShowTotalsV2" Codebehind="ShowTotalsV2.aspx.vb" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Trans Total</title>
</head>
<body style="padding:10px; background-color:White!important; background-image:none!important;">
   <table width="100%"  border="0" cellspacing="0" cellpadding="0">	
		<tr>
			<td align="left">
				<netpay:Totals3 runat="server" ID="Total1" />
			</td>
		</tr>
		<tr>
			<td align="left">
				<br />
				<asp:Literal ID="litCloseWindow" runat="server" />
			</td>
		</tr>
	</table>
</body>
</html>
