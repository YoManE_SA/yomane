﻿<%@ Page Title="PageTitle" Language="VB" AutoEventWireup="true" Inherits="Netpay.MerchantControlPanel.ShopProducts" CodeBehind="ShopProducts.aspx.vb" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Website/Controls/ShareLinks.ascx" TagPrefix="uc" TagName="ShareLinks" %>
<%@ Register Src="~/Website/Controls/ShopsWizard.ascx" TagPrefix="uc" TagName="ShopsWizard" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:  ProductsPageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Literal runat="server" Text="<%$Resources:  Titleproud %>" />
        </div>
        <div class="content-background">
           
               <uc:ShopsWizard runat="server" id="ShopsWizard" />
          
            <asp:DropDownList runat="server" ID="ddlLanguage" Visible="false" AppendDataBoundItems="true" onchange="document.location='?PrdLanguage=' + options[selectedIndex].value" CssClass="Field_200">
                <asp:ListItem Text="<%$Resources:  ShowInAllLanguages %>" Value="" />
            </asp:DropDownList>
            <div class="section">
                <div class="form-group margin-bottom-10 align-left percent80">
                    <span class="form-group-title">
                        <asp:Literal runat="server" Text="<%$Resources:  SearchbytextStr %>" /></span>&nbsp;<span class="form-group-title-eg"><asp:Literal runat="server" Text="<%$Resources:  exampleStr %>" /></span>
                    <div>
                        <asp:TextBox ID="txtText" runat="server" class="percent100" />
                    </div>
                </div>

                <div class="form-group margin-bottom-10 align-left percent15">
                    <span class="form-group-title">
                        <asp:Literal runat="server" Text="<%$Resources:  CatagoryStr %>" /></span>&nbsp;<span class="form-group-title-eg"><asp:Literal runat="server" Text="<%$Resources:  exampleFoodStr %>" /></span>
                    <div>
                        <netpay:DropDownBase runat="server" ID="ddlCategory" class="percent100" EnableBlankSelection="true" BlankSelectionText="All" DataTextField="Name" DataValueField="ID" />
                    </div>
                </div>
                <div class="spacer"></div>
                <div class="wrap-button-bar">
                    <asp:LinkButton runat="server" ID="btnSearch" OnClick="Search_Click" CssClass="btn btn-cons-short btn-inverse"><asp:Literal runat="server" Text="<%$Resources:  SearchStr %>" /></asp:LinkButton>
                </div>
            </div>
            <div class="section">
                <div class="shops-product-table">
                   <div class="alert alert-info">
                           <asp:Literal runat="server" Text="<%$Resources: ProductsDscTable %>" />
                   </div>
                </div>

               <div class="margin-bottom-10">
               <a href="ShopProduct.aspx?shopId=<%= CurrentShop.ShopId %>" class="btn btn-success btn-cons-short"><i class="fa fa-plus-circle"></i> <asp:Literal runat="server" Text="<%$Resources:  AddNewStr%>" /></a>
            </div>
             
               <asp:Repeater runat="server" ID="rptItems" OnItemDataBound="Product_OnItemDataBound" OnItemCommand="Product_OnItemCommand">
                        <HeaderTemplate>
                            <table class="exspand-table">
                                <tr>
                                    <th>
                                        <asp:Literal runat="server" Text="<%$Resources:  ID %>" /></th>
                                    <th>
                                        <asp:Literal runat="server" Text="<%$Resources:  ProudctName %>" /></th>
                                    <th>
                                        <asp:Literal runat="server" Text="<%$Resources:  Amount %>" /></th>
                                    <th>
                                        <asp:Literal runat="server" Text="<%$Resources:  Proudctimg %>" /></th>
                                    <th>
                                        <asp:Literal runat="server" Text="<%$Resources:  Available %>" /></th>
                                    <th>
                                        <asp:Literal runat="server" Text="<%$Resources:  Order %>" /></th>
                                    <th>
                                        <asp:Literal runat="server" Text="<%$Resources:  Visable %>" /></th>
                                   
                                    <th>
                                         <asp:Literal runat="server" Text="<%$Resources:  Actions %>" />
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td class="text-align-center">
                                  <asp:HyperLink runat="server" NavigateUrl='<%# "ShopProduct.aspx?ID=" & Eval("ID") & "&shopId=" & CurrentShop.ShopId  %>' Text='<%# Eval("ID") %>' /></td>
                                <td>
                                     <asp:HyperLink runat="server" NavigateUrl='<%# "ShopProduct.aspx?ID=" & Eval("ID") & "&shopId=" & CurrentShop.ShopId  %>' Text='<%# CType(Container.DataItem, Netpay.Bll.Shop.Products.Product).GetTextForLanguage(Nothing).Name %>' /></td>
                                <td style="direction: ltr; text-align: center;"> <%# IIf(CType(Eval("IsDynamicProduct"), Boolean), GetLocalResourceObject("Unlimited"), CType(Eval("Price"), Decimal).ToAmountFormat(CType(Eval("CurrencyIsoCode"), String)))%></td>
                                <td class="text-align-center">
                                    <asp:Image runat="server" ID="imgProduct" Width="150" Height="51" />&nbsp;</td>
                              
                                <td class="text-align-center"><%# Eval("QtyAvailable") %></td>
                                <td class="text-align-center">
                                    <asp:UpdatePanel runat="server" ID="upOrder" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:MultiView runat="server" ID="viewStatus" ActiveViewIndex="0">
                                                <asp:View runat="server" ID="viewNormal">
                                                    <asp:Literal runat="server" ID="ltOrder" Text='<%# Eval("SortOrder") %>' />
                                                    <asp:LinkButton runat="server" ID="btnOrderChange" CommandName="ShowOrder" Font-Size="15px" CommandArgument='<%# Eval("ID") %>'><i runat="server" id="imageEdit" class="fa fa-pencil-square-o"></i></asp:LinkButton>
                                                    <i runat="server" class="statusImage fa fa-check-circle" style="display: none;" id="imageSaved"></i>
                                                </asp:View>
                                                <asp:View runat="server" ID="viewEdit">
                                                    <div class="pull-left">
                                                        <asp:TextBox runat="server" ID="txtOrder" Text='<%# Eval("SortOrder") %>' Width="40" /></div>
                                                    <div class="pull-left saveImageicon">
                                                        <asp:LinkButton runat="server" ID="btnOrderSet" ForeColor="#088159" Font-Size="15px" CommandName="SetOrder" CommandArgument='<%# Eval("ID") %>'><i class="fa fa-floppy-o"></i></asp:LinkButton></div>
                                                </asp:View>
                                            </asp:MultiView>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="btnOrderChange" />
                                            <asp:AsyncPostBackTrigger ControlID="btnOrderSet" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:UpdateProgress runat="server" AssociatedUpdatePanelID="upOrder" DisplayAfter="0">
                                        <ProgressTemplate><i class="fa fa-circle-o-notch fa-spin"></i></ProgressTemplate>
                                    </asp:UpdateProgress>
                                </td>
                                <td class="text-align-center">
                                    <asp:UpdatePanel runat="server" ID="tpStatus" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:LinkButton runat="server" ID="btnEnable" Font-Size="17" ForeColor="Gray" ClientIDMode="Static" CommandName="EnableItem" CommandArgument='<%#Eval("ID")%>' Visible='<%# Not CType(Eval("IsActive"), Boolean)%>'><i class="fa fa-toggle-off"></i></asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="btnDisable" Font-Size="17" ForeColor="#1D9E74" ClientIDMode="Static" CommandName="DisableItem" CommandArgument='<%#Eval("ID")%>' Visible='<%# CType(Eval("IsActive"), Boolean)%>'><i class="fa fa-toggle-on"></i></asp:LinkButton>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="btnEnable" />
                                            <asp:AsyncPostBackTrigger ControlID="btnDisable" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                              
                                <td>
                                    <asp:LinkButton Font-Size="12" class="btn btn-danger btn-small" title="<%$Resources: Delete %>" runat="server" CommandName="DeleteItem" CommandArgument='<%#Eval("ID")%>' OnClientClick="if(!confirm('Are you sure ?')) return false;"><i class="fa fa-fw fa-trash-o white-color"></i></asp:LinkButton>
                                    <asp:HyperLink Font-Size="12" class="btn btn-info btn-small" runat="server" title="<%$Resources: Edit %>" NavigateUrl='<%# "ShopProduct.aspx?ID=" & Eval("ID") & "&shopId=" & CurrentShop.ShopId  %>'><i class="fa fa-fw fa-edit  white-color"></i></asp:HyperLink>
                                    <a class="btn btn-success btn-small white-color" title="Preview" href='<%= CurrentShop.ShopUrl %>/product/<%# Eval("ID") %>' target="_blank" style="font-size: 12pt"><i class="fa fa-fw fa-eye white-color"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="9" style="display: none; padding: 0px; margin: 0px;"></td>
                            </tr>
                            
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
               <netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
                   
    </div>
    <asp:PlaceHolder runat="server" ID="phShare" Visible="false">
                <script type="text/javascript">
                    $(function () {
                        $("#dvShareProduct").dialog({
                            resizable: false,
                            modal: true,
                            draggable: false,
                            autoOpen: true,
                            show: {
                                effect: "fade",
                                duration: 1000
                            },
                            hide: {
                                effect: "fade",
                                duration: 1000
                            }
                        });
                    });
                </script>
                <div id="dvShareProduct" style="width: 400px; display: none; padding: 20px;">
                    <div class="alert-info">
                        <i class="fa fa-share"></i>
                        <asp:Literal runat="server" Text="<%$Resources:  ShareFriendsStr %>" />
                    </div>
                    <div class="margin-top-10">
                        <uc:ShareLinks shareurl='<%#ShareProductLink %>' runat="server" ID="ShareLinksFinish" />
                    </div>
                    <div class="spacer"></div>
                </div>
            </asp:PlaceHolder>
        </div>
    </div>
</asp:Content>






