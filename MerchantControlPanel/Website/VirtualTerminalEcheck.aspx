﻿<%@ Page Title="PageTitle" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master"
    AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_VirtualTerminalEcheck" MaintainScrollPositionOnPostback="true"
    CodeBehind="VirtualTerminalEcheck.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <script language="JavaScript" type="text/javascript">
        function dopostddlstate() {
            var IndexValue = document.getElementById('<%=ddlCountry.ClientID %>').selectedIndex;
            var SelectedVal = document.getElementById('<%=ddlCountry.ClientID %>').options[IndexValue];
            var DivUsa = document.getElementById('<%=DivUsa.ClientID %>');
            var DivCanada = document.getElementById('<%=DivCanada.ClientID %>');

            if (SelectedVal.innerHTML == "United States") {
                document.getElementById('<%= ddlStateUsa.ClientID %>').disabled = false
                DivUsa.style.display = 'block';
                DivCanada.style.display = 'none';
            }
            else {
                document.getElementById('<%= ddlStateUsa.ClientID %>').disabled = true
                document.getElementById('<%= ddlStateUsa.ClientID %>').value = 0
                document.getElementById('<%= ddlStateCanada.ClientID %>').value = 0
            }

            if (SelectedVal.innerHTML == "Canada") {
                document.getElementById('<%= ddlStateCanada.ClientID %>').disabled = false
                DivUsa.style.display = 'none';
                DivCanada.style.display = 'block';
            }
            else {
                document.getElementById('<%= ddlStateCanada.ClientID %>').disabled = true
                document.getElementById('<%= ddlStateCanada.ClientID %>').value = 0
                document.getElementById('<%= ddlStateUsa.ClientID %>').value = 0
            }
        }

        function ShowRecurringInput() {
            var chkIsUseRecurring = document.getElementById('<%=chkIsUseRecurring.ClientID %>');
            var RecurringMode = document.getElementById('<%=RecurringMode.ClientID %>');
            var RecurringCycle = document.getElementById('<%=RecurringCycle.ClientID %>');
            var RecurringCount = document.getElementById('<%=RecurringCount.ClientID %>');

            if (chkIsUseRecurring.checked) {
                RecurringMode.disabled = false;
                RecurringCycle.disabled = false;
                RecurringCount.disabled = false;
                document.getElementById("RecurringTxt1").style.color = "black";
                document.getElementById("RecurringTxt2").style.color = "black";
                document.getElementById("RecurringTxt3").style.color = "black";
                document.getElementById("RecurringReq1").style.color = "maroon";
                document.getElementById("RecurringReq2").style.color = "maroon";
                document.getElementById("RecurringReq3").style.color = "maroon";
            }
            else {
                RecurringMode.disabled = true;
                RecurringCycle.disabled = true;
                RecurringCount.disabled = true;
                document.getElementById("RecurringTxt1").style.color = "gray";
                document.getElementById("RecurringTxt2").style.color = "gray";
                document.getElementById("RecurringTxt3").style.color = "gray";
                document.getElementById("RecurringReq1").style.color = "gray";
                document.getElementById("RecurringReq2").style.color = "gray";
                document.getElementById("RecurringReq3").style.color = "gray";
            }
        }
    </script>
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content" id="divForm">
        <netpay:ElementBlocker ID="wcElementBlocker" ElementClientID="divForm" Text="<%$ Resources:MultiLang,FeatureDisabled%>" runat="server" />
        <asp:Label ID="lblResult" runat="server" />

        <div class="top-nav">

            <asp:Localize ID="Localize1" Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">

            <div class="section">

                <div class="margin-bottom-10 frame-border-option">
                    <div class="margin-bottom-10"><i class="fa fa-bank"></i> <asp:Literal runat="server" Text="<%$Resources:BankDetails %>" /></div> 
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">
                        <div>
                            <asp:Literal runat="server" Text="<%$Resources:BexactName %>" /><span class="asterisk">*</span>
                        </div>
                        <div>
                            <asp:TextBox ID="txtaccountName" MaxLength="50" CssClass="percent50" TabIndex="4" runat="server" />
                        </div>
                    </div>
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">
                        <div class="align-left percent33">
                            <div>
                                <asp:Literal runat="server" Text="<%$Resources:IDNumber %>"></asp:Literal>
                                <span class="asterisk">*</span>
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtPersonalNum" CssClass="percent85" MaxLength="50" TabIndex="4" runat="server" />
                            </div>

                            <div>
                                <asp:Literal runat="server" Text="<%$Resources:RoutingNumber %>"></asp:Literal><span
                                    class="asterisk">*</span>
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtroutingNumber" TabIndex="4" CssClass="percent85" runat="server" />
                            </div>
                            <div>
                                <asp:Literal runat="server" Text="<%$Resources:AccountNumber %>"></asp:Literal>
                                <span class="asterisk">*</span>
                                <netpay:PageItemHelp ID="PageItemHelp1" runat="server">
                                    <img alt="" src="/NPCommon/Images/echeck_help_large.gif" />
                                </netpay:PageItemHelp>
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtAccountNumber" CssClass="percent85" TabIndex="4" runat="server" />

                            </div>


                        </div>
                        <div class="align-left percent33">

                            <div>
                                <asp:Literal runat="server" Text="<%$Resources:AccountType %>" />
                                <span class="asterisk">*</span>
                                <netpay:PageItemHelp ID="PageItemHelp2" runat="server">
                                    <img src="/NPCommon/Images/echeck_help_large.gif" />
                                </netpay:PageItemHelp>


                            </div>
                            <div class="margin-bottom-10">
                                <asp:DropDownList ID="ddlBankAccountType" runat="server" CssClass="percent85">
                                    <asp:ListItem Value="1" Text="<%$Resources:Checking %>"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="<%$Resources:Savings %>"></asp:ListItem>
                                </asp:DropDownList>

                            </div>

                            <div>
                                <asp:Literal ID="litBankName" runat="server" Text="<%$Resources:BankName %>" /><%=Validation.Mandatory%>
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtBankName" CssClass="percent85" TabIndex="4" runat="server" />
                            </div>

                            <div>
                                <asp:Literal runat="server" Text="<%$Resources:BankPhone %>" />
                                <span class="asterisk">*</span>
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtBankPhone" CssClass="percent85" TabIndex="4" runat="server" />

                            </div>


                        </div>
                        <div class="align-left percent33">
                            <div>
                                <asp:Literal runat="server" Text="<%$Resources:BankState %>" />
                                <span class="asterisk">*</span>
                            </div>
                            <div class="margin-bottom-10">
                                <netpay:StateDropDown ID="ddlBankState" runat="server" CssClass="percent85">
                                </netpay:StateDropDown>
                            </div>

                            <div>
                                <asp:Literal runat="server" Text="<%$Resources:BankCity  %>" />
                                <span class="asterisk">*</span>
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtBankCity" CssClass="percent85" TabIndex="4" runat="server" />
                            </div>
                        </div>
                        <div class="spacer"></div>
                    </div>
                    <div class="margin-bottom-10"><i class="fa fa-user"></i> <asp:Literal runat="server" Text="<%$Resources:CustomerDetails %>" /></div>
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">

                        <div class="align-left percent33">
                            <div>
                                <asp:Literal runat="server" Text="<%$Resources:PhoneNumber %>" />
                                <span class="asterisk">*</span>
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtPhoneNumber" CssClass="percent85" TabIndex="4" runat="server" />
                            </div>
                        </div>

                        <div class="align-left percent33">

                            <div>
                                <asp:Literal runat="server" Text="<%$Resources:MultiLang,Email %>" />
                                <span class="asterisk">*</span>
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtEmail" CssClass="percent85" TabIndex="4" runat="server" />
                            </div>
                        </div>

                        <div class="align-left percent33">
                            <div>
                                <asp:Literal runat="server" Text="<%$Resources:BirthDate %>" />
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtBirthDate" CssClass="percent85" TabIndex="4" runat="server" />
                            </div>

                        </div>

                        <div class="spacer"></div>

                        <div>
                            <asp:Literal runat="server" Text="<%$Resources:Memo %>" />
                        </div>
                        <div class="margin-bottom-10">
                            <asp:TextBox ID="txtComment" CssClass="percent100 percent-height-10" TextMode="MultiLine" TabIndex="4" runat="server" />
                        </div>
                    </div>
                    <div class="margin-bottom-10"><i class="fa fa-building"></i> <asp:Literal runat="server" Text="<%$Resources:MultiLang,Billingaddress %>" /></div>
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">

                        <div class="align-left percent33">

                            <div>
                                <asp:Literal runat="server" Text="<%$Resources:MultiLang, Address1 %>" /><span class="asterisk">*</span>
                                <netpay:PageItemHelp ID="PageItemHelp4" Text="<%$Resources:MultiLang,AddressLine1Help%>" runat="server" />
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtBillingAddress1" TabIndex="4" runat="server" CssClass="percent85" />
                            </div>

                            <div>
                                <asp:Literal runat="server" Text="<%$Resources:MultiLang,Country %>" /><span class="asterisk">*</span>
                            </div>
                            <div class="margin-bottom-10">
                                <netpay:CountryDropDown ID="ddlCountry" runat="server" onchange="javascript:dopostddlstate()"
                                    AutoPostBack="false" TabIndex="10" CssClass="percent85">
                                </netpay:CountryDropDown>
                            </div>



                        </div>
                        <div class="align-left percent33">

                            <div>
                                <asp:Literal runat="server" Text="<%$Resources:MultiLang, Address2 %>"></asp:Literal>
                                <netpay:PageItemHelp ID="PageItemHelp3" Text="<%$Resources:MultiLang,AddressLine2Help%>" runat="server" />
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtBillingAddress2" TabIndex="4" runat="server" CssClass="percent85" />
                            </div>

                            <div>
                                <asp:Literal runat="server" Text="<%$Resources:MultiLang, State %>" />
                                <span class="asterisk" style="display: none;" id="span_stateRequired">*</span>
                            </div>
                            <div class="margin-bottom-10">
                                <div class="headTD" id="DivUsa" runat="server">
                                    <netpay:StateDropDown ID="ddlStateUsa" CountryID="228" CssClass="percent85" TabIndex="10"
                                        runat="server">
                                    </netpay:StateDropDown>
                                </div>
                                <div class="headTD" id="DivCanada" runat="server">
                                    <netpay:StateDropDown ID="ddlStateCanada" CountryID="43" CssClass="percent85" TabIndex="13"
                                        runat="server">
                                    </netpay:StateDropDown>
                                </div>

                            </div>
                        </div>

                        <div class="align-left percent33">
                            <div>
                                <asp:Literal runat="server" Text="<%$Resources:MultiLang, CCity %>"></asp:Literal>
                                <span class="asterisk">*</span>

                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtBillingCity" TabIndex="4" runat="server" CssClass="percent85" />
                            </div>

                            <div>
                                <asp:Literal runat="server" Text="<%$Resources:MultiLang, Zipcode %>" />
                                <span class="asterisk">*</span>
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtBillingZipCode" CssClass="percent85" TabIndex="4" runat="server" />
                            </div>

                        </div>
                        <div class="spacer"></div>
                    </div>
                    <div class="margin-bottom-10"><i class="fa fa-money"></i> <asp:Literal runat="server" Text="<%$Resources:MultiLang,TransactionDetails  %>" /></div>
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">
                        <div class="align-left percent33">
                            <div>
                                <asp:Literal runat="server" Text="<%$Resources:MultiLang,Amount %>" />
                                <span class="asterisk">*</span>
                            </div>
                            <div>
                                <asp:TextBox ID="txtAmount" TabIndex="4" CssClass="percent85" runat="server" />
                            </div>
                        </div>
                        <div class="align-left percent33">
                            <div>
                                <asp:Literal runat="server" Text="<%$Resources:MultiLang,CreditType %>" />
                                <span class="asterisk">*</span>
                            </div>
                            <div>
                                <asp:DropDownList ID="ddlCreditType" runat="server" CssClass="percent85">
                                    <asp:ListItem Text="<%$Resources:MultiLang,Regular %>" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="<%$Resources:MultiLang,Refunds %>" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="spacer"></div>
                    </div>
                    <div class="margin-bottom-10"><i class="fa fa-refresh"></i> <asp:Literal runat="server" Text="<%$Resources:MultiLang,RecurringTransaction %>" /></div>
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">
                        <div class="margin-bottom-10">
                            <asp:CheckBox runat="server" ID="chkIsUseRecurring" OnCheckedChanged="ShowHideRecurring" onclick="netpay.Blocker.enabled=false;" CssClass="option" AutoPostBack="true" />
                            <asp:Literal ID="litRecurringNotAllowed" runat="server" />
                            <asp:Literal runat="server" Text="<%$Resources:MultiLang,CreateRecurring %>" />

                        </div>
                        <div class="line-height-35">

                            <asp:Label ID="lblRecurringMode" runat="server" Text="<%$Resources:IntervalBetweenCharges %>" />
                            <asp:Label ID="lblRecurringRequiredMode" runat="server" Text="*" />
                            <asp:TextBox ID="RecurringCycle" CssClass="Field_60" runat="server" Text="1" />
                            <asp:DropDownList ID="RecurringMode" runat="server" CssClass="Field_120">
                                <asp:ListItem Value="" Text=""></asp:ListItem>
                                <asp:ListItem Value="D" Text="<%$Resources:Days %>"></asp:ListItem>
                                <asp:ListItem Value="W" Text="<%$Resources:Weeks %>">></asp:ListItem>
                                <asp:ListItem Value="M" Text="<%$Resources:Months %>" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="Q" Text="<%$Resources:Quarters %>"></asp:ListItem>
                                <asp:ListItem Value="Y" Text="<%$Resources:Years %>"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="lblRecurringRequiredCount" runat="server" Text="*" />
                            <asp:Label ID="lblRecurringCount" runat="server" Text="<%$Resources:NumberOfCharges %>" />
                            <asp:TextBox ID="RecurringCount" CssClass="Field_60" runat="server" />

                        </div>
                    </div>
                    <div class="require align-left">
                        *<asp:Literal Text="<%$Resources:MultiLang,RequiredFields %>" runat="server" />
                    </div>
                    <div class="wrap-button">
                        <asp:LinkButton ID="BtnCharge" Text="<%$Resources:MultiLang,Charge %>" CssClass="btn btn-cons-short btn-success" TabIndex="15" OnClick="BtnCharge_OnClick" runat="server" />
                    </div>

                </div>








            </div>
        </div>




    </div>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cphHead">
</asp:Content>
