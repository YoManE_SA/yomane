﻿Imports System.Collections.Generic
Imports Netpay.Web.Controls
Imports Netpay.Bll
Imports Netpay.Infrastructure
Imports System.Linq

Partial Class Website_CreditCardStorage
    Inherits MasteredPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim processSettings As Bll.Merchants.ProcessSettings = Merchant.ProcessSettings
        If processSettings.IsCcStorage Then wcElementBlocker.Visible = False
        If Not Page.IsPostBack Then
            ddlExpYear.YearFrom = Date.Now.Year - 5
            ddlExpYear.YearTo = Date.Now.Year + 5
            OnPageChanged(sender, e)
        End If
    End Sub

    Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs)
		pager.Info.PageCurrent = 0
        OnPageChanged(sender, e)
        pager.Visible = True
	End Sub

	Sub btnAdd_Click(ByVal o As Object, ByVal e As System.EventArgs)
		Response.Redirect("CreditCardStorageCreate.aspx")
	End Sub

 	Protected Sub OnPageChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim filters = New Bll.Merchants.MethodStorage.SearchFilters()
		filters.InsertDate.From = wcDateRangePicker.FromDate
		filters.InsertDate.To = wcDateRangePicker.ToDate
		filters.PaymentMethod = ddlCardType.SelectedValue.ToNullableInt32()
		filters.Owner_Name = txtFindCardFullName.Text.NullIfEmpty()
		filters.PM_Last4 = txtFindLastDigit.Text.NullIfEmpty()
		filters.MerchantID = Merchant.ID
		filters.PM_ExpirationDate = New Infrastructure.Range(Of Nullable(Of DateTime)) With { .To = New DateTime(ddlExpYear.SelectedYear.GetValueOrDefault(1900), ddlExpMonth.SelectedMonth.GetValueOrDefault(1), 1).AddMonths(1).AddDays(-1) }
		if isCcDel.SelectedValue = "0" Then
            filters.IsActive = True
            btnCancel.Text = GetGlobalResourceObject("MultiLang", "Remove")
        Else
            filters.IsActive = False
            btnCancel.Text = GetGlobalResourceObject("MultiLang", "Activate")
		End If

        repeaterResults.DataSource = Bll.Merchants.MethodStorage.Search(filters, pager.Info)
		repeaterResults.DataBind()
		phResult.Visible = (repeaterResults.Items.Count > 0)
	End Sub

	Sub OnClick_BtnCancel(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim switchList As New List(Of String)(Split(Request("CcStorageID"), ","))
		Dim cardIDs As List(Of Integer) = switchList.Select(Function(id As String) Integer.Parse(id)).ToList()
        Bll.Merchants.MethodStorage.MarkIsActive(cardIDs, isCcDel.SelectedValue <> "0")
        btnSearch_Click(sender, e)
	End Sub

    Protected Function FormatPaid(item As Bll.Merchants.MethodStorage) As String
        If item.Settlement_id = 0 Then
            FormatPaid = "---"
        Else
            FormatPaid = GetLocalResourceObject("Paid")
        End If
    End Function
End Class
