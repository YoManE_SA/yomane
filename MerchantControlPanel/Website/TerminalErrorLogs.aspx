﻿<%@ Page Title="PageTitle" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master"
    EnableEventValidation="false" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_TerminalErrorLogs"
    CodeBehind="TerminalErrorLogs.aspx.vb" %>

<%@ Import Namespace="Netpay.MerchantControlPanel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <script language="JavaScript" type="text/javascript">
        function ExpandNode(cellID) {
            trObj = document.getElementById("trInfo" + cellID)
            imgObj = document.getElementById("oListImg" + cellID)
            if (trObj) {
                if (trObj.style.display == '') {
                    imgObj.src = '/NPCommon/Images/tree_expand.gif';
                    trObj.style.display = 'none';
                }
                else {
                    imgObj.src = '/NPCommon/Images/tree_collapse.gif';
                    trObj.style.display = '';
                }
            }
        }  
    </script>
    <asp:Localize ID="locPageTitle" Text="<%$ Resources:PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize  Text="<%$ Resources:PageTitle%>" runat="server" /></div>
        <div class="content-background">
            <div class="section">
                 <div class="percent33 align-left">
                      <div><asp:Literal runat="server" Text="<%$Resources:MultiLang,InsertDates %>" /></div>
                      <div><netpay:DateRangePicker ID="wcDateRangePicker" HtmlLayout="Flow" Layout="Horizontal" runat="server" /></div>
                 </div>
                 <div class="percent33 align-left">
                     <div><asp:Literal runat="server" Text="<%$Resources:MultiLang,CardsHolder %>" /></div>
                     <div><asp:TextBox ID="txtFindCardFullName" CssClass="meduim" runat="server" placeholder="<%$ Resources:MultiLang,CardsHolder %>" /></div>
                 </div>
                 <div class="percent33 align-left">
                     <div><asp:Literal runat="server" Text="<%$Resources:MultiLang,Last4Digits %>" /></div>
                     <div><asp:TextBox ID="txtccLast4Number" CssClass="meduim" runat="server" placeholder="<%$ Resources:MultiLang,Last4Digits %>" /></div>     
                 </div>
                 <div class="spacer"></div>
                 <div class="wrap-button-bar">
                     <asp:LinkButton ID="BtnSearch" runat="server" CssClass="btn btn-inverse btn-cons-short" OnClick="btnSearch_Click"><i class="fa fa-search"></i> <asp:Literal runat="server" Text="<%$Resources:MultiLang,Search %>" /></asp:LinkButton>
                 </div>
            </div>
            <div id="divDataWrapperSection" runat="server" visible="false" class="section">
                <div id="divDataSection" visible="false" runat="server">
                    <table class="exspand-table">
                        <tr>
                            <th>
                                &nbsp;
                            </th>
                            <th>
                                <asp:Literal runat="server" Text="<%$ Resources:MultiLang,Date %>" />
                            </th>
                            <th>
                                <asp:Literal runat="server" Text="<%$ Resources:MultiLang,Card %>" />
                            </th>
                            <th>
                                <asp:Literal runat="server" Text="<%$ Resources:MultiLang,CCType %>" />
                            </th>
                            <th>
                                <asp:Literal runat="server" Text="<%$ Resources:MultiLang,Amount %>" />
                            </th>
                            <th>
                                <asp:Literal runat="server" Text="<%$ Resources:MultiLang,CardsHolder %>" />
                            </th>
                        </tr>
                        <asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server">
                            <ItemTemplate>
                                <tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
                                    <td>
                                        <img onclick="ExpandNode('<%#Eval("ID")%>');" style="cursor: pointer;" id="oListImg<%#Eval("ID")%>"
                                            src="/NPCommon/Images/tree_expand.gif" alt="" width="16" height="16" border="0"
                                            align="middle" /><br />
                                    </td>
                                    <td>
                                        <%# dbPages.FormatDatesTimes(Eval("InsertDate"), 1, 1, 0)%>
                                    </td>
                                    <td>
                                        <%# Eval("ccLast4Number")%>
                                    </td>
                                    <td>
                                        <%# IIf(WebUtils.CurrentLanguageShort = "he", Eval("NameHeb"), Eval("NameEng"))%>
                                    </td>
                                    <td style="text-align: <%= WebUtils.CurrentAlignment %>">
                                        <%# dbPages.FormatCurr(Eval("Currency"), Eval("Amount"))%>
                                    </td>
                                    <td>
                                        <%# Eval("CHFullName")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td colspan="7">
                                        <div id="trInfo<%# Eval("ID")%>" style="display: none;">
                                            <table class="formNormal" style="width: 93%; border-top: 1px dashed #e2e2e2;" border="0">
                                                <tr>
                                                    <td height="10">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 150px;">
                                                        <asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:MultiLang,CCHolderMail %>" />
                                                    </td>
                                                    <td style="text-align: left">
                                                        <a href="mailto:<%# Eval("CHEmail") %>">
                                                            <%# Eval("CHEmail") %></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 150px;">
                                                        <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:MultiLang,CCHolderPhone %>" />
                                                    </td>
                                                    <td>
                                                        <%# Eval("CHPhoneNumber")%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
                <netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
            </div>
        </div>
    </div>
</asp:Content>
