﻿
Imports Netpay.Web.Controls
Imports Netpay.Infrastructure
Imports Netpay.Web
Imports Netpay.Bll

Partial Class Website_FailedConnectionsLog
    Inherits MasteredPage
	
	Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
		pager.info.PageCurrent = 0
		OnPageChanged(sender, e)
	End Sub

 	Protected Sub OnPageChanged(ByVal sender As Object, ByVal e As System.EventArgs)
		repeaterResults.DataSource = Bll.Process.ConnectionErrorLog.Search( New Bll.Process.ConnectionErrorLog.SearchFilters() With { .Date = New Infrastructure.Range(Of Nullable(Of System.DateTime)) With { .From = wcDateRangePicker.FromDate, .To = wcDateRangePicker.ToDate } }, pager.info)
		repeaterResults.DataBind()
		phResult.Visible = True
	End Sub
	
	Protected Function GetTranactionSource(transactionSourceID As Integer) As String
        Return Bll.Process.TransactionSource.Get(transactionSourceID).Name
	End Function
End Class
