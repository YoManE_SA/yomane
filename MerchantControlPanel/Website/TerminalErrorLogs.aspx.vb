﻿Imports System.Collections.Generic
Imports Netpay.Bll
Imports Netpay.Web
Imports Netpay.Infrastructure
Imports Netpay.Web.Controls

Partial Class Website_TerminalErrorLogs
	Inherits MasteredPage

	Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
		pager.Info.PageCurrent = 0
		OnPageChanged(sender, e)
	End Sub

 	Protected Sub OnPageChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        repeaterResults.DataSource = Process.ConnectionErrorLog.Search(New Process.ConnectionErrorLog.SearchFilters() With { 
				.Date = New Infrastructure.Range(Of Nullable(Of DateTime))(wcDateRangePicker.FromDate, wcDateRangePicker.ToDate), 
				.PaymentMethodOwnerName = txtFindCardFullName.Text.NullIfEmpty(), 
				.PaymentMethodLast4 = txtccLast4Number.Text.NullIfEmpty() 
			}, pager.Info)
        repeaterResults.DataBind()

        divDataSection.Visible = If(repeaterResults.Items.Count > 0, True, False)
        divDataWrapperSection.Visible = True
    End Sub

End Class
