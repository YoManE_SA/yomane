﻿<%@ Page Language="VB" Title="PageTitle" AutoEventWireup="false" EnableEventValidation="false" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" Inherits="Netpay.MerchantControlPanel.Website_ViewPaymentsReport" CodeBehind="ViewPaymentsReport.aspx.vb" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$ Resources:PageTitle %>" runat="server" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <script language="javascript" type="text/javascript">
        netpay.Blocker.enabled = false;
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">
            <div class="section">
                <div class="percent23 align-left margin-right-left-3">
                    <div>
                        <asp:Literal runat="server" Text="<%$Resources:MultiLang,No %>" />
                    </div>
                    <div>
                        <asp:TextBox ID="txtnumber" runat="server" CssClass="percent100" placeholder="<%$Resources:MultiLang,No %>" />
                    </div>
                </div>
                <div class="percent23 align-left margin-right-left-3">
                    <div>
                        <asp:Literal runat="server" Text="<%$Resources:MultiLang,cardType %>" /></div>
                    <div>
                        <asp:DropDownList ID="ddlcardType" CssClass="percent100" runat="server" />
                    </div>
                </div>

                <div class="percent23 align-left margin-right-left-3">
                    <div>
                        <asp:Literal runat="server" Text="<%$Resources:MultiLang,CustomerNumber %>" /></div>
                    <div>
                        <asp:TextBox ID="txtCustomerNum" runat="server" CssClass="percent100" placeholder="<%$Resources:MultiLang,CustomerNumber %>" /></div>
                </div>

                <div class="percent23 align-left margin-right-left-3">
                    <div>
                        <asp:Literal runat="server" Text="<%$Resources:MultiLang,CustomerName %>" /></div>
                    <div>
                        <asp:TextBox ID="txtCustomerName" runat="server" CssClass="percent100" placeholder="<%$Resources:MultiLang,CustomerName %>" /></div>
                </div>


                <div class="spacer"></div>
                <div class="wrap-button-bar">
                    <asp:LinkButton ID="BtnSearch" runat="server" CssClass="btn btn-cons-short btn-inverse" OnClick="btnSearch_Click"><i class="fa fa-search"></i> <asp:Literal runat="server" Text="<%$Resources:MultiLang,Search %>" /></asp:LinkButton>
                </div>
            </div>


            <div class="section">
                <table class="exspand-table">
                    <tbody id="tblData">
                        <tr>
                            <th>
                                <asp:Literal Text="<%$Resources:MultiLang,No %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:MultiLang,NameAndCompany %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal  Text="<%$Resources:MultiLang,PaymentType %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal  Text="<%$Resources:MultiLang,Date %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal  Text="<%$Resources:MultiLang,Amount %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal  Text="<%$Resources:Note %>" runat="server"></asp:Literal>
                            </th>
                            <th>
                                <asp:Literal  Text="<%$Resources:TransactionStatus %>" runat="server" />
                            </th>
                        </tr>
                        <asp:Repeater ID="repeaterSettlements" runat="server">
                            <ItemTemplate>
                                <tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
                                    <td>
                                        <asp:Literal ID="ltNumber" runat="server" Text='<%#Eval("ID")%>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Text='<%# FormatName(CType(Container.DataItem, Netpay.Bll.Wires.Wire))%>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text='<%# CType(Container.DataItem, Netpay.Bll.Wires.Wire)%>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label3" runat="server" Text='<%# FormatDate(CType(Container.DataItem, Netpay.Bll.Wires.Wire))%>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label4" runat="server" Text='<%# FormatMoney(CType(Container.DataItem, Netpay.Bll.Wires.Wire))%>' />
                                    </td>
                                    <td>
                                        <netpay:PageItemComment ID="PaymentRequestComment" UseNewDesign="true" Text="<%# CType(Container.DataItem, Netpay.Bll.Wires.Wire).Comment %>" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal12" runat="server" Text='<%# FormatTransStatus(CType(Container.DataItem, Netpay.Bll.Wires.Wire))%>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                <netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
            </div>
        </div>
    </div>
</asp:Content>
