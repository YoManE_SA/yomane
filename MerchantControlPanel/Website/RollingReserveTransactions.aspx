﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="false"
    Inherits="Netpay.MerchantControlPanel.Website_RollingReserveTransactions" CodeBehind="RollingReserveTransactions.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
   
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize2" Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">
            <asp:MultiView ID="mvMain" runat="server">
                <asp:View ID="vRollingReserves" runat="server">
				    <div class="section">
                        <div class="margin-bottom-10 frame-border-option">
                            <div class="frame-border-option-inline padding-10">
                                <div class="align-left percent33">
							    <div><asp:Literal runat="server" Text="<%$Resources:MultiLang,Currency %>" /></div>
							    <div><netpay:CurrencyDropDown ID="ddCurrency" CssClass="percent85" EnableBlankSelection="false" OnSelectedIndexChanged="ChangeCurrency" AutoPostBack="true" runat="server" /></div>
						   </div>
                           <div class="align-left percent33">
                               <div><asp:Literal Text="<%$Resources:MultiLang,Balance %>" runat="server" />:</div>	
					           <div><asp:Label ID="txtBalance" runat="server" CssClass="line-height-35" /></div>
                           </div>
                            <div class="spacer"></div>

                            </div>
                        </div>
					</div>
					<div class="section">
						<table class="exspand-table" align="center" cellpadding="1" cellspacing="1">
							<tr>
								<th>
									&nbsp;
								</th>
								<th>
									<asp:Literal Text="<%$Resources:MultiLang,Date %>" runat="server" />
								</th>
								<th>
									<asp:Literal Text="<%$Resources:MultiLang,Transaction %>" runat="server" />
								</th>
								<th>
									<asp:Literal Text="<%$Resources:MultiLang,Settlement %>" runat="server" />
								</th>
								<th>
									<asp:Literal Text="<%$Resources:MultiLang,ReserveHold %>" runat="server" />
								</th>
								<th>
									<asp:Literal Text="<%$Resources:MultiLang,ReserveRelease %>" runat="server" />
								</th>
								<th>
									<asp:Literal Text="<%$Resources:MultiLang,Comment %>" runat="server" />
								</th>
							</tr>
							<asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server">
								<ItemTemplate>
									<tr style="border-bottom: 1px solid black;">
										<td>
											<%# GetRollingLineArrow(CType(Container.DataItem, Transactions.Transaction)) %>
										</td>
										<td>
											<%# CType(Container.DataItem, Transactions.Transaction).InsertDate.ToDateTimeFormat() %>
										</td>
										<td>
											<a href="TransApproved.aspx?transID=<%# CType(Container.DataItem, Transactions.Transaction).ID %>">
												<%# CType(Container.DataItem, Transactions.Transaction).ID %></a>
										</td>
										<td>
											<a href="SettlementsReport.aspx?settlementID=<%# CType(Container.DataItem, Transactions.Transaction).PrimaryPaymentID %>">
												<%# CType(Container.DataItem, Transactions.Transaction).PrimaryPaymentID %></a>
										</td>
									<%# GetRollingLine(CType(Container.DataItem, Transactions.Transaction)) %>
										<td>
											<%# CType(Container.DataItem, Transactions.Transaction).Comment %>
										</td>
									</tr>
								</ItemTemplate>
							</asp:Repeater>
						</table>
						<netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
		                <div class="xls-wrap">
				            <netpay:ExcelButton ID="btnExportExcel" Text="<%$Resources:MultiLang,ExportToXls %>" Enabled="false" IconImageOn="xls.png" IconImageOff="xls_off.png" runat="server" OnClick="ExportExcel_Click" />
						</div>
					</div>
	            </asp:View> 
                <asp:View ID="vNoRollingReserves" runat="server">
                    <asp:Localize Text="<%$Resources:NoReserves %>" runat="server" />
                    <asp:HiddenField ID="hIsTransactions" Value="false" runat="server" />
                </asp:View>
            </asp:MultiView>
        </div>
    </div>
</asp:Content>
