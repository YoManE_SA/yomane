﻿Imports Netpay.Bll

Public Class HostedPageV2_Admin
    Inherits MasteredPage

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        If Not IsPostBack Then
            Dim options As Merchants.Hosted.PaymentPage = Merchants.Hosted.PaymentPage.Load(Netpay.Bll.Merchants.Merchant.Current.ID)
            LoadData()
        End If
    End Sub

    Protected Sub btnSave_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim settings = Netpay.Bll.Merchants.Hosted.PaymentPage.Load(Merchant.ID)
        If settings Is Nothing Then settings = New Netpay.Bll.Merchants.Hosted.PaymentPage(Merchant.ID)
        settings.RedirectionUrl = ReplyURL.Text.Trim()
        settings.NotificationUrl = ReplySilentURL.Text.Trim()
        settings.Save()
    End Sub

    Protected Sub LoadData()
        Dim reqString As String = " *"
        Dim bImageExist As Boolean = False
        Dim settings = Netpay.Bll.Merchants.Hosted.PaymentPage.Load(Merchant.ID)
        If settings Is Nothing Then Exit Sub

        ReplySilentURL.Text = settings.NotificationUrl
        ReplyURL.Text = settings.RedirectionUrl
    End Sub

    Protected Sub btnSendRequest_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
        phReplyContent.Visible = True
        Dim nStatus As Integer = 0
        If Trim(ReplySilentURL.Text) <> "" Then
            Try
                Dim url As String = ReplySilentURL.Text + "?replyCode=000&trans_id=1000&trans_date=01/01/1900+00:00:01"
                Dim reply As String = Nothing
                Netpay.Infrastructure.HttpClient.SendHttpRequest(url, reply)
                'nStatus = dbPages.SendRequestHTTP("POST", ReplySilentURL.Text, "replyCode=000&trans_id=1000&trans_date=01/01/1900+00:00:01")
                ltRequestRet.Text = "<span style=""color:green"">String was sent succesfully, return code: " & reply & "</span><br />"
            Catch ex As Exception
                ltRequestRet.Text = "<span style=""color:red"">String cannot be send due to the following error:<div>" & ex.Message & "</div></span>"
            End Try
        Else
            ltRequestRet.Text = "<span style=""color:maroon"">Please set url for hidden reply</span><br />"
        End If
    End Sub

End Class