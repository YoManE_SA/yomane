﻿Imports System.Collections.Generic
Imports Netpay.Web.Controls
Imports Netpay.Bll.Reports
Imports Netpay.Bll
Imports Netpay.Infrastructure

Partial Class Website_RejectedTransactions
	Inherits MasteredPage

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
			If Request.QueryString("replyCode") <> "" Then txtReplyCode.Text = Request.QueryString("replyCode")
			btnSearch_Click(sender, e)
		End If
	End Sub

	Protected Sub OnPageChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim filters = New Bll.Transactions.Transaction.SearchFilters()
		If ddCreditType.SelectedValue.ToNullableInt32() IsNot Nothing Then filters.creditTypes = New List(Of Integer)() From { ddCreditType.SelectedValue.ToNullableInt32() }
		filters.paymentMethodID = ddPaymentMethod.SelectedValue.ToNullableInt32()
		filters.date.From = wcDateRangePicker.FromDate
		filters.date.To = wcDateRangePicker.ToDate
		filters.replyCode = txtReplyCode.Text.ToSql().NullIfEmpty()
		filters.orderID = txtOrderNumber.Text.ToSql().NullIfEmpty()
		filters.declinedType = ddDeclinedTypeDropDown.SelectedValue.ToNullableInt32()
        filters.PayerFilters = New Transactions.Payer.SearchFilters()
		filters.PayerFilters.FirstName = txtPayerFirstName.Text.ToSql().NullIfEmpty
		filters.PayerFilters.LastName = txtPayerLastName.Text.ToSql().NullIfEmpty
		filters.PayerFilters.EmailAddress = txtCardHolderEmail.Text.ToSql().NullIfEmpty()
		filters.PayerFilters.PhoneNumber = txtPhone.Text.ToSql().NullIfEmpty()
        filters.PaymentFilters = New Transactions.Payment.SearchFilters()
		filters.PaymentFilters.Last4Digits = txtLast4Digits.Text.NullIfEmpty()

		If TypeOf e Is ExcelButton.ExportEventArgs Then
			wcFiltersView.LoadFilters(filters)
            btnExportExcel.SetExportInfo(wcFiltersView.RenderText, ReportType.MerchantDeclinedTransactions, Function(pi) Bll.Transactions.Transaction.Search(TransactionStatus.Declined, filters, pi, True, False))
        Else
            repeaterResults.DataSource = Bll.Transactions.Transaction.Search(TransactionStatus.Declined, filters, pager.Info, True, False)
			repeaterResults.DataBind()
			btnExportExcel.ResultCount = pager.Info.RowCount
		End If
		phResults.Visible = repeaterResults.Items.Count > 0
	End Sub

	Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
		pager.Info.PageCurrent = 0
		hdLastSerachMode.Value = "search"
		OnPageChanged(sender, e)
		pager.Visible = True
	End Sub

	Protected Sub btnShow_Click(ByVal sender As Object, ByVal e As EventArgs)
		hdLastSerachMode.Value = "show"

        Dim filters = New Bll.Transactions.Transaction.SearchFilters()
		Dim transactionID As Integer
		If Integer.TryParse(txtTransactionID.Text, transactionID) Then
			filters.transactionID = transactionID
			If TypeOf e Is ExcelButton.ExportEventArgs Then
				wcFiltersView.LoadFilters(filters)
                btnExportExcel.SetExportInfo(wcFiltersView.RenderText, ReportType.MerchantDeclinedTransactions, Function(pi) Bll.Transactions.Transaction.Search(TransactionStatus.Declined, filters, pi, True, False))
            Else
                repeaterResults.DataSource = Bll.Transactions.Transaction.Search(TransactionStatus.Declined, filters, Nothing, True, False)
				repeaterResults.DataBind()
				btnExportExcel.ResultCount = IIf(repeaterResults.Items.Count > 0, 1, 0)
			End If
		End If
		pager.Visible = False
		phResults.Visible = (repeaterResults.Items.Count > 0)
	End Sub

	Protected Sub ExportExcel_Click(ByVal sender As Object, ByVal e As EventArgs)
		If (hdLastSerachMode.Value = "show") Then btnShow_Click(Me, e) Else btnSearch_Click(Me, e)
		btnExportExcel.Export()
	End Sub
End Class
