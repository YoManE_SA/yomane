﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Tmp_netpayintl/page.master"
    CodeBehind="LiveKashAddCustomer.aspx.vb" Inherits="Netpay.MerchantControlPanel.LiveKashAddCustomer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
	<table width="100%">
		<tr>
			<td><h1><asp:Localize ID="Localize1" Text="<%$Resources:title %>" runat="server" /></h1></td>
			<td style="text-align:<%= WebUtils.CurrentReverseAlignment %>; text-transform:uppercase;">
				<a href="LiveKashCustomers.aspx"><asp:Literal ID="Literal15" Text="<%$Resources:backToPayees %>" runat="server"></asp:Literal></a>
			</td>
		</tr>
	</table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
	<div id="divBlocker">
        <netpay:ElementBlocker ID="wcElementBlocker" Visible="true" ElementClientID="divBlocker" Text="<%$ Resources:MultiLang,FeatureDisabled %>" runat="server" />
         <table border="0" width="92%" align="center">
            <tr>
                <td>
                    <asp:Label ID="lblError" CssClass="error" runat="server"></asp:Label>
                    <asp:Label ID="lblSuccess" CssClass="noError" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <table class="htmlForm" align="center" border="0" width="92%">
            <tbody>          
                <tr>
			        <td class="Heading">
				        <asp:Literal ID="Literal14" Text="<%$Resources:payeeDetails %>" runat="server"></asp:Literal>
			        </td>
		        </tr>
                <tr>
                    <td>
                       <asp:Literal ID="Literal1" Text="<%$Resources:MultiLang,FirstName %>" runat="server"></asp:Literal> <span class="asterisk">*</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Literal ID="Literal2" Text="<%$Resources:MultiLang,LastName %>" runat="server"></asp:Literal> <span class="asterisk">*</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Literal ID="Literal3" Text="<%$Resources:MultiLang,Email %>" runat="server"></asp:Literal> <span class="asterisk">*</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Literal ID="Literal4" Text="<%$Resources:MultiLang,Phone %>" runat="server"></asp:Literal> <span class="asterisk">*</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtMobileNumber" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Literal ID="Literal11" Text="<%$Resources:MultiLang,Gender %>" runat="server"></asp:Literal> <span class="asterisk">*</span>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddGender" runat="server">
                            <asp:ListItem Text="" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Male" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Female" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Literal ID="Literal12" Text="<%$Resources:MultiLang,BirthDate %>" runat="server"></asp:Literal> <span class="asterisk">*</span>
                    </td>
                    <td>
                        <netpay:DatePicker ID="wcBirthdate" Date="1/1/1970" runat="server" />
                    </td>
                </tr>
                <tr>
			        <td class="Heading">
				        <asp:Literal ID="Literal16" Text="<%$Resources:payeeAddressDetails %>" runat="server"></asp:Literal>
			        </td>
		        </tr>
                <tr>
                    <td>
                        <asp:Literal ID="Literal5" Text="<%$Resources:MultiLang,Address %>" runat="server"></asp:Literal> 1 <span class="asterisk">*</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAddress1" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Literal ID="Literal6" Text="<%$Resources:MultiLang,Address %>" runat="server"></asp:Literal> 2 <span class="asterisk">*</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAddress2" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Literal ID="Literal7" Text="<%$Resources:MultiLang,City %>" runat="server"></asp:Literal> <span class="asterisk">*</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Literal ID="Literal8" Text="<%$Resources:MultiLang,State %>" runat="server"></asp:Literal> <span class="asterisk">*</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtState" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Literal ID="Literal9" Text="<%$Resources:MultiLang,Country %>" runat="server"></asp:Literal> <span class="asterisk">*</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtCountry" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Literal ID="Literal10" Text="<%$Resources:MultiLang,Zipcode %>" runat="server"></asp:Literal> <span class="asterisk">*</span>
                    </td>
                    <td>
                        <asp:TextBox ID="txtZipcode" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>                
                    <td class="Heading">
                       &nbsp;
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <asp:Literal ID="Literal13" Text="<%$Resources:MultiLang,Comment %>" runat="server"></asp:Literal>
                    </td>
                    <td colspan="4">
                        <textarea id="txtComment" style="width: 92%;" runat="server"></textarea>
                    </td>
                </tr>
            </tbody>
        </table>
	    <table border="0" align="center" style="width:92%; margin-top:19px;">
		    <tr>				
			    <td class="txt11">
				    <span class="asterisk">*</span> <asp:Literal ID="Literal17" Text="<%$Resources:MultiLang,RequiredFields %>" runat="server" />
			    </td>
			    <td style="text-align:<%=WebUtils.CurrentReverseAlignment%>;vertical-align:bottom;">
                    <asp:Button ID="btnSubmit" CssClass="button" runat="server" Text="<%$Resources:AddCustomer %>" />
                </td>
            </tr>
	    </table>   
    </div>
</asp:Content>
