﻿Imports Netpay.Infrastructure.VO
Imports Netpay.Bll
Imports Netpay.Infrastructure

Public Class PayoneerCustomers
	Inherits MasteredPage

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Request("__EVENTTARGET") = "ctl00$cphBody$pager" Then
			GetCustomers()
		End If

		If Not IsPostBack Then
            wcElementBlocker.Visible = Not Netpay.Bll.ExternalCards.IsProviderAvailable(LoggedUser.CredentialsToken, ExternalCardProviderEnum.Payoneer)
			GetCustomers()

			btnShowAddCustomer.Attributes.Add("style", "width:120px;")
		End If
		lblError.Visible = False
		lblError.Text = ""
	End Sub

	Protected Sub GetCustomers()
        Dim results As List(Of ExternalCardCustomerVO) = Netpay.Bll.ExternalCards.GetCustomers(LoggedUser.CredentialsToken, GetFilters(), pager.Info)
        repeaterResults.DataSource = results
        repeaterResults.DataBind()

        If results IsNot Nothing Then
            phResults.Visible = (results.Count > 0)
        End If
    End Sub

    Protected Sub ShowPayee()
        Dim payeeID As Integer
        If Not Integer.TryParse(txtPayeeID.Text, payeeID) Then Exit Sub

        Dim result As ExternalCardCustomerVO = Netpay.Bll.ExternalCards.GetCustomer(LoggedUser.CredentialsToken, payeeID)
        If result Is Nothing Then Exit Sub

        Dim results As New List(Of ExternalCardCustomerVO)()
        results.Add(result)
        repeaterResults.DataSource = results
        repeaterResults.DataBind()
        phResults.Visible = True
        pager.Visible = False
    End Sub

    Protected Sub ShowAddCustomerForm()
        btnShowAddCustomer.Visible = False
        phAddCustomerForm.Visible = True
        GetCustomers()
    End Sub

    Private Function GetFilters() As SearchFilters
        Dim filters As New SearchFilters()
        filters.externalCardProvider = ExternalCardProviderEnum.Payoneer
        If ddCustomerStatus.SelectedValue <> "" Then
            Dim status As ExternalCardCustomerStatus = ddCustomerStatus.SelectedValue
            filters.CustomerStatus = status
        End If

        filters.comment = txtFilterComment.Value.NullIfEmpty()
        filters.email = txtFilterEmail.Value.NullIfEmpty()

        Return filters
    End Function

    Protected Function GetPaymentLink(customer As ExternalCardCustomerVO) As String
        If customer.IsPayable Then
            Return "<a href='ExternalCardCustomer.aspx?customerID=" & customer.ID & "'>" + GetLocalResourceObject("payments") + "</a>"
        Else
            Return "<span style='color:silver;'>" + GetLocalResourceObject("payments") + "</span>"
        End If
    End Function

    Protected Sub AddCustomer(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim name As String = txtName.Value
        Dim email As String = txtEmail.Value
        Dim comment As String = txtComment.Value
		lblError.Visible = True
        lblError.Text = ""
        If name.Trim() = "" Or email.Trim() = "" Then
            lblError.Text = "<div class=""Error2"">" & GetGlobalResourceObject("MultiLang", "ErrorAdding") & ": " & GetLocalResourceObject("requiredFieldsErr") & "</div>"
            GetCustomers()
            Exit Sub
        End If

        Dim addResult As String = Netpay.Bll.ExternalCards.AddPayoneerCustomer(LoggedUser.CredentialsToken, name, email, comment)
        If addResult <> "ok" Then
            lblError.Text = "<div class=""Error2"">" & GetGlobalResourceObject("MultiLang", "ErrorAdding") & ": " & addResult & "</div>"
        End If

        GetCustomers()
    End Sub

    Protected Function GetCssClass(customer As ExternalCardCustomerVO) As String
        If customer.IsPayable Then
            Return "payoneerNoError"
        Else
            Return "payoneerError"
        End If
    End Function
End Class