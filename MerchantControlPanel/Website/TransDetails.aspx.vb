﻿Imports Netpay.Web.Controls
Imports Netpay.Bll.Risk
Imports Netpay.CommonTypes
Imports Netpay.Bll
Imports Netpay.Web
Imports Netpay.Infrastructure

Partial Class Website_TransDetailsCreditcard
	Inherits IFramePage

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Dim transactionID As Integer = Request("transactionID")
        Dim transStatus As TransactionStatus = [Enum].Parse(GetType(TransactionStatus), Request("transactionStatus"), True)
        TransactionInfo.Info = Netpay.Bll.Transactions.Transaction.GetTransaction(Nothing, transactionID, transStatus)
        'wcDetails.Transaction = Transactions.GetTransaction(LoggedUser.CredentialsToken, transactionID, transStatus)
      
    End Sub

    Protected Overrides Sub OnPreRender(e As System.EventArgs)
        Dim cssTag As String = String.Format("<link rel=""stylesheet"" type=""text/css"" href=""{0}/Website/Styles/style.css"" />", MapTemplateVirPath(""))
		Page.Header.Controls.Add(New LiteralControl(cssTag))
        If System.Threading.Thread.CurrentThread.CurrentUICulture.TextInfo.IsRightToLeft Then
            cssTag = String.Format("<link rel=""stylesheet"" type=""text/css"" href=""{0}/Website/Styles/StyleRTL.css"" />", MapTemplateVirPath(""))
	        Page.Header.Controls.Add(New LiteralControl(cssTag))
        End If
        MyBase.OnPreRender(e)
    End Sub
End Class