'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.42000
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Imports System

Namespace Resources.ShopLocations
    
    'This class was auto-generated by the StronglyTypedResourceBuilder
    'class via a tool like ResGen or Visual Studio.
    'To add or remove a member, edit your .ResX file then rerun ResGen
    'with the /str option or rebuild the Visual Studio project.
    '''<summary>
    '''  A strongly-typed resource class, for looking up localized strings, etc.
    '''</summary>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "15.0.0.0"),  _
     Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute()>  _
    Friend Class aspx
        
        Private Shared resourceMan As Global.System.Resources.ResourceManager
        
        Private Shared resourceCulture As Global.System.Globalization.CultureInfo
        
        <Global.System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>  _
        Friend Sub New()
            MyBase.New
        End Sub
        
        '''<summary>
        '''  Returns the cached ResourceManager instance used by this class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend Shared ReadOnly Property ResourceManager() As Global.System.Resources.ResourceManager
            Get
                If Object.ReferenceEquals(resourceMan, Nothing) Then
                    Dim temp As Global.System.Resources.ResourceManager = New Global.System.Resources.ResourceManager("Resources.ShopLocations.aspx", Global.System.Reflection.[Assembly].Load("App_GlobalResources"))
                    resourceMan = temp
                End If
                Return resourceMan
            End Get
        End Property
        
        '''<summary>
        '''  Overrides the current thread's CurrentUICulture property for all
        '''  resource lookups using this strongly typed resource class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend Shared Property Culture() As Global.System.Globalization.CultureInfo
            Get
                Return resourceCulture
            End Get
            Set
                resourceCulture = value
            End Set
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Clear.
        '''</summary>
        Friend Shared ReadOnly Property Clear() As String
            Get
                Return ResourceManager.GetString("Clear", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Country.
        '''</summary>
        Friend Shared ReadOnly Property Country() As String
            Get
                Return ResourceManager.GetString("Country", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to e.g &quot;United states&quot;.
        '''</summary>
        Friend Shared ReadOnly Property ExCountrystr() As String
            Get
                Return ResourceManager.GetString("ExCountrystr", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Global.
        '''</summary>
        Friend Shared ReadOnly Property Globalstr() As String
            Get
                Return ResourceManager.GetString("Globalstr", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Choose Locations.
        '''</summary>
        Friend Shared ReadOnly Property LocationsPageTitle() As String
            Get
                Return ResourceManager.GetString("LocationsPageTitle", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Next.
        '''</summary>
        Friend Shared ReadOnly Property NextBtn() As String
            Get
                Return ResourceManager.GetString("NextBtn", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Previous.
        '''</summary>
        Friend Shared ReadOnly Property PrevBtn() As String
            Get
                Return ResourceManager.GetString("PrevBtn", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Quick Search.
        '''</summary>
        Friend Shared ReadOnly Property QuickSearch() As String
            Get
                Return ResourceManager.GetString("QuickSearch", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Regions.
        '''</summary>
        Friend Shared ReadOnly Property Regions() As String
            Get
                Return ResourceManager.GetString("Regions", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Please select a shop location.
        '''</summary>
        Friend Shared ReadOnly Property SelectLocationAlert() As String
            Get
                Return ResourceManager.GetString("SelectLocationAlert", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Use the list below to assign the relevant regions and countries relevant to the specific shop you are editing. 
        '''                    Same values can not be applied to multiple shops.
        '''                    Currencies matching the region/country will be automatically assigned to the products managed under this shop..
        '''</summary>
        Friend Shared ReadOnly Property ShopLocationDesc() As String
            Get
                Return ResourceManager.GetString("ShopLocationDesc", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Shop Location.
        '''</summary>
        Friend Shared ReadOnly Property ShopLocationTitle() As String
            Get
                Return ResourceManager.GetString("ShopLocationTitle", resourceCulture)
            End Get
        End Property
    End Class
End Namespace
