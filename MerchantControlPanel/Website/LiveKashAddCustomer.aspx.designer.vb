﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class LiveKashAddCustomer

    '''<summary>
    '''Localize1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Localize1 As Global.System.Web.UI.WebControls.Localize

    '''<summary>
    '''Literal15 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal15 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''wcElementBlocker control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents wcElementBlocker As Global.Netpay.Web.Controls.ElementBlocker

    '''<summary>
    '''lblError control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblError As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblSuccess control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSuccess As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Literal14 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal14 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''Literal1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal1 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''txtFirstName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFirstName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Literal2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal2 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''txtLastName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtLastName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Literal3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal3 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''txtEmail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtEmail As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Literal4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal4 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''txtMobileNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMobileNumber As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Literal11 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal11 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''ddGender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddGender As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Literal12 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal12 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''wcBirthdate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents wcBirthdate As Global.Netpay.Web.Controls.DatePicker

    '''<summary>
    '''Literal16 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal16 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''Literal5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal5 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''txtAddress1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAddress1 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Literal6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal6 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''txtAddress2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAddress2 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Literal7 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal7 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''txtCity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCity As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Literal8 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal8 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''txtState control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtState As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Literal9 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal9 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''txtCountry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCountry As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Literal10 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal10 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''txtZipcode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtZipcode As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Literal13 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal13 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''txtComment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtComment As Global.System.Web.UI.HtmlControls.HtmlTextArea

    '''<summary>
    '''Literal17 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal17 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''btnSubmit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSubmit As Global.System.Web.UI.WebControls.Button
End Class
