﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="false"
    Inherits="Netpay.MerchantControlPanel.Website_Content" CodeBehind="Content.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Literal ID="literalTitle" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav"><asp:Localize ID="Localize1" Text="" runat="server" /></div>
        <div class="content-background">
            <div class="section">
                <asp:Label ID="lblContent" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
