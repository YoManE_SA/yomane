﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" CodeBehind="ShopLocations.aspx.vb" Inherits="Netpay.MerchantControlPanel.ShopLocations" %>
<%@ Register Src="~/Website/Controls/ShopsWizard.ascx" TagPrefix="uc1" TagName="ShopsWizard" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTitle" runat="server">
    <asp:Localize ID="locPageTitle" Text="<%$ Resources: LocationsPageTitle %>" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBody" runat="server">

    <script type="text/javascript">
        $(function () { init(); })

        function init() {
            if ($("#<%= cbToggleGlobal.ClientID %>").attr("disabled")) {
                $("#<%= cbToggleGlobal.ClientID %>").siblings("label").css("text-decoration", "line-through");
                $("#<%= cbToggleGlobal.ClientID %>").closest("div").css("opacity", 0.4);
            }
            
            $(":checkbox[id!='<%= cbToggleGlobal.ClientID %>']").each(function (index) {
                var element = $(this);
                element.attr("checkedCache", element.attr("checked"));
                element.attr("disabledCache", element.attr("disabled"));
                if (element.attr("disabledCache") == "true") {
                    element.siblings("label").css("text-decoration", "line-through");
                    element.closest("li").css("opacity", 0.4);
                }
            });

            toggleGlobal();
        }

        function toggleGlobal() {
            var isGlobal = $("#<%= cbToggleGlobal.ClientID %>").attr("checked");
            $(":checkbox[id!='<%= cbToggleGlobal.ClientID %>']").each(function (index) {
                var element = $(this);
                if (isGlobal) {
                    element.attr("checkedCache", element.attr("checked"));
                    element.attr("checked", false);
                    element.attr("disabled", true);
                }
                else {
                    element.attr("checked", element.attr("checkedCache") == "true");
                    element.attr("disabled", element.attr("disabledCache") == "true");
                }
            });
        }

        function search() {
            var keyword = $("#txtSearch").val().toLowerCase().trim();
            var regions = $("#clRegions li");
            var countries = $("#clCountries li");

            if (keyword == "") {
                regions.css("display", "block");
                countries.css("display", "block");
                return;
            }

            searchFilter(keyword, regions);
            searchFilter(keyword, countries);
        }

        function searchFilter(keyword, root) {
            var results = root.find("span").filter(function () {
                return $(this).attr("searchterm").toLowerCase().match("^" + keyword);
            }).closest("li");

            root.css("display", "none");
            results.css("display", "block");
        }

        function clearSearch() {
            $("#txtSearch").val("");
            search();
        }
    </script>
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize Text="<%$Resources: ShopLocationTitle %>" runat="server" />
        </div>
        <div class="content-background">
       
            <uc1:ShopsWizard runat="server" id="ShopsWizard" />
            <div class="section">
                <div id="divAlert" class="alert-warning" visible="false" runat="server">
                    <asp:Literal runat="server" Text="<%$Resources: SelectLocationAlert %>" />
                </div>
                <div><br /></div>
                <div class="alert alert-info margin-bottom-10">
                    <i class="fa fa-info-circle"></i>
                    <asp:Literal runat="server" Text="<%$Resources: ShopLocationDesc %>" />
                </div>
                <div class="margin-bottom-10 frame-border-option">
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">
                        <div class="form-group">
                            <span class="form-group-title"><asp:Literal runat="server" Text="<%$Resources: QuickSearch %>" /></span><span class="form-group-title-eg"><asp:Literal runat="server" Text="<%$Resources: ExCountrystr %>" /></span>
                            <div class="line-height-35">
                                <input id="txtSearch" type="text" onkeyup="search()" />
                                <asp:Button runat="server" type="button" class="btn btn-primary" OnClientClick="clearSearch()" Text="<%$Resources: Clear %>" />
                            </div>
                        </div>
                    </div>
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">
                        <input id="cbToggleGlobal" type="checkbox" onclick="toggleGlobal()" runat="server" />
                        <label for="cbToggleGlobal"><asp:Literal runat="server" Text="<%$Resources: Globalstr %>" /></label>
                    </div>
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">
                        <h3><asp:Literal runat="server" Text="<%$Resources: Regions %>" /></h3>
                        <div>
                            <asp:CheckBoxList ID="clRegions" ClientIDMode="Static" RepeatLayout="UnorderedList" RepeatDirection="Vertical" runat="server"></asp:CheckBoxList>
                            <div class="spacer"></div>
                        </div>
                    </div>
                    <div class="frame-border-option-inline padding-10 ">
                        <h3><asp:Literal runat="server" Text="<%$Resources: Country %>" /></h3>
                        <div>
                            <asp:CheckBoxList ID="clCountries" ClientIDMode="Static" RepeatLayout="UnorderedList" RepeatDirection="Vertical" runat="server"></asp:CheckBoxList>
                            <div class="spacer"></div>
                        </div>
                    </div>

                    <div class="margin-top-10">
                        <div class="align-left">
                            <asp:LinkButton ID="btnPrev" Text="<%$ Resources: PrevBtn %>" CssClass="btn btn-primary btn-cons-short" OnClick="btnPrev_Click" runat="server">
                               <asp:Literal runat="server" Text="<%$Resources: PrevBtn %>" />
                            </asp:LinkButton>
                        </div>
                        <div class="align-right">
                            <asp:LinkButton ID="btnNext" Text="<%$ Resources:  NextBtn %>" CssClass="btn btn-primary btn-cons-short" OnClick="btnNext_Click" runat="server">
                                <asp:Literal runat="server" Text="<%$Resources: NextBtn %>" />
                            </asp:LinkButton>
                        </div>

                        <div class="spacer"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
