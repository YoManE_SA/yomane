﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_CartsItemsView" Codebehind="CartsItemsView.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" Runat="Server">
	<netpay:PageHelpBox Text="<%$Resources:HelpBox %>" runat="server" />
	<table>
		<tr>
			<td><h1><asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" /></h1></td>
		</tr>
	</table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" Runat="Server">
	<table border="0" align="center" style="width:92%; background-color:#f5f5f5; border:1px solid silver;">
		<tr>
			<td>
				<table border="0">
					<tr>
						<td colspan="2">
							<asp:Literal runat="server" Text="<%$Resources:MultiLang,InsertDates %>" />
						</td>
						<td>
							<asp:Localize Text="<%$Resources:Multilang,Name%>" runat="server"></asp:Localize>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<netpay:DateRangePicker ID="wcDateRangePicker" Layout="Horizontal" runat="server" />
						</td>
						<td>
							<asp:TextBox ID="txtItemName" runat="server"></asp:TextBox>
						</td>
						<td>&nbsp;</td>
						<td>
							<asp:Button ID="BtnSearch" runat="server" CssClass="button" Text="<%$Resources:MultiLang,Search %>" OnClick="btnSearch_Click" />
						</td>
					</tr>
				</table>			
			</td>
		</tr>
	</table>
	<br /><br />
	<asp:PlaceHolder ID="phResults" Visible="false" runat="server">
		<table class="formNormal" align="center" style="width: 92%" cellpadding="1" cellspacing="1">
			<tr>
				<th><asp:Literal Text="<%$Resources:MultiLang,CartID %>" runat="server"></asp:Literal></th>
				<th><asp:Literal Text="<%$Resources:MultiLang,Date %>" runat="server"></asp:Literal></th>
				<th><asp:Literal Text="<%$Resources:MultiLang,Name %>" runat="server"></asp:Literal></th>
				<th><asp:Literal Text="<%$Resources:MultiLang,Shipping %>" runat="server"></asp:Literal></th>	
				<th><asp:Literal Text="<%$Resources:MultiLang,Tax %>" runat="server"></asp:Literal></th>			
				<th><asp:Literal Text="<%$Resources:MultiLang,Price %>" runat="server"></asp:Literal></th>			
				<asp:Literal ID="lblTableHeadings" runat="server"/>
			</tr>	
			<asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server">
				<ItemTemplate>
					<tr>
						<td>
							<%# CType(Container.DataItem, CartItemVO).CartID %>
						</td>
						<td>
							<%# CType(Container.DataItem, CartItemVO).Date %>
						</td>
						<td>
							<%# CType(Container.DataItem, CartItemVO).Name %>
						</td>
						<td>
							<%# CType(Container.DataItem, CartItemVO).ShippingFee.ToAmountFormat(WebUtils.CurrentDomain.Host, CType(Container.DataItem, CartItemVO).CurrencyID) %>
						</td>
						<td>
							% <%# CType(Container.DataItem, CartItemVO).TaxRatio.ToAmountFormat() %>
						</td>
						<td>
							<%# CType(Container.DataItem, CartItemVO).Price.ToAmountFormat(WebUtils.CurrentDomain.Host, CType(Container.DataItem, CartItemVO).CurrencyID) %>
						</td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
		<table class="pager" align="center">
			<tr><td><netpay:LinkPager ID="pager" runat="server" /></td></tr>
		</table>	
	</asp:PlaceHolder>
</asp:Content>