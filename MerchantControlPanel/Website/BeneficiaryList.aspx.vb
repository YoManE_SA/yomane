﻿
Partial Class Website_BeneficiaryList
	Inherits MasteredPage

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        litOwnBeneficiary.Text = "<img src=""Images/icon_contactRed.gif"" alt=""" & GetLocalResourceObject("OwnBeneficiary") & """ title=""" & GetLocalResourceObject("OwnBeneficiary") & """ />"
		If Not IsPostBack Then
			repeaterResults.DataSource = Bll.Merchants.Beneficiary.GetBeneficiaries()
			repeaterResults.DataBind()
		End If
	End Sub

    Function GetRowBackground(ByVal beneficiary As Bll.Merchants.Beneficiary) As String
        Return IIf(CanRequestPayment(beneficiary), "", "#ffebe1;")
    End Function

    Function GetEditButton(ByVal beneficiary As Bll.Merchants.Beneficiary, ByVal retButton As Boolean) As String
        If beneficiary.IsSystem Then
            If retButton Then Return "&nbsp;"
            Return String.Format("<img src='Images/icon_contactRed.gif' alt='{1}' title='{1}' />", GetLocalResourceObject("OwnBeneficiary"))
        Else
            Dim link As String = String.Format("BeneficiaryAddUpdate.aspx?CompanyMakePaymentsProfiles_id={0}&ProfileType={1}", beneficiary.ID, beneficiary.ProfileType)
            If retButton Then Return String.Format("<input type=""button"" onclick=""document.location='{0}'"" value='  {1}  ' class=""btn btn-inverse btn-cons-short""  />", link, GetGlobalResourceObject("MultiLang", "Edit"))
            Return String.Format("<a href='{0}'><img src='Images/icon_contactBlue.gif' alt='{1}' title='{1}' /></a>", link, GetGlobalResourceObject("MultiLang", "Edit"))
        End If
    End Function

    Sub btnAdd_OnClick(ByVal o As Object, ByVal e As System.EventArgs)
        Response.Redirect("BeneficiaryAddUpdate.aspx?ProfileType=" & Request("ProfileType"))
    End Sub

    Function CanRequestPayment(ByVal Beneficiary As Bll.Merchants.Beneficiary) As Boolean
        If Not String.IsNullOrEmpty(Beneficiary.BankAbroadAccountNumber) Then Return True
        If Not String.IsNullOrEmpty(Beneficiary.BankAbroadAccountNumber2) Then Return True
        If (Not String.IsNullOrEmpty(Beneficiary.BankIsraelInfoAccountNumber)) And (Beneficiary.BankIsraelInfoPersonalIDNumber <> "" Or Beneficiary.BankIsraelInfoCompanyLegalNumber <> "") Then Return True
        Return False
    End Function

    Sub repeaterResults_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        Dim bCanRequestPayment As Boolean = CanRequestPayment(e.Item.DataItem)
        e.Item.FindControl("trBeneficiaryError").Visible = Not bCanRequestPayment
        Dim btnPay As Button = CType(e.Item.FindControl("btnPay"), Button)
        If Not bCanRequestPayment Then
            btnPay.Enabled = False
            btnPay.CssClass = "btn btn-default btn-cons-short"
        End If
    End Sub

    Function PaymentButtonClickJS(ByVal Beneficiary As Bll.Merchants.Beneficiary) As String
        Dim sbJS As New StringBuilder
        If CanRequestPayment(Beneficiary) Then
            sbJS.AppendFormat("PayBeneficiary({0}, {1});", Beneficiary.ID, Beneficiary.ProfileType)
        Else
            sbJS.AppendFormat("if (confirm(""{2}"")) EditBeneficiary({0}, {1});", Beneficiary.ID, Beneficiary.ProfileType, GetLocalResourceObject("MissingBankDetails"))
        End If
        sbJS.Append("return false;")
        Return sbJS.ToString
    End Function
End Class
