﻿Imports Netpay.Web
Imports Netpay.Infrastructure

Public Class MasterLogin
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lnkIcon.Href = WebUtils.MapTemplateVirPath(Me, "favicon.ico")
        footer.DataBind()

    End Sub
    Protected Overrides Sub OnPreRender(e As System.EventArgs)
        ltCopyright.Text = CType(GetGlobalResourceObject("Login.aspx", "Copyright"), String).Replace("%BRAND%", WebUtils.CurrentDomain.BrandName)
        ltCopyright.Text = CType(GetGlobalResourceObject("MultiLang", "Copyright"), String).Replace("%BRAND%", WebUtils.CurrentDomain.BrandName)
    End Sub

End Class