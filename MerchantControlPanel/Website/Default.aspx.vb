﻿Imports Netpay.Bll
Imports Netpay.Web
Imports Netpay.Infrastructure
Imports Netpay.Web.Controls
Imports Netpay.Dal.Reports
Imports Netpay.Infrastructure.Security

Partial Class Website_Default
    Inherits MasteredPage

    Protected Overrides Sub OnLoad(e As System.EventArgs)
        MyBase.OnLoad(e)
    End Sub

    Protected Overrides Sub OnPreRender(e As System.EventArgs)
        If Notifications IsNot Nothing Then Notifications.Visible = WebUtils.IsLoggedin
        MyBase.OnPreRender(e)
    End Sub

    Protected Sub PopulateNotifications(sender As Object, e As EventArgs) Handles hiddenAsyncTrigger.Click
        If WebUtils.IsLoggedin Then
            phNotifications.Visible = True
            If Bll.Accounts.LimitedLogin.Current IsNot Nothing Then
                Dim obj = SecurityModule.GetSecuredObject(PermissionObjects.ShowMainPageNotifications, True)
                If Not obj.HasPermission(PermissionValue.Execute) Then
                    If (wcNotificationSlipVerify IsNot Nothing) Then wcNotificationSlipVerify.Visible = False
                    If (wcNotificationSlipTest IsNot Nothing) Then wcNotificationSlipTest.Visible = False
                    If (newNotifications IsNot Nothing) Then newNotifications.Visible = False
                    Exit Sub
                End If
            End If


            If (newNotifications IsNot Nothing) Then
                Dim filters2 = New Bll.Content.Bulletin.SearchFilters()
                filters2.OnlyNotExpired = True
                filters2.Solution = Solution.MerchantControlPanel

                Dim list = Bll.Content.Bulletin.Search(filters2, Nothing)
                If list.Count > 0 Then
                    newNotifications.DataSource = list
                End If

                If list.Count = 0 Then
                    newNotifications.Visible = False
                End If

            End If


            If (wcNotificationSlipVerify IsNot Nothing) Then
                ' get verification count
                Dim filters = New Bll.Transactions.Transaction.SearchFilters()
                filters.deniedStatus = New System.Collections.Generic.List(Of DeniedStatus)
                filters.deniedStatus.Add(DeniedStatus.UnsettledBeenSettledAndValid)
                filters.deniedStatus.Add(DeniedStatus.DuplicateTransactionWorkedOut)
                filters.deniedDate.From = DateTime.Now.AddMonths(-2)
                Dim pi = New SortAndPage(0, 1, Nothing, False)
                Bll.Transactions.Transaction.Search(TransactionStatus.Captured, filters, pi, False, False)
                wcNotificationSlipVerify.Number = pi.RowCount
                If pi.RowCount = 0 Then wcNotificationSlipVerify.Visible = False
            End If

            If wcNotificationSlipTest IsNot Nothing Then
                Dim filters = New Bll.Transactions.Transaction.SearchFilters()
                filters.isTest = True
                Dim pi = New SortAndPage(0, 1, Nothing, False)

                Bll.Transactions.Transaction.Search(TransactionStatus.Captured, filters, pi, False, False)
                wcNotificationSlipTest.Number = pi.RowCount
                If pi.RowCount = 0 Then wcNotificationSlipTest.Visible = False
            End If

            Try
                ' get chb ratios
                If wcNotificationSlipVisaChbRatio IsNot Nothing Or wcNotificationSlipMastercardChbRatio IsNot Nothing Then
                    Dim stats As DailyRiskReportByMerchant = Netpay.Bll.Merchants.Merchant.GetRiskStats(Merchant.ID)
                    If stats IsNot Nothing Then
                        If stats.VisaThisMonthChargebacksCount IsNot Nothing And stats.VisaThisMonthNotChargebacksCount IsNot Nothing Then
                            Dim visaChbRatio As Decimal = Math.GetPercentage(stats.VisaThisMonthChargebacksCount.Value, stats.VisaThisMonthNotChargebacksCount.Value)
                            wcNotificationSlipVisaChbRatio.Number = visaChbRatio.ToPercentFormat()
                            wcNotificationSlipVisaChbRatio.Visible = True
                            If visaChbRatio > Netpay.Bll.Risk.Thresholds.VisaMaxChbCountRatio Then wcNotificationSlipVisaChbRatio.Type = NotificationSlipType.Warning
                        End If

                        If stats.MastercardThisMonthChargebacksCount IsNot Nothing And stats.MastercardLastMonthNotChargebacksCount IsNot Nothing Then
                            Dim mastercardChbRatio As Decimal = Math.GetPercentage(stats.MastercardThisMonthChargebacksCount.Value, stats.MastercardLastMonthNotChargebacksCount.Value)
                            wcNotificationSlipMastercardChbRatio.Number = mastercardChbRatio.ToPercentFormat()
                            wcNotificationSlipMastercardChbRatio.Visible = True
                            If mastercardChbRatio > Netpay.Bll.Risk.Thresholds.MastercardMaxChbCountRatio Then wcNotificationSlipMastercardChbRatio.Type = NotificationSlipType.Warning
                        End If
                    End If
                End If
            Catch ex As Exception
                Logger.Log(ex)
            End Try
        End If
    End Sub
End Class