﻿Imports Netpay.Web.Controls
Imports Netpay.Bll
Imports Netpay.Infrastructure
Imports System.IO

Partial Class Website_PaymentManagment
	Inherits MasteredPage

    Protected Overrides Sub OnLoad(e As EventArgs)
        MyBase.OnLoad(e)

        If Not IsPostBack Then
            Dim settings = Bll.Merchants.Hosted.PaymentPage.Load(Merchant.ID)

            If settings IsNot Nothing Then
                Dim reqString As String = " *"

                If Not String.IsNullOrEmpty(settings.LogoImage) Then ImageUpload.ImageUrl = Account.MapPublicVirtualPath(settings.LogoImage)
                ImageUpload.DataBind()

                hfVersion.Value = settings.UIVersion
                hfTheme.Value = settings.ThemeCssFileName


                chIsShowAddress1.Checked = settings.IsShowAddress1
                chIsShowAddress2.Checked = settings.IsShowAddress2
                chIsShowCity.Checked = settings.IsShowCity
                chIsShowZipCode.Checked = settings.IsShowZipCode
                chIsShowState.Checked = settings.IsShowState
                chIsShowCountry.Checked = settings.IsShowCountry
                chIsShowEmail.Checked = settings.IsShowEmail
                chIsShowPhone.Checked = settings.IsShowPhone
                chIsShowPersonalID.Checked = settings.IsShowPersonalID
                chkIsShowBirthDate.Checked = settings.IsShowBirthDate
                chkIsShippingAddressRequire.Checked = settings.IsShippingAddressRequire

                If settings.IsCreditCardRequiredEmail Then chIsShowEmail.Text += reqString ': chIsShowEmail.Enabled = False
                If settings.IsCreditCardRequiredPhone Then chIsShowPhone.Text += reqString ': chIsShowPhone.Enabled = False
                If settings.IsCreditCardRequiredID Then chIsShowPersonalID.Text += reqString ': chIsShowPersonalID.Enabled = False

                Dim IsBillingAddressMust As Boolean = True
                Dim riskSettings = Merchants.RiskSettings.Load(Merchant.ID)
                If riskSettings IsNot Nothing Then IsBillingAddressMust = riskSettings.IsForceBillingAddress
                If IsBillingAddressMust Then
                    chIsShowAddress1.Text += reqString
                    chIsShowAddress2.Text += reqString
                    chIsShowCity.Text += reqString
                    chIsShowZipCode.Text += reqString
                    chIsShowState.Text += reqString
                    chIsShowCountry.Text += reqString
                End If
            End If
            LoadLanguages()
        End If
    End Sub

    Protected Sub DeleteImage_Command(sender As Object, args As EventArgs)
        Dim settings = Bll.Merchants.Hosted.PaymentPage.Load(Merchant.ID)
        If File.Exists(Account.MapPublicPath(settings.LogoImage)) Then File.Delete(Account.MapPublicPath(settings.LogoImage))
        settings.LogoImage = Nothing
        settings.Save()

        Dim options = Bll.Shop.MerchantSettings.Load(Merchant.ID)
        options.LogoFileName = Nothing
        options.Save()

        ImageUpload.ImageUrl = Nothing
        ImageUpload.DataBind()
    End Sub

    Protected Sub LoadLanguages()
        Dim hppLanguages = System.Configuration.ConfigurationManager.AppSettings("HppV2Languages").Split(",")
        Dim defLanguage = Bll.Merchants.Hosted.PaymentPage.GetDefaultLanguage(Merchant.ID)
        Dim allLanguages = Bll.International.Language.Cache.Where(Function(l) hppLanguages.Contains(l.Culture)).ToDictionary(Function(l) l.ID)
        Dim availLanguages As New List(Of String)
        For Each l In allLanguages
            Dim hptexts = Merchants.Hosted.Translation.GetTranslations(Merchant.ID, l.Key, TranslationGroup.HostedPaymentPage)
            If hptexts IsNot Nothing Then
                Dim tab As TabView = tbLanguageText.AddTemplateTab(l.Value.Name)
                tab.ItemData = l.Key
                If hptexts.ContainsKey("FrontTop") Then CType(tab.FindControl("txtFrontTop"), TextBox).Text = hptexts("FrontTop")
                If hptexts.ContainsKey("FrontBottom") Then CType(tab.FindControl("txtFrontBottom"), TextBox).Text = hptexts("FrontBottom")
                If hptexts.ContainsKey("TransSuccess") Then CType(tab.FindControl("txtTransSuccess"), TextBox).Text = hptexts("TransSuccess")
                If hptexts.ContainsKey("TransDecline") Then CType(tab.FindControl("txtTransDecline"), TextBox).Text = hptexts("TransDecline")
                If hptexts.ContainsKey("TransPending") Then CType(tab.FindControl("txtTransPending"), TextBox).Text = hptexts("TransPending")
                CType(tab.FindControl("chkLanguageIsDefault"), CheckBox).Checked = (defLanguage = l.Key)
                CType(tab.FindControl("ddlRemoveLanguage"), LinkButton).Text = GetLocalResourceObject("Remove") + " " + l.Value.Name
                availLanguages.Add(l.Key)
            End If
        Next
        If tbLanguageText.Tabs.Count > 0 And Not IsPostBack Then tbLanguageText.ActiveViewIndex = 0
        For Each l In availLanguages
            allLanguages.Remove(l)
        Next
        Dim oldValue = ddlLanguage.SelectedValue
        ddlLanguage.DataSource = allLanguages.Values
        ddlLanguage.DataTextField = "Name"
        ddlLanguage.DataValueField = "ID"
        ddlLanguage.DataBind()
        ddlLanguage.SelectedValue = oldValue
    End Sub


    Protected Sub AddLanguage_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlLanguage.SelectedIndex < 0 Then Return
        Dim language = CType(ddlLanguage.SelectedValue, Netpay.CommonTypes.Language)
        Dim tab As TabView = tbLanguageText.AddTemplateTab(language.ToString())
        tab.ItemData = language
        tbLanguageText.ActiveViewIndex = tbLanguageText.Tabs.Count - 1
        ddlLanguage.Items.RemoveAt(ddlLanguage.SelectedIndex)
    End Sub

    Protected Sub RemoveLanguage_Command(ByVal sender As Object, ByVal e As CommandEventArgs)
        Dim tab As TabView = tbLanguageText.Tabs(tbLanguageText.ActiveViewIndex)
        ddlLanguage.Items.Add(New ListItem(tab.Text, tab.ItemData))
        Dim wasDefault As Boolean = CType(tab.FindControl("chkLanguageIsDefault"), CheckBox).Checked
        tbLanguageText.Tabs.Remove(tab)
        Merchants.Hosted.Translation.DeleteTranslation(Merchant.ID, CType(tab.ItemData, Netpay.CommonTypes.Language), TranslationGroup.HostedPaymentPage)
        If wasDefault Then
            Dim defLanguageId As Netpay.CommonTypes.Language = EnsureLanguageDefaultExist()
            'dbPages.ExecSql("UPDATE tblMerchantLanguagesHosted SET IsDefault = 1 Where MerchantID=" & WebUtils.LoggedUser.ID & " And LanguageID=" & defLanguageId)
        End If
    End Sub

    Private Function EnsureLanguageDefaultExist() As Netpay.CommonTypes.Language
        Dim defLanguageId As Netpay.CommonTypes.Language = Netpay.CommonTypes.Language.Unknown
        For Each tab As TabView In tbLanguageText.Tabs
            If tab.ItemData IsNot Nothing Then
                Dim isDefault As Boolean = CType(tab.FindControl("chkLanguageIsDefault"), CheckBox).Checked
                If isDefault Then defLanguageId = tab.ItemData
            End If
        Next
        If defLanguageId = Netpay.CommonTypes.Language.Unknown Then
            If tbLanguageText.Tabs.Count > 0 Then
                If CType(tbLanguageText.Tabs(0), TabView).ItemData IsNot Nothing Then
                    CType(tbLanguageText.Tabs(0).FindControl("chkLanguageIsDefault"), CheckBox).Checked = True
                    defLanguageId = CType(tbLanguageText.Tabs(0), TabView).ItemData
                End If
            End If
        End If
        Return defLanguageId
    End Function

    Protected Overrides Sub OnPreRender(e As System.EventArgs)
        If Not ClientScript.IsClientScriptBlockRegistered("EnsureSingleSelect") Then
            Dim Script As String = "function EnsureSingleSelect(chk) {" & vbCrLf & " if(!chk.checked) return;" & vbCrLf
            For Each tab As TabView In tbLanguageText.Tabs
                If tab.ItemData IsNot Nothing Then _
                    Script &= " if (chk.id != '" & tab.FindControl("chkLanguageIsDefault").ClientID & "') document.getElementById('" & tab.FindControl("chkLanguageIsDefault").ClientID & "').checked=false;" & vbCrLf
            Next
            Script &= "}"
            ClientScript.RegisterClientScriptBlock(Me.GetType, "EnsureSingleSelect", Script, True)
        End If
        MyBase.OnPreRender(e)
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        EnsureLanguageDefaultExist()
        For Each tab As TabView In tbLanguageText.Tabs
            If tab.ItemData IsNot Nothing Then
                Merchants.Hosted.Translation.SetTranslation(Merchant.ID, tab.ItemData, TranslationGroup.HostedPaymentPage, "FrontTop", CType(tab.FindControl("txtFrontTop"), TextBox).Text)
                Merchants.Hosted.Translation.SetTranslation(Merchant.ID, tab.ItemData, TranslationGroup.HostedPaymentPage, "FrontBottom", CType(tab.FindControl("txtFrontBottom"), TextBox).Text)
                Merchants.Hosted.Translation.SetTranslation(Merchant.ID, tab.ItemData, TranslationGroup.HostedPaymentPage, "TransSuccess", CType(tab.FindControl("txtTransSuccess"), TextBox).Text)
                Merchants.Hosted.Translation.SetTranslation(Merchant.ID, tab.ItemData, TranslationGroup.HostedPaymentPage, "TransDecline", CType(tab.FindControl("txtTransDecline"), TextBox).Text)
                Merchants.Hosted.Translation.SetTranslation(Merchant.ID, tab.ItemData, TranslationGroup.HostedPaymentPage, "TransPending", CType(tab.FindControl("txtTransPending"), TextBox).Text)
            End If
        Next

        Dim settings = Bll.Merchants.Hosted.PaymentPage.Load(Merchant.ID)
        If settings Is Nothing Then settings = New Bll.Merchants.Hosted.PaymentPage(Merchant.ID)

        If ImageUpload.HasFile Then
            Dim imageFileName = "UIServices\HP_Logo.jpg"
            If ImageUpload.SaveAs(Account.MapPublicPath(imageFileName)) Then
                settings.LogoImage = imageFileName
                ImageUpload.ImageUrl = Account.MapPublicVirtualPath(settings.LogoImage)
                ImageUpload.DataBind()

                ' workaround - shops takes logo from shops settings, saving logo url there too
                'Dim shopsSettings As Bll.Shop.MerchantSettings = Bll.Shop.MerchantSettings.Load(Merchant.ID)
                'ShopsSettings.LogoFileName = imageFileName
                'ShopsSettings.Save()
            End If
        End If

        settings.UIVersion = hfVersion.Value.ToNullableInt().GetValueOrDefault(1)
        settings.ThemeCssFileName = hfTheme.Value

        settings.IsShowAddress1 = chIsShowAddress1.Checked
        settings.IsShowAddress2 = chIsShowAddress2.Checked
        settings.IsShowCity = chIsShowCity.Checked
        settings.IsShowZipCode = chIsShowZipCode.Checked
        settings.IsShowState = chIsShowState.Checked
        settings.IsShowCountry = chIsShowCountry.Checked
        settings.IsShowEmail = chIsShowEmail.Checked
        settings.IsShowPhone = chIsShowPhone.Checked
        settings.IsShowPersonalID = chIsShowPersonalID.Checked
        settings.IsShowBirthDate = chkIsShowBirthDate.Checked
        settings.IsShippingAddressRequire = chkIsShippingAddressRequire.Checked

        For Each tab As TabView In tbLanguageText.Tabs
            If tab.ItemData IsNot Nothing Then
                If CType(tab.FindControl("chkLanguageIsDefault"), CheckBox).Checked Then _
                    settings.MerchantTextDefaultLanguage = tab.ItemData
            End If
        Next
        settings.Save()
    End Sub
End Class
