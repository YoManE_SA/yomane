﻿<%@ Page Title="PageTitle" Language="VB" AutoEventWireup="true" Inherits="Netpay.MerchantControlPanel.Website_Productmanagmnet" CodeBehind="ProductManagment.aspx.vb" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Website/Controls/ShareLinks.ascx" TagPrefix="uc" TagName="ShareLinks" %>
<asp:Content ID="Content3" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
<%--    
    <script type="text/javascript">
        var changeCurrencyAlert = '<asp:Literal runat="server" Text="<%$Resources: ChangeCurrencyAlert %>" />';
    </script>
--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Literal runat="server" Text="<%$Resources: Titleproud %>" />
        </div>
        <div class="content-background">
         <div>
           <div class="circle-step circle-step-active">4</div>
           <div class="circle-step ">3</div>
           <div class="circle-step">2</div>
           <div class="circle-step">1</div>   
           <div class="spacer"></div>
        </div>

            <div class="align-left line-height-35">
                <i class="fa fa-search"></i>
                <asp:Literal runat="server" Text="<%$Resources: SearchByStr %>" />
            </div>
            <div class="align-right">
                <asp:HyperLink runat="server" CssClass="btn btn-info" NavigateUrl="ProductEdit.aspx" Text="<%$Resources: AddNewStr %>" />
            </div>
            <div class="spacer"></div>
            <asp:DropDownList runat="server" ID="ddlLanguage" Visible="false" AppendDataBoundItems="true" onchange="document.location='?PrdLanguage=' + options[selectedIndex].value" CssClass="Field_200">
                <asp:ListItem Text="<%$Resources: ShowInAllLanguages %>" Value="" />
            </asp:DropDownList>
            <div class="section">
                <div class="form-group margin-bottom-10 align-left percent80">
                    <span class="form-group-title">
                        <asp:Literal runat="server" Text="<%$Resources: SearchbytextStr %>" /></span>&nbsp;<span class="form-group-title-eg"><asp:Literal runat="server" Text="<%$Resources: exampleStr %>" /></span>
                    <div>
                        <asp:TextBox ID="txtText" runat="server" class="percent100" />
                    </div>
                </div>

                <div class="form-group margin-bottom-10 align-left percent15">
                    <span class="form-group-title">
                        <asp:Literal runat="server" Text="<%$Resources: CatagoryStr %>" /></span>&nbsp;<span class="form-group-title-eg"><asp:Literal runat="server" Text="<%$Resources: exampleFoodStr %>" /></span>
                    <div>
                        <netpay:DropDownBase runat="server" ID="ddlCategory" class="percent100" EnableBlankSelection="true" BlankSelectionText="All" DataTextField="Name" DataValueField="ID" />
                    </div>
                </div>
                <div class="spacer"></div>
                <div class="wrap-button-bar">
                    <asp:LinkButton runat="server" ID="btnSearch" OnClick="Search_Click" CssClass="btn btn-cons-short btn-inverse"><asp:Literal runat="server" Text="<%$Resources: SearchStr %>" /></asp:LinkButton>
                </div>
            </div>
            <div class="section">
                <asp:Repeater runat="server" ID="rptItems" OnItemDataBound="Product_OnItemDataBound" OnItemCommand="Product_OnItemCommand">
                    <HeaderTemplate>
                        <table class="exspand-table">
                            <tr>
                                <th>
                                    <asp:Literal runat="server" Text="<%$Resources: ID %>" /></th>
                                <th>
                                    <asp:Literal runat="server" Text="<%$Resources: ProudctName %>" /></th>
                                <th>
                                    <asp:Literal runat="server" Text="<%$Resources: Amount %>" /></th>
                                <th>
                                    <asp:Literal runat="server" Text="<%$Resources: Proudctimg %>" /></th>
                                <th>
                                    <asp:Literal runat="server" Text="<%$Resources: Sqrcode %>" /></th>
                                <th>
                                    <asp:Literal runat="server" Text="<%$Resources: Available %>" /></th>
                                <th>
                                    <asp:Literal runat="server" Text="<%$Resources: Order %>" /></th>
                                <th>
                                    <asp:Literal runat="server" Text="<%$Resources: Visable %>" /></th>
                                <th>
                                    <asp:Literal runat="server" Text="<%$Resources: Delete %>" /></th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="text-align-center">
                                <asp:HyperLink runat="server" NavigateUrl='<%# "ProductEdit.aspx?ID=" & Eval("ID")  %>' Text='<%# Eval("ID") %>' /></td>
                            <td>
                                <asp:HyperLink runat="server" NavigateUrl='<%# "ProductEdit.aspx?ID=" & Eval("ID")  %>' Text='<%# CType(Container.DataItem, Netpay.Bll.Shop.Products.Product).GetTextForLanguage(Nothing).Name %>' /></td>
                            <td><%# IIf(CType(Eval("IsDynamicProduct"), Boolean), GetLocalResourceObject("Unlimited"), CType(Eval("Price"), Decimal).ToAmountFormat(CType(Eval("CurrencyIsoCode"), String)))%></td>
                            <td class="text-align-center">
                                <asp:Image runat="server" ID="imgProduct" Width="150" Height="51" />&nbsp;</td>
                            <td class="text-align-center">
                                <asp:Image runat="server" ID="imgQrCode" CssClass="sqr-border" Width="50" /></td>
                            <td class="text-align-center"><%# IIf(CType(Eval("IsDynamicProduct"), Boolean), GetLocalResourceObject("Unlimited"), Eval("QtyAvailable"))%></td>
                            <td class="text-align-center">
                                <asp:UpdatePanel runat="server" ID="upOrder" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:MultiView runat="server" ID="viewStatus" ActiveViewIndex="0">
                                            <asp:View runat="server" ID="viewNormal">
                                                <asp:Literal runat="server" ID="ltOrder" Text='<%# Eval("SortOrder") %>' />
                                                <asp:LinkButton runat="server" ID="btnOrderChange" CommandName="ShowOrder" Font-Size="15px" CommandArgument='<%# Eval("ID") %>'><i runat="server" id="imageEdit" class="fa fa-pencil-square-o"></i></asp:LinkButton>
                                                <i runat="server" class="statusImage fa fa-check-circle" style="display: none;" id="imageSaved"></i>
                                            </asp:View>
                                            <asp:View runat="server" ID="viewEdit">
                                                <div class="pull-left">
                                                    <asp:TextBox runat="server" ID="txtOrder" Text='<%# Eval("SortOrder") %>' Width="40" /></div>
                                                <div class="pull-left saveImageicon">
                                                    <asp:LinkButton runat="server" ID="btnOrderSet" ForeColor="#088159" Font-Size="15px" CommandName="SetOrder" CommandArgument='<%# Eval("ID") %>'><i class="fa fa-floppy-o"></i></asp:LinkButton></div>
                                            </asp:View>
                                        </asp:MultiView>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnOrderChange" />
                                        <asp:AsyncPostBackTrigger ControlID="btnOrderSet" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                <asp:UpdateProgress runat="server" AssociatedUpdatePanelID="upOrder" DisplayAfter="0">
                                    <ProgressTemplate><i class="fa fa-circle-o-notch fa-spin"></i></ProgressTemplate>
                                </asp:UpdateProgress>
                            </td>
                             <td class="text-align-center">
                                <asp:UpdatePanel runat="server" ID="tpStatus" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:LinkButton runat="server" ID="btnEnable" Font-Size="17" ForeColor="Gray" ClientIDMode="Static" CommandName="EnableItem" CommandArgument='<%#Eval("ID")%>' Visible='<%# Not CType(Eval("IsActive"), Boolean)%>'><i class="fa fa-toggle-off"></i></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="btnDisable" Font-Size="17" ForeColor="#1D9E74" ClientIDMode="Static" CommandName="DisableItem" CommandArgument='<%#Eval("ID")%>' Visible='<%# CType(Eval("IsActive"), Boolean)%>'><i class="fa fa-toggle-on"></i></asp:LinkButton>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnEnable" />
                                        <asp:AsyncPostBackTrigger ControlID="btnDisable" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                            <td class="text-align-center">
                                <asp:LinkButton Font-Size="17" runat="server" CommandName="DeleteItem" CommandArgument='<%#Eval("ID")%>' OnClientClick="if(!confirm('Are you sure ?')) return false;"><i class="fa fa-trash-o"></i></asp:LinkButton></td>
                        </tr>
                        <tr>
                            <td colspan="9" style="display: none; padding: 0px; margin: 0px;"></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
            </div>
            <asp:PlaceHolder runat="server" ID="phShare" Visible="false">
                <script type="text/javascript">
                    $(function () {
                        $("#dvShareProduct").dialog({
                            resizable: false,
                            modal: true,
                            draggable: false,
                            autoOpen: true,
                            show: {
                                effect: "fade",
                                duration: 1000
                            },
                            hide: {
                                effect: "fade",
                                duration: 1000
                            }
                        });
                    });
                </script>
                <div id="dvShareProduct" style="width: 400px; display: none; padding: 20px;">
                    <div class="alert-info">
                        <i class="fa fa-share"></i>
                        <asp:Literal runat="server" Text="<%$Resources:ProductEdit.aspx, ShareFriendsStr %>" />
                    </div>
                    <div class="margin-top-10">
                        <uc:ShareLinks shareurl='<%#ShareProductLink %>' runat="server" ID="ShareLinksFinish" />
                    </div>
                    <div class="spacer"></div>
                </div>
            </asp:PlaceHolder>

<%--            
        <i class="fa fa-money"></i> <asp:Literal ID="Literal1" runat="server" Text="<%$Resources: ShopCurrency %>" />
            <div class="section">
                <div class="form-group margin-bottom-10 align-left percent23">
                    <span class="form-group-title">
                   <asp:Literal ID="Literal3" runat="server" Text="<%$Resources: Currency %>" /> </span>&nbsp;<span class="form-group-title-eg"><asp:Literal ID="Literal2" runat="server" Text="<%$Resources: EGCurrency %>" /></span>
                    <div>
                          <netpay:CurrencyDropDown ID="ddlCurrencyISOCode" AutoPostBack="false" CssClass="percent100" ClientIDMode="Static" EnableBlankSelection="false" runat="server" />
                    </div>
                </div>
                <div class="spacer"></div>
                <div class="wrap-button-bar">
                    <asp:LinkButton ID="btnCurrencyChange" OnClick="btnCurrencyChange_Click" CssClass="btn btn-cons-short btn-primary" OnClientClick="return confirm(changeCurrencyAlert)" Text="<%$Resources: ChangeCurrency %>" runat="server" />
                </div>
            </div>
--%>
        </div>
    </div>
</asp:Content>






