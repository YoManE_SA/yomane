﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Linq
Imports Netpay.Bll
Imports Netpay.Bll.Reports
Imports System.Collections.Generic
Imports Netpay.Web
Imports Netpay.Infrastructure
Imports Netpay.Web.Controls

Partial Class Website_CartsView
	Inherits MasteredPage

	Protected Sub Search(ByVal sender As Object, ByVal currentPage As Integer, ByVal e As EventArgs)
		Dim filters As New SearchFilters
		If BtnSearch Is BtnSearch Then
			If Not String.IsNullOrEmpty(txAmountFrom.Text) Then filters.amountFrom = txAmountFrom.Text
			If Not String.IsNullOrEmpty(txAmountTo.Text) Then filters.amountTo = txAmountTo.Text
			If wcDateRangePicker.FromDate IsNot Nothing Then filters.dateFrom = wcDateRangePicker.FromDate.Value
			If wcDateRangePicker.ToDate IsNot Nothing Then filters.dateTo = wcDateRangePicker.ToDate.Value
			If ddCartStatus.SelectedItem IsNot Nothing Then filters.cartStatus = ddCartStatus.SelectedValue.ToInt32(0)
		Else
			If Not String.IsNullOrEmpty(txCart.Text) Then filters.transactionID = txCart.Text.ToInt32(0)
		End If
		If TypeOf e Is ExcelButton.ExportEventArgs Then
			btnExportExcel.SetExportInfo("Merchant Carts", ReportType.MerchantCarts, Function(credToken, pi) Cart.SearchCarts(credToken, filters, pi))
		Else
			repeaterResults.DataSource = Cart.SearchCarts(WebUtils.CredentialsToken, filters, pager.Info)
			repeaterResults.DataBind()
			btnExportExcel.ResultCount = pager.Info.TotalItems
			phResults.Visible = repeaterResults.Items.Count > 0
		End If
	End Sub

	Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
		Search(sender, 1, e)
		pager.Visible = True
	End Sub

	Protected Sub ExportExcel_Click(ByVal sender As Object, ByVal e As EventArgs)
		Search(sender, 1, e)
		btnExportExcel.Export()
	End Sub
End Class
