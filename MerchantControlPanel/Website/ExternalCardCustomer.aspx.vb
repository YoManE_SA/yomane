﻿Imports Netpay.Bll
Imports Netpay.Infrastructure.VO
Imports Netpay.Bll.ExternalCards

Public Class ExternalCardCustomer
    Inherits MasteredPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request("__EVENTTARGET") = "ctl00$cphBody$pager" Then
            GetPayments()
        End If

        If Not IsPostBack Then
            GetPayments()
            Dim customer As ExternalCardCustomerVO = Netpay.Bll.ExternalCards.GetCustomer(LoggedUser.CredentialsToken, CustomerID)
            txtCustomerName.Text = customer.Name + " | " + customer.Email + ""
            btnPayToCustomer.Enabled = customer.IsPayable
            wcElementBlocker.Visible = Not Netpay.Bll.ExternalCards.IsProviderAvailable(LoggedUser.CredentialsToken, customer.Provider)

            Select Case customer.Provider
                Case Infrastructure.ExternalCardProviderEnum.LiveKash
                    linkBackToCustomers.HRef = "LiveKashCustomers.aspx"
                    txtTitle.Text = GetLocalResourceObject("titleLiveKash")
                Case Infrastructure.ExternalCardProviderEnum.Payoneer
                    linkBackToCustomers.HRef = "PayoneerCustomers.aspx"
                    txtTitle.Text = GetLocalResourceObject("titlePayoneer")
                Case Else
                    Throw New ApplicationException("unknown provider")
            End Select
        End If
		lblMsg.Text = ""
    End Sub

    Private ReadOnly Property CustomerID As Int32
        Get
            Return Int32.Parse(Request("customerID"))
        End Get
    End Property

    Private Sub GetPayments()
        Dim results As List(Of ExternalCardCustomerPaymentVO) = Netpay.Bll.ExternalCards.GetPayments(LoggedUser.CredentialsToken, CustomerID, pager.Info)
        repeaterResults.DataSource = results
        repeaterResults.DataBind()

        If results IsNot Nothing Then
            phResults.Visible = (results.Count > 0)
        End If
    End Sub

    Protected Sub PayToCustomer(ByVal sender As Object, ByVal e As System.EventArgs)
        lblMsg.Text = ""
        Dim amount As Decimal
        If Not Decimal.TryParse(txtAmount.Value, amount) Then
            lblMsg.Text = "<div class=""Error2"">" & GetGlobalResourceObject("MultiLang", "ErrorAdding") & ": " & GetLocalResourceObject("InvalidAmount") & "</div>"
            GetPayments()
            Exit Sub
        End If

        Dim result As ExternalCardPaymentResult = Netpay.Bll.ExternalCards.PayToCustomer(LoggedUser.CredentialsToken, CustomerID, amount)
        If result.Result = ExternalCardPaymentResultEnum.Success Then
            lblMsg.Text = GetLocalResourceObject("paymentSuccess")
        Else
			lblMsg.Text = "<div class=""Error2"">" & GetLocalResourceObject("PayoutDeclined") & " " & GetLocalResourceObject(result.ToString()) & "</div>"
        End If

        GetPayments()
    End Sub

    Protected Function GetCssClass(result As ExternalCardCustomerPaymentVO) As String
        If result.Result = "000" Then
            Return "payoneerNoError"
        Else
            Return "payoneerError"
        End If
    End Function
End Class