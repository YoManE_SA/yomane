﻿Imports System.Data.SqlClient
Imports Netpay.Infrastructure
Imports Netpay.CommonTypes
Imports Netpay.Web
Imports Netpay.Bll
Imports Netpay.Web.Controls

Partial Class Website_RecurringEditCharge
	Inherits MasteredPage

	Protected sSQL As String, sSQL1 As String, sSQL2 As String, CcStorageID As String, strState As Integer = 0, sCardNumber As String, sMember As String
	Protected nCharge As Integer, sStateISO As String, sDateSQL As String, sAmountSQL As String, sCurrencySQL As String, sCreditCardSQL As String, sWhereCharge As String
	Protected nSeries As Integer, nChargeNumber As Integer, nChargeCount As Integer, nDelayLength As Integer, i As Integer
	Protected dr As SqlDataReader, dr1 As SqlDataReader, dr2 As SqlDataReader
	Protected dtCharge As DateTime
	Protected sBack As String = ""

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sBack = GetGlobalResourceObject("MultiLang", "Back").ToString()

        nCharge = dbPages.TestVar(Request("Charge"), 1, 0, 0)

		If Not Page.IsPostBack Then
			For i As Integer = 1 To 12
				ddlsExpMonth.Items.Add(i.ToString("00"))
			Next
			For i As Integer = Date.Now.Year To Date.Now.Year + 11
				ddlsExpYear.Items.Add(i.ToString("0000"))
			Next

			For i As Integer = 0 To 50
				ddlDelayLength.Items.Add(i.ToString("0"))
			Next

			If (ddlCountry.SelectedValue = "228") Then ' Usa for state
				ddlStateUsa.SelectedValue = dr("stateId").ToString
				DivUsa.Style.Add("display", "block")
				DivCanada.Style.Add("display", "none")
			Else
				DivUsa.Style.Add("display", "block")
				DivCanada.Style.Add("display", "none")
			End If

			If (ddlCountry.SelectedValue = "43") Then	' Canada for state
				ddlStateCanada.SelectedValue = dr("stateId").ToString
				DivUsa.Style.Add("display", "none")
				DivCanada.Style.Add("display", "block")
			End If

			sSQL = "SELECT tblRecurringCharge.*, rs_ChargeCount FROM tblRecurringCharge" & _
			" INNER JOIN tblRecurringSeries ON tblRecurringCharge.rc_Series=tblRecurringSeries.ID" & _
			" WHERE rs_Deleted=0 AND tblRecurringCharge.ID=" & dbPages.TestVar(nCharge, 0, -1, 0)
			dr = dbPages.ExecReader(sSQL)

			If Not dr.Read Then
				nSeries = 0
				nChargeNumber = 0
				nChargeCount = 0
			Else
				nSeries = dr("rc_Series")
				nChargeNumber = dr("rc_ChargeNumber")
				nChargeCount = dr("rs_ChargeCount")
				txtAmount.Text = Decimal.Parse(dr("rc_Amount")).ToString("0.00")
                ddlCurrency.Value = dr("rc_Currency")
                txtMemo.Text = dr("rc_Comments")

				ddlDelayUnit.SelectedIndex = -1
				ddlDelayLength.SelectedIndex = -1
				dtCharge = dr("rc_Date")
			End If
			dr.Close()

			sSQL1 = "SELECT * ,dbo.fnFormatCcNumToHideDigits(CardNumber) as cardProtected FROM RecurringGetPaymentMethod(" & dbPages.TestVar(nCharge, 0, -1, 0) & ") WHERE rs_Company=" & Merchant.ID
			dr1 = dbPages.ExecReader(sSQL1)
			If dr1.Read Then
				If dr1("CardNumber").ToString <> "" Then
                    litCardProtected.Text = "<span>" & dr1("cardProtected").ToString & "</span>"
					sCardNumber = dr1("CardNumber").ToString
					sMember = dr1("Member").ToString
					pnlCardData.Visible = True
				Else
					pnlEcheck.Visible = True
				End If
				txtCHPhoneNumber.Text = dr1("PhoneNumber").ToString().ToEncodedHtml()
				txtcc_cui.Text = Len(dr1("CUI").ToString).ToString("xxx")
				txtCHEmail.Text = dr1("Email").ToString().ToEncodedHtml()
				txtAddress1.Text = dr1("address1").ToString().ToEncodedHtml()
				txtAddress2.Text = dr1("address2").ToString().ToEncodedHtml()
				txtCHSCity.Text = dr1("City").ToString().ToEncodedHtml()
				txtZipcode.Text = dr1("zipCode").ToString().ToEncodedHtml()
				ddlsExpMonth.SelectedValue = dr1("ExpMonth")
				Dim nNewYear As Integer = dbPages.TestVar(dr1("ExpYear"), 0, -1, 0)
				If nNewYear < 2000 Then nNewYear += 2000

				Dim item1 As ListItem
				item1 = ddlsExpYear.Items.FindByValue(nNewYear)
				If Not IsNothing(item1) Then
					item1.Selected = True
				Else
					ddlsExpYear.SelectedValue = ""
				End If

				ddlCountry.SelectedValue = dr1("countryId").ToString
				If (ddlCountry.SelectedValue = "228") Then ' Usa for state
					ddlStateUsa.SelectedValue = dr1("stateId").ToString
					DivUsa.Style.Add("display", "block")
					ddlStateUsa.Enabled = True
					DivCanada.Style.Add("display", "none")
				Else
					ddlStateUsa.Enabled = False
					DivUsa.Style.Add("display", "block")
					DivCanada.Style.Add("display", "none")
				End If

				If (ddlCountry.SelectedValue = "43") Then	' Canada for state
					ddlStateCanada.SelectedValue = dr1("stateId").ToString
					DivUsa.Style.Add("display", "none")
					DivCanada.Style.Add("display", "block")
				End If
                sSQL2 = "SELECT tblCreditCardBIN.isoCode, PaymentMethod, GD_Text, IsNull(cl.Name, '') CountryName, IsNull(cl.Name, '') CountryNameHe FROM tblCreditCardBIN" & _
                   " INNER JOIN tblGlobalData ON tblCreditCardBIN.PaymentMethod=GD_ID" & _
                   " LEFT JOIN [List].[CountryList] AS cl ON tblCreditCardBIN.isoCode = cl.CountryISOCode" & _
                   " WHERE GD_Group=42 AND GD_Lng=" & Netpay.Web.WebUtils.CurrentLanguage & " AND BIN LIKE '" & sCardNumber.Substring(0, 6).ToSql(True) & "%' ORDER BY BinLen DESC"

				dr2 = dbPages.ExecReader(sSQL2)
				If Not dr2.Read() Then
					litPaymentMethod.Text = GetLocalResourceObject("Unknown")
				Else
                    litPaymentMethod.Text = "<img src=""/NPCommon/ImgPaymentMethod/23X12/" & dr2("PaymentMethod") & ".gif"" alt=""" & dr2("GD_Text") & """ align=""middle"" border=""0"" />&nbsp;" & dr2("GD_Text")
                    litPaymentMethod.Text = litPaymentMethod.Text & "&nbsp;&nbsp;&nbsp;<img src=""/NPCommon/ImgCountry/18X12/" & dr2("isoCode") & ".gif"" alt=""" & dr2("isoCode") & " " & IIf(Netpay.Web.WebUtils.CurrentLanguage = Language.English, dr2("CountryName"), dr2("CountryNameHe")) & """ align=""middle"" border=""0"" />"
					litPaymentMethod.Text = litPaymentMethod.Text & "&nbsp;" & IIf(Netpay.Web.WebUtils.CurrentLanguage = Language.English, dr2("CountryName"), dr2("CountryNameHe"))
				End If
				dr2.Close()
			Else
                litCardProtected.Text = "<span>No charge found for recurring series #" & nSeries & "<span>"
			End If
			dr1.Close()
		End If

	End Sub

	Sub BtnUpdate_Click(ByVal o As Object, ByVal e As System.EventArgs)
		nCharge = dbPages.TestVar(Request("Charge"), 1, 0, 0)
		sSQL = "SELECT rc_Series FROM tblRecurringCharge WHERE ID = " & nCharge
		dr = dbPages.ExecReader(sSQL)
		If dr.Read Then
			nSeries = dr("rc_Series")
		Else
			dr.Close()
			Throw New ApplicationException("Counld not get series id.")
		End If
		dr.Close()
		ddlStateUsa.Enabled = True
		ddlStateCanada.Enabled = True
		If (ddlCountry.SelectedValue = "228") Then ' Usa for state
			strState = ddlStateUsa.SelectedValue.ToInt32(0)
			DivUsa.Style.Add("display", "block")
			DivCanada.Style.Add("display", "none")
		End If

		If (ddlCountry.SelectedValue = "43") Then	' Canada for state
			strState = ddlStateCanada.SelectedValue.ToInt32(0)
			DivCanada.Style.Add("display", "block")
			DivUsa.Style.Add("display", "none")
		End If

		If Not String.IsNullOrEmpty(strState) Then
            sStateISO = dbPages.ExecScalar("SELECT StateISOCode FROM [List].[StateList] WHERE StateID=" & strState)
		Else
			sStateISO = ""
			strState = 0
		End If

        Dim sCountryISO As String = dbPages.ExecScalar("SELECT CountryISOCode FROM [List].[CountryList] WHERE CountryID = " & ddlCountry.SelectedValue.ToInt32(0))

		sSQL = "SET NOCOUNT ON;INSERT INTO tblBillingAddress" & _
		" (address1, address2, city, zipCode, stateId, countryId, stateIso, countryIso)" & _
		" VALUES ('" & txtAddress1.Text.ToSql(True) & "', '" & txtAddress2.Text.ToSql(True) & "', '" & txtCHSCity.Text.ToSql(True) & "', '" & txtZipcode.Text.ToSql(True) & "'," & _
		" " & strState & ", " & ddlCountry.SelectedValue.ToInt32(0) & ", '" & sStateISO.ToSql(True) & "', '" & sCountryISO.ToSql(True) & "')" & _
		" ;SELECT @@IDENTITY;SET NOCOUNT ON;"
		Dim nBillingAddressID As Integer = dbPages.TestVar(dbPages.ExecScalar(sSQL), 0, -1, 0)

		'Saving new credit card
		sSQL = "SELECT rc_CreditCard FROM tblRecurringCharge WHERE ID=" & dbPages.TestVar(nCharge, 0, -1, 0)
		Dim nCreditCardOld As Integer = dbPages.ExecScalar(sSQL)
		Dim nCreditCardNew As Integer = 0
		If nCreditCardOld > 0 Then
			'it is credit card charge
			Dim nCVV2 As Integer = dbPages.TestVar(txtcc_cui.Text, 0, 9999, -1)
			If nCVV2 = -1 Then
				txtcc_cui.Text = ""
			End If

			sSQL = "EXEC CloneCreditCard " & nCreditCardOld & ", " & Merchant.ID & ", " & Convert.ToInt32(ddlsExpMonth.SelectedValue) & ", " & Convert.ToInt32(ddlsExpYear.SelectedValue) & ", '" & txtcc_cui.Text.ToSql() & "', '" & txtCHPhoneNumber.Text.ToSql(True) & "', '" & txtCHEmail.Text.ToSql(True) & "', " & nBillingAddressID
			nCreditCardNew = dbPages.ExecScalar(sSQL)
		End If

		'Saving charge settings
		sDateSQL = ""
		Dim nDelayUnit As Integer = dbPages.TestVar(ddlDelayUnit.SelectedValue, 0, 50, 0)
		If nDelayUnit > 0 Then
			Dim nDelayLength As Integer = dbPages.TestVar(Request.Form("DelayLength"), 1, 4, 0)
			If nDelayLength > 0 Then
				sDateSQL = "dbo.RecurringGetChargeDate(rc_Date, 2, " & nDelayUnit & ", " & nDelayLength & ")"
			End If
		End If

		If sDateSQL = "" Then sDateSQL = "rc_Date"
        Dim nAmount As Decimal = txtAmount.Text.ToDecimal(0)

		If nAmount > 0 Then
			sAmountSQL = nAmount
		Else
			sAmountSQL = "rc_Amount"
		End If

		Dim maxCurrency As Integer = Bll.Currency.Cache.Count - 1
		Dim nCurrency As Integer = dbPages.TestVar(ddlCurrency.SelectedValue, 0, maxCurrency, -1)
		If nCurrency < 0 Then
			sCurrencySQL = "rc_Currency"
		Else
			sCurrencySQL = nCurrency
		End If

		Dim sMemo As String = txtMemo.Text.ToSql(True)
		If CLng(nCreditCardNew) = 0 Then
			sCreditCardSQL = "rc_CreditCard"
		Else
			sCreditCardSQL = nCreditCardNew
		End If

		Dim bApplyToAll As Boolean = False
		If dbPages.TestVar(Request.Form("ApplyToAll"), 1, 0, 0) = 1 Then bApplyToAll = True

		If bApplyToAll Then
			sWhereCharge = "rc_ChargeNumber>=(SELECT rc_ChargeNumber FROM tblRecurringCharge WHERE ID=" & dbPages.TestVar(nCharge, 0, -1, 0) & ")"
		Else
			sWhereCharge = "ID=" & dbPages.TestVar(nCharge, 0, -1, 0)
		End If

		sSQL = "UPDATE tblRecurringCharge SET rc_Date=" & sDateSQL & ", rc_Amount=" & sAmountSQL & "," & _
		" rc_Currency=" & dbPages.TestVar(sCurrencySQL, 0, -1, 0) & ", rc_CreditCard=" & sCreditCardSQL & "," & _
		" rc_Comments='" & sMemo.ToSql(True) & "' WHERE " & sWhereCharge & _
		" AND rc_Paid=0 AND rc_Series=" & dbPages.TestVar(nSeries, 1, 0, 0) & " AND rc_Series IN" & _
		" (SELECT ID FROM tblRecurringSeries WHERE rs_Deleted=0 AND rs_Company=" & Merchant.ID & ")"
		dbPages.ExecSql(sSQL)

		If bApplyToAll Then
			sSQL = "EXEC RecurringRecalculateChargeDates " & dbPages.TestVar(nCharge, 0, -1, 0)
			dbPages.ExecSql(sSQL)
		End If

		' recalculate series total
        Netpay.Bll.Transactions.Recurring.Series.UpdateSeriesTotal(nSeries)

		' redirect
		Response.Redirect("RecurringChargesMain.aspx?seriesID=" & dbPages.TestVar(nSeries, 1, 0, 0))
	End Sub
End Class
