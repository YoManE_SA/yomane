﻿<%@ Page Language="VB" Title="PageTitle" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="true" Inherits="Netpay.MerchantControlPanel.Website_StoreMethoodPage_Wizard" CodeBehind="DevStorage_Wizard.aspx.vb" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:menu, StorageWizard%>" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphHead" runat="Server">
    <script type="text/javascript">
        function showSection(secName) {
            with (document.getElementById(secName)) {
                if (style.display != '')
                    style.display = '';
                else
                    style.display = 'none';

                document.getElementById(secName + 'Img').src = (style.display != '' ? '/NPCommon/Images/tree_expand.gif' : '/NPCommon/Images/tree_collapse.gif');
            }
        }
    </script>

</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content" id="divContent">

        <div class="top-nav">
            <asp:Literal Text="<%$Resources: Title %>" runat="server" />
        </div>
        <div class="content-background">
            <div class="alert alert-info margin-bottom-10">
                <asp:Literal Text="<%$Resources: Intro %>" runat="server" />
            </div>


            <div class="section">
                <!-- can't remove it -->
                <!--
			        <tr>
				        merchantID
				        <asp:TextBox runat="server" class="wide" id="merchantID" Enabled="false" />
			        </tr>
			    -->

                <div class="alert alert-info margin-bottom-10">
                    <asp:Literal Text="<%$Resources: Help_Create %>" runat="server" />
                </div>

                <div class="margin-bottom-10 frame-border-option">
                    <div class="margin-bottom-10">
                        <i class="fa fa-globe"></i>
                        <asp:Literal Text="<%$Resources: UI_Language %>" runat="server" />
                    </div>

                    <asp:RadioButtonList runat="server" CssClass="group-list" ID="disp_lng" RepeatDirection="Vertical" RepeatLayout="UnorderedList">
                        <asp:ListItem Text="<%$Resources: English %>" Value="en-us" Selected="True" />
                        <asp:ListItem Text="<%$Resources: Hebrew %>" Value="he-il" />
                        <asp:ListItem Text="<%$Resources: Italian %>" Value="it-it" />
                    </asp:RadioButtonList>

                    <asp:PlaceHolder ID="phMD5Required" Visible="false" runat="server">
                        <div class="margin-top-20">
                           <div class="margin-bottom-10">
                            <asp:Literal Text="<%$Resources: MD5_signature %>" runat="server" /></div>
                            <ul class="group-list">
                                <li>
                                    <asp:CheckBox runat="server" ID="cbMD5Required" Checked="true" Enabled="false" />&nbsp;
                                    <asp:Literal Text="<%$Resources: signature_string %>" runat="server" />
                                </li>
                            </ul>
                        </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="phOptionalMD5" Visible="false" runat="server">
                        <ul class="group-list">
                            <li>
                                <asp:CheckBox runat="server" ID="cbMD5Optional" />
                                <asp:Literal Text="<%$Resources: signature_string %>" runat="server" />
                            </li>
                        </ul>
                        <asp:Literal Text="<%$Resources: MD5_signature %>" runat="server" />
                    </asp:PlaceHolder>
                </div>
                <div class="margin-bottom-10 frame-border-option">
                    <div class="margin-bottom-10">
                       <i class="fa fa-info-circle"></i>
                        <asp:Literal Text="<%$Resources: Optional_Data %>" runat="server" />
                    </div>
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">

                        <div class="form-group">
                            <div class="margin-bottom-10">
                                <span>
                                    <asp:Literal Text="<%$Resources: Pay_For %>" runat="server" />
                                    <netpay:PageItemHelp ID="PageItemHelp1" Text="<%$Resources: Text_display %>" Width="300" runat="server" />
                                </span>
                                <div>
                                    <asp:TextBox runat="server" class="percent100" ID="disp_payFor" Text="<%$Resources: Purchase %>" />
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="margin-bottom-10">
                                <span>
                                    <asp:Literal Text="<%$Resources: Trans_Comment %>" runat="server" /></span>
                                <div>

                                    <asp:TextBox runat="server" class="percent100" ID="trans_comment" MaxLength="500" Text="<%$Resources: Test_PP %>" />
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="margin-bottom-10">
                                <span>
                                    <asp:Literal Text="<%$Resources: Redirect_URL %>" runat="server" />
                                    <netpay:PageItemHelp ID="PageItemHelp2" Text="<%$Resources: URL_Customer %>"
                                        Width="300" runat="server" />
                                </span>
                                <div>
                                    <asp:TextBox runat="server" class="percent100 url-input" ID="url_redirect" />
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="margin-bottom-10">
                                <span>
                                    <asp:Literal Text="<%$Resources: Notify_URL %>" runat="server" />
                                    <netpay:PageItemHelp ID="PageItemHelp3" Text="<%$Resources: URL_Server %>"
                                        Width="300" runat="server" />
                                </span>
                                <div>
                                    <asp:TextBox runat="server" class="percent100 url-input" ID="url_notify" />
                                </div>
                            </div>
                        </div>
                        <div class="spacer">
                        </div>
                        <div class="wrap-expand-details">
                            <div onclick="showSection('tdClientInfo')" class="cursor">
                                <img id="tdClientInfoImg" src="/NPCommon/Images/tree_expand.gif" alt="expand" align="top" /><span
                                    class="SecHeading"><asp:Literal Text="<%$Resources: Optional_Client_Data %>" runat="server" /></span>
                                <netpay:PageItemHelp ID="PageItemHelp5" Text="<%$Resources: Depending_Payment %>"
                                    Width="300" runat="server" />
                            </div>
                            <div style="display: none;" class="margin-top-10" id="tdClientInfo">
                                <div class="align-left percent32 form-group">
                                    <div class="margin-bottom-10">
                                        <span>
                                            <asp:Literal Text="<%$Resources: Full_Name %>" runat="server" /></span>
                                        <div>
                                            <asp:TextBox ID="client_fullName" class="percent100" runat="server" />
                                        </div>
                                    </div>
                                </div>


                                <div class="align-left percent32 form-group">
                                    <div class="margin-bottom-10">
                                        <span>
                                            <asp:Literal Text="<%$Resources: Mail_Address %>" runat="server" /></span>
                                        <div>

                                            <asp:TextBox ID="client_email" class="percent100" runat="server" />
                                        </div>
                                    </div>
                                </div>

                                <div class="align-left percent32 form-group">
                                    <div class="margin-bottom-10">
                                        <span>
                                            <asp:Literal Text="<%$Resources: Phone_Number %>" runat="server" /></span>
                                        <div>
                                            <asp:TextBox ID="client_phoneNum" class="percent100" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="align-left percent32 form-group">
                                    <div class="margin-bottom-10">
                                        <span>
                                            <asp:Literal Text="<%$Resources: ID_Number %>" runat="server" /></span>
                                        <div>
                                            <asp:TextBox ID="client_idNum" class="percent100" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="align-left percent32 form-group">
                                    <div class="margin-bottom-10">
                                        <span>
                                            <asp:Literal Text="<%$Resources: Billing_Address_1 %>" runat="server" /></span>
                                        <div>
                                            <asp:TextBox ID="client_billaddress1" class="percent100" runat="server" />
                                        </div>
                                    </div>
                                </div>


                                <div class="align-left percent32 form-group">
                                    <div class="margin-bottom-10">
                                        <span>
                                            <asp:Literal Text="<%$Resources: Billing_Address_2 %>" runat="server" /></span>
                                        <div>
                                            <asp:TextBox ID="client_billaddress2" class="percent100" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="align-left percent32 form-group">
                                    <div class="margin-bottom-10">
                                        <span>
                                            <asp:Literal Text="<%$Resources: Billing_City %>" runat="server" /></span>
                                        <div>
                                            <asp:TextBox ID="client_billcity" class="percent100" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="align-left percent32 form-group">
                                    <div class="margin-bottom-10">
                                        <span>
                                            <asp:Literal Text="<%$Resources: Billing_Zipcode %>" runat="server" /></span>
                                        <div>
                                            <asp:TextBox ID="client_billzipcode" class="percent100" runat="server" />
                                        </div>
                                    </div>
                                </div>

                                <div class="align-left percent32 form-group">
                                    <div class="margin-bottom-10">
                                        <span>
                                            <asp:Literal Text="<%$Resources: Billing_State %>" runat="server" /></span>
                                        <div>
                                            <asp:TextBox ID="client_billstate" class="percent100" runat="server" />
                                        </div>
                                    </div>
                                </div>

                                <div class="align-left percent32 form-group">
                                    <div class="margin-bottom-10">
                                        <span>
                                            <asp:Literal Text="<%$Resources: Billing_Country %>" runat="server" /></span>
                                        <div>
                                            <asp:TextBox ID="client_billcountry" class="percent100" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="spacer"></div>
                            </div>
                        </div>


                    </div>

                </div>
                <div class="margin-bottom-10 frame-border-option">
                    <div class="margin-bottom-10">
                        <i class="fa fa-code"></i>
                        <asp:Literal Text="<%$Resources: Generated_Code %>" runat="server" />
                    </div>





                    <div class="frame-border-option-inline padding-10 margin-bottom-10">

                        <asp:TextBox runat="server" CssClass="percent100 url-input" Height="60" ReadOnly="true" ID="txQuery" TextMode="MultiLine" />
                        <div class="margin-top-10">
                            <asp:LinkButton runat="server" CssClass="btn btn-info btn-cons-short" ID="btnGenerate" OnClick="btnGenerate_Click"><i class="fa fa-code"></i> <asp:Literal Text="<%$Resources: Refresh_Code %>" runat="server" /></asp:LinkButton>
                             <asp:LinkButton runat="server" CssClass="btn btn-info btn-cons-short" ID="btnOpenPage" OnClick="btnOpenPage_Click" UseSubmitBehavior="true"><i class="fa fa-external-link"></i> <asp:Literal Text="<%$Resources: Open_Window %>" runat="server" /></asp:LinkButton>
                        </div>
                           
                       

                        <div class="margin-top-10">
                            <asp:Literal Text="<%$Resources: Refresh_Copy %>" runat="server" />
                        </div>
                        <asp:Literal ID="OpenScript" runat="server" Mode="PassThrough" />
                    </div>

                </div>
            </div>
        </div>
    </div>

</asp:Content>
