﻿Imports Netpay.Web
Imports Netpay.Infrastructure

Partial Class Website_CreditCardStorageUpdate
	Inherits MasteredPage

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Dim cardID As Integer = Request("cardID").ToInt32(0)
            Dim card As Bll.Merchants.MethodStorage = Bll.Merchants.MethodStorage.Load(Merchant.ID, cardID)
			If card Is Nothing Then Throw New ApplicationException("Card not found.")
			wcExpirationYear.YearFrom = DateTime.Now.Year
			wcExpirationYear.YearTo = DateTime.Now.Year + 10

			If card.MethodInstance.ExpirationDate IsNot Nothing Then
				If card.MethodInstance.ExpirationDate.Value.Year < DateTime.Now.Year Then wcExpirationYear.YearFrom = card.MethodInstance.ExpirationDate.Value.Year
				wcExpirationYear.SelectedYear = card.MethodInstance.ExpirationDate.Value.Year
				wcExpirationMonth.SelectedMonth = card.MethodInstance.ExpirationDate.Value.Month
			End if
			wcPaymentMethod.MethodData = card.MethodInstance

			txtCardholderPhoneNumber.Text = card.Owner_PhoneNumber
			txtCardholderFullName.Text = card.Owner_FullName
			txtCardholderCity.Text = card.BA_City
			txtCardholderEmail.Text = card.Owner_EmailAddress
			txtCardholderID.Text = card.Owner_PersonalNumber
			txtClientIP.Text = card.IPAddress

			txtCardholderStreet1.Text = card.BA_Street1
			txtCardholderStreet2.Text = card.BA_Street2
			txtCardholderZipcode.Text = card.BA_PostalCode
			txtComment.Text = card.Comment

			If card.BA_CountryISOCode IsNot Nothing Then
				ddlCountry.ISOCode = card.BA_CountryISOCode
				If card.BA_CountryISOCode = "USD" Then
					ddlStateUsa.ISOCode = card.BA_StateISOCode
					DivCanada.Attributes.Add("Style", "display:none;")
				ElseIf card.BA_CountryISOCode = "CAN" Then
					ddlStateCanada.ISOCode = card.BA_StateISOCode
					DivUsa.Attributes.Add("Style", "display:none;")
				Else
					DivCanada.Attributes.Add("Style", "display:none;")
					ddlStateUsa.Enabled = False
				End If
			Else
				DivCanada.Attributes.Add("Style", "display:none;")
			End If
		End If
	End Sub

	Protected Sub UpdateStoredCard(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
		Dim expirationDate As DateTime = New DateTime(wcExpirationYear.SelectedYear, wcExpirationMonth.SelectedMonth, 1)
        If expirationDate < DateTime.Now Then
            lblResult.Visible = True
            lblResult.Text = GetLocalResourceObject("cardExpired")
            Return
        End If

        If txtCardholderFullName.Text.Trim() = "" Then
            lblResult.Visible = True
            lblResult.Text = GetLocalResourceObject("cardholderNameRequired")
            Return
        End If

        Dim data = Bll.Merchants.MethodStorage.Load(Merchant.ID, Request("cardID").ToInt32(0))
		data.MethodInstance.SetExpirationMonth(wcExpirationYear.SelectedYear.Value, wcExpirationMonth.SelectedMonth.Value)
		data.IPAddress = txtClientIP.Text
		data.BA_City = txtCardholderCity.Text
		data.Owner_EmailAddress = txtCardholderEmail.Text
		data.Owner_FullName = txtCardholderFullName.Text
		data.Owner_PersonalNumber = txtCardholderID.Text
		data.Owner_PhoneNumber = txtCardholderPhoneNumber.Text
		data.BA_Street1 = txtCardholderStreet1.Text
		data.BA_Street2 = txtCardholderStreet2.Text
		data.BA_PostalCode = txtCardholderZipcode.Text
		data.Comment = txtComment.Text
		data.BA_CountryISOCode = ddlCountry.ISOCode
		If data.BA_CountryISOCode IsNot Nothing Then
			If data.BA_CountryISOCode = "USD" Then
				data.BA_StateISOCode = ddlStateUsa.SelectedValue.ToNullableInt32()
			ElseIf data.BA_CountryISOCode = "CAD"  Then
				data.BA_StateISOCode = ddlStateCanada.SelectedValue.ToNullableInt32()
			End If
		End If
		data.Save()
		Response.Redirect("CreditCardStorage.aspx")
	End Sub
End Class
