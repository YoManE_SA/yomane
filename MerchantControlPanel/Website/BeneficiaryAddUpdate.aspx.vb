﻿Imports System.Data.SqlClient
Imports Netpay.Web
Imports Netpay.Infrastructure

Partial Class Website_BeneficiaryAddUpdate
	Inherits MasteredPage

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
			Dim data = Infrastructure.GlobalData.GetGroup(GlobalDataGroup.BeneficiaryProfileType, WebUtils.CurrentLanguage)
			data = data.Where(Function(d) d.Flags = 0).ToList()
			ddlProfileType.DataSource = data
			ddlProfileType.DataTextField = "Value"
			ddlProfileType.DataValueField = "ID"
			ddlProfileType.DataBind()

			If Not Request("CompanyMakePaymentsProfiles_id") Is Nothing Then
				Dim beneficiaryID As Integer = Request("CompanyMakePaymentsProfiles_id").ToInt32(0)
				Dim beneficiary As Bll.Merchants.Beneficiary = Bll.Merchants.Beneficiary.GetBeneficiary(beneficiaryID)
				If beneficiary.IsSystem Then
					Throw New ApplicationException("Invalid profile type")
				End If

				Netpay.Web.Validation.HtmlDecodeStringProperties(beneficiary)

				ddlProfileType.SelectedValue = beneficiary.ProfileType
				basicInfo_costumerNumber.Text = beneficiary.BasicInfoCostumerNumber
				basicInfo_costumerName.Text = beneficiary.BasicInfoCostumerName
				basicInfo_contactPersonName.Text = beneficiary.BasicInfoContactPersonName
				basicInfo_phoneNumber.Text = beneficiary.BasicInfoPhoneNumber
				basicInfo_faxNumber.Text = beneficiary.BasicInfoFaxNumber
				basicInfo_email.Text = beneficiary.BasicInfoEmail
				basicInfo_address.Text = beneficiary.BasicInfoAddress
				basicInfo_comment.Text = beneficiary.BasicInfoComment
				bankAbroadAccountName.Text = beneficiary.BankAbroadAccountName
				bankAbroadAccountNumber.Text = beneficiary.BankAbroadAccountNumber
				bankAbroadBankName.Text = beneficiary.BankAbroadBankName
				bankAbroadBankAddress.Text = beneficiary.BankAbroadBankAddress
				bankAbroadBankAddressSecond.Text = beneficiary.BankAbroadBankAddressSecond
				bankAbroadBankAddressCity.Text = beneficiary.BankAbroadBankAddressCity
				bankAbroadBankAddressState.Text = beneficiary.BankAbroadBankAddressState
				bankAbroadBankAddressZip.Text = beneficiary.BankAbroadBankAddressZip
				If beneficiary.BankAbroadBankAddressCountry > 0 Then ddlCountry.SelectedValue = beneficiary.BankAbroadBankAddressCountry
				bankAbroadSwiftNumber.Text = beneficiary.BankAbroadSwiftNumber
				bankAbroadSepaBic.Text = beneficiary.BankAbroadSepaBic
				bankAbroadIBAN.Text = beneficiary.BankAbroadIBAN
				bankAbroadABA.Text = beneficiary.BankAbroadABA
				bankAbroadSortCode.Text = beneficiary.BankAbroadSortCode
				bankAbroadAccountName2.Text = beneficiary.BankAbroadAccountName2
				bankAbroadAccountNumber2.Text = beneficiary.BankAbroadAccountNumber2
				bankAbroadBankName2.Text = beneficiary.BankAbroadBankName2
				bankAbroadBankAddress2.Text = beneficiary.BankAbroadBankAddress2
				bankAbroadBankAddressSecond2.Text = beneficiary.BankAbroadBankAddressSecond2
				bankAbroadBankAddressCity2.Text = beneficiary.BankAbroadBankAddressCity2
				bankAbroadBankAddressState2.Text = beneficiary.BankAbroadBankAddressState2
				bankAbroadBankAddressZip2.Text = beneficiary.BankAbroadBankAddressZip2
				If beneficiary.BankAbroadBankAddressCountry2 > 0 Then ddlCountry2.SelectedValue = beneficiary.BankAbroadBankAddressCountry2
				bankAbroadSwiftNumber2.Text = beneficiary.BankAbroadSwiftNumber2
				bankAbroadSepaBic2.Text = beneficiary.BankAbroadSepaBic2
				bankAbroadIBAN2.Text = beneficiary.BankAbroadIBAN2
				bankAbroadABA2.Text = beneficiary.BankAbroadABA2
				bankAbroadSortCode2.Text = beneficiary.BankAbroadSortCode2
				bankIsraelInfo_PayeeName.Text = beneficiary.BankIsraelInfoPayeeName
				bankIsraelInfo_CompanyLegalNumber.Text = beneficiary.BankIsraelInfoCompanyLegalNumber
				bankIsraelInfo_personalIdNumber.Text = beneficiary.BankIsraelInfoPersonalIDNumber
				bankIsraelInfo_bankBranch.Text = beneficiary.BankIsraelInfoBankBranch
				bankIsraelInfo_AccountNumber.Text = beneficiary.BankIsraelInfoAccountNumber
				bankIsraelInfo_PaymentMethod.Text = beneficiary.BankIsraelInfoPaymentMethod
				bankIsraelInfo_BankCode.Value = beneficiary.BankIsraelInfoBankCode
			End If
		End If
	End Sub

	Protected Function CheckValidation() As Boolean
		lblError.Text = ""
		If Validation.IsEmptyField(basicInfo_costumerName, lblError, GetLocalResourceObject("PersonalName")) Then Return False
		If Validation.IsEmptyField(ddlProfileType, lblError, "ProfileType") Then Return False

		If Not (Validation.IsEmptyField(bankAbroadAccountName)) Then
			If Validation.IsEmptyField(bankAbroadAccountNumber, lblError, GetLocalResourceObject("AccountNumber")) Then Return False
			If Validation.IsEmptyField(bankAbroadBankName, lblError, GetLocalResourceObject("BankName")) Then Return False
			If Validation.IsEmptyField(bankAbroadBankAddressCity, lblError, GetGlobalResourceObject("Multilang", "City")) Then Return False
			If Validation.IsEmptyField(bankAbroadBankAddressState, lblError, GetLocalResourceObject("Region")) Then Return False
			If Validation.IsEmptyField(ddlCountry, lblError, GetGlobalResourceObject("Multilang", "Country")) Then Return False
			If Validation.IsEmptyField(bankAbroadSwiftNumber, lblError, "Bank Swift Code") Then Return False
			If Validation.IsInvalidIban(bankAbroadIBAN, ddlCountry.SelectedValue, lblError, "IBAN") Then Return False
			If Validation.IsInvalidAba(bankAbroadABA, ddlCountry.SelectedValue, lblError, "Bank bankAbroadABA") Then Return False
		End If

		If Not (Validation.IsEmptyField(bankAbroadAccountName2)) Then
			If Validation.IsEmptyField(bankAbroadAccountNumber2, lblError, GetLocalResourceObject("AccountNumber")) Then Return False
			If Validation.IsEmptyField(bankAbroadBankName2, lblError, GetLocalResourceObject("BankName")) Then Return False
			If Validation.IsEmptyField(bankAbroadBankAddressCity2, lblError, "Correspondent City Name") Then Return False
			If Validation.IsEmptyField(bankAbroadBankAddressState2, lblError, GetLocalResourceObject("Region")) Then Return False
			If Validation.IsEmptyField(ddlCountry2, lblError, GetGlobalResourceObject("Multilang", "Country")) Then Return False
			If Validation.IsEmptyField(bankAbroadSwiftNumber2, lblError, "Correspondent Swift Code") Then Return False
			If Validation.IsInvalidIban(bankAbroadIBAN2, ddlCountry2.SelectedValue, lblError, "Correspondent IBAN") Then Return False
			If Validation.IsInvalidAba(bankAbroadABA2, ddlCountry2.SelectedValue, lblError, "Correspondent bankAbroadABA") Then Return False
		End If

		Return True
	End Function

	Protected Sub BtnDelete_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim beneficiaryID As Integer = Request("CompanyMakePaymentsProfiles_id").ToInt32(0)
		Bll.Merchants.Beneficiary.GetBeneficiary(beneficiaryID).Delete()
		Response.Redirect("BeneficiaryList.aspx", True)
	End Sub

	Protected Function GetBeneficiaryData() As Bll.Merchants.Beneficiary
		Dim beneficiaryID As Integer = Request("CompanyMakePaymentsProfiles_id").ToInt32(0)
		Dim beneficiaryData = Bll.Merchants.Beneficiary.GetBeneficiary(beneficiaryID)
		If beneficiaryData Is Nothing Then beneficiaryData = New Bll.Merchants.Beneficiary(Merchant.ID)

		beneficiaryData.ProfileType = ddlProfileType.SelectedValue
		beneficiaryData.BankAbroadBankAddressCountry = IIf(ddlCountry.SelectedCountryID Is Nothing, 0, ddlCountry.SelectedCountryID)
		beneficiaryData.BankAbroadBankAddressCountry2 = IIf(ddlCountry2.SelectedCountryID Is Nothing, 0, ddlCountry2.SelectedCountryID)
		beneficiaryData.BasicInfoCostumerNumber = basicInfo_costumerNumber.Text
		beneficiaryData.BasicInfoCostumerName = basicInfo_costumerName.Text
		beneficiaryData.BasicInfoContactPersonName = basicInfo_contactPersonName.Text
		beneficiaryData.BasicInfoPhoneNumber = basicInfo_phoneNumber.Text
		beneficiaryData.BasicInfoFaxNumber = basicInfo_faxNumber.Text
		beneficiaryData.BasicInfoEmail = basicInfo_email.Text
		beneficiaryData.BasicInfoAddress = basicInfo_address.Text
		beneficiaryData.BasicInfoComment = basicInfo_comment.Text
		beneficiaryData.BankAbroadAccountName = bankAbroadAccountName.Text
		beneficiaryData.BankAbroadAccountNumber = bankAbroadAccountNumber.Text
		beneficiaryData.BankAbroadBankName = bankAbroadBankName.Text
		beneficiaryData.BankAbroadBankAddress = bankAbroadBankAddress.Text
		beneficiaryData.BankAbroadBankAddressSecond = bankAbroadBankAddressSecond.Text
		beneficiaryData.BankAbroadBankAddressCity = bankAbroadBankAddressCity.Text
		beneficiaryData.BankAbroadBankAddressState = bankAbroadBankAddressState.Text
		beneficiaryData.BankAbroadBankAddressZip = bankAbroadBankAddressZip.Text
		beneficiaryData.BankAbroadSwiftNumber = bankAbroadSwiftNumber.Text
		beneficiaryData.BankAbroadSepaBic = bankAbroadSepaBic.Text
		beneficiaryData.BankAbroadIBAN = bankAbroadIBAN.Text
		beneficiaryData.BankAbroadABA = bankAbroadABA.Text
		beneficiaryData.BankAbroadSortCode = bankAbroadSortCode.Text
		beneficiaryData.BankAbroadAccountName2 = bankAbroadAccountName2.Text
		beneficiaryData.BankAbroadAccountNumber2 = bankAbroadAccountNumber2.Text
		beneficiaryData.BankAbroadBankName2 = bankAbroadBankName2.Text
		beneficiaryData.BankAbroadBankAddress2 = bankAbroadBankAddress2.Text
		beneficiaryData.BankAbroadBankAddressSecond2 = bankAbroadBankAddressSecond2.Text
		beneficiaryData.BankAbroadBankAddressCity2 = bankAbroadBankAddressCity2.Text
		beneficiaryData.BankAbroadBankAddressState2 = bankAbroadBankAddressState2.Text
		beneficiaryData.BankAbroadBankAddressZip2 = bankAbroadBankAddressZip2.Text
		beneficiaryData.BankAbroadSwiftNumber2 = bankAbroadSwiftNumber2.Text
		beneficiaryData.BankAbroadSepaBic2 = bankAbroadSepaBic2.Text
		beneficiaryData.BankAbroadIBAN2 = bankAbroadIBAN2.Text
		beneficiaryData.BankAbroadABA2 = bankAbroadABA2.Text
		beneficiaryData.BankAbroadSortCode2 = bankAbroadSortCode2.Text
		beneficiaryData.BankIsraelInfoPayeeName = bankIsraelInfo_PayeeName.Text
		beneficiaryData.BankIsraelInfoCompanyLegalNumber = bankIsraelInfo_CompanyLegalNumber.Text
		beneficiaryData.BankIsraelInfoPersonalIDNumber = bankIsraelInfo_personalIdNumber.Text
		beneficiaryData.BankIsraelInfoBankBranch = bankIsraelInfo_bankBranch.Text
		beneficiaryData.BankIsraelInfoAccountNumber = bankIsraelInfo_AccountNumber.Text
		beneficiaryData.BankIsraelInfoPaymentMethod = bankIsraelInfo_PaymentMethod.Text
		beneficiaryData.BankIsraelInfoBankCode = bankIsraelInfo_BankCode.Value

		Return beneficiaryData
	End Function

	Protected Sub BtnUpdate_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
		If Not CheckValidation() Then Exit Sub
		GetBeneficiaryData().Save()
		Response.Redirect("BeneficiaryList.aspx", True)
	End Sub

	Protected Sub BtnInsert_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
		If Not CheckValidation() Then Exit Sub
		GetBeneficiaryData().Save()
		Response.Redirect("BeneficiaryList.aspx", True)
	End Sub
End Class
