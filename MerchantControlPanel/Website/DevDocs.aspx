﻿<%@ Page Title="DevDocs - API Documents" Language="vb" AutoEventWireup="false" MasterPageFile="~/Website/Login.Master" CodeBehind="DevDocs.aspx.vb" Inherits="Netpay.MerchantControlPanel.DevDocs" %>

<%@ Register Src="~/Website/Controls/DeveloperDocs.ascx" TagName="DeveloperDocs" TagPrefix="uc" %>
<%@ Register Src="~/Website/Controls/AdminDocs.ascx" TagName="AdminDocs" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BoxInfoPlaceholder" runat="server">
    <div class="api-preview">
        <div class="pull-left">
            <h4>
                <asp:Localize runat="server" Text="<%$ Resources:DevDocs.aspx, PreviewTitle %>" /></h4>
        </div>
        <div class="pull-right">
            <a href="Login.aspx" class="login-button"><i class="fa fa-arrow-circle-o-left"></i>
                <asp:Localize runat="server" Text="<%$ Resources:DevDocs.aspx, LoginBtn %>" /></a>
        </div>
        <div class="clearfix"></div>
        <p>
            <asp:Localize runat="server" Text="<%$ Resources:DevDocs.aspx, PreviewDsc %>" />
        </p>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <script type="text/javascript">
        function expendClick(sender, itemId)
        {
            if (sender.isOpened) {
                $(itemId).animate({ 'height': '270' });
                $(sender).html('Show more <i class="fa fa-angle-double-down"></i>');
                sender.isOpened = null;
            } else {
                $(itemId).animate({ 'height': $(itemId)[0].scrollHeight });
                $(sender).html('Show less <i class="fa fa-angle-double-up"></i>');
                sender.isOpened = true;
            }
        }
    </script>
    <!-- API DOCUMENTS -->
    <div class="flexible">
        <div>
            <div id="apiWrapper" class="wrap-api-table margin-right-10">
                <div class="header-api-table">
                    <div class="pull-left">
                        <h4>
                            <asp:Localize runat="server" Text="<%$ Resources:DevDocs.aspx, ServicesApi %>" /></h4>
                    </div>
                    <div class="pull-right"><i class="fa fa-list-alt color-icon-header"></i></div>
                    <div class="clearfix"></div>
                </div>
                <div>
                    <div class="info-alert">
                        <i class="fa fa-info-circle"></i>
                        <asp:Localize runat="server" Text="<%$ Resources:DevDocs.aspx, DscServicesApi %>" />
                    </div>
                    <div id="dvApi" style="overflow: hidden; height:270px;">
                        <uc:DeveloperDocs RenderMode="Full" ZipFileName="ServicesAPI.zip" ID="ddApi" DocFilters="Service.V2.XX.Guide.,Service.V2.12.Merchant.,Service.V2.12.Account.,Service.V1.,Service.ASP." runat="server" />
                    </div>
                </div>

                <div class="shadow-documents">
                    <img src="images/Images.Login/shadow-middle.png" alt="" />
                </div>
            </div>
            <div class="wrap-show-button" runat="server" Visible="<%# ddApi.Rows > 5 %>"><span class="show-button" onclick="expendClick(this, '#dvApi');">Show more <i class="fa fa-angle-double-down"></i></span></div>
        </div>
        <div>
            <div id="infoWrapper" class="wrap-api-table">
                <div class="header-api-table">
                    <div class="pull-left">
                        <h4><asp:Localize runat="server" Text="<%$ Resources:DevDocs.aspx, ServicesInfo %>" /></h4>
                    </div>
                    <div class="pull-right"><i class="fa fa-list-alt color-icon-header"></i></div>
                    <div class="clearfix"></div>
                </div>
                <div>
                    <div class="info-alert">
                        <i class="fa fa-info-circle"></i>
                        <asp:Localize runat="server" Text="<%$ Resources:DevDocs.aspx, DscServiceInfo %>" />
                    </div>
                    <div id="dvInfo" style="overflow: hidden; height:270px;">
                        <uc:DeveloperDocs ID="ddInfo" ZipFileName="ServiceInfo.zip" RenderMode="Full" DocFilters="Info." runat="server" />
                    </div>
                </div>
              
                <div class="shadow-documents">
                    <img src="images/Images.Login/shadow-middle.png" alt="" />
                </div>
            </div>
            <div class="wrap-show-button" runat="server" Visible="<%# ddInfo.Rows > 5 %>"><span class="show-button" style="cursor:pointer;" onclick="expendClick(this, '#dvInfo');">Show more <i class="fa fa-angle-double-down"></i></span></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="flexible">
        <div>
            <div id="generalWrapper" class="wrap-api-table margin-right-10">
                <div class="header-api-table">
                    <div class="pull-left">
                        <h4>
                            <asp:Localize runat="server" Text="<%$ Resources:DevDocs.aspx, General %>" /></h4>
                    </div>
                    <div class="pull-right"><i class="fa fa-list-alt color-icon-header"></i></div>
                    <div class="clearfix"></div>
                </div>
                <div>
                    <div class="info-alert">
                        <i class="fa fa-info-circle"></i>
                        <asp:Localize runat="server" Text="<%$ Resources:DevDocs.aspx, DscGeneral %>" />
                    </div>
                    <div id="dvGeneral" style="overflow: hidden; height:270px;">
                        <uc:AdminDocs ID="ddGeneral" RenderMode="Full" runat="server" />
                    </div>
                </div>
               
                <div class="shadow-documents">
                    <img src="images/Images.Login/shadow-middle.png" alt="" />
                </div>
            </div>
            <div class="wrap-show-button" runat="server" Visible="<%# ddGeneral.Rows > 5 %>"><span class="show-button" style="cursor:pointer;" onclick="expendClick(this, '#dvGeneral');">Show more <i class="fa fa-angle-double-down"></i></span></div>
        </div>
        <div>
        <div id="obsoleteWrapper" class="wrap-api-table">
            <div class="header-api-table">
                <div class="pull-left">
                    <h4>
                        <asp:Localize runat="server" Text="<%$ Resources:DevDocs.aspx, Obsolete %>" /></h4>
                </div>
                <div class="pull-right"><i class="fa fa-list-alt color-icon-header"></i></div>
                <div class="clearfix"></div>
            </div>
            <div>
                <div class="info-warning">
                    <i class="fa fa-info-circle"></i>
                    <asp:Localize runat="server" Text="<%$ Resources:DevDocs.aspx, DscObsolete %>" />
                </div>
                <div id="dvObsolete" style="overflow: hidden; height:270px;">
                    <uc:DeveloperDocs ID="ddObsolete" ZipFileName="Obsolete.zip" RenderMode="Full" DocFilters="Obsolete" runat="server" />
                </div>
            </div>
            <div class="shadow-documents">
                <img src="images/Images.Login/shadow-middle.png" alt="" />
            </div>
        </div>
            <div class="wrap-show-button" runat="server" Visible="<%# ddObsolete.Rows > 5 %>"><span class="show-button" style="cursor:pointer;" onclick="expendClick(this, '#dvObsolete');">Show more <i class="fa fa-angle-double-down"></i></span></div>
        </div>
        <div class="clearfix"></div>
    </div>
</asp:Content>
