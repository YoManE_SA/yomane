﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="true"
    Inherits="Netpay.MerchantControlPanel.DeviceManagment" CodeBehind="DeviceManagment.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:MultiLang,ManageLimitdedUserManagment %>"
        runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Literal runat="server" Text="<%$Resources:MultiLang,ManageLimitdedUserManagment %>" />
        </div>
        <div class="content-background">
            <div class="section">
                <div class="margin-bottom-10 frame-border-option">
                    <asp:Repeater runat="server" ID="rptDevices">
                        <ItemTemplate>
                            <div class="margin-bottom-10"><i class="fa fa-user"></i> <asp:Literal runat="server" Text="<%$Resources: UserProfile %>" /></div>
                                    <div class="frame-border-option-inline margin-bottom-10 padding-10">
                                        <div class="percent32">
                                            <div><asp:Literal runat="server" Text="<%$Resources:MultiLang,NameDeviceUser %>" /></div>
                                            <div class="margin-bottom-10"><asp:TextBox runat="server" MaxLength="10" ID="txtDeviceName" CssClass="percent85" Text='<%#Eval("FriendlyName")%>' /></div>
                                         </div>
                                     </div>
                                     <div class="wrap-button margin-bottom-10">
                                        <asp:LinkButton runat="server" ID="btnBlock" Visible='<%# Eval("IsActive") %>' CssClass="btn btn-inverse btn-cons-short" OnClick="btnBlock_Click"><i class="fa fa-stop"></i> <asp:Literal runat="server" Text="<%$Resources:MultiLang,block %>" /></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="btnUnblock" Visible='<%# Not Eval("IsActive") %>' CssClass="btn btn-default btn-cons-short" OnClick="btnUnblock_Click"><i class="fa fa-play"></i> <asp:Literal runat="server" Text="<%$Resources:MultiLang,unblock %>" /></asp:LinkButton>
                                        <asp:LinkButton runat="server" ID="btnUpdate" CssClass="btn btn-success btn-cons-short" OnClick="Update_Click"><i class="fa fa-floppy-o"></i> <asp:Literal runat="server" Text="<%$Resources:MultiLang,update %>" /></asp:LinkButton>
                                        <asp:HiddenField runat="server" ID="hdDeviceId" Value='<%#Eval("ID")%>' />
                                     </div>
                            <ul class="group-list">

                                <li>
                                    <asp:Literal runat="server" Text="<%$Resources: IsActivated %>" />:
                                       <b><%#Eval("IsActivated")%></b>
                                </li>
                                <li>
                                    <asp:Literal runat="server" Text="<%$Resources: IsActive %>" />:
                                        <b><%#Eval("IsActive")%></b>
                                </li>
                                <li>
                                    <asp:Literal runat="server" Text="<%$Resources: AppVersion %>" />:
                                        <b><%#Eval("AppVersion")%></b>
                                </li>
                                <li>
                                    <asp:Literal runat="server" Text="<%$Resources: DevicePhoneNumber %>" />:
                                        <b><%#Eval("DevicePhoneNumber")%></b>
                                </li>
                                <li>
                                    <asp:Literal runat="server" Text="<%$Resources: DeviceUserAgent %>" />:
                                        <b><%#Eval("DeviceUserAgent")%></b>
                                </li>
                                <li>
                                    <asp:Literal runat="server" Text="<%$Resources: SubUserName%>" />:
                                        <b><%#Eval("SubUserName")%></b>
                                </li>
                                <li>
                                    <asp:Literal runat="server" Text="<%$Resources: LastLogin %>" />:
                                        <b><%#Eval("LastLogin")%></b>
                                </li>
                            </ul>


                            <div class="frame-border-option-inline margin-bottom-10  margin-top-10 padding-10">

                                <div class="text-align-center" style="position: relative;">
                                    <div style="background-image: url('Images/iphone.png'); width: 265px; height: 500px; background-position: center; margin: 0 auto;">
                                        <div style="padding-top: 150px;">
                                            <h2 style="color: #eee;"><%#Eval("FriendlyName")%></h2>
                                            <p style="color: #fff;">
                                                <%#Eval("DevicePhoneNumber")%><br />
                                                <asp:Literal runat="server" Text="<%$Resources: AppVersion %>" />
                                                <%#Eval("AppVersion")%>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <div class="wrap-pager">
                        <netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
