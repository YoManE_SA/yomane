﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="false"
    Inherits="Netpay.MerchantControlPanel.Website_Facebook" CodeBehind="Facebook.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Literal ID="literalTitle" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">Guide To use on Facbook Apps</div>
        <div class="content-background">
            <div class="section">
                <div id="facbook-tabs">
                  <h3>STEP 1:</h3>
                 <p> Go to this link - <a href="http://www.facebook.com/add.php?api_key=674151289282843&pages=1" target="_blank">Add Netpay Shops app's</a> </p>
                     <h3>STEP 2:</h3>
                 <p>Choose your fan page that you want to add the application</p>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
