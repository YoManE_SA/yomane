﻿<%@ Page Title="PageTitle" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master"
    AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_UnsettledTransactions"
    CodeBehind="UnsettledTransactions.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$ Resources:PageTitle %>" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">
            <div class="legend-menu">
                <div class="align-right">
                    <netpay:PopupButton ID="PopupButton1" IconSrc="Images/iconLegend.gif" Width="120" Height="145" Text="<%$Resources:MultiLang,Legend  %>" runat="server">
                        <netpay:Legend Capture="true" Admin="true" System="true" Test="true" runat="server" ID="ttLegend" />
                    </netpay:PopupButton>
                </div>
            </div>
            <div class="section">
                <div>
                    <asp:Literal runat="server" Text="<%$Resources:MultiLang,InsertDates  %>" />
                    <asp:PlaceHolder ID="phDatePicker" Visible="true" runat="server">
                        <netpay:DateRangePicker Visible="true" ID="wcDateRangePicker" HtmlLayout="Flow" Layout="Horizontal" runat="server" class="Field_100" />
                    </asp:PlaceHolder>
                    &nbsp;&nbsp;
                    <asp:Literal  runat="server" Text="<%$Resources:MultiLang,Currency  %>" />
                    <netpay:CurrencyDropDown ID="ddCurrency" EnableBlankSelection="false" CssClass="Field_90"
                        runat="server">
                    </netpay:CurrencyDropDown>
                   
                    <asp:LinkButton ID="btnAdvancedSearch" OnClick="btnAdvancedSearch_Click" Text="<%$Resources:MultiLang,AdvancedSearch  %>"
                        Visible="false" runat="server" CssClass="grey-color-link"></asp:LinkButton>
                </div>
                   <div class="wrap-button-bar">
                        <asp:LinkButton ID="btnSearch" OnClick="btnSearch_Click" CssClass="btn btn-cons-short btn-inverse" runat="server"><i class="fa fa-search"></i> <asp:Literal runat="server" Text="<%$Resources:MultiLang,Search  %>" /></asp:LinkButton>
                </div>
            </div>
            <asp:PlaceHolder ID="phResults" Visible="false" runat="server">
                <div class="section">
                    <table class="exspand-table">
                        <tr>
                            <th>
                                &nbsp;
                            </th>
                            <th>
                                &nbsp;
                            </th>
                            <th>
                                <asp:Literal  Text="<%$Resources:MultiLang,Transaction %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal  Text="<%$Resources:MultiLang,Date %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal  Text="<%$Resources:MultiLang,PaymentMethod %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal  Text="<%$Resources:MultiLang,CreditType %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal  Text="<%$Resources:MultiLang,Amount %>" runat="server" />
                            </th>
                        </tr>
                        <asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server">
                            <ItemTemplate>
                                <netpay:MerchantTransactionRowView ID="ucTransactionView" Transaction="<%# CType(Container.DataItem, Transactions.Transaction)%>" ShowCreditType="true" runat="server" />
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <br />
                    <div class="align-left">
                        <div class="xls-wrap">
                            <netpay:ExcelButton ID="btnExportExcel" Text="<%$Resources:MultiLang,ExportToXls %>" Enabled="false" IconImageOn="xls.png" IconImageOff="xls_off.png" runat="server" OnClick="ExportExcel_Click" />
                        </div>
                    </div>
                    <div class="align-right">
                        <netpay:ShowTotalsButton ID="ucShowTotalsButton" CssClass="btn btn-default" runat="server" />
                    </div>
                    <div class="spacer">
                    </div>
                    <netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
                </div>
            </asp:PlaceHolder>
        </div>
    </div>
</asp:Content>
