﻿Imports System.Collections.Generic
Imports Netpay.Bll.Risk
Imports Netpay.Bll
Imports Netpay.Infrastructure
Imports Netpay.Web.Controls

Partial Class Website_ItemBlackList
	Inherits MasteredPage

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		lblError.Visible = False
		lblError.Text = ""
		If Not IsPostBack Then btnSearch_Click(sender, e)
	End Sub

	Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
		pager.Info.PageCurrent = 0
		OnPageChanged(sender, e)
	End Sub

 	Protected Sub OnPageChanged(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim filters = New Risk.RiskItem.SearchFilters()
		filters.Source = ddItemSourceFilter.SelectedValue.ToNullableEnumByValue(Of BlockedItemSource)()
		filters.ValueType = ddItemTypeFilter.SelectedValue.ToNullableEnumByValue(Of BlockedItemType)()
		filters.Value = txtItemValueFilter.Text.NullIfEmpty()
		filters.Text = txtItemCommentFilter.Text.NullIfEmpty()

		repeaterResults.DataSource = Risk.RiskItem.Search(filters, pager.Info)
		repeaterResults.DataBind()
        phResults.Visible = (repeaterResults.Items.Count > 0)

        phItemAddedBlack.Visible = False
    End Sub

	Sub ToggleAddItem(ByVal o As Object, ByVal e As System.EventArgs)
		phAddItemLink.Visible = False
        phAddItemForm.Visible = True

        OnPageChanged(o, e)
    End Sub

	Protected Sub AddBlockedItem(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim itemType As Risk.RiskItem.RiskValueType = ddItemType.SelectedValue
        Dim result As AddBlockedItemResult = AddBlockedItemResult.Success
		Dim itemList = New List(Of Bll.Risk.RiskItem.RiskItemValue)
		itemList.Add( New Bll.Risk.RiskItem.RiskItemValue() With { .ValueType = itemType, .Value = txtItemValue.Value })

        Risk.RiskItem.Create(RiskItem.RiskSource.Merchant, Risk.RiskItem.RiskListType.Black, itemList, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, txtComment.Value, Nothing)
        If result <> AddBlockedItemResult.Success Then
            lblError.Visible = True
            lblError.Text = "<div class=""Error2"">" & GetGlobalResourceObject("MultiLang", "ErrorAdding") & ": " & GetLocalResourceObject(result.ToString()) & "</div>"
            Return
        End If

        pager.Info.PageCurrent = 0
        OnPageChanged(sender, e)
        phItemAddedBlack.Visible = True
    End Sub

	Protected Sub DeleteSelectedItems(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim blocked As String = Request.Form("blockedItemID")
		If blocked Is Nothing Then Return
		Dim blockedList As String() = blocked.Split(",")
		Dim blockedListConverted As List(Of Integer) = New List(Of Integer)
		For Each currentID In blockedList
			blockedListConverted.Add(Integer.Parse(currentID))
		Next
        Bll.Risk.RiskItem.Delete(RiskItem.RiskValueType.Email, Risk.RiskItem.RiskListType.Black, blockedListConverted)
        pager.Info.PageCurrent = 0
        OnPageChanged(sender, e)
    End Sub

	Protected Function GetLegend(item As Risk.RiskItem) As String
		If item.Source = BlockedItemSource.Merchant Then
            Return "<td><div style='text-align:center;background-color:#6699CC; width: 15px; height: 15px;'>&nbsp;</div></td><td>" + GetGlobalResourceObject("MultiLang", "Merchant") + "</td>"
		Else
            Return "<td><div style='text-align:center;background-color:#484848; width: 15px; height: 15px;'>&nbsp;</div></td><td>" + GetGlobalResourceObject("MultiLang", "System") + "</td>"
		End If
	End Function

	Protected Function GetCheckbox(item As Risk.RiskItem) As String
		If item.Source = BlockedItemSource.Merchant Then
			Return "<input type='checkbox' name='blockedItemID' value='" + item.ID.ToString() + "' style='background-color:#ffffff!important;' />"
		Else
			Return "<input type='checkbox' disabled='true' name='blockedItemID' value='" + item.ID.ToString() + "' style='background-color:#ffffff!important;' />"
		End If
	End Function

	Protected Function GetComment(ByVal item As Risk.RiskItem) As String
		If item.Source = BlockedItemSource.Merchant Then
			Return item.Comment
		Else
			Return "---"
		End If
	End Function
End Class
