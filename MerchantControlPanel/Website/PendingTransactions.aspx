﻿<%@ Page Title="PageTitle" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master"
    AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_PendingTransactions"
    CodeBehind="PendingTransactions.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <netpay:PageHelpBox Text="<%$Resources:HelpBox %>" runat="server" />
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">
            <div class="section">
                <div class="align-left percent80">
                    <div>
                       <div>     <asp:Literal runat="server" Text="<%$Resources:MultiLang,InsertDate %>" /></div>
                       <div class="margin-bottom-10">
                       <netpay:DateRangePicker Visible="true" ID="wcDateRangePicker" HtmlLayout="Flow" Layout="Horizontal" runat="server" class="percent100" />
                       </div>
                    </div>

                    <div class="align-left percent32 margin-right-left-3">

                        <div class="margin-top-10">
                            <asp:Literal runat="server" Text="<%$Resources:MultiLang,PaymentMethod %>" /></div>
                        <div>
                            <netpay:PaymentMethodDropDown ID="ddPaymentMethod" runat="server" BlankSelectionText="<%$Resources:MultiLang,PaymentMethod %>" CssClass="percent100" /></div>
                    </div>
                    <div class="align-left percent32  margin-right-left-3">

                            <div class="margin-top-10">
                            <asp:Literal runat="server" Text="<%$Resources:MultiLang,PayerFirstName %>" /></div>
                        <div>
                            <asp:TextBox ID="txtPayerFirstName" CssClass="percent100" runat="server" placeholder="<%$Resources:MultiLang,PayerFirstName %>" /></div>

                    </div>
                    <div class="align-left percent32  margin-right-left-3">

                          <div class="margin-top-10">
                            <asp:Literal runat="server" Text="<%$Resources:MultiLang,PayerLastName %>" /></div>
                        <div>
                            <asp:TextBox ID="txtPayerLastName" CssClass="percent100" runat="server" placeholder="<%$Resources:MultiLang,PayerLastName %>" /></div>

                    </div>
                </div>
                <div class="align-right">
                    <div class="filter">
                        <div class="quick-search">
                            <asp:Literal ID="Literal4" runat="server" Text="<%$Resources: TransApproved.aspx,QuickSearch%>"></asp:Literal>
                            <div class="field-quick">
                                <asp:TextBox ID="txtTransactionID" runat="server" placeholder="<%$Resources:MultiLang,TransactionID  %>" CssClass="Field_110"></asp:TextBox>
                            </div>
                            <div class="button-quick">
                                <asp:LinkButton ID="btnShow" runat="server" CssClass="btn" Text="<%$Resources:MultiLang,Show %>" OnClick="btnShow_Click" />
                            </div>
                        </div>
                    </div>
                    <netpay:SearchFiltersView ID="wcFiltersView" runat="server" Visible="false" />
                </div>
                <div class="spacer">
                </div>
                <div class="wrap-button-bar">
                    <asp:LinkButton ID="BtnSearch" runat="server" CssClass="btn btn-cons-short btn-inverse" Text="<%$Resources:MultiLang,Search %>" OnClick="btnSearch_Click" />
                </div>
            </div>
            <asp:PlaceHolder ID="phResults" Visible="false" runat="server">
                <div class="section">
                    <table class="exspand-table">
                        <tr>
                            <th colspan="2">&nbsp;
                            </th>
                            <th>
                                <asp:Literal ID="Literal5" Text="<%$Resources:MultiLang,Transaction %>" runat="server"></asp:Literal>
                            </th>
                            <th>
                                <asp:Literal ID="Literal6" Text="<%$Resources:MultiLang,Date %>" runat="server"></asp:Literal>
                            </th>
                            <th>
                                <asp:Literal ID="Literal7" Text="<%$Resources:MultiLang,PaymentMethod %>" runat="server"></asp:Literal>
                            </th>
                            <th>
                                <asp:Literal ID="Literal8" Text="<%$Resources:MultiLang,Amount %>" runat="server"></asp:Literal>
                            </th>
                        </tr>
                        <asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server">
                            <ItemTemplate>
                                <netpay:MerchantTransactionRowView ID="ucTransactionView" Transaction="<%# CType(Container.DataItem, Transactions.Transaction) %>"
                                    ShowReplyCode="false" ShowStatus="false" ShowApprovalNumber="false" ShowTransactionFee="false"
                                    ShowPassedTransactionID="false" runat="server" />
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
                    <div class="xls-wrap">
                        <netpay:ExcelButton ID="btnExportExcel" Text="<%$Resources:MultiLang,ExportToXls %>"
                            Enabled="false" IconImageOn="xls.png" IconImageOff="xls_off.png" runat="server"
                            OnClick="ExportExcel_Click" />
                    </div>
                </div>
            </asp:PlaceHolder>
        </div>
    </div>
</asp:Content>
