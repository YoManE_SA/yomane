﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" CodeBehind="ContentPage.aspx.vb" Inherits="Netpay.MerchantControlPanel.ContentPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
	<asp:PlaceHolder runat="server" ID="phTitle" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
	<asp:Literal ID="ltContent" runat="server" Mode="PassThrough" />
</asp:Content>
