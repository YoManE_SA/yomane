﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FAQ.aspx.vb" Inherits="Netpay.MerchantControlPanel.FAQuestions" MasterPageFile="~/Templates/Tmp_netpayintl/page.master"  %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <script type="text/javascript">
        function setAllFaq(faqCount, display) {
            for (var faqId = 0; faqId < faqCount; faqId++) {
                var answer = $("#ANS_" + faqId);
                var image = $("#IMG_" + faqId);
                if (display == "") {
                    answer.css("display", "block")
                    image.attr("src", "<%= MapTemplateVirPath("images/menu/minus-ltr.png")%>");
                }
                else {
                    answer.css('display', "none");
                    image.attr("src", "<%= MapTemplateVirPath("images/menu/plus-ltr.png")%>");
                }
            }
        }
        function toggleFaq(faqId) {
            var answer = $("#ANS_" + faqId);
            var image = $("#IMG_" + faqId);
            if (answer.css("display") == "none") {
                answer.css("display", "block")
                image.attr("src", "<%= MapTemplateVirPath("images/menu/minus-ltr.png")%>");
            }
            else {
                answer.css('display', "none");
                image.attr("src", "<%= MapTemplateVirPath("images/menu/plus-ltr.png")%>");
            }
        }
    </script>
    <div class="top-nav">
        <div style="float: right; padding-right: 8px;">
            <h1>
                <asp:Literal ID="Literal5" runat="server" Text="<%$Resources:MultiLang,QuestionAnswers %>" /></h1>
        </div>
        <div style="float: left; padding-left: 8px;">
            <a href="#" onclick="setAllFaq(<%=rptQuestions.Items.Count %>, '')" class="links-faq">
                <asp:Literal ID="Literal2" runat="server" Text="<%$Resources:MultiLang,OpenAll %>" /></a>&nbsp;
           <img src="<%= MapTemplateVirPath("images/list-type.png")%>" />&nbsp; <a href="#" onclick="setAllFaq(<%=rptQuestions.Items.Count %>, 'none')" class="links-faq">
                <asp:Literal ID="Literal3" runat="server" Text="<%$Resources:MultiLang,CloseAll %>" /></a>
        </div>
    </div>
    <div class="section">
        <asp:Repeater runat="server" ID="rptQuestions">
            <ItemTemplate>
                <h3 class="faq-question" id="QST_<%# Container.ItemIndex %>" onclick="toggleFaq(<%# Container.ItemIndex %>)">
                    <img id="IMG_<%# Container.ItemIndex %>" alt="" src="<%= MapTemplateVirPath("images/menu/plus-" & WebUtils.CurrentDirection & ".png")%>" class="head-faq-arrow" /><%# Eval("Question") %></h3>
						<p id="ANS_<%# Container.ItemIndex %>" style="display: none;"><%# Eval("Answer")%></p>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Content>