﻿
Partial Class Website_Content
    Inherits MasteredPage
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Dim text As String = GetGlobalResourceObject("MultiLang", Request("CN").ToString()).ToString()
			literalTitle.Text = text
		End If
	End Sub
End Class
