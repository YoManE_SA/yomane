﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" CodeBehind="DevInvoice_Admin.aspx.vb" Inherits="Netpay.MerchantControlPanel.Invoice_Admin" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:menu, ExternalInvoice%>" runat="server" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphBody" runat="Server">
    <netpay:ElementBlocker ID="elementBlocker" ElementClientID="divContent" Enabled="false" Text="<%$ Resources:MultiLang,FeatureDisabled%>" runat="server" />
    <div id="divContent" class="area-content">
        <div class="top-nav">
            <asp:Literal Text="<%$Resources: External_Invoices %>" runat="server" />
        </div>

        <div class="content-background">
            <div class="alert-info">
                <asp:Literal Text="<%$Resources: Intro %>" runat="server" />
                <b>"<asp:Literal ID="literalInvoiceAccount" runat="server" />"</b>
            </div>
            <div class="section">
                <div class="margin-bottom-10 frame-border-option">
                    <div class="margin-bottom-10">
                        <i class="fa fa-plus-square-o fa-fw"></i>
                        <asp:Literal Text="<%$Resources: Creation %>" runat="server" />
                    </div>
                    <ul class="group-list">
                        <li>
                            <asp:CheckBox runat="server" ID="chkCreateInvoice" />
                            <asp:Literal Text="<%$Resources: Create_Invoice %>" runat="server" />
                        </li>
                        <li>
                            <asp:CheckBox runat="server" ID="chkCreateReceipt" />
                            <asp:Literal Text="<%$Resources: Create_Receipt %>" runat="server" />
                        </li>

                    </ul>

                     <div class="margin-bottom-10 margin-top-10">
                        <i class="fa fa-pencil-square-o fa-fw"></i>
                        <asp:Literal Text="<%$Resources: Auto_generate %>" runat="server" />
                    </div>
                    <ul class="group-list">
                        <li>
                            <asp:CheckBox runat="server" ID="chkAutoGenerateILS" Checked="true" />
                            <asp:Literal Text="<%$Resources: ILS_transactions %>" runat="server" /></li>
                        <li>
                            <asp:CheckBox runat="server" ID="chkAutoGenerateOther" />
                            <asp:Literal Text="<%$Resources: Currency_transactions %>" runat="server" /></li>
                        <li>
                            <asp:CheckBox runat="server" ID="chkAutoGenerateRefund" />
                            <asp:Literal Text="<%$Resources: On_refunds %>" runat="server" /></li>
                        <li>
                            <asp:CheckBox runat="server" ID="chkIncludeTax" />
                            <asp:Literal Text="<%$Resources: Include_tax %>" runat="server" />
                        </li>
                    </ul>
               
                    <div class="margin-bottom-10 margin-top-10">
                        <i class="fa fa-text-width fa-fw"></i>
                        <asp:Literal Text="<%$Resources: Invoice_Text %>" runat="server" />
                    </div>
                    <div>
                        <asp:TextBox runat="server" ID="txtItemText" MaxLength="80" class="percent100" />
                    </div>
                
                
                    <div class="wrap-button margin-top-10">
                        <asp:LinkButton ID="btnSave" CssClass="btn btn-success btn-cons-short" runat="server" OnClick="SaveForm"><i class="fa fa-floppy-o"></i> <asp:Literal runat="server" Text="<%$Resources: Update %>" /></asp:LinkButton>
                    </div>
                </div>
               
                   
                
            </div>
        </div>
    </div>
</asp:Content>
