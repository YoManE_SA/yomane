﻿<%@ Page Title="PageTitle" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_VirtualTerminalCC" CodeBehind="VirtualTerminalCC.aspx.vb" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:VirtualTerminalCC.aspx, PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            // section toggle event
            $(".wrap-edit-product").css("cursor", "pointer").click(function () {
                toggleSection($(this));
            });

            // open first section if new product
            openSection($($(".wrap-edit-product")[0]));

            if (<%= isBillingAddressMust.ToString().ToLower() %>)
            openSection($($(".wrap-edit-product")[1]));
        });

        function CheckCredit()
        {
            var displayProp = document.getElementById("<%= ddlCreditType.ClientID %>").value == <%= CreditType.Installments %> ? "block" : "none";
            document.getElementById("divInstallments").style.display = displayProp;

            if (document.getElementById("<%= ddlPayment.ClientID %>").disabled) 
                document.getElementById("<%= ddlPayment.ClientID %>").selectedIndex=0;

            if (document.getElementById("<%= ddlCreditType.ClientID %>").value == <%= CreditType.Installments %>)
            {
                document.getElementById("<%= chkIsUseRecurring.ClientID %>").checked=false;
            document.getElementById("<%= chkIsUseRecurring.ClientID %>").disabled=true;
            document.getElementById("<%= RecurringCount.ClientID %>").disabled=true;
            document.getElementById("<%= RecurringCycle.ClientID %>").disabled=true;
            document.getElementById("<%= RecurringMode.ClientID %>").disabled=true;
            document.getElementById("<%= lblRecurringCount.ClientID %>").style.color="Gray";
            document.getElementById("<%= lblRecurringMode.ClientID %>").style.color="Gray";
            document.getElementById("<%= lblRecurringRequiredCount.ClientID %>").style.color="Gray";
            document.getElementById("<%= lblRecurringRequiredMode.ClientID %>").style.color="Gray";
            document.getElementById("<%= RecurringCount.ClientID %>").style.color="Gray";
            document.getElementById("<%= RecurringCycle.ClientID %>").style.color="Gray";
            document.getElementById("<%= RecurringMode.ClientID %>").style.color="Gray";
        }
        else
        {
                document.getElementById("<%= chkIsUseRecurring.ClientID %>").disabled = false;
        }
        }

        function dopostddlstate()
        {
            var IndexValue = document.getElementById('<%= ddlCountry.ClientID %>').selectedIndex;
            var SelectedVal = document.getElementById('<%= ddlCountry.ClientID %>').options[IndexValue];
            var DivUsa = document.getElementById('<%= DivUsa.ClientID %>');
            var DivCanada = document.getElementById('<%= DivCanada.ClientID %>');

            if (SelectedVal.innerHTML == "United States")
            {
                document.getElementById('<%= ddlStateUsa.ClientID %>').disabled = false
                DivUsa.style.display = 'block';
                DivCanada.style.display = 'none';
            }
            else
            {
                document.getElementById('<%= ddlStateUsa.ClientID %>').disabled = true
                document.getElementById('<%= ddlStateUsa.ClientID %>').value = 0
                document.getElementById('<%= ddlStateCanada.ClientID %>').value = 0
            }

            if (SelectedVal.innerHTML == "Canada")
            {
                document.getElementById('<%= ddlStateCanada.ClientID %>').disabled = false
                DivUsa.style.display = 'none';
                DivCanada.style.display = 'block';
            }
            else
            {
                document.getElementById('<%= ddlStateCanada.ClientID %>').disabled = true
                document.getElementById('<%= ddlStateCanada.ClientID %>').value = 0
                document.getElementById('<%= ddlStateUsa.ClientID %>').value = 0
            }
        }

        function toggleSection(section) {
            if (section.next().css("display") == "none")
                openSection(section);
            else
                closeSection(section);
        }


        function openSection(section) {
            section.children(".align-right").children("i").switchClass("fa-angle-right", "fa-angle-down", 0, null);
            section.next().css("display", "block");
        }

        function closeSection(section) {
            section.children(".align-right").children("i").switchClass("fa-angle-down", "fa-angle-right", 0, null);
            section.next().css("display", "none");
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <asp:HiddenField runat="server" ID="hfTransID" Value="" />


    <div class="area-content" id="divForm">

        <div class="top-nav">
            <asp:Localize Text="<%$Resources:VirtualTerminalCC.aspx, PageTitle%>" runat="server" />
        </div>
        <netpay:ElementBlocker ID="wcElementBlocker" ElementClientID="divForm" Text="<%$ Resources:MultiLang,FeatureDisabled %>" runat="server" />
        <div class="content-background">

            <div id="divResult" runat="server" visible="false" class="section">
                <!-- Approved Transaction -->
                <asp:PlaceHolder ID="phTransApproved" runat="server" Visible="false">
                    <div class="transaction-approved">
                        <div><i class="fa fa-check-circle fa-4x"></i></div>
                        <div class="margin-top-5">
                            <asp:Label ID="lblSuccessMessage" runat="server"></asp:Label>
                        </div>
                        <div class="margin-top-10">
                            <span>
                                <asp:HyperLink NavigateUrl="~/Website/VirtualTerminalCC.aspx" runat="server" CssClass="btn btn-success" Text="<%$Resources:VirtualTerminalCC.aspx, TransactionApproved %>" /></span>
                            <div class="spacer"></div>
                        </div>
                    </div>
                </asp:PlaceHolder>
                <!-- Decline Transaction -->
                <asp:PlaceHolder ID="phTransDecline" runat="server" Visible="false">
                    <div class="transaction-decline">
                        <div><i class="fa fa-times-circle fa-4x"></i></div>
                        <div class="margin-top-5">
                            <asp:Label ID="lblErrorMessage" runat="server"></asp:Label>
                        </div>
                        <div class="margin-top-10">
                            <span>
                                <asp:LinkButton ID="btnChargeAgain" OnClick="btnChargeAgain_Click" PostBackUrl="~/Website/VirtualTerminalCC.aspx" runat="server" CssClass="btn btn" Text="<%$Resources:VirtualTerminalCC.aspx, TransactionDecline %>" /></span>
                            <div class="spacer"></div>
                        </div>
                    </div>
                </asp:PlaceHolder>
            </div>

            <!-- Virtual Terminal Form -->

            <asp:PlaceHolder runat="server" ID="phVTForm" Visible="true">
                <!-- Credit Card Storage -->
                <asp:PlaceHolder ID="phStorageCard" runat="server" Visible="true">
                    <div class="margin-bottom-10 text-align-right">
                        <asp:LinkButton ID="BtnCUS" runat="server" type="button" CssClass="btn btn-cons-short btn-inverse" OnClick="AddStorageCard"><i class="fa fa-credit-card-alt"></i> <asp:Literal runat="server" Text="<%$Resources:VirtualTerminalCC.aspx, CardUseStorageID %>" /></asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="phAddcardStorage" runat="server" Visible="false">
                    <div class="section">
                        <div class="line-height-35">
                            <asp:Literal runat="server" Text="<%$Resources:VirtualTerminalCC.aspx, CardUseStorageID %>" />:
                                <asp:TextBox ID="txStorageID" CssClass="margin-right-10" runat="server" />
                        </div>
                        <div class="wrap-button-bar">
                            <asp:LinkButton ID="btnLoadStorage" runat="server" CssClass="btn btn-cons-short btn-inverse" OnClick="LoadStorage_Click" Text="<%$Resources:VirtualTerminalCC.aspx, CardStorageLoad%>" />
                        </div>
                    </div>
                </asp:PlaceHolder>
                <!-- End Credit Card Storage -->

                <div class="section">
                    <div class="margin-bottom-10 frame-border-option">
                        <ul class="edit-list-product">
                            <li>
                                <!-- Transaction Details -->
                                <div class="wrap-edit-product">
                                    <div class="align-left">
                                        <i class="fa fa-credit-card-alt"></i>
                                        <asp:Literal runat="server" Text="<%$Resources:VirtualTerminalCC.aspx, Transaction %>" />
                                    </div>
                                    <div class="align-right">
                                        <i class="fa fa-2x fa-angle-right"></i>
                                    </div>
                                    <div class="spacer"></div>
                                </div>
                                <div class="frame-border-option-inline padding-10 margin-bottom-10">

                                    <div class="form-group-title margin-bottom-10">
                                        <i class="fa fa-credit-card"></i>
                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang,CreditCardDetails %>" />
                                    </div>
                                    <div class="align-right form-group-title">
                                        <netpay:SwipeManager runat="server" ID="smSwipe" ShowImage="true" CardholderNameID="txtCHFullName" CardNumberID="wcCCInput" ExpMonthID="ddlsExpMonth" ExpYearID="ddlsExpYear" CvvID="txtCHCvv2" />
                                        &nbsp;
                                        <asp:Literal runat="server" Text="<%$Resources:VirtualTerminalCC.aspx, SwipeCard %>" />
                                    </div>
                                    <div class="spacer">
                                    </div>
                                    <div class="margin-bottom-10 frame-border-single">
                                        <!-- Credit Card Details -->
                                        <div class="align-left percent33">
                                            <div class="margin-bottom-10">
                                                <div>
                                                    <asp:Label ID="lblCreditCardNum" Text="<%$Resources:MultiLang,CreditCardNumber %>" runat="server" />
                                                    <span class="require">
                                                        <asp:Literal ID="ltCreditCardNumber" runat="server" Text="<%$Resources:VirtualTerminalCC.aspx, RequiredField %>" /></span>
                                                </div>
                                                <div>
                                                    <netpay:CreditcardInput ID="wcCCInput" CssClass="percent85" runat="server" SingleFieldMode="true" />
                                                </div>

                                            </div>
                                            <div class="margin-bottom-10">
                                                <div>
                                                    <asp:Literal runat="server" Text="<%$Resources:MultiLang,CCValidity  %>" />
                                                    <span class="require">
                                                        <asp:Literal ID="ltExpiritionDate" runat="server" Text="<%$Resources:VirtualTerminalCC.aspx, RequiredField %>" /></span>
                                                </div>
                                                <div>
                                                    <asp:DropDownList ID="ddlsExpMonth" CssClass="percent40" runat="server" />
                                                    /
                                                    <asp:DropDownList ID="ddlsExpYear" CssClass="percent40" runat="server" />
                                                </div>

                                            </div>
                                            <div class="margin-bottom-10">
                                                <div>
                                                    <asp:Literal runat="server" Text="<%$Resources:MultiLang,CVV %>" />&nbsp;
                                                    <netpay:PageItemHelp ID="CvvTip" Width="200px" runat="server" />
                                                    <span class="require">
                                                        <asp:Literal ID="ltCvv" runat="server" Text="<%$Resources:VirtualTerminalCC.aspx, RequiredField %>" /></span>
                                                </div>
                                                <div>
                                                    <asp:TextBox ID="txtCHCvv2" CssClass="percent85" runat="server" />
                                                </div>

                                            </div>
                                        </div>
                                        <div class="align-left percent33">
                                            <div class="margin-bottom-10">
                                                <div>
                                                    <asp:Literal runat="server" Text="<%$Resources:MultiLang,CCHolderName %>" />
                                                    <span class="require">
                                                        <asp:Literal ID="ltCardHolderName" runat="server" Text="<%$Resources:VirtualTerminalCC.aspx, RequiredField %>" /></span>
                                                </div>
                                                <div>
                                                    <asp:TextBox ID="txtCHFullName" CssClass="percent85" runat="server" />
                                                </div>

                                            </div>
                                            <div class="margin-bottom-10">
                                                <div>
                                                    <asp:Literal runat="server" Text="<%$Resources:MultiLang,CCHolderPhone %>" />&nbsp;
                                                    <span class="require">
                                                        <asp:Literal ID="litRequiredCCHolderPhone" runat="server" Text="<%$Resources:VirtualTerminalCC.aspx, RequiredField %>" /></span>
                                                </div>
                                                <div>
                                                    <asp:TextBox ID="txtPhoneNumber" CssClass="percent85" runat="server" />
                                                </div>

                                            </div>
                                            <div class="margin-bottom-10">
                                                <div>
                                                    <asp:Literal runat="server" Text="<%$Resources:MultiLang,CCHolderMail %>" />
                                                    <span class="require">
                                                        <asp:Literal ID="litRequiredMail" runat="server" Text="<%$Resources:VirtualTerminalCC.aspx, RequiredField %>" /></span>
                                                </div>
                                                <div>
                                                    <asp:TextBox ID="txtEmail" MaxLength="80" CssClass="percent85" runat="server" />
                                                </div>

                                            </div>
                                        </div>
                                        <div class="align-left percent33">

                                            <div class="margin-bottom-10">
                                                <div>
                                                    <asp:Literal runat="server" Text="<%$Resources:MultiLang,CCHolderIP %>" />&nbsp;
                                                    <span class="require">
                                                        <asp:Literal ID="litRequiredIP" runat="server" Text="<%$Resources:VirtualTerminalCC.aspx, RequiredField %>" /></span>
                                                </div>

                                                <div>
                                                    <asp:TextBox ID="txtClientIP" class="percent85" runat="server" />
                                                </div>
                                                <div><span class="help">(<asp:Literal runat="server" Text="<%$Resources:MultiLang,NeededRisk %>" />)</span></div>

                                            </div>

                                            <div class="margin-bottom-10">
                                                <div>
                                                    <asp:Literal runat="server" Text="<%$Resources:MultiLang,CCHolderID %>" />&nbsp;
                                                     <span class="require">
                                                         <asp:Literal ID="litRequiredCCHolderID" runat="server" Text="<%$Resources:VirtualTerminalCC.aspx, RequiredField %>" /></span>
                                                </div>
                                                <div>
                                                    <asp:TextBox ID="txtCCHolderID" class="percent85" runat="server" />
                                                </div>

                                            </div>
                                        </div>
                                        <div class="spacer"></div>
                                        <asp:PlaceHolder runat="server" ID="phCreditCardStorage" Visible="false">
                                            <asp:CheckBox ID="ChkStorageCard" Text="<%$Resources: VirtualTerminalCC.aspx,  StorageCardSave %>" runat="server" />
                                        </asp:PlaceHolder>
                                    </div>
                                    <!-- End Credit Card Details -->




                                    <!-- Transaction Details -->
                                    <div class="form-group-title margin-bottom-10">
                                        <i class="fa fa-sticky-note-o"></i>
                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang,TransactionDetails %>" />
                                    </div>
                                    <div class="margin-bottom-10 frame-border-single">
                                        <div class="margin-bottom-10">
                                            <asp:Literal runat="server" Text="<%$Resources:VirtualTerminalCC.aspx, TransactionType %>" />: 
                                            <asp:RadioButtonList CssClass="RadioButtonWidthVT" RepeatDirection="horizontal" ID="rblTransactionType" runat="server" RepeatLayout="Flow" OnSelectedIndexChanged="SetTransactionType" AutoPostBack="true">
                                                <asp:ListItem Value="TransactionTypeSale" Text="<%$Resources:VirtualTerminalCC.aspx, TransactionTypeSale %>" Selected="True" />
                                                <asp:ListItem Value="TransactionTypeAuthorization" Text="<%$Resources:VirtualTerminalCC.aspx, TransactionTypeAuthorization %>" />
                                                <asp:ListItem Value="TransactionTypeCapture" Text="<%$Resources:VirtualTerminalCC.aspx, TransactionTypeCapture %>" />
                                                <asp:ListItem Value="TransactionTypePhoneConfirmed" Text="<%$Resources:VirtualTerminalCC.aspx, TransactionTypePhoneConfirmed %>" />
                                            </asp:RadioButtonList>
                                        </div>
                                        <hr class="style-line margin-bottom-10" />
                                        <div class="align-left percent33">
                                            <div class="margin-bottom-10">
                                                <div>
                                                    <asp:Literal runat="server" Text="<%$Resources:MultiLang,Amount %>" />&nbsp;
                                                    <asp:RequiredFieldValidator ID="ValidAmount" runat="server" CssClass="require" Display="Dynamic" ErrorMessage="<%$Resources:VirtualTerminalCC.aspx, RequiredField %>" ControlToValidate="txtAmount" ValidationGroup="VTForm" />
                                                </div>
                                                <div>
                                                    <asp:TextBox ID="txtAmount" class="percent85" runat="server" />
                                                </div>

                                            </div>
                                            <div class="margin-bottom-10">
                                                <div>
                                                    <asp:Literal runat="server" Text="<%$Resources:MultiLang,CreditType %>" />
                                                </div>
                                                <div>
                                                    <netpay:CreditTypeDropDown ID="ddlCreditType" EnableBlankSelection="false" OnSelectedIndexChanged="SetInstallmentsVisibility" OnChange="CheckCredit();" class="percent85" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="align-left percent33">
                                            <div class="margin-bottom-10">
                                                <div>
                                                    <asp:Literal runat="server" Text="<%$Resources:VirtualTerminalCC.aspx, Currency %>" />
                                                    <asp:RequiredFieldValidator ID="ValidCurrency" runat="server" CssClass="require" ControlToValidate="ddlCurrency" ValidationGroup="VTForm" ErrorMessage="<%$Resources:VirtualTerminalCC.aspx, Required_Field %>" Display="Dynamic" />
                                                </div>
                                                <div>
                                                    <netpay:CurrencyDropDown class="percent85" EnableBlankSelection="false" ID="ddlCurrency" runat="server" />
                                                </div>
                                                <div>
                                                    <asp:Label runat="server" CssClass="help" Text="<%$Resources:VirtualTerminalCC.aspx, ConversionChargeTip %>" />
                                                </div>

                                            </div>
                                            <div id="divInstallments" class="margin-bottom-10" style="display: none;">
                                                <div><span id="PaymentsHead">&nbsp;<asp:Literal runat="server" Text="<%$Resources:MultiLang,Installments %>" /></span></div>
                                                <div>
                                                    <asp:DropDownList ID="ddlPayment" class="percent85" runat="server">
                                                        <asp:ListItem></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="align-left percent33">
                                            <div class="margin-bottom-10">
                                                <!-- Code for Conversion fee -->
                                            </div>
                                            <asp:Panel ID="pnlApproval" runat="server" Visible="false">
                                                <div>
                                                    <asp:Literal ID="litApprove" runat="server" />
                                                    <netpay:PageItemHelp ID="pihApprove" Width="300" Height="28" runat="server" />
                                                </div>
                                                <div class="margin-bottom-10">
                                                    <asp:TextBox ID="txtApprove" CssClass="percent85" runat="server" />
                                                </div>
                                            </asp:Panel>
                                        </div>
                                        <div class="spacer"></div>
                                    </div>
                                </div>
                                <!-- End Transaction Details -->

                            </li>
                            <li>
                                <!-- Address Section -->

                                <div class="wrap-edit-product">
                                    <div class="align-left">
                                        <i class="fa fa-building"></i>
                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang,Billingaddress %>" />
                                    </div>
                                    <div class="align-right">
                                        <i class="fa fa-2x fa-angle-right"></i>
                                    </div>
                                    <div class="spacer"></div>
                                </div>

                                <div class="frame-border-option-inline padding-10 margin-bottom-10" style="display: none;">
                                    <div class="form-group-title margin-bottom-10">
                                         <i class="fa fa-home"></i>
                                          <asp:Literal runat="server" Text="<%$Resources:VirtualTerminalCC.aspx, AddressDetalis %>" />&nbsp;<span class="require"><asp:Literal runat="server" ID="ltAddress" Text="<%$Resources:VirtualTerminalCC.aspx, RequiredField %>" /></span><asp:Literal runat="server" ID="ltOptionalAddress" Text="<%$Resources:VirtualTerminalCC.aspx, OptionalField %>" />
                                    </div>
                                    <div class="margin-bottom-10 frame-border-single">
                                        <div class="align-left percent33">
                                            <div class="margin-bottom-10">
                                                <div>
                                                    <asp:Literal runat="server" Text="<%$Resources:MultiLang,Address1 %>" />
                                                    <span class="require">
                                                        <asp:Literal ID="litRequiredAddress1" runat="server" Text="<%$Resources:VirtualTerminalCC.aspx, RequiredField %>" /></span>
                                                    <netpay:PageItemHelp ID="PageItemHelp4" Text="<%$Resources:MultiLang,AddressLine1Help%>" Width="300" Height="28" runat="server" />
                                                </div>
                                                <div>
                                                    <asp:TextBox ID="txtAddress1" CssClass="percent85" placeholder="Street Address P.O.B" runat="server" />
                                                </div>

                                            </div>
                                            <div class="margin-bottom-10">
                                                <div>
                                                    <asp:Literal runat="server" Text="<%$Resources:MultiLang,Address2 %>" />&nbsp;
                                                        <netpay:PageItemHelp ID="PageItemHelp1" Text="<%$Resources:MultiLang,AddressLine2Help%>" Width="300" Height="28" runat="server" />
                                                </div>
                                                <div>
                                                    <asp:TextBox ID="txtAddress2" CssClass="percent85" placeholder="Street Address P.O.B" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="align-left percent33">
                                            <div class="margin-bottom-10">
                                                <div>
                                                    <asp:Literal runat="server" Text="<%$Resources:MultiLang,Country %>" />
                                                    <span class="require">
                                                        <asp:Literal ID="litRequiredCountry" runat="server" Text="<%$Resources:VirtualTerminalCC.aspx, RequiredField %>" /></span>
                                                </div>
                                                <div>
                                                    <netpay:CountryDropDown ID="ddlCountry" EnableBlankSelection="true" AutoPostBack="false" CssClass="percent85" onchange="javascript:dopostddlstate()" runat="server" />
                                                </div>
                                            </div>
                                            <div class="margin-bottom-10">
                                                <div>
                                                    <asp:Literal runat="server" Text="<%$Resources:MultiLang,State %>" />
                                                </div>
                                                <div id="DivUsa" runat="server">
                                                    <netpay:StateDropDown ID="ddlStateUsa" CountryID="228" CssClass="percent85" runat="server" />
                                                </div>
                                                <div id="DivCanada" runat="server">
                                                    <netpay:StateDropDown ID="ddlStateCanada" CountryID="43" CssClass="percent85" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="align-left percent33">
                                            <div class="margin-bottom-10">
                                                <div>
                                                    <asp:Literal runat="server" Text="<%$Resources:MultiLang,CCity %>" />
                                                    <span class="require">
                                                        <asp:Literal ID="litRequiredCity" runat="server" Text="<%$Resources:VirtualTerminalCC.aspx, RequiredField %>" /></span>
                                                </div>
                                                <div>
                                                    <asp:TextBox ID="txtCity" CssClass="percent85" runat="server" />
                                                </div>

                                            </div>

                                            <div class="margin-bottom-10">
                                                <div>
                                                    <asp:Literal runat="server" Text="<%$Resources:MultiLang,ZipCode %>" />
                                                    <span class="require">
                                                        <asp:Literal ID="litRequiredZipcode" runat="server" Text="<%$Resources:VirtualTerminalCC.aspx, RequiredField %>" /></span>
                                                </div>
                                                <div>
                                                    <asp:TextBox ID="txtZipCode" CssClass="percent85" runat="server" />
                                                </div>

                                            </div>
                                        </div>
                                        <div class="spacer"></div>
                                    </div>
                                </div>
                                <!-- End Address section -->
                            </li>
                            <li>
                                <!-- Recurring Transaction -->
                                <asp:PlaceHolder runat="server" ID="pnRecurring">
                                    <div class="wrap-edit-product">
                                        <div class="align-left">
                                            <i class="fa fa-refresh"></i>
                                            <asp:Literal runat="server" Text="<%$Resources:MultiLang,RecurringTransaction %>" />
                                        </div>
                                        <div class="align-right">
                                            <i class="fa fa-2x fa-angle-right"></i>
                                        </div>
                                        <div class="spacer"></div>
                                    </div>
                                    <div class="frame-border-option-inline padding-10 margin-bottom-10" style="display: none;">
                                        <asp:MultiView ID="mvRecurring" runat="server">
                                            <asp:View ID="vDisabled" runat="server">
                                                <div class="margin-bottom-10">
                                                    <div class="error" style="margin: 0px 0px 8px 0px;">
                                                        <asp:Literal ID="litRecurringNotAllowed" runat="server" Text="<%$Resources:VirtualTerminalCC.aspx,RecurringNotAllowed %>" />
                                                    </div>
                                                </div>
                                            </asp:View>
                                            <asp:View ID="vEnabled" runat="server">
                                                <div class="margin-bottom-10">
                                                    <asp:CheckBox runat="server" ID="chkIsUseRecurring" CssClass="option" />&nbsp;
                                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang,CreateRecurring %>" />
                                                </div>
                                                <div class="margin-bottom-10 line-height-35 align-left percent50">
                                                    <asp:Label ID="lblRecurringMode" runat="server" Text="<%$Resources:VirtualTerminalCC.aspx, IntervalBetweenCharges %>" />&nbsp;
                                                        <asp:Label ID="lblRecurringRequiredMode" CssClass="require" runat="server" Text="*" />&nbsp;
                                                        <asp:TextBox ID="RecurringCycle" CssClass="small" runat="server" Text="1" />&nbsp;
                                                        <asp:DropDownList ID="RecurringMode" runat="server" CssClass="percent55">
                                                            <asp:ListItem Value="" Text="" />
                                                            <asp:ListItem Value="D" Text="<%$Resources:VirtualTerminalCC.aspx, Days %>" />
                                                            <asp:ListItem Value="W" Text="<%$Resources:VirtualTerminalCC.aspx, Weeks %>" />
                                                            <asp:ListItem Value="M" Text="<%$Resources:VirtualTerminalCC.aspx, Months %>" Selected="True" />
                                                            <asp:ListItem Value="Q" Text="<%$Resources:VirtualTerminalCC.aspx, Quarters %>" />
                                                            <asp:ListItem Value="Y" Text="<%$Resources:VirtualTerminalCC.aspx, Years %>" />
                                                        </asp:DropDownList>
                                                </div>
                                                <div class="margin-bottom-10 line-height-35 align-right percent50">
                                                    <div>
                                                        <asp:Label ID="lblRecurringCount" runat="server" Text="<%$Resources:VirtualTerminalCC.aspx, NumberOfCharges %>" />&nbsp;<asp:Label ID="lblRecurringRequiredCount" CssClass="require" runat="server" Text="*" />
                                                        &nbsp;
                                                            <asp:TextBox ID="RecurringCount" CssClass="small" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="spacer"></div>
                                            </asp:View>
                                        </asp:MultiView>
                                    </div>
                                </asp:PlaceHolder>



                                <!-- End Recurring Transaction -->
                            </li>
                            <li>
                                <!-- Comment -->
                                <div class="wrap-edit-product">
                                    <div class="align-left">
                                        <i class="fa fa-comments-o"></i>
                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang,Comment %>" />
                                    </div>
                                    <div class="align-right">
                                        <i class="fa fa-2x fa-angle-right"></i>
                                    </div>
                                    <div class="spacer"></div>
                                </div>
                                <div class="frame-border-option-inline padding-10 margin-bottom-10" style="display: none;">
                                    <div class="margin-bottom-10">
                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang, WriteComment %>" />
                                    </div>
                                    <div class="percent100">
                                        <asp:TextBox ID="txtComment" runat="server" class="percent100 percent-height-10" TextMode="MultiLine" />
                                    </div>
                                </div>
                                <!-- End Comment -->
                            </li>
                        </ul>
                    </div>
                      <div class="require align-left">
                    *
                    <asp:Literal Text="<%$Resources:MultiLang,RequiredFields %>" runat="server" />
                </div>
                 <div class="align-right">
                      <asp:Button ID="BtnCharge" Text="<%$Resources:MultiLang,Charge %>" CssClass="btn btn-success btn-cons-short" UseSubmitBehavior="false" CausesValidation="true" ValidationGroup="VTForm" OnClick="BtnCharge_OnClick" OnClientClick="if(Page_ClientValidate()) this.disabled=true" runat="server" />
                 </div>
                <div class="spacer"></div>
                </div>
              
            </asp:PlaceHolder>
        </div>
    </div>
</asp:Content>
