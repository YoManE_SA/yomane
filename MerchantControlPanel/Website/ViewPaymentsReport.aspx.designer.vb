﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Website_ViewPaymentsReport
    
    '''<summary>
    '''locPageTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents locPageTitle As Global.System.Web.UI.WebControls.Localize
    
    '''<summary>
    '''Localize1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Localize1 As Global.System.Web.UI.WebControls.Localize
    
    '''<summary>
    '''txtnumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtnumber As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''ddlcardType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlcardType As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''txtCustomerNum control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCustomerNum As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtCustomerName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCustomerName As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''BtnSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents BtnSearch As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''repeaterSettlements control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents repeaterSettlements As Global.System.Web.UI.WebControls.Repeater
    
    '''<summary>
    '''pager control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pager As Global.Netpay.Web.Controls.LinkPager
End Class
