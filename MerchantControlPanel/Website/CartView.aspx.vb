﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Linq
Imports Netpay.Bll
Imports Netpay.Bll.Reports
Imports System.Collections.Generic
Imports Netpay.Infrastructure.VO
Imports Netpay.Web
Imports Netpay.Infrastructure


Partial Class Website_CartView
	Inherits MasteredPage

	Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
		MyBase.OnLoad(e)
		Dim cartID As Guid = Request("cartID").ToGuid()
		Dim cart As CartVO = Netpay.Bll.Cart.GetCart(WebUtils.DomainHost, cartID)
		If cart Is Nothing Then Return

		Dim Curency As CurrencyVO = CurrentDomain.Cache.GetCurrency(cart.Currency)
		ltRecepientName.Text = cart.RecepientName
		ltRecepientPhone.Text = cart.RecepientPhone
		ltRecepientMail.Text = cart.RecepientMail
		ltShippingAddress.Text = cart.ShippingAddress
		ltShippingAddress2.Text = cart.ShippingAddress2
		ltShippingCity.Text = cart.ShippingCity
		ltShippingZip.Text = cart.ShippingZip
		If cart.ShippingState IsNot Nothing Then ltShippingState.Text = cart.ShippingState
		If cart.ShippingCountry IsNot Nothing Then ltShippingCountry.Text = cart.ShippingCountry
		If cart.CheckoutDate IsNot Nothing Then ltCheckoutDate.Text = cart.CheckoutDate

		ltTotalProsucts.Text = cart.TotalItems
		ltTax.Text = cart.TotalTax.ToAmountFormat(Curency)
		ltShipping.Text = cart.TotalShipping.ToAmountFormat(Curency)
		ltTotal.Text = cart.Total.ToAmountFormat(Curency)

		repeaterResults.DataSource = Netpay.Bll.Cart.GetCartItems(WebUtils.DomainHost, cartID)
		repeaterResults.DataBind()
	End Sub

End Class
