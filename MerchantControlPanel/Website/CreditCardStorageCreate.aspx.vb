﻿Imports Netpay.Infrastructure
Imports Netpay.Bll
Imports Netpay.Web.Controls

Partial Class Website_CreditCardStorageCreate
	Inherits MasteredPage

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			wcExpirationYear.YearFrom = DateTime.Now.Year
			wcExpirationYear.YearTo = DateTime.Now.AddYears(10).Year
			DivCanada.Attributes.Add("Style", "display:none;")
		End If
	End Sub

	Protected Sub CreateStoredCard(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        Dim data As Bll.Merchants.MethodStorage = New Bll.Merchants.MethodStorage(Merchant.ID)
		If wcExpirationMonth.IsSelected And wcExpirationYear.IsSelected Then
			Dim expirationDate As DateTime = New DateTime(wcExpirationYear.SelectedYear, wcExpirationMonth.SelectedMonth, 1)
			data.MethodInstance.ExpirationDate = expirationDate
		Else
			lblResult.Text = "<div class=""Error"">" & GetGlobalResourceObject("MultiLang", "expirationDateRequired") & "</div>"
			Return
		End If

		If data.MethodInstance.ExpirationDate < DateTime.Now Then
			lblResult.Text = "<div class=""Error"">" & GetGlobalResourceObject("MultiLang", "cardExpired") & "</div>"
			Return
		End If

		If txtCardholderFullName.Text.Trim() = "" Then
			lblResult.Text = "<div class=""Error"">" & GetGlobalResourceObject("MultiLang", "cardholderNameRequired") & "</div>"
			Return
		End If
		data.MethodInstance.PaymentMethodId = Netpay.Bll.PaymentMethods.CreditCard.GetPaymentMethodId(wcCreditCard.Card.CardNumber)
		data.MethodInstance.SetEncryptedData(wcCreditCard.Card.CardNumber, Nothing) 'txtCvv.Text
		data.MethodInstance.ExpirationDate = New DateTime(data.MethodInstance.ExpirationDate.GetValueOrDefault().Year, data.MethodInstance.ExpirationDate.GetValueOrDefault().Month, 1).AddMonths(1).AddDays(-1)
        'If txtCvv IsNot Nothing Then data.Cui = txtCvv.Text
		If data.MethodInstance.Validate() <> ValidationResult.Success Then
			lblResult.Text = "<div class=""Error"">" & GetGlobalResourceObject("MultiLang", "invalidCard") & "</div>"
			Return
		End If
		data.IPAddress = txtClientIP.Text

		data.BA_City = txtCardholderCity.Text
		data.Owner_EmailAddress = txtCardholderEmail.Text
		data.Owner_FirstName = txtCardholderFullName.Text
		data.Owner_PersonalNumber = txtCardholderID.Text
		data.Owner_PhoneNumber = txtCardholderPhoneNumber.Text
		data.BA_Street1 = txtCardholderStreet1.Text
		data.BA_Street2 = txtCardholderStreet2.Text
		data.BA_PostalCode = txtCardholderZipcode.Text
		data.Comment = txtComment.Text
		data.BA_CountryISOCode = ddlCountry.ISOCode
		'If data.CountryID.Value = 228 Then
			data.BA_StateISOCode = ddlStateUsa.ISOCode
		'ElseIf data.CountryID = 43 Then
			data.BA_StateISOCode = ddlStateCanada.ISOCode
		'End If

		data.Save()
		Response.Redirect("CreditCardStorage.aspx")
	End Sub
End Class
