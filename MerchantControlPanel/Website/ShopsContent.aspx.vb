﻿Imports Netpay.Web.Controls
Imports Netpay.Infrastructure 

Partial Class ShopsContent
    Inherits MasteredPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then LoadLanguages()
    End Sub

    Private Sub SetVisibility()
        tbLanguageText.Visible = tbLanguageText.Tabs.Count > 0
        divContent.Visible = tbLanguageText.Tabs.Count > 0
        divUpdate.Visible = tbLanguageText.Tabs.Count > 0
    End Sub

    Protected Sub LoadLanguages()
        Dim hppLanguages = System.Configuration.ConfigurationManager.AppSettings("HppV2Languages").Split(",")
        Dim defLanguage = Bll.Merchants.Hosted.PaymentPage.GetDefaultLanguage(Merchant.ID)
        Dim allLanguages = Bll.International.Language.Cache.Where(Function(l) hppLanguages.Contains(l.Culture)).ToDictionary(Function(l) l.ID)
        Dim availLanguages As New List(Of Integer)

        For Each language In allLanguages
            Dim aboutData As String = Bll.Merchants.Merchant.GetContent(Merchant.AccountID, language.Value.ID, "About.html")
            If aboutData IsNot Nothing Then
                Dim tab As TabView = tbLanguageText.AddTemplateTab(language.Value.Name)
                tab.ItemData = language.Key
                CType(tab.FindControl("txtAbout"), TextBox).Text = aboutData.EmptyIfNull().Replace("<br />", vbCrLf)
                CType(tab.FindControl("txtPrivacy"), TextBox).Text = Bll.Merchants.Merchant.GetContent(Merchant.AccountID, language.Value.ID, "Privacy.html").EmptyIfNull().Replace("<br />", vbCrLf)
                CType(tab.FindControl("txtTerms"), TextBox).Text = Bll.Merchants.Merchant.GetContent(Merchant.AccountID, language.Value.ID, "Terms.html").EmptyIfNull().Replace("<br />", vbCrLf)
                availLanguages.Add(language.Key)
            End If
        Next

        If tbLanguageText.Tabs.Count > 0 And Not IsPostBack Then tbLanguageText.ActiveViewIndex = 0

        For Each l In availLanguages
            allLanguages.Remove(l)
        Next

        Dim oldValue = ddlLanguage.SelectedValue
        ddlLanguage.DataSource = allLanguages.Values
        ddlLanguage.DataTextField = "Name"
        ddlLanguage.DataValueField = "ID"
        ddlLanguage.DataBind()
        ddlLanguage.SelectedValue = oldValue

        SetVisibility()
    End Sub

    Protected Sub AddLanguage_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlLanguage.SelectedIndex < 1 Then Return
        Dim language = CType(ddlLanguage.SelectedValue, Netpay.CommonTypes.Language)
        Dim tab As TabView = tbLanguageText.AddTemplateTab(Bll.International.Language.Get(language).Name)
        tab.ItemData = language
        tbLanguageText.ActiveViewIndex = tbLanguageText.Tabs.Count - 1
        ddlLanguage.Items.RemoveAt(ddlLanguage.SelectedIndex)
        AddlngAlert.Visible = True

        SetVisibility()
    End Sub

    Protected Sub RemoveLanguage_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim tab As TabView = tbLanguageText.Tabs(tbLanguageText.ActiveViewIndex)
        Dim language As Netpay.CommonTypes.Language = tab.ItemData

        Bll.Merchants.Merchant.RemoveContent(Merchant.AccountID, language, "About.html")
        Bll.Merchants.Merchant.RemoveContent(Merchant.AccountID, language, "Privacy.html")
        Bll.Merchants.Merchant.RemoveContent(Merchant.AccountID, language, "Terms.html")

        tbLanguageText.Tabs.Remove(tab)

        SetVisibility()
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        For Each tab As TabView In tbLanguageText.Tabs
            If tab.ItemData IsNot Nothing Then
                Dim language As Netpay.CommonTypes.Language = tab.ItemData
                Bll.Merchants.Merchant.SaveContent(Merchant.AccountID, language, "About.html", CType(tab.FindControl("txtAbout"), TextBox).Text.Replace(vbLf, vbLf & "<br />"))
                Bll.Merchants.Merchant.SaveContent(Merchant.AccountID, language, "Privacy.html", CType(tab.FindControl("txtPrivacy"), TextBox).Text.Replace(vbLf, vbLf & "<br />"))
                Bll.Merchants.Merchant.SaveContent(Merchant.AccountID, language, "Terms.html", CType(tab.FindControl("txtTerms"), TextBox).Text.Replace(vbLf, vbLf & "<br />"))
            End If
        Next
    End Sub
End Class
