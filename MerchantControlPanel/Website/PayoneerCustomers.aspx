﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Tmp_netpayintl/page.master"
    CodeBehind="PayoneerCustomers.aspx.vb" Inherits="Netpay.MerchantControlPanel.PayoneerCustomers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:title%>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$Resources:title%>" runat="server" /></div>
        <div class="content-background">
            <div class="legend-menu">
                <div class="align-right">
                    <netpay:PopupButton ID="PopupButton1" IconSrc="../Tmp_netpayintl/Images/iconLegend.gif"
                        Text="<%$Resources:MultiLang,Legend  %>" runat="server">
                        <netpay:Legend ID="ttLegend" Approved="true" NotApproved="true" runat="server" />
                    </netpay:PopupButton>
                </div>
            </div>
            <div class="section">
                <netpay:ElementBlocker ID="wcElementBlocker" Visible="true" ElementClientID="divBlocker"
                    Text="<%$ Resources:MultiLang,FeatureDisabled %>" runat="server" />
                <div>
                    <asp:Button ID="btnShowAddCustomer" Text="<%$Resources:AddCustomer %>" CssClass="btn btn-primary" OnClick="ShowAddCustomerForm" runat="server" /></div>
                <asp:PlaceHolder ID="phAddCustomerForm" Visible="false" runat="server">
                    <table style="width: 60%">
                        <tr>
                            <td>
                                <input type="text" id="txtName" title="<%$Resources:MultiLang,Name %>" runat="server" />
                            </td>
                            <td>
                                <input type="text" id="txtEmail" runat="server" title="<%$Resources:MultiLang,Email %>" />
                            </td>
                            <td>
                                <input type="text" id="txtComment" runat="server" title="<%$Resources:MultiLang,Comment %>" />
                            </td>
                            <td>
                                <asp:Button ID="btnAdd" Text="<%$Resources:MultiLang,Add %>" CssClass="btn btn-primary"
                                    BorderWidth="0" TabIndex="18" OnClick="AddCustomer" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:PlaceHolder>
            </div>
            <div class="section">
                <div class="align-left">
                    <table>
                        <tr>
                            <td>
                                <asp:DropDownList ID="ddCustomerStatus" runat="server">
                                    <asp:ListItem Text="<%$Resources:MultiLang,Status %>" Value=""></asp:ListItem>
                                    <asp:ListItem Text="<%$Resources:Added %>" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="<%$Resources:RegistrationEmailSent %>" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="<%$Resources:Registered %>" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="<%$Resources:Approved %>" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="<%$Resources:Declined %>" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="<%$Resources:RegistrationEmailFailed %>" Value="6"></asp:ListItem>
                                    <asp:ListItem Text="<%$Resources:Activated %>" Value="7"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <input type="text" id="txtFilterEmail" runat="server" title="<%$Resources:MultiLang,EMail %>" />
                            </td>
                            <td>
                                <input type="text" id="txtFilterComment" runat="server" title="<%$Resources:MultiLang,Comment %>" />
                            </td>
                            <td>
                                <asp:Button ID="btnFilter" Text="<%$Resources:MultiLang,Search %>" CssClass="btn btn-primary"
                                    TabIndex="18" OnClick="GetCustomers" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="align-right">
                    <div class="filter">
                        <div class="quick-search">
                            <asp:Literal ID="Literal4" runat="server" Text="<%$Resources:PayeeID %>"></asp:Literal>
                            <div class="field-quick">
                                <asp:TextBox ID="txtPayeeID" runat="server" CssClass="middle-value"></asp:TextBox>
                            </div>
                            <div class="button-quick">
                                <asp:Button ID="btnShowPayee" OnClick="ShowPayee" Text="<%$Resources:MultiLang,Show %>"
                                    CssClass="btn btn-primary" BorderWidth="0" runat="server" OnClientClick="javascript:return document.getElementById('ctl00_cphBody_txtPayeeID').value!=''" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="spacer">
                </div>
            </div>
            <div class="section">
                <asp:PlaceHolder ID="phResults" Visible="false" runat="server">
                    <table class="exspand-table">
                        <tr>
                            <th>
                                &nbsp;
                            </th>
                            <th>
                                <asp:Literal ID="Literal5" Text="<%$Resources:MultiLang,ID %>" runat="server"></asp:Literal>
                            </th>
                            <th>
                                <asp:Literal ID="Literal6" Text="<%$Resources:MultiLang,Name %>" runat="server"></asp:Literal>
                            </th>
                            <th>
                                <asp:Literal ID="Literal7" Text="<%$Resources:MultiLang,Email %>" runat="server"></asp:Literal>
                            </th>
                            <th>
                                <asp:Literal ID="Literal8" Text="<%$Resources:MultiLang,Status %>" runat="server"></asp:Literal>
                            </th>
                            <th>
                                <asp:Literal ID="Literal9" Text="<%$Resources:MultiLang,Comment %>" runat="server"></asp:Literal>
                            </th>
                            <th>
                                &nbsp;
                            </th>
                        </tr>
                        <asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server">
                            <ItemTemplate>
                                <tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
                                    <td width="8px" class="<%# GetCssClass(CType(Container.DataItem, ExternalCardCustomerVO))%>">
                                        &nbsp;
                                    </td>
                                    <td>
                                        <%# CType(Container.DataItem, ExternalCardCustomerVO).ID %>
                                    </td>
                                    <td>
                                        <%# CType(Container.DataItem, ExternalCardCustomerVO).Name %>
                                    </td>
                                    <td>
                                        <%# CType(Container.DataItem, ExternalCardCustomerVO).Email%>
                                    </td>
                                    <td>
                                        <%# GetLocalResourceObject(CType(Container.DataItem, ExternalCardCustomerVO).Status.ToString())%>
                                    </td>
                                    <td>
                                        <%# CType(Container.DataItem, ExternalCardCustomerVO).Comment%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%# GetPaymentLink(CType(Container.DataItem, ExternalCardCustomerVO))%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
                </asp:PlaceHolder>
                <asp:Label ID="lblError" CssClass="error" runat="server"></asp:Label>
            </div>
        </div>
    </div>
</asp:Content>
