﻿
Imports Netpay.Web.Controls
Imports Netpay.Web
Imports Netpay.Infrastructure
Imports Netpay.Bll

Partial Class Website_EditLogin
	Inherits MasteredPage

	Sub SaveEditForm(ByVal o As Object, ByVal e As EventArgs)
		lblResult.Text = String.Empty
		Dim nID As Integer = Merchant.ID
		If nID = 0 Then
			lblResult.Text = "<div class=""error"">" & CType(GetGlobalResourceObject("MultiLang", "ErrorUserNotFound"), String) & "</div>"
			Exit Sub
		End If

		If LoggedUser.IsFirstLogin Then
			If Validation.IsEmptyField(txtPassword, lblResult, CType(GetGlobalResourceObject("MultiLang", "Password"), String)) Then Exit Sub
		End If

        Dim bMailChanged As Boolean = False
        If (txtMail.Text.Trim <> hidMailOld.Value Or txtMail2.Text.Trim <> hidMailOld.Value) And Not LoggedUser.IsFirstLogin Then
			If Validation.IsEmptyField(txtMail, lblResult, CType(GetGlobalResourceObject("MultiLang", "Email"), String)) Then Exit Sub
			If Validation.IsNotMail(txtMail, lblResult, CType(GetGlobalResourceObject("MultiLang", "Email"), String)) Then Exit Sub
			If Validation.IsMismatchFields(txtMail, txtMail2, lblResult, CType(GetGlobalResourceObject("MultiLang", "Email"), String), CType(GetGlobalResourceObject("MultiLang", "ValidateEmail"), String)) Then Exit Sub
            Security.Login.Current.EmailAddress = txtMail.Text
            Dim ret = Security.Login.Current.Validate()
            Select Case ret
                Case ValidationResult.EmailAlreadyExist
                    lblResult.Text = "<div class=""error"">" & CType(GetGlobalResourceObject("MultiLang", "ErrorDuplicateMailPrevention"), String).Replace("%BRAND%", CurrentDomain.BrandName).Replace("%MAIL%", txtMail.Text) & "</div>"
                    Exit Sub
                Case ValidationResult.Invalid_Email
                    lblResult.Text = "<div class=""error"">" & CType(GetGlobalResourceObject("MultiLang", "ErrorEmptyField"), String).Replace("%FIELD%", CType(GetGlobalResourceObject("MultiLang", "Email"), String)) & "</div>"
                Case ValidationResult.Success
                    Security.Login.Current.Save()
                    bMailChanged = True
                    Exit Sub
                Case Else
                    lblResult.Text = "<div class=""error"">" & CType(GetGlobalResourceObject("MultiLang", "Error"), String) & " " & ret.ToString() & "</div>"
                    Exit Sub
            End Select
        End If
		If Validation.IsEmptyField(txtPassword) And Validation.IsEmptyField(txtPassword2) Then
		Else
			If Validation.IsEmptyField(txtPassword, lblResult, CType(GetGlobalResourceObject("MultiLang", "Password"), String)) Then Exit Sub
			If Validation.IsInvalidPassword(txtPassword, lblResult, CType(GetGlobalResourceObject("MultiLang", "Password"), String)) Then Exit Sub
            If Validation.IsMismatchFields(txtPassword, txtPassword2, lblResult, CType(GetGlobalResourceObject("MultiLang", "Password"), String), CType(GetGlobalResourceObject("MultiLang", "ValidatePassword"), String)) Then Exit Sub
            Dim ret = Netpay.Infrastructure.Security.Login.Current.SetPassword(txtPassword.Text, Request.ServerVariables("REMOTE_ADDR"))
            If ret <> Security.Login.ChangePasswordResult.Success Then
                lblResult.Text = "<div class=""error"">" & CType(GetGlobalResourceObject("MultiLang", "ErrorPasswordAlreadyUsed"), String) & "</div>"
            Else
                lblResult.Text = "<div class=""noError"">" & CType(GetGlobalResourceObject("MultiLang", "PasswordChanged"), String) & "</div>"
                If LoggedUser.IsFirstLogin Then Response.Redirect("content.aspx?CN=AccountActivated", True)
                Response.Redirect("content.aspx?CN=DetailsUpdated", True)
            End If
        End If
		If bMailChanged Then lblResult.Text = "<div class=""noerror"">" & CType(GetGlobalResourceObject("MultiLang", "EmailChanged"), String) & "</div>" & lblResult.Text
		If Not lblResult.Text.Contains("""error""") Then Response.Redirect("content.aspx?CN=DetailsUpdated")
	End Sub

	Sub Page_Load()
		If Not Page.IsPostBack Then
            Dim sMail As String = Merchant.EmailAddress
            hidMailOld.Value = sMail
			If Not String.IsNullOrEmpty(sMail) Then
				txtMail.Text = sMail
				txtMail2.Text = sMail
			End If
			Dim sResourceField As String = IIf(LoggedUser.IsFirstLogin, "ChangePasswordTextNotVerified", "ChangePasswordTextVerified")
			lblText.Text = CType(GetGlobalResourceObject("MultiLang", sResourceField), String).Replace("%BRAND%", CurrentDomain.BrandName)
			If LoggedUser.IsFirstLogin Then
				lblMailText.Text = String.Empty
				txtMail.Enabled = False
				txtMail2.Enabled = False
			Else
				lblMailText.Text = CType(GetGlobalResourceObject("MultiLang", "SignupEMailText"), String)
				lblMailText.Text &= "<ul style=""list-style:circle; font-size:11px; margin:3px 20px;"">"
				lblMailText.Text &= "<li>" & CType(GetGlobalResourceObject("MultiLang", "SignupEMailText1"), String) & "</li>"
				lblMailText.Text &= "<li>" & CType(GetGlobalResourceObject("MultiLang", "SignupEMailText2"), String) & "</li>"
				lblMailText.Text &= "</ul><br />"
			End If
		End If
	End Sub
End Class
