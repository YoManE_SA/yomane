﻿
Partial Class Website_PayeeList
    Inherits MasteredPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        litOwnBeneficiary.Text = "<img src=""Images/icon_contactRed.gif"" alt=""" & GetLocalResourceObject("OwnBeneficiary") & """ title=""" & GetLocalResourceObject("OwnBeneficiary") & """ />"
        If Not IsPostBack And Account IsNot Nothing Then
            repeaterResults.DataSource = Bll.Accounts.Payee.LoadForAccount(Account.AccountID)
            repeaterResults.DataBind()
        End If
    End Sub

    Function GetRowBackground(ByVal beneficiary As Bll.Accounts.Payee) As String
        Return IIf(CanRequestPayment(beneficiary), "", "#ffebe1;")
    End Function

    Function GetEditButton(ByVal payee As Bll.Accounts.Payee, ByVal retButton As Boolean) As String
        If False Then
            If retButton Then Return "&nbsp;"
            Return String.Format("<img src='Images/icon_contactRed.gif' alt='{1}' title='{1}' />", GetLocalResourceObject("OwnBeneficiary"))
        Else
            Dim link As String = String.Format("Payee.aspx?ID={0}&ProfileType={1}", payee.ID, 0) 'beneficiary.ProfileType
            If retButton Then Return String.Format("<input type=""button"" onclick=""document.location='{0}'"" value='  {1}  ' class=""btn btn-danger""  />", link, GetGlobalResourceObject("MultiLang", "Edit"))
            Return String.Format("<a href='{0}'><img src='Images/icon_contactBlue.gif' alt='{1}' title='{1}' /></a>", link, GetGlobalResourceObject("MultiLang", "Edit"))
        End If
    End Function

    Sub btnAdd_OnClick(ByVal o As Object, ByVal e As System.EventArgs)
        Response.Redirect("Payee.aspx?ProfileType=" & Request("ProfileType"))
    End Sub

    Function CanRequestPayment(ByVal payee As Bll.Accounts.Payee) As Boolean
        If Not String.IsNullOrEmpty(payee.BankAccount.ABA) Then Return True
        If Not String.IsNullOrEmpty(payee.BankAccount.IBAN) Then Return True
        If Not String.IsNullOrEmpty(payee.BankAccount.SwiftNumber) Then Return True
        Return False
    End Function

    Sub repeaterResults_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        Dim bCanRequestPayment As Boolean = CanRequestPayment(e.Item.DataItem)
        e.Item.FindControl("trBeneficiaryError").Visible = Not bCanRequestPayment
        Dim btnPay As Button = CType(e.Item.FindControl("btnPay"), Button)
        If Not bCanRequestPayment Then
            btnPay.Enabled = False
            btnPay.CssClass = "btn btn-default"
        End If
    End Sub

    Function PaymentButtonClickJS(ByVal payee As Bll.Accounts.Payee) As String
        Dim sbJS As New StringBuilder
        If CanRequestPayment(payee) Then
            sbJS.AppendFormat("PayBeneficiary({0});", payee.ID)
        Else
            sbJS.AppendFormat("if (confirm(""{1}"")) EditBeneficiary({0});", payee.ID, GetLocalResourceObject("MissingBankDetails"))
        End If
        sbJS.Append("return false;")
        Return sbJS.ToString
    End Function
End Class
