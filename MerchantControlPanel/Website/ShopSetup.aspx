<%@ Page Title="PageTitle" Language="vb" AutoEventWireup="false" MasterPageFile="~/Templates/Tmp_netpayintl/page.master"
    MaintainScrollPositionOnPostback="true" CodeBehind="ShopSetup.aspx.vb" Inherits="Netpay.MerchantControlPanel.ShopSetup" %>

<%@ Register Src="~/Website/Controls/ImageUpload.ascx" TagName="ImageUpload" TagPrefix="UC" %>
<%@ Register Src="~/Website/Controls/ShopsWizard.ascx" TagPrefix="UC" TagName="ShopsWizard" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:  SetupPageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function onImageLoaded()
        {
            $("#<%= divPromotionUrl.ClientID%>").css("display", "block");
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBody" runat="server">
    <div class="area-content">
        <asp:Label ID="lblError" CssClass="error" Style="display: block;" runat="server" Visible="false" />
        <asp:Label ID="lblSuccess" CssClass="noerror" Style="display: block;" runat="server" Visible="false" />
        <div class="top-nav">
            <asp:Localize ID="Locallize" Text="<%$Resources:  SetupPageTitle%>" runat="server" />
        </div>
        <div class="content-background">
            <UC:ShopsWizard runat="server" id="ShopsWizard" />
            <div class="section">
                <asp:PlaceHolder runat="server" ID="phShopNonActive" Visible="false">
                    <div id="dvNotActive" class="alert error percent85 align-left">
                        <i class="fa fa-warning"></i>
                        <asp:Literal Text="<%$Resources:  ShopNonActive%>" runat="server" />
                    </div>
                    <div class="align-right padding-5-top">
                        <asp:LinkButton runat="server" ID="btnEnableShop" Font-Size="17" ForeColor="Gray" ClientIDMode="Static" OnClick="EnableShop"><i class="fa fa-toggle-off fa-2x"></i></asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder runat="server" ID="phShopActive">
                    <div id="dvActive" class="alert noerror align-left percent85">
                        <i class="fa fa-check-circle"></i>
                        <asp:Literal Text="<%$Resources:  ShopActive%>" runat="server" />
                    </div>
                    <div class="align-right padding-5-top">
                        <asp:LinkButton runat="server" ID="btnDisableShop" Font-Size="17" ForeColor="#1D9E74" ClientIDMode="Static" OnClick="DisableShop"><i class="fa fa-toggle-on fa-2x"></i></asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
                <div class="spacer"></div>
            </div>
            <div class="section">
                <div class="margin-bottom-10 frame-border-option">             

                    <!-- Shop Domain Name //Comment -->
                    <div class="frame-border-option-inline padding-10 margin-bottom-10" style="display: none;">
                        <div class="form-group">
                            <span class="form-group-title">
                                <asp:Literal Text="<%$Resources: MerchantShopStr%>" runat="server" /></span>
                            <span class="form-group-title-eg">
                                <asp:Literal ID="MerchantShopEgStr" runat="server" /></span>
                            <div class="line-height-35" style="direction: ltr;">
                              <asp:Literal ID="ltPathInfo" runat="server" /> / <asp:TextBox runat="server" ID="txtSubDomainName" CssClass="percent15" MaxLength="20" Text='<%# CurrentShop.SubDomainName %>' />
                               <%-- 
                                <asp:RegularExpressionValidator  ID="validatorSubDomainName" CssClass="alert error" runat="server" Display="Dynamic" ValidationGroup="subdomainValid" ControlToValidate="txtSubDomainName" ValidationExpression="^[a-z]+$" ErrorMessage="<%$Resources:  RequiredExpresssionMerchantDomain%>" />
                                <asp:CustomValidator ID="validatorUsedName" CssClass="alert error" Display="Dynamic" ErrorMessage="<%$Resources:  SubDomainNameAlert%>" ValidationGroup="subdomainValid" SetFocusOnError="true" runat="server" />
                               --%>
                            </div>
                        </div>
                        <div class="line-height-35">
                        </div>
                        <div class="alert alert-warning margin-top-10">
                            <asp:Literal Text="<%$Resources:  AlertDomainStr%>" runat="server" />
                        </div>
                    </div>
                    <!-- End Shop Domain Name -->
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">
                        <div class="form-group align-left percent33">
                            <span class="form-group-title">
                              <i class="fa fa-money"></i>  <asp:Literal Text="<%$Resources: Currency%>" runat="server" /></span>
                            <span class="form-group-title-eg">
                                <asp:Literal Text="<%$Resources: CurrencyStr%>" runat="server" /></span>
                            <div class="line-height-35">
                                <netpay:CurrencyDropDown ID="ddCurrency" CssClass="percent100" EnableBlankSelection="false" runat="server"></netpay:CurrencyDropDown>
                            </div>
                        </div>
                        <div class="spacer"></div>
                    </div>
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">
                        <div class="alert alert-info margin-top-10">
                            <asp:Literal Text="<%$Resources:  ShopAdjustmentColor%>" runat="server" />
                        </div>
                        <netpay:ColorPicker runat="server" HideTextBox="false" CssClass="margin-top-10" ID="txtUIBaseColor" Text='<%# CurrentShop.UIBaseColor %>' />
                    </div>
                    
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">
                        
                        <!-- Alert -->
                        <div class="alert alert-info">
                            <asp:Literal ID="Literal1" Text="<%$Resources:  ValidFileUpload%>" runat="server" />
                        </div>
                        <!-- Banner image -->
                        <div class="margin-top-10">
                            <UC:ImageUpload runat="server" ID="iuImage" Width="620px" Height="100px" BytesSizeLimit="2048000" ImageUrl='<%# Account.MapPublicVirtualPath(CurrentShop.BannerFileName)%>' OnImageDelete="OnImageDeleteClick" />
                        </div>
                        <div class="spacer"></div>
                        <!-- Promotion Url -->
                        <div id="divPromotionUrl" runat="server" class="margin-top-10">
                            <div class="form-group ">
                                <span class="form-group-title">
                                    <asp:Literal ID="Literal66" Text="<%$Resources:  Link%>" runat="server" /></span>
                                <span class="form-group-title-eg">http://dev.shops.netpay-intl.com</span>
                                <div>
                                    <input type="text" id="txtPromotionUrl" maxlength="100" class="percent85 lnk-input" runat="server" />&nbsp;
                        <input type="submit" id="btnUpdatePromotionUrl" class="btn btn-default" onserverclick="btnUpdatePromotionUrl_Click" value="<%$Resources: MultiLang, Update%>" runat="server" />
                                </div>
                                <div class="alert alert-info margin-top-10">
                                    <asp:Literal ID="Literal6" Text="<%$Resources:  bannerAlert%>" runat="server" />

                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">
                         <div class="alert-info margin-bottom-10"><asp:Literal Text="<%$Resources:  installmetStepsTitle %>" runat="server" /> 
                             <br />
                             <asp:Literal Text="<%$Resources: installmetStepsTitleAttention %>" runat="server" /> 
                         </div>
                        <netpay:DynamicRepeater ID="repeaterInstallments" runat="server">
                            <HeaderTemplate>
                                <table class="cart-steps margin-bottom-10">
                                    <thead>
                                        <tr> 
                                            <th>
                                                <asp:Literal ID="Literal4" Text="<%$Resources:  FromAmount %>" runat="server" /></th>
                                            <th>
                                                <asp:Literal ID="Literal2" Text="<%$Resources:  ToAmount %>" runat="server" /></th>
                                            <th>
                                                <asp:Literal ID="Literal3" Text="<%$Resources:  installments %>" runat="server" /></th>
                                            <th>
                                                <asp:Literal ID="Literal5" Text="<%$Resources:  remove %>" runat="server" /></th>
                                        </tr>
                                        <tbody>
                                    </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:Literal ID="literalInstFromAmount" Text='<%# Eval("CalculatedFromAmount")%>' runat="server" />
                                    </td>
                                    <td>
                                        <asp:Literal ID="literalInstToAmount" Text='<%# Eval("ToAmount")%>' runat="server" />
                                    </td>
                                    <td>
                                        <asp:Literal ID="literalMaxInst" Text='<%# Eval("MaxInstallments")%>' runat="server" /></td>
                                    <td>
                                        <asp:LinkButton ID="btnDeleteInst" CssClass="btn btn-danger" Text="<%$Resources:  remove%>" OnCommand="btnDeleteInst_Command" CommandName="delete" CommandArgument='<%# Container.ItemIndex %>' runat="server"></asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                            </table>
                            </FooterTemplate>
                        </netpay:DynamicRepeater>

                        <div class="form-group align-left percent20">
                            <asp:Literal Text="<%$Resources:  FromAmount%>" runat="server" />
                            <div>
                                <asp:TextBox ID="txtAddInstFromAmount" Enabled="false" CssClass="percent100" runat="server" />
                            </div>
                        </div>
                        <div class="form-group align-left percent20">
                            <asp:Literal Text="<%$Resources:  ToAmount%>" runat="server" />
                            <div>
                                <asp:TextBox ID="txtAddInstToAmount" CssClass="percent100" runat="server" />
                            </div>
                        </div>
                        <div class="form-group align-left percent20">
                            <asp:Literal Text="<%$Resources:  maxInstallments%>" runat="server" />
                            <div>
                                <asp:DropDownList ID="ddAddInst" CssClass="percent100" runat="server" />
                            </div>
                        </div>
                        <div class="form-group align-left percent20">
                            <br />
                            <asp:LinkButton ID="btnAddInst" Text="<%$Resources:  AddStep %>" CssClass="btn btn-primary" OnClick="btnAddInst_Click" runat="server" />
                        </div>
                        <div class="spacer"></div>
                        <div id="divInstAlert" class="error margin-top-10" runat="server">
                            <asp:Literal Text="<%$Resources:  instAlert%>" runat="server" />
                        </div>

                    </div>
             
                <div class="frame-border-option-inline padding-10 margin-bottom-10">
                    <div class="alert-info margin-bottom-10">   <asp:Literal Text="<%$Resources:  SocialMedia%>" runat="server" /></div>
                    <div>
                        
                         <div class="form-group-title">   <i class="fa fa-facebook-square"></i>
                            <asp:Literal Text="<%$Resources:  URLFacebook%>" runat="server" /></div>
                       
                        <div class="margin-bottom-10">
                            <asp:TextBox runat="server" ID="txtFacebookUrl" Text='<%# CurrentShop.FacebookUrl %>' CssClass="percent100" />
                        </div>
                    </div>
                    <div>
                       
                             <div class="form-group-title">  
                            <i class="fa fa-google-plus-square"></i>
                            <asp:Literal Text="<%$Resources:  URLGoogle%>" runat="server" /></div>
                        
                        <div class="margin-bottom-10">
                            <asp:TextBox runat="server" ID="txtGooglePlusUrl" Text='<%# CurrentShop.GooglePlusUrl %>' CssClass="percent100" />
                        </div>
                    </div>
                    <div>
                         <div class="form-group-title">  
                            <i class="fa fa-twitter-square"></i>
                            <asp:Literal Text="<%$Resources:  URLTwitter%>" runat="server" />
                        </div>
                        <div class="margin-bottom-10">
                            <asp:TextBox runat="server" ID="txtTwitterUrl" Text='<%# CurrentShop.TwitterUrl %>' CssClass="percent100" />
                        </div>
                    </div>
                    <div>
                         <div class="form-group-title">  
                            <i class="fa fa-linkedin-square"></i>
                            <asp:Literal Text="<%$Resources:  URLLinkedin%>" runat="server" />
                        </div>
                        <div class="margin-bottom-10">
                            <asp:TextBox runat="server" ID="txtLinkedinUrl" Text='<%# CurrentShop.LinkedinUrl %>' CssClass="percent100" />
                        </div>
                    </div>
                    <div>
                         <div class="form-group-title">  
                            <i class="fa fa-pinterest-square"></i>
                            <asp:Literal Text="<%$Resources:  URLPinterest%>" runat="server" />
                        </div>
                        <div class="margin-bottom-10">
                            <asp:TextBox runat="server" ID="txtPinterestUrl" Text='<%# CurrentShop.PinterestUrl %>' CssClass="percent100" />
                        </div>
                    </div>
                    <div>
                       <div class="form-group-title">  
                            <i class="fa fa-youtube-square"></i>
                            <asp:Literal Text="<%$Resources:  URLYoutube%>" runat="server" />
                        </div>
                        <div class="margin-bottom-10">
                            <asp:TextBox runat="server" ID="txtYoutubeUrl" Text='<%# CurrentShop.YoutubeUrl %>' CssClass="percent100" />
                        </div>
                    </div>
                    <div>
                        <div class="form-group-title">  
                            <i class="fa fa-vimeo-square"></i>
                            <asp:Literal Text="<%$Resources:  URLVimeo%>" runat="server" />
                        </div>
                        <div class="margin-bottom-10">
                            <asp:TextBox runat="server" ID="txtVimeoUrl" Text='<%# CurrentShop.VimeoUrl %>' CssClass="percent100" />
                        </div>
                    </div>

                </div>
                    <asp:ValidationSummary CssClass="error" DisplayMode="SingleParagraph" ValidationGroup="subdomainValid" runat="server" />
                   
                    <div class="align-right">
                       <asp:LinkButton UseSubmitBehavior="true" ID="btnUpdate" Text="<%$Resources:  NextBtn %>" CssClass="btn btn-cons-short btn-success" ValidationGroup="subdomainValid" runat="server" OnClick="btnUpdate_Click" /></div>

                    <div class="spacer"></div>

            </div>
        </div>
            <asp:PlaceHolder ID="phShopSettings" runat="server"></asp:PlaceHolder>
        </div>
   </div>
</asp:Content>
