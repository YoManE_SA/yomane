﻿Imports Netpay.Web
Imports Netpay.Infrastructure
Imports Netpay.Bll

Partial Class Website_ProcessRequestLog
	Inherits MasteredPage

	Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
		Pager.CurrentPage = 0
		OnPageChanged(sender, e)
	End Sub

 	Protected Sub OnPageChanged(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim filters As New Bll.Process.ChargeAttemptLog.SearchFilters()
		filters.Date.From = wcDateRangePicker.FromDate
		filters.Date.To = wcDateRangePicker.ToDate
        filters.ReplyCode = txtReplyCode.Text.NullIfEmpty()
        filters.TransactionId = txtTransNum.Text.ToNullableInt()

        repeaterResults.DataSource = Bll.Process.ChargeAttemptLog.Search(filters, pager.Info)
		repeaterResults.DataBind()

        divDataSection.Visible = If(repeaterResults.Items.Count > 0, True, False)
        divDataWrapperSection.Visible = True

        If Not Page.IsPostBack Then
			phbHelp.Text = GetLocalResourceObject("HelpBox").ToString.Replace("%BRAND_NAME%", WebUtils.CurrentDomain.BrandName)
		End If
	End Sub

	Protected Function GetStatusStyle(entity As Bll.Process.ChargeAttemptLog) As String
		If entity.ReplyCode = "000" Then
			Return "transactionStatusCaptured"
		End If

		Return "transactionStatusDeclined"
	End Function

	Protected Function GetTranactionSource(transactionSourceID As Integer) As String
        Return Bll.Process.TransactionSource.Get(transactionSourceID).Name
	End Function
End Class
