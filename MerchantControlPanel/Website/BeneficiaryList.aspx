﻿<%@ Page Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_BeneficiaryList" CodeBehind="BeneficiaryList.aspx.vb" Title="PageTitle" %>

<%@ Import Namespace="Netpay.MerchantControlPanel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <script language="javascript" type="text/javascript">
        function EditBeneficiary(nID, nProfileType) {
            location.href = "BeneficiaryAddUpdate.aspx?CompanyMakePaymentsProfiles_id=" + nID + "&ProfileType=" + nProfileType;
        }

        function PayBeneficiary(nID, nProfileType) {
            location.href = "BeneficiaryMakePayment.aspx?ProfileType=" + nProfileType + (nID == 0 ? "" : "&CompanyMakePaymentsProfiles_id=" + nID);
        }
    </script>
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="locheadPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">
            <div class="section">
                <div class="margin-bottom-10 frame-border-option">
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">
                        <div class="border-bottom">
                            <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn btn-success btn-cons-short" OnClick="btnAdd_OnClick"><i class="fa fa-plus-circle"></i> <asp:Literal Text="<%$Resources:AddNewBeneficiary %>" runat="server" /></asp:LinkButton>
                        </div>

                        <table class="exspand-table" style="padding-top: 10px;">
                            <tr>
                                <th></th>
                                <th>
                                    <asp:Literal Text="<%$Resources:MultiLang,No %>" runat="server" />
                                </th>
                                <th>
                                    <asp:Literal Text="<%$Resources:MultiLang,CompanyPersonName %>" runat="server" />
                                </th>
                                <th>
                                    <asp:Literal Text="<%$Resources:MultiLang,Identifier %>" runat="server" />
                                </th>
                                <th>
                                    <asp:Literal Text="<%$Resources:MultiLang,ProfileType %>" runat="server" />
                                </th>
                                <th></th>
                                <th>
                                    <asp:Literal Text="<%$Resources:MultiLang,Payments %>" runat="server" />
                                </th>
                            </tr>
                            <tr>
                                <td style="background-color: #e9e9e9;">
                                    <asp:Literal runat="server" ID="litOwnBeneficiary" />
                                </td>
                                <td style="background-color: #e9e9e9;">000
                                </td>
                                <td style="background-color: #e9e9e9;">
                                    <%=dbPages.dbtextShow(LoggedUser.UserName).ToString%>
                                </td>
                                <td style="background-color: #e9e9e9;">
                                    <%= Account.AccountNumber %>
                                </td>
                                <td style="background-color: #e9e9e9;">
                                    <asp:Literal ID="Literal6" Text='<%$Resources:MerchantProfile %>' runat="server" />
                                </td>
                                <td style="background-color: #e9e9e9;"></td>
                                <td style="background-color: #e9e9e9; width: 25px;">
                                    <asp:Button Text='<%$Resources:MultiLang,Pay %>' OnClientClick="PayBeneficiary(0, 0);return false;" runat="server" CssClass="btn btn-success btn-cons-short" />
                                </td>
                            </tr>
                            <asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server" OnItemDataBound="repeaterResults_ItemDataBound">
                                <ItemTemplate>
                                    <tr onmouseover="this.oldBgColor=this.style.backgroundColor;this.style.backgroundColor='#d8d8d8';"
                                        onmouseout="this.style.backgroundColor=this.oldBgColor;" style="background-color: <%# GetRowBackground(CType(Container.DataItem, Netpay.Bll.Merchants.Beneficiary))%>">
                                        <td>
                                            <%# GetEditButton(CType(Container.DataItem, Netpay.Bll.Merchants.Beneficiary), False)%>
                                        </td>
                                        <td>
                                            <%# CType(Container.DataItem, Netpay.Bll.Merchants.Beneficiary).ID %>
                                        </td>
                                        <td>
                                            <%# CType(Container.DataItem, Netpay.Bll.Merchants.Beneficiary).BasicInfoCostumerName %>
                                        </td>
                                        <td>
                                            <%# CType(Container.DataItem, Netpay.Bll.Merchants.Beneficiary).BasicInfoCostumerNumber %>
                                        </td>
                                        <td>
                                            <%# Netpay.Infrastructure.GlobalData.GetText(GlobalDataGroup.BeneficiaryProfileType, WebUtils.CurrentLanguage, CType(Container.DataItem, Netpay.Bll.Merchants.Beneficiary).ProfileType)%>
                                        </td>
                                        <td>
                                            <%# GetEditButton(CType(Container.DataItem, Netpay.Bll.Merchants.Beneficiary), True) %>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnPay" Text='<%$Resources:MultiLang,Pay%>' OnClientClick='<%# PaymentButtonClickJS(Container.DataItem) %>'
                                                runat="server" CssClass="btn btn-success btn-cons-short" />
                                        </td>
                                    </tr>
                                    <tr runat="server" id="trBeneficiaryError">
                                        <td colspan="7" style="background-color: #ffebe1;">
                                            <img alt="alert" src="/NPCommon/images/alert_small.png" align="top" />
                                            <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:BeneficiaryError %>" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
