﻿
Public Class Security_Settings
    Inherits MasteredPage

    Dim recurringSeetings As Netpay.Bll.Transactions.Recurring.MerchantSettings = Nothing

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        recurringSeetings = Netpay.Bll.Transactions.Recurring.MerchantSettings.Load(Merchant.ID)
        If recurringSeetings Is Nothing Then recurringSeetings = New Netpay.Bll.Transactions.Recurring.MerchantSettings(Merchant.ID)

        If Not Page.IsPostBack Then
            chkRecurringForceMD5OnModify.Checked = If(recurringSeetings Is Nothing, False, recurringSeetings.ForceMD5OnModify)
            chkForceCCStorageMD5.Checked = IIf(Merchant.ForceCCStorageMD5.HasValue, Merchant.ForceCCStorageMD5, False)
            SetHashKeyVisibilty(Merchant.HashKey)
        End If
    End Sub

    Protected Sub SetHashKeyVisibilty(ByVal sHashKey As String)
        If String.IsNullOrEmpty(sHashKey) Then
            lblHashKey.Text = GetLocalResourceObject("Personal_Hash_Key_Generate")
            btnHashKeyChange.Visible = False
            'btnHashKeyDestroy.Visible = False
            btnHashKeyCreate.Visible = True
        Else
            lblHashKey.Text = GetLocalResourceObject("Show_Personal_Hash_Key") & " <b>" & sHashKey & "</b>"
            btnHashKeyChange.Visible = True
            'btnHashKeyDestroy.Visible = True
            btnHashKeyCreate.Visible = False
        End If
    End Sub

    Protected Sub NewHashKey(ByVal o As Object, ByVal e As EventArgs) Handles btnHashKeyChange.Click, btnHashKeyCreate.Click
        Dim hashKey As String = Netpay.Infrastructure.Security.Encryption.GetHashKey()
        Merchant.HashKey = hashKey
        Merchant.Save()
        SetHashKeyVisibilty(hashKey)
    End Sub

    Protected Sub btnSave_Save(ByVal o As Object, ByVal e As EventArgs)
        recurringSeetings.ForceMD5OnModify = chkRecurringForceMD5OnModify.Checked
        recurringSeetings.Save()

        Merchant.ForceCCStorageMD5 = chkForceCCStorageMD5.Checked
        Merchant.Save()
    End Sub

End Class