﻿<%@ Page Title="PageTitle" Language="VB" AutoEventWireup="false" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" Inherits="Netpay.MerchantControlPanel.Website_DownloadBanners" CodeBehind="DownloadBanners.aspx.vb" %>

<asp:content id="Content1" contentplaceholderid="cphTitle" runat="Server">
	
			<asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
		
</asp:content>
<asp:content id="Content2" contentplaceholderid="cphBody" runat="Server">
  <div class="area-content">
        <div class="top-nav">		<asp:Localize ID="locPageTitle1" Text="<%$Resources:PageTitle%>" runat="server" />
         </div>
        <div class="content-background">
            <div class="section">
              <table>
              <tr>
              <td><img src="/NPCommon/NPBanners/Netpay_accept_l.gif" alt="" width="195" height="80" border="0"></td>
               <td>	Netpay_accept_l.gif (7.78 Kb)</td>
              <td>	<img src="/NPCommon/NPBanners/Netpay_accept_a.gif" alt="" width="80" height="100" border="0"></td>
              <td>Netpay_accept_a.gif<br/>(5.44 Kb)</td>
              </tr>
              <tr>
              <td><img src="/NPCommon/NPBanners/Netpay_accept_b.gif" alt="" width="95" height="100" border="0"></td>
              <td>Netpay_accept_b.gif<br/>(5.93 Kb)</td>
              <td><img src="/NPCommon/NPBanners/Netpay_accept_c.gif" alt="" width="200" height="42" border="0"></td>
              <td>Netpay_accept_c2.gif (4.75 Kb)</td>
              </tr>
              <tr>
              <td>	<img src="/NPCommon/NPBanners/Netpay_195_55.gif" width="195" height="55" alt="" border="0"></td>
<td>Netpay_195_55.gif (1.22 Kb)</td>
<td><img src="/NPCommon/NPBanners/Netpay_accept_s.gif" alt="" width="158" height="65" border="0"></td>
<td>Netpay_accept_s.gif (5.97 Kb)</td>
</tr>
<tr>
              <td><img src="/NPCommon/NPBanners/netpay_accept_d.gif" alt="" width="195" height="80" border="0"></td>
              <td>netpay_accept_d.gif (13.2 Kb)</td>
              <td> <img src="/NPCommon/NPBanners/netpay_accept_t2.gif" alt="" width="195" height="100" border="0"></td>
    </tr>
    
    </table>
    </div>
    <div class="section">
   <p>
				Place the following banner in the page accepting credit cards
				to be charged via <%= CurrentDomain.BrandName %> terminals.
				
				Use the following sample code:
			
			
								<%           Dim sHost, sMerchantHost As String
                                    If Left(LCase(CurrentDomain.ProcessUrl), 5) = "https" Then
                                        sHost = "https://" & CurrentDomain.Host
                                    Else
                                        sHost = "http://" & CurrentDomain.Host
                                    End If
                                    If LCase(Request("HTTPS")) = "off" Then
                                        sMerchantHost = "http"
                                    Else
                                        sMerchantHost = "https"
                                    End If
                                    sMerchantHost = sMerchantHost & "://" & Request("SERVER_NAME")
								%>
                                </p>

								<textarea cols="35" rows="5" style="background-color: #ebebeb; height: 120px; width: 95%">&lt;a href=&quot;<%= sHost %>/site_eng/rules.asp&quot; target="_blank" title="Click to view <%= CurrentDomain.BrandName %> Rules & Regulations"&gt;&lt;img src=&quot;<%= sMerchantHost %>/NPCommon/NPBanners/netpay_accept_t2.gif&quot; alt="Click to view <%= CurrentDomain.BrandName %> Rules & Regulations" width="195" height="100" border="0"&gt;&lt;/a&gt;</textarea>
             
            
           
            </div>
        </div>
    </div>
</asp:content>
