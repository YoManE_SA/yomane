﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Website_RecurringSeries
    
    '''<summary>
    '''locPageTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents locPageTitle As Global.System.Web.UI.WebControls.Localize
    
    '''<summary>
    '''hdLastSearchMode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdLastSearchMode As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Localize1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Localize1 As Global.System.Web.UI.WebControls.Localize
    
    '''<summary>
    '''PopupButton1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PopupButton1 As Global.Netpay.Web.Controls.PopupButton
    
    '''<summary>
    '''ttLegend control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ttLegend As Global.Netpay.Web.Controls.Legend
    
    '''<summary>
    '''txtSeriesID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSeriesID As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''buttonSearchSingle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents buttonSearchSingle As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''wcDateRangePicker control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents wcDateRangePicker As Global.Netpay.Web.Controls.DateRangePicker
    
    '''<summary>
    '''txtAmount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAmount As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''ddCurrency control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddCurrency As Global.Netpay.Web.Controls.CurrencyDropDown
    
    '''<summary>
    '''ddPaymentMethod control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddPaymentMethod As Global.Netpay.Web.Controls.PaymentMethodDropDown
    
    '''<summary>
    '''ddStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddStatus As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''buttonSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents buttonSearch As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''phResults control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents phResults As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''wcFiltersView control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents wcFiltersView As Global.Netpay.Web.Controls.SearchFiltersView
    
    '''<summary>
    '''Literal14 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal14 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''repeaterResults control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents repeaterResults As Global.System.Web.UI.WebControls.Repeater
    
    '''<summary>
    '''btnExportExcel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnExportExcel As Global.Netpay.Web.Controls.ExcelButton
    
    '''<summary>
    '''pager control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pager As Global.Netpay.Web.Controls.LinkPager
End Class
