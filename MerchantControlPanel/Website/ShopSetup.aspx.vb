Imports Netpay.Infrastructure
Imports Netpay.Web
Imports System.IO
Imports Netpay.Bll
Imports Netpay.Bll.Shop

Public Class ShopSetup
    Inherits MasteredPage

    Private _shop As RegionShop
    Protected ReadOnly Property CurrentShop As RegionShop
        Get
            If _shop Is Nothing Then
                Dim id As Integer
                Dim result As Boolean = Integer.TryParse(Request("shopId"), id)
                If result Then
                    _shop = RegionShop.Load(id)
                End If
                If _shop Is Nothing Then Response.Redirect("ShopSelection.aspx", True)
            End If
            Return _shop
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not System.IO.Directory.Exists(Account.MapPublicPath() + "UIServices") Then _
            System.IO.Directory.CreateDirectory(Account.MapPublicPath() + "UIServices")

        If Not IsPostBack Then
            UpdateShopStatus(CurrentShop.IsShopEnabled)

            DataBindChildren()
            txtPromotionUrl.Value = CurrentShop.BannerLinkUrl
            SetPromoUrlVisibility(Not String.IsNullOrEmpty(CurrentShop.BannerFileName))

            'currency
            ddCurrency.SetCurrencyIDs(Bll.Merchants.Merchant.GetSupportedCurrencies(Merchant.ID).Select(Function(c) c.ID).ToArray())
            ddCurrency.SelectedCurrencyIso = CurrentShop.CurrencyIsoCode
            ddCurrency.DataBind()

            ' installments
            ddAddInst.Items.Clear()
            For idx As Byte = 1 To 12
                ddAddInst.Items.Add(idx)
            Next
            repeaterInstallments.DataSource = CurrentShop.InstallmentSteps
            repeaterInstallments.DataBind()
            divInstAlert.Visible = False
            If CurrentShop.InstallmentSteps.Count > 0 Then
                txtAddInstFromAmount.Text = CurrentShop.InstallmentSteps.Last.ToAmount + 1
            Else
                txtAddInstFromAmount.Text = 0
            End If

            ' shop subdomain name
            Dim domain As String = Request.Url.Authority
            Dim split As String() = domain.Split(".")
            If split.Count > 2 Then
                domain = split(1) + "." + split(2)
            End If
            Dim shopUrl = "shop." + domain
            MerchantShopEgStr.Text = "(" + shopUrl + "/myshop)"
            ltPathInfo.Text = shopUrl
        End If
    End Sub

    Protected Sub UpdateShopStatus(isEnabled As Boolean)
        phShopActive.Visible = isEnabled
        phShopNonActive.Visible = Not isEnabled
    End Sub


    Protected Sub EnableShop(sender As Object, args As EventArgs)
        CurrentShop.IsShopEnabled = True
        CurrentShop.Save()
        UpdateShopStatus(CurrentShop.IsShopEnabled)
    End Sub

    Protected Sub DisableShop(sender As Object, args As EventArgs)
        CurrentShop.IsShopEnabled = False
        CurrentShop.Save()
        UpdateShopStatus(CurrentShop.IsShopEnabled)
    End Sub

    Protected Sub OnImageDeleteClick(sender As Object, e As EventArgs)
        SetPromoUrlVisibility(False)
    End Sub

    Protected Sub SetPromoUrlVisibility(isVisibale As Boolean)
        divPromotionUrl.Style.Clear()
        If isVisibale Then
            divPromotionUrl.Style.Add("display", "block")
        Else
            divPromotionUrl.Style.Add("display", "none")
        End If
    End Sub

    Protected Sub btnUpdatePromotionUrl_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim options = Bll.Shop.MerchantSettings.Load(Merchant.ID)
        options.BannerLinkUrl = txtPromotionUrl.Value.NullIfEmpty()
        options.Save()
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ' If Not validatorSubDomainName.IsValid Then
        'Exit Sub
        ' End If

        'If (CurrentShop.IsNameUsed(txtSubDomainName.Text)) Then
        ' validatorUsedName.IsValid = False
        'Exit Sub
        'End If

        CurrentShop.SubDomainName = txtSubDomainName.Text
        CurrentShop.UIBaseColor = txtUIBaseColor.Text
        CurrentShop.FacebookUrl = txtFacebookUrl.Text
        CurrentShop.GooglePlusUrl = txtGooglePlusUrl.Text
        CurrentShop.TwitterUrl = txtTwitterUrl.Text
        CurrentShop.LinkedinUrl = txtLinkedinUrl.Text
        CurrentShop.PinterestUrl = txtPinterestUrl.Text
        CurrentShop.YoutubeUrl = txtYoutubeUrl.Text
        CurrentShop.VimeoUrl = txtVimeoUrl.Text
        CurrentShop.CurrencyIsoCode = ddCurrency.SelectedCurrencyIso

        CurrentShop.InstallmentSteps = New List(Of Shop.RegionShop.InstallmentStep)
        For Each item As RepeaterItem In repeaterInstallments.Items
            Dim toAmountLiteral As Literal = item.FindControl("literalInstToAmount")
            Dim maxInstLiteral As Literal = item.FindControl("literalMaxInst")
            Dim toAmount As Decimal = toAmountLiteral.Text
            Dim maxInst As Byte = maxInstLiteral.Text
            Dim instStep As Netpay.Bll.Shop.RegionShop.InstallmentStep = New Shop.RegionShop.InstallmentStep(0, toAmount, maxInst)
            CurrentShop.InstallmentSteps.Add(instStep)
        Next

        If iuImage.HasFile Then
            Dim imageFileName = "UIServices/PP_Banner_" & CurrentShop.ShopId & ".png"
            If iuImage.SaveAs(Account.MapPublicPath(imageFileName)) Then CurrentShop.BannerFileName = imageFileName
        Else
            Dim filePhisicalPath As String = Account.MapPublicPath() + CurrentShop.BannerFileName
            If File.Exists(filePhisicalPath) Then File.Delete(filePhisicalPath)
            CurrentShop.BannerFileName = Nothing
        End If

        CurrentShop.Save()
        iuImage.DataBind()

        Response.Redirect("ShopProducts.aspx?shopId=" & CurrentShop.ShopId)
    End Sub

    Private Sub ReorderInstallments()
        If repeaterInstallments.Items.Count = 0 Then
            txtAddInstFromAmount.Text = 0
            Exit Sub
        End If

        For idx As Integer = 0 To repeaterInstallments.Items.Count - 1
            Dim item As RepeaterItem = repeaterInstallments.Items(idx)
            Dim fromAmount As Literal = item.FindControl("literalInstFromAmount")
            If idx = 0 Then
                fromAmount.Text = 0
            Else
                Dim prevToAmountLiteral As Literal = repeaterInstallments.Items(idx - 1).FindControl("literalInstToAmount")
                Dim prevToAmount As Decimal = prevToAmountLiteral.Text
                fromAmount.Text = prevToAmount + 1
            End If
        Next

        Dim lastToAmountLiteral As Literal = repeaterInstallments.Items(repeaterInstallments.Items.Count - 1).FindControl("literalInstToAmount")
        Dim lastToAmount As Decimal = lastToAmountLiteral.Text
        txtAddInstFromAmount.Text = lastToAmount + 1
    End Sub

    Protected Sub btnAddInst_Click(sender As Object, e As EventArgs)
        iuImage.DataBind()

        Dim fromAmount As Decimal
        Dim parseResult As Boolean
        parseResult = Decimal.TryParse(txtAddInstFromAmount.Text, fromAmount)
        divInstAlert.Visible = Not parseResult
        If Not parseResult Then Exit Sub

        Dim toAmount As Decimal
        parseResult = Decimal.TryParse(txtAddInstToAmount.Text, toAmount)
        divInstAlert.Visible = Not parseResult
        If Not parseResult Then Exit Sub

        If fromAmount >= toAmount Then
            divInstAlert.Visible = True
            Exit Sub
        End If

        Dim maxInst As Byte = ddAddInst.SelectedValue
        If repeaterInstallments.Items.Count > 0 Then
            Dim lastMaxInstLiteral As Literal = repeaterInstallments.Items(repeaterInstallments.Items.Count - 1).FindControl("literalMaxInst")
            Dim lastMaxInst As Byte = lastMaxInstLiteral.Text
            If maxInst <= lastMaxInst Then
                divInstAlert.Visible = True
                Exit Sub
            End If
        End If

        divInstAlert.Visible = False
        Dim instStep As Shop.RegionShop.InstallmentStep = New Shop.RegionShop.InstallmentStep(fromAmount, toAmount, maxInst)
        repeaterInstallments.AddItem(instStep)

        txtAddInstToAmount.Text = ""
        ReorderInstallments()
    End Sub

    Protected Sub btnDeleteInst_Command(sender As Object, e As CommandEventArgs)
        Dim itemIndex As Integer = e.CommandArgument
        repeaterInstallments.RemoveItem(itemIndex)

        ReorderInstallments()
    End Sub
End Class