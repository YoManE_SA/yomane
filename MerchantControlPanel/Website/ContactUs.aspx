﻿<%@ Page Title="" Language="vb" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_ContactUs" CodeBehind="ContactUs.aspx.vb" %>

<%@ Register Src="~/Common/Captcha.ascx" TagPrefix="UC" TagName="Captcha" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:MultiLang,ContactUs%>" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$Resources:MultiLang, ContactUs%>" runat="server" />
        </div>
        <div class="content-background">

            <div class="section">


                <div class="margin-bottom-10 frame-border-option">
                    <div class="margin-bottom-10">
                       <i class="fa fa-user"></i>  <asp:Literal Text="<%$Resources: ContactUsDetails %>" runat="server" /></div>
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">
                        <asp:PlaceHolder ID="phContactus" runat="server">
                            <div class="contact-us-box">
                                <h3><i class="fa fa-users"></i>
                                    <asp:Literal Text="<%$Resources: CustomerService %>" runat="server" /></h3>
                                <p>
                                    <asp:PlaceHolder Visible='<%# Not String.IsNullOrEmpty(WebUtils.CurrentDomain.CustomerServiceSkype) %>' runat="server">
                                        <a href="skype:<%# WebUtils.CurrentDomain.CustomerServiceSkype %>?chat"><i class="fa fa-skype" style="color: #2CC7F5;"></i>Skype</a><br />
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder Visible='<%# Not String.IsNullOrEmpty(WebUtils.CurrentDomain.CustomerServicePhone) %>' runat="server">
                                        <asp:Localize Text="<%$Resources:MultiLang,Phone%>" runat="server" />: <span class="orange-color"><%# WebUtils.CurrentDomain.CustomerServicePhone %></span><br />
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder Visible='<%# Not String.IsNullOrEmpty(WebUtils.CurrentDomain.CustomerServiceFax) %>' runat="server">
                                        <asp:Localize Text="<%$Resources:MultiLang,Fax%>" runat="server" />: <span class="orange-color"><%# WebUtils.CurrentDomain.CustomerServiceFax %></span><br />
                                    </asp:PlaceHolder>
                                    <%--<asp:PlaceHolder Visible='<%# Not String.IsNullOrEmpty(WebUtils.CurrentDomain.CustomerServiceEmail) %>' runat="server">
                                        <asp:Literal Text="<%$Resources:MultiLang,Email%>" runat="server" />: <a href="mailto:<%# WebUtils.CurrentDomain.CustomerServiceEmail %>" class="orange-color"><%# WebUtils.CurrentDomain.CustomerServiceEmail %></a>
                                    </asp:PlaceHolder>--%>
                                </p>
                            </div>
                            <div class="contact-us-box margin-left-10">
                                <h3><i class="fa fa-cogs"></i>
                                    <asp:Literal Text="<%$Resources: TechnicalSupport %>" runat="server" /></h3>
                                <p>
                                    <asp:PlaceHolder Visible='<%# Not String.IsNullOrEmpty(WebUtils.CurrentDomain.TechnicalSupportSkype) %>' runat="server">
                                        <a href="skype:<%# WebUtils.CurrentDomain.TechnicalSupportSkype %>?chat"><i class="fa fa-skype" style="color: #2CC7F5;"></i>Skype</a>
                                        <br />
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder Visible='<%# Not String.IsNullOrEmpty(WebUtils.CurrentDomain.TechnicalSupportPhone) %>' runat="server">
                                        <asp:Localize Text="<%$Resources:MultiLang,Phone%>" runat="server" />: <span class="orange-color"><%# WebUtils.CurrentDomain.TechnicalSupportPhone %></span><br />
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder Visible='<%# Not String.IsNullOrEmpty(WebUtils.CurrentDomain.TechnicalSupportFax) %>' runat="server">
                                        <asp:Localize Text="<%$Resources:MultiLang,Fax%>" runat="server" />: <span class="orange-color"><%# WebUtils.CurrentDomain.TechnicalSupportFax %></span><br />
                                    </asp:PlaceHolder>
                                    <%--<asp:PlaceHolder Visible='<%# Not String.IsNullOrEmpty(WebUtils.CurrentDomain.TechnicalSupportEmail) %>' runat="server">
                                        <asp:Literal Text="<%$Resources:MultiLang,Email%>" runat="server" />: <a href="mailto:<%# WebUtils.CurrentDomain.TechnicalSupportEmail %>" class="orange-color"><%# WebUtils.CurrentDomain.TechnicalSupportEmail %></a>
                                    </asp:PlaceHolder>--%>
                                </p>
                            </div>
                            <div class="spacer"></div>
                        </asp:PlaceHolder>
                    </div>
                    <div class="margin-bottom-10">
                        <i class="fa fa-edit"></i> <asp:Literal Text="<%$Resources:ContactUsForm %>" runat="server" /></div>
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">

                        <div class="form-group align-left percent32">
                            <asp:Localize runat="server" Text="<%$Resources:FullName%>" /><%=Validation.Mandatory%>
                            <div>
                                <div class="margin-bottom-10">
                                    <asp:TextBox ID="txtFullName" runat="server" CssClass="percent100" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group align-left percent32">
                            <asp:Localize runat="server" Text="<%$Resources:EmailAddress%>" /><%=Validation.Mandatory%>
                            <div>
                                <div class="margin-bottom-10">
                                    <asp:TextBox ID="txtEmail" runat="server" CssClass="percent100" />
                                </div>
                            </div>
                        </div>

                        <div class="form-group align-left percent32">
                            <asp:Localize ID="Localize12" runat="server" Text="<%$Resources:Phone%>" /><%=Validation.Mandatory%>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtPhone" runat="server" CssClass="percent100" />
                            </div>
                        </div>
                        <div class="form-group align-left percent32">
                            <asp:Localize ID="Localize10" runat="server" Text="<%$Resources:Country%>" /><%=Validation.Mandatory%>
                            <div class="margin-bottom-10">
                                <netpay:CountryDropDown ID="ddlCountry" CssClass="percent100" runat="server">
                                </netpay:CountryDropDown>
                            </div>
                        </div>
                        <div class="spacer"></div>
                        <div class="form-group">
                            <asp:Localize ID="Localize13" runat="server" Text="<%$Resources:Subject%>" /><%=Validation.Mandatory%>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtSubject" CssClass="percent100" runat="server" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Localize runat="server" Text="<%$Resources:Message%>" /><%=Validation.Mandatory%>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtMessage" TextMode="MultiLine" runat="server" CssClass="percent100 percent-height-10" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Localize runat="server" Text="<%$ Resources:MultiLang,CaptchaDescription %>" />
                            <div class="margin-bottom-10">
                                <asp:Localize ID="Localize15" runat="server" Text="<%$Resources:SecurityCode %>" /><%=Validation.Mandatory%>
                                <asp:TextBox ID="txtCaptcha" runat="server" ValidationGroup="page" CssClass="Field_120" Style="vertical-align: middle;" />
                                <UC:Captcha ID="Captcha3" runat="server" />
                            </div>
                        </div>

                    </div>
                    <div class="wrap-button">
                        <asp:LinkButton ID="btnContactSend" UseSubmitBehavior="false" CssClass="btn btn-success btn-cons-short" BorderWidth="0" OnClick="btnContactSend_Click" runat="server"><i class="fa fa-envelope"></i> <asp:Literal runat="server" Text="<%$Resources:MultiLang,ButtonSend %>" /> </asp:LinkButton>
                    </div>
                </div>
            </div>

            <div>
                <asp:Label ID="lblResult" runat="server" />
            </div>
        </div>

    </div>
</asp:Content>
