﻿Imports Netpay.Web
Imports Netpay.Infrastructure

Public Class DevDocs
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then DataBindChildren()
    End Sub
    Protected Overrides Sub OnPreRender(e As System.EventArgs)

        Dim link As New HtmlControls.HtmlLink()
        link.Href = WebUtils.MapTemplateVirPath(Me, "Website/Styles/LoginStyleSheet.css")
        'link.Href = "style/LoginStyleSheet.css"
        link.Attributes.Add("rel", "stylesheet")
        link.Attributes.Add("type", "text/css")
        link.Attributes.Add("media", "screen")
        Page.Header.Controls.Add(link)
        If System.Threading.Thread.CurrentThread.CurrentUICulture.TextInfo.IsRightToLeft Then
            link = New HtmlControls.HtmlLink()
            link.Href = WebUtils.MapTemplateVirPath(Me, "Website/Styles/LoginStyleSheetRTL.css")
            'link.Href = "style/LoginStyleSheetRTL.css"
            link.Attributes.Add("rel", "stylesheet")
            link.Attributes.Add("type", "text/css")
            link.Attributes.Add("media", "screen")
            Page.Header.Controls.Add(link)
        End If
        MyBase.OnPreRender(e)

    End Sub

End Class