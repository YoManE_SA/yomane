﻿Imports System.Data.SqlClient
Imports Netpay.Web
Imports Netpay.Infrastructure

Partial Class Website_BeneficiaryAddUpdate
	Inherits MasteredPage
	Protected PayeeId As Integer 
	protected DataItem As Bll.Accounts.Payee
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		BtnUpdate.Text = GetGlobalResourceObject("Multilang", "Update").ToString.ToUpper()
		BtnDelete.Text = GetGlobalResourceObject("Multilang", "Delete").ToString.ToUpper()
		Button1.Text = GetGlobalResourceObject("Multilang", "Create").ToString.ToUpper()
		PayeeId = Request("CompanyMakePaymentsProfiles_id").ToInt32(0)
		If Not Page.IsPostBack Then
			'Dim data As List(Of GlobalDataVO) = WebUtils.DomainCache.GetGlobalDatas(GlobalDataGroup.BeneficiaryProfileType, WebUtils.CurrentLanguage)
			'data = data.Where(Function(d As GlobalDataVO) d.Flags = 0).ToList()
			'ddlProfileType.DataSource = data
			'ddlProfileType.DataTextField = "Value"
			'ddlProfileType.DataValueField = "ID"
			'ddlProfileType.DataBind()
			DataItem = Bll.Accounts.Payee.Load(PayeeId)
			If DataItem Is Nothing Then DataItem = New Bll.Accounts.Payee(Bll.Accounts.Account.Current.AccountID)
			DataBind()
		End If
	End Sub

	Protected Function CheckValidation() As Boolean
		lblError.Text = ""
		If Validation.IsEmptyField(txtCompanyName, lblError, GetLocalResourceObject("PersonalName")) Then Return False
		If Validation.IsEmptyField(txtLegalNumber, lblError, "ProfileType") Then Return False

		If Not (Validation.IsEmptyField(bankAbroadAccountName)) Then
			If Validation.IsEmptyField(bankAbroadAccountNumber, lblError, GetLocalResourceObject("AccountNumber")) Then Return False
			If Validation.IsEmptyField(bankAbroadBankName, lblError, GetLocalResourceObject("BankName")) Then Return False
			If Validation.IsEmptyField(bankAbroadBankAddressCity, lblError, GetGlobalResourceObject("Multilang", "City")) Then Return False
			If Validation.IsEmptyField(bankAbroadBankAddressState, lblError, GetLocalResourceObject("Region")) Then Return False
			If Validation.IsEmptyField(ddlCountry, lblError, GetGlobalResourceObject("Multilang", "Country")) Then Return False
			If Validation.IsEmptyField(bankAbroadSwiftNumber, lblError, "Bank Swift Code") Then Return False
			If Validation.IsInvalidIban(bankAbroadIBAN, ddlCountry.SelectedValue, lblError, "IBAN") Then Return False
			If Validation.IsInvalidAba(bankAbroadABA, ddlCountry.SelectedValue, lblError, "Bank bankAbroadABA") Then Return False
		End If

		'If Not (Validation.IsEmptyField(bankAbroadAccountName2)) Then
		'	If Validation.IsEmptyField(bankAbroadAccountNumber2, lblError, GetLocalResourceObject("AccountNumber")) Then Return False
		'	If Validation.IsEmptyField(bankAbroadBankName2, lblError, GetLocalResourceObject("BankName")) Then Return False
		'	If Validation.IsEmptyField(bankAbroadBankAddressCity2, lblError, "Correspondent City Name") Then Return False
		'	If Validation.IsEmptyField(bankAbroadBankAddressState2, lblError, GetLocalResourceObject("Region")) Then Return False
		'	If Validation.IsEmptyField(ddlCountry2, lblError, GetGlobalResourceObject("Multilang", "Country")) Then Return False
		'	If Validation.IsEmptyField(bankAbroadSwiftNumber2, lblError, "Correspondent Swift Code") Then Return False
		'	If Validation.IsInvalidIban(bankAbroadIBAN2, ddlCountry2.SelectedValue, lblError, "Correspondent IBAN") Then Return False
		'	If Validation.IsInvalidAba(bankAbroadABA2, ddlCountry2.SelectedValue, lblError, "Correspondent bankAbroadABA") Then Return False
		'End If

		Return True
	End Function

	Protected Sub BtnDelete_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim PayeeId As Integer = Request("CompanyMakePaymentsProfiles_id").ToInt32(0)
		Bll.Accounts.Payee.Load(PayeeId).Delete()
		Response.Redirect("BeneficiaryList.aspx", True)
	End Sub

	Protected Sub Save()
		DataItem = Bll.Accounts.Payee.Load(PayeeId)
		If DataItem Is Nothing Then DataItem = New Bll.Accounts.Payee(Bll.Accounts.Account.Current.AccountID)
		DataItem.LegalNumber = txtLegalNumber.Text
		'DataItem.CostumerNumber = txtcostumerNumber.Text
		DataItem.CompanyName = txtCompanyName.Text
		DataItem.ContactName  = txtContactName .Text
		DataItem.PhoneNumber = txtPhoneNumber.Text
		DataItem.FaxNumber = txtFaxNumber.Text
		DataItem.EmailAddress = txtEmailAddress.Text
		DataItem.StreetAddress = txtStreetAddress.Text
		DataItem.Comment = txtComment.Text

		'DataItem.BankAbroadBankAddressCountry = IIf(ddlCountry.SelectedCountryID Is Nothing, 0, ddlCountry.SelectedCountryID)
		'DataItem.BankAbroadBankAddressCountry2 = IIf(ddlCountry2.SelectedCountryID Is Nothing, 0, ddlCountry2.SelectedCountryID)
		'DataItem.BankAbroadAccountName = bankAbroadAccountName.Text
		'DataItem.BankAbroadAccountNumber = bankAbroadAccountNumber.Text
		'DataItem.BankAbroadBankName = bankAbroadBankName.Text
		'DataItem.BankAbroadBankAddress = bankAbroadBankAddress.Text
		'DataItem.BankAbroadBankAddressSecond = bankAbroadBankAddressSecond.Text
		'DataItem.BankAbroadBankAddressCity = bankAbroadBankAddressCity.Text
		'DataItem.BankAbroadBankAddressState = bankAbroadBankAddressState.Text
		'DataItem.BankAbroadBankAddressZip = bankAbroadBankAddressZip.Text
		'DataItem.BankAbroadSwiftNumber = bankAbroadSwiftNumber.Text
		'DataItem.BankAbroadSepaBic = bankAbroadSepaBic.Text
		'DataItem.BankAbroadIBAN = bankAbroadIBAN.Text
		'DataItem.BankAbroadABA = bankAbroadABA.Text
		'DataItem.BankAbroadSortCode = bankAbroadSortCode.Text
		'DataItem.BankAbroadAccountName2 = bankAbroadAccountName2.Text
		'DataItem.BankAbroadAccountNumber2 = bankAbroadAccountNumber2.Text
		'DataItem.BankAbroadBankName2 = bankAbroadBankName2.Text
		'DataItem.BankAbroadBankAddress2 = bankAbroadBankAddress2.Text
		'DataItem.BankAbroadBankAddressSecond2 = bankAbroadBankAddressSecond2.Text
		'DataItem.BankAbroadBankAddressCity2 = bankAbroadBankAddressCity2.Text
		'DataItem.BankAbroadBankAddressState2 = bankAbroadBankAddressState2.Text
		'DataItem.BankAbroadBankAddressZip2 = bankAbroadBankAddressZip2.Text
		'DataItem.BankAbroadSwiftNumber2 = bankAbroadSwiftNumber2.Text
		'DataItem.BankAbroadSepaBic2 = bankAbroadSepaBic2.Text
		'DataItem.BankAbroadIBAN2 = bankAbroadIBAN2.Text
		'DataItem.BankAbroadABA2 = bankAbroadABA2.Text
		'DataItem.BankAbroadSortCode2 = bankAbroadSortCode2.Text
		'DataItem.BankIsraelInfoPayeeName = bankIsraelInfo_PayeeName.Text
		'DataItem.BankIsraelInfoCompanyLegalNumber = bankIsraelInfo_CompanyLegalNumber.Text
		'DataItem.BankIsraelInfoPersonalIDNumber = bankIsraelInfo_personalIdNumber.Text
		'DataItem.BankIsraelInfoBankBranch = bankIsraelInfo_bankBranch.Text
		'DataItem.BankIsraelInfoAccountNumber = bankIsraelInfo_AccountNumber.Text
		'DataItem.BankIsraelInfoPaymentMethod = bankIsraelInfo_PaymentMethod.Text
		'DataItem.BankIsraelInfoBankCode = bankIsraelInfo_BankCode.Text

		DataItem.Save()
	End Sub

	Protected Sub BtnUpdate_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
		If Not CheckValidation() Then Exit Sub
		Save()
		Response.Redirect("BeneficiaryList.aspx", True)
	End Sub

	Protected Sub BtnInsert_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
		If Not CheckValidation() Then Exit Sub
		Save()
		Response.Redirect("BeneficiaryList.aspx", True)
	End Sub
End Class
