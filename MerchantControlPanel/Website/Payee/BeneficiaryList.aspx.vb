﻿
Partial Class Website_BeneficiaryList
	Inherits MasteredPage

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		litOwnBeneficiary.Text = "<img src=""../" & CurrentDomain.ThemeFolder & "/Images/icon_contactRed.gif"" alt=""" & GetLocalResourceObject("OwnBeneficiary") & """ title=""" & GetLocalResourceObject("OwnBeneficiary") & """ />"
		If Not IsPostBack Then
			repeaterResults.DataSource = Netpay.Bll.Accounts.Payee.LoadForAccount(Merchant.AccountID)
			repeaterResults.DataBind()
		End If
	End Sub

    Function GetRowBackground(ByVal payee As Bll.Accounts.Payee) As String
        Return IIf(CanRequestPayment(payee), "", "#ffebe1;")
    End Function

    Function GetEditButton(ByVal payee As Bll.Accounts.Payee, ByVal retButton As Boolean) As String
        If False Then
            If retButton Then Return "&nbsp;"
            Return String.Format("<img src='../{0}/Images/icon_contactRed.gif' alt='{1}' title='{1}' />", CurrentDomain.ThemeFolder, GetLocalResourceObject("OwnBeneficiary"))
        Else
            Dim link As String = String.Format("BeneficiaryAddUpdate.aspx?CompanyMakePaymentsProfiles_id={0}", payee.ID)
            If retButton Then Return String.Format("<input type=""button"" onclick=""document.location='{0}'"" value='  {1}  ' class=""btn btn-danger""  />", link, GetGlobalResourceObject("MultiLang", "Edit"))
            Return String.Format("<a href=''><img src='../{1}/Images/icon_contactBlue.gif' alt='{2}' title='{2}' /></a>", link, Web.WebUtils.CurrentDomain.ThemeFolder, GetGlobalResourceObject("MultiLang", "Edit"))
        End If
    End Function

    Sub btnAdd_OnClick(ByVal o As Object, ByVal e As System.EventArgs)
        Response.Redirect("BeneficiaryAddUpdate.aspx?ProfileType=" & Request("ProfileType"))
    End Sub

    Function CanRequestPayment(ByVal payee As Bll.Accounts.Payee) As Boolean
        If Not String.IsNullOrEmpty(payee.BankAccount.AccountNumber) Then Return True
        'If Not String.IsNullOrEmpty(payee.BankAccount.AccountNumber2) Then Return True
        'If (Not String.IsNullOrEmpty(payee.BankIsraelInfoAccountNumber)) And (payee.BankIsraelInfoPersonalIDNumber <> "" Or payee.BankIsraelInfoCompanyLegalNumber <> "") Then Return True
        Return False
    End Function

    Sub repeaterResults_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        Dim bCanRequestPayment As Boolean = CanRequestPayment(e.Item.DataItem)
        e.Item.FindControl("trBeneficiaryError").Visible = Not bCanRequestPayment
        Dim btnPay = CType(e.Item.FindControl("btnPay"), HyperLink)
        If Not bCanRequestPayment Then
            btnPay.Enabled = False
            btnPay.CssClass = "btn btn-default"
        End If
    End Sub

    'Function PaymentButtonClickJS(ByVal payee As Bll.Accounts.Payee) As String
    '    Dim sbJS As New StringBuilder
    '    If CanRequestPayment(payee) Then
    '        sbJS.AppendFormat("PayBeneficiary({0});", payee.ID)
    '    Else
    '        sbJS.AppendFormat("if (confirm(""{2}"")) EditBeneficiary({0}, {1});", payee.ID, payee.Identifier, GetLocalResourceObject("MissingBankDetails"))
    '    End If
    '    sbJS.Append("return false;")
    '    Return sbJS.ToString
    'End Function
End Class
