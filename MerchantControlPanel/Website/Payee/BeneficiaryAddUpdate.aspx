﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_BeneficiaryAddUpdate" CodeBehind="BeneficiaryAddUpdate.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
	<asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
	<div class="area-content">
		<div class="top-nav">
			<asp:Localize ID="Localize1" Text="<%$Resources:PageTitle%>" runat="server" />
		</div>
		<div class="content-background">
			<div class="section">
				<table>
					<tr>
						<td>
							<asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:MultiLang,ProfileType %>"></asp:Literal><span>*</span>
							<asp:TextBox ID="txtSearchTag" CssClass="meduim" runat="server" AutoPostBack="false" TabIndex="1" Text='<%# DataItem.SearchTag %>' />
						</td>
					</tr>
				</table>
			</div>
			<div>
				<asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:MultiLang,PersonalInfo %>" />
			</div>BanksDropDownBanksDropDown
			<div class="section">
				<table>
					<tr>
						<td class="Field_132">
							<asp:Literal ID="Literal3" Text="<%$Resources:MultiLang,Identifier%>" runat="server" />
						</td>
						<td>
							<asp:TextBox ID="txtLegalNumber" CssClass="meduim" runat="server" TabIndex="2" Text='<%# DataItem.LegalNumber %>' />
						</td>
						<td class="Field_132">
							<asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:PersonalName %>"></asp:Literal><span>*</span>
						</td>
						<td>
							<asp:TextBox ID="txtCompanyName" CssClass="meduim" runat="server" TabIndex="3" />
						</td>
					</tr>
					<tr>
						<td class="Field_132">
							<asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:ContactPerson %>" />
						</td>
						<td>
							<asp:TextBox ID="txtContactName" CssClass="meduim" runat="server" TabIndex="4" Text='<%# DataItem.ContactName %>' />
						</td>
						<td class="Field_132">
							<asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:MultiLang,Phone %>"></asp:Literal>
						</td>
						<td>
							<asp:TextBox ID="txtPhoneNumber" CssClass="meduim" runat="server" TabIndex="5" Text='<%# DataItem.PhoneNumber %>' />
						</td>
					</tr>
					<tr>
						<td class="Field_132">
							<asp:Literal ID="Literal7" Text="<%$Resources:MultiLang,Fax%>" runat="server" />
						</td>
						<td>
							<asp:TextBox ID="txtFaxNumber" CssClass="meduim" runat="server" TabIndex="6" Text='<%# DataItem.FaxNumber %>' />
						</td>
						<td class="Field_132">
							<asp:Literal ID="Literal8" Text="<%$Resources:MultiLang,Email%>" runat="server" />
						</td>
						<td>
							<asp:TextBox ID="txtEmailAddress" CssClass="meduim" runat="server" TabIndex="7" Text='<%# DataItem.EmailAddress %>' />
						</td>
					</tr>
					<tr>
						<td class="Field_132">
							<asp:Literal ID="Literal9" Text="<%$Resources:MultiLang,Address%>" runat="server" />
						</td>
						<td>
							<asp:TextBox ID="txtStreetAddress" CssClass="meduim" runat="server" TabIndex="8" Text='<%# DataItem.StreetAddress %>' />
						</td>
						<td class="Field_132">
							<asp:Literal ID="Literal10" Text="<%$Resources:MultiLang,Comment%>" runat="server" />
						</td>
						<td>
							<asp:TextBox ID="txtComment" CssClass="meduim" runat="server" TabIndex="9" Text='<%# DataItem.Comment %>' />
						</td>
					</tr>
				</table>
			</div>
			<div>
			<%
			If WebUtils.CurrentLanguageShort = "he" Then
			%>
				<asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:IsraeliBankDetails %>" />
			</div>
			<div class="section">
				<table>
					<tr>
						<td class="Field_132">
							<asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:BeneficiaryName %>" /><span>**</span>
						</td>
						<td>
							<asp:TextBox ID="txtAccountName" CssClass="meduim" runat="server" TabIndex="10" Text='<%# DataItem.BankAccount.AccountName %>' />
						</td>
					</tr>
					<tr>
						<td class="Field_132">
							<asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:BranchNumber %>" /><span>*</span>
						</td>
						<td>
							<asp:TextBox ID="bankIsraelInfo_bankBranch" CssClass="meduim" runat="server" TabIndex="13" Text='<%# DataItem.BankAccount.IBAN %>' />
						</td>
					</tr>
					<tr>
						<td class="Field_132">
							<asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:AccountNumber %>" /><span>*</span>
						</td>
						<td>
							<asp:TextBox ID="bankIsraelInfo_AccountNumber" CssClass="meduim" runat="server" TabIndex="14" Text='<%# DataItem.BankAccount.AccountNumber %>' />
						</td>
						<td class="Field_132">
							<asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:PaymentType %>" />
						</td>
						<td class="Field_132">
							<asp:TextBox ID="bankIsraelInfo_PaymentMethod" CssClass="meduim" runat="server" TabIndex="15" />
						</td>
					</tr>
					<tr>
						<td>
							<asp:Literal ID="Literal18" runat="server" Text="<%$ Resources:BankName %>" /><span>*</span>
						</td>
						<td>
							<netpay:DropDownBase ID="bankIsraelInfo_BankCode" CssClass="meduim" runat="server" TabIndex="16" />
						</td>
					</tr>
				</table>
			</div>
			<%
			End If
			%>
			<div>
				<asp:Literal ID="Literal19" runat="server" Text="<%$ Resources:AbroadBankDetails %>" />
			</div>
			<div class="section">
				<table>
					<tr>
						<td class="Field_132">
							<asp:Literal ID="Literal20" runat="server" Text="<%$ Resources:AccountName %>" /><span>**</span>
						</td>
						<td>
							<asp:TextBox ID="bankAbroadAccountName" CssClass="meduim" runat="server" TabIndex="17" Text='<%# DataItem.BankAccount.AccountName %>' />
						</td>
						<td class="Field_132">
							<asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:AccountNumber %>" /><span>*</span>
						</td>
						<td>
							<asp:TextBox ID="bankAbroadAccountNumber" CssClass="meduim" runat="server" TabIndex="18" Text='<%# DataItem.BankAccount.AccountNumber %>' />
						</td>
					</tr>
					<tr>
						<td class="Field_132">
							<asp:Literal ID="Literal22" runat="server" Text="<%$ Resources:BankName %>" /><span>*</span>
						</td>
						<td>
							<asp:TextBox ID="bankAbroadBankName" CssClass="meduim" runat="server" TabIndex="19" Text='<%# DataItem.BankAccount.BankName %>' />
						</td>
						<td class="Field_132">
							<asp:Literal ID="Literal23" Text="<%$Resources:MultiLang,City%>" runat="server" /><span>*</span>
						</td>
						<td>
							<asp:TextBox ID="bankAbroadBankAddressCity" CssClass="meduim" runat="server" TabIndex="20" Text='<%# DataItem.BankAccount.BankCity %>' />
						</td>
					</tr>
					<tr>
						<td class="Field_132">
							<asp:Literal ID="Literal24" runat="server" Text="<%$ Resources:BankAddressLine1 %>" />
							<netpay:PageItemHelp ID="PageItemHelp4" Text="<%$Resources:MultiLang,AddressLine1Help%>" Width="300" Height="28" runat="server" />
						</td>
						<td>
							<asp:TextBox ID="bankAbroadBankAddress" CssClass="meduim" runat="server" TabIndex="21" Text='<%# DataItem.BankAccount.BankStreet1 %>' />
						</td>
						<td class="Field_132">
							<asp:Literal ID="Literal25" runat="server" Text="<%$ Resources:BankAddressLine2 %>" />
							<netpay:PageItemHelp ID="PageItemHelp3" Text="<%$Resources:MultiLang,AddressLine2Help%>"
								Width="300" Height="28" runat="server" />
						</td>
						<td>
							<asp:TextBox ID="bankAbroadBankAddressSecond" CssClass="meduim" runat="server" TabIndex="22" Text='<%# DataItem.BankAccount.BankStreet2 %>' />
						</td>
					</tr>
					<tr>
						<td class="Field_132">
							<asp:Literal ID="Literal26" runat="server" Text="<%$ Resources:Region %>" /><span>*</span>
						</td>
						<td>
							<asp:TextBox ID="bankAbroadBankAddressState" CssClass="meduim" runat="server" TabIndex="23" Text='<%# DataItem.BankAccount.BankStateISOCode %>' />
						</td>
						<td class="Field_132">
							<asp:Literal ID="Literal27" runat="server" Text="<%$ Resources:PostalCode %>" />
						</td>
						<td>
							<asp:TextBox ID="bankAbroadBankAddressZip" CssClass="meduim" runat="server" TabIndex="24" Text='<%# DataItem.BankAccount.BankPostalCode %>' />
						</td>
					</tr>
					<tr>
						<td class="Field_132">
							<asp:Literal ID="Literal28" Text="<%$Resources:MultiLang,Country%>" runat="server" /><span>*</span>
						</td>
						<td>
							<netpay:CountryDropDown ID="ddlCountry" CssClass="meduim" runat="server" TabIndex="25" ISOCode='<%# DataItem.BankAccount.CurrencyISOCode %>' />
						</td>
					</tr>
					<tr>
						<td>
							Swift Code<span>*</span>
						</td>
						<td>
							<asp:TextBox ID="bankAbroadSwiftNumber" CssClass="meduim" runat="server" TabIndex="27" ISOCode='<%# DataItem.BankAccount.SwiftNumber %>' />
						</td>
						<td class="Field_132">
							IBAN<span>****</span>
						</td>
						<td>
							<asp:TextBox ID="bankAbroadIBAN" CssClass="meduim" runat="server" TabIndex="28" ISOCode='<%# DataItem.BankAccount.IBAN %>' />
						</td>
					</tr>
					<tr>
						<td class="Field_132">
							ABA<span>****</span>
						</td>
						<td>
							<asp:TextBox ID="bankAbroadABA" CssClass="meduim" runat="server" TabIndex="29" ISOCode='<%# DataItem.BankAccount.ABA %>' />
						</td>
					</tr>
				</table>
			</div>
			<div>
				<asp:Literal ID="Literal29" runat="server" Text="<%$ Resources:CorrespondentBankDetails %>" />
			</div>
			<div class="Beneficiary-button">
				<%
					If Request("CompanyMakePaymentsProfiles_id") > 0 Then
				%>
				<div>
					<asp:LinkButton ID="BtnUpdate" CssClass="btn btn-default" Text="<%$Resources:MultiLang,Update%>" OnClick="BtnUpdate_OnClick" runat="server" TabIndex="45" />
					<asp:LinkButton ID="BtnDelete" CssClass="btn btn-default" Text="<%$Resources:MultiLang,Delete%>" OnClick="BtnDelete_OnClick" runat="server" TabIndex="46" />
				</div>
				<%
				Else
				%>
				<div>
					<asp:LinkButton ID="Button1" CssClass="btn btn-default" OnClick="BtnInsert_OnClick" Text="CREATE " runat="server" TabIndex="47" />
				</div>
				<%
				End If
				%>
			</div>
			<div class="spacer">
				<div class="section">
					<div class="Disclaimer">
						*
                        <asp:Literal ID="Literal39" runat="server" Text="<%$ Resources:MandatoryField %>" /><br />
						**
                        <asp:Literal ID="Literal40" runat="server" Text="<%$ Resources:MandatoryField1 %>" /><br />
						***
                        <asp:Literal ID="Literal41" runat="server" Text="<%$ Resources:MandatoryField2 %>" /><br />
						****
                        <asp:Literal ID="Literal42" runat="server" Text="<%$ Resources:MandatoryField3 %>" /><br />
					</div>
				</div>
			</div>
			<asp:Label ID="lblError" Style="display: block;" runat="server" />
		</div>
	</div>
</asp:Content>
