﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_BeneficiaryMakePayment" Codebehind="BeneficiaryMakePayment.aspx.vb" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$ Resources:PageTitle %>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$ Resources:PageTitle %>"
                runat="server" />
        </div>
        <div class="content-background">
            <div class="section">
                <h3><asp:Literal ID="litProfileTitle" runat="server" /></h3>
                <asp:Literal ID="litDataHeading" runat="server" />
            </div>
            <div class="section">
                <div>
                    <div class="Beneficiary-detalis">
                        <h3><asp:Literal ID="Literal2" runat="server" Text="<%$ Resources: PersonalBusinessDetails %>" /></h3>
                        <span class="bold"><asp:Literal ID="Literal8" runat="server" Text="<%$ Resources: Multilang, Identifier %>" />:</span>&nbsp;<%# Payee.LegalNumber %><br />
                        <span class="bold"><asp:Literal ID="Literal9" runat="server" Text="<%$ Resources: PersonalName %>" /></span>:&nbsp;<%# Payee.CompanyName %><br />
                        <asp:PlaceHolder runat="server" visible="<%# Not String.IsNullOrEmpty(Payee.ContactName) %>">
							<span class="bold"><asp:Literal ID="Literal10" runat="server" Text="<%$ Resources: ContactPerson %>" />:</span>&nbsp;<%# Payee.ContactName %><br />
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" visible="<%# Not String.IsNullOrEmpty(Payee.PhoneNumber) %>">
							<span class="bold"><asp:Literal ID="Literal11" runat="server" Text="<%$ Resources: Multilang, Phone %>" />:</span>&nbsp;<%# Payee.PhoneNumber %><br />
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" visible="<%# Not String.IsNullOrEmpty(Payee.FaxNumber) %>">
							<span class="bold"><asp:Literal ID="Literal12" runat="server" Text="<%$ Resources: Multilang, Fax %>" />:</span>&nbsp;<%# Payee.FaxNumber %><br />
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" visible="<%# Not String.IsNullOrEmpty(Payee.EmailAddress) %>">
							<span class="bold"><asp:Literal ID="Literal13" runat="server" Text="<%$ Resources: Multilang, Email %>" />:</span>&nbsp;<%# Payee.EmailAddress %><br />
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" visible="<%# Not String.IsNullOrEmpty(Payee.StreetAddress) %>">
							<span class="bold"><asp:Literal ID="Literal14" runat="server" Text="<%$ Resources: Multilang, Address %>" />:</span>&nbsp;<%# Payee.StreetAddress %><br />
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" visible="<%# Not String.IsNullOrEmpty(Payee.Comment) %>"><span class="bold">
                            <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources: Multilang, Comment %>" />:</span>&nbsp;<%# Payee.Comment %><br />
                        </asp:PlaceHolder>
                    </div>
                    <div class="Beneficiary-detalis">
                        <asp:PlaceHolder runat="server" ID="phAbroadBank">
                            <h3><asp:Literal ID="Literal25" runat="server" Text="<%$ Resources: BankAccountDetails %>" /></h3>
							<asp:Literal runat="server" ID="BankAccountText" Text='<%# Format.FormatBankAccount(Payee.BankAccount, FormatAddressStyle.MultipleLines) %>' />
                        </asp:PlaceHolder>
                    </div>
                    <div class="Beneficiary-detalis">
                        <asp:PlaceHolder runat="server" ID="phCorrespondingBank">
                            <h3><asp:Literal ID="Literal99" runat="server" Text="<%$ Resources: CorrespondingBankAccountDetails %>" /></h3>
							<asp:Literal runat="server" ID="BankAccountCoresText" Text='<%# Format.FormatBankAccount(Payee.BankAccount, FormatAddressStyle.MultipleLines) %>' />
                        </asp:PlaceHolder>
                    </div>
                    <div class="spacer"></div>
                </div>
            </div>
            <div class="section">
                <table>
                    <tr id="trConfirmationFile" runat="server">
                        <td class="Field_100"><asp:Literal ID="Literal1" Text="<%$ Resources:MultiLang,File %>" runat="server" /></td>
                        <td><asp:FileUpload ID="fuConfirmation" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="Field_100"><asp:Literal ID="Literal18" Text="<%$ Resources:MultiLang,Date %>" runat="server" /></td>
                        <td><netpay:DateRangePicker ID="wcDateRangePicker" class="Field_150" Layout="Horizontal" runat="server" IsToDateVisible="false" /></td>
                    </tr>
                    <tr>
                        <td class="Field_100"><asp:Literal ID="Literal3" Text="<%$ Resources:MultiLang,Comment %>" runat="server" /></td>
                        <td><asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" Width="600" Height="80" /></td>
                    </tr>
                    <tr>
                        <td class="Field_100"><asp:Literal ID="Literal4" Text="<%$ Resources:MultiLang,Currency %>" runat="server" /></td>
                        <td><netpay:CurrencyDropDown ID="ddlCurrency" EnableBlankSelection="false" runat="server" Width="150" /></td>
                    </tr>
                    <tr>
                        <td class="Field_100"><asp:Literal ID="Literal5" Text="<%$ Resources:MultiLang,Amount %>" runat="server" /></td>
                        <td><asp:TextBox ID="txtAmount" runat="server" Width="150" /></td>
                    </tr>
                    <tr>
                        <td colspan="2">
							<asp:Repeater runat="server" ID="rptLimits">
								<HeaderTemplate>
									<table cellpadding="0" cellspacing="0" border="0" width="250">
										<tr>
											<td valign="bottom"><asp:Label runat="server" CssClass="DataHeading" Text='<%$ Resources:, MinimumAmount %>' /></td>
											<td valign="bottom"><asp:Label runat="server" CssClass="DataHeading" Text='<%$ Resources:, MaximumAmount %>' /></td>
											<td valign="bottom"><asp:Label runat="server" CssClass="DataHeading" Text='<%$ Resources:, FeeAmount %>' /></td>
										</tr>
								</HeaderTemplate>
								<ItemTemplate>
									<tr>
										<td class="txt11" valign="top"><%# New Money(WebUtils.CurrentDomain, CType(Eval("MinPayoutAmount"), Decimal), CType(Eval("CurrencyID"), Integer)).ToIsoString() %></td>
										<td class="txt11" valign="top"><%# New Money(WebUtils.CurrentDomain, AvailableBalance(CType(Eval("CurrencyID"), Integer)), CType(Eval("CurrencyID"), Integer)).ToIsoString() %></td>
										<td class="txt11" valign="top" style="white-space: nowrap;"><%# New Money(WebUtils.CurrentDomain, CType(Eval("WireFee"), Decimal), CType(Eval("CurrencyID"), Integer)).ToIsoString() %> + <%# Eval("WireFeePercent", "{0:0.00}") %>%</td>
									</tr>
								</ItemTemplate>
								<FooterTemplate>
									</table>
								</FooterTemplate>
							</asp:Repeater>
			                <asp:Label ID="litNoBalance" runat="server" style="color:maroon;" Text="<%$ Resources:, BalanceTooLow %>" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><asp:LinkButton ID="BtnSubmit" class="btn btn-default" OnClick="BtnSubmit_OnClick" Text="<%$ Resources:SubmitOrder %>" runat="server" /></td>
                    </tr>
                </table>
            </div>
            <div class="section">
                <asp:Literal ID="litInsertText" runat="server" />
                <asp:Literal ID="litOrderBeneficiary" runat="server" />
                <asp:Literal ID="Literal6" Text="<%$ Resources:CompleteOrdersList %>" runat="server" />
                <a class="btn btn-default" href="ViewPaymentsReport.aspx"><asp:Literal ID="Literal7" Text="<%$ Resources:PaymentsView %>" runat="server" /></a>
            </div>
        </div>
    </div>
</asp:Content>
