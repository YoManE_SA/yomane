﻿<%@ Page Language="VB" MasterPageFile="~/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_BeneficiaryList" Codebehind="BeneficiaryList.aspx.vb" Title="PageTitle" %>
<%@ Import Namespace="Netpay.MerchantControlPanel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <script language="javascript" type="text/javascript">
        function EditBeneficiary(nID, nProfileType) {
            location.href = "BeneficiaryAddUpdate.aspx?CompanyMakePaymentsProfiles_id=" + nID + "&ProfileType=" + nProfileType;
        }

        function PayBeneficiary(nID, nProfileType) {
            location.href = "BeneficiaryMakePayment.aspx?ProfileType=" + nProfileType + (nID == 0 ? "" : "&CompanyMakePaymentsProfiles_id=" + nID);
        }
    </script>
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="locheadPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">
            <div class="section">
                <div class="BeneficiaryList"><asp:LinkButton ID="btnAdd" runat="server" CssClass="btn btn-default" Text="<%$Resources:AddNewBeneficiary %>" OnClick="btnAdd_OnClick" /></div>
                <table class="exspand-table" style="padding-top: 10px;">
                    <tr>
                        <th></th>
                        <th><asp:Literal ID="Literal1" Text="<%$Resources:MultiLang,No %>" runat="server"></asp:Literal></th>
                        <th><asp:Literal ID="Literal2" Text="<%$Resources:MultiLang,CompanyPersonName %>" runat="server"></asp:Literal></th>
                        <th><asp:Literal ID="Literal3" Text="<%$Resources:MultiLang,Identifier %>" runat="server"></asp:Literal></th>
                        <th><asp:Literal ID="Literal4" Text="<%$Resources:MultiLang,ProfileType %>" runat="server"></asp:Literal></th>
                        <th></th>
                        <th><asp:Literal ID="Literal5" Text="<%$Resources:MultiLang,Payments %>" runat="server"></asp:Literal></th>
                    </tr>
                    <tr>
                        <td style="background-color: #e9e9e9;">
                            <%--Don't see icons--%>
                            <asp:Literal runat="server" ID="litOwnBeneficiary" />
                        </td>
                        <td style="background-color: #e9e9e9;">000</td>
                        <td style="background-color: #e9e9e9;"><%=dbPages.dbtextShow(Merchant.Name).ToString%></td>
                        <td style="background-color: #e9e9e9;"><%= Account.AccountNumber %></td>
                        <td style="background-color: #e9e9e9;"><asp:Literal ID="Literal6" Text='<%$Resources:MerchantProfile %>' runat="server" /></td>
                        <td style="background-color: #e9e9e9;"></td>
                        <td style="background-color: #e9e9e9; width: 25px;">
                            <asp:Button ID="Button1" Text='<%$Resources:MultiLang,Pay%>' OnClientClick="PayBeneficiary(0, 0);return false;" runat="server" CssClass="btn btn-primary" />
                        </td>
                    </tr>
                    <asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server" OnItemDataBound="repeaterResults_ItemDataBound">
                        <ItemTemplate>
                            <tr onmouseover="this.oldBgColor=this.style.backgroundColor;this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor=this.oldBgColor;" style="background-color: <%# GetRowBackground(CType(Container.DataItem, Accounts.Payee))%>">
                                <td><%# GetEditButton(CType(Container.DataItem, Accounts.Payee), False)%></td>
                                <td><%# Eval("ID") %></td>
                                <td><%# Eval("CompanyName") %></td>
                                <td><%# Eval("ContactName") %></td>
                                <td><% 'WebUtils.DomainCache.GetGlobalDataValue(GlobalDataGroup.BeneficiaryProfileType, WebUtils.CurrentLanguage, CType(Container.DataItem, MerchantBeneficiaryVO).ProfileType) %></td>
                                <td><%# GetEditButton(CType(Container.DataItem, Accounts.Payee), True) %></td>
                                <td><asp:HyperLink ID="btnPay" Text='<%$Resources:MultiLang,Pay%>' NavigateUrl='<%# "BeneficiaryMakePayment.aspx?CompanyMakePaymentsProfiles_id=" & Eval("ID") %>' runat="server" CssClass="btn btn-primary"  /></td>
                            </tr>
                            <tr runat="server" id="trBeneficiaryError">
                                <td colspan="7" style="background-color: #ffebe1;">
                                    <img alt="alert" src="/NPCommon/images/alert_small.png" align="top" />
                                    <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:BeneficiaryError %>" />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
