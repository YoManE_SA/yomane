﻿Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Linq
Imports Netpay.CommonTypes
Imports Netpay.Dal.Netpay
Imports Netpay.Infrastructure
Imports Netpay.Bll
Imports Netpay.Web
Imports Netpay.Web.Controls

Partial Class Website_BeneficiaryMakePayment
	Inherits MasteredPage
	Protected PayeeID As Integer 
	Protected Payee As Bll.Accounts.Payee
	Protected bIsAvailableBalance As Boolean
	Private _availableBalance As Dictionary(Of Integer, Decimal)

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Dim PayeeID = Request("CompanyMakePaymentsProfiles_id").ToInt32()
		If Page.IsPostBack Then
			If wcDateRangePicker.FromDate Is Nothing Then wcDateRangePicker.FromDate = DateTime.Now.Date
		Else
			wcDateRangePicker.FromDate = DateTime.Today
			ddlCurrency.SetCurrencyIDs(AvailableBalance.Keys.ToDelimitedString())
		End If
		If PayeeID = 0 Then 
			Payee = New Bll.Accounts.Payee(Merchant.AccountID) With { 
				.CompanyName = merchant.LegalName, 
				.LegalNumber = merchant.LegalNumber, 
				.EmailAddress = merchant.EmailAddress, 
				.ContactName = String.Format("{0} {1}", merchant.ContactFirstName, merchant.ContactLastName),
				.FaxNumber = merchant.Fax, 
				.PhoneNumber = merchant.Phone
			}
		Else 
			Payee = Bll.Accounts.Payee.Load(PayeeID)
		End If
        litProfileTitle.Text = Payee.ContactName
        litDataHeading.Text = GetLocalResourceObject("BeneficiaryNumber").ToString & ": " & Payee.ID
        'litDataHeading.Text = WebUtils.DomainCache.GetGlobalDataValue(GlobalDataGroup.BeneficiaryProfileType, WebUtils.CurrentLanguage, Request("ProfileType"))

		'Dim merchantBeneficiaryType As Nullable(Of MerchantBeneficiaryProfileType) = Request("ProfileType").ToNullableEnumByValue(Of MerchantBeneficiaryProfileType)()
		'If merchantBeneficiaryType Is Nothing Then merchantBeneficiaryType = MerchantBeneficiaryProfileType.SelfPay
		'If trConfirmationFile IsNot Nothing Then trConfirmationFile.Visible = (merchantBeneficiaryType = MerchantBeneficiaryProfileType.Payoneer)
        'If merchantBeneficiaryID IsNot Nothing Then
        '    Dim beneficiary = Netpay.Bll.Merchant.Beneficiary.GetBeneficiary(WebUtils.LoggedUser.CredentialsToken, merchantBeneficiaryID)
        '    If beneficiary Is Nothing Then Response.Redirect("BeneficiaryList.aspx", True)
        'End If
		'phIsraeliBank.Visible = (WebUtils.CurrentLanguageShort = "he" And Not String.IsNullOrEmpty(bankIsraelInfo_PayeeName))
		'phAbroadBank.Visible = (bankAbroadInfo_AccountName <> "")
		'phCorrespondingBank.Visible = (bankAbroadInfo_AccountName2 <> "")

        'If PayeeID <> 0 Then
        '    ' self pay
        '    litProfileTitle.Text = WebUtils.DomainCache.GetGlobalDataValue(GlobalDataGroup.BeneficiaryProfileType, WebUtils.CurrentLanguage, 0)
        '    litDataHeading.Text = GetLocalResourceObject("DataDisplayHeading").ToString().Replace("%IDENTITY%", Session("CurrentDomain"))
        'Else
        '    litProfileTitle.Text = Payee.ContactName
        '    litDataHeading.Text = WebUtils.DomainCache.GetGlobalDataValue(GlobalDataGroup.BeneficiaryProfileType, WebUtils.CurrentLanguage, Request("ProfileType"))
        '    litDataHeading.Text &= ", " & GetLocalResourceObject("BeneficiaryNumber").ToString & ": " & Payee.ID

        '    If merchantBeneficiaryType = MerchantBeneficiaryProfileType.Payoneer Then
        '        litDataHeading.Text &= merchantBeneficiaryID.ToString()
        '    Else
        '        litDataHeading.Text &= "<a href=""BeneficiaryAddUpdate.aspx?CompanyMakePaymentsProfiles_id=" & merchantBeneficiaryID & """>" & merchantBeneficiaryID & "</a>"
        '    End If
        'End If

        'If merchantBeneficiaryType = MerchantBeneficiaryProfileType.MerchantCard Then sSQLWhere = " AND paymentType = 0"
        'If merchantBeneficiaryType <> MerchantBeneficiaryProfileType.MerchantCard Then sSQLWhere = " AND CompanyMakePaymentsProfiles_id = " & merchantBeneficiaryID

        'sSQL = "SELECT COUNT(CompanyMakePaymentsRequests_id) AS NumOfRequests FROM tblCompanyMakePaymentsRequests WHERE company_id=" & LoggedUser.ID & sSQLWhere
        ''Logger.Log(sSQL)
        'dr = dbPages.ExecReader(sSQL)
        'If dr.Read() Then
        '    If Int(dr("NumOfRequests")) = 1 Then
        '        litOrderBeneficiary.Text = GetLocalResourceObject("OneOrderBeneficiary")
        '    ElseIf Int(dr("NumOfRequests")) > 1 Then
        '        litOrderBeneficiary.Text = GetLocalResourceObject("MultipleOrderBeneficiary")
        '    Else
        '        litOrderBeneficiary.Text = GetLocalResourceObject("NoOrderBeneficiary")
        '    End If
        'End If
        'dr.Close()

        bIsAvailableBalance = False
        Dim currencies As Dictionary(Of Integer, Bll.Currency) = Bll.Currency.Cache.ToDictionary(Function(c) c.ID)
        For Each currentCurrency As KeyValuePair(Of Integer, Bll.Currency) In currencies
            If AvailableBalance.ContainsKey(currentCurrency.Value.ID) Then
                If AvailableBalance(currentCurrency.Value.ID) > 0 Then bIsAvailableBalance = True
            End If
        Next
        Dim merchantCurrencySettings = Bll.Merchants.Merchant.GetCurrencySettings(Merchant.ID)
		Dim feeList = merchantCurrencySettings.Where(Function(i) AvailableBalance.ContainsKey(i.Key)).ToDictionary(Function(i) i.Key, Function(i) i.Value)
        For Each n In AvailableBalance.Where(Function(i) Not feeList.ContainsKey(i.Key))
			feeList.Add(n.Key, New Bll.OldVO.SetMerchantSettlementsVO())
		Next
		litNoBalance.Visible = Not bIsAvailableBalance 
		If bIsAvailableBalance Then rptLimits.DataSource = feeList.Values
		DataBind()
    End Sub

    Protected ReadOnly Property AvailableBalance() As Dictionary(Of Integer, Decimal)
        Get
            If _availableBalance Is Nothing Then
                Dim merchantBalance = Bll.Accounts.Balance.GetAccountBalance(Merchant.AccountID, False)
                Dim merchantCurrencySettings As Dictionary(Of Integer, Bll.OldVO.SetMerchantSettlementsVO) = Merchants.Merchant.GetCurrencySettings(Merchant.ID)
                _availableBalance = New Dictionary(Of Integer, Decimal)
                For Each currentBalance In merchantBalance
                    Dim calculatedBalance As Decimal = 0
                    Dim merchantCurrencySetting As Bll.OldVO.SetMerchantSettlementsVO = Nothing
                    If merchantCurrencySettings.ContainsKey(currentBalance.CurrencyIso) Then merchantCurrencySetting = merchantCurrencySettings(currentBalance.CurrencyIso)
                    If merchantCurrencySetting Is Nothing Then calculatedBalance = currentBalance.Total _
                    Else calculatedBalance = (currentBalance.Total / (1 + merchantCurrencySetting.WireFeePercent / 100)) - merchantCurrencySetting.WireFee
                    If calculatedBalance < 0 Then calculatedBalance = 0
                    If calculatedBalance > 0 Then _availableBalance.Add(currentBalance.CurrencyIso, calculatedBalance)
                Next
            End If
            Return _availableBalance
        End Get
    End Property

	Protected Sub BtnSubmit_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim amount As Decimal = dbPages.TestVar(Replace(txtAmount.Text, ",", ""), 0D, -1D, 0D)
		Dim currency = ddlCurrency.Value.ToInt32()
        Dim merchantCurrencySettings As Dictionary(Of Integer, Bll.OldVO.SetMerchantSettlementsVO) = Bll.Merchants.Merchant.GetCurrencySettings(Merchant.ID)
		Dim bDone As Boolean = False

        'Dim merchant = Bll.Merchant.Merchant.Load(LoggedUser.CredentialsToken)
		'If merchantBeneficiaryType = MerchantBeneficiaryProfileType.Payoneer Then
		'	If fuConfirmation.PostedFile.ContentLength = 0 Then
		'		litInsertText.Text = "<span style=""color:maroon;"">" & GetLocalResourceObject("OrderNotAccepted") & "<br />" & GetLocalResourceObject("NoFileUploaded") & "</span><br />"
		'		Return
		'	End If
		'End If

		If Not AvailableBalance.ContainsKey(currency) Then
			litInsertText.Text = "<span style=""color:maroon;"">" & GetLocalResourceObject("OrderNotAccepted") & "<br />" & GetLocalResourceObject("InvalidCurrency") & "</span><br />"
			Return
		End If

		If wcDateRangePicker.FromDate.GetValueOrDefault().Date < DateTime.Now.Date Then
			litInsertText.Text = "<span style=""color:maroon;"">" & GetLocalResourceObject("OrderNotAccepted") & "<br />" & GetLocalResourceObject("DateTooEarly") & "</span><br />"
			Return
		End If

        Dim minPayout As Decimal = 0
        If merchantCurrencySettings.ContainsKey(currency) Then minPayout = merchantCurrencySettings(currency).MinPayoutAmount
        If amount - minPayout < 0 Then
            litInsertText.Text = "<span style=""color:maroon;"">" & GetLocalResourceObject("OrderNotAccepted") & "<br />" & GetLocalResourceObject("AmountTooLow") & "</span><br />"
            Return
        End If

        Dim availableBalanceTmp As Decimal = AvailableBalance(currency)
        If System.Math.Round(amount, 2) > System.Math.Round(availableBalanceTmp, 2) Then
            litInsertText.Text = "<span style=""color:maroon;"">" & GetLocalResourceObject("OrderNotAccepted") & "<br />" & GetLocalResourceObject("AmountTooHigh") & "</span><br />"
            Return
        End If

        Dim wire = New Bll.Wires.Wire(Payee, amount, currency, txtComment.Text)
        'Add to balance table (status pending)
        Call Netpay.Bll.Accounts.Balance.Create(Merchant.AccountID, currency, -amount, "Wire Request " & wire.ID, "Wire", wire.ID, True)
        litInsertText.Text = "<span style=""color:green;"">" & GetLocalResourceObject("OrderAccepted").ToString() & "</span><br />"
        bDone = True
        BtnSubmit.Visible = False


   '     Dim nExchangeRate As String = dbPages.ConvertCurrencyRate(Request("ctl00$cphBody$ddPaymentCurrency"), Currency.ILS)

   '     sSQL = "SET NOCOUNT ON;INSERT tblCompanyMakePaymentsRequests(CompanyMakePaymentsProfiles_id, Company_id," & _
   '     " paymentType, paymentDate, paymentAmount, paymentCurrency, paymentExchangeRate, paymentMerchantComment," & _
   '     " bankIsraelInfo_PayeeName, bankIsraelInfo_CompanyLegalNumber, bankIsraelInfo_personalIdNumber, " & _
   '     " bankIsraelInfo_bankBranch, bankIsraelInfo_AccountNumber, bankIsraelInfo_PaymentMethod, bankIsraelInfo_BankCode)" & _
   '     " SELECT " & IIf(merchantBeneficiaryID Is Nothing, "NULL", merchantBeneficiaryID) & ", " & LoggedUser.ID & ", " & merchantBeneficiaryType & "," & _
   '     " '" & wcDateRangePicker.FromDate.Value.ToString("yyyyMMdd") & "', " & amount & ", " & Request("ctl00$cphBody$ddPaymentCurrency").ToInt32(0) & "," & _
   '     " " & nExchangeRate & ", Left('" & paymentMerchantComment.Text.ToSql(True) & "', 250), "

   '     If merchantBeneficiaryType = MerchantBeneficiaryProfileType.MerchantCard Then
   '         'Get data from company table
   '         sSQL = sSQL & "Left(PaymentPayeeName, 80), Left(CompanyLegalNumber, 80), Left(IDNumber, 80)," & _
   '         " Left(PaymentBranch, 5), Left(PaymentAccount, 80), Left(PaymentMethod, 80), IsNull(PaymentBank, 0)" & _
   '         " FROM tblCompany WHERE ID=" & LoggedUser.ID
   '     Else
   '         'Get data from card
   '         sSQL = sSQL & "Left(bankIsraelInfo_PayeeName, 80), Left(bankIsraelInfo_CompanyLegalNumber, 80)," & _
   '         " Left(bankIsraelInfo_personalIdNumber, 80), Left(bankIsraelInfo_bankBranch, 5)," & _
   '         " Left(bankIsraelInfo_AccountNumber, 80), Left(bankIsraelInfo_PaymentMethod, 80), IsNull(bankIsraelInfo_BankCode, 0)" & _
   '         " FROM tblCompanyMakePaymentsProfiles WHERE CompanyMakePaymentsProfiles_id=" & Request("CompanyMakePaymentsProfiles_id").ToInt32(0)
   '     End If
   '     sSQL = sSQL & ";SELECT @@IDENTITY AS NewID;SET NOCOUNT OFF"

   '     dr = dbPages.ExecReader(sSQL)
   '     If dr.Read() Then merchantBeneficiaryID = dr("NewID").ToString().ToNullableInt32()
   '     dr.Close()
   '     'Add a transaction with this Payments request data
   '     sIP = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
   '     If sIP = "" Then
   '         sIP = Request.ServerVariables("REMOTE_ADDR")
   '     End If

   '     ' save confirmation file
   '     Dim confiramtionFile As HttpPostedFile = fuConfirmation.PostedFile
   '     Dim confirmationFileName As String = "NULL"
   '     If confiramtionFile IsNot Nothing Then
			'Dim confirmationFileFullName As String = FilesManager.GetFileUniqueFullName(String.Format("{0}{1}", FilesManager.GetPrivateAccessPath(LoggedUser.CredentialsToken, FilesManager.SubFolders.Wires), confiramtionFile.FileName))
   '         confiramtionFile.SaveAs(confirmationFileFullName)
   '         confirmationFileName = "'" + System.IO.Path.GetFileName(confirmationFileFullName) + "'"
   '     End If

   '     'Add this Payments requests to wire table
   '     sSQL = "INSERT tblWireMoney(Company_id, WireSourceTbl_id, WireType, WireDate, WireAmount, WireCurrency, WireExchangeRate, ConfirmationFileName, " & _
   '     " wireIDnumber, wireCompanyName, wireCompanyLegalName, wireCompanyLegalNumber," & _
   '     " wirePaymentMethod, wirePaymentPayeeName, wirePaymentAccount, wirePaymentBank, wirePaymentBranch," & _
   '     " wirePaymentAbroadAccountName," & _
   '     " wirePaymentAbroadAccountNumber," & _
   '     " wirePaymentAbroadBankName," & _
   '     " wirePaymentAbroadBankAddress," & _
   '     " wirePaymentAbroadBankAddressSecond," & _
   '     " wirePaymentAbroadBankAddressCity," & _
   '     " wirePaymentAbroadBankAddressState," & _
   '     " wirePaymentAbroadBankAddressZip," & _
   '     " wirePaymentAbroadBankAddressCountry," & _
   '     " wirePaymentAbroadSwiftNumber," & _
   '     " wirePaymentAbroadIBAN," & _
   '     " wirePaymentAbroadABA," & _
   '     " wirePaymentAbroadSortCode," & _
   '     " wirePaymentAbroadAccountName2," & _
   '     " wirePaymentAbroadAccountNumber2," & _
   '     " wirePaymentAbroadBankName2," & _
   '     " wirePaymentAbroadBankAddress2," & _
   '     " wirePaymentAbroadBankAddressSecond2," & _
   '     " wirePaymentAbroadBankAddressCity2," & _
   '     " wirePaymentAbroadBankAddressState2," & _
   '     " wirePaymentAbroadBankAddressZip2," & _
   '     " wirePaymentAbroadBankAddressCountry2," & _
   '     " wirePaymentAbroadSwiftNumber2," & _
   '     " wirePaymentAbroadIBAN2," & _
   '     " wirePaymentAbroadABA2," & _
   '     " wirePaymentAbroadSortCode2," & _
   '     " isShow)" & _
   '     " SELECT " & LoggedUser.ID & ", " & merchantBeneficiaryID & ", 2, '" & wcDateRangePicker.FromDate.Value.ToString("yyyyMMdd") & "'," & _
   '     " " & amount & ", " & Request("ctl00$cphBody$ddPaymentCurrency").ToInt32(0) & ", " & nExchangeRate & ", " & confirmationFileName & ", "
   '     If merchantBeneficiaryType = MerchantBeneficiaryProfileType.MerchantCard Then
   '         'Get data from company table
   '         sSQL = sSQL & " IDNumber, PaymentPayeeName, PaymentPayeeName, CompanyLegalNumber,"
   '         sSQL = sSQL & " PaymentMethod, PaymentPayeeName, PaymentAccount, PaymentBank, PaymentBranch,"
   '         sSQL = sSQL & " PaymentAbroadAccountName,"
   '         sSQL = sSQL & " PaymentAbroadAccountNumber,"
   '         sSQL = sSQL & " PaymentAbroadBankName,"
   '         sSQL = sSQL & " PaymentAbroadBankAddress,"
   '         sSQL = sSQL & " PaymentAbroadBankAddressSecond,"
   '         sSQL = sSQL & " PaymentAbroadBankAddressCity,"
   '         sSQL = sSQL & " PaymentAbroadBankAddressState,"
   '         sSQL = sSQL & " PaymentAbroadBankAddressZip,"
   '         sSQL = sSQL & " PaymentAbroadBankAddressCountry,"
   '         sSQL = sSQL & " PaymentAbroadSwiftNumber,"
   '         sSQL = sSQL & " PaymentAbroadIBAN,"
   '         sSQL = sSQL & " PaymentAbroadABA,"
   '         sSQL = sSQL & " PaymentAbroadSortCode,"
   '         sSQL = sSQL & " PaymentAbroadAccountName2,"
   '         sSQL = sSQL & " PaymentAbroadAccountNumber2,"
   '         sSQL = sSQL & " PaymentAbroadBankName2,"
   '         sSQL = sSQL & " PaymentAbroadBankAddress2,"
   '         sSQL = sSQL & " PaymentAbroadBankAddressSecond2,"
   '         sSQL = sSQL & " PaymentAbroadBankAddressCity2,"
   '         sSQL = sSQL & " PaymentAbroadBankAddressState2,"
   '         sSQL = sSQL & " PaymentAbroadBankAddressZip2,"
   '         sSQL = sSQL & " PaymentAbroadBankAddressCountry2,"
   '         sSQL = sSQL & " PaymentAbroadSwiftNumber2,"
   '         sSQL = sSQL & " PaymentAbroadIBAN2,"
   '         sSQL = sSQL & " PaymentAbroadABA2,"
   '         sSQL = sSQL & " PaymentAbroadSortCode2,"
   '         sSQL = sSQL & " 1 FROM tblCompany WHERE id=" & LoggedUser.ID
   '     Else
   '         'Get data from card
   '         sSQL = sSQL & " bankIsraelInfo_personalIdNumber, bankIsraelInfo_PayeeName, bankIsraelInfo_PayeeName, bankIsraelInfo_CompanyLegalNumber,"
   '         sSQL = sSQL & " bankIsraelInfo_PaymentMethod, bankIsraelInfo_PayeeName, bankIsraelInfo_AccountNumber, bankIsraelInfo_BankCode, bankIsraelInfo_bankBranch,"
   '         sSQL = sSQL & " bankAbroadAccountName,"
   '         sSQL = sSQL & " bankAbroadAccountNumber,"
   '         sSQL = sSQL & " bankAbroadBankName,"
   '         sSQL = sSQL & " bankAbroadBankAddress,"
   '         sSQL = sSQL & " bankAbroadBankAddressSecond,"
   '         sSQL = sSQL & " bankAbroadBankAddressCity,"
   '         sSQL = sSQL & " bankAbroadBankAddressState,"
   '         sSQL = sSQL & " bankAbroadBankAddressZip,"
   '         sSQL = sSQL & " bankAbroadBankAddressCountry,"
   '         sSQL = sSQL & " bankAbroadSwiftNumber,"
   '         sSQL = sSQL & " bankAbroadIBAN,"
   '         sSQL = sSQL & " bankAbroadABA,"
   '         sSQL = sSQL & " bankAbroadSortCode,"
   '         sSQL = sSQL & " bankAbroadAccountName2,"
   '         sSQL = sSQL & " bankAbroadAccountNumber2,"
   '         sSQL = sSQL & " bankAbroadBankName2,"
   '         sSQL = sSQL & " bankAbroadBankAddress2,"
   '         sSQL = sSQL & " bankAbroadBankAddressSecond2,"
   '         sSQL = sSQL & " bankAbroadBankAddressCity2,"
   '         sSQL = sSQL & " bankAbroadBankAddressState2,"
   '         sSQL = sSQL & " bankAbroadBankAddressZip2,"
   '         sSQL = sSQL & " bankAbroadBankAddressCountry2,"
   '         sSQL = sSQL & " bankAbroadSwiftNumber2,"
   '         sSQL = sSQL & " bankAbroadIBAN2,"
   '         sSQL = sSQL & " bankAbroadABA2,"
   '         sSQL = sSQL & " bankAbroadSortCode2,"
   '         sSQL = sSQL & " 1 FROM tblCompanyMakePaymentsProfiles WHERE CompanyMakePaymentsProfiles_id=" & Request("CompanyMakePaymentsProfiles_id").ToInt32(0)
   '     End If

   '     'Response.Write(sSQL) : Response.End()
   '     dbPages.ExecSql(sSQL)
    End Sub

    'Sub BalanceInsert(ByVal companyID As Integer, ByVal sourceTableID As Integer, ByVal sourceType As String, ByVal sourceInfo As String, ByVal amount As String, ByVal currency As Integer, ByVal status As String, ByVal comment As String, ByVal isAddFee As String)
    '    Dim sSQL As String, currencyName As String, sSourceInfoTmp As String, MakePaymentsFee As String
    '    sSQL = "INSERT INTO tblCompanyBalance(company_id, sourceTbl_id, sourceType, sourceInfo, amount, currency, status, comment) VALUES" & _
    '    " (" & companyID & ", " & sourceTableID & ", " & sourceType & ", '" & sourceInfo.ToSql(True) & "', " & amount & "," & _
    '    " " & currency & "," & status & ", '" & comment & "')"
    '    dbPages.ExecSql(sSQL)

    '    If CBool(isAddFee) Then
    '        currencyName = IIf(currency = 0, "Shekel", "Dollar")
    '        sSourceInfoTmp = "Bank transfer"    'IIF(fCurrency = 0, "העברה בנקאית", "Bank transfer")
    '        sSQL = "SELECT MakePaymentsFee" & currencyName & " AS MakePaymentsFee, MakePaymentsFixFee FROM tblCompany WHERE id=" & companyID
    '        dr = dbPages.ExecReader(sSQL)
    '        If Not dr.Read() Then
    '            MakePaymentsFee = dr("MakePaymentsFee") - amount * (dr("MakePaymentsFixFee") / 100)
    '            If MakePaymentsFee > 0 Then
    '                'Insert fee line into balance
    '                sSQL = "INSERT INTO tblCompanyBalance(company_id, sourceTbl_id, sourceType, sourceInfo, amount, currency, status, comment)" & _
    '                " VALUES (" & companyID & ", 0, 4, '" & sSourceInfoTmp & "', " & -MakePaymentsFee & "," & currency & ", 0, '')"
    '                dbPages.ExecSql(sSQL)
    '            End If
    '        End If
    '        dr.Close()
    '    End If
    'End Sub

End Class
