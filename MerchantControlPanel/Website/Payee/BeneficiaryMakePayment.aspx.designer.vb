﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Website_BeneficiaryMakePayment
    
    '''<summary>
    '''locPageTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents locPageTitle As Global.System.Web.UI.WebControls.Localize
    
    '''<summary>
    '''Localize1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Localize1 As Global.System.Web.UI.WebControls.Localize
    
    '''<summary>
    '''litProfileTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litProfileTitle As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''litDataHeading control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litDataHeading As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Literal2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal2 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Literal8 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal8 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Literal9 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal9 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Literal10 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal10 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Literal11 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal11 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Literal12 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal12 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Literal13 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal13 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Literal14 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal14 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Literal15 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal15 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''phAbroadBank control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents phAbroadBank As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Literal25 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal25 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''BankAccountText control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents BankAccountText As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''phCorrespondingBank control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents phCorrespondingBank As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Literal99 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal99 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''BankAccountCoresText control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents BankAccountCoresText As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''trConfirmationFile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trConfirmationFile As Global.System.Web.UI.HtmlControls.HtmlTableRow
    
    '''<summary>
    '''Literal1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal1 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''fuConfirmation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents fuConfirmation As Global.System.Web.UI.WebControls.FileUpload
    
    '''<summary>
    '''Literal18 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal18 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''wcDateRangePicker control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents wcDateRangePicker As Global.Netpay.Web.Controls.DateRangePicker
    
    '''<summary>
    '''Literal3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal3 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''txtComment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtComment As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Literal4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal4 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''ddlCurrency control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCurrency As Global.Netpay.Web.Controls.CurrencyDropDown
    
    '''<summary>
    '''Literal5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal5 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''txtAmount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAmount As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''rptLimits control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rptLimits As Global.System.Web.UI.WebControls.Repeater
    
    '''<summary>
    '''litNoBalance control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litNoBalance As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''BtnSubmit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents BtnSubmit As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''litInsertText control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litInsertText As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''litOrderBeneficiary control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litOrderBeneficiary As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Literal6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal6 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''Literal7 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal7 As Global.System.Web.UI.WebControls.Literal
End Class
