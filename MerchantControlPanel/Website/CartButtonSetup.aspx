﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_CartButtonSetup" Codebehind="CartButtonSetup.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" Runat="Server">
	<script type="text/javascript">
		function changeCurrency()
		{
			var selectedCurrency = $("#ctl00_cphBody_ddCurrency option:selected").text();
			$("#ctl00_cphBody_lblCurrency1").html(selectedCurrency);
			$("#ctl00_cphBody_lblCurrency2").html(selectedCurrency);
		}

		function clearResult()
		{
			$("#ctl00_cphBody_txtResult").text("");
		}

		function selectResult()
		{
			$("#ctl00_cphBody_txtResult").select();
		}
	</script>
	
	<netpay:PageHelpBox Text="<%$Resources:HelpBox %>" runat="server" />
	<table>
		<tr>
			<td><h1><asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" /></h1></td>
		</tr>
	</table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" Runat="Server">


  <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$Resources:Contactinfo%>" runat="server" /></div>
        <div class="content-background">
            <div class="section">
                
            </div>
            <asp:Localize ID="Localize7" Text="<%$Resources:OnLineSupport%>" runat="server" />
            <div class="section">
          
                <div class="spacer">
                </div>
            </div>
         
        </div>
    </div>

	<table border="0" align="center" style="width: 92%;">
		<tbody>
			<tr>
				<td>
					<div id="txtError" class="error" visible="false" runat="server"></div>
				</td>
			</tr>
		</tbody>
	</table>
	<table border="0" align="center" style="width:92%; background-color:#f5f5f5; border:1px solid silver; padding:10px;">
		<tr>
			<td valign="top">
				<table border="0">
					<tr>
						<td colspan="2" style="font-weight:bold;">
							<asp:Localize Text="<%$Resources:ItemButton%>" runat="server"></asp:Localize>
						</td>
					</tr>
					<tr>
						<td>
							<asp:Localize Text="<%$Resources:ItemName%>" runat="server"></asp:Localize>
						</td>
						<td>
							<asp:TextBox ID="txtItemName" runat="server"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td>
							<asp:Localize Text="<%$Resources:Multilang, Currency%>" runat="server"></asp:Localize>
						</td>
						<td>
							<netpay:CurrencyDropDown ID="ddCurrency" onchange="changeCurrency()" runat="server"></netpay:CurrencyDropDown>
						</td>
					</tr>
					<tr>
						<td>
							<asp:Localize Text="<%$Resources:Multilang, Price%>" runat="server"></asp:Localize>
						</td>
						<td>
							<asp:TextBox ID="txtPrice" runat="server"></asp:TextBox>
							<span id="lblCurrency1" runat="server"></span>
						</td>
					</tr>
					<tr>
						<td>
							<asp:Localize Text="<%$Resources:TaxRate%>" runat="server"></asp:Localize>
						</td>
						<td>
							<asp:TextBox ID="txtTaxRate" runat="server"></asp:TextBox> %
						</td>
					</tr>
					<tr>
						<td>
							<asp:Localize Text="<%$Resources:Shipping%>" runat="server"></asp:Localize>
						</td>
						<td>
							<asp:TextBox ID="txtShipping" runat="server"></asp:TextBox>
							<span id="lblCurrency2" runat="server"></span>
						</td>
					</tr>
					<tr>
						<td>
							<asp:Localize Text="<%$Resources:ItemInfoUrl%>" runat="server"></asp:Localize>
						</td>
						<td>
							<asp:TextBox ID="txtItemInfoUrl" runat="server"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td>
							<asp:Localize Text="<%$Resources:ItemImageUrl%>" runat="server"></asp:Localize>
						</td>
						<td>
							<asp:TextBox ID="txtItemImageUrl" runat="server"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td>
							<asp:Localize Text="<%$Resources:ActivationMode%>" runat="server"></asp:Localize>
						</td>
						<td>
							<asp:DropDownList ID="ddActivationModeItem" runat="server">
								<asp:ListItem Text="Popup" Value="popup"></asp:ListItem>
								<asp:ListItem Text="Redirect" Value="redirect"></asp:ListItem>
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td>
				
						</td>
						<td>
							<asp:Button ID="btnCreateItemButton" Text="<%$Resources:Multilang, Create%>" OnClientClick="clearResult()" runat="server" />
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2" style="font-weight:bold;">
							<asp:Localize Text="<%$Resources:CartButton%>" runat="server"></asp:Localize>
						</td>
					</tr>
					<tr>
						<td>
							<asp:Localize Text="<%$Resources:ActivationMode%>" runat="server"></asp:Localize>
						</td>
						<td>
							<asp:DropDownList ID="ddActivationModeCart" runat="server">
								<asp:ListItem Text="Popup" Value="popup"></asp:ListItem>
								<asp:ListItem Text="Redirect" Value="redirect"></asp:ListItem>
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td>
				
						</td>
						<td>
							<asp:Button ID="btnCreateCartButton" Text="<%$Resources:Multilang, Create%>" OnClientClick="clearResult()" runat="server" />
						</td>
					</tr>
				</table>			
			</td>
			<td>
				<table>
					<tr>
						<td>
							<asp:TextBox ID="txtResult" TextMode="MultiLine" Width="500px" Height="400px" runat="server"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td>
							<input type="button" id="btnSelect" value="<%$Resources:SelectText%>" onclick="selectResult()" runat="server" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</asp:Content>

