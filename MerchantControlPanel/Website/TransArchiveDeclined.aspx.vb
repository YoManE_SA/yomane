﻿Imports System.Collections.Generic
Imports Netpay.Web.Controls
Imports Netpay.Bll.Reports
Imports Netpay.Bll
Imports Netpay.Infrastructure

Partial Class Website_TransArchiveDeclined
	Inherits MasteredPage

 	Protected Sub OnPageChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim filters = New Bll.Transactions.Transaction.SearchFilters()
		'filters.paymentMethodID = ddPaymentMethod.SelectedValue.ToNullableInt32()
		filters.date.From = wcDateRangePicker.FromDate
		filters.date.To = wcDateRangePicker.ToDate
		filters.replyCode = txtReplyCode.Text.ToSql(True).NullIfEmpty()
		filters.declinedType = ddDeclinedTypeDropDown.SelectedValue.ToNullableInt32()

		If TypeOf e Is ExcelButton.ExportEventArgs Then
			wcFiltersView.LoadFilters(filters)
            btnExportExcel.SetExportInfo(wcFiltersView.RenderText, ReportType.MerchantDeclinedArchive, Function(pi) Bll.Transactions.Transaction.Search(TransactionStatus.DeclinedArchived, filters, pi, False, False))
        Else
            repeaterResults.DataSource = Bll.Transactions.Transaction.Search(TransactionStatus.DeclinedArchived, filters, pager.Info, False, False)
			repeaterResults.DataBind()
			btnExportExcel.ResultCount = pager.Info.RowCount
		End If
		phResults.Visible = repeaterResults.Items.Count > 0
	End Sub

	Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
		hdLastSerachMode.Value = "search"
		pager.Info.PageCurrent = 0
		OnPageChanged(sender, e)
        pager.Visible = True
	End Sub

	Protected Sub btnShow_Click(ByVal sender As Object, ByVal e As EventArgs)
		hdLastSerachMode.Value = "show"

		Dim transactionID As Integer
        Dim filters = New Bll.Transactions.Transaction.SearchFilters()
		If Integer.TryParse(txtTransactionID.Text, transactionID) Then
			filters.transactionID = transactionID
			If TypeOf e Is ExcelButton.ExportEventArgs Then
				wcFiltersView.LoadFilters(filters)
                btnExportExcel.SetExportInfo(wcFiltersView.RenderText, ReportType.MerchantDeclinedArchive, Function(pi) Bll.Transactions.Transaction.Search(New Bll.Transactions.Transaction.LoadOptions(TransactionStatus.DeclinedArchived, pi, False, False), filters))
            Else
                repeaterResults.DataSource = Bll.Transactions.Transaction.Search(New Bll.Transactions.Transaction.LoadOptions(TransactionStatus.DeclinedArchived, Nothing, False, False), filters)
				repeaterResults.DataBind()
				btnExportExcel.ResultCount = 1
			End If
		End If
		pager.Visible = False
		phResults.Visible = repeaterResults.Items.Count > 0
	End Sub

	Protected Sub ExportExcel_Click(ByVal sender As Object, ByVal e As EventArgs)
		If (hdLastSerachMode.Value = "show") Then btnShow_Click(Me, e) Else OnPageChanged(sender, e)
		btnExportExcel.Export()
	End Sub

End Class
