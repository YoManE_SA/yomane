﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_RecurringChargeHistory" Codebehind="RecurringChargeHistory.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">
            <netpay:PopupButton ID="PopupButton1" IconSrc="Images/iconLegend.gif"
                Width="80" Height="80" Text="<%$Resources:MultiLang,Legend  %>" runat="server">
                <netpay:Legend ID="ttLegend" Capture="true" Decline="true" Pending="true" Authorize="true"
                    runat="server" />
            </netpay:PopupButton>
            <div class="section">
                <table class="exspand-table" align="center">
                    <tr>
                        <th colspan="2">
                            &nbsp;
                        </th>
                        <th>
                            <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:MultiLang,Transaction  %>" />
                        </th>
                        <th>
                            <asp:Literal ID="Literal2" runat="server" Text="<%$Resources:MultiLang,Seriesnumber  %>" />
                        </th>
                        <th>
                            <asp:Literal ID="Literal3" runat="server" Text="<%$Resources:MultiLang,PaymentNum  %>" />
                        </th>
                        <th>
                            <asp:Literal ID="Literal4" runat="server" Text="<%$Resources:MultiLang,PaymentMethod  %>" />
                        </th>
                        <th>
                            <asp:Literal ID="Literal5" runat="server" Text="<%$Resources:MultiLang,Date  %>" />
                        </th>
                        <th>
                            <asp:Literal ID="Literal6" runat="server" Text="<%$Resources:MultiLang,Amount  %>" />
                        </th>
                        <th>
                            <asp:Literal ID="Literal7" runat="server" Text="<%$Resources:MultiLang,Reply  %>" />
                        </th>
                    </tr>
                    <asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server" OnItemDataBound="SetPaymentMethodCountry">
                        <ItemTemplate>
                            <tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
                                <td>
                                    <div style="width: 15px; height: 15px;" class="transactionStatus<%# CType(Container.DataItem, Netpay.Bll.Transactions.Recurring.ChargeHistory).Transaction.Status.ToString() %>">
                                    </div>
                                </td>
                                <td>
                                    <img onclick="netpay.Common.toggleTransactionInfo('<%# CType(Container.DataItem, Netpay.Bll.Transactions.Recurring.ChargeHistory).Transaction.ID %>', '<%# CType(Container.DataItem, Netpay.Bll.Transactions.Recurring.ChargeHistory).Transaction.PaymentMethodID %>', 'false', 'white', '<%# CType(Container.DataItem, Netpay.Bll.Transactions.Recurring.ChargeHistory).Transaction.Status.ToString() %>', '<%# MapTemplateVirPath("")%>')"
                                        style="cursor: pointer;" id="expandImage-<%# CType(Container.DataItem, Netpay.Bll.Transactions.Recurring.ChargeHistory).Transaction.ID %>"
                                        src="<%= MapTemplateVirPath("Images/plus.png")%>" alt="" border="0" align="middle" />
                                </td>
                                <td>
                                    <%# CType(Container.DataItem, Netpay.Bll.Transactions.Recurring.ChargeHistory).Transaction.ID %>
                                </td>
                                <td>
                                    <a href="RecurringChargesMain.aspx?seriesID=<%# CType(Container.DataItem, Netpay.Bll.Transactions.Recurring.ChargeHistory).Charge.SeriesID %>">
                                        <%# CType(Container.DataItem, Netpay.Bll.Transactions.Recurring.ChargeHistory).Charge.SeriesID %></a>
                                </td>
                                <td>
                                    <%# CType(Container.DataItem, Netpay.Bll.Transactions.Recurring.ChargeHistory).Charge.ChargeNumber %>
                                </td>
                                <td>
                                    <netpay:PaymentMethodView ID="PaymentMethodView" runat="server" PaymentMethodID="<%# CType(Container.DataItem, Netpay.Bll.Transactions.Recurring.ChargeHistory).Transaction.PaymentMethodID %>"
                                        PaymentMethodDisplay="<%# CType(Container.DataItem, Netpay.Bll.Transactions.Recurring.ChargeHistory).Transaction.PaymentMethodDisplay %>" />
                                </td>
                                <td>
                                    <%# CType(Container.DataItem, Netpay.Bll.Transactions.Recurring.ChargeHistory).Transaction.InsertDate %>
                                </td>
                                <td class="Numeric">
                                    <%# CType(Container.DataItem, Netpay.Bll.Transactions.Recurring.ChargeHistory).Transaction.CurrencySymbol %>
                                    <%# CType(Container.DataItem, Netpay.Bll.Transactions.Recurring.ChargeHistory).Transaction.Amount.ToString("#,0.00") %>
                                </td>
                                <td>
                                    <%# CType(Container.DataItem, Netpay.Bll.Transactions.Recurring.ChargeHistory).Transaction.ReplyCode %>
                                </td>
                            </tr>
                            <tr>
                                <td id="infoContailner-<%# CType(Container.DataItem, Netpay.Bll.Transactions.Recurring.ChargeHistory).Transaction.ID %>"style="display: none; margin: 0px; padding: 0px;" colspan="9"></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
