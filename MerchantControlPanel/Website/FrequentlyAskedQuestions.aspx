﻿<%@ Page Language="VB" Title="" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_FrequentlyAskedQuestions"
    CodeBehind="FrequentlyAskedQuestions.aspx.vb" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:MultiLang,FaqQustionAnswers%>" runat="server" />
</asp:Content>
<asp:Content ContentPlaceHolderID="cphHead" runat="server">
   
  
    <script type="text/javascript">
        function setAllFaq(faqCount, display) {
            for (var faqId = 0; faqId < faqCount; faqId++) {
                var answer = $("#ANS_" + faqId);
                var image = $("#IMG_" + faqId);
                if (display == "") {
                    answer.css("display", "block")
                    image.attr("src", "<%= MapTemplateVirPath("images/menu/minus-ltr.png")%>");
                }
                else {
                    answer.css('display', "none");
                    image.attr("src", "<%= MapTemplateVirPath("images/menu/plus-ltr.png")%>");
                }
            }
        }

        function toggleFaq(faqId) {
            var answer = $("#ANS_" + faqId);
            var image = $("#IMG_" + faqId);
            if (answer.css("display") == "none") {
                answer.css("display", "block")
                image.attr("src", "<%= MapTemplateVirPath("images/menu/minus-ltr.png")%>");
            }
            else {
                answer.css('display', "none");
                image.attr("src", "<%= MapTemplateVirPath("images/menu/plus-ltr.png")%>");
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <div class="faq-align-left">
                <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:MultiLang,FaqQustionAnswers %>" />
            </div>
            <div class="exspand-close-faq">
                <a href="#" onclick="setAllFaq(<%=rptQuestions.Items.Count %>, '')" class="links-faq">
                    <asp:Literal ID="Literal2" runat="server" Text="<%$Resources:MultiLang,OpenAll %>" /></a>
                | <a href="#" onclick="setAllFaq(<%=rptQuestions.Items.Count %>, 'none')" class="links-faq">
                    <asp:Literal ID="Literal3" runat="server" Text="<%$Resources:MultiLang,CloseAll %>" /></a>
            </div>
        </div>
        <div class="content-background">
            <div class="section-faq">
                <asp:Repeater runat="server" ID="rptQuestions">
                    <ItemTemplate>
                        <h3 class="faq-question" id="QST_<%# Container.ItemIndex %>" onclick="toggleFaq(<%# Container.ItemIndex %>)">

                            <img id="IMG_<%# Container.ItemIndex %>" alt="" src="<%= MapTemplateVirPath("images/menu/plus-" & WebUtils.CurrentDirection & ".png")%>"

                          
                                class="head-faq-arrow" /><%# Eval("Question") %></h3>
                        <div class="faq" id="ANS_<%# Container.ItemIndex %>" style="display: none;">
                            <%# Eval("Answer")%>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
</asp:Content>
