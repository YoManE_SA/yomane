﻿<%@ Page Title="PageTitle" Language="VB" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_DownloadButtons" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" CodeBehind="DownloadButtons.aspx.vb" %>

<asp:content id="Content1" contentplaceholderid="cphTitle" runat="Server">
	<asp:Localize Text="<%$Resources:PageTitle%>" runat="server" />
</asp:content>
<asp:content id="Content2" contentplaceholderid="cphBody" runat="Server">
  <div class="area-content">
        <div class="top-nav"><asp:Localize Text="<%$Resources:PageTitle%>" runat="server" /></div>
        <div class="content-background">
            <div class="section">
							<table border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td align="center">
										<img src="/NPCommon/NPButtons/Netpay_payby.gif" alt="" width="90" height="29" border="0" /><br />
										Netpay_payby.gif (1.65 Kb)
									</td>
									<td align="center">
										<img src="/NPCommon/NPButtons/Netpay_payby_bevel.gif" alt="" width="90" height="29" border="0" /><br />
										Netpay_payby_bevel.gif (1.84 Kb)
									</td>
								</tr>
                                <tr>
                                <td colspan="2"></td>
                                </tr>
								<tr>
									<td align="center">
										<img src="/NPCommon/NPButtons/Netpay_payby_eng.gif" alt="" width="90" height="29" border="0" /><br />
										Netpay_payby_eng.gif (1.59 Kb)
									</td>
									<td align="center">
										<img src="/NPCommon/NPButtons/Netpay_payby_bevel_eng.gif" alt="" width="90" height="29" border="0" /><br />
										Netpay_payby_bevel_eng.gif (1.78 Kb)
									</td>
								</tr>
                                <tr>
                                <td colspan="2"></td>
                                </tr>
								<tr>
									<td align="center">
										<img src="/NPCommon/NPButtons/Netpay_basket.gif" width="90" height="19" alt="" border="0" /><br />
										Netpay_basket.gif (1.22 Kb)
									</td>
									<td  align="center">
										<img src="/NPCommon/NPButtons/Netpay_basket_bevel.gif" width="90" height="19" alt="" border="0" /><br />
										Netpay_basket_bevel.gif (1.30 Kb)
									</td>
								</tr>
							</table>
            </div>
        </div>
    </div>


</asp:content>
