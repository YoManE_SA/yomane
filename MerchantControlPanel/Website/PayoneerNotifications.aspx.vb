﻿Public Class PayoneerNotifications
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Request("apuid") Is Nothing Then
			Netpay.Infrastructure.Logger.Log(Infrastructure.LogSeverity.Error, Infrastructure.LogTag.Exception, "payoneer apuid is missing")
			Response.Write("apuid is missing")
			Exit Sub
		End If

		Dim uniqueId As Guid
		If Not Guid.TryParse(Request("apuid"), uniqueId) Then
			Netpay.Infrastructure.Logger.Log(Infrastructure.LogSeverity.Error, Infrastructure.LogTag.Exception, "payoneer apuid is invalid")
			Response.Write("apuid is invalid")
			Exit Sub
		End If

		Dim action As String = Request("action")
		If action Is Nothing Then
			Netpay.Infrastructure.Logger.Log(Infrastructure.LogSeverity.Error, Infrastructure.LogTag.Exception, "payoneer action is missing")
			Response.Write("action is missing")
			Exit Sub
		End If

		Dim result As Boolean
		If action = "approved" Then
            result = Netpay.Bll.ExternalCards.ChangeCustomerStatus(Web.WebUtils.CurrentDomain.Host, uniqueId, Infrastructure.ExternalCardCustomerStatus.Approved)
        ElseIf action = "registered" Then
            result = Netpay.Bll.ExternalCards.ChangeCustomerStatus(Web.WebUtils.CurrentDomain.Host, uniqueId, Infrastructure.ExternalCardCustomerStatus.Registered)
        ElseIf action = "declined" Then
            result = Netpay.Bll.ExternalCards.ChangeCustomerStatus(Web.WebUtils.CurrentDomain.Host, uniqueId, Infrastructure.ExternalCardCustomerStatus.Declined)
        ElseIf action = "activation" Then
            result = Netpay.Bll.ExternalCards.ChangeCustomerStatus(Web.WebUtils.CurrentDomain.Host, uniqueId, Infrastructure.ExternalCardCustomerStatus.Activated)
		Else
			Netpay.Infrastructure.Logger.Log(Infrastructure.LogSeverity.Error, Infrastructure.LogTag.Exception, "payoneer action is invalid")
			Response.Write("action is invalid")
			Exit Sub
		End If

		If result Then
			Response.Write("action successful")
		Else
			Netpay.Infrastructure.Logger.Log(Infrastructure.LogSeverity.Error, Infrastructure.LogTag.Exception, "payoneer action has failed")
			Response.Write("action has failed")
		End If
	End Sub
End Class