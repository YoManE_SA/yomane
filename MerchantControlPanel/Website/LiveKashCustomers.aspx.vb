﻿Imports Netpay.Infrastructure.VO
Imports Netpay.Bll
Imports Netpay.Infrastructure

Public Class LiveKashCustomers
    Inherits MasteredPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request("__EVENTTARGET") = "ctl00$cphBody$pager" Then
            GetCustomers()
        End If

        If Not IsPostBack Then
            wcElementBlocker.Visible = Not Netpay.Bll.ExternalCards.IsProviderAvailable(LoggedUser.CredentialsToken, ExternalCardProviderEnum.LiveKash)
            GetCustomers()

            btnAddCustomer.Attributes.Add("style", "width:120px;")
        End If
    End Sub

    Protected Sub GetCustomers()
        Dim results As List(Of ExternalCardCustomerVO) = Netpay.Bll.ExternalCards.GetCustomers(LoggedUser.CredentialsToken, GetFilters(), pager.Info)
        repeaterResults.DataSource = results
        repeaterResults.DataBind()

        If results IsNot Nothing Then
            phResults.Visible = (results.Count > 0)
        End If
    End Sub

    Protected Sub ShowCard()
        Dim cardID As Integer
        If Not Integer.TryParse(txtCardID.Text, cardID) Then Exit Sub

        Dim result As ExternalCardCustomerVO = Netpay.Bll.ExternalCards.GetCustomer(LoggedUser.CredentialsToken, cardID)
        If result Is Nothing Then Exit Sub

        Dim results As New List(Of ExternalCardCustomerVO)()
        results.Add(result)
        repeaterResults.DataSource = results
        repeaterResults.DataBind()
        phResults.Visible = True
        pager.Visible = False
    End Sub

    Private Function GetFilters() As SearchFilters
        Dim filters As New SearchFilters()
        filters.externalCardProvider = ExternalCardProviderEnum.LiveKash
        filters.comment = txtFilterComment.Value.NullIfEmpty()
        filters.email = txtFilterEmail.Value.NullIfEmpty()

        Return filters
    End Function

    Protected Sub AddCustomer(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("LiveKashAddCustomer.aspx")
    End Sub

    Protected Function GetPaymentLink(customer As ExternalCardCustomerVO) As String
        If customer.IsPayable Then
            Return "<a href='ExternalCardCustomer.aspx?customerID=" & customer.ID & "'>" + GetLocalResourceObject("payments") + "</a>"
        Else
            Return "<span style='color:silver;'>" + GetLocalResourceObject("payments") + "</span>"
        End If
    End Function

    Protected Function GetCssClass(customer As ExternalCardCustomerVO) As String
        If customer.IsPayable Then
            Return "payoneerNoError"
        Else
            Return "payoneerError"
        End If
    End Function
End Class