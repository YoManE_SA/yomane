﻿Imports Netpay.Infrastructure

Public Class LiveKashAddCustomer
    Inherits MasteredPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            wcElementBlocker.Visible = Not Netpay.Bll.ExternalCards.IsProviderAvailable(LoggedUser.CredentialsToken, ExternalCardProviderEnum.LiveKash)
        End If
    End Sub

    Protected Sub AddCustomer(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        lblError.Text = ""
        lblSuccess.Text = ""

        If txtEmail.Text.Trim() = "" Or txtMobileNumber.Text.Trim() = "" Or txtFirstName.Text.Trim() = "" Or txtLastName.Text.Trim() = "" Or Not wcBirthdate.IsDateSelected Or txtAddress1.Text.Trim() = "" Or txtAddress2.Text.Trim() = "" Or txtCity.Text = "" Or txtState.Text.Trim() = "" Or txtCountry.Text.Trim() = "" Or txtZipcode.Text.Trim() = "" Or ddGender.SelectedValue.Trim() = "" Then
            lblError.Text = "Please fill all the required fields"
            Exit Sub
        End If

        Dim result As String = Netpay.Bll.ExternalCards.AddLivekashCustomer(Web.WebUtils.CredentialsToken, txtEmail.Text, txtMobileNumber.Text, txtFirstName.Text, txtLastName.Text, wcBirthdate.Date, txtAddress1.Text, txtAddress2.Text, txtCity.Text, txtState.Text, txtCountry.Text, txtZipcode.Text, ddGender.SelectedValue, txtComment.Value)
        If result = "ok" Then
            lblSuccess.Text = "Success"
        Else
            lblError.Text = result
        End If
    End Sub
End Class