﻿<%@ Page Title="PageTitle" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_RejectedTransactions" CodeBehind="~/Website/TransDeclined.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$ Resources:TransDeclined.aspx, PageTitle %>"
        runat="server" />
</asp:Content>
<asp:Content ContentPlaceHolderID="cphBody" runat="server">
    <asp:HiddenField ID="hdLastSerachMode" runat="server" />
    <div class="area-content">
        <asp:RegularExpressionValidator Display="Dynamic" ID="Last4digits" ClientIDMode="Static" ControlToValidate="txtLast4Digits" ValidationExpression="\d{4,4}$" runat="server" ErrorMessage="<%$Resources:MultiLang,Last4digitsValid  %>" />
        <div class="top-nav">
            <asp:Literal runat="server" Text="<%$Resources: TransDeclined.aspx, PageTitle%>" />
        </div>
        <div class="content-background">
            <div class="legend-menu">
                <div class="align-right">
                    <netpay:PopupButton ID="PopupButton2" IconSrc="Images/iconLegend.gif"
                        Text="<%$Resources:MultiLang,Legend  %>" runat="server">
                        <netpay:Legend ID="Legend1" Decline="true" AuthorizeDecline="true" Test="true" runat="server" />
                    </netpay:PopupButton>
                </div>
            </div>
            <div class="section">
                <div class="filter">
                    <div class="quick-search">
                        <asp:Literal  runat="server" Text="<%$Resources: TransApproved.aspx,QuickSearch%>" />
                        <div class="field-quick">
                            <asp:TextBox ID="txtTransactionID" runat="server" CssClass="Field_110" placeholder="<%$Resources:MultiLang,TransactionID  %>"></asp:TextBox>
                        </div>
                        <div class="button-quick">
                            <asp:LinkButton ID="btnShowTransaction" OnClick="btnShow_Click" CssClass="btn" runat="server" OnClientClick="javascript:return document.getElementById('ctl00_cphBody_txtTransactionNumber').value!=''"><i class="fa fa-search"></i> <asp:Literal  runat="server" Text="<%$Resources:MultiLang,Search %>" /></asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="Declined">
                    <div class="margin-bottom-10">
                        <netpay:DateRangePicker ID="wcDateRangePicker" HtmlLayout="Flow" Layout="Horizontal"
                            runat="server" />
                        <asp:TextBox ID="txtOrderNumber" placeholder="<%$Resources:MultiLang,Order %>" runat="server"
                            CssClass="Field_110"></asp:TextBox>
                        <netpay:PaymentMethodDropDown class="Field_200" BlankSelectionText="<%$Resources:MultiLang,PaymentMethod %>" ID="ddPaymentMethod" runat="server" />
                        <netpay:CreditTypeDropDown class="Field_140" BlankSelectionText="<%$Resources:MultiLang,CreditType %>" ID="ddCreditType" runat="server" />
                    </div>
                    <div class="margin-bottom-10">
                        <netpay:DeclinedTypeDropDown class="Field_120" BlankSelectionText="<%$Resources:MultiLang,Type %>"
                            runat="server" ID="ddDeclinedTypeDropDown">
                        </netpay:DeclinedTypeDropDown>
                        <asp:TextBox ID="txtReplyCode" class="Field_100" placeholder="<%$Resources:MultiLang,ReplyCode %>" runat="server" />
                        <asp:TextBox ID="txtLast4Digits" class="Field_100" placeholder="<%$Resources:MultiLang,Last4Digits  %>" runat="server" />
                        <asp:TextBox ID="txtPayerFirstName" class="Field_160" placeholder="<%$Resources:MultiLang,PayerFirstName  %>" runat="server" />
                        <asp:TextBox ID="txtPayerLastName" class="Field_160" placeholder="<%$Resources:MultiLang,PayerLastName  %>" runat="server" />
                    </div>
                    <div>
                        <asp:TextBox ID="txtPhone" class="Field_160" placeholder="<%$Resources:MultiLang,CCHolderPhone  %>" runat="server" />
                        <asp:TextBox ID="txtCardHolderEmail" class="Field_200" placeholder="<%$Resources:MultiLang,CCHolderMail  %>" runat="server" />
                    </div>
                </div>
                 <div class="spacer">
                    </div>

                <div class="wrap-button-bar">
                      <asp:LinkButton ID="BtnSearch" CssClass="btn btn-cons-short btn-inverse" runat="server" OnClick="btnSearch_Click"><i class="fa fa-search"></i>  <asp:Literal  runat="server" Text="<%$Resources:MultiLang,Search %>" /></asp:LinkButton>
                </div>
            </div>
            <div class="Error" visible="false" runat="server">
                <asp:Literal ID="ltError" Visible="false" runat="server" />
            </div>
            <asp:PlaceHolder ID="phResults" Visible="false" runat="server">
                <div class="section">
                    <netpay:SearchFiltersView ID="wcFiltersView" runat="server" Visible="false" />
                    <table class="exspand-table">
                        <thead>
                            <tr>
                                <th colspan="2"></th>
                                <th>
                                    <asp:Literal Text="<%$Resources:MultiLang,Transaction %>" runat="server" />
                                </th>
                                <th>
                                    <asp:Literal ID="litDateTitle" Text="<%$Resources:MultiLang,Date %>" runat="server" />
                                </th>
                                <th>
                                    <asp:Literal Text="<%$Resources:MultiLang,PaymentMethod %>" runat="server" />
                                </th>
                                <th>
                                    <asp:Literal Text="<%$Resources:MultiLang,Amount %>" runat="server" />
                                </th>
                                <th>
                                    <asp:Literal Text="<%$Resources:MultiLang,Replaycode %>" runat="server" />
                                </th>
                            </tr>
                        </thead>
                        <asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server">
                            <ItemTemplate>
                                <netpay:MerchantTransactionRowView ID="ucTransactionView" Transaction="<%# CType(Container.DataItem, Transactions.Transaction) %>"
                                    ShowStatus="false" ShowReplyCode="true" runat="server" />
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <div class="xls-wrap">
                        <netpay:ExcelButton ID="btnExportExcel" Text="<%$Resources:MultiLang,ExportToXls %>"
                            Enabled="false" IconImageOn="xls.png" IconImageOff="xls_off.png" runat="server"
                            OnClick="ExportExcel_Click" />
                    </div>
                    <netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
                </div>
            </asp:PlaceHolder>
        </div>
    </div>
</asp:Content>
