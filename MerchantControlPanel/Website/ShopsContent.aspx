﻿<%@ Page Title="PageTitle" Language="VB" AutoEventWireup="false" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" CodeBehind="ShopsContent.aspx.vb" Inherits="Netpay.MerchantControlPanel.ShopsContent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">
            <asp:PlaceHolder Visible="false" runat="server" ID="AddlngAlert">
                <div class="noerror margin-top-10">
                    <asp:Literal Text="<%$Resources:LanguageAdded %>" runat="server" />
                </div>
            </asp:PlaceHolder>
            <div class="section">
                <div class="form-group percent40">
                    <span class="form-group-title"><asp:Literal Text="<%$Resources: AddLanguage%>" runat="server" /></span>&nbsp;
                    <span class="form-group-title-eg"><asp:Literal Text="<%$Resources: ExLanguage %>" runat="server" /></span>
                    <div><netpay:DropDownBase CssClass="Field_200" runat="server" ID="ddlLanguage" BlankSelectionText="<%$Resources: ChooseLanguage%>" /></div>
                </div>
                <div class="wrap-button-bar">
                   <asp:LinkButton runat="server" CssClass="btn btn-primary btn-cons-short" OnClick="AddLanguage_Click"><i class="fa fa-plus-circle"></i> <asp:Literal Text="<%$Resources: Add%>" runat="server" /></asp:LinkButton>
                </div>
            </div>
            <div id="divContent" runat="server" class="section">
                <netpay:TabControl runat="server" ID="tbLanguageText" TabStyle="OrganicTabs" CssStyle="organicTabs">
                    <tabtemplate>
                        <asp:PlaceHolder Visible="false" runat="server" ID="EditlngAlert">
                        <div class="noerror">
                          <asp:Literal Text="<%$Resources: LanguageEdited%>" runat="server" />
                        </div>
                        </asp:PlaceHolder>
                        <div class="align-right" >
                            <asp:Button ID="btnRemoveLanguage" CssClass="btn btn-danger" Text="<%$Resources:MultiLang,RemoveLng %>" OnClick="RemoveLanguage_Click" runat="server" />
                        </div>
                        <div class="clear"></div>
                        <div class="shops-content">
                            <div class="margin-top-10">
                                <div class="form-group-title">
                                    <asp:Literal Text="<%$Resources:  Aboutus%>" runat="server" />
                                    <asp:RegularExpressionValidator runat="server" ErrorMessage="<%$Resources:  ValidationCharacters%>" ValidationExpression="^([\S\s]{100,4000})$" ValidationGroup="ContentValidation" ControlToValidate="txtAbout" Display="Dynamic" />
                                </div>
                                  <div class="alert alert-info margin-bottom-10">
                                <asp:Literal Text="<%$Resources:  AboutusAlert%>" runat="server" />
                            </div>
                                <asp:TextBox runat="server" ID="txtAbout" TextMode="MultiLine" CssClass="percent100" Style="min-height: 200px;" />
                            </div>
           
                            <div class="margin-top-10">
                                <div class="form-group-title">
                                    <asp:Literal Text="<%$Resources:  PrivacyPolicy%>" runat="server" />
                                    <asp:RegularExpressionValidator runat="server"  ErrorMessage="<%$Resources:  ValidationCharacters%>" ValidationExpression="^([\S\s]{100,4000})$" ValidationGroup="ContentValidation" ControlToValidate="txtPrivacy" Display="Dynamic" />
                                </div>
                                 <div class="alert alert-info margin-bottom-10">
                            <asp:Literal Text="<%$Resources:  PrivacyPolicyAlert%>" runat="server" />
                            </div>
                                <asp:TextBox runat="server" ID="txtPrivacy" TextMode="MultiLine"  CssClass="percent100" Style="min-height: 200px;" />
                            </div>
                            <div class="margin-top-10">
                                <div class="form-group-title">
                                    <asp:Literal Text="<%$Resources: TermsConditions%>" runat="server" />
                                    <asp:RegularExpressionValidator runat="server"  ErrorMessage="<%$Resources:  ValidationCharacters%>" ValidationExpression="^([\S\s]{100,4000})$" ValidationGroup="ContentValidation" ControlToValidate="txtTerms" Display="Dynamic" />
                                </div>
                                 <div class="alert alert-info margin-bottom-10">
                            <asp:Literal Text="<%$Resources:  TermsConditionsAlert%>" runat="server" />
                            </div>
                                <asp:TextBox runat="server" ID="txtTerms" TextMode="MultiLine" CssClass="percent100" Style="min-height: 200px;" />
                            </div>
                        </div>
                    </tabtemplate>
                </netpay:TabControl>
            </div>
            <div id="divUpdate" runat="server" class="wrap-button margin-bottom-10">
                <asp:LinkButton UseSubmitBehavior="true" ID="btnUpdate" Text="<%$Resources:MultiLang,Update %>" CssClass="btn btn-cons-short btn-success" ValidationGroup="ContentValidation" runat="server" OnClick="btnUpdate_Click" />
            </div>
        </div>
    </div>
</asp:Content>
