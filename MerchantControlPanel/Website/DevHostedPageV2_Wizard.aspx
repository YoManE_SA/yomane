﻿<%@ Page Language="VB" Title="PageTitle" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="true" Inherits="Netpay.MerchantControlPanel.Website_HostedPageV2_Wizard" CodeBehind="DevHostedPageV2_Wizard.aspx.vb" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:menu, PaymentWizard%>" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphHead" runat="Server">
    <script type="text/javascript">

        $(document).ready(function () {
            // section toggle event
            $(".wrap-edit-product").css("cursor", "pointer").click(function () {
                toggleSection($(this));
            });

        });
        function showSection(secName) {
            with (document.getElementById(secName)) {
                if (style.display != '')
                    style.display = '';
                else
                    style.display = 'none';
                document.getElementById(secName + 'Img').src = (style.display != '' ? '/NPCommon/Images/tree_expand.gif' : '/NPCommon/Images/tree_collapse.gif');
            }
        }
        function toggleSection(section) {
            if (section.next().css("display") == "none")
                openSection(section);
            else
                closeSection(section);
        }


        function openSection(section) {
            section.children(".align-right").children("i").switchClass("fa-angle-right", "fa-angle-down", 0, null);
            section.next().css("display", "block");
        }

        function closeSection(section) {
            section.children(".align-right").children("i").switchClass("fa-angle-down", "fa-angle-right", 0, null);
            section.next().css("display", "none");
        }




    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Literal Text="<%$Resources: Title %>" runat="server" />
        </div>
        <div class="content-background">
            <div class="alert alert-info margin-bottom-10">
                <asp:Literal Text="<%$Resources: Intro  %>" runat="server" />
            </div>
            <div class="section">
                <div class="margin-bottom-10 frame-border-option">
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">
                        <div class="align-left percent32 form-group">
                            <div class="margin-bottom-10">
                                <span>
                                    <asp:Literal Text="<%$Resources: Merchant_Number  %>" runat="server" />

                                </span>
                                <div>
                                    <asp:TextBox runat="server" ID="merchantID" Enabled="false" CssClass="percent100" />
                                </div>
                            </div>
                        </div>
                        <div class="align-left percent32 form-group">
                            <div class="margin-bottom-10">
                                <span>
                                    <asp:Literal Text="<%$Resources: Pay_for %>" runat="server" />
                                    <netpay:PageItemHelp ID="PageItemHelp1" Text="<%$Resources: Display_page %>" Width="300" runat="server" />
                                </span>
                                <div>
                                    <asp:TextBox runat="server" ID="disp_payFor" CssClass="percent100" MaxLength="100" Text="<%$Resources: Purchase %>" />
                                </div>
                            </div>
                        </div>
                        <div class="align-left percent32 form-group">
                            <div class="margin-bottom-10">
                                <span>
                                    <asp:Literal Text="<%$Resources: Trans_Comment %>" runat="server" /></span>
                                <netpay:PageItemHelp ID="PageItemHelp7" Text="<%$Resources: TransCommentHelp %>" Width="300" runat="server" />
                                <div>
                                    <asp:TextBox runat="server" CssClass="percent100" ID="trans_comment" />
                                </div>
                            </div>
                        </div>
                        <div class="align-left percent32 form-group">
                            <div class="margin-bottom-10">
                                <span>
                                    <asp:Literal Text="<%$Resources: Trans_RefNum %>" runat="server" /></span>
                                <netpay:PageItemHelp ID="PageItemHelp8" Text="<%$Resources: TransRefNumHelp %>" Width="300" runat="server" />
                                <div>
                                    <asp:TextBox runat="server" CssClass="percent100" ID="trans_refNum" />
                                </div>
                            </div>
                        </div>
                        <div class="align-left percent32 form-group">
                            <div class="margin-bottom-10">
                                <span>
                                    <asp:Literal Text="<%$Resources: Redirect_URL %>" runat="server" /></span>
                                <span>
                                    <netpay:PageItemHelp ID="PageItemHelp2" Text="<%$Resources: URLRedirectHelp %>" Width="300" runat="server" />
                                </span>
                                <div>
                                    <asp:TextBox runat="server" CssClass="percent100 url-input" ID="url_redirect" />
                                </div>
                            </div>
                        </div>
                        <div class="align-left percent32 form-group">
                            <div class="margin-bottom-10">
                                <span>
                                    <asp:Literal Text="<%$Resources: Notify_URL %>" runat="server" />
                                    <netpay:PageItemHelp ID="PageItemHelp3" Text="<%$Resources: urlNotifyHelp %>" Width="300" runat="server" />
                                </span>
                                <div>
                                    <asp:TextBox runat="server" CssClass="percent100 url-input" ID="url_notify" />
                                </div>
                            </div>
                        </div>
                        <div class="align-left percent32 form-group">
                            <div class="margin-bottom-10">
                                <span>
                                    <asp:Literal Text="<%$Resources: Skin_Number %>" runat="server" /></span>
                                <div>
                                    <asp:DropDownList CssClass="percent100" ID="ddlSkin" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="align-left percent32 form-group">
                            <div class="margin-bottom-10">
                                <span>
                                    <asp:Literal Text="<%$Resources: mobile_version %>" runat="server" /></span>
                                <div>
                                    <asp:DropDownList runat="server" CssClass="percent100" ID="ddldisp_mobile">
                                        <asp:ListItem Text="auto" />
                                        <asp:ListItem Text="false" Value="false" />
                                        <asp:ListItem Text="true" Value="true" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="align-left percent32 form-group">
                            <span>
                                <asp:Literal Text="<%$Resources: signature %>" runat="server" /></span>
                            <div>
                                <asp:PlaceHolder ID="phSignature" runat="server">
                                    <asp:DropDownList runat="server" CssClass="percent100" ID="ddlSignature">

                                        <asp:ListItem Text="<%$Resources: MD5 %>" />
                                        <asp:ListItem Text="<%$Resources: SHA256 %>" Selected="True" />
                                    </asp:DropDownList>
                                </asp:PlaceHolder>
                            </div>
                        </div>
                        <div class="spacer"></div>
                    </div>
                    <div class="frame-border-option-inline padding-10 margin-top-10 margin-bottom-10">
                        <div class="wrap-expand-details">
                            <div onclick="showSection('tdRecurring')" style="cursor: pointer;">
                                <img id="tdRecurringImg" src="/NPCommon/Images/tree_expand.gif" align="top" /><span class="SecHeading"><asp:Literal Text="<%$Resources: Optional_Recurring %>" runat="server" />
                                </span>
                                <netpay:PageItemHelp ID="PageItemHelp6" Text="<%$Resources: Transaction_process %>" Width="300" runat="server" />
                            </div>
                            <div style="display: none;" id="tdRecurring">
                                <div class="margin-bottom-10 margin-top-10">
                                    <div class="align-left percent32 form-group">
                                        <div class="margin-bottom-10">
                                            <span>
                                                <asp:Literal Text="<%$Resources: Trans_Recurring_1 %>" runat="server" /></span>
                                            <div>
                                                <asp:TextBox runat="server" CssClass="percent100" ID="trans_recurring1" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="align-left percent32 form-group">
                                        <div class="margin-bottom-10">
                                            <span>
                                                <asp:Literal Text="<%$Resources: Trans_Recurring_2 %>" runat="server" /></span>
                                            <div>
                                                <asp:TextBox runat="server" CssClass="percent100" ID="trans_recurring2" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="align-left percent32 form-group">
                                        <div class="margin-bottom-10">
                                            <span>
                                                <asp:Literal Text="<%$Resources: Recurring_Display  %>" runat="server" /></span>
                                            <div>
                                                <asp:DropDownList runat="server" CssClass="percent100" ID="disp_recurring">
                                                    <asp:ListItem Text="<%$Resources: Standard_Display %>" Value="0" />
                                                    <asp:ListItem Text="<%$Resources: Last_charge %>" Value="1" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="spacer"></div>
                                </div>
                            </div>
                        </div>


                        <div class="wrap-expand-details">
                            <div>
                                <img id="tdClientInfoImg" onclick="showSection('tdClientInfo')" style="cursor: pointer;" src="/NPCommon/Images/tree_expand.gif" alt="expand" align="top" /><asp:Literal Text="<%$Resources: Optional_Client_Data %>" runat="server" />
                                <netpay:PageItemHelp ID="PageItemHelp5" Text="<%$Resources: Depending_Payment %>" Width="300" runat="server" />

                                <div style="display: none;" id="tdClientInfo">
                                    <div class="margin-top-10 margin-bottom-10">
                                        <div class="align-left percent23 form-group">
                                            <div class="margin-bottom-10">
                                                <span>
                                                    <asp:Literal Text="<%$Resources: Full_Name %>" runat="server" />
                                                </span>
                                                <div>
                                                    <asp:TextBox ID="client_fullName" CssClass="percent100" runat="server" />
                                                </div>
                                            </div>

                                            <div class="margin-bottom-10">
                                                <span>
                                                    <asp:Literal Text="<%$Resources: Email_Address %>" runat="server" />
                                                </span>
                                                <div>
                                                    <asp:TextBox ID="client_email" CssClass="percent100" class="wide" runat="server" />
                                                </div>
                                            </div>

                                            <div class="margin-bottom-10">
                                                <span>
                                                    <asp:Literal Text="<%$Resources: Phone_Number %>" runat="server" /></span>
                                                <div>
                                                    <asp:TextBox ID="client_phoneNum" CssClass="percent100" runat="server" />
                                                </div>
                                            </div>

                                        </div>
                                        <div class="align-left percent23 form-group">
                                            <div class="margin-bottom-10">
                                                <span>
                                                    <asp:Literal Text="<%$Resources: ID_Number %>" runat="server" /></span>
                                                <div>
                                                    <asp:TextBox ID="client_idNum" CssClass="percent100" runat="server" />
                                                </div>
                                            </div>

                                            <div class="margin-bottom-10">
                                                <span>
                                                    <asp:Literal Text="<%$Resources: Billing_Address_1 %>" runat="server" /></span>
                                                <div>
                                                    <asp:TextBox ID="client_billaddress1" CssClass="percent100" runat="server" />
                                                </div>
                                            </div>

                                            <div class="margin-bottom-10">
                                                <span>
                                                    <asp:Literal Text="<%$Resources: Billing_Address_2 %>" runat="server" />
                                                </span>
                                                <div>
                                                    <asp:TextBox ID="client_billaddress2" CssClass="percent100" runat="server" />
                                                </div>
                                            </div>

                                        </div>
                                        <div class="align-left percent23 form-group">
                                            <div class="margin-bottom-10">
                                                <span>
                                                    <asp:Literal runat="server" Text="<%$Resources: Billing_Zipcode %>" /></span>
                                                <div>
                                                    <asp:TextBox ID="client_billzipcode" CssClass="percent100" runat="server" />
                                                </div>
                                            </div>
                                            <div class="margin-bottom-10">
                                                <span>
                                                    <asp:Literal runat="server" Text="<%$Resources: Billing_State %>" />
                                                </span>
                                                <div>
                                                    <asp:TextBox ID="client_billstate" CssClass="percent100" runat="server" />
                                                </div>
                                            </div>
                                            <div class="margin-bottom-10">
                                                <span>
                                                    <asp:Literal runat="server" Text="<%$Resources: Billing_Country %>" />
                                                </span>
                                                <div>
                                                    <asp:TextBox ID="client_billcountry" CssClass="percent100" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="align-left percent23 form-group">
                                            <div class="margin-bottom-10">
                                                <span>
                                                    <asp:Literal runat="server" Text="<%$Resources: Billing_City %>" /></span>
                                                <div>
                                                    <asp:TextBox ID="client_billcity" CssClass="percent100" runat="server" />
                                                </div>
                                            </div>

                                        </div>

                                        <div class="spacer"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="frame-border-option-inline padding-10 margin-top-10 margin-bottom-10">

                        <div class="alert alert-info margin-bottom-10">
                            <asp:Literal Text="<%$Resources: Create_Link %>" runat="server" />
                        </div>

                        <div class="align-left percent23 form-group">
                            <div class="margin-bottom-10">
                                <span>
                                    <asp:Literal Text="<%$Resources: Trans_Amount %>" runat="server" /></span>
                                <div>
                                    <input runat="server" onkeydown="return IsKeyDigit();" id="trans_amount" class="percent100" value="1.23" />
                                </div>
                            </div>
                        </div>

                        <div class="align-left percent23 form-group">
                            <div class="margin-bottom-10">
                                <span>
                                    <asp:Literal Text="<%$Resources: Currency %>" runat="server" /></span>
                                <div>
                                    <netpay:CurrencyDropDown ID="trans_currency" CssClass="percent100" runat="server" EnableBlankSelection="false" />
                                </div>
                            </div>
                        </div>

                        <div class="align-left percent23 form-group">
                            <div class="margin-bottom-10">
                                <span>
                                    <asp:Literal Text="<%$Resources: Installments %>" runat="server" /></span>
                                <div>
                                    <asp:DropDownList CssClass="percent100" ID="ddlInstallments" runat="server" />
                                </div>
                            </div>
                        </div>

                        <div class="spacer"></div>


                    </div>

                    <div class="wrap-edit-product" style="cursor: pointer; border-bottom: 1px solid #EDEDED; margin-bottom: 10px">
                        <div class="align-left">
                            <i class="fa fa-credit-card-alt"></i>
                            <asp:Literal Text="<%$Resources: Payment_Type %>" runat="server" />
                            <netpay:PageItemHelp ID="PageItemHelp4" Text="<%$Resources: PaymentTypeHelp %>" Width="300" runat="server" />
                        </div>
                        <div class="align-right">
                            <i class="fa fa-2x fa-angle-down" style=""></i>
                        </div>
                        <div class="spacer"></div>
                    </div>



                    <ul class="group-list">

                        <li>
                            <asp:CheckBox runat="server" ID="PaymentMedhodCC" value="CC" Checked="true" />
                            <asp:Literal Text="<%$Resources: CC_Credit %>" runat="server" /></li>
                        <li>
                            <asp:CheckBox runat="server" ID="PaymentMedhodEC" value="EC" />
                            <asp:Literal Text="<%$Resources: Echeck %>" runat="server" /></li>
                        <li>
                            <asp:CheckBox runat="server" ID="PaymentMedhodCS" value="CS" />
                            <asp:Literal Text="<%$Resources: Wallet %>" runat="server" />
                        </li>
                        <li>
                            <asp:CheckBox runat="server" ID="PaymentMedhodID" value="ID" />
                            <asp:Literal Text="<%$Resources: Bank_Transfer %>" runat="server" /></li>
                        <li>
                            <asp:CheckBox runat="server" ID="PaymentMedhodDD" value="DD" />
                            <asp:Literal Text="<%$Resources: European_Debit %>" runat="server" /></li>
                        <li>
                            <asp:CheckBox runat="server" ID="PaymentMedhodOB" value="OB" />
                            <asp:Literal Text="<%$Resources: Online_bank %>" runat="server" /></li>
                        <li>
                            <asp:CheckBox runat="server" ID="PaymentMedhodWM" value="WM" />
                            <asp:Literal Text="<%$Resources: Web_Money %>" runat="server" /></li>
                        <li>
                            <asp:CheckBox runat="server" ID="PaymentMedhodCUP" value="CUP" />
                            <asp:Literal Text="<%$Resources: China_union %>" runat="server" /></li>

                    </ul>
                    <div class="margin-top-10">
                        <i class="fa fa-globe"></i>
                        <asp:Literal Text="<%$Resources: Language_UI %>" runat="server" />
                    </div>
                    <div class="frame-border-option-inline padding-10 margin-top-10 margin-bottom-10">
                        <div class="align-left percent23">

                            <asp:DropDownList runat="server" CssClass="percent100" ID="disp_lng" />

                        </div>
                        <div class="spacer"></div>


                    </div>

                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:RadioButtonList runat="server" CssClass="group-list" RepeatLayout="UnorderedList" RepeatDirection="Vertical" ID="disp_lngType" OnSelectedIndexChanged="disp_lngType_OnSelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Selected="True" Text="<%$Resources: Show_lng %>"></asp:ListItem>
                                <asp:ListItem Text="<%$Resources: Hide_lng %>"></asp:ListItem>
                                <asp:ListItem Text="<%$Resources: Choose_lng %>"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:Panel ID="lngList_Disp" runat="server" Visible="false">
                                <div class="margin-top-10">
                                    <asp:CheckBoxList runat="server" ID="disp_lngList" RepeatDirection="Vertical" RepeatLayout="UnorderedList" ClientIDMode="Static" CssClass="group-list" Enabled="false" />
                                </div>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                
                 
                        <div class="margin-top-10">
                            <i class="fa fa-code"></i>
                            <asp:Literal runat="server" Text="<%$Resources: Generated_Code %>" /></div>
                            <div class="frame-border-option-inline padding-10 margin-top-10 margin-bottom-10">
                                <asp:UpdatePanel ChildrenAsTriggers="false" RenderMode="Block" UpdateMode="Conditional" runat="server">
                                    <ContentTemplate>
                                        <div runat="server" id="txQuery" class="wrap-code" visible="false" />
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnGenerateUrl" />
                                        <asp:AsyncPostBackTrigger ControlID="btnGeneraeForm" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                <div class="margin-bottom-10">
                                    <asp:Literal runat="server" Text="<%$Resources: Actions %>" />
                                    <asp:LinkButton runat="server" ID="btnGenerateUrl" OnClick="btnGenerateUrl_Click"  CssClass="btn btn-info btn-cons-short"><i class="fa fa-magic"></i> <asp:Literal runat="server" Text="<%$Resources: Refresh_Code_Url %>" /></asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnGeneraeForm" OnClick="btnGeneraeForm_Click"  CssClass="btn btn-info btn-cons-short"><i class="fa fa-code"></i> <asp:Literal runat="server" Text="<%$Resources: Refresh_Code_Form %>" /></asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnOpenPage" OnClick="btnOpenPage_Click" CssClass="btn btn-info btn-cons-short" UseSubmitBehavior="true"><i class="fa fa-external-link"></i> <asp:Literal runat="server" Text="<%$Resources: Open_Window %>" /></asp:LinkButton>
                                </div>
                                <asp:Literal ID="OpenScript" runat="server" Mode="PassThrough" ViewStateMode="Disabled" />
                                <div class="alert alert-info margin-top-10 margin-bottom-10">
                                    <asp:Literal runat="server" Text="<%$Resources: Refresh_Copy %>" />
                                </div>

                            </div>
                       
                        <asp:UpdatePanel ChildrenAsTriggers="false" RenderMode="Block" UpdateMode="Conditional" runat="server">
                            <ContentTemplate>
                                <asp:PlaceHolder runat="server" Visible="false" ID="phQrCode">
                                    <asp:Literal runat="server" Text="<%$Resources: QR_Code %>" />
                                    <div class="margin-bottom-10 frame-border-option">
                                        <div class="frame-border-option-inline padding-10 margin-top-10 margin-bottom-10">
                                            <div class="pull-left">
                                                <asp:Literal runat="server" Text="<%$Resources: QR_HPP %>" />
                                                <br />
                                                <i class="fa fa-barcode"></i>
                                                <asp:Literal runat="server" Text="<%$Resources: Iso %>" />
                                            </div>

                                            <div class="pull-right">
                                                <asp:Image runat="server" ID="imgQrCode" />
                                            </div>
                                            <div class="spacer"></div>
                                        </div>
                                    </div>
                                </asp:PlaceHolder>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnGenerateUrl" />
                                <asp:AsyncPostBackTrigger ControlID="btnGeneraeForm" />
                            </Triggers>
                        </asp:UpdatePanel>
                  
                </div>
            </div>
        </div>
    </div>
</asp:Content>
