﻿Imports Netpay.Bll
Imports Netpay.Web
Imports Netpay.Infrastructure

Partial Class Website_HostedPageV2_Wizard
    Inherits MasteredPage
    Dim RequestString As System.Text.StringBuilder = New System.Text.StringBuilder()
    Dim SignatureString As System.Text.StringBuilder = New System.Text.StringBuilder()
    Dim generateForm As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
        merchantID.Text = Merchant.Number

        If Not IsPostBack Then
            Dim options As Merchants.Hosted.PaymentPage = Merchants.Hosted.PaymentPage.Load(Bll.Merchants.Merchant.Current.ID)

            For i As Integer = 1 To 36
                ddlInstallments.Items.Add(i)
            Next

            ddlSkin.Items.Clear()
            ddlSkin.Items.Add(String.Empty)
            For Each sSkin As String In Bll.Merchants.Hosted.Customization.GetCustomizations().Split(",")
                ddlSkin.Items.Add(sSkin)
            Next

            disp_lngList.Items.Clear()
            Dim languages = System.Configuration.ConfigurationManager.AppSettings("HppV2Languages").Split(",")
            For Each Language In Bll.International.Language.Cache.Where(Function(l) languages.Contains(l.Culture))
                If Language.Name.ToLower() <> "hebrew" Or CurrentDomain.IsHebrewVisible Then
                    disp_lng.Items.Add(New ListItem(Language.NativeName, Language.Culture.ToLower()))
                    disp_lngList.Items.Add(New ListItem(Language.NativeName, Language.Culture.ToLower()))
                End If
            Next

            'Regenerate(False)
        End If
    End Sub

    Protected Sub SetText(text As String)
        txQuery.InnerText = text
        txQuery.Style.Remove("display")
        'btnHideQuery.Style.Remove("display")
    End Sub

    Protected Sub beginRequestData(genForm As Boolean)
        RequestString.Clear()
        SignatureString.Clear()
        generateForm = genForm
    End Sub

    Protected Sub addRequestParam(paramName As String, paramValue As String)
        If generateForm Then RequestString.AppendLine(String.Format("<input type=""hidden"" name=""{0}"" value=""{1}"" />", paramName, HttpUtility.HtmlEncode(paramValue))) _
        Else RequestString.Append(String.Format("&{0}={1}", paramName, HttpUtility.UrlEncode(paramValue)))
        SignatureString.Append(paramValue)
    End Sub

    Protected Function endRequestData(servicePath As String) As String
        Dim sb As New System.Text.StringBuilder()
        If generateForm Then
            sb.AppendLine("<form method=""post"" action=""" + servicePath + "/"">")
            sb.AppendLine(RequestString.ToString())
            sb.AppendLine("<input type=""submit"" value=""Send"">")
            sb.AppendLine("</form>")
        Else
            sb.Append(servicePath + "?")
            If RequestString.Length > 0 Then sb.Append(RequestString.ToString(1, RequestString.Length - 1))
        End If
        Return sb.ToString()
    End Function

    <System.Web.Services.WebMethod()>
    Protected Function GetOpenString(genForm As Boolean) As String
        beginRequestData(genForm)
        addRequestParam("merchantID", merchantID.Text)
        addRequestParam("url_redirect", url_redirect.Text)
        addRequestParam("url_notify", url_notify.Text)
        addRequestParam("trans_comment", trans_comment.Text)
        addRequestParam("trans_refNum", trans_refNum.Text)
        addRequestParam("trans_installments", ddlInstallments.SelectedValue)
        addRequestParam("trans_amount", trans_amount.Value)
        addRequestParam("trans_currency", Bll.Currency.Get(trans_currency.SelectedValue.ToInt32()).IsoCode)
        If Not String.IsNullOrEmpty(ddlSkin.SelectedValue) Then addRequestParam("skin_no", ddlSkin.SelectedValue)
        Dim paymentMethod As String = IIf(PaymentMedhodCC.Checked, ",CC", "") & IIf(PaymentMedhodEC.Checked, ",EC", "") & IIf(PaymentMedhodCS.Checked, ",CS", "") & IIf(PaymentMedhodID.Checked, ",ID", "") & IIf(PaymentMedhodDD.Checked, ",DD", "") & IIf(PaymentMedhodOB.Checked, ",OB", "") & IIf(PaymentMedhodWM.Checked, ",WM", "") & IIf(PaymentMedhodCUP.Checked, ",CUP", "")
        If (paymentMethod.Length > 0) Then paymentMethod = paymentMethod.Substring(1)
        addRequestParam("disp_paymentType", paymentMethod)
        addRequestParam("disp_payFor", disp_payFor.Text)
        If Not String.IsNullOrEmpty(trans_recurring1.Text) Then addRequestParam("trans_recurring1", trans_recurring1.Text)
        If Not String.IsNullOrEmpty(trans_recurring2.Text) Then addRequestParam("trans_recurring2", trans_recurring2.Text)
        If disp_recurring.SelectedItem IsNot Nothing Then addRequestParam("disp_recurring", disp_recurring.SelectedValue)
        If disp_lng.SelectedItem IsNot Nothing Then addRequestParam("disp_lng", disp_lng.SelectedValue)
        If Not String.IsNullOrEmpty(client_fullName.Text) Then addRequestParam("client_fullName", client_fullName.Text)
        If Not String.IsNullOrEmpty(client_email.Text) Then addRequestParam("client_email", client_email.Text)
        If Not String.IsNullOrEmpty(client_phoneNum.Text) Then addRequestParam("client_phoneNum", client_phoneNum.Text)
        If Not String.IsNullOrEmpty(client_idNum.Text) Then addRequestParam("client_idNum", client_idNum.Text)
        If Not String.IsNullOrEmpty(client_billaddress1.Text) Then addRequestParam("client_billaddress1", client_billaddress1.Text)
        If Not String.IsNullOrEmpty(client_billaddress2.Text) Then addRequestParam("client_billaddress2", client_billaddress2.Text)
        If Not String.IsNullOrEmpty(client_billcity.Text) Then addRequestParam("client_billcity", client_billcity.Text)
        If Not String.IsNullOrEmpty(client_billzipcode.Text) Then addRequestParam("client_billzipcode", client_billzipcode.Text)
        If Not String.IsNullOrEmpty(client_billstate.Text) Then addRequestParam("client_billstate", client_billstate.Text)
        If Not String.IsNullOrEmpty(client_billcountry.Text) Then addRequestParam("client_billcountry", client_billcountry.Text)
        If disp_lngType.SelectedIndex = 1 Then addRequestParam("disp_lngList", "hide")
        If disp_lngType.SelectedIndex = 2 Then
            Dim lngList As String = ""
            For Each item As ListItem In disp_lngList.Items
                If item.Selected Then lngList &= "," & item.Value
            Next
            If lngList.Length > 0 Then lngList = lngList.Substring(1)
            addRequestParam("disp_lngList", lngList)
        End If
        If Not String.IsNullOrEmpty(ddldisp_mobile.SelectedValue) Then addRequestParam("disp_mobile", ddldisp_mobile.SelectedValue)
        If (Request("DebugTest") = "1") Then addRequestParam("DebugTest", "1")
        If (Not String.IsNullOrEmpty(merchantID.Text) And (Not String.IsNullOrEmpty(ddlSignature.Text))) Then
            Dim merchant As Merchants.Merchant = Bll.Merchants.Merchant.Load(merchantID.Text)
            If (merchant IsNot Nothing) Then
                If merchant.HashKey IsNot Nothing Then
                    If merchant.HashKey.Trim() <> "" Then
                        Dim sign As String
                        If True Then sign = SignatureString.ToString() _
                        Else sign = merchantID.Text & trans_amount.Value & Bll.Currency.Get(trans_currency.SelectedValue.ToInt32(0)).IsoCode
                        sign &= merchant.HashKey
                        If ddlSignature.Text = "MD5" Then
                            Dim md As System.Security.Cryptography.MD5 = System.Security.Cryptography.MD5.Create()
                            sign = System.Convert.ToBase64String(md.ComputeHash(System.Text.Encoding.UTF8.GetBytes(sign.ToString())))
                        ElseIf (ddlSignature.Text = "SHA256") Then
                            Dim md As System.Security.Cryptography.SHA256 = System.Security.Cryptography.SHA256.Create()
                            sign = System.Convert.ToBase64String(md.ComputeHash(System.Text.Encoding.UTF8.GetBytes(sign.ToString())))
                        End If
                        addRequestParam("signature", sign)
                    End If
                End If
            End If
        End If
        Return endRequestData(WebUtils.CurrentDomain.ProcessV2Url & "hosted/")
    End Function

    Protected Sub Regenerate(genForm As Boolean)
        Dim hppUrl As String = GetOpenString(genForm)
        SetText(hppUrl)
        phQrCode.Visible = Not genForm
        If phQrCode.Visible Then
            hppUrl = hppUrl.Substring((WebUtils.CurrentDomain.ProcessV2Url & "hosted/?").Length)
            imgQrCode.ImageUrl = String.Format("{0}QrCodes.asmx/RenderGenerateCode?target=HostedPaymentPageV2&urlParams={1}", WebUtils.CurrentDomain.WebServicesUrl, HttpUtility.UrlEncode(hppUrl))
        End If
        txQuery.Visible = True
    End Sub

    Protected Sub btnGenerateUrl_Click(sender As Object, e As EventArgs)
        Regenerate(False)
    End Sub

    Protected Sub btnGeneraeForm_Click(sender As Object, e As EventArgs)
        Regenerate(True)
    End Sub

    Protected Sub btnOpenPage_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnOpenPage.Click
        OpenScript.Text = "<script type=""text/javascript"">window.open('" & GetOpenString(False) & "', 'fraPay', 'scrollbars=1, width=700, height=800, resizable=1, Status=1, top=200, left=250');</" & "script>"
    End Sub

    Protected Sub disp_lngType_OnSelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles disp_lngType.SelectedIndexChanged
        disp_lngList.Enabled = (disp_lngType.SelectedIndex = 2)
        disp_lngList.Visible = (disp_lngType.SelectedIndex = 2)
        lngList_Disp.Visible = True

    End Sub
End Class
