﻿Imports System.Collections.Generic
Imports Netpay.Bll.Reports
Imports Netpay.Bll
Imports Netpay.Infrastructure
Imports Netpay.Web
Imports Netpay.Web.Controls

Partial Class Website_RecurringSeries
    Inherits MasteredPage
	Dim isSingleSearch As Boolean
    
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PopupButton1.Text = GetGlobalResourceObject("Multilang", "Legend").ToString().ToUpper()
        If Not IsPostBack Then
            wcDateRangePicker.FromDate = DateTime.Now.Date.AddMonths(-1)
            wcDateRangePicker.ToDate = DateTime.Now

            ddStatus.DataSource = Enums.GetEnumDataSource(Of Bll.Transactions.Recurring.SeriesStatus)()
            ddStatus.DataTextField = "Key"
            ddStatus.DataValueField = "Value"
            ddStatus.DataBind()

            If dbPages.TestVar(Request("ID"), 1, 0, 0) > 0 Then
                txtSeriesID.Text = dbPages.TestVar(Request("ID"), 1, 0, 0)
                wcDateRangePicker.FromDate = Nothing
                wcDateRangePicker.ToDate = Nothing
                btnSearchSingle_Click(buttonSearchSingle, EventArgs.Empty)
            End If
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles buttonSearch.Click
		isSingleSearch = False 
        OnPageChanged(sender, e)
        hdLastSearchMode.Value = "multi"
    End Sub

    Protected Sub btnSearchSingle_Click(ByVal sender As Object, ByVal e As EventArgs) Handles buttonSearchSingle.Click
		isSingleSearch = true 
        OnPageChanged(sender, e)
        hdLastSearchMode.Value = "single"
    End Sub

    Protected Sub OnPageChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim filters = New Bll.Transactions.Recurring.Series.SearchFilters()
        If isSingleSearch Then
            If txtSeriesID.Text.Trim().Length = 0 Then
                Return
            End If
            filters.ID = New Infrastructure.Range(Of Integer?)(txtSeriesID.Text.ToNullableInt32())
        Else
            filters.Date.From = wcDateRangePicker.FromDate
            filters.Date.To = wcDateRangePicker.ToDate
            filters.paymentMethodID = ddPaymentMethod.SelectedValue.ToNullableInt32()
            filters.currencyID = ddCurrency.SelectedValue.ToNullableInt32()
            filters.Amount.From = filters.Amount.To = txtAmount.Text.ToNullableDecimal()
            Dim statusID As Nullable(Of Int32) = ddStatus.SelectedValue.ToNullableInt32()
            If (statusID IsNot Nothing) Then filters.Status = [Enum].Parse(GetType(Bll.Transactions.Recurring.SeriesStatus), statusID.Value.ToString())
        End If

        If TypeOf e Is ExcelButton.ExportEventArgs Then
            wcFiltersView.LoadFilters(filters)
            btnExportExcel.SetExportInfo(wcFiltersView.RenderText(), ReportType.MerchantRecurringSeries, Function(pi) Bll.Transactions.Recurring.Series.Search(filters, pi))
        Else
            Dim results As List(Of Bll.Transactions.Recurring.Series) = Netpay.Bll.Transactions.Recurring.Series.Search(filters, pager.Info)
            If results.Count > 0 Then
                repeaterResults.DataSource = results
                repeaterResults.DataBind()
                btnExportExcel.ResultCount = pager.Info.RowCount
                phResults.Visible = True
            Else
                phResults.Visible = False
            End If
        End If
    End Sub

    Protected Sub SuspendSeries(ByVal sender As Object, ByVal e As CommandEventArgs)
        Dim seriesID As Integer = Integer.Parse(e.CommandArgument)
        Bll.Transactions.Recurring.Series.Load(seriesID).Suspend()
		isSingleSearch = isSingleMode() 
        OnPageChanged(sender, e)
    End Sub

    Protected Sub ResumeSeries(ByVal sender As Object, ByVal e As CommandEventArgs)
        Dim seriesID As Integer = Integer.Parse(e.CommandArgument)
        Bll.Transactions.Recurring.Series.Load(seriesID).Resume()
		isSingleSearch = isSingleMode() 
        OnPageChanged(sender, e)
    End Sub

    Protected Sub DeleteSeries(ByVal sender As Object, ByVal e As CommandEventArgs)
        Dim seriesID As Integer = Integer.Parse(e.CommandArgument)
        Bll.Transactions.Recurring.Series.Load(seriesID).Delete()
		isSingleSearch = isSingleMode() 
        OnPageChanged(sender, e)
    End Sub

    Private Function isSingleMode() As Boolean
        If hdLastSearchMode.Value = "single" Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub ExportExcel_Click(ByVal sender As Object, ByVal e As EventArgs)
		isSingleSearch = isSingleMode() 
        OnPageChanged(sender, e)
        btnExportExcel.Export()
    End Sub

    Protected Function GetChargesText(ByVal series As Bll.Transactions.Recurring.Series) As String
        Dim result As String = ""
        If series.IsFlexible Then
            result = GetLocalResourceObject("ChargesFlexibleInterval")
        Else
            Dim intervalUnit As String = Infrastructure.GlobalData.GetValue(GlobalDataGroup.IntervalUnit, WebUtils.CurrentLanguage, CInt(series.IntervalUnit)).Value
            result = String.Format(GetLocalResourceObject("ChargesPerIntervalUnit"), series.Charges, series.IntervalCount, intervalUnit)
        End If

        Return result
    End Function

    Protected Sub ResultsItemDataBound(ByVal Sender As Object, ByVal e As RepeaterItemEventArgs)
        Dim series = CType(e.Item.DataItem, Bll.Transactions.Recurring.Series)
        Dim suspendButton As ImageButton = e.Item.FindControl("btnSuspend")
        Dim resumeButton As ImageButton = e.Item.FindControl("btnResume")
        Dim deleteButton As ImageButton = e.Item.FindControl("btnDelete")
        If series.Paid Then
            suspendButton.Visible = False
            resumeButton.Visible = False
        Else
            resumeButton.Visible = series.Suspended
            suspendButton.Visible = Not resumeButton.Visible
        End If
        deleteButton.ImageUrl = "/NPCommon/Images/iconB_delete" & IIf(series.Deleted, "Dim", String.Empty) & ".png"
        deleteButton.Enabled = Not series.Deleted
    End Sub

    Protected Function GetStatusClass(ByVal series As Bll.Transactions.Recurring.Series) As String
        If series.IsPrecreated Then
            Return "statusPrecreated"
        ElseIf series.Paid Then
            Return "statusPaid"
        ElseIf series.Blocked Then
            Return "statusBlocked"
        ElseIf series.Suspended Then
            Return "statusSuspended"
        Else
            Return "statusActive"
        End If
    End Function

    Protected Function GetTypeText(ByVal isPreAuthorized As Boolean) As String
        If isPreAuthorized Then
            Return GetGlobalResourceObject("MultiLang", "PreAuth")
        Else
            Return GetGlobalResourceObject("MultiLang", "Debit")
        End If
    End Function
End Class
