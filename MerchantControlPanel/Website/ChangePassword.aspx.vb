﻿Imports Netpay.Infrastructure
Imports Netpay.Web

Partial Class Website_ChangePassword
	Inherits MasteredPage

	Sub btnChangePassword_OnClick(sender As Object, e As EventArgs)
		If Bll.Accounts.LimitedLogin.Current IsNot Nothing Then
			Response.Redirect("", True)
			Return
		End If

		Dim result As Infrastructure.Security.Login.ChangePasswordResult
		Dim currentPassword As String = txtCurrentPassword.Text
		Dim newPassword As String = txtNewPassword.Text
		Dim newPasswordConfirm As String = txtNewPasswordConfirm.Text
		Dim clientIP As String = Request.Params("REMOTE_ADDR")
		If(newPassword <> newPasswordConfirm) Then result = Infrastructure.Security.Login.ChangePasswordResult.PasswordConfirmationMissmatch _
		Else result = LoggedUser.ChangePassword(currentPassword, newPassword, clientIP)

        lblError.Text = ""
        lblError.Visible = False
        lblSuccess.Text = ""
        lblSuccess.Visible = False
		Select Case result
			Case Infrastructure.Security.Login.ChangePasswordResult.PasswordConfirmationMissmatch
                lblError.Text = "The new password does not match the new password confirmation."
                lblError.Visible = True
			Case Infrastructure.Security.Login.ChangePasswordResult.PasswordUsedInThePast
                lblError.Text = "This password has been used in the past. Please choose another password."
                lblError.Visible = True
			Case Infrastructure.Security.Login.ChangePasswordResult.Success
                lblSuccess.Text = "The password has been changed successfully."
                lblSuccess.Visible = True
			Case Infrastructure.Security.Login.ChangePasswordResult.WeakPassword
                lblError.Text = "The password must contain atleast 8 characters of which atleast 2 letters and 2 numbers."
                lblError.Visible = True
			Case Infrastructure.Security.Login.ChangePasswordResult.WrongCurrentPassword
                lblError.Text = "The current password is wrong."
                lblError.Visible = True
			Case Else
                lblError.Text = "Unknown result: " + result.ToString()
                lblError.Visible = True
		End Select
	End Sub
End Class
