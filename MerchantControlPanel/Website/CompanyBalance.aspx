﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" Inherits="Netpay.MerchantControlPanel.Website_CompanyBalance" Codebehind="CompanyBalance.aspx.vb" %>
<%@ Import Namespace="Netpay.MerchantControlPanel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
 <asp:Localize ID="locPageTitle" Text="<%$Resources: CompanyBalance.aspx, PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="bodyContent" ContentPlaceHolderID="cphBody" runat="server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$Resources: CompanyBalance.aspx, AccountBalance%>"
                runat="server" />
        </div>
        <div class="content-background">

            <div class="section">
       
                 <div class="alert-info margin-bottom-10"><i class="fa fa-info-circle"></i> <asp:Literal ID="Literal1" runat="server" Text="<%$Resources: CompanyBalance.aspx, SubTitle1 %>" /></div>
                 <div class="alert-info margin-bottom-10"><i class="fa fa-info-circle"></i> <asp:Literal ID="Literal2" runat="server" Text="<%$Resources: CompanyBalance.aspx, SubTitle2 %>" /></div>
                <div class="margin-top-10">
                    <div><asp:Literal runat="server" Text="<%$Resources: CompanyBalance.aspx, Choosecurrency %>" /></div>
                    <div><asp:DropDownList ID="ddlCurrencyBalance" runat="server" CssClass="percent40" AutoPostBack="true" /></div>
                </div>
                 
            </div>
            <div class="section">
                <table class="exspand-table">
                    <thead>
                        <asp:Literal runat="server" ID="ltAvalableBalace" Mode="PassThrough" />
                        <tr>
                            <th><asp:Literal runat="server" Text="<%$Resources: CompanyBalance.aspx, Date %>" /></th>
                            <th><asp:Literal runat="server" Text="<%$Resources: CompanyBalance.aspx, Action %>" /></th>
                            <th><asp:Literal runat="server" Text="<%$Resources: CompanyBalance.aspx, Credit %>" /></th>
                            <th><asp:Literal runat="server" Text="<%$Resources: CompanyBalance.aspx, Debit %>" /></th>
                            <th><asp:Literal runat="server" Text="<%$Resources: CompanyBalance.aspx, Balance %>" /></th>
                        </tr>
                    </thead>
                    <asp:PlaceHolder ID="phBalancePending" runat="server" Visible="false">
                        <thead><td colspan="6" style="background-color:#fff9e9;">Pending</td></thead>
                        <tbody>
                            <asp:Repeater ID="repeaterPending" EnableViewState="false" runat="server">
                                <ItemTemplate>
                                    <tr onmouseover="this.style.backgroundColor='#ececec';" onmouseout="this.style.backgroundColor='#ffffff';">
                                        <td><%# dbPages.FormatDatesTimes(Eval("InsertDate"), 1, 1, 0) %><br /></td>
                                        <td><asp:Literal ID="lblPayment" runat="server" Text='<%# FormatPayment(CType(Container.DataItem, Netpay.Bll.Accounts.Balance))%>' /></td>
                                        <td class="Numeric" style="text-align:right"><asp:Literal ID="Literal3" runat="server" Visible='<%# CType(Container.DataItem, Netpay.Bll.Accounts.Balance).Amount >= 0 %>' Text='<%# FormatAmount(CType(Container.DataItem, Netpay.Bll.Accounts.Balance).Amount, CType(Container.DataItem, Netpay.Bll.Accounts.Balance).CurrencyIso)%>' /></td>
                                        <td class="Numeric" style="text-align:right"><asp:Literal ID="Literal4" runat="server" Visible='<%# CType(Container.DataItem, Netpay.Bll.Accounts.Balance).Amount < 0 %>' Text='<%# FormatAmount(CType(Container.DataItem, Netpay.Bll.Accounts.Balance).Amount, CType(Container.DataItem, Netpay.Bll.Accounts.Balance).CurrencyIso)%>' /></td>
                                        <td class="Numeric" style="text-align:right">---</td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="phBalanceCurrent" runat="server" Visible="false">
                        <thead><td colspan="6" style="background-color:#fff9e9;">Current</td></thead>
                        <tbody>
                            <asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server">
                                <ItemTemplate>
                                    <tr onmouseover="this.style.backgroundColor='#ececec';" onmouseout="this.style.backgroundColor='#ffffff';">
                                        <td><%# dbPages.FormatDatesTimes(Eval("InsertDate"), 1, 1, 0) %><br /></td>
                                        <td><asp:Literal ID="lblPayment" runat="server" Text='<%# FormatPayment(CType(Container.DataItem, Netpay.Bll.Accounts.Balance))%>' /></td>
                                        <td class="Numeric" style="text-align:right"><asp:Literal ID="Literal3" runat="server" Visible='<%# CType(Container.DataItem, Netpay.Bll.Accounts.Balance).Amount >= 0 %>' Text='<%# FormatAmount(CType(Container.DataItem, Netpay.Bll.Accounts.Balance).Amount, CType(Container.DataItem, Netpay.Bll.Accounts.Balance).CurrencyIso)%>' /></td>
                                        <td class="Numeric" style="text-align:right"><asp:Literal ID="Literal4" runat="server" Visible='<%# CType(Container.DataItem, Netpay.Bll.Accounts.Balance).Amount < 0 %>' Text='<%# FormatAmount(CType(Container.DataItem, Netpay.Bll.Accounts.Balance).Amount, CType(Container.DataItem, Netpay.Bll.Accounts.Balance).CurrencyIso)%>' /></td>
                                        <td class="Numeric" style="text-align:right"><asp:Literal ID="Literal5" runat="server" Text='<%# FormatAmount(CType(Container.DataItem, Netpay.Bll.Accounts.Balance).Total, CType(Container.DataItem, Netpay.Bll.Accounts.Balance).CurrencyIso)%>' /></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </asp:PlaceHolder>
                </table>
                <netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
                <asp:PlaceHolder ID="phNoData" runat="server" Visible="false">
                    <asp:Literal ID="litNoData" runat="server" />
                </asp:PlaceHolder>
            </div>
        </div>
    </div>
</asp:Content>
