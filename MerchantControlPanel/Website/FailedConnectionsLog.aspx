﻿<%@ Page Title="PageTitle" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master"
    AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_FailedConnectionsLog"
    CodeBehind="FailedConnectionsLog.aspx.vb" %>

<%@ Import Namespace="Netpay.MerchantControlPanel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <script language="JavaScript" type="text/javascript">
        function ExpandNode(cellID) {
            trObj = document.getElementById("trInfo" + cellID)
            imgObj = document.getElementById("oListImg" + cellID)
            if (trObj) {
                if (trObj.style.display == '') {
                    imgObj.src = '/NPCommon/Images/tree_expand.gif';
                    trObj.style.display = 'none';
                }
                else {
                    imgObj.src = '/NPCommon/Images/tree_collapse.gif';
                    trObj.style.display = '';
                }
            }
        }  
    </script>
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$Resources:PageTitle%>" runat="server" /></div>
        <div class="content-background">
            <div class="section">
                <table width="40%">
                    <tr>
                        <td>
                            <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:MultiLang,InsertDates %>"></asp:Literal>
                            <netpay:DateRangePicker ID="wcDateRangePicker" HtmlLayout="Flow" Layout="Horizontal" runat="server" />
                        
                        </td>
                    </tr>
                </table>
                 <div class="wrap-button-bar">
                        <asp:LinkButton ID="BtnSearch" runat="server" CssClass="btn btn-cons-short btn-inverse" Text="<%$Resources:MultiLang,Search %>" OnClick="btnSearch_Click" />
                </div>
            </div>
			<asp:PlaceHolder runat="server" ID="phResult" Visible="false">
				<div class="section">
					<table class="exspand-table">
						<tr>
							
							<th>
								<asp:Literal ID="Literal2" runat="server" Text="<%$Resources:MultiLang,Date %>"></asp:Literal>
							</th>
							<th>
								<asp:Literal ID="Literal3" runat="server" Text="<%$Resources:MultiLang,TransactionSource %>"></asp:Literal>
							</th>
							<th>
								<asp:Literal ID="Literal4" runat="server" Text="<%$Resources:MultiLang,Amount %>"></asp:Literal>
							</th>
						</tr>
						<asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server">
							<ItemTemplate>
								<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='#FFFFFF';">
									
									<td><%# Eval("InsertDate", "{0:dd/MM/yyyy}") %></td>
									<td><%# GetTranactionSource(Eval("TransactionTypeID")) %></td>
									<td><%# CType(Eval("Amount"), Decimal).ToAmountFormat(CType(Eval("Currency"), Integer)) %></td>
								</tr>
								<!--				
								<tr id="trInfo<%#Eval("ID")%>" style="display:none;">
									<td></td>
									<td colspan="3">
										<table class="formNormal" style="width:93%; border-top:1px dashed #e2e2e2;" border="0">
										   <tr><td height="10" colspan="2"></td></tr>
										   <tr>
												<td style="width:150px;"><asp:Literal runat="server" Text="info title"></asp:Literal></td>
												<td style="text-align:left">info value</td>
											</tr>
										</table>
									</td>
								</tr>
								-->
							</ItemTemplate>
						</asp:Repeater>
					</table>
					<netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
				</div>
			</asp:PlaceHolder>
        </div>
    </div>
</asp:Content>
