﻿Imports System.Data.SqlClient
Imports Netpay.Infrastructure
Imports Netpay.CommonTypes
Imports Netpay.Bll
Imports Netpay.Web
Imports Netpay.Web.Controls
Imports System.Net

Partial Class Website_VirtualTerminalCC
    Inherits MasteredPage

    Dim bIsCcStorageEnabled As Boolean = False
    Dim sWhere As String
    Dim bRefund As Boolean
    Dim IssuanceFee As Decimal = 0D
    Dim strState As String = ""
    Protected isBillingAddressMust As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim processSettings As Bll.Merchants.ProcessSettings = Merchant.ProcessSettings
        If processSettings.IsSystemPayCreditCard Then wcElementBlocker.Visible = False
        If processSettings.IsCcStorage Then phCreditCardStorage.Visible = True
        If processSettings.IsCcStorage Then bIsCcStorageEnabled = True

        If Not processSettings.IsApprovalOnly Then
            rblTransactionType.Items.FindByValue("TransactionTypeAuthorization").Enabled = False
            rblTransactionType.Items.FindByValue("TransactionTypeCapture").Enabled = False
        End If

        CvvTip.Text = CType(GetGlobalResourceObject("MultiLang", "CvvTip"), String)
        IssuanceFee = dbPages.TestVar(dbPages.getConfig("IssuanceFee"), 0D, -1D, 0D)
        If Not Page.IsPostBack Then
            ddlsExpMonth.Items.Add(String.Empty)
            For i As Integer = 1 To 12
                ddlsExpMonth.Items.Add(i.ToString("00"))
            Next
            ddlsExpYear.Items.Add(String.Empty)
            For i As Integer = Date.Now.Year To Date.Now.Year + 11
                ddlsExpYear.Items.Add(i.ToString("0000"))
            Next

            For i As Integer = 2 To 36
                ddlPayment.Items.Add(i)
            Next

            DivUsa.Style.Add("display", "block")
            DivCanada.Style.Add("display", "none")
            Dim enableRefund As Boolean = False
            If processSettings IsNot Nothing Then
                enableRefund = processSettings.IsRefund
                litRequiredMail.Visible = processSettings.IsSystemPayEmail
                litRequiredCCHolderID.Visible = processSettings.IsSystemPayPersonalNumber
                litRequiredCCHolderPhone.Visible = processSettings.IsSystemPayPhoneNumber
                litRequiredIP.Visible = processSettings.IsRequiredClientIP
                ltCreditCardNumber.Visible = processSettings.IsSystemPayCreditCard
                ltCvv.Visible = processSettings.IsSystemPayCVV2

            End If

            litRequiredAddress1.Visible = False
            litRequiredCity.Visible = False
            litRequiredZipcode.Visible = False
            litRequiredCountry.Visible = False
            ltAddress.Visible = False
            ltOptionalAddress.Visible = True

            Dim riskSettings = Bll.Merchants.RiskSettings.Load(Merchant.ID)
            If riskSettings IsNot Nothing Then
                isBillingAddressMust = riskSettings.IsForceBillingAddress
                ltAddress.Visible = isBillingAddressMust
                ltOptionalAddress.Visible = Not isBillingAddressMust

                If isBillingAddressMust Then
                    litRequiredAddress1.Visible = True
                    litRequiredCity.Visible = True
                    litRequiredZipcode.Visible = True
                    litRequiredCountry.Visible = True

                    If riskSettings.IsForceBillingAddressOnlyZipcodeAndCountryAreRequired Then
                        litRequiredAddress1.Visible = False
                        litRequiredCity.Visible = False
                    End If
                End If
            End If

            Dim supportedCurrcies = Netpay.Bll.Merchants.Merchant.GetSupportedCurrencies(Merchant.ID)
            Dim supportedCurrciesIds = supportedCurrcies.Select(Function(c) c.ID).ToDelimitedString()
            ddlCurrency.IncludeCurrencyIDs = supportedCurrciesIds

            Dim hebItem = ddlCountry.Items.FindByValue("115")
            If Not Domain.IsHebrewVisible Then
                ddlCountry.Items.Remove(hebItem)
            Else
                If WebUtils.CurrentLanguage = Language.Hebrew Then
                    ddlCountry.Items.Remove(hebItem)
                    ddlCountry.Items.Insert(1, hebItem)
                End If
            End If

            ddlPayment.Enabled = True
            ddlCreditType.Value = CreditType.Regular
            ddlCreditType.Items.Remove(ddlCreditType.Items.FindByValue(CreditType.DelayedCharge))
            ddlCreditType.Items.Remove(ddlCreditType.Items.FindByValue(CreditType.CreditCharge))
            If Not enableRefund Then ddlCreditType.Items.Remove(ddlCreditType.Items.FindByValue(CreditType.Refund))

            If Request.QueryString.AllKeys.Contains("ccStorageId") Then
                Dim storageId = Request("ccStorageId").ToString().ToInt32(0)
                txStorageID.Text = storageId
                AddStorageCard(Nothing, Nothing)
                LoadStorage_Click(Nothing, Nothing)
            End If
        End If

        If ddlCreditType.SelectedValue.ToNullableInt32() = CreditType.Installments Then
            chkIsUseRecurring.Enabled = False
            chkIsUseRecurring.Checked = False
        Else
            chkIsUseRecurring.Enabled = True
        End If

        If Bll.Transactions.Recurring.MerchantSettings.Current.IsEnabled Then
            mvRecurring.ActiveViewIndex = 1
        Else
            mvRecurring.ActiveViewIndex = 0
        End If

        'ShowHideRecurring()
    End Sub

    Sub AddStorageCard(ByVal o As Object, ByVal e As System.EventArgs)
        BtnCUS.Visible = False
        phAddcardStorage.Visible = True
        phCreditCardStorage.Visible = False
        phStorageCard.Visible = False
    End Sub

    Protected Sub LoadStorage_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        phCreditCardStorage.Visible = False
        Dim creditCardStorage As Bll.Merchants.MethodStorage = Nothing
        If txStorageID.Enabled Then
            creditCardStorage = Bll.Merchants.MethodStorage.Load(Merchant.ID, txStorageID.Text.ToInt32(0))
        End If

        If creditCardStorage Is Nothing Then
            If txStorageID.Enabled Then lblErrorMessage.Text = "<div>" & GetGlobalResourceObject("VirtualTerminalCC.aspx", "StorageIDNotExist").ToString & "</div>"
            wcCCInput.Enabled = True
            txStorageID.Enabled = True
            txStorageID.Text = ""
            creditCardStorage = New Bll.Merchants.MethodStorage(Merchant.ID)
            btnLoadStorage.Text = Resources.VirtualTerminalCC.aspx.CardStorageLoad
            wcCCInput.Card = Nothing
            txtCHCvv2.Text = ""
        Else
            lblErrorMessage.Text = ""
            wcCCInput.Enabled = False
            txStorageID.Enabled = False
            btnLoadStorage.Text = Resources.VirtualTerminalCC.aspx.CardStorageReset
            wcCCInput.Card = New PaymentMethods.CreditCard With {.CardNumber = "************" & creditCardStorage.MethodInstance.Value1Last4.EmptyIfNull()}
            txtCHCvv2.Text = IIf(Not creditCardStorage.MethodInstance.HasValue2, "", "*****")
        End If

        If Not creditCardStorage.MethodInstance.ExpirationDate Is Nothing Then
            ddlsExpMonth.SelectedValue = creditCardStorage.MethodInstance.ExpirationMonth.ToString("00")
            If ddlsExpYear.Items.FindByValue(creditCardStorage.MethodInstance.ExpirationYear) IsNot Nothing Then _
               ddlsExpYear.SelectedValue = creditCardStorage.MethodInstance.ExpirationYear
        End If

        txtCHFullName.Text = creditCardStorage.Owner_FirstName + " " + creditCardStorage.Owner_LastName
        txtAddress1.Text = creditCardStorage.BA_Street1
        txtAddress2.Text = creditCardStorage.BA_Street2
        txtCity.Text = creditCardStorage.BA_City
        txtZipCode.Text = creditCardStorage.BA_PostalCode
        ddlCountry.ISOCode = creditCardStorage.BA_CountryISOCode
        'If creditCardStorage.CountryID = 228 Then
        ddlStateUsa.ISOCode = creditCardStorage.BA_StateISOCode
        'ElseIf creditCardStorage.CountryID = 43 Then
        ddlStateCanada.ISOCode = creditCardStorage.BA_StateISOCode
        'End If
        txtPhoneNumber.Text = creditCardStorage.Owner_PhoneNumber
        txtEmail.Text = creditCardStorage.Owner_EmailAddress
        txtCCHolderID.Text = creditCardStorage.Owner_PersonalNumber

    End Sub

    Protected Sub btnChargeAgain_Click(sender As Object, e As EventArgs)
        phVTForm.Visible = True
        divResult.Visible = False
    End Sub

    Private Function SetResult(isSuccess As Boolean)
        divResult.Visible = True

        If isSuccess Then
            phTransApproved.Visible = True
            phTransDecline.Visible = False
        Else
            phTransApproved.Visible = False
            phTransDecline.Visible = True
        End If
    End Function

    Protected Sub BtnCharge_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Page.Validate()
        If Not Page.IsValid Then Exit Sub

        phVTForm.Visible = False
        phCreditCardStorage.Visible = False
        If Not String.IsNullOrEmpty(hfTransID.Value) Then Exit Sub
        If Not String.IsNullOrEmpty(RecurringCount.Text) Then
            If Not Bll.Transactions.Recurring.MerchantSettings.Current.IsEnabled Then
                lblErrorMessage.Text = "<div>" & GetGlobalResourceObject("VirtualTerminalCC.aspx", "RecurringNotAllowed").ToString & "</div>"
                Exit Sub
            End If
        End If

        ddlStateUsa.Enabled = True
        ddlStateCanada.Enabled = True
        If (ddlCountry.SelectedValue = "228") Then ' Usa for state
            strState = ddlStateUsa.SelectedValue
            DivUsa.Style.Add("display", "block")
            DivCanada.Style.Add("display", "none")
        End If

        If (ddlCountry.SelectedValue = "43") Then   ' Canada for state
            strState = ddlStateCanada.SelectedValue
            DivCanada.Style.Add("display", "block")
            DivUsa.Style.Add("display", "none")
        End If

        Dim cardNum = wcCCInput.Card.CardNumber.ToString()
        lblErrorMessage.Text = ""
        If Not String.IsNullOrEmpty(txStorageID.Text) Then
            If Validation.IsNotSelected(ddlsExpMonth, lblErrorMessage, CType(GetGlobalResourceObject("MultiLang", "CCMonth"), String)) Then SetResult(False) : Exit Sub
            If Validation.IsNotSelected(ddlsExpYear, lblErrorMessage, CType(GetGlobalResourceObject("MultiLang", "CCYear"), String)) Then SetResult(False) : Exit Sub
            If Validation.IsPastDate(ddlsExpYear, ddlsExpMonth, , lblErrorMessage, CType(GetGlobalResourceObject("MultiLang", "CCValidity"), String)) Then SetResult(False) : Exit Sub

            If Validation.IsEmptyField(txtCHFullName, lblErrorMessage, CType(GetGlobalResourceObject("MultiLang", "CCHolderName"), String)) Then SetResult(False) : Exit Sub
            If Validation.IsEmptyField(txtCHCvv2, lblErrorMessage, CType(GetGlobalResourceObject("MultiLang", "CVV"), String)) Then SetResult(False) : Exit Sub
            If Validation.IsNotLimitedTo(txtCHCvv2, "1234567890*", lblErrorMessage, CType(GetGlobalResourceObject("MultiLang", "CVV"), String)) Then SetResult(False) : Exit Sub

            If litRequiredCCHolderPhone.Visible Then
                If Validation.IsEmptyField(txtPhoneNumber, lblErrorMessage, CType(GetGlobalResourceObject("MultiLang", "Phone"), String)) Then SetResult(False) : Exit Sub
            End If
            If litRequiredMail.Visible Then
                If Validation.IsEmptyField(txtEmail, lblErrorMessage, CType(GetGlobalResourceObject("MultiLang", "CCHolderMail"), String)) Then SetResult(False) : Exit Sub
                If Validation.IsNotMail(txtEmail, lblErrorMessage, CType(GetGlobalResourceObject("MultiLang", "CCHolderMail"), String)) Then SetResult(False) : Exit Sub
            End If
            If litRequiredCCHolderID.Visible Then
                If Validation.IsEmptyField(txtCCHolderID, lblErrorMessage, CType(GetGlobalResourceObject("MultiLang", "CCHolderID"), String)) Then SetResult(False) : Exit Sub
                If Validation.IsNotRegEx(txtCCHolderID, "^\d{9}$", lblErrorMessage, CType(GetGlobalResourceObject("MultiLang", "CCHolderID"), String)) Then SetResult(False) : Exit Sub
            End If
        End If

        If ltAddress.Visible Then
            If litRequiredCity.Visible Then
                If Validation.IsEmptyField(txtAddress1, lblErrorMessage, CType(GetGlobalResourceObject("MultiLang", "CCaddress"), String)) Then SetResult(False) : Exit Sub
                If Validation.IsEmptyField(txtCity, lblErrorMessage, CType(GetGlobalResourceObject("MultiLang", "CCity"), String)) Then SetResult(False) : Exit Sub
            End If
            If Validation.IsEmptyField(txtZipCode, lblErrorMessage, CType(GetGlobalResourceObject("MultiLang", "ZipCode"), String)) Then SetResult(False) : Exit Sub
        End If

        If ddlCreditType.SelectedValue = CreditType.Installments Then
            If Validation.IsNotSelected(ddlPayment, lblErrorMessage, CType(GetGlobalResourceObject("MultiLang", "Installments"), String)) Then SetResult(False) : Exit Sub
            If chkIsUseRecurring.Checked Then
                lblErrorMessage.Text = GetGlobalResourceObject("VirtualTerminalCC.aspx", "InstallmentsCannotBeRecurring")
                Exit Sub
            End If
        End If

        If Validation.IsEmptyField(txtAmount, lblErrorMessage, CType(GetGlobalResourceObject("MultiLang", "Amount"), String)) Then SetResult(False)
        If Validation.IsNotLimitedTo(txtAmount, "1234567890.", lblErrorMessage, CType(GetGlobalResourceObject("MultiLang", "Amount"), String)) Then SetResult(False) : Exit Sub

        Dim sRecurring1 As String = String.Empty
        If chkIsUseRecurring.Checked Then
            If Validation.IsNotSelected(RecurringMode, lblErrorMessage, CType(GetGlobalResourceObject("MultiLang", "IntervalUnit"), String)) Then SetResult(False) : Exit Sub
            If Validation.IsEmptyField(RecurringCycle, lblErrorMessage, CType(GetGlobalResourceObject("MultiLang", "IntervalLength"), String)) Then SetResult(False) : Exit Sub
            If Validation.IsNotLimitedTo(RecurringCycle, "1234567890", lblErrorMessage, CType(GetGlobalResourceObject("MultiLang", "IntervalLength"), String)) Then SetResult(False) : Exit Sub
            If Validation.IsEmptyField(RecurringCount, lblErrorMessage, CType(GetGlobalResourceObject("MultiLang", "NumberOfCharges"), String)) Then SetResult(False) : Exit Sub
            If Validation.IsNotLimitedTo(RecurringCount, "1234567890", lblErrorMessage, CType(GetGlobalResourceObject("MultiLang", "NumberOfCharges"), String)) Then SetResult(False) : Exit Sub
            sRecurring1 = RecurringCount.Text & RecurringMode.SelectedValue & RecurringCycle.Text
        End If

        Dim nTransType As Integer = rblTransactionType.SelectedIndex, sConfirmationNum As String = String.Empty, sTransApprovalID As String = String.Empty
        If nTransType = 3 Then 'confirmed by phone
            nTransType = 0
            sConfirmationNum = Server.UrlEncode(txtApprove.Text)
        ElseIf nTransType = 2 Then  'confirmed by phone
            sTransApprovalID = Server.UrlEncode(txtApprove.Text)
        ElseIf Not String.IsNullOrEmpty(txStorageID.Text) Then
            nTransType = 3
        End If
        Dim clientIp As String = HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_FOR")
        If (String.IsNullOrEmpty(clientIp)) Then clientIp = Request.ServerVariables("REMOTE_ADDR")

        Dim sendStr As String = "remote_charge2.aspx"
        'Dim sendStr As String = "remote_charge.asp"
        sendStr &= "?CompanyNum=" & Accounts.Account.Current.AccountNumber
        sendStr &= "&requestSource=" & IIf(WebUtils.CurrentLanguage = Language.Hebrew, 7, 18).ToString
        sendStr &= "&TransType=" & nTransType
        sendStr &= "&TypeCredit=" & ddlCreditType.SelectedItem.Value
        If Not String.IsNullOrEmpty(txStorageID.Text) Then
            sendStr &= "&ccStorageID=" & txStorageID.Text
            If txtCHCvv2.Text.IndexOf("*") = -1 Then sendStr &= "&CVV2=" & dbPages.TestVar(txtCHCvv2.Text, 8, "")
        Else
            sendStr &= "&CardNum=" & dbPages.TestVar(cardNum, 20, "")
            If smSwipe.HasTrackData Then
                sendStr &= "&Track2=" & Server.UrlEncode(dbPages.TestVar(smSwipe.TrackII, 50, ""))
            End If
            sendStr &= "&CVV2=" & dbPages.TestVar(txtCHCvv2.Text, 8, "")
            sendStr &= "&ExpMonth=" & dbPages.TestVar(ddlsExpMonth.SelectedValue, 1, 12, 1).ToString("00")
            sendStr &= "&ExpYear=" & (dbPages.TestVar(ddlsExpYear.SelectedValue, 2000, 2500, 1) - 2000).ToString("00")
        End If

        sendStr &= "&Currency=" & ddlCurrency.SelectedValue
        sendStr &= "&ClientIP=" & IIf(String.IsNullOrEmpty(txtClientIP.Text.Trim), clientIp, txtClientIP.Text)
        sendStr &= "&Amount=" & (txtAmount.Text + IssuanceFee).ToString("0.00")
        sendStr &= "&Member=" & Server.UrlEncode(txtCHFullName.Text)
        sendStr &= "&BillingAddress1=" & Server.UrlEncode(txtAddress1.Text)
        sendStr &= "&BillingAddress2=" & Server.UrlEncode(txtAddress2.Text)
        sendStr &= "&BillingCity=" & Server.UrlEncode(txtCity.Text)
        sendStr &= "&BillingZipCode=" & Server.UrlEncode(txtZipCode.Text)
        sendStr &= "&BillingState=" & Server.UrlEncode(strState)
        sendStr &= "&BillingCountry=" & Server.UrlEncode(ddlCountry.SelectedValue)
        sendStr &= "&PhoneNumber=" & Server.UrlEncode(txtPhoneNumber.Text)
        sendStr &= "&Email=" & Server.UrlEncode(txtEmail.Text)
        sendStr &= "&PersonalNum=" & Server.UrlEncode(txtCCHolderID.Text)
        sendStr &= "&Payments=" & ddlPayment.SelectedValue.ToInt32(1)
        If Not String.IsNullOrEmpty(sConfirmationNum) Then sendStr &= "&ConfirmationNum=" & sConfirmationNum
        If Not String.IsNullOrEmpty(sTransApprovalID) Then sendStr &= "&TransApprovalID=" & sTransApprovalID
        If Not String.IsNullOrEmpty(sRecurring1) Then sendStr &= "&Recurring1=" & sRecurring1

        sendStr &= "&Comment=" & Server.UrlEncode(txtComment.Text)

        If bIsCcStorageEnabled Then
            If ChkStorageCard.Checked Then
                sendStr &= "&StoreCc=" & Server.UrlEncode("1")
            Else
                sendStr &= "&StoreCc=" & Server.UrlEncode("0")
            End If
        End If

        Dim wc As WebClient = New WebClient()
        Try
            Dim result = wc.DownloadString(WebUtils.CurrentDomain.ProcessUrl & sendStr)
            Dim replyCode = dbPages.GetUrlValue(result, "Reply")
            Dim transactionId As Integer = dbPages.TestVar(dbPages.GetUrlValue(result, "TransID"), 0, -1, 0)
            Dim replyDesc = dbPages.GetUrlValue(result, "ReplyDesc").ToDecodedUrl()
            If replyCode = "000" Or replyCode = "001" Then
                hfTransID.Value = replyCode
                Dim messgae = "<div>" & GetGlobalResourceObject("VirtualTerminalCC.aspx", "TransactionSuccessful") & transactionId & "</div>"
                lblSuccessMessage.Text = messgae
                SetResult(True)
            Else
                Dim messgae = "<div>" & GetGlobalResourceObject("VirtualTerminalCC.aspx", "TransactionFailed") & " " & replyCode & "</div><div>" & GetGlobalResourceObject("VirtualTerminalCC.aspx", "ErrorDescription") & " " & replyDesc & "</div>"
                lblErrorMessage.Text = messgae
                SetResult(False)
            End If
        Catch ex As WebException
            Dim messgae = "<div>" & GetGlobalResourceObject("VirtualTerminalCC.aspx", "ErrorString") & ": " & GetGlobalResourceObject("VirtualTerminalCC.aspx", "ComunicationProblem") & "</div>"
            lblErrorMessage.Text = messgae
            SetResult(False)
        End Try
    End Sub

    Sub SetInstallmentsVisibility(ByVal o As Object, ByVal e As EventArgs)
        ddlPayment.Enabled = IIf(ddlCreditType.SelectedValue = CreditType.Installments, True, False)
        If ddlCreditType.SelectedValue = CreditType.Installments And chkIsUseRecurring.Checked Then
            chkIsUseRecurring.Checked = False
            'ShowHideRecurring()
        End If
    End Sub

    Sub SetTransactionType(ByVal o As Object, ByVal e As EventArgs)
        Select Case rblTransactionType.SelectedIndex
            Case 3
                pnlApproval.Visible = True
                litApprove.Text = GetGlobalResourceObject("VirtualTerminalCC.aspx", "PhoneConfirmationNumber")
                pihApprove.Text = GetGlobalResourceObject("VirtualTerminalCC.aspx", "PhoneConfirmationNumberToolTip")
                txtApprove.Focus()
            Case 2
                pnlApproval.Visible = True
                litApprove.Text = GetGlobalResourceObject("VirtualTerminalCC.aspx", "AuthTransactionNumber")
                pihApprove.Text = GetGlobalResourceObject("VirtualTerminalCC.aspx", "AuthTransactionNumberToolTip")
                txtApprove.Focus()
            Case Else
                pnlApproval.Visible = False
        End Select
    End Sub

    'Sub ShowHideRecurring(Optional ByVal o As Object = Nothing, Optional ByVal e As EventArgs = Nothing)
    '    If TypeOf (o) Is CheckBox And chkIsUseRecurring.Checked Then
    '        litRecurringNotAllowed.Text = String.Empty

    '        Dim settings = Bll.Transactions.Recurring.MerchantSettings.Current
    '        If settings Is Nothing OrElse Not settings.IsEnabled Then
    '            chkIsUseRecurring.Checked = False
    '            litRecurringNotAllowed.Text = "<div class=""error"" style=""margin:0px 0px 8px 0px;"">" & GetGlobalResourceObject("VirtualTerminalCC.aspx", "RecurringNotAllowed").ToString & "</div>"
    '        End If
    '    End If

    '    If chkIsUseRecurring.Checked Then
    '        RecurringCount.Enabled = True
    '        RecurringCycle.Enabled = True
    '        RecurringMode.Enabled = True
    '        lblRecurringCount.ForeColor = Drawing.Color.Black
    '        lblRecurringMode.ForeColor = Drawing.Color.Black
    '        lblRecurringRequiredCount.ForeColor = Drawing.Color.Maroon
    '        lblRecurringRequiredMode.ForeColor = Drawing.Color.Maroon
    '        RecurringCount.ForeColor = Drawing.Color.Black
    '        RecurringCycle.ForeColor = Drawing.Color.Black
    '        RecurringMode.ForeColor = Drawing.Color.Black
    '    Else
    '        RecurringCount.Enabled = False
    '        RecurringCycle.Enabled = False
    '        RecurringMode.Enabled = False
    '        lblRecurringCount.ForeColor = Drawing.Color.Gray
    '        lblRecurringMode.ForeColor = Drawing.Color.Gray
    '        lblRecurringRequiredCount.ForeColor = Drawing.Color.Gray
    '        lblRecurringRequiredMode.ForeColor = Drawing.Color.Gray
    '        RecurringCount.ForeColor = Drawing.Color.Gray
    '        RecurringCycle.ForeColor = Drawing.Color.Gray
    '        RecurringMode.ForeColor = Drawing.Color.Gray
    '    End If
    'End Sub
End Class
