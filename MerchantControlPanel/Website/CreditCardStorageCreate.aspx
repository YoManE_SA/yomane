﻿<%@ Page Title="PageTitle" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master"
    AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_CreditCardStorageCreate"
    CodeBehind="CreditCardStorageCreate.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <script type="text/javascript">
        function dopostddlstate() {
            var IndexValue = document.getElementById('ctl00_cphBody_ddlCountry').selectedIndex;
            var SelectedVal = document.getElementById('ctl00_cphBody_ddlCountry').options[IndexValue];
            var DivUsa = document.getElementById('ctl00_cphBody_DivUsa');
            var DivCanada = document.getElementById('ctl00_cphBody_DivCanada');

            if (SelectedVal.innerHTML == "United States") {
                document.getElementById('ctl00_cphBody_ddlStateUsa').disabled = false
                DivUsa.style.display = 'block';
                DivCanada.style.display = 'none';
            }
            else {
                document.getElementById('ctl00_cphBody_ddlStateUsa').disabled = true
                document.getElementById('ctl00_cphBody_ddlStateUsa').value = 0
                document.getElementById('ctl00_cphBody_ddlStateCanada').value = 0
            }

            if (SelectedVal.innerHTML == "Canada") {
                document.getElementById('ctl00_cphBody_ddlStateCanada').disabled = false
                DivUsa.style.display = 'none';
                DivCanada.style.display = 'block';
            }
            else {
                document.getElementById('ctl00_cphBody_ddlStateCanada').disabled = true
                document.getElementById('ctl00_cphBody_ddlStateCanada').value = 0
                document.getElementById('ctl00_cphBody_ddlStateUsa').value = 0
            }
        }
    </script>
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
    <a href="CreditCardStorage.aspx">
        <asp:Literal Text="<%$Resources:MultiLang, BackToList %>" runat="server" /></a>
    <asp:Label ID="lblResult" ForeColor="Red" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">
            <div class="section">

                <div class="margin-bottom-10 frame-border-option">
                    <div class="margin-bottom-10"><i class="fa fa-credit-card"></i> <asp:Literal Text="<%$Resources:MultiLang,CreditCardDetails %>" runat="server" /></div>
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">

                        <div class="percent33 align-left">
                            <div class="margin-bottom-10">
                                 <div><asp:Literal Text="<%$Resources:MultiLang,CreditCardNumber %>" runat="server" /> *</div>
                                 <div><netpay:CreditcardInput ID="wcCreditCard" CssClass="percent85" TabIndex="1" SingleFieldMode="true" runat="server" /></div>
                            </div>
                           <div class="margin-bottom-10">
                                  <div><asp:Literal Text="<%$Resources:MultiLang,CCHolderPhone  %>" runat="server" /></div>
                                  <div><asp:TextBox ID="txtCardholderPhoneNumber" runat="server" TabIndex="5" CssClass="percent85" /></div>
                           </div>
                            <div class="margin-bottom-10">
                                 <div><asp:Literal Text="<%$Resources:MultiLang,CCHolderIP  %>" runat="server" /></div>
                                 <div><asp:TextBox ID="txtClientIP" runat="server" CssClass="percent85" TabIndex="8" /></div>
                                 <div><span class="help">(<asp:Literal runat="server" Text="<%$Resources:MultiLang,NeededRisk %>" />)</span></div>
                            </div>
                        </div>

                        <div class="percent33 align-left">
                           
                            <div class="margin-bottom-10">
                                <div><asp:Literal Text="<%$Resources:MultiLang,CardExpirationDate  %>" runat="server" />*</div>
                                <div>
                                     <netpay:MonthDropDown ID="wcExpirationMonth" TabIndex="2" Width="100" runat="server" />
                                     <span class="line-height-35">&nbsp;/&nbsp;</span>
                                     <netpay:YearDropDown ID="wcExpirationYear" TabIndex="3" Width="100" runat="server" />
                                </div>
                            </div>
                           
                            <div class="margin-bottom-10">
                                  <div><asp:Literal Text="<%$Resources:MultiLang,CCHolderID  %>" runat="server" /></div>
                                  <div><asp:TextBox ID="txtCardholderID" runat="server" CssClass="percent85" TabIndex="6" /></div>
                            </div>
                        </div>

                        <div class="percent33 align-left">
                             <div class="margin-bottom-10">
                                 <div><asp:Literal Text="<%$Resources:MultiLang,CCHolderName  %>" runat="server" /> *</div>
                                 <div><asp:TextBox ID="txtCardholderFullName" runat="server" TabIndex="4" CssClass="percent85" /></div>
                            </div>
                            <div class="margin-bottom-10">
                                  <div><asp:Literal Text="<%$Resources:MultiLang,CCHolderMail  %>" runat="server" /></div>
                                  <div><asp:TextBox ID="txtCardholderEmail" runat="server" CssClass="percent85" TabIndex="7" /></div>
                            </div>
                        </div>
                        <div class="spacer"></div>
                    </div>
                    <div class="margin-bottom-10"><i class="fa fa-building"></i> <asp:Literal Text="<%$Resources:MultiLang,BillingAddress  %>" runat="server" /></div>
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">
                        <div class="percent33 align-left">
                            <div class="margin-bottom-10">
                                <div><asp:Literal Text="<%$Resources:MultiLang,Street  %>" runat="server" /> 1</div>
                                <div><asp:TextBox runat="server" ID="txtCardholderStreet1" CssClass="percent85" TabIndex="10" /></div>
                            </div>
                            <div class="margin-bottom-10">
                                <div><asp:Literal Text="<%$Resources:MultiLang,State  %>" runat="server" /></div>
                                <div>
                                    <div id="DivUsa" runat="server">
                                    <netpay:StateDropDown ID="ddlStateUsa" CountryID="228" Enabled="false" CssClass="percent85" TabIndex="12"  runat="server" />
                                    </div>
                                    <div id="DivCanada" runat="server">
                                    <netpay:StateDropDown ID="ddlStateCanada" CountryID="43" TabIndex="12" CssClass="percent85" runat="server" />
                                    </div>
                              </div>
                            </div>
                         </div>
                        <div class="percent33 align-left">
                            <div class="margin-bottom-10">
                                <div> <asp:Literal Text="<%$Resources:MultiLang,Street  %>" runat="server" /> 2
                        </div>
                                <div><asp:TextBox runat="server" ID="txtCardholderStreet2" CssClass="percent85" TabIndex="10" /></div>
                            </div>
                            <div class="margin-bottom-10">
                                <div><asp:Literal Text="<%$Resources:MultiLang,CCity  %>" runat="server" /></div>
                                <div><asp:TextBox ID="txtCardholderCity" CssClass="percent85" TabIndex="13" runat="server" /></div>
                            </div>
                         </div>
                        <div class="percent33 align-left">
                            <div class="margin-bottom-10">
                                <div><asp:Literal Text="<%$Resources:MultiLang,Country  %>" runat="server" /></div>
                                <div><netpay:CountryDropDown ID="ddlCountry" AutoPostBack="false" onchange="javascript:dopostddlstate()" TabIndex="11" CssClass="percent85" runat="server" /></div>
                            </div>
                            <div class="margin-bottom-10">
                                 <div><asp:Literal Text="<%$Resources:MultiLang,Zipcode  %>" runat="server" /></div>
                                 <div><asp:TextBox ID="txtCardholderZipcode" CssClass="percent85" runat="server" TabIndex="14" /></div>
                            </div>
                         </div>
                        <div class="spacer"></div>
                    </div>
                    <div class="margin-bottom-10"><i class="fa fa-comment"></i> <asp:Literal Text="<%$Resources:MultiLang,Comment  %>" runat="server" /></div>

                    <div class="frame-border-option-inline padding-10 margin-bottom-10">
                        <div class="margin-bottom-10">
                            <div><asp:Literal Text="<%$Resources:MultiLang,Comment  %>" runat="server" /> *</div>
                            <div><asp:TextBox ID="txtComment" Rows="2" TextMode="MultiLine" CssClass="percent100 percent-height-10" runat="server" TabIndex="15" /></div>    
                        </div>
                  
                </div>
                      <div class="text-align-right align-right">
                        <asp:LinkButton ID="btnCreate" CssClass="btn btn-cons-short btn-success" TabIndex="16" OnClick="CreateStoredCard" runat="server">
                        <i class="fa fa-floppy-o"></i> <asp:Literal Text="<%$Resources:MultiLang,StoreCard  %>" runat="server" />
                        </asp:LinkButton>
                    </div>
                    <div class="align-left">
                        <div class="require">
                            * <asp:Literal Text="<%$Resources:MultiLang,RequiredFields %>" runat="server" />
                        </div>
                    </div>
                    <div class="spacer"></div>
            </div>
        </div>
        </div>
      </div>
</asp:Content>
