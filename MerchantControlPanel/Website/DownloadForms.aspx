﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" Inherits="Netpay.MerchantControlPanel.Website_DownloadForms" CodeBehind="DownloadForms.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>
<%@ Import Namespace="Netpay.MerchantControlPanel" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">
            <div class="section">
                <ul>
                    <li>Authorization for bank debiting (Hebrew) - (1.04 Mb) <a href="https://quicksell.netpay-intl.com/NPCommon/NPForms/BankDebiting01_Heb.pdf">
                        <img src="Images/pdf.png" /></a> </li>
                    <li>Bank Debit Authorization form (US Banking system) - (0.18 Mb) <a href="https://quicksell.netpay-intl.com/NPCommon/NPForms/Netpay_Bank_Authorization_eCheck_Form.pdf">
                        <img src="Images/pdf.png" /></a> </li>
                </ul>
            </div>
        </div>
    </div>
</asp:Content>
