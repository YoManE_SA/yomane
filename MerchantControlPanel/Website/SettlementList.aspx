﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_SettlementList" Codebehind="SettlementList.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
    <netpay:ExcelButton ID="btnExportExcel" Text="<%$Resources:MultiLang,ExportToXls %>" Enabled="false" IconImageOn="xls.png" IconImageOff="xls_off.png" runat="server" OnClick="ExportExcel_Click" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">
            <div class="legend-menu">
                <div class="align-right">
                    <netpay:PopupButton ID="PopupButton1" IconSrc="Images/iconLegend.gif"
                        Width="100" Height="150" Text="<%$Resources:MultiLang,Legend  %>" runat="server">
                        <netpay:Legend Capture="true" Chargeback="true" Admin="true" System="true" Test="true"
                            runat="server" ID="ttLegend" />
                    </netpay:PopupButton>
                </div>
            </div>
            <div class="section">  <h3><asp:Literal ID="ltSettlement" runat="server" />
                    &nbsp;&nbsp;
                    <asp:Literal ID="ltTime" runat="server" /></h3>
                </div>
            <div class="section">
                <netpay:SearchFiltersView ID="wcFiltersView" runat="server" Visible="false" />
                <table class="exspand-table">
                    <tr>
                        <th colspan="2">
                            &nbsp;
                        </th>
                        <th>
                            <asp:Literal Text="<%$Resources:MultiLang,Transaction %>" runat="server"></asp:Literal>
                        </th>
                        <th>
                            <asp:Literal Text="<%$Resources:MultiLang,Date %>" runat="server"></asp:Literal>
                        </th>
                        <th>
                            <asp:Literal Text="<%$Resources:MultiLang,PaymentMethod %>" runat="server"></asp:Literal>
                        </th>
                        <th>
                            <asp:Literal Text="<%$Resources:MultiLang,CreditType %>" runat="server"></asp:Literal>
                        </th>
                        <th>
                            <asp:Literal Text="<%$Resources:MultiLang,Amount %>" runat="server"></asp:Literal>
                        </th>
                    </tr>
                    <asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server">
                        <ItemTemplate>
                            <netpay:MerchantTransactionRowView ID="ucTransactionView" Transaction="<%# CType(Container.DataItem, Transactions.Transaction) %>"
                                SettlementContext="<%# GetSettlemntContext() %>" ShowCreditType="true" ShowStatus="false"
                                runat="server" />
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
                <!-- Export To excel -->
                <!-- <div class="xls-wrap">
                    <netpay:ExcelButton ID="ExcelButton1" Text="<%$Resources:MultiLang,ExportToXls %>"
                        Enabled="false" IconImageOn="xls.png" IconImageOff="xls_off.png" runat="server" OnClick="ExportExcel_Click" />
                </div> -->
                <div class="align-right">
                <netpay:ShowTotalsButton ID="ucShowTotalsButton" runat="server" /></div>
                <div class="spacer"></div>
                <netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
            </div>
        </div>
    </div>
</asp:Content>
