﻿Imports System.IO
Imports System.Net.Mail
Imports System.Net
Imports Netpay.Web.Controls
Imports Netpay.Web
Imports Netpay.Infrastructure

Partial Class Website_ContactUs
	Inherits MasteredPage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim contentFileName = Netpay.Bll.Content.GetPrivateAccessPath("Site Content/MCP/Content/SiteContact.xml")
        'Label1.Text = dbPages.GetFileText(contentFileName)
        phContactus.DataBind()
    End Sub

    Protected Sub btnContactSend_Click(ByVal sender As Object, ByVal e As System.EventArgs)
		lblResult.Text = String.Empty
		If Validation.IsEmptyField(txtFullName, lblResult, CType(GetLocalResourceObject("FullName"), String)) Then Exit Sub
		If Validation.IsNotSelected(ddlCountry, lblResult, CType(GetLocalResourceObject("Country"), String)) Then Exit Sub
		If Validation.IsEmptyField(txtEmail, lblResult, CType(GetLocalResourceObject("EmailAddress"), String)) Then Exit Sub
		If Validation.IsNotMail(txtEmail, lblResult, CType(GetLocalResourceObject("EmailAddress"), String)) Then Exit Sub
		If Validation.IsEmptyField(txtPhone, lblResult, CType(GetLocalResourceObject("Phone"), String)) Then Exit Sub
		If Validation.IsEmptyField(txtSubject, lblResult, CType(GetLocalResourceObject("Subject"), String)) Then Exit Sub
		If Validation.IsEmptyField(txtMessage, lblResult, CType(GetLocalResourceObject("Message"), String)) Then Exit Sub
        If Validation.IsNotCaptcha(txtCaptcha, Captcha3.Validate(txtCaptcha.Text), lblResult) Then Exit Sub

        'Dim sbForms As New StringBuilder
        'sbForms.Append(CurrentDomain.FormsInboxUrl & "&Form=1")	'1 = "contact us" form
        'sbForms.Append("&IP=" & Request.ServerVariables("REMOTE_ADDR"))
        'sbForms.Append("&SendDateTime=" & Date.Now.ToString)
        'sbForms.Append("&Language=" & WebUtils.CurrentLanguageFull)
        'sbForms.Append("&FullName=" & txtFullName.Text)
        'sbForms.Append("&Country=" & ddlCountry.SelectedItem.Text)
        'sbForms.Append("&Email=" & txtEmail.Text)
        'sbForms.Append("&Phone=" & txtPhone.Text)
        'sbForms.Append("&Subject=" & txtSubject.Text)
        'sbForms.Append("&Message=" & txtMessage.Text)
        'Dim sOutput As String = String.Empty
        'Try
        '	Dim hwrForms As HttpWebRequest = WebRequest.Create(sbForms.ToString)
        '	hwrForms.Method = "GET"
        '	Dim srForms As New StreamReader(hwrForms.GetResponse.GetResponseStream)
        '	sOutput = srForms.ReadToEnd
        'Catch ex As Exception
        '	lblResult.Text &= "<div class=""error"">" & CType(GetGlobalResourceObject("MultiLang", "Error"), String) & ex.Message & "</div>"
        'End Try
        'If sOutput <> String.Empty Then lblResult.Text &= "<div class=""noError"">" & CType(GetLocalResourceObject("YourRequestNumberIs"), String).Replace("%NUMBER%", sOutput.Substring(sOutput.LastIndexOf("<hr />") + 6, sOutput.Length - sOutput.LastIndexOf("<hr />") - 6)) & "</div>"

        'If 1 = 0 Then
        '	Dim sbXML As New StringBuilder
        '	sbXML.Append("<?xml version=""1.0"" encoding=""UTF-8"" ?><message>")
        '	sbXML.Append("<form_type display_name=""Form"">Contact</form_type>")
        '	sbXML.Append("<language display_name=""Language""><![CDATA[" & Netpay.Web.WebUtils.CurrentLanguageFull & "]]></language>")
        '	sbXML.Append("<full_name display_name=""Full Name""><![CDATA[" & txtFullName.Text & "]]></full_name>")
        '	sbXML.Append("<country display_name=""Country""><![CDATA[" & ddlCountry.SelectedItem.Text & "]]></country>")
        '	sbXML.Append("<phone display_name=""Phone""><![CDATA[" & txtPhone.Text & "]]></phone>")
        '	sbXML.Append("<email display_name=""E-mail""><![CDATA[" & txtEmail.Text & "]]></email>")
        '	sbXML.Append("<country display_name=""Subject""><![CDATA[" & txtSubject.Text & "]]></country>")
        '	sbXML.Append("<date_time display_name=""Sent On""><![CDATA[" & Now() & "]]></date_time>")
        '	sbXML.Append("<ip_address display_name=""IP Address""><![CDATA[" & Request.ServerVariables("REMOTE_ADDR") & "]]></ip_address>")
        '	sbXML.Append("<message display_name=""""><![CDATA[" & txtMessage.Text & "]]></message>")
        '	sbXML.Append("</message>")
        '	Dim sPathXML As String
        '	sPathXML = Server.MapPath("../Data/ContactsMsg_xml/" & Right(Year(Now()).ToString(), 4) & Right("0" + Month(Now()).ToString(), 2) & Right("0" + Day(Now()).ToString(), 2) & Right("0" + Hour(Now()).ToString(), 2) & Right("0" + Minute(Now()).ToString(), 2) & Right("0" + Second(Now()).ToString(), 2) & "_" & Request.ServerVariables("REMOTE_ADDR").Replace(".", "-") & ".xml")
        '	Dim swXML As New StreamWriter(sPathXML)
        '	swXML.Write(sbXML.ToString())
        '	swXML.Close()
        '	swXML.Dispose()
        'End If

        Dim sbSend As New StringBuilder
		sbSend.Append("<style>")
		sbSend.Append("th, td {text-align:" & CType(GetGlobalResourceObject("MultiLang", "CssAlign"), String) & ";}")
		sbSend.Append("table, div {font-family:Arial;font-size:12px;direction:" & CType(GetGlobalResourceObject("MultiLang", "CssDirection"), String) & ";}")
		sbSend.Append("</style>")
		sbSend.Append("<table cellpadding=""2"" cellspacing=""1"" border=""1"">")
		sbSend.Append("<tr><th>" & CType(GetLocalResourceObject("Form"), String) & "</th><td>" & CType(GetLocalResourceObject("ContactUs"), String) & "</td></tr>")
		sbSend.Append("<tr><th>" & CType(GetLocalResourceObject("Language"), String) & "</th><td>" & Netpay.Web.WebUtils.CurrentLanguageFull & "</td></tr>")
		sbSend.Append("<tr><th>" & CType(GetLocalResourceObject("FullName"), String) & "</th><td>" & txtFullName.Text & "</td></tr>")
		sbSend.Append("<tr><th>" & CType(GetLocalResourceObject("Country"), String) & "</th><td>" & ddlCountry.SelectedItem.Text & "</td></tr>")
		sbSend.Append("<tr><th>" & CType(GetLocalResourceObject("Phone"), String) & "</th><td>" & txtPhone.Text & "</td></tr>")
		sbSend.Append("<tr><th>" & CType(GetLocalResourceObject("EmailAddress"), String) & "</th><td>" & txtEmail.Text & "</td></tr>")
		sbSend.Append("<tr><th>" & CType(GetLocalResourceObject("Subject"), String) & "</th><td>" & txtSubject.Text & "</td></tr>")
		sbSend.Append("<tr><th>" & CType(GetLocalResourceObject("SentOn"), String) & "</th><td style=""direction:ltr;"">" & Now() & "</td></tr>")
		sbSend.Append("<tr><th>" & CType(GetLocalResourceObject("IPAddress"), String) & "</th><td>" & Request.ServerVariables("REMOTE_ADDR") & "</td></tr>")
		sbSend.Append("</table>")
		sbSend.Append("<hr /><div>" & Replace(txtMessage.Text, vbCrLf, "<br />") & "</div>")

		Try
			Dim sendAddressFrom As New MailAddress(WebUtils.CurrentDomain.MailAddressFrom, WebUtils.CurrentDomain.MailNameFrom)
			Dim sendAddressTo As New MailAddress(WebUtils.CurrentDomain.MailAddressTo, WebUtils.CurrentDomain.MailNameTo)
			Dim subject As String = CType(GetLocalResourceObject("ContactMailFromWebsite"), String).Replace("%BRAND%", CurrentDomain.BrandName)
			Email.SmtpClient.Send(sendAddressFrom, sendAddressTo, subject, sbSend.ToString(), txtEmail.Text, Nothing)
			lblResult.Text = "<div class=""noError"">" & CType(GetLocalResourceObject("MessageSent"), String) & "</div>"
		Catch ex As Exception
			Logger.Log(LogTag.MerchantControlPanel, ex)
			lblResult.Text = "<div class=""error"">" & CType(GetGlobalResourceObject("MultiLang", "Error"), String) & "</div>"
		End Try

		txtFullName.Text = ""
		ddlCountry.SelectedIndex = 0
		txtSubject.Text = ""
		txtPhone.Text = ""
		txtEmail.Text = ""
		txtMessage.Text = ""
	End Sub
End Class
