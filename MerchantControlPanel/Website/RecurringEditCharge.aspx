﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_RecurringEditCharge" Codebehind="RecurringEditCharge.aspx.vb" %>
<%@ Import Namespace="Netpay.MerchantControlPanel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
    <script language="javascript" type="text/javascript">
        function dopostddlstate() {

            var IndexValue = document.getElementById('<%=ddlCountry.ClientID %>').selectedIndex;
            var SelectedVal = document.getElementById('<%=ddlCountry.ClientID %>').options[IndexValue];
            var DivUsa = document.getElementById('<%=DivUsa.ClientID %>');
            var DivCanada = document.getElementById('<%=DivCanada.ClientID %>');

            if (SelectedVal.innerHTML == "United States") {
                document.getElementById('<%= ddlStateUsa.ClientID %>').disabled = false
                DivUsa.style.display = 'block';
                DivCanada.style.display = 'none';
            }
            else {
                document.getElementById('<%= ddlStateUsa.ClientID %>').disabled = true
                document.getElementById('<%= ddlStateUsa.ClientID %>').value = 0
                document.getElementById('<%= ddlStateCanada.ClientID %>').value = 0
            }

            if (SelectedVal.innerHTML == "Canada") {
                document.getElementById('<%= ddlStateCanada.ClientID %>').disabled = false
                DivUsa.style.display = 'none';
                DivCanada.style.display = 'block';
            }
            else {
                document.getElementById('<%= ddlStateCanada.ClientID %>').disabled = true
                document.getElementById('<%= ddlStateCanada.ClientID %>').value = 0
                document.getElementById('<%= ddlStateUsa.ClientID %>').value = 0
            }
        }

        function checkAllCancel() {
            if (aspnetForm.CcStorageID) {
                if (aspnetForm.CcStorageID.length) {
                    for (i = 0; i < aspnetForm.CcStorageID.length; i++)
                        if (aspnetForm.CcStorageID[i].checked == true) {
                            return true;
                        }
                }
                else {
                    if (aspnetForm.CcStorageID.checked == true) {
                        return true;
                    }
                }
            }
            alert('Please select cards!');
            return false;
        }
			 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="locheadPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">
                <div class="section">
                    <div class="margin-bottom-10 frame-border-option">
                        <div class="margin-bottom-10"><i class="fa fa-refresh"></i> <asp:Literal runat="server" Text="<%$Resources:MultiLang,ChargeDetails %>" /></div>
                        <div class="frame-border-option-inline padding-10 margin-bottom-10">
                             <div class="alert-info">
                            <asp:Literal Text="<%$Resources:MultiLang,agreement %>" runat="server" />
                            #<%= nChargeNumber %>
                            <asp:Literal Text="<%$Resources:MultiLang,inseries %>" runat="server" />
                            #<%= nSeries %>
                        </div>
                        </div>
                        <ul class="group-list margin-top-10">
                            <li>
                                <b><asp:Literal Text="<%$Resources:MultiLang,ChargeDate %>" runat="server" /></b> <%=dbPages.FormatDatesTimes(dtCharge, 1, 0, 0)%>
                            </li>
                            <li>
                                <b><asp:Literal Text="<%$Resources:MultiLang,ChargeNo %>" runat="server" /></b> <%= nChargeNumber %> <b><asp:Literal Text="<%$Resources:MultiLang,of %>" runat="server" /></b> <%=nChargeCount%> 
                            </li>
                            <li>
                                <b><asp:Literal Text="<%$Resources:MultiLang,DelayCharge %>" runat="server" /></b>
                            </li>
                        </ul>
                        <div class="frame-border-option-inline padding-10 margin-bottom-10 margin-top-10">
                             <div class="align-left percent25">
                                 <div><asp:Literal Text="<%$Resources: DelayLength %>" runat="server" /></div>
                                 <div><asp:DropDownList ID="ddlDelayLength" runat="server" CssClass="percent85" /></div>
                             </div>
                             <div class="align-left percent25">
                              <div><asp:Literal Text="<%$Resources: DelayUnit %>" runat="server" /></div>
                                 <div>
                                    <asp:DropDownList ID="ddlDelayUnit" runat="server" CssClass="percent85">
                                        <asp:ListItem Value="1" Text="<%$Resources:MultiLang,Day  %>"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="<%$Resources:MultiLang,Week  %>">></asp:ListItem>
                                        <asp:ListItem Value="3" Text="<%$Resources:MultiLang,Month  %>" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="<%$Resources:MultiLang,Year  %>"></asp:ListItem>
                                    </asp:DropDownList>
                                 </div>
                             </div>
                             <div class="align-left percent25">
                                 <div><asp:Literal Text="<%$Resources:MultiLang,Currency %>" runat="server" /></div>
                                 <div><netpay:CurrencyDropDown ID="ddlCurrency" CssClass="percent85" runat="server" BlankSelectionText="<%$Resources:MultiLang,Currency %>" /></div>
                             </div>
                             <div class="align-left percent25">
                                 <div><asp:Literal Text="<%$Resources:MultiLang,Amount %>" runat="server" /></div>
                                 <div><asp:TextBox ID="txtAmount" runat="server" CssClass="percent85"/></div>
                             </div>
                             <div class="spacer"></div>
                        </div>
                        <div class="margin-top-10 margin-bottom-10"><i class="fa fa-comment"></i> <asp:Literal Text="<%$Resources:MultiLang,Memo %>" runat="server" />  </div>
                        <div class="frame-border-option-inline padding-10 margin-bottom-10">
                    
                             <asp:TextBox ID="txtMemo" runat="server" TextMode="MultiLine"  CssClass="percent-height-10 percent100" />
                         </div>
                        <asp:Panel ID="pnlCardData" runat="server" Visible="true">
                             <div class="margin-bottom-10"><i class="fa fa-credit-card"></i> <asp:Literal ID="litCreditCardDetails" runat="server" Text="<%$Resources:MultiLang,CreditCardDetails %>" /></div>
                           <ul class="group-list">                      
                                <li><asp:Literal Text="<%$Resources:MultiLang,CreditCardNumber %>" runat="server" />*</li>
                                <li><asp:Literal ID="litCardProtected" runat="server" /></li>  
                                <li><asp:Literal Text="<%$Resources:MultiLang,Type %>" runat="server" /></li>
                                <li><asp:Literal ID="litPaymentMethod" runat="server" /></li>
                                <li><asp:Literal Text="<%$Resources:MultiLang,CCHolderName  %>" runat="server" />*</li>
                                <li><%=sMember %></li>
                                <li>
                                    <asp:Panel ID="pnlEcheck" runat="server" Visible="true">
                                    <asp:Literal Text="<%$Resources:MultiLang,PaymentMethod  %>" runat="server" />
                                    <img src="/NPCommon/ImgPaymentMethod/23X12/15.gif" alt="eCheck" align="middle" border="0" />
                                    <asp:Literal Text="<%$Resources:MultiLang,ECheck  %>" runat="server" />
                                    </asp:Panel>
                                 </li>
                           </ul>
                           <div class="frame-border-option-inline padding-10 margin-bottom-10 margin-top-10">
                                <div class="align-left percent33">
                                    <div><asp:Literal Text="<%$Resources:MultiLang,CVV  %>" runat="server" />*</div>
                                    <div><asp:TextBox ID="txtcc_cui" CssClass="percent85 margin-bottom-10" TabIndex="8" runat="server" /></div>
                                    <div><asp:Literal Text="<%$Resources:MultiLang,CardExpirationDate  %>" runat="server" />*</div>
                                    <div>
                                    <asp:DropDownList ID="ddlsExpMonth" TabIndex="2" runat="server" Width="110px" CssClass="percent85 margin-bottom-10" />
                                    /
                                    <asp:DropDownList ID="ddlsExpYear" TabIndex="3" runat="server" Width="110px" CssClass="percent85 margin-bottom-10" />
                                    </div>

                                    <div><asp:Literal Text="<%$Resources:MultiLang,CCity  %>" runat="server" /></div>
                                    <div><asp:TextBox ID="txtCHSCity" TabIndex="11" runat="server" CssClass="percent85 margin-bottom-10" /></div>

                                    <div><asp:Literal Text="<%$Resources:MultiLang,Zipcode  %>" runat="server" /></div>
                                    <div><asp:TextBox ID="txtZipcode" runat="server" TabIndex="14" CssClass="percent85 margin-bottom-10" /></div>
                                </div>
                                <div class="align-left percent33">
                                     <div><asp:Literal Text="<%$Resources:MultiLang,CCHolderMail  %>" runat="server" />*</div>
                                               <div><asp:TextBox ID="txtCHEmail" runat="server" TabIndex="7" CssClass="percent85 margin-bottom-10" /></div>

                                         <div><asp:Literal  Text="<%$Resources:MultiLang,Address1 %>" runat="server" /></div>                          
                                               <div><asp:TextBox ID="txtAddress1" runat="server" CssClass="percent85 margin-bottom-10" /></div>

                                               <div><asp:Literal Text="<%$Resources:MultiLang,Country  %>" runat="server" />*</div>
                                               <div><netpay:CountryDropDown ID="ddlCountry" runat="server" EnableBlankSelection="true" CssClass="percent85 margin-bottom-10" onchange="javascript:dopostddlstate()" AutoPostBack="false" TabIndex="12" /></div>
                            
                                </div>
                                <div class="align-left percent33">
                                      <div><asp:Literal Text="<%$Resources:MultiLang,CCHolderPhone  %>" runat="server" /></div>
                                      <div><asp:TextBox ID="txtCHPhoneNumber" runat="server" TabIndex="5" CssClass="percent85 margin-bottom-10" /></div>

                                       <div><asp:Literal Text="<%$Resources:MultiLang,Address2  %>" runat="server" /></div>
                                        <div><asp:TextBox ID="txtAddress2" runat="server" CssClass="percent85 margin-bottom-10" /></div>

                                        <asp:Literal Text="<%$Resources:MultiLang,State  %>" runat="server" />
                                        <span id="DivUsa" runat="server">
                                        <netpay:StateDropDown ID="ddlStateUsa" CssClass="percent85 margin-bottom-10" CountryID="228" TabIndex="13" runat="server" />
                                        </span>
                                        <span id="DivCanada" runat="server">
                                        <netpay:StateDropDown ID="ddlStateCanada" CssClass="percent85 margin-bottom-10" CountryID="43" TabIndex="13" runat="server" />
                                        </span>
                                </div>
                            <div class="spacer"></div>      
                           </div>
                        </asp:Panel>
                        <div class="margin-top-10 margin-bottom-10"><i class="fa fa-check"></i> <asp:Literal Text="<%$Resources:MultiLang,ApplyChanges %>" runat="server" /></div>
                        <ul class="group-list">
                            <li><input type="Radio" name="ApplyToAll" value="1" checked="checked" /> <asp:Literal Text="<%$Resources:ApplyAllchanges %>" runat="server" /></li>
                            <li><input type="Radio" name="ApplyToAll" value="0" /> <asp:Literal Text="<%$Resources:ApplyJustchanges %>" runat="server" /></li>
                        </ul>
                         <div class="align-left text-align-left margin-top-10">
                                <% If Request("main") = 1 Then%>
                                <input type="button" class="btn btn-primary btn-cons-short" value="<%=sBack %>" onclick="location.replace('RecurringChargesMain.aspx?future=<%= Request("future") %>');" />
                                <%Else%>
                                <input type="button" class="btn btn-primary btn-cons-short" value="<%=sBack %>" onclick="location.replace('RecurringCharges.aspx?seriesID=<%= nSeries %>');" />
                                <%End If%>
                            </div>
                            <div class="align-right text-align-right margin-top-10">
                                <asp:LinkButton ID="BtnUpdate" CssClass="btn btn-success btn-cons-short" TabIndex="15" OnClick="BtnUpdate_Click" runat="server"><i class="fa fa-floppy-o"></i> <asp:Literal runat="server" Text="<%$Resources:MultiLang,Save %>" /></asp:LinkButton>
                            </div>
                            <div class="spacer"></div>   
                          <div class="button-wrap margin-top-10">
                        <span class="require">*<asp:Literal Text="<%$Resources:MultiLang,RequiredFields %>" runat="server" /></span>
                    </div>
                    </div>
               </div> 
        </div>
    </div>
</asp:Content>
