﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Netpay.MerchantControlPanel.Website_UpdateAccount"
    MasterPageFile="~/Templates/Tmp_netpayintl/page.master" CodeBehind="UpdateAccount.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$ Resources:PageTitle %>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <asp:Label ID="lblError" CssClass="error" Style="display: block;" runat="server"
            Visible="false" />
        <asp:Label ID="lblSuccess" CssClass="noerror" Style="display: block;" runat="server"
            Visible="false" />
        <div class="top-nav">
            <asp:Localize Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">
            <div class="section">
                <div class="margin-bottom-10 frame-border-option">

                    <div class="margin-bottom-10">
                       <i class="fa fa-building"></i> <asp:Literal Text="<%$Resources:CompanyInfo%>" runat="server" />
                    </div>
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">
                        <div class="margin-bottom-10">
                            <asp:Literal Text="<%$Resources:CompanyName%>" runat="server" />: 
                    <asp:Literal ID="literalMerchantName" runat="server" />
                            *
                        </div>
                        <div class="align-left percent33">
                            <div>
                                <asp:Literal Text="<%$Resources:MultiLang,Address%>" runat="server" />
                                *
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtMerchantAddress" runat="server" CssClass="percent85" />
                            </div>
                            <div>
                                <asp:Literal Text="<%$Resources:MultiLang,City%>" runat="server" />
                                *
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtMerchantCity" runat="server" CssClass="percent85" />
                            </div>
                            <div>
                                <asp:Literal Text="<%$Resources:MultiLang,ZipCode%>" runat="server" />
                                *

                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtMerchantZIP" runat="server" CssClass="percent85" />
                            </div>
                        </div>

                        <div class="align-left percent33">
                            <div>
                                <asp:Literal Text="<%$Resources:MultiLang,Country%>" runat="server" />
                                *
                            </div>
                            <div class="margin-bottom-10">
                                <netpay:CountryDropDown ID="ddMerchantCountry" runat="server" CssClass="percent85" />

                            </div>
                            <div>
                                <asp:Literal Text="<%$Resources:MultiLang,WebSite%>" runat="server" />
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtUrl" runat="server" CssClass="percent85" />
                            </div>
                            <div>
                                <asp:Literal Text="<%$Resources:MultiLang,Phone%>" runat="server" />
                                *
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtMerchantPhone" runat="server" CssClass="percent85" />
                            </div>
                        </div>

                        <div class="align-left percent33">

                            <div>
                                <asp:Literal Text="<%$Resources:MultiLang,Fax%>" runat="server" />
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtMerchantFax" runat="server" CssClass="percent85" />
                            </div>

                            <div>
                                <asp:Literal Text="<%$Resources:SupportEmail%>" runat="server" />
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtMerchantSupportEmail" runat="server" CssClass="percent85" />
                            </div>
                            <div>
                                <asp:Literal Text="<%$Resources:SupportPhone%>" runat="server" />
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtMerchantSupportPhone" runat="server" CssClass="percent85" />
                            </div>
                        </div>
                        <div class="spacer"></div>
                    </div>
                    <div class="margin-bottom-10">
                         <i class="fa fa-user"></i> <asp:Literal Text="<%$Resources:MultiLang,PersonalInfo%>" runat="server" />
                    </div>
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">

                        <div class="align-left percent33">

                            <div>
                                <asp:Literal Text="<%$Resources:MultiLang,FirstName%>" runat="server" />
                                *
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtFirstName" runat="server" CssClass="percent85" />
                            </div>
                            <div>
                                <asp:Literal Text="<%$Resources:MultiLang,LastName%>" runat="server" />
                                *
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtLastName" runat="server" CssClass="percent85" />
                            </div>
                            <div>
                                <asp:Literal Text="<%$Resources:MultiLang,Address%>" runat="server" />
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtContactAddress" runat="server" CssClass="percent85" />
                            </div>

                        </div>


                        <div class="align-left percent33">

                            <div>
                                <asp:Literal Text="<%$Resources:MultiLang,City%>" runat="server" />
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtContactCity" runat="server" CssClass="percent85" />
                            </div>
                            <div>
                                <asp:Literal ID="Literal1" Text="<%$Resources:MultiLang,ZipCode%>" runat="server" />
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtContactZipcode" runat="server" CssClass="percent85" />
                            </div>
                            <div>
                                <asp:Literal Text="<%$Resources:MultiLang,Country%>" runat="server" />
                            </div>
                            <div class="margin-bottom-10">
                                <netpay:CountryDropDown ID="ddContactCountry" runat="server" CssClass="percent85" />
                            </div>

                        </div>

                        <div class="align-left percent33">
                            <div>
                                <asp:Literal Text="<%$Resources:MultiLang,Phone%>" runat="server" />
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtContactPhone" runat="server" CssClass="percent85" />
                            </div>
                            <div>
                                <asp:Literal Text="<%$Resources:MultiLang,Cellular%>" runat="server" />
                            </div>
                            <div class="margin-bottom-10">
                                <asp:TextBox ID="txtContactMobile" runat="server" CssClass="percent85" />
                            </div>


                        </div>
                        <div class="spacer"></div>
                    </div>
                    <div class="margin-bottom-10">
                        <i class="fa fa-sign-in"></i>
                        <asp:Literal Text="<%$Resources:LoginData%>" runat="server" />
                    </div>
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">

                        <div class="align-left percent33">

                            <div>
                                <asp:Literal Text="<%$Resources:MultiLang,EMail%>" runat="server" />
                                *
                            </div>
                            <div>
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="percent85" />
                            </div>
                        </div>
                        <div class="align-left percent33">

                            <div>
                                <asp:Literal Text="<%$Resources:MultiLang,UserName%>" runat="server" />
                                *
                            </div>
                            <div>
                                <asp:TextBox ID="txtUserName" CssClass="percent85" runat="server" />
                            </div>

                        </div>
                        <div class="align-left percent33">
                            <div>
                                <asp:Literal Text="<%$Resources:MultiLang,Password%>" runat="server" />
                                *  <a id="A1" href="~/Website/ChangePassword.aspx" class="help" runat="server">
                                    <asp:Literal ID="Literal3" Text="<%$Resources:ChangePasswordText%>" runat="server" /></a>
                            </div>
                            <div>
                                <input type="text" readonly="readonly" value="********" class="percent85" />
                            </div>
                        </div>

                        <div class="spacer"></div>

                    </div>
                    <div>
                        <div class="require align-left">
                            *
                            <asp:Literal ID="Literal2" Text="<%$Resources:MultiLang,RequiredFields%>" runat="server" />
                        </div>
                        <div class="align-right">
                            <asp:LinkButton ID="btnUpdate"  CssClass="btn btn-cons-short btn-success" runat="server"><i class="fa fa-floppy-o"></i> <asp:Literal Text="<%$Resources:MultiLang,Save%>" runat="server" /> </asp:LinkButton>
                        </div>
                        <div class="spacer">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
