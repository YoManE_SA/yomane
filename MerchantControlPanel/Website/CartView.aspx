﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_CartView" Codebehind="CartView.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" Runat="Server">
	<netpay:PageHelpBox ID="PageHelpBox1" Text="<%$Resources:HelpBox %>" runat="server" />
	<table>
		<tr>
			<td><h1><asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" /></h1></td>
		</tr>
	</table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" Runat="Server">
	<table cellspacing="2" cellpadding="2" border="0" width="90%" class="FormView">
		<tr>
			<td>
				<b><asp:Literal runat="server" Text="<%$Resources:RecepientName%>" />:</b><br />
				<asp:Literal runat="server" ID="ltRecepientName" />
			</td><td>
				<b><asp:Literal runat="server" Text="<%$Resources:RecepientPhone%>" />:</b><br />
				<asp:Literal runat="server" ID="ltRecepientPhone" />
			</td>
		</tr><tr>
			<td>
				<b><asp:Literal runat="server" Text="<%$Resources:RecepientMail%>" />:</b><br />
				<asp:Literal runat="server" ID="ltRecepientMail" />
			</td><td>
				<b><asp:Literal runat="server" Text="<%$Resources:ShippingAddress%>" />:</b><br />
				<asp:Literal runat="server" ID="ltShippingAddress" />
			</td>
		</tr><tr>
			<td>
				<b><asp:Literal runat="server" Text="<%$Resources:ShippingAddress2%>" />:</b><br />
				<asp:Literal runat="server" ID="ltShippingAddress2" />
			</td><td>
				<b><asp:Literal runat="server" Text="<%$Resources:ShippingCity%>" />:</b><br />
				<asp:Literal runat="server" ID="ltShippingCity" />
			</td>
		</tr><tr>
			<td>
				<b><asp:Literal runat="server" Text="<%$Resources:ShippingState%>" />:</b><br />
				<asp:Literal runat="server" ID="ltShippingState" />
			</td><td>
				<b><asp:Literal runat="server" Text="<%$Resources:ShippingCountry%>" />:</b><br />
				<asp:Literal runat="server" ID="ltShippingCountry" />
			</td>
		</tr><tr>
			<td>
				<b><asp:Literal runat="server" Text="<%$Resources:ShippingZip%>" />:</b><br />
				<asp:Literal runat="server" ID="ltShippingZip" />
			</td><td>
				<b><asp:Literal runat="server" Text="CheckoutDate" />:</b><br />
				<asp:Literal runat="server" ID="ltCheckoutDate" />
			</td>
		</tr>
	</table>
	<br />

	<h3><asp:Literal runat="server" Text="Items" /></h3>
	<table cellspacing="2" cellpadding="2" border="0" width="90%" class="formNormal" >
		<COL span="4" />
		<COL style="text-align:right;" />
		<tr>
			<th nowrap width="15%"><asp:Literal runat="server" Text="<%$Resources:ItemNo%>" /></th>
			<th width="50%"><asp:Literal runat="server" Text="<%$Resources:Item%>" /></th>
			<th width="10%"><asp:Literal runat="server" Text="<%$Resources:Price%>" /></th>
			<th width="15%"><asp:Literal runat="server" Text="<%$Resources:Quantity%>" /></th>
			<th width="10%" style="text-align:right !important;"><asp:Literal runat="server" Text="<%$Resources:Total%>" /></th>
		</tr>
		<asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server" >
			<ItemTemplate>
				<tr>
					<td>
						<asp:Image runat="server" ID="imgItem" Width="30" /><br />
						<asp:Literal runat="server" ID="ltCartItemID" Text="<%# CType(Container.DataItem, CartItemVO).ID %>" />
					</td>
					<td><a runat="server" id="ahItemURL" target="_blank"><%# CType(Container.DataItem, CartItemVO).Name %></a></td>
					<td nowrap><%# CType(Container.DataItem, CartItemVO).Price.ToAmountFormat(WebUtils.CurrentDomain.Host, CType(Container.DataItem, CartItemVO).CurrencyID)%></td>
					<td><%# CType(Container.DataItem, CartItemVO).Quantity %></td>
					<td nowrap><%# (CType(Container.DataItem, CartItemVO).Quantity * CType(Container.DataItem, CartItemVO).Price).ToAmountFormat(WebUtils.CurrentDomain.Host, CType(Container.DataItem, CartItemVO).CurrencyID)%></td>
				</tr>
			</ItemTemplate>
		</asp:Repeater>
		<tr>
			<td colspan="5"><br /></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
			<td colspan="2">Total Items (<asp:Literal runat="server" ID="ltTotalQuantity" />)</td>
			<td><asp:Literal runat="server" ID="ltTotalProsucts" /></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
			<td colspan="2">Tax</td>
			<td><asp:Literal runat="server" ID="ltTax" /></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
			<td colspan="2">Shipping</td>
			<td><asp:Literal runat="server" ID="ltShipping" /></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
			<td colspan="2">Total</td>
			<td style="font-weight:bold;"><asp:Literal runat="server" ID="ltTotal" /></td>
		</tr>
	</table>

</asp:Content>

