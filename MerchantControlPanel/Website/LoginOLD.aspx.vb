﻿Imports System.Xml
Imports Netpay.Web
Imports Netpay.Infrastructure

Partial Class LoginOLD
    Inherits Page
    Public ShowRegister As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lnkIcon.Href = WebUtils.MapTemplateVirPath(Me, "favicon.ico")

        If Not Page.IsPostBack And Request.QueryString("login") = "outer" Then
            Dim email As String = Request("mail").Trim()
            Dim username As String = Request("username").Trim()
            Dim password As String = Request("userpassword").Trim()
            'If Not IsPostBack Then Netpay.Web.WebUtils.Logout()
            'Dim result As LoginResult = WebUtils.Login(UserType.MerchantPrimary, username, email, password)
            'If result = LoginResult.UserNotFound Then
            '   result = WebUtils.Login(UserType.MerchantLimited, username, email, password)
            'End If

            'If result = LoginResult.Success Then
            'Response.Redirect("default.aspx")
            'Else
            loginEmail.Text = Request.QueryString("Mail")
            loginUsername.Text = Request.QueryString("Username")
            loginPassword.TextMode = TextBoxMode.SingleLine
            loginPassword.Visible = False
            loginPassword.Text = Request.QueryString("UserPassword")
            'Response.Redirect("/error.aspx?error=" + result.ToString())
            'End If
        ElseIf Not IsPostBack Then
            ShowRegister = Request.QueryString("ShowRegister").ToNullableBool().GetValueOrDefault(False)
        Else
            If WebUtils.IsLoggedin Then Response.Redirect("~/Website/default.aspx")
        End If
    End Sub

    Sub AlertAndFocus(ByVal message As String, ByRef txtField As TextBox)
        lblError.Text = "<div class=""error"">" & message & "</div>"
        lblError.Visible = True
        txtField.Focus()
    End Sub

    Protected Sub btnLogin_Click(ByVal o As Object, ByVal e As System.EventArgs)
        ShowRegister = False
        lblError.Text = ""
        Dim isValidData As Boolean = True
        If Validation.IsEmptyField(loginEmail, lblError, CType(GetGlobalResourceObject("Login.aspx", "Mail"), String)) Then isValidData = False
        If Validation.IsNotMail(loginEmail, lblError, CType(GetGlobalResourceObject("Login.aspx", "Mail"), String)) Then isValidData = False
        If Validation.IsEmptyField(loginUsername, lblError, CType(GetGlobalResourceObject("Login.aspx", "Username"), String)) Then isValidData = False
        If Validation.IsEmptyField(loginPassword, lblError, CType(GetGlobalResourceObject("Login.aspx", "Password"), String)) Then isValidData = False

        If Not isValidData Then
            'dvAlert.Visible = True
            Exit Sub
        End If

        Dim email As String = loginEmail.Text.ToSql(True).Trim()
        Dim username As String = loginUsername.Text.ToSql(True).Trim()
        Dim password As String = loginPassword.Text.ToSql(True).Trim()

        Dim result = WebUtils.Login(New Infrastructure.Security.UserRole() {Infrastructure.Security.UserRole.Merchant, Infrastructure.Security.UserRole.MerchantSubUser}, username, email, password)

        If result = Infrastructure.Security.LoginResult.Success Then
            If WebUtils.LoggedUser.IsFirstLogin Then Response.Redirect("EditLogin.aspx", True)
            Dim url As String = Request.RawUrl
            If Not String.IsNullOrEmpty(Session("GoToURL")) Then
                url = Session("GoToURL")
                Session("GoToURL") = Nothing
            End If

            If url.ToLower.Contains("signup.aspx") Then url = "."
            If url.ToLower.Contains("login.aspx") Then url = "."
            If url.ToLower.Contains("logout.aspx") Then url = "."
            If url.ToLower.Contains("userlockedout") Then url = "."
            If url.ToLower.Contains("userblocked") Then url = "."
            If url.ToLower.Contains("password") Then url = "."

            'Server.Transfer(url, False)
            'Response.End()
            Response.Redirect(url, True)
        Else
            AlertAndFocus(CType(GetGlobalResourceObject("menu", result.ToString()), String), loginPassword)
        End If
    End Sub

    Protected Overrides Sub OnPreRender(e As System.EventArgs)
        ltCopyright.Text = CType(GetGlobalResourceObject("Login.aspx", "Copyright"), String).Replace("%BRAND%", WebUtils.CurrentDomain.BrandName)
        Dim link As New HtmlControls.HtmlLink()
        link.Href = WebUtils.MapTemplateVirPath(Me, "WebSite/Styles/login.css")
        link.Attributes.Add("rel", "stylesheet")
        link.Attributes.Add("type", "text/css")
        link.Attributes.Add("media", "screen")
        Page.Header.Controls.Add(link)
        If System.Threading.Thread.CurrentThread.CurrentUICulture.TextInfo.IsRightToLeft Then
            link = New HtmlControls.HtmlLink()
            link.Href = WebUtils.MapTemplateVirPath(Me, "WebSite/Styles/loginRTL.css")
            link.Attributes.Add("rel", "stylesheet")
            link.Attributes.Add("type", "text/css")
            link.Attributes.Add("media", "screen")
            Page.Header.Controls.Add(link)
        End If

        MyBase.OnPreRender(e)
    End Sub



End Class