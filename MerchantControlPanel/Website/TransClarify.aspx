﻿<%@ Page Title="" Language="VB" AutoEventWireup="true" Inherits="Netpay.MerchantControlPanel.Website_TransClarify" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" CodeBehind="TransClarify.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$ Resources:PageTitle %>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <asp:RegularExpressionValidator Display="Dynamic" ID="Last4digits" ClientIDMode="Static" ControlToValidate="txtLast4Digits" ValidationExpression="\d{4,4}$" runat="server" ErrorMessage="<%$Resources:MultiLang,Last4digitsValid  %>"></asp:RegularExpressionValidator>
        <div class="top-nav">
            <asp:Localize ID="Localize2" Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">
            <div class="legend-menu">
                <div class="align-right">
                    <netpay:PopupButton ID="PopupButton1" IconSrc="Images/iconLegend.gif" Text="<%$Resources:MultiLang,Legend  %>" runat="server">
                        <netpay:Legend ID="ttLegend" Clarification="true" PendingChargeback="true" Test="true" runat="server" />
                    </netpay:PopupButton>
                </div>
            </div>
            <div class="section">
                <div class="TransClarify-level-bottom">
                    <div class="margin-bottom-10">
                        <netpay:DateRangePicker ID="wcDateRangePicker" HtmlLayout="Flow" runat="server" />
                        <netpay:PaymentMethodDropDown ID="ddPaymentMethod" runat="server" BlankSelectionText="<%$Resources:MultiLang,PaymentMethod  %>">
                        </netpay:PaymentMethodDropDown>
                        <netpay:CurrencyDropDown ID="ddCurrency" runat="server" CssClass="Field_120" BlankSelectionText="<%$Resources:MultiLang,Currency  %>">
                        </netpay:CurrencyDropDown>
                        <asp:DropDownList ID="ddRefundability" runat="server">
                            <asp:ListItem Text="<%$Resources:Refundability %>" Value="" class="emptyOption"></asp:ListItem>
                            <asp:ListItem Text="<%$Resources:NonRefundable %>" Value="NonRefundable"></asp:ListItem>
                            <asp:ListItem Text="<%$Resources:Refundable %>" Value="Refundable"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtLast4Digits" runat="server" CssClass="Field_120" placeholder="<%$Resources:MultiLang,Last4Digits %>"></asp:TextBox>
                    </div>
                    <div class="line-height-35">
                        <asp:TextBox ID="txtPayerId" runat="server" placeholder="<%$Resources:MultiLang,PayerID %>" CssClass="Field_100"></asp:TextBox>
                        <asp:TextBox ID="txtOrderNumber" runat="server" placeholder="<%$Resources:MultiLang,Order %>" CssClass="Field_150" />
                        <asp:TextBox ID="txtPayerFirstName" placeholder="<%$Resources:MultiLang,PayerFirstName %>" CssClass="Field_150" runat="server"></asp:TextBox>
                        <asp:TextBox ID="txtPayerLastName" placeholder="<%$Resources:MultiLang,PayerLastName %>" CssClass="Field_150" runat="server"></asp:TextBox>
                        <asp:TextBox ID="txtCardHolderEmail" runat="server" placeholder="<%$Resources:MultiLang,CCHolderMail %>" CssClass="Field_150"></asp:TextBox>&nbsp;&nbsp;
						
                    </div>
                </div>

                <div>
                    <asp:Literal ID="ltError" Visible="false" runat="server" /><asp:HiddenField ID="hdLastSerachMode" runat="server" />
                </div>
              

                <div class="wrap-button-bar">
                      <asp:LinkButton ID="btnSearch" OnClick="btnSearch_Click" CssClass="btn btn-cons-short btn-inverse" runat="server"><i class="fa fa-search"></i>  <asp:Literal Text="<%$Resources:MultiLang,Search %>" runat="server" /></asp:LinkButton>
               </div>
            </div>
            <asp:PlaceHolder ID="phResults" Visible="false" runat="server">
                <div class="section">
                    <netpay:SearchFiltersView ID="wcFiltersView" runat="server" Visible="false" />
                    <table class="exspand-table">
                        <tr>
                            <th colspan="2">&nbsp;
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:MultiLang,Transaction %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:MultiLang,Date %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:MultiLang,PaymentMethod %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:MultiLang,Amount %>" runat="server" />
                            </th>
                            <asp:Literal ID="lblTableHeadings" runat="server" />
                        </tr>
                        <asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server" OnItemDataBound="SetRowView">
                            <ItemTemplate>
                                <netpay:MerchantTransactionRowView ID="ucTransactionView" Transaction="<%# CType(Container.DataItem, Transactions.Transaction) %>" ShowStatus="true" runat="server" />
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
                    <div class="xls-wrap">
                        <netpay:ExcelButton ID="btnExportExcel" Text="<%$Resources:MultiLang,ExportToXls %>"
                            Enabled="false" IconImageOn="xls.png" IconImageOff="xls_off.png" runat="server" OnClick="ExportExcel_Click" />
                    </div>
                </div>
            </asp:PlaceHolder>
        </div>
    </div>
</asp:Content>
