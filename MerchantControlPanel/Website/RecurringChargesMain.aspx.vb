﻿Imports System.Collections.Generic
Imports Netpay.Web.Controls
Imports Netpay.Bll.Reports
Imports Netpay.Bll
Imports Netpay.Infrastructure

Partial Class Website_RecurrindChargesMain
	Inherits MasteredPage
	Dim isSingleSearch As Boolean

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		PopupButton1.Text = GetGlobalResourceObject("Multilang", "Legend").ToString().ToUpper()
        If (Not Page.IsPostBack) And (Not String.IsNullOrEmpty(Request("seriesID"))) Then
            txtSeriesID.Text = dbPages.TestVar(Request("seriesID"), 1, 0, 0)
			btnSearchSingle_Click(sender, e)
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles buttonSearch.Click
		isSingleSearch = False
        OnPageChanged(sender, e)
        hdLastSearchMode.Value = "multi"
    End Sub

    Protected Sub btnSearchSingle_Click(ByVal sender As Object, ByVal e As EventArgs) Handles buttonSearchSingle.Click
		isSingleSearch = true 
        OnPageChanged(sender, e)
        hdLastSearchMode.Value = "single"
    End Sub

    Protected Sub OnPageChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim nSeriesID = dbPages.TestVar(txtSeriesID.Text, 1, 0, 0)
        Dim filters = New Bll.Transactions.Recurring.Charge.SearchFilters()
        If isSingleSearch Then
            If (txtSeriesID.Text.Trim() = "") Then Return
            filters.seriesID = nSeriesID
        Else
            filters.Date.From = wcDateRangePicker.FromDate
            filters.Date.To = wcDateRangePicker.ToDate
            filters.paymentMethodID = ddPaymentMethod.SelectedValue.ToNullableInt32()
        End If

        If TypeOf e Is ExcelButton.ExportEventArgs Then
            wcFiltersView.LoadFilters(filters)
            btnExportExcel.SetExportInfo(wcFiltersView.RenderText(), ReportType.MerchantRecurringCharges, Function(pi) Bll.Transactions.Recurring.Charge.Search(filters, pi))
        Else
            Dim results = Bll.Transactions.Recurring.Charge.Search(filters, pager.Info)
            If results.Count > 0 Then
                repeaterResults.DataSource = results
                repeaterResults.DataBind()
                btnExportExcel.ResultCount = pager.Info.RowCount
                phResults.Visible = True
            Else
                phResults.Visible = False
            End If
        End If

        btnSuspend.Visible = False
        btnResume.Visible = False
        litSeries.Text = String.Empty
        litSeriesAfter.Text = String.Empty
        hlSeries.Visible = False
        If nSeriesID > 0 Then
            Dim series = Netpay.Bll.Transactions.Recurring.Series.Load(nSeriesID)
            If series IsNot Nothing Then
                litSeries.Text = GetGlobalResourceObject("MultiLang", "Series") & " " & nSeriesID & " - " & GetGlobalResourceObject("MultiLang", series.Status) & " ("
                hlSeries.NavigateUrl = "RecurringSeries.aspx?ID=" & nSeriesID
                hlSeries.Text = GetLocalResourceObject("ManageTheSeries") & "<span style=""text-decoration:none;"">&nbsp;</span><img src=""/NPCommon/Images/iconNewWin" & GetGlobalResourceObject("MultiLang", "CssAlignRev") & ".GIF"" border=""0"" align=""middle""/>"
                hlSeries.Visible = True
                litSeriesAfter.Text = ")"
                If True Or series.Blocked Or series.Deleted Or series.Paid Then
                    'the series cannot be modified
                Else
                    If series.Suspended Then
                        btnResume.Visible = True
                    Else
                        btnSuspend.Visible = True
                    End If
                End If
            End If
        End If
    End Sub

	Protected Sub ExportExcel_Click(ByVal sender As Object, ByVal e As EventArgs)
		isSingleSearch = isSingleMode() 
        OnPageChanged(sender, e)
		btnExportExcel.Export()
	End Sub

    Protected Function GetStatusClass(ByVal charge As Transactions.Recurring.Charge) As String
        If charge.Paid Then
            Return IIf(charge.Series.IsPreAuthorized, "statusAuthorized", "statusPaid")
        ElseIf charge.Blocked Then
            Return "statusBlocked"
        ElseIf charge.Suspended Then
            Return "statusSuspended"
        Else
            Return "statusActive"
        End If
    End Function

    Protected Function GetSeriesStatusClass(ByVal charge As Transactions.Recurring.Series) As String
        If charge.Paid Then
            Return "statusPaid"
        ElseIf charge.Blocked Then
            Return "statusBlocked"
        ElseIf charge.Suspended Then
            Return "statusSuspended"
        Else
            Return "statusActive"
        End If
    End Function

	Protected Function GetTypeText(ByVal isPreAuthorized As Boolean) As String
		If isPreAuthorized Then
			Return GetGlobalResourceObject("MultiLang", "PreAuth")
		Else
			Return GetGlobalResourceObject("MultiLang", "Debit")
		End If
	End Function

    Protected Function GetEditUrl(ByVal charge As Transactions.Recurring.Charge) As String
        Return "RecurringEditCharge.aspx?charge=" + charge.ID.ToString() + "&main=1"
    End Function

    Protected Function GetHistoryUrl(ByVal charge As Transactions.Recurring.Charge) As String
        Return "RecurringChargeHistory.aspx?chargeID=" + charge.ID.ToString()
    End Function

    Protected Function GetSeriesFilterLink(ByVal charge As Transactions.Recurring.Charge) As String
        Return "RecurringChargesMain.aspx?seriesID=" + charge.Series.ToString()
    End Function

	Protected Sub SuspendCharge(ByVal sender As Object, ByVal e As CommandEventArgs)
		Dim chargeID As Integer = Integer.Parse(e.CommandArgument)
        Dim charge = Transactions.Recurring.Charge.Load(chargeID)
        If charge IsNot Nothing Then charge.Suspend()
        isSingleSearch = isSingleMode() 
        OnPageChanged(sender, e)
	End Sub

	Protected Sub ResumeCharge(ByVal sender As Object, ByVal e As CommandEventArgs)
		Dim chargeID As Integer = Integer.Parse(e.CommandArgument)
        Dim charge = Transactions.Recurring.Charge.Load(chargeID)
		If charge IsNot Nothing Then charge.Resume()
		isSingleSearch = isSingleMode() 
        OnPageChanged(sender, e)
	End Sub

	Protected Sub SuspendSeries(ByVal sender As Object, ByVal e As CommandEventArgs)
		Dim nSeriesID As Integer = dbPages.TestVar(txtSeriesID.Text, 1, 0, 0)
		If nSeriesID = 0 Then nSeriesID = dbPages.TestVar(Request("seriesID"), 1, 0, 0)
        Dim series = Transactions.Recurring.Series.Load(nSeriesID)
		If series IsNot Nothing Then series.Suspend()
		isSingleSearch = isSingleMode() 
        OnPageChanged(sender, e)
	End Sub

	Protected Sub ResumeSeries(ByVal sender As Object, ByVal e As CommandEventArgs)
		Dim nSeriesID = dbPages.TestVar(txtSeriesID.Text, 1, 0, 0)
		If nSeriesID = 0 Then nSeriesID = dbPages.TestVar(Request("seriesID"), 1, 0, 0)
        Dim series = Transactions.Recurring.Series.Load(nSeriesID)
		If series IsNot Nothing Then series.Resume()
		isSingleSearch = isSingleMode() 
        OnPageChanged(sender, e)
    End Sub

    Private Function isSingleMode() As Boolean
        If hdLastSearchMode.Value = "single" Then
            Return True
        Else
            Return False
        End If
    End Function

	Protected Sub ResetFilterClassSeries(o As Object, e As EventArgs)
		Dim nSeriesID = dbPages.TestVar(txtSeriesID.Text, 1, 0, 0)
		Dim bEnabled As Boolean = (nSeriesID = 0)
		Dim sCssClass As String = IIf(bEnabled, "filterLink", "filterNoLink")
		Dim records As System.Web.UI.WebControls.Repeater = o
		For Each record As RepeaterItem In records.Items
			Dim filterLink As HyperLink = record.FindControl("hlSeriesFilter")
			filterLink.CssClass = sCssClass
			filterLink.Enabled = bEnabled
			filterLink.ToolTip = IIf(bEnabled, GetLocalResourceObject("FilterByThisSeries").ToString.Replace("###", filterLink.Text.Replace("<span>", String.Empty).Replace("</span>", String.Empty)), String.Empty)
			filterLink.Style.Add("background-position", GetGlobalResourceObject("MultiLang", "CssAlignRev").ToString)
		Next
	End Sub
End Class
