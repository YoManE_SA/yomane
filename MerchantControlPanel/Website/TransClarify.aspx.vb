﻿Imports Netpay.Web.Controls
Imports Netpay.Bll
Imports Netpay.Web
Imports Netpay.Infrastructure
Imports Netpay.Bll.Reports

Partial Class Website_TransClarify
    Inherits MasteredPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then 
			wcDateRangePicker.FromDate = DateTime.Now.AddMonths(-2)
			OnPageChanged(sender, e)
		End If
    End Sub

    Sub SetRowView(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        CType(e.Item.FindControl("ucTransactionView"), TransactionRowView).ShowStatus = True
    End Sub

    Protected Sub SetHeaders()
        lblTableHeadings.Text = "<th style=""text-align:" & WebUtils.CurrentAlignment & ";"">" & GetGlobalResourceObject("MultiLang", "PayoutStatus") & "</th>"
    End Sub

 	Protected Sub OnPageChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim filters = New Bll.Transactions.Transaction.SearchFilters()
        filters.paymentMethodID = ddPaymentMethod.SelectedValue.ToNullableInt32()
        filters.deniedDate.From = wcDateRangePicker.FromDate
        filters.deniedDate.To = wcDateRangePicker.ToDate
        filters.orderID = txtOrderNumber.Text.NullIfEmpty()
        filters.currencyID = ddCurrency.SelectedValue.ToNullableInt32()
        filters.deniedStatus = New System.Collections.Generic.List(Of DeniedStatus)(New DeniedStatus() {DeniedStatus.UnsettledBeenSettledAndValid, DeniedStatus.DuplicateTransactionWorkedOut})
        filters.date.From = DateTime.Now.AddMonths(-18) 'only transactions from the last 18 month
        filters.PayerFilters = New Transactions.Payer.SearchFilters()
        filters.PayerFilters.FirstName = txtPayerFirstName.Text.NullIfEmpty()
        filters.PayerFilters.LastName = txtPayerLastName.Text.NullIfEmpty()
        filters.PayerFilters.PersonalNumber = txtPayerId.Text.NullIfEmpty()
        filters.PayerFilters.EmailAddress = txtCardHolderEmail.Text.NullIfEmpty()
        filters.PaymentFilters = New Transactions.Payment.SearchFilters()
        filters.PaymentFilters.Last4Digits = txtLast4Digits.Text.NullIfEmpty()

        If ddRefundability.SelectedValue = "NonRefundable" Then
            filters.isPendingChargeback = True
        ElseIf ddRefundability.SelectedValue = "Refundable" Then
            filters.isPendingChargeback = False
        End If
        SetHeaders()

        If TypeOf e Is ExcelButton.ExportEventArgs Then
            wcFiltersView.LoadFilters(filters)
            btnExportExcel.SetExportInfo(wcFiltersView.RenderText, ReportType.MerchantCapturedTransactions, Function(pi) Bll.Transactions.Transaction.Search(TransactionStatus.Captured, filters, pi, True, False))
        Else
            repeaterResults.DataSource = Bll.Transactions.Transaction.Search(TransactionStatus.Captured, filters, pager.Info, True, False)
            repeaterResults.DataBind()
            btnExportExcel.ResultCount = pager.Info.RowCount
        End If
        phResults.Visible = repeaterResults.Items.Count > 0
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
        hdLastSerachMode.Value = "search"
        pager.Info.PageCurrent = 0
        OnPageChanged(sender, e)
        pager.Visible = True
    End Sub

    Protected Sub ExportExcel_Click(ByVal sender As Object, ByVal e As ExcelButton.ExportEventArgs)
        OnPageChanged(sender, e)
        btnExportExcel.Export()
    End Sub

End Class
