﻿Public Class ContentPage
	Inherits MasteredPage

	Protected Overrides Sub OnPreInit(e As System.EventArgs)
		'IsMobile = (Request.QueryString("IsMobile") = "1")
		MyBase.OnPreInit(e)
    End Sub

    Protected Function GetPageData(fileName As String) As System.Xml.XmlDocument
        fileName = Domain.MapPrivateDataPath("Site Content/MCP/Content/" + fileName + ".xml")
        If Not System.IO.File.Exists(fileName) Then Return Nothing
        Dim doc As New System.Xml.XmlDocument
        doc.Load(fileName)
        Return doc
    End Function

	Public Function GetXmlNodeText(pNode As System.Xml.XmlNode, sChildElement As String) As String
		Dim ppNode As System.Xml.XmlNode = pNode.SelectSingleNode(sChildElement)
		If (ppNode Is Nothing) Then Return Nothing
		Return ppNode.InnerText
	End Function

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Dim doc As System.Xml.XmlDocument = GetPageData(Request("f"))
		If doc Is Nothing Then Return
		Title = GetXmlNodeText(doc.DocumentElement, "PageTitle")
		Dim pMeta As HtmlMeta = New HtmlMeta() : Page.Header.Controls.Add(pMeta) : pMeta.HttpEquiv = "meta-keywords"
		pMeta.Content = GetXmlNodeText(doc.DocumentElement, "MetaKeywords")
		pMeta = New HtmlMeta() : Page.Header.Controls.Add(pMeta) : pMeta.HttpEquiv = "meta-description"
		pMeta.Content = GetXmlNodeText(doc.DocumentElement, "MetaDescription")
		ltContent.Text = GetXmlNodeText(doc.DocumentElement, "Content")
	End Sub
End Class