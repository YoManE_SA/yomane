﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Tmp_netpayintl/page.master"
    CodeBehind="LiveKashCustomers.aspx.vb" Inherits="Netpay.MerchantControlPanel.LiveKashCustomers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
    <asp:Button ID="btnAddCustomer" Text="<%$Resources:AddCustomer %>" CssClass="buttonAdd"
        TabIndex="18" Width="112" OnClick="AddCustomer" runat="server" />
    &nbsp;&nbsp;
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="locPageTitle" Text="<%$Resources:title%>" runat="server" /></div>
        <div class="content-background">
            <div class="legend-menu">
                <div class="align-right">
                    <netpay:PopupButton ID="PopupButton1" IconSrc="../Tmp_netpayintl/Images/iconLegend.gif"
                        Text="<%$Resources:MultiLang,Legend  %>" runat="server">
                        <netpay:Legend ID="ttLegend" Approved="true" NotApproved="true" runat="server" />
                    </netpay:PopupButton>
                </div>
            </div>
            <div class="section">
                <div id="divBlocker">
                    <netpay:ElementBlocker ID="wcElementBlocker" Visible="true" ElementClientID="divBlocker"
                        Text="<%$ Resources:MultiLang,FeatureDisabled %>" runat="server" />
                    <asp:Literal ID="Literal1" Text="<%$Resources:MultiLang,EMail %>" runat="server"></asp:Literal>
                    <asp:Literal ID="Literal2" Text="<%$Resources:MultiLang,Comment %>" runat="server"></asp:Literal>
                    <input type="text" id="txtFilterEmail" runat="server" />
                    <input type="text" id="txtFilterComment" runat="server" />
                    <asp:Button ID="btnFilter" Text="<%$Resources:MultiLang,Search %>" CssClass="btn btn-primary"
                        TabIndex="18" OnClick="GetCustomers" runat="server" />
                    <asp:Literal ID="Literal3" runat="server" Text="<%$Resources:CustomerID %>"></asp:Literal>
                    <asp:TextBox ID="txtCardID" runat="server"></asp:TextBox>
                    <asp:Button ID="btnShowCard" OnClick="ShowCard" Text="<%$Resources:MultiLang,Show %>"
                        CssClass="btn btn-primary" runat="server" OnClientClick="javascript:return document.getElementById('ctl00_cphBody_txtCardID').value!=''" />
                </div>
            </div>
            <div class="section">
                <asp:PlaceHolder ID="phResults" Visible="false" runat="server">
                    <table class="exspand-table">
                        <tr>
                            <th>
                                &nbsp;
                            </th>
                            <th>
                                <asp:Literal ID="Literal4" Text="<%$Resources:MultiLang,ID %>" runat="server"></asp:Literal>
                            </th>
                            <th>
                                <asp:Literal ID="Literal5" Text="<%$Resources:MultiLang,Name %>" runat="server"></asp:Literal>
                            </th>
                            <th>
                                <asp:Literal ID="Literal6" Text="<%$Resources:MultiLang,Email %>" runat="server"></asp:Literal>
                            </th>
                            <th>
                                <asp:Literal ID="Literal7" Text="<%$Resources:MultiLang,Comment %>" runat="server"></asp:Literal>
                            </th>
                            <th>
                                &nbsp;
                            </th>
                        </tr>
                        <asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server">
                            <ItemTemplate>
                                <tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
                                    <td width="8px" class="<%# GetCssClass(CType(Container.DataItem, ExternalCardCustomerVO))%>">
                                        &nbsp;
                                    </td>
                                    <td>
                                        <%# CType(Container.DataItem, ExternalCardCustomerVO).ID %>
                                    </td>
                                    <td>
                                        <%# CType(Container.DataItem, ExternalCardCustomerVO).Name %>
                                    </td>
                                    <td>
                                        <%# CType(Container.DataItem, ExternalCardCustomerVO).Email%>
                                    </td>
                                    <td>
                                        <%# CType(Container.DataItem, ExternalCardCustomerVO).Comment%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%# GetPaymentLink(CType(Container.DataItem, ExternalCardCustomerVO))%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
                </asp:PlaceHolder>
            </div>
        </div>
    </div>
</asp:Content>
