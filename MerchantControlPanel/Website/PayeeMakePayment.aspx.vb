﻿Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Linq
Imports Netpay.CommonTypes
Imports Netpay.Dal.Netpay
Imports Netpay.Infrastructure
Imports Netpay.Bll
Imports Netpay.Web
Imports Netpay.Web.Controls

Partial Class Website_PayeeMakePayment
    Inherits MasteredPage
    Dim PayeeID As Nullable(Of Integer)
    Protected Item As Bll.Accounts.Payee
    Protected MasterAccount As Bll.Accounts.BankAccount
    Protected CorAccount As Bll.Accounts.BankAccount
    Protected ILBankAccount As Bll.Wires.Masav.IsraelBankAccount

    Protected nCurrencyFirst As Integer
    Protected bIsAvailableBalance As Boolean
    Private _availableBalance As Dictionary(Of Integer, Decimal)
    Public nCurrencyDefault As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack Then
            If wcDateRangePicker.FromDate Is Nothing Then wcDateRangePicker.FromDate = DateTime.Now.Date
        Else
            wcDateRangePicker.FromDate = DateTime.Today
            ddPaymentCurrency.SetCurrencyIDs(AvailableBalance.Keys.ToDelimitedString())
        End If

        Dim payeeId As Nullable(Of Integer) = Request("Payee").ToNullableInt32()

        If payeeId IsNot Nothing Then
            Item = Netpay.Bll.Accounts.Payee.Load(payeeId)
            If Item Is Nothing Then Response.Redirect("PayeeList.aspx", True)
            MasterAccount = Item.BankAccount
        Else
            Item = New Netpay.Bll.Accounts.Payee(Account.AccountID) With
            {
                .LegalNumber = Merchant.LegalNumber,
                .CompanyName = Account.AccountName,
                .ContactName = Merchant.ContactFirstName & " " & Merchant.ContactLastName,
                .EmailAddress = Merchant.ContactEmail,
                .PhoneNumber = Merchant.ContactPhone,
                .SearchTag = "Self Pay"
            }
            MasterAccount = Accounts.BankAccount.LoadForAccount(Account.AccountID, nCurrencyDefault)
        End If
        CorAccount = MasterAccount.CorrespondentAccount
        ILBankAccount = New Wires.Masav.IsraelBankAccount(MasterAccount)
        If CorAccount Is Nothing Then CorAccount = New Accounts.BankAccount()
        Page.DataBind()
        phIsraeliBank.Visible = (WebUtils.CurrentLanguageShort = "he" And MasterAccount.IBAN.EmptyIfNull().StartsWith("IL"))
        phAbroadBank.Visible = Not phIsraeliBank.Visible
        phCorrespondingBank.Visible = phAbroadBank.Visible And MasterAccount.RefAccountBankAccount_id.HasValue

        If payeeId Is Nothing Then
            litProfileTitle.Text = Infrastructure.GlobalData.GetText(GlobalDataGroup.BeneficiaryProfileType, WebUtils.CurrentLanguage, 0)
            litDataHeading.Text = GetLocalResourceObject("DataDisplayHeading").ToString().Replace("%IDENTITY%", WebUtils.CurrentDomain.BrandName)
        Else
            litProfileTitle.Text = Item.CompanyName
            litDataHeading.Text = Item.SearchTag 'Infrastructure.GlobalData.GetText(GlobalDataGroup.BeneficiaryProfileType, WebUtils.CurrentLanguage, Request("ProfileType"))
            litDataHeading.Text &= ", " & GetLocalResourceObject("BeneficiaryNumber").ToString & ": "
            litDataHeading.Text &= "<a href=""Payee.aspx?ID=" & payeeId & """>" & payeeId & "</a>"
        End If

        bIsAvailableBalance = False
        Dim currencies As Dictionary(Of Integer, Bll.Currency) = Bll.Currency.Cache.ToDictionary(Function(k) k.ID)
        For Each currentCurrency As KeyValuePair(Of Integer, Bll.Currency) In currencies
            If AvailableBalance.ContainsKey(currentCurrency.Value.ID) Then
                If AvailableBalance(currentCurrency.Value.ID) > 0 Then bIsAvailableBalance = True
            End If
        Next
        litNoBalance.Text = IIf(bIsAvailableBalance, String.Empty, "<tr><td></td><td style=""color:maroon;""><br />" & GetLocalResourceObject("BalanceTooLow").ToString & "</td></tr>")
        If bIsAvailableBalance Then
            litLimits.Text = "<table cellpadding=""0"" cellspacing=""0"" border=""0"" width=""250"">"
            litLimits.Text &= "<tr>"
            litLimits.Text &= "<td valign=""bottom""><span class=""DataHeading"">" & GetLocalResourceObject("MinimumAmount") & "</span></td>"
            litLimits.Text &= "<td valign=""bottom""><span class=""DataHeading"">" & GetLocalResourceObject("MaximumAmount") & "</span></td>"
            litLimits.Text &= "<td valign=""bottom""><span class=""DataHeading"">" & GetLocalResourceObject("FeeAmount") & "</span></td>"
            litLimits.Text &= "</tr>"

            Dim merchantCurrencySettings = Bll.Settlements.MerchantSettings.LoadForMerchant(Merchant.ID)
            nCurrencyFirst = 1 ' hebrew Or english
            For Each currentCurrency As System.Collections.Generic.KeyValuePair(Of Integer, Bll.Currency) In currencies
                If AvailableBalance.ContainsKey(currentCurrency.Value.ID) Then
                    If AvailableBalance(currentCurrency.Value.ID) > 0 Then
                        Dim currencyFee As Bll.Settlements.MerchantSettings = Nothing
                        Dim i = currentCurrency.Value.ID
                        If merchantCurrencySettings.ContainsKey(currentCurrency.Value.ID) Then currencyFee = merchantCurrencySettings(i)
                        'If ddPaymentCurrency.SelectedIndex = 0 Then ddPaymentCurrency.SelectedValue = i
                        litLimits.Text &= "<tr>"
                        litLimits.Text &= "<td class=""txt11"" valign=""top"">"
                        If (currencyFee IsNot Nothing) Then litLimits.Text &= dbPages.FormatCurr(i, currencyFee.MinPayoutAmount) Else litLimits.Text &= dbPages.FormatCurr(i, 0)
                        litLimits.Text &= "</td>"
                        litLimits.Text &= "<td class=""txt11"" valign=""top"">" & dbPages.FormatCurr(i, AvailableBalance(i)) & "</td>"
                        litLimits.Text &= "<td class=""txt11"" valign=""top"" style=""white-space: nowrap;"">"
                        If (currencyFee IsNot Nothing) Then litLimits.Text &= dbPages.FormatCurr(i, currencyFee.WireFee) & " + " & currencyFee.WireFeePercent.ToString("0.00") & "%"
                        litLimits.Text &= "</td>"
                        litLimits.Text &= "</tr>"
                    End If
                End If
            Next
            litLimits.Text &= "</table>"
        Else
            litLimits.Text = String.Empty
        End If
    End Sub

    Protected Sub BtnSubmit_OnClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim amount As Decimal = dbPages.TestVar(Replace(paymentAmount.Text, ",", ""), 0D, -1D, 0D)
        Dim merchantCurrencySettings = Bll.Settlements.MerchantSettings.LoadForMerchant(Merchant.ID)
        Dim merchantBeneficiaryID As Nullable(Of Integer) = Request("CompanyMakePaymentsProfiles_id").ToNullableInt32()
        Dim merchantBeneficiaryType As Nullable(Of MerchantBeneficiaryProfileType) = Request("ProfileType").ToNullableEnumByValue(Of MerchantBeneficiaryProfileType)()
        Dim bDone As Boolean = False
        Dim currencyID = ddPaymentCurrency.Value.ToNullableInt().GetValueOrDefault(-1)
        If merchantBeneficiaryType = MerchantBeneficiaryProfileType.Payoneer Then
            If fuConfirmation.PostedFile.ContentLength = 0 Then
                litInsertText.Text = "<span style=""color:maroon;"">" & GetLocalResourceObject("OrderNotAccepted") & "<br />" & GetLocalResourceObject("NoFileUploaded") & "</span><br />"
                Return
            End If
        End If

        If Not AvailableBalance.ContainsKey(currencyID) Then
            litInsertText.Text = "<span style=""color:maroon;"">" & GetLocalResourceObject("OrderNotAccepted") & "<br />" & GetLocalResourceObject("InvalidCurrency") & "</span><br />"
            Return
        End If

        If wcDateRangePicker.FromDate.GetValueOrDefault().Date < DateTime.Now.Date Then
            litInsertText.Text = "<span style=""color:maroon;"">" & GetLocalResourceObject("OrderNotAccepted") & "<br />" & GetLocalResourceObject("DateTooEarly") & "</span><br />"
            Return
        End If

        Dim minPayout As Decimal = 0
        If ddPaymentCurrency.Value <> "" Then
            If merchantCurrencySettings.ContainsKey(dbPages.TestVar(currencyID, 0, -1, 0)) Then minPayout = merchantCurrencySettings(dbPages.TestVar(currencyID, 0, -1, 0)).MinPayoutAmount
            If amount - minPayout < 0 Then
                litInsertText.Text = "<span style=""color:maroon;"">" & GetLocalResourceObject("OrderNotAccepted") & "<br />" & GetLocalResourceObject("AmountTooLow") & "</span><br />"
                Return
            End If
        End If

        Dim availableBalanceTmp As Decimal = AvailableBalance(currencyID)
        If System.Math.Round(amount, 2) > System.Math.Round(availableBalanceTmp, 2) Then
            litInsertText.Text = "<span style=""color:maroon;"">" & GetLocalResourceObject("OrderNotAccepted") & "<br />" & GetLocalResourceObject("AmountTooHigh") & "</span><br />"
            Return
        End If
        Dim nExchangeRate As String = dbPages.ConvertCurrencyRate(currencyID, CommonTypes.Currency.ILS)

        'create wire
        Dim wire = New Bll.Wires.Wire(Item, amount, currencyID, paymentMerchantComment.Text.TruncEnd(250))

        'Add a transaction with this Payments request data

        'Dim sIP As String = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
        'If sIP = "" Then sIP = Request.ServerVariables("REMOTE_ADDR")

        ' save confirmation file
        Dim confiramtionFile As HttpPostedFile = fuConfirmation.PostedFile
        Dim confirmationFileName As String = "NULL"
        If confiramtionFile IsNot Nothing And confiramtionFile.ContentLength > 0 Then
            Dim confirmationFileFullName As String = Infrastructure.Domain.GetUniqueFileName(System.IO.Path.Combine(Domain.MapPrivateDataPath("Wires"), confiramtionFile.FileName))
            confiramtionFile.SaveAs(confirmationFileFullName)
            confirmationFileName = "'" + System.IO.Path.GetFileName(confirmationFileFullName) + "'"
        End If

        'Add this Payments requests to wire table

        'Dim payyName = dbPages.ExecScalar("Select bankIsraelInfo_PayeeName From tblCompanyMakePaymentsRequests Where CompanyMakePaymentsRequests_id =" & merchantBeneficiaryID)

        'Add to balance table (status pending)
        Dim balanceText As String = "Beneficiary Payment #" & wire.ID & " - " & wire.PayeeName
        Dim bl = Netpay.Bll.Accounts.Balance.Create(Account.AccountID, Bll.Currency.Get(currencyID).IsoCode, -amount, balanceText, Wires.Balance.BALANCE_SOURCE_PAYMENTREQUEST, wire.ID, True)
        Call Netpay.Bll.Accounts.Balance.Create(Account.AccountID, Bll.Currency.Get(currencyID).IsoCode, Wires.Balance.GetWireFee(Merchant.ID, Bll.Currency.Get(currencyID).IsoCode, amount), balanceText, Accounts.Balance.SOURCE_WITHDRAWAL_FEE, bl.ID, False)
        litInsertText.Text = "<span style=""color:green;"">" & GetLocalResourceObject("OrderAccepted").ToString & "</span><br />"
        bDone = True
        BtnSubmit.Visible = False
    End Sub

    Protected ReadOnly Property AvailableBalance() As Dictionary(Of Integer, Decimal)
        Get
            If _availableBalance Is Nothing Then
                Dim merchantBalance = Bll.Accounts.Balance.GetStatus(Account.AccountID)
                Dim merchantCurrencySettings = Bll.Settlements.MerchantSettings.LoadForMerchant(Merchant.ID)
                _availableBalance = New Dictionary(Of Integer, Decimal)
                For Each currentBalance In merchantBalance
                    Dim calculatedBalance As Decimal = 0
                    Dim currencyId = Bll.Currency.Get(currentBalance.CurrencyIso).ID
                    Dim merchantCurrencySetting As Bll.Settlements.MerchantSettings = Nothing
                    If merchantCurrencySettings.ContainsKey(currencyId) Then merchantCurrencySetting = merchantCurrencySettings(currencyId)
                    If merchantCurrencySetting Is Nothing Then calculatedBalance = currentBalance.Expected _
                    Else calculatedBalance = (currentBalance.Expected / (1 + merchantCurrencySetting.WireFeePercent / 100)) - merchantCurrencySetting.WireFee
                    If calculatedBalance < 0 Then calculatedBalance = 0
                    If calculatedBalance > 0 Then _availableBalance.Add(currencyId, calculatedBalance)
                Next
            End If

            Return _availableBalance
        End Get
    End Property

    Function ProfileItem(ByVal sValue As String, ByVal sTitle As String, Optional ByVal bResource As Boolean = True, Optional ByVal sGlobalResourceClass As String = Nothing) As String
        If String.IsNullOrEmpty(sValue) Then Return String.Empty
        If bResource Then
            If String.IsNullOrEmpty(sGlobalResourceClass) Then
                sTitle = GetLocalResourceObject(sTitle).ToString
            Else
                sTitle = GetGlobalResourceObject(sGlobalResourceClass, sTitle).ToString
            End If
        End If
        If Not String.IsNullOrEmpty(sTitle) Then sTitle &= ":"
        Return "<tr><td class=""txt11""><span class=""DataHeading"">" & sTitle & "</span> " & sValue & "</td></tr>"
    End Function

    Function ProfileHead(ByVal sTitle As String, Optional ByVal bResource As Boolean = True, Optional ByVal sGlobalResourceClass As String = Nothing) As String
        If String.IsNullOrEmpty(sTitle) Then Return String.Empty
        If bResource Then
            If String.IsNullOrEmpty(sGlobalResourceClass) Then
                sTitle = GetLocalResourceObject(sTitle).ToString
            Else
                sTitle = GetGlobalResourceObject(sGlobalResourceClass, sTitle).ToString
            End If
        End If
        Return "<tr><td class=""MainHead"" style=""text-decoration:underline;""><br />" & sTitle & "</td></tr>"
    End Function

    Function ProfileAddressThird(ByVal sCity As String, ByVal sState As String, ByVal sZip As String) As String
        Dim sAddress As String = sCity
        If sState <> "none" And Not String.IsNullOrEmpty(sState) Then sAddress &= ", " & sState
        If Not String.IsNullOrEmpty(sZip) Then sAddress &= ", " & sZip
        Return sAddress
    End Function
End Class
