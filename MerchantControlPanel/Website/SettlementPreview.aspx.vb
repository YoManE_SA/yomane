﻿Imports System.Collections.Generic
Imports System.Linq
Imports Netpay.Bll
Imports Netpay.Web.Controls

Partial Class Website_SettlementPreview
	Inherits MasteredPage

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		wcTotals.CompanyID = Merchant.ID
		wcTotals.SettlemntPreview = True

		If Not IsPostBack Then
			' filter unused currencies
            Dim filters = New Bll.Transactions.Transaction.SearchFilters()
            filters.hasEpa = True
            filters.merchantIDs = New List(Of Integer)
            filters.merchantIDs.Add(Merchant.ID)
            Dim currencyCount As Dictionary(Of Integer, Netpay.Infrastructure.CountAmount) = Bll.Transactions.Transaction.GetUnsettledBalance(filters)
            Dim usedCurrenciesIDs() As Integer = currencyCount.Where(Function(kvp) kvp.Value.Count > 0).Select(Function(kvp) kvp.Key).ToArray()
			ddCurrency.SetCurrencyIDs(usedCurrenciesIDs)

            If usedCurrenciesIDs.Count() > 0 Then
                mvTotals.SetActiveView(vTransactionsFound)
                wcTotals.Currency = usedCurrenciesIDs(0)
                wcTotals.Visible = True
                phCurrency.Visible = True
            Else
                mvTotals.SetActiveView(vTransactionsNotFound)
                phCurrency.Visible = False
            End If
		End If
	End Sub

	Protected Sub Show(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShow.Click
		Dim currencyID As Integer
		If Not Integer.TryParse(ddCurrency.SelectedValue, currencyID) Then
			Exit Sub
		End If

		wcTotals.Currency = currencyID
		wcTotals.Visible = True
	End Sub
End Class
