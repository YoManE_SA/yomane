﻿Imports Netpay.Infrastructure
Imports Netpay.Bll
Imports Netpay.Web
Imports Netpay.Bll.Shop

Partial Class ShopProducts
    Inherits MasteredPage

    Public ShareProductLink As String
    Private dataSource As List(Of Netpay.Bll.Shop.Products.Product)

    Private _shop As RegionShop
    Protected ReadOnly Property CurrentShop As RegionShop
        Get
            If _shop Is Nothing Then
                Dim id As Integer
                Dim result As Boolean = Integer.TryParse(Request("shopId"), id)
                If result Then
                    _shop = RegionShop.Load(id)
                End If
                If _shop Is Nothing Then Response.Redirect("ShopSelection.aspx", True)
            End If
            Return _shop
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not IsPostBack Then
            ShareProductLink = Request("ShareProductLink")
            phShare.Visible = Not String.IsNullOrEmpty(ShareProductLink)
            If phShare.Visible Then phShare.DataBind()
            ddlCategory.DataSource = Bll.Shop.Products.Category.LoadForMerchant(Merchant.ID)
            ddlCategory.DataBind()
            OnPageChanged(Me, e)
        Else
            phShare.Visible = False
        End If
    End Sub

    Protected Sub OnPageChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim filters = New Bll.Shop.Products.Product.SearchFilters()
        filters.MerchantId = Merchant.ID
        filters.Name = txtText.Text.Trim()
        filters.ShopId = CurrentShop.ShopId
        If Not String.IsNullOrEmpty(ddlCategory.Value) Then filters.Categoryies = New List(Of Integer) From {ddlCategory.Value.ToNullableInt().GetValueOrDefault()}
        dataSource = Netpay.Bll.Shop.Products.Product.Search(filters, pager.Info).OrderBy(Function(x) x.SortOrder).ToList()
        rptItems.DataSource = dataSource
        rptItems.DataBind()
    End Sub

    Protected Sub Search_Click(Sender As Object, e As EventArgs)
        pager.Info.PageCurrent = 0
        OnPageChanged(Sender, e)
    End Sub

    Protected Sub Product_OnItemCommand(Sender As Object, e As RepeaterCommandEventArgs)
        Dim product = Bll.Shop.Products.Product.Load(e.CommandArgument)
        If e.CommandName = "EnableItem" Or e.CommandName = "DisableItem" Then
            product.IsActive = (e.CommandName = "EnableItem")
            product.Save()
            e.Item.FindControl("btnEnable").Visible = Not product.IsActive
            e.Item.FindControl("btnDisable").Visible = product.IsActive
        ElseIf e.CommandName = "DeleteItem" Then
            If Not String.IsNullOrEmpty(product.ImageFileName) Then
                Dim imageName As String = Account.MapPublicPath(product.ImageFileName)
                If System.IO.File.Exists(imageName) Then System.IO.File.Delete(imageName)
            End If
            product.Delete()
            rptItems.DataSource = Netpay.Bll.Shop.Products.Product.Search(New Bll.Shop.Products.Product.SearchFilters() With {.MerchantId = Merchant.ID}, Nothing).OrderBy(Function(x) x.SortOrder)
            'rptItems.DataBind()
            OnPageChanged(Me, e)
            Return
        ElseIf e.CommandName = "ShowOrder" Then
            CType(e.Item.FindControl("viewStatus"), MultiView).ActiveViewIndex = 1
        ElseIf e.CommandName = "SetOrder" Then
            product.SortOrder = CType(e.Item.FindControl("txtOrder"), TextBox).Text.ToNullableInt().GetValueOrDefault()
            product.Save()
            e.Item.DataItem = product
            e.Item.DataBind()
            Dim mv As MultiView = CType(e.Item.FindControl("viewStatus"), MultiView)
            mv.ActiveViewIndex = 0
            Dim script = "$('#" & mv.FindControl("imageEdit").ClientID & "').css('display', 'none'); $('#" & mv.FindControl("imageSaved").ClientID & "').css('display', 'inline');" &
                "setTimeout(function () { $('#" & mv.FindControl("imageSaved").ClientID & "').fadeOut(); $('#" & mv.FindControl("imageEdit").ClientID & "').fadeIn(); }, 5000);"
            ScriptManager.RegisterStartupScript(mv, mv.GetType(), "Load_Ani", script, True)
        End If
    End Sub

    Protected Sub Product_OnItemDataBound(Sender As Object, e As RepeaterItemEventArgs)
        If e.Item.DataItem Is Nothing Then Exit Sub
        Dim item = CType(e.Item.DataItem, Bll.Shop.Products.Product)
        Dim pppParams As String = "merchantID=" & Bll.Accounts.Account.Current.AccountNumber & "&item=" & item.ID
        'CType(e.Item.FindControl("imgQrCode"), Image).ImageUrl = String.Format("{0}QrCodes.asmx/RenderGenerateCode?target=PublicPaymentPageV2&urlParams={1}", WebUtils.CurrentDomain.WebServicesUrl, HttpUtility.UrlEncode(pppParams))
        CType(e.Item.FindControl("imgProduct"), Image).ImageUrl = Account.MapPublicVirtualPath(item.ImageFileName)
        CType(e.Item.FindControl("imgProduct"), Image).Visible = Not String.IsNullOrEmpty(item.ImageFileName)
    End Sub
End Class
