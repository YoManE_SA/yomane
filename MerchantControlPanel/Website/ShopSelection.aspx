<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" CodeBehind="ShopSelection.aspx.vb" Inherits="Netpay.MerchantControlPanel.ShopSelection" %>

<%@ Register Src="~/Website/Controls/ShopsWizard.ascx" TagPrefix="uc1" TagName="ShopsWizard" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$ Resources: SelectionPageTitle %>" runat="server" />
</asp:Content>
<asp:Content ContentPlaceHolderID="cphBody" runat="server">

    <script type="text/javascript">
        function confirmDelete() {
            var msg = '<asp:Literal runat="server" Text="<%$ Resources: deleteConfirmation%>" />';
            return confirm(msg);
        }
    </script>
    <div class="area-content">
        <div class="top-nav">
            <asp:Literal Text="<%$ Resources: PageTitle %>" runat="server" />
        </div>
        <div class="content-background">
            <uc1:ShopsWizard runat="server" ID="ShopsWizard" />

            <div class="section">
                <div class="alert alert-info margin-bottom-10">
                    <i class="fa fa-info-circle"></i>
                    <asp:Literal Text="<%$ Resources: IntroShopsGuide %>" runat="server" />
                </div>
                <div class="margin-bottom-10 frame-border-option">
                    <asp:Repeater ID="repeaterShops" runat="server">
                        <ItemTemplate>
                            <div class="frame-border-option-inline padding-10 margin-bottom-10">
                                <div class="align-left">
                                    <a href="<%# Eval("ShopUrl") %>" class="btn btn-info btn-cons-short" target="_blank"><i class="fa fa-shopping-bag"></i>
                                        <asp:Literal Text="<%$ Resources: PreviewShop %>" runat="server" />
                                    </a>
                                    <asp:Repeater ID="repeaterRegions" Visible="<%# CType(Container.DataItem, Netpay.Bll.Shop.RegionShop).Locations.Regions.Count > 0 %>" DataSource="<%# CType(Container.DataItem, Netpay.Bll.Shop.RegionShop).Locations.Regions %>" runat="server">
                                        <ItemTemplate>
                                            <div class="line-height-30">
                                                <a href="<%# DataBinder.Eval(CType(Container.Parent.NamingContainer, RepeaterItem).DataItem, "ShopUrl") %>" class="grey-color" target="_blank"><%# Netpay.Web.WebUtils.GetResourceValue("Regions", Eval("IsoCode")) %></a>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                                <div class="align-right">
                                    <a href="ShopLocations.aspx?shopId=<%# Eval("ShopId") %>" class="btn btn-inverse btn-cons-short"><i class="fa fa-globe"></i>
                                        <asp:Localize Text="<%$ Resources: Locations %>" runat="server" /></a>
                                    <a href="ShopSetup.aspx?shopId=<%# Eval("ShopId") %>" class="btn btn-inverse btn-cons-short"><i class="fa fa-cog"></i>
                                        <asp:Localize Text="<%$ Resources: Settings %>" runat="server" /></a>
                                    <a href="ShopProducts.aspx?shopId=<%# Eval("ShopId") %>" class="btn btn-inverse btn-cons-short"><i class="fa fa-folder-open"></i>
                                        <asp:Localize Text="<%$ Resources: Products %>" runat="server" /></a>
                                    <asp:LinkButton ID="btnDelete" OnCommand="btnDelete_Command" CssClass="btn btn-danger btn-cons-short" CommandArgument='<%# Eval("ShopId") %>' OnClientClick="return confirmDelete()" runat="server"><i class="fa fa-trash-o"></i> <asp:Localize Text="<%$ Resources: Delete %>" runat="server" /></asp:LinkButton>
                                </div>
                                <div class="spacer"></div>
                                <asp:Repeater ID="repeaterCountries" Visible="<%# CType(Container.DataItem, Netpay.Bll.Shop.RegionShop).Locations.Countries.Count > 0 %>" DataSource="<%# CType(Container.DataItem, Netpay.Bll.Shop.RegionShop).Locations.Countries %>" OnItemDataBound="repeaterCountries_ItemDataBound" runat="server">
                                    <HeaderTemplate>
                                        <div style="margin: 10px 0 0 0; padding: 5px !important; border-top: 1px solid #eee; background-color: #F8F8F8; border: 1px solid #E9E9E9; border-radius: 3px;">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="country-list">
                                            <img id="imgFlag" width="20" height="12" runat="server" />
                                            <%# Netpay.Web.WebUtils.GetResourceValue("Countries", Eval("IsoCode2")) %>
                                        </div>

                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <div class="spacer"></div>
                                        </div>
                                         
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <div class="create-button">
                        <a href="ShopLocations.aspx" class="btn btn-success btn-cons-short"><i class="fa fa-plus-circle"></i>
                            <asp:Localize Text="<%$ Resources: CreateNew %>" runat="server" /></a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
