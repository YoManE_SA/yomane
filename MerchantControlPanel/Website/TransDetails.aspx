﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_TransDetailsCreditcard" Codebehind="TransDetails.aspx.vb" %>
<%@ Import Namespace="Netpay.CommonTypes" %>
<%@ Register Src="~/Website/Controls/TransactionInfo.ascx" TagPrefix="uc1" TagName="TransactionInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
		<title></title>
          <link href="Plugins/font-awesome-4.5.0/css/font-awesome.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="Plugins/jquery-ui-1.8.10.custom/js/jquery-1.4.4.min.js"></script>
        <script type="text/javascript" src="Plugins/jquery-ui-1.8.10.custom/js/jquery-ui-1.8.10.custom.min.js"></script>

        <netpay:ScriptManager ID="ScriptManager1" runat="server" EnableSessionAlerts="true" />

        <script type="text/javascript">
            $(function () { netpay.Timezone.convert(); });
        </script>
	</head>
	<body runat="server" style="height:auto;">
		<form id="form1" runat="server">
			<div class="table-iframe">
				<div class="table-iframe-content">
                    <uc1:TransactionInfo runat="server" id="TransactionInfo" />
				</div>
			</div>
		</form>
	</body>
</html>
