﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_SettlementPreview" CodeBehind="SettlementPreview.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$ Resources:PageTitle %>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="alert-info margin-bottom-10">
            <asp:Localize Text="<%$ Resources:Disclaimer %>" runat="server" />
        </div>
        <div class="top-nav">
            <asp:Localize Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">
            <asp:PlaceHolder ID="phCurrency" runat="server">
                <div class="section">
                    <div class="line-height-35">
                        <asp:Literal Text="<%$Resources:MultiLang,Currency %>" runat="server" />&nbsp;
                        <netpay:CurrencyDropDown ID="ddCurrency" EnableBlankSelection="false" runat="server" CssClass="Field_90" />
                    </div>
                    <div class="wrap-button-bar">
                        <asp:LinkButton ID="btnShow"  CssClass="btn btn-cons-short btn-inverse" runat="server"><i class="fa fa-search"></i> <asp:Literal Text="<%$Resources:MultiLang,Search %>" runat="server" /></asp:LinkButton>
                    </div>
                </div>
            </asp:PlaceHolder>
            <asp:MultiView ID="mvTotals" runat="server">
                <asp:View ID="vTransactionsFound" runat="server">
                    <div class="section">
                        <table border="0" align="center" class="exspand-table">
                            <tr>
                                <td style="border: 0;">
                                    <netpay:Totals Visible="false" runat="server" ID="wcTotals" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:View>
                <asp:View ID="vTransactionsNotFound" runat="server">
                    <div class="section">
                        <table>
                            <tr>
                                <td>
                                    <asp:Literal ID="Literal2" Text="<%$ Resources:NoTransactions %>" runat="server"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:View>
            </asp:MultiView>
        </div>
    </div>
</asp:Content>
