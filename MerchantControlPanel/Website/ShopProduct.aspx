﻿<%@ Page Language="vb" AutoEventWireup="false" EnableViewState="true" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" CodeBehind="ShopProduct.aspx.vb" Inherits="Netpay.MerchantControlPanel.ShopProduct" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Website/Controls/ImageUpload.ascx" TagName="ImageUpload" TagPrefix="UC" %>
<%@ Register Src="~/Website/Controls/ShareLinks.ascx" TagPrefix="UC" TagName="ShareLinks" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
    <link href="Plugins/tags/jquery.tagsinput.css" rel="stylesheet" />
    <script type="text/javascript" src="Plugins/tooltipsy-master/tooltipsy.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#chkRecurring').change(function () {
                if ($(this).is(":checked")) {
                    $('#RecurringPanel').slideDown();

                    if ($('#tblRecurring > tbody > tr').length == 0) {
                        __doPostBack('<%= btnAddRecurring.UniqueID %>', '')
                    }

                    $('#chkIsDynamicProduct').attr('checked', false);
                    setDynamicMode(false);
                }
                else {
                    $('#RecurringPanel').slideUp();
                }
            });

            $('#txtPrice').change(function () {
                $('#txtRcurringAmount').val($('#txtPrice').val());
            });

            // section toggle event
            $(".wrap-edit-product").css("cursor", "pointer").click(function () {
                toggleSection($(this));
            });

            // open section on validation error
            $(".btn-success").click(function () {
                setTimeout(openInvalidSections, 100);
            });

            // open first section if new product
            if ($("[id$=btnDelete]").length == 0)
                openSection($($(".wrap-edit-product")[0]));

            // set prod type buttons
            $("[id*='rblProdType']").buttonset()
        });

        function setDynamicMode(isDynamic) {
            $('#<%= txtPrice.ClientID %>').attr('disabled', isDynamic);
            ValidatorEnable(document.getElementById("<%= ValidRequiredfieldAmount.ClientID %>"), !isDynamic);
            ValidatorEnable(document.getElementById("<%= ValidNegativeNumbersfield.ClientID %>"), !isDynamic);

            if (isDynamic)
                $('#chkRecurring').attr('checked', false).trigger("change");
        }

        //var lastSelLanguage = null;
        //function changeLanguage(newLng) {
        //    if (lastSelLanguage == newLng) return;
        //    if (lastSelLanguage != null) {
        //        lastSelLanguage.slideUp();
        //        lastSelLanguage = null;
        //    }
        //    if (!newLng) return;
        //    newLng.slideDown();
        //    lastSelLanguage = newLng;
        //}

        function toggleSection(section) {
            if (section.next().css("display") == "none")
                openSection(section);
            else
                closeSection(section);
        }

        function openInvalidSections() {
            $(".require").filter(function () { return $(this).css("display") != "none" && $(this).css("visibility") != "hidden" }).parents(".frame-border-option-inline").prev().each(function (index, value) {
                openSection($(value));
            });
        }

        function openSection(section) {
            section.children(".align-right").children("i").switchClass("fa-angle-right", "fa-angle-down", 0, null);
            section.next().css("display", "block");
        }

        function closeSection(section) {
            section.children(".align-right").children("i").switchClass("fa-angle-down", "fa-angle-right", 0, null);
            section.next().css("display", "none");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources: ProductDetailsStr %>" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBody" runat="server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Literal runat="server" Text="<%$Resources: ProductDetailsStr %>" />
        </div>
        <div class="content-background">
            <div class="align-left">
                <a href="ShopProducts.aspx?shopId=<%= CurrentShop.ShopId %>" class="btn btn-inverse btn-cons-short"><i class="fa fa-arrow-circle-left"></i>
                    <asp:Literal runat="server" Text="<%$Resources:  BacktoProductslist%>" /></a>
            </div>
            <div class="spacer"></div>
            <asp:UpdatePanel ID="upPublished" ChildrenAsTriggers="true" RenderMode="Block" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <div class="section" style="margin-bottom: 20px !important;">
                        <div id="divNotActive" runat="server" class="alert alert-warning percent80 align-left">
                            <i class="fa fa-warning"></i>
                            <asp:Literal runat="server" Text="<%$Resources: PublishedStr %>" />
                        </div>
                        <div id="divActive" runat="server" class="alert noerror align-left percent85">
                            <i class="fa fa-check-circle"></i>
                            <asp:Literal runat="server" Text="<%$Resources: NotPublishedStr %>" />
                        </div>
                        <div class="align-right padding-5-top">
                            <asp:LinkButton runat="server" ID="btnEnableProduct" OnClick="ToggleProduct" CausesValidation="false" Font-Size="17" ForeColor="Gray"><i class="fa fa-toggle-off fa-2x"></i></asp:LinkButton>
                            <asp:LinkButton runat="server" ID="btnDisableProduct" OnClick="ToggleProduct" CausesValidation="false" Font-Size="17" ForeColor="#1D9E74"><i class="fa fa-toggle-on fa-2x"></i></asp:LinkButton>
                        </div>
                        <div class="spacer"></div>
                    </div>
                </ContentTemplate>

                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnEnableProduct" />
                    <asp:AsyncPostBackTrigger ControlID="btnDisableProduct" />
                </Triggers>
            </asp:UpdatePanel>

            <div class="section">
                <div class="margin-bottom-10">
                    <div class="align-right">
                        <asp:LinkButton ID="btnDelete" runat="server" class="btn btn-danger btn-cons-short" CausesValidation="false" OnClick="Delete_Click" OnClientClick="return confirm('Are you sure you want to delete this item?');"> <i class="fa fa-trash-o"></i> <asp:Literal runat="server" Text="<%$Resources: DeleteStr %>" /></asp:LinkButton><asp:HyperLink ID="btnPreview" class="btn btn-info btn-cons-short" runat="server" NavigateUrl='<%# ProductLink %>' Target="_blank"><i class="fa fa-eye"></i> <asp:Literal runat="server" Text="<%$Resources: PreviewStr %>" /></asp:HyperLink><asp:LinkButton runat="server" class="btn btn-success btn-cons-short" ValidationGroup="EditForm" OnClick="Save_Click"><i class="fa fa-floppy-o"></i> <asp:Literal runat="server" Text="<%$Resources: SaveStr %>" /></asp:LinkButton></div><div class="align-left line-height-35">
                        <i class="fa fa-pencil-square-o"></i>
                        <asp:Literal runat="server" Text="<%$Resources: ProductDetailsStr %>" />
                    </div>
                    <div class="spacer"></div>
                </div>
                <div class="margin-bottom-10 frame-border-option">
                    <ul class="edit-list-product">

                        <li>
                            <!-- Product published -->
                            <div class="wrap-edit-product"> <div class="align-left">
                                    <i class="fa fa-file"></i>
                                    <asp:Literal runat="server" Text="<%$Resources: TypeofProduct %>" />
                                </div>
                                <div class="align-right">
                                    <i class="fa fa-angle-right fa-2x"></i>
                                </div>
                                <div class="spacer"></div>
                            </div>
                            <div class="frame-border-option-inline padding-10 margin-bottom-10" style="display: none;">
                                <!-- Product type -->
                                <div class="alert-info margin-bottom-10">
                                    <asp:Literal runat="server" Text="<%$Resources: PhysicalProducts %>" /><br />
                                    <asp:Literal runat="server" Text="<%$Resources: DownloadProducts %>" /><br />
                                    <asp:Literal runat="server" Text="<%$Resources: VirtualProducts %>" />

                                </div>

                                <div>
                                    <div>
                                        <asp:DropDownList ID="ddProdType" OnSelectedIndexChanged="ddProdType_SelectedIndexChanged" AutoPostBack="true" runat="server">
                                            <asp:ListItem Value="1" Text="<%$Resources: Product_Pyshical %>" Selected="True"></asp:ListItem><asp:ListItem Value="2" Text="<%$Resources: Product_Virtual %>"></asp:ListItem><asp:ListItem Value="3" Text="<%$Resources: Product_Download %>"></asp:ListItem></asp:DropDownList></div><div class="spacer"></div>
                                </div>
                            </div>
                            <!-- End Product published -->
                        </li>
                        <li>
                            <!-- Product Image -->
                            <div class="wrap-edit-product">
                                <div class="align-left">
                                    <i class="fa fa-picture-o"></i>
                                    <asp:Literal runat="server" Text="<%$Resources: ProductPictureStr %>" />
                                </div>
                                <div class="align-right">
                                    <i class="fa fa-angle-right fa-2x"></i>
                                </div>
                                <div class="spacer"></div>
                            </div>
                            <div class="frame-border-option-inline padding-10 margin-bottom-10 " style="display: none;">
                                <!-- Product image -->
                                <div class="alert alert-info">
                                    <i class="fa fa-picture-o"></i>
                                    <asp:Literal runat="server" Text="<%$Resources: ImageSizeStr %>" /><br />
                                    <asp:Literal runat="server" Text="<%$Resources: FileTypes %>" />
                                </div>
                                <div class="margin-top-10 margin-bottom-10">
                                    <div id='injection_site'></div>
                                    <UC:ImageUpload runat="server" ID="iuImage" Width="400px" Height="400px" BytesSizeLimit="1024000" ImageUrl='<%# Account.MapPublicVirtualPath(CurrentProduct.ImageFileName)%>' OnImageDelete="OnImageDeleteClick" /><div class="spacer"></div>
                                </div>
                            </div>
                            <!-- End Product Image -->
                        </li>
                        <li>
                            <!-- Product properties -->
                            <div class="wrap-edit-product">
                                <div class="align-left">
                                    <i class="fa fa-pencil-square"></i>
                                    <asp:Literal ID="Literal19" runat="server" Text="<%$Resources: ProductProperties %>" />
                                </div>
                                <div class="align-right">
                                    <i class="fa fa-angle-right fa-2x"></i>
                                </div>
                                <div class="spacer"></div>
                            </div>
                            <div class="frame-border-option-inline padding-10 margin-bottom-10" style="display: none;">
                                <asp:UpdatePanel ID="upLanguages" runat="server" ChildrenAsTriggers="true" RenderMode="Block" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div runat="server" id="dvLanguageData">
                                            <div class="tabs_container">
                                                <ul id="productProperties_Tabs" class="tabs-product">
                                                    <li class="active"><a href="#productProperties_Tab1"><i class="fa fa-pencil-square-o"></i>
                                                        <asp:Literal runat="server" Text="<%$Resources: ProductDetalis %>" />
                                                    </a></li>
                                                    <li><a href="#productProperties_Tab2"><i class="fa fa-chain-broken"></i>
                                                        <asp:Literal runat="server" Text="<%$Resources: SEOTools %>" /></a></li>
                                                    <li><a href="#productProperties_Tab3"><i class="fa fa-share"></i>
                                                        <asp:Literal runat="server" Text="<%$Resources: Share %>" /></a></li>
                                                    <li><a href="#productProperties_Tab4"><i class="fa fa-qrcode"></i>
                                                        <asp:Literal runat="server" Text="<%$Resources: SQRCode %>" /></a></li>
                                                </ul>
                                            </div>
                                            <div id="productProperties_TabViews" class="tabs_content_container">
                                                <div id="productProperties_Tab1" class="tab_content" style="display: block;">
                                                    <div class="sub-language">
                                                        <div>
                                                            <span class="form-group-title">
                                                                <asp:Literal runat="server" Text="<%$Resources: ProductNameStr %>" /></span>
                                                        </div>
                                                        <div class="margin-bottom-10">
                                                            <asp:TextBox ID="txtName" MaxLength="100" placeholder="<%$Resources: EGproductName %>" runat="server" class="percent100 margin-right-left-3" />
                                                            <asp:RequiredFieldValidator ValidationGroup="EditForm" runat="server" CssClass="require" Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtName" ErrorMessage="<%$Resources: ValidRequiredfieldProductName %>" />
                                                        </div>
                                                        <div>
                                                            <span class="form-group-title">
                                                                <asp:Literal runat="server" Text="<%$Resources: DescriptionStr %>" /></span>
                                                        </div>
                                                        <div class="margin-bottom-10">
                                                            <asp:TextBox ID="txtDescription" TextMode="Multiline" MaxLength="200" placeholder="<%$Resources: EGMetaDescription %>" runat="server" CssClass="percent100 percent-height-10" />
                                                            <asp:RequiredFieldValidator ValidationGroup="EditForm" runat="server" CssClass="require" Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtDescription" ErrorMessage="<%$Resources: ValidRequiredfieldDescription %>" />
                                                        </div>
                                                        <div>
                                                            <span class="form-group-title">
                                                                <asp:Literal runat="server" Text="<%$Resources: ReceiptTextStr %>" /></span>
                                                        </div>
                                                        <div class="margin-bottom-10">
                                                            <asp:TextBox ID="txtReceiptText" TextMode="MultiLine" MaxLength="200" placeholder="<%$Resources: EGReceiptTextStr %>" runat="server" CssClass="percent100 percent-height-10" />
                                                        </div>
                                                        <div>
                                                            <span class="form-group-title">
                                                                <asp:CheckBox ID="chkReceiptLink" AutoPostBack="true" OnCheckedChanged="chkReceiptLink_CheckedChanged" ClientIDMode="Static" runat="server" />
                                                                <asp:Literal runat="server" Text="<%$Resources: LinkProductStr %>" />
                                                        </div>
                                                        <div class="alert alert-warning margin-bottom-10">
                                                            <i class="fa  fa-info-circle"></i>
                                                            <asp:Literal runat="server" Text="<%$Resources: EmailLink %>" />

                                                        </div>
                                                        <div class="margin-bottom-10">
                                                            <asp:TextBox ID="txtReceiptLink" ClientIDMode="Static" MaxLength="200" placeholder="http://www.test.com/example.pdf" runat="server" CssClass="percent100" />
                                                        </div>

                                                    </div>
                                                </div>
                                                <div id="productProperties_Tab2" class="tab_content">
                                                    <div class="sub-language">
                                                        <div>
                                                            <span class="form-group-title">
                                                                <asp:Literal runat="server" Text="<%$Resources: MetaTitleStr %>" /></span>
                                                        </div>
                                                        <div class="margin-bottom-10">
                                                            <asp:TextBox ID="txtMetaTitle" MaxLength="200" placeholder="<%$Resources: EGproductName %>" runat="server" CssClass="percent100 percent-height-10" />
                                                        </div>
                                                        <div>
                                                            <span class="form-group-title">
                                                                <asp:Literal runat="server" Text="<%$Resources: MetaKewordStr %>" /></span>
                                                        </div>
                                                        <div class="margin-bottom-10">
                                                            <asp:TextBox ID="txtMetaKeyword" MaxLength="200" placeholder="<%$Resources: EGMetaKeywords %>" runat="server" CssClass="percent100 percent-height-10" />
                                                        </div>
                                                        <div>
                                                            <span class="form-group-title">
                                                                <asp:Literal runat="server" Text="<%$Resources: MetaDescriptionStr %>" /></span>
                                                        </div>
                                                        <div class="margin-bottom-10">
                                                            <asp:TextBox ID="txtMetaDescription" TextMode="MultiLine" MaxLength="200" placeholder="<%$Resources: EGMetaDescription %>" runat="server" CssClass="percent100 percent-height-10" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="productProperties_Tab3" class="tab_content">
                                                    <% If Request("ID") = Nothing Then%>
                                                    <div class="sub-language">
                                                        <asp:Literal runat="server" Text="<%$Resources: SaveBeforeShare %>" />
                                                        <div class="spacer"></div>
                                                    </div>
                                                    <% Else%>
                                                    <div class="sub-language">
                                                        <div>
                                                            <span class="form-group-title">
                                                                <asp:Literal runat="server" Text="<%$Resources: ProductLinkStr %>" /></span>&nbsp;<span class="form-group-title-eg"></span> </div><div class="margin-bottom-10 line-height-35">
                                                            <asp:TextBox ID="txtProductLink" ClientIDMode="Static" runat="server" ReadOnly="true" CssClass="percent85" />
                                                        </div>
                                                        <div id="ShareLinks">
                                                            <UC:ShareLinks ID="ctlShareLinks" ClientIDMode="Static" runat="server" />
                                                        </div>
                                                        <div class="spacer"></div>
                                                    </div>
                                                    <% End If%>
                                                </div>
                                                <div id="productProperties_Tab4" class="tab_content">
                                                    <div class="sub-language">
                                                        <asp:Literal runat="server" Text="<%$Resources: SQRCodeDesc %>" />
                                                        <div>
                                                            <asp:Image ID="imgQrCode" CssClass="sqr-border" Width="100" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <%--                                        
                                        <div class="margin-bottom-10 line-height-35">
                                            <asp:Literal runat="server" Text="<%$Resources:  DifferentLanguagesStr %>" />
                                            <netpay:DropDownBase runat="server" ID="ddlLanguages" BlankSelectionText="<%$Resources:  ChooseLanguagesStr %>" DataTextField="Name" DataValueField="Culture" CssClass="percent25" />
                                            <asp:LinkButton runat="server" Text="<%$Resources: CreateStr %>" ID="btnLanguageAdd" CommandName="Language_Add" CausesValidation="false" OnCommand="Button_OnCommand" CssClass="btn btn-inverse" />
                                        </div>
                                        --%>
                                    </span></ContentTemplate>
                                </asp:UpdatePanel>
                                <div class="align-left form-group">
                                    <div>
                                        <span class="form-group-title">
                                            <asp:Literal runat="server" Text="<%$Resources: OrderStr %>" /></span>&nbsp;<span class="form-group-title-eg"><asp:Literal runat="server" Text="<%$Resources: EGsortStr %>" /></span>
                                    </div>
                                    <div class="margin-bottom-10">
                                        <asp:TextBox ID="txtSortOrder" MaxLength="4" placeholder="<%$Resources: EGsortStr %>" Text="<%# CurrentProduct.SortOrder%>" runat="server" class="percent23  margin-right-left-3" />
                                        <asp:RegularExpressionValidator runat="server" Display="Dynamic" CssClass="require" ValidationExpression="\d+" ControlToValidate="txtSortOrder" ErrorMessage="<%$Resources: ValidNumbersfieldSort %>" />
                                        <asp:RangeValidator runat="server" Type="Integer" Display="Dynamic" CssClass="require" MinimumValue="1" MaximumValue="200" ControlToValidate="txtSortOrder" ErrorMessage="<%$Resources: ValidRangeOrder %>" />
                                    </div>
                                </div>
                                <div class="spacer"></div>
                            </div>
                            <!-- End Product properties -->
                        </li>
                        <li>
                            <!-- Product price -->
                            <div class="wrap-edit-product">
                                <div class="align-left">
                                    <i class="fa fa-money"></i>
                                    <asp:Literal runat="server" Text="<%$Resources: PriceStr %>" />
                                </div>
                                <div class="align-right">
                                    <i class="fa fa-angle-right fa-2x"></i>
                                </div>
                                <div class="spacer"></div>
                            </div>
                            <div class="frame-border-option-inline padding-10 margin-bottom-10" style="display: none;">

                                <div class="form-group align-left percent23">
                                    <span class="form-group-title">
                                        <asp:Literal runat="server" Text="<%$Resources: PriceStr %>" /></span>
                                    <%--                    <span class="form-group-title-eg">
                        <asp:Literal runat="server" Text="<%$Resources: EGAmount %>" /></span>--%>
                                    <div>
                                        <asp:TextBox ID="txtPrice" MaxLength="8" ClientIDMode="Static" Text='<%# CurrentProduct.Price.ToString("0.00") %>' placeholder="<%$Resources: EGAmount %>" runat="server" CssClass="percent100" />
                                        <asp:RequiredFieldValidator ID="ValidRequiredfieldAmount" Display="Dynamic" ClientIDMode="Static" ValidationGroup="EditForm" SetFocusOnError="true" runat="server" CssClass="require" ControlToValidate="txtPrice" ErrorMessage="<%$Resources: ValidRequiredfieldAmount %>" />
                                        <asp:RangeValidator ID="ValidNegativeNumbersfield" Display="Dynamic" ClientIDMode="Static" ValidationGroup="EditForm" SetFocusOnError="true" runat="server" CssClass="require" MinimumValue="1" MaximumValue="999999" Type="Double" ControlToValidate="txtPrice" ErrorMessage="<%$Resources: ValidNegativeNumbersfield %>" />
                                    </div>
                                </div>
                                <div class="form-group align-left percent23">
                                    <span class="form-group-title">
                                        <asp:Literal runat="server" Text="<%$Resources: CurrencyStr %>" /></span>&nbsp;<%--<span class="form-group-title-eg"><asp:Literal runat="server" Text="<%$Resources: EGCurrency %>" /></span>--%> <div>
                                        <%# CurrentShop.CurrencyIsoCode %>
                                        <%--<netpay:CurrencyDropDown ID="ddlCurrencyISOCode" Visible="true" runat="server" ClientIDMode="Static" SelectedCurrencyIso='<%# CurrentProduct.CurrencyISOCode %>' EnableBlankSelection="false" CssClass="percent100" />--%>
                                    </div>
                                </div>
                                <div class="form-group align-left percent23">
                                    <span class="form-group-title">
                                        <asp:Literal runat="server" Text="<%$Resources: AuthorizeStr %>" /></span>
                                    <div>
                                        <asp:DropDownList runat="server" ID="ddlIsAuthorize" SelectedIndex='<%# IIf(CurrentProduct.IsAuthorize, 0, 1)  %>' CssClass="percent100">
                                            <asp:ListItem Text="<%$Resources: AuthorizeOptionStr %>" />
                                            <asp:ListItem Text="<%$Resources: AuthorizeCaptureOptionStr %>" />
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="spacer"></div>
                                <div class="margin-top-10 padding-10">
                                    <div>
                                        <asp:CheckBox ID="chkIsDynamicProduct" ClientIDMode="Static" runat="server" Checked='<%# CurrentProduct.IsDynamicProduct %>' onclick="setDynamicMode($(this).is(':checked'))" />&nbsp; <span class="form-group-title"><asp:Literal runat="server" Text="<%$Resources: setAmountStr %>" /></span>
                                    </div>
                                    <div>
                                        <asp:CheckBox ID="chkRecurring" ClientIDMode="Static" runat="server" Checked='<%# ItemRecurring.Count() > 0 %>' />&nbsp; <span class="form-group-title"><asp:Literal runat="server" Text="<%$Resources: CrtRecurring %>" /></span>
                                    </div>
                                </div>
                                <div id="RecurringPanel" class="sub-language" style="display: <%# IIF(ItemRecurring.Count() > 0, "", "none") %>;">
                                    <div>
                                        <asp:LinkButton ID="btnAddRecurring" runat="server" CssClass="grey-color" CausesValidation="false" CommandName="Recurring_Add" OnCommand="Button_OnCommand">
                                            <i class="fa fa-plus-circle"></i>
                                            <asp:Literal ID="Literal7" runat="server" Text="<%$Resources: AddRecurringStr %>" />
                                        </asp:LinkButton></div><asp:UpdatePanel ID="UpdatePanel3" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" RenderMode="Block">
                                        <ContentTemplate>
                                            <netpay:DynamicRepeater runat="server" ID="rptRecurring" OnItemDataBound="rptRecurring_ItemDataBound" OnItemCommand="Button_OnCommand">
                                                <HeaderTemplate>
                                                    <div style="border-top: 1px solid #dadada; padding-top: 10px; padding-bottom: 10px; margin-top: 10px;">
                                                        <table id="tblRecurring">
                                                            <tr>
                                                                <th>
                                                                    <span class="form-group-title">
                                                                        <asp:Literal ID="Literal8" runat="server" Text="<%$Resources: Amount %>" /></span>
                                                                </th>
                                                                <th>
                                                                    <span class="form-group-title">
                                                                        <asp:Literal ID="Literal2" runat="server" Text="<%$Resources: ChargeEveryStr %>" /></span>
                                                                    <span class="form-group-title-eg">
                                                                        <asp:Literal ID="Literal3" runat="server" Text="<%$Resources: EGChargeEveryStr %>" /></span>
                                                                </th>
                                                                <th>
                                                                    <span class="form-group-title">
                                                                        <asp:Literal ID="Literal4" runat="server" Text="<%$Resources: PeriodStr %>" /></span>
                                                                </th>
                                                                <th>
                                                                    <span class="form-group-title">
                                                                        <asp:Literal ID="Literal5" runat="server" Text="<%$Resources: OverallStr %>" /></span>
                                                                </th>
                                                            </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txtRcurringAmount" ClientIDMode="Static" runat="server" Enabled="<%# Container.ItemIndex > 0 %>" CssClass="percent80" Text='<%# Eval("Amount") %>' />
                                                            <asp:RequiredFieldValidator Display="Dynamic" ValidationGroup="EditForm" SetFocusOnError="true" runat="server" CssClass="require" ControlToValidate="txtRcurringAmount" ErrorMessage="<%$Resources: ValidRequiredfieldAmount %>" />
                                                            <asp:RegularExpressionValidator Display="Dynamic" ValidationGroup="EditForm" SetFocusOnError="true" runat="server" CssClass="require" ValidationExpression="^\d+(\.\d{1,2})?$" ControlToValidate="txtRcurringAmount" ErrorMessage="<%$Resources: ValidNumbersfield %>" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox MaxLength="3" ID="txtCharges" ClientIDMode="Static" runat="server" CssClass="percent80" Text='<%# Eval("Charges") %>' />
                                                            <asp:RequiredFieldValidator ValidationGroup="EditForm" SetFocusOnError="true" runat="server" CssClass="require" Display="Dynamic" ControlToValidate="txtCharges" ErrorMessage="<%$Resources: ValidRequiredfieldCharges %>" />
                                                            <asp:RegularExpressionValidator ValidationGroup="EditForm" SetFocusOnError="true" runat="server" CssClass="require" Display="Dynamic" ValidationExpression="^\d+$" ControlToValidate="txtCharges" ErrorMessage="<%$Resources: ValidNumbersfield %>" />
                                                        </td>
                                                        <td>
                                                            <netpay:EnumDropDown ID="ddlIntervalUnit" runat="server" CssClass="percent80" ViewEnumName="Netpay.Infrastructure.ScheduleInterval" Value='<%# CType(Eval("IntervalUnit"), Integer) %>' />
                                                            <asp:RequiredFieldValidator ValidationGroup="EditForm" SetFocusOnError="true" runat="server" CssClass="require" Display="Dynamic" ControlToValidate="ddlIntervalUnit" ErrorMessage="<%$Resources: ValidRequiredfieldScheduleInterval %>" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox MaxLength="3" ID="txtIntervalCount" ClientIDMode="Static" runat="server" CssClass="percent80" Text='<%# Eval("IntervalCount") %>' />
                                                            <asp:RequiredFieldValidator ValidationGroup="EditForm" SetFocusOnError="true" runat="server" CssClass="require" Display="Dynamic" ControlToValidate="txtIntervalCount" ErrorMessage="<%$Resources: ValidRequiredfieldIntervalCount %>" />
                                                            <asp:RegularExpressionValidator ValidationGroup="EditForm" SetFocusOnError="true" runat="server" CssClass="require" Display="Dynamic" ValidationExpression="^\d+$" ControlToValidate="txtIntervalCount" ErrorMessage="<%$Resources: ValidNumbersfield %>" />
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="LinkButton1" runat="server" ValidationGroup="EditForm" CssClass="grey-color" CausesValidation="false" CommandName="Recurring_Remove">
                                                                <i class="fa fa-minus-circle"></i>
                                                                <asp:Literal ID="Literal6" runat="server" Text="<%$Resources: RemoveRecurringStr %>" />
                                                            </asp:LinkButton></td></tr></ItemTemplate><FooterTemplate>
                                                    </table>
                                    </div>
                                                </FooterTemplate>
                                            </netpay:DynamicRepeater>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="btnAddRecurring" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <div class="spacer"></div>
                                </div>


                            </div>
                            <!-- End Product price -->
                        </li>
                        <li>
                            <!-- Product stock manamgement -->
                            <div class="wrap-edit-product">
                                <div class="align-left">
                                    <i class="fa fa-briefcase"></i>
                                    <asp:Literal ID="Literal16" runat="server" Text="<%$Resources: StockManagementStr %>" />
                                </div>
                                <div class="align-right">
                                    <i class="fa fa-angle-right fa-2x"></i>
                                </div>
                                <div class="spacer"></div>
                            </div>
                            <div class="frame-border-option-inline padding-10 margin-bottom-10" style="display: none;">
                                <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" RenderMode="Block">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddProdType" />
                                    </Triggers>
                                    <ContentTemplate>
                                        <asp:MultiView runat="server" ID="mvProductType" ActiveViewIndex="<%# CType(CurrentProduct.Type, Integer) - 1%>">
                                            <asp:View runat="server" ID="viewPhysical">
                                                <div class="form-group align-left percent18">
                                                    <span class="form-group-title">
                                                        <asp:Literal ID="Literal14" runat="server" Text="<%$Resources: skuStr %>" /></span>&nbsp;<span class="form-group-title-eg"><asp:Literal ID="Literal15" runat="server" Text="<%$Resources: EGskuStr %>" /></span>
                                                    <div>
                                                        <asp:TextBox MaxLength="10" ID="txtSKU" placeholder="<%$Resources: EGskuStr %>" Text="<%# CurrentProduct.SKU %>" runat="server" class="percent100" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="EditForm" runat="server" Display="Dynamic" CssClass="require" ValidationExpression="^\d+$" ControlToValidate="txtSKU" ErrorMessage="<%$Resources: ValidNumbersfield %>" />
                                                    </div>
                                                </div>
                                                <div class="form-group align-left percent18">
                                                    <span class="form-group-title">
                                                        <asp:Literal runat="server" Text="<%$Resources: AvailableStr %>" /></span>&nbsp;<span class="form-group-title-eg"> <asp:Literal runat="server" Text="<%$Resources: EGMaxStr %>" />
                                                        </span>
                                                    <div>
                                                        <asp:TextBox MaxLength="10" ID="txtQtyAvailable" Text="<%# CurrentProduct.QtyAvailable %>" placeholder="<%$Resources: EGMaxStr %>" runat="server" class="percent100" />
                                                        <asp:RequiredFieldValidator Enabled="false" ValidationGroup="EditForm" runat="server" SetFocusOnError="true" CssClass="require" Display="Dynamic" ControlToValidate="txtQtyAvailable" ErrorMessage="<%$Resources: ValidRequiredfieldAvailable %>" />
                                                    </div>
                                                </div>
                                                <div class="form-group align-left percent18">
                                                    <span class="form-group-title">
                                                        <asp:Literal runat="server" Text="<%$Resources: StartStr %>" /></span>&nbsp;<span class="form-group-title-eg"><asp:Literal runat="server" Text="<%$Resources: EGStart %>" />
                                                        </span>
                                                    <div>
                                                        <asp:TextBox MaxLength="6" ID="txtQtyStart" Text="<%# CurrentProduct.QtyStart %>" placeholder="eg. Start" runat="server" CssClass="percent100" />
                                                        <asp:RequiredFieldValidator ValidationGroup="EditForm" runat="server" SetFocusOnError="true" CssClass="require" Display="Dynamic" ControlToValidate="txtQtyStart" ErrorMessage="<%$Resources: ValidRequiredfieldStart %>" />
                                                        <asp:RegularExpressionValidator ValidationGroup="EditForm" runat="server" CssClass="require" Display="Dynamic" ValidationExpression="^\d+$" ControlToValidate="txtQtyStart" ErrorMessage="<%$Resources: ValidNumbersfield %>" />
                                                    </div>
                                                </div>
                                                <div class="form-group align-left percent18">
                                                    <span class="form-group-title">
                                                        <asp:Literal runat="server" Text="<%$Resources: MaxStr %>" /></span>&nbsp;<span class="form-group-title-eg"><asp:Literal runat="server" Text="<%$Resources: EGMaxStr %>" /></span>
                                                    <div>
                                                        <asp:TextBox MaxLength="6" ID="txtQtyEnd" Text="<%# CurrentProduct.QtyEnd%>" placeholder="eg. Max" runat="server" CssClass="percent100" />
                                                        <asp:RequiredFieldValidator ValidationGroup="EditForm" runat="server" SetFocusOnError="true" CssClass="require" Display="Dynamic" ControlToValidate="txtQtyEnd" ErrorMessage="<%$Resources: ValidRequiredfield %>" />
                                                        <asp:RegularExpressionValidator ValidationGroup="EditForm" runat="server" CssClass="require" Display="Dynamic" SetFocusOnError="true" ValidationExpression="^\d+$" ControlToValidate="txtQtyEnd" ErrorMessage="<%$Resources: ValidNumbersfield %>" />
                                                        <asp:CompareValidator ID="RegularExpressionValidator2" ValidationGroup="EditForm" Display="Dynamic" runat="server" SetFocusOnError="true" CssClass="require" Operator="GreaterThanEqual" Type="Integer" ControlToValidate="txtQtyEnd" ControlToCompare="txtQtyStart" ErrorMessage="<%$Resources: ValidQtyEndLessStart %>" />

                                                    </div>
                                                </div>
                                                <div class="form-group align-left percent18">
                                                    <span class="form-group-title">
                                                        <asp:Literal runat="server" Text="<%$Resources: StepStr %>" /></span>&nbsp;<span class="form-group-title-eg"><asp:Literal runat="server" Text="<%$Resources: EGOverallStr %>" /></span>
                                                    <div>
                                                        <asp:TextBox MaxLength="6" ID="txtQtyStep" Text="<%# CurrentProduct.QtyStep%>" placeholder="eg. Min" runat="server" CssClass="percent100" />
                                                        <asp:RequiredFieldValidator ValidationGroup="EditForm" runat="server" SetFocusOnError="true" CssClass="require" Display="Dynamic" ControlToValidate="txtQtyStep" ErrorMessage="<%$Resources: ValidRequiredfieldStep %>" />
                                                        <asp:RegularExpressionValidator ValidationGroup="EditForm" runat="server" SetFocusOnError="true" CssClass="require" ValidationExpression="^\d+$" ControlToValidate="txtQtyStep" ErrorMessage="<%$Resources: ValidNumbersfield %>" />
                                                    </div>
                                                </div>
                                                <div class="spacer"></div>
                                                <div class="alert-info margin-bottom-10">
                                                    <i class="fa fa-info-circle"></i>
                                                    <asp:Literal runat="server" Text="<%$Resources: QuantityCommentStr %>" />
                                                </div>
                                                <div id="MultipleProducts">
                                                    <div class="form-group-title margin-bottom-10">
                                                        <i class="fa fa-pencil-square-o"></i>
                                                        <asp:Literal runat="server" Text="<%$Resources: CreateOptionStr %>" />
                                                        <span class="form-group-title-eg">
                                                            <asp:Literal runat="server" Text="<%$Resources: EGMultipleoptionsStr %>" /></span>
                                                    </div>
                                                    <div class="margin-bottom-10 frame-border-option">
                                                        <div class="line-height-35 margin-bottom-10">
                                                            <asp:TextBox runat="server" ID="txtPropertyName" MaxLength="25" />
                                                            <asp:RequiredFieldValidator Display="Dynamic" CssClass="require" runat="server" ErrorMessage="Name is required" ValidationGroup="StockOptions" ControlToValidate="txtPropertyName" />
                                                            <asp:LinkButton runat="server" ID="btnAddProperty" class="btn btn-primary" CausesValidation="false" CommandName="Property_Add" OnClientClick="if(Page_ClientValidate('StockOptions')){ return confirm('Adding or removing properties may reset your stock rows\r\ncontinue?');}" OnCommand="Button_OnCommand">
												                <i class="fa fa-plus-circle"></i> <asp:Literal runat="server" Text="<%$Resources: AddOptionStr %>" />
                                                            </asp:LinkButton></div></div><asp:UpdatePanel ID="UpdatePanel2" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" RenderMode="Block">
                                                        <ContentTemplate>
                                                            <asp:PlaceHolder runat="server" ID="phProperties">
                                                                <div class="form-group-title margin-bottom-10">
                                                                    <i class="fa fa-pencil-square-o"></i>
                                                                    <asp:Literal runat="server" Text="<%$Resources: AddOptionStr %>" />
                                                                </div>
                                                                <div class="margin-bottom-10 frame-border-option">
                                                                    <netpay:DynamicRepeater runat="server" ID="rptProperties" DataSource='<%# CurrentProduct.Properties %>' OnItemCommand="Button_OnCommand">
                                                                        <ItemTemplate>
                                                                            <div class="padding-10">
                                                                                <asp:HiddenField runat="server" ID="hfID" Value='<%# Eval("ID") %>' />
                                                                                <asp:HiddenField runat="server" ID="hfName" Value='<%# Eval("Name") %>' />
                                                                                <div class="align-left">
                                                                                    <span class="form-group-title"><%# Eval("Name") %></span>
                                                                                    <span class="form-group-title-eg">
                                                                                        <asp:Literal runat="server" Text="<%$Resources: EGSizeStr %>" /></span>
                                                                                </div>
                                                                                <div class="spacer"></div>
                                                                                <div class="align-left percent85">
                                                                                    <netpay:TagsInput runat="server" ID="txtValuesString" Text='<%# Eval("ValuesString") %>' CssClass="tags percent75 tags-height" />
                                                                                </div>
                                                                                <div class="align-right" style="padding-top: 8px;">
                                                                                    <asp:LinkButton runat="server" CssClass="btn btn-danger" Style="float: right;" OnClientClick="confirm('Are you sure you want to delete this item?');" CausesValidation="false" CommandName="Property_Remove">
                                                                                        <i class="fa fa-trash-o"></i>
                                                                                        <asp:Literal ID="Literal1" runat="server" Text="<%$Resources: RemoveStr %>" />
                                                                                    </asp:LinkButton></div><div class="spacer"></div>

                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </netpay:DynamicRepeater>
                                                                    <div class="alert alert-info margin-top-10">
                                                                        <asp:Literal runat="server" Text="<%$Resources: AddOptionStrAlert %>" />

                                                                    </div>
                                                                    <div class="percent100 margin-top-10 text-align-right" style="clear: both;">
                                                                        <div style="border-top: 1px solid #dadada; padding-top: 10px;">
                                                                            <asp:LinkButton runat="server" ID="btnGenerate" CausesValidation="false" CssClass="btn btn-inverse" CommandName="Stock_Generate" OnCommand="Button_OnCommand">
																                <i class="fa fa-plus-circle"></i> <asp:Literal runat="server" Text="<%$Resources: AutoCreateVariants %>" />
                                                                            </asp:LinkButton></div></div></div></asp:PlaceHolder></ContentTemplate><Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="btnAddProperty" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" RenderMode="Block">
                                                        <ContentTemplate>
                                                            <netpay:DynamicRepeater runat="server" ID="rptStock" OnItemCreated="rptStock_ItemCreated" OnItemCommand="Button_OnCommand">
                                                                <HeaderTemplate>
                                                                    <div class="margin-bottom-10 frame-border-option">
                                                                        <table class="tblProductProperties" id="tblStockItems">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>
                                                                                        <asp:Literal runat="server" Text="<%$Resources: VariantStr %>" />
                                                                                    </th>
                                                                                    <th>
                                                                                        <asp:Literal runat="server" Text="<%$Resources: skuStr %>" />
                                                                                    </th>
                                                                                    <th>
                                                                                        <asp:Literal runat="server" Text="<%$Resources: QuantityStr %>" />
                                                                                    </th>
                                                                                    <th>
                                                                                        <asp:Literal runat="server" Text="<%$Resources: ActionStr %>" />
                                                                                    </th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox Checked="true" runat="server" />
                                                                            <span class="checkbox-table-product">
                                                                                <asp:HiddenField runat="server" ID="hfID" Value='<%# Eval("ID") %>' />
                                                                                <asp:Repeater runat="server" ID="rptProperties" DataSource='<%# Eval("Properties") %>'>
                                                                                    <ItemTemplate>
                                                                                        <asp:HiddenField runat="server" ID="hfPropName" Value='<%# Eval("Name") %>' />
                                                                                        <%# Eval("Name") %>
                                                                                    </ItemTemplate>
                                                                                    <SeparatorTemplate>,</SeparatorTemplate></asp:Repeater></span></td><td>
                                                                            <asp:TextBox runat="server" MaxLength="10" ID="txtSKU" Text='<%# Eval("SKU") %>' CssClass="percent100 align-left" />
                                                                        <td>
                                                                            <asp:TextBox runat="server" MaxLength="10" ID="txtQtyAvailable" Text='<%# Eval("QtyAvailable") %>' CssClass="percent100 align-left" />
                                                                        </td>
                                                                        <td class="text-align-center">
                                                                            <asp:LinkButton ID="btnRemove" runat="server" ForeColor="White" CausesValidation="false" CommandName="Stock_Remove" class="btn btn-danger">
															                    <i class="fa fa-trash-o"></i>
                                                                            </asp:LinkButton></td></tr></ItemTemplate><FooterTemplate>
                                                                    </tbody>
														        </table>
                                                                <div class="margin-top-10 text-align-right">
                                                                    <asp:LinkButton CssClass="btn btn-inverse btn-cons-short" runat="server" ID="btnDeleteAllStock" CausesValidation="false" CommandName="Stock_DeleteAll" OnCommand="Button_OnCommand">
                                                                        <i class="fa fa-trash"></i>  <asp:Literal runat="server" Text="<%$Resources: DeleteVariants %>" />
                                                                    </asp:LinkButton></div></div></FooterTemplate></netpay:DynamicRepeater></ContentTemplate><Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="btnGenerate" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </div>
                                                <div class="spacer"></div>

                                            </asp:View>
                                            <asp:View runat="server" ID="viewVirtual">
                                            </asp:View>
                                            <asp:View runat="server" ID="viewDownload">
                                                <i class="fa fa-file-audio-o"></i>
                                                <asp:Literal ID="Literal17" runat="server" Text="<%$Resources: UploadFileStr %>" />

                                                <div class="spacer margin-bottom-10"></div>
                                                <div class="form-group">
                                                    <asp:PlaceHolder runat="server" ID="phMediaFileUpload" Visible='<%# String.IsNullOrEmpty(CurrentProduct.MediaFileName) %>'>
                                                        <span class="form-group-title">
                                                            <asp:Literal runat="server" Text="<%$Resources: UploadFileStr %>" /></span>&nbsp;<span class="form-group-title-eg"><asp:Literal runat="server" Text="<%$Resources: SizeFileStr %>" /></span>
                                                        <div>
                                                            <div class="fileUpload btn btn-cons-short btn-inverse margin-bottom-10 ">
                                                                <span><i class="fa fa-folder-open"></i>
                                                                    <asp:Literal runat="server" Text="<%$Resources: UploadFileStr %>" /></span>
                                                                <asp:FileUpload ID="fuProductFile" CssClass="upload" runat="server" onchange="document.getElementById('dvFileIndicator').style.display='';" />
                                                            </div>
                                                            <img id="imgUploadProgress" src="../Templates/Tmp_MahalaUS/Images/ajax-download-loader.gif" class="vertical-middle" style="display: none;" />
                                                            <div id="dvFileIndicator" class="margin-top-10" style="display: none;">
                                                                <div>
                                                                    <i class="fa fa-check-circle" style="color: #51A351"></i>
                                                                    <asp:Literal runat="server" Text="<%$Resources: FileSelectedStr %>" />
                                                                </div>
                                                                <div class="alert alert-warning">
                                                                    <asp:Literal runat="server" Text="<%$Resources: AttentionUploadedStr %>" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:PlaceHolder>
                                                    <asp:PlaceHolder runat="server" ID="phMediaFileRemove" Visible='<%# Not String.IsNullOrEmpty(CurrentProduct.MediaFileName) %>'>
                                                        <asp:Literal runat="server" Text="<%$Resources: UploadFileStr %>" />
                                                        <div class="file-process">
                                                            <div class="pull-left">
                                                                <i><%# System.IO.Path.GetFileName(CurrentProduct.MediaPysicalPath.EmptyIfNull()) %></i> <i class="fa fa-check-circle" style="color: #51A351"></i>
                                                            </div>
                                                            <div class="pull-right">
                                                                <asp:LinkButton runat="server" ID="btnRemoveMediaFile" CausesValidation="false" OnCommand="Button_OnCommand" CommandName="RemoveMediaFile" OnClientClick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"> <i class="fa fa-times-circle"></i> <asp:Literal runat="server" Text="<%$Resources: RemoveStr %>" /></asp:LinkButton></div><div class="spacer"></div>
                                                        </div>
                                                    </asp:PlaceHolder>
                                                    <asp:PlaceHolder runat="server" ID="phMediaFileError" Visible="false">
                                                        <div class="error margin-top-10">
                                                            <asp:Literal ID="literalMediaFileError" runat="server" Text="<%$Resources: ErrorSizeFileStr %>" />
                                                        </div>
                                                    </asp:PlaceHolder>
                                                </div>
                                                <div class="spacer"></div>

                                            </asp:View>
                                        </asp:MultiView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <!-- End Product stock manamgement -->
                        </li>
                        <li>
                            <!-- Product Tag -->
                            <div class="wrap-edit-product">
                                <div class="align-left">
                                    <i class="fa fa-tags"></i>
                                    <asp:Literal runat="server" Text="<%$Resources: TagsStr %>" />
                                </div>
                                <div class="align-right">
                                    <i class="fa fa-angle-right fa-2x"></i>
                                </div>
                                <div class="spacer"></div>
                            </div>
                            <div class="frame-border-option-inline padding-10 margin-bottom-10" style="display: none;">
                                <!-- Product tags -->
                                <div class="alert alert-info margin-bottom-10">
                                    <asp:Literal runat="server" Text="<%$Resources: TagsAlert %>" />

                                </div>

                                <netpay:TagsInput runat="server" ID="txtTags" Text='<%# String.Join(",", CurrentProduct.Tags) %>' CssClass="tags percent100 tags-height" />

                            </div>
                            <!-- End Product Tag -->
                        </li>
                        <li>
                            <!-- Product catagory -->
                            <div class="wrap-edit-product">
                                <div class="align-left">
                                    <i class="fa fa-folder"></i>
                                    <asp:Literal runat="server" Text="<%$Resources: CatagoryStr %>" />
                                </div>
                                <div class="align-right">
                                    <i class="fa fa-angle-right fa-2x"></i>
                                </div>
                                <div class="spacer"></div>
                            </div>
                            <div class="frame-border-option-inline padding-10 margin-bottom-10" style="display: none;">
                                <div class="alert alert-info margin-bottom-10">
                                    <asp:Literal runat="server" Text="<%$Resources: CatagoryAlert %>" />
                                </div>
                                <div class="form-group">
                                    <div class="margin-bottom-10">
                                        <asp:DropDownList ID="dlCatagory" runat="server" CssClass="percent23" DataValueField="ID" DataTextField="Name" AutoPostBack="true" OnSelectedIndexChanged="Catagory_SelectedIndexChanged" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="form-group-title">
                                        <asp:Literal runat="server" Text="<%$Resources: SubCatagoryStr %>" /></span>&nbsp;<span class="form-group-title-eg"><asp:Literal runat="server" Text="<%$Resources: EGSubCatagoryStr %>" />
                                        </span>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Block" ChildrenAsTriggers="false" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:CheckBoxList runat="server" ID="cblSubCategory" ClientIDMode="Static" RepeatDirection="Horizontal" RepeatColumns="8" RepeatLayout="Table" DataValueField="ID" DataTextField="Name" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="dlCatagory" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>

                            </div>
                            <!-- Product catagory -->
                        </li>
                    </ul>
                    <asp:ValidationSummary ShowSummary="true" HeaderText="<%$Resources: SummaryValidation %>" CssClass="error" ValidationGroup="EditForm" runat="server" DisplayMode="BulletList" />
                    <div class="align-right">
                        <asp:LinkButton runat="server" ValidationGroup="EditForm" ClientIDMode="Static" ID="btnSave" CssClass="btn btn-success btn-cons-short" OnClick="Save_Click" OnClientClick="$('#imgUploadProgress').css('display', '');"><i class="fa fa-floppy-o"></i> <asp:Literal runat="server" Text="<%$Resources: SaveStr %>" /></asp:LinkButton></div><div class="spacer"></div>
                </div>
            </div>
        </div>

    </div>
</asp:Content>
