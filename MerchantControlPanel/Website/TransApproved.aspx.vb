﻿Imports System.Collections.Generic
Imports Netpay.Web.Controls
Imports Netpay.Bll.Reports
Imports Netpay.Web
Imports Netpay.Bll
Imports Netpay.Infrastructure

Partial Class Website_TransApproved
	Inherits MasteredPage

	Protected _isCaptured As Nullable(Of Boolean) = Nothing

	Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
		MyBase.OnLoad(e)
		If (Not IsPostBack) And repeaterResults.DataSource Is Nothing Then
			If Request("TransID") IsNot Nothing Then
				txtTransactionNumber.Text = Request("TransID").ToInt32(0)
				btnShowTransaction_Click(me, e)
			ElseIf Not String.IsNullOrEmpty(Request("FromDate")) Then
				wcDateRangePicker.FromDate = System.DateTime.Parse(Request("FromDate"))
				wcDateRangePicker.ToDate = DateTime.Now
				btnSearch_Click(Me, e)
			Else If Not String.IsNullOrEmpty(Request("deviceId")) Then 
				hdDeviceId.Value = Request("deviceId")
			End If
		End If
	End Sub

	Protected Function IsCaptured() As Boolean
		If _isCaptured Is Nothing Then _isCaptured = IIf(rblSearchType.SelectedValue = "1", False, True)
		Return _isCaptured.Value
	End Function

	Sub SetRowView(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
		If IsCaptured() Then
			CType(e.Item.FindControl("ucTransactionView"), TransactionRowView).ShowStatus = True
		Else
			Dim trView As TransactionRowView = CType(e.Item.FindControl("ucTransactionView"), TransactionRowView)
			trView.ShowStatus = False
			trView.ShowApprovalNumber = True
			trView.ShowTransactionFee = True
			trView.ShowPassedTransactionID = True
		End If
	End Sub

	Protected Sub SetHeaders()
		litDateTitle.Text = GetGlobalResourceObject("MultiLang", IIf(rblSearchType.SelectedIndex = 2, "ChargebackDate", "Date")).ToString()
		If IsCaptured() Then
			lblTableHeadings.Text = "<th style=""text-align:" & WebUtils.CurrentAlignment & ";"">" & GetGlobalResourceObject("MultiLang", "PayoutStatus") & "</th>"
		Else
			lblTableHeadings.Text = "<th style=""text-align:" & WebUtils.CurrentAlignment & ";"">" & GetGlobalResourceObject("MultiLang", "Confirmation") & "</th>"
			lblTableHeadings.Text = lblTableHeadings.Text & "<th style=""text-align:" & WebUtils.CurrentAlignment & ";"">" & GetGlobalResourceObject("MultiLang", "Fee") & "</th>"
			lblTableHeadings.Text = lblTableHeadings.Text & "<th style=""text-align:" & WebUtils.CurrentAlignment & ";"">" & GetGlobalResourceObject("MultiLang", "Capture") & "</th>"
		End If
	End Sub

 	Protected Sub OnPageChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim filters = New Bll.Transactions.Transaction.SearchFilters()
		filters.paymentMethodID = ddPaymentMethod.SelectedValue.ToNullableInt32()
		filters.date.From = wcDateRangePicker.FromDate
		filters.date.To = wcDateRangePicker.ToDate
		If ddCreditType.SelectedValue.ToNullableInt32() IsNot Nothing Then filters.creditTypes = New List(Of Integer)() From { ddCreditType.SelectedValue.ToNullableInt32() }
		filters.orderID = txtOrderNumber.Text.NullIfEmpty()
		filters.currencyID = ddCurrency.SelectedValue.ToNullableInt32()
		filters.paymentsStatus = ddPaymentsStatus.SelectedValue.NullIfEmpty
		filters.deviceId = hdDeviceId.Value.ToNullableInt32()
        filters.PayerFilters = New Transactions.Payer.SearchFilters()
		filters.PayerFilters.FirstName = txtPayerFirstName.Text.NullIfEmpty()
		filters.PayerFilters.LastName = txtPayerLastName.Text.NullIfEmpty()
		filters.PayerFilters.PersonalNumber = txtPayerId.Text.NullIfEmpty()
		filters.PayerFilters.EmailAddress = txtCardHolderEmail.Text.NullIfEmpty()
        filters.PaymentFilters = New Transactions.Payment.SearchFilters()
		filters.PaymentFilters.Last4Digits = txtLast4Digits.Text.NullIfEmpty()

		SetHeaders()

		Dim transStatus As TransactionStatus = IIf(IsCaptured, TransactionStatus.Captured, TransactionStatus.Authorized)
		filters.isChargeback = IIf(rblSearchType.SelectedValue = "2", True, False)
		If TypeOf e Is ExcelButton.ExportEventArgs Then
			wcFiltersView.LoadFilters(filters)
            btnExportExcel.SetExportInfo(wcFiltersView.RenderText, ReportType.MerchantCapturedTransactions, Function(pi) Bll.Transactions.Transaction.Search(transStatus, filters, pi, True, False))
        Else
            repeaterResults.DataSource = Bll.Transactions.Transaction.Search(transStatus, filters, pager.Info, False, False)
			repeaterResults.DataBind()
			btnExportExcel.ResultCount = Pager.Info.RowCount
		End If
		phResults.Visible = repeaterResults.Items.Count > 0
	End Sub

	Protected Sub btnShowTransaction_Click(ByVal sender As Object, ByVal e As EventArgs)
		hdLastSearchMode.Value = "show"

		Dim testInt As Integer
		If Not IsNumeric(txtTransactionNumber.Text) Then
            ltError.Text = GetGlobalResourceObject("TransApproved.aspx", "IllegalTransNum")
			ltError.Visible = True
			Exit Sub
		End If
		If Not Integer.TryParse(txtTransactionNumber.Text, testInt) Then
            ltError.Text = GetGlobalResourceObject("TransApproved.aspx", "IllegalTransNum")
			ltError.Visible = True
			Exit Sub
		End If

		SetHeaders()
		Dim transStatus As TransactionStatus = IIf(IsCaptured, TransactionStatus.Captured, TransactionStatus.Authorized)
        Dim filters = New Bll.Transactions.Transaction.SearchFilters()
		filters.transactionID = txtTransactionNumber.Text.ToNullableInt32()
		If TypeOf e Is ExcelButton.ExportEventArgs Then
			wcFiltersView.LoadFilters(filters)
            btnExportExcel.SetExportInfo(wcFiltersView.RenderText, ReportType.MerchantCapturedTransactions, Function(pi) Bll.Transactions.Transaction.Search(transStatus, filters, pi, True, False))
        Else
            repeaterResults.DataSource = Bll.Transactions.Transaction.Search(transStatus, filters, Nothing, False, False)
			repeaterResults.DataBind()
			btnExportExcel.ResultCount = IIf(repeaterResults.Items.Count > 0, 1, 0)
		End If
		Pager.Visible = False
		phResults.Visible = repeaterResults.Items.Count > 0
	End Sub

	Protected Sub ExportExcel_Click(ByVal sender As Object, ByVal e As ExcelButton.ExportEventArgs)
		If (hdLastSearchMode.Value = "show") Then btnShowTransaction_Click(Me, e) Else OnPageChanged(sender, e)
		btnExportExcel.Export()
	End Sub

	Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
		Pager.Info.PageCurrent = 0
		hdLastSearchMode.Value = "search"
		OnPageChanged(sender, e)
		Pager.Visible = True
	End Sub

End Class
