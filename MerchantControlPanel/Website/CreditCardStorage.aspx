﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" EnableEventValidation="false"
    AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_CreditCardStorage"
    CodeBehind="CreditCardStorage.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="Localize2" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <script type="text/javascript">
        function checkAllCancel() {
            if (aspnetForm.CcStorageID) {
                if (aspnetForm.CcStorageID.length) {
                    for (i = 0; i < aspnetForm.CcStorageID.length; i++)
                        if (aspnetForm.CcStorageID[i].checked == true) {
                            return true;
                        }
                }
                else {
                    if (aspnetForm.CcStorageID.checked == true) {
                        return true;
                    }
                }
            }
            alert('Please select cards!');
            return false;
        }
        function ExpandNode(fTrNum) {
            trObj = document.getElementById("trInfo" + fTrNum)
            imgObj = document.getElementById("oListImg" + fTrNum)
            if (trObj) {
                if (trObj.style.display == '') {
                    imgObj.src = '<%= MapTemplateVirPath("Images/plus.png")%>';
				    trObj.style.display = 'none';
				}
				else {
                    imgObj.src = '<%= MapTemplateVirPath("Images/minus.png")%>';
				    trObj.style.display = '';
				}
            }
        }
    </script>
    <div class="area-content" id="divForm">
        <netpay:ElementBlocker ID="wcElementBlocker" ElementClientID="divForm" Text="<%$ Resources:MultiLang,FeatureDisabled%>" runat="server" />
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">
          
            <div class="section">
                <div class="button-border" >
                    <asp:LinkButton ID="BtnAdd" CssClass="btn btn-success btn-cons-short"
                    TabIndex="18" OnClick="btnAdd_Click" runat="server" ><i class="fa fa-plus-circle"></i> <asp:Literal runat="server" Text="<%$Resources: AddNewCard  %>" />  </asp:LinkButton>
                    </div>

                <div class="align-left percent33">
                    <div>
                        <div>
                            <asp:Literal runat="server" Text="<%$Resources:MultiLang,InsertDates  %>" />
                        </div>
                        <div>
                            <netpay:DateRangePicker Visible="true" ID="wcDateRangePicker" HtmlLayout="Flow" Layout="Horizontal" runat="server" />
                        </div>
                    </div>
                    <div class="margin-top-10">
                        <div>
                            <asp:Literal runat="server" Text="<%$Resources:MultiLang,CCHolderName  %>" />
                        </div>
                        <div>
                            <asp:TextBox ID="txtFindCardFullName" placeholder="<%$Resources:MultiLang,CCHolderName  %>" runat="server" CssClass="percent85" />
                        </div>

                    </div>
                </div>
                <div class="align-left percent33">
                    <div>
                        <div>
                            <asp:Literal runat="server" Text="<%$Resources:MultiLang,CCType  %>" />
                        </div>
                        <div>
                            <netpay:PaymentMethodDropDown ID="ddlCardType" SinglePaymentMethodType="CreditCard" BlankSelectionText="<%$Resources:MultiLang,CCType  %>" runat="server" Class="percent85" />
                        </div>
                    </div>

                    <div class="margin-top-10">
                        <div>
                            <asp:Literal runat="server" Text="<%$Resources:MultiLang,CCValidity  %>" />
                        </div>
                        <div>
                            <netpay:MonthDropDown ID="ddlExpMonth" CssClass="Field_100" ShowNames="False" runat="server" BlankSelectionText="<%$Resources:MultiLang,Month  %>" />
                            <netpay:YearDropDown ID="ddlExpYear" CssClass="Field_100" runat="server" BlankSelectionText="<%$Resources:MultiLang,Year %>" />
                        </div>
                    </div>
                </div>
                <div class="align-left percent33">
                    <div>
                        <div>
                            <asp:Literal runat="server" Text="<%$Resources:MultiLang,Last4Digits  %>" />
                        </div>
                        <div>
                            <asp:TextBox ID="txtFindLastDigit" placeholder="<%$Resources:MultiLang,Last4Digits  %>" runat="server" Class="percent85" />
                        </div>
                    </div>
                    <div class="margin-top-10">

                        <div>
                            <div>
                                <asp:Literal runat="server" Text="<%$Resources:MultiLang,Activecards  %>" />
                            </div>
                        </div>
                        <div>
                            <asp:DropDownList ID="isCcDel" runat="server" Class="percent85">
                                <asp:ListItem Text="<%$Resources:MultiLang,Activecards %>" Value="0" Selected="True" />
                                <asp:ListItem Text="<%$Resources:MultiLang,Cancelledcards %>" Value="1" />
                            </asp:DropDownList>
                        </div>
                    </div>

                </div>
                <div class="spacer"></div>

                <div class="wrap-button-bar">
                    <asp:LinkButton ID="BtnSearch" runat="server" CssClass="btn btn-cons-short btn-inverse" OnClick="btnSearch_Click"><i class="fa fa-search"></i> <asp:Literal runat="server" Text="<%$Resources:MultiLang,Search  %>" /></asp:LinkButton>
                </div>


            </div>
            <asp:PlaceHolder runat="server" ID="phResult" Visible="false">
                <div class="section">
                    <table class="exspand-table">
                        <tr>
                            <th>&nbsp;
                            </th>
                            <th>ID
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:MultiLang,CCNumber %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:MultiLang,CCValidity %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:MultiLang,CCHolderName %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:MultiLang,Fee %>" runat="server" />
                            </th>
                            <th>&nbsp;
                            </th>
                            <th>&nbsp;
                            </th>
                        </tr>
                        <asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server">
                            <ItemTemplate>
                                <tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
                                    <td>
                                        <img onclick="ExpandNode('<%# CType(Container.DataItem, Merchants.MethodStorage).ID %>');" class="ExpandButtonCCstorage" id="oListImg<%# CType(Container.DataItem, Merchants.MethodStorage).ID %>" src="<%= MapTemplateVirPath("Images/plus.png")%>" alt=""  /><br />
                                    </td>
                                    <td><a href="VirtualTerminalCC.aspx?ccStorageId=<%# CType(Container.DataItem, Merchants.MethodStorage).ID %>"><%# CType(Container.DataItem, Merchants.MethodStorage).ID %></a></td>
                                    <td>
                                        <netpay:PaymentMethodView ID="PaymentMethodView1" StoredData="<%# CType(Container.DataItem, Merchants.MethodStorage) %>" runat="server" />
                                    </td>
                                    <td><%# CType(Container.DataItem, Merchants.MethodStorage).MethodInstance.ExpirationMonth %> / <%# CType(Container.DataItem, Merchants.MethodStorage).MethodInstance.ExpirationYear %></td>
                                    <td><%# CType(Container.DataItem, Merchants.MethodStorage).Owner_FullName %></td>
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Text='<%# FormatPaid(CType(Container.DataItem, Merchants.MethodStorage))%>' />
                                    </td>
                                    <td class="text-align-center">
                                        <a href="CreditCardStorageUpdate.aspx?cardID=<%# CType(Container.DataItem, Merchants.MethodStorage).ID %>" class="btn btn-danger btn-small"><i class="fa fa-fw fa-edit white-color"></i></a>
                                        <a href="VirtualTerminalCC.aspx?ccStorageId=<%# CType(Container.DataItem, Merchants.MethodStorage).ID %>" class="btn btn-success btn-small"><i class="fa fa-fw fa-credit-card-alt white-color"></i></a>
                                    </td>
                                    <td class="text-align-center">
                                        <input type="checkbox" name="CcStorageID" value="<%# CType(Container.DataItem, Merchants.MethodStorage).ID %>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="9" id="trInfo<%# CType(Container.DataItem, Merchants.MethodStorage).ID %>"
                                        style="display: none; background-color: #efefef;">
                                        <div>
                                            <table id="Credit-detalis">
                                                <tr>
                                                    <td>
                                                        <h3>
                                                            <asp:Literal runat="server" Text="<%$Resources:subHeadStorageDetails %>" />
                                                        </h3>
                                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang,InsertDates %>" />:
                                                        <%# CType(Container.DataItem, Merchants.MethodStorage).InsertDate %><br />
                                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang,Comment %>" />:
                                                        <%# CType(Container.DataItem, Merchants.MethodStorage).Comment%><br />
                                                    </td>
                                                    <td>
                                                        <h3><asp:Literal runat="server" Text="<%$Resources:MultiLang,CreditCardDetails %>" /></h3>
                                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang,CCHolderName %>" />:
                                                        <%# CType(Container.DataItem, Merchants.MethodStorage).Owner_FullName %><br />
                                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang,CCHolderMail %>" />:
                                                        <%# CType(Container.DataItem, Merchants.MethodStorage).Owner_EmailAddress %><br />
                                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang,CCHolderPhone %>" />:
                                                        <%# CType(Container.DataItem, Merchants.MethodStorage).Owner_PhoneNumber %><br />
                                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang,CCHolderID %>" />:
                                                        <%# CType(Container.DataItem, Merchants.MethodStorage).Owner_PersonalNumber %><br />
                                                    </td>
                                                    <td>
                                                        <h3>
                                                            <asp:Literal runat="server" Text="<%$Resources:MultiLang,Billingaddress %>"/>
                                                        </h3>
                                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang,Address %>" />
                                                        1:</span>
                                                        <%# CType(Container.DataItem, Merchants.MethodStorage).BA_Street1 %><br />
                                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang,Address %>" />
                                                        2:
                                                        <%# CType(Container.DataItem, Merchants.MethodStorage).BA_Street2 %><br />
                                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang,InsertDates %>" />:
                                                        <%# CType(Container.DataItem, Merchants.MethodStorage).BA_City %><br />
                                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang,ZipCode %>" />:
                                                        <%# CType(Container.DataItem, Merchants.MethodStorage).BA_PostalCode %><br />
                                                        <asp:Literal runat="server" Text="<%$Resources:MultiLang,State %>" />:
                                                        <%# WebUtils.GetStateName(CType(Container.DataItem, Merchants.MethodStorage).BA_StateISOCode)%><br />
                                                        <asp:Literal ID="Literal26" runat="server" Text="<%$Resources:MultiLang,Country %>" />:
                                                        <%# WebUtils.GetCountryName(CType(Container.DataItem, Merchants.MethodStorage).BA_CountryISOCode) %><br />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <tr>
                            <td colspan="9" style="border: 0px; padding-top: 10px;" align="<%= WebUtils.CurrentReverseAlignment %>">
                            <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-danger" OnClick="OnClick_BtnCancel" OnClientClick="return checkAllCancel()"></asp:LinkButton>
                        </tr>
                    </table>
                    <netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
                </div>
            </asp:PlaceHolder>
        </div>
    </div>
</asp:Content>
