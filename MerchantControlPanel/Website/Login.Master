﻿<%@ Master Language="VB" AutoEventWireup="false" CodeBehind="Login.master.vb" Inherits="Netpay.MerchantControlPanel.MasterLogin" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><%= WebUtils.CurrentDomain.BrandName %> - Merchant Control Panel</title>
    <link href="Plugins/font-awesome-4.5.0/css/font-awesome.css" rel="stylesheet" />
    <script src="Plugins/jquery-ui-1.8.10.custom/js/jquery-1.4.4.min.js"></script>
    <script src="Plugins/jquery-ui-1.8.10.custom/js/jquery-ui-1.8.10.custom.min.js"></script>
    <link id="lnkIcon" runat="server" rel="icon" type="image/x-icon" />
    <netpay:ScriptManager ID="ScriptManager1" runat="server" EnableSessionAlerts="true" />
    <meta name="robots" content="noindex, nofollow" />
</head>
<body>
    <!-- Facebook SDK -->
    <div id="fb-root"></div>
    <script>(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=1428278404052835";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <!-- End Facebook SDK -->
    <script type="text/javascript">
        var currentShownElementId = null;
        $(document).ready(function () {
            currentShownElementId = 'divNotificationsContent';
            var culture = '<%=System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower()%>';
            $("#selectLanguage > option").each(function () {
                this.selected = false;
                if (this.value == culture)
                    this.selected = true;
            });
        });

        function changeLanguage() {
            var element = document.getElementById("selectLanguage");
            var culture = element.options[element.selectedIndex].value;
            window.location = netpay.Common.setURLValue(window.location.href, 'culture', culture)
        }

        function showContent(elemntId) {
            $('#' + currentShownElementId).fadeOut("slow", function () {
                $("#" + elemntId).fadeIn("slow");
                currentShownElementId = elemntId;
            });
        }
    </script>

    <form runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
        <!-- HEADER -->
        <header class="header">
            <div class="container">
                <div class="pull-left">
                    <img src="<%= WebUtils.MapTemplateVirPath(Me, "images/login/logo.png")%>" />
                </div>
                <div class="pull-right">
                    <select id="selectLanguage" onchange="changeLanguage()" class="form-control">
                        <option value="en-us">English</option>
                        <option value="he-il">עברית</option>
                    </select>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </header>

        <!-- BOX INFO -->
        <article class="wrap-box-info">
            <section class="container">
                <asp:ContentPlaceHolder ID="BoxInfoPlaceholder" runat="server">
                </asp:ContentPlaceHolder>

            </section>
        </article>

        <!-- CONTENT AREA -->
        <article class="wrap-content-area">
            <section class="container">
                <asp:ContentPlaceHolder ID="ContentPlaceHolder" runat="server">
                </asp:ContentPlaceHolder>
            </section>
        </article>

        <!-- FOOTER -->
        <footer id="footer" runat="server">
            <article>
                <section class="container footer">
                    <ul class="footer-list">
                        <li>
                            <h4><i class="fa fa-info-circle"></i>
                                <asp:Localize Text="<%$ Resources:Login.aspx, AboutIdentity%>" runat="server" />
                                <%= WebUtils.CurrentDomain.BrandName %> </h4>
                            <p> 

                                <%= WebUtils.CurrentDomain.BrandName %>
                                <asp:Localize Text="<%$ Resources:Login.aspx, IdentityDesc%>" runat="server" />
                            </p>
                        </li>

                        <li>
                            <h4><i class="fa fa-users"></i>
                                <asp:Localize Text="<%$ Resources:Login.aspx, CustomerService%>" runat="server" /></h4>
                            <p>
                                <asp:PlaceHolder Visible='<%# Not String.IsNullOrEmpty(WebUtils.CurrentDomain.CustomerServiceSkype) %>' runat="server">
                                    <a href="skype:<%# WebUtils.CurrentDomain.CustomerServiceSkype %>?chat"><i class="fa fa-skype" style="color: #2CC7F5;"></i>
                                    <asp:Localize Text="<%$ Resources:Login.aspx, Skype %>" runat="server" /></a><br />
                                </asp:PlaceHolder>
                                 <asp:PlaceHolder Visible='<%# Not String.IsNullOrEmpty(WebUtils.CurrentDomain.CustomerServicePhone) %>' runat="server">
                                    <asp:Localize Text="<%$Resources:MultiLang, Phone%>" runat="server" />:
                                    <span class="orange-color"><%# WebUtils.CurrentDomain.CustomerServicePhone %></span><br />
                                </asp:PlaceHolder>
                                <asp:PlaceHolder Visible='<%# Not String.IsNullOrEmpty(WebUtils.CurrentDomain.CustomerServiceFax) %>' runat="server">
                                    <asp:Localize Text="<%$Resources:MultiLang, Fax%>" runat="server" />:
                                    <span class="orange-color"><%# WebUtils.CurrentDomain.CustomerServiceFax %></span><br />
                                </asp:PlaceHolder>
                                <asp:PlaceHolder Visible='<%# Not String.IsNullOrEmpty(WebUtils.CurrentDomain.CustomerServiceEmail) %>' runat="server">
                                    <asp:Localize Text="<%$Resources:MultiLang,Email%>" runat="server" />
                                    <a href="mailto:<%# WebUtils.CurrentDomain.CustomerServiceEmail %>"><%= WebUtils.CurrentDomain.CustomerServiceEmail %></a>
                                </asp:PlaceHolder>
                            </p>
                        </li>

                        <li>
                            <h4><i class="fa fa-cogs"></i>
                                <asp:Localize Text="<%$Resources:Login.aspx, TechnicalSupport %>" runat="server" />:</h4>
                            <p>
                                <asp:PlaceHolder Visible='<%# Not String.IsNullOrEmpty(WebUtils.CurrentDomain.TechnicalSupportSkype) %>' runat="server">
                                    <a href="skype:<%# WebUtils.CurrentDomain.TechnicalSupportSkype %>?chat"><i class="fa fa-skype" style="color: #2CC7F5;"></i>
                                    <asp:Localize Text="<%$ Resources:Login.aspx, Skype %>" runat="server" /></a><br />
                                </asp:PlaceHolder>
                                <asp:PlaceHolder Visible='<%# Not String.IsNullOrEmpty(WebUtils.CurrentDomain.TechnicalSupportPhone) %>' runat="server">
                                    <asp:Localize Text="<%$Resources:MultiLang, Phone%>" runat="server" />:
                                    <span class="orange-color"><%# WebUtils.CurrentDomain.TechnicalSupportPhone %></span><br />
                                </asp:PlaceHolder>
                                <asp:PlaceHolder Visible='<%# Not String.IsNullOrEmpty(WebUtils.CurrentDomain.TechnicalSupportFax) %>' runat="server">
                                    <asp:Localize Text="<%$Resources:MultiLang, Fax%>" runat="server" />:
                                    <span class="orange-color"><%# WebUtils.CurrentDomain.TechnicalSupportFax %></span><br />
                                </asp:PlaceHolder>
                                <asp:PlaceHolder Visible='<%# Not String.IsNullOrEmpty(WebUtils.CurrentDomain.TechnicalSupportEmail) %>' runat="server">
                                    <asp:Localize Text="<%$Resources:MultiLang,Email%>" runat="server" />
                                    <a href="mailto:<%# WebUtils.CurrentDomain.TechnicalSupportEmail %>"><%= WebUtils.CurrentDomain.TechnicalSupportEmail %></a>
                                </asp:PlaceHolder>
                            </p>
                        </li>
                        <li>
                            <h4><i class="fa fa-file-pdf-o"></i>
                                <asp:Localize Text="<%$ Resources:Login.aspx, AcrobatReader %>" runat="server" /></h4>
                            <p class="newsletter-panel">
                                <asp:Localize Text="<%$ Resources:Login.aspx, AcrobatReaderDesc %>" runat="server" />
                            </p>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </section>
            </article>
            <article class="copyright">
                <section class="container">
                    <div class="pull-left">
                        <p>
                            <asp:Localize ID="ltCopyright" runat="server" Text="<%$ Resources:Login.aspx, Copyright %>" />
                        </p>
                    </div>
                   <asp:PlaceHolder Visible='<%# Not String.IsNullOrEmpty(WebUtils.CurrentDomain.SocialMediaFacebook) %>' runat="server">
                        <div class="pull-left wrap-fb-like">
                            <div class="fb-like" data-href="<%# WebUtils.CurrentDomain.SocialMediaFacebook %>" data-width="400px" data-layout="standard" data-action="like" data-show-faces="false" data-share="false"></div>
                        </div>
                   </asp:PlaceHolder>
                    <div class="social-media-icons">
                        <asp:PlaceHolder Visible='<%# Not String.IsNullOrEmpty(WebUtils.CurrentDomain.SocialMediaFacebook) %>' runat="server">
                           <a href="<%# WebUtils.CurrentDomain.SocialMediaFacebook %>" target="_blank"><img src="images/Images.SocialMedia.32X32/facebook.32X32.png" /></a>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder Visible='<%# Not String.IsNullOrEmpty(WebUtils.CurrentDomain.SocialMediaGooglePlus) %>' runat="server">
                          <a href="<%# WebUtils.CurrentDomain.SocialMediaGooglePlus %>" target="_blank"><img src="images/Images.SocialMedia.32X32/googleplus.32X32.png" /></a>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder Visible='<%# Not String.IsNullOrEmpty(WebUtils.CurrentDomain.SocialMediaLinkedIn) %>' runat="server">
                           <a href="<%# WebUtils.CurrentDomain.SocialMediaLinkedIn %>" target="_blank"><img src="images/Images.SocialMedia.32X32/linkedin.32X32.png" /></a>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder Visible='<%# Not String.IsNullOrEmpty(WebUtils.CurrentDomain.SocialMediaTwitter) %>' runat="server">
                            <a href="<%# WebUtils.CurrentDomain.SocialMediaTwitter %>" target="_blank"><img src="images/Images.SocialMedia.32X32/twitter.32X32.png" /></a>
                        </asp:PlaceHolder>
                    </div>
                    <div class="clearfix">
                    </div>
                </section>
            </article>
        </footer>
    </form>
</body>
</html>

