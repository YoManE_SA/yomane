﻿Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports Netpay.Web.Controls
Imports System.Linq
Imports Netpay.Bll
Imports Netpay.Web
Imports Netpay.Infrastructure

Partial Class Website_LimitedUserManagement
	Inherits MasteredPage
	Dim _accessiblePages As List(Of Security.SecuredObject)

	Protected Overrides Sub OnInit(e As System.EventArgs)
        Dim xmlDoc As New System.Xml.XmlDocument
        xmlDoc.Load(Server.MapPath(MapTemplateVirPath("Web.sitemap")))

        For Each n As System.Xml.XmlNode In xmlDoc.DocumentElement.ChildNodes(0).ChildNodes
            AddXmlLevel(n, phSiteMapAccess, 0)
        Next
		MyBase.OnInit(e)
	End Sub

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		pihPassword.Text = GetGlobalResourceObject("MultiLang", "SignupPasswordText") & ":<ul style=""list-style:circle;margin-top:0;"">" & _
			"<li>" & GetGlobalResourceObject("MultiLang", "SignupPasswordText1") & "</li><li>" & GetGlobalResourceObject("MultiLang", "SignupPasswordText2") & "</li>" & _
			"<li>" & GetGlobalResourceObject("MultiLang", "SignupPasswordText3") & "</li><li>" & GetGlobalResourceObject("MultiLang", "SignupPasswordText4") & "</li>" & _
			"<li>" & GetGlobalResourceObject("MultiLang", "SignupPasswordText5") & "</li></ul>"
		lblResult.Text = String.Empty
		If Not IsPostBack Then
			phResultMsg.Visible = True
			LoadUsers()
		End If
	End Sub

	Protected Sub SetLevel(parent As System.Web.UI.Control, values As Dictionary(Of Integer, Infrastructure.Security.PermissionValue?), bSave As Boolean)
		For Each c In parent.Controls
			If (TypeOf c Is System.Web.UI.WebControls.CheckBox) Then
				Dim chk as System.Web.UI.WebControls.CheckBox = CType(c, System.Web.UI.WebControls.CheckBox)
				If bSave Then 
					values.Add(SecurityModule.GetSecuredObject(c.Attributes("value"), True).ID, IIf(chk.Checked, Infrastructure.Security.PermissionValue.Execute, Nothing))
				Else
		            chk.Checked = SecurityModule.GetPermissionValue(values, c.Attributes("value")) <> Security.PermissionValue.None
				End If
			Else 
				SetLevel(c, values, bSave)
			End If
		Next
	End Sub

    Protected Sub AddXmlLevel(node As System.Xml.XmlNode, parent As System.Web.UI.Control, level As Integer)
        If level = 0 Then
            For Each n As System.Xml.XmlNode In node.ChildNodes
                Dim div As New System.Web.UI.WebControls.WebControl(HtmlTextWriterTag.Div)
                Dim secTitle = New System.Web.UI.HtmlControls.HtmlGenericControl("H3")
                secTitle.InnerText = GetGlobalResourceObject("Menu", n.Attributes("title").Value)
                Dim ul = New System.Web.UI.HtmlControls.HtmlGenericControl("UL")
                ul.Attributes.Add("class", "group-list")
                div.Controls.Add(secTitle)
                div.Controls.Add(ul)
                parent.Controls.Add(div)
                AddXmlLevel(n, ul, level + 1)
            Next
            Return
        End If
        For Each n As System.Xml.XmlNode In node.ChildNodes
            Dim itemUrl As String = System.IO.Path.GetFileName(n.Attributes("url").Value).ToLower()
            Dim chk As New System.Web.UI.WebControls.CheckBox()
            chk.Text = GetGlobalResourceObject("Menu", n.Attributes("title").Value)
            chk.Attributes("value") = itemUrl
            Dim li = New System.Web.UI.HtmlControls.HtmlGenericControl("LI")
            li.Attributes.Add("class", "fa fa-cog")
            li.Controls.Add(chk)
            parent.Controls.Add(li)
        Next
    End Sub

	Protected Sub LoadUsers(Optional ByVal sUsername As String = Nothing)
		' load users
		Dim users = Bll.Accounts.LimitedLogin.Search(Account.AccountID)
		ddUsers.Visible = users.Count > 0
		ddUsers.DataSource = users
		ddUsers.DataValueField = "ID"
		ddUsers.DataTextField = "UserName"
		ddUsers.DataBind()
        ddUsers.Items.Insert(0, New ListItem("", ""))

		ddlDeleteSelectUser.DataSource = users
		ddlDeleteSelectUser.DataValueField = "ID"
		ddlDeleteSelectUser.DataTextField = "UserName"
		ddlDeleteSelectUser.DataBind()
		ddlDeleteSelectUser.Items.Insert(0, New ListItem("", ""))
		
		If users.Count = 1 Then
			ddUsers.SelectedIndex = 1
			UserSelected()
		ElseIf users.Count > 1 And Not String.IsNullOrEmpty(sUsername) Then
			For Each liUser As ListItem In ddUsers.Items
				If liUser.Text = sUsername Then
					liUser.Selected = True
					UserSelected()
					Exit For
				End If
			Next
		End If
	End Sub

	Protected Sub Save_Click(ByVal sender As Object, ByVal e As EventArgs) 
		Dim limitedUserID As Integer = Integer.Parse(ddUsers.SelectedValue)
		Dim user = Bll.Accounts.LimitedLogin.Load(limitedUserID)
		Dim vales = New Dictionary(Of Integer, Infrastructure.Security.PermissionValue?)
		SetLevel(phSiteMapAccess, vales, True)
		
        vales.Add(SecurityModule.GetSecuredObject(PermissionObjects.TransactionPrintSlip.ToString()).ID, IIf(cbPrintSlipPermission.Checked, Infrastructure.Security.PermissionValue.Execute, Nothing))
		vales.Add(SecurityModule.GetSecuredObject(PermissionObjects.RefundRequest.ToString()).ID, IIf(cbRefundRequest.Checked, Infrastructure.Security.PermissionValue.Execute, Nothing))
		vales.Add(SecurityModule.GetSecuredObject(PermissionObjects.CreateRecurringSeries.ToString()).ID, IIf(cbCreateRecurring.Checked, Infrastructure.Security.PermissionValue.Execute, Nothing))
		vales.Add(SecurityModule.GetSecuredObject(PermissionObjects.AddTransactionCardToBacklist.ToString()).ID, IIf(cbBlockCard.Checked, Infrastructure.Security.PermissionValue.Execute, Nothing))
		vales.Add(SecurityModule.GetSecuredObject(PermissionObjects.ShowTotals.ToString()).ID, IIf(cbShowTotals.Checked, Infrastructure.Security.PermissionValue.Execute, Nothing))
		vales.Add(SecurityModule.GetSecuredObject(PermissionObjects.ShowMainPageNotifications.ToString()).ID, IIf(cbShowMainNotifications.Checked, Infrastructure.Security.PermissionValue.Execute, Nothing))


		vales.Add(SecurityModule.GetSecuredObject("billingShow_actions.aspx").ID, IIf(cdAllowInvoices.Checked, Infrastructure.Security.PermissionValue.Execute, Nothing))
		vales.Add(SecurityModule.GetSecuredObject("billingShow_frameset.aspx").ID, IIf(cdAllowInvoices.Checked, Infrastructure.Security.PermissionValue.Execute, Nothing))
		vales.Add(SecurityModule.GetSecuredObject("billingShow_menu.aspx").ID, IIf(cdAllowInvoices.Checked, Infrastructure.Security.PermissionValue.Execute, Nothing))
		vales.Add(SecurityModule.GetSecuredObject("billingShow_pages.aspx").ID, IIf(cdAllowInvoices.Checked, Infrastructure.Security.PermissionValue.Execute, Nothing))
		Infrastructure.Security.Permission.SetToLogin(user.LoginID, vales)
	End Sub

    Protected Sub LoadUser(ByVal userID As Integer)
		Dim user = Bll.Accounts.LimitedLogin.Load(userID)
		Dim values = Infrastructure.Security.Permission.LoadForLogin(user.LoginID).ToDictionary(Function(x) x.ObjectID, Function(x) CType(x.Value, Infrastructure.Security.PermissionValue?))
		SetLevel(phSiteMapAccess, values, False)

        ' load additional features
		cbPrintSlipPermission.Checked = SecurityModule.GetPermissionValue(values, PermissionObjects.TransactionPrintSlip.ToString()) <> Security.PermissionValue.None
		cbRefundRequest.Checked = SecurityModule.GetPermissionValue(values, PermissionObjects.RefundRequest.ToString())<> Security.PermissionValue.None
		cbCreateRecurring.Checked = SecurityModule.GetPermissionValue(values, PermissionObjects.CreateRecurringSeries.ToString()) <> Security.PermissionValue.None
		cbBlockCard.Checked = SecurityModule.GetPermissionValue(values, PermissionObjects.AddTransactionCardToBacklist.ToString()) <> Security.PermissionValue.None
		cbShowTotals.Checked = SecurityModule.GetPermissionValue(values, PermissionObjects.ShowTotals.ToString())<> Security.PermissionValue.None
		'cbAllowSwitch.Checked = GetPermissionValue(values, PermissionObjects.SwitchLinkedMerchants.ToString()) <> Security.PermissionValue.None
		cbShowMainNotifications.Checked = SecurityModule.GetPermissionValue(values, PermissionObjects.ShowMainPageNotifications.ToString()) <> Security.PermissionValue.None
		cdAllowInvoices.Checked = SecurityModule.GetPermissionValue(values, "billingShow_actions.aspx") <> Security.PermissionValue.None
        locChangePassword.Text = GetLocalResourceObject("SubHeader1").ToString.Replace("%USER%", ddUsers.SelectedItem.Text)
    End Sub

	Protected Sub UserSelected(Optional ByVal sender As Object = Nothing, Optional ByVal e As EventArgs = Nothing) Handles ddUsers.SelectedIndexChanged
		If ddUsers.SelectedValue = "" Then
			phUserDetails.Visible = False
			Return
		Else
			phUserDetails.Visible = True
		End If

		Dim limitedUserID As Integer = Integer.Parse(ddUsers.SelectedValue)
		LoadUser(limitedUserID)
	End Sub

	Protected Sub ChangePassword(ByVal sender As Object, ByVal e As EventArgs)
		If Validation.IsEmptyField(txtChangePassword, lblResult, "Password", True) Then Exit Sub
		If Validation.IsEmptyField(txtChangePasswordConfirm, lblResult, "ValidatePassword", True) Then Exit Sub
		If Validation.IsInvalidPassword(txtChangePassword, lblResult) Then Exit Sub
		If Validation.IsMismatchFields(txtChangePassword, txtChangePasswordConfirm, lblResult, GetGlobalResourceObject("MultiLang", "Password"), GetGlobalResourceObject("MultiLang", "ValidatePassword")) Then Exit Sub
		Dim result As Security.Login.ChangePasswordResult
		If txtChangePassword.Text <> txtChangePasswordConfirm.Text Then result = Security.Login.ChangePasswordResult.PasswordConfirmationMissmatch _ 
		Else result = Accounts.LimitedLogin.Load(ddUsers.SelectedValue).SetPassword(txtChangePassword.Text, Request.UserHostAddress)
		Select Case result
			Case Security.Login.ChangePasswordResult.Success
				lblResult.Text = "<div class='noerror'>" + GetLocalResourceObject("passwordChanged").ToString() + "</div>"
			Case Security.Login.ChangePasswordResult.PasswordAlreadyExists
				lblResult.Text = "<div class='error'>" + GetLocalResourceObject("chooseAnotherPassword").ToString() + "</div>"
			Case Security.Login.ChangePasswordResult.PasswordConfirmationMissmatch
				lblResult.Text = "<div class='error'>" + GetLocalResourceObject("passwordConfirmMissmatch").ToString() + "</div>"
			Case Security.Login.ChangePasswordResult.PasswordUsedInThePast
				lblResult.Text = "<div class='error'>" + GetLocalResourceObject("passwordPreviouslyUsed").ToString() + "</div>"
			Case Security.Login.ChangePasswordResult.WeakPassword
				lblResult.Text = "<div class='error'>" + GetLocalResourceObject("weakPassword").ToString() + "</div>"
		End Select
	End Sub

	Protected Sub CreateUser(ByVal sender As Object, ByVal e As EventArgs)
		If Validation.IsEmptyField(txtNewUserName, lblResult, "UserName", True) Then Exit Sub
		If Validation.IsEmptyField(txtNewPassword, lblResult, "Password", True) Then Exit Sub
		If Validation.IsEmptyField(txtNewPasswordConfirm, lblResult, "ValidatePassword", True) Then Exit Sub
		If Validation.IsInvalidPassword(txtNewPassword, lblResult) Then Exit Sub
		If Validation.IsMismatchFields(txtNewPassword, txtNewPasswordConfirm, lblResult, GetGlobalResourceObject("MultiLang", "Password"), GetGlobalResourceObject("MultiLang", "ValidatePassword")) Then Exit Sub

		Dim lmuser = New Bll.Accounts.LimitedLogin(Account.AccountID, txtNewUserName.Text, txtNewPassword.Text)
		lmuser.EmailAddress = Merchant.Login.EmailAddress
		'lmuser.SetPassword(txtNewPassword.Text, Request.UserHostAddress)
		Select Case lmuser.Save()
			Case CreateLimitedUserResult.Success
				lblResult.Text = "<div class='noerror'>" + GetLocalResourceObject("userCreated").ToString() + "</div>"
			Case CreateLimitedUserResult.WeakPassword
				lblResult.Text = "<div class='error'>" + GetLocalResourceObject("weakPassword").ToString() + "</div>"
			Case CreateLimitedUserResult.InvalidUserName
				lblResult.Text = "<div class='error'>" + GetLocalResourceObject("invalidUserName").ToString() + "</div>"
			Case CreateLimitedUserResult.UserAlreadyExists
				lblResult.Text = "<div class='error'>" + GetLocalResourceObject("existingUser").ToString() + "</div>"
		End Select

		LoadUsers(txtNewUserName.Text)
		txtNewUserName.Text = ""
	End Sub

	Protected Sub DeleteUser(ByVal sender As Object, ByVal e As EventArgs) Handles btnDeleteUser.Click
		Dim limitedUserID As Integer = Integer.Parse(ddlDeleteSelectUser.SelectedValue)
		Bll.Accounts.LimitedLogin.Load(limitedUserID).Delete()
		phUserDetails.Visible = False
		LoadUsers()
	End Sub

	Protected Sub DeleteSelectUser_Change(ByVal sender As Object, ByVal e As EventArgs)
		Dim isSelected = (ddlDeleteSelectUser.SelectedItem IsNot Nothing)
		locDeleteWarning.Visible = isSelected
		btnDeleteUser.Visible = isSelected
        locDeleteWarning.Text = GetLocalResourceObject("DeleteWarning").ToString.Replace("%USER%", ddlDeleteSelectUser.SelectedItem.Text)
	End Sub
    
End Class
