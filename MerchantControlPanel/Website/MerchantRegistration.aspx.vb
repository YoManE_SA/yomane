﻿Imports Netpay.Infrastructure
Imports Netpay.Web
Imports Netpay.Bll
Imports Netpay.Bll.ThirdParty


Public Class MerchantRegistration
    Inherits Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'lnkIcon.Href = "MapTemplateVirPath("favicon.ico")../"
        lnkIcon.Href = "../favicon.ico"


        If Not IsPostBack Then
            'Dim industries As Dictionary(Of String, String) = CardConnect.GetIndustries()
            'ddIndustry.DataSource = industries
            'ddIndustry.DataValueField = "key"
            'ddIndustry.DataTextField = "value"
            'ddIndustry.DataBind()

            'Dim types As Dictionary(Of String, String) = CardConnect.GetBusinessTypes()
            'ddTypeOfBusiness.DataSource = types
            'ddTypeOfBusiness.DataValueField = "key"
            'ddTypeOfBusiness.DataTextField = "value"
            'ddTypeOfBusiness.DataBind()

            dateBusinessStart.Culture = New Globalization.CultureInfo("en-us")
            dateOwnerDob.Culture = New Globalization.CultureInfo("en-us")
        End If
    End Sub

    Protected Sub Register(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegister.Click
        If Not Page.IsValid Then
            Exit Sub
        End If

        Dim vo = New Netpay.Bll.Merchants.Registration()
        vo.Address = txtAddress.Text.NullIfEmpty
        vo.AnticipatedAverageTransactionAmount = txtAnticipatedAverageTransAmount.Text.NullIfEmpty
        vo.AnticipatedLargestTransactionAmount = txtAnticipatedLargestTransactionAmount.Text.NullIfEmpty
        vo.PercentDelivery0to7 = txtPercentDelivery0to7.Text.ToInt32(0)
        vo.PercentDelivery15to30 = txtPercentDelivery15to30.Text.ToInt32(0)
        vo.PercentDelivery8to14 = txtPercentDelivery8to14.Text.ToInt32(0)
        vo.PercentDeliveryOver30 = txtPercentDeliveryOver30.Text.ToInt32(0)
        vo.AnticipatedMonthlyVolume = txtAnticipatedMonthlyVol.Text.NullIfEmpty
        vo.BankAccountNumber = txtBankAccountNumber.Text.NullIfEmpty
        vo.BankRoutingNumber = txtBankRoutingNumber.Text.NullIfEmpty
        vo.BusinessDescription = txtBusinessDescription.Text.NullIfEmpty
        vo.BusinessStartDate = dateBusinessStart.Date
        vo.City = txtCity.Text.NullIfEmpty
        vo.DbaName = txtDbaName.Text.NullIfEmpty
        vo.Email = txtEmail.Text.NullIfEmpty
        vo.FirstName = txtFirstName.Text.NullIfEmpty
        vo.Industry = ddIndustry.SelectedValue.ToInt32()
        vo.LastName = txtLastName.Text.NullIfEmpty
        vo.LegalBusinessName = txtLegalBusinessName.Text.NullIfEmpty
        vo.LegalBusinessNumber = txtbuisnessFEIN.Text
        vo.OwnerDob = dateOwnerDob.Date
        vo.OwnerSsn = txtOwnerSsn.Text.NullIfEmpty
        vo.PhisicalAddress = txtPhisicalAddress.Text.NullIfEmpty
        vo.PhisicalCity = txtPhisicalCity.Text.NullIfEmpty
        vo.PhisicalState = ddPhysicalState.ISOCode 'txtPhisicalState.Text.NullIfEmpty
        vo.PhisicalZip = txtPhisicalZipcode.Text.NullIfEmpty
        vo.Phone = txtPhone.Text.NullIfEmpty
        vo.Fax = txtfax.Text.NullIfEmpty
        vo.State = ddState.ISOCode 'txtState.Text.NullIfEmpty
        vo.StateOfIncorporation = ddStateOfIncorporation.ISOCode 'txtStateOfIncorporation.Text.NullIfEmpty
        vo.TypeOfBusiness = ddTypeOfBusiness.SelectedValue.ToInt32
        vo.Url = txtUrl.Text.NullIfEmpty
        vo.Zipcode = txtZipcode.Text.NullIfEmpty
        vo.CanceledCheckImageFileName = fuCanceledCheckImage.PostedFile.FileName
        vo.CanceledCheckImageMimeType = fuCanceledCheckImage.PostedFile.ContentType
        vo.CanceledCheckImageContent = New System.IO.BinaryReader(fuCanceledCheckImage.PostedFile.InputStream).ReadBytes(fuCanceledCheckImage.PostedFile.InputStream.Length).ToBase64()

        vo.Save()
        Dim result As Boolean = True
        If result Then
            SuccessResultMessage.Visible = True
            SuccessResultMessage.InnerText = "Registration has completed successfully"
            Try
                vo.SendRegistrationEmail()
            Catch ex As Exception
                SuccessResultMessage.InnerText &= ", Email not sent - " & ex.Message
            End Try
        Else
            ErrorResultMessage.Visible = True
            ErrorResultMessage.InnerText = "Registration failure"
        End If
    End Sub


    Sub AlertAndFocus(ByVal message As String, ByRef txtField As TextBox)
        lblError.Text = "<div class=""error"">" & message & "</div>"
        txtField.Focus()
    End Sub

    Protected Overrides Sub OnPreRender(e As System.EventArgs)
        ltCopyright.Text = CType(GetGlobalResourceObject("Login.aspx", "Copyright"), String).Replace("%BRAND%", WebUtils.CurrentDomain.BrandName)
        ltCopyright.Text = CType(GetGlobalResourceObject("MultiLang", "Copyright"), String).Replace("%BRAND%", WebUtils.CurrentDomain.BrandName)
        Dim link As New HtmlControls.HtmlLink()
        link.Href = WebUtils.MapTemplateVirPath(Me, "WebSite/Styles/login.css")
        link.Attributes.Add("rel", "stylesheet")
        link.Attributes.Add("type", "text/css")
        link.Attributes.Add("media", "screen")
        Page.Header.Controls.Add(link)
        If System.Threading.Thread.CurrentThread.CurrentUICulture.TextInfo.IsRightToLeft Then
            link = New HtmlControls.HtmlLink()
            link.Href = WebUtils.MapTemplateVirPath(Me, "WebSite/Styles/loginRTL.css")
            link.Attributes.Add("rel", "stylesheet")
            link.Attributes.Add("type", "text/css")
            link.Attributes.Add("media", "screen")
            Page.Header.Controls.Add(link)
        End If
        MyBase.OnPreRender(e)
    End Sub

End Class