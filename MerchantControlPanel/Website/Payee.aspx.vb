﻿Imports Netpay.Infrastructure

Public Class Payee
    Inherits MasteredPage
    Protected Item As Bll.Accounts.Payee
    Protected ILBankAccount As Bll.Wires.Masav.IsraelBankAccount

    Dim PayeeID As Nullable(Of Integer)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PayeeID = Request("ID").ToNullableInt32()
        If PayeeID.HasValue Then Item = Bll.Accounts.Payee.Load(PayeeID)
        If Item Is Nothing Then Item = New Bll.Accounts.Payee(Account.AccountID)
        chkCorrespondentAccount.Attributes.Add("onclick", "$('#" & dvBankAccountCorresponded.ClientID & "').css('display', this.checked ? '' : 'none');")
        If Not IsPostBack Then
            BankAccountMaster.Item = Item.BankAccount
            ILBankAccount = New Bll.Wires.Masav.IsraelBankAccount(Item.BankAccount)
            If ddlILBankCode.Items.FindByValue(ILBankAccount.ILBankCode) IsNot Nothing Then ddlILBankCode.SelectedValue = ILBankAccount.ILBankCode
            If Item.BankAccount IsNot Nothing Then
                BankAccountCorresponded.Item = Item.BankAccount.CorrespondentAccount
                chkCorrespondentAccount.Checked = False
                If BankAccountCorresponded.Item IsNot Nothing Then chkCorrespondentAccount.Checked = BankAccountCorresponded.Item.ID > 0
            End If
            container.DataBind()
        End If
    End Sub

    Protected Overrides Sub OnPreRender(e As EventArgs)
        chkCorrespondentAccount.Checked = Not BankAccountCorresponded.IsEmpty
        If Not chkCorrespondentAccount.Checked Then dvBankAccountCorresponded.Style.Add("display", "none")
        MyBase.OnPreRender(e)
    End Sub

    Protected Sub Button_Command(sender As Object, e As CommandEventArgs)
        If Item Is Nothing Then Item = New Bll.Accounts.Payee(Account.AccountID)
        Item.SearchTag = txtSearchTag.Text
        Item.LegalNumber = txtLegalNumber.Text
        Item.CompanyName = txtCompanyName.Text
        Item.ContactName = txtContactName.Text
        Item.EmailAddress = txtEmailAddress.Text
        Item.PhoneNumber = txtPhoneNumber.Text
        Item.FaxNumber = txtFaxNumber.Text
        Item.StreetAddress = txtStreetAddress.Text
        Item.Comment = txtComment.Text

        If mvIsraelBankAccount.ActiveViewIndex = 0 Then
            ILBankAccount = New Bll.Wires.Masav.IsraelBankAccount(Item.BankAccount)
            'ILBankAccount.AccountName = txtILAccountName.Text
            ILBankAccount.ILAccountNumber = txtILAccountNumber.Text
            ILBankAccount.ILBankCode = ddlILBankCode.SelectedValue
            ILBankAccount.ILBranchCode = txtILBranchCode.Text
        Else
            BankAccountMaster.Save(Item.BankAccount)
            If chkCorrespondentAccount.Checked Then
                If Item.BankAccount.CorrespondentAccount Is Nothing Then Item.BankAccount.CorrespondentAccount = New Bll.Accounts.BankAccount()
                BankAccountCorresponded.Save(Item.BankAccount.CorrespondentAccount)
            End If
        End If
        Item.Save()
        Response.Redirect("PayeeList.aspx")
    End Sub

    Protected Sub BtnDelete_Command(sender As Object, e As CommandEventArgs)
        Item = Bll.Accounts.Payee.Load(PayeeID)
        If Not Item Is Nothing Then Item.Delete()
        Response.Redirect("PayeeList.aspx")
    End Sub
End Class