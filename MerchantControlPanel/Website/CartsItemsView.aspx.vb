﻿Imports Netpay.Bll
Imports Netpay.Web
Imports Netpay.Web.Controls
Imports Netpay.Infrastructure

Partial Class Website_CartsItemsView
	Inherits MasteredPage

	Protected Sub Search(ByVal currentPage As Integer, ByVal e As EventArgs)
		Dim pagingInfo As PagingInfo = New PagingInfo()
		pagingInfo.PageSize = 20
		pagingInfo.CurrentPage = currentPage

		Dim filters As SearchFilters = New SearchFilters()
		filters.cartItemName = txtItemName.Text.NullIfEmpty()

		repeaterResults.DataSource = Cart.SearchCartsItems(WebUtils.CredentialsToken, filters, pagingInfo)
		repeaterResults.DataBind()

		pager.Info = pagingInfo
		phResults.Visible = repeaterResults.Items.Count > 0
	End Sub

	Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
		Search(1, e)
		pager.Visible = True
	End Sub
End Class
