﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" CodeBehind="DevHostedPageV2_Admin.aspx.vb" Inherits="Netpay.MerchantControlPanel.HostedPageV2_Admin" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:menu, URLSettings%>" runat="server" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Literal Text="<%$Resources: Title %>" runat="server" />
        </div>
        <div class="content-background">
            <div class="alert alert-info margin-bottom-10">
                <asp:Literal Text="<%$Resources: Intro %>" runat="server" />
            </div>
            <div class="section">
                <div class="margin-bottom-10 frame-border-option">

                    <div class="margin-bottom-10">
                      <i class="fa fa-external-link"></i>  <asp:Literal Text="<%$Resources: Global_Request %>" runat="server" />
                    </div>

                    <div class="frame-border-option-inline padding-10 margin-bottom-10">
                        <div id="divContent">
                            <div>
                                <asp:Literal Text="<%$Resources: Redirection_URL %>" runat="server" />
                                <span class="form-group-title-eg">
                                    <asp:Literal Text="<%$Resources: Url %>" runat="server" /></span>
                            </div>
                            <asp:TextBox runat="server" ID="ReplyURL" MaxLength="200" CssClass="percent100 url-input" />

                            <div class="margin-top-10">
                                <asp:Literal Text="<%$Resources: Notification_URL %>" runat="server" />
                                <span class="form-group-title-eg">
                                    <asp:Literal Text="<%$Resources: Url %>" runat="server" /></span>
                            </div>
                            <asp:TextBox runat="server" ID="ReplySilentURL" MaxLength="200" CssClass="percent100 url-input" />

                            <div class="query-test margin-top-10">
                                <asp:LinkButton runat="server" ID="btnSendRequest" CssClass="btn btn-info query-button"  OnClick="btnSendRequest_OnClick"><i class="fa fa-magic"></i> <asp:Literal Text="<%$Resources: Test %>" runat="server" /></asp:LinkButton>
                                <asp:Literal Text="<%$Resources: TestPage %>" runat="server" />
                            </div>
                            <asp:PlaceHolder ID="phReplyContent" runat="server" Visible="false">
                                <div class="wrap-box-result url-input">
                                    <asp:Literal runat="server" ID="ltRequestRet" Mode="PassThrough" />
                                </div>
                            </asp:PlaceHolder>
                        </div>
                    </div>
                     <div class="wrap-button">
                    <asp:LinkButton runat="server" CssClass="btn btn-success btn-cons-short" UseSubmitBehavior="true" ID="btnSave" OnClick="btnSave_OnClick">
                        <i class="fa fa-floppy-o"></i> <asp:Literal Text="<%$Resources: Update %>" runat="server" />
                    </asp:LinkButton>
                </div>
                </div>
               
            </div>
           
        </div>

    </div>
</asp:Content>
