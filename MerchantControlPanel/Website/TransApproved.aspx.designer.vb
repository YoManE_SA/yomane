﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Website_TransApproved
    
    '''<summary>
    '''locPageTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents locPageTitle As Global.System.Web.UI.WebControls.Localize
    
    '''<summary>
    '''hdLastSearchMode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdLastSearchMode As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''hdDeviceId control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdDeviceId As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Last4digits control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Last4digits As Global.System.Web.UI.WebControls.RegularExpressionValidator
    
    '''<summary>
    '''Literal4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal4 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''PopupButton1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PopupButton1 As Global.Netpay.Web.Controls.PopupButton
    
    '''<summary>
    '''ttLegend control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ttLegend As Global.Netpay.Web.Controls.Legend
    
    '''<summary>
    '''Literal6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal6 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''txtTransactionNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTransactionNumber As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''btnShowTransaction control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnShowTransaction As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''rblSearchType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rblSearchType As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''wcDateRangePicker control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents wcDateRangePicker As Global.Netpay.Web.Controls.DateRangePicker
    
    '''<summary>
    '''ddPaymentMethod control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddPaymentMethod As Global.Netpay.Web.Controls.PaymentMethodDropDown
    
    '''<summary>
    '''ddCurrency control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddCurrency As Global.Netpay.Web.Controls.CurrencyDropDown
    
    '''<summary>
    '''ddPaymentsStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddPaymentsStatus As Global.Netpay.Web.Controls.PaymentsStatusDropDown
    
    '''<summary>
    '''ddCreditType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddCreditType As Global.Netpay.Web.Controls.CreditTypeDropDown
    
    '''<summary>
    '''txtPayerId control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPayerId As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtOrderNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtOrderNumber As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtPayerFirstName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPayerFirstName As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtPayerLastName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPayerLastName As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtCardHolderEmail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCardHolderEmail As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtLast4Digits control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtLast4Digits As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''btnSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSearch As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''phResults control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents phResults As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''wcFiltersView control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents wcFiltersView As Global.Netpay.Web.Controls.SearchFiltersView
    
    '''<summary>
    '''litDateTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litDateTitle As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''lblTableHeadings control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTableHeadings As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''repeaterResults control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents repeaterResults As Global.System.Web.UI.WebControls.Repeater
    
    '''<summary>
    '''btnExportExcel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnExportExcel As Global.Netpay.Web.Controls.ExcelButton
    
    '''<summary>
    '''pager control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pager As Global.Netpay.Web.Controls.LinkPager
    
    '''<summary>
    '''Div1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Div1 As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''ltError control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ltError As Global.System.Web.UI.WebControls.Literal
End Class
