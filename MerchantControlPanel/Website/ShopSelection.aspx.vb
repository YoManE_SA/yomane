﻿Imports Netpay.Bll
Imports Netpay.Bll.Shop

Public Class ShopSelection
    Inherits MasteredPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadShops()
        End If
    End Sub

    Private Sub LoadShops()
        Dim shops As List(Of RegionShop) = RegionShop.LoadAll(Merchant.ID)
        For Each shop As RegionShop In shops
            If shop.Locations.IsGlobal Then
                Dim r As Region = New Region()
                r.Name = "Global"
                r.IsoCode = "GLOBAL"
                shop.Locations.Regions.Add(r)
            End If
        Next

        repeaterShops.DataSource = shops
        repeaterShops.DataBind()
    End Sub

    Protected Sub btnDelete_Command(sender As Object, e As CommandEventArgs)
        Dim shopId As Integer = Integer.Parse(e.CommandArgument)
        Dim shop As RegionShop = RegionShop.Load(shopId)
        shop.Delete()
        LoadShops()
    End Sub

    Protected Sub repeaterCountries_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim item As Country = e.Item.DataItem
        Dim flagImage As HtmlImage = e.Item.FindControl("imgFlag")
        flagImage.Src = "/NPCommon/ImgCountry/18X12/--.gif"
        If item IsNot Nothing Then
            Dim imgUrl As String = "/NPCommon/ImgCountry/50X30/" + item.IsoCode2 + ".png"
            If Not System.IO.File.Exists(MapPath(imgUrl)) Then imgUrl = "/NPCommon/ImgCountry/18X12/--.gif"
            flagImage.Src = imgUrl
        End If
    End Sub
End Class