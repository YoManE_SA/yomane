﻿Imports System.Collections.Generic
Imports Netpay.Web.Controls
Imports Netpay.Web
Imports Netpay.Infrastructure
Imports Netpay.Bll

Partial Class Website_CustomerTransactions
	Inherits MasteredPage

	Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
		lblResult.Text = String.Empty
		Dim isError As Boolean = String.IsNullOrEmpty(txtPayerEmailAddress.Text) And String.IsNullOrEmpty(txtPayerPhoneNumber.Text) And String.IsNullOrEmpty(txtPayerFirstName.Text) And String.IsNullOrEmpty(txtPayerLastName.Text)
		If Validation.IsCustomError(txtPayerFirstName, isError, lblResult, "ErrorCustomerFilterWarning", True) Then Exit Sub
		If Validation.IsCustomError(txtPayerLastName, isError, lblResult, "ErrorCustomerFilterWarning", True) Then Exit Sub
		If Not String.IsNullOrEmpty(txtPayerEmailAddress.Text) Then
			If Validation.IsNotMail(txtPayerEmailAddress, lblResult, GetGlobalResourceObject("MultiLang", "CCHolderMail")) Then Exit Sub
		End If

		SearchCustomers()
	End Sub

	Protected Sub SearchCustomers()
        Dim filters = New Bll.Transactions.Payer.SearchFilters()

		'Dim cardNumber As String = wcCreditCard.Card.ToString().Trim().NullIfEmpty()
		'If cardNumber <> Nothing Then
		'	If cardNumber.Length = 4 Then
		'		filters.last4Digits = cardNumber
		'	ElseIf cardNumber.Length = 6 Then
		'		filters.first6Digits = cardNumber
		'	Else
        '        filters.creditCard = wcCreditCard.Card.ToString()
		'	End If
		'End If

		filters.FirstName = txtPayerFirstName.Text.NullIfEmpty()
		filters.LastName = txtPayerLastName.Text.NullIfEmpty()
		filters.EmailAddress = txtPayerEmailAddress.Text.NullIfEmpty()
		filters.PhoneNumber = txtPayerPhoneNumber.Text.NullIfEmpty()

        phNoResults.Visible = False
        phCustomerResults.Visible = False
        phTransactionsResult.Visible = False

        Dim results = Transactions.Payer.Search(filters)
        If results.Count > 0 Then
            repeaterCustomerResults.DataSource = results
            repeaterCustomerResults.DataBind()

            phCustomerResults.Visible = True
        Else
            phNoResults.Visible = True
        End If
    End Sub

	Protected Sub repeaterCustomerResults_itemCommand(ByVal sender As Object, ByVal e As RepeaterCommandEventArgs)
        Dim values As String() = e.CommandArgument.ToString().Split(New String() {"^@$$@^"}, StringSplitOptions.None)
        hiddenPayerFirstName.Value = values(0)
        hiddenPayerLastName.Value = values(1)
        hiddenPayerEmailAddress.Value = values(2)
        hiddenPayerPhoneNumber.Value = values(3)
        OnPageChanged(sender, e)
    End Sub

 	Protected Sub OnPageChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim filters As New Bll.Transactions.Transaction.SearchFilters
        filters.PayerFilters = New Transactions.Payer.SearchFilters()
        filters.PayerFilters.FirstName = hiddenPayerFirstName.Value.EmptyIfNull()
        filters.PayerFilters.LastName = hiddenPayerLastName.Value.EmptyIfNull()
        filters.PayerFilters.EmailAddress = hiddenPayerEmailAddress.Value.EmptyIfNull()
        filters.PayerFilters.PhoneNumber = hiddenPayerPhoneNumber.Value.EmptyIfNull()

        ltFilterText.Text = ""
        If Not String.IsNullOrEmpty(filters.PayerFilters.FirstName) Then ltFilterText.Text &= "<span class=""FilterMsgHead"">" & Resources.MultiLang.PayerFirstName & ":</span> " & filters.PayerFilters.FirstName & ", &nbsp;"
        If Not String.IsNullOrEmpty(filters.PayerFilters.LastName) Then ltFilterText.Text &= "<span class=""FilterMsgHead"">" & Resources.MultiLang.PayerLastName & ":</span> " & filters.PayerFilters.LastName & ", &nbsp;"
        If Not String.IsNullOrEmpty(filters.PayerFilters.EmailAddress) Then ltFilterText.Text &= "<span class=""FilterMsgHead"">" & Resources.MultiLang.CCHolderMail & ":</span> " & filters.PayerFilters.EmailAddress & ", &nbsp;"
        If Not String.IsNullOrEmpty(filters.PayerFilters.PhoneNumber) Then ltFilterText.Text &= "<span class=""FilterMsgHead"">" & Resources.MultiLang.CCHolderPhone & ":</span> " & filters.PayerFilters.PhoneNumber & ", &nbsp;"
        If ltFilterText.Text.EndsWith(", &nbsp;") Then ltFilterText.Text = ltFilterText.Text.Substring(0, ltFilterText.Text.Length - 8)

        'Dim so = New Transactions.Transaction.LoadOptions()
        'so.searchFail = True : so.searchPass = True : so.searchPending = True : so.searchApproval = True : so.loadPaymentData = True
		'repeaterTransactionsResults.DataSource = Bll.Transactions.Transaction.Search(LoggedUser.CredentialsToken, so, filters)
        repeaterTransactionsResults.DataSource = Bll.Transactions.TransactionBasicInfo.SearchPayerTransactions(Nothing, filters.PayerFilters.EmailAddress, filters.PayerFilters.FirstName, filters.PayerFilters.LastName, filters.PayerFilters.PhoneNumber, pager.Info)
        repeaterTransactionsResults.DataBind()
        phCustomerResults.Visible = False
        phTransactionsResult.Visible = True
    End Sub
End Class

