﻿Imports System.Collections.Generic
Imports Netpay.Web
Imports Netpay.Bll

Partial Class Website_UnsettledTransactionsBalance
	Inherits MasteredPage

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Dim preparedCollection As Dictionary(Of Bll.Currency, Integer) = New Dictionary(Of Bll.Currency, Integer)()
        Dim collection As Dictionary(Of Integer, Netpay.Infrastructure.CountAmount) = Transactions.Transaction.GetUnsettledBalance(Nothing)

        For Each pair As KeyValuePair(Of Integer, Netpay.Infrastructure.CountAmount) In collection
            preparedCollection.Add(Bll.Currency.Get(pair.Key), pair.Value.Count)
        Next

		repeaterResults.DataSource = preparedCollection
		repeaterResults.DataBind()
	End Sub
End Class
