﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Linq
Imports Netpay.Bll
Imports System.Collections.Generic
Imports Netpay.Web.Controls
Imports Netpay.Web
Imports Netpay.Infrastructure

Partial Class Website_ViewPaymentsReport
    Inherits MasteredPage
    Implements IPostBackEventHandler

    Dim filters As Bll.Wires.Wire.SearchFilters = New Bll.Wires.Wire.SearchFilters()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If dbPages.TestVar(Request("RequestsID"), 0, -1, 0) > 0 Then
                txtnumber.Text = dbPages.TestVar(Request("RequestsID"), 0, -1, 0)
            End If
        End If
        If Not Page.IsPostBack Then
            btnSearch_Click(sender, e)

            Dim drData As SqlDataReader = dbPages.ExecReader("SELECT GD_ID, GD_Text FROM tblGlobalData WHERE GD_Group=52 AND GD_Lng=" & IIf(Netpay.Web.WebUtils.CurrentLanguageShort = "he", "0", "1") & " ORDER BY GD_Text")
            ddlcardType.Items.Add(New ListItem(GetGlobalResourceObject("MultiLang", "PaymentType"), String.Empty))
            Do While drData.Read()
                ddlcardType.Items.Add(New ListItem(drData(1), drData(0)))
            Loop
            drData.Close()
        End If
    End Sub

    Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pager.Info.PageCurrent = 0
        OnPageChanged(sender, e)
    End Sub

    Protected Sub OnPageChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        filters.ID = New Infrastructure.Range(Of Integer?)(txtnumber.Text.ToNullableInt(), txtnumber.Text.ToNullableInt())
        filters.AccountIDs = New List(Of Integer) From {Account.AccountID}
        filters.PayeeNumber = txtCustomerNum.Text.NullIfEmpty()
        filters.PayeeName = txtCustomerName.Text.NullIfEmpty()
        'If ddlcardType.SelectedValue <> "" And IsNumeric(ddlcardType.SelectedValue) Then filters.Source = New List(Of Wires.Wire.WireSource) From {ddlcardType.SelectedValue}
        'filters.Source = New List(Of Netpay.Bll.Wires.Wire.WireSource) From {Netpay.Bll.Wires.Wire.WireSource.MerchantMakePayments}
        repeaterSettlements.DataSource = Bll.Wires.Wire.Search(filters, pager.Info)
        repeaterSettlements.DataBind()
    End Sub

    Protected Function FormatName(ByVal item As Bll.Wires.Wire) As String
        Dim name As String = ""
        Return item.PayeeName
        'If item.PayeeName <> "" Then name = name & "/" & dbPages.dbtextShow(item.PayeeName.ToString)
        'If item.AccountName <> "" Then name = name & "/" & dbPages.dbtextShow(item.AccountName.ToString)
        'If item.BankAccount. <> "" Then name = name & "/" & dbPages.dbtextShow(item.BankAbroadAccountName2.ToString)
        'If name <> "" Then name = Mid(name, 2)
        FormatName = name
    End Function

    Protected Function FormatDate(ByVal item As Bll.Wires.Wire) As String
        FormatDate = dbPages.FormatDatesTimes(item.InsertDate, 1, 0, 0)
    End Function

    Protected Function FormatMoney(ByVal item As Bll.Wires.Wire) As String
        FormatMoney = dbPages.FormatCurr(item.Currency, item.Amount)
    End Function

    Public Shadows Sub RaisePostBackEvent(eventArgument As String) Implements IPostBackEventHandler.RaisePostBackEvent
        Dim values = eventArgument.Split("$")
        If (values.Length = 2 And values(0) = "Download") Then
            WebUtils.SendFile(Domain.MapPrivateDataPath("Wires/WireFiles/" + values(1)), Nothing, "attachment")
        End If
    End Sub

    Protected Function FormatHidden(ByVal item As Bll.Wires.Wire) As String
        Dim sData As New StringBuilder(String.Empty)
        sData.Append(GetGlobalResourceObject("MultiLang", "Situation") & " :")

        sData.Append(item.Status.EnumText("Enums") & " &nbsp;")
        If item.Status <> Wires.Wire.WireStatus.Pending Then sData.Append("," & dbPages.FormatDatesTimes(item.Date, 1, 0, 0))
        sData.Append("<br />")
        Dim sWireMoney_id As String = GetGlobalResourceObject("MultiLang", "None")
        If Not String.IsNullOrEmpty(item.ID) Then sWireMoney_id = item.ID.ToString()
        sData.Append(GetGlobalResourceObject("MultiLang", "WireNo") & ": <span class=""txt10"">" & sWireMoney_id & "</span><br />")
        Dim sWireComment As String = GetGlobalResourceObject("MultiLang", "None")
        If Not String.IsNullOrEmpty(item.Comment) Then sWireComment = item.Comment.ToString()
        sData.Append(GetGlobalResourceObject("MultiLang", "Comment") & " :" & dbPages.dbtextShow(sWireComment) & "<br />")
        FormatHidden = sData.ToString()
    End Function

    Protected Function FormatTransStatus(ByVal item As Bll.Wires.Wire) As String
        Dim sData As New StringBuilder(String.Empty)
        sData.Append(GetLocalResourceObject("Status") & ": ")
        'If item.WireStatus Is Nothing Then sData.Append("Not for transfer")
        'Infrastructure.GlobalData.GetValue(40, IIf(WebUtils.CurrentLanguageShort = "he", "0", "1"), dbPages.TestVar(, 0, -1, 0)).Value
        sData.Append(item.Status.EnumText("Enums") & " &nbsp;")
        If item.Status <> Wires.Wire.WireStatus.Pending Then sData.Append("," & dbPages.FormatDatesTimes(item.Date, 1, 0, 0))
        sData.Append("<br />")
        'Dim sWireMoney_id As String = GetGlobalResourceObject("MultiLang", "None")
        'sWireMoney_id = item.ID.ToString()
        sData.Append(GetGlobalResourceObject("MultiLang", "WireNo") & ": <span class=""txt10"">" & item.ID.ToString() & "</span><br />")
        Dim sWireComment As String = GetGlobalResourceObject("MultiLang", "None")
        If Not String.IsNullOrEmpty(item.Comment) Then sWireComment = item.Comment.ToString()
        sData.Append(GetLocalResourceObject("WireComment") & ": " & dbPages.dbtextShow(sWireComment))
        Dim nFile = Netpay.Bll.Wires.WireLog.GetLastFile(item.ID)
        If nFile IsNot Nothing Then
            Dim sExt As String = System.IO.Path.GetExtension(nFile.FileName)
            If sExt.StartsWith(".") Then sExt = sExt.Substring(1)
            sData.Append(" <a href=""" & ClientScript.GetPostBackClientHyperlink(Me, "Download$" & nFile.FileName) & """><img src=""/NPCommon/ImgFileExt/14X14/" & sExt & ".gif"" style=""border-width:0;"" /></a>")
        End If

        FormatTransStatus = sData.ToString()
    End Function

End Class
