﻿Imports Netpay.Infrastructure

Public Class UrlNotificationSettings
    Inherits MasteredPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Page.IsPostBack Then
            Dim settings As Netpay.Bll.Merchants.NoticatonUrlSettings = Netpay.Bll.Merchants.NoticatonUrlSettings.Load(Merchant.ID)
            chkIsRecurringReply.Checked = settings.IsRecurringReply
            txtRecurringReplyUrl.Text = settings.RecurringReplyURL
            chkIsRecurringModifyReply.Checked = settings.IsRecurringModifyReply
            txtRecurringModifyReplyUrl.Text = settings.RecurringModifyReplyURL
            chkIsPendingReply.Checked = settings.IsPendingReply
            txtPendingReplyUrl.Text = settings.PendingReplyURL
            chkIsWalletReply.Checked = settings.IsWalletReply
            txtWalletReplyUrl.Text = settings.WalletReplyURL
            chkUseHasKeyInWalletReply.Checked = settings.UseHasKeyInWalletReply
            txtNotifyRefundRequestApprovedUrl.Text = settings.NotifyRefundRequestApprovedUrl
            chkIsNotifyRefundRequestApproved.Checked = settings.IsNotifyRefundRequestApproved
            txtNotifyProcessURL.Text = settings.NotifyProcessURL

            Dim status As IList(Of GlobalData) = Netpay.Infrastructure.GlobalData.Cache.Where(Function(gd) gd.Group = GlobalDataGroup.RecurringChargeStatus And gd.LNG = Netpay.Web.WebUtils.CurrentLanguage).ToList()
            repeaterRecurringStatus.DataSource = status
            repeaterRecurringStatus.DataBind()

            If String.IsNullOrEmpty(Merchant.HashKey) Then
                chkUseHasKeyInWalletReply.Checked = False
                chkUseHasKeyInWalletReply.Enabled = False
                lblHash.Text = "<div class=""error margin-top-10"">" & GetLocalResourceObject("HashDisabled") & "</div>"
            Else
                lblHash.Text = GetLocalResourceObject("PersonalHashkey")
            End If
        End If

        Dim recurringSeetings As Netpay.Bll.Transactions.Recurring.MerchantSettings = Netpay.Bll.Transactions.Recurring.MerchantSettings.Load(Merchant.ID)
        If recurringSeetings IsNot Nothing Then
            panelRecurringAlert.Visible = Not recurringSeetings.IsEnabled
        End If
        panelEWalletAlert.Visible = False
    End Sub

    Protected Sub SaveForm(ByVal o As Object, ByVal e As EventArgs)
        Dim settings As Netpay.Bll.Merchants.NoticatonUrlSettings = Netpay.Bll.Merchants.NoticatonUrlSettings.Load(Merchant.ID)
        settings.IsRecurringReply = chkIsRecurringReply.Checked
        settings.RecurringReplyURL = txtRecurringReplyUrl.Text
        settings.IsRecurringModifyReply = chkIsRecurringModifyReply.Checked
        settings.RecurringModifyReplyURL = txtRecurringModifyReplyUrl.Text
        settings.IsPendingReply = chkIsPendingReply.Checked
        settings.PendingReplyURL = txtPendingReplyUrl.Text
        settings.IsWalletReply = chkIsWalletReply.Checked
        settings.WalletReplyURL = txtWalletReplyUrl.Text
        settings.UseHasKeyInWalletReply = chkUseHasKeyInWalletReply.Checked
        settings.NotifyRefundRequestApprovedUrl = txtNotifyRefundRequestApprovedUrl.Text
        settings.IsNotifyRefundRequestApproved = chkIsNotifyRefundRequestApproved.Checked
        settings.NotifyProcessURL = txtNotifyProcessURL.Text
        settings.Save()
    End Sub

    Protected ReadOnly Property PendingTestSendData As String
        Get
            Return "Reply=000&transID=12345&Date=01/01/2000&TypeCredit=0" & vbCrLf & "&Payments=1&Amount=10.00&Currency=1&Order=54321&reference=12345"
        End Get
    End Property

    Protected Sub btnTestPendingNotification(ByVal sender As Object, ByVal e As System.EventArgs)
        ltPendingTextResult.Visible = True
        If txtPendingReplyUrl.Text.Trim() = "" Then
            ltPendingTextResult.Text = "<div class=""error margin-top-10"">" & GetLocalResourceObject("UrlMissing") & "</div>"
            Return
        End If
        Try
            Dim url As String = txtPendingReplyUrl.Text + "?" + PendingTestSendData.Replace(vbCrLf, "")
            Dim reply As String = Nothing
            Netpay.Infrastructure.HttpClient.SendHttpRequest(url, reply)
            ltPendingTextResult.Text = "<div class=""noerror"">" & GetLocalResourceObject("SuccesfullyMsg") & reply & "</div>"
        Catch ex As Exception
            ltPendingTextResult.Text = "<div class=""error"">" & GetLocalResourceObject("ErrorSendMsg") & ex.Message & "</div>"
        End Try
    End Sub

    Protected ReadOnly Property RefundReqTestSendData As String
        Get
            Return "RequestId=432&OriginalTransId=123212&RefundTransId=14323" & vbCrLf & "&Amount=100.00&Currency=1&RefundStatus=ProcessedAccepted&Date=01/01/2012"
        End Get
    End Property

    Protected Sub btnTestRefundRequestNotification(ByVal sender As Object, ByVal e As System.EventArgs)
        ltRefundRequestTextResult.Visible = True
        If txtNotifyRefundRequestApprovedUrl.Text.Trim() = "" Then
            ltRefundRequestTextResult.Text = "<div class=""error margin-top-10"">" & GetLocalResourceObject("UrlMissing") & "</div>"
            Return
        End If
        Try
            Dim url As String = txtNotifyRefundRequestApprovedUrl.Text + "?" + RefundReqTestSendData.Replace(vbCrLf, "")
            Dim reply As String = Nothing
            Netpay.Infrastructure.HttpClient.SendHttpRequest(url, reply)
            ltRefundRequestTextResult.Text = "<div class=""noerror"">" & GetLocalResourceObject("SuccesfullyMsg") & reply & "</div>"
        Catch ex As Exception
            ltRefundRequestTextResult.Text = "<div class=""error"">" & GetLocalResourceObject("ErrorSendMsg") & ex.Message & "</div>"
        End Try
    End Sub
End Class