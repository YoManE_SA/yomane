﻿<%@ Page Title="PageTitle" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_RecurrindChargesMain" CodeBehind="RecurringChargesMain.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
	<asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />

	<script language="javascript" type="text/javascript">
		function expandNode(chargeID) {
			trchargeInfo = document.getElementById("trchargeInfo" + chargeID)
			imgExpandNode = document.getElementById("imgExpandNode" + chargeID)

			if (trchargeInfo.style.display == '') {
				imgExpandNode.src = '/NPCommon/Images/tree_expand.gif';
				trchargeInfo.style.display = 'none';
			}
			else {
				imgExpandNode.src = '/NPCommon/Images/tree_collapse.gif';
				trchargeInfo.style.display = '';
			}
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
	<asp:HiddenField ID="hdLastSearchMode" runat="server" />
	<div class="area-content">
		<div class="top-nav">
			<asp:Localize ID="Localize1" Text="<%$Resources:PageTitle%>" runat="server" />
		</div>
		<div class="content-background">
			<div class="legend-menu">
				<div class="align-right">
					<netpay:PopupButton ID="PopupButton1" IconSrc="Images/iconLegend.gif"
						Width="100" Height="120" Text="<%$Resources:MultiLang,Legend  %>" runat="server">
						<netpay:Legend ID="ttLegend" Waiting="true" Passed="true" Canceled="true" Blocked="true"
							Authorize="true" runat="server" />
					</netpay:PopupButton>
				</div>
			</div>
			<div class="section">
				<div class="filter">
					<div class="quick-search">
						<asp:Literal ID="Literal2" runat="server" Text="<%$Resources:MultiLang,Quicksearch  %>"></asp:Literal>
						<div class="field-quick">
							<asp:TextBox ID="txtSeriesID" runat="server" placeholder="<%$Resources:MultiLang,Chargenumber  %>"
								CssClass="percent100"></asp:TextBox>
						</div>
						<div class="button-quick">
							<asp:LinkButton ID="buttonSearchSingle" CssClass="btn" runat="server"><i class="fa fa-search"></i> <asp:Literal runat="server" Text="<%$Resources:MultiLang,Search  %>" /></asp:LinkButton>
						</div>
					</div>
				</div>
				<div class="align-left percent33">
					<div>
						<asp:Literal runat="server" Text="<%$Resources:MultiLang,ChargeDate  %>" /></div>
					<div>
						<netpay:DateRangePicker ID="wcDateRangePicker" HtmlLayout="Flow" runat="server" />
					</div>

				</div>

				<div class="align-left percent33">

					<div><asp:Literal runat="server" Text="<%$Resources:MultiLang,Paymentmethod  %>" /></div>
					<div>
						<netpay:PaymentMethodDropDown ID="ddPaymentMethod" BlankSelectionText="<%$Resources:MultiLang,Paymentmethod  %>" EnableBlankSelection="true" PaymentMethodType="CreditCard" CssClass="percent100" runat="server">
						</netpay:PaymentMethodDropDown>
					</div>
				</div>
				<div class="spacer"></div>
				<div class="wrap-button-bar">
					<asp:LinkButton ID="buttonSearch" CssClass="btn btn-cons-short btn-inverse" runat="server"><i class="fa fa-search"></i> <asp:Literal runat="server" Text="<%$Resources:MultiLang,Search  %>" /></asp:LinkButton>
				</div>
			</div>
			<asp:PlaceHolder ID="phResults" Visible="false" runat="server">
				<div class="section">
					<netpay:SearchFiltersView ID="wcFiltersView" runat="server" Visible="false" />
					<table class="exspand-table">
						<tr>
							<th>
								&nbsp;
							</th>
							<th>
								<asp:Literal Text="<%$Resources:MultiLang,Charge %>" runat="server" />
							</th>
							<th>
								<asp:Literal Text="<%$Resources:MultiLang,Date %>" runat="server" />
							</th>
							<th>
								<asp:Literal Text="<%$Resources:MultiLang,Type %>" runat="server" />
							</th>
							<th>
								<asp:Literal Text="<%$Resources:MultiLang,PaymentMethod %>" runat="server" />
							</th>
							<th>
								<asp:Literal Text="<%$Resources:MultiLang,Amount %>" runat="server" />
							</th>
							<th>
								<asp:Literal Text="<%$Resources:MultiLang,Actions %>" runat="server" />
							</th>
							<th colspan="2">
								<asp:Literal Text="<%$Resources:MultiLang,Series %>" runat="server" />
							</th>
						</tr>
						<asp:Repeater ID="repeaterResults" EnableViewState="true" runat="server" OnLoad="ResetFilterClassSeries">
							<ItemTemplate>
								<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
									<td style="width: 10px; padding: 0px;">
										<div style="margin: 10px; width: 15px; height: 15px;" class="<%# GetStatusClass(CType(Container.DataItem, Transactions.Recurring.Charge)) %>">
											&nbsp;
										</div>
									</td>
									<td>
										<asp:Literal Text="<%# CType(Container.DataItem, Transactions.Recurring.Charge).ChargeNumber %>"
											runat="server"></asp:Literal>
									</td>
									<td>
										<asp:Literal Text='<%# CType(Container.DataItem, Transactions.Recurring.Charge).Date.ToString("dd/MM/yyyy") %>'
											runat="server"></asp:Literal>
									</td>
									<td>
										<asp:Literal Text="<%# GetTypeText(CType(Container.DataItem, Transactions.Recurring.Charge).Series.IsPreAuthorized) %>"
											runat="server"></asp:Literal>
									</td>
									<td>
										<netpay:PaymentMethodView ID="PaymentMethodView" StoredData="<%# CType(Container.DataItem, Transactions.Recurring.Charge).Payment %>" runat="server" />
									</td>
									<td>
										<asp:Literal Text="<%# CType(Container.DataItem, Transactions.Recurring.Charge).Amount.ToAmountFormat(CType(Container.DataItem, Transactions.Recurring.Charge).Currency.GetValueOrDefault()) %>" runat="server" />
									</td>
									<td>
										<asp:HyperLink ID="btnShowHistory" NavigateUrl="<%# GetHistoryUrl(CType(Container.DataItem, Transactions.Recurring.Charge)) %>"
											Text="<%$Resources:MultiLang,ShowChargeHistory %>" Visible="<%# IIf(CType(Container.DataItem, Transactions.Recurring.Charge).Attempts>0, True, False) %>"
											runat="server" />
										<asp:ImageButton ID="btnCancel" ImageUrl="/NPCommon/Images/iconB_suspend.png" AlternateText="<%$Resources:MultiLang,Cancel %>"
											OnCommand="SuspendCharge" CommandArgument="<%# CType(Container.DataItem, Transactions.Recurring.Charge).ID %>"
											Visible="<%# (Not CType(Container.DataItem, Transactions.Recurring.Charge).Suspended) And Not CType(Container.DataItem, Transactions.Recurring.Charge).Blocked And Not CType(Container.DataItem, Transactions.Recurring.Charge).Paid %>"
											runat="server" />
										<asp:ImageButton ID="btnReinstate" ImageUrl="/NPCommon/Images/iconB_resume.png" AlternateText="<%$Resources:MultiLang,Reinstate %>"
											OnCommand="ResumeCharge" CommandArgument="<%# CType(Container.DataItem, Transactions.Recurring.Charge).ID %>"
											Visible="<%# CType(Container.DataItem, Transactions.Recurring.Charge).Suspended And Not CType(Container.DataItem, Transactions.Recurring.Charge).Blocked And Not CType(Container.DataItem, Transactions.Recurring.Charge).Paid %>"
											runat="server" />
										&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:HyperLink ID="btnEdit" NavigateUrl="<%# GetEditUrl(CType(Container.DataItem, Transactions.Recurring.Charge)) %>"
											AlternateText="<%$Resources:MultiLang,Edit %>" Visible="<%# Not CType(Container.DataItem, Transactions.Recurring.Charge).Paid And Not CType(Container.DataItem, Transactions.Recurring.Charge).Blocked %>"
											runat="server"><img src="/NPCommon/Images/b_update_eng.gif" /></asp:HyperLink>
									</td>
									<td style="width: 10px; padding: 0px;">
										<div style="margin: 10px; width: 15px; height: 15px" class="<%# GetSeriesStatusClass(CType(Container.DataItem, Transactions.Recurring.Charge).Series) %>">
											&nbsp;
										</div>
									</td>
									<td class="filterCell">
										<asp:HyperLink ID="hlSeriesFilter" NavigateUrl="<%# GetSeriesFilterLink(CType(Container.DataItem, Transactions.Recurring.Charge)) %>"
											Text='<%# "<span>" & CType(Container.DataItem, Transactions.Recurring.Charge).SeriesID.ToString() & "</span>" %>'
											runat="server" />
									</td>
								</tr>
							</ItemTemplate>
						</asp:Repeater>
					</table>
					<div class="xls-wrap">
						<netpay:ExcelButton ID="btnExportExcel" Text="<%$Resources:MultiLang,ExportToXls %>"
							Enabled="false" runat="server" OnClick="ExportExcel_Click" IconImageOn="xls.png" IconImageOff="xls_off.png" />
					</div>
					<netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
					<netpay:FilterCss ID="FilterCss1" runat="server" Direction="<%$Resources:MultiLang,CssDirection %>" CellWidth="75" />
					<table>
						<tr>
							<td>
								<asp:Literal ID="litSeries" runat="server" /><asp:HyperLink ID="hlSeries" runat="server" Font-Bold="false" />
								<asp:Literal ID="litSeriesAfter" runat="server" />
							</td>
							<th>
								<asp:ImageButton ID="btnSuspend" ImageUrl="/NPCommon/Images/iconB_suspend.png" AlternateText="<%$Resources:MultiLang,Suspend %>" ToolTip="<%$Resources:MultiLang,Suspend %>" OnCommand="SuspendSeries" runat="server" />
								<asp:ImageButton ID="btnResume" ImageUrl="/NPCommon/Images/iconB_resume.png" AlternateText="<%$Resources:MultiLang,Resume %>" ToolTip="<%$Resources:MultiLang,Resume %>" OnCommand="ResumeSeries" runat="server" />
							</th>
						</tr>
					</table>
				</div>
			</asp:PlaceHolder>
		</div>
	</div>
</asp:Content>
