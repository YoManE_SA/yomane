﻿Imports Netpay.Web.Controls
Imports Netpay.Web

Partial Class Website_CartButtonSetup
	Inherits MasteredPage

	Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
		MyBase.OnLoad(e)

		If ddCurrency.IsSelected Then
			lblCurrency1.InnerText = ddCurrency.SelectedItem.Text
			lblCurrency2.InnerText = ddCurrency.SelectedItem.Text
		End If
	End Sub

	Protected Sub CreateItemButton(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreateItemButton.Click
		txtError.Visible = False

		If txtItemName.Text.Trim() = "" Then
			txtError.InnerHtml = GetLocalResourceObject("ItemNameValidation")
			txtError.Visible = True
			Return
		End If

		If Not ddCurrency.IsSelected Then
			txtError.InnerHtml = GetLocalResourceObject("CurrencyValidation")
			txtError.Visible = True
			Return
		End If

		If txtPrice.Text.Trim() = "" Then
			txtError.InnerHtml = GetLocalResourceObject("PriceValidation")
			txtError.Visible = True
			Return
		End If

		Dim preHash As String = txtItemName.Text + txtPrice.Text + txtShipping.Text + txtTaxRate.Text + Merchant.HashKey
		Dim signature As String = Netpay.Infrastructure.Security.Encryption.GetMd5Hash(preHash)

		Dim htmlBuilder As New StringBuilder()
		Dim target As String = "_blank"
		If ddActivationModeItem.SelectedValue = "redirect" Then target = "_self"
		htmlBuilder.AppendFormat("<form target='{0}' action='{1}' method='post'>", target, WebUtils.CurrentDomain.CartUrl)
		htmlBuilder.AppendLine()
		htmlBuilder.AppendFormat("<input type='hidden' name='merchantID' value='{0}'/>", Merchant.Number)
		htmlBuilder.AppendLine()
		htmlBuilder.AppendFormat("<input type='hidden' name='itemName' value='{0}'/>", txtItemName.Text)
		htmlBuilder.AppendLine()
		htmlBuilder.AppendFormat("<input type='hidden' name='itemSignature' value='{0}'/>", signature)
		htmlBuilder.AppendLine()
		htmlBuilder.AppendFormat("<input type='hidden' name='currency' value='{0}'/>", ddCurrency.SelectedValue)
		htmlBuilder.AppendLine()
		htmlBuilder.AppendFormat("<input type='hidden' name='price' value='{0}'/>", txtPrice.Text)
		htmlBuilder.AppendLine()
		htmlBuilder.AppendFormat("<input type='hidden' name='taxRate' value='{0}'/>", txtTaxRate.Text)
		htmlBuilder.AppendLine()
		htmlBuilder.AppendFormat("<input type='hidden' name='shipping' value='{0}'/>", txtShipping.Text)
		htmlBuilder.AppendLine()
		htmlBuilder.AppendFormat("<input type='hidden' name='infoUrl' value='{0}'/>", txtItemInfoUrl.Text)
		htmlBuilder.AppendLine()
		htmlBuilder.AppendFormat("<input type='hidden' name='imageUrl' value='{0}'/>", txtItemImageUrl.Text)
		htmlBuilder.AppendLine()
		htmlBuilder.AppendFormat("<input type='hidden' name='activationMode' value='{0}'/>", ddActivationModeItem.SelectedValue)
		htmlBuilder.AppendLine()
		htmlBuilder.AppendFormat("<input type='image' src='{0}/Images/addToCart.gif' border='0' name='submit' alt='Netpay'/>", WebUtils.CurrentDomain.CartUrl)
		htmlBuilder.AppendLine()
		htmlBuilder.Append("</form>")

		txtResult.Text = htmlBuilder.ToString()
	End Sub

	Protected Sub CreateCartButton(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreateCartButton.Click
		txtError.Visible = False

		Dim htmlBuilder As New StringBuilder()
		Dim target As String = "_blank"
		If ddActivationModeCart.SelectedValue = "redirect" Then target = "_self"
		htmlBuilder.AppendFormat("<form target='{0}' action='{1}' method='post'>", target, WebUtils.CurrentDomain.CartUrl)
		htmlBuilder.AppendLine()
		htmlBuilder.AppendFormat("<input type='image' src='{0}/Images/showCart.gif' border='0' name='submit' alt='Netpay'/>", WebUtils.CurrentDomain.CartUrl)
		htmlBuilder.AppendLine()
		htmlBuilder.Append("</form>")

		txtResult.Text = htmlBuilder.ToString()
	End Sub
End Class