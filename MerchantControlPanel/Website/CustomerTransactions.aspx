﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_CustomerTransactions" CodeBehind="CustomerTransactions.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
    <asp:HiddenField runat="server" ID="hiddenPayerFirstName" />
    <asp:HiddenField runat="server" ID="hiddenPayerLastName" />
    <asp:HiddenField runat="server" ID="hiddenPayerEmailAddress" />
    <asp:HiddenField runat="server" ID="hiddenPayerPhoneNumber" />
    <div class="area-content">
     
        <div class="top-nav">
            <asp:Literal ID="Literal20" runat="server" Text="<%$Resources:MultiLang,CustomerSearch %>" />
        </div>
        <div class="content-background">
        
            <div class="legend-menu">
                <div class="align-right">
                    <netpay:PopupButton ID="PopupButton1" IconSrc="Images/iconLegend.gif" Width="130" Height="180" Text="<%$Resources:MultiLang,Legend %>" runat="server">
                        <netpay:Legend Capture="true" Chargeback="true" Test="true" Decline="true" Authorize="true" AuthorizeDecline="true" runat="server" ID="ttLegend" />
                    </netpay:PopupButton>
                </div>
            </div>
            <div class="section">
                <asp:Label ID="lblResult" runat="server" />
                <div style="text-align: <%= WebUtils.CurrentAlignment %>; color: red">
                    <asp:Literal ID="ltError" Visible="false" runat="server" />
                </div>
                <div class="align-left percent23 margin-right-10 ">
                    <div><asp:Literal Text="<%$Resources:MultiLang,PayerFirstName %>" runat="server" /></div>
                    <div><asp:TextBox ID="txtPayerFirstName" runat="server" CssClass="percent100" /></div>
                </div>
                <div class="align-left percent23 margin-right-10 ">
                    <div><asp:Literal Text="<%$Resources:MultiLang,PayerLastName %>" runat="server" /></div>
                    <div><asp:TextBox ID="txtPayerLastName" runat="server" CssClass="percent100" /></div>
                </div>
                <div class="align-left percent23 margin-right-10 ">
                    <div>
                        <asp:Literal Text="<%$Resources:MultiLang,CCHolderMail %>" runat="server" /></div>
                    <div>
                        <asp:TextBox ID="txtPayerEmailAddress" runat="server" CssClass="percent100" /></div>
                </div>
                <div class="align-left percent23 margin-right-10 ">
                    <div><asp:Literal Text="<%$Resources:MultiLang,CCHolderPhone %>" runat="server" /></div>
                    <div><asp:TextBox ID="txtPayerPhoneNumber" runat="server" CssClass="percent100" /></div>
                </div>
                <div class="spacer"></div>
                <div class="wrap-button-bar">
                    <asp:LinkButton ID="btnSearch" OnClick="btnSearch_Click"  CssClass="btn btn-cons-short btn-inverse" runat="server"><i class="fa fa-search"></i> <asp:Literal Text="<%$Resources:MultiLang,Search %>" runat="server" /></asp:LinkButton>
                </div>
            </div>

            <asp:PlaceHolder ID="phNoResults" Visible="false" runat="server">
                <div class="section">
                    <asp:Literal Text="<%$Resources:MultiLang,NoResults %>" runat="server" />
                </div>
            </asp:PlaceHolder>

            <asp:PlaceHolder ID="phCustomerResults" Visible="false" runat="server">
                <div class="section">
                    <table class="exspand-table">
                        <tr>
                            <th>
                                <asp:Literal Text="<%$Resources:MultiLang,PayerFirstName %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:MultiLang,PayerLastName %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:MultiLang,CCHolderMail %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal Text="<%$Resources:MultiLang,CCHolderPhone %>" runat="server" />
                            </th>
                            <th colspan="2">
                                <asp:Literal Text="<%$Resources:MultiLang,Transactions %>" runat="server" />
                                *
                            </th>
                        </tr>
                        <asp:Repeater ID="repeaterCustomerResults" OnItemCommand="repeaterCustomerResults_itemCommand" runat="server">
                            <ItemTemplate>
                                <tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
                                    <td><%# Eval("FirstName") %></td>
                                    <td><%# Eval("LastName") %></td>
                                    <td><%# Eval("EmailAddress") %></td>
                                    <td><%# Eval("PhoneNumber") %></td>
                                    <td><%# Eval("Count") %></td>
                                    <td align="center">
                                        <asp:LinkButton ID="LinkButton1" CssClass="btn btn-default" Text="<%$Resources:MultiLang,Show %>" CommandArgument='<%# Eval("FirstName") + "^@$$@^" + Eval("LastName") + "^@$$@^" + Eval("EmailAddress") + "^@$$@^" + Eval("PhoneNumber") %>' runat="server" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <tr>
                            <td colspan="5" style="font-size: 10px; border: 0px;">* Final transaction count may be different from number shown
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phTransactionsResult" Visible="false" runat="server">
                <div class="section">
                    <table class="exspand-table" align="center" border="0">
                        <tr>
                            <td style="padding-bottom: 6px;">
                                <span class="FilterHeading">Shown:</span>
                                <asp:Literal runat="server" ID="ltFilterText" />
                            </td>
                        </tr>
                    </table>
                    <table class="exspand-table" align="center" cellpadding="1" cellspacing="1">
                        <tr>
                            <th colspan="2">&nbsp;
                            </th>
                            <th>
                                <asp:Literal ID="Literal15" Text="<%$Resources:MultiLang,Transaction %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal ID="Literal16" Text="<%$Resources:MultiLang,Date %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal ID="Literal17" Text="<%$Resources:MultiLang,PaymentMethod %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal ID="Literal18" Text="<%$Resources:MultiLang,Amount %>" runat="server" />
                            </th>
                            <th>
                                <asp:Literal ID="Literal19" Text="<%$Resources:MultiLang,PayoutStatus %>" runat="server" />
                            </th>
                        </tr>
                        <asp:Repeater ID="repeaterTransactionsResults" EnableViewState="false" runat="server">
                            <ItemTemplate>
                                <netpay:MerchantTransactionRowView ID="ucTransactionView" Transaction="<%# Container.DataItem %>" ShowStatus="true" runat="server" />
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
                </div>
            </asp:PlaceHolder>
        </div>
    </div>
</asp:Content>
