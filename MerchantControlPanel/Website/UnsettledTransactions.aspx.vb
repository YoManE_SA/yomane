﻿Imports System.Collections.Generic
Imports Netpay.Web.Controls
Imports Netpay.Bll.Reports
Imports System.Linq
Imports Netpay.Bll
Imports Netpay.Infrastructure

Partial Class Website_UnsettledTransactions
	Inherits MasteredPage

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
		ucShowTotalsButton.CurrencyID = ddCurrency.SelectedValue.ToInt32()
		If Not IsPostBack Then
			' filter unused currencies
            Dim currencyCount As Dictionary(Of Integer, Netpay.Infrastructure.CountAmount) = Bll.Transactions.Transaction.GetUnsettledBalance(Nothing)
            Dim usedCurrenciesIDs As Integer() = currencyCount.Where(Function(kvp) kvp.Value.Count > 0).Select(Function(kvp) kvp.Key).ToArray()
			ddCurrency.SetCurrencyIDs(usedCurrenciesIDs)
		End If
	End Sub

	Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
		pager.Info.PageCurrent = 0
		OnPageChanged(sender, e)
		pager.Visible = True
	End Sub

	Protected Sub btnAdvancedSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
		phDatePicker.Visible = True
		phDatePicker.Visible = True
		wcDateRangePicker.FromDate = DateTime.Now.AddMonths(-1)
		wcDateRangePicker.ToDate = DateTime.Now

		phResults.Visible = False
		btnAdvancedSearch.Visible = False
	End Sub

 	Protected Sub OnPageChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim filters = New Transactions.Transaction.SearchFilters()
		If wcDateRangePicker.Visible Then
			filters.date.From = wcDateRangePicker.FromDate
			filters.date.To = wcDateRangePicker.ToDate
        End If
        filters.currencyID = ddCurrency.SelectedValue.ToInt32()
		filters.merchantIDs = New List(Of Integer) From { Merchant.ID }
        filters.HasUnsettledInstallments = True

		If TypeOf e Is ExcelButton.ExportEventArgs Then
            btnExportExcel.SetExportInfo(Nothing, ReportType.MerchantCapturedTransactions, Function(pi) Transactions.Transaction.Search(Infrastructure.TransactionStatus.Captured, filters, pi, False, False))
		Else
            repeaterResults.DataSource = Transactions.Transaction.Search(Infrastructure.TransactionStatus.Captured, filters, pager.Info, False, False)
			repeaterResults.DataBind()
			btnExportExcel.ResultCount = pager.Info.RowCount
			phResults.Visible = pager.Info.RowCount > 0
		End If
		ucShowTotalsButton.InsDateFrom = wcDateRangePicker.FromDate
		ucShowTotalsButton.InsDateTo = wcDateRangePicker.ToDate
		ucShowTotalsButton.CurrencyID = ddCurrency.SelectedValue.ToInt32()
	End Sub

	Protected Sub ExportExcel_Click(ByVal sender As Object, ByVal e As EventArgs)
		OnPageChanged(sender, e)
		btnExportExcel.Export()
	End Sub

End Class
