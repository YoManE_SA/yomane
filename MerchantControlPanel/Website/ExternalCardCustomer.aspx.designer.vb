﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ExternalCardCustomer
    
    '''<summary>
    '''txtTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTitle As Global.System.Web.UI.WebControls.Localize
    
    '''<summary>
    '''linkBackToCustomers control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents linkBackToCustomers As Global.System.Web.UI.HtmlControls.HtmlAnchor
    
    '''<summary>
    '''Literal3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal3 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''hfCustomerID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfCustomerID As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''txtCustomerName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCustomerName As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''lblSuccess control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSuccess As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ltAddingNew control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ltAddingNew As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''lblMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMsg As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Literal1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal1 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''txtAmount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAmount As Global.System.Web.UI.HtmlControls.HtmlInputText
    
    '''<summary>
    '''btnPayToCustomer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnPayToCustomer As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''wcElementBlocker control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents wcElementBlocker As Global.Netpay.Web.Controls.ElementBlocker
    
    '''<summary>
    '''phResults control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents phResults As Global.System.Web.UI.WebControls.PlaceHolder
    
    '''<summary>
    '''Literal2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal2 As Global.System.Web.UI.WebControls.Literal
    
    '''<summary>
    '''repeaterResults control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents repeaterResults As Global.System.Web.UI.WebControls.Repeater
    
    '''<summary>
    '''pager control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pager As Global.Netpay.Web.Controls.LinkPager
End Class
