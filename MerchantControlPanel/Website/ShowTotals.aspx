﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_ShowTotals" Codebehind="ShowTotals.aspx.vb" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Trans Total</title>
</head>
<body class="ShowtotalsBack">
<div class="wrap-showtotals">
   <table width="100%" border="0" cellspacing="0" cellpadding="0">	
		<tr>
			<td class="tdShowtotal">
				<netpay:Totals runat="server" ID="Total1" />
			</td>
		</tr>
        	</table>
			<div class="Showtotalclose">
				<asp:Literal ID="litCloseWindow" runat="server" />
			</div>
    </div>
</body>
</html>
