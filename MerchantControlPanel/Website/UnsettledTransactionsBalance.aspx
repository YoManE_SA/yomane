﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_UnsettledTransactionsBalance" Codebehind="UnsettledTransactionsBalance.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" Runat="Server">
	<asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle %>" runat="server" />
	
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" Runat="Server">

    <div class="area-content">
        <div class="top-nav">
           <asp:Localize ID="Localize1" Text="<%$Resources:PageTitle %>" runat="server" /></div>
        <div class="content-background">
            <div class="section">
             	<table class="exspand-table" align="center" border="0" >
		<tr>
			<th style="text-align:<%= WebUtils.CurrentAlignment %>;"><asp:Literal ID="Literal1" Text="<%$Resources:MultiLang,Currency %>" runat="server"></asp:Literal></th>
			<th style="text-align:<%= WebUtils.CurrentAlignment %>;"><asp:Literal ID="Literal2" Text="<%$Resources:TransactionCount %>" runat="server"></asp:Literal></th>
			<th>&nbsp;</th>		
		</tr>	
		<asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server">
			<ItemTemplate>
				<tr>
					<td>
						<%# CType(Eval("Key"), CurrencyVO).Symbol %>
					</td>
					<td>
						<%# Eval("Value") %>
					</td>
					<td style="text-align: center;">
						[<a href="UnsettledTransactions.aspx?Currency=<%# CType(Eval("Key"), CurrencyVO).ID %>" class="faq"><asp:Literal ID="Literal3" runat="server" Text="<%$Resources:MultiLang, Display %>" /></a>]
					</td>
				</tr>
			</ItemTemplate>
		</asp:Repeater>
	</table>
            </div>
        </div>
    </div>




</asp:Content>

