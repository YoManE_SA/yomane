﻿<%@ Page Title="PageTitle" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master"
    AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_ProcessRequestLog"
    CodeBehind="ProcessRequestLog.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <netpay:PageHelpBox ID="phbHelp" Width="100%" Text="<%$Resources:HelpBox %>" runat="server" />
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <script type="text/javascript" language="javascript">
        function SwitchImage(img) {
            if (img.src.indexOf('minus.png') > 0) img.src = '<%= MapTemplateVirPath("Images/plus.png")%>';
            else img.src = '<%= MapTemplateVirPath("Images/minus.png")%>';
        }
    </script>
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">
            <div class="legend-menu">
                <div class="align-right">
                    <netpay:PopupButton ID="PopupButton1" IconSrc="Images/iconLegend.gif"
                        Text="<%$Resources:MultiLang,Legend  %>" runat="server">
                        <netpay:Legend ID="Legend1" Capture="true" Decline="true" runat="server" />
                    </netpay:PopupButton>
                </div>
            </div>
            <div class="section">
                <div class="align-left percent33">
                    <div>
                        <asp:Literal runat="server" Text="<%$Resources:MultiLang,InsertDates  %>" />
                    </div>
                    <div>
                        <netpay:DateRangePicker ID="wcDateRangePicker" Layout="Horizontal" runat="server" />
                    </div>
                </div>
                <div class="align-left percent33">
                    <div>
                        <asp:Literal runat="server" Text="<%$Resources:MultiLang,ReplyCode  %>" />
                    </div>
                    <div>
                        <asp:TextBox ID="txtReplyCode" runat="server" CssClass="percent85" />
                    </div>
                </div>
                <div class="align-left percent33">
                    <div>
                        <asp:Literal runat="server" Text="<%$Resources:MultiLang,TransactionNumber  %>" />
                    </div>
                    <div>
                        <asp:TextBox ID="txtTransNum" runat="server" CssClass="percent85" />
                    </div>
                </div>
                <div class="spacer"></div>

                <div>
                    <asp:Literal ID="ltError" Visible="true" runat="server" />
                </div>
                <div class="wrap-button-bar">
                    <asp:LinkButton ID="btnSearch" CssClass="btn btn-cons-short btn-inverse" runat="server" OnClick="btnSearch_Click"><i class="fa fa-search"></i> <asp:Literal runat="server" Text="<%$Resources:MultiLang,Search  %>" /></asp:LinkButton>
                </div>
            </div>
            <div id="divDataWrapperSection" runat="server" visible="false" class="section">
                <div class="section">
                    <div id="divDataSection" visible="false" runat="server">
                        <table class="exspand-table">
                            <tr>
                                <th colspan="2">&nbsp;
                                </th>
                                <th>
                                    <asp:Literal ID="Literal3" Text="<%$Resources:MultiLang,Date %>" runat="server"></asp:Literal>
                                </th>
                                <th>
                                    <asp:Literal ID="Literal4" Text="<%$Resources:MultiLang,TransactionSource %>" runat="server"></asp:Literal>
                                </th>
                                <th>
                                    <asp:Literal ID="Literal5" Text="<%$Resources:MultiLang,RemoteAddress %>" runat="server"></asp:Literal>
                                </th>
                                <th>
                                    <asp:Literal ID="Literal6" Text="<%$Resources:MultiLang,RequestMethod %>" runat="server"></asp:Literal>
                                </th>
                                <th>
                                    <asp:Literal ID="Literal7" Text="<%$Resources:MultiLang,ReplyCode %>" runat="server"></asp:Literal>
                                </th>
                                <th>
                                    <asp:Literal ID="Literal8" Text="<%$Resources:MultiLang,Transaction %>" runat="server"></asp:Literal>
                                </th>
                            </tr>
                            <asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server">
                                <ItemTemplate>
                                    <tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">

                                        <td colspan="2">
                                            <img onclick="$('#rowInfo<%# Eval("ID") %>').toggle(0);SwitchImage(this);"
                                                style="cursor: pointer;" src="<%= MapTemplateVirPath("Images/plus.png")%>" alt=""
                                                border="0" align="middle" id="imgInfo<%# Eval("ID") %>" />
                                        </td>
                                        <td><%# Eval("DateStart", "{0:dd/MM/yyyy HH:mm}") %></td>
                                        <td><%# Eval("TransactionTypeId") %></td>
                                        <td><%# Eval("RemoteIPAddress") %></td>
                                        <td><%# Eval("RequestMethod") %></td>
                                        <td><%# Eval("ReplyCode") %></td>
                                        <td><%# Eval("TransactionID") %></td>
                                    </tr>
                                    <tr>
                                        <td id="rowInfo<%# Eval("ID") %>" style="display: none; border: 0px;"
                                            colspan="8">
                                            <div style="overflow: hidden; width: 100%;">
                                                <table cellpadding="1" cellspacing="5">
                                                    <tr>
                                                        <td valign="top" nowrap="nowrap" style="background-color: #f8f8f8; color: #000000;">
                                                            <span>
                                                                <asp:Literal ID="Literal9" Text="<%$Resources:MultiLang,ReplyDescription %>" runat="server"></asp:Literal></span>:
                                                        </td>
                                                        <td>
                                                            <%# Eval("ReplyDescription") %>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" nowrap="nowrap" style="background-color: #f8f8f8; color: #000000;">
                                                            <span>
                                                                <asp:Literal ID="Literal10" Text="<%$Resources:MultiLang,HttpHost %>" runat="server"></asp:Literal></span>:
                                                        </td>
                                                        <td>
                                                            <%# Eval("HttpHost") %>
                                                        </td>
                                                    </tr>
                                                    <tr id="Tr1" visible='<%# CType(Eval("QueryString"), String).EmptyIfNull().Length > 0 %>' runat="server">
                                                        <td valign="top" nowrap="nowrap" style="background-color: #f8f8f8; color: #000000;">
                                                            <span>
                                                                <asp:Literal ID="Literal11" Text="<%$Resources:MultiLang,QueryString %>" runat="server"></asp:Literal></span>:
                                                        </td>
                                                        <td>
                                                            <%# CType(Eval("QueryString"), String).EmptyIfNull().Replace("|"," &") %>
                                                        </td>
                                                    </tr>
                                                    <tr id="Tr2" visible='<%# CType(Eval("RequestForm"), String).EmptyIfNull().Length > 0 %>' runat="server">
                                                        <td valign="top" nowrap="nowrap" style="background-color: #f8f8f8; color: #000000;">
                                                            <span>
                                                                <asp:Literal ID="Literal12" Text="<%$Resources:MultiLang,RequestForm %>" runat="server"></asp:Literal></span>:
                                                        </td>
                                                        <td><%# CType(Eval("RequestForm"), String).EmptyIfNull().Replace("|"," &") %></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </div>
                    <netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
