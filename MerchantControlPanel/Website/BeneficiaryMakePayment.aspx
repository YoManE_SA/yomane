﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_BeneficiaryMakePayment" CodeBehind="BeneficiaryMakePayment.aspx.vb" %>

<%@ Import Namespace="System.Data.SqlClient" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$ Resources:PageTitle %>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$ Resources:PageTitle %>" runat="server" />
        </div>
        <div class="content-background">

            <div class="section">
                <div class="margin-bottom-10 frame-border-option">
                    <div class="margin-bottom-10"><i class="fa fa-credit-card"></i>
                        <asp:Literal ID="litProfileTitle" runat="server" /></div>
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">
                        <asp:Literal ID="litDataHeading" runat="server" /></div>
                    <div class="margin-bottom-10 margin-top-10"><i class="fa fa-bank"></i>
                        <asp:Literal runat="server" Text="<%$ Resources: PersonalBusinessDetails %>" /></div>
                    <ul class="group-list">
                        <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(basicInfo_costumerNumber) %>">
                            <li>
                                <asp:Literal runat="server" Text="<%$ Resources: Multilang, Identifier %>" />: <%# basicInfo_costumerNumber %></li>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(basicInfo_costumerNumber) %>">
                            <li>
                                <asp:Literal runat="server" Text="<%$ Resources: PersonalName %>" />
                                : <%# basicInfo_costumerName %></li>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(basicInfo_contactPersonName) %>">
                            <li>
                                <asp:Literal runat="server" Text="<%$ Resources: ContactPerson %>" />: <%# basicInfo_contactPersonName %></li>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(basicInfo_phoneNumber) %>">
                            <li>
                                <asp:Literal runat="server" Text="<%$ Resources: Multilang, Phone %>" />: <%# basicInfo_phoneNumber %> </li>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(basicInfo_faxNumber) %>">
                            <li>
                                <asp:Literal runat="server" Text="<%$ Resources: Multilang, Fax %>" />: <%# basicInfo_faxNumber %> </li>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(basicInfo_email) %>">
                            <li>
                                <asp:Literal runat="server" Text="<%$ Resources: Multilang, Email %>" />: <%# basicInfo_email %> </li>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(basicInfo_address) %>">
                            <li>
                                <asp:Literal runat="server" Text="<%$ Resources: Multilang, Address %>" />: <%# basicInfo_address %> </li>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(basicInfo_comment) %>">
                            <li>
                                <asp:Literal runat="server" Text="<%$ Resources: Multilang, Comment %>" />: <%# basicInfo_comment %> </li>
                        </asp:PlaceHolder>
                    </ul>
                    <asp:PlaceHolder runat="server" ID="phIsraeliBank">
                        <div class="margin-bottom-10 margin-top-10"><i class="fa fa-bank"></i>
                            <asp:Literal runat="server" Text="<%$ Resources: IsraeliBankAccountDetails %>" /></div>
                        <ul class="group-list">
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankIsraelInfo_PayeeName) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: AccountName %>" />: <%# bankIsraelInfo_PayeeName %></li>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankIsraelInfo_CompanyLegalNumber) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: CompanyLegalNumber %>" />: <%# bankIsraelInfo_CompanyLegalNumber %></li>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankIsraelInfo_personalIdNumber) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: PersonalID %>" />: <%# bankIsraelInfo_personalIdNumber %></li>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankIsraelInfo_bankBranch) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: BranchNumber %>" />: <%# bankIsraelInfo_bankBranch %></li>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankIsraelInfo_AccountNumber) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: AccountNumber %>" />: <%# bankIsraelInfo_AccountNumber %></li>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankIsraelInfo_PaymentMethod) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: AccountType %>" />: <%# bankIsraelInfo_PaymentMethod %></li>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankName) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: BankName %>" />: <%# bankName %></li>
                            </asp:PlaceHolder>
                        </ul>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="phAbroadBank">
                        <div class="margin-bottom-10 margin-top-10"><i class="fa fa-bank"></i>
                            <asp:Literal runat="server" Text="<%$ Resources: BankAccountDetails %>" /></div>
                        <ul class="group-list">
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankAbroadInfo_AccountName) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: AccountName %>" />: <%# bankAbroadInfo_AccountName %></li>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankAbroadInfo_AccountNumber) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: AccountNumber %>" />: <%# bankAbroadInfo_AccountNumber %></li>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankAbroadInfo_BankName) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: BankName %>" />: <%# bankAbroadInfo_BankName %></li>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankAbroadInfo_BankAddress) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: BankAddress %>" />: <%# bankAbroadInfo_BankAddress %>, <%# bankAbroadInfo_BankAddressSecond %> <%# ProfileAddressThird(bankAbroadInfo_BankAddressCity, bankAbroadInfo_BankAddressState, bankAbroadInfo_BankAddressZip) %></li>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankAbroadInfo_BankAddressCountry) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: Country %>" />: <%# bankAbroadInfo_BankAddressCountry %></li>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankAbroadInfo_SwiftNumber) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: SwiftCode %>" />: <%# bankAbroadInfo_SwiftNumber %></li>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankAbroadInfo_IBAN) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: IBAN %>" />: <%# bankAbroadInfo_IBAN %></li>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankAbroadInfo_ABA) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: ABA %>" />: <%# bankAbroadInfo_ABA %></li>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankAbroadInfo_SortCode) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: SortCode %>" />: <%# bankAbroadInfo_SortCode %></li>
                            </asp:PlaceHolder>
                        </ul>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="phCorrespondingBank">
                        <div class="margin-bottom-10 margin-top-10"><i class="fa fa-bank"></i>
                            <asp:Literal runat="server" Text="<%$ Resources: CorrespondingBankAccountDetails %>" /></div>
                        <ul class="group-list">
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankAbroadInfo_AccountName2) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: AccountName %>" />: <%# bankAbroadInfo_AccountName2 %></li>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankAbroadInfo_AccountNumber2) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: AccountNumber %>" />: <%# bankAbroadInfo_AccountNumber2 %></li>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankAbroadInfo_BankName2) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: BankName %>" />: <%# bankAbroadInfo_BankName2 %></li>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankAbroadInfo_BankAddress2) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: BankAddress %>" />: <%# bankAbroadInfo_BankAddress2 %></li>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankAbroadInfo_BankAddressSecond2) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: BankAddress %>" />
                                    2:
                                  <%# bankAbroadInfo_BankAddressSecond2 %>
                                    <%# ProfileAddressThird(bankAbroadInfo_BankAddressCity2, bankAbroadInfo_BankAddressState2, bankAbroadInfo_BankAddressZip2) %>
                                </li>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankAbroadInfo_BankAddressCountry2) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: Country %>" />: <%# bankAbroadInfo_BankAddressCountry2 %></li>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankAbroadInfo_SwiftNumber2) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: SwiftCode %>" />: <%# bankAbroadInfo_SwiftNumber2 %></li>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankAbroadInfo_IBAN2) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: IBAN %>" />: <%# bankAbroadInfo_IBAN2 %></li>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankAbroadInfo_ABA2) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: ABA %>" />: <%# bankAbroadInfo_ABA2 %></li>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" Visible="<%# Not String.IsNullOrEmpty(bankAbroadInfo_SortCode2) %>">
                                <li>
                                    <asp:Literal runat="server" Text="<%$ Resources: SortCode %>" />: <%# bankAbroadInfo_SortCode2 %></li>
                            </asp:PlaceHolder>
                        </ul>
                    </asp:PlaceHolder>

                    <div class="frame-border-option-inline padding-10 margin-bottom-10 margin-top-10">
                        <div id="trConfirmationFile" runat="server" class="margin-bottom-10">
                            <div><asp:Literal Text="<%$ Resources:MultiLang,File %>" runat="server" /></div>
                            <div><asp:FileUpload ID="fuConfirmation" runat="server" /></div>
                        </div>
                        <div class="align-left percent33">
                            <div><asp:Literal Text="<%$ Resources:MultiLang,Date %>" runat="server" /></div>
                            <div><netpay:DateRangePicker ID="wcDateRangePicker" Layout="Horizontal" CssClass="percent85" runat="server" IsToDateVisible="false" /></div>
                       </div>
                        <div class="align-left percent33">
                           <div><asp:Literal Text="<%$ Resources:MultiLang,Currency %>" runat="server" /></div>
                           <div><netpay:CurrencyDropDown ID="ddPaymentCurrency" EnableBlankSelection="false" runat="server"  CssClass="percent85" /></div>
                       </div>
                        <div class="align-left percent33">
                             <div><asp:Literal Text="<%$ Resources:MultiLang,Amount %>" runat="server" /></div>
                             <div><asp:TextBox ID="paymentAmount" runat="server"  CssClass="percent85" /></div>
                       </div>
                        <div class="spacer"></div>
                        <div class="margin-top-10"> 
                        <div><asp:Literal  Text="<%$ Resources:MultiLang,Comment %>" runat="server" /></div>
                        <div><asp:TextBox ID="paymentMerchantComment" runat="server" TextMode="MultiLine" CssClass="percent100 percent-height-10" /></div>
                        </div>     
                    </div>
                     <div class="frame-border-option-inline padding-10 margin-bottom-10 margin-top-10">
                           <div class="margin-top-10"><asp:Literal ID="litLimits" runat="server" /></div>
                     </div>
                    
                    <div class="alert-info"> 
                        <asp:Literal ID="litInsertText" runat="server" />
                        <asp:Literal ID="litNoBalance" runat="server" />
                        <asp:Literal ID="litOrderBeneficiary" runat="server" />
                        <asp:Literal Text="<%$ Resources:CompleteOrdersList %>" runat="server" />
                        <a class="btn" href="ViewPaymentsReport.aspx"><asp:Literal Text="<%$ Resources:PaymentsView %>" runat="server" /></a>
                    </div>
                     <div class="margin-top-10 text-align-right"><asp:Button ID="BtnSubmit" class="btn btn-success"  OnClick="BtnSubmit_OnClick" Text="<%$ Resources:SubmitOrder %>" runat="server" /></div>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
