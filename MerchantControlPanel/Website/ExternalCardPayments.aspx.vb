﻿Imports Netpay.Infrastructure.VO
Imports Netpay.Bll
Imports Netpay.Web
Imports Netpay.Infrastructure

Public Class ExternalCardPayments
    Inherits MasteredPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request("__EVENTTARGET") = "ctl00$cphBody$pager" Then
            GetPayments()
        End If

        If Not IsPostBack Then
            Dim provider As ExternalCardProviderEnum = CType(Request("providerID"), Integer)
            wcElementBlocker.Visible = Not Netpay.Bll.ExternalCards.IsProviderAvailable(LoggedUser.CredentialsToken, provider)

            If provider = ExternalCardProviderEnum.Payoneer Then
                txtTitle.Text = GetLocalResourceObject("titlePayoneer")
                GetBalance()
                phBalance.Visible = True
            ElseIf provider = ExternalCardProviderEnum.LiveKash Then
                txtTitle.Text = GetLocalResourceObject("titleLivekash")
                phBalance.Visible = False
            End If

            GetPayments()
        End If
    End Sub

    Private Sub GetBalance()
        Dim balance As Decimal
        Dim dueFees As Decimal
        If Netpay.Bll.ExternalCards.GetPayoneerBalance(LoggedUser.CredentialsToken, dueFees, balance) Then
            divBalance.InnerText = balance.ToAmountFormat(Netpay.Web.WebUtils.CurrentDomain.Host, Netpay.CommonTypes.Currency.USD)
            divDueFees.InnerText = dueFees.ToAmountFormat(Netpay.Web.WebUtils.CurrentDomain.Host, Netpay.CommonTypes.Currency.USD)
        End If
    End Sub

    Protected Sub GetPayments()
        Dim results As List(Of ExternalCardCustomerPaymentVO) = Netpay.Bll.ExternalCards.GetPayments(LoggedUser.CredentialsToken, GetFilters(), pager.Info)
        repeaterResults.DataSource = results
        repeaterResults.DataBind()

        If results IsNot Nothing Then
            phResults.Visible = (results.Count > 0)
        End If
    End Sub

    Protected Sub ShowPayment()
        Dim paymentID As Integer
        If Not Integer.TryParse(txtPaymentID.Text, paymentID) Then Exit Sub

        Dim result As ExternalCardCustomerPaymentVO = Netpay.Bll.ExternalCards.GetPayment(LoggedUser.CredentialsToken, paymentID)
        If result Is Nothing Then Exit Sub

        Dim results As New List(Of ExternalCardCustomerPaymentVO)()
        results.Add(result)
        repeaterResults.DataSource = results
        repeaterResults.DataBind()
        phResults.Visible = True
        pager.Visible = False
    End Sub

    Private Function GetFilters() As SearchFilters
        Dim filters As New SearchFilters()
        filters.dateFrom = wcDate.FromDate
        filters.dateTo = wcDate.ToDate
        filters.replyCode = txtReplyCodeFilter.Value.NullIfEmpty()
        filters.amountFrom = wcAmountSlider.MinValue
        filters.amountTo = wcAmountSlider.MaxValue
        filters.externalCardProvider = CType(Request("providerID"), Integer)
        Return filters
    End Function

    Protected Function GetCssClass(result As ExternalCardCustomerPaymentVO) As String
        If result.Result = "000" Then
            Return "payoneerNoError"
        Else
            Return "payoneerError"
        End If
    End Function
End Class