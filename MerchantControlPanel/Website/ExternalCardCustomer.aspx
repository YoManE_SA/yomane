﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Tmp_netpayintl/page.master" CodeBehind="ExternalCardCustomer.aspx.vb" Inherits="Netpay.MerchantControlPanel.ExternalCardCustomer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="server">
	<table width="100%">
		<tr>
			<td><h1><asp:Localize ID="txtTitle" Text="<%$Resources:title %>" runat="server" /></h1></td>
			<td style="text-align:<%= WebUtils.CurrentReverseAlignment %>; text-transform:uppercase;">
				<a id="linkBackToCustomers" runat="server"><asp:Literal ID="Literal3" Text="<%$Resources:BackToCustomers %>" runat="server" /></a>
			</td>		
		</tr>
	</table>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
	<script type="text/javascript">
		function confirmPayment()
		{
			return confirm("<asp:Literal Text="<%$Resources:PaymentConfirmation %>" runat="server"></asp:Literal>");
		}
	</script>
	
	<input type="hidden" value="<%= Request("customerID") %>" id="customerID" />
	<asp:HiddenField ID="hfCustomerID" runat="server" />
	<table border="0" width="92%" align="center">
		<tr>
			<td class="txt15"><asp:Literal ID="txtCustomerName" runat="server" /></td>
		</tr>
		<tr>
			<td>
				<asp:Label ID="lblSuccess" CssClass="noError" runat="server"></asp:Label>
			</td>
		</tr>
	</table>
	<br />
	<br />
	<div id="divBlocker">
		<table border="0" align="center" style="width:92%; background-color:#ffffff; border:1px solid silver;">
			<tr>
				<td style="background-color:#f5f5f5; width:60px; font-size:12px; padding:0px 8px; font-weight:bold; text-align:center;"><asp:Literal ID="ltAddingNew" runat="server" Text="<%$Resources:AddingPayout%>" /></td>
				<td valign="top">
					<asp:Label ID="lblMsg" runat="server"></asp:Label>
					<table border="0" cellspacing="0" cellpadding="1" width="30%" align="<%=WebUtils.CurrentAlignment%>" style="padding-<%=WebUtils.CurrentAlignment%>:10px;">
						<tr>
							<td><asp:Literal ID="Literal1" Text="<%$Resources:MultiLang,Amount %>" runat="server"></asp:Literal> <span style="color: maroon;">*</span></td>
						</tr>
						<tr>
							<td><input type="text" id="txtAmount" size="7" runat="server" /></td>
							<td>
								<asp:Button ID="btnPayToCustomer" OnClientClick="return confirmPayment()" Text="<%$Resources:Pay %>" CssClass="button" OnClick="PayToCustomer" runat="server" />
							</td>
						</tr>
					</table>	
				</td>
			</tr>
		</table>	
	</div>
	<netpay:ElementBlocker ID="wcElementBlocker" Visible="true" ElementClientID="divBlocker" Text="<%$ Resources:MultiLang,FeatureDisabled %>" runat="server" />
	<br /><br />
	<asp:PlaceHolder ID="phResults" Visible="false" runat="server">
		<table class="formNormal" align="center" cellpadding="1" cellspacing="1" style="width: 92%">
			<tr>
				<th>
					&nbsp;
				</th>
				<th>
					<asp:Literal Text="<%$Resources:Payout %>" runat="server"></asp:Literal>
				</th>
				<th>
					<asp:Literal ID="Literal2" Text="<%$Resources:MultiLang,Date %>" runat="server"></asp:Literal>
				</th>
				<th>
					<asp:Literal Text="<%$Resources:MultiLang,Amount %>" runat="server"></asp:Literal>
				</th>
				<th>
					<asp:Literal Text="<%$Resources:Result %>" runat="server"></asp:Literal>
				</th>
			</tr>					
			<asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server">
				<ItemTemplate>
					<tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
						<td width="8px" class="<%# GetCssClass(CType(Container.DataItem, ExternalCardCustomerPaymentVO))%>">
							&nbsp;
						</td>
						<td>						
							<%# CType(Container.DataItem, ExternalCardCustomerPaymentVO).ID%>
						</td>
						<td>						
							<%# CType(Container.DataItem, ExternalCardCustomerPaymentVO).InsertDate%>
						</td>
						<td>						
							$<%# CType(Container.DataItem, ExternalCardCustomerPaymentVO).Amount.ToAmountFormat()%>
						</td>
						<td>
							<%# CType(Container.DataItem, ExternalCardCustomerPaymentVO).Result%> - <%# CType(Container.DataItem, ExternalCardCustomerPaymentVO).ResultDescription%>
						</td>
					</tr>				
					<tr><td height="1" colspan="11" bgcolor="silver"></td></tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
		<table class="pager" align="center">
			<tr>			
				<td>				
					<netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
				</td>			
			</tr>
		</table>	
	</asp:PlaceHolder>
</asp:Content>
