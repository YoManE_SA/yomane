﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_ChangePassword" CodeBehind="ChangePassword.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="area-content">
        <div class="top-nav">
            <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:MultiLang,ChangePassword %>" />
        </div>
        <div class="content-background">
            <div class="section">
                <div class="margin-bottom-10 frame-border-option">
                    <div class="frame-border-option-inline padding-10 margin-bottom-10">

                        <div class="align-left percent32 margin-right-10">
                            <div>
                                <asp:Localize Text="<%$Resources:CurrentPassword%>" runat="server" />
                            </div>
                            <div>
                                <asp:TextBox ID="txtCurrentPassword" TextMode="Password" runat="server" CssClass="percent100" />
                            </div>
                        </div>
                        <div class="align-left percent32 margin-right-10">
                            <div>
                                <asp:Localize Text="<%$Resources:NewPassword%>" runat="server" />
                            </div>
                            <div>
                                <asp:TextBox ID="txtNewPassword" TextMode="Password" CssClass="percent100" runat="server" />
                            </div>
                        </div>
                        <div class="align-left percent32 margin-right-10">
                            <div>
                                <asp:Localize Text="<%$Resources:ConfirmNewPassword%>" runat="server" />
                            </div>
                            <div>
                                <asp:TextBox ID="txtNewPasswordConfirm" TextMode="Password" CssClass="percent100" runat="server" />
                            </div>
                        </div>
                        

                        <div class="spacer">
                        </div>

                        <div class="All-alerts">
                            <asp:Label ID="lblError" CssClass="error" Style="display: block;" Visible="false" runat="server" />
                            <asp:Label ID="lblSuccess" CssClass="noerror" Style="display: block;" Visible="false" runat="server" />
                        </div>

                    </div>
                    <div class="wrap-button">
                         
                                <asp:LinkButton ID="btnChangePassword" OnClick="btnChangePassword_OnClick" CssClass="btn btn-success btn-cons-short"  runat="server"><i class="fa fa-floppy-o"></i> <asp:Literal Text="<%$Resources:MultiLang, Save %>" runat="server" /> </asp:LinkButton>
                            
                        </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
