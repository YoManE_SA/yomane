﻿Imports Netpay.Web.Controls

Partial Class Website_RecurringChargeHistory
	Inherits MasteredPage

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Request("chargeID") = Nothing Then Return
		Dim chargeID As Integer = dbPages.TestVar(Request("chargeID"), 0, -1, 0)
        repeaterResults.DataSource = Netpay.Bll.Transactions.Recurring.ChargeHistory.Search(New Bll.Transactions.Recurring.ChargeHistory.SearchFilters() With {.ChargeID = chargeID}, Nothing)
		repeaterResults.DataBind()
	End Sub

    Protected Function GetTransactionDetailsUrl(transaction As Bll.Transactions.Transaction) As String
        Return String.Format("TransDetailCc.aspx?transID={0}&PaymentMethod={1}", transaction.ID, transaction.PaymentMethodID)
    End Function

	Sub SetPaymentMethodCountry(o As Object, e As RepeaterItemEventArgs)
		Dim pmv As PaymentMethodView = e.Item.FindControl("PaymentMethodView")
		If pmv IsNot Nothing Then
            Dim vo As Bll.Transactions.Recurring.ChargeHistory = e.Item.DataItem
			If vo.Transaction IsNot Nothing Then
                If vo.Transaction.PaymentData IsNot Nothing Then
                    pmv.BinCountryCode = vo.Transaction.PaymentData.IssuerCountryIsoCode
                End If
			End If
		End If
	End Sub
End Class
