﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_CCBlackList" CodeBehind="CCBlackList.aspx.vb" %>

<asp:Content ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ContentPlaceHolderID="cphBody" runat="Server">
    <script type="text/javascript">
        function deleteConfirmation()
        {
            if ($("[name='blockedItemID']:checked").length == 0)
                return false;
            
            return confirm("<asp:Literal Text="<%$Resources:DeleteConfirmation %>" runat="server"></asp:Literal>");
        }

        function toggleAll()
        {
            var toggleAllCheckbox = document.getElementById("cbToggleAll");
            var toggleCheckboxGroup = document.getElementsByName("blockedItemID");
            for (var i = 0; i < toggleCheckboxGroup.length; i++)
            {
                if (!toggleCheckboxGroup[i].disabled)
                    toggleCheckboxGroup[i].checked = toggleAllCheckbox.checked;
            }
        }
    </script>
    <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$Resources:PageTitle%>" runat="server" />
        </div>
        <div class="content-background">
            <div class="legend-menu">
                <div class="align-right">
                    <netpay:PopupButton ID="PopupButton1" IconSrc="Images/iconLegend.gif"
                        Text="<%$Resources:MultiLang,Legend  %>" runat="server">
                        <netpay:Legend ID="ttLegend" System="true" Merchant="true" runat="server" />
                    </netpay:PopupButton>
                </div>
            </div>
            <div class="section">
                <asp:PlaceHolder runat="server" ID="phButtonAdd">
                    <div class="margin-bottom-10 frame-border-option">
                        <div class="frame-border-option-inline padding-10">
                            <asp:LinkButton ID="btnToggleAddCard" CssClass="btn btn-success btn-cons-short" OnClick="ToggleAddCard" runat="server"><i class="fa fa-plus-circle"></i> <asp:Literal runat="server" Text="<%$Resources:AddCard%>" /></asp:LinkButton>
                        </div>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="phAddCardForm" Visible="false" runat="server">
                    <asp:Label ID="lblError" runat="server" Style="display: block;" />
                    <div class="margin-bottom-10 frame-border-option">
                        <div class="margin-bottom-10"><i class="fa fa-plus-circle"></i>
                            <asp:Literal ID="ltAddingNew" runat="server" Text="<%$Resources:AddingNew%>" /></div>
                        <div class="frame-border-option-inline padding-10 margin-bottom-10">

                            <div>
                                <asp:Literal Text="<%$Resources:MultiLang,CCNumber %>" runat="server" />*</div>
                            <div>
                                <netpay:CreditcardInput ID="ccNumber" CssClass="percent32" SingleFieldMode="true" runat="server" />
                            </div>

                            <div class="margin-top-10">
                                <asp:Literal Text="<%$Resources:MultiLang,ReasonAndNote %>" runat="server" /></div>
                            <div>
                                <textarea id="txtComment" type="text" class="percent100 percent-height-10" tabindex="7" runat="server" /></div>

                        </div>



                        <div class="align-left padding-5-side">
                        </div>
                    </div>
                    <div class="spacer"></div>
                    <div class="wrap-button-bar">
                        <asp:LinkButton ID="btnAddBlockedCard" CssClass="btn btn-cons-short btn-success" TabIndex="18" OnClick="AddBlockedCard" runat="server"><i class="fa fa-plus-circle"></i> <asp:Literal Text="<%$Resources:MultiLang,Add %>" runat="server" /></asp:LinkButton>
                    </div>
                </asp:PlaceHolder>
            </div>
            <div class="section">
                <div class="align-left percent33">
                    <div><asp:Literal Text="<%$Resources:MultiLang,Source %>" runat="server" /></div>
                    <div>
                        <asp:DropDownList ID="ddItemSourceFilter" runat="server" CssClass="percent85">
                            <asp:ListItem Text="" Value=""></asp:ListItem>
                            <asp:ListItem Text="<%$Resources:MultiLang,Merchant %>" Value="1" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="<%$Resources:MultiLang,System %>" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="align-left percent33">
                    <div><asp:Literal runat="server" Text="<%$Resources:MultiLang,Last4Digits  %>" /></div>
                    <div><asp:TextBox ID="txtFindLastDigit" CssClass="percent85" runat="server" MaxLength="4" /></div>
                </div>
                <div class="spacer"></div>
                <div class="wrap-button-bar">
                <asp:LinkButton ID="BtnSearch" runat="server" CssClass="btn btn-cons-short btn-inverse" OnClick="btnSearch_Click"><i class="fa fa-search"></i> <asp:Literal Text="<%$Resources:MultiLang,Search %>" runat="server" /></asp:LinkButton>
            </div>
            </div>
            
        </div>
        <asp:PlaceHolder ID="phResults" Visible="false" runat="server">
            <div class="section">
                <table class="exspand-table" align="center" cellpadding="1" cellspacing="1">
                    <tr>
                        <th style="vertical-align: middle;">&nbsp;
                        </th>
                        <th style="vertical-align: middle;">
                            <asp:Literal runat="server" Text="<%$Resources:MultiLang,Source %>"></asp:Literal>
                        </th>
                        <th style="vertical-align: middle;">
                            <asp:Literal runat="server" Text="<%$Resources:MultiLang,Type %>"></asp:Literal>
                        </th>
                        <th style="vertical-align: middle;">
                            <asp:Literal runat="server" Text="<%$Resources:MultiLang,CCNumber %>"></asp:Literal>
                        </th>
                        <th style="vertical-align: middle;">
                            <asp:Literal runat="server" Text="<%$Resources:MultiLang,ReasonAndNote %>"></asp:Literal>
                        </th>
                        <th style="vertical-align: middle; text-align: center !important;">
                            <input type="checkbox" id="cbToggleAll" onclick="toggleAll()" />
                        </th>
                    </tr>
                    <asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server">
                        <ItemTemplate>
                            <tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
                                <%# GetLegend(CType(Container.DataItem, Risk.RiskItem)) %>
                                <td>
                                    <netpay:PaymentMethodView ID="PaymentMethodView1" MethodData='<%# Netpay.Bll.PaymentMethods.CreditCard.FromCardNumber(CType(Container.DataItem, Risk.RiskItem).Value) %>' runat="server" />
                                </td>
                                <td>
                                    <%# CType(Container.DataItem, Risk.RiskItem).Display %>
                                </td>
                                <td>
                                    <%# GetComment(CType(Container.DataItem, Risk.RiskItem))%>
                                </td>
                                <td style="text-align: center;">
                                    <%# GetCheckbox(CType(Container.DataItem, Risk.RiskItem)) %>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
                <br />
                <div class="align-left">
                    <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-primary" OnClientClick="return deleteConfirmation()"
                        OnClick="DeleteSelectedCards" Text="<%$Resources:MultiLang,Delete %>" />
                </div>
                <div class="spacer">
                </div>
                <netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
            </div>
        </asp:PlaceHolder>
    </div>
    </div>
</asp:Content>
