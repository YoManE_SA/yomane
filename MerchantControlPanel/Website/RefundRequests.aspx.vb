﻿Imports Netpay.Bll
Imports Netpay.Infrastructure
Imports Netpay.Web.Controls
Imports Netpay.Web

Public Class Website_RefundRequests
	Inherits MasteredPage

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		lvRefundRequests.Remove(Bll.Transactions.RefundRequest.RequestStatus.Batched)
		lvRefundRequests.Remove(Bll.Transactions.RefundRequest.RequestStatus.Processed)
		lvRefundRequests.Remove(Bll.Transactions.RefundRequest.RequestStatus.SourceCancel)
		If Not IsPostBack Then
			wcDateRangePicker.FromDate = DateTime.Now.AddMonths(-1)
			wcDateRangePicker.ToDate = DateTime.Now
		End If
	End Sub

	Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
		Pager.CurrentPage = 0
		OnPageChanged(sender, e)
		Pager.Visible = True
	End Sub

	Protected Sub btnIDSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
		Dim id As Nullable(Of Integer) = txtSearchID.Text.ToNullableInt32()
		If id Is Nothing Then
			phResults.Visible = False
			Return
		End If

        Dim filters = New Bll.Transactions.RefundRequest.SearchFilters()
		If cbNumType.SelectedIndex = 0 Then filters.ID = Infrastructure.Range(Of Integer?).FromValue(id) Else filters.TransactionID = Infrastructure.Range(Of Integer?).FromValue(id)
		Pager.CurrentPage = 0
        repeaterResults.DataSource = Bll.Transactions.RefundRequest.Search(filters, pager.Info)
		repeaterResults.DataBind()
		phResults.Visible = pager.Info.RowCount > 0
	End Sub

	Protected Sub ExportExcel_Click(ByVal sender As Object, ByVal e As ExcelButton.ExportEventArgs)
		OnPageChanged(sender, e)
		btnExportExcel.Export()
	End Sub

 	Protected Sub OnPageChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim filters = New Bll.Transactions.RefundRequest.SearchFilters()
		filters.Date.From = wcDateRangePicker.FromDate
		filters.Date.To = wcDateRangePicker.ToDate
		If cbStatus.SelectedIndex > 0 Then
            filters.Status = New List(Of Bll.Transactions.RefundRequest.RequestStatus)
			Dim rrs As Bll.Transactions.RefundRequest.RequestStatus = cbStatus.SelectedValue
			If rrs = Bll.Transactions.RefundRequest.RequestStatus.SourceCancel Then
				filters.Status.Add(Bll.Transactions.RefundRequest.RequestStatus.SourceCancel)
			ElseIf rrs = Bll.Transactions.RefundRequest.RequestStatus.Processed Then
                filters.Status.Add(Bll.Transactions.RefundRequest.RequestStatus.Created)
                filters.Status.Add(Bll.Transactions.RefundRequest.RequestStatus.Batched)
			End If
			filters.Status.Add(rrs)
		End If

		If TypeOf e Is ExcelButton.ExportEventArgs Then
            btnExportExcel.SetExportInfo("Refund Requests", Reports.ReportType.MerchantRefundRequests, Function(pi) Bll.Transactions.RefundRequest.Search(filters, pi))
		Else
            repeaterResults.DataSource = Bll.Transactions.RefundRequest.Search(filters, pager.Info)
			repeaterResults.DataBind()
			btnExportExcel.ResultCount = Pager.Info.RowCount
		End If
		phResults.Visible = Pager.Info.RowCount > 0
	End Sub

    Protected Function GetStatusStyle(ByVal entity As Bll.Transactions.RefundRequest) As String
        If entity.Status = Bll.Transactions.RefundRequest.RequestStatus.Batched Or entity.Status = Bll.Transactions.RefundRequest.RequestStatus.InProgress Then
            Return lvRefundRequests.GetItemClass(Bll.Transactions.RefundRequest.RequestStatus.InProgress)
        ElseIf entity.Status = Bll.Transactions.RefundRequest.RequestStatus.SourceCancel Then
            Return lvRefundRequests.GetItemClass(Bll.Transactions.RefundRequest.RequestStatus.SourceCancel)
        Else
            Return lvRefundRequests.GetItemClass(entity.Status)
        End If
    End Function

	Protected Function GetRefundStatusText(ByVal RefundStatusID As Bll.Transactions.RefundRequest.RequestStatus) As String
		Return RefundStatusID.EnumText("Enums")
	End Function

	Protected Sub CancelRefundRequest(ByVal sender As Object, ByVal e As CommandEventArgs)
		Dim id As Integer = Integer.Parse(e.CommandArgument)
        Dim req = Bll.Transactions.RefundRequest.Load(id)
		If req.CancelRequest() Then
			Dim btnDelete As LinkButton = CType(sender, LinkButton)
			CType(btnDelete.Parent.FindControl("ltStatusText"), Literal).Text = GetRefundStatusText( Bll.Transactions.RefundRequest.RequestStatus.SourceCancel)
			btnDelete.Visible = False
		End If
	End Sub
	
End Class