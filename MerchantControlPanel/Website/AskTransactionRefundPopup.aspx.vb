﻿Imports Netpay.Infrastructure
Imports Netpay.Web.Controls

Partial Class Website_AskTransactionRefundPopup
    Inherits PopupPage
	
	Sub Page_Load() Handles Me.Load
		dsData.ConnectionString = dbPages.DSN()
        litCSS.Text = "<link href=""" & MapTemplateVirPath("style.css") & """ rel=""stylesheet"" type=""text/css"" />"

		If Not Page.IsPostBack Then
			Dim nID As Integer = dbPages.TestVar(Request("TransID"), 1, 0, 0)
			Page.Title &= nID
			Dim nAmount As Decimal = dbPages.ExecScalar("SELECT dbo.GetRefundAmountAvailableMerchant(" & nID & ", " & Merchant.ID & ")")
			txtNewAmount.Text = Decimal.Round(nAmount, 2)
			If nAmount <= 0D Then mvNewRequest.ActiveViewIndex = 3
			txtNewAmount.ReadOnly = Not dbPages.ExecScalar("SELECT dc_isAllowPartialRefund FROM tblDebitCompany WHERE DebitCompany_ID=(SELECT DebitCompanyID FROM tblCompanyTransPass WHERE ID=" & nID & ")")
		End If
	End Sub

	Sub ResetView(ByVal o As Object, ByVal e As EventArgs)
		mvNewRequest.ActiveViewIndex = 0
	End Sub

	Sub AddNewRequest(ByVal o As Object, ByVal e As EventArgs)
		Dim nID As Integer = dbPages.TestVar(Request("TransID"), 1, 0, 0)
		If nID = 0 Then
			Response.Clear()
			Response.Write("<html><head></head><body><s" & "cript language=""javascript"" type=""text/javascript"">top.close();</s" & "cript></body></html>")
			Response.End()
		End If
		Dim oMerchant As Object = dbPages.ExecScalar("SELECT IsNull(CompanyID, 0) FROM tblCompanyTransPass WHERE ID=" & nID)
		If Merchant.ID <> IIf(TypeOf (oMerchant) Is DBNull, 0, CType(oMerchant, Integer)) Then
			Response.Clear()
			Response.Write("<html><head></head><body><s" & "cript language=""javascript"" type=""text/javascript"">top.close();</s" & "cript></body></html>")
			Response.End()
		End If
		Dim nCurrency As Integer = dbPages.ExecScalar("SELECT Currency FROM tblCompanyTransPass WHERE ID=" & nID)
		Dim nAmount As Decimal = txtNewAmount.Text
		Dim nAmountMax As Decimal = dbPages.ExecScalar("SELECT dbo.GetRefundAmountAvailableMerchant(" & nID & ", " & Merchant.ID & ")")
		If dbPages.TestVar(nAmount, 0D, nAmountMax, 0D) > 0D Then
			Dim sComment As String = txtNewComment.Text.ToSql(True)
			Dim nRefund As Integer = dbPages.ExecScalar("EXEC CreateRefundRequest " & Merchant.ID & ", " & nID & ", " & nAmount & ", " & nCurrency & ", '" & sComment & "'")
			mvNewRequest.ActiveViewIndex = IIf(nRefund > 0, 1, 2)
			gvData.DataBind()
		Else
			txtNewAmount.Text = Decimal.Round(nAmountMax, 2)
			mvNewRequest.ActiveViewIndex = 2
		End If
	End Sub
End Class
