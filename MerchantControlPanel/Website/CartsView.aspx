﻿<%@ Page Title="PageTitle" Language="VB" MasterPageFile="~/Tmp_netpayintl/page.master" AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_CartsView" Codebehind="CartsView.aspx.vb" %>
<%@ Import Namespace="Netpay.Web" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphTitle" Runat="Server">
	<table width="100%">
		<tr>
			<td><h1><asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" /></h1></td>
			<td style="text-align:<%= WebUtils.CurrentReverseAlignment %>">
				<netpay:ExcelButton ID="btnExportExcel" Text="<%$Resources:MultiLang,ExportToXls %>" Enabled="false" runat="server" OnClick="ExportExcel_Click" />&nbsp;&nbsp;|&nbsp;&nbsp;
				<netpay:PopupButton ID="PopupButton1" IconSrc="../Tmp_netpayintl/Images/iconLegend.gif" Width="100" Height="150" Text="<%$Resources:MultiLang,Legend  %>" runat="server" >
					<netpay:LegendView runat="server" ID="ttLegend" ViewEnumName="Netpay.Infrastructure.CartStatus" />
				</netpay:PopupButton>
			</td>
		</tr>
	</table>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphBody" Runat="Server">
	<asp:HiddenField ID="hdLastSerachMode" runat="server" />



       <div class="area-content">
        <div class="top-nav">
            <asp:Localize ID="Localize1" Text="<%$Resources:Contactinfo%>" runat="server" /></div>
        <div class="content-background">
            <div class="section">
                <asp:Label ID="lblContent" runat="server" />
            </div>
        </div>
    </div>



	<table border="0" align="center" style="width:92%; background-color:#f5f5f5; border:1px solid silver;" id="tblSearchForm">
		<tr>
			<td>
				<table border="0" cellspacing="0" cellpadding="1" width="98%" align="center">					
					<tr>
						<td colspan="2"><asp:Literal runat="server" Text="<%$Resources:MultiLang,InsertDates %>" /></td>
						<td><asp:Literal runat="server" Text="<%$Resources:MultiLang,Amount %>" /></td>
						<td style="padding-<%=WebUtils.CurrentReverseAlignment %>:14px;"><asp:Literal runat="server" Text="<%$Resources:MultiLang,Status %>" /></td>																				
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2">
							<netpay:DateRangePicker ID="wcDateRangePicker" Layout="Horizontal" runat="server" />
						</td>
						<td><asp:TextBox ID="txAmountFrom" runat="server" Width="60px" /> To <asp:TextBox ID="txAmountTo" runat="server" Width="60px" /></td>
						<td style="padding-<%=WebUtils.CurrentReverseAlignment%>:14px;"><netpay:EnumDropDown runat="server" ID="ddCartStatus" ViewEnumName="Netpay.Infrastructure.CartStatus" EnableBlankSelection="true" /></td>
						<td><asp:Button ID="BtnSearch" runat="server" CssClass="button" Text="<%$Resources:MultiLang,Search %>" OnClick="btnSearch_Click" /></td>
					</tr>
				</table>
			</td>
			<td rowspan="2" style="border-<%= WebUtils.CurrentAlignment %>:1px dotted silver; padding-<%= WebUtils.CurrentAlignment %>:14px;">
				<table border="0" cellspacing="0" cellpadding="1" width="98%" align="center">
					<tr><td><asp:Literal  runat="server" Text="<%$Resources:MultiLang,Cart %>" /></td></tr>
					<tr>
						<td><asp:TextBox ID="txCart" runat="server" Font-Size="11px" CssClass="input" Width="70px" /></td>
						<td><asp:Button ID="btnShow" runat="server" CssClass="button" Text="<%$Resources:MultiLang,Show %>" OnClick="btnSearch_Click" /></td>
					</tr>
				</table>							
			</td>	
		</tr>
	</table>
	<br /><br />
	<asp:PlaceHolder ID="phResults" Visible="false" runat="server">
		<table class="formNormal" align="center" style="width: 92%" cellpadding="1" cellspacing="1">
			<tr>
				<th><asp:Literal ID="Literal1" Text="<%$Resources:MultiLang,Cart %>" runat="server"></asp:Literal></th>
				<th><asp:Literal ID="Literal2" Text="<%$Resources:MultiLang,Date %>" runat="server"></asp:Literal></th>
				<th><asp:Literal ID="Literal3" Text="<%$Resources:MultiLang,TotalProducts %>" runat="server"></asp:Literal></th>
				<th><asp:Literal ID="Literal4" Text="<%$Resources:MultiLang,TotalShipping %>" runat="server"></asp:Literal></th>			
				<th><asp:Literal ID="Literal5" Text="<%$Resources:MultiLang,Total %>" runat="server"></asp:Literal></th>			
			</tr>	
			<asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server">
				<ItemTemplate>
					<tr>
						<td><a href="CartView.aspx?cartID=<%# CType(Container.DataItem, CartVO).ID%>"><%# CType(Container.DataItem, CartVO).ReferenceNumber%></a></td>
						<td><%# CType(Container.DataItem, CartVO).StartDate%></td>
						<td><%# CType(Container.DataItem, CartVO).TotalProducts.ToAmountFormat(WebUtils.CurrentDomain.Host, CType(Container.DataItem, CartVO).Currency)%></td>
						<td><%# CType(Container.DataItem, CartVO).TotalShipping.ToAmountFormat(WebUtils.CurrentDomain.Host, CType(Container.DataItem, CartVO).Currency)%></td>
						<td><%# CType(Container.DataItem, CartVO).Total.ToAmountFormat(WebUtils.CurrentDomain.Host, CType(Container.DataItem, CartVO).Currency)%></td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
		<table class="pager" align="center">
			<tr><td><netpay:LinkPager ID="pager" runat="server" /></td></tr>
		</table>	
	</asp:PlaceHolder>
</asp:Content>
