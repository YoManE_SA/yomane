﻿Imports System.IO
Imports System.Collections.Generic
Imports Netpay.Infrastructure.Tasks
Imports Netpay.Bll.Reports
Imports Netpay.Bll
Imports Netpay.Web
Imports Netpay.Infrastructure
Imports Netpay.Web.Controls
Imports Netpay.CommonTypes

Partial Class Website_Reports
	Inherits MasteredPage

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			GetGeneratedReportList()
			GetQueuedReportList()
		End If
	End Sub

	Protected Sub GetGeneratedReportList()
		Dim generatedReports As List(Of GeneratedReportInfo) = FiledReports.GetGeneratedReports()
		repeaterGeneratedReports.DataSource = generatedReports
		repeaterGeneratedReports.DataBind()

		phGeneratedReports.Visible = generatedReports.Count > 0
	End Sub

	Protected Sub GetQueuedReportList()
		Dim queuedReports As List(Of TaskInfo) = FiledReports.GetQueuedReports()
        repeaterQueuedReports.DataSource = queuedReports
		repeaterQueuedReports.DataBind()

		phQueuedTasks.Visible = queuedReports.Count > 0
	End Sub

	Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Dim filters = New Bll.Transactions.Transaction.SearchFilters()
		If wcMonthlyReportDate.FromDate Is Nothing Then Exit Sub
		If wcMonthlyReportDate.ToDate Is Nothing Then Exit Sub
		filters.date.From = wcMonthlyReportDate.FromDate.Value.MinTime
		filters.date.To = wcMonthlyReportDate.ToDate.Value.MaxTime
		If wcMonthlyReportDate.IsOutOfRange Then Exit Sub

		Dim args As List(Of Object) = New List(Of Object)()

        If ddlReportType.SelectedItem Is Nothing Then Exit Sub

        Dim description As String = "Queued task"
		Dim methodName As String = ddlReportType.SelectedValue
		Dim isOverLimit As Boolean = False
        If methodName = "CreateMerchantCapturedTransactionsReport" Then
            description = "Approved_Transactions_" + filters.date.From.Value.ToString("dd-MM-yyyy") + "_" + filters.date.To.Value.ToString("dd-MM-yyyy")
            filters.isChargeback = False
            isOverLimit = FiledReports.IsOverLimit(Function(pagingInfo As ISortAndPage) Bll.Transactions.Transaction.Search(TransactionStatus.Captured, filters, pagingInfo, True, True))
			args.Add(filters)
        ElseIf methodName = "CreateMerchantDeclinedTransactionsReport" Then
            description = "Declined_Transactions_" + filters.date.From.Value.ToString("dd-MM-yyyy") + "_" + filters.date.To.Value.ToString("dd-MM-yyyy")
            filters.isChargeback = False
            isOverLimit = FiledReports.IsOverLimit(Function(pagingInfo As ISortAndPage) Bll.Transactions.Transaction.Search(TransactionStatus.Declined, filters, pagingInfo, True, True))
			args.Add(filters)
        ElseIf methodName = "CreateMerchantChargebackTransactionsReport" Then
            Dim sf As New Bll.Transactions.History.SearchFilters
			sf.Date.From = filters.date.From : sf.Date.To = filters.date.To
            description = "Chargeback_Transactions_" + filters.date.From.Value.ToString("dd-MM-yyyy") + "_" + filters.date.To.Value.ToString("dd-MM-yyyy")
            'filters.isChargeback = True
            isOverLimit = False
			args.Add(sf)
        ElseIf methodName = "CreateMerchantSummaryReport" Then
            description = "Processing_Summary_" + filters.date.From.Value.ToString("dd-MM-yyyy") + "_" + filters.date.To.Value.ToString("dd-MM-yyyy")
			args.Add(filters)
        ElseIf methodName = "CreateMerchantSettlementReport" Then
            description = "Settlement_Report_" + filters.date.From.Value.ToString("dd-MM-yyyy") + "_" + filters.date.To.Value.ToString("dd-MM-yyyy")
			args.Add(filters)
        ElseIf methodName = "CreateMerchantCustomerTransactionsReport" Then
            description = "Customers_Transactions_" + filters.date.From.Value.ToString("dd-MM-yyyy") + "_" + filters.date.To.Value.ToString("dd-MM-yyyy")
			args.Add(filters)
        Else
            Exit Sub
        End If

		txtError.Visible = False
		If isOverLimit Then
            txtError.InnerText = String.Format(GetLocalResourceObject("FiledOverLimit"), Infrastructure.Application.MaxFiledReportSize)
			txtError.Visible = True
			Exit Sub
		End If

        Dim task As Task = New Task(LoggedUser, task.GetUserGroupID(LoggedUser), description, LogTag.Reports, "Netpay.Bll", GetType(FiledReports).ToString(), methodName, args)
		If Not Netpay.Infrastructure.Application.Queue.IsTaskQueued(task) Then Netpay.Infrastructure.Application.Queue.Enqueue(task)
		GetQueuedReportList()
	End Sub

	Protected Sub btnDownloadFile_click(ByVal source As Object, ByVal e As CommandEventArgs)
		Dim parameters As String() = e.CommandArgument.ToString().Split("|")
		Dim fileType As ReportType = [Enum].Parse(GetType(ReportType), parameters(1))
		Dim fileName As String = parameters(0)
		Response.ClearHeaders()
		Response.Clear()
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName)
		Response.Cache.SetCacheability(HttpCacheability.Private)
		Response.TransmitFile(FiledReports.GetReportFullName(fileType, fileName))
		Response.End()
	End Sub

	Protected Sub btnDeleteFile_click(ByVal source As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
		Dim parameters As String() = e.CommandArgument.ToString().Split("|")
		Dim fileType As ReportType = [Enum].Parse(GetType(ReportType), parameters(1))
		Dim fileName As String = parameters(0)

		FiledReports.DeleteReport(fileType, fileName)
		GetGeneratedReportList()
	End Sub

	Protected Sub btnCancelReport_click(ByVal source As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
		Dim taskID As Guid = New Guid(e.CommandArgument.ToString())
		FiledReports.CancelReport(taskID)
		GetQueuedReportList()
	End Sub

	Protected Sub btnRefresh_click(ByVal source As Object, ByVal e As EventArgs) Handles btnRefreshFiles.Click
		GetGeneratedReportList()
		GetQueuedReportList()
	End Sub
End Class
