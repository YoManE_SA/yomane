﻿Imports Netpay.Infrastructure
Imports Netpay.Bll

Partial Class Website_RecurringCharges
	Inherits MasteredPage

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Populate()
	End Sub

	Protected Sub Populate()
		Dim seriesID As Integer = Request("seriesID").ToInt32(0)
        repeaterResults.DataSource = Bll.Transactions.Recurring.Charge.Search(New Bll.Transactions.Recurring.Charge.SearchFilters() With {.SeriesId = seriesID}, Nothing)
		repeaterResults.DataBind()
	End Sub

    Protected Function GetStatusClass(series As Bll.Transactions.Recurring.Series) As String
        If series.Paid Then
            Return "statusPaid"
        ElseIf series.Blocked Then
            Return "statusBlocked"
        ElseIf series.Suspended Then
            Return "statusSuspended"
        Else
            Return "statusActive"
        End If
    End Function

	Protected Function GetTypeText(isPreAuthorized As Boolean) As String
		If isPreAuthorized Then
			Return GetGlobalResourceObject("MultiLang", "PreAuth")
		Else
			Return GetGlobalResourceObject("MultiLang", "Debit")
		End If
	End Function

    Protected Function GetEditUrl(charge As Bll.Transactions.Recurring.Charge) As String
        Return "RecurringEditCharge.aspx?charge=" + charge.ID.ToString()
    End Function

    Protected Function GetStatusClass(charge As Bll.Transactions.Recurring.Charge) As String
        If charge.Paid Then
            Return "statusPaid"
        ElseIf charge.Blocked Then
            Return "statusBlocked"
        ElseIf charge.Suspended Then
            Return "statusSuspended"
        Else
            Return "statusActive"
        End If
    End Function

	Protected Sub SuspendCharge(ByVal sender As Object, ByVal e As CommandEventArgs)
		Dim chargeID As Integer = Integer.Parse(e.CommandArgument)
        Bll.Transactions.Recurring.Charge.Load(chargeID).Suspend()
		Populate()
	End Sub

	Protected Sub ResumeCharge(ByVal sender As Object, ByVal e As CommandEventArgs)
		Dim chargeID As Integer = Integer.Parse(e.CommandArgument)
        Bll.Transactions.Recurring.Charge.Load(chargeID).Resume()
		Populate()
	End Sub
End Class
