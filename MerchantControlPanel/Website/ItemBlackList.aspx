﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Templates/Tmp_netpayintl/page.master" MaintainScrollPositionOnPostback="true"
    AutoEventWireup="false" Inherits="Netpay.MerchantControlPanel.Website_ItemBlackList"
    CodeBehind="ItemBlackList.aspx.vb" %>

<asp:Content ContentPlaceHolderID="cphTitle" runat="Server">
    <asp:Localize ID="Localize1" Text="<%$Resources:PageTitle%>" runat="server" />
</asp:Content>
<asp:Content ContentPlaceHolderID="cphBody" runat="Server">
    <script type="text/javascript">
        function deleteConfirmation()
        {
            return confirm("<asp:Literal Text="<%$Resources:DeleteConfirmation %>" runat="server"></asp:Literal>");
        }

        function toggleAll() 
        {
            var toggleAllCheckbox = document.getElementById("cbToggleAll");
            var toggleCheckboxGroup = document.getElementsByName("blockedItemID");
            for (var i = 0; i < toggleCheckboxGroup.length; i++) 
            {
                if (!toggleCheckboxGroup[i].disabled)
                    toggleCheckboxGroup[i].checked = toggleAllCheckbox.checked;
            }
        }
    </script>

    <asp:UpdatePanel ChildrenAsTriggers="true" EnableViewState="true" RenderMode="Block" UpdateMode="Always" runat="server">
        <ContentTemplate>
            <div class="area-content">
                <div class="top-nav">
                    <asp:Localize ID="locPageTitle" Text="<%$Resources:PageTitle%>" runat="server" />
                </div>
                <div class="content-background">
                    <div class="legend-menu">
                        <div class="align-right">
                            <netpay:PopupButton ID="PopupButton1" IconSrc="Images/iconLegend.gif"
                                Text="<%$Resources:MultiLang,Legend  %>" runat="server">
                                <netpay:Legend ID="ttLegend" System="true" Merchant="true" runat="server" />
                            </netpay:PopupButton>
                        </div>
                    </div>

                    <div class="section">
                        <div class="align-left percent23 margin-right-10 ">
                            <div>
                                <asp:Literal runat="server" Text="<%$Resources:MultiLang, Source %>" />
                            </div>
                            <div>
                                <asp:DropDownList ID="ddItemSourceFilter" runat="server" CssClass="percent100">
                                    <asp:ListItem Text="" Value=""></asp:ListItem>
                                    <asp:ListItem Text="<%$Resources:MultiLang,Merchant %>" Value="1" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="<%$Resources:MultiLang,System %>" Value="0"></asp:ListItem>
                                </asp:DropDownList>

                            </div>
                        </div>
                        <div class="align-left percent23 margin-right-10 ">
                            <div>
                                <asp:Literal runat="server" Text="Item Type" />
                            </div>
                            <div>
                                <asp:DropDownList ID="ddItemTypeFilter" runat="server" CssClass="percent100">
                                    <asp:ListItem Text="" Value=""></asp:ListItem>
                                    <asp:ListItem Text="<%$Resources:Email %>" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="<%$Resources:FullName %>" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="<%$Resources:Phone %>" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="<%$Resources:PersonalNumber %>" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="<%$Resources:Bin %>" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="<%$Resources:BinCountry %>" Value="6"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="align-left percent23 margin-right-10 ">
                            <div>
                                <asp:Literal runat="server" Text="<%$Resources:ItemValue %>" />
                            </div>
                            <div>
                                <asp:TextBox ID="txtItemValueFilter" runat="server" CssClass="percent100" />
                            </div>
                        </div>
                        <div class="spacer"></div>

                        <div class="margin-top-10">
                            <div>
                                <asp:Literal ID="Literal122" runat="server" Text="<%$Resources:MultiLang, Comment %>" />
                            </div>
                            <asp:TextBox ID="txtItemCommentFilter" runat="server" CssClass="percent100" />
                        </div>

                        <div class="wrap-button-bar">
                            <asp:LinkButton ID="btnSearch" OnClick="btnSearch_Click" Text="<%$Resources:MultiLang,Search %>" CssClass="btn btn-cons-short btn-inverse" runat="server" />
                        </div>
                    </div>
                    <asp:PlaceHolder ID="phResults" Visible="false" runat="server">
                        <div class="section">
                            <asp:PlaceHolder ID="phAddItemLink" Visible="true" runat="server">
                                <div class="margin-bottom-10">
                                    <asp:LinkButton ID="btnToggleAddCard" OnClick="ToggleAddItem" CssClass="btn btn-default" TabIndex="18" runat="server"><i class="fa fa-plus-circle"></i> <asp:Literal runat="server" Text="<%$Resources: AddItem %>" /></asp:LinkButton>
                                </div>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder ID="phAddItemForm" Visible="false" runat="server">
                                <div class="margin-bottom-10 frame-border-option">
                                    <div class="frame-border-option-inline padding-10">
                                        <div class="percent23 align-left margin-right-10">
                                            <div>
                                                <asp:Literal runat="server" Text="<%$Resources:ItemType %>" />
                                            </div>
                                            <div>
                                                <asp:DropDownList ID="ddItemType" CssClass="percent100" runat="server">

                                                    <asp:ListItem Text="<%$Resources:Email %>" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="<%$Resources:FullName %>" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="<%$Resources:Phone %>" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="<%$Resources:PersonalNumber %>" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="<%$Resources:Bin %>" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="<%$Resources:BinCountry %>" Value="6"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="percent23 align-left">
                                            <div>
                                                <asp:Literal runat="server" Text="<%$Resources:ItemValue %>" />
                                            </div>
                                            <div>
                                                <input id="txtItemValue" type="text" class="percent100" tabindex="7" maxlength="500" runat="server" title="<%$Resources:ItemValue %>" />
                                            </div>
                                        </div>
                                        <div class="spacer"></div>
                                        <div class="margin-top-10">
                                            <div>
                                                <asp:Literal runat="server" Text="<%$Resources:MultiLang,Comment %>" />
                                            </div>
                                            <div>
                                                <input id="txtComment" type="text" class="percent100" tabindex="8" maxlength="50" runat="server" title="<%$Resources:MultiLang,Comment %>" />
                                            </div>

                                        </div>
                                        <asp:Label ID="lblError" runat="server" />
                                        <div class="text-align-right margin-top-10">
                                            <asp:LinkButton ID="btnAddBlockedItem" CssClass="btn btn-cons-short btn-inverse" TabIndex="18" OnClick="AddBlockedItem" runat="server"><i class="fa fa-plus-circle"></i> <asp:Literal runat="server" Text="<%$Resources:MultiLang,Add %>" /> </asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder ID="phItemAddedBlack" runat="server" Visible="false">
                                <div class="noerror margin-bottom-10 margin-top-10">
                                    <asp:Literal runat="server" Text="<%$Resources:ItemAdded %>" />
                                </div>
                            </asp:PlaceHolder>
                            <table class="exspand-table margin-top-10">
                                <tr>
                                    <th style="vertical-align: middle;">&nbsp;
                                    </th>
                                    <th style="vertical-align: middle;">
                                        <asp:Literal ID="Literal5" runat="server" Text="<%$Resources:MultiLang,Source %>"></asp:Literal>
                                    </th>
                                    <th style="vertical-align: middle;">
                                        <asp:Literal ID="Literal6" runat="server" Text="<%$Resources:ItemType %>"></asp:Literal>
                                    </th>
                                    <th style="vertical-align: middle;">
                                        <asp:Literal ID="Literal7" runat="server" Text="<%$Resources:ItemValue %>"></asp:Literal>
                                    </th>
                                    <th style="vertical-align: middle;">
                                        <asp:Literal ID="Literal8" runat="server" Text="<%$Resources:MultiLang,Comment %>"></asp:Literal>
                                    </th>
                                    <th style="vertical-align: middle; text-align: center !important;">
                                        <input type="checkbox" id="cbToggleAll" onclick="toggleAll()" />
                                    </th>
                                </tr>
                                <asp:Repeater ID="repeaterResults" EnableViewState="false" runat="server">
                                    <ItemTemplate>
                                        <tr onmouseover="this.style.backgroundColor='#d8d8d8';" onmouseout="this.style.backgroundColor='';">
                                            <%# GetLegend(CType(Container.DataItem, Risk.RiskItem)) %>
                                            <td>
                                                <%# GetLocalResourceObject(CType(Container.DataItem, Risk.RiskItem).ValueType.ToString()) %>
                                            </td>
                                            <td>
                                                <%# CType(Container.DataItem, Risk.RiskItem).Value %>
                                            </td>
                                            <td>
                                                <%# GetComment(CType(Container.DataItem, Risk.RiskItem)) %>
                                            </td>
                                            <td style="text-align: center;">
                                                <%# GetCheckbox(CType(Container.DataItem, Risk.RiskItem))%>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <tr>
                                    <td colspan="6" align="<%=WebUtils.CurrentReverseAlignment%>" style="border: 0px;">
                                        <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger" OnClientClick="return deleteConfirmation()"
                                            OnClick="DeleteSelectedItems" Text="<%$Resources:MultiLang,Delete %>" />
                                    </td>
                                </tr>
                            </table>
                            <netpay:LinkPager ID="pager" runat="server" OnPageChanged="OnPageChanged" />
                        </div>
                    </asp:PlaceHolder>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>




</asp:Content>
