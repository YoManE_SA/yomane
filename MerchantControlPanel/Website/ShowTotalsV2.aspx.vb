﻿Imports Netpay.Web
Imports Netpay.Web.Controls

Partial Class Website_ShowTotalsV2
	Inherits PopupPage

	Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
		Dim cssTag As String = String.Format("<link rel=""stylesheet"" type=""text/css"" href=""../{0}/style.css"" />", WebUtils.CurrentDomain.ThemeFolder)
		Dim cssLangTag As String = String.Format("<link rel=""stylesheet"" type=""text/css"" href=""../{0}/style" & Netpay.Web.WebUtils.CurrentLanguageShort & ".css"" />", WebUtils.CurrentDomain.ThemeFolder)
		Page.Header.Controls.Add(New LiteralControl(cssTag))
		Page.Header.Controls.Add(New LiteralControl(cssLangTag))
		litCloseWindow.Text = "<a href=""javascript:window.close();"">" & GetGlobalResourceObject("Multilang", "CloseWindow") & "</a>"
		MyBase.OnInit(e)
	End Sub

	Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
		ShowTotalsButton.LoadTotalsParams(Total1)
	End Sub
End Class
