﻿Imports Netpay.Web
Imports Netpay.Infrastructure

Public Class Login
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then DataBindChildren()
        If Not IsPostBack And Request.QueryString("login") = "outer" Then
            Dim email As String = Request("mail").Trim()
            Dim username As String = Request("username").Trim()
            Dim password As String = Request("userpassword").Trim()

            loginEmail.Text = Request.QueryString("Mail")
            loginUsername.Text = Request.QueryString("Username")
            loginPassword.TextMode = TextBoxMode.SingleLine
            loginPassword.Visible = False
            loginPassword.Text = Request.QueryString("UserPassword")
        End If
    End Sub

    Protected Sub btnLogin_Click(ByVal o As Object, ByVal e As System.EventArgs)
        divError.Visible = False

        If loginEmail.Text.Trim() = String.Empty Then
            literalError.Text = GetGlobalResourceObject("Login.aspx", "EmailAlert")
            divError.Visible = True
            Exit Sub
        End If

        If loginUsername.Text.Trim() = String.Empty Then
            literalError.Text = GetGlobalResourceObject("Login.aspx", "UsernameAlert")
            divError.Visible = True
            Exit Sub
        End If

        If loginPassword.Text.Trim() = String.Empty Then
            literalError.Text = GetGlobalResourceObject("Login.aspx", "PasswordAlert")
            divError.Visible = True
            Exit Sub
        End If

        Dim email As String = loginEmail.Text.ToSql(True).Trim()
        Dim username As String = loginUsername.Text.ToSql(True).Trim()
        Dim password As String = loginPassword.Text.ToSql(True).Trim()

        Dim result = WebUtils.Login(New Infrastructure.Security.UserRole() {Infrastructure.Security.UserRole.Merchant, Infrastructure.Security.UserRole.MerchantSubUser}, username, email, password)
        If result = Infrastructure.Security.LoginResult.Success Then
            If WebUtils.LoggedUser.IsFirstLogin Then
                Response.Redirect("EditLogin.aspx", True)
            Else
                Response.Redirect("Default.aspx", True)
            End If


            'If url.ToLower.Contains("signup.aspx") Then url = "."
            'If url.ToLower.Contains("login.aspx") Then url = "."
            'If url.ToLower.Contains("logout.aspx") Then url = "."
            'If url.ToLower.Contains("userlockedout") Then url = "."
            'If url.ToLower.Contains("userblocked") Then url = "."
            'If url.ToLower.Contains("password") Then url = "."

            'Response.Redirect(url, True)
        Else
            literalError.Text = GetGlobalResourceObject("menu", result.ToString())
            divError.Visible = True
        End If
    End Sub

    Protected Overrides Sub OnPreRender(e As System.EventArgs)

        Dim link As New HtmlControls.HtmlLink()
        link.Href = WebUtils.MapTemplateVirPath(Me, "Website/Styles/LoginStyleSheet.css")
        'link.Href = "style/LoginStyleSheet.css"
        link.Attributes.Add("rel", "stylesheet")
        link.Attributes.Add("type", "text/css")
        link.Attributes.Add("media", "screen")
        Page.Header.Controls.Add(link)
        If System.Threading.Thread.CurrentThread.CurrentUICulture.TextInfo.IsRightToLeft Then
            link = New HtmlControls.HtmlLink()
            link.Href = WebUtils.MapTemplateVirPath(Me, "Website/Styles/LoginStyleSheetRTL.css")
            'link.Href = "style/LoginStyleSheetRTL.css"
            link.Attributes.Add("rel", "stylesheet")
            link.Attributes.Add("type", "text/css")
            link.Attributes.Add("media", "screen")
            Page.Header.Controls.Add(link)
        End If
        MyBase.OnPreRender(e)

    End Sub

End Class