﻿Partial Class Tmp_PBTPay_Mobile
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Overrides Sub OnPreRender(e As System.EventArgs)
        If System.Threading.Thread.CurrentThread.CurrentUICulture.TextInfo.IsRightToLeft Then
            Dim link As New HtmlControls.HtmlLink()
            link.Href = "Website/Styles/mobile-Style-rtl.css"
            link.Attributes.Add("rel", "stylesheet")
            link.Attributes.Add("type", "text/css")
            link.Attributes.Add("media", "screen")
            Page.Header.Controls.Add(link)
        End If
        pageTitle.InnerText = Page.Title
        MyBase.OnPreRender(e)
    End Sub

End Class