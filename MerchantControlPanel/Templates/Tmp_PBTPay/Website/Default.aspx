﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Templates/Tmp_PBTPay/page.Master" MaintainScrollPositionOnPostback="true" CodeBehind="Default.aspx.vb" Inherits="Netpay.MerchantControlPanel.Tmp_PBTPay_Default" %>
<%@ Register Src="~/Common/Counter.ascx" TagPrefix="netpay" TagName="Counter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTitle" Runat="Server">
	<asp:Localize ID="locPageTitle" Text="<%$Resources:default.aspx, HomePage %>" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="left-menu">
        <div class="icon-list-first">
            <div class="icon-list-back">
                <div class="icon">
                    <a href="TransApproved.aspx">
                        <img src="../Templates/Tmp_PBTPay/images/icons/icon-left-menu-04.png" border="0" /></a>
                </div>
                <div>
                    <a href="TransApproved.aspx" class="button">
                        <asp:Literal ID="Literal5" runat="server" Text="<%$Resources:MultiLang,Refund %>" /></a>
                </div>
            </div>
        </div>
        <div class="icon-list">
            <div class="icon-list-back">
                <div class="icon">
                    <a href="VirtualTerminalCC.aspx">
                        <img alt="Virtual Terminal" src="../Templates/Tmp_PBTPay/images/icons/icon-left-menu-01.png" border="0" /></a>
                </div>
                <div>
                    <a href="VirtualTerminalCC.aspx" class="button">
                        <asp:Literal ID="Literal4" runat="server" Text="<%$Resources:MultiLang,VirtualTerminal %>" /></a>
                </div>
            </div>
        </div>
        <div class="icon-list">
            <div class="icon-list-back">
                <div class="icon">
                    <a href="Reports.aspx">
                        <img alt="Reports" src="../Templates/Tmp_PBTPay/images/icons/icon-left-menu-02.png" border="0" /></a>
                </div>
                <div>
                    <a href="Reports.aspx" class="button">
                        <asp:Literal ID="Literal2" runat="server" Text="<%$Resources:MultiLang,Reports %>" /></a>
                </div>
            </div>
        </div>
        <div class="icon-list">
            <div class="icon-list-back">
                <div class="icon">
                    <a href="FrequentlyAskedQuestions.aspx">
                        <img alt="faq" src="../Templates/Tmp_PBTPay/images/icons/icon-left-menu-03.png" border="0" /></a>
                </div>
                <div>
                    <a href="FrequentlyAskedQuestions.aspx" class="button">
                        <asp:Literal ID="Literal3" runat="server" Text="<%$Resources:MultiLang,FAQ %>" /></a>
                </div>
            </div>
        </div>
        <div class="icon-list">

            <div class="icon-list-back">
                <div class="icon">
                    <a href="UpdateAccount.aspx">
                        <img alt="Settings" src="../Templates/Tmp_PBTPay/images/icons/icon-left-menu.png" border="0" /></a>
                </div>
                <div>
                    <a href="UpdateAccount.aspx" class="button">
                        <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:MultiLang,Settings %>" /></a>
                </div>
            </div>
        </div>
    </div>
    <div class="right-menu">
        <div class="top-nav">
            <asp:Literal ID="Literal6" runat="server" Text="<%$Resources:MultiLang,Newsupdate %>" />
        </div>
        <div class="top-nav-main">
			 <div class="wrap-notification" runat="server" clientidmode="Static">
                <div class="notification-spacer"></div>
                <asp:UpdatePanel ID="upNotifications" runat="server">
                    <ContentTemplate>
                        <asp:PlaceHolder ID="phNotifications" Visible="false" runat="server">
                            <netpay:NotificationSlip ID="NotificationSlip1" Type="Reminder" Text="<%$Resources:default.aspx, SlipSiteNews %>" Title="<%$Resources:default.aspx, SlipSiteNewsTitle %>" runat="server" />
                            <netpay:NotificationSlip ID="wcNotificationSlipVerify" Type="Reminder" Text="<%$Resources:default.aspx, SlipRetrievalRequest %>" Title="<%$Resources:default.aspx, SlipRetrievalRequestTitle %>" runat="server" />
                            <netpay:NotificationSlip ID="wcNotificationSlipTest" Type="Warning" Text="<%$Resources:default.aspx, SlipTestTrans %>" Title="<%$Resources:default.aspx, SlipTestTransTitle %>" runat="server" />
                            <netpay:NotificationSlip ID="wcNotificationSlipVisaChbRatio" Visible="false" Type="Reminder" Text="<%$Resources:default.aspx, SlipChbViaRatio %>" Title="<%$Resources:default.aspx, SlipChbViaRatioTitle %>" runat="server" />
                            <netpay:NotificationSlip ID="wcNotificationSlipMastercardChbRatio" Visible="false" Type="Reminder" Text="<%$Resources:default.aspx, SlipChbMastercardRatio %>" Title="<%$Resources:default.aspx, SlipChbMastercardRatioTitle %>" runat="server" />
                        </asp:PlaceHolder>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="hiddenAsyncTrigger" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="hiddenAsyncTrigger" runat="server" Text="AsyncUpdate" style="display:none;" />
                 <div class="notification-spacer"></div>
			</div>
        </div>
        <div class="clear"></div>
		<netpay:Counter runat="server" id="ctlCounter" />
        <div class="clear"></div>
        <div class="shortcut">
            <div class="shortcut-icon-left">
                <p>
                    <span><asp:Literal ID="Literal8" runat="server" Text="<%$Resources:MultiLang,LastSettlements%>" /></span>
                </p>
                <hr class="style-two" />
                <a href="SettlementsReport.aspx">
                    <img alt="" src="../Templates/Tmp_PBTPay/images/icons/pictre-shortcut-04.png" />
                </a>
            </div>
            <div class="shortcut-icon-left">
                <p>
                    <span>
                        <asp:Literal ID="Literal9" runat="server" Text="<%$Resources:MultiLang,LastMonth%>" /></span>
                </p>
                <hr class="style-two" />
                <a runat="server" id="aApprovedLast30">
                    <img alt="" src="../Templates/Tmp_PBTPay/images/icons/pictre-shortcut-01.png" />
                </a>
            </div>
            <div class="shortcut-icon-left">
                <p>
                    <span><asp:Literal ID="Literal10" runat="server" Text="<%$Resources:MultiLang,LastWeek%>" /></span>
                </p>
                <hr class="style-two" />
                <a runat="server" id="aApprovedLast7">
                    <img alt="" src="../Templates/Tmp_PBTPay/images/icons/pictre-shortcut-02.png" />
                </a>
            </div>
            <div class="shortcut-icon-right">
                <p>
                    <span><asp:Literal ID="Literal11" runat="server" Text="<%$Resources:MultiLang,LastDay%>" /></span>
                </p>
                <hr class="style-two" />
                <a runat="server" id="aApprovedLast1">
                    <img alt="" src="../Templates/Tmp_PBTPay/images/icons/pictre-shortcut-03.png" />
                </a>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="spacer"></div>
</asp:Content>
