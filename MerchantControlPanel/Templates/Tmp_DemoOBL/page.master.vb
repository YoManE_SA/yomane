﻿Imports System.Xml
Imports Netpay.Web

Public Class Tmp_DemoOBL_page
    Inherits System.Web.UI.MasterPage

    Protected Sub renderMenuSection(menuBuilder As StringBuilder, ByVal menuSection As XmlNode)
        menuBuilder.AppendLine("<ul>")
        menuBuilder.AppendLine(" <li class=""ldd_heading"">" + GetMenuItem(menuSection) + "</li>")
        For Each item As XmlNode In menuSection.ChildNodes
            menuBuilder.AppendLine("  <li>" + GetMenuItem(item) + "</li>")
        Next
        menuBuilder.AppendLine("</ul>")
    End Sub

    Protected Sub renderMenuPopup(menuBuilder As StringBuilder, ByVal menuPopup As XmlNode)
        Dim strText As String = menuPopup.Attributes.GetNamedItem("title").Value
        menuBuilder.AppendLine(" <span id=""MenuBar_" & strText & """>" & CType(GetGlobalResourceObject("Menu", strText), String) & "</span>")
        menuBuilder.AppendLine(" <div class=""ldd_submenu"" style=""min-width:" & (menuPopup.ChildNodes.Count * 260) & "px;"">")
        For Each item As XmlNode In menuPopup.ChildNodes
            renderMenuSection(menuBuilder, item)
        Next
        menuBuilder.AppendLine(" </div>")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If WebUtils.IsLoggedin Then
            phLoggedinPanel.Visible = True
            literalLastLogin.Text = WebUtils.LoggedUser.LastLogin.GetValueOrDefault().ToString("dd/MM/yyyy HH:mm")

            pnlLoggedIn.Visible = True
            pnlLoggedOut.Visible = False
        Else
            pnlLoggedIn.Visible = False
            pnlLoggedOut.Visible = True
        End If
        dvNoSession.Visible = Not WebUtils.IsLoggedin
        'menu
        Dim menuBuilder As StringBuilder = New StringBuilder()
        Dim xmlDoc As New XmlDocument : xmlDoc.Load(MapPath(WebUtils.MapTemplateVirPath(Page, "Web.sitemap")))
        menuBuilder.Append("<ul id=""ldd_menu"" class=""ldd_menu"">")
        For Each node As XmlNode In xmlDoc.DocumentElement.FirstChild.ChildNodes
            menuBuilder.Append("<li>")
            renderMenuPopup(menuBuilder, node)
            menuBuilder.Append("</li>")
        Next
        menuBuilder.Append("</ul>")
        literalMenu.Text = menuBuilder.ToString()

        DvSupport.DataBind()
        DvSocialMedia.DataBind()

        If String.IsNullOrEmpty(WebUtils.CurrentDomain.CustomerServiceSkype) And String.IsNullOrEmpty(WebUtils.CurrentDomain.CustomerServicePhone) And String.IsNullOrEmpty(WebUtils.CurrentDomain.CustomerServiceFax) And String.IsNullOrEmpty(WebUtils.CurrentDomain.CustomerServiceEmail) Then
            literalCustomerService.Visible = False
        End If

        If String.IsNullOrEmpty(WebUtils.CurrentDomain.TechnicalSupportSkype) And String.IsNullOrEmpty(WebUtils.CurrentDomain.TechnicalSupportPhone) And String.IsNullOrEmpty(WebUtils.CurrentDomain.TechnicalSupportFax) And String.IsNullOrEmpty(WebUtils.CurrentDomain.TechnicalSupportEmail) Then
            literalTechnicalSupport.Visible = False
        End If

    End Sub

    Private Function GetMenuItem(ByVal menuItem As XmlNode) As String
        Dim menuItemTitle As String = menuItem.Attributes.GetNamedItem("title").Value
        menuItemTitle = CType(GetGlobalResourceObject("Menu", menuItemTitle), String)

        If menuItem.Attributes.GetNamedItem("url") IsNot Nothing Then
            Dim menuItemUrl As String = menuItem.Attributes.GetNamedItem("url").Value
            If Request.ApplicationPath = "/" Then
                menuItemUrl = menuItemUrl.Replace("~", "")
            Else
                menuItemUrl = menuItemUrl.Replace("~", Request.ApplicationPath)
            End If

            Dim isAllowed As Boolean
            If Not WebUtils.IsLoggedin Then
                isAllowed = False
            Else
                isAllowed = CType(Me.Page, NetpayPage).IsAuthorized(System.IO.Path.GetFileName(menuItemUrl))
            End If

            If isAllowed Then
                Return "<a href=""" + menuItemUrl + """>" + menuItemTitle + "</a>"
            Else
                Return menuItemTitle
            End If
        Else
            Return menuItemTitle
        End If
    End Function

    Private Function GetUrl(ByVal url As String) As String
        If Request.ApplicationPath = "/" Then
            Return url.Replace("~", "")
        Else
            Return url.Replace("~", Request.ApplicationPath)
        End If
    End Function

    Protected Overrides Sub OnPreRender(e As System.EventArgs)
        'hlDevCenter.NavigateUrl = WebUtils.CurrentDomain.DevCentertUrl
        If System.Threading.Thread.CurrentThread.CurrentUICulture.TextInfo.IsRightToLeft Then
            Dim link As New HtmlControls.HtmlLink()
            link.Href = "Website/Styles/StyleRTL.css"
            link.Attributes.Add("rel", "stylesheet")
            link.Attributes.Add("type", "text/css")
            link.Attributes.Add("media", "screen")
            Page.Header.Controls.Add(link)
        End If
        'pageTitle.Text = Page.Title
        ltCopyright.Text = CType(GetGlobalResourceObject("MultiLang", "Copyright"), String).Replace("%BRAND%", WebUtils.CurrentDomain.BrandName)
        MyBase.OnPreRender(e)


    End Sub
End Class