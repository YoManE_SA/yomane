﻿Imports Netpay.Web
Imports Netpay.Infrastructure

Partial Class Tmp_OBL_Default
    Inherits Website_Default

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        aApprovedLast30.HRef = String.Format("~/WebSite/TransApproved.aspx?FromDate={0}", DateTime.Now.AddMonths(-1).ToString("s").ToEncodedUrl())
        aApprovedLast7.HRef = String.Format("~/WebSite/TransApproved.aspx?FromDate={0}", DateTime.Now.AddDays(-7).ToString("s").ToEncodedUrl())
        aApprovedLast1.HRef = String.Format("~/WebSite/TransApproved.aspx?FromDate={0}", DateTime.Now.AddDays(-1).ToString("s").ToEncodedUrl())
    End Sub
End Class