﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Tmp_Yomane_Default

    '''<summary>
    '''Literal5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal5 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''Literal4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal4 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''Literal2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal2 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''Literal3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal3 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''Literal1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal1 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''Literal6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal6 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''ctlCounter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ctlCounter As Global.Netpay.MerchantControlPanel.Counter

    '''<summary>
    '''Literal8 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal8 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''Literal9 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal9 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''aApprovedLast30 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents aApprovedLast30 As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''Literal10 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal10 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''aApprovedLast7 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents aApprovedLast7 As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''Literal11 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Literal11 As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''aApprovedLast1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents aApprovedLast1 As Global.System.Web.UI.HtmlControls.HtmlAnchor
End Class
