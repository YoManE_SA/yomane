﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" Title="Black List" CodeBehind="BlackList.aspx.cs" Inherits="mobileAdmincash.BlackList" %>

<asp:Content ID="cntSearch" ContentPlaceHolderID="PageProperies" runat="server">
    <table width="100%" border="0">
        <tr>
            <td valign="top">Type
                <asp:RadioButtonList ID="rbType" runat="server" RepeatDirection="Horizontal" CellPadding="3">
                    <asp:ListItem Text=" CC Black List" Value="FraudCcBlackLists" Selected="True" />
                    <asp:ListItem Text=" Item Black List" Value="BLCommons" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td valign="top">VIEW
                <asp:RadioButtonList runat="server" ID="rblFilter" RepeatDirection="Horizontal" CellPadding="4" />
            </td>
        </tr>
        <tr>
            <td valign="top">MERCHANT
                <netpay:MerchantsDropDown runat="server" ID="mddMerchant" />
            </td>
        </tr>
        <tr>
            <td valign="top">SORT
                <asp:RadioButtonList ID="rblSort" runat="server" RepeatDirection="Horizontal" CellPadding="4">
                    <asp:ListItem Text=" Ascending" />
                    <asp:ListItem Text=" Descending" Selected="True" />
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="cntBody" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Repeater runat="server" ID="lstResults">
        <HeaderTemplate>
            <br />
            <table cellpadding="1" cellspacing="0" border="0" class="DataTable">
                <%--    <tr>
		 <th style="vertical-align:middle;">&nbsp;</th>
		 <th><asp:Literal runat="server" Text="Card" /></th>
		 <th><asp:Literal runat="server" Text="Merchant" /></th>
		 <th><asp:Literal runat="server" Text="MerchantID" /></th>
		 <th><asp:Literal runat="server" Text="Blocked" /></th>
		 <th><asp:Literal runat="server" Text="Unblock" /></th>
         <th></th>
        </tr>--%>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td colspan="3" style="padding: 10px 0;">
                    <hr style="border-top: dotted 1px gray;" />
                </td>
            </tr>
            <tr>
                <%# GetLegend(Container.DataItem as Netpay.Bll.Risk.RiskItem) %>
                <td>
                    <h3>
                        <netpay:PaymentMethodView ID="PaymentMethodView1" PaymentMethodID="<%# (Container.DataItem as Netpay.Bll.Risk.RiskItem).ValueType %>" PaymentMethodDisplay='<%# Eval("Display") %>' runat="server" />
                    </h3>
                </td>
                <td align="right">
                    <asp:Button ID="Button1" runat="server" Text="Unblock" CommandArgument='<%# Eval("ID") %>' OnCommand="Delete_Command" OnClientClick="if(!confirm('Are you sure you wish to unblock this card?')) return false;" /></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="2">
                    <h4>
                        <%# ((Container.DataItem as Netpay.Bll.Risk.RiskItem).MerchantID == 0 || (Container.DataItem as Netpay.Bll.Risk.RiskItem).MerchantID == null) ? "SYSTEM BLOCK" : Netpay.Bll.Merchants.Merchant.CachedNamesForDomain()[(Container.DataItem as Netpay.Bll.Risk.RiskItem).MerchantID.GetValueOrDefault()] + " - " + (Container.DataItem as Netpay.Bll.Risk.RiskItem).MerchantID.Value.ToString() %>
                    </h4>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="2">
                    <h4><%#(Container.DataItem as Netpay.Bll.Risk.RiskItem).InsertDate.ToString("dd/MM/yyy HH:mm")%> &nbsp;
				(END <%#((Container.DataItem as Netpay.Bll.Risk.RiskItem).Duration.GetValueOrDefault()).ToString("dd/MM/yyy HH:mm")%>)</h4>
                </td>
            </tr>
            <tr>
                <td style="height: 2px;"></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</asp:Content>
<asp:Content ContentPlaceHolderID="PageBottom" runat="server">
    <asp:Button runat="server" Visible="false" ID="btnDelete" Text="Delete" OnClick="DeleteMulti_Click" />
</asp:Content>
