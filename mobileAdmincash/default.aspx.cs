﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;

namespace mobileAdmincash
{
    public partial class _default : Code.BasePage
    {
        protected override void OnPreInit(EventArgs e)
        {
            checkAccess = false;
            base.OnPreInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (WebUtils.IsLoggedin) StatusText = WebUtils.LoggedUser.UserName;
        }
    }
}