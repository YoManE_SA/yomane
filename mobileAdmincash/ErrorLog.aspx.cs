﻿using System;
using Netpay.Infrastructure;
using Netpay.Bll;
using Netpay.Web;

namespace mobileAdmincash
{
    public partial class ErrorLog : Code.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			Paging = new SortAndPage(CurrentPage, 10, "InsertDate", true);
			if (!IsPostBack)
            {
                ddlSeverityID.DataSource = Enums.GetEnumDataSource<LogSeverity>();
                ddlSeverityID.DataTextField = "Key";
                ddlSeverityID.DataValueField = "Value";
                ddlSeverityID.DataBind();
                ddlSeverityID.Items.Insert(0, new System.Web.UI.WebControls.ListItem("", "-1"));

                ddlTag.DataSource = Enums.GetEnumDataSource<LogTag>();
                ddlTag.DataTextField = "Key";
                ddlTag.DataValueField = "Key";
                ddlTag.DataBind();
                ddlTag.Items.Insert(0, new System.Web.UI.WebControls.ListItem("", "-1"));
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
			var searchFilters = new Netpay.Infrastructure.Logger.SearchFilters();
            if (int.Parse(ddlSeverityID.SelectedValue) >= 0) searchFilters.Severity = ddlSeverityID.SelectedValue.ToNullableEnumByValue<LogSeverity>();
            if (ddlTag.SelectedValue != "-1") searchFilters.Tag = ddlTag.SelectedValue;
            repeaterResults.DataSource = Netpay.Infrastructure.Logger.GetLogs(searchFilters, Paging);
            repeaterResults.DataBind();
            base.OnPreRender(e);
        }

        protected string FormatInfo(Netpay.Infrastructure.Logger item)
        {
            if (item.LongMessage == null)
                return string.Empty;
            else
                return "<strong>More info</strong></br/>" + item.LongMessage;
        }
    
    }
}