﻿using System;
using Netpay.Infrastructure;
using Netpay.Bll;
using Netpay.Web;

namespace mobileAdmincash
{
    public partial class Transactions : Code.BasePage
    {
        protected override void OnLoad(EventArgs e) 
        {
            if(!IsPostBack){
                rblFilter.Items.Add(new System.Web.UI.WebControls.ListItem(" PASSED", ((int)TransactionStatus.Captured).ToString()));
                rblFilter.Items.Add(new System.Web.UI.WebControls.ListItem(" DECLINED", ((int)TransactionStatus.Declined).ToString()));
                rblFilter.Items.Add(new System.Web.UI.WebControls.ListItem(" APPROVED", ((int)TransactionStatus.Authorized).ToString()));
                rblFilter.Items.Add(new System.Web.UI.WebControls.ListItem(" PENDING", ((int)TransactionStatus.Pending).ToString()));
                rblFilter.SelectedIndex = 0;
            }
			Paging = new SortAndPage(CurrentPage, 10, "InsertDate", true);
            base.OnLoad(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
			var searchFilters = new Netpay.Bll.Transactions.Transaction.SearchFilters();
			Paging.SortDesc = rblSort.SelectedIndex != 0;
            TransactionStatus ts = (TransactionStatus)rblFilter.SelectedValue.ToInt32((int)TransactionStatus.Captured);
            if (mddMerchant.IsSelected) {
                searchFilters.merchantIDs = new System.Collections.Generic.List<int>();
                searchFilters.merchantIDs.Add(mddMerchant.SelectedValue.ToInt32());
            }
            if (dcddBank.IsSelected) searchFilters.debitCompanyID = dcddBank.SelectedValue.ToInt32();
			repeaterResults.DataSource = Netpay.Bll.Transactions.Transaction.Search(ts, searchFilters, Paging, false, false);
            repeaterResults.DataBind();
            base.OnPreRender(e);
        }
    
    }
}