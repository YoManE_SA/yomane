﻿<%@ Page Language="C#" AutoEventWireup="true" Title="ChargeAttempts" MasterPageFile="~/Site.Master" CodeBehind="ChargeAttempts.aspx.cs" Inherits="mobileAdmincash.ChargeAttempts" %>
<asp:Content ContentPlaceHolderID="PageProperies" runat="server">
    <table width="100%" border="0">
        <tr>
            <td valign="top">
                Free Text<br />
                <asp:TextBox runat="server" ID="txtFreeText" />
                <br />
            </td>
            <td valign="top">
                REPLY CODE<br />
                <asp:TextBox runat="server" ID="txtReplyCode" />
                <br />
                MERCHANT<br />
                <netpay:MerchantsDropDown runat="server" ID="mddMerchant" />
                <br />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
		<table class="formNormal" align="center" style="width: 92%" cellpadding="1" cellspacing="1">
			<tr>
				<th><asp:Literal Text="ID" runat="server" /></th>
				<th><asp:Literal Text="Date" runat="server" /></th>
				<th><asp:Literal Text="Merchant" runat="server" /></th>
				<th><asp:Literal Text="Reply" runat="server" /></th>
            </tr>
            <tr>
				<th><asp:Literal Text="TransID" runat="server" /></th>
				<th><asp:Literal Text="Duration" runat="server" /></th>
				<th><asp:Literal Text="IP Address" runat="server" /></th>
				<th><asp:Literal Text="Source" runat="server" /></th>
			</tr>	
	        <asp:Repeater ID="repeaterResults" runat="server">
		        <ItemTemplate>
			        <tr>
				        <td><%# Eval("ID")%></td>
				        <td><%# ((DateTime?)Eval("DateStart")).GetValueOrDefault().ToString("d") %></td>
				        <td><%# Eval("MerchantNumber")%></td>
				        <td><%# string.IsNullOrEmpty((string) Eval("ReplyCode")) ? "---" : Eval("ReplyCode") %></td>
                    </tr><tr>
				        <td><%# Eval("TransactionID")%></td>
				        <td><%# ((DateTime?)Eval("DateStart")).GetValueOrDefault().ToString("t") + " (" + ((TimeSpan)Eval("Duration")).TotalSeconds.ToString("0.0") + "s)"%></td>
				        <td><%# Eval("RemoteIPAddress")%></td>
				        <td><%# Eval("TransactionTypeId")%></td>
			        </tr>
                    <tr><td height="2" style="background-color:Black;" colspan="4"></td></tr>
		        </ItemTemplate>
	        </asp:Repeater>
		</table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageBottom" runat="server">
</asp:Content>
