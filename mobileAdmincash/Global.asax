﻿<%@ Application Language="C#" %>
<%@ Import Namespace="Netpay.Infrastructure" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>
<script runat="server">
	void Application_Start(object sender, EventArgs e) 
    {
        Netpay.Web.ContextModule.AppName = "Mobile Admincash";
	}

	void Application_Error(object sender, EventArgs e) 
    {
		Exception ex = Server.GetLastError();
		if (ex is System.Web.HttpException) {
			if (ex.Message.StartsWith("Validation of viewstate MAC failed.")) {
				Response.Redirect("~/Default.aspx");
				return;
			}
		}
		Netpay.Infrastructure.Logger.Log(LogTag.MerchantControlPanel, ex);
		System.Configuration.Configuration config = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
		System.Web.Configuration.CompilationSection configSection = (config.GetSection("system.web/compilation") as System.Web.Configuration.CompilationSection);
		if (!configSection.Debug)
        {
            Response.Write("Unexpected system error occurred.");
			Response.End();
        }
	}
	
	void Application_PreRequestHandlerExecute(object sender, EventArgs e)
    {
		WebUtils.SelectLanguage(null);
	}
    
	public void Application_AcquireRequestState(object sender, EventArgs e)
    {
		if (Request.IsSecureConnection) return;
		if (Request.ServerVariables["HTTP_HOST"].StartsWith("192.168") || Request.ServerVariables["HTTP_HOST"] == "127.0.0.1"
			|| Request.ServerVariables["HTTP_HOST"] == "localhost" || Request.ServerVariables["HTTP_HOST"].StartsWith("80.179.180.")) return;
        if (WebUtils.CurrentDomain.ForceSSL) Response.Redirect("https://" + Request.Url.ToString().Substring(7));
    }
	
</script>