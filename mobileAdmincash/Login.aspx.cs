﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mobileAdmincash
{
    public partial class Login : Code.BasePage
    {
        protected override void OnPreInit(EventArgs e) 
        {
            checkUser = false;
            base.OnPreInit(e);
        }

        protected void netpayLogin_LoginSuccess(object sender, EventArgs e) 
        {
            Response.Redirect("default.aspx");
        }
    }
}