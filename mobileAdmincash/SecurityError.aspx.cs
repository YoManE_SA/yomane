﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mobileAdmincash
{
    public partial class SecurityError : Code.BasePage
    {
        protected override void OnPreInit(EventArgs e)
        {
            checkAccess = false;
            base.OnPreInit(e);
        }
    }
}