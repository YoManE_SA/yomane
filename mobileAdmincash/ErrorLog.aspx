﻿<%@ Page Language="C#" Title="ErrorLog" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ErrorLog.aspx.cs" Inherits="mobileAdmincash.ErrorLog" %>
<asp:Content ID="Content3" ContentPlaceHolderID="PageProperies" runat="server">
    <table width="100%" border="0">
        <tr>
            <td valign="top">
                Severity<br />
                <asp:DropDownList ID="ddlSeverityID" runat="server" EnableViewState="true" />
                <br />
            </td>
            <td valign="top">
                Tag<br />
                <asp:DropDownList ID="ddlTag" runat="server" EnableViewState="true" />
                <br />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<table border="0" width="100%" class="formNormal" >
		<tr>
			<th>Severity</th>
			<th>Date</th>
			<th>Instance</th>
			<th>Tag</th>
        </tr><tr>
			<th>Info</th>
		</tr>
		<asp:Repeater ID="repeaterResults" runat="server">
			<ItemTemplate>
				<tr onclick="with(parentNode.rows[rowIndex + 2].style) { display= display != '' ? '' : 'none'; }">
					<td class="reportCell" nowrap="nowrap"><%# (LogSeverity)Eval("Severity") %></td>
					<td class="reportCell" nowrap="nowrap"><%# ((DateTime)Eval("InsertDate")).ToString("d") %></td>
					<td class="reportCell" nowrap="nowrap"><%# Eval("Source") %></td>
					<td class="reportCell" nowrap="nowrap"><%# Eval("Tag") %></td>
                </tr><tr>
					<td class="reportCell" colspan="4"><%# Eval("Message") %></td>										
				</tr>
				<tr style="margin:0;padding:0; display:none;">
					<td colspan="4" style="margin:0;padding:15px;background-color:#F0F0F0;">
						<pre style="width:0px;"><asp:Literal ID="ltMoreInfo" Text='<%# FormatInfo((Netpay.Infrastructure.Logger)Container.DataItem) %>' runat="server" /></pre>							
					</td>
				</tr>
                <tr>
                    <td colspan="4" height="2" style="background:black;"></td>
                </tr>
			</ItemTemplate>
		</asp:Repeater>
	</table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageBottom" runat="server">
</asp:Content>
