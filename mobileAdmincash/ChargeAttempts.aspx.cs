﻿using System;
using System.Linq;
using Netpay.Infrastructure;
using Netpay.Bll;
using Netpay.Web;

namespace mobileAdmincash
{
    public partial class ChargeAttempts : Code.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			Paging = new SortAndPage(CurrentPage, 10, null, false);
        }

        protected override void OnPreRender(EventArgs e)
        {
            var searchFilters = new Netpay.Bll.Process.ChargeAttemptLog.SearchFilters();
            /*
            var sortInfo = new SortingInfo();
            sortInfo.Property = "InsertDate";
            sortInfo.VOType = typeof(Netpay.Infrastructure.VO.TransactionVO);
            sortInfo.Direction = rblSort.SelectedIndex != 0 ? SortDirection.Descending : SortDirection.Ascending;
            */
            if (mddMerchant.IsSelected)
            {
				searchFilters.MerchantNumber = Netpay.Bll.Merchants.Merchant.CachedNumbersForDomain().Where(v => v.Value == mddMerchant.SelectedValue.ToInt32()).Select(v => v.Key).SingleOrDefault();
            }
            if (!string.IsNullOrEmpty(txtReplyCode.Text)) searchFilters.ReplyCode = (txtReplyCode.Text == "---" ? "" : txtReplyCode.Text);
            if (!string.IsNullOrEmpty(txtFreeText.Text)) searchFilters.Text = txtFreeText.Text;
            repeaterResults.DataSource = Netpay.Bll.Process.ChargeAttemptLog.Search(searchFilters, Paging);
            repeaterResults.DataBind();
            base.OnPreRender(e);
        }
    
    }
}