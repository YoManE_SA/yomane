﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Bll;
using Netpay.Infrastructure;

namespace mobileAdmincash
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            mvUserInfo.ActiveViewIndex = WebUtils.IsLoggedin ? 1 : 0;
        }

        protected void Page_Command(object sender, CommandEventArgs e) 
        {
            if (!(Page is Code.BasePage)) return;
            int page;
            var paging = (Page as Code.BasePage).Paging;
            if (paging == null) return;
            string arg = (string)e.CommandArgument;
			if (arg == "Next") paging.PageCurrent++;
			else if (arg == "Prev" && paging.PageCurrent > 0) paging.PageCurrent--;
			else if (int.TryParse(arg, out page)) paging.PageCurrent = page;
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (Page is Code.BasePage)
            {
                var page = Page as Code.BasePage;
                if (page.Paging != null)
                {
					ltCurrentPage.Text = "<input type=\"hidden\" name=\"hfCurrentPage\" value=\"" + page.Paging.PageCurrent + "\" />";
                    if (string.IsNullOrEmpty(page.StatusText))
						page.StatusText = string.Format("Page {0} of {1}", page.Paging.PageCurrent + 1, page.Paging.PageCount);
                    btnPagePrev.Enabled = page.Paging.PageCurrent > 0;
					btnPageNext.Enabled = page.Paging.PageCurrent + 1 < page.Paging.PageCount;
                } else btnPagePrev.Visible = btnPageNext.Visible = false;
                ltStatus.Text = page.StatusText;
            }
            btnProperties.Visible = dvProperties.Visible = PageProperies.Controls.Count > 0;
            dvPageBottom.Visible = PageBottom.Controls.Count > 0;

            string userAgent = Request.ServerVariables["HTTP_USER_AGENT"].EmptyIfNull().ToLower();
            string deviceCss = null;
            if (userAgent.Contains("iphone") && Page.Header != null) deviceCss = "~/Site.iPhone.css";
            if (deviceCss != null) {
                var devCssLink = new System.Web.UI.HtmlControls.HtmlLink();
                devCssLink.Href = deviceCss;
                devCssLink.Attributes.Add("rel", "stylesheet");
                devCssLink.Attributes.Add("type", "text/css");
                HeadContent.Controls.Add(devCssLink);
            }
            base.OnPreRender(e);
        }

    }
}
