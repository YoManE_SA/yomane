﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mobileAdmincash
{
    public partial class Logout : Code.BasePage
    {
        protected override void OnPreInit(EventArgs e)
        {
            checkUser = false;
            base.OnPreInit(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            Logout();
			Response.Redirect("login.aspx");
            base.OnLoad(e);
        }
    }
}