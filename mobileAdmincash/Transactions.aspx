﻿<%@ Page Language="C#" AutoEventWireup="True" Title="Transactions" MasterPageFile="~/Site.Master" CodeBehind="Transactions.aspx.cs" Inherits="mobileAdmincash.Transactions" %>
<asp:Content ContentPlaceHolderID="PageProperies" runat="server">
    <table width="100%" border="0">
        <tr>
            <td valign="top">
                VIEW<br />
                <asp:RadioButtonList runat="server" ID="rblFilter" />
            </td>
            <td valign="top">
                MERCHANT<br />
                <netpay:MerchantsDropDown runat="server" ID="mddMerchant" />
                <br />
                BANK<br />
                <netpay:DebitCompanyDropDown runat="server" ID="dcddBank" />
                <br />
                SORT<br />
                <asp:RadioButtonList ID="rblSort" runat="server">
                    <asp:ListItem Text=" Ascending" Selected="True" />
                    <asp:ListItem Text=" Descending" />
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
		<table class="formNormal" align="center" style="width: 92%" cellpadding="1" cellspacing="1">
			<tr>
				<th colspan="2">&nbsp;</th>
				<th><asp:Literal Text="ID" runat="server"></asp:Literal></th>
				<th><asp:Literal ID="litDateTitle" Text="Date" runat="server"></asp:Literal></th>
				<th><asp:Literal Text="Amount" runat="server"></asp:Literal></th>
				<asp:Literal ID="lblTableHeadings" runat="server"/>
			</tr>	
	        <asp:Repeater ID="repeaterResults" runat="server">
		        <ItemTemplate>
			        <tr>
				        <td>
					        <netpay:TransactionRowView ID="ucTransactionView" Transaction="<%# Container.DataItem as Netpay.Bll.Transactions.Transaction %>" ShowExpandButton="false" ShowStatus="false" ShowPaymentMethod="false" runat="server" />
				        </td>
			        </tr>
		        </ItemTemplate>
	        </asp:Repeater>
		</table>
</asp:Content>
<asp:Content ContentPlaceHolderID="PageBottom" runat="server">
</asp:Content>
