﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Netpay.Web;
using Netpay.Bll;
using Netpay.Infrastructure;

namespace mobileAdmincash
{
    public partial class WireConfirm : Code.BasePage
    {
        //private const string FinanceUser = "limor";
        //private const string FinanceGroup = "Financial";
        private int myWireLevel = 0;
        protected int WireID { get { return hfWireID.Value.ToInt32(0); } set { hfWireID.Value = value.ToString(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            ltWireLevel1Group.Text = Domain.Current.FinanceLevel1Group;
            ltWireLevel2Group.Text = Domain.Current.FinanceLevel2Group;
			Paging = new SortAndPage(CurrentPage, 1, "wireInsertDate", true);
            if (Netpay.Infrastructure.Security.AdminUser.Current.IsInGroup(Domain.Current.FinanceLevel1Group)) myWireLevel = 1;
            else if (Netpay.Infrastructure.Security.AdminUser.Current.IsInGroup(Domain.Current.FinanceLevel2Group)) myWireLevel = 2;
			//if (Netpay.Infrastructure.Security.SecuredObject.Get(Netpay.Bll.Wires.Module.ModuleName, "Confirm Seconed").HasPermission(Netpay.Infrastructure.Security.PermissionValue.Execute)) myWireLevel = 2;
            //else if (Netpay.Infrastructure.Security.SecuredObject.Get(Netpay.Bll.Wires.Module.ModuleName, "Confirm First").HasPermission(Netpay.Infrastructure.Security.PermissionValue.Execute)) myWireLevel = 1;
        }

        protected override void OnPreRender(EventArgs e)
        {
            LoadWire();
            base.OnPreRender(e);
        }

        protected void LoadWire()
        {
			var sf = new Netpay.Bll.Wires.Wire.SearchFilters();
            switch (rblFilter.SelectedValue) { 
                case "NOTAPPROVED":
                    if (myWireLevel == 1) sf.ApprovalLevel2 = true;
					else if (myWireLevel == 2) sf.ApprovalLevel1 = true;
					sf.ApprovalStatus = new List<Netpay.Bll.Wires.Wire.WireApprovalStatus>() { Netpay.Bll.Wires.Wire.WireApprovalStatus.PartiallyApproved };
                    break;
                case "APPROVED":
					if (myWireLevel == 1) sf.ApprovalLevel1 = true;
					else if (myWireLevel == 2) sf.ApprovalLevel2 = true;
                    break;
                case "DELIEND":
					if (myWireLevel == 1) sf.ApprovalLevel1 = false;
					else if (myWireLevel == 2) sf.ApprovalLevel2 = false;
                    break;
            }
			Paging.SortDesc = rblSort.SelectedIndex == 1;
			var wires = Netpay.Bll.Wires.Wire.Search(sf, Paging);
            StatusText = string.Format("{0} Items", Paging.PageCount); /* - (pg.CurrentPage - 1)*/
            bool wireExist = false;
            if (wires != null && wires.Count > 0)
            {
                wireExist = true;
                LoadWire(wires[0]);
            }
            btnDecline.Visible = btnConfirm.Visible = tblWire.Visible = wireExist;
            ltNoRowsMessage.Visible = !wireExist;
        }

        protected void LoadWire(Netpay.Bll.Wires.Wire wire) 
        {
            WireID = wire.ID;
            ltWireNumber.Text = wire.ID.ToString();
            //ltCompanyName.Text = wire.AccountName;
            ltPayeeName.Text = wire.PayeeName;
            ltCreateDate.Text = wire.InsertDate.ToString("dd/MM/yy HH:mm");
            ltDueDate.Text = wire.Date.GetValueOrDefault().ToString("dd/MM/yy HH:mm");
            ltAmount.Text = wire.Amount.ToAmountFormat(wire.Currency);
            ltFeeTaken.Text = wire.Fee.ToAmountFormat(wire.Currency);
            var merchantCurrencySettings = Netpay.Bll.Settlements.MerchantSettings.LoadForMerchant(wire.Account.MerchantID.Value);
			ltFeeExpected.Text = (merchantCurrencySettings.ContainsKey((int)wire.Currency) ? merchantCurrencySettings[(int)wire.Currency].WireFee : 0).ToAmountFormat(wire.Currency);
            ltDueDate.Visible = wire.InsertDate.Date != wire.Date.GetValueOrDefault();

			//rptBalance.DataSource = Netpay.Bll.Accounts.Balance.GetAccountBalance(wire.AccountID, false);
            rptBalance.DataSource = Netpay.Bll.Accounts.Balance.GetStatus(wire.AccountID);
            rptBalance.DataBind();
            ltBalanceNone.Visible = rptBalance.Items.Count == 0;

			var sf = new Netpay.Bll.Transactions.Transaction.SearchFilters();
			sf.merchantIDs = new List<int>(new int[] { wire.Account.MerchantID.Value });
            rptUnsettledTransactions.DataSource = Netpay.Bll.Transactions.Transaction.GetUnsettledBalance(sf);
            rptUnsettledTransactions.DataBind();
            ltUnsettledTransactionsNone.Visible = rptUnsettledTransactions.Items.Count == 0;

			rptRollingReserve.DataSource = Netpay.Bll.Merchants.RollingReserve.GetMerchantStatus(wire.Account.MerchantID.Value);
            rptRollingReserve.DataBind();
            ltRollingReserveNone.Visible = rptRollingReserve.Items.Count == 0;

            ltApprovalLevel1.Text = wire.ApprovalLevel1 != null ? wire.ApprovalLevel1.Value ? "Approved" : "Declined" : "";
            ltApprovalLevel2.Text = wire.ApprovalLevel2 != null ? wire.ApprovalLevel2.Value ? "Approved" : "Declined" : "";
        }

        protected void Confirm_Click(object sender, EventArgs e)
        {
			Netpay.Bll.Wires.Wire.Load(WireID).Approve(myWireLevel, true);
        }

        protected void Decline_Click(object sender, EventArgs e)
        {
			Netpay.Bll.Wires.Wire.Load(WireID).Approve(myWireLevel, false);
		}
    }
}