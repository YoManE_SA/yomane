﻿using System;
using Netpay.Infrastructure;
using Netpay.Bll;
using Netpay.Web;
using Netpay.Bll.Risk;

namespace mobileAdmincash
{
    public partial class BlackList : Code.BasePage
    {
        protected override void OnLoad(EventArgs e) 
        {
            if (!IsPostBack) {
                rblFilter.Items.Add(new System.Web.UI.WebControls.ListItem("All", ""));
                rblFilter.Items.Add(new System.Web.UI.WebControls.ListItem("Merchant", ((int)BlockedCardLevel.Merchant).ToString()));
                rblFilter.Items.Add(new System.Web.UI.WebControls.ListItem("System", ((int)BlockedCardLevel.SystemPermanent).ToString()));
                rblFilter.Items.Add(new System.Web.UI.WebControls.ListItem("System Temp", ((int)BlockedCardLevel.SystemTemporary).ToString()));
                rblFilter.SelectedIndex = 0;
            }
			Paging = new SortAndPage(CurrentPage, 3, "InsertDate", true);
            base.OnLoad(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            var searchFilters = new Netpay.Bll.Risk.RiskItem.SearchFilters();
            Paging.SortDesc = rblSort.SelectedIndex != 0;

            searchFilters.Table = (RiskItem.TableFilter)Enum.Parse(typeof(RiskItem.TableFilter), rbType.SelectedValue);
            if (!string.IsNullOrEmpty(rblFilter.SelectedValue))
                searchFilters.Source = (Netpay.Bll.Risk.RiskItem.RiskSource)int.Parse(rblFilter.SelectedValue);
            if (!string.IsNullOrEmpty(mddMerchant.SelectedValue))
                searchFilters.MerchantID = int.Parse(mddMerchant.SelectedValue);
			
            searchFilters.InsertDate.From = DateTime.Now.AddHours(-12);
			lstResults.DataSource = Netpay.Bll.Risk.RiskItem.Search(searchFilters, Paging);
            lstResults.DataBind();
            base.OnPreRender(e);
        }
        
        protected string GetLegend(Netpay.Bll.Risk.RiskItem item) 
        {
            if (item.Source == Netpay.Bll.Risk.RiskItem.RiskSource.Merchant) return "<td style=\"padding-right:10px;\"><span style='width:6px; height:14px; text-align:center;background-color:#6699CC;'>&nbsp;</span></td>";
            else return "<td style=\"padding-right:10px;\"><span style='width:6px; height:14px; text-align:center;background-color:#484848;'>&nbsp;</span></td>";
        }

        protected void DeleteMulti_Click(object sender, EventArgs e)
        {
		    string blocked  = Request.Form["blockedItemID"];
		    if (string.IsNullOrEmpty(blocked)) return;
		    string[] blockedList = blocked.Split(',');
		    var blockedListConverted  = new System.Collections.Generic.List<int>();
		    foreach(var currentID in blockedList)
			    blockedListConverted.Add(int.Parse(currentID));
			Netpay.Bll.Risk.RiskItem.Delete(Netpay.Bll.Risk.RiskItem.RiskValueType.AccountValue1, Netpay.Bll.Risk.RiskItem.RiskListType.Black, blockedListConverted);
            OnLoad(e);
        }

        protected void Delete_Command(object sender, System.Web.UI.WebControls.CommandEventArgs e)
        {
            string blocked = (string) e.CommandArgument;
            if (string.IsNullOrEmpty(blocked)) return;
            var blockedListConverted = new System.Collections.Generic.List<int>();
            blockedListConverted.Add(int.Parse(blocked));
			Netpay.Bll.Risk.RiskItem.Delete(Netpay.Bll.Risk.RiskItem.RiskValueType.AccountValue1, Netpay.Bll.Risk.RiskItem.RiskListType.Black, blockedListConverted);
        }
    }
}