﻿using System;
using System.Web;
using System.Collections.Generic;
using Netpay.Bll;
using Netpay.Web;
using Netpay.Infrastructure;

namespace mobileAdmincash.Code
{
    public class SecurityModule : Netpay.Infrastructure.Module
    {
        public override string Name { get { return "MA.Security"; } }
        public static SecurityModule Current { get { return Module.Get("MA.Security") as SecurityModule; } }
        public override decimal Version { get { return 1.0m; } }

        public static Netpay.Infrastructure.Security.SecuredObject GetSecuredObject(string name, bool addIfNotExist = false)
        {
            var obj = Netpay.Infrastructure.Security.SecuredObject.Get(SecurityModule.Current, name);
            if (obj == null && addIfNotExist) obj = Netpay.Infrastructure.Security.SecuredObject.Create(SecurityModule.Current, name, Netpay.Infrastructure.Security.PermissionGroup.Execute);
            return obj;
        }

        public static Netpay.Infrastructure.Security.PermissionValue GetPermissionValue(Dictionary<int, Netpay.Infrastructure.Security.PermissionValue?> values, string name)
        {
            var sobj = GetSecuredObject(name);
            if (sobj == null) return Netpay.Infrastructure.Security.PermissionValue.None;
            if (!values.ContainsKey(sobj.ID)) return Netpay.Infrastructure.Security.PermissionValue.None;
            return values[sobj.ID].GetValueOrDefault(Netpay.Infrastructure.Security.PermissionValue.None);
        }
    }

    public class BasePage : Netpay.Web.BasePage 
    {
        public const string DateTimeFormat = "dd/MM/yyyy HH:mm";
        public string StatusText { get; set; }
        public bool IsFilter { get { return Request["hfIsFiter"] == "1"; } }
        public int CurrentPage { get { return IsFilter ? 0 : Request["hfCurrentPage"].ToInt32(0); } }
        public SortAndPage Paging { get; set; }
        protected bool checkUser = true;
        protected bool checkAccess = true;

        protected bool CheckPageAccess(bool redirect) 
        {
            string pageTitle = "Untitled";
            try { pageTitle = Title; } catch { }
            string pagePath = Request.Path;
            if (pagePath.StartsWith("/")) pagePath = pagePath.Substring(1);
            if (pagePath.StartsWith("mobileAdmincash/")) pagePath = pagePath.Substring("mobileAdmincash/".Length);
            var obj = SecurityModule.GetSecuredObject(pageTitle, true);
            if (!obj.HasPermission(Netpay.Infrastructure.Security.PermissionValue.Read))
            {
                if (redirect) Server.Transfer("SecurityError.aspx");
                else return false;
            }
            return true;
        }

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
            if (checkUser)
            {
                if (IsLoggedin) {
                    if (checkAccess) CheckPageAccess(true);
                } else Server.Transfer("Login.aspx", true);
            }
        }
    }
}