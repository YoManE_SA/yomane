﻿<%@ Page Language="C#" AutoEventWireup="true" Title="Wire Confirm" CodeBehind="WireConfirm.aspx.cs" Inherits="mobileAdmincash.WireConfirm" MasterPageFile="~/Site.Master" %>
<asp:Content ContentPlaceHolderID="PageProperies" runat="server">
    <table width="100%" border="0">
        <tr>
            <td valign="top">
                VIEW<br />
                <asp:RadioButtonList runat="server" ID="rblFilter">
                    <asp:ListItem Text=" Unmanaged" Value="NOTAPPROVED" Selected="True" />
                    <asp:ListItem Text=" Approved" Value="APPROVED" />
                    <asp:ListItem Text=" Decliend" Value="DELIEND" />
                </asp:RadioButtonList>
            </td>
            <td valign="top">
                SORT<br />
                <asp:RadioButtonList ID="rblSort" runat="server">
                    <asp:ListItem Text=" Ascending" />
                    <asp:ListItem Text=" Descending" Selected="True" />
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField runat="server" ID="hfWireID" />
    <asp:Literal runat="server" ID="ltNoRowsMessage" Text="No wires found" />
	<table border="0" cellpadding="1" cellspacing="0" id="tblWire" runat="server" width="100%">
		<tr>
			<td valign="top" style="padding-right:10px; width:50%;">
				<table cellpadding="1" cellspacing="0" class="DataTable">
					<tr>
						<td>
							AMOUNT<br />
							<h3><asp:Literal runat="server" ID="ltAmount" /></h3>
						</td>
					</tr>
					<tr>
						<td>
							WIRE NUMBER<br />
							<h3><asp:Literal runat="server" ID="ltWireNumber" /></h3>
						</td>
					</tr>
					<tr>
						<td>
							MERCHANT<br />
							<h3><asp:Literal runat="server" ID="ltCompanyName" /><br />
							<asp:Literal runat="server" ID="ltPayeeName" /></h3>
						</td>
					</tr>
					<tr>
						<td>
							DATE CREATE / DUE<br />
							<h3 style="padding-bottom:0px;"><asp:Literal runat="server" ID="ltCreateDate" /></h3>
							<h3><asp:Literal runat="server" ID="ltDueDate" /></h3>
						</td>
					</tr>
					<tr>
						<td>
							FEE TAKEN<br />
							<h3><asp:Literal runat="server" ID="ltFeeTaken" /></h3>
						</td>
					</tr>
					<tr>
						<td valign="top">
							FEE EXPECTED<br />
							<h3><asp:Literal runat="server" ID="ltFeeExpected" /> </h3>
						</td>
					</tr>
				</table>
			</td>
			<td valign="top" style="padding-left:10px; border-left:1px solid gray;">
				<table cellpadding="1" cellspacing="0" class="DataTable">

					<tr><td class="SecHeading">Balance</td></tr>
					<tr>
						<td>
                            <asp:Literal runat="server" id="ltBalanceNone" Text="No Balance" />
							<asp:Repeater runat="server" ID="rptBalance">
								<HeaderTemplate>
									<table>
										<tr>
											<th style="padding-right:8px;">Expected</th>
											<th>Current</th>
										</tr>
								</HeaderTemplate>
								<ItemTemplate>
									<tr>
										<td style="padding-right:8px;"><%# ((decimal)Eval("Expected")).ToAmountFormat(Eval("CurrencyIso").ToString())%></td>
										<td><%# ((decimal)Eval("Current")).ToAmountFormat(Eval("CurrencyIso").ToString())%></td>
									</tr>
								</ItemTemplate>                
								<FooterTemplate>
									</table>                    
								</FooterTemplate>
							</asp:Repeater>
						</td>
					</tr>
					<tr><td class="SecHeading"><br />Unsettled transactions</td></tr>
					<tr>
						<td>
                            <asp:Literal runat="server" id="ltUnsettledTransactionsNone" Text="No Unsettled Transactions" />
							<asp:Repeater runat="server" ID="rptUnsettledTransactions">
								<HeaderTemplate>
									<table>
										<tr>
											<th style="padding-right:8px;">Count</th>
											<th>Amount</th>
										</tr>
								</HeaderTemplate>
								<ItemTemplate>
									<tr>
										<td style="padding-right:8px;"><%# ((CountAmount)Eval("Value")).Count %></td>
										<td><%# ((CountAmount)Eval("Value")).Amount.ToAmountFormat((int) Eval("Key"))%></td>
									</tr>
								</ItemTemplate>
								<FooterTemplate>
									</table>                    
								</FooterTemplate>
							</asp:Repeater>
						</td>
					</tr>
					<tr><td class="SecHeading"><br />Rolling Reserve<br /></td></tr>
					<tr>
						<td>
                            <asp:Literal runat="server" id="ltRollingReserveNone" Text="No Rolling Reserve" />
							<asp:Repeater runat="server" ID="rptRollingReserve">
								<HeaderTemplate>
									<table>
								</HeaderTemplate>
								<ItemTemplate>
									<tr>
										<td><%#(-((decimal)Eval("Value"))).ToAmountFormat((int)Eval("Key"))%></td>
									</tr>
								</ItemTemplate>
								<FooterTemplate>
									</table>                    
								</FooterTemplate>
							</asp:Repeater>
						</td>
					</tr>
					<tr runat="server" visible="false">
						<td class="SecHeading">Approval</td>
						<td>
							<asp:Literal runat="server" ID="ltWireLevel1Group" />: <asp:Literal runat="server" ID="ltApprovalLevel1" />
							<asp:Literal runat="server" ID="ltWireLevel2Group" />: <asp:Literal runat="server" ID="ltApprovalLevel2" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
    <br />
</asp:Content>
<asp:Content ContentPlaceHolderID="PageBottom" runat="server">
	<asp:Button runat="server" ID="btnConfirm" Text="Confirm" OnClick="Confirm_Click" />
	<asp:Button runat="server" ID="btnDecline" Text="Decline" OnClick="Decline_Click" />
</asp:Content>
