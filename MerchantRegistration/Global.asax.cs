﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace Netpay.MerchantRegistration
{
    public class Global : System.Web.HttpApplication
    {
		protected void Application_Start(object sender, EventArgs e)
        {
			Web.ContextModule.AppName = "Merchant Registration Panel";
        }

        void Application_BeginRequest(object sender, EventArgs e)
        {
            var application = sender as HttpApplication;
            if (application != null && application.Context != null)
            {
                application.Context.Response.Headers.Remove("Server");
            }
        }

        protected void Application_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            Netpay.Web.WebUtils.SelectLanguage(null);
	    }
    }
}