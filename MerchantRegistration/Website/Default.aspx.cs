﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Netpay.Infrastructure;
using Netpay.Web;
using Netpay.Bll;
using Netpay.Bll.ThirdParty;
using System.Web.UI.WebControls;

namespace Netpay.MerchantRegistration
{
    public partial class Default : System.Web.UI.Page
    {
        protected Netpay.Bll.Merchants.Registration DataItem { get; set; }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            //lnkIcon.Href = "../Templates/" & WebUtils.CurrentDomain.ThemeFolder & "/favicon.ico"
            lnkIcon.Href = "../favicon.ico";
            if (!IsPostBack)
            {
                DataItem = new Netpay.Bll.Merchants.Registration();

                //Dictionary<string, string> industries = CardConnect.GetIndustries();
                //ddIndustry.DataSource = industries;
                //ddIndustry.DataValueField = "key";
                //ddIndustry.DataTextField = "value";
                ////ddIndustry.DataBind();

                //Dictionary<string, string> types = CardConnect.GetBusinessTypes();
                //ddTypeOfBusiness.DataSource = types;
                //ddTypeOfBusiness.DataValueField = "key";
                //ddTypeOfBusiness.DataTextField = "value";
                ////ddTypeOfBusiness.DataBind();

                dateBusinessStart.Culture = new System.Globalization.CultureInfo("en-us");
                dateOwnerDob.Culture = new System.Globalization.CultureInfo("en-us");

                DataBind();
            }
        }

        protected void Register(object sender, System.EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }
            DataItem = new Netpay.Bll.Merchants.Registration();
            DataItem.Address = txtAddress.Text.NullIfEmpty();
            DataItem.AnticipatedAverageTransactionAmount = txtAnticipatedAverageTransAmount.Text.ToNullableDecimal();
            DataItem.AnticipatedLargestTransactionAmount = txtAnticipatedLargestTransactionAmount.Text.ToNullableDecimal();
            DataItem.PercentDelivery0to7 = txtPercentDelivery0to7.Text.ToInt32(0);
            DataItem.PercentDelivery15to30 = txtPercentDelivery15to30.Text.ToInt32(0);
            DataItem.PercentDelivery8to14 = txtPercentDelivery8to14.Text.ToInt32(0);
            DataItem.PercentDeliveryOver30 = txtPercentDeliveryOver30.Text.ToInt32(0);
            DataItem.AnticipatedMonthlyVolume = txtAnticipatedMonthlyVol.Text.ToNullableDecimal(); ;
            DataItem.BankAccountNumber = txtBankAccountNumber.Text.NullIfEmpty();
            DataItem.BankRoutingNumber = txtBankRoutingNumber.Text.NullIfEmpty();
            DataItem.BusinessDescription = txtBusinessDescription.Text.NullIfEmpty();
            DataItem.BusinessStartDate = dateBusinessStart.Date;
            DataItem.City = txtCity.Text.NullIfEmpty();
            DataItem.DbaName = txtDbaName.Text.NullIfEmpty();
            DataItem.Email = txtEmail.Text.NullIfEmpty();
            DataItem.FirstName = txtFirstName.Text.NullIfEmpty();
            DataItem.Industry = ddIndustry.SelectedValue.ToInt32();
            DataItem.LastName = txtLastName.Text.NullIfEmpty();
            DataItem.LegalBusinessName = txtLegalBusinessName.Text.NullIfEmpty();
            DataItem.LegalBusinessNumber = txtbuisnessFEIN.Text;
            DataItem.OwnerDob = dateOwnerDob.Date;
            DataItem.OwnerSsn = txtOwnerSsn.Text.NullIfEmpty();
            DataItem.PhisicalAddress = txtPhisicalAddress.Text.NullIfEmpty();
            DataItem.PhisicalCity = txtPhisicalCity.Text.NullIfEmpty();
            DataItem.PhisicalState = ddPhysicalState.ISOCode;
            //txtPhisicalState.Text.NullIfEmpty
            DataItem.PhisicalZip = txtPhisicalZipcode.Text.NullIfEmpty();
            DataItem.Phone = txtPhone.Text.NullIfEmpty();
            DataItem.Fax = txtfax.Text.NullIfEmpty();
            DataItem.State = ddState.ISOCode;
            //txtState.Text.NullIfEmpty
            DataItem.StateOfIncorporation = ddStateOfIncorporation.ISOCode;
            //txtStateOfIncorporation.Text.NullIfEmpty
            DataItem.TypeOfBusiness = ddTypeOfBusiness.SelectedValue.ToInt32();
            DataItem.Url = txtUrl.Text.NullIfEmpty();
            DataItem.Zipcode = txtZipcode.Text.NullIfEmpty();
            DataItem.CanceledCheckImageFileName = fuCanceledCheckImage.PostedFile.FileName;
            DataItem.CanceledCheckImageMimeType = fuCanceledCheckImage.PostedFile.ContentType;
            var strram = fuCanceledCheckImage.PostedFile.InputStream;
            DataItem.CanceledCheckImageContent = new System.IO.BinaryReader(strram).ReadBytes((int)strram.Length).ToBase64();
            DataItem.Save();
            bool result = true;
            if (result)
            {
                SuccessResultMessage.Visible = true;
                SuccessResultMessage.InnerText = "Registration has completed successfully";
                try
                {
                    DataItem.SendRegistrationEmail();
                }
                catch (Exception ex)
                {
                    SuccessResultMessage.InnerText += ", Email not sent - " + ex.Message;
                }
            }
            else
            {
                ErrorResultMessage.Visible = true;
                ErrorResultMessage.InnerText = "Registration failure";
            }
        }
        public void AlertAndFocus(string message, ref TextBox txtField)
        {
            lblError.Text = "<div class=\"error\">" + message + "</div>";
            //dvAlert.Visible = True
            txtField.Focus();
        }
        protected override void OnPreRender(System.EventArgs e)
        {
            var link = new System.Web.UI.HtmlControls.HtmlLink();
            link.Href = "~/Templates/" + WebUtils.CurrentDomain.ThemeFolder + "/WebSite/Styles/login.css";
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            link.Attributes.Add("media", "screen");
            Page.Header.Controls.Add(link);
            if (System.Threading.Thread.CurrentThread.CurrentUICulture.TextInfo.IsRightToLeft)
            {
                link = new System.Web.UI.HtmlControls.HtmlLink();
                link.Href = "~/Templates/" + WebUtils.CurrentDomain.ThemeFolder + "/WebSite/Styles/loginRTL.css";
                link.Attributes.Add("rel", "stylesheet");
                link.Attributes.Add("type", "text/css");
                link.Attributes.Add("media", "screen");
                Page.Header.Controls.Add(link);
            }
            ltCopyright.Text = ((string)GetGlobalResourceObject("default.aspx", "Copyright")).Replace("%BRAND%", WebUtils.CurrentDomain.BrandName);
            base.OnPreRender(e);
        }
    }
}