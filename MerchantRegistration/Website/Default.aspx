﻿<%@ Page Language="C#" AutoEventWireup="True" Inherits="Netpay.MerchantRegistration.Default" CodeBehind="Default.aspx.cs" MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <asp:Localize runat="server" Text="<%$ Resources:default.aspx, Title %>" /></title>
    <link rel="stylesheet" href="js/jquery-ui-1.8.10.custom/css/custom-theme/jquery-ui-1.8.10.custom.css" type="text/css" />
    <link href="Plugins/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <script type="text/javascript" src="js/jquery-ui-1.8.10.custom/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.8.10.custom/js/jquery-ui-1.8.10.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.8.10.custom/js/jquery.ui.datepicker-he.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type='text/css' />
    <link id="lnkIcon" runat="server" rel="icon" type="image/x-icon" />
    <netpay:ScriptManager ID="ScriptManager1" runat="server" EnableSessionAlerts="false" />
</head>
<body>
    <form runat="server">
        <script type="text/javascript">

            var currentSection = 1;
            function next() {
                var validationGroup = null;
                switch (currentSection) {
                    case 1:
                        validationGroup = "signupGroupContactDetails";
                        break;
                    case 2:
                        validationGroup = "signupGroupPersonalInfo";
                        break;
                    case 3:
                        validationGroup = "signupGroupBuisnessInfo";
                        break;
                    case 4:
                        validationGroup = "signupGroupBankDetalis";
                        break;
                    default:
                }

                if (validationGroup == null)
                    return;
                if (!Page_ClientValidate(validationGroup))
                    return;

                currentSection++;
                if (currentSection > 4)
                    currentSection = 4;

                openSection(currentSection);
            }
            function back() {
                currentSection--;
                if (currentSection < 1)
                    currentSection = 1;

                openSection(currentSection);
            }

            function openSection(sectionId) {
                for (var idx = 1; idx <= 4; idx++) {
                    if (idx == sectionId) {
                        $("#formSection" + idx).slideDown("slow");
                        $("#sectionArrow" + idx).attr("class", "fa fa-chevron-down");
                        $("#image_step" + idx).attr("src", "../Templates/<%=WebUtils.CurrentDomain.ThemeFolder %>/Images/step-" + idx + ".png");

                    }
                    else {
                        $("#formSection" + idx).slideUp("slow");
                        $("#sectionArrow" + idx).attr("class", "fa fa-chevron-right");
                        $("#image_step" + idx).attr("src", "../Templates/<%=WebUtils.CurrentDomain.ThemeFolder %>/Images/step-" + idx + "-off.png");
                    }
                }
            }

        </script>

        <div class="wrapper" runat="server">
            <div class="language-selector">
                <ul>
                    <li><a class="hyperlink" href="javascript:window.location = netpay.Common.setURLValue(window.location.href, 'culture', 'en-us')"><i class="fa fa-globe"><span class="font-arial"> English</span></i></a></li>
                    <li><a class="hyperlink" href="javascript:window.location = netpay.Common.setURLValue(window.location.href, 'culture', 'he-il')"><i class="fa fa-globe"><span class="font-arial"> עברית</span></i></a></li>
                </ul>
            </div>
            <div class="clearfix"></div>

            <div class="wrap-login-box">
                <div class="login-box">
                    <header class="header-top">
                        <div class="pull-left max-width-50 ">
                            <img alt="Logo LTD" src="../Templates/<%=WebUtils.CurrentDomain.ThemeFolder %>/images/logo.png" />
                        </div>
                       <div class="pull-right max-width-50">
                            <img alt="Logo LTD" src="../Tmp_MahalaUS/Images/logo-customer.png" />
                        </div>
                        <div class="spacer"></div>
                    </header>
                    <div>
                        <hr class="style-line margin-10-top" />
                    </div>
                    <section id="signUp">
                          <div id="ErrorResultMessage" visible="false" class="error margin-10-top" runat="server"></div>
                        <div id="SuccessResultMessage" visible="false" class="alert-info margin-10-top" runat="server"></div>
                        <asp:Label ID="lblError" CssClass="margin-10-top" style="display: block;" Visible="false" runat="server" />
                        <div class="padding-10 align-center ">
                            <img id="image_step1" class="padding-10-both" src="../Templates/<%=WebUtils.CurrentDomain.ThemeFolder %>/Images/step-1.png" />
                            <img id="image_step2" class="padding-10-both" src="../Templates/<%=WebUtils.CurrentDomain.ThemeFolder %>/Images/step-2-off.png" />
                            <img id="image_step3"  class="padding-10-both" src="../Templates/<%=WebUtils.CurrentDomain.ThemeFolder %>/Images/step-3-off.png" />
                            <img id="image_step4" class="padding-10-both" src="../Templates/<%=WebUtils.CurrentDomain.ThemeFolder %>/Images/step-4-off.png" />
                        </div>
                        <hr class="style-line" />
                        <div id="section-one" class="block-head margin-10-top">
                            <div class="pull-left">
                                <asp:Localize runat="server" Text="<%$ Resources:default.aspx, Contact %>" />
                            </div>
                            <div class="pull-right">
                                <i id="sectionArrow1" class="fa fa-chevron-down"></i>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div id="formSection1" runat="server">

                            <div class="line-height-30 margin-10-top">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, DBA %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="txtDbaName" autofocus="autofocus" type='text' runat="server" class="width-100-precent" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupContactDetails" Display="Dynamic" ControlToValidate="txtDbaName" Text="<%$ Resources:default.aspx, DBA %>" runat="server"></asp:RequiredFieldValidator>
                                </div>
                            </div>


                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, Lbusiness %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox type='text' ID="txtLegalBusinessName" runat="server" class="width-100-precent" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupContactDetails" Display="Dynamic" ControlToValidate="txtLegalBusinessName" Text="<%$ Resources:default.aspx, Lbusiness %>" runat="server" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, Phone %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox type='text' ID="txtPhone" runat="server" class="width-100-precent" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupContactDetails" Display="Dynamic" ControlToValidate="txtPhone" Text="<%$ Resources:default.aspx, Phone %>" runat="server" />
                                    <asp:RegularExpressionValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupContactDetails" Display="Dynamic" ControlToValidate="txtPhone" runat="server" ValidationExpression="\d+" ErrorMessage="Invalid Phone Numbers only" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, Url %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox type='text' ID="txtUrl" runat="server" class="width-100-precent" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupContactDetails" Display="Dynamic" ControlToValidate="txtUrl" Text="<%$ Resources:default.aspx, Url %>" runat="server" />
                                    <asp:RegularExpressionValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupContactDetails" Display="Dynamic" ControlToValidate="txtUrl" runat="server"  ValidationExpression="([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?" ErrorMessage="Invalid Url e.g: www.example.com" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, Padress %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox type='text' ID="txtPhisicalAddress" runat="server" class="width-100-precent" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupContactDetails" Display="Dynamic" ControlToValidate="txtPhisicalAddress" Text="<%$ Resources:default.aspx, Padress %>" runat="server" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, Pcity %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox type='text' ID="txtPhisicalCity" runat="server" class="width-100-precent" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupContactDetails" Display="Dynamic" ControlToValidate="txtPhisicalCity" Text="<%$ Resources:default.aspx, Pcity %>" runat="server" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, Pstate %>" runat="server" />
                                </div>
                                <div>
                                    <netpay:StateDropDown ID="ddPhysicalState" EnableBlankSelection="false" CssClass="width-100-precent" CountryID="228" runat="server" />
                                </div>
                                <div></div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, Pzipcode %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox type='text' ID="txtPhisicalZipcode" runat="server" class="width-100-precent" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupContactDetails" Display="Dynamic" ControlToValidate="txtPhisicalZipcode" Text="<%$ Resources:default.aspx, Pzipcode %>" runat="server" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, State %>" runat="server" />
                                </div>
                                <div>
                                    <netpay:StateDropDown ID="ddStateOfIncorporation" CssClass="width-100-precent" EnableBlankSelection="false" CountryID="228" runat="server" />
                                </div>
                                <div></div>
                            </div>
                            <div class="line-height-30 margin-10-top align-right ">
                                <button class="btn btn-primary" type="button" onclick="next();">
                                    <asp:Literal Text="<%$ Resources:default.aspx, NextValid %>" runat="server" /></button>
                            </div>
                        </div>

                        <div id="click-selesction-01" class="block-head margin-10-top">
                            <div class="pull-left">
                                <asp:Literal Text="<%$ Resources:default.aspx, Personalinf %>" runat="server" />
                            </div>
                            <div class="pull-right">
                                <i id="sectionArrow2" class="fa fa-chevron-right"></i>
                            </div>
                            <div class="clearfix"></div>

                        </div>

                        <div id="formSection2" style="display: none">
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, Fname %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="txtFirstName" type='text' runat="server" class="width-100-precent" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupPersonalInfo" Display="Dynamic" ControlToValidate="txtFirstName" Text="<%$ Resources:default.aspx, Fname %>" runat="server" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, Lname %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="txtLastName" type='text' runat="server" class="width-100-precent" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupPersonalInfo" Display="Dynamic" ControlToValidate="txtLastName" Text="<%$ Resources:default.aspx, Lname %>" runat="server" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, Phone %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="txtPhoneNew" type='text' runat="server" class="width-100-precent" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupPersonalInfo" Display="Dynamic" ControlToValidate="txtPhoneNew" Text="<%$ Resources:default.aspx, Phone %>" runat="server" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, Fax %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="txtfax" type='text' runat="server" class="width-100-precent" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupPersonalInfo" Display="Dynamic" ControlToValidate="txtfax" Text="<%$ Resources:default.aspx, Fax %>" runat="server" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, Email %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="txtEmail" type='email' class="width-100-precent" runat="server" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupPersonalInfo" Display="Dynamic" ControlToValidate="txtEmail" Text="<%$ Resources:default.aspx, Email %>" runat="server" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, Address %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="txtAddress" type='text' class="width-100-precent" runat="server" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupPersonalInfo" Display="Dynamic" ControlToValidate="txtAddress" Text="<%$ Resources:default.aspx, Address %>" runat="server" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, City %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="txtCity" type='text' class="width-100-precent" runat="server" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupPersonalInfo" Display="Dynamic" ControlToValidate="txtCity" Text="<%$ Resources:default.aspx, City %>" runat="server" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, Stateabr %>" runat="server" />
                                </div>
                                <div>
                                    <netpay:StateDropDown ID="ddState" EnableBlankSelection="false" CssClass="width-100-precent" CountryID="228" runat="server" />
                                </div>
                                <div></div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, Zipcode %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="txtZipcode" class="width-100-precent" type='text' runat="server" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupPersonalInfo" Display="Dynamic" ControlToValidate="txtZipcode" Text="<%$ Resources:default.aspx, Zipcode %>" runat="server" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, OwnerDate %>" runat="server" />
                                </div>
                                <div>
                                    <netpay:DatePicker ClientIDMode="Static" ID="dateOwnerDob" ShowMonth="true" CssClass="width-100-precent" ShowYear="true" runat="server" />
                                </div>
                                <div></div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, OwnerSSN %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="txtOwnerSsn" type='text' runat="server" class="width-100-precent" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupPersonalInfo" Display="Dynamic" ControlToValidate="txtOwnerSsn" Text="<%$ Resources:default.aspx, OwnerSSN %>" runat="server" />
                                    <asp:RegularExpressionValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupPersonalInfo" Display="Dynamic" ControlToValidate="txtOwnerSsn" runat="server" ValidationExpression="\d{3}-\d{2}-\d{4}" ErrorMessage="Invalid SSN (XXX-XX-XXXX)" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top">
                                <div class="pull-right">
                                    <button class="btn btn-primary" type="button" onclick="next();">
                                        <asp:Literal Text="<%$ Resources:default.aspx, NextValid %>" runat="server" /></button>
                                </div>
                                <div class="pull-left">
                                    <button class="btn btn-primary" type="button" onclick="back();">
                                        <asp:Literal ID="Literal2" Text="<%$ Resources:default.aspx, BackButton %>" runat="server" /></button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div id="click-selesction-02" class="block-head margin-10-top">

                            <div class="pull-left">
                                <asp:Literal Text="<%$ Resources:default.aspx, Buisnessinfo %>" runat="server" />
                            </div>
                            <div class="pull-right">
                                  <i id="sectionArrow3" class="fa fa-chevron-right"></i>
                            </div>
                            <div class="clearfix"></div>


                        </div>
                        <div id="formSection3" style="display: none">
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, TypeBuisness %>" runat="server" />
                                </div>
                                <div>
                                    <asp:DropDownList ID="ddTypeOfBusiness" CssClass="width-100-precent" runat="server" onchange="ValidatorEnable(document.getElementById('FEINValidator'), (options[selectedIndex].value != '1'));">
                                        <asp:ListItem Value="1" Text="<%$ Resources:default.aspx, SoleProprietor %>"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="<%$ Resources:default.aspx, Partnership %>"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="<%$ Resources:default.aspx, Corporation %>"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="<%$ Resources:default.aspx, LLC %>"></asp:ListItem>
                                        <asp:ListItem Value="5" Text="<%$ Resources:default.aspx, LP %>"></asp:ListItem>
                                        <asp:ListItem Value="6" Text="<%$ Resources:default.aspx, Other %>"></asp:ListItem>
                                        <asp:ListItem Value="7" Text="<%$ Resources:default.aspx, Association %>"></asp:ListItem>
                                        <asp:ListItem Value="8" Text="<%$ Resources:default.aspx, EstateTrust %>"></asp:ListItem>
                                        <asp:ListItem Value="10" Text="<%$ Resources:default.aspx, Government %>"></asp:ListItem>
                                        <asp:ListItem Value="11" Text="<%$ Resources:default.aspx, TaxExempt %>"></asp:ListItem>
                                        <asp:ListItem Value="12" Text="<%$ Resources:default.aspx, PublicCorporation %>"></asp:ListItem>
                                        <asp:ListItem Value="13" Text="<%$ Resources:default.aspx, PrivateCorporation %>"></asp:ListItem>
                                        <asp:ListItem Value="21" Text="<%$ Resources:default.aspx, NonProfitOrg %>"></asp:ListItem>
                                        <asp:ListItem Value="22" Text="<%$ Resources:default.aspx, School %>"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, BuisnessS %>" runat="server" />
                                </div>
                                <div>
                                    <netpay:DatePicker CssClass="width-100-precent" ID="dateBusinessStart" ShowYear="true" ShowMonth="true" runat="server" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, Industry %>" runat="server" /></div>
                                <div>
                                    <asp:DropDownList ID="ddIndustry" CssClass="width-100-precent" runat="server">
                                        <asp:ListItem Value="2" Text="Auto Repairs Dealers"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Retail Furniture"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Retail Supermarkets"></asp:ListItem>
                                        <asp:ListItem Value="5" Text="Lodging"></asp:ListItem>
                                        <asp:ListItem Value="6" Text="Wholesale"></asp:ListItem>
                                        <asp:ListItem Value="7" Text="Moto"></asp:ListItem>
                                        <asp:ListItem Value="8" Text="Retail Apparel"></asp:ListItem>
                                        <asp:ListItem Value="9" Text="Auto Rentals"></asp:ListItem>
                                        <asp:ListItem Value="10" Text="Restaurants"></asp:ListItem>
                                        <asp:ListItem Value="11" Text="Services Education"></asp:ListItem>
                                        <asp:ListItem Value="12" Text="Auto Gas Stations"></asp:ListItem>
                                        <asp:ListItem Value="13" Text="Retail Stores"></asp:ListItem>
                                        <asp:ListItem Value="14" Text="Services Professional"></asp:ListItem>
                                        <asp:ListItem Value="15" Text="Services Other"></asp:ListItem>
                                        <asp:ListItem Value="16" Text="Healthcare"></asp:ListItem>
                                        <asp:ListItem Value="17" Text="Services Entertainment"></asp:ListItem>
                                        <asp:ListItem Value="18" Text="Services Utilities Telecom"></asp:ListItem>
                                        <asp:ListItem Value="19" Text="Airlines"></asp:ListItem>
                                        <asp:ListItem Value="20" Text="Services Contracted"></asp:ListItem>
                                        <asp:ListItem Value="21" Text="Auto Transportation Services"></asp:ListItem>
                                        <asp:ListItem Value="22" Text="Services Government"></asp:ListItem>
                                        <asp:ListItem Value="23" Text="Services Personal"></asp:ListItem>
                                        <asp:ListItem Value="24" Text="Non-Profit"></asp:ListItem>
                                        <asp:ListItem Value="25" Text="Other"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, Businessdescription %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="txtBusinessDescription" class="width-100-precent" type='text' runat="server" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="signupGroupBuisnessInfo" CssClass="RequiredFieldValidator" Display="Dynamic" ControlToValidate="txtBusinessDescription" Text="<%$ Resources:default.aspx, Businessdescription %>" runat="server" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, FEIN %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="txtbuisnessFEIN" ClientIDMode="Static" runat="server" class="width-100-precent" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator ID="FEINValidator" ValidationGroup="signupGroupBuisnessInfo" CssClass="RequiredFieldValidator" Display="Dynamic" ControlToValidate="txtbuisnessFEIN" Text="<%$ Resources:default.aspx, FEIN %>" runat="server" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, Monthlyamount %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="txtAnticipatedMonthlyVol" class="width-100-precent" runat="server" />
                                </div>
                                <div>

                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupBuisnessInfo" Display="Dynamic" ControlToValidate="txtAnticipatedMonthlyVol" Text="<%$ Resources:default.aspx, Monthlyamount %>" runat="server" />
                                    <asp:RegularExpressionValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupBuisnessInfo" Display="Dynamic" ValidationExpression="^[1-9]\d*(\.\d+)?$" ControlToValidate="txtAnticipatedMonthlyVol" ErrorMessage="<%$ Resources:default.aspx, InvalidNumber %>" runat="server" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, AverageTransaction %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="txtAnticipatedAverageTransAmount" class="width-100-precent" type='text' runat="server" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupBuisnessInfo" Display="Dynamic" ControlToValidate="txtAnticipatedAverageTransAmount" Text="<%$ Resources:default.aspx, AverageTransaction %>" runat="server" />
                                    <asp:RegularExpressionValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupBuisnessInfo" Display="Dynamic" ValidationExpression="^[1-9]\d*(\.\d+)?$" ControlToValidate="txtAnticipatedAverageTransAmount" ErrorMessage="<%$ Resources:default.aspx, InvalidNumber %>" runat="server" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, LargestTransaction %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="txtAnticipatedLargestTransactionAmount" class="width-100-precent" type='text' runat="server" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupBuisnessInfo" Display="Dynamic" ControlToValidate="txtAnticipatedLargestTransactionAmount" Text="<%$ Resources:default.aspx, LargestTransaction %>" runat="server" />
                                    <asp:RegularExpressionValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupBuisnessInfo" Display="Dynamic" ValidationExpression="^[1-9]\d*(\.\d+)?$" ControlToValidate="txtAnticipatedLargestTransactionAmount" ErrorMessage="<%$ Resources:default.aspx, InvalidNumber %>" runat="server" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, PercentDelivery0to7 %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="txtPercentDelivery0to7" class="width-100-precent" type='text' runat="server" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupBuisnessInfo" Display="Dynamic" ControlToValidate="txtPercentDelivery0to7" Text="<%$ Resources:default.aspx, RequiredField %>" runat="server" />
                                    <asp:RegularExpressionValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupBuisnessInfo" Display="Dynamic" ValidationExpression="^(100|[0-9]?[0-9])$" ControlToValidate="txtPercentDelivery0to7" ErrorMessage="<%$ Resources:default.aspx, InvalidPercent %>" runat="server" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, PercentDelivery8to14 %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="txtPercentDelivery8to14" class="width-100-precent" type='text' runat="server" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupBuisnessInfo" Display="Dynamic" ControlToValidate="txtPercentDelivery8to14" Text="<%$ Resources:default.aspx, RequiredField %>" runat="server" />
                                    <asp:RegularExpressionValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupBuisnessInfo" Display="Dynamic" ValidationExpression="^(100|[0-9]?[0-9])$" ControlToValidate="txtPercentDelivery8to14" ErrorMessage="<%$ Resources:default.aspx, InvalidPercent %>" runat="server" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, PercentDelivery15to30 %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="txtPercentDelivery15to30" class="width-100-precent" type='text' runat="server" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupBuisnessInfo" Display="Dynamic" ControlToValidate="txtPercentDelivery15to30" Text="<%$ Resources:default.aspx, RequiredField %>" runat="server" />
                                    <asp:RegularExpressionValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupBuisnessInfo" Display="Dynamic" ValidationExpression="^(100|[0-9]?[0-9])$" ControlToValidate="txtPercentDelivery15to30" ErrorMessage="<%$ Resources:default.aspx, InvalidPercent %>" runat="server" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, PercentDeliveryOver30 %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox ID="txtPercentDeliveryOver30" class="width-100-precent" type='text' runat="server" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupBuisnessInfo" Display="Dynamic" ControlToValidate="txtPercentDeliveryOver30" Text="<%$ Resources:default.aspx, RequiredField %>" runat="server" />
                                    <asp:RegularExpressionValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupBuisnessInfo" Display="Dynamic" ValidationExpression="^(100|[0-9]?[0-9])$" ControlToValidate="txtPercentDeliveryOver30" ErrorMessage="<%$ Resources:default.aspx, InvalidPercent %>" runat="server" />

                                </div>
                            </div>
                                  <div class="line-height-30 margin-10-top ">
                                <div class="pull-right">
                                    <button  class="btn btn-primary" type="button" onclick="next();">
                                        <asp:Literal ID="Literal3" Text="<%$ Resources:default.aspx, NextValid %>" runat="server" /></button>
                                </div>
                                <div class="pull-left">
                                    <button class="btn btn-primary" type="button" onclick="back();">
                                        <asp:Literal ID="Literal4" Text="<%$ Resources:default.aspx, BackButton %>" runat="server" /></button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div id="click-selesction-03" class="block-head margin-10-top">
                            <div class="pull-left">
                                <asp:Literal ID="Literal1" Text="<%$ Resources:default.aspx, BankDetalis %>" runat="server" />
                            </div>
                            <div class="pull-right">
                                 <i id="sectionArrow4" class="fa fa-chevron-right"></i>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div id="formSection4" style="display: none">
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, BankRouting %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox class="width-100-precent" ID="txtBankRoutingNumber" type='text' runat="server" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupBankDetalis" Display="Dynamic" ControlToValidate="txtBankRoutingNumber" Text="<%$ Resources:default.aspx, BankRouting %>" runat="server" />
                                    <asp:RegularExpressionValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupBankDetalis" Display="Dynamic" ControlToValidate="txtBankRoutingNumber" runat="server" ValidationExpression="^\d+?$" ErrorMessage="Please enter a number" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, BankAccount %>" runat="server" />
                                </div>
                                <div>
                                    <asp:TextBox  class="width-100-precent" ID="txtBankAccountNumber" type='text' runat="server" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupBankDetalis" Display="Dynamic" ControlToValidate="txtBankAccountNumber" Text="<%$ Resources:default.aspx, BankAccount %>" runat="server" />
                                    <asp:RegularExpressionValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupBankDetalis" Display="Dynamic" ControlToValidate="txtBankAccountNumber" runat="server" ValidationExpression="^\d+?$" ErrorMessage="Please enter a number" />
                                </div>
                            </div>
                            <div class="line-height-30 margin-10-top ">
                                <div>
                                    <asp:Literal Text="<%$ Resources:default.aspx, CanceledCheckImage %>" runat="server" />
                                </div>
                                <div>
                                    <asp:FileUpload ID="fuCanceledCheckImage" runat="server" />
                                </div>
                                <div>
                                    <asp:RequiredFieldValidator CssClass="RequiredFieldValidator" ValidationGroup="signupGroupBankDetalis" Display="Dynamic" ControlToValidate="fuCanceledCheckImage" Text="<%$ Resources:default.aspx, CanceledCheckImage %>" runat="server" />
                                </div>
                            </div>
                             <div class="line-height-30 margin-10-top ">
                                <div class="pull-right">
                                    <asp:Button class="btn btn-primary" ID="btnRegister" OnClick="Register" OnClientClick="return Page_ClientValidate('signupGroupBankDetalis')" Text="<%$ Resources:default.aspx, Register %>" runat="server" />
                                </div>
                                <div class="pull-left">
                                    <button class="btn btn-primary" type="button" onclick="back();">
                                        <asp:Literal ID="Literal5" Text="<%$ Resources:default.aspx, BackButton %>" runat="server" /></button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
            <div>
                <img src="../Templates/<%=WebUtils.CurrentDomain.ThemeFolder %>/images/shaddow.png" />
            </div>
            <!-- <div id="Btnlogin" class="wrap-account margin-10-bottom">
                <asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:default.aspx, HaveAccount %>" />
                <span class="padding-5-left"><i class="fa fa-hand-o-right"></i></span>
                <a href="login.aspx" class="hyperlink" id="myButton4"><asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:default.aspx, Loginhere %>" /></a>
            </div> -->
             
            <div class="push"></div>
        </div>
        <footer class="footer">
            <div class="wrap-footer">
                <div class="grid-1">
                    <img src="../Templates/<%=WebUtils.CurrentDomain.ThemeFolder %>/images/main-logo-white.png" />

                </div>
                <div class="grid-2">
                    <asp:Localize ID="ltCopyright" runat="server" Text="<%$ Resources:default.aspx, Copyright %>" />
                </div>
                <div class="grid-3">
                    <ul>
                        <%--  
                        <li><a class="footer-link" href="http://www.netpay-intl.co.il/en/about/contact-us/">  <asp:Localize runat="server" Text="<%$ Resources:Login.aspx,  txtContactUs %>" /></a></li>
                        <li><a class="footer-link" href="http://www.netpay-intl.co.il/en/map/privacy-policy/">   <asp:Localize runat="server" Text="<%$ Resources:Login.aspx, txtPolicyPrivacy %>" /></a></li>
                        --%>
                     
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </footer>
    </form>
</body>
</html>




























