function addEvent(obj, strEvent, objFunction)
{ 
	if (obj.addEventListener)
	{ 
		obj.addEventListener(strEvent, objFunction, true); 
		return true; 
	}
	else if (obj.attachEvent)
	{ 
		var returnValue = obj.attachEvent("on"+strEvent, objFunction); 
		return returnValue; 
	} 
	else return false; 
}

	function getAbsPos(xObj){
		var lx = 0, ly = 0;
		while(xObj){
			lx += xObj.offsetLeft;
			ly += xObj.offsetTop;
			xObj = xObj.offsetParent;
		}
		return {x:lx, y:ly};
	}

	function PosObject(xObj, xRefObj, sPos){
		var lPos = getAbsPos(xRefObj);
		switch(sPos.substring(0, 2)){
			case 'LL': break;
			case 'LR': lPos.x -= xObj.clientWidth; break;
			case 'RL': lPos.x += xRefObj.clientWidth; break;
			case 'RR': lPos.x += (xRefObj.clientWidth - xObj.clientWidth); break;
		}
		switch(sPos.substring(2, 4)){
			case 'TT': break;
			case 'TB': lPos.y -= xObj.clientHeight; break;
			case 'BT': lPos.y += xRefObj.clientHeight; break;
			case 'BB': lPos.y += (xRefObj.clientHeight - xObj.clientHeight); break;
		}
		xObj.style.left = lPos.x + 'px'; xObj.style.top = lPos.y + 'px';
	}

	function isChildOf(nParentObj, nChildObj){
		while(nChildObj && (nChildObj != document.body)){
			if(nParentObj == nChildObj) return true;
			nChildObj = nChildObj.parentNode;
		}
		return false;
	}

	var G_MENU_STACK = null;
	function trackMenu(e){
		e = (e || window.event);
		showMenu(null, e.relatedTarget ? e.relatedTarget : e.toElement, null);
	}

	function showMenu(xObj, xParent, sPos){
		while(G_MENU_STACK && !isChildOf(G_MENU_STACK, xParent)){
			G_MENU_STACK.style.visibility = 'hidden';
			G_MENU_STACK = G_MENU_STACK._MenuPrev;
		}
		if(!xObj) return;
		xObj._MenuPrev = G_MENU_STACK; G_MENU_STACK = xObj;
		xObj._MenuParent = xParent;
		PosObject(xObj, xParent, sPos);
		addEvent(xObj, 'mouseout', trackMenu);
		addEvent(xParent, 'mouseout', trackMenu);
		xObj.style.visibility = '';
	}

