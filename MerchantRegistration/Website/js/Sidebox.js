﻿jQuery(function ($) {
    $(document).ready(function () {
        $('#panelHandle').hover(function () {
            $('#sidePanel').stop(true, false).animate({
                'right': '0px'
            }, 900);
        }, function () {
            //jQuery.noConflict();
        }); 

        jQuery('#sidePanel').hover(function () {
            // Do nothing
        }, function () {
            //jQuery.noConflict();
            jQuery('#sidePanel').animate({
                right: '-124px'
            }, 800);

        });
    });
});