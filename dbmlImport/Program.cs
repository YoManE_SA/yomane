using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace dbmlImport
{
    class Program
    {
        static XmlNamespaceManager nsmgr = null;

        static string getItemName(string name)
        {
            int i = name.LastIndexOf('.');
            if (i < 0) return name;
            return name.Substring(i + 1);
        }

        static string makeMany(string name) 
        {
            name = makeSingle(name);
            if (name.EndsWith("ss") || name.EndsWith("ch")) return name + "es";
            else if (name.EndsWith("y") && !(name.EndsWith("iy") || name.EndsWith("ey") || name.EndsWith("ay"))) return name.Substring(0, name.Length - 1) + "ies";
            else if (!name.EndsWith("s")) return name + "s";
            return name;
        }

        static string makeSingle(string name)
        {
            if (name.EndsWith("ies")) return name.Substring(0, name.Length - 3) + "y";
            else if (name.EndsWith("s") && !(name.EndsWith("ss") || name.EndsWith("us"))) return name.Substring(0, name.Length - 1);
            return name;
        }

        static string makeStartLower(string name)
        {
            if (string.IsNullOrEmpty(name)) return null;
            if ((name[0] >= 'A' && name[0] <= 'Z') && !(name[1] >= 'A' && name[1] <= 'Z')) 
                return name[0].ToString().ToLower() + name.Substring(1);
            return name;
        }
        
        static void ProcessXmlNode(XmlElement xe) 
        {
            switch (xe.LocalName) { 
                case "Table":
                    xe.SetAttribute("Member", makeMany(getItemName(xe.GetAttribute("Name"))));
                    break;
                case "Type":
                    {
                        string orName = xe.GetAttribute("Name");
                        string newName = makeSingle(getItemName((xe.ParentNode as XmlElement).GetAttribute("Name")));
                        xe.SetAttribute("Name", newName);
                        var nodeList = xe.OwnerDocument.DocumentElement.SelectNodes("//ml:Association[@Type='" + orName + "']", nsmgr);
                        foreach (XmlNode n in nodeList)
                            n.Attributes["Type"].Value = newName;
                    }
                    break;
                case "Column":
                    string strName = xe.GetAttribute("Name");
                    if (strName.Equals(xe.GetAttribute("Member"), StringComparison.InvariantCultureIgnoreCase)) {
                        var nodeList = xe.ParentNode.SelectNodes("ml:Association[@ThisKey='" + xe.GetAttribute("Member") + "']", nsmgr);
                        foreach (XmlNode n in nodeList)
                            n.Attributes["ThisKey"].Value = strName;
                        xe.Attributes.RemoveNamedItem("Member");
                    }
                    var pkValue = xe.GetAttribute("IsPrimaryKey");
                    if (pkValue == null || pkValue.ToLower() != "true") xe.SetAttribute("UpdateCheck", "Never");
                    break;
                case "Association":
                    int curIndex = 0;
                    string ascName = makeStartLower(xe.GetAttribute("Member"));
                    if (xe.GetAttribute("IsForeignKey") != "true") ascName = makeMany(ascName);
                    while (xe.ParentNode.SelectSingleNode("ml:Association[@Member='" + ascName + (curIndex > 0 ? curIndex.ToString() : "") + "']", nsmgr) != null)
                        curIndex++;
                    ascName += (curIndex > 0 ? curIndex.ToString() : "");
                    xe.SetAttribute("Member", ascName);

                    string sRefType = xe.GetAttribute("Type");
                    string stridName = xe.OwnerDocument.DocumentElement.SelectSingleNode("//ml:Type[translate(@Name, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')='" + sRefType.ToLower() + "']/ml:Column[translate(@Name, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')='" + xe.GetAttribute("OtherKey").ToLower() + "']", nsmgr).Attributes["Name"].Value;
                    xe.SetAttribute("OtherKey", stridName);

                    xe.Attributes.RemoveNamedItem("DeleteOnNull");
                    break;
                case "Function":
                    xe.Attributes.RemoveNamedItem("HasMultipleResults");
                    break;
            }
            foreach (var e in xe.ChildNodes)
                if (e is XmlElement) ProcessXmlNode(e as XmlElement);
        }
        
        static void Main(string[] args)
        {
            if(args.Length < 2) {
                Console.Write("Usage dbmlImport <databaseName> <netpayDir>\r\n");
                return;
            }

            // dbmlImport GW_OBLDEV Netpay C:\Dev\Netpay\Projects\Dal\ serialization
            string extParams = "";
            string databaseName = args[0];
			string className = args[1];
            string targetPath = args[2];
            if (!targetPath.EndsWith("\\")) targetPath += "\\";
            string dbmlFileName = targetPath + className + @".dbml";
			string codeFileName = targetPath + className + @".designer.cs";
            const string dbUsername = "sa";
            const string dbPassword = @"#ptah@101";
            const string serverName = @"DESKTOP-PEM83AR\SQL14";
            if (args.Length > 3 && args[3] == "serialization") extParams = " /serialization:Unidirectional";

            System.Diagnostics.Process process = new System.Diagnostics.Process(); 
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.UseShellExecute = false;
            startInfo.FileName = @"C:\Program Files (x86)\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.6.1 Tools\SqlMetal.exe";
            var argumentString = SetArguments(serverName, databaseName, dbUsername, dbPassword, codeFileName, dbmlFileName);
            startInfo.Arguments = @"/server:DESKTOP-PEM83AR\SQL14 /database:" + databaseName + " /user:sa /password:#ptah@101 /views /functions /sprocs /language:csharp /namespace:Netpay.Dal." + className + " /context:" + className + "DataContextBase /pluralize" + extParams + " /dbml:\"" + dbmlFileName+"\"";
            Console.Write("Start {0} {1}\r\n", startInfo.FileName, startInfo.Arguments);
			process.StartInfo = startInfo;
            process.Start();
            process.WaitForExit();

            XmlDocument doc = new XmlDocument();
            doc.Load(dbmlFileName);
            nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("xsl", "http://www.w3.org/1999/XSL/Transform");
            nsmgr.AddNamespace("ml", "http://schemas.microsoft.com/linqtosql/dbml/2007");

            foreach (var e in doc.DocumentElement.ChildNodes)
                if (e is XmlElement) ProcessXmlNode(e as XmlElement);
            doc.Save(dbmlFileName);

			startInfo.Arguments = @"/views /functions /sprocs /language:csharp /namespace:Netpay.Dal." + className + " /context:" + className + "DataContextBase" + extParams + " /code:\"" + codeFileName + "\" \"" + dbmlFileName+"\"";
			Console.Write("\r\nStart {0} {1} \r\n", startInfo.FileName, startInfo.Arguments);
			process.Start();
            process.WaitForExit();
            //Console.ReadKey();
            Console.Write("Refresh DBML Completed\r\nPress any key to exit...");
            Console.ReadLine();
        }

        private static string SetArguments(string serverName,string databaseName,string dbUsername, string dbPassword, string codeFileName, string dbmlFileName )
        {
            var argumentString = string.Format("/server:{0} /database:{1} /user:{2}/password:{3} /views /functions /sprocs /language:csharp /namespace:Netpay.Dal.Netpay /context:NetpayDataContextBase /pluralize /dbml:\"{4}\" ",
                serverName,databaseName,dbUsername,dbPassword,dbmlFileName);
            return argumentString;
        }
    }
}
